const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const TerserPlugin = require('terser-webpack-plugin');

const isAnalyst = 'analyst' === process.env.npm_lifecycle_event;

module.exports = {
    entry: {
        vContractReport: './src/pages/contract/contract.report.js',
        vContractUpdate: './src/pages/contract/index.js',
        vContractManipulationLockBox: './src/pages/contract/contract.manipulationLockBox.js',

        vCustomerCrud: './src/pages/customers/crud.js',

        vCustomersAdminIndexPage: './src/pages/customers/admin.index.js',
        vCustomersAdminUpdatePage: './src/pages/customers/admin.update.js',

        vContractReceiptsAdminIndexPage: './src/pages/contract/receipts.index.js',

        vGoogleadsAdminIndexPage: './src/pages/googleads/admin.index.js',
        vGoogleadsAdminViewPage: './src/pages/googleads/admin.view.js',
        vGoogleadsFixJoinContract: './src/pages/googleads/admin.fixJoinContract.js',

        vFacebookadsAdminIndexPage: './src/pages/facebookads/admin.index.js',
        vFacebookadsAdminViewPage: './src/pages/facebookads/admin.view.js',
        vFacebookadsFixJoinContract: './src/pages/facebookads/admin.fixJoinContract.js',

        vZaloadsAdminIndexPage: './src/pages/zaloads/admin.index.js',
        vZaloadsAdminViewPage: './src/pages/zaloads/admin.view.js',

        vContractPrintable: './src/pages/contract/printable.js',
        vOneWebIndexBkp: './src/pages/oneweb/index.js',
        vFbAdsSetting: './src/pages/facebookads/setting.js',
        vServicePrice1Web: './src/components/vServicePrice1Web.vue',

        vOnewebConfigurationContractBox: './src/pages/oneweb/configurationContractBox.js',
        vPushdyConfigurationContractBox: './src/pages/pushdy/configurationContractBox.js',

        vGoogleadsConfigurationBox: './src/pages/googleads/configurationBox.js',
        vGoogleadsConfigurationReJoinBox: './src/pages/googleads/configurationReJoinBox.js',
        vGoogleadsConfigurationStopBox: './src/pages/googleads/configurationStopBox.js',
        vGoogleadsConfigurationContractBox: './src/pages/googleads/configurationContractBox.js',
        vGoogleadsConfigurationContractEditPageBox: './src/pages/googleads/configurationContractEditPageBox.js',
        vGoogleadsConfigurationContractCuratorsEditPageBox: './src/pages/googleads/configurationContractCuratorsEditPageBox.js',
        vGoogleadsConfigurationContractServiceFeePlanBox: './src/pages/googleads/configurationContractServiceFeePlanBox.js',
        vGoogleadsConfigurationServiceBox2: './src/pages/googleads/configurationServiceBox2.js',
        vGoogleadsConfigurationUnjoinContract: './src/pages/googleads/configurationUnjoinContract.js',
        vGoogleConfigurationAssignedKpi: './src/pages/googleads/configurationAssignedKpi.js',
        vGoogleadsContractManipulationLockBox: './src/pages/googleads/contractManipulationLockBox.js',
        vGoogleUtilities: './src/pages/googleads/utilities.js',
        vGoogleadsConfigurationServiceViewBox: './src/pages/googleads/configurationServiceViewBox.js',

        vTiktokadsAdminIndexPage: './src/pages/tiktokads/admin.index.js',
        vTiktokadsAdminViewPage: './src/pages/tiktokads/admin.view.js',
        vTiktokadsConfigurationBox: './src/pages/tiktokads/configurationBox.js',
        vTiktokadsConfigurationStopBox: './src/pages/tiktokads/configurationStopBox.js',
        vTiktokadsConfigurationContractBox: './src/pages/tiktokads/configurationContractBox.js',
        vTiktokadsConfigurationContractEditPageBox: './src/pages/tiktokads/configurationContractEditPageBox.js',
        vTiktokadsConfigurationContractCuratorsEditPageBox: './src/pages/tiktokads/configurationContractCuratorsEditPageBox.js',
        vTiktokadsConfigurationKpiBox: './src/pages/tiktokads/configurationKpiBox.js',
        vTiktokConfigurationAssignedKpi: './src/pages/tiktokads/configurationAssignedKpi.js',
        vTiktokadsConfigurationReJoinBox: './src/pages/tiktokads/configurationReJoinBox.js',
        vTiktokadsConfigurationUnjoinContract: './src/pages/tiktokads/configurationUnjoinContract.js',
        vTiktokadsFixJoinContract: './src/pages/tiktokads/admin.fixJoinContract.js',
        vTiktokadsContractManipulationLockBox: './src/pages/tiktokads/contractManipulationLockBox.js',
        vTiktokadsConfigurationServiceViewBox: './src/pages/tiktokads/configurationServiceViewBox.js',

        vZaloadsConfigurationBox: './src/pages/zaloads/configurationBox.js',
        vZaloadsConfigurationStopBox: './src/pages/zaloads/configurationStopBox.js',
        vZaloadsConfigurationContractBox: './src/pages/zaloads/configurationContractBox.js',
        vZaloadsConfigurationContractEditPageBox: './src/pages/zaloads/configurationContractEditPageBox.js',
        vZaloadsConfigurationContractCuratorsEditPageBox: './src/pages/zaloads/configurationContractCuratorsEditPageBox.js',
        vZaloadsConfigurationKpiBox: './src/pages/zaloads/configurationKpiBox.js',
        // vZaloadsConfigurationServiceBox: './src/pages/zaloads/configurationServiceBox.js',
        vZaloConfigurationAssignedKpi: './src/pages/zaloads/configurationAssignedKpi.js',

        vFacebookadsConfigurationBox: './src/pages/facebookads/configurationBox.js',
        vFacebookadsConfigurationReJoinBox: './src/pages/facebookads/configurationReJoinBox.js',
        vFacebookadsConfigurationStopBox: './src/pages/facebookads/configurationStopBox.js',
        vFacebookadsConfigurationContractBox: './src/pages/facebookads/configurationContractBox.js',
        vFacebookadsConfigurationContractEditPageBox: './src/pages/facebookads/configurationContractEditPageBox.js',
        vFacebookadsConfigurationContractCuratorsEditPageBox: './src/pages/facebookads/configurationContractCuratorsEditPageBox.js',
        vFacebookadsConfigurationSpentBox: './src/pages/facebookads/configurationSpentBox.js',
        vFacebookadsConfigurationContractServiceFeePlanBox: './src/pages/facebookads/configurationContractServiceFeePlanBox.js',
        vFacebookadsConfigurationUnjoinContract: './src/pages/facebookads/configurationUnjoinContract.js',
        vFacebookadsContractManipulationLockBox: './src/pages/facebookads/contractManipulationLockBox.js',
        vFacebookConfigurationAssignedKpi: './src/pages/facebookads/configurationAssignedKpi.js',
        vFacebookadsConfigurationServiceViewBox: './src/pages/facebookads/configurationServiceViewBox.js',

        vCourseadsConfigurationBox: './src/pages/courseads/configurationBox.js',
        vCourseadsIndex: './src/pages/courseads/courseads.index.js',

        vAdStorageAdminIndexPage: './src/pages/contract/adstorage.index.js',
        vAdStorageDatawarehouseAdminIndexPage: './src/pages/contract/adstorage.datawarehouse.index.js',
        
        vStaffIndex: './src/pages/staffs/admin.index.js',

        vOneadIndex: './src/pages/onead/onead.index.js',
        vOnewebIndex: './src/pages/oneweb/oneweb.index.js',
        vDomainIndex: './src/pages/domain/domain.index.js', 
        vHostingIndex: './src/pages/hosting/hosting.index.js',
        vContractAuditIndex: './src/pages/contract/contract.audit.js',

        vServiceManagementIndex: './src/pages/service_management/service_management.index.js',

        // GWS
        vGwsContractConfiguration: './src/pages/gws/contract.configuration.js',
        vGwsAdminIndexPage: './src/pages/gws/admin.index.js',

        // Example builder
        vUiBuilder: './src/pages/builder/index.js',
        vUIBuilderGroup: './src/pages/builder/group.js',
        vUiBuilderManager: './src/pages/builder/manager.js',
    },
    module: {
        rules: [
        {
            test: /\.vue$/,
            loader: 'vue-loader',
            options: {
                loaders: {
                    js: 'babel-loader'
                }
            }
        },
        {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: [{
                loader: "babel-loader",
            }]
        },
        {
            test: /\.css$/,
            loader:'css-loader'
        }
        ]
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src/'),
        }
    },
    output: {	
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    watchOptions: {
        ignored: /node_modules/
    },
    plugins: [
        new VueLoaderPlugin(),
        new BundleAnalyzerPlugin({
            analyzerMode: isAnalyst ? 'server' : 'disabled',
        }),
    ],
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all',
                },
            },        
        },
        runtimeChunk: 'single',
        minimize: true,
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    format: {
                        comments: false
                    }
                },
                extractComments: false
            })
        ]
    }
};
