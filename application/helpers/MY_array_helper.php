<?php

/**
 * Redirect to previous page if http referer is set. Otherwise to server root.
 */
if ( ! function_exists('array_merge_keep_key'))
{
    function array_merge_keep_key()
    {
        $args   = func_get_args();
        $result = array();

        foreach ((array) $args as $arg)
        {
            if(empty($arg)) continue;

            $result += (array) $arg;
        }

        return $result;
    }
}

/* End of file MY_array_helper.php */
/* Location: ./application/helpers/MY_array_helper.php */