<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
function is_module($module_name = '')
{
	if(!$module_name)
		return (CI::$APP->router->fetch_module() != '');
	return  CI::$APP->router->fetch_module() == $module_name;
}

function module_url($url = '')
{
	$module_name =  CI::$APP->router->fetch_module();
	return admin_url().$module_name.'/'.$url;
}

function is_module_active($module = '')
{
	$modules_active = get_active_modules();
	return isset($modules_active[$module]);
}

function get_active_modules()
{
	return get_instance()->option_m->get_value('module_active', TRUE);
}