<?php defined('BASEPATH') OR exit('No direct script access allowed');

function get_log_meta_value($log_id = 0, $meta_value ='')
{
	return get_instance()->logmeta_m->get_meta_value($log_id, $meta_value);
}

function update_log_meta($log_id,$meta_key,$meta_value,$prev_value = ''){

	return get_instance()->logmeta_m->update_meta( $log_id, $meta_key, $meta_value, $prev_value);		
}

function get_term_meta( $post_id, $key = '', $single = FALSE , $as_value = FALSE)
{
	return get_instance()->termmeta_m->get_meta($post_id, $key, $single, $as_value);
}

function get_term_meta_value($term_id = 0, $meta_value ='')
{
	return get_instance()->termmeta_m->get_meta_value($term_id, $meta_value);
}

function update_term_meta($term_id,$meta_key,$meta_value,$prev_value = '', $event = ''){

	return get_instance()->termmeta_m->update_meta( $term_id, $meta_key, $meta_value, $prev_value, $event);
}

function get_post_meta_value($post_id = 0, $meta_value ='')
{
	return get_instance()->postmeta_m->get_meta_value($post_id, $meta_value);
}

function update_post_meta($post_id,$meta_key,$meta_value,$prev_value = ''){

	return get_instance()->postmeta_m->update_meta( $post_id, $meta_key, $meta_value, $prev_value);		
}

function get_user_meta( $user_id, $key = '', $single = FALSE , $as_value = FALSE)
{
	return get_instance()->usermeta_m->get_meta($user_id, $key, $single, $as_value);
}

function get_user_meta_value($user_id = 0, $meta_value ='')
{
	return get_instance()->usermeta_m->get_meta_value($user_id, $meta_value);
}

function update_user_meta($user_id,$meta_key,$meta_value,$prev_value = ''){

	return get_instance()->usermeta_m->update_meta( $user_id, $meta_key, $meta_value, $prev_value);		
}

function get_api_user_meta_value($auser_id = 0, $meta_value ='')
{
	return get_instance()->api_usermeat_m->get_meta_value($auser_id, $meta_value);
}

function update_api_user_meta($auser_id,$meta_key,$meta_value,$prev_value = ''){

	return get_instance()->api_usermeat_m->update_meta( $auser_id, $meta_key, $meta_value, $prev_value);		
}