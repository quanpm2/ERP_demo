<?php 

function money_round($amount = 1, $step = 1000)
{
    return round(div($amount,$step))*$step;
}

function money_convert2vnd($amount = 1, $from = 'USD', $to ='VND',$rate = 1)
{
    //http://api.exchangeratelab.com/api/convert?apikey=EF88355119B80FE10E101190737B2CDC&from=USD&to=VND&amount=1

    $rate = 1;
    if($from == 'USD')
        $rate = 22340;
    
    if($from == 'AUD')
        $rate = 16605;

    return $amount*$rate;
}

function my_date_format($time = null) {
    if (null == $time) $time = time();
    $day_of_week = array( 'Chủ nhật' , 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy' );

    return @$day_of_week[date('w', $time)] .', '. date('d/m/Y | H:i ', $time);
}

function numberformat($number =0, $decimals = 0 , $dec_point = "." ,  $thousands_sep = "," ) { 
    if(!$number || $number == 0)
        return 0;
    return number_format($number,$decimals,$dec_point ,$thousands_sep); 
}

function currency_numberformat($number =0,$currency = ' VNĐ', $decimals = 0){
    return numberformat($number, $decimals, '.','.') . $currency;
}

function my_week_name($time = null) {
	if (null == $time) 
		$time = time();
	$day_of_week = array(
		'Chủ nhật' , 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy'
     );
	
	return @$day_of_week[date('w', $time)];
}

// rewiretURL tiếng việt
function create_slug($str) {

    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/i", 'a', $str);

    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/i", 'e', $str);

    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/i", 'i', $str);

    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/i", 'o', $str);

    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/i", 'u', $str);

    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/i", 'y', $str);

    $str = preg_replace("/(đ)/i", 'd', $str);

    
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);

    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);

    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);

    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);

    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);

    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);

    $str = preg_replace("/(Đ)/", 'D', $str);

    $str = strtolower($str);
    // sử dụng cho ai có mục đích rewriteURL
    $str = str_replace(" ", "-", str_replace("&*#39;","",$str));

    $str = url_title($str, 'dash', true);
    return $str;
}

function human_timing($from, $to = '', $label = 'trước'){
    if($to == '')
        $to = time();
    if($from == '')
        $from = time();
    $label = ' '.$label;
    $time = $to - $from; // to get the time since that moment
    $max_year = 94608000; //3 năm trước
    if($time <10 ) return 'vài giây'.$label;
    if($time >$max_year) return 'đã lâu lắm rồi';
    $tokens = array (
        31536000 => 'năm',
        2592000 => 'tháng',
        604800 => 'tuần',
        86400 => 'ngày',
        3600 => 'giờ',
        60 => 'phút',
        1 => 'giây'
        );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?$label:$label);
    }
}

function array_merge_key($array_1 = array(), $array_default = array())
{
    $arr_temp = array_merge($array_default, $array_1);
    return array_intersect_key($arr_temp, $array_default);
}

function iif($expression, $returntrue, $returnfalse = '')
{
    return ($expression ? $returntrue : $returnfalse);
}
function key_value($datas, $key_name, $value_name)
{
    $result = array();
    if(!empty($datas))
    {
        $is_object = isset($datas[0]) && is_object($datas[0]);
        if($is_object)
        {
            foreach($datas as $data)
            {
                $result[$data->{$key_name}] = $data->{$value_name};
            }
        }
        else
        {
            foreach($datas as $data)
            {
                if(!is_array($data))
                    $data = (array)$data;
                $result[$data[$key_name]] = $data[$value_name];
            }
        }
    }
    return $result;
}

function key_array_value($datas, $key_name, $value_name= FALSE)
{
    $result = array();
    if(!empty($datas) && is_array($datas))
    {
        $is_object =  isset($datas[0]) && is_object($datas[0]);
        if($is_object)
        {
            foreach($datas as $data)
            {
                $r = $data->{$key_name};
                if(!isset($result[$r]))
                    $result[$r] = array();
                $result[$r][] = ($value_name === FALSE) ? $data: $data->{$value_name};
            }
        }
        else
        {
            foreach($datas as $data)
            {
                if(!is_array($data))
                    $data = (array)$data;
                $r = $data[$key_name];
                if(!isset($result[$r]))
                    $result[$r] = array();
                $result[$r][] = ($value_name === FALSE) ? $data: $data[$value_name];
            }
        }
    }
    return $result;
}

function myNumber_format($number, $fractional=false) { 
    if ($fractional) { 
        $number = sprintf('%.2f', $number); 
    } 
    while (true) { 
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1.$2', $number); 
        if ($replaced != $number) { 
            $number = $replaced; 
        } else { 
            break; 
        } 
    } 
    return $number; 
}

function array_same_key($args = array(), $default = array()) {
    $args = array_merge($default, $args);
    return array_intersect_key($args, $default);
}


function mb_ucwords($str)
{
    return mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
}

function my_date($time=0, $format ='Y/m/d', $name ='')
{
    if($time ==0)
        $time = time();
    return date($format, $time);
}
/**
 * Validate URL
 *
 * @access    public
 * @param    string
 * @return    string
 */
function valid_url($url)
{
    return filter_var($url, FILTER_VALIDATE_URL);
}

// --------------------------------------------------------------------

/**
 * Real URL
 *
 * @access    public
 * @param    string
 * @return    string
 */
function real_url($url)
{
    return @fsockopen("$url", 80, $errno, $errstr, 30);
}

function show_frontend_404($msg = '')
{
    show_404();
}
/**
 * Merge user defined arguments into defaults array.
 *
 * This function is used throughout WordPress to allow for both string or array
 * to be merged into another array.
 *
 * @since 2.2.0
 *
 * @param string|array $args     Value to merge with $defaults
 * @param array        $defaults Optional. Array that serves as the defaults. Default empty.
 * @return array Merged user defined values with defaults.
 */
function wp_parse_args( $args, $defaults = '' ) {
    if ( is_object( $args ) )
        $r = get_object_vars( $args );
    elseif ( is_array( $args ) )
        $r =& $args;
    else
        wp_parse_str( $args, $r );

    if ( is_array( $defaults ) )
        return array_merge( $defaults, $r );
    return $r;
}

function wp_parse_str( $string, &$array ) {
    parse_str( $string, $array );
    if ( get_magic_quotes_gpc() )
        $array = stripslashes_deep( $array );
            // $array = apply_filters( 'wp_parse_str', $array );
}

/**
 * @ignore
 */
function __() {}

function absint( $maybeint ) {
    return abs( intval( $maybeint ) );
}

function _db(){
    
    $ci =& get_instance();
    prd($ci->db->last_query());
}

function audit($event = 'created', $type = null, $id = null, $field = null, $new = null, $old = null, $user_id = null, $user_type = 'erp')
{
    if(is_array($event))
    {
        $data = $event;
        foreach ($data as $row)
        {
            audit($row['event'] ?? 'created', $row['type'] ?? null, $row['id'] ?? null, $row['field'] ?? null, $row['new'] ?? '', $row['old'] ?? '', $row['user_id'] ?? null, $row['user_type'] ?? 'erp');
        }

        return true;
    }

    $ci =& get_instance();

    $payload = array(
        'event' => $event,
        'auditable_type' => $type,
        'auditable_id' => $id,
        'auditable_field' => $field,
        'new_values' => $new,
        'old_values' => $old ?? '',
        'user_id' => (int) $user_id ?: $ci->admin_m->id,
        'user_type' => $user_type, 
        'ip_address' => $ci->input->ip_address() ?? null,
        'user_agent' => $ci->input->user_agent(true),
        'timestamp' => time()
    );

    $attemp         = 0;
    $max_attemps    = 10;

    while($attemp < $max_attemps)
    {
        try
        {
            $rabbitMQ = new \AdsService\MessageQueue\RabbitMQ();
            $rabbitMQ->publish('maintenance_service', $payload);
            $attemp = 5;
            break;
        }
        catch (\Exception $e)
        {
            $attemp++;
        }
    }

    return true;
}

function copy_directory( $source, $destination ) {
    if ( is_dir( $source ) ) {
        @mkdir( $destination );
        $directory = dir( $source );
        while ( FALSE !== ( $readdirectory = $directory->read() ) ) {
            if ( $readdirectory == '.' || $readdirectory == '..' ) {
                continue;
            }
            $PathDir = $source . '/' . $readdirectory; 
            if ( is_dir( $PathDir ) ) {
                copy_directory( $PathDir, $destination . '/' . $readdirectory );
                continue;
            }
            copy( $PathDir, $destination . '/' . $readdirectory );
        }

        $directory->close();
    }else {
        copy( $source, $destination );
    }
}

function arrayUnique($array, $preserveKeys = false)  
{  
    // Unique Array for return  
    $arrayRewrite = array();  
    // Array with the md5 hashes  
    $arrayHashes = array();  
    foreach($array as $key => $item) {  
        // Serialize the current element and create a md5 hash  
        $hash = md5(serialize($item));  
        // If the md5 didn't come up yet, add the element to  
        // to arrayRewrite, otherwise drop it  
        if (!isset($arrayHashes[$hash])) {  
            // Save the current element hash  
            $arrayHashes[$hash] = $hash;  
            // Add element to the unique Array  
            if ($preserveKeys) {  
                $arrayRewrite[$key] = $item;  
            } else {  
                $arrayRewrite[] = $item;  
            }  
        }  
    }  
    return $arrayRewrite;  
}  

/** 
 * Force a var to be an array.
 *
 * @param Var
 * @return Array
**/
function force_array($array = null){
    
    return (is_array($array) ? $array : array());
}

function prepare_dropdown($datsource = array(), $title = 'Tất cả'){

    if(!is_array($title)) $title = array('' => $title);

    if(empty($datsource)) return $title; 

    return array_replace($title, $datsource);
}

function checkdomain($url){

    $validation = FALSE;
    /*Parse URL*/
    $urlparts = parse_url(filter_var($url, FILTER_SANITIZE_URL));
    /*Check host exist else path assign to host*/
    if(!isset($urlparts['host'])){
        $urlparts['host'] = $urlparts['path'];
    }

    if(empty($urlparts['host'])){
       /*Add scheme if not found*/
       return $validation;
    }

    if (!isset($urlparts['scheme'])){
        $urlparts['scheme'] = 'http';
    }

    /*Validation*/
    if(!in_array($urlparts['scheme'],array('http','https'))){ 
        return $validation;
    }

    $urlparts['host'] = preg_replace('/^www\./', '', $urlparts['host']);

    $hostparts = explode('.',$urlparts['host']);

    if(empty($hostparts) && is_array($hostparts)){
        return $validation;
    }

    $located = end($hostparts);

    if(count($hostparts) < 2 || empty($located)) return $validation = FALSE;
    

    $url = $urlparts['scheme'].'://'.$urlparts['host']. "/";            
    
    if (filter_var($url, FILTER_VALIDATE_URL) !== false) {
        return $validation = TRUE;
    }
    
    return $validation;
}

function dateDifference($begin =false, $end =false, $prefix_past = 'Trễ ', $prefix_future = 'Còn lại ')
{    
    if(!$begin || !$end) return FALSE;

    $diff_timestamp  = $end - $begin;

    $past = FALSE;

    if($diff_timestamp < 0){

        $past = TRUE;   

        $diff_timestamp = absint($diff_timestamp);
    } 

    $time_rounds = array (
    31536000 => 'năm',
    2592000 => 'tháng',
    604800 => 'tuần',
    86400 => 'ngày',
    3600 => 'giờ',
    60 => 'phút',
    1 => 'giây'
    );

    $result = '';

    foreach ($time_rounds as $key => $value) {

        if($diff_timestamp < $key) continue;

        $var = floor($diff_timestamp / $key);

        $diff_timestamp = $diff_timestamp - $key*$var;

        $result .= $var . ' ' . $value . ' ';
    }

    return ($past ? $prefix_past : $prefix_future) .$result;
}

//chuyển thành ký tự không dấu
function convert_utf82ascci($string = '')
{
    $string  = create_slug($string);
    return str_replace('-', ' ', $string);
}

function force_var($input, $default = 0, $result = ''){

    if($result !== ''){

        return empty($input) ? $default : $result;
    }

    return empty($input) ? $default : $input;
}

function money_to_string(){
    $amount = 6595000;

    $vnd_round = array (
    31536000 => 'năm',
    2592000 => 'tháng',
    604800 => 'tuần',
    86400 => 'ngày',
    3600 => 'giờ',
    60 => 'phút'
    );
}

function convert_number_to_words($number) {
 
    $hyphen      = ' ';
    $conjunction = ' ';
    $separator   = ' ';
    $negative    = 'âm ';
    $decimal     = ' phẩy ';
    $dictionary  = array(
    0                   => 'Không',
    1                   => 'Một',
    2                   => 'Hai',
    3                   => 'Ba',
    4                   => 'Bốn',
    5                   => 'Năm',
    6                   => 'Sáu',
    7                   => 'Bảy',
    8                   => 'Tám',
    9                   => 'Chín',
    10                  => 'Mười',
    11                  => 'Mười một',
    12                  => 'Mười hai',
    13                  => 'Mười ba',
    14                  => 'Mười bốn',
    15                  => 'Mười năm',
    16                  => 'Mười sáu',
    17                  => 'Mười bảy',
    18                  => 'Mười tám',
    19                  => 'Mười chín',
    20                  => 'Hai mươi',
    30                  => 'Ba mươi',
    40                  => 'Bốn mươi',
    50                  => 'Năm mươi',
    60                  => 'Sáu mươi',
    70                  => 'Bảy mươi',
    80                  => 'Tám mươi',
    90                  => 'Chín mươi',
    100                 => 'trăm',
    1000                => 'ngàn',
    1000000             => 'triệu',
    1000000000          => 'tỷ',
    1000000000000       => 'nghìn tỷ',
    1000000000000000    => 'ngàn triệu triệu',
    1000000000000000000 => 'tỷ tỷ'
    );
     
    if (!is_numeric($number)) {
        return false;
    }
     
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
    // overflow
    trigger_error(
    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
    E_USER_WARNING
    );
    return false;
    }
     
    if ($number < 0) {
    return $negative . convert_number_to_words(abs($number));
    }
     
    $string = $fraction = null;
     
    if (strpos($number, '.') !== false) {
    list($number, $fraction) = explode('.', $number);
    }
     
    switch (true) {
    case $number < 21:
    $string = $dictionary[$number];
    break;
    case $number < 100:
    $tens   = ((int) ($number / 10)) * 10;
    $units  = $number % 10;
    $string = $dictionary[$tens];
    if ($units) {
    $string .= $hyphen . $dictionary[$units];
    }
    break;
    case $number < 1000:
    $hundreds  = $number / 100;
    $remainder = $number % 100;
    $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
    if ($remainder) {
    $string .= $conjunction . convert_number_to_words($remainder);
    }
    break;
    default:
    $baseUnit = pow(1000, floor(log($number, 1000)));
    $numBaseUnits = (int) ($number / $baseUnit);
    $remainder = $number % $baseUnit;
    $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
    if ($remainder) {
    $string .= $remainder < 100 ? $conjunction : $separator;
    $string .= convert_number_to_words($remainder);
    }
    break;
    }
     
    if (null !== $fraction && is_numeric($fraction)) {
    $string .= $decimal;
    $words = array();
    foreach (str_split((string) $fraction) as $number) {
    $words[] = $dictionary[$number];
    }
    $string .= implode(' ', $words);
    }
     
    return $string;
}

function currency_number_to_words($number,$currency = ' đồng ',$is_uc_first = true){
    $str = convert_number_to_words($number) . $currency;

    if($is_uc_first) $str = mb_ucfirst($str,'UTF-8',true);

    return $str;
}

function cal_vat($cash = 0, $vat = 10){
    return $cash*(1 + $vat/100);
}

function diffInMonths($startDate = 0, $endDate = 0)
{
    if($endDate == 0)
        $endDate = time();

    // $endDate = strtotime("+1 day",$endDate);

   return abs((date('Y', $endDate) - date('Y', $startDate))*12 + (date('m', $endDate) - date('m', $startDate)));
}

function diffInDates($startDate = 0, $endDate = 0){
    
    if($endDate == 0) $endDate = time();
    $endDate = strtotime("+1 day",$endDate);

    return abs(($endDate-$startDate)/86400);
}

function div($dividend,$divisor)
{
    if(empty($divisor)) return 0;
    return $dividend/$divisor;
}

function phone_replace($phone_string = '')
{
    $phone = preg_replace('/\s+/', '', $phone_string);
    $phone = str_replace('+84(0)', '0', $phone);
    $phone = str_replace('+084(0)', '0', $phone);

    $phone = str_replace('+84', '0', $phone);
    $phone = str_replace('(84)', '0', $phone);
    $phone = str_replace('(848)', '08', $phone);
    $phone = str_replace('(84-8)', '08', $phone);
    $phone = str_replace('(84-08)', '08', $phone);

    if(in_array(substr($phone, 0,4), array('84-8','(08)')))
    {
        $phone = substr_replace($phone,'08',0,4);
    }
    if(substr($phone, 0,3) == '848'){
        $phone = substr_replace($phone,'08',0,3);
    }

    return $phone;
}

function convert_pattern($phone_string = '')
{
    $phone = phone_replace($phone_string);

    $i=0;
    $phone_formated = '';
    $size = strlen($phone);
    for ($i; $i < $size; $i++) 
        $phone_formated.=(int)$phone[$i];
    $j=0;
    $result = '';
    $size = strlen($phone) ;
    for ($j; $j < $size; $j++){
        if($phone_formated[$j] == $phone[$j])
            $result.=$phone_formated[$j];
    }
    if(strlen($result) > 11){
        $delimiters = array('-','/');
        $result = str_replace($delimiters, reset($delimiters), $phone_string);
        $strings = explode(reset($delimiters), $result);
        $count = count($strings);
        if($count > 1){
            $arg = reset($strings);
            $count = count($strings);
            if($count>2){
                array_pop($strings);
                $arg = implode('-', $strings);
            }

            $result = convert_pattern($arg);
        }
    }

    if(in_array(strlen($result), array(7,8)))
        $result = '08'.$result;

    return $result;
}

function is_mobile($phone = '')
{
    if(empty($phone)) return FALSE;

    $phone = phone_replace($phone);
    $phone = convert_pattern($phone);

    $first_number = substr($phone, 0,1);
    if($first_number=='0'){
        if(in_array(substr($phone, 1,1),array(1,9))) return TRUE;
        return FALSE;
    }
    else{
        if(in_array($first_number,array(1,9))) return TRUE;
        return FALSE;
    }

    return FALSE;
}

if(!function_exists('mb_ucfirst')){
    function mb_ucfirst($str,$encoding = 'UTF-8',$lower_str_end = false){

        $first_letter = mb_strtoupper(mb_substr($str,0,1,$encoding),$encoding);
        $str_end = '';
        
        if($lower_str_end){
            $str_end = mb_strtolower(mb_substr($str, 1,mb_strlen($str,$encoding),$encoding),$encoding);
        }
        else {
            $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }

        return $first_letter . $str_end;
    }
}

function in_array_case_insensitive($needle, $haystack) 
{
    return in_array( strtolower($needle), array_map('strtolower', $haystack) );
}

function pingomatic($url,$title)
{
    $this->load->library('xmlrpc');
    $this->xmlrpc->server('http://rpc.pingomatic.com/', 80);
    $this->xmlrpc->method('weblogUpdates.ping');

    $result = array('flerror'=>1,'message'=>'');

    $request = array($title, $backlink->blink_referral);
    $this->xmlrpc->request($request);

    if (!$this->xmlrpc->send_request()){
        $result['message'] = $this->xmlrpc->display_error();
        return $result;
    }
    
    $result = $this->xmlrpc->display_response();
    return $result;
}

function convert_sth_to_string($input)
{
    $output = '';

    if(is_array($input))
        $output.= implode('.', array_map(function($k,$v){
            if(is_array($v)) return $k.'.'.convert_sth_to_string($v);

            return (is_string($k)?$k.'.':'').$v;

        }, array_keys($input),$input));

    else if(is_string($input)) $output.='.'.$input;

    return $output;
}

if ( ! function_exists('array_group_by') )
{
    /**
     * Groups an array by a given key.
     *
     * Groups an array into arrays by a given key, or set of keys, shared between all array members.
     *
     * Based on {@author Jake Zatecky}'s {@link https://github.com/jakezatecky/array_group_by array_group_by()} function.
     * This variant allows $key to be closures.
     *
     * @param array $array   The array to have grouping performed on.
     * @param mixed $key,... The key to group or split by. Can be a _string_,
     *                       an _integer_, a _float_, or a _callable_.
     *
     *                       If the key is a callback, it must return
     *                       a valid key from the array.
     *
     *                       ```
     *                       string|int callback ( mixed $item )
     *                       ```
     *
     * @return array|null Returns a multidimensional array or `null` if `$key` is invalid.
     */
    function array_group_by( array $array, $key )
    {
        if ( ! is_string( $key ) && ! is_int( $key ) && ! is_float( $key ) && ! is_callable( $key ) ) {
            trigger_error( 'array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR );
            return null;
        }
        $func = ( is_callable( $key ) ? $key : null );
        $_key = $key;
        // Load the new array, splitting by the target key
        $grouped = [];
        foreach ( $array as $value ) {
            if ( is_callable( $func ) ) {
                $key = call_user_func( $func, $value );
            } elseif ( is_object( $value ) && isset( $value->{ $_key } ) ) {
                $key = $value->{ $_key };
            } elseif ( isset( $value[ $_key ] ) ) {
                $key = $value[ $_key ];
            } else {
                continue;
            }
            $grouped[ $key ][] = $value;
        }
        // Recursively build a nested grouping if more parameters are supplied
        // Each grouped array value is grouped according to the next sequential key
        if ( func_num_args() > 2 ) {
            $args = func_get_args();
            foreach ( $grouped as $key => $value ) {
                $params = array_merge( [ $value ], array_slice( $args, 2, func_num_args() ) );
                $grouped[ $key ] = call_user_func_array( 'array_group_by', $params );
            }
        }
        return $grouped;
    }
}

if ( ! function_exists('get_exchange_rate') )
{
    function get_exchange_rate($code = 'USD', $term_id = 0)
    {
        if( 'VND' == $code) return 1;

        $exchange_rate = 0;

        // Get exchange_rate by contract id
        if(is_numeric($term_id) && $term_id > 0){
            $meta_key = strtolower("exchange_rate_{$code}_to_vnd");
            $exchange_rate = get_term_meta_value($term_id, $meta_key) ?: 0;
        }

        if($exchange_rate != 0){
            return $exchange_rate;
        }

        // Get exchange_rate from API
        $ci = get_instance();
        $cache_key = 'vietcombank_exchange_rates_' . strtolower($code);
        $sell = $ci->scache->get($cache_key);
        if(empty($sell))
        {
            $sell = 0;

            try 
            {    
                $api_url = 'https://portal.vietcombank.com.vn/Usercontrols/TVPortal.TyGia/pXML.aspx';
                $response = Requests::get($api_url);
                if($response->status_code != 200) return $sell;

                $xml_dat = $response->body;
                $xml_dat = str_replace('(', '', str_replace(')', '', $response->body));
                // $xml_dat = json_decode($xml_dat);

                // $items      = array_column($xml_dat->items, 'bantienmat', 'type');

                // $sell = (double) $items['USD'];
                // $ci->scache->write($sell, $cache_key, 3600);


                $xml_dat = simplexml_load_string($xml_dat);

                if(!$xml_dat) return $sell;

                if(empty($xml_dat->Exrate->count())) return $sell;

                $code = strtoupper($code);
                foreach ($xml_dat->Exrate as $item) 
                {
                    $attributes = $item->attributes();
                    $currency_code = (string)$attributes['CurrencyCode'];
                    
                    if($currency_code != $code) continue;

                    $sell = (double) str_replace(',', '', (string) $attributes['Sell']);
                }
                
                $ci->scache->write($sell,$cache_key,3600);
            } 
            catch (Exception $e) {
                $sell = 0;    
            }            
        }

        return $sell;
    }
}

if ( ! function_exists('get_work_days'))
{
    /**
     * Count the number of working days between two dates
     *
     * This function calculate the number of working days between two given
     * dates, taking account of the Public festivities and the working Saturday.
     *
     * @param      string   $from    Start date ('YYYY-MM-DD' format)
     * @param      string   $to    Ending date ('YYYY-MM-DD' format)
     * @param      boolean  $workSat  TRUE if Saturday is a working day
     *
     * @return     integer  Number of working days ('zero' on error)
     */
    function get_work_days($from, $to, $workSat = FALSE)
    {
        if ( ! defined('SATURDAY')) define('SATURDAY', 6);
        if ( ! defined('SUNDAY')) define('SUNDAY', 0);

        // Array of all public festivities
        $publicHolidays = array('01-01');

        $start = strtotime($from);
        $end   = strtotime($to);
        $workdays = 0;

        for ($i = $start; $i <= $end; $i = strtotime("+1 day", $i))
        {
            $day    = date('w', $i);  // 0=sun, 1=mon, ..., 6=sat
            $mmgg   = date('m-d', $i);

            if ($day != SUNDAY && !in_array($mmgg, $publicHolidays) && !($day == SATURDAY && $workSat == FALSE))
            {
                $workdays++;
            }
        }

        return intval($workdays);
    }
}


if ( ! function_exists('count_weekend_days'))
{
    function count_weekend_days($start_time, $end_time)
    {
        defined('SATURDAY') OR define('SATURDAY', 6);
        defined('SUNDAY')   OR define('SUNDAY', 0);

        $start_time_f   = my_date($start_time, 'Y-m-d H:i:s');
        $end_time_f     = my_date($end_time, 'Y-m-d H:i:s');

        $add_days   = 0;
        $interval   = DateInterval::createFromDateString('1 day');
        $period     = new DatePeriod(date_create($start_time_f), $interval, date_create($end_time_f));
        foreach ( $period as $dt )
        {
            if( ! in_array($dt->format('w'), [SATURDAY, SUNDAY])) continue;
            $add_days++;
        }
        
        return $add_days;
    }
}

if ( ! function_exists('is_serialized'))
{
    function is_serialized( $data, $strict = TRUE )
    {
        // if it isn't a string, it isn't serialized.
        if( ! is_string($data)) return FALSE;

        $data = trim($data);

        if( 'N;' == $data ) return TRUE;

        if( strlen( $data ) < 4 ) return FALSE;

        if( ':' !== $data[1] ) return FALSE;

        if( $strict )
        {
            $lastc = substr( $data, -1 );
            if( ';' !== $lastc && '}' !== $lastc )
            {
                return FALSE;
            }
        } 
        else
        {
            $semicolon = strpos( $data, ';' );
            $brace     = strpos( $data, '}' );
            // Either ; or } must exist.
            if( FALSE === $semicolon && FALSE === $brace ) return FALSE;
            // But neither must be in the first X characters.
            if( FALSE !== $semicolon && $semicolon < 3 ) return FALSE;
            if( FALSE !== $brace && $brace < 4 ) return FALSE;
        }

        $token = $data[0];
        switch ( $token )
        {
            case 's' :
            if( $strict )
            {
                if( '"' !== substr( $data, -2, 1 ) ) return FALSE;
            } 
            elseif( FALSE === strpos( $data, '"' ) ) return FALSE;
            // or else fall through

            case 'a' :
            case 'O' : return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
            case 'b' :
            case 'i' :
            case 'd' : 
                $end = $strict ? '$' : '';
                return (bool) preg_match( "/^{$token}:[0-9.E-]+;$end/", $data );
        }

        return FALSE;
    }
}

if ( ! function_exists('short_full_name'))
{
    function short_full_name($display_name = '')
    {
        if(empty($display_name)) return $display_name;
        
        $segmentName    = explode(' ', $display_name);
        $display_name   = '';

        $count  = count($segmentName);           
        $i      = $count - 1;

        for($i; $i >= 0; $i--)
        {
            $segmentName[$i] = trim($segmentName[$i]);
            if(empty($segmentName[$i])) continue;

            if($i == ($count - 1) || $i == ($count - 2))
            {
                $display_name = $segmentName[$i] .' '. $display_name;
                continue;
            }

            $display_name = mb_substr($segmentName[$i],0,1, 'UTF-8').'.'.$display_name;
        }

        return $display_name;
    }
}

if( ! function_exists('cid'))
{
    /**
     * Khởi tạo mã khách hàng từ ID & Loại Khách hàng
     *
     * @param      integer  $id     The identifier
     * @param      string   $type   The type
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    function cid(int $id, string $type)
    { 
        $cid = get_instance()->usermeta_m->get_meta_value($id, 'cid');
        if( ! empty($cid)) return $cid;
        
        $maxLength  = 6;
        $typeEnum   = [  'customer_person' => ['code' => 'PE'], 'customer_company' => ['code' => 'CO'] ];

        if( ! in_array($type, array_keys($typeEnum))) return null;
        return strtoupper($typeEnum[$type]['code']).str_pad($id, $maxLength, 0, STR_PAD_LEFT);
    }
}

if( ! function_exists('booleanToStr'))
{
    /**
     * Khởi tạo mã khách hàng từ ID & Loại Khách hàng
     *
     * @param      integer  $id     The identifier
     * @param      string   $type   The type
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    function booleanToStr($value)
    {
        return $value ? 'TRUE' : 'FALSE';
    }
}


if( ! function_exists('convertMicroCost'))
{
    /**
     * Khởi tạo mã khách hàng từ ID & Loại Khách hàng
     *
     * @param      integer  $id     The identifier
     * @param      string   $type   The type
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    function convertMicroCost($value)
    {
        return div($value, 1000000);
    }
}


if( ! function_exists('minHtmlContent'))
{
    /**
     * Khởi tạo mã khách hàng từ ID & Loại Khách hàng
     *
     * @param      integer  $id     The identifier
     * @param      string   $type   The type
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    function minHtmlContent($content = null)
    {
        $re = '%            # Collapse ws everywhere but in blacklisted elements.
            (?>             # Match all whitespans other than single space.
              [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
            | \s{2,}        # or two or more consecutive-any-whitespace.
            ) # Note: The remaining regex consumes no text at all...
            (?=             # Ensure we are not in a blacklist tag.
              (?:           # Begin (unnecessary) group.
                (?:         # Zero or more of...
                  [^<]++    # Either one or more non-"<"
                | <         # or a < starting a non-blacklist tag.
                            # Skip Script and Style Tags
                  (?!/?(?:textarea|pre|script|style)\b)
                )*+         # (This could be "unroll-the-loop"ified.)
              )             # End (unnecessary) group.
              (?:           # Begin alternation group.
                <           # Either a blacklist start tag.
                            # Dont foget the closing tags 
                (?>textarea|pre|script|style)\b
              | \z          # or end of file.
              )             # End alternation group.
            )  # If we made it here, we are not in a blacklist tag.
            %ix';

        return preg_replace($re, " ", $content);
    }
}

/**
 * Detect audit
 * 
 * @access public
 * @return string
 */
function detect_audit($value, $config)
{
    if(empty($value)){
        return '';
    }

    switch ($config['type']) {
        case 'timestamp':
            $value = $value ? date('h:i:s d/m/Y', $value) : '';
            break;
        case 'number':
            $value = $value ? number_format((int)$value, 0, ',', '.') : '';
            break;
        case 'percentage':
            $value = $value ? number_format((float)$value * 100, 2, ',', '.') . '%' : '';
            break;
        case 'float_percentage':
            $value = $value ? number_format((float)$value, 2, ',', '.') . '%' : '';
            break;
        case 'ref':
            $ref = $config['ref'];
            
            $ref_type = $ref['type'];
            if($ref_type == 'default' && !empty($ref['model'])){
                $model = $ref['model'];
                $model = get_instance()->$model;

                $data = $model->select($ref['field'])
                    ->where("{$ref['primary_key']}", $value)
                    ->order_by($ref['primary_key'], 'DESC')
                    ->as_array()
                    ->get_by();

                // if($config['key'] == 'adaccounts'){
                //     dd($value, $data);
                // }
                
                $value = $data[$ref['field']];
            }

            // dd(1, $value);
            
            break;
        default:
            break;
    }

    return $value;
}

/**
 * Tin validator
 * 
 * @access public
 * @return string
 */
function tin_validator($value){
    // Validation Syntax with Vietnam TIN
    $isValidTaxVNM = preg_match('/^[0-9a-zA-Z]{10}(-\d{3})?$/', $value, $matches);
    if($isValidTaxVNM) return TRUE;

    // Validation Syntax with Australia TIN
    $isValidTaxAUS = preg_match('/^(((\d *?){11})|([0-9a-zA-Z]{2}-[0-9a-zA-Z]{7}))$/', $value, $matches);
    if($isValidTaxAUS) return TRUE;

    // Validation Syntax with Japan TIN
    $isValidTaxJPN = preg_match('/^[0-9]{12,13}$/', $value, $matches);
    if($isValidTaxJPN) return TRUE;

    // Validation Syntax with Hongkong TIN
    $isValidTaxHKG = preg_match('/^(([A-Z]\d{6}[0-9A])|([0-9]{8}-[0-9A-Z]{3}-[0-9A-Z]{2}-[0-9A-Z]{2}-[0-9A-Z]{1}))$/', $value, $matches);
    if($isValidTaxHKG) return TRUE;

    // Validation Syntax with Singapore TIN
    $isValidTaxSGP = preg_match('/^(([0-9]{8,9}[a-zA-Z])|((F000|F   )[0-9]{5}[a-zA-Z])|((A|4)[0-9]{7}[a-zA-Z])|((20|19)[0-9]{2}(LP|LL|FC|PF|RF|MQ|MM|NB|CC|CS|MB|FM|GS|DP|CP|NR|CM|CD|MD|HS|VH|CH|MH|CL|XL|CX|HC|RP|TU|TC|FB|FN|PA|PB|SS|MC|SM|GA|GB)[0-9]{4}[a-zA-Z]))$/', $value, $matches);
    if($isValidTaxSGP) return TRUE;

    // Validation Syntax with Taiwan TIN
    $isValidTaxTWN = preg_match('/^[0-9]{8}$/', $value, $matches);
    if($isValidTaxTWN) return TRUE;

    return FALSE;
}

/**
 * Phone validator
 * 
 * @access public
 * @return string
 */
function phone_validator($value){
    // Validate VN phone number
    $isValidPhoneVN = preg_match('/^((03|05|07|08|09|01[2|6|8|9]|02[0|1|2|3|4|5|6|7|8|9]{1,2})+([0-9]{8})|(1900+([0-9]{4}|[0-9]{6})))$/', $value, $matches);
    if($isValidPhoneVN) return TRUE;

    // Validate Australia phone number
    $isValidPhoneAUS = preg_match('/^\+61+[0-9]{9}$/', $value, $matches);
    if($isValidPhoneAUS) return TRUE;

    // Validate US phone number
    $isValidPhoneUS = preg_match('/^((001|\+1)+[0-9]{10})$/', $value, $matches);
    if($isValidPhoneUS) return TRUE;

    // Validate South Korea phone number
    $isValidPhoneKR = preg_match('/^((001|\+82)+[0-9]{10})$/', $value, $matches);
    if($isValidPhoneKR) return TRUE;

    // Validate Hong Kong phone number
    $isValidPhoneHK = preg_match('/^((001|\+852)+[0-9]{8})$/', $value, $matches);
    if($isValidPhoneHK) return TRUE;

    // Validate Singapore phone number
    $isValidPhoneSGP = preg_match('/^((001|\+65)+[0-9]{8})$/', $value, $matches);
    if($isValidPhoneSGP) return TRUE;

    // Validate Taiwan phone number
    $isValidPhoneTWN = preg_match('/^((0(2|3|4|5|6|7|8|9)|\+8869)+[0-9]{8})$/', $value, $matches);
    if($isValidPhoneTWN) return TRUE;

    // Validate Poland phone number
    $isValidPhonePOL = preg_match('/^((0048|\+48)+[0-9]{9,10})$/', $value, $matches);
    if($isValidPhonePOL) return TRUE;

    return FALSE;
}

// UUIDv4
function uuidv4() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}