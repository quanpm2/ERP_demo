<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

function admin_url($path = '', $protocol = NULL)
{
	return site_url("admin/{$path}", $protocol);
}

function current_module_url($path = '') {
	
	return site_url()."admin/".$path;
}

function page404() {

	return base_url() . '404.html';
}

function admin_page404() {

	return admin_url() . '404.html';
}

function theme_url($uri = '') {
	return base_url() . 'template/frontend/' . $uri;
}

function admin_theme_url($uri = '')
{
	return base_url("template/admin/{$uri}");
}

function dist_url($uri = '')
{
	return base_url("dist/{$uri}");
}

function prd() {
	echo "<pre style='clear: both; border-top: 1px solid red;'>";
	if (func_get_args()) foreach (func_get_args() as $item) {
		print_r($item);
		echo "<hr />";
	}
	
	$bt = debug_backtrace();
	$caller = array_shift($bt);
	echo "<div style='color:red' >Call from line <strong>".$caller['line'] . "</strong> file " . $caller['file'] ." on server: ".@$_SERVER['SERVER_ADDR']."</div>";
	echo "</pre>";
	die();
}

function dd() {

	$stack = array();

	is_cli() OR $stack[] = "<pre style='clear: both; border-top: 1px solid red;'>";

	if($args = func_get_args())
	{
		foreach($args as $item)
		{
			if( ! is_cli())
			{
				$stack[] = print_r($item, true);
				$stack[] = '<hr />';
				continue;
			}

			$stack[] = $item;
		}
	}

	$bt = debug_backtrace();
	$caller = array_shift($bt);

	if( ! is_cli())
	{
		echo "<pre style='clear: both; border-top: 1px solid red;'>";
		if (func_get_args()) foreach (func_get_args() as $item) {
			print_r($item);
			echo "<hr />";
		}
		
		$bt = debug_backtrace();
		$caller = array_shift($bt);
		echo "<div style='color:red' >Call from line <strong>".$caller['line'] . "</strong> file " . $caller['file'] ." on server: ".@$_SERVER['SERVER_ADDR']."</div>";
		echo "</pre>";
		die();
	}

	echo json_encode($stack, JSON_PRETTY_PRINT);
	die();
}

function module_option_url($module_name, $url = '')
{
	return admin_url().'option/module/'.$module_name.'/'.$url;
}

function image($image_path, $preset = '') {

	if (strpos($image_path, 'ttp://') || strpos($image_path, 'ttps://'))
		return $image_path;

	if($image_path == '')
		return theme_url('img/no-image.jpg?v=1');

	if($preset == '')
		return base_url($image_path);

	$ci = &get_instance();

        // load the allowed image presets
	$ci->load->config("images");
	$sizes = $ci->config->item("image_sizes");

	$new_path = $image_path;
        // check if requested preset exists
	if (isset($sizes[$preset])) {
		$pathinfo = pathinfo($image_path);
		$new_path = @$pathinfo["dirname"] . "/" . @$pathinfo["filename"] . "-" . implode("x", $sizes[$preset]) . "." . @$pathinfo["extension"];
	}
	$new_path = str_replace('images/','image/',$new_path);
	return base_url().$new_path;
}

if ( ! function_exists('active_link'))
{
	function active_link($controller, $action = '')
	{
		$CI =& get_instance();

		$class = $CI->router->fetch_class();
		$method = $CI->router->fetch_method();

		if($action =='')
			return ($class == $controller) ? 'active' : '';

		return ($class == $controller && $method == $action) ? 'active' : '';

	}
}

function post_url($post = null)
{
	return site_url($post->post_slug.'-'.$post->post_id);
}

function term_url($term = NULL, $base_url = FALSE)
{
	$url = '';
	if(!$term) return '#';
	switch ($term->term_type) {
		case 'career':
			$url = 'viec-lam/'.$term->term_slug;
			break;
		case 'company':
			$url = 'cong-ty/'.$term->term_slug;
			break;
		
		default:
			# code...
			break;
	}
	if($base_url)
		return base_url($url);
	return site_url($url);
}

function is_admin()
{
	return get_instance()->uri->segment(1) ==='admin';
}
?>