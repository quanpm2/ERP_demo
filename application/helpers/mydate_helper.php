<?php defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! function_exists('convert_time'))
{
	function convert_time($time = '')
	{
		if($time =='')
		{
			return time();
		}
		$t = strtotime($time);
		if(!is_numeric($time) && $t !== false && $t >=0)
		{
			return $t;
		}
		return $time;
	}
}

if ( ! function_exists('start_of_day'))
{
	function start_of_day($time = '', $to_time = TRUE)
	{
		$time = convert_time($time);
		$date = date('Y-m-d',$time);
		if($to_time)
			return strtotime($date);
		return $date;
	}
}


if ( ! function_exists('end_of_day'))
{
	function end_of_day($time = '', $to_time = TRUE)
	{
		$time = start_of_day($time);
		$time = strtotime('+1 day', $time)-1;
		$date = date('Y-m-d H:i:s',$time);
		if($to_time)
			return $time;
		return $date;
	}
}

if ( ! function_exists('start_of_month'))
{
	function start_of_month($time = '', $to_time = TRUE)
	{
		$time = convert_time($time);
		$time = strtotime(date('Y-m-01',$time));
		if($to_time)
			return $time;
		return date('Y-m-d H:i:s',$time);
	}
}

if ( ! function_exists('end_of_month'))
{
	function end_of_month($time = '', $to_time = TRUE)
	{
		$time = convert_time($time);
		$time = strtotime(date('Y-m-t',$time));
		if($to_time)
			return $time;
		return date('Y-m-d H:i:s',$time);
	}
}

if ( ! function_exists('start_of_year'))
{
	function start_of_year($time = '', $to_time = TRUE)
	{
		$time = convert_time($time);
		$time = strtotime(date('Y-01-01',$time));
		if($to_time)
			return $time;
		return date('Y-m-d H:i:s',$time);
	}
}

if ( ! function_exists('end_of_year'))
{
	function end_of_year($time = '', $to_time = TRUE)
	{
		$time = convert_time($time);
		$time = strtotime(date('Y-12-t',$time));
		if($to_time)
			return $time;
		return date('Y-m-d H:i:s',$time);
	}
}

if ( ! function_exists('get_work_days'))
{
	/**
	 * Count the number of working days between two dates
	 *
	 * This function calculate the number of working days between two given
	 * dates, taking account of the Public festivities and the working Saturday.
	 *
	 * @param      string   $from    Start date ('YYYY-MM-DD' format)
	 * @param      string   $to    Ending date ('YYYY-MM-DD' format)
	 * @param      boolean  $workSat  TRUE if Saturday is a working day
	 *
	 * @return     integer  Number of working days ('zero' on error)
	 */
	function get_work_days($from, $to, $workSat = FALSE)
	{
		if ( ! defined('SATURDAY')) define('SATURDAY', 6);
		if ( ! defined('SUNDAY')) define('SUNDAY', 0);

		// Array of all public festivities
		$publicHolidays = array('01-01','02-14','02-15','02-16','02-17','02-18','02-19','02-20');

		$start = strtotime($from);
		$end   = strtotime($to);
		$workdays = 0;

		for ($i = $start; $i <= $end; $i = strtotime("+1 day", $i))
		{
			$day 	= date('w', $i);  // 0=sun, 1=mon, ..., 6=sat
			$mmgg 	= date('m-d', $i);

			if ($day != SUNDAY && !in_array($mmgg, $publicHolidays) && !($day == SATURDAY && $workSat == FALSE))
			{
				$workdays++;
			}
		}

		return intval($workdays);
	}
}
/* End of file My_date_helper.php */
/* Location: ./application/core/My_date_helper.php */