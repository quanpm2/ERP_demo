<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_Table_Audit_For_Null_New_Values extends CI_Migration {

    public function up()
    {
		$fields = array(
			'new_values' => array(
				'type' => 'text',
				'null' => true
			),
		);

		$this->dbforge->modify_column('audits', $fields);
    }
}