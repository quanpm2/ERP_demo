<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Addition_Ad_Account_Metadata extends CI_Migration {

    public function up()
    {
        $this->load->model('facebookads/adaccount_m');
        
        $adAccounts = $this->adaccount_m->set_term_type()
		->select('term.term_id')
		->select('max(if(meta_key = "currency", meta_value, null)) as currency')
		->select('max(if(meta_key = "isExternal", meta_value, null)) as isExternal')
		->select('max(if(meta_key = "created_time", meta_value, null)) as created_time')
		->select('max(if(meta_key = "owner", meta_value, null)) as owner')
		->join('termmeta', 'termmeta.term_id = term.term_id', 'left')
		->group_by('term.term_id')
		->get_all();

        $userIds = array_map('intval', array_filter(array_column($adAccounts, 'owner')));

		$users = $this->admin_m->select('user_id')->set_get_admin()->get_many($userIds);
		$users AND $users = array_column($users, null, 'user_id');

		foreach($adAccounts as $adAccount)
		{
			if(empty($users[$adAccount->owner]))
			{
				update_term_meta($adAccount->term_id, 'source', 'linked');
				continue;
			}

			update_term_meta($adAccount->term_id, 'source', 'direct');
		}
    }
}