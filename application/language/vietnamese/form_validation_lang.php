<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= 'Trường {field} là bắt buộc.';
$lang['form_validation_isset']			= 'Trường {field} phải có dữ liệu.';
$lang['form_validation_valid_email']		= 'Trường {field} phải chứa email hợp lệ.';
$lang['form_validation_valid_emails']		= 'Trường {field} phải chứa các email hợp lệ.';
$lang['form_validation_valid_url']		= 'Trường {field} phải chứa URL hợp lệ.';
$lang['form_validation_valid_ip']		= 'Trường {field} phải chứa IP hợp lệ.';
$lang['form_validation_min_length']		= 'Trường {field} có độ dài tối thiểu {param} ký tự.';
$lang['form_validation_max_length']		= 'Trường {field} không vượt quá {param} ký tự.';
$lang['form_validation_exact_length']		= 'Trường {field} phải có chính xác {param} ký tự.';
$lang['form_validation_alpha']			= 'Trường {field} chỉ chứ các ký tự chữ cái.';
$lang['form_validation_alpha_numeric']		= 'Trường {field} chỉ chứ các ký tự chữ cái và chữ số.';
$lang['form_validation_alpha_numeric_spaces']	= 'Trường {field} chỉ chứ các ký tự chữ cái, chữ số và khoảnh trắng.';
$lang['form_validation_alpha_dash']		= 'Trường {field} chỉ chứ các ký tự chữ cái, chữ số, dấu gạch dưới và dấu gạch nối.';
$lang['form_validation_numeric']		= 'Trường {field} chỉ chứ chữ số.';
$lang['form_validation_is_numeric']		= 'Trường {field} chỉ chứ ký tự chữ số.';
$lang['form_validation_integer']		= 'Trường {field} phải là số nguyên.';
$lang['form_validation_regex_match']		= 'Trường {field} không giống mẫu.';
$lang['form_validation_matches']		= 'Trường {field} không giống với trường {param}.';
$lang['form_validation_differs']		= 'Trường {field} phải khác với trường {param}.';
$lang['form_validation_is_unique'] 		= 'Trường {field} phải là duy nhất.';
$lang['form_validation_is_natural']		= 'Trường {field} phải là số tự nhiên.';
$lang['form_validation_is_natural_no_zero']	= 'Trường {field} phải là số tự nhiên lớn hơn 0.';
$lang['form_validation_decimal']		= 'Trường {field} phải là số thập phân.';
$lang['form_validation_less_than']		= 'Trường {field} phải bé hơn {param}.';
$lang['form_validation_less_than_equal_to']	= 'Trường {field} phải bé hơn hoặc bằng {param}.';
$lang['form_validation_greater_than']		= 'Trường {field} phải lớn hơn {param}.';
$lang['form_validation_greater_than_equal_to']	= 'Trường {field} phải lớn hơn hoặc bằng {param}.';
$lang['form_validation_error_message_not_set']	= 'Không tìm thấy cấu hình lỗi cho trường {field}.';
$lang['form_validation_in_list']		= 'Trường {field} phải là một trong các dữ liệu sau: {param}.';
