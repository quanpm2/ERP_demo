<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Commission_m extends Base_model {

    protected $instance = NULL;
    protected $contract = NULL;

    function __construct()
    {
        parent::__construct();

        $this->instance = $this;

        $models = array(
            'contract/contract_m'
        );

        $this->config->load('mbusiness/commission');
    }

    /**
     * Gets the instance.
     *
     * @return     <type>  The instance.
     */
    public function get_instance()
    {
        return $this->instance;
    }

    /**
     * Sets the instance.
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function set_instance($term_id = 0)
    {
        $contract = $this->contract_m->set_term_type()->get($term_id); 
        if( ! $contract) return FALSE;

        $this->contract = $contract;
        switch ($contract->term_type)
        {
            case 'google-ads' : 
                $this->load->model('googleads/commission_googleads_m');
                $this->instance = $this->commission_googleads_m;
                break;

            case 'facebook-ads' : 
                $this->load->model('facebookads/commission_facebookads_m');
                $this->instance = $this->commission_facebookads_m;
                break;

            default:
                $this->instance = $this;
                break;
        }

        return TRUE;
    }

    /**
     * Gets the rate.
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     float   The rate.
     */
    public function get_rate($term_id = 0)
    {
        $default_package    = $this->config->item('default', 'commission');
        $commission         = $this->config->item($default_package, 'commission');

        return $commission['rate'] ?? 0;
    }

    /**
     * Gets the receipts paid.
     *
     * @param      integer  $term_id     The term identifier
     * @param      integer  $user_id     The user identifier
     * @param      string   $start_time  The start time
     * @param      string   $end_time    The end time
     *
     * @return     <type>   The receipts paid.
     */
    public function get_receipts_paid($term_id = 0, $user_id = 0, $start_time = '', $end_time = '')
    {
        $start_time = $this->mdate->startOfDay($start_time);
        $end_time   = $this->mdate->endOfDay($end_time);

        $encrypt_key = md5(json_encode(func_get_args()));
        $cache_key   = "modules/mbusiness/receipts_{$user_id}_amount_actually_collected_{$encrypt_key}";

        if($data = $this->scache->get($cache_key) && 1==2) return $data;

        $this->load->model('contract/receipt_m');

        if( ! empty($term_id)) $this->receipt_m->where('term_posts.term_id', $term_id);
        if( ! empty($user_id)) $this->receipt_m->where('term_users.user_id', $user_id);

        $receipts   = $this->receipt_m
        ->select('posts.post_id, posts.post_author,posts.post_status,posts.end_date,term_posts.term_id,term_users.user_id')
        ->join('term_posts','posts.post_id = term_posts.post_id')
        ->join('term_users','term_users.term_id = term_posts.term_id')
        ->where('posts.post_status','paid')
        ->where('posts.end_date >=', $start_time)
        ->where('posts.end_date <=', $end_time)
        ->where('posts.post_type', 'receipt_payment')
        ->group_by('posts.post_id')
        ->as_array()
        ->get_all();
        $receipts = array_map(function($x){ 

            $x['amount'] = (int) get_post_meta_value($x['post_id'], 'amount');

            $vat = (int) get_term_meta_value($x['term_id'],'vat');
            $x['vat'] = $vat;
            $x['amount_not_vat'] = $vat > 0 ? div($x['amount'], (1+div($vat, 100))) : $x['amount'];

            return $x; 
        }, $receipts);

        if( ! $receipts) return FALSE;

        $this->scache->write($receipts, $cache_key);
        return $receipts;
    }

    /**
     * Gets the amount.
     *
     * @param      integer  $term_id     The term identifier
     * @param      integer  $user_id     The user identifier
     * @param      string   $start_time  The start time
     * @param      string   $end_time    The end time
     *
     * @return     integer  The amount.
     */
    public function get_amount($term_id = 0, $user_id = 0, $start_time = '', $end_time = '')
    {
        $receipts = $this->get_receipts_paid($term_id, $user_id, $start_time, $end_time);
        if(empty($receipts)) return 0;

        return array_sum(array_column($receipts, 'amount'));
    }

    /**
     * Gets the amount not vat.
     *
     * @param      integer  $term_id     The term identifier
     * @param      integer  $user_id     The user identifier
     * @param      string   $start_time  The start time
     * @param      string   $end_time    The end time
     *
     * @return     integer  The amount not vat.
     */
    public function get_amount_not_vat($term_id = 0, $user_id = 0, $start_time = '', $end_time = '')
    {
        $receipts = $this->get_receipts_paid($term_id, $user_id, $start_time, $end_time);
        if(empty($receipts)) return 0;

        return array_sum(array_column($receipts, 'amount_not_vat'));   
    }

    /**
     * Gets the actually collected.
     *
     * @param      integer  $term_id     The term identifier
     * @param      integer  $user_id     The user identifier
     * @param      string   $start_time  The start time
     * @param      string   $end_time    The end time
     *
     * @return     <type>   The actually collected.
     */
    public function get_actually_collected($term_id = 0, $user_id = 0, $start_time = '', $end_time = '')
    {
        return $this->get_amount_not_vat($term_id, $user_id, $start_time, $end_time);
    }

    /**
     * Gets the amount kpi.
     *
     * @param      integer  $term_id     The term identifier
     * @param      integer  $user_id     The user identifier
     * @param      string   $start_time  The start time
     * @param      string   $end_time    The end time
     *
     * @return     <type>   The amount kpi.
     */
    public function get_amount_kpi($term_id = 0, $user_id = 0, $start_time = '', $end_time = '')
    {
        return $this->get_actually_collected($term_id, $user_id, $start_time, $end_time);
    }
}
/* End of file Commission_m.php */
/* Location: ./application/modules/models/Commission_m.php */