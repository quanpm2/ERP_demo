<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Kpi_progress_m extends Base_model {

	public $_table = 'kpi_progress';
	public $primary_key = "id";

	public $before_create = array('callback_before_create');
	public $before_update = array('callback_before_update');

	function __construct()
	{
		parent::__construct();

        $this->load->config('reporting/kpi_progress_enums');
	}

	/**
	 * Callback function that is executed before creating a new row.
	 *
	 * @param array $row The row to be created.
	 * @return array The modified row.
	 */
	protected function callback_before_create($row)
    {
    	if(empty($row['created_at'])) $row['created_at'] = my_date(time(), 'Y-m-d H:i:s');
    	if(empty($row['updated_at'])) $row['updated_at'] = my_date(time(), 'Y-m-d H:i:s');

        return $row;
    }

    
	/**
	 * Updates the given row before performing an update operation.
	 *
	 * @param array $row The row to be updated.
	 * @return array The updated row.
	 */
	protected function callback_before_update($row)
    {
    	if(empty($row['updated_at'])) $row['updated_at'] = my_date(time(), 'Y-m-d H:i:s');

        return $row;
    }
}