<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fact_warehouse_summary_m extends Base_model
{
	public $primary_key = 'id';
	public $_table = 'fact_warehouse_summary';
}
/* End of file Fact_warehouse_summary_m.php */
/* Location: ./application/models/Fact_warehouse_summary_m.php */