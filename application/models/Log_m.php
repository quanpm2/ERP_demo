<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Log_m extends Base_model {

	public $_table = 'log';
	public $primary_key = "log_id";
	public $meta;

	public $status_pending = 0;
	public $status_running = 1;
	public $status_success = 2;
	public $status_error = 3;
	public $status_finish = 4;

	public $before_create = array('timestamps');

	function __construct()
	{
		parent::__construct();
		$this->load->model('logmeta_m');
		$this->meta = $this->logmeta_m;
	}

	protected function timestamps($row)
    {
    	if(empty($row['log_time_create'])) $row['log_time_create'] = my_date(time(), 'Y-m-d H:i:s');
        return $row;
    }
}