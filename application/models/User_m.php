<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_m extends Base_model {

    public $_table      = 'user';
    public $primary_key = 'user_id';
    
    public $id = 0;
    public $name;
    public $account;
    public $avatar;

    public $role_id = 0;
    public $role_name;

    public $after_update    = ['delete_cache'];

    /**
     * Initialise the model, tie into the CodeIgniter object and
     * try our best to guess the table name.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('term_users_m');
    }

    public function meta($id, $meta_key)
    {
        return get_user_meta_value($id, $meta_key);
    }
    
    /**
     * Log out
     *
     * @return     integer  the result
     */
    public function logout()
    {
        $this->session->sess_destroy();
        return 1;
    }

    /**
     * Compare 2 pwd
     *
     * @param      string   $password       The password
     * @param      string   $user_password  The user password
     * @param      string   $salt           The salt
     *
     * @return     boolean  the result
     */
    public function compare_password($password = '', $user_password = '', $salt = '')
    {
        if(empty($password) || empty($user_password) || empty($salt)) return FALSE;

        $hash_pass = $this->hash_pass($password,$salt);

        if($hash_pass == $user_password) return TRUE;

        return FALSE;
    }

    /**
     * Hash password
     *
     * @param      string  $pass   The pass
     * @param      string  $salt   The salt
     *
     * @return     String
     */
    public function hash_pass($pass, $salt = '')
    {
        return md5(base64_encode(md5($pass.':'.$salt)).$salt);
    }


    /**
     * Random Salt
     *
     * @param      integer     $num    The number
     *
     * @return     String(10)
     */
    public function random_salt($num = 10)
    {
        $d = base64_encode(md5(time() . rand()));
        return substr($d, 0, $num);
    }


    /**
     * Check User is logged In
     *
     * @return     bool  the result
     */
    public function checkLogin()
    {
        return ($this->session->userdata('user_id') > 0);
    }

    /**
     * Delete all cache file has relation between user vs term
     *
     * @param      array   $args   The arguments
     *
     * @return     boolean  the Result
     */
    protected function delete_cache($args = array())
    {
        if( empty($args[1])) return FALSE; // $args[1] is "Query result state"
        if( empty($args[2])) return FALSE; // $args[2] is Primary Key

        $this->load->config('contract/contract');
        $taxonomies     = $this->config->item('taxonomy');
        $taxonomies     = $taxonomies ? array_keys($taxonomies) : [];
        $taxonomies[]   = 'website';

        $this->load->model('term_users_m'); 
        $terms = $this->term_users_m->get_user_terms($args[2], $taxonomies);
        if( ! $terms) return FALSE;

        foreach ($terms as $term)
        {
            $cache_key = "user/{$term->term_id}_users_";
            $this->scache->delete_group($cache_key);
        }

        return TRUE;
    }
}
/* End of file User_m.php */
/* Location: ./application/models/User_m.php */