<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This class describes an option m.
 */
class Option_m extends Base_Model {

	public $primary_key = 'option_name';

	public $_table = 'options';

	private $cache_folder = 'options/';

	/**
	 * Gets the value.
	 *
	 * @param      string  $option_name  The option name
	 * @param      <type>  $unserialize  The unserialize
	 *
	 * @return     string  The value.
	 */
	function get_value($option_name, $unserialize = FALSE)
	{
		$cache_key 	= $this->cache_folder . '/' . $option_name . '-' . (bool) $unserialize;
		$result 	= $this->scache->get($cache_key);
		if( ! empty($result)) return $result;

		$result = $this->select('option_value')->as_array()->get_by('option_name', $option_name);
		if(empty($result)) return '';

		$result = $result['option_value'] ?: '';

		if(empty($result)) return $result;

		FALSE !== $unserialize AND $result = @unserialize($result);

		$this->scache->write($result, $cache_key);

		return $result;
	}

	/**
	 * Sets the value (Update or Insert)
	 *
	 * @param      <type>  $option_key    The option key
	 * @param      string  $option_value  The option value
	 *
	 * @return     string  ( description_of_the_return_value )
	 */
	function set_value($option_key, $option_value = '')
	{
		if(!$option_key) return '';

		if(is_array($option_value) || is_object($option_value))
			$option_value = serialize($option_value);

		if($this->where('option_name', $option_key)->count_by() > 0)
		{
			$this->update($option_key, array( 'option_value' => $option_value ));
			return true;
		}

		$this->insert(array(
			'option_name'=>$option_key,
			'option_value' => $option_value));
	}


	/**
	 *  Update the record
	 *
	 * @param      <type>  $data               The data
	 * @param      <type>  $column_name_where  The column name where
	 * @param      <type>  $escape             The escape
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function update($data = NULL, $column_name_where = NULL, $escape = TRUE)
	{
		$this->scache->delete_group($this->cache_folder);
		return parent::update($data, $column_name_where, $escape);
	}


	/**
	 * Insert new record
	 *
	 * @param      <type>  $data             The data
	 * @param      bool    $skip_validation  The skip validation
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function insert($data, $skip_validation = false)
	{
		return parent::insert($data, $skip_validation);
	}
}
/* End of file Option_m.php */
/* Location: ./application/models/Option_m.php */