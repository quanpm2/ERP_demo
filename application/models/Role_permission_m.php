<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Role_permission_m extends Base_model {

	public $_table = 'role_permissions';
	public $primary_key = 'role_id';

	public function get_by_role_id($role_id = 0)
	{
		if(!$role_id) return false;
		return $this->get_many_by('role_id', $role_id);
	}
	
	public function get_by_permission_id($permission_id = 0)
	{
		if(!$permission_id) return false;
		return $this->get_many_by('permission_id', $permission_id);
	}

}