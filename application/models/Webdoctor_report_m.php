<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Base_report_m.php');

class Webdoctor_report_m extends Base_report_m {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Sends an activation email.
     *
     * @param      string  $type_to  The type to
     */
    public function send_activation_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;

        $this->recipients['cc'][]   = 'ketoan@adsplus.vn';

        $this->set_mail_to($type_to); /* Add người nhận mặc định theo hợp đồng vào email */
        
        $default = array(
            'term'      => $this->term,
            'term_id'   => $this->term_id,
            'title'     => 'Thông tin kích hoạt dịch vụ',
            'subject'   => '[WEBDOCTOR.VN] Thông báo dịch vụ thiết kế website '.$this->term->term_name,
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'content_tpl' => 'webbuild/report/activation_email',
        );

        $this->data = wp_parse_args($this->data, $default);
        if( empty($this->data['content_tpl'])) return FALSE;
        
        $this->data['content'] = $this->render_content($this->data['content_tpl']);

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);
        $result = $this->send_email();
        return $result;
    }

    /**
     * Clear all data relate with contract
     *
     * @return     self
     */
    public function clear()
    {
        parent::clear();

        $this->recipients  = array('to' => [], 'cc' => [], 'bcc' => ['luannn@webdoctor.vn']);

        return $this;
    }

    /**
     * Render Send content;
     *
     * @param      <type>  $content_view  The content view
     * @param      string  $layout        The layout
     *
     * @return     Mixed  Result
     */
    public function render_content($content_view, $layout = 'report/template/email/webdoctor')
    {
        if( ! $content_view || ! $layout) return FALSE;

        $data = $this->data;
        if( ! is_array($data)) $data = array($data);

        $data['content'] = $this->load->view($content_view, $data, TRUE);

        $content = $this->load->view($layout, $data, TRUE);
        if( ! $content) return FALSE;
        return $content;
    }
}
/* End of file Webdoctor_report_m.php */
/* Location: ./application/models/Webdoctor_report_m.php */