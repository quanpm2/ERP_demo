<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Dim_time_m extends Base_model {

	public $_table = 'dim_time';
	public $primary_key = "id";

	function __construct()
	{
		parent::__construct();
	}
}