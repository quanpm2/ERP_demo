<?php  defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * A base model with a series of functions,
 * validation-in-model support, event callbacks and more.
 * 
 * @copyright Copyright (c) 2018, thonh@webdoctor.vn
 */

class Base_report_m extends Base_Model
{
	protected 	$data 		= [];
	protected 	$sale 		= [];
	protected 	$workers 	= [];

	protected 	$term 		= NULL;
	protected 	$term_id 	= NULL;
	protected 	$customer 	= NULL;
	protected 	$template 	= NULL;

    protected   $from       = [ 'email' => 'support@adsplus.vn', 'name' => 'Adsplus.vn' ];

    /* Allow Include user group emails to any Email */
    protected   $inc_ugroups        = FALSE; 
    /* Allow Include departments emails to any Email */
    protected   $inc_departments    = FALSE;

	protected 	$autogen_msg        = FALSE;

	public 		$type               = 'email';
	protected 	$recipients         = array( 'to' => [], 'cc' => [], 'bcc' => []);
	protected 	$autoload           = array(
		'models' 	=> ['option_m', 'contract/contract_m', 'term_users_m', 'message/message_m'],
		'libraries' => ['template','email']
	);

	function __construct() 
	{
		parent::__construct();

		$this->load->model($this->autoload['models']);
		$this->load->library($this->autoload['libraries']);
        $this->load->library('table');
	}

	public function init($term = 0)
	{
        if(is_numeric($term)) $term = $this->contract_m->set_term_type()->get($term);
		if( ! $term) return FALSE;

		$this->term 	= $term;
		$this->term_id 	= $term->term_id;

		$this->init_people_belongs();
		return $this;
	}

	/**
	 * Load sales|staffs|customer belongs to contract
	 *
	 * @return     <type>
	 */
	public function init_people_belongs()
	{
		if( ! $this->term) return FALSE;

		$this->load_sale();
		$this->load_workers();
		$this->load_customer();
	}

	/**
	 * Loads a sale.
	 *
	 * @return     self
	 */
	public function load_sale()
	{
		if( ! $this->term) return FALSE;
		
		/* Load thông tin nhân viên kinh doanh phụ trách */
		$staff_business = (int) get_term_meta_value($this->term_id, 'staff_business');
		if($staff_business) $this->sale = $this->admin_m->get_field_by_id($staff_business);

        $this->load_user_groups();

		return $this;
	}


    /**
     * Loads user groups.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_user_groups()
    {
        if(empty($this->sale)) return FALSE;

        $this->load->model('staffs/department_m');
        $this->load->model('staffs/user_group_m');
        $taxonomies = [$this->department_m->term_type, $this->user_group_m->term_type];
        $groups = $this->term_users_m->get_user_terms($this->sale['user_id'], $taxonomies);
        if(empty($groups)) return FALSE;

        $manager_role_id    = $this->option_m->get_value('manager_role_id');
        $leader_role_id     = $this->option_m->get_value('leader_role_id');
        $groups = array_group_by($groups, 'term_type');

        if( ! empty($groups['department']))
        {
            $this->departments      = $groups['department'];
            $head_of_departments    = array();

            foreach ($groups['department'] as $_department)
            {
                $_h_users = $this->term_users_m->get_the_users($_department->term_id, 'admin', ['fields'=>'user.user_id, term_id, role_id, display_name, user_email','where'=> ['role_id'=>$this->option_m->get_value('manager_role_id')]]);
                if(empty($_h_users)) continue;

                $head_of_departments = array_merge($_h_users, $head_of_departments) ;
            }

            if( ! empty($head_of_departments)) $this->head_of_departments = $head_of_departments;
        }

        if( ! empty($groups['user_group']))
        {
            $this->user_groups  = $groups['user_group'];
            $head_of_ugroups    = array();
            foreach ($groups['user_group'] as $_ugroup)
            {
                $_ug_users = $this->term_users_m->get_the_users($_ugroup->term_id, 'admin', ['fields'=>'user.user_id, term_id, role_id, display_name, user_email','where'=> ['role_id'=>$this->option_m->get_value('leader_role_id')]]);
                if(empty($_ug_users)) continue;
                
                $head_of_ugroups = array_merge($_ug_users, $head_of_ugroups);
            }

            if( ! empty($head_of_ugroups)) $this->head_of_ugroups = $head_of_ugroups;
        }

        return $this;
    }

    /**
     * Loads a customer.
     *
	 * @return     self
	 */
	public function load_customer()
	{
		if( ! $this->term) return FALSE;

		/* Load thông tin khách hàng */
		$customers 		= $this->term_users_m->get_the_users($this->term_id, ['customer_person', 'customer_company']);
		$customer 		= $customers ? reset($customers) : NULL;
		$this->customer = $customer;

		return $this;
	}

	/**
	 * Loads workers.
	 *
	 * @return     self
	 */
	public function load_workers()
	{
		return $this;
	}

	/**
	 * Clear all data relate with contract
	 *
	 * @return     self
	 */
	public function clear()
	{
		$this->data 	= [];
		$this->template = NULL;
		$this->term 	= NULL;
		$this->term_id 	= NULL;
		$this->customer = NULL;
		$this->sale 	= [];
		$this->workers 	= [];
		$this->type 	= 'email';
		$this->recipients  = array( 'to' => [], 'cc' => [], 'bcc' => []);
		$this->autogen_msg = FALSE;

		return $this;
	}

    public function set_send_from($arg = '')
    { 	
    	return $this;
    }

    public function set_mail_to($type_to = 'admin')
    {
    	$this->email->clear(TRUE);

        if(is_array($type_to) && !empty($type_to)) 
        {
            $this->recipients = array_merge_recursive($type_to, $this->recipients);
            $type_to 	= $type_to['type_to'] ?? 'specified';
        }

        if( ! empty($this->head_of_departments) && $this->get_inc_departments())
        {
            $this->add_cc(array_column($this->head_of_departments, 'user_email'));
        } 

        if( ! empty($this->head_of_ugroups) && $this->get_inc_ugroups()) 
        {
            $this->add_cc(array_column($this->head_of_ugroups, 'user_email'));
        }

        // if( ! empty($this->user_groups) && $this->get_inc_ugroups())
        // {
        //     $this->add_cc(array_filter(array_map(function($x){ return get_term_meta_value($x->term_id, 'email'); }, $this->user_groups)));
        // } 

        
        // if( ! empty($this->departments) && $this->get_inc_departments()) 
        // {
        //     $this->add_cc(array_filter(array_map(function($x){ return get_term_meta_value($x->term_id, 'email'); }, $this->departments)));
        // }

        switch ($type_to)
        {
        	case 'admin':

        		if( $this->workers )
        		{
        			$_workers = array_map(function($x){ return $x['user_email']; }, $this->workers);
                    $this->add_to(array_values($_workers));
                }

                if( $this->sale ) $this->add_cc($this->sale['user_email']);

        		break;

        	case 'customer':
                if( $this->customer) $this->add_to($this->customer->user_email);

                if( $this->workers ) 
                {
                    $_workers = array_map(function($x){ return $x['user_email']; }, $this->workers);
                    $this->add_cc(array_values($_workers));
                }

        		if( $this->sale ) $this->add_cc($this->sale['user_email']);

        		break;

        	case 'mailreport':
                $this->add_to('thonh@webdoctor.vn');
        }

        return $this;
    }

    public function prepare_email_data($type_to = 0)
    {
        if( ! $this->term) return FALSE;
        $this->set_mail_to($type_to); /* Add người nhận mặc định theo hợp đồng vào email */
        $this->set_send_from('reporting'); /* Cấu hình email gửi tự động random */

        $default = array(
            'term'      => $this->term,
            'term_id'   => $this->term_id,
            'title'     => '    ',
            'subject'   => '',
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'content_tpl' => '',
        );

        $this->data = wp_parse_args($this->data, $default);
        if( empty($this->data['content_tpl'])) return FALSE;

        $this->data['content'] = $this->render_content($this->data['content_tpl']);

        return $this;
    }

    /**
     * Sends an email.
     *
     * @return     boolean  The result
     */
    public function send_email()
    {
    	if( ! $this->recipients) return FALSE;

        if( empty($this->data['subject']) || empty($this->data['content'])) return FALSE;

    	$result = $this->email
        ->from($this->from['email'], $this->from['name'])
    	->to($this->recipients['to'])
    	->cc($this->recipients['cc'])
    	->bcc($this->recipients['bcc'])
    	->send();

    	/* Gửi email không thành công tiến hành  */
    	if( ! $result) 
    	{
    		/* reset class property default */
    		$this->clear(); 
    		return FALSE; 
    	}

    	/* Call callback auto generate message for clients from email resource */
    	if(TRUE === $this->autogen_msg) $this->create_message();

    	/* reset class property default */
    	$this->clear();
    	return TRUE;
    }

    /**
     * Creates a message.
     *
     * @param      array   $data   The data
     * @param      array  $recipients  The recipients
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    protected function create_message()
    {
    	if( ! $this->autogen_msg || empty($this->data)) return FALSE;

    	$message = array(    
            'term_id' 		=> $this->data['term_id'] ?? 0,
            'msg_title' 	=> $this->data['title'] ?? '',
            'msg_content' 	=> $this->data['content'] ?? '',
            'user_id' 		=> $this->customer->user_id ?? 0,
            'created_on' 	=> time(),
            'metadata' 		=> '',
            'msg_status' 	=> 'sent',
            'msg_type' 		=> 'default'
        );

        $headers = array();
        if( ! empty($this->recipients['to']))
        {
            $header_def = array(
                'term_id'       => $message['term_id'],
                'hed_type'      => $message['hed_type'] ?? $this->type,
                'hed_subject'   => $message['msg_title'],
                'user_id'       => $message['user_id'],
                'term_id'       => $message['term_id'],
            );

            $headers = array_map(function($x)use($header_def){ return wp_parse_args(['hed_to'=>$x], $header_def);}, $this->recipients['to']);
        }
        
        return $this->message_m->create($message,$headers);
    }

    /**
     * Adds to.
     *
     * @param      string  $email  The email
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function add_to($email = '') 
    {
        return $this->_add($email, 'to');
    }

    /**
     * Adds a cc.
     *
     * @param      string  $email  The email
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function add_cc($email = '') 
    {
        return $this->_add($email, 'cc');
    }

    /**
     * Adds a bcc.
     *
     * @param      string  $email  The email
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function add_bcc($email = '') 
    {
        return $this->_add($email, 'bcc');
    }


    /**
     * { function_description }
     *
     * @param      <type>  $email  The email
     * @param      string  $type   The type
     *
     * @return     self    ( description_of_the_return_value )
     */
    protected function _add($email , $type = 'to')
    {
        if(empty($email)) return $this;

        if(is_string($email))
        {
            $emails = explode(',', $email);
            if(count($emails) > 1) return $this->_add($emails, $type);

            $this->recipients[$type][]  = $email;
            $this->recipients[$type]    = array_unique(array_filter($this->recipients[$type]));
            return $this;
        }

        if(is_array($email)) foreach ($email as $e) $this->_add($e, $type);

        return $this;
    }

    /**
     * Gets the increment ugroups.
     *
     * @return     <type>  The increment ugroups.
     */
    public function get_inc_ugroups()
    {
        return $this->inc_ugroups;
    }

    /**
     * Gets the increment departments.
     *
     * @return     <type>  The increment departments.
     */
    public function get_inc_departments()
    {
        return $this->inc_departments;
    }

    /**
     * Sets the increment ugroups.
     *
     * @param      <type>  $value  The value
     *
     * @return     self    ( description_of_the_return_value )
     */
    public function set_inc_ugroups($value = TRUE)
    {
        $this->inc_ugroups = (bool) $value;
        return $this;
    }

    /**
     * Sets the increment departments.
     *
     * @param      <type>  $value  The value
     *
     * @return     self    ( description_of_the_return_value )
     */
    public function set_inc_departments($value = TRUE)
    {
        $this->inc_departments = (bool) $value;
        return $this;
    }
}
/* End of file Base_report_m.php */
/* Location: ./application/models/Base_report_m.php */