<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Credentials_m extends Base_model {

    public $_table      = 'credentials';
    public $primary_key = 'id';

    public $type         = 'key';
    protected $before_get   = ['set_type'];
    protected $default_attributes   = ['type' => 'key'];

    protected function set_type()
    {
        return parent::where('type', $this->type);
    }

    public function checkKey($key = '')
    {
        if(empty($key)) return FALSE;

        $instance = parent::select('id,type')::count_by(['password'=>$key]);
        if( ! $instance) return FALSE;
        return TRUE;
    }
}
/* End of file Credentials.php */
/* Location: ./application/models/User_m.php */