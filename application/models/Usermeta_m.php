<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Usermeta_m extends Meta_m {
	public $primary_key = 'umeta_id';
	public $object_id = 'user_id';
	public $_table = 'usermeta';
	public $meta_type = 'user';
}
