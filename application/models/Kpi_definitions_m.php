<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Kpi_definitions_m extends Base_model {

	public $_table = 'kpi_definitions';
	public $primary_key = "id";

	public $before_create = array('callback_before_create');
	public $before_update = array('callback_before_update');

    public $kpi_type = null;

	function __construct()
	{
		parent::__construct();

        $this->load->config('reporting/kpi_definition_enums');
	}

	/**
	 * Callback function that is executed before creating a new row.
	 *
	 * @param array $row The row to be created.
	 * @return array The modified row.
	 */
	protected function callback_before_create($row)
    {
    	if(empty($row['created_at'])) $row['created_at'] = my_date(time(), 'Y-m-d H:i:s');
    	if(empty($row['updated_at'])) $row['updated_at'] = my_date(time(), 'Y-m-d H:i:s');

        return $row;
    }

    
	/**
	 * Updates the given row before performing an update operation.
	 *
	 * @param array $row The row to be updated.
	 * @return array The updated row.
	 */
	protected function callback_before_update($row)
    {
    	if(empty($row['updated_at'])) $row['updated_at'] = my_date(time(), 'Y-m-d H:i:s');

        return $row;
    }

	/**
	 * Sets the kpi_type property of the class.
	 *
	 * If the kpi_type property is empty, it is initialized with the values from the configuration file.
	 * The kpi_type property is then used to set the 'kpi_definitions.type' condition in a database query.
	 *
	 * @return void
	 */
	public function set_kpi_type()
	{
		if(empty($this->kpi_type))
		{
			/* Init kpi_type*/
			$kpi_type = $this->config->item('type');
			$this->kpi_type = array_values($kpi_type);
			return $this->where_in('kpi_definitions.type', $this->kpi_type);
		}

		return is_array($this->kpi_type) 
               ? $this->where_in('kpi_definations_m.type', $this->kpi_type) 
               : $this->where('kpi_definitions.type', $this->kpi_type);
	}
}