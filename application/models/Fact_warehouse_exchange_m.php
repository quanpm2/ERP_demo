<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fact_warehouse_exchange_m extends Base_model
{
	public $primary_key = 'id';
	public $_table = 'fact_warehouse_exchange';
}
/* End of file AdsSegment.php */
/* Location: ./application/modules/contract/models/AdsSegment.php */