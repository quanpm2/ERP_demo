<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_business_managers_ad_accounts_m extends Base_model
{
    public $primary_key = 'id';
    public $_table = 'ad_business_managers_ad_accounts';
    
    function get_business_managers($ad_account_id = 0)
    {
        if( ! $ad_account_id) return FALSE;

        $this->where('ad_business_managers_ad_accounts.ad_account_id', $ad_account_id);
        $result = $this->select('ad_business_managers_ad_accounts.bm_id')->as_array()->get_all();

        if($result)
        {
            return array_column($result, 'bm_id');
        }
        return array();
    }

    /**
     * Set the terms for a post.
     *
     *
     * @see wp_set_object_terms()
     *
     * @param int    $post_id  Optional. The Post ID. Does not default to the ID of the global $post.
     * @param string $tags     Optional. The tags to set for the post, separated by commas. Default empty.
     * @param string $taxonomy Optional. Taxonomy name. Default 'post_tag'.
     * @param bool   $append   Optional. If true, don't delete existing tags, just add on. If false,
     *                         replace the tags with the new tags. Default false.
     * @return mixed Array of affected term IDs. False on failure.
     */
    function set_ad_account_business_managers($ad_account_id = 0, $business_managers = array()) {
        $ad_account_id = (int) $ad_account_id;

        if ( !$ad_account_id)
            return false;

        if(!is_array($business_managers)) $business_managers = array($business_managers);

        // Insert new terms for the post
        $old_business_manager_ad_accounts = $this->get_business_managers($ad_account_id);
        if($old_business_manager_ad_accounts)
        {
            foreach($old_business_manager_ad_accounts as $old_business_manager_ad_account){
                if(!in_array($old_business_manager_ad_account, $business_managers))
                {
                    $this->delete_by(array(
                        'ad_account_id' => $ad_account_id, 
                        'bm_id' => $old_business_manager_ad_account
                    ));
                }
            }
        }

        if($business_managers)
        {
            foreach($business_managers as $business_manager)
            {
                if($business_manager && !in_array($business_manager, $old_business_manager_ad_accounts))
                {
                    $this->insert(array(
                        'ad_account_id' => $ad_account_id,
                        'bm_id' => $business_manager,
                    ));
                }
            }
        }

        return true;
    }

    /**
     * Delete term posts
     *
     * @param      integer  $term_id   The term identifier
     * @param      array    $posts     The posts
     * @param      string   $taxonomy  The taxonomy
     *
     * @return     boolean  TRUE    if successfuly, otherwise return FALSE;
     */
    function delete_business_managers_ad_account($business_manager_id = 0, $ad_accounts = array())
    {
        $business_manager_id = (int) $business_manager_id;
        if( ! $business_manager_id) return FALSE;

        if(empty($ad_accounts)) return true;

        foreach($ad_accounts as $ad_account_id)
        {
            $this->delete_ad_account_business_managers($ad_account_id, [$business_manager_id]);
        }

        return TRUE;
    }

    /**
     * Delete term posts
     *
     * @param      integer  $term_id   The term identifier
     * @param      array    $posts     The posts
     * @param      string   $taxonomy  The taxonomy
     *
     * @return     boolean  TRUE    if successfuly, otherwise return FALSE;
     */
    function delete_ad_account_business_managers($ad_account_id = 0, $business_manager_ids = array())
    {
        $ad_account_id = (int) $ad_account_id;
        if(empty($ad_account_id)) return false;

        if($business_manager_ids)
        {
            $this->where_in('bm_id', $business_manager_ids)->where('ad_account_id', $ad_account_id)->delete_by();
            array_walk($business_manager_ids, function($business_manager_id){
                $this->scache->delete_group("ad_warehouse/business_manager/{$business_manager_id}_ad_accounts");
            });
        }

        $this->scache->delete_group("ad_warehouse/ad_account/{$ad_account_id}_business_managers");
        return TRUE;
    }


    /**
     * Set the posts for term.
     *
     *
     * @see        set_term_posts()
     *
     * @param      int     $term_id   Optional. The User ID. Does not default to the ID of the global $user.
     * @param      array   $posts     The posts
     * @param      string  $taxonomy  Optional. Taxonomy name. Default 'category'.
     * @param      bool  $append  Optional. If true, don't delete existing tags, just
     *                            add on. If false, replace the tags with the new tags.
     *                            Default false.
     *
     * @return     boolean  TRUE if affected term IDs. False on failure.
     */
    function set_business_manager_ad_accounts( $business_manager_id = 0, $ad_accounts = array(), $append = TRUE)
    {
        if(empty($business_manager_id) || empty($ad_accounts)) return FALSE;

        $business_manager_id    = (int) $business_manager_id;
        $business_manager_ad_accounts = $this->get_business_manager_ad_accounts($business_manager_id);
        $business_manager_ad_accounts = $business_manager_ad_accounts ? array_column($business_manager_ad_accounts, 'post_id') : [];

        if( ! empty($business_manager_ad_accounts) && FALSE === $append)
        {
            $this->delete_business_manager_ad_accounts($business_manager_id, $business_manager_ad_accounts);
            $business_manager_ad_accounts = array();
        }

        foreach($ad_accounts as $ad_account_id)
        {
            if( in_array($ad_account_id, $business_manager_ad_accounts)) continue;
            $this->insert(array('ad_account_id' => $ad_account_id, 'bm_id' => $business_manager_id ));
        }

        $this->scache->delete_group("ad_warehouse/business_manager/{$business_manager_id}_ad_accounts");
        return TRUE;
    }


    /*
    Retrieve the terms for a post.
    Return: List of post tags.
    */
    function get_ad_account_business_managers($ad_account_id = 0, $args = array())
    {
        $ad_account_id = (int) $ad_account_id;
        $this->load->helper('array');
        $defaults = array('orderby' => 'term.term_id', 'order' => 'ASC', 'fields' => 'all', 'where'=>'');
        $args = wp_parse_args( $args, $defaults );
        
        $key_cache = "ad_warehouse/ad_account/{$ad_account_id}_business_managers";
        array_multisort($args);
        $key_cache.= '-'.md5(json_encode($args));

        $tags = $this->scache->get($key_cache);
        if(!$tags)
        {
            if($args['fields'] != 'all') $this->select($args['fields']);
            if($args['where'] != '') $this->where($args['where']);

            $this->where('ad_account_id', $ad_account_id);

            $this->join('ad_business_manager','ad_business_manager.id = ad_business_managers_ad_accounts.bm_id');

            $this->order_by($args['orderby'], $args['order']);
            $tags = $this->get_many_by();
            $this->scache->write($tags, $key_cache);
        }

        return $tags;
    }

    /**
     * Gets the users.
     *
     * @param      integer  $term_id   The term identifier
     * @param      string   $taxonomy  The taxonomy
     * @param      array    $args      The arguments
     *
     * @return     <type>   The users.
     */
    function get_business_manager_ad_accounts($business_manager_id, $args = array())
    {
        $business_manager_id = (int) $business_manager_id;

        $defaults           = array('orderby' => 'posts.post_id', 'order' => 'ASC', 'fields' => 'all', 'where'=>'');
        $args               = wp_parse_args( $args, $defaults );
        $args_encrypt       = md5(json_encode($args));

        $key_cache  = "ad_warehouse/business_manager/{$business_manager_id}_ad_accounts";
        $result     = $this->scache->get($key_cache);
        if($result) return $result;

        if($args['fields'] !='all') $this->select($args['fields']);
        if(!empty($args['where'])) $this->where($args['where']);
        if(!empty($args['where_in'])) $this->where_in('ad_business_managers_ad_accounts.ad_account_id', $args['where_in']);

        $ad_accounts = $this
        ->join('ad_account', 'ad_account.id = ad_business_managers_ad_accounts.ad_account_id')
        ->order_by($args['orderby'], $args['order'])
        ->where('ad_business_managers_ad_accounts.bm_id', $business_manager_id)
        ->get_all();
        
        if( ! $ad_accounts) return FALSE;

        $ad_accounts = array_column($ad_accounts, NULL, 'ad_account_id');
        $this->scache->write($ad_accounts, $key_cache);

        return $ad_accounts;
    }

    public function delete_ad_accounts_cached($business_manager_id = 0)
    {
        $this->scache->delete_group("ad_warehouse/business_manager/{$business_manager_id}_ad_accounts");
    }
}
/* End of file AdsSegment.php */
/* Location: ./application/modules/contract/models/AdsSegment.php */