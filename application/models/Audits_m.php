<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Audits_m extends Base_model {

    public $_table          = 'audits';
    public $primary_key     = 'id';
    public $before_create   = array('timestamp');
    
    function __construct() 
    {
        parent::__construct();

        $this->load->config('audit');
        $table = $this->config->item('table', 'audit');
        $this->_table = $table;
    }

    protected function timestamp($data)
    {
        $data['created_at'] = my_date(0, 'Y-m-d H:i:s');
        return $data;
    }

    public function get_events_config(){
        $this->load->config('audit');

        $event_field = $this->config->item('event');
        $event_field = key_value($event_field, 'key', 'title');

        return $event_field;
    }

    public function get_auditable_fields_config(){
        $this->load->config('audit');

        $term_field = $this->config->item('term');
        $term_field = key_value($term_field, 'key', 'title');

        $termmeta_field = $this->config->item('termmeta');
        $termmeta_field = key_value($termmeta_field, 'key', 'title');

        $post_field = $this->config->item('post');
        $post_field = key_value($post_field, 'key', 'title');

        $postmeta_field = $this->config->item('postmeta');
        $postmeta_field = key_value($postmeta_field, 'key', 'title');

        $auditable_field = array_merge($term_field, $termmeta_field, $post_field, $postmeta_field);

        return $auditable_field;
    }
}
/* End of file Audits_m.php */
/* Location: ./application/models/Audits_m.php */