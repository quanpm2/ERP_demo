<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Post_m extends Base_model {
	
	public $primary_key = 'post_id';
	public $_table = 'posts';

    /**
     * OVERIDE INSERT METHOD
     * Write record to audits table
     * 
     */
    public function insert($data, $skip_validation = FALSE)
    {
        $result = parent::insert($data, $skip_validation);
        if(FALSE == $result) return $result;

        $audits = array();
        array_walk($data, function($value, $key) use (&$audits, $result){
            $audits[] = [ 'type' => $this->_table, 'id' => $result, 'field' => $key, 'new' => $value ];
        });
        audit($audits);

        return $result;
    }

	function get_posts($args = array())
	{
		$defaults = array(
			'numberposts' => 0, 
			'offset' => 0,	
			'orderby' => 'start_date',
			'order' => 'DESC', 
			'include' => array(),
			'exclude' => array(), 
			// 'post_status' => 'publish',
			'meta_key' => '',
			'meta_value' =>'', 
			'select' => 'posts.post_id,post_author,post_title,post_name,post_excerpt,post_type,start_date,end_date,post_thumb,post_slug,post_content',
			'where' => array('post_status' => 'publish'),
			'where_in' => array(),
			'like' => array(),
			'tax_query' => array(),
			'meta_query' => array(),
			'post_type' => 'post',
			'post_status' => '',
			'cur_page' => 1,
			'suppress_filters' => true
			);

		$r = wp_parse_args( $args, $defaults );
		if(!empty($r['post_status']))
		{
			$r['where']['post_status'] = $r['post_status'];
			unset($r['post_status']);
		}
		if(isset($r['tax_query']) && is_array($r['tax_query']) && !empty($r['tax_query']))
		{
			$this->join('term_posts', 'posts.post_id = term_posts.post_id');
			
			$this->join('term', 'term.term_id = term_posts.term_id');

			if(is_array($r['tax_query']))
			{
				$is_mutl_array = FALSE;
				foreach($r['tax_query'] as $q)
				{
					if(is_array($q))
					{
						$is_mutl_array = TRUE;
						$this->set_tax_query($q);
					}
				}
				if(!$is_mutl_array)
					$this->set_tax_query($r['tax_query']);
			}
			
			unset($r['tax_query']);
		}
		if($r['meta_key'] || $r['meta_query'])
		{
			$this->join('postmeta', 'posts.post_id = postmeta.post_id');
			$r['meta_query'][] = array('meta_key'=> $r['meta_key'], 'meta_value' => $r['meta_value']);
			$this->set_meta_query($r['meta_query']);
			unset($r['meta_query']);
		}

		$r['cur_page'] = (int)$r['cur_page'];
		if($r['offset'] ==0 && $r['cur_page'] > 1)
			$r['offset'] = $r['numberposts'] * ($r['cur_page'] - 1);
		else
			$r['offset'] =0;

		$this->limit($r['numberposts'],$r['offset']);
		$this->order_by($r['orderby'], $r['order']);

		unset($r['meta_key']);
		unset($r['meta_value']);
		unset($r['suppress_filters']);
		unset($r['include']);
		unset($r['exclude']);
		unset($r['numberposts']);
		unset($r['offset']);
		unset($r['orderby']);
		unset($r['order']);
		

		$this->set_query_array($r['where'], 'where');
		$this->set_query_array($r['like'], 'like');


		// if(!empty($r['like']))
		// {
		// 	$f = TRUE;
		// 	foreach($r['like'] as $like)
		// 	{
		// 		if(is_array($like))
		// 		{
		// 			$f = FALSE;
		// 			$this->like($like);
		// 		}
		// 	}
		// 	if($f)
		// 		$this->like($r['like']);
		// }

		$this->where(array('post_type' => $r['post_type']));
		$this->select($r['select']);
		return $this->get_many_by();
	}

	private function set_query_array($datas = array(), $col = '')
	{
		if(!empty($datas))
		{
			$f = TRUE;
			
			foreach($datas as $data)
			{
				if(is_array($data))
				{
					$f = FALSE;
					$this->{$col}($data);

				}
			}
			if($f)
			{
				$this->{$col}($datas);
			}
		}
	}

	function set_tax_query($arg = array())
	{
		$defaults = array(
			'taxonomy' => '',
			'field' => 'id',
			'terms' => 0
			);
		$r = wp_parse_args( $arg, $defaults );

		if($r['taxonomy'])
			$this->where('term_type', $r['taxonomy']);
		$r['field'] = str_replace('id','term.term_id', $r['field']);
		if($r['terms'])
		{

			if(strpos($r['terms'], ','))
			{
				$this->where_in($r['field'], explode(',', $r['terms']));
			}
			else
			{
				$this->where($r['field'], $r['terms'] );
			}
		}
		return $this;
	}

	function set_meta_query($args = array())
	{
		$defaults = ['meta_key' => '','meta_value' => ''];

		if(empty($args)) return $this;

		foreach($args as $arg)
		{
			$r = wp_parse_args( $arg, $defaults );

			if($r['meta_key']) 
			{
				$this->where('meta_key', $r['meta_key']);
			}

			if($r['meta_value'] || $r['meta_value'] === 0) 
			{
				$this->where('meta_value', $r['meta_value']);
			}
		}

		return $this;
	}

	function get_post($condition = array())
	{

		if(!is_array($condition))
			return $this->get($condition);
		if(isset($condition['like']))
		{

		}
		
		$this->where_not_in('post_status', array('create'));
		return $this->get_by($condition);
	}

	function clean_cache($argv = NULL)
	{
		if( ! is_numeric($argv)) return FALSE;

		if(empty($argv))
		{
			$this->scache->delete_group('post/.cache');
			return TRUE;
		}

		$this->scache->delete_group('post/'.$argv.'_');
		return TRUE;
	}

	function insert_post($args = array())
	{
		
	}

	function delete_post($post_id)
	{
		$this->load->model('postmeta_m');
		$this->load->model('term_posts_m');
		$this->postmeta_m->delete_by(array('post_id'=> $post_id));
		$this->term_posts_m->delete_by(array('post_id' => $post_id));
		$this->clean_cache($post_id);
		$this->delete($post_id);
		return TRUE;
	}
}
