<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Term_m extends Base_model
{
	public $primary_key = 'term_id';
	public $_table = 'term';
	public $cms_taxonomies;

	function set_object($term = NULL)
	{
		if($term) return $term;
		$term = new stdClass();
		$term->term_id = 0;
		$term->term_name = NULL;
		$term->term_slug = NULL;
		$term->term_type = NULL;
		$term->term_description = NULL;
		$term->term_parent = 0;
		$term->term_count = 0;
		$term->term_status = 0;
		$term->term_order = 0;
		return $term;
	}

    public function meta($id, $meta_key)
    {
        return get_term_meta_value($id, $meta_key);
    }

    /**
     * OVERIDE INSERT METHOD
     * Write record to audits table
     * 
     */
    public function insert($data, $skip_validation = FALSE)
    {
        $result = parent::insert($data, $skip_validation);
        if(FALSE == $result) return $result;

        $audits = array();
        array_walk($data, function($value, $key) use (&$audits, $result){
            $audits[] = [ 'type' => $this->_table, 'id' => $result, 'field' => $key, 'new' => $value ];
        });
        audit($audits);

        return $result;
    }

    /**
     * OVERIDE UPDATE METHOD
     * Write record to audits table
     */
    public function update($primary_value, $data, $skip_validation = FALSE)
    {
        $previous = $this->as_array()->get($primary_value);
        $result = parent::update($primary_value, $data, $skip_validation);

        if(empty($result)) return $result;

        $changed_values = array_filter($data, function($v, $k) use($previous){
            return $previous[$k] != $v;
        }, ARRAY_FILTER_USE_BOTH);
        if(empty($changed_values)) return $result;

        $records = array();
        array_walk($changed_values, function($value, $key) use (&$records, $primary_value, $previous){
            $records[] = ['event' => 'updated', 'type' => $this->_table, 'id' => $primary_value, 'field' => $key, 'new' => $value, 'old' => $previous[$key] ];
        });
        audit($records);

        return $result;
    }

	function get_terms($taxonomy)
	{
		$key_cache = 'term/'.$taxonomy.'';
		$tags = $this->scache->get($key_cache);
		if(!$tags)
		{
			$tags = $this->term_m->select('term_id,term_name')->order_by('term_order,term_id')->get_many_by('term_type',$taxonomy);
			$this->scache->write($tags, $key_cache);
		}
		return $tags;
	}

	function register_taxonomy( $taxonomy, $object_type, $args = array() ) {
		if ( ! is_array( $this->cms_taxonomies ) )
			$this->cms_taxonomies = array();

		$defaults = array(
			'labels'                => array('name'=>''),
			'description'           => '',
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => null,
			'show_in_menu'          => null,
			'show_in_nav_menus'     => null,
			'show_tagcloud'         => null,
			'show_in_quick_edit'	=> null,
			'show_admin_column'     => false,
			'meta_box_cb'           => null,
			'capabilities'          => array(),
			'rewrite'               => true,
			'query_var'             => $taxonomy,
			'update_count_callback' => '',
			'_builtin'              => false,
			);

		$args = wp_parse_args( $args, $defaults );
		$args['name'] = $taxonomy;
		$args['object_type'] = array_unique( (array) $object_type );
		$this->cms_taxonomies[ $taxonomy ] = (object) $args;
		$this->hook->do_action( 'registered_taxonomy', $taxonomy, $object_type, $args );
	}

	function get_taxonomy( $taxonomy ) {

		if ( ! $this->taxonomy_exists( $taxonomy ) )
			return false;

		return $this->cms_taxonomies[$taxonomy];
	}

	function taxonomy_exists( $taxonomy ) {
		return isset( $this->cms_taxonomies[$taxonomy] );
	}

	function get_recursive(&$allmenu, $parent = 0) {
		$id_field = 'term_id';
		$parent_field = 'term_parent';
		
		//get current child
		$menu = array();
		if ($allmenu && is_array($allmenu)) foreach ($allmenu as $k => $v) {
			if ($v->{$parent_field} == $parent || $parent == -1) {
				$menu[] = $v;
				unset($allmenu[$k]);
			}
		}
		if ($menu) foreach ($menu as & $item) {
			$temp_menu = $this->get_recursive($allmenu, $item->{$id_field});
			if ($temp_menu) {
				$item->child = $temp_menu;
			}
		}
		return $menu;
	}
	
	function get_object_taxonomies( $object, $output = 'names' ) {

		$object = (array) $object;

		$taxonomies = array();
		foreach ((array) $this->cms_taxonomies as $tax_name => $tax_obj )
		{
			if(array_intersect($object, (array) $tax_obj->object_type))
			{
				if ( 'names' == $output )
					$taxonomies[] = $tax_name;
				else
					$taxonomies[ $tax_name ] = $tax_obj;
			}
		}

		return $taxonomies;
	}

	function get_recursive_array($allmenu, $parent = 0, $pre_fix = '', $tpre_fix = '', &$rmenu = array()) {
		$tpre_fix = $tpre_fix . $pre_fix;
		foreach ($allmenu as $menu) {
			if ($menu->term_parent == $parent) $tpre_fix = '';
			$rmenu[$menu->term_id] = $tpre_fix . $menu->term_name;
			if (isset($menu->child) && $menu->child) {
				$this->get_recursive_array($menu->child, $parent, $pre_fix, $tpre_fix, $rmenu);
			}
		}
		return $rmenu;
	}

	public function get_taxonomies($args = array())
	{
		return $this->cms_taxonomies;
	}

	function clean_cache($taxonomy = NULL)
	{
		if($taxonomy === NULL)
			$this->scache->delete_group('term/.cache');
		else
			$this->scache->delete_group('term/'.$taxonomy);
	}

	function m_find($args = array())
	{
		$relation = 'AND';
		if(isset($args['relation']))
		{
			$relation = $args['relation'];
			unset($args['relation']);
		}

		if(count($args) >= 2 && empty($args['key']))
		{
			parent::where(1,1);
			$relation == 'OR' AND parent::or_group_start();
			foreach ($args as $arg) $this->m_find($arg);
			$relation == 'OR' AND parent::group_end();
			return $this;
		}

		if(!isset($args['key']) || !isset($args['key'])) throw new Exception('Tham số truyền vào không hợp lệ');

		$_alias 	= uniqid('meta_');
		$_compare 	= $args['compare'] ?? 'where';

		switch (TRUE)
		{
			case in_array($_compare, ['=', '!=', '>', '>=', '<', '<=']): parent::where("{$_alias}.meta_value {$_compare}", $args['value']); break;
			case in_array($_compare, ['BETWEEN']): 
				$_start = reset($args['value']);
				$_end 	= end($args['value']);
				parent::where("{$_alias}.meta_value >=", $_start);
				parent::where("{$_alias}.meta_value <=", $_end);
				break;
			case 'where_in' == $_compare:
				parent::where_in("{$_alias}.meta_value", $args['value']);
				break;
			default: parent::{$_compare}("{$_alias}.meta_value", $args['value']); break;
		}

		$relation == 'OR' AND parent::or_group_start()::or_where(1,1);
		
		parent
		::select("{$_alias}.meta_value as '{$args['key']}'")
		::join("termmeta {$_alias}", "{$_alias}.{$this->primary_key} = {$this->_table}.{$this->primary_key} AND {$_alias}.meta_key = '{$args['key']}'");

		$relation == 'OR' AND parent::group_end();

		return $this;
	}
}