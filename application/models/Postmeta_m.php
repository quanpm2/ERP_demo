<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Postmeta_m extends Meta_m {
	public $primary_key = 'meta_id';
	public $object_id = 'post_id';
	public $_table = 'postmeta';
	public $meta_type = 'post';

    function update_meta($post_id, $meta_key, $meta_value, $prev_value = '')
    {
        $meta_ids = $this->as_array()->get_many_by(array('post_id' => $post_id, 'meta_key' => $meta_key));
        if (empty($meta_ids)) {
            return $this->add_metadata($this->meta_type, $post_id, $meta_key, $meta_value);
        }

        if (empty($prev_value)) {
            $prev_value = get_term_meta_value($post_id, $meta_key);
        }

        $result = parent::update_meta($post_id, $meta_key, $meta_value, $prev_value);

        // Data is not change
        if ($prev_value == $meta_value) return $result;

        // Audit
        audit('updated', $this->_table, $post_id, $meta_key, $meta_value, $prev_value);

        // Update
        return $result;
    }

    function add_metadata($meta_type, $object_id, $meta_key, $meta_value, $unique = false)
    {
        // Create
        $result = parent::add_metadata($meta_type, $object_id, $meta_key, $meta_value, $unique);
        if (FALSE == $result) return $result;

        // Audit
        audit('created', $this->_table, $object_id, $meta_key, $meta_value);

        return $result;
    }
}
