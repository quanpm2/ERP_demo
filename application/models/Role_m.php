<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Role_m extends Base_model {

    public $_table = 'role';
    
    public $primary_key = 'role_id';

    protected $soft_delete = TRUE;

    public function existed_check($value){
        // Disable Administrator role
        if($value == 1){
            $this->form_validation->set_message('existed_check', 'Vai trò không tồn tại.');
            return false;
        }

        $isExisted = $this->where('role.role_id', (int) $value)->count_by() > 0;
        if( ! $isExisted)
        {
            $this->form_validation->set_message('existed_check', 'Vai trò không tồn tại.');
            return false;
        }
		
        return true;
    }
}