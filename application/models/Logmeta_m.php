<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Logmeta_m extends Meta_m {
	public $primary_key = 'meta_id';
	public $object_id = 'log_id';
	public $_table = 'logmeta';
	public $meta_type = 'log';
}
