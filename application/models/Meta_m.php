<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Meta_m extends Base_Model {

	public $primary_key = '';
	public $_table = '';
	public $object_id = '';
	public $meta_type = '';

	function get_meta( $post_id, $key = '', $single = FALSE , $as_value = FALSE) {

		return $this->get_metadata($this->meta_type, $post_id, $key, $single,$as_value);
	}

	function get_meta_value( $post_id, $key = '') {
		
		return $this->get_metadata($this->meta_type, $post_id, $key, TRUE,TRUE);
	}

	function add_meta( $post_id, $meta_key, $meta_value, $unique = FALSE )
	{
		return $this->add_metadata($this->meta_type, $post_id, $meta_key, $meta_value, $unique);
	}

	function update_meta( $post_id, $meta_key, $meta_value, $prev_value = '' )
	{
		return $this->update_metadata( $this->meta_type, $post_id, $meta_key, $meta_value, $prev_value);
	}
	
	function delete_meta( $post_id, $meta_key, $meta_value = '' ) {

		return $this->delete_metadata($this->meta_type , $post_id, $meta_key, $meta_value);
	}

	function get_metadata($meta_type, $object_id, $meta_key = '', $single = false, $as_value = FALSE) {
		if ( ! $meta_type || ! is_numeric( $object_id ) ) {
			return false;
		}

		$object_id = absint( $object_id );
		if ( ! $object_id ) {
			return false;
		}


		$check = $this->hook->apply_filters( "get_{$meta_type}_metadata", null, $object_id, $meta_key, $single );
		if ( null !== $check ) {
			if ( $single && is_array( $check ) )
				return $check[0];
			else
				return $check;
		}
		
		$key_cache = $meta_type.'/'.$object_id . '_meta';
		$meta_cache = $this->scache->get($key_cache);

		if ( ! $meta_cache )
		{
			$condition 	= array( $this->object_id => $object_id );
			$meta_cache = $this->select("{$this->primary_key}, meta_key, meta_value")->as_array()->get_many_by($condition);
			$meta_cache = key_array_value($meta_cache, 'meta_key');

			$this->scache->write($meta_cache, $key_cache);
		}

		$meta = array();
		if ( ! $meta_key )
		{
			if($single)
			{
				foreach($meta_cache as $key =>$val)
				{
					$meta_cache[$key] = @$val[0];
				}
			}
			return $meta_cache;
		}

		if ( isset($meta_cache[$meta_key]) ) {
			if ( $single )
				$meta = @$meta_cache[$meta_key][0];
			else
				$meta = $meta_cache[$meta_key];

			if($as_value)
			{
				
				if ( $single )
				{

					return @$meta['meta_value'];
				}
				else
				{
					$meta_cache = array();
					foreach($meta as $m)
					{
						array_push($meta_cache, $m['meta_value']);
					}
					return $meta_cache;
				}
			}
			return $meta;
		}


		if ($single)
			return '';
		else
			return array();
	}

	function add_metadata($meta_type, $object_id, $meta_key, $meta_value, $unique = false) {
		if ( ! $meta_type || ! $meta_key || ! is_numeric( $object_id ) ) {
			return false;
		}
		$object_id = absint( $object_id );
		if ( ! $object_id ) {
			return false;
		}

		$key_cache = $meta_type.'/'.$object_id . '_meta';

		$this->hook->do_action( "add_{$meta_type}_meta", $object_id, $meta_key, $meta_value );

		if($unique && $this->get_metadata($meta_type, $object_id, $meta_key ,$unique))
		{
			$this->scache->delete($key_cache);
			return FALSE;
		}
		
		$result_id = $this->insert(array(
			$this->object_id => $object_id,
			'meta_key' => $meta_key,
			'meta_value' => $meta_value
			) );

		$this->scache->delete($key_cache);

		if ( ! $result_id )
			return false;

		$this->hook->do_action( "added_{$meta_type}_meta", $result_id, $object_id, $meta_key, $meta_value );

		return $result_id;
	}

	function update_metadata($meta_type, $object_id, $meta_key, $meta_value, $prev_value = '') {
		if ( ! $meta_type || ! $meta_key || ! is_numeric( $object_id ) ) {
			return false;
		}

		$object_id = absint( $object_id );
		if ( ! $object_id ) {
			return false;
		}
		$passed_value = $meta_value;

		$key_cache = $meta_type.'/'.$object_id . '_meta';

		$where = array( $this->object_id => $object_id, 'meta_key' => $meta_key );

		$check = $this->hook->apply_filters( "update_{$meta_type}_metadata", null, $object_id, $meta_key, $meta_value, $prev_value );
		if ( null !== $check )
			return (bool) $check;
		// Compare existing value to new value if no prev value given and the key exists only once.
		if ( empty($prev_value) ) {
			$old_value = $this->get_metadata($meta_type, $object_id, $meta_key);
			if ( count($old_value) == 1 ) {
				if ( $old_value[0] === $meta_value )
					return false;
			}
		}

		$meta_ids = $this->as_array()->get_many_by(array($this->object_id=>$object_id, 'meta_key'=>$meta_key));
		if ( !empty( $prev_value ) ) {
			$where['meta_value'] = $prev_value;
		}

		if ( empty( $meta_ids ) ) {
			return $this->add_metadata($meta_type, $object_id, $meta_key, $passed_value);
		}

		foreach ( $meta_ids as $meta_id ) {
			$this->hook->do_action( "update_{$meta_type}_meta", $meta_id, $object_id, $meta_key, $meta_value );
		}

		$result = $this->update_by($where, array('meta_value'=>$meta_value));
		if ( ! $result )
			return false;

		$this->scache->delete($key_cache);
		return $result;
	}

	function delete_metadata($meta_type, $object_id, $meta_key, $meta_value = '', $delete_all = false) {

		if ( ! $meta_type || ! $meta_key || ! is_numeric( $object_id ) && ! $delete_all ) {
			return false;
		}

		$object_id = absint( $object_id );
		if ( ! $object_id && ! $delete_all ) {
			return false;
		}

		$check = $this->hook->apply_filters( "delete_{$meta_type}_metadata", null, $object_id, $meta_key, $meta_value, $delete_all );
		if ( null !== $check )
			return (bool) $check;

		$_meta_value = $meta_value;
		// $this->hook->do_action( "delete_{$meta_type}_meta", $meta_ids, $object_id, $meta_key, $_meta_value );

		$where = array($this->object_id => $object_id, 'meta_key' => $meta_key );

		if($meta_value != '' && $delete_all != TRUE)
		{
			$where['meta_value'] = $meta_value;
		}

		$key_cache = $meta_type.'/'.$object_id . '_meta';
		$this->scache->delete($key_cache);

		return $this->delete_by($where);
	}
}