<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_user_m extends Base_model {

    public $primary_key  = 'auser_id';
    public $_table       = 'api_user';
    public $token        = '';
    public $phone_number = '';

    
    /**
     * Sets the access token.
     *
     * @param      <type>  $token  The token
     */
    public function set_access_token($token)
    {
        $this->token = $token;
    }


    /**
     * Sets the phone number.
     *
     * @param      <type>  $phone_number  The phone number
     */
    public function set_phone_number($phone_number)
    {   
        $this->phone_number = $phone_number;
    }


    /**
     * Authenticate the Api-user's token
     *
     * @return     bool  TRUE if token is valid , otherwise return FALSE
     */
    public function authenticate()
    {
        $api_user = $this->get_by_token($this->token);
        if( ! $api_user) return FALSE;

        $this->setInfo($api_user);

        $result = FALSE;
        switch ($api_user['auser_status'])
        {
            case 'lock':
                $result = FALSE;
                break;
            
            default:
                $result = TRUE;
                break;
        }

        return $result;
    }


    /**
     * Creates an API USER.
     *
     * @param      array  $data   The data
     *
     * @return     array  api user
     */
    public function create_api_user($data = array())
    {
        $default = array('created_on' => time(),'auser_status' => 'inactive');
        $data    = array_merge($data,$default);

        $data['auser_token']    = $this->hash_token();
        $data['auser_otp']      = 123;
        $data['auser_salt']     = 'adsplus123';
        $data['auser_password'] = $this->hash_pass('ABC123@123',$data['auser_salt']);

        $insert_id = $this->insert($data);
        if(!$insert_id) return FALSE;

        $data['auser_id'] = $insert_id;
        return $data;
    }


    /**
     * Gets the default datafields.
     *
     * @return     array  The default datafields.
     */
    public function get_default_datafields()
    {
        return [
            'created_on' => time(),
            'auser_status' => 'inactive',
            'auser_token' => $this->hash_token(),
            'auser_otp' => time(),
            'auser_salt' => 'adsplus123',
            'auser_password' => $this->hash_pass('ABC123@123', 'adsplus123'),
            // 'expire_on' => strtotime("+1 seconds"),
            'expire_on' => strtotime("+1 days"),
        ];
    }


    /**
     * Determines if exist user.
     */
    public function is_exist_user()
    {
        $phone_number = $this->phone_number;
    }


    /**
     * Gets the by phone.
     *
     * @param      string  $phone  The phone
     *
     * @return     array  The api user data.
     */
    public function get_by_phone($phone)
    {
        $data       = array();
        $api_user   = $this->as_array()->get_by(array('auser_phone'=>$phone));
        if($api_user) return $api_user;

        $api_user_insert_data = array('auser_phone'=>$phone,'user_id'=>0);

        # check valid and exists phone number in customer db
        $customer_id    = 0;
        $customer_meta  = $this->usermeta_m->select('user_id')
        ->get_by(['meta_key' => 'customer_phone','meta_value !=' => '','meta_value' => $phone]);
        if($customer_meta) $customer_id = $customer_meta->user_id;

        if($customer_id)
        {
            $api_user_insert_data['user_id'] = $customer_id;
            return $this->create_api_user($api_user_insert_data);
        }

        $minPhoneLen = 7;
        if(strlen($phone) <= $minPhoneLen) return FALSE;

        # if phone not exitst in customer, then find it in curators of adsplus service
        $term_meta = $this->termmeta_m
        ->where('meta_key','contract_curators')
        ->like('meta_value',$phone)
        ->get_many_by();

        if(empty($term_meta)) return FALSE;

        return $this->create_api_user($api_user_insert_data);
    }


    /**
     * Gets the by token.
     *
     * @param      string  $token  The token
     *
     * @return     array  The api user data.
     */
    public function get_by_token($token)
    {
        $data = array();
        $api_user = $this->as_array()->get_by(array('auser_token'=>$token));
        return $api_user;
    }


    /**
     * Gets the OTP by token.
     *
     * @param      string  $token  The token
     *
     * @return     string  The user OTP.
     */
    public function get_otp_by($token)
    {
        $api_user = $this->select('auser_otp')->get_by_token($token);
        if(empty($api_user)) return FALSE;

        return $api_user['auser_otp'];
    }


    /**
     * generate token
     *
     * @param      <type>  $user_id  The user identifier
     */
    public function generate_token($user_id)
    {
        return FALSE;
    }


    /**
     * OTP generation function
     *
     * @return     int  OTP
     */
    public function generate_otp()
    {
        $rand_num = rand(0,999);
        $otp = str_pad($rand_num, 3,'0',STR_PAD_LEFT);
        return $otp;
    }


     
    /**
     * Hash TOKEN FUNC
     *
     * @param      string  $salt   The salt
     *
     * @return     string token
     */
    public function hash_token($salt = '')
    {
        $salt = $salt ?: $this->random_salt();
        return md5(base64_encode(md5(uniqid($salt,TRUE).':'.$salt)).$salt).md5(time().$salt);
    }


    /**
     * HASH PASSWORD FUNC
     *
     * @param      string  $pass   The pass
     * @param      string  $salt   The salt
     *
     * @return     string pwd hash
     */
    public function hash_pass($pass, $salt = '')
    {
        return md5(base64_encode(md5($pass.':'.$salt)).$salt);
    }


    /**
     * Random Salt
     * @return String(10)
     */
    public function random_salt($num = 10) {
        $d = base64_encode(md5(time() . rand()));
        return substr($d, 0, $num);
    }


    /**
     * Gets the user services.
     *
     * @param      integer  $token  The token
     *
     * @return     array    The user services.
     */
    public function get_user_services($token = 0)
    {
        $result     = array('adsplus' => 0,'facebook-ads' => 0,'webgeneral' => 0,'webbuild' => 0,'hosting' => 0,'domain' => 0);
        $token      = $token ?: $this->token;

        $api_user   = $this->get_by_token($token);
        if( ! $api_user) return $result;

        $term_ids = array();

        if(!empty($api_user['user_id']))
        {
            $user_id = $api_user['user_id'];
            $this->load->model('customer/customer_m');
            $customer = $this->customer_m->set_get_customer()->get($user_id);
            
            $this->load->model('term_users_m');
            $term_ids = $this->term_users_m->get_the_terms($user_id);
        }

        if(!empty($api_user['auser_phone']))
        {
            $term_meta = $this->termmeta_m
            ->select('term_id')
            ->where('meta_key', 'contract_curators')
            ->like('meta_value',$api_user['auser_phone'])
            ->get_many_by();

            if(!empty($term_meta))
            {
                $term_ids = array_unique(array_merge($term_ids, array_column($term_meta, 'term_id')));
            }
        }

        if(empty($term_ids)) return $result;

        $terms = $this->term_m
        ->select('term_id,term_name,term_type')
        ->where_in('term_status',array('pending','publish','ending','liquidation'))
        ->where('term_type !=','website')
        ->where_in('term_id',$term_ids)
        ->get_many_by();

        $api_services = array(
            'adsplus' => 'google-ads',
            'webgeneral' => 'webgeneral', 
            'webbuild' => 'webdesign', 
            'hosting'=>'hosting',
            'domain'=>'Web Tổng hợp',
            'facebook-ads' => 'facebook-ads'
            );

        $services = array_group_by($terms,'term_type');
        foreach ($api_services as $key => $term_type) 
        {
            $api_services[$key] = (empty($services[$term_type]) ? 0 : 1);
        }

        $result = $api_services;

        return $result;
    }


    /**
     * Gets the user services.
     *
     * @param      integer  $token  The token
     *
     * @return     array    The user services.
     */
    public function list_services($token = 0)
    {
        $result     = array('adsplus' => 0,'facebook-ads' => 0,'webgeneral' => 0,'webbuild' => 0,'hosting' => 0,'domain' => 0);
        $token      = $token ?: $this->token;
        $api_user   = $this->get_by_token($token);

        if( ! $api_user) return $result;
        
        $term_ids = array();

        # QUERY ALL CONTRACT WITH BELONGS TO CUSTOMER (USER_ID)
        if( ! empty($api_user['user_id']))
        {
            $this->load->model('term_users_m');
            $term_ids = $this->term_users_m->get_the_terms($api_user['user_id']);
        }

        # QUERY ALL CONTRACT WITH PHONE SETTING WITHIN
        if( ! empty($api_user['auser_phone']) 
            && $termmetas = $this->termmeta_m->select('term_id')
            ->where('meta_key', 'contract_curators')->like('meta_value', $api_user['auser_phone'])
            ->get_many_by())
        {
            $term_ids = array_unique(array_merge($term_ids, array_column($termmetas, 'term_id')));
        }

        if(empty($term_ids)) return $result;

        $this->load->model('contract/contract_m');

        $contracts = $this->contract_m->select('term_id,term_name,term_type')->set_term_type()
        ->where_in('term_status', ['pending','publish','ending','liquidation'])
        ->where_in('term_id', $term_ids)
        ->get_all();

        $services = array_group_by($contracts, 'term_type');
        $service_type_mapping = array(
            'adsplus'       => 'google-ads',
            'facebook-ads'  => 'facebook-ads',
            'webbuild'      => 'webdesign', 
            'hosting'       => 'hosting',
            'domain'        => 'domain',
            'webgeneral'    => 'webgeneral', 
        );

        $result = array();
        foreach ($service_type_mapping as $key => $value)
        {
            $result[$key] = (empty($services[$value]) ? 0 : 1);
        }

        return $result;
    }


    /**
     * Sets the information.
     *
     * @param      <type>  $data   The data
     */
    function setInfo($data)
    {
        if(empty($data) || !is_array($data)) return FALSE;
        
        foreach ($data as $key => $value) 
        {
            if($key == 'auser_password') continue;
            $this->{$key} = $value;
        }
    }
}
/* End of file Api_user_m.php */
/* Location: ./application/models/Api_user_m.php */