<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_business_manager_m extends Base_model
{
	public $primary_key = 'id';
	public $_table = 'ad_business_manager';
    
    /**
     * force_fill
     *
     * @param  array $attributes
     * @return void
     */
    public function force_fill(array $attributes)
    {
        $this->attributes = array_merge($this->attributes, $attributes);
        return $this;
    }
        
    /**
     * get_system_user
     *
     * @param  int|string $original_term_id
     * @return void
     */
    final public function get_system_user($original_term_id = 0){
        $original_term_id = $original_term_id ?: $this->attributes['original_term_id'] ?: 0;
        if(empty($original_term_id)) return [];

        $this->load->model('term_users_m');
        
        $system_users = $this->term_users_m->where('term_users.term_id', $original_term_id)
        ->join('user', 'user.user_id = term_users.user_id AND user.user_type = "fbaccount"')
        ->select('user.*')
        ->as_array()
        ->get_all();
        
        return $system_users;
    }
}
/* End of file AdsSegment.php */
/* Location: ./application/modules/contract/models/AdsSegment.php */