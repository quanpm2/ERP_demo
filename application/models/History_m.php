<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class History_m extends Base_model {

	public $primary_key = 'his_id';

	public $_table = 'history';

	public $table;

	public $object_id = NULL;

	public $updatedData = array();

	public $original_data  = array();

	public $is_metadata = FALSE;

	public function save($clear = TRUE){

		if(empty($this->updatedData) || empty($this->table)) return FALSE;

        $revisions = array();

        foreach ($this->updatedData as $key => $change) {

            $revisions[] = array(
                'object_id'		=> $this->object_id,
                'table'			=> $this->table,
                'key'           => $key,
                'old_value'     => $this->original_data[$key],
                'new_value'     => $this->updatedData[$key],
                'user_id'       => $this->admin_m->id,
                'created_time'	=> time(),	
            );
        }

        if($clear) $this->clear();

        if(empty($revisions)) return FALSE;
        
    	return $this->insert_many($revisions);	
    }

    private function clear(){

    	$this->is_metadata = FALSE;

        $this->original_data = array();

        $this->updatedData = array();
    }

    protected function set_original_data($original_data){

		$this->original_data = $original_data;

		return $this;
	}

	protected function get_dirty($newData){

		if(empty($newData)) return FALSE;

		foreach ($newData as $key => $value) {

			if(empty($this->original_data[$key]) || $this->original_data[$key] != $newData[$key]){

				$this->updatedData[$key] = $value;
			}
		}

		return $this;
	}

	public function is_metadata($is_metadata = FALSE){

		$this->is_metadata = $is_metadata;

		return $this;
	}
}