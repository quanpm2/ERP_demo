<?php

/**
 * Permissions Settings Model
 *
 * Provides access and utility methods for handling permission storage in the database.
 *
 * Permissions are a simple string made up of 3 parts:
 * - Domain  - Typically the module name for application modules.
 * - Context - The context name (e.g. Content, Reports, Settings, or Developer).
 * - Action  - The permitted action (View, Manage, Create, Edit, Delete, etc.).
 *
 * Example permissions would be:
 * - Site.Developer.View
 * - Bonfire.Users.Manage
 * - Appmodule.Content.Delete
 *
 * @package Bonfire\Modules\Permissions\Models\Permission_model
 * @author  Bonfire Dev Team
 * @link    http://cibonfire.com/docs/developer/roles_and_permissions
 *
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Permission_m extends Base_model {

	public $_table = 'permissions';

	public $primary_key = 'permission_id';

	/** @var string The url to redirect to on successful login. */
	public $login_destination = '';

    /**
     * @var string The date format used for users.last_login, login_attempts.time,
     * and user_cookies.created_on. Passed as the first argument of the PHP date()
     * function when handling any of these values.
     */
    protected $loginDateFormat = 'Y-m-d H:i:s';

    /** @var boolean Allow use of the "Remember Me" checkbox/cookie. */
    private $allowRemember;

    /** @var string The ip_address of the current user. */
    private $ip_address;

    /** @var array The names of all existing permissions. */
    private $permissions = null;

    /** @var array The permissions by role. */
    private $role_permissions = array();

    /** @var object The logged-in user. */
    private $user;


    function  __construct()
    {
    	parent::__construct();
    	if (! class_exists('user_m', false)) {
    		$this->load->model('user_m');
    	}
    	$this->load->model('role_permission_m');
    }

	/**
     * Check whether a user is logged in (and, optionally of the correct role) and,
     * if not, send them to the login screen.
     *
     * If no permission is checked, will simply verify that the user is logged in.
     * If a permission is passed in to the first parameter, it will check the user's
     * role and verify that role has the appropriate permission.
     *
     * @param string $permission (Optional) The permission to check for.
     * @param string $uri        (Optional) The redirect URI if the user does not
     * have the correct permission.
     *
     * @return boolean True if the user has the appropriate access permissions.
     * Redirect to the previous page if the user doesn't have permissions.
     * Redirect to LOGIN_AREA page if the user is not logged in.
     */
	public function restrict($permission = null, $uri = null)
	{

        // Check whether the user has the proper permissions.
		if (empty($permission) || $this->has_permission($permission)) {
			return true;
		}

        // If the user is logged in, but does not have permission...

        // If $uri is not set, get the previous page from the session.
		if (! $uri) {
            $this->load->library('user_agent');
			$uri = $this->agent->referrer() ?: current_url();
            // If previous page and current page are the same, but the user no longer
            // has permission, redirect to site URL to prevent an infinite loop.
			if ($uri == current_url()) {
				$uri = admin_url();
			}
		}
        $this->messages->error('Bạn không có quyền vào phần này. Mã lỗi: Error Permission '.$permission.'');
        redirect($uri,'refresh');
    }

	/**
     * Check the session for the required info, then verify it against the database.
     *
     * @return boolean True if the user is logged in, else false.
     */
	public function is_logged_in()
	{
		return $this->user_m->checkLogin();
	}

    //--------------------------------------------------------------------------
    // Permissions
    //--------------------------------------------------------------------------

    /**
     * Verify that the user is logged in and has the appropriate permissions.
     *
     * @param string  $permission The permission to check for, e.g. 'Site.Signin.Allow'.
     * @param integer $role_id    The id of the role to check the permission against.
     * If role_id is not passed into the method, it assumes the current user's role_id.
     * @param boolean $override   Whether access is granted if this permission doesn't
     * exist in the database.
     *
     * @return boolean True if the user/role has permission or the permission was
     * not found in the database and $override is true, else false.
     */
    public function has_permission($permission, $role_id = null, $override = false)
    {
        // Move permission to lowercase for easier checking.
        $permission     = strtolower($permission);
        $permissions    = explode(',', $permission);

        if(count($permissions) > 1)
        {
            foreach($permissions as $npermission)
            {
                if( ! $this->has_permission(trim($npermission), $role_id, $override)) continue;
                return true;
            }
        }

        $action             = '';
        $array_permission   = explode('.', $permission);
        (count($array_permission) == 3) AND $action = array_pop($array_permission);

        // If no role is provided, assume it's for the current logged in user.
        if (empty($role_id))
        {
            $role_id = $this->role_id();
        }

        $permissions = $this->loadPermissions();
      
        // Does the user/role have the permission?
        if (isset($permissions[$permission]))
        {
            $role_permissions = $this->loadRolePermissions($role_id);
            $permission_id    = $permissions[$permission];

            if($action !== '')
            {
                return isset($role_permissions[$role_id][$permission_id][$action]);
            }

            if (isset($role_permissions[$role_id][$permission_id]))
            {
                return true;
            }
        }
        elseif ($override)
        {
            return true;
        }

        return false;
    }

    /**
     * Check whether a permission is in the system.
     *
     * @param string $permission The case-insensitive name of the permission to check.
     *
     * @return boolean True if the permission was found, else false.
     */
    public function permission_exists($permission)
    {
        // Move permission to lowercase for easier checking.
    	$permission  = strtolower($permission);
    	$permissions = $this->loadPermissions();
    	return isset($permissions[$permission]);
    }

    //--------------------------------------------------------------------------
    // Permissions
    //--------------------------------------------------------------------------

    /**
     * Load the permission names from the database.
     *
     * @return array Permissions: key - lowercase name, value - permission ID.
     */
    private function loadPermissions()
    {
    	if (! empty($this->permissions)) {
    		return $this->permissions;
    	}

        $cache_key = 'role/permissions-all';
        if(!$this->permissions = $this->scache->get($cache_key))
        {
            $this->permissions = array();

            $perms = $this->get_all();
            if (! empty($perms)) 
            {
              foreach ($perms as $perm) 
              {
                $perm->name = strtolower($perm->name);
                $this->permissions[$perm->name] = $perm->permission_id;
                $actions = @unserialize($perm->action);
                if($actions && is_array($actions))
                {
                    foreach($actions as $action)
                    {
                        $perm_name = $perm->name.'.'.$action;
                        $this->permissions[$perm_name] = $perm->permission_id;
                    }
                }
            }
        }
        $this->scache->write($this->permissions, $cache_key);
    }
    return $this->permissions;
}

    /**
     * Load the role permissions from the database.
     *
     * @param integer $role_id The role id for which permissions are loaded. Uses
     * the current user's role ID if none is provided.
     *
     * @return void
     */
    private function loadRolePermissions($role_id = null)
    {
        if (is_null($role_id)) {
            $role_id = $this->role_id();
        }
        if(!$role_id)
            return array();
        if (! empty($this->role_permissions[$role_id])) {
            return $this->role_permissions;
        }
        $cache_key = 'role/role-permissions-'.$role_id;
        $this->role_permissions[$role_id] = array();

        if(!$role_permissions = $this->scache->get($cache_key))
        {
            $role_permissions = array();
            $role_perms = $this->role_permission_m->get_many_by('role_id', $role_id);

            if($role_perms)
            {
                foreach ($role_perms as $permission) {
                    $role_permissions[$permission->permission_id] = array();
                    $actions = @unserialize($permission->action);
                    if($actions && is_array($actions))
                    {
                        foreach($actions as $action)
                        {
                           $action = strtolower($action);
                           $role_permissions[$permission->permission_id][$action] = true;
                       }
                   }
               }
           }
           $this->scache->write($role_permissions, $cache_key);
       }

       $this->role_permissions[$role_id] = $role_permissions;
       return $this->role_permissions;
   }
    /**
     * Retrieves the role_id from the current session.
     *
     * @return integer/boolean The user's role_id or false.
     */
    public function role_id()
    {
    	if (! $this->is_logged_in()) {
    		return false;
    	}
    	return $this->session->userdata('role_id');
    }


    /**
     * Add new permission
     *
     * @param      string  $permission_name  The permission name
     * @param      array   $actions          The actions
     * @param      string  $description      The description
     *
     * @return     bool    ( description_of_the_return_value )
     */
    public function add($permission_name = '', $actions = array(), $description = '')
    {
        if(!$permission_name || !is_array($actions)) return false;

        if(!is_array($actions)) return false;

        if( ! $this->permission_exists($permission_name)) 
        {
            $id = $this->insert(array(
                'name'          => $permission_name,
                'description'   => $description,
                'status'        => 1,
                'action'        => @serialize(array_values($actions))
            ));

            $this->delete_cache();
            return $id;
        }

        $permission = $this->select('permission_id')->get_by('name',$permission_name);
        if(!$permission) return false;

        $result = $this->update($permission->permission_id, array(
            'name' => $permission_name,
            'description' => $description,
            'status' => 1,
            'action' => @serialize(array_values($actions))
        ));

        $id = $permission->permission_id;
        $this->delete_cache();

        return $id;
    }

    public function delete_by_name($permission_name = '')
    {
        if(!$this->permission_exists($permission_name))
            return false;
        $permission = $this->get_by('name', $permission_name);

        $this->load->model('role_permission_m');
        $this->role_permission_m->delete_by('permission_id', $permission->permission_id);
        $this->delete_by('name', $permission_name);
        $this->delete_cache();
    }

    public function delete_cache($role_id = 0)
    {
        if(!$role_id)
        {
            $this->scache->delete_group('role/.cache');
        }
        else
        {
            $this->scache->delete('role/role-permissions-'.$role_id);
        }
        $this->role_permissions = array();
        $this->permissions = array();
    }
}


//------------------------------------------------------------------------------
// Helper Functions
//------------------------------------------------------------------------------

if (! function_exists('has_permission')) {
    /**
     * A convenient shorthand for checking user permissions.
     *
     * @param string  $permission The permission to check for, ie 'Site.Signin.Allow, Site.Signin.Login'.
     * @param boolean $override   Whether access is granted if this permission doesn't
     * exist in the database.
     *
     * @return boolean True if the user has the permission or $override is true
     * and the permission wasn't found in the system, else false.
	 * @use: has_permission('Site.Signin.Allow, Site.Signin.Login');

     */
    function has_permission($permission, $override = false)
    {
    	// return true;
    	return get_instance()->permission_m->has_permission($permission, null, $override);
    }
}

if (! function_exists('permission_exists')) {
    /**
     * Check to see whether a permission is in the system.
     *
     * @param string $permission Case-insensitive permission to check.
     *
     * @return boolean True if the permission exists, else false.
     */
    function permission_exists($permission)
    {
    	return get_instance()->permission_m->permission_exists($permission);
    }
}

if (! function_exists('restrict')) {

	function restrict($permission = null, $uri = null)
	{
       return get_instance()->permission_m->restrict($permission,$uri);
   }
}