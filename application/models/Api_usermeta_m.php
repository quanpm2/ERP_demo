<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Api_usermeta_m extends Meta_m {
	public $primary_key = 'aumeta_id';
	public $object_id = 'auser_id';
	public $_table = 'api_usermeta';
	public $meta_type = 'api_user';
}
