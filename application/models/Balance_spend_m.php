<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Balance_spend_m extends Post_m
{
	/**
     * This model's post_type default field value identifier.
     */
    public $post_type = 'balance_spend';

    public $after_update = array('clear_cache');
    public $before_delete = array('delete_relations');

    /**
     * Sets the post type.
     *
     * @return     this
     */
    public function set_post_type()
	{
		return $this->where('post_type',$this->post_type);
	}

    protected function clear_cache($args)
    {
        if( ! isset($args[2]) && is_numeric($args[2])) return $args;

        $post_id = (int) $args[2];

        $contracts = $this->term_posts_m->get_post_terms($post_id, $this->contract_m->getTypes(), [ 'fields' => 'term.term_id, term_type' ]);
        $contracts AND $contracts = array_values(array_filter($contracts));
        $contracts AND $contracts = array_merge(...$contracts);
        $contracts AND array_walk($contracts, function($x){   
            $this->term_posts_m->delete_posts_cached($x->term_id);
        });

        $this->scache->delete_group("post/{$post_id}_terms_");

        return $args;
    }

    protected function delete_relations($id)
    {
        $contracts = $this->term_posts_m->get_post_terms($id, $this->contract_m->getTypes());
        $contracts AND $contracts = array_values(array_filter($contracts));
        $contracts AND $contracts = array_merge(...$contracts);
        $contracts AND $this->term_posts_m->delete_post_terms($id, array_column((array) $contracts, 'term_id'));

        $contracts AND array_walk($contracts, function($x){   
            $this->term_posts_m->delete_posts_cached($x->term_id);
        });
        return $id;
    }

    public function contracts(int $id)
    {
        $iSegment = $this->select('posts.post_id,start_date')->get($id);
        if(empty($iSegment)) return false;

        $adAccounts = $this->term_posts_m->get_post_terms($iSegment->post_id, $this->adaccount_m->term_type);
        if(empty($adAccounts)) return false;

        $segments = $this->ads_segment_m
        ->select('posts.post_id')
        ->set_post_type()
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->where_in('term_posts.term_id', array_unique(array_map('intval', array_filter(array_column($adAccounts, 'term_id')))))
        ->where('start_date <=', $iSegment->start_date)
        ->where('if(end_date = 0, UNIX_TIMESTAMP(), end_date) >=', $iSegment->start_date)
        ->get_all();

        if(empty($segments)) return false;

        $contracts = $this->facebookads_m
        ->set_term_type()
        ->select('term.term_id,term_name,term_status,term_type')
        ->join('term_posts', 'term_posts.term_id = term.term_id')
        ->where_in('term_posts.post_id', array_map('intval', array_filter(array_column($segments, 'post_id'))))
        ->get_all();
        
        if(empty($contracts)) return false;

        return $contracts;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function contract_id_check($value)
    {
        $this->load->model('contract/contract_m');
        $isExisted = $this->contract_m->where_in('term_type', ['facebook-ads', 'google-ads', 'tiktok-ads'])->where('term_id', (int) $value)->count_by() > 0;
		if( ! $isExisted)
        {
            $this->form_validation->set_message('contract_id_check', 'ID Hợp đồng không tồn tại');
            return false;
        }
        
        return true;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function editable_mode_check($value)
    {
        $isExisted = $this->set_post_type()->where('comment_status', 'direct')->where('post_id', (int) $value)->count_by() > 0;
		if( ! $isExisted)
        {
            $this->form_validation->set_message('editable_mode_check', 'ID Phiếu chỉ có thể xem , không thể cập nhật');
            return false;
        }
        
        return true;
    }
    
    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function id_check($value)
    {
        $isExisted = $this->set_post_type()->where('post_id', (int) $value)->count_by() > 0;
		if( ! $isExisted)
        {
            $this->form_validation->set_message('id_check', 'ID không tồn tại');
            return false;
        }

        return true;
    }

    /**
     * Insert a new row into the table. $data should be an associative array
     * of data to be inserted. Returns newly created ID.
     * 
     * @param mixed $data
     * @param  $skip_validation
     * 
     * @return [type]
     */
    public function insert($data, $skip_validation = FALSE)
    {
        $result = parent::insert($data, $skip_validation);
        if(FALSE == $result) return $result;

        $audits = array();
        array_walk($data, function($value, $key) use (&$audits, $result){
            $audits[] = [ 'type' => $this->_table, 'id' => $result, 'field' => $key, 'new' => $value ];
        });

        audit($audits);
        return $result;
    }

    /**
     * Updated a record based on the primary value.
     * 
     * @param mixed $primary_value
     * @param mixed $data
     * @param  $skip_validation
     * 
     * @return [type]
     */
    public function update($primary_value, $data, $skip_validation = FALSE)
    {
        $previous   = $this->as_array()->get($primary_value);
        $result     = parent::update($primary_value, $data, $skip_validation);

        if(empty($result)) return $result;

        $changed_values = array_filter($data, function($v, $k) use($previous){
            return $previous[$k] != $v;
        }, ARRAY_FILTER_USE_BOTH);

        if(empty($changed_values)) return $result;

        $records = array();
        array_walk($changed_values, function($value, $key) use (&$records, $primary_value, $previous){

            $records[] = ['event' => 'updated', 'type' => $this->_table, 'id' => $primary_value, 'field' => $key, 'new' => $value, 'old' => $previous[$key] ];
        });

        audit($records);

        return $result;
    }

    /**
     * @param mixed $id
     * 
     * @return [type]
     */
    public function delete($id)
    {
        $result = parent::delete($id);
        if(empty($result)) return $result;
        
        audit('deleted', $this->_table, $id);
        return $result;
    }
}
/* End of file Balance_spend_m.php */
/* Location: ./application/models/Balance_spend_m.php */