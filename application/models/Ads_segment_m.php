<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH. 'models/Post_m.php');

class Ads_segment_m extends Post_m
{
    public $_table = 'posts';
    public $primary_key = 'post_id';
	
    public $post_type = 'ads_segment';

	public $before_create = array('init_default_data');
	public $before_update = array('timestamp');
	public $after_update = array('clear_cache');

    /**
     * OVERIDE UPDATE METHOD
     * Write record to audits table
     */
    public function update($primary_value, $data, $skip_validation = FALSE)
    {
        $previous = $this->as_array()->get($primary_value);
        $result = parent::update($primary_value, $data, $skip_validation);

        if(empty($result)) return $result;

        $changed_values = array_filter($data, function($v, $k) use($previous){
            return $previous[$k] != $v;
        }, ARRAY_FILTER_USE_BOTH);
        if(empty($changed_values)) return $result;

        $records = array();
        array_walk($changed_values, function($value, $key) use (&$records, $primary_value, $previous){
            $records[] = ['event' => 'updated', 'type' => $this->_table, 'id' => $primary_value, 'field' => $key, 'new' => $value, 'old' => $previous[$key] ];
        });
        audit($records);

        return $result;
    }

    /**
     * Overrides the delete method and writes a record to the audits table.
     *
     * @param int $primary_value The primary value of the record to be deleted.
     * @return bool|int The result of the delete operation.
     */
    public function delete($primary_value)
    {
        // Get the previous record before deletion
        $previous = $this->as_array()->get($primary_value);
        $previous['uuid'] = get_post_meta_value($primary_value, 'uuid');
        
        // Delete the record
        $result = parent::delete($primary_value);
        if (empty($result)) {
            return $result;
        }

        // Get the term ID associated with the deleted record
        $this->load->model('term_posts_m');
        $term_id = $this->term_posts_m
            ->where('post_id', $primary_value)
            ->select('term_id')
            ->get_by();
        
        // If no term ID is found, return the result
        if (empty($term_id)) {
            return $result;
        }

        $term_id = reset($term_id);

        // Add meta to the term ID
        $this->termmeta_m->add_meta($term_id, 'segment_deleted_id', $primary_value);

        // Log the deletion event in the audits table
        audit([
            [
                'event' => 'deleted', 
                'type' => $this->_table, 
                'id' => $primary_value, 
                'field' => '', 
                'new' => '', 
                'old' => $previous,
            ]
        ]);

        return $result;
    }

    /**
     * OVERRIDE DELETE MANY METHOD
     * Write record to audits table
     *
     * @param mixed $primary_value The primary key value(s) of the record(s) to delete
     * @return mixed The result of the parent delete_many() method
     */
    public function delete_many($primary_value)
    {
        // Get the previous segments with the given post_id values
        $previous_segments = $this->as_array()
            ->where_in('post_id', $primary_value)
            ->get_all();

        // Update the 'uuid' field of each segment with the corresponding post_id
        foreach($previous_segments as &$segments){
            $segments['uuid'] = get_post_meta_value($segments['post_id'], 'uuid');
        }

        // Load the contract_m model and retrieve the term_ids associated with the post_ids
        $this->load->model('contract_m');
        $term_ids = $this->contract_m
            ->set_term_type()
            ->join('term_posts AS tp_contract', 'tp_contract.term_id = term.term_id')
            ->where_in('tp_contract.post_id', $primary_value)
            ->group_by('tp_contract.post_id')
            ->select('term.term_id, tp_contract.post_id')
            ->as_array()
            ->get_all();

        // Call the delete_many() method of the parent class
        $result = parent::delete_many($primary_value);

        // If the result is empty, return it
        if(empty($result)) {
            return $result;
        }

        // If term_ids is empty, return the result
        if(empty($term_ids)) {
            return $result;
        }

        // Load the termmeta_m model and add meta for each post_id
        $this->load->model('termmeta_m');
        $post_ids = array_group_by($term_ids, 'post_id');
        foreach($post_ids as $post_id => $term_ids)
        {
            $term_id = reset($term_ids);
            $term_id = $term_id['term_id'] ?? null;
            if(empty($term_id))
            {
                continue;
            }

            $this->termmeta_m->add_meta($term_id, 'segment_deleted_id', $post_id);
        }

        // Add an audit record for each previous segment
        foreach($previous_segments as $segments){
            audit([
                [
                    'event' => 'deleted', 
                    'type' => $this->_table, 
                    'id' => $segments['post_id'],
                    'field' => '', 
                    'new' => '', 
                    'old' => $segments,
                ]
            ]);
        }

        return TRUE;
    }

	protected function clear_cache($args)
	{
		if( ! isset($args[2]) && is_numeric($args[2])) return $args;

		$post_id = (int) $args[2];

		$this->load->model('facebookads/adaccount_m');
		$adaccounts = $this->term_posts_m->get_post_terms($post_id, $this->adaccount_m->term_type, [ 'fields' => 'term.term_id, term_type' ]);
		$adaccounts	AND array_walk($adaccounts, function($x){	
			$this->term_posts_m->delete_posts_cached($x->term_id);
		});
		$this->scache->delete_group("post/{$post_id}_terms_");

		$this->load->model('facebookads/facebookads_m');
		$contracts = $this->term_posts_m->get_post_terms($post_id, $this->facebookads_m->term_type, [ 'fields' => 'term.term_id, term_type' ]);
		$contracts	AND array_walk($contracts, function($x){	
			$this->term_posts_m->delete_posts_cached($x->term_id);
		});


		$this->load->model('googleads/mcm_account_m');
		$adaccounts = $this->term_posts_m->get_post_terms($post_id, $this->mcm_account_m->term_type, [ 'fields' => 'term.term_id, term_type' ]);
		$adaccounts	AND array_walk($adaccounts, function($x){	
			$this->term_posts_m->delete_posts_cached($x->term_id);
		});
		$this->scache->delete_group("post/{$post_id}_terms_");

		$this->load->model('googleads/googleads_m');
		$contracts = $this->term_posts_m->get_post_terms($post_id, $this->googleads_m->term_type, [ 'fields' => 'term.term_id, term_type' ]);
		$contracts	AND array_walk($contracts, function($x){	
			$this->term_posts_m->delete_posts_cached($x->term_id);
		});

		$this->scache->delete_group("post/{$post_id}_terms_");

		return $args;
	}

	protected function timestamp($data)
	{
		$data['updated_on'] = time();
		return $data;
	}

	protected function init_default_data($data)
	{
		$data['post_type']	= $this->post_type;
		$data['created_on'] = time(); 
		return $data;
	}

	/**
	 * Sets the post type.
	 *
	 * @return     self  set default post type query for this model
	 */
	public function set_post_type()
	{
		$this->where('posts.post_type', $this->post_type);
		return $this;
	}

	/**
	 * { function_description }
	 *
	 * @param      array  $attributes  The attributes
	 *
	 * @return     self   ( description_of_the_return_value )
	 */
	public function forceFill(array $attributes)
	{
		$this->attributes = array_merge($this->attributes, $attributes);
		return $this;
	}

	public function  __set($name, $value)
	{
		$this->attributes[$name] = $value;
		$this->{$name} = $value;
		return $this;
    }

    /**
     * Attaches an ad account to a segment.
     *
     * @param int $segment_id The ID of the segment.
     * @param int $ad_acount_id The ID of the ad account.
     * @param string $type The type of the ad account.
     * @throws Some_Exception_Class If there is an error.
     * @return void
     */
    public function attach_ad_account($segment_id, $ad_acount_id, $type)
    {
        $this->load->model('term_posts_m');

        $old_ad_account_ids = $this->term_posts_m->get_the_terms($segment_id);
        if(in_array($ad_acount_id, $old_ad_account_ids))
        {
            return;
        }

        $ad_account = null;
        switch($type)
        {
            case 'mcm_account':
                $this->load->model('googleads/mcm_account_m');
                $ad_account = $this->mcm_account_m->where('term.term_id', $ad_acount_id)->as_array()->get_by();
                if(empty($ad_account))
                {
                    return;
                }

                $ad_account = [
                    'account_id' => $ad_acount_id,
                    'ad_account_id' => $ad_account['term_name'],
                    'ad_account_name' => get_term_meta_value($ad_acount_id, 'account_name'),
                ];
                break;

            case 'adaccount':
                $this->load->model('facebookads/adaccount_m');
                $ad_account = $this->adaccount_m->where('term.term_id', $ad_acount_id)->as_array()->get_by();
                if(empty($ad_account))
                {
                    return;
                }

                $ad_account = [
                    'account_id' => $ad_acount_id,
                    'ad_account_id' => $ad_account['term_slug'],
                    'ad_account_name' => $ad_account['term_name'],
                ];
                break;

            case 'tiktok_adaccount':
                $this->load->model('tiktokads/adaccount_m');
                $ad_account = $this->adaccount_m->where('term.term_id', $ad_acount_id)->as_array()->get_by();
                if(empty($ad_account))
                {
                    return;
                }

                $ad_account = [
                    'account_id' => $ad_acount_id,
                    'ad_account_id' => $ad_account['term_slug'],
                    'ad_account_name' => $ad_account['term_name'],
                ];
                break;

            default:
                return;
        }
        
        $result = $this->term_posts_m->set_post_terms($segment_id, $ad_acount_id, $type);
        if(empty($result))
        {
            return;
        }

        // Log the deletion event in the audits table
        audit([
            [
                'event' => 'ads_segment_attached_ad_account', 
                'type' => $this->_table, 
                'id' => $segment_id,
                'field' => 'ad_account', 
                'new' => $ad_account, 
                'old' => '',
            ]
        ]);
    }

    /**
     * Detach ad account from segment.
     *
     * @param int $segment_id The ID of the segment.
     * @param array $ad_account_ids The IDs of the ad accounts to detach.
     * @param string $type The type of ad account ('mcm_account', 'adaccount', or 'tiktok_adaccount').
     * @return void
     */
    public function detach_ad_account($segment_id, $ad_account_ids, $type)
    {
        $this->load->model('term_posts_m');

        $old_ad_account_ids = $this->term_posts_m->get_the_terms($segment_id);
        if(empty($old_ad_account_ids))
        {
            return;
        }

        $ad_accounts = [];
        switch($type)
        {
            case 'mcm_account':
                $this->load->model('googleads/mcm_account_m');
                $ad_accounts = $this->mcm_account_m->where_in('term.term_id', $ad_account_ids)->as_array()->get_all();
                if(empty($ad_accounts))
                {
                    return;
                }

                $ad_accounts = array_map(function($ad_account){
                    return [
                        'account_id' => $ad_account['term_id'],
                        'ad_account_id' => $ad_account['term_name'],
                        'ad_account_name' => get_term_meta_value($ad_account['term_id'], 'account_name'),
                    ];
                }, $ad_accounts);
                break;

            case 'adaccount':
                $this->load->model('facebookads/adaccount_m');
                $ad_accounts = $this->adaccount_m->where_in('term.term_id', $ad_account_ids)->as_array()->get_all();
                if(empty($ad_accounts))
                {
                    return;
                }

                $ad_accounts = array_map(function($ad_account){
                    return [
                        'account_id' => $ad_account['term_id'],
                        'ad_account_id' => $ad_account['term_slug'],
                        'ad_account_name' => $ad_account['term_name'],
                    ];
                }, $ad_accounts);
                break;

            case 'tiktok_adaccount':
                $this->load->model('tiktokads/adaccount_m');
                $ad_account = $this->adaccount_m->where_in('term.term_id', $ad_account_ids)->as_array()->get_all();
                if(empty($ad_account))
                {
                    return;
                }

                $ad_accounts = array_map(function($ad_account){
                    return [
                        'account_id' => $ad_account['term_id'],
                        'ad_account_id' => $ad_account['term_slug'],
                        'ad_account_name' => $ad_account['term_name'],
                    ];
                }, $ad_accounts);
                break;

            default:
                return;
        }
        
        $result = $this->term_posts_m->delete_post_terms($segment_id, $ad_account_ids);
        if(empty($result))
        {
            return;
        }

        // Log the deletion event in the audits table
        $audits = [];
        foreach($ad_accounts as $ad_account)
        {
            $audits[] = [
                    'event' => 'ads_segment_detached_ad_account', 
                    'type' => $this->_table, 
                    'id' => $segment_id,
                    'field' => 'ad_account', 
                    'new' => $ad_account, 
                    'old' => '',
            ];
        }
        audit($audits);
    }
}
/* End of file AdsSegment.php */
/* Location: ./application/modules/contract/models/AdsSegment.php */