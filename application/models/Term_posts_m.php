<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Term_posts_m extends Base_model {
	public $primary_key = 'term_id';
	public $_table = "term_posts";
	
	function get_the_terms($post_id = 0, $taxonomy = '')
	{
		if( ! $post_id) return FALSE;

		if(is_array($taxonomy) && !empty($taxonomy))
		{
			$this->join('term','term.term_id = term_posts.term_id');
			$this->where_in('term_type',$taxonomy);	
		}
		else if ($taxonomy != '')
		{
			$this->join('term','term.term_id = term_posts.term_id');
			$this->where('term_type',$taxonomy);
		}

		$this->where('term_posts.post_id', $post_id);
		$result = $this->select('term_posts.term_id')->as_array()->get_all();

		if($result)
		{
			return array_column($result, 'term_id');
		}
		return array();
	}

	/**
	 * Set the terms for a post.
	 *
	 *
	 * @see wp_set_object_terms()
	 *
	 * @param int    $post_id  Optional. The Post ID. Does not default to the ID of the global $post.
	 * @param string $tags     Optional. The tags to set for the post, separated by commas. Default empty.
	 * @param string $taxonomy Optional. Taxonomy name. Default 'post_tag'.
	 * @param bool   $append   Optional. If true, don't delete existing tags, just add on. If false,
	 *                         replace the tags with the new tags. Default false.
	 * @return mixed Array of affected term IDs. False on failure.
	 */
	function set_post_terms( $post_id = 0, $terms = array(), $taxonomy='category' ) {
		$post_id = (int) $post_id;

		if ( !$post_id)
			return false;

		if(!is_array($terms)) 
			$terms = array($terms);

        // Insert new terms for the post
		$old_term_posts = $this->get_the_terms($post_id, $taxonomy);
		if($old_term_posts)
		{
			foreach($old_term_posts as $old_term_post){
				if(!in_array($old_term_post, $terms))
				{
					$this->delete_by(array(
						'post_id' => $post_id, 
						'term_id' => $old_term_post
						));
				}
			}
		}

		if($terms)
		{
			foreach($terms as $term)
			{
				if($term && !in_array($term, $old_term_posts))
				{
					$this->insert(array(
						'post_id' => $post_id,
						'term_id' => $term
						));
				}
			}
		}

		return true;
	}

	/**
	 * Delete term posts
	 *
	 * @param      integer  $term_id   The term identifier
	 * @param      array    $posts     The posts
	 * @param      string   $taxonomy  The taxonomy
	 *
	 * @return     boolean  TRUE 	if successfuly, otherwise return FALSE;
	 */
	function delete_term_posts($term_id = 0, $posts = array(), $taxonomy = 'category')
	{
		$term_id = (int) $term_id;
		if( ! $term_id) return FALSE;

		if(empty($posts)) return true;

		foreach($posts as $post_id)
		{
			$this->delete_post_terms($post_id, [$term_id]);
		}

		return TRUE;
	}

	/**
	 * Delete term posts
	 *
	 * @param      integer  $term_id   The term identifier
	 * @param      array    $posts     The posts
	 * @param      string   $taxonomy  The taxonomy
	 *
	 * @return     boolean  TRUE 	if successfuly, otherwise return FALSE;
	 */
	function delete_post_terms($post_id = 0, $term_ids = array(), $taxonomy = 'category')
	{
		$post_id = (int) $post_id;
		if(empty($post_id)) return false;

		if($term_ids)
		{
			$this->where_in('term_id', $term_ids)->where('post_id', $post_id)->delete_by();
			array_walk($term_ids, function($term_id){
				$this->scache->delete_group("term/{$term_id}_posts_");	
			});
		}

		$this->scache->delete_group("post/{$post_id}_terms_");
		return TRUE;
	}


	/**
	 * Set the posts for term.
	 *
	 *
	 * @see        set_term_posts()
	 *
	 * @param      int     $term_id   Optional. The User ID. Does not default to the ID of the global $user.
	 * @param      array   $posts     The posts
	 * @param      string  $taxonomy  Optional. Taxonomy name. Default 'category'.
	 * @param      bool  $append  Optional. If true, don't delete existing tags, just
	 *                            add on. If false, replace the tags with the new tags.
	 *                            Default false.
	 *
	 * @return     boolean 	TRUE if affected term IDs. False on failure.
	 */
	function set_term_posts( $term_id = 0, $posts = array(), $taxonomy = 'category', $append = TRUE)
	{
		if(empty($term_id) || empty($posts)) return FALSE;

		$term_id 	= (int) $term_id;
		$term_posts = $this->get_term_posts($term_id, $taxonomy);
		$term_posts = $term_posts ? array_column($term_posts, 'post_id') : [];

		if( ! empty($term_posts) && FALSE === $append)
		{
			$this->delete_term_posts($term_id, $term_posts, $taxonomy);
			$term_posts = array();
		}

		foreach($posts as $post_id)
		{
			if( in_array($post_id, $term_posts)) continue;
			$this->insert(array('post_id' => $post_id, 'term_id' => $term_id ));
		}

		$this->scache->delete_group("term/{$term_id}_posts_");
		return TRUE;
	}


	/*
	Retrieve the terms for a post.
	Return: List of post tags.
	*/
	function get_post_terms( $post_id = 0, $taxonomy = 'post_tag', $args = array() ) 
	{
		$post_id = (int) $post_id;
		$this->load->helper('array');
		$defaults = array('orderby' => 'term.term_id', 'order' => 'ASC', 'fields' => 'all', 'where'=>'');
		$args = wp_parse_args( $args, $defaults );
		$j_taxonomy = $taxonomy;
		if(is_array($taxonomy))
			$j_taxonomy = md5(json_encode($taxonomy));
		
		$key_cache = 'post/'.$post_id.'_terms_'.$j_taxonomy;
		array_multisort($args);
		$key_cache.= '-'.md5(json_encode($args));

		$tags = $this->scache->get($key_cache);
		if(!$tags)
		{
			if($args['fields'] !='all')
				$this->select($args['fields']);
			if($args['where'] !='')
				$this->where($args['where']);

			$this->where('post_id',$post_id);

			$this->join('term','term.term_id = term_posts.term_id');

			if(is_array($taxonomy))
				$this->where_in('term_type',$taxonomy);
			else
				$this->where('term_type',$taxonomy);

			$this->order_by($args['orderby'], $args['order']);
			$tags = $this->get_many_by();
			$tags = key_array_value($tags, 'term_type');
			$this->scache->write($tags, $key_cache);
		}

		if($taxonomy ==='')
			return $tags;
		else if(is_string($taxonomy))
			return isset($tags[$taxonomy]) ? $tags[$taxonomy] : FALSE;
		else
			return elements($taxonomy ,$tags);
	}

	/**
	 * Gets the users.
	 *
	 * @param      integer  $term_id   The term identifier
	 * @param      string   $taxonomy  The taxonomy
	 * @param      array    $args      The arguments
	 *
	 * @return     <type>   The users.
	 */
	function get_term_posts($term_id, $taxonomy, $args = array())
	{
		$term_id = (int) $term_id;

		if(empty($taxonomy)) return FALSE;

		$taxonomies = array();
		if(is_array($taxonomy)) $taxonomies = $taxonomy;
		else if(is_string($taxonomy)) $taxonomies = explode(',', $taxonomy);

		$taxonomies_encrypt	= md5(json_encode($taxonomies));
		$defaults 			= array('orderby' => 'posts.post_id', 'order' => 'ASC', 'fields' => 'all', 'where'=>'');
		$args 				= wp_parse_args( $args, $defaults );
		$args_encrypt		= md5(json_encode($args));

		$key_cache	= "term/{$term_id}_posts_{$taxonomies_encrypt}_{$args_encrypt}";
		$result		= $this->scache->get($key_cache);
		if($result) return $result;

		if($args['fields'] !='all') $this->select($args['fields']);
		if(!empty($args['where'])) $this->where($args['where']);
		if(!empty($args['where_in'])) $this->where_in('term_posts.post_id', $args['where_in']);

		$posts = $this
		->join('posts','posts.post_id = term_posts.post_id')
		->order_by($args['orderby'],$args['order'])
		->where('term_posts.term_id',$term_id)
		->where_in('posts.post_type',$taxonomies)
		->get_all();
		
		if( ! $posts) return FALSE;

		$posts = array_column($posts, NULL,'post_id');
		$this->scache->write($posts,$key_cache);

		return $posts;
	}

	public function delete_posts_cached($term_id = 0)
	{
		$this->scache->delete_group("term/{$term_id}_posts_");
	}
}
/* End of file Term_categories_m.php */
/* Location: ./application/modules/contract/models/Term_categories_m.php */