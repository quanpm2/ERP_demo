<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class File_m extends Base_model {

	public $_table = '_files';

    public $primary_key = "file_id";
}
?>