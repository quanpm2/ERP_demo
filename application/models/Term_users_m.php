<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Term_users_m extends Base_model {

	public $primary_key = 'object_id';

	public $_table = 'term_users';

	function get_the_terms($user_id = 0, $taxonomy = '') {

		if(!$user_id) return false;

		$this->as_array();

		$where = array();

		if($taxonomy != ''){

			$this->join('term','term.term_id = term_users.term_id');

			$where['term_type'] = $taxonomy;
		}

		$where['term_users.user_id'] = $user_id;

		$result = $this->get_many_by($where);
		if($result){

			return array_column($result, 'term_id');
		}

		return array();
	}

	/**
	 * Set the terms for a user.
	 *
	 *
	 * @see wp_set_object_terms()
	 *
	 * @param int    $user_id  Optional. The User ID. Does not default to the ID of the global $user.
	 * @param string $tags     Optional. The tags to set for the user, separated by commas. Default empty.
	 * @param string $taxonomy Optional. Taxonomy name. Default 'website'.
	 * @param bool   $append   Optional. If true, don't delete existing tags, just add on. If false,
	 *                         replace the tags with the new tags. Default false.
	 * @return mixed Array of affected term IDs. False on failure.
	 */
	function set_user_terms( $user_id = 0, $terms = array(), $taxonomy = 'website' ) {

		$user_id = (int) $user_id;

		if( ! $user_id) return FALSE;

		$terms = is_array($terms) ? $terms : array($terms);

        // Insert new terms for the user
		$old_term_users = $this->get_the_terms($user_id, $taxonomy);

		if( ! empty($old_term_users))
		{
			foreach($old_term_users as $term_id)
			{
				if(in_array($term_id, $terms)) continue;

				$this->delete_by(array('user_id' => $user_id,'term_id' => $term_id));
				$this->scache->delete_group("user/{$term_id}_users_");
			}
		}

		$this->scache->delete_group("user/{$user_id}_terms");
		$this->scache->delete_group('user');

		if( ! $terms) return TRUE;

		foreach($terms as $term)
		{
			if(empty($term) || in_array($term, $old_term_users)) continue;

			$this->insert(['user_id'=>$user_id,'term_id'=>$term]);
		}
		
		return TRUE;
	}

	function delete_term_users($term_id=0,$users = array(),$taxonomy = 'website'){

		$term_id = (int) $term_id;

		if( ! $term_id || empty($users)) return FALSE;

		if( ! is_array($users))
		{
			$users = [$users];
		}

		$this->where_in('user_id', $users)->delete_by(['term_id'=>$term_id]);
		$this->scache->delete_group("user/{$term_id}_users_");
		array_walk($users, function($x){
			$this->scache->delete_group("user/{$x}_terms_");
		});
		
		return TRUE;
	}

	function set_term_users( $term_id = 0, $users = array(), $taxonomy = '',$args = array()) {

		$term_id = (int) $term_id;

		if ( !$term_id){
			return false;
		}

		$old_terms_user = $this->get_term_users($term_id,$taxonomy);
		$id_old_terms_user = array();

		if($old_terms_user){

			foreach($old_terms_user as $old_term_user){

				if(!empty($old_term_user)){

					array_push($id_old_terms_user, $old_term_user->user_id);
				}
			}
		}

		if($users){

			if(isset($args['command'])){

				if($args['command'] == 'replace'){

					if(!empty($id_old_terms_user))
					{
						$this->delete_term_users($term_id,$id_old_terms_user,$taxonomy);
						$id_old_terms_user = array();
					}

				}
			}

			foreach($users as $user) {

				if(!in_array($user, $id_old_terms_user)){

					$this->insert(array(
						'user_id' => $user,
						'term_id' => $term_id
						));
				}
			}
		}
        else if($args['command'] == 'remove'){
            $this->delete_term_users($term_id,$id_old_terms_user,$taxonomy);
        }

		// Clear All cache related
		$this->scache->delete_group("user/{$term_id}_users_");	
		
		return true;
	}

	public function get_term_users($term_id = 0,$taxonomy = 'website',$args = array())
	{
		$term_id = (int) $term_id;

		$this->load->helper('array');

		$defaults = array('orderby' => 'user.user_id', 'order' => 'ASC', 'fields' => 'all', 'where'=>'');

		$args = wp_parse_args( $args, $defaults );

		array_multisort($args);

		if($args['fields'] !='all'){

			$this->select($args['fields']);
		}

		if($args['where'] !=''){

			$this->where($args['where']);
		}

		if( ! empty($args['where_not_in']) && is_array($args['where_not_in']))
		{
			$k = key($args['where_not_in']);
			$v = $args['where_not_in'][$k];
			$this->where_not_in($k, $v);
		}

		if( ! empty($args['where_in']) && is_array($args['where_in']))
		{
			$k = key($args['where_in']);
			$v = $args['where_in'][$k];
			$this->where_in($k, $v);
		}

		$this->where('term_id',$term_id);

		$this->join('user','user.user_id = term_users.user_id');

		if(is_array($taxonomy)){

			$this->where_in('user_type',$taxonomy);
		}
		else{

			$this->where('user_type',$taxonomy);
		}

		$this->order_by($args['orderby'], $args['order']);

		$tags = $this->get_many_by();

		return $tags;
	}

	/**
	 * Gets the users.
	 *
	 * @param      integer  $term_id   The term identifier
	 * @param      string   $taxonomy  The taxonomy
	 * @param      array    $args      The arguments
	 *
	 * @return     <type>   The users.
	 */
	function get_the_users($term_id = 0,$taxonomy = '',$args = array())
	{
		$term_id = (int) $term_id;

		if(empty($taxonomy)) return FALSE;

		$taxonomies = array();
		if(is_array($taxonomy)) $taxonomies = $taxonomy;
		else if(is_string($taxonomy)) $taxonomies = explode(',', $taxonomy);

		$taxonomies_encrypt	= md5(json_encode($taxonomies));
		$defaults 			= array('orderby' => 'user.user_id', 'order' => 'ASC', 'fields' => 'all', 'where'=>'');
		$args 				= wp_parse_args( $args, $defaults );
		$args_encrypt		= md5(json_encode($args));

		$key_cache	= "user/{$term_id}_users_{$taxonomies_encrypt}_{$args_encrypt}";
		$result		= $this->scache->get($key_cache);
		if($result) return $result;

		if($args['fields'] !='all') $this->select($args['fields']);
		if(!empty($args['where'])) $this->where($args['where']);
		if(!empty($args['where_in'])) $this->where_in($args['where_in']);

		$users = $this
		->join('user','user.user_id = term_users.user_id')
		->order_by($args['orderby'],$args['order'])
		->where('term_users.term_id',$term_id)
		->where_in('user.user_type',$taxonomies)
		->get_all();

		if( ! $users) return FALSE;

		$users = array_column($users, NULL,'user_id');
		$this->scache->write($users,$key_cache);

		return $users;
	}


	/**
	 * Sets the relations by term.
	 *
	 * @param      <type>  $term_id   The term identifier
	 * @param      array   $users     The users
	 * @param      string  $taxonomy  The taxonomy
	 * @param      array   $args      The arguments
	 * @param      <type>  $replace   The replace
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	function set_relations_by_term($term_id,$users = array(), $taxonomy = '',$args = array(),$replace = FALSE)
	{
		$term_id = (int) $term_id;
		if(empty($term_id)) return FALSE;

		// $old_term_users = $this->get_relations_by_term($term_id);
		$old_users 		= $this->get_the_users($term_id, $taxonomy, $args);
		$old_users_id 	= $old_users ? array_column($old_users,'user_id') : array();
		
		if($replace && !empty($old_users_id))
		{
			$this->where_in('user_id', $old_users_id)->where('term_id',$term_id)->delete_by();
			$old_users_id = array();
		}

		if(!empty($users))
		{
			foreach ($users as $user_id)
			{
				if(in_array($user_id, $old_users_id)) continue;

				$this->insert(['user_id'=>$user_id,'term_id'=>$term_id]);
				
				$this->scache->delete_group("user/{$user_id}_terms_");	
			}
		}

		// Clear All cache related
		$this->scache->delete_group("user/{$term_id}_users_");	
	}

	/**
	 * Gets the user terms.
	 *
	 * @param      integer|string  $user_id   The user identifier
	 * @param      string          $taxonomy  The taxonomy
	 * @param      array           $args      The arguments
	 *
	 * @return     <type>          The user terms.
	 */
	function get_user_terms($user_id = 0, $taxonomy = '', $args = array() ) 
	{
		$user_id = (int) $user_id;

		if(empty($taxonomy)) return FALSE;

		$taxonomies = array();
		if(is_array($taxonomy)) $taxonomies = $taxonomy;
		else if(is_string($taxonomy)) $taxonomies = explode(',', $taxonomy);

		$taxonomies_encrypt	= md5(json_encode($taxonomies));
		$defaults 			= array('orderby' => 'term.term_id', 'order' => 'DESC', 'fields' => 'all', 'where'=>'');
		$args 				= wp_parse_args( $args, $defaults );
		$args_encrypt		= md5(json_encode($args));

		$key_cache	= "user/{$user_id}_terms_{$taxonomies_encrypt}_{$args_encrypt}";
		$result		= $this->scache->get($key_cache);
		if($result) return $result;

		if($args['fields'] !='all') $this->select($args['fields']);
		if(!empty($args['where'])) $this->where($args['where']);
		if(!empty($args['where_in'])) 
		{
			if(is_array($args['where_in']))
				foreach ($args['where_in'] as $key => $value)
					$this->where_in($key,$value);
		}

		$terms = $this
		->join('term','term.term_id = term_users.term_id')
		->order_by($args['orderby'],$args['order'])
		->where('term_users.user_id',$user_id)
		->where_in('term.term_type',$taxonomies)
		->get_all();

		if( ! $terms) return FALSE;

		$terms = array_column($terms, NULL,'term_id');
		$this->scache->write($terms,$key_cache,3600);

		return $terms;
	}
}
/* End of file Term_users_m.php */
/* Location: ./application/models/Term_users_m.php */