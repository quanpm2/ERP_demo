<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Termmeta_m extends Meta_m
{
    public $primary_key = 'meta_id';
    public $object_id = 'term_id';
    public $_table = 'termmeta';
    public $_reference_table = 'term';
    public $meta_type = 'term';

    function update_meta($term_id, $meta_key, $meta_value, $prev_value = '', $event = '')
    {
        $meta_ids = $this->as_array()->get_many_by(array('term_id' => $term_id, 'meta_key' => $meta_key));
        if (empty($meta_ids)) {
            return $this->add_metadata($this->meta_type, $term_id, $meta_key, $meta_value);
        }

        if (empty($prev_value)) {
            $prev_value = get_term_meta_value($term_id, $meta_key);
        }

        $result = parent::update_meta($term_id, $meta_key, $meta_value, $prev_value);

        // Data is not change
        if ($prev_value == $meta_value) return $result;

        // Audit
        audit($event ? $event : 'updated', $this->_table, $term_id, $meta_key, $meta_value, $prev_value);

        // Update
        return $result;
    }

    function add_metadata($meta_type, $object_id, $meta_key, $meta_value, $unique = false)
    {
        // Create
        $result = parent::add_metadata($meta_type, $object_id, $meta_key, $meta_value, $unique);
        if (FALSE == $result) return $result;

        // Audit
        audit('created', $this->_table, $object_id, $meta_key, $meta_value);

        return $result;
    }
}
