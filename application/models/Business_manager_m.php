<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH. 'models/Term_m.php');

class Business_manager_m extends Term_m
{
    public $term_type = 'business_manager';

    /**
     * Sets the term type.
     *
     * @return     self  set default term type query for this model
     */
    public function set_term_type()
    {
        return $this->where('term.term_type',$this->term_type);
    }

    /**
     * { function_description }
     *
     * @param      array  $attributes  The attributes
     *
     * @return     self   ( description_of_the_return_value )
     */
    public function forceFill(array $attributes)
    {
        $this->attributes = array_merge($this->attributes, $attributes);
        return $this;
    }

    public function  __set($name, $value)
    {
        $this->attributes[$name] = $value;
        $this->{$name} = $value;
        return $this;
    }
}
/* End of file Business_manager_m.php */
/* Location: ./application/modules/contract/models/Business_manager_m.php */