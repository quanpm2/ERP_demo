<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Client_m extends Base_model {

	public $_table = '_clients';

    public $primary_key = "cli_id";
}
?>