<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_account_m extends Base_model
{
	public $primary_key = 'id';
	public $_table = 'ad_account';
    
    /**
     * force_fill
     *
     * @param  array $attributes
     * @return void
     */
    public function force_fill(array $attributes)
    {
        $this->attributes = array_merge($this->attributes, $attributes);
        return $this;
    }

    /**
     * get_ad_business_manager
     *
     * @param  int $ad_account_id
     * @return array
     */
    public function get_ad_business_manager(int $ad_account_id = 0)
    {
        $result = $this->join('ad_business_managers_ad_accounts', 'ad_business_managers_ad_accounts.ad_account_id = ad_account.id')
            ->join('ad_business_manager', 'ad_business_manager.id = ad_business_managers_ad_accounts.bm_id')
            ->select('ad_business_manager.*')
            ->select('ad_business_managers_ad_accounts.type')
            ->select('ad_business_managers_ad_accounts.status')
            ->where('ad_account.id', $ad_account_id)
            ->as_array()
            ->get_all();
        if(empty($result)) return [];
        
        return $result;
    }

    /**
     * get_ad_business_manager
     *
     * @param  int $ad_account_id
     * @return array
     */
    public function get_industries(int $ad_account_id = 0)
    {
        $result = $this->join('term_categories', 'term_categories.term_id = ad_account.original_term_id')
            ->join('term AS categories', 'categories.term_id = term_categories.cat_id AND categories.term_type = "category"')
            ->select('categories.*')
            ->where('ad_account.id', $ad_account_id)
            ->as_array()
            ->get_all();
        if(empty($result)) return [];
        
        return $result;
    }
}
/* End of file AdsSegment.php */
/* Location: ./application/modules/contract/models/AdsSegment.php */