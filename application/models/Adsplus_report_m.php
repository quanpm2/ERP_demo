<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Base_report_m.php');

class Adsplus_report_m extends Base_report_m {

    protected $curators = [];

    function __construct()
    {
        $this->recipients['cc'][]   = '';
        parent::__construct();
    }

    /**
     * Sends a started mail to local staffs - department
     */
    public function send_started_email()
    {
        if( ! $this->term) return FALSE;

        $this->autogen_msg  = TRUE;

        $term_id        = $this->term->term_id;
        $trading_type   = get_term_meta_value($term_id, 'trading_type');
        if($trading_type == 'agency') return FALSE;

        $this->set_mail_to('admin'); /* Add người nhận mặc định theo hợp đồng vào email */
        $this->set_send_from('reporting'); /* Cấu hình email gửi tự động random */

        $this->data['subject'] = "[ADSPLUS] THÔNG BÁO THỰC HIỆN HĐ {$this->data['contract_code']}";
        $content = 'Dear nhóm Adsplus <br>';
        $content.= 'Thông tin hợp đồng Adsplus vừa được khởi chạy như sau: <br>';
        $content.= anchor(admin_url("googleads/setting/{$term_id}"),'Xin vui lòng vào link sau để cấu hình thông tin Adword và thông tin người giám sát để nhận thống kê số liệu hằng ngày <br>', 'target="_blank"');

        $this->config->load('table_mail');
        $this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Thông tin hợp đồng');
        $this->table->add_row('Mã hợp đồng:', $this->data['contract_code'])->add_row('Tên dịch vụ:', 'Dịch vụ Adsplus');

        $default_network_types = $this->config->item('network_types');
        $network_type = explode(',', get_term_meta_value($term_id,'network_type'));
        $service_type = get_term_meta_value($term_id, 'service_type');

        $this->table
        ->add_row('Gói dịch vụ:', implode(', ', array_map(function($x) use ($default_network_types) { return $default_network_types[$x]??FALSE; },$network_type)))
        ->add_row('Loại hình:', $this->config->item($service_type,'googleads_service_type'));

        $vat = get_term_meta_value($term_id,'vat');
        $this->table->add_row('Thuế VAT:', (($vat > 0) ? 'Có' : 'Không'));
        $this->table->add_row('Thời gian bắt đầu (trên HĐ):', my_date(($this->data['contract_begin'] ?? ''),'d/m/Y'));
        $this->table->add_row('Thời gian kết thúc (trên HĐ):', my_date(($this->data['contract_end'] ?? ''),'d/m/Y'));

        $googleads_begin_time = get_term_meta_value($term_id, 'googleads-begin_time');
        $this->table->add_row('Thời gian bắt đầu thực tế:', my_date($googleads_begin_time,'d/m/Y'));

        $content.= $this->table->generate();

        $this->table->clear();
        $this->table->set_caption('Thông tin nhận báo cáo');

        if( ! empty($this->curators))
        {
            foreach ($this->curators as $key => $curator) 
            {
                if(empty($curator['name'])) continue;

                $this->table
                ->add_row('<b>Người nhận báo cáo ('.($key+1).')</b>',"<b>{$curator['name']}</b>")
                ->add_row('Mail liên hệ', $curator['email'])
                ->add_row('Số điện thoại', $curator['phone'])
                ->add_row();
            }
        }

        $content.= $this->table->generate();
        $this->data['content'] = $content;

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        $result = $this->send_email();
        return $result;
    }

    /**
     * Sends an activation email.
     *
     * @param      string  $type_to  The type to
     */
    public function send_activation_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;

        $this->autogen_msg          = TRUE;
        $this->recipients['cc'][]   = 'ketoan@adsplus.vn';

        $this->set_mail_to($type_to); /* Add người nhận mặc định theo hợp đồng vào email */
        $this->set_send_from('reporting'); /* Cấu hình email gửi tự động random */

        $default = array(
            'term'      => $this->term,
            'term_id'   => $this->term_id,
            'title'     => 'Thông tin kích hoạt dịch vụ',
            'subject'   => '[ADSPLUS.VN] Thông báo kích hoạt dịch vụ Adsplus của website '.$this->term->term_name,
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'content_tpl' => 'googleads/report/activation_email',
        );

        $this->data = wp_parse_args($this->data, $default);
        if( empty($this->data['content_tpl'])) return FALSE;

        $this->data['content'] = $this->render_content($this->data['content_tpl']);

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        $result = $this->send_email();
        return $result;
    }

    /**
     * Sends a finished email.
     *
     * @param      string  $type_to  The type to
     */
    public function send_finished_email($type_to = 'admin')
    {
        if( ! $this->term) return FALSE;

        $this->autogen_msg          = TRUE;
        $this->recipients['cc'][]   = 'ketoan@adsplus.vn';

        /*
         * contract_code
         * thời gian hợp đồng
         * 
         */
        $this->data['contract_code']    = get_term_meta_value($this->term_id,'contract_code');

        $contract_begin = get_term_meta_value($this->term_id, 'contract_begin');
        $contract_end   = get_term_meta_value($this->term_id, 'contract_end');
        $this->data['contract_begin']   = $contract_begin;
        $this->data['contract_end']     = $contract_end;
        $this->data['contract_date']    = my_date($contract_begin,'d/m/Y').' - '.my_date($contract_end,'d/m/Y');

        $this->data['title']        = 'Thông tin kết thúc dịch vụ';
        $this->data['content_tpl']  = 'googleads/report/finished_email';
        
        $_subject_prefix            = ($type_to != 'customer') ? '[XEM TRƯỚC]' : '';
        $this->data['subject']      = "{$_subject_prefix} Thông báo kết thúc dịch vụ Adsplus của website {$this->term->term_name}";

        if( ! parent::prepare_email_data($type_to)) return FALSE;
        return parent::send_email();
    }

    /**
     * Sends a progress email.
     *
     * @param      string  $budget_type  The budget type commit : progress data
     *                                   with contract budget actual : progress
     *                                   data with actual payment budget
     * @param      string  $type_to  The type to
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function send_progress_email($budget_type = 'commit')
    {
        if( ! $this->term) return FALSE;

        parent::set_inc_ugroups(FALSE);
        parent::set_inc_departments(TRUE);
        
        $this->set_mail_to('admin'); /* Add người nhận mặc định theo hợp đồng vào email */
        $this->set_send_from('warning'); /* Cấu hình email gửi tự động random */

        $budget_text = '';
        $actual_progress_percent = 0;
        switch ($budget_type)
        {
            case 'actual':
                $actual_progress_percent = $this->data['actual_progress_percent_net']*100;
                $budget_text = 'tiến độ thực thu';
                break;
            
            default:
                $actual_progress_percent = $this->data['actual_progress_percent'];
                $budget_text = 'tiến độ cam kết';
                break;
        }
        
        $actual_progress_percent_f = currency_numberformat($actual_progress_percent, '%', 2);
        
        $mail_id        = 'ID:'.time();
        $title          = "{$this->term->term_name} [{$actual_progress_percent_f}] {$budget_text} {$mail_id}";

        $contract_m     = (new contract_m())->set_contract($this->term);
        $contract_days  = $contract_m->get_behaviour_m()->calc_contract_days();
        $start_service_time = get_term_meta_value($this->term_id, 'start_service_time');
        $end_service_time   = strtotime("+{$contract_days} day", $start_service_time);
        $contract_budget    = $contract_m->get_behaviour_m()->calc_budget();
        $actual_budget      = $contract_m->get_behaviour_m()->calc_budget();
        
        $type       = $this->term->term_type ?? 'google-ads';
        $subject    = $type == 'facebook-ads' ? "[CẢNH BÁO][FB] {$title}" : "[ADSPLUS.VN] {$title}";

        $default = array(
            'term'      => $this->term,
            'term_id'   => $this->term_id,
            'title'     => $title,
            'subject'   => $subject,
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'content_tpl' => 'googleads/report/progress_email',
        );

        $this->data = wp_parse_args($this->data, $default);
        if( empty($this->data['content_tpl'])) return FALSE;

        $this->data['content'] = $this->render_content($this->data['content_tpl']);

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);
        $result = $this->send_email();
        return $result;
    }


    /**
     * Clear all data relate with contract
     *
     * @return     self
     */
    public function clear()
    {
        parent::clear();

        $this->recipients  = array('to' => [], 'cc' => [], 'bcc' => 'mailreport@webdoctor.vn');
        $this->curators    = array();

        return $this;
    }

    public function set_send_from($group_name = FALSE)
    {
        if($group_name == FALSE) return FALSE;

        $this->config->load('googleads/email');
        $group_name     = strtolower(trim($group_name));
        $conf_emails    = $this->config->item($group_name,'email-groups');
        if(empty($conf_emails)) return FALSE;

        $rand_key = array_rand($conf_emails);
        $this->email->initialize($conf_emails[$rand_key]);

        return TRUE;
    }


    /**
     * Sets the mail to.
     *
     * @param      string  $type_to  The type to
     *
     * @return     self    ( description_of_the_return_value )
     */
    public function set_mail_to($type_to = 'admin')
    {
        parent::set_mail_to($type_to);
        if($type_to == 'customer')
        {
            /* Append curator of contract recipients */
            if($this->curators) parent::add_to(array_filter(array_column($this->curators, 'email')));
            return $this;
        }

        return $this;
    }

    public function init($term_id = 0)
    {
        parent::init($term_id);
        if( ! $this->term_id) return FALSE;

        $contract_m     = (new contract_m())->set_contract($this->term);
        $contract_days  = 0;
        try { $contract_days = $contract_m->get_behaviour_m()->calc_contract_days(); }  catch (Exception $e){}

        $contract_budget = 0;
        try { $contract_budget = $contract_m->get_behaviour_m()->calc_budget(); }  catch (Exception $e){}

        $actual_result = 0;
        try { $actual_result = $contract_m->get_behaviour_m()->get_actual_result(); }  catch (Exception $e){}

        $term_id            = $this->term->term_id;
        $start_service_time = get_term_meta_value($term_id, 'start_service_time') ?: 0;
        $end_service_time   = strtotime("+{$contract_days} day", $start_service_time);

        $data = array(
            'contract_code' => get_term_meta_value($term_id, 'contract_code'),
            'contract_begin' => get_term_meta_value($term_id, 'contract_begin'),
            'contract_end' => get_term_meta_value($term_id, 'contract_end'),

            'actual_budget' => (float) get_term_meta_value($term_id, 'actual_budget'),
            
            'expected_end_time' => get_term_meta_value($term_id, 'expected_end_time'),
            'payment_expected_end_time' => get_term_meta_value($term_id, 'payment_expected_end_time'),

            'real_progress' => get_term_meta_value($term_id, 'real_progress'),
            'payment_real_progress' => get_term_meta_value($term_id, 'payment_real_progress'),

            'actual_progress_percent' => get_term_meta_value($term_id, 'actual_progress_percent'),
            'actual_progress_percent_net' => get_term_meta_value($term_id, 'actual_progress_percent_net'),

            'start_service_time' => $start_service_time,
            'end_service_time' => $end_service_time,
            'contract_days' => $contract_days,
            'contract_budget' => $contract_budget,
            'actual_result' => $actual_result
        );

        $this->data = wp_parse_args($data, $this->data);
        return $this;
    }
    
    public function init_people_belongs()
    {
        if( ! $this->term) return FALSE;

        $this->load_curators();
        return parent::init_people_belongs();
    }

    /**
     * Loads a customer.
     *
     * @return     self
     */
    public function load_customer()
    {
        parent::load_customer();
        if( ! $this->term) return FALSE;

        $this->load_curators();
        return $this;
    }

    /**
     * Loads curators.
     *
     * @return     self
     */
    public function load_curators()
    {
        if( ! $this->term) return FALSE;

        $curators = get_term_meta_value($this->term_id, 'contract_curators');
        $curators = @unserialize($curators);

        if( empty($curators)) return FALSE;
        $this->curators = $curators;

        return $this;
    }


    /**
     * Render Send content;
     *
     * @param      <type>  $content_view  The content view
     * @param      string  $layout        The layout
     *
     * @return     Mixed  Result
     */
    public function render_content($content_view, $layout = 'report/template/email/adsplus')
    {
        if( ! $content_view || ! $layout) return FALSE;

        $data = $this->data;
        if( ! is_array($data)) $data = array($data);

        $data['content'] = $this->load->view($content_view, $data, TRUE);
        $content        = $this->load->view($layout, $data, TRUE);
        if( ! $content) return FALSE;
        return $content;
    }
}
/* End of file Adsplus_report_m.php */
/* Location: ./application/modules/googleads/models/Adsplus_report_m.php */