<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:15px solid #fff; margin: auto; max-width:640px">
  <?php
  if(!empty($direct_downlink))
  {
    echo '<tr><td>';
    echo '* Link tải báo cáo dự phòng :';
    echo implode(', ', $direct_downlink);
    echo '</td></td>';
  }
  ?>
  <tr>
    <td>
      <table width="100%" height="50" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="4%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td bgcolor="#f58220" style="color:#f58220;" ><br/>
                  <br/>
                </td>
              </tr>
            </table>
          </td>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Roboto;font-size:0.8em; font-weight:500; border-bottom:#d4670a 3px solid; padding:0 20px;  ">
              <tr>
                <td width="40%" ><a href="#"><img alt="logo Webdoctor" src="http://webdoctor.vn/images/adsplus-logo.png" style="max-width:100%; padding-bottom:6px"></a>
                </td>
                <td style="text-align:right" ><a target="_blank" href="mailto:sales@adsplus.vn" style=" text-decoration:none; color:#333;"><img alt="icon-mail" src="http://webdoctor.vn/images/icon-mail2.png" style="float:right; padding-right:6px; padding-left:5px;" />sales@adsplus.vn </a><br />
                  <a target="_blank" rel="nofollow" href="tel:0873004488" style=" text-decoration:none; color:#333;"><img alt="icon-phone" src="http://webdoctor.vn/images/icon-phone2.png" style="float:right; padding-right:6px; " /> 08.7300 4488 </a>&nbsp;
                </td>
              </tr>
            </table>
          </td>
          <td width="4%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td >&nbsp;</td>
              </tr>
              <tr>
                <td bgcolor="#f58220" style="color:#f58220;" ><br/>
                  <br/>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
    
  <tr  bgcolor="rgb(245, 130, 32);">
    <td align="center">
      <p style="font-family:Roboto;color:#ffffff;font-size:30px; font-weight:300; margin:5px 0;  text-transform:uppercase;">
      [GURU] Ý KIẾN KHÁCH HÀNG
      </p>
    </td>
  </tr>
  <tr>
    <td align="center">
      <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px"></p>
    </td>
  </tr>

  <?php if ($customer_meta): ?>
  <tr>
    <td bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6;border-right: 20px solid #e6e6e6;  ">
      <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
          <tr>
            <td bgcolor="#f58220" >
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px">
                <tbody>
                  <tr>
                    <td>
                      <p style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:16px ; font-weight:bold;margin:10px 20px">Thông tin khách hàng</p>
                    </td>
                  </tr>  
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#e6e6e6">
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px">
                <tbody>
                  <tr>
                    <td valign="top"><img alt="arrow down" src="http://webdoctor.vn/images/arrowdown-1.png"></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                <tbody>
                  <tr>
                    <td bgcolor="#ffffff" style="font-size: 13px;color: #363636;font-family: Arial, Helvetica, sans-serif;" >
                      <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                        <tbody>
                        <?php foreach ($customer_meta as $label => $text): ?>
                          <tr bgcolor="#f4f8fb">
                            <td height="30px" width="25%"><?php echo $label; ?> :</td>
                            <td><?php echo $text; ?></td>
                          </tr>
                          <?php endforeach ?>
                        </tbody>
                      </table>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
  <?php endif ?>

  <?php if ($message): ?>    
  <tr>
    <td bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6;border-right: 20px solid #e6e6e6;  ">
      <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
          <tr>
            <td bgcolor="#f58220" >
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px">
                <tbody>
                  <tr>
                    <td>
                      <p style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:16px ; font-weight:bold;margin:10px 20px">Nội dung</p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#e6e6e6">
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px">
                <tbody>
                  <tr>
                    <td valign="top"><img alt="arrow down" src="http://webdoctor.vn/images/arrowdown-1.png"></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                <tbody>
                  <tr>
                    <td bgcolor="#ffffff" style="font-size: 13px;color: #363636;font-family: Arial, Helvetica, sans-serif;" >
                      <p><b>Nội dung :</b></p>
                      <p><?php echo $message; ?></p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
  <?php endif ?>

  <?php
  if(!empty($kpis))
  {            
    $template = array(
      'table_open' => '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">',
      'row_start'   => '<tr bgcolor="#f4f8fb">');

    $this->table->set_template($template);

    $names = $emails = $phones = array();
    foreach ($kpis as $kpi) 
    {
      $display_name = trim($this->admin_m->get_field_by_id($kpi['user_id'],'display_name'));
      if($display_name) $names[] = $display_name;

      $user_email = trim($this->admin_m->get_field_by_id($kpi['user_id'],'user_email'));
      if($user_email) $emails[] = mailto($user_email);

      $user_phone = $this->usermeta_m->get_meta_value( $kpi['user_id'],'user_phone');
      $user_phone = preg_replace('/\s+/', '', $user_phone);
      if($user_phone) $phones[] = $user_phone;
    }

    if(!empty($names))
      $this->table->add_row(array('data'=>'Phụ trách','width'=>'25%','height'=>'30px'),implode(',', $names));

    if(!empty($phones))
      $this->table->add_row(array('data'=>'Số điện thoại','width'=>'25%','height'=>'30px'),implode(',', $phones));

    if(!empty($emails))
      $this->table->add_row(array('data'=>'Email','width'=>'25%','height'=>'30px'),implode(',', $emails));

    $technical_contact_table = $this->table->generate();
    ?>
    <tr>
      <td align="center" style="border:20px solid #e6e6e6;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
          <tbody>
            <tr>
              <td bgcolor="#fff">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                  <tbody>
                    <tr>
                      <td>
                        <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Phụ trách kỹ thuật</p>
                        <?php echo $technical_contact_table;?>
                      </td>
                    </tr>
                  </tbody>
                </table>        
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <?php
  }
  ?>

  <tr>
    <td align=bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6; border-right: 20px solid #e6e6e6; ">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" >
        <tbody>
          <tr>
            <td bgcolor="#ffffff">
              <table cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:#ffffff 20px solid; border-top:3px solid #d4670a; width:100% ">
                <tbody>
                  <tr>
                    <td>
                      <table width="100%" cellspacing="10" cellpadding="0" border="0">
                        <tbody>
                          <tr>
                            <td width="20%" >
                              <p style=" float:right;font-family:Roboto ;color:#b8b8b8;line-height:18px"><img alt="#" src="http://webdoctor.vn/images/1461914788_location.png"  />
                              </td>
                              <td>
                                <p style=" font-family:Roboto;font-size:13px;color:#333;line-height:18px; margin-left:20px; ">Tầng 8, 402 Nguyễn Thị Minh Khai , Phường 5, Quận 3, TP.HCM, Việt Nam</p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td width="50%" align="right">
                                  <a href="http://webdoctor.vn"><img alt="#" src="http://webdoctor.vn/images/wedoctor-logo.png" style=" margin-right:20px; max-width:100%"  /></a>
                                </td>
                                <td width="50%">
                                  <a href="http://adsplus.vn"><img alt="#" src="http://webdoctor.vn/images/adsplus-logo.png"  style="max-width:100%" /></a>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>

    </table>
