<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:15px solid #fff; margin: auto; max-width:640px">
  <?php
  if(!empty($direct_downlink))
  {
    echo '<tr><td>';
    echo '* Link tải báo cáo dự phòng :';
    echo implode(', ', $direct_downlink);
    echo '</td></td>';
  }
  ?>
  <tr>
    <td>
      <table width="100%" height="50" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="4%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td bgcolor="#f58220" style="color:#f58220;" ><br/>
                  <br/>
                </td>
              </tr>
            </table>
          </td>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Roboto;font-size:0.8em; font-weight:500; border-bottom:#d4670a 3px solid; padding:0 20px;  ">
              <tr>
                <td width="40%" ><a href="#"><img alt="logo Webdoctor" src="http://webdoctor.vn/images/adsplus-logo.png" style="max-width:100%; padding-bottom:6px"></a>
                </td>
                <td style="text-align:right" ><a target="_blank" href="mailto:sales@adsplus.vn" style=" text-decoration:none; color:#333;"><img alt="icon-mail" src="http://webdoctor.vn/images/icon-mail2.png" style="float:right; padding-right:6px; padding-left:5px;" />sales@adsplus.vn </a><br />
                  <a target="_blank" rel="nofollow" href="tel:0873004488" style=" text-decoration:none; color:#333;"><img alt="icon-phone" src="http://webdoctor.vn/images/icon-phone2.png" style="float:right; padding-right:6px; " /> 08.7300 4488 </a>&nbsp;
                </td>
              </tr>
            </table>
          </td>
          <td width="4%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td >&nbsp;</td>
              </tr>
              <tr>
                <td bgcolor="#f58220" style="color:#f58220;" ><br/>
                  <br/>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td bgcolor="#f58220"><?php echo $content;?></td>
  </tr>

  <?php
  if(!empty($kpis))
  {            
    $template = array(
      'table_open' => '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">',
      'row_start'   => '<tr bgcolor="#f4f8fb">');

    $this->table->set_template($template);

    $names = $emails = $phones = array();
    foreach ($kpis as $kpi) 
    {
      $display_name = trim($this->admin_m->get_field_by_id($kpi['user_id'],'display_name'));
      if($display_name) $names[] = $display_name;

      $user_email = trim($this->admin_m->get_field_by_id($kpi['user_id'],'user_email'));
      if($user_email) $emails[] = mailto($user_email);

      $user_phone = $this->usermeta_m->get_meta_value( $kpi['user_id'],'user_phone');
      $user_phone = preg_replace('/\s+/', '', $user_phone);
      if($user_phone) $phones[] = $user_phone;
    }

    if(!empty($names))
      $this->table->add_row(array('data'=>'Phụ trách','width'=>'25%','height'=>'30px'),implode(',', $names));

    if(!empty($phones))
      $this->table->add_row(array('data'=>'Số điện thoại','width'=>'25%','height'=>'30px'),implode(',', $phones));

    if(!empty($emails))
      $this->table->add_row(array('data'=>'Email','width'=>'25%','height'=>'30px'),implode(',', $emails));

    $technical_contact_table = $this->table->generate();
    ?>
    <tr>
      <td align="center" style="border:20px solid #e6e6e6;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
          <tbody>
            <tr>
              <td bgcolor="#fff">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                  <tbody>
                    <tr>
                      <td>
                        <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Phụ trách kỹ thuật</p>
                        <?php echo $technical_contact_table;?>
                      </td>
                    </tr>
                  </tbody>
                </table>        
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <?php
  }
  ?>

  <tr>
    <td align="center" style="border:20px solid #e6e6e6;">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
        <tbody>
          <tr>
            <td bgcolor="#fff">
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                <tbody>
                  <tr>
                    <td bgcolor="#ffffff" style="font-size: 13px;  font-family: Arial, Helvetica, sans-serif;">
                      <p><strong>Thời gian làm việc : </strong><em >Thứ 2 đến thứ 6 : 8:00 - 17:00 ,Thứ 7 : 8:00 - 12:00</p>
                    </td>
                  </tr>
                  <tr>
                    <td bgcolor="#ffffff" style="font-size: 13px;  font-family: Arial, Helvetica, sans-serif;">
                      <p>Cảm ơn quý khách đã sử dụng dịch vụ của <strong><a href="//www.adsplus.vn" target="_blank">WWW.ADSPLUS.VN</a></strong>. 
                        <br>
                        <em style="color:#999">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không  phải reply lại email này.</em> <br>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>        
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
</table>

<!--  EMAIL START -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td height="15"></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#ffffff" style="border-top:3px solid #f58220;border-bottom:2px solid #f58220">
                <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="m_5318762315415474924table-inner">
                    <tbody>


                        <tr>
                            <td height="25"></td>
                        </tr>



                        <tr>
                            <td align="center" style="font-family:open sans,arial,sans-serif;color:#444444;font-size:14px;line-height:28px;text-align:justify">
                                <table class="m_5318762315415474924table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="center" style="line-height:0px">
                                                <a href="http://adsplus.vn/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://adsplus.vn/&amp;source=gmail&amp;ust=1529638969451000&amp;usg=AFQjCNGkKBTyjC4psggYEL2w3UZkErOH7g"><img src="https://ci6.googleusercontent.com/proxy/___iNI7J0Tyz5jQpIDXDkBlCigUzS8FWfXidVRl2_-bU1n1yVfQGiPhrxrVX7uP9J3TG2WmIp-DQua8U4eU6K8Xw=s0-d-e1-ft#https://webdoctor.vn/images/adsplus-logo.png" class="CToWUd"></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="m_5318762315415474924table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="center" style="line-height:0px">
                                                <a href="http://webdoctor.vn/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://webdoctor.vn/&amp;source=gmail&amp;ust=1529638969451000&amp;usg=AFQjCNHcueJbnnToZbfTMaYaV-0uB4pG1g"><img src="https://ci6.googleusercontent.com/proxy/9d3sqQKvKBiC5-zvoyorbipqkO72B-CzY7Y5aX1Ot-NGRSmLX7KCT80nXbsO9ewxe52uixPd4vR0SBO4V_zAxO8SjGY=s0-d-e1-ft#https://webdoctor.vn/images/webdoctor-logo.png" class="CToWUd"></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="font-family:open sans,arial,sans-serif;color:#444444;font-size:14px;line-height:28px;text-align:justify">

                                <p>
                                    <b>Sứ mệnh của Adsplus.vn</b>
                                    <br>Google và Facebook lần lượt là hai kênh quảng cáo trực tuyến phổ biến nhất Việt Nam về tính hiệu quả và tỷ lệ quy đổi ra đơn hàng. Adsplus.vn nỗ lực giúp Khách hàng sử dụng hai kênh quảng cáo này tốt nhất, đơn giản nhất, minh bạch
                                    nhất và qua đó giúp Khách hàng nâng cao doanh số cũng như phát triển thương hiệu.
                                </p>

                                <p>
                                    <b>Sứ mệnh của Webdoctor.vn</b>
                                    <br>Là sát cánh với Khách hàng, giúp Khách hàng có một website tốt với chi phí vận hành tiết kiệm và qua đó giúp Khách hàng phát huy được tối đa hiệu quả kinh doanh
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td height="25"></td>
                        </tr>

                        <tr>
                            <td align="center">
                                <table border="0" cellspacing="0" cellpadding="0" style="/* border:2px solid #f58220 */">
                                    <tbody>
                                        <tr>
                                            <td align="center" height="40" style="font-family:open sans,arial,sans-serif;color:#444444;font-size:14px;padding-left:25px;padding-right:25px;border: 2px solid #f58220;">
                                                <a href="http://adsplus.vn/" style="color:#f58220" target="_blank">Xem chi tiết</a>
                                            </td>
                                            <td align="center" height="40" style="font-family:open sans,arial,sans-serif;color:#444444;font-size:14px;padding-left: 5px;padding-right: 5px;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td height="35"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="15"></td>
        </tr>
    </tbody>
</table>
<!--  FOOTER END  -->