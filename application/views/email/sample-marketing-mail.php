<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:15px solid #fff; margin: auto; max-width:640px">

<tr>
  <td>
    <table width="100%" height="50" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4%">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td bgcolor="#f58220" style="color:#f58220;" ><br/>
                <br/>
              </td>
            </tr>
          </table>
        </td>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Roboto;font-size:0.8em; font-weight:500; border-bottom:#d4670a 3px solid; padding:0 20px;  ">
            <tr>
              <td width="40%" ><a href="#"><img alt="logo Webdoctor" src="http://webdoctor.vn/images/adsplus-logo.png" style="max-width:100%; padding-bottom:6px"></a>
              </td>
              <td style="text-align:right" ><a target="_blank" href="mailto:sales@adsplus.vn" style=" text-decoration:none; color:#333;"><img alt="icon-mail" src="http://webdoctor.vn/images/icon-mail2.png" style="float:right; padding-right:6px; padding-left:5px;" />sales@adsplus.vn </a><br />
                <a target="_blank" rel="nofollow" href="tel:0873004488" style=" text-decoration:none; color:#333;"><img alt="icon-phone" src="http://webdoctor.vn/images/icon-phone2.png" style="float:right; padding-right:6px; " /> 08.7300 4488 </a>&nbsp;
              </td>
            </tr>
          </table>
        </td>
        <td width="4%">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td >&nbsp;</td>
            </tr>
            <tr>
              <td bgcolor="#f58220" style="color:#f58220;" ><br/>
                <br/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
</tr>
<tr>
  <td bgcolor="#f58220">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
          <p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">
            Xin chào quý khách ,
          </p>
          <p>Rất nhiều trường hợp Quý khách bỏ sót mail báo cáo từ Google. Vậy làm sao để theo sát chiến dịch mà không lệ thuộc vào email báo cáo?</p>
          <p>Thấu hiểu những hạn chế đó của email, phòng Kỹ thuật Adsplus.vn đã cải tiến và nâng cấp thành công phương pháp tối ưu quản lý tài khoản cho Quý khách trên <strong><a href="http://guru.adsplus.vn">http://guru.adsplus.vn</a></strong> </p>
        </td>
      </tr>
      <tr>
        <td bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6;border-right: 20px solid #e6e6e6;  ">
          <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
              <tr>
                <td bgcolor="#f58220" >
                  <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px">
                    <tbody>
                      <tr>
                        <td>
                          <p style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:16px ; font-weight:bold;margin:10px 20px">
                            Hướng dẫn sử dụng </p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td bgcolor="#e6e6e6">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px">
                      <tbody>
                        <tr>
                          <td valign="top"><img alt="arrow down" src="http://webdoctor.vn/images/arrowdown-1.png"></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                      <tbody>
                        <tr>
                          <td bgcolor="#ffffff" style="font-size: 13px;color: #363636;font-family: Arial, Helvetica, sans-serif;" >
                            <p ><strong>1. Quý khách truy cập link : </strong> <a href="http://guru.adsplus.vn">http://guru.adsplus.vn</a></p>
                              
                            <?php 
                            if(!empty($phone))

                            ?>
                          <?php if (!empty($phone)): ?>
                            <p ><strong>2. Tài khoản truy cập : </strong> <?php echo implode(' <b>hoặc</b> ', $phone); ?> (Số điện thoại Quý khách đăng ký dịch vụ)</p>
                          <?php endif ?>
                            <p ><strong>3. Mật khẩu : </strong> ABC123@123 (Mật khẩu mặc đinh - bao gồm chữ in hoa và viết liền)</p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>

        <tr>
          <td bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6;border-right: 20px solid #e6e6e6;  ">
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
              <tbody>
                <tr>
                  <td bgcolor="#f58220" >
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px">
                      <tbody>
                        <tr>
                          <td>
                            <p style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:16px ; font-weight:bold;margin:10px 20px">Hỗ trợ kỹ thuật</p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td bgcolor="#e6e6e6">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px">
                      <tbody>
                        <tr>
                          <td valign="top"><img alt="arrow down" src="http://webdoctor.vn/images/arrowdown-1.png"></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                      <tbody>
                        <tr>
                          <td bgcolor="#ffffff" style="font-size: 13px;color: #363636;font-family: Arial, Helvetica, sans-serif;" >

                            <p ><strong>Mọi trường hợp Quý khách gặp khó khăn hoặc thắc mắc cần hỗ trợ. Quý khách vui lòng liên hệ :</p>

                          <?php if ($staffs): ?>
                            <?php foreach ($staffs as $staff):?>  
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size: 13px;color: #363636;font-family: Arial, Helvetica, sans-serif;">
                              <tbody>
                                <?php if (!empty($staff['display_name'])): ?>  
                                <tr bgcolor="#f4f8fb">
                                  <td height="30px" width="25%">Nhân viên phụ trách :</td>
                                  <td><?php echo $staff['display_name']; ?></td>
                                </tr>
                                <?php endif ?>

                                <?php if (!empty($staff['phone'])): ?>
                                <tr>
                                  <td height="30px" width="25%">SĐT :</td>
                                  <td><?php echo $staff['phone']; ?></td>
                                </tr>  
                                <?php endif ?>

                                <?php if (!empty($staff['email'])): ?>  
                                <tr bgcolor="#f4f8fb">
                                  <td height="30px" width="25%">Email</td>
                                  <td><a href="<?php echo $staff['email']; ?>"><?php echo $staff['email']; ?></a></td>
                                </tr>
                                <?php endif ?>
                              </tbody>
                            </table>
                            <br/>
                            <?php endforeach ?>
                          <?php endif ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>

      </table>
    </td>
  </tr>

  <tr>
    <td align="center" style="border:20px solid #e6e6e6;">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
        <tbody>
          <tr>
            <td bgcolor="#fff">
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                <tbody>
                  <tr>
                    <td><h3>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</h3></td>
                  </tr>
                  <tr>
                    <td bgcolor="#ffffff" style="font-size: 13px;  font-family: Arial, Helvetica, sans-serif;">
                      <p><strong>Trụ sở : </strong>Tầng 8, 402 Nguyễn Thị Minh Khai , Phường 5, Quận 3, TP.HCM, Việt Nam</p>
                      <p><strong>Chi nhánh : </strong>Tầng 5, Tòa nhà 79 Trương Định, P. Bến Nghé, Q.1, TP. HCM</p>
                      <p><strong>Hotline : </strong><a href="tel:0873004488">(08) 7300 4488</a></p>
                      <br/>
                    </td>
                  </tr>
                  <tr>
                    <td bgcolor="#ffffff" style="font-size: 13px;  font-family: Arial, Helvetica, sans-serif;">
                      <p>Cảm ơn quý khách đã sử dụng dịch vụ của <strong><a href="//www.adsplus.vn" target="_blank">WWW.ADSPLUS.VN</a></strong>. 
                        <br>
                        <em style="color:#999">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không  phải reply lại email này.</em> 
                        <br>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>        
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>

  <tr>
    <td align=bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6; border-right: 20px solid #e6e6e6; ">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" >
        <tbody>
          <tr>
            <td bgcolor="#ffffff">
              <table cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:#ffffff 20px solid; border-top:3px solid #d4670a; width:100% ">
                <tbody>
                  <tr>
                    <td>
                      <table width="100%" cellspacing="10" cellpadding="0" border="0">
                        <tbody>
                          <tr>
                            <td width="20%" >
                              <p style=" float:right;font-family:Roboto ;color:#b8b8b8;line-height:18px"><img alt="#" src="http://webdoctor.vn/images/1461914788_location.png"  />
                              </td>
                              <td>
                                <p style="font-size: 13px;color: #363636;font-family: Arial, Helvetica, sans-serif;">Tầng 8, 402 Nguyễn Thị Minh Khai , Phường 5, Quận 3, TP.HCM, Việt Nam </p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td width="50%" align="right">
                                  <a href="http://webdoctor.vn"><img alt="#" src="http://webdoctor.vn/images/wedoctor-logo.png" style=" margin-right:20px; max-width:100%"  /></a>
                                </td>
                                <td width="50%">
                                  <a href="http://adsplus.vn"><img alt="#" src="http://webdoctor.vn/images/adsplus-logo.png"  style="max-width:100%" /></a>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>

    </table>
    <hr />
</table>