<div style="background:#f2f2ff">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin: auto; max-width:640px">
  <tr>
    <td>
      <table width="100%" height="50" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="4%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td bgcolor="#0074bd" style="color:#0074bd;" ><br/><br/>
                  </td>
                </tr>
              </table>
            </td>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Roboto;font-size:0.8em; font-weight:500; border-bottom:#07649e 3px solid; padding:0 20px;  ">
                <tr>
                  <td width="40%" >
                  <?php
                  $img = img(array('alt'=>'logo Webdoctor','src'=>'http://webdoctor.vn/images/webdoctor-logo.png','style'=>'style="max-width:100%; padding-bottom:6px"'));
                  $anchor_text = anchor('http://webdoctor.vn',$img,'target="_blank"'); 
                  echo $anchor_text;
                  ?>
                  </td>
                  <td style="text-align:right" >
                  <?php
                  $img = img(array('alt'=>'icon-mail','src'=>'http://webdoctor.vn/images/icon-mail.png','style="float:right; padding-right:6px; padding-left:5px;"'));
                  $anchor_text = mailto('sales@webdoctor.vn','sales@webdoctor.vn'.$img,'style=" text-decoration:none; color:#333;"');
                  echo $anchor_text.br();

                  $img = img(array('alt'=>'icon-phone','src'=>'http://webdoctor.vn/images/icon-phone.png','style="float:right; padding-right:6px; padding-left:5px;"'));
                  $anchor_text = mailto('tel:0873004488','08.7300 4488 '.$img,'style="text-decoration:none;color:#333;" rel="nofollow"');
                  echo $anchor_text.br();
                  ?>
                  </td>
                </tr>
              </table>
            </td>
              <td width="4%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td >&nbsp;</td>
                </tr>
                <tr>
                  <td bgcolor="#0074bd" style="color:#0074bd;" ><br/>
                    <br/>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td bgcolor="#0074bd"><table width="100%" border="0" cellspacing="0" cellpadding="0" >
          <?php echo @$content;?>

          <tr>
    <td align="center" bgcolor="#ffffff" style="border-top:3px solid #3498db;border-bottom:2px solid #e0e0e0;">
      <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="table-inner">
        <tr>
          <td align="center">
            <!-- title -->
            <table class="table-full" align="center" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><img src="http://webdoctor.vn/images/quote-left.png" width="25" height="37" alt="img" /></td>
                <td align="center" bgcolor="#3498db" style="font-family: open sans, arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 25px;padding-right: 25px;"><a style="color: #ffffff;text-decoration: none;" href="#">WEBDOCTOR.VN</a></td>
                <td><img src="http://webdoctor.vn/images/quote-right.png" width="25" height="37" alt="img" /></td>
              </tr>
            </table>
            <!-- end title -->
          </td>
        </tr>
        <tr>
          <td height="25"></td>
        </tr>
        <!-- headline -->
        <tr>
          <td align="center" valign="top" style="font-family: open sans, arial, sans-serif; font-size:30px; color:#4a4a4a; font-weight:bold;"><a style="color: #4a4a4a;text-decoration: none;" href="#">GIẢI PHÁP WEBDOCTOR.VN</a></td>
        </tr>
        <!-- End headline -->
        <tr>
          <td height="15"></td>
        </tr>
        <!-- Content -->
        <tr>
          <td align="center" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px; line-height:28px; text-align: justify;">
            <table class="table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
              <tbody><tr>
                <td align="center" style="line-height: 0px;"><a href="http://webdoctor.vn/" target="_blank"><img src="https://webdoctor.vn/images/webdoctor-logo.png"></a></td>
              </tr>
            </tbody></table>

            <table class="table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
              <tbody><tr> <td align="center" style="line-height: 0px;">
                <a href="http://adsplus.vn/" target="_blank"><img src="https://webdoctor.vn/images/adsplus-logo.png"></a></td>
              </tr>
            </tbody></table>
          </td>

        </tr>

        <tr>
          <td align="center" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px; line-height:28px; text-align: justify;">
            <p><b>Sứ mệnh của Webdoctor.vn</b><br>
              Là sát cánh với Khách hàng; giúp Khách hàng có một website tốt với chi phí vận hành tiết kiệm; và qua đó giúp Khách hàng phát huy được tối đa hiệu quả kinh doanh.
            </p>
            <p><b>Sứ mệnh của Adsplus.vn</b> <br>
             Google và Facebook lần lượt là hai kênh quảng cáo trực tuyến phổ biến nhất Việt Nam về tính hiệu quả và tỷ lệ quy đổi ra đơn hàng. Adsplus.vn nỗ lực giúp Khách hàng sử dụng hai kênh quảng cáo này tốt nhất; đơn giản nhất; minh bạch nhất và qua đó giúp Khách hàng nâng cao doanh số cũng như phát triển thương hiệu.
           </p>
         </td>
       </tr>
       <!-- End Content -->
       <tr>
        <td height="25"></td>
      </tr>
      <!-- Button -->
      <tr>
        <td align="center">
          <table border="0" cellspacing="0" cellpadding="0" style="">
            <tr>
              <td align="center" height="40" style="font-family: open sans, arial, sans-serif; color:#999999;border:2px solid #3498db; font-size:14px;padding-left: 25px;padding-right: 25px;"><a href="http://webdoctor.vn/" target="_blank">Xem chi tiết</a></td>

                <?php
                              $_args = array(
                                 'event' => 'july-june-event',
                                 'name' => get_term_meta_value($term->term_id, 'representative_name'),
                                 'email' => get_term_meta_value($term->term_id, 'representative_email'),
                                 'phone' => get_term_meta_value($term->term_id, 'representative_phone'),
                                 'source' => $email_source ?? ''
                             );
                            $guru_emmbed_link = base_url('wservice/v1/guru/subscribe?'.http_build_query($_args));
                            ?>
                            <td align="center" height="40" style="font-family:open sans,arial,sans-serif;color:#444444;font-size:14px;border: 2px solid #f58220;background-color:  #f58220;">
                                <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTcuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDM0NS44MzQgMzQ1LjgzNCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzQ1LjgzNCAzNDUuODM0OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjY0cHgiIGhlaWdodD0iNjRweCI+CjxnPgoJPHBhdGggZD0iTTMzOS43OTgsMjYwLjQyOWMwLjEzLTAuMDI2LDAuMjU3LTAuMDYxLDAuMzg1LTAuMDk0YzAuMTA5LTAuMDI4LDAuMjE5LTAuMDUxLDAuMzI2LTAuMDg0ICAgYzAuMTI1LTAuMDM4LDAuMjQ3LTAuMDg1LDAuMzY5LTAuMTI5YzAuMTA4LTAuMDM5LDAuMjE3LTAuMDc0LDAuMzI0LTAuMTE5YzAuMTE1LTAuMDQ4LDAuMjI2LTAuMTA0LDAuMzM4LTAuMTU3ICAgYzAuMTA5LTAuMDUyLDAuMjItMC4xLDAuMzI3LTAuMTU4YzAuMTA3LTAuMDU3LDAuMjA4LTAuMTIyLDAuMzEyLTAuMTg0YzAuMTA3LTAuMDY0LDAuMjE1LTAuMTI0LDAuMzE5LTAuMTk0ICAgYzAuMTExLTAuMDc0LDAuMjE0LTAuMTU2LDAuMzIxLTAuMjM2YzAuMDktMC4wNjcsMC4xODItMC4xMywwLjI3LTAuMjAyYzAuMTYyLTAuMTMzLDAuMzE2LTAuMjc1LDAuNDY2LTAuNDIxICAgYzAuMDI3LTAuMDI2LDAuMDU2LTAuMDQ4LDAuMDgzLTAuMDc1YzAuMDI4LTAuMDI4LDAuMDUyLTAuMDU5LDAuMDc5LTAuMDg4YzAuMTQ0LTAuMTQ4LDAuMjg0LTAuMywwLjQxNi0wLjQ2ICAgYzAuMDc3LTAuMDk0LDAuMTQ0LTAuMTkyLDAuMjE2LTAuMjg5YzAuMDc0LTAuMSwwLjE1Mi0wLjE5NywwLjIyMS0wLjMwMWMwLjA3NC0wLjExMSwwLjEzOS0wLjIyNiwwLjIwNy0wLjM0ICAgYzAuMDU3LTAuMDk2LDAuMTE4LTAuMTksMC4xNzEtMC4yODljMC4wNjItMC4xMTUsMC4xMTQtMC4yMzQsMC4xNjktMC4zNTFjMC4wNDktMC4xMDQsMC4xMDEtMC4yMDcsMC4xNDYtMC4zMTQgICBjMC4wNDgtMC4xMTUsMC4wODYtMC4yMzIsMC4xMjgtMC4zNDljMC4wNDEtMC4xMTQsMC4wODUtMC4yMjcsMC4xMi0wLjM0M2MwLjAzNi0wLjExOCwwLjA2Mi0wLjIzOCwwLjA5Mi0wLjM1OCAgIGMwLjAyOS0wLjExOCwwLjA2My0wLjIzNCwwLjA4Ni0wLjM1M2MwLjAyOC0wLjE0MSwwLjA0NS0wLjI4MywwLjA2NS0wLjQyNWMwLjAxNC0wLjEsMC4wMzMtMC4xOTksMC4wNDMtMC4zICAgYzAuMDI1LTAuMjQ5LDAuMDM4LTAuNDk4LDAuMDM4LTAuNzQ4VjkyLjc2YzAtNC4xNDMtMy4zNTctNy41LTcuNS03LjVoLTIzNi4yNWMtMC4wNjYsMC0wLjEzLDAuMDA4LTAuMTk2LDAuMDEgICBjLTAuMTQzLDAuMDA0LTAuMjg1LDAuMDEtMC40MjcsMC4wMjJjLTAuMTEzLDAuMDA5LTAuMjI1LDAuMDIyLTAuMzM3LDAuMDM3Yy0wLjEyOCwwLjAxNi0wLjI1NSwwLjAzNS0wLjM4MiwwLjA1OCAgIGMtMC4xMTksMC4wMjEtMC4yMzcsMC4wNDYtMC4zNTQsMC4wNzNjLTAuMTE5LDAuMDI4LTAuMjM4LDAuMDU4LTAuMzU2LDAuMDkyYy0wLjExNywwLjAzMy0wLjIzMiwwLjA2OS0wLjM0NiwwLjEwNyAgIGMtMC4xMTcsMC4wNC0wLjIzNCwwLjA4Mi0wLjM0OSwwLjEyOGMtMC4xMDksMC4wNDMtMC4yMTYsMC4wODctMC4zMjIsMC4xMzVjLTAuMTE4LDAuMDUzLTAuMjM1LDAuMTEtMC4zNTEsMC4xNjkgICBjLTAuMDk5LDAuMDUxLTAuMTk2LDAuMTAzLTAuMjkyLDAuMTU4Yy0wLjExNiwwLjA2Ni0wLjIzLDAuMTM2LTAuMzQzLDAuMjA4Yy0wLjA5MywwLjA2LTAuMTg0LDAuMTIyLTAuMjc0LDAuMTg1ICAgYy0wLjEwNiwwLjA3NS0wLjIxMSwwLjE1My0wLjMxNCwwLjIzNWMtMC4wOTQsMC4wNzUtMC4xODYsMC4xNTItMC4yNzcsMC4yMzFjLTAuMDksMC4wNzktMC4xNzksMC4xNTgtMC4yNjYsMC4yNDIgICBjLTAuMDk5LDAuMDk1LTAuMTk0LDAuMTk0LTAuMjg4LDAuMjk0Yy0wLjA0NywwLjA1LTAuMDk3LDAuMDk0LTAuMTQyLDAuMTQ1Yy0wLjAyNywwLjAzLTAuMDQ4LDAuMDYzLTAuMDc0LDAuMDkzICAgYy0wLjA5NCwwLjEwOS0wLjE4MiwwLjIyMy0wLjI3LDAuMzM4Yy0wLjA2NCwwLjA4NC0wLjEzLDAuMTY4LTAuMTksMC4yNTRjLTAuMDc4LDAuMTEyLTAuMTUsMC4yMjctMC4yMjIsMC4zNDMgICBjLTAuMDU5LDAuMDk1LTAuMTIsMC4xODktMC4xNzQsMC4yODZjLTAuMDYzLDAuMTEyLTAuMTE4LDAuMjI3LTAuMTc1LDAuMzQyYy0wLjA1MiwwLjEwNS0wLjEwNiwwLjIxLTAuMTUzLDAuMzE3ICAgYy0wLjA0OSwwLjExMy0wLjA5MiwwLjIzLTAuMTM1LDAuMzQ1Yy0wLjA0MywwLjExMy0wLjA4NywwLjIyNS0wLjEyNCwwLjMzOWMtMC4wMzcsMC4xMTUtMC4wNjcsMC4yMzItMC4wOTksMC4zNDkgICBjLTAuMDMyLDAuMTItMC4wNjYsMC4yMzktMC4wOTMsMC4zNmMtMC4wMjUsMC4xMTMtMC4wNDIsMC4yMjgtMC4wNjIsMC4zNDJjLTAuMDIyLDAuMTMtMC4wNDQsMC4yNi0wLjA2LDAuMzkgICBjLTAuMDEzLDAuMTA4LTAuMDE5LDAuMjE4LTAuMDI3LDAuMzI4Yy0wLjAxLDAuMTQtMC4wMTksMC4yOC0wLjAyMSwwLjQyMWMtMC4wMDEsMC4wNDEtMC4wMDYsMC4wODEtMC4wMDYsMC4xMjJ2NDYuMjUyICAgYzAsNC4xNDMsMy4zNTcsNy41LDcuNSw3LjVzNy41LTMuMzU3LDcuNS03LjV2LTI5LjU5NWw2Ni42ODEsNTkuMDM3Yy0wLjM0OCwwLjI0NS0wLjY4MywwLjUxNi0wLjk5NSwwLjgyN2wtNjUuNjg3LDY1LjY4N3YtNDkuMjg4ICAgYzAtNC4xNDMtMy4zNTctNy41LTcuNS03LjVzLTcuNSwzLjM1Ny03LjUsNy41djkuMTY0aC0zOC43NWMtNC4xNDMsMC03LjUsMy4zNTctNy41LDcuNXMzLjM1Nyw3LjUsNy41LDcuNWgzOC43NXY0My4yMzEgICBjMCw0LjE0MywzLjM1Nyw3LjUsNy41LDcuNWgyMzYuMjVjMC4yNDcsMCwwLjQ5NC0wLjAxMywwLjc0LTAuMDM3YzAuMTE1LTAuMDExLDAuMjI2LTAuMDMzLDAuMzM5LTAuMDQ5ICAgQzMzOS41NDIsMjYwLjQ2OSwzMzkuNjcsMjYwLjQ1NCwzMzkuNzk4LDI2MC40Mjl6IE0zMzAuODM0LDIzNC45NjdsLTY1LjY4OC02NS42ODdjLTAuMDQyLTAuMDQyLTAuMDg3LTAuMDc3LTAuMTMtMC4xMTcgICBsNDkuMzgzLTQxLjg5N2MzLjE1OC0yLjY4LDMuNTQ2LTcuNDEyLDAuODY2LTEwLjU3MWMtMi42NzgtMy4xNTctNy40MS0zLjU0Ny0xMC41NzEtMC44NjZsLTg0LjM4MSw3MS41OWwtOTguNDQ0LTg3LjE1OGgyMDguOTY1ICAgVjIzNC45Njd6IE0xODUuODc4LDE3OS44ODhjMC41MzUtMC41MzUsMC45NjktMS4xMzEsMS4zMDgtMS43NjVsMjguMDUxLDI0LjgzNWMxLjQxOCwxLjI1NSwzLjE5NCwxLjg4NSw0Ljk3MiwxLjg4NSAgIGMxLjcyNiwwLDMuNDUxLTAuNTkzLDQuODUzLTEuNzgxbDI4LjU4Ny0yNC4yNTRjMC4yNiwwLjM4LDAuNTUzLDAuNzQzLDAuODksMS4wOGw2NS42ODcsNjUuNjg3SDEyMC4xOTFMMTg1Ljg3OCwxNzkuODg4eiIgZmlsbD0iI0ZGRkZGRiIvPgoJPHBhdGggZD0iTTcuNSwxNzAuNjc2aDEyNi42NjdjNC4xNDMsMCw3LjUtMy4zNTcsNy41LTcuNXMtMy4zNTctNy41LTcuNS03LjVINy41Yy00LjE0MywwLTcuNSwzLjM1Ny03LjUsNy41ICAgUzMuMzU3LDE3MC42NzYsNy41LDE3MC42NzZ6IiBmaWxsPSIjRkZGRkZGIi8+Cgk8cGF0aCBkPSJNMjAuNjI1LDEyOS4zNDVINzcuNWM0LjE0MywwLDcuNS0zLjM1Nyw3LjUtNy41cy0zLjM1Ny03LjUtNy41LTcuNUgyMC42MjVjLTQuMTQzLDAtNy41LDMuMzU3LTcuNSw3LjUgICBTMTYuNDgyLDEyOS4zNDUsMjAuNjI1LDEyOS4zNDV6IiBmaWxsPSIjRkZGRkZGIi8+Cgk8cGF0aCBkPSJNNjIuNSwyMjYuNTFoLTU1Yy00LjE0MywwLTcuNSwzLjM1Ny03LjUsNy41czMuMzU3LDcuNSw3LjUsNy41aDU1YzQuMTQzLDAsNy41LTMuMzU3LDcuNS03LjVTNjYuNjQzLDIyNi41MSw2Mi41LDIyNi41MXoiIGZpbGw9IiNGRkZGRkYiLz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" style="display: inline-block !important;width:  24px;height:  24px;position: relative;top: 7px;left: 18px;">
                                <a href="<?php echo $guru_emmbed_link ;?>" style="color: #fff;padding-left: 25px;padding-right: 25px;padding-top:  10px;padding-bottom:  10px;display:  inline-block;" target="_blank">
                                    Đăng ký Event Adwords miễn phí
                                </a>
                            </td>

            </tr>
          </table>
        </td>
      </tr>
      <!--End Button-->
      <tr>
        <td height="35"></td>
      </tr>
    </table>
  </td>
</tr>
      </table>
    </td>
  </tr>

</table>
</div>