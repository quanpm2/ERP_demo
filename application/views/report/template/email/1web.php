<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$content_style = 'font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;';
?>

<table width="100%" bgcolor="#ecf0f1" align="center" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td bgcolor="#3498db" style="border-top:5px solid #43a8eb;">
            <table align="center" class="table-inner" width="600" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="right">
                        <table class="table-full" align="right" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="35" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:12px; font-style:italic; color:#ffffff;"><?php echo date('d/m/Y');?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF">
                        <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#3498db" class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr>
                                <td height="55" align="center" bgcolor="#3498db" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;padding-left: 15px;padding-right: 15px;" src="https://1web.vn/wp-content/themes/Webdoctor/images/logo-white.png"  height="50" alt="Logo" /></td>
                                <td align="left" class="hide" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/header_shape.png" width="25" alt="img" /></td>
                            </tr>
                        </table>
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                            <tr>
                                <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                </td>
                            </tr>
                        </table>
                        <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="table-full">
                            <tr>
                                <td height="55" style="font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#7f8c8d;padding-left: 25px;padding-right: 25px;"> KHỞI TẠO WEBSITE MIỄN PHÍ</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="15"></td>
    </tr>
</table>

<?php echo $content;?>

<?php if( ! empty($workers)) : ?>
    <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td height="15"></td>
            </tr>
            <tr>
                <td align="center">
                    <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                                    <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Nhân viên phụ trách</td>
                                                <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                        <tbody>
                                            <tr>
                                                <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                    <p style="padding-left: 24px;">&nbsp;</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Nhân viên thực hiện và phụ trách sản phẩm</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff">
                                    <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td height="25"></td>
                                            </tr>
                                            <?php foreach($workers as $worker):

                                            if($worker['user_status'] != 1) continue; 
                                            $admin_phone = get_user_meta_value($worker['user_id'], 'user_phone') ?: '(08) 6273.6363';
                                            ?>
                                            <tr><td height="25"></td></tr>

                                            <tr>
                                                <td>
                                                    <table class="table1-3" width="150" border="0" align="left" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" class="img1" src="<?php echo $worker['user_avatar']; ?>" width="150" height="150" alt="img"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                                        <tbody>
                                                            <tr>
                                                                <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                                    <p style="padding-left:24px;">&nbsp;</p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table3-1" width="375" border="0" align="right" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" style="<?php echo $content_style; ?>">

                                                                    <span style="color: #3498db; font-size: 17px;"><?php echo  $worker['display_name'];?></span>
                                                                    <p>Chuyên viên phụ trách kỹ thuật</p>
                                                                    <p>Điện thoại: <?php echo $admin_phone; ?></p>
                                                                    <p>
                                                                        Email:
                                                                        <a href="mailto:<?php echo $worker['user_email']; ?>" target="_blank">
                                                                            <?php echo $worker['user_email']; ?>    
                                                                        </a>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                        <tr><td height="25"></td></tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="15"></td>
        </tr>
    </tbody>
</table>
<?php endif; ?>

<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="15"></td>
    </tr>
    <tr>
        <td align="center" bgcolor="#ffffff" style="border-top:3px solid #3498db;border-bottom:2px solid #e0e0e0;">
            <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="table-inner">
                <tr>
                    <td align="center">
                        <table class="table-full" align="center" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <img src="http://webdoctor.vn/images/quote-left.png" width="25" height="37" alt="img" />
                                </td>
                                <td align="center" bgcolor="#3498db" style="font-family: open sans, arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 24px;padding-right: 25px;">
                                    <a style="color: #ffffff;text-decoration: none;" href="https://1web.vn">
                                        1WEB.VN
                                    </a>
                                </td>
                                <td><img src="http://webdoctor.vn/images/quote-right.png" width="25" height="37" alt="img" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-family: open sans, arial, sans-serif; font-size:30px; color:#4a4a4a; font-weight:bold;">
                        <a style="color: #4a4a4a;text-decoration: none;" href="https://1web.vn">GIẢI PHÁP 1WEB.VN</a>
                    </td>
                </tr>
                <tr>
                    <td height="15"></td>
                </tr>
                <tr>
                    <td align="center" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px; line-height:28px; text-align: justify;">


                       <table class="table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td align="center" style="line-height: 0px;"><a href="https://1web.vn/" target="_blank">
                                        <img src="https://1web.vn/wp-content/themes/Webdoctor/images/logo.png"></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr> <td align="center" style="line-height: 0px;">
                                    <a href="http://adsplus.vn/" target="_blank"><img src="https://webdoctor.vn/images/adsplus-logo.png"></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>

            </tr>

            <tr>
                <td align="center" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px; line-height:28px; text-align: justify;">

                    <p>
                        <b>Sứ mệnh của 1Web.vn</b><br>
                        Nền tảng khởi tạo Website miễn phí dành cho các doanh nghiệp tại Việt Nam dựa trên nền tảng AI, hoàn thiện và thiết lập website chỉ trong 1 giờ.
                    </p>
                    <p>
                        <b>Sứ mệnh của Webdoctor.vn</b><br>
                        Là sát cánh với Khách hàng; giúp Khách hàng có một website tốt với chi phí vận hành tiết kiệm; và qua đó giúp Khách hàng phát huy được tối đa hiệu quả kinh doanh.
                    </p>
                    <p>
                        <b>Sứ mệnh của Adsplus.vn</b> <br>
                        Google và Facebook lần lượt là hai kênh quảng cáo trực tuyến phổ biến nhất Việt Nam về tính hiệu quả và tỷ lệ quy đổi ra đơn hàng. Adsplus.vn nỗ lực giúp Khách hàng sử dụng hai kênh quảng cáo này tốt nhất; đơn giản nhất; minh bạch nhất và qua đó giúp Khách hàng nâng cao doanh số cũng như phát triển thương hiệu.
                    </p>
                </td>
            </tr>
            <tr>
                <td height="25"></td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" height="40" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px;padding-left: 25px;padding-right: 25px;border:2px solid #3498db;">
                                <a href="https://1web.vn/" target="_blank">Xem chi tiết</a>
                            </td>

                            <td align="center" height="40" style="font-family:open sans,arial,sans-serif;color:#444444;font-size:14px;padding-left: 5px;padding-right: 5px;"></td>
                                                
                            <?php if ( ! empty($customer)): 
                            $_args = array(
                                'event' => 'july-june-event',
                                'email' => $customer->user_email,
                                'name' => $customer->display_name,
                                'phone' => get_user_meta_value($customer->user_id, 'customer_phone'),
                                'source' => $subject ?? ''
                            );
                            $guru_emmbed_link = base_url('wservice/v1/guru/subscribe?'.http_build_query($_args));
                            ?>
                            <td align="center" height="40" style="font-family:open sans,arial,sans-serif;color:#444444;font-size:14px;border: 2px solid #f58220;background-color:  #f58220;">
                                <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTcuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDM0NS44MzQgMzQ1LjgzNCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzQ1LjgzNCAzNDUuODM0OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjY0cHgiIGhlaWdodD0iNjRweCI+CjxnPgoJPHBhdGggZD0iTTMzOS43OTgsMjYwLjQyOWMwLjEzLTAuMDI2LDAuMjU3LTAuMDYxLDAuMzg1LTAuMDk0YzAuMTA5LTAuMDI4LDAuMjE5LTAuMDUxLDAuMzI2LTAuMDg0ICAgYzAuMTI1LTAuMDM4LDAuMjQ3LTAuMDg1LDAuMzY5LTAuMTI5YzAuMTA4LTAuMDM5LDAuMjE3LTAuMDc0LDAuMzI0LTAuMTE5YzAuMTE1LTAuMDQ4LDAuMjI2LTAuMTA0LDAuMzM4LTAuMTU3ICAgYzAuMTA5LTAuMDUyLDAuMjItMC4xLDAuMzI3LTAuMTU4YzAuMTA3LTAuMDU3LDAuMjA4LTAuMTIyLDAuMzEyLTAuMTg0YzAuMTA3LTAuMDY0LDAuMjE1LTAuMTI0LDAuMzE5LTAuMTk0ICAgYzAuMTExLTAuMDc0LDAuMjE0LTAuMTU2LDAuMzIxLTAuMjM2YzAuMDktMC4wNjcsMC4xODItMC4xMywwLjI3LTAuMjAyYzAuMTYyLTAuMTMzLDAuMzE2LTAuMjc1LDAuNDY2LTAuNDIxICAgYzAuMDI3LTAuMDI2LDAuMDU2LTAuMDQ4LDAuMDgzLTAuMDc1YzAuMDI4LTAuMDI4LDAuMDUyLTAuMDU5LDAuMDc5LTAuMDg4YzAuMTQ0LTAuMTQ4LDAuMjg0LTAuMywwLjQxNi0wLjQ2ICAgYzAuMDc3LTAuMDk0LDAuMTQ0LTAuMTkyLDAuMjE2LTAuMjg5YzAuMDc0LTAuMSwwLjE1Mi0wLjE5NywwLjIyMS0wLjMwMWMwLjA3NC0wLjExMSwwLjEzOS0wLjIyNiwwLjIwNy0wLjM0ICAgYzAuMDU3LTAuMDk2LDAuMTE4LTAuMTksMC4xNzEtMC4yODljMC4wNjItMC4xMTUsMC4xMTQtMC4yMzQsMC4xNjktMC4zNTFjMC4wNDktMC4xMDQsMC4xMDEtMC4yMDcsMC4xNDYtMC4zMTQgICBjMC4wNDgtMC4xMTUsMC4wODYtMC4yMzIsMC4xMjgtMC4zNDljMC4wNDEtMC4xMTQsMC4wODUtMC4yMjcsMC4xMi0wLjM0M2MwLjAzNi0wLjExOCwwLjA2Mi0wLjIzOCwwLjA5Mi0wLjM1OCAgIGMwLjAyOS0wLjExOCwwLjA2My0wLjIzNCwwLjA4Ni0wLjM1M2MwLjAyOC0wLjE0MSwwLjA0NS0wLjI4MywwLjA2NS0wLjQyNWMwLjAxNC0wLjEsMC4wMzMtMC4xOTksMC4wNDMtMC4zICAgYzAuMDI1LTAuMjQ5LDAuMDM4LTAuNDk4LDAuMDM4LTAuNzQ4VjkyLjc2YzAtNC4xNDMtMy4zNTctNy41LTcuNS03LjVoLTIzNi4yNWMtMC4wNjYsMC0wLjEzLDAuMDA4LTAuMTk2LDAuMDEgICBjLTAuMTQzLDAuMDA0LTAuMjg1LDAuMDEtMC40MjcsMC4wMjJjLTAuMTEzLDAuMDA5LTAuMjI1LDAuMDIyLTAuMzM3LDAuMDM3Yy0wLjEyOCwwLjAxNi0wLjI1NSwwLjAzNS0wLjM4MiwwLjA1OCAgIGMtMC4xMTksMC4wMjEtMC4yMzcsMC4wNDYtMC4zNTQsMC4wNzNjLTAuMTE5LDAuMDI4LTAuMjM4LDAuMDU4LTAuMzU2LDAuMDkyYy0wLjExNywwLjAzMy0wLjIzMiwwLjA2OS0wLjM0NiwwLjEwNyAgIGMtMC4xMTcsMC4wNC0wLjIzNCwwLjA4Mi0wLjM0OSwwLjEyOGMtMC4xMDksMC4wNDMtMC4yMTYsMC4wODctMC4zMjIsMC4xMzVjLTAuMTE4LDAuMDUzLTAuMjM1LDAuMTEtMC4zNTEsMC4xNjkgICBjLTAuMDk5LDAuMDUxLTAuMTk2LDAuMTAzLTAuMjkyLDAuMTU4Yy0wLjExNiwwLjA2Ni0wLjIzLDAuMTM2LTAuMzQzLDAuMjA4Yy0wLjA5MywwLjA2LTAuMTg0LDAuMTIyLTAuMjc0LDAuMTg1ICAgYy0wLjEwNiwwLjA3NS0wLjIxMSwwLjE1My0wLjMxNCwwLjIzNWMtMC4wOTQsMC4wNzUtMC4xODYsMC4xNTItMC4yNzcsMC4yMzFjLTAuMDksMC4wNzktMC4xNzksMC4xNTgtMC4yNjYsMC4yNDIgICBjLTAuMDk5LDAuMDk1LTAuMTk0LDAuMTk0LTAuMjg4LDAuMjk0Yy0wLjA0NywwLjA1LTAuMDk3LDAuMDk0LTAuMTQyLDAuMTQ1Yy0wLjAyNywwLjAzLTAuMDQ4LDAuMDYzLTAuMDc0LDAuMDkzICAgYy0wLjA5NCwwLjEwOS0wLjE4MiwwLjIyMy0wLjI3LDAuMzM4Yy0wLjA2NCwwLjA4NC0wLjEzLDAuMTY4LTAuMTksMC4yNTRjLTAuMDc4LDAuMTEyLTAuMTUsMC4yMjctMC4yMjIsMC4zNDMgICBjLTAuMDU5LDAuMDk1LTAuMTIsMC4xODktMC4xNzQsMC4yODZjLTAuMDYzLDAuMTEyLTAuMTE4LDAuMjI3LTAuMTc1LDAuMzQyYy0wLjA1MiwwLjEwNS0wLjEwNiwwLjIxLTAuMTUzLDAuMzE3ICAgYy0wLjA0OSwwLjExMy0wLjA5MiwwLjIzLTAuMTM1LDAuMzQ1Yy0wLjA0MywwLjExMy0wLjA4NywwLjIyNS0wLjEyNCwwLjMzOWMtMC4wMzcsMC4xMTUtMC4wNjcsMC4yMzItMC4wOTksMC4zNDkgICBjLTAuMDMyLDAuMTItMC4wNjYsMC4yMzktMC4wOTMsMC4zNmMtMC4wMjUsMC4xMTMtMC4wNDIsMC4yMjgtMC4wNjIsMC4zNDJjLTAuMDIyLDAuMTMtMC4wNDQsMC4yNi0wLjA2LDAuMzkgICBjLTAuMDEzLDAuMTA4LTAuMDE5LDAuMjE4LTAuMDI3LDAuMzI4Yy0wLjAxLDAuMTQtMC4wMTksMC4yOC0wLjAyMSwwLjQyMWMtMC4wMDEsMC4wNDEtMC4wMDYsMC4wODEtMC4wMDYsMC4xMjJ2NDYuMjUyICAgYzAsNC4xNDMsMy4zNTcsNy41LDcuNSw3LjVzNy41LTMuMzU3LDcuNS03LjV2LTI5LjU5NWw2Ni42ODEsNTkuMDM3Yy0wLjM0OCwwLjI0NS0wLjY4MywwLjUxNi0wLjk5NSwwLjgyN2wtNjUuNjg3LDY1LjY4N3YtNDkuMjg4ICAgYzAtNC4xNDMtMy4zNTctNy41LTcuNS03LjVzLTcuNSwzLjM1Ny03LjUsNy41djkuMTY0aC0zOC43NWMtNC4xNDMsMC03LjUsMy4zNTctNy41LDcuNXMzLjM1Nyw3LjUsNy41LDcuNWgzOC43NXY0My4yMzEgICBjMCw0LjE0MywzLjM1Nyw3LjUsNy41LDcuNWgyMzYuMjVjMC4yNDcsMCwwLjQ5NC0wLjAxMywwLjc0LTAuMDM3YzAuMTE1LTAuMDExLDAuMjI2LTAuMDMzLDAuMzM5LTAuMDQ5ICAgQzMzOS41NDIsMjYwLjQ2OSwzMzkuNjcsMjYwLjQ1NCwzMzkuNzk4LDI2MC40Mjl6IE0zMzAuODM0LDIzNC45NjdsLTY1LjY4OC02NS42ODdjLTAuMDQyLTAuMDQyLTAuMDg3LTAuMDc3LTAuMTMtMC4xMTcgICBsNDkuMzgzLTQxLjg5N2MzLjE1OC0yLjY4LDMuNTQ2LTcuNDEyLDAuODY2LTEwLjU3MWMtMi42NzgtMy4xNTctNy40MS0zLjU0Ny0xMC41NzEtMC44NjZsLTg0LjM4MSw3MS41OWwtOTguNDQ0LTg3LjE1OGgyMDguOTY1ICAgVjIzNC45Njd6IE0xODUuODc4LDE3OS44ODhjMC41MzUtMC41MzUsMC45NjktMS4xMzEsMS4zMDgtMS43NjVsMjguMDUxLDI0LjgzNWMxLjQxOCwxLjI1NSwzLjE5NCwxLjg4NSw0Ljk3MiwxLjg4NSAgIGMxLjcyNiwwLDMuNDUxLTAuNTkzLDQuODUzLTEuNzgxbDI4LjU4Ny0yNC4yNTRjMC4yNiwwLjM4LDAuNTUzLDAuNzQzLDAuODksMS4wOGw2NS42ODcsNjUuNjg3SDEyMC4xOTFMMTg1Ljg3OCwxNzkuODg4eiIgZmlsbD0iI0ZGRkZGRiIvPgoJPHBhdGggZD0iTTcuNSwxNzAuNjc2aDEyNi42NjdjNC4xNDMsMCw3LjUtMy4zNTcsNy41LTcuNXMtMy4zNTctNy41LTcuNS03LjVINy41Yy00LjE0MywwLTcuNSwzLjM1Ny03LjUsNy41ICAgUzMuMzU3LDE3MC42NzYsNy41LDE3MC42NzZ6IiBmaWxsPSIjRkZGRkZGIi8+Cgk8cGF0aCBkPSJNMjAuNjI1LDEyOS4zNDVINzcuNWM0LjE0MywwLDcuNS0zLjM1Nyw3LjUtNy41cy0zLjM1Ny03LjUtNy41LTcuNUgyMC42MjVjLTQuMTQzLDAtNy41LDMuMzU3LTcuNSw3LjUgICBTMTYuNDgyLDEyOS4zNDUsMjAuNjI1LDEyOS4zNDV6IiBmaWxsPSIjRkZGRkZGIi8+Cgk8cGF0aCBkPSJNNjIuNSwyMjYuNTFoLTU1Yy00LjE0MywwLTcuNSwzLjM1Ny03LjUsNy41czMuMzU3LDcuNSw3LjUsNy41aDU1YzQuMTQzLDAsNy41LTMuMzU3LDcuNS03LjVTNjYuNjQzLDIyNi41MSw2Mi41LDIyNi41MXoiIGZpbGw9IiNGRkZGRkYiLz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" style="display: inline-block !important;width:  24px;height:  24px;position: relative;top: 7px;left: 18px;">
                                <a href="<?php echo $guru_emmbed_link ;?>" style="color: #fff;padding-left: 25px;padding-right: 25px;padding-top:  10px;padding-bottom:  10px;display:  inline-block;" target="_blank">
                                    Đăng ký Event Adwords miễn phí
                                </a>
                            </td>
                            <?php endif ?>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="35"></td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td height="15"></td>
</tr>
</table>

<table width="100%" bgcolor="#ecf0f1" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="15"></td>
    </tr>
    <tr>
        <td bgcolor="#34495e" style="border-top:4px solid #3498db;">
            <table class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="35"></td>
                </tr>
                <tr>
                    <td>
                        <table class="table-full" bgcolor="#405366" width="310" border="0" align="right" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#34495e">
                                        <tr>
                                            <td align="left" bgcolor="#34495e" style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Tư vấn miễn phí</td>
                                            <td width="25" align="left" bgcolor="#405366" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/note-1.png" width="23" height="30" alt="img" /></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="table-inner" align="center" width="260" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="footer-link" align="left" bgcolor="#405366" style="font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7; line-height:28px;">
                                                <b>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</b><br>
                                                <b>Hotline:</b>1800.0098</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="25" align="right" valign="bottom" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/note-2.png" width="25" height="25" alt="img" /></td>
                                </tr>
                            </table>
                            <table width="1" border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                        <p style="padding-left:24px;">&nbsp;</p>
                                    </td>
                                </tr>
                            </table>
                            <table class="table-full" align="left" width="120" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Thông tin</td>
                                </tr>
                                <tr>
                                    <td height="10"></td>
                                </tr>
                                <tr>
                                    <td class="footer-link" style="line-height:28px; font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7;">
                                        <a href="https://1web.vn/" target="_blank">Về chúng tôi</a><br/>
                                        <a href="https://1web.vn/bang-gia/" target="_blank">Bảng giá</a><br/>
                                        <a href="https://1web.vn/lien-he/" target="_blank">Liên hệ</a>
                                    </td>
                                </tr>
                            </table>
                            <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                <tr>
                                    <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                        <p style="padding-left:24px;">&nbsp;</p>
                                    </td>
                                </tr>
                            </table>
                            <table class="table-full" align="left" width="120" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Social</td>
                                </tr>
                                <tr>
                                    <td height="10"></td>
                                </tr>
                                <tr>
                                    <td class="footer-link" style="line-height:28px; font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7;">
                                        <a href="https://www.facebook.com/1web.thietkewebmienphi/" target="_blank" style="color:#bdc3c7;font-weight: normal;">
                                            Facebook
                                        </a> 
                                        <br/>
                                        <a href="https://www.youtube.com/channel/UCmZ7m02SjMf4V8W06PDTcsQ" target="_blank">
                                            Youtube
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="35"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#2c3e50">
                <table align="center" width="600" class="table-inner" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td class="footer-link">
                            <table align="left" class="table-full" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="font-family: 'Open sans', Arial, sans-serif; color:#bdc3c7; font-size:14px;">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không phải reply lại email này. </td>
                                </tr>       
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
