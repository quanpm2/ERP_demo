<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="60;url=<?php echo base_url('tv_overview/top_sale.html');?>" />
<title>Thống kê hợp đồng</title>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700&subset=latin,vietnamese' rel='stylesheet' type='text/css'>

<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,700,500,500italic,700italic,900,900italic&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
<style type="text/css">
body, td, th {
  font-size: 18px;
  color: #000;
  font-family: 'Roboto', sans-serif;
}
body {
  margin-left: 0px;
  margin-top: 0px;
  margin-right: 0px;
  margin-bottom: 0px;
  background: #39a8dd;
}
.gold {
  color: #fb0;
  float: left;
  font-size: 260px;
  font-weight: 700;
  line-height: 217px;
  text-shadow: 2px 2px 5px #000;
}
.hd {
  float: left;
  font-family: roboto slab;
  font-size: 50px;
  font-weight: 600;
}
.wow.flash.hd {
}
.new {
  color: #fff;
  font-family: roboto slab;
  font-size: 129px;
  font-weight: 700;
  text-shadow: 2px 2px 5px #000;
}
.do{font-size:50px; text-align:center}
.do img {
    vertical-align: top;
} 
</style>
<link rel="stylesheet" href="<?php echo theme_url('tv_overview/css/animate.css');?>">
<script src="<?php echo theme_url('tv_overview/js/wow.min.js');?>"></script>
<script>
new WOW().init();
</script>


</head>

<body>
<div style="width:100vw; height:100vh; position:relative; overflow:hidden">
  <div style="position:absolute; left:50px; top:50px"><img width="172" height="89" style="float: right; visibility: visible; animation-duration: 30s; animation-name: slideInRight;" src="<?php echo theme_url('tv_overview/images/cloud1.png');?>" data-wow-duration="30s" class="wow slideInRight"></div>
  <div style="position: absolute; top: 200px; left: 140px;"><img width="172" height="89" style="float: right; visibility: visible; animation-duration: 40s; animation-name: slideInRight;" src="<?php echo theme_url('tv_overview/images/cloud2.png');?>" data-wow-duration="40s" class="wow slideInRight"></div>
  <div style="position: absolute; left: 480px; top: -30px;"><img width="172" height="89" style="float: right; visibility: visible; animation-duration: 20s; animation-name: slideInRight;" src="<?php echo theme_url('tv_overview/images/cloud3.png');?>" data-wow-duration="20s" class="wow slideInRight"></div>
  <div style="position: absolute; top: 50px; left: 930px;"><img width="172" height="89" style="float: right; visibility: visible; animation-duration: 20s; animation-name: slideInLeft;" src="<?php echo theme_url('tv_overview/images/cloud4.png');?>" data-wow-duration="20s" class="wow slideInLeft"></div>
  <div style="position: absolute; left: 1280px; top: -50px;"><img width="172" height="89" style="float: right; visibility: visible; animation-duration: 30s; animation-name: slideInRight;" src="<?php echo theme_url('tv_overview/images/cloud5.png');?>" data-wow-duration="30s" class="wow slideInRight"></div>
  <div style="position: absolute; top: 50px; left: 1580px;"><img width="172" height="89" style="float: right; visibility: visible; animation-duration: 40s; animation-name: slideInRight;" src="<?php echo theme_url('tv_overview/images/cloud5.png');?>" data-wow-duration="40s" class="wow slideInLeft"></div>
  <div style="position: absolute; top: 50px; left: 1710px;"><img width="172" height="89" style="float: right; visibility: visible; animation-duration: 20s; animation-name: slideInLeft;" src="<?php echo theme_url('tv_overview/images/cloud6.png');?>" data-wow-duration="20s" class="wow slideInLeft"></div>
  <div style="position: absolute; float: left; top: 70px; left: 300px;">
    <div class="gold wow flash" data-wow-iteration="5"><?php echo $adsplus_new_count;?></div>
    <div class="wow flash hd" data-wow-delay="0.2s">HỢP ĐỒNG</div>
    <div class="wow flash new" data-wow-delay="0.5s">MỚI</div>
  </div>
  <div style="position: absolute; float: right; top: 70px; right: 300px;">
    <div class="gold wow flash" data-wow-iteration="5"><?php echo $webgeneral_new_count;?></div>
    <div class="wow flash hd" data-wow-delay="0.2s">HỢP ĐỒNG</div>
    <div class="wow flash new" data-wow-delay="0.5s">MỚI</div>
  </div>
  <div style="padding: 15px; background: rgba(255, 255, 255, 0.2) none repeat scroll 0% 0%; text-align: center; font-family: roboto slab; color: rgb(255, 255, 255); position: relative; top: 35%; font-size: 70px; text-shadow: 2px 2px 5px rgb(0, 0, 0);" >HỢP ĐỒNG ĐANG THỰC HIỆN</div>

  <?php 
  $webdoctor_goal = 100;
  $adsplus_goal = 200;
  ?>

  <div style="position: absolute; float: left; bottom: 110px; left: 300px;">
    <div class="do wow bounceInUp">
      <img src="<?php echo theme_url('tv_overview/images/adsplus.png');?>" width="203" height="200" /> 
      <span class="new">
      <?php echo $adsplus_count;?></span>/<?php echo $adsplus_goal; ?><br>
      <span style="font-family: roboto; font-weight: 100; font-size: 100px; color: rgb(255, 255, 255);">
      <?php echo div($adsplus_count,$adsplus_goal) >= 0.5 ? 'CỐ LÊN' : 'QUÁ BÈO';?>
      </span>
    </div>
  </div>
  <div style="position: absolute; float: right; bottom: 110px; right: 300px;">
    <div class="do wow bounceInUp">
      <img width="203" height="200" src="<?php echo theme_url('tv_overview/images/wd.png');?>"> 
        <span class="new"><?php echo $webgeneral_count;?></span>/<?php echo $webdoctor_goal; ?>
        <br>
        <span style="font-family: roboto; font-weight: 100; font-size: 100px; color: rgb(255, 255, 255);">
        <?php echo div($webgeneral_count,$webdoctor_goal) >= 0.5 ? 'CỐ LÊN' : 'QUÁ BÈO';?>
        </span>
    </div>
  </div>
  <div style="position:absolute; bottom:0; left:30%"><img class="wow fadeInUp" data-wow-delay="0.8s" src="<?php echo theme_url('tv_overview/images/sm.png');?>" width="512" height="226" /></div>
</div>
</body>
</html>