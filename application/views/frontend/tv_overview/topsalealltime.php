﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>You are my Idol</title>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,700,500,500italic,700italic,900,900italic&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
<style type="text/css">
body, td, th {
	font-size: 18px;
	color: #000;
	font-family: 'Roboto', sans-serif;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background: #39a8dd url(<?php echo theme_url('tv_overview/images/3.jpg');?>) no-repeat top center;
    background-size: cover;
	
}
.gold {
	color: #fb0;
	float: left;
	font-size: 260px;
	font-weight: 700;
	line-height: 217px;
	text-shadow: 2px 2px 5px #000;
}
.hd {
	float: left;
	font-family: roboto slab;
	font-size: 50px;
	font-weight: 600;
}
.new {
	color: #fff;
	font-family: roboto slab;
	font-size: 129px;
	font-weight: 700;
	text-shadow: 2px 2px 5px #000;
}
.do{font-size:50px; text-align:center}
.title {
    color: #fff;
    font-size: 100px;
    font-weight: 100;
    line-height: 130px;
    text-align: center;
    text-shadow: 1px 1px 2px #000;
	margin:140px 100px;
	border-bottom:1px solid #fff
}

.title strong {
    font-weight: 700;
}

.son {
    bottom: 100px;
    position: absolute;
    right: 0;
}
</style>
<link rel="stylesheet" href="<?php echo theme_url('tv_overview/css/animate.css');?>">
<script src="<?php echo theme_url('tv_overview/js/wow.min.js');?>"></script>
<script>
new WOW().init();
</script>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script>
$(function () {
	 /**
     * A small Highcharts snippet/plugin for adding images to axis labels
     */
    (function (H) {
        function addImages(proceed) {

            proceed.call(this);

            var chart = this,
                axis = chart.xAxis[0],
                images = axis.options.images;
            H.each(axis.tickPositions, function (pos) {
                var tick = axis.ticks[pos],
                    x,
                    y;
                if (images[pos]) {
                    x = axis.toPixels(pos) - 80;
                    y = chart.plotTop-150;
                    if (!tick.image) {
                        tick.image = chart.renderer.image(images[pos], x, y, 148, 208)
                        .add();
                    } else { // Update existing
                        tick.image.animate({
                            x: x,
                            y: y
                        });
                    }
                }
            });
        }

        H.wrap(H.Chart.prototype, 'render', addImages);
        H.wrap(H.Chart.prototype, 'redraw', addImages);
    }(Highcharts));
    // Create the chart

    $('#container').highcharts({
        chart: {
            type: 'column',
			backgroundColor: 'none',
			spacingTop: 150,
			
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
			
            type: 'category',
		images: [
                '/template/frontend/tv_overview/images/hai.gif',
                '/template/frontend/tv_overview/images/phuong.gif',
		'/template/frontend/tv_overview/images/thao.gif',
		'/template/frontend/tv_overview/images/trinh.gif',
		'/template/frontend/tv_overview/images/thuy.gif',
		'/template/frontend/tv_overview/images/linh.gif'
            ],
		labels: {
			style: {
				color: '#fff',
				font: '40px roboto ',
				fontWeight: '100',
			},
			formatter: function () {
				return this.value;
			},
			y: 40,
		},
        },
        yAxis: {
			labels: {
                enabled: false
            },
			gridLineWidth: 0,
            minorGridLineWidth: 0,
            title: {
                text: '',
				
            }
        },
        legend: {
            enabled: false
        },
		rect:{
			enabled: false
		},
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
					color:'#fff',
                    format: '{point.y}HĐ',
					style: {
						fontWeight: 'bold',
						fontSize:'50px'
					}
                }
            }
        },

        tooltip: {
            headerFormat: '',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}HĐ</b><br/>'
        },
		

        series: [{
            name: 'NV',
            colorByPoint: true,
            data: <?php echo $series_data;?>
        }],
        
    });
});
</script>

</head>

<body>
<div class="title" ><strong>You are my Idol</strong></div>
<div id="container" style="min-width: 250px; height: 600px; margin: 0 300px 0 100px "></div>
<img src="<?php echo theme_url('tv_overview/images/son.gif');?>" class="son"/>
</body>
</html>
