<!DOCTYPE html>
<html>
<?php echo $this->template->header->view('include/header');?>
<body class="<?php echo $this->template->body_class;?>">
	<script type="text/javascript">
		var base_url = "<?php echo base_url();?>";
		var admin_url = "<?php echo admin_url();?>";
		var admin_theme_url = "<?php echo admin_theme_url();?>";
	</script>
	<!-- Site wrapper -->
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="<?php echo admin_url();?>" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini">ERP</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg">ERP</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<i class="fa fa-bars"></i>
				</button>

				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<!-- Collect the nav links, forms, and other content for toggling -->

				<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
				<?php
				echo $this->menu->render_menu('navbar',array(
				'nav_tag_open'        => '<ul class="nav navbar-nav">',            
				'parentl1_tag_open'   => '<li class="dropdown">',
				'parentl1_anchor'     => '<a tabindex="0" data-toggle="dropdown" href="%s"> <span>%s</span> <i class="caret"></i></a>',
				'parent_tag_open'     => '<li class="dropdown-submenu">',
				'parent_anchor'       => '<a href="%s" data-toggle="dropdown"><span>%s</span> <i class="fa fa-angle-right pull-right"></i></a>',
				'children_tag_open'   => '<ul class="dropdown-menu" role="menu">'
				));
				?>                    
				</div><!-- /.navbar-collapse -->
				<?php if($this->admin_m->checkLogin()): ?>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?php echo image($this->admin_m->avatar);?>" class="user-image" alt="User Image"/>
								<span class="hidden-xs"><?php echo $this->admin_m->name;?></span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="<?php echo image($this->admin_m->avatar);?>" class="img-circle" alt="User Image" />
									<p>
										<?php echo $this->admin_m->name;?> -
										<?php echo $this->admin_m->role_name ?? null;?>

										<small><?php echo 'Thành viên từ Tháng ' .my_date($this->admin_m->user_time_create, 'm, Y');?></small>
									</p>
								</li>
								<?php /*
								<!-- Menu Body -->
								<li class="user-body">
									<div class="col-xs-4 text-center">
										<a href="#">Followers</a>
									</div>
									<div class="col-xs-4 text-center">
										<a href="#">Sales</a>
									</div>
									<div class="col-xs-4 text-center">
										<a href="#">Friends</a>
									</div>
								</li>
								*/?>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="<?php echo admin_url();?>staffs/profile" class="btn btn-default btn-flat">Thông tin cá nhân</a>
									</div>
									<div class="pull-right">
										<a href="<?php echo admin_url();?>staffs/logout" class="btn btn-default btn-flat">Đăng xuất</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				<?php endif; ?>
				</nav>
			</header>

			<!-- =============================================== -->

			<?php echo $this->template->left->view('include/left');?>

			<!-- =============================================== -->

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1><?php echo $this->template->title;?><small> <?php echo $this->template->description;?></small></h1>
					<!-- <ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
						<li><a href="#">Examples</a></li>
						<li class="active">Blank page</li>
					</ol> -->
				</section>
				<!-- Main content -->
				<section class="content" id="app-container">
					<div class="row">
						<div class="col-md-12">
						<?php if($this->template->is_box_open->content()) :  
						 echo $this->template->content->content();
							else : ?>
							<div class="<?php echo $this->template->content_box_class;?> ">
								<div class="box-body">
									<?php echo $this->template->content->content();?>
								</div>
							</div>
						<?php endif; ?>
						</div>
					</div>
				</section>
			</div><!-- /.content-wrapper -->

			<?php echo $this->template->footer->view('include/footer');?>
		</body>
		</html>