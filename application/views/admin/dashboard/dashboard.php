<div class="col-md-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<i class="fa fw fa-external-link"></i>
			<h3 class="box-title">v20170901</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<ol>
				<li><b>Dự thu hợp đồng :</b>
					<ul>
						<li>Feature : Bổ sung hợp đồng "tối ưu website" vào bảng thống kê doanh thu | nghiệm thu</li>
						<li>Feature : Bổ sung hợp đồng "thiết kế banner" vào bảng thống kê doanh thu | nghiệm thu</li>
						<li>Feature : Bổ sung hợp đồng "domain" vào bảng thống kê doanh thu | nghiệm thu</li>
					</ul>
				</li>
				<li><b>Hợp đồng tên miền :</b>
					<ul>
						<li>Feature : Quản trị hợp đồng quản lý "tên miên"</li>
						<li>Feature : Chức năng Gia hạn hợp đồng</li>
					</ul>
				</li>
				<li><b>Hợp đồng thiết kế banner :</b>
					<ul>
						<li>Feature : Quản trị hợp đồng quản lý "thiết kế banner"</li>
					</ul>
				</li>
				<li><b>Hợp đồng tối ưu :</b>
					<ul>
						<li>Feature : Có thể lấy điểm tự động và gửi "nghiệm thu" hợp đồng đến khách hàng</li>
						<li>Feature : Tự động lấy điểm mobile | desktop trước khi kích hoạt dịch vụ</li>
						<li>Feature : "Shortlink" đến trang Google pagespeed Insight</li>
					</ul>
				</li>
			</ul>
		</div>
	<!-- /.box-body -->
	</div>
	<!-- /.box -->
</div>
<!-- END ALERT BOX -->

<div class="col-md-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<i class="fa fw fa-external-link"></i>
			<h3 class="box-title">v20160000</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<ul>
				<li>Feature : Bộ phận kinh doanh đã có thể giám sát được các hợp đồng quảng cáo Adword của mình trong phần quản lý<a href="<?php echo admin_url('googleads/');?>"></a></li>
			</ul>
		</div>
	<!-- /.box-body -->
	</div>
	<!-- /.box -->
</div>
<!-- END ALERT BOX -->

<div class="col-md-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<i class="fa fw fa-external-link"></i>
			<h3 class="box-title">v20160000</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<ul>
				<li>Feature : Chức năng "Đặt lịch hẹn KH" được đặt trong mục "Quản lý" đã được khởi tạo để phục vụ việc tính KPI cho bộ phận kinh doanh</li>
			</ul>
		</div>
	<!-- /.box-body -->
	</div>
	<!-- /.box -->
</div>
<!-- END ALERT BOX -->

<div class="col-md-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<i class="fa fw fa-external-link"></i>
			<h3 class="box-title">v20160000</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<ul>
				<li><s>Feature : Nhóm Backlink đã có thể thấy được các HĐ của mình trong phần quản lý Backlink</a></b>.</s></li>
			</ul>
		</div>
	<!-- /.box-body -->
	</div>
	<!-- /.box -->
</div>
<!-- END ALERT BOX -->