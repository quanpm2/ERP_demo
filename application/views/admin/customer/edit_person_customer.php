<div class="row">
	<div class="col-md-12">
		<?php 

		echo $this->admin_form->form_open();

		echo $this->admin_form->formGroup_begin(0,'Tên khách hàng');

		echo '<div class="col-xs-2" style="padding-left: 0;">';

		echo form_dropdown(array('name'=>'edit[meta][customer_gender]','class'=>'form-control'),array(1=>'Ông',0=>'Bà'),$this->usermeta_m->get_meta(@$edit->user_id,'customer_gender',TRUE,TRUE));

		echo '</div>';

		echo '<div class="col-xs-10">';

		echo form_input(array('name'=>'edit[display_name]','class'=>'form-control'), @$edit->display_name);

		echo '</div>';


		echo $this->admin_form->formGroup_end();

		echo $this->admin_form->input('Địa chỉ email','edit[meta][customer_email]',$this->usermeta_m->get_meta(@$edit->user_id,'customer_email',TRUE,TRUE));

		echo $this->admin_form->input('Địa chỉ liên hệ','edit[meta][customer_address]',$this->usermeta_m->get_meta(@$edit->user_id,'customer_address',TRUE,TRUE));

		echo $this->admin_form->input('Điện thoại di động','edit[meta][customer_phone]',$this->usermeta_m->get_meta(@$edit->user_id,'customer_phone',TRUE,TRUE));

		echo $this->admin_form->hidden('','edit[user_type]',  $user_type);	

		echo $this->admin_form->submit('add_customer','Save change');

		echo $this->admin_form->form_close();
		?>
	</div>
</div>
