<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
  <?php if($this->admin_m->checkLogin()): ?>
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo image($this->admin_m->avatar);?>" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p><?php echo $this->admin_m->name;?></p>

        <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $this->admin_m->role_name;?></a>
      </div>
    </div>
  <?php endif; ?>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <?php
    $this->menu->initialize(array(
      'nav_tag_open' => '<ul class="sidebar-menu" data-widget="tree">',
      'parentl1_tag_open' => '<li class="treeview">',
      'parentl1_anchor' => '<a href="%s"> %s</a>',
      'parent_tag_open' => '<li class="treeview">',
      'parent_anchor_tag' => '<a href="%s"><span>%s</span></a>',
      'children_tag_open' => '<ul class="treeview-menu">',
      'item_divider' => "<li class='divider'></li>",
    ));

    $this->menu->inject_item('<li class="header">KINH DOANH - HCNS - KẾ TOÁN</li>', 'first');
    echo $this->menu->render_menu('left','Item-0');
    $this->menu->clear('left');


    $this->menu->inject_item('<li class="header">DỊCH VỤ QUẢNG CÁO + ĐÀO TẠO</li>', 'first');
    echo $this->menu->render_menu('left-ads-service','Item-0');
    $this->menu->clear('left-ads-service');
      
    $this->menu->inject_item('<li class="header">DỊCH VỤ WEBSITE</li>', 'first');
    echo $this->menu->render_menu('left-service','Item-0');
    $this->menu->clear('left-service');

    $this->menu->inject_item('<li class="header">DỊCH VỤ KHÁC</li>', 'first');
    echo $this->menu->render_menu('left-external-service','Item-0');
    $this->menu->clear('left-external-service');
    ?>
  </section>
  <!-- /.sidebar -->
</aside>