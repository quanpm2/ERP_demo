<head>
    <meta charset="UTF-8">
    <title><?php echo $this->template->title;?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
   
    <link rel="icon" type="image/png" href="<?php echo base_url('template/admin/img/fav-icon.ico');?>" />
    <?php echo $this->template->stylesheet; ?>
    <?php echo $this->template->javascript; ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>