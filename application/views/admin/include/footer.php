
<footer class="main-footer">
<?php if($this->admin_m->checkLogin()): ?>
  <div class="pull-right hidden-xs">
    <b>Memory:</b> {memory_usage}/ CI: <strong><?php echo CI_VERSION;?></strong>/<strong><?php echo ENVIRONMENT;?></strong>
    <span> | <a href="https://adsplus.vn/chinh-sach-bao-mat-thong-tin-adsplus/" target="_blank"><u>Chính sách bảo mật thông tin</u></a></span>
  </div>
<?php endif; ?>
  Page rendered in <strong>{elapsed_time}</strong> seconds All rights reserved.
</footer>

<?php

$this->template->aside_class->set($this->usermeta_m->get_meta_value($this->admin_m->id,'user_sidebar_class'));

if(empty($this->template->aside_class)){

  $this->template->aside_class->set('control-sidebar control-sidebar-dark');
}
?>
<!-- Control Sidebar -->      
<aside class="<?php echo $this->template->aside_class;?>">                
  <!-- Create the tabs -->
  <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
    <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
  </ul>
  <!-- Tab panes -->
  <div class="tab-content"> </div>
</aside><!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class='control-sidebar-bg'></div>
</div><!-- ./wrapper -->
    
 <?php
 $_messages = $this->messages->get();
if(isset($_messages) && $_messages)
{ ?>
<script type="text/javascript">
<?php
 foreach($_messages as $key => $messages) { 
  foreach($messages as $message)
  {
  ?>
 $.notify("<?php echo $message;?>", "<?php echo $key;?>");
<?php 
}
}
 ?>
 </script>
<?php } ?>

<script src="<?php echo admin_theme_url('js/demo.min.js');?>"></script>
<script src="<?php echo admin_theme_url('js/custom.js');?>"></script>
<script src="<?php echo dist_url('runtime.js');?>"></script>
<script src="<?php echo dist_url('vendors.js');?>"></script>
<?php echo $this->template->footer_javascript;?>