<?php 

echo $this->admin_form->form_open();	

echo $this->admin_form->input('Tên nhóm', 'edit[role_name]', @$edit->role_name);
echo $this->admin_form->input('Chú thích', 'edit[role_description]', @$edit->role_description);
echo $this->admin_form->input('URL đích login', 'edit[login_destination]', @$edit->login_destination);
echo '<div id="permissions">';
if($permissions)
{
	foreach($permissions as $permission_name => $domains)
	{
		$headings = array();
		if($domains)
		{
			foreach($domains as $domain_name => $_actions)
			{
				$rows = array();
				$name_context = $permission_name.'.'.$domain_name;
				$rows[] = $name_context;
				foreach($actions[$permission_name] as $action => $action_name)
				{
					$action_name = ucfirst($action_name);
					if(in_array($action, $_actions))
					{
						$full_permission = $name_context.'.'.$action;
						$is_permission = $this->permission_m->has_permission($full_permission, $edit_id);
						$checked = ($is_permission == true) ? 'checked' : '';
						$rows[] = '<input type="checkbox" name="permissions['.$permission[$name_context]->permission_id.'][]" value="'.$action.'" '.$checked.'>';
					} else {
						$rows[] = '--';
					}
				}
				$rows[] = $permission[$name_context]->description;
				$this->table->add_row($rows);
			}
		}

		$headings = array_keys($actions[$permission_name]);
		array_unshift($headings, $permission_name);
		array_push($headings, 'Description');

		$this->table->set_caption($permission_name);
		$this->table->set_heading($headings);

		echo $this->table->generate();
		$this->table->clear();
	}
}


// $this->table->add_row('Fred', 'Blue', 'Small');
echo '</div>';
echo $this->admin_form->submit('submit_permission','Save change');
echo $this->admin_form->form_close();
?>


<style type="text/css">
	.content .row .box-body table tbody tr td
	{

	}
	.content .row .box-body table tbody td:nth-child(2), .content .row .box-body table thead th:nth-child(2){
		text-align: center;
	}
	.content .row .box-body table tbody td:nth-child(3), .content .row .box-body table thead th:nth-child(3){
		background-color: #dff0d8;
		text-align: center;
	}
	.content .row .box-body table tbody td:nth-child(4), .content .row .box-body table thead th:nth-child(4){
		background-color: #d9edf7;
		text-align: center;
	}
	.content .row .box-body table tbody td:nth-child(5), .content .row .box-body table thead th:nth-child(5){
		background-color: #f2dede;
		text-align: center;
	}
	.content .row .box-body table tbody td:last-child, .content .row .box-body table thead th:last-child{
		background-color: #fff;
	}

	.content .row .box-body table tbody tr:hover td
	{
		background-color: #f5f5f5;
	}
</style>

<script type="text/javascript">
	$('#permissions table > thead >tr >th').click(function(){
		var i = $(this).index();
		var ti = $(this).parents('table').index();
		var trs = $('table').eq(ti).find('tr');
		set_checked = false;
		var flag = false;
		$(trs).each(function ()
		{
			x =  $(this).find('td').eq(i).find('input');
			$(x).each(function ()
			{
				if($(this).is('input'))
				{
					set_checked =  ! $(this).prop('checked');
					console.log($(this).is('input'));
					flag = true;
					return false;
				}
			});
			if(flag == true)
			{
				return false;
			}
		});

		$(trs).each(function ()
		{
			input = $(this).find('td').eq(i).find('input');
			$(input).prop('checked', set_checked);

		});

	});

</script>