<?php
if(has_permission('Admin.Role.add')){
	echo 
	'<div class="row">
		<div class="col-md-2 pull-right control-group form-group">' 
			. anchor($url_add, 'Thêm mới', 'class="btn btn-primary"') .
		'</div>
	</div>';
}

if(!empty($content)){

	echo $content['table'] . $content['pagination'];
}
?>

<script type="text/javascript">

$('table input[type="checkbox"]').iCheck({
	checkboxClass: 'icheckbox_flat-blue',
	radioClass: 'iradio_flat-blue'
});

 //Enable check and uncheck all functionality
$(".checkbox-toggle").click(function () {

	var clicks = $(this).data('clicks');

	if (clicks) {
		//Uncheck all checkboxes
		$("table input[type='checkbox']").iCheck("uncheck");
		$(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
	} else {
		//Check all checkboxes
		$("table input[type='checkbox']").iCheck("check");
		$(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
	}          
	$(this).data("clicks", !clicks);
});

$("#remove-post").click(function () {

    if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked

      	var ids = [];

      	$('.deleteRow').each(function(){

      		if($(this).is(':checked')) { 

      			ids.push($(this).val());
      		}
      	});

        var ids_string = ids.toString();  // array to string conversion 

        $.ajax({
        	type: "POST",

        	url: "<?php echo $url_delete;?>",

        	data: {data_ids:ids_string},

        	success: function(data) {

        		data.success = data.success ? 'info' : 'error';

    			$.notify(data.msg, 'info');

        		if(data.success){

        			$('.deleteRow').each(function(){

			      		if($(this).is(':checked')) { 

			      			$(this).closest('tr').replaceWith('');
			      		}
			      	});
        		}
            },
            async:false
        });
    }
});

</script>