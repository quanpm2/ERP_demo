<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['protocol']		= 'smtp';
$config['smtp_host']    = 'smtp.gmail.com';
$config['smtp_port']    = 587;
$config['smtp_crypto']	= 'tls';
$config['smtp_timeout'] = 7;
$config['smtp_user'] 	= getenv('WEBDOCTOR_SMTP_USER');
$config['smtp_pass'] 	= getenv('WEBDOCTOR_SMTP_PWD');
$config['charset']    	= 'utf-8';
$config['newline']    	= "\r\n";
$config['mailtype'] 	= 'html';
$config['dsn'] 			= TRUE;
$config['bcc_batch_mode'] = TRUE;
// $config['validation'] = FALSE;