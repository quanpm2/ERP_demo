<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config['amqps'] = [
	'host' 		=> getenv('AMQPS_HOST') ?: 'localhost',
	'port' 		=> getenv('AMQPS_PORT') ?: '5672',
	'user' 		=> getenv('AMQPS_USER') ?: 'guest',
	'password' 	=> getenv('AMQPS_PASSWORD') ?: 'guest',
];


$_insights_queues = array_filter(explode(',', getenv('AMQS_WATCHES_INSIGHT_QUEUES')));

$config['amqps_queues'] = [	

	'watches_insight_queues' => $_insights_queues,
	'googleads' 	=> 'googleads_events.default',
	'facebookads' 	=> 'facebookads_events.default',
	'utility' 	    => 'utility.default',

    'internal_queue' => [
        'contract_events' => getenv('AMQPS_INTERNAL_QUEUE_CONTRACT_EVENT') ?: 'contracts.event.default',
    ],

    'external_queue' => [
        'exchange_rate' => [
            'queue_name' => getenv('AMQPS_EXTERNAL_QUEUE_UPDATE_EXCHANGE_RATE_NAME'), 
            'job_class' => getenv('AMQPS_EXTERNAL_QUEUE_UPDATE_EXCHANGE_RATE_JOB_CLASS'),
            'job_queue' => getenv('AMQPS_EXTERNAL_QUEUE_UPDATE_EXCHANGE_RATE_JOB_QUEUE') ?: 'default',
        ],

        'notification' => [
            'queue_name' => getenv('AMQPS_EXTERNAL_QUEUE_NOTIFICATION_NAME'), 
            'job_class' => getenv('AMQPS_EXTERNAL_QUEUE_NOTIFICATION_JOB_CLASS'),
            'job_queue' => getenv('AMQPS_EXTERNAL_QUEUE_NOTIFICATION_JOB_QUEUE') ?: 'default',
        ],

        'maintenance_service' => [
            'queue_name' => getenv('AMQPS_EXTERNAL_QUEUE_MAINTENANCE_SERVICE_NAME'), 
            'job_class' => getenv('AMQPS_EXTERNAL_QUEUE_MAINTENANCE_SERVICE_JOB_CLASS'),
            'job_queue' => getenv('AMQPS_EXTERNAL_QUEUE_MAINTENANCE_SERVICE_JOB_QUEUE') ?: 'default',
        ]
    ],
];