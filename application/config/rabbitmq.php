<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
|  RabbitMQ AMQS COnfig
|--------------------------------------------------------------------------
| If Must be set if Money24h app integreated is enable
|
*/

$config['rabbitmq'] = array(
	'RABBITMQ_HOST' 	=> getenv('RABBITMQ_HOST', 'localhost'),
	'RABBITMQ_PORT' 	=> getenv('RABBITMQ_PORT', '5672'),
	'RABBITMQ_USER' 	=> getenv('RABBITMQ_USER', 'guest'),
	'RABBITMQ_PASSWORD' => getenv('RABBITMQ_PASSWORD', 'guest'),
	'RABBITMQ_VHOST' 	=> getenv('RABBITMQ_VHOST', '/'),
	'RABBITMQ_QUEUE' 	=> getenv('RABBITMQ_QUEUE', 'hello')
);

$config['apiCallerService'] = array(
	'queues' => [
		'name' => 'facebookads-insight-api-caller-service',
	]
);