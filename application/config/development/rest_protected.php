<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config['rest_auth'] 	= 'basic';
$config['auth_source'] 	= 'library';

$config['auth_library_class'] 		= 'Jwt_authentication';
$config['auth_library_function'] 	= 'authenticate';