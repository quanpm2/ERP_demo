<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['slack'] = array(
	'webhookURL' => getenv('SLACK_WEBHOOK_URL'),
	'token' => getenv('SLACK_TOKEN')
);