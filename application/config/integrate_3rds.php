<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
|  Money 24h API COnfig
|--------------------------------------------------------------------------
| If Must be set if Money24h app integreated is enable
|
*/
$config['money24h'] = array(
	'base' 				=> getenv('MONEY24H_BASE') ?: '',
	'username' 			=> getenv('MONEY24H_USERNAME') ?: '',
	'pwd' 				=> getenv('MONEY24H_PWD') ?: '',
	'client_oauth_key'	=> getenv('MONEY24H_CLIENT_OAUTH_KEY') ?: '',
);

$config['queueStockService'] = array(
	'uri' 				=> getenv('QUEUE_STOCK_SERVICE_URI') ?: '',
);