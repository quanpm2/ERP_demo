<?php

defined('BASEPATH') OR exit('No direct script access allowed');
$config['auth_status_codes'] = array(
	0 => 'Đăng nhập lại',
	1 => 'Token còn hạn sử dụng',
	2 => 'Tài khoản đang bị khóa',
	);