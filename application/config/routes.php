<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
http://cibonfire.com/docs/developer/routes
*/
$route['default_controller'] = 'uindex';
$route['404_override'] = 'uindex/page404';
$route['translate_uri_dashes'] = FALSE;
$route['image/(.+)'] = 'image/index/$1';

$route['report/seo-traffic/(.+)'] = 'seotraffic/report/$1';
$route['report/webdoctor/(.+)'] = 'webdoctor/report/$1';
$route['report/website-tong-hop/(.+)'] = 'webgeneral/report/$1';
$route['report/adsplus/download/(.+)/(.+)'] = 'googleads/report/download/$1/$2';
$route['report/facebookads/download/(.+)/(.+)'] = 'facebookads/report/download/$1/$2';


$route['tv_overview/top_staffs'] = 'googleads/statistical/top_staffs';
$route['tv_overview/top_staffs_active'] = 'googleads/statistical/top_staffs_active';
$route['tv_overview/top_inefficient'] = 'googleads/statistical/top_inefficient';

$route['admin'] = 'staffs';
$route['admin/login'] = 'staffs/login';
$route['admin/admin/oauth_login'] = 'staffs/oauth_login';
$route['admin/(:any)/init/(.+)'] = $route['404_override'];
$route['admin/(:any)/init'] = $route['404_override'];

// API route for module reporting
$route['api-v2/reporting/sales/(:any)'] = 'reporting/api_v2/sales/$1';
$route['api-v2/reporting/sales/groups/(:any)'] = 'reporting/api_v2/sales_group/$1';

$route['admin/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/$2/$3/$4/$5/$6/$7/$8/$9/$10';
$route['admin/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/$2/$3/$4/$5/$6/$7/$8/$9';
$route['admin/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/$2/$3/$4/$5/$6/$7/$8';
$route['admin/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/$2/$3/$4/$5/$6/$7';
$route['admin/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/$2/$3/$4/$5/$6';
$route['admin/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/$2/$3/$4/$5';
$route['admin/(:any)/(:any)/(:any)/(:any)'] = '$1/$2/$3/$4';
$route['admin/(:any)/(:any)/(:any)'] = '$1/$2/$3';
$route['admin/(:any)/(:any)'] = '$1/$2';
$route['admin/(:any)'] = '$1';




//API router
$route['api/user'] = 'api/user';
$route['api/document'] = 'api/document';


$route['api/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api/$2/$3/$4/$5/$6/$7';
$route['api/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api/$2/$3/$4/$5/$6';
$route['api/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api/$2/$3/$4/$5';
$route['api/(:any)/(:any)/(:any)/(:any)'] = '$1/api/$2/$3/$4';
$route['api/(:any)/(:any)/(:any)'] = '$1/api/$2/$3';
$route['api/(:any)/(:any)'] = '$1/api/$2';
$route['api/(:any)'] = '$1/api';

$route['api-v2/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api_v2/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13';
$route['api-v2/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api_v2/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12';
$route['api-v2/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api_v2/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11';
$route['api-v2/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api_v2/$2/$3/$4/$5/$6/$7/$8/$9/$10';
$route['api-v2/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api_v2/$2/$3/$4/$5/$6/$7/$8/$9';
$route['api-v2/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api_v2/$2/$3/$4/$5/$6/$7/$8';
$route['api-v2/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api_v2/$2/$3/$4/$5/$6/$7';
$route['api-v2/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api_v2/$2/$3/$4/$5/$6';
$route['api-v2/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$1/api_v2/$2/$3/$4/$5';
$route['api-v2/(:any)/(:any)/(:any)/(:any)'] = '$1/api_v2/$2/$3/$4';
$route['api-v2/(:any)/(:any)/(:any)'] = '$1/api_v2/$2/$3';
$route['api-v2/(:any)/(:any)'] = '$1/api_v2/$2';
$route['api-v2/(:any)'] = '$1/api_v2';