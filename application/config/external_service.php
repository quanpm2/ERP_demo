<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

$config['external_service'] = [
    'adsInsight' => [
        'host' => getenv('ADS_INSIGHT_HOST'),
        'password_access' => getenv('ADS_INSIGHT_PASSWORD_ACCESS')
    ],
    'adsReport' => [
        'host' => getenv('ADS_REPORT_HOST'),
        'password_access' => getenv('ADS_REPORT_PASSWORD_ACCESS')
    ],
    'crawler' => [
        'host' => getenv('CRAWLER_HOST'),
        'password_access' => getenv('CRAWLER_PASSWORD_ACCESS')
    ],
    'adWarehouse' => [
        'host' => getenv('AD_WAREHOUSE_HOST'),
    ]
];

$config['api'] = [
    
    'adWarehouse' => getenv('ADWAREHOUSE_API'),

    'adsInsight' => [
        'googleads' => getenv('ADS_INSIGHT_GOOGLE_ADS_API'),
        'facebookads' => getenv('ADS_INSIGHT_FACEBOOK_ADS_API'),
        'tiktokads' => getenv('ADS_INSIGHT_TIKTOK_ADS_API'),
    ],

    'adsReport' => [
        'googleads' => getenv('ADS_REPORT_GOOGLE_ADS_API'),
        'facebookads' => getenv('ADS_REPORT_FACEBOOK_ADS_API'),
    ],

    'crawler' => [
        'tax_code' => getenv('CRAWLER_TAX_CODE_API'),
    ]
];
