<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['mail_template'] = array(
	'table_open' => '<table  border="0" cellpadding="4" cellspacing="0" style="font-size:12px; font-family:Arial" width="100%"><tr>
	<th colspan="15" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">',
	'thead_open'           => '</th></tr><thead>',
	'thead_close'           => '</thead>',

	'heading_row_start'     => '<tr>',
	'heading_row_end'       => '</tr>',
	'heading_cell_start'    => '<th style="border-bottom:1px dotted #999;     text-align: left;">',
	'heading_cell_end'      => '</th>',

	'tbody_open'            => '<tbody>',
	'tbody_close'           => '</tbody>',

	'row_start'             => '<tr>',
	'row_end'               => '</tr>',
	'cell_start'            => '<td style="border-bottom:1px dotted #999">',
	'cell_end'              => '</td>',

	'row_alt_start'         => '<tr>',
	'row_alt_end'           => '</tr>',
	'cell_alt_start'        => '<td style="border-bottom:1px dotted #999">',
	'cell_alt_end'          => '</td>',

	'table_close'           => '</table><br>'
	);