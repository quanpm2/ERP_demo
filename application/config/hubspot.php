<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
|  RabbitMQ AMQS COnfig
|--------------------------------------------------------------------------
| If Must be set if Money24h app integreated is enable
|
*/

$config['hubspot'] = array(
    'HUBSPOT_URL'      => getenv('HUBSPOT_API_URL', 'https://api.hubapi.com'),
    'HUBSPOT_HAPIKEY'  => getenv('HUBSPOT_HAPIKEY', ''),
);
