<?php
/**
 * Part of CI PHPUnit Test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 */

class Welcome_welcome_test extends TestCase
{
	public function test_index()
	{
		$output = $this->request('GET', 'welcome');
		$this->assertContains('<title>Welcome to CodeIgniter</title>', $output);
	}

	public function test_method_404()
	{
		$this->request('GET', 'welcome/metssshod_not_exist');
		$this->assertResponseCode(404);
	}
}
