<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @group contract-admin
 */
class Contract_test extends TestCase {

	public $CI = NULL;

	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		// $CI->load->library('Seeder');
	}

	public function setUp()
	{
		$this->resetInstance();
	}
}