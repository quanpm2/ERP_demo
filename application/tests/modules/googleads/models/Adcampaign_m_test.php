<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @group googleads-models
 */
class Adcampaign_m_test extends TestCase {

	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		$CI->load->library('Seeder');

		// $CI->seeder->call('visitorsessionsSeeder');
		// $CI->seeder->call('contractadwordSeeder');
	}

	public function setUp()
	{
		$this->resetInstance();
		$this->CI->load->model('googleads/adcampaign_m');
		$this->CI->load->model('termmeta_m');
		$this->adcampaign_m = $this->CI->adcampaign_m;
	}


	/**
	 * @dataProvider provide_mutateIpBlock
	 */
	public function test_mutateIpBlock($mcm_account_id,$adCampaignId,$ip_addresses)
	{
		$this->adcampaign_m->mutateIpBlock($mcm_account_id,$adCampaignId,$ip_addresses);
	}

	/**
	 * Data Provide : IPs blocked
	 */
	public function provide_mutateIpBlock()
	{
		return [
			['mcm_account_id' => 0, 'adCampaignId' => 0,'ip_addresses' => ['115.79.42.123']]
		];
	}
}
/* End of file Adcampaign_m.php */
/* Location: ./application/modules/googleads/models/Adcampaign_m.php */