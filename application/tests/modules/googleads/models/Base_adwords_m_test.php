<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @group adwords-api-models
 */
class Base_adwords_m_test extends TestCase {

	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		$CI->load->library('Seeder');

		$CI->load->model('usermeta_m');

		// $CI->seeder->call('visitorsessionsSeeder');
		// $CI->seeder->call('contractadwordSeeder');
	}

	public function setUp()
	{
		$this->resetInstance();
		$this->CI->load->model('googleads/base_adwords_m');
	}


	public function test_download_normal()
	{
		$adaccount_id = 1;
		$this->CI->base_adwords_m->set_mcm_account($adaccount_id);
		$result = $this->CI->base_adwords_m->download('KEYWORDS_PERFORMANCE_REPORT',strtotime('2017/11/01'),strtotime('2017/12/01'));
	}
}
/* End of file Adcampaign_m.php */
/* Location: ./application/modules/googleads/models/Adcampaign_m.php */