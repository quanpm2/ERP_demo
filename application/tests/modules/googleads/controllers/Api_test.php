<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @group googleads
 * @group tracking
 */
class Api_googleads_test extends TestCase {

	public $CI = NULL;

	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		$CI->load->library('Seeder');
		$CI->seeder->call('visitorsessionsSeeder');
		$CI->seeder->call('FraudsIpBlockedLogSeeder');
	}

	public function setUp()
	{
		$this->resetInstance();
		$this->CI->load->model('log_m');
	}

	/**
	 * @dataProvider provide_ipBlockListener_data
	 */
	public function test_ipBlockListener_when_receive_IP_risk_once($tracking_code,$ip,$expected_http_code)
	{
		$this->CI->load->config('googleads/adwords');
		$output = $this->request('POST','/api/googleads/ipBlockListener',['tracking_code'=>$tracking_code,'ip'=>$ip]);
		$response = json_decode($output);
		var_dump($response->msg);
		$this->assertResponseCode($expected_http_code);
	}

	/**
	 * Data provide
	 */
	public function provide_ipBlockListener_data()
	{
		return [
			['tracking_code'=>'','ip'=>'84.45.32.219','expected_http_code'=>406], # invalid POST DATA
			['tracking_code'=>'b28xxxxxxxxxd9bac25f','ip'=>'84.45.32.219','expected_http_code'=>406], # invalid Tracking code - term not exists
			['tracking_code'=>'b28ccd02269b9a569bc11882d9bac25f','ip'=>'115.79.42.123','expected_http_code'=>200], # invalid POST DATA
			['tracking_code'=>'b28ccd02269b9a569bc11882d9bac25f','ip'=>'115.79.42.123','expected_http_code'=>200], # IP đã đươc khóa trước đó
			['tracking_code'=>'b28ccd02269b9a569bc11882d9bac25f','ip'=>'84.45.0.0/16','expected_http_code'=>200], # insert log
		];
	}

	/**
	 * @dataProvider provide_logs_when_blocked_ip_data
	 */
	public function test_logs_when_blocked_ip($term_id, $expected)
	{
		$logs = $this->CI->log_m->get_many_by(['term_id' => $term_id,'log_type'=>'frauds_ip_blocked']);
		$this->assertEquals(count($logs), $expected);
	}

	/**
	 * Data provide
	 */
	public function provide_logs_when_blocked_ip_data()
	{
		return [
			['term_id' => 2, 'expected' => 2],
			['term_id' => 1, 'expected' => 0],
		];
	}


	/**
	 * @dataProvider provide_test_clearIpBlock_data
	 */
	public function test_clearIpBlock($term_id, $expected)
	{
		$output = $this->request('POST','/api/googleads/clearIpBlock', ['term_id'=>$term_id]);
		$response = json_decode($output);
		
		$this->assertEquals($response->status, $expected['status']);
		$this->assertEquals($response->msg, $expected['msg']);
		// $this->assertResponseCode($expected);
	}

	/**
	 * Data provide
	 */
	public function provide_test_clearIpBlock_data()
	{
		return [
			['term_id' => 0, 'expected' => ['msg'=>'Input parameter Invalid !','status'=>FALSE]],
			['term_id' => 1, 'expected' => ['msg'=>'Contract not found or deleted!','status'=>FALSE]],
			['term_id' => 2, 'expected' => ['msg'=>'Successfully.','status'=>TRUE]],
		];
	}


	/**
	 * @dataProvider provide_test_logs_after_remove_ip_blocked_data
	 */
	public function test_logs_after_remove_ip_blocked($term_id, $expected)
	{
		$logs = $this->CI->log_m->get_many_by(['term_id' => $term_id,'log_type'=>'api_googleads_clearIpBlock']);
		$this->assertEquals(count($logs), $expected);
	}

	/**
	 * Data provide
	 */
	public function provide_test_logs_after_remove_ip_blocked_data()
	{
		return [
			['term_id' => 1, 'expected' => 0],
			['term_id' => 2, 'expected' => 1],
		];
	}
}