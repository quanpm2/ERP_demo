<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @group googleads-tracking
 */
class Ajax_tracking_test extends TestCase {

	private $localCI = NULL;
	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		$CI->load->library('Seeder');
		// $CI->seeder->call('visitorsessionsSeeder');
		// $CI->seeder->call('FraudsIpBlockedLogSeeder');
	}

	public function setUp()
	{
		$this->resetInstance();
		$this->mdate = new mdate();


		/* Require Admin Logged */
		$this->CI->load->model('user_m');
		$this->CI->load->model('staffs/admin_m');
		$admin = $this->CI->admin_m->get(47);
		$admin->role_name = 'Administrator';
		$this->CI->admin_m->setLogin($admin);
	}

	/**
	 * @dataProvider provide_test_get_visitors
	 */
	public function test_get_visitors($input,$expected)
	{
		$output = $this->request('GET','admin/googleads/ajax/tracking/getVisitors?'.http_build_query($input));
		$response = json_decode($output);
		var_dump($response);
		$this->assertResponseCode($expected['http_code']);
	}
	/**
	 * Data provide
	 */
	public function provide_test_get_visitors()
	{
		return [
			array(
				'params' => [
					'term_id' => 2,
					'group_by' => 'date,status',
				],
				'expected' => ['http_code' => 200]
			), // case 1
		];
	}
}