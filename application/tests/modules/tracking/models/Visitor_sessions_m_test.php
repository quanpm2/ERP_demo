<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * @group tracking
 * @group models
 */
class Visitor_sessions_m_test extends TestCase {

	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		$CI->load->library('Seeder');
		$CI->seeder->call('visitorsessionsSeeder');
	}

	public function setUp()
	{
		$this->resetInstance();
		$this->CI->load->model('visitor_sessions_m');
		$this->obj = $this->CI->visitor_sessions_m;
	}

	public function test_when_you_get_all_visitors_sessions()
	{
		$result = $this->obj->get_all();
		$this->assertCount(4,$result);
	}
}
/* End of file Visitor_sessions_m_test.php */
/* Location: ./application/tests/modules/tracking/models/Visitor_sessions_m_test.php */