<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @group tracking
 */
class Api_tracking_test extends TestCase {

	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		$CI->load->library('Seeder');

		$CI->seeder->call('visitorsessionsSeeder');
		// $CI->seeder->call('contractadwordSeeder');
	}


	public function setUp()
	{
		$this->resetInstance();
	}

	/**
	 * @dataProvider provide_visitors_data
	 */
	public function test_visitors_api($tracking_code,$expected_count,$expected_http_code)
	{
		$_GET['tracking_code'] = $tracking_code;
		$output = $this->request('GET', '/api/tracking/visitors');

		$response = json_decode($output);

		$this->assertResponseCode($expected_http_code);
		$this->assertEquals(count($response->data),$expected_count);
	}

	/**
	 * Data provide
	 */
	public function provide_visitors_data()
	{
		return [
			['tracking_code'=>'b28ccd02269b9a569bc11882d9bac25f','expected_count'=>4,'expected_http_code'=>200],
			['tracking_code'=>'_leak_in_database_______________','expected_count'=>0,'expected_http_code'=>200],
			['tracking_code'=>'_404_ninja______________________','expected_count'=>0,'expected_http_code'=>200],
			['tracking_code'=>'','expected_count'=>0,'expected_http_code'=>200],
		];
	}
}
/* End of file Api_test.php */
/* Location: ./application/tests/modules/tracking/controllers/Api_test.php */