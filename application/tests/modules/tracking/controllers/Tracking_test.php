<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @group tracking_module_controller
 */
class Tracking_test extends TestCase {

	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		$CI->load->library('Seeder');
		$CI->seeder->call('visitorsessionsSeeder');
	}

	public function setUp()
	{
		$this->resetInstance();
		$this->CI->load->model('visitor_sessions_m');
		$this->obj = $this->CI->visitor_sessions_m;
	}

	public function test_when_client_access_ads_via_search_engine_once()
	{
		$params = array(
		    'lpurl' => 'https://adsplus.vn/',
		    'matchtype' => 'p',
		    'device' => 'c',
		    'campaignid' => '734919008',
		    'keyword' => 'adsplus',
		    'adposition' => '1t1',
		    'loc_interest_ms' => '',
		    'loc_physical_ms' => '1028581',
		    'placement' => '',
		    'network' => 'g',
		    'devicemodel' => '',
		    'creative' => '189647012628',
		    'aceid' => '',
		    'adgroupid' => '42388823270',
		    'feeditemid' => '',
		    'gclid' => 'EAIaIQobChMIxpjWrPay1wIVBA4rCh0yAAJVEAAYASAAEgIgu_D_BwE',
		    'tracking_code' => 'b28ccd02269b9a569bc11882d9bac25f'
		);

		$url = '/tracking/update?'.http_build_query($params);
		$this->request('GET', $url);
		$this->assertResponseCode(200);
	}

	public function test_when_visitor_tracked()
	{
		$result = $this->obj->get_all();
		$this->assertCount(5,$result);
	}
}