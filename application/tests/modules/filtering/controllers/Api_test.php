<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * @group controllers
	 */
class Api_filtering_test extends TestCase {

	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		$CI->load->library('Seeder');

		$CI->seeder->call('visitorsessionsSeeder');
		$CI->seeder->call('contractadwordSeeder');
	}


	public function setUp()
	{
		$this->resetInstance();
		$this->CI->load->model('visitor_sessions_m');
		$this->obj = $this->CI->visitor_sessions_m;
	}


	/**
	 * @dataProvider provide_detect_data
	 */
	public function test_detect($visitor_session_id,$expected)
	{
		$output 	= $this->request('GET', "api/filtering/detect/{$visitor_session_id}");
		$response 	= json_decode($output);

		$this->assertEquals($response->status, $expected->status);
		$this->assertEquals($response->data, $expected->data);
	}


	/**
	 * Data Provide : Detect risk IP
	 *
	 * @return     array  visitor_session_id - expected pairs
	 */
	public function provide_detect_data()
	{
		return array(

			array(1, (object) array(
					'status' => 1,
				    'msg' => 'blocked successfuly',
				    'data' => (object) array(
			            'risk' => 10,
			            'notify' => 1,
			            'ip' => '109.245.32.219',
			            'description' => 'IP by BEHAVIOR restricted !!!'
				    )
				)
			),
			array(2, (object) array(
					'status' => 1,
				    'msg' => 'blocked successfuly',
				    'data' => (object) array(
			            'risk' => 10,
			            'notify' => 1,
			            'ip' => '84.45.32.219',
			            'description' => 'IP by BEHAVIOR restricted !!!'
				    )
				)
			),
			array(5, (object) array(
					'status' => 0,
				    'msg' => 'The rules is undefined or unloaded',
				    'data' => array()
				)
			)
		);
	}
}
/* End of file Api_test.php */
/* Location: ./application/tests/modules/tracking/controllers/Api_test.php */