<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @group filtering
 * @group models
 */
class Filtering_m_test extends TestCase {

	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		$CI->load->library('Seeder');

		$CI->seeder->call('visitorsessionsSeeder');
		// $CI->seeder->call('contractadwordSeeder');
	}


	public function setUp()
	{
		$this->resetInstance();
		$this->CI->load->model('filtering/filtering_m');
		$this->obj = $this->CI->filtering_m;
	}

	/**
	 * @dataProvider provide_adcheck_data
	 */
	public function test_adCheck_with_ip_creative_max_durations($ip, $creative, $max, $durations, $expected)
	{
		$result = $this->obj->adCheck($ip,$creative,$max,$durations);
		$this->assertEquals($result, $expected);
	}
	public function provide_adcheck_data()
	{
		return array(
			['109.245.32.219','189647012628',3,500, TRUE],
			['84.45.32.219','189647012628',3,500, TRUE],
			['46.128.1.12','189647012628',3,500, TRUE],
		);
	}

	/**
	 * @dataProvider provide_adgroupCheck_data
	 */
	public function test_adgroupcheck_with_ip_adgroup_max_durations($ip, $creative, $max, $durations, $expected)
	{
		$result = $this->obj->adgroupCheck($ip,$creative,$max,$durations);
		$this->assertEquals($result, $expected);
	}
	public function provide_adgroupCheck_data()
	{
		return array(
			['109.245.32.219', '42388823270', 3, 500, TRUE],
			['84.45.32.219', '42388823270', 2, 500, FALSE],
			['46.128.1.12', '42388823270', 3, 500, TRUE],
		);
	}



	/**
	 * @dataProvider provide_adcampaignCheck_data
	 */
	public function test_adcampaignCheck_with_ip_campaign_max_durations($ip, $creative, $max, $durations, $expected)
	{
		$result = $this->obj->adcampaignCheck($ip,$creative,$max,$durations);
		$this->assertEquals($result, $expected);
	}
	public function provide_adcampaignCheck_data()
	{
		return array(
			['109.245.32.219', '734919008', 2, 500, TRUE],
			['84.45.32.219', '734919008', 2, 500, FALSE],
		);
	}


	/**
	 * @dataProvider provide_ipOctetsCheck_data
	 */
	public function test_ipOctetsCheck_with_trackingcode_ip_octet_max_durations($tracking_code,$ip,$octet,$max,$durations,$expected)
	{
		$result = $this->obj->ipOctetsCheck($tracking_code,$ip,$octet,$max,$durations);
		$this->assertEquals($result, $expected);
	}
	public function provide_ipOctetsCheck_data()
	{
		return array(
			['b28ccd02269b9a569bc11882d9bac25f', '84.45.32.219', 2, 4, 500, TRUE],
			['b28ccd02269b9a569bc11882d9bac25f', '84.45.32.219', 3, 2, 500, FALSE],
		);
	}


	/**
	 * @dataProvider provide_riskPlatformCheck_data
	 */
	public function test_riskPlatformCheck_with_trackingcode_ip_octet_max_durations($platform, $expected)
	{
		$result = $this->obj->riskPlatformCheck($platform);
		$this->assertEquals($result, $expected);
	}
	public function provide_riskPlatformCheck_data()
	{
		return array(
			['windows nt 10.0'	, TRUE],
			['windows nt 6.3'	, TRUE],
			['windows nt 6.2'	, TRUE],
			['windows nt 6.1'	, TRUE],
			['windows nt 6.0'	, TRUE],
			['windows nt 5.2'	, FALSE],
			['windows nt 5.1'	, TRUE],
			['windows nt 5.0'	, FALSE],
			['windows nt 4.0'	, FALSE],
			['winnt4.0'			, FALSE],
			['winnt 4.0'		, FALSE],
			['winnt'			, FALSE],
			['windows 98'		, FALSE],
			['win98'			, FALSE],
			['windows 95'		, FALSE],
			['win95'			, FALSE],
			['windows phone'	, TRUE],
			['windows'			, FALSE],
			['android'			, TRUE],
			['blackberry'		, TRUE],
			['iphone'			, TRUE],
			['ipad'				, TRUE],
			['ipod'				, TRUE],
			['os x'				, TRUE],
			['ppc mac'			, TRUE],
			['freebsd'			, FALSE],
			['ppc'				, TRUE],
			['linux'			, TRUE],
			['debian'			, FALSE],
			['sunos'			, FALSE],
			['beos'				, FALSE],
			['apachebench'		, FALSE],
			['aix'				, FALSE],
			['irix'				, FALSE],
			['osf'				, FALSE],
			['hp-ux'			, FALSE],
			['netbsd'			, FALSE],
			['bsdi'				, FALSE],
			['openbsd'			, FALSE],
			['gnu'				, FALSE],
			['unix'				, FALSE],
			['symbian' 			, FALSE]
		);
	}


	/**
	 * @dataProvider provide_riskBrowserCheck_data
	 */
	public function test_riskBrowserCheck_with_client_browser($browser, $expected)
	{
		$result = $this->obj->riskBrowserCheck($browser);
		$this->assertEquals($result, $expected);
	}
	public function provide_riskBrowserCheck_data()
	{
		return array(
			['OPR'				, TRUE],
			['Flock'			, FALSE],
			['Edge'				, FALSE],
			['Chrome'			, TRUE],
			['Opera.*?Version'	, TRUE],
			['Opera'			, TRUE],
			['MSIE'				, TRUE],
			['Internet Explorer', TRUE],
			['Trident.* rv'		, TRUE],
			['Shiira'			, FALSE],
			['Firefox'			, TRUE],
			['Chimera'			, FALSE],
			['Phoenix'			, FALSE],
			['Firebird'			, FALSE],
			['Camino'			, FALSE],
			['Netscape'			, FALSE],
			['OmniWeb'			, FALSE],
			['Safari'			, TRUE],
			['Mozilla'			, TRUE],
			['Konqueror'		, FALSE],
			['icab'				, FALSE],
			['Lynx'				, TRUE],
			['Links'			, FALSE],
			['hotjava'			, FALSE],
			['amaya'			, FALSE],
			['IBrowse'			, TRUE],
			['Maxthon'			, FALSE],
			['Ubuntu'			, FALSE],
		);
	}
}
/* End of file Filtering_m_test.php */
/* Location: ./application/tests/modules/Filtering/models/Filtering_m_test.php */