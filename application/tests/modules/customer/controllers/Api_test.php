<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @group customer-api
 */
class Api_test extends TestCase {

	public $CI = NULL;

	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		// $CI->load->library('Seeder');
	}

	public function setUp()
	{
		$this->resetInstance();
	}

	// public function test_find_and_renew_expired_api_user_by_customer()
	// {
	// 	$postdata = array(
	// 		'phone' => '0904875828',
	// 		'destroy_expired' => TRUE,
	// 		'deep_search' => TRUE,
	// 	);

	// 	$output 	= $this->request('POST','/api/customer/find', $postdata);
	// 	$response 	= json_decode($output);
	// 	$this->assertResponseCode(200);
	// }

	// public function test_find_after_renew_api_user_by_service_setting()
	// {
	// 	$postdata = array(
	// 		'phone' => '09013040141',
	// 		'destroy_expired' => TRUE,
	// 		'deep_search' => TRUE,
	// 	);

	// 	$output = $this->request('POST','/api/customer/find', $postdata);
	// 	$this->assertResponseCode(200);
	// }


	// /**
	//  * @dataProvider provide_login_data
	//  */
	// public function test_login($phone, $method, $password, $msg_expected)
	// {
	// 	/* Request token by phone number */
	// 	$find_ouput		= $this->request('POST','/api/customer/find', ['phone'=>$phone,'destroy_expired'=>TRUE,'deep_search'=>TRUE]);
	// 	$find_response 	= json_decode($find_ouput);
	// 	$token 			= $find_response->token;

	// 	/* LOGIN */
	// 	$main_output	= $this->request('POST',"/api/customer/login/{$method}?token={$token}", ['token'=>$token,'password'=>$password]);
	// 	$main_response	= json_decode($main_output);

	// 	if( ! empty($main_response) && is_object($main_response))
	// 	{
	// 		$this->assertEquals($main_response->msg, $msg_expected);
	// 	}
	// }

	// public function provide_login_data()
	// {
	// 	return array(
	// 		[
	// 			'phone' => '0904875828',
	// 			'method' => 'otp',
	// 			'password' => 'ABC123@123',
	// 			'msg_expected' => 'Login method is disabled ! Please email thonh@webdoctor.vn to request support.'
	// 		],
	// 		[
	// 			'phone' => '0904875828',
	// 			'method' => 'pwd',
	// 			'password' => 'adsplussieunhan',
	// 			'msg_expected' => 'Logged in'
	// 		],
	// 		[
	// 			'phone' => '09013040141',
	// 			'method' => 'pwd',
	// 			'password' => 'ABC1e23@123',
	// 			'msg_expected' => 'Incorrect Pwd.'
	// 		],
	// 	);
	// }

	/**
	 * @dataProvider provide_test_services_data
	 */
	public function test_services($phone)
	{
		/* Request token by phone number */
		// customer_id : 1391 | 1313
		$find_ouput		= $this->request('POST','/api/customer/find', ['phone'=>$phone,'destroy_expired'=>TRUE,'deep_search'=>TRUE]);
		$find_response 	= json_decode($find_ouput);
		$token 			= $find_response->token;

		$find_ouput		= $this->request('POST', "/api/customer/profile?token={$token}");
		var_dump($find_ouput);
		$find_response 	= json_decode($find_ouput);
	}

	public function provide_test_services_data()
	{
		return array(
			// ['phone' => '0905685555'],
			// ['phone' => '0966929929'],
			// ['phone' => '0945671428'],
			// ['phone' => '0916415757'],
			// ['phone' => '0909361681'],
			// ['phone' => ''],
			// ['phone' => '0982340354'],
			// ['phone' => '0903645333'],
			// ['phone' => '0937321113'],
			// ['phone' => '0902418742'],
			// ['phone' => '0914137699'],
			['phone' => '0967332266'],
		);
	}

	// /**
	//  * @dataProvider provide_token_data
	//  */
	// public function test_list_contract($token)
	// {
	// 	var_dump($token);
	// 	$find_ouput		= $this->request('POST', "/api/googleads/list?token={$token}");
	// 	var_dump($find_ouput);
	// 	$find_response 	= json_decode($find_ouput);
	// }

	// public function provide_token_data()
	// {
	// 	return array(
	// 		['token' => 'd721a7f6c4c1d877aafba96345e6e32ad4bb5e0ea2de756fc9b287d8a18ff3ab'],
	// 		['token' => '654987651316'],
	// 	);
	// }
}