<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @group helpers-date
 */
class MY_sdate_helper_test extends TestCase {

	public $CI = NULL;

	public static function setUpBeforeClass()
	{
		parent::setUpBeforeClass();

		$CI =& get_instance();
		$CI->load->helper('date');
	}

	public function setUp()
	{
		$this->resetInstance();
	}


	/**
	 * @dataProvider provide_get_work_days_data
	 */
	public function test_get_work_days($from, $to, $workSat, $expected)
	{
		$result = get_work_days($from,$to,$workSat);
		$this->assertEquals($result,$expected);
	}

	public function provide_get_work_days_data()
	{
		return array(
			['from' => '2017-11-13', 'to' => '2017-11-15', 'workSat' => TRUE, 'expected'=> 3],
			['from' => '2017-11-13', 'to' => '2017-11-17', 'workSat' => TRUE, 'expected'=> 6],
			['from' => '2017-11-13', 'to' => '2017-11-20', 'workSat' => TRUE, 'expected'=> 6],
			['from' => '2017-11-17', 'to' => '2017-11-19', 'workSat' => TRUE, 'expected'=> 1],
			['from' => '2017-11-17', 'to' => '2017-11-20', 'workSat' => TRUE, 'expected'=> 2],
		);
	}
}