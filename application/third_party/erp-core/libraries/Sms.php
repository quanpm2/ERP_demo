<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms
{
	protected $ci;
	protected $recipients = array();
	protected $config = array();
	protected $message = '';

	public function __construct(array $config = array())
	{
		$CI =& get_instance();
		$sms_config = $CI->config->load('sms/sms', TRUE);
		if(!$sms_config)
			$sms_config  = array();
		$this->config = array_merge($sms_config, $config);
		log_message('info', 'Sms Class Initialized');
	}

	public function to($recipients = array())
	{
		if(is_string($recipients))
			$recipients = explode(',', $recipients);
		if(is_array($recipients))
		{
			foreach($recipients as $recipient)
			{
				$this->recipients[] = trim($recipient);
			}
		}
		return $this;
	}

	public function message($message)
	{
		$this->message = rtrim(str_replace("\r", '', $message));
		return $this;
	}

	public function send($recipient = '', $msg = '', $is_fake_code = -1)
	{
		if(ENVIRONMENT !== 'production') $is_fake_code = 0;

		$result = array();
		if($msg)
		{
			$this->message($msg);
		}

		if($recipient)
		{
			$this->to($recipient);
		}

		$recipients = $this->recipients;
		if(!$recipients)
			return 'No recipients';
		foreach($recipients as $recipient)
		{
			if($is_fake_code == -1)
			{
				$response = $this->_send($this->message, $recipient);
			}
			else
			{
				$response = new stdClass();
				$response->SendSMSResult = $is_fake_code;
			}

			$result[] = array(
				'message' => $this->message,
				'recipient' => $recipient,
				'response' => $response,
				'config' => $this->config
				);
		}
		$this->clean();
		return $result;
	}

	//fake send sms
	public function fake_send($recipient = '', $msg = '', $fake_code = 0)
	{
		return $this->send($recipient, $msg, $fake_code);
	}

	public function get_status_label($status = 0)
	{
		return isset($this->config['sms_status'][$status]) ? $this->config['sms_status'][$status] : $this->config['sms_status']['not found'] ;
	}
	protected function _send($msg = '', $recipient = '')
	{
		$config = $this->config;
		
		$soap_client = new SoapClient($config['wsdl_url'], array('trace' => true, 'exceptions' => true));

		$params = array(    
			'account_name'      =>  $config['account_name'],
			'account_password'  =>  $config['account_password'],
			'Service_ID'        =>  $config['service_id'],
			'Command_Code'      =>  $config['command_code'],
			'Request_ID'        =>  $config['request_id'],
			'Message_Type'      =>  $config['message_type'],
			'Total_Message'     =>  $config['total_message'],
			'Message_Index'     =>  $config['message_index'],
			'IsMore'            =>  $config['is_more'],
			'Content_Type'      =>  $config['content_type'] );

		$params['Content'] = $msg;

		$params['User_ID'] = $this->phone_format($recipient);
		return $soap_client->SendSMS($params);
	}

	public function phone_format($number)
	{
		$number = ltrim($number, '0');
		$this->config['countrycode'] = empty($this->config['countrycode']) ? 84 : $this->config['countrycode'];

		if(strpos($this->config['countrycode'], substr($number, 0,2)) === false)
			$number = $this->config['countrycode'].$number;
		return $number;
	}

	public function clean()
	{
		$this->message = '';
		$this->recipients = array();
		return $this;
	}
}
/* End of file Sms.php */
/* Location: ./application/third_party/SMS/libraries/Sms.php */
