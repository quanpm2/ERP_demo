<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require_once (APPPATH . 'third_party/google-api/autoload.php');
// require_once (APPPATH . 'third_party/google-api/Client.php');

class Ga extends Google_Client {

	private $ci;
	private $config;
	private $analytic;
	private $client_id;
	private $service_account_name;
	private $key_file_location;
	private $profileId;
	private $results;

	function __construct($params = array()) {
		parent::__construct();
		$this->ci =& get_instance();
		$this->ci->config->load('google-analytics');
		$this->config = $this->ci->config->item('google_analytics');
		$this->client_id = $this->config['client_id'];
		$this->service_account_name = $this->config['service_account_name'];
		$this->key_file_location = $this->config['key_file_location'];

		$this->init();
	}

	function __get($method)
	{
		if(method_exists($this,$method))
		{
			return $this->$method();
		}
		else
		{
			return $this->analytic;
		}
	}

	function init()
	{
		$client =  new Google_Client();
		$client->setApplicationName($this->config['app_name']);
		$analytics = new Google_Service_Analytics($client);
		/*
		//Đọc nội dung từ file key và xác thực tài khoản GA
		*/
		$key = file_get_contents($this->key_file_location);
		$cred = new Google_Auth_AssertionCredentials(
			$this->service_account_name,
			array(
				'https://www.googleapis.com/auth/analytics',
				),
			$key,
			'notasecret'
			);
		$client->setAssertionCredentials($cred);
		if($client->getAuth()->isAccessTokenExpired()) {
			$client->getAuth()->refreshTokenWithAssertion($cred);
		}
		$_SESSION['service_token'] = $client->getAccessToken();

		$this->analytic =  $analytics;

	}

	public function getListAccount()
	{
		$results =  $this->analytic->management_accounts->listManagementAccounts();
		return ($results) ? $results->getItems() : null;
	}

	public function getListWebproperties($accountID = 0)
	{
		$results = $this->analytic->management_webproperties->listManagementWebproperties($accountID);
		return ($results) ? $results->getItems() : null;
	}

	public function getListProfiles($WebID = 0, $propertyId = 0)
	{
		$results = $this->analytic->management_profiles->listManagementProfiles($WebID, $propertyId);
		return ($results) ? $results->getItems() : null;
	}


	/**
	 * Get all Profiles
	 *
	 * @access public
	 * @return array data
	 */
	// public function getProfiles() {
		

	// }

	/**
	 * Get all WebProperties
	 *
	 * @access public
	 * @return array data
	 */
	// public function getWebProperties() {
		

	// }

	function get($profileId, $startDate, $endDate, $metrics, $optParams)
	{
		// $analytics = $this->analytic;
		// $accounts = $this->analytic->management_accounts->listManagementAccounts();
		 // $accountUserlinks = $this->analytic->management_accountUserLinks->listManagementAccountUserLinks('61328013');
		// $properties = $analytics->management_webproperties->listManagementWebproperties('61328013');
      // $x = 'UA-61328013-1';
       // $profiles = $analytics->management_profiles->listManagementProfiles('61328013', $x);
		// prd($profiles);
		
		$this->results = $this->analytic->data_ga->get($profileId, $startDate, $endDate, $metrics, $optParams);
		return $this;
	}

	function rows()
	{
		return $this->results('rows');
	}

	function columns()
	{
		return $this->results()->getColumnHeaders();
	}

	function results($type = '')
	{
		if($type != '')
			return $this->results->{$type};
		return $this->results;
	}
}