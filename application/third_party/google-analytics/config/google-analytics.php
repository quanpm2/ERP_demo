<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['google_analytics'] = array();
$config['google_analytics']['client_id'] = '3771302061c9b7c7263c678a8af80e82e0c2fbfc';
$config['google_analytics']['service_account_name'] = 'google-analytics@erp-webdoctor.iam.gserviceaccount.com';
$config['google_analytics']['key_file_location'] = APPPATH . 'third_party/google-analytics/Ga-903a432118ce.p12';
$config['google_analytics']['app_name'] = 'APP';