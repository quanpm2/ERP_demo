<?php  

if (!defined('BASEPATH')) exit('No direct script access allowed');

define('SRC_PATH', APPPATH.'/third_party/Adwords/src/');

define('LIB_PATH', 'Google/Api/Ads/AdWords/Lib');

define('UTIL_PATH', 'Google/Api/Ads/Common/Util');

define('AW_UTIL_PATH', 'Google/Api/Ads/AdWords/Util');

define('ADWORDS_VERSION', 'v201802');

ini_set('include_path', implode(array(ini_get('include_path'), PATH_SEPARATOR, SRC_PATH)));

require_once SRC_PATH.LIB_PATH. '/AdWordsUser.php';

require_once AW_UTIL_PATH . '/ReportUtils.php';

class Adwords extends AdWordsUser {

public function __construct() { 

  parent::__construct();
  }   

  public function lookup_location($atts = array('critetiaIds'=>array(),'locationName' => array())) {

  extract($atts);

  if(empty($atts) || (empty($critetiaIds) && empty($locationName))) return;

  $locationCriterionService = $this->GetService('LocationCriterionService', ADWORDS_VERSION);

  $selector = new Selector();

  $selector->fields = array('Id', 'LocationName', 'CanonicalName', 'DisplayType','ParentLocations','Reach','TargetingStatus');

  if(!empty($critetiaIds)){

  $selector->predicates[] = new Predicate('Id', 'IN', $critetiaIds);
  }

  if(!empty($LocationName)){

  $selector->predicates[] = new Predicate('LocationName', 'IN', $locationName);
  }

  $selector->predicates[] = new Predicate('Locale', 'EQUALS', 'vi');

  $locationCriteria = $locationCriterionService->get($selector);

  $out = array();

  if (isset($locationCriteria)) {

  foreach ($locationCriteria as $locationCriterion) {

  $out[$locationCriterion->location->id] = $locationCriterion->location->locationName;
  }
  }

  return $out;
  }
}