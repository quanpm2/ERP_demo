<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once (APPPATH . 'third_party/google-api/autoload.php');
require_once (APPPATH . 'third_party/google-api/Client.php');
require_once (APPPATH . 'third_party/google-api/Service/Gmail.php');


class Gmail extends Google_Client {

	private $ci;
	private $client;
	public $access_token_path;

	function __construct($params = array()) {
		parent::__construct();
		$this->ci =& get_instance();
		$this->ci->load->helper('file');
		$this->access_token_path = APPPATH . 'third_party/google-gmail/access_token.cache';
		$this->client = new Google_Client();
	}

	function init()
	{
		$client = $this->client;

		$this->ci->config->load('google-gmail');
		$config = $this->ci->config->item('google_gmail');

		$client->setApplicationName('Sign In with google account');

		$client->setClientId($config['client_id']);

		$client->setClientSecret($config['client_secret']);

		$client->setRedirectUri($config['redirect_uri']);
		return $client;
	}

	function aut()
	{
		$client = $this->init();

		$client->addScope(Google_Service_Gmail::GMAIL_READONLY);
		$client->addScope(Google_Service_Gmail::GMAIL_MODIFY);

		$client->setAccessType("offline");
		$client->setIncludeGrantedScopes(true);

		$token = $client->getAccessToken();

		if(!$token || $client->getAuth()->isAccessTokenExpired())
		{
			$tmp_token = read_file($this->access_token_path);

			if($tmp_token)
			{
				$tmp_token = json_decode($tmp_token);
				$client->refreshToken($tmp_token->refresh_token);
				$token = $client->getAccessToken();
				$client->setAccessToken($token);
				$_SESSION['service_token'] = $token;
				redirect(current_url(),'refresh');
			}
		}

		$this->objOAuthService = new Google_Service_Oauth2($client);

		$authUrl = $client->createAuthUrl();
		echo anchor($authUrl, 'linkname', 'attributes');

		if($this->ci->input->get('code'))
		{

			$client->authenticate($this->ci->input->get('code'));
			$access_token = $client->getAccessToken();

			if($access_token)
			{
				//lưu token
				write_file($this->access_token_path,$access_token);
				$_SESSION['service_token'] = $access_token;
				redirect(current_url(),'refresh');
			}
		}

	}

	function get_service()
	{
		if(isset($_SESSION['service_token']))
		{
			$client = $this->init();

			$token = $client->getAccessToken();

			if(!$token || $client->getAuth()->isAccessTokenExpired())
			{
				$tmp_token = read_file($this->access_token_path);

				if(!$tmp_token)
				{
					return $this->aut();
				}
				$tmp_token = json_decode($tmp_token);
				$client->refreshToken($tmp_token->refresh_token);
				$token = $client->getAccessToken();
				$client->setAccessToken($token);
				$_SESSION['service_token'] = $token;
			}

			return new Google_Service_Gmail($client);
			
		}
		else
		{
			return $this->aut();
		}
	}

	function get_subject($message)
	{
		$headers = $message->getPayload()->getHeaders();
		if(!$headers)
			return false;
		return $this->getHeader($headers, 'Subject');
	}

	/*
	
	$type: text/html, text/plain
	*/
	function get_body($message, $type = 'text/html')
	{
		$body = $this->get_body_1($message) OR 
		$body = $this->get_body_2($message) OR
		$body = $this->get_body_3($message, $type) OR
		$body = $this->get_body_4($message, $type) OR
		$body = '';
		$body = strtr($body,'-_', '+/');
		$body = base64_decode($body);
		return $body;
	}

	private function get_body_1($message)
	{
		$parts = $message->getPayload()->getParts();
		return $message->getPayload()->getBody()->getData();
	}

	private function get_body_2($message)
	{
		$parts = $message->getPayload()->getParts();
		$body = '';
		if($parts && isset($parts[0]))
		{
			return $body = $parts[0]->getBody()->getData(); 
		}
		return $body;
	}

	private function get_body_3($message, $type = '')
	{
		$parts = $message->getPayload()->getParts();
		$body = '';
		if($parts && isset($parts[0]))
		{
			foreach($parts[0]->getParts() as $part)
			{
				if($part->getMimeType() == $type)
				{
					$body = $part->getBody()->getData();
				}
			}
		}
		return $body;
	}

	private function get_body_4($message)
	{
		$parts = $message->getPayload()->getParts();
		$body = '';
		if($parts && isset($parts[0]))
		{
			foreach($parts[0]->getParts() as $part)
			{
				$body = $part->getBody()->getData(); 
				if($body)
					break;
			}
		}
		return $body;
	}

	function listMessagesUnRead($service)
	{
		return $this->listMessages($service, 'me', array('q'=>'is:unread'));
	}

	function makeMessagesIsRead($service, $messages_id)
	{
		return $this->modifyMessage($service, 'me',$messages_id,'',array('UNREAD'));
	}

	function makeMessagesUnRead($service, $messages_id)
	{
		return $this->modifyMessage($service, 'me',$messages_id,array('UNREAD'),'');
	}

	private function getHeader($headers, $name) {
		foreach($headers as $header) {
			if($header['name'] == $name) {
				return $header['value'];
			}
		}
	}
	/**
 * Get all Threads in the user's mailbox.
 *
 * @param  Google_Service_Gmail $service Authorized Gmail API instance.
 * @param  string $userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @return array Array of Threads.
 */
	function listThreads($service, $userId) {
		$threads = array();
		$pageToken = NULL;
		do {
			try {
				$opt_param = array();
				if ($pageToken) {
					$opt_param['pageToken'] = $pageToken;
				}
				$threadsResponse = $service->users_threads->listUsersThreads($userId, $opt_param);
				if ($threadsResponse->getThreads()) {
					$threads = array_merge($threads, $threadsResponse->getThreads());
					$pageToken = $threadsResponse->getNextPageToken();
				}
			} catch (Exception $e) {
				print 'An error occurred: ' . $e->getMessage();
				$pageToken = NULL;
			}
		} while ($pageToken);



		return $threads;
	}

	/**
 * Get list of Messages in user's mailbox.
 *
 * @param  Google_Service_Gmail $service Authorized Gmail API instance.
 * @param  string $userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @return array Array of Messages.
 */
	function listMessages($service, $userId, $opt_param = array()) {
		$pageToken = NULL;
		$messages = array();
		do {
			try {
				if ($pageToken) {
					$opt_param['pageToken'] = $pageToken;
				}
				$messagesResponse = $service->users_messages->listUsersMessages($userId, $opt_param);
				if ($messagesResponse->getMessages()) {
					$messages = array_merge($messages, $messagesResponse->getMessages());
					$pageToken = $messagesResponse->getNextPageToken();
				}
			} catch (Exception $e) {
				print 'An error occurred: ' . $e->getMessage();
			}
		} while ($pageToken);

		return $messages;
	}


	/**
	 * Modify the Labels a Message is associated with.
	 * @url https://developers.google.com/gmail/api/v1/reference/users/messages/modify#examples
	 * @param  Google_Service_Gmail $service Authorized Gmail API instance.
	 * @param  string $userId User's email address. The special value 'me'
	 * can be used to indicate the authenticated user.
	 * @param  string $messageId ID of Message to modify.
	 * @param  array $labelsToAdd Array of Labels to add.
	 * @param  array $labelsToRemove Array of Labels to remove.
	 * @return Google_Service_Gmail_Message Modified Message.
	 */
	function modifyMessage($service, $userId, $messageId, $labelsToAdd ='', $labelsToRemove ='') {
		$mods = new Google_Service_Gmail_ModifyMessageRequest();

		if(!empty($labelsToAdd))
			$mods->setAddLabelIds($labelsToAdd);
		if(!empty($labelsToRemove))
			$mods->setRemoveLabelIds($labelsToRemove);
		try {
			$message = $service->users_messages->modify($userId, $messageId, $mods);
			return $message;
		} catch (Exception $e) {
			print 'An error occurred: ' . $e->getMessage();
		}
	}

	/**
 * Modify the Labels applied to a Thread.
 *
 * @param  Google_Service_Gmail $service Authorized Gmail API instance.
 * @param  string $userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @param  string $threadId ID of Thread to modify.
 * @param  array $labelsToAdd Array of Labels to add.
 * @param  array $labelsToRemove Array of Labels to remove.
 * @return Google_Service_Gmail_Thread Modified Thread.
 */
	function modifyThread($service, $userId, $threadId, $labelsToAdd = '', $labelsToRemove ='') {
		$mods = new Google_Service_Gmail_ModifyThreadRequest();
		if(!empty($labelsToAdd))
			$mods->setAddLabelIds($labelsToAdd);
		if(!empty($labelsToRemove))
			$mods->setRemoveLabelIds($labelsToRemove);
		try {
			$thread = $service->users_threads->modify($userId, $threadId, $mods);
			print 'Thread with ID: ' . $threadId . ' successfully modified.';
			return $thread;
		} catch (Exception $e) {
			print 'An error occurred: ' . $e->getMessage();
		}
	}

	/**
 * Get Message with given ID.
 *
 * @param  Google_Service_Gmail $service Authorized Gmail API instance.
 * @param  string $userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @param  string $messageId ID of Message to get.
 * @return Google_Service_Gmail_Message Message retrieved.
 */
	function getMessage($service, $userId, $messageId, $opt = array()) {
		try {
			$message = $service->users_messages->get($userId, $messageId,$opt);
			return $message;
		} catch (Exception $e) {
			print 'An error occurred: ' . $e->getMessage();
		}
	}
}