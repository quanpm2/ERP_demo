<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$autoload['libraries'] = array('post_type');
$autoload['model'] = array('meta_m');

require_once(APPPATH.'third_party/core/libraries/' . 'Core.php');
require_once(APPPATH.'third_party/core/libraries/' . 'Package.php');
// require_once(APPPATH.'third_party/core/libraries/' . 'Widget.php');