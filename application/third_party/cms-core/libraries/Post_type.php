<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_type
{
	protected $ci;
	private $post_types = array();

	public function __construct()
	{
		$this->ci =& get_instance();
	}

	public function register( $post_type, $args = array())
	{
		if ( ! is_array( $this->post_types ) )
			$this->post_types = array();

		// Args prefixed with an underscore are reserved for internal use.
		$defaults = array(
			'labels'               => array(),
			'description'          => '',
			'public'               => false,
			'hierarchical'         => false,
			'exclude_from_search'  => null,
			'publicly_queryable'   => null,
			'show_ui'              => null,
			'show_in_menu'         => null,
			'show_in_nav_menus'    => null,
			'show_in_admin_bar'    => null,
			'menu_position'        => null,
			'menu_icon'            => null,
			'capability_type'      => 'post',
			'capabilities'         => array(),
			'map_meta_cap'         => null,
			'supports'             => array(),
			'register_meta_box_cb' => null,
			'taxonomies'           => array(),
			'has_archive'          => false,
			'rewrite'              => true,
			'query_var'            => true,
			'can_export'           => true,
			'delete_with_user'     => null,
			'_builtin'             => false,
			'_edit_link'           => 'post.php?post=%d',
			);
		$args = wp_parse_args( $args, $defaults );
		$args = (object) $args;
		if($args->taxonomies)
		{
			$this->ci->load->model('term_m');
			if(is_string($args->taxonomies))
				$args->taxonomies = explode(',', $args->taxonomies);

			foreach($args->taxonomies as $tax)
			{
				$this->ci->term_m->register_taxonomy($tax, $post_type);
			}
		}
		$this->post_types[$post_type] = $args;

		$this->ci->hook->do_action( 'registered_post_type', $post_type, $args );
	}

	public function posttype_exists( $post_type ) {
		return isset( $this->post_types[$post_type] );
	}

}
