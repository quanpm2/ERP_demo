<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');
class Core
{
	/**
	 * __get
	 *
	 * Allows classes to access 's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string
	 * @access private
	 */
	function __get($key)
	{
		// Method (under development)
		/*
		$B =& CI_Controller::get_instance();
		$this->$key =& $B->$key;
		return $this->$key;
		*/
		
		//CodeIgniter standard Method
		$B =& CI_Controller::get_instance();
		return $B->$key;		
	}
}