<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
/**
 * Package Class
 *
 *
 * @package		Base
 * @author		Nicholas Valbusa - info@squallstar.it - @squallstar
 * @copyright	Copyright (c) 2011-2014, Squallstar
 * @license		GNU/GPL (General Public License)
 * @link		http://squallstar.it
 *
 */

class Package extends Core
{
	function __construct()
	{
	}
	/**
	 * @var string Return the package title
	 */
	public function init(){}


	public function title(){}
	
	/**
	 * @var string Return the package description
	 */
	public function description(){}

	/**
	 * @var string Return the package version
	 */
	public function version(){}

	/**
	 * @var string Return the package author
	 */
	public function author(){}

	
	public function install()
	{

	}

	public function upgrade()
	{
		//Nothing to do
	}

	public function uninstall()
	{
		//Nothing to do
	}

	/**
	 * Additional operations to perform after the install
	 * @optional public function install();
	 */

	/**
	 * Additional operations to perform after the upgrade
	 * @optional public function upgrade();
	 */

	/**
	 * Additional operations to perform before the uninstall
	 * @optional public function uninstall();
	 */
}