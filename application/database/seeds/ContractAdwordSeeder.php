<?php
/**
 * Part of CI PHPUnit Test
 *
 * @author     Thọ Nguyễn <thonh@webdoctor.vn>
 * @license    MIT License
 * @copyright  2017 Thọ Nguyễn <thonh@webdoctor.vn>
 */

class ContractAdwordSeeder extends Seeder {

	private $table = 'visitor_sessions';
	protected $CI = NULL;

	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('termmeta_m');
		$this->CI->load->model('term_posts_m');
		$this->CI->load->model('googleads/googleads_m');
		$this->CI->load->model('googleads/mcm_account_m');
		$this->CI->load->model('googleads/adcampaign_m');
		$this->CI->load->model('googleads/mcm_account_clients_m');
	}

	public function run()
	{
		$this->CI->termmeta_m->truncate();
		$this->CI->term_posts_m->truncate();
		$this->CI->googleads_m->truncate();
		$this->CI->adcampaign_m->truncate();

		# Create new contract with type googleads
		$this->create_new_googleads_contract();
	}

	/**
	 * Creates a new samples googleads contract.
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	protected function create_new_googleads_contract()
	{
		# create mcm_account
		$account_data = array('term_name' => '7145073616','term_type'=>'mcm_account', 'term_parent'=>2242347530,'term_status'=>'active');
		$insert_account_id = $this->CI->mcm_account_m->insert($account_data);

		# mapping 'mcm_account' to 'adword-client';
		$default_clients = array(830,831,833,1402);
		$this->CI->mcm_account_clients_m->set_mcm_account_clients($insert_account_id, $default_clients);

		# mapping adcampaign
		$campaign_data = array('post_name'=>'Search-Brand','post_status'=>'enabled','post_type'=>'adcampaign','post_slug'=>'734919008');
		$insert_campaign_id = $this->CI->adcampaign_m->insert($campaign_data);
		$this->CI->term_posts_m->set_term_posts($insert_account_id, [$insert_campaign_id], 'adcampaign');

		# Create new term
		$term_data = array('term_name' => 'adsplus.vn','term_type'=>'google-ads', 'term_parent'=>0,'term_status'=>'publish');
		$insert_id = $this->CI->googleads_m->insert($term_data);
		if( ! $insert_id) return FALSE;
		
		$termmetas = array(
			'mcm_client_id' => $insert_account_id,
			'tracking_code' => 'b28ccd02269b9a569bc11882d9bac25f',
			'googleads-begin_time' => 1485882000,
			'googleads-end_time' => 0,
			'started_service' => 1485147606,
			'start_service_time' => 1485882000,
			'contract_curators'	=> 'a:3:{i:0;a:3:{s:4:"name";s:7:"testing";s:5:"phone";s:9:"999999999";s:5:"email";s:16:"trind@adsplus.vn";}i:1;a:3:{s:4:"name";s:20:"Thọ Nguyễn (dev)";s:5:"phone";s:11:"09013040141";s:5:"email";s:18:"thonh@webdoctor.vn";}i:2;a:3:{s:4:"name";s:0:"";s:5:"phone";s:0:"";s:5:"email";s:0:"";}}',

			'frauds_rules' => 'a:8:{s:2:"ad";a:3:{s:6:"active";b:1;s:10:"max_clicks";i:2;s:9:"durations";i:60;}s:7:"adgroup";a:3:{s:6:"active";b:1;s:10:"max_clicks";i:3;s:9:"durations";i:120;}s:10:"adcampaign";a:3:{s:6:"active";b:1;s:10:"max_clicks";i:5;s:9:"durations";i:360;}s:12:"ip_series_p3";a:3:{s:6:"active";b:0;s:10:"max_clicks";i:7;s:9:"durations";i:480;}s:12:"ip_series_p2";a:3:{s:6:"active";b:0;s:10:"max_clicks";i:9;s:9:"durations";i:600;}s:8:"behavior";a:4:{s:6:"active";b:1;s:16:"min_time_on_site";i:720;s:12:"max_sessions";i:12;s:9:"durations";i:780;}s:7:"devices";a:6:{s:6:"active";b:1;s:12:"browser_olds";b:1;s:18:"browser_undetected";b:1;s:16:"mouse_undetected";b:1;s:17:"screen_undetected";b:1;s:13:"os_undetected";b:1;}s:8:"recovery";s:1:"1";}',
		);

		foreach ($termmetas as $meta_key => $meta_value)
		{
			update_term_meta($insert_id,$meta_key,$meta_value);
		}

		return TRUE;
	}
}