<?php
/**
 * Part of CI PHPUnit Test
 *
 * @author     Thọ Nguyễn <thonh@webdoctor.vn>
 * @license    MIT License
 * @copyright  2017 Thọ Nguyễn <thonh@webdoctor.vn>
 */

class ApiUsersSeeder extends Seeder {
	
	protected $CI = NULL;

	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('api_user_m');
	}

	public function run()
	{
		$this->construct_api_user_data();
	}

	protected function construct_api_user_data()
	{
		$this->CI->api_user_m->truncate();	

		$api_users = $this->init_apiusers_sample();
		if(empty($api_users)) return FALSE;

		foreach ($api_users as $api_user)
		{
			$this->CI->api_user_m->insert($api_user);
		}
	}

	protected function init_apiusers_sample()
	{
		return  array(
			[
				// 'auser_id' => 651,
				'auser_phone' => '0904875828',
				'auser_token' => '0184c753f6cd70c861f63fb3461901936e9a974a520f09951bb5a790f4c3b19e',
				'auser_otp' => '123',
				'auser_status' => 'inactive',
				'created_on' => '1510020602',
				'user_id' => 1315,
				'auser_password' => '5228a2334b788f12c92bd23f9be875b6',	
				'auser_salt' => 'adsplus123'
			],
			// [
			// 	// 'auser_id' => 650,
			// 	'auser_phone' => '01679844152',
			// 	'auser_token' => '9c58075877560010a12737e8ea77432f824689e60251103ffd272a663a186656',
			// 	'auser_otp' => '123',
			// 	'auser_status' => 'inactive',
			// 	'created_on' => '1510020232',
			// 	'user_id' => 1327,
			// 	'auser_password' => '5228a2334b788f12c92bd23f9be875b6',	
			// 	'auser_salt' => 'adsplus123'
			// ],
			// [
			// 	// 'auser_id' => 649,
			// 	'auser_phone' => '0933384567',
			// 	'auser_token' => '2aba47f1ad0dc389700128b2247df0f463d56e8867f175bc92fb582979d268db',
			// 	'auser_otp' => '123',
			// 	'auser_status' => 'inactive',
			// 	'created_on' => '1509951935',
			// 	'user_id' => 1393,
			// 	'auser_password' => '5228a2334b788f12c92bd23f9be875b6',	
			// 	'auser_salt' => 'adsplus123'
			// ],
			// [
			// 	// 'auser_id' => 647,
			// 	'auser_phone' => '0918196122',
			// 	'auser_token' => '02f0b54c14237e19331476898448a8e2367341d35a956c69c46e7359b3c6b32d',
			// 	'auser_otp' => '123',
			// 	'auser_status' => 'inactive',
			// 	'created_on' => '1509937267',
			// 	'user_id' => 0,
			// 	'auser_password' => '5228a2334b788f12c92bd23f9be875b6',	
			// 	'auser_salt' => 'adsplus123'
			// ],
			// [
			// 	// 'auser_id' => 646,
			// 	'auser_phone' => '0909656901',
			// 	'auser_token' => 'cc524b84edd3495cbeaa853e4c7775d8e0b32744b7811b67c41396f00a9d9816',
			// 	'auser_otp' => '123',
			// 	'auser_status' => 'inactive',
			// 	'created_on' => '1509764960',
			// 	'user_id' => 1322,
			// 	'auser_password' => '5228a2334b788f12c92bd23f9be875b6',	
			// 	'auser_salt' => 'adsplus123'
			// ],
			[
				// 'auser_id' => 645,
				'auser_phone' => '09013040141',
				'auser_token' => 'c0703904904b29ad16fa97df554f01f4688beb9bd3037bed9cf30b25451a77c3',
				'auser_otp' => '123',
				'auser_status' => 'inactive',
				'created_on' => '1509761527',
				'user_id' => 0,
				'auser_password' => '5228a2334b788f12c92bd23f9be875b6',	
				'auser_salt' => 'adsplus123'
			],
		);
	}
}