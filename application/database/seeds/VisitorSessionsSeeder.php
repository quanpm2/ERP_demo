<?php
/**
 * Part of CI PHPUnit Test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 */

class VisitorSessionsSeeder extends Seeder {

	private $table = 'visitor_sessions';

	public function run()
	{
		$this->db->truncate($this->table);

		$visitor_sessions = $this->initVisitorSessions();
		if(empty($visitor_sessions)) return FALSE;

		foreach ($visitor_sessions as $visitor_session)
		{
			$this->db->insert($this->table,$visitor_session);
		}
	}

	protected function initVisitorSessions()
	{
		return  array(
			[
				'ip' => '109.245.32.219',
				'network' => 'g',
				'device' => 'computer',
				'loc_interest_ms' => '',
				'loc_physical_ms' => '1028581',
				'campaignid' => '734919008',
				'adgroupid' => '42388823270',
				'creative' => '189647012628',
				'targetid' => 0,
				'keyword' => 'adsplus',
				'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
				'resolution' => '',
				'browser' => 'Chrome',
				'is_browser' => 1,
				'is_mobile' => 0,
				'is_desktop' => 0,
				'is_robot' => 0,
				'created_on' => strtotime("-10 second"),
				'tracking_code' => 'b28ccd02269b9a569bc11882d9bac25f',
				'platform' => 'Windows 10',
				'version' => '62.0.3202.94'
			],
			[
				'ip' => '84.45.32.219',
				'network' => 'g',
				'device' => 'computer',
				'loc_interest_ms' => '',
				'loc_physical_ms' => '1028581',
				'campaignid' => '734919008',
				'adgroupid' => '42388823270',
				'creative' => '189647012628',
				'targetid' => 0,
				'keyword' => 'adsplus',
				'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
				'resolution' => '',
				'browser' => 'Chrome',
				'is_browser' => 1,
				'is_mobile' => 0,
				'is_desktop' => 0,
				'is_robot' => 0,
				'created_on' => strtotime("-20 second"),
				'tracking_code' => 'b28ccd02269b9a569bc11882d9bac25f',
				'platform' => 'Windows 10',
				'version' => '62.0.3202.94'
			],
			[
				'ip' => '84.45.32.219',
				'network' => 'g',
				'device' => 'computer',
				'loc_interest_ms' => '',
				'loc_physical_ms' => '1028581',
				'campaignid' => '734919008',
				'adgroupid' => '42388823270',
				'creative' => '189647012628',
				'targetid' => 0,
				'keyword' => 'adsplus',
				'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
				'resolution' => '',
				'browser' => 'Chrome',
				'is_browser' => 1,
				'is_mobile' => 0,
				'is_desktop' => 0,
				'is_robot' => 0,
				'created_on' => strtotime("-40 second"),
				'tracking_code' => 'b28ccd02269b9a569bc11882d9bac25f',
				'platform' => 'Windows 10',
				'version' => '62.0.3202.94'
			],
			[
				'ip' => '84.45.32.219',
				'network' => 'g',
				'device' => 'computer',
				'loc_interest_ms' => '',
				'loc_physical_ms' => '1028581',
				'campaignid' => '734919008',
				'adgroupid' => '42388823270',
				'creative' => '189647012628',
				'targetid' => 0,
				'keyword' => 'adsplus',
				'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
				'resolution' => '',
				'browser' => 'Chrome',
				'is_browser' => 1,
				'is_mobile' => 0,
				'is_desktop' => 0,
				'is_robot' => 0,
				'created_on' => strtotime("-60 second"),
				'tracking_code' => 'b28ccd02269b9a569bc11882d9bac25f',
				'platform' => 'Windows 10',
				'version' => '62.0.3202.94'
			]
		);
	}
}