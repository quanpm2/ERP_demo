<?php

/**
 * Part of api/googleads PHPUnit Test
 *
 * @author     Thọ Nguyễn <thonh@webdoctor.vn>
 * @license    MIT License
 * @copyright  2017 Thọ Nguyễn <thonh@webdoctor.vn>
 */

class FraudsIpBlockedLogSeeder extends Seeder {

	protected $CI = NULL;

	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('log_m');
	}

	public function run()
	{
		/* clear all blocked IP logs */
		$this->CI->log_m->delete_by(['log_type'=>'frauds_ip_blocked']);
		$this->CI->log_m->delete_by(['log_type'=>'api_googleads_clearIpBlock']);
	}
}