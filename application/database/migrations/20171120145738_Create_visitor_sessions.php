<?php
/**
 * Migration: Create_visitor_sessions
 *
 * Created by: thonh@webdoctor.vn
 * Created on: 2017/11/20 14:57:38
 */
class Migration_Create_visitor_sessions extends CI_Migration {

	public $primary_key = 'id';
	public $table = 'visitor_sessions';

	/**
	 * Upgrade migration
	 */
	public function up()
	{
		$this->dbforge->add_field($this->getFields());
		$this->dbforge->add_key($this->primary_key, TRUE);
		$this->dbforge->create_table($this->table);
	}


	/**
	 * Downgrade migration
	 */
	public function down()
	{
		$this->dbforge->drop_table($this->table);
	}


	/**
	 * Gets the visitor_sessions's fields.
	 *
	 * @return     array  The fields.
	 */
	protected function getFields()
	{
		return array(
			'id' 		=> ['type' => 'INT'		,'constraint' => 11,'auto_increment' => TRUE],
			'ip' 		=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'network' 	=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'device' 	=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],

			'loc_interest_ms' => ['type' => 'VARCHAR','constraint' => 255, 'null'=> TRUE],
			'loc_physical_ms' => ['type' => 'VARCHAR','constraint' => 255, 'null'=> TRUE],

			'campaignid'	=> ['type' => 'BIGINT'	,'constraint' => 20, 'null'=> TRUE],
			'adgroupid'		=> ['type' => 'BIGINT'	,'constraint' => 20, 'null'=> TRUE],
			'creative'		=> ['type' => 'BIGINT'	,'constraint' => 20, 'null'=> TRUE],
			'targetid'		=> ['type' => 'BIGINT'	,'constraint' => 20, 'null'=> TRUE],
			'keyword'		=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'user_agent'	=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'resolution' 	=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'browser' 		=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'is_browser' 	=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'is_mobile' 	=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'is_desktop' 	=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'is_robot' 		=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'created_on' 	=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'tracking_code' => ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'platform' 		=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
			'version' 		=> ['type' => 'VARCHAR'	,'constraint' => 255, 'null'=> TRUE],
		);
	}
}