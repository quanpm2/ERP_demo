<?php

namespace Crawler;

use Exception;

class Base{
    public $ci = NULL;

    protected $host = NULL;

    private $password_access = NULL;

    public function __construct()
    {
        $this->ci = &get_instance();
        $this->initClass();
    }

    private function initClass(){
        $this->ci->load->config('external_service');
        $crawler = $this->ci->config->item('crawler', 'external_service');
        $this->host = $crawler['host'];
        $this->password_access = $crawler['password_access'];
    }

    public function get($url, $filter = []){
        $default_filter = [
            'password' => $this->password_access,
        ];
        $filter = wp_parse_args($filter, $default_filter);
        $params = http_build_query($filter);

        $endpoint = $url . '?' . $params;

        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $endpoint);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
		curl_close($ch);

        if(isset($error_msg)){
            throw $error_msg;
        }

        try{
            $result = json_decode($result);
        }
        catch(Exception $e){
            throw $e->getMessage();
        }

        return $result;
    }
}