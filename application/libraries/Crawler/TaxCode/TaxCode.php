<?php

namespace Crawler\TaxCode;

use Crawler\Base;
use Exception;

class TaxCode extends Base{
    private $base_url = '';

    public function __construct()
    {
        parent::__construct();

        $api = $this->ci->config->item('crawler', 'api');
        $this->base_url = $api['tax_code'];
    }

    final public function find($tax_code){
        $filter = [
            'filters' => [
                'taxCode' => [
                    'operator' => '=',
                    'value' => $tax_code
                ]
            ]
        ];

        try{
            $data = parent::get($this->base_url, $filter);
        }
        catch(Exception $e){
            return FALSE;
        }

        if(empty($data) || 200 != $data->code){
            return FALSE;
        }

        $company = reset($data->data->data);

        return $company;
    }

    final public function list($filter = []){
        try{
            $data = parent::get($this->base_url, $filter);
        }
        catch(Exception $e){
            return FALSE;
        }

        if(empty($data) || 200 != $data->code){
            return FALSE;
        }

        return $data->data;
    }
}