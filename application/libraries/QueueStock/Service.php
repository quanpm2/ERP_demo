<?php
namespace QueueStock;

class Service
{
    public static function push($payload)
    {
    	$CI =&get_instance();
    	$CI->config->load('integrate_3rds');
        return \Requests::put($CI->config->item('uri', 'queueStockService'), [], $payload);
    }
}