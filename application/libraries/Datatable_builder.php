<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datatable_builder {

    private $ci;
    private $table;
    private $distinct;
    private $group_by       = array();
    public $select         = array();
    private $joins          = array();
    private $columns        = array();
    public  $where          = array();
    public  $having         = array();
    public  $limit          = array();
    private $or_where       = array();
    private $where_in       = array();
    private $or_where_in    = array();
    private $where_not_in   = array();
    private $like           = array();
    private $order_by       = array();
    private $filter         = array();
    private $add_columns    = array();
    private $edit_columns   = array();
    private $unset_columns  = array();
    private $option_columns = array();
    private $column_callback   = array();
    public $raw_query_mode  = false;
    private $add_search = array();
    private $add_action = array();
    private $paging_config   = array();
    private $table_template   = array();
    private $set_order  = TRUE;
    private $total_contract_value = array();
    public  $last_query = '';
    public  $filter_position = 1;

    public  $format = "HTML";

    public function __construct()
    {
        $this->ci =& get_instance();

        defined('FILTER_TOP_OUTTER') OR define('FILTER_TOP_OUTTER', 2);
        defined('FILTER_TOP_INNER') OR define('FILTER_TOP_INNER', 1);
    }

    /**
     * 
     * Set Output Format
     * 
     * @param string $format
     * 
     * @return [type]
     */
    public function setOutputFormat(string $format)
    {
        $this->format = $format;
        return $this;
    }

    private function get_display_result()
    {
        return $this->ci->db->get($this->table);
    }

    public function from($table, $custom = false)
    {
        $custom AND $this->raw_query_mode = true;
        $this->table = $table;
        return $this;
    }

    public function select($columns, $backtick_protect = TRUE)
    {
        foreach($this->explode(',', $columns) as $val)
        {
            $column = trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$2', $val));
            $column = preg_replace('/.*\.(.*)/i', '$1', $column); // get name after `.`
            if(!in_array($column, $this->columns))
                $this->columns[] =  $column;

            $this->select[$column] = $val;
            // $this->select[$column] =  trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$1', $val));
        }

        $this->ci->db->select($columns, $backtick_protect);
        return $this;
    }

    public function join($table, $fk, $type = NULL)
    {
        $this->joins[] = array($table, $fk, $type);
        // $this->ci->db->join($table, $fk, $type);
        return $this;
    }

    public function or_where($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->or_where[] = array($key_condition, $val, $backtick_protect);
        $this->ci->db->or_where($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function where($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->where[] = array($key_condition, $val, $backtick_protect);
        //$this->ci->db->where($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function limit($value, $offset = 0)
    {
        $this->limit = array($value, $offset);
        //$this->ci->db->where($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function group_by($val)
    {
        $this->group_by[] = $val;
        //$this->ci->db->group_by($val);
        return $this;
    }

    public function distinct($column)
    {
        $this->distinct = $column;
        $this->ci->db->distinct($column);
        return $this;
    }

    public function like($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->like[] = array($key_condition, $val, $backtick_protect);
        // $this->ci->db->like($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function or_like($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->or_like[] = array($key_condition, $val, $backtick_protect);
        $this->ci->db->or_like($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function order_by($key_condition, $val = NULL)
    {
        $this->order_by[] = array($key_condition, $val);
        return $this;
    }

    public function where_in($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->where_in[] = array($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function or_where_in($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->or_where_in[] = array($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function where_not_in($key_condition, $val, $backtick_protect = TRUE)
    {
        $this->where_not_in[] = array($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function having($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->having[] = array($key_condition, $val, $backtick_protect);
        return $this;
    }

    private function balanceChars($str, $open, $close)
    {
        $openCount = substr_count($str, $open);
        $closeCount = substr_count($str, $close);
        $retval = $openCount - $closeCount;
        return $retval;
    }


    private function explode($delimiter, $str, $open = '(', $close=')')
    {
        $retval = array();
        $hold = array();
        $balance = 0;
        $parts = explode($delimiter, $str);

        foreach($parts as $part)
        {
            $hold[] = $part;
            $balance += $this->balanceChars($part, $open, $close);
            if($balance < 1)
            {
                $retval[] = implode($delimiter, $hold);
                $hold = array();
                $balance = 0;
            }
        }

        if(count($hold) > 0)
        {
            $retval[] = implode($delimiter, $hold);
        }

        return $retval;
    }

    private function get_ordering()
    {
        return TRUE;
    }

    private function get_filtering()
    {
        
        if(!empty($this->filter)) foreach ($this->filter as $key => $value) 
        {
           
            if(empty($value))
            {

                $this->ci->db->where($key);

                continue;
            }

            $this->ci->db->like($key, $value);

        }
        
        return $this->ci->db;
    }

    public function add_filter($key, $value)
    {
        $this->filter[$key] = $value;
    }

    private function get_paging()
    {
        $page = $this->paging_config['cur_page'];

        $page = (isset($page) && $page >0) ? $page : 1;

        $per_page = (isset($this->paging_config['per_page']) ? $this->paging_config['per_page'] : 1);

        $iStart = ($page - 1) * $per_page;

        $iLength = $per_page;

        if($iLength != '' && $iLength > 0)
            $this->ci->db->limit($iLength, ($iStart)? $iStart : 0);
    }

    private function exec_replace($custom_val, $row_data)
    {
        $replace_string = '';

        if(isset($custom_val['replacement']) && is_array($custom_val['replacement']))
        {
            //Added this line because when the replacement has over 10 elements replaced the variable "$1" first by the "$10"
            $custom_val['replacement'] = array_reverse($custom_val['replacement'], true);

            foreach($custom_val['replacement'] as $key => $val)
            {
                $sval = preg_replace("/(?<!\w)([\'\"])(.*)\\1(?!\w)/i", '$2', trim($val));

                if(preg_match('/(\w+::\w+|\w+)\((.*)\)/i', $val, $matches) && is_callable($matches[1]))
                {
                    $func = $matches[1];

                    $args = preg_split("/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[,]+/", $matches[2], 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

                    foreach($args as $args_key => $args_val){

                        $args_val = preg_replace("/(?<!\w)([\'\"])(.*)\\1(?!\w)/i", '$2', trim($args_val));

                        $args[$args_key] = (in_array($args_val, $this->columns))? ($row_data[($this->check_cType())? $args_val : array_search($args_val, $this->columns)]) : $args_val;
                    }

                    $replace_string = call_user_func_array($func, $args);
                }
                elseif(in_array($sval, $this->columns)){

                    $replace_string = $row_data[($this->check_cType())? $sval : array_search($sval, $this->columns)];
                }
                else{

                    $replace_string = $sval;
                }

                $custom_val['content'] = str_ireplace('$' . ($key + 1), $replace_string, $custom_val['content']);
            }
        }
        return $custom_val['content'];
    }


    private function check_cType()
    {
        $column = $this->ci->input->post('columns');

        if(empty($column)) return true;

        if(is_numeric($column[0]['data'])) return FALSE;
        else return TRUE;
    }

    public function add_column($column, $options = array(), $content = NULL, $match_replacement = NULL, $row_attrs =array())
    {
        if(is_array($column)){

            $options = array_merge($column, $options);

            return $this->add_column($column['data'], $options, $content, $match_replacement, $row_attrs);
        }

        if(!is_array($options)){

            $title = $options;

            $options = array();

            $options['title'] = $title;
        }

        
        $column_v = trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$2', $column));

        if(empty($options['alias'])) $column_v =  preg_replace('/.*\.(.*)/i', '$1', $column_v); // get name after `.`
        else $column_v = $options['alias'];

        if(!empty($this->unset_columns) && (in_array($column_v,$this->unset_columns) || $column_v == 'action')) 
        {
            return $this;
        }

        $options['data'] = $column_v;

        $options['row_attrs'] = $row_attrs;

        $this->columns[] =  $column_v;

        if(!isset($options['set_select'])  || $options['set_select'] != FALSE){

        // $this->select[$column_v] =  trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$1', $column));
            $this->select[$column_v] =  trim($column);
            $this->select(trim($column));
        }

        unset($options['set_select']);

        if(!isset($options['set_order'])  || $options['set_order'] != FALSE){

            $options['row_attrs']['order'] = TRUE;
        }

        unset($options['set_order']);

        if($content !== NULL){

            $this->add_columns[$column] = array('content' => $content, 'replacement' => $this->explode(',', $match_replacement));
        }

        $this->option_columns[] = $options;

        return $this;
    }

    public function get_query($filtering = FALSE)
    {
        if($filtering) $this->get_filtering();
        if(!empty($this->joins))
        {
            $this->joins = arrayUnique($this->joins);
            foreach($this->joins as $val)
            {
                $this->ci->db->join($val[0], $val[1], $val[2]);
            }
        }

        foreach($this->where as $val)
        {
            $this->ci->db->where($val[0], $val[1], $val[2]);
        }

        foreach($this->or_where as $val)
        {
            $this->ci->db->or_where($val[0], $val[1], $val[2]);
        }

        foreach($this->where_in as $val)
        {
            $this->ci->db->where_in($val[0], $val[1], $val[2]);
        }

        foreach($this->or_where_in as $val)
        {
            $this->ci->db->or_where_in($val[0], $val[1], $val[2]);
        }

        foreach($this->where_not_in as $val)
        {
            $this->ci->db->where_not_in($val[0], $val[1], $val[2]);
        }

        foreach($this->group_by as $val)
        {
            $this->ci->db->group_by($val);
        }

        foreach($this->like as $val)
        {
            $this->ci->db->like($val[0], $val[1], $val[2]);
        }

        if(!empty($this->limit))
        {
            $this->ci->db->limit($this->limit[0], $this->limit[1]);
        }

        if(!empty($this->order_by))
        {
            $order_by = @array_unique($this->order_by, SORT_REGULAR);

            $has_joined = !empty($this->joins);

            foreach ($order_by as $item)
            {
                $criteria = $item[0];
                $order = $item[1];
                if(!$has_joined)
                {
                    $this->ci->db->order_by($criteria,$order);
                    continue;
                }

                // If order criteria is custom alias (SUM|COUNT|ETC.. WITH ALIAS IN SELECT)
                if(FALSE !== strpos($criteria,'[custom-select].'))
                {
                    $criteria = str_replace('[custom-select].', '', $criteria);
                    $this->ci->db->order_by($criteria,$order);
                    continue;
                }

                // Auto prepend alias table to order criteria
                if(FALSE === strpos($criteria,'.'))
                {
                    $this->ci->db->order_by("{$this->table}.{$criteria}",$order);
                    continue;
                }

                $this->ci->db->order_by($criteria,$order);
            }
        }

        if(!empty($this->having))
        {
            foreach($this->having as $val)
            {
                $this->ci->db->having($val[0], $val[1], $val[2]);
            }
        }

        if(strlen($this->distinct) > 0)
        {
            $this->ci->db->distinct($this->distinct);
            $this->ci->db->select($this->columns);
        }

        $result = null;

        if($this->raw_query_mode)
        {
            $this->ci->db->from("({$this->table}) as tmp");
            $result = $this->ci->db->query($this->ci->db->get_compiled_select());
        }
        else
        {
            $result = $this->ci->db->get($this->table, NULL, NULL, FALSE);
        }

        $this->last_query = $this->ci->db->last_query();

        return $result;
    }

    private function get_total_results($filtering = FALSE)
    {
        $result = $this->get_query($filtering);
        
        if(!$result) return 0;

        return $result->num_rows();
    }

    /**
     * Render datatable output data
     *
     * @param      array  $results  The results
     *
     * @return     array  The result values
     */
    public function produce_output($results)
    {
        $output  = array('headings' => $this->get_heading_components(), 'rows' => []);

        # RENDER DATA TABLE ROWS
        if(empty($this->columns) || empty($results)) return FALSE; // NO RESULT FOUND
        foreach($results as $result)
        {
            $data = $this->build_callback_cells($this->build_row($result));
            if( ! is_array($data)) continue;

            array_push($output['rows'], 'JSON' == $this->format ? $data : array_map(function($x) use($data) { return ($data[$x['data']] ?? ''); }, $this->option_columns));
        }

        return $output;
    }


    /**
     * Gets the heading components.
     *
     * @return     array  The heading components.
     */
    protected function get_heading_components()
    {
        if(empty($this->option_columns)) return array();

        $result         = array();
        $query_order_by = $this->ci->input->get('order_by'); // Sortable Field being pointed
        // ( $query_order_by && !$this->raw_query_mode ) AND $query_order_by = $this->set_default_order_by();

        $order_by       = $order    = ''; // [field-name] [asc|desc]
        if( ! empty($query_order_by))
        {
            $order      = key($query_order_by);
            $order_by   = end($query_order_by);
        }
        foreach ($this->option_columns as $col)
        {
            $col_name           = $col['data'];
            $result[$col_name]  = $col['title'];

            if( ! $this->set_order) continue;

            if(empty($col['row_attrs']['order'])) continue;
           
            $field      = str_replace('.','-', $col_name);
            $attrs      = ['name' => "order_by[{$field}]",'type' => 'button','class' => 'button-simulate btn-sortable','value' => 'desc'];
            $sort_icon  = '<i class="fa fa-fw fa-sort"></i>';

            if( FALSE !== strpos($order, $col['data']))
            {
                $sort_icon      = "<i class='fa fa-fw fa-sort-{$order_by}'></i>";
                $attrs['value'] = $order_by == 'asc' ? 'desc' : 'asc';
            }
            
            if(array_key_exists ( $field , $this->total_contract_value )){
                $total = currency_numberformat($this->total_contract_value[$field],' đ<br>');
                $sum_contract_value = "<span type='text' style=' padding-right: -27%; color:red'>{$total}</span>";
                $result[$col_name] = $sum_contract_value;
                $result[$col_name] .= form_button($attrs, "{$col['title']}{$sort_icon}");
            }else{
                $result[$col_name] = form_button($attrs, "{$col['title']}{$sort_icon}");
            }
        }
        return $result;
    }


    /**
    * Gets the filter row.
    *
    * @return     array  The filter row.
    */
    protected function get_filter_row()
    {
        $result = array();

        $option_columns = $this->option_columns;
        if(empty($option_columns)) return $result;

        // check filters is allowed in table
        if($this->filter_position != FILTER_TOP_INNER) return $result;

        $filter_components = $this->build_filter_components();
        if(empty($filter_components)) return $result;

        foreach($option_columns as $col)
        {
            $result[] = $filter_components[$col['data']] ?? ''; 
        }

        return $result;
    }


    /**
    * Render filter box when filter_possition is being set '2'
    *
    * @return     string  filter box HTML
    */
    protected function render_filter_box()
    {
        $html = '';

        $filter_components = $this->build_filter_components();
        if(empty($filter_components)) return $html;

        // Check filters box is allowed render
        if($this->filter_position != FILTER_TOP_OUTTER) return $filter_components;

        return array_map(function($x){ return "<div class='col-xs-2 form-group'>{$x}</div>" ;}, $filter_components);
    }

    /**
    * Builds filter components.
    *
    * @param      <type>  $out_of_table  The out of table
    *
    * @return     array   The filter components.
    */
    protected function build_filter_components($out_of_table = FALSE)
    {
        $result = array();
        $filters = $this->add_search;
        if(empty($filters)) return $result;

        foreach ($filters as $key => $filter)
        {
            if(!empty($filter['content']))
            {
                $result[$key] = $filter['content'];
                continue;
            }

            $field = strpos($key,'.') !== FALSE ? str_replace('.','-',$key) : $key;
            $name = "where[{$field}]";
            $value = $this->ci->input->get($name);
            $default = ['name' => $name,'value' => $value,'class'=>'form-control'];
            $filter['placeholder'] = $filter['placeholder'] ?? 'Search..';

            $pos = strpos($key,'.');
            if($pos !== FALSE)
            {
                $key = substr($key,($pos+1));
            }

            $result[$key] = form_input(array_merge($default, $filter));
        }

        return $result;
    }

    /**
    * Builds a row for table by db's data.
    *
    * @param      <type>  $result  The result
    *
    * @return     array   The row.
    */
    private function build_row($result)
    {
        $row = array();

        $columns = $this->columns;
        if(empty($columns)) return $row;

        $add_columns = $this->add_columns;

        foreach($columns as $col)
        {
            if(!empty($add_columns[$col]))
            {
                $row[$col] = $this->exec_replace($add_columns[$col],$result);
                continue;
            }

            if(empty($result[$col]))
            {
                $row[$col] = '';
                continue;
            }

            $row[$col] = $result[$col];
        }

        return $row;
    }

    /**
    * Builds callback cells.
    *
    * @param      <type>  $row    The row
    *
    * @return     <type>  The row.
    */
    private function build_callback_cells($row)
    {
        if(empty($row)) return $row;

        $column_callback = $this->column_callback;
        if(empty($column_callback)) return $row;

        foreach($row as $key => $val)
        {
            if(empty($column_callback[$key])) continue;

            foreach($column_callback[$key] as $callback)
            {
                $r = NULL;

                if($callback['row_data'] === TRUE)
                {
                    $r = $row[$key] = call_user_func_array($callback['function'], array($row[$key], $key, $callback['args']));
                }
                else
                {
                    $r = $row = call_user_func_array($callback['function'], array($row, $key, $callback['args']));
                }

                //if result == false then no process callback
                if($r === FALSE)
                    break 2;
            }
        }

        return $row;
    }

    private function set_default_order_by()
    {
        if( ! $this->set_order) return FALSE;

        $get_order_by = $this->ci->input->get('order_by');

        if(!empty($get_order_by) || empty($this->columns)) return FALSE;

        $primary_key = $this->ci->{$this->table.'_m'}->primary_key ?? FALSE;
        $primary_key = $primary_key ?: ($this->ci->{substr($this->table,0,-1).'_m'}->primary_key ?? FALSE);

        if($primary_key)
        {
            $field = "{$this->table}.{$primary_key}";
            $this->order_by($field, 'desc');
            return array($field=>'desc');
        }

        $primary_key = $this->ci->scache->get('primary_key-'.$this->table);
        if(!$primary_key)
        {
            $fields = $this->ci->db->field_data($this->table);

            foreach ($fields as $field)
            {
                if(empty($field->primary_key)) continue;

                $primary_key = $field->name;
                $this->ci->scache->write($primary_key, 'primary_key-'.$this->table);
                break;
            }
        }

        $field = "{$this->table}.{$primary_key}";
        $this->order_by($field, 'desc');
        return array($field=>'desc');
    }

    public function parse_relations_searches($args = array(),$replace = '.', $search = '-'){

        if(empty($args) || !is_array($args)) return FALSE;

        foreach($args as $condition => $params){

            if(!empty($params) && is_array($params)) foreach ($params as $field => $value) {

                if(!strpos($field, '-')) continue;

                $args[$condition][str_replace('-', '.', $field)] = $value;

                unset($args[$condition][$field]);
            }
        }

        return $args;
    }

    function generate_pagination($custom_config = array())
    {
        $this->ci->load->library('pagination');
        $this->ci->load->config('pagination');
        $total = $this->get_total_results(TRUE);
        
        $custom_config['cur_page'] = $custom_config['cur_page'] ?: 1;

        $default = array(
            'total_rows' => $total,
            'base_url'  => '#',
            'per_page' => $this->ci->pagination->per_page,
            // 'use_page_numbers' => FALSE,
            'attributes' => array('class' => 'pagination_anchor_navigator'),
            'enable_query_strings' => FALSE,
            'use_page_numbers' => TRUE,
            'full_tag_open' => '<ul class="pagination pagination-sm no-margin pull-right">',
            'full_tag_close' => '</ul>',
        );

        $config = array_merge($default,$custom_config);

        $this->paging_config = $config;
        $this->ci->pagination->initialize($config);
        
        $cur_page = ($this->ci->pagination->cur_page - 1 )  * $config['per_page'];
        $max_page = ($cur_page + $config['per_page']);
        $max_page = $max_page > $config['total_rows'] ? $config['total_rows'] : $max_page;

        $result = array(
            'html' => $this->ci->pagination->create_links(),
            'min' => (($this->ci->pagination->cur_page - 1) * $config['per_page']) + 1,
            'max' => $max_page,
            'total_rows' => $config['total_rows']
        );

        return $result;
    }

    public function add_search($column, $options = array(), $content = NULL, $match_replacement = NULL){

        if(is_array($column)){

            $options = array_merge($column, $options);

            return $this->add_search($column['data'], $options, $content, $match_replacement);
        }

        if(!is_array($options)){

            $title = $options;

            $options = array();

            $options['title'] = $title;
        }

        if($column == 'action')
        {
            $this->add_action[] = $options;
            return $this;
        }

        $this->add_search[$column] = $options;
        return $this;
    }


    public function generate($config_pagination = array(), $output = 'html', $charset = 'UTF-8')
    {
        'JSON' == $output AND $this->setOutputFormat('JSON');

        $data = array(
            'actions'       => !empty($this->add_action) ? implode('', array_map(function($x){ return $x['content']??''; }, $this->add_action)) : NULL,
            'filters'        => $this->render_filter_box(),
            'headings'      => $this->get_heading_components(),
            'pagination'    => $this->generate_pagination($config_pagination),
            'rows'          => []
        );

        $content = $this->generate_data();
        if(empty($content)) return $data;

        return array_merge($data, $content);
    }

    public function sum($table_join, $fk ,$column , $where =array(),$field,$type='')
    {   
        $this->ci->db->select($table_join .'.'.$column);
        $this->ci->db->join($table_join,$fk,$type);
        foreach ($where as $key => $value) {
            $this->ci->db->where($table_join .'.'.$key,$value);
        }
        $result = $this->get_query(TRUE);
        $sum_contract_value = 0;
        
        foreach ($result->result_array() as $row) {
            $sum_contract_value += (int)$row['meta_value'];
        }
        foreach ($field as $key => $value) {
            $field[$key] = $sum_contract_value;
        }
        
        $this->total_contract_value = $field;
    }

    public function generate_data()
    {
        $this->get_paging();

        $this->get_ordering();

        $this->get_filtering();

        // $this->set_default_order_by();
        $this->select(implode(',',$this->select));

        $result = $this->get_query(TRUE);
        if(!$result) return FALSE;

        $results = $result->result_array();
        return $this->produce_output($results);
    }

    public function get_columns(){

        return $this->columns;
    }

    public function get_option_columns(){

        return $this->option_columns;
    }

    public function add_column_callback($column, $function, $row_data = TRUE, $args = array()){

        if (is_callable($function)){

            if(is_array($column)) {

                foreach($column as $c) {

                    $this->add_column_callback($c,$function, $row_data);
                }
            }
            else{

                $this->column_callback[$column][] = array('function'=>$function, 'row_data' =>$row_data, 'args' =>$args);
            }
        }

        return $this;
    }

    function set_table_template($template){

        $this->table_template = $template;

        return $this;
    }

    function set_ordering($condition = FALSE){
        $this->set_order = is_bool($condition) ? $condition : TRUE;
        return $this;
    }

    /**
    * Unset column
    *
    * @param string $column
    * @return mixed
    */
    public function unset_column($column)
    {
        $this->unset_columns[] = $column;
        return $this;
    }

    public function set_filter_position($position = FILTER_TOP_INNER)
    {
        $this->filter_position = $position;
        return $this;
    }


    public function last_query()
    {
        return $this->last_query;
    }
}
/* End of file Admin_ui.php */
/* Location: ./application/libraries/Admin_ui.php */