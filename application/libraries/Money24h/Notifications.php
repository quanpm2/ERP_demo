<?php
namespace Money24h;

use QueueStock\Service;

class Notifications
{
    protected $base 			= null;
	protected $username 		= null;
	protected $pwd 				= null;
	protected $client_oauth_key = null;

    protected $webhook          = null;

    /**
     * Constructs a new instance.
     *
     * @param      <type>  $endpoint  The endpoint
     * @param      <type>  $token     The token
     */
    function __construct()  
    {
        $CI =&get_instance();
        $CI->load->config('integrate_3rds');

        $this->base 			= $CI->config->item('base', 'money24h');
        $this->username 		= $CI->config->item('username', 'money24h');
        $this->pwd 				= $CI->config->item('pwd', 'money24h');
        $this->client_oauth_key = $CI->config->item('client_oauth_key', 'money24h');
    }

    public function send($heading, $content, $phone = null, $os = null)
    {
        try
        {
            $response = Service::push(array(
                'uri'       => "{$this->base}/web-hook/ads-plus/push",
                'method'    => 'POST',
                'body'      => array(
                    'content'   => $content,
                    'heading'   => $heading,
                    'phone'     => $phone,
                    'os'        => $os
                ),
                'headers'   => $this->getHeader(),
                'webhook'   => $this->webhook
            ));
        }
        catch (Exception $e)
        {
            return false;    
        }

        return true;
    }

    protected function getHeader()
    {
    	$autoKey 		= time();
    	$accessTokenApi = md5("{$this->client_oauth_key}.{$autoKey}." . md5($this->pwd));

		return [
			'auto_key' 			=> $autoKey,
			'user_name' 		=> $this->username,
			'client_oauth_key'	=> $this->client_oauth_key,
			'token_access_api'	=> $accessTokenApi
		];
    }

    public function setWebhook($webhook)
    {
        $this->webhook = $webhook;
    }
}