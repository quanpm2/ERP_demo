<?php

namespace AdsService\AdsReport;

use Exception;

class Base{
    public $ci = NULL;

    protected $host = NULL;

    private $password_access = NULL;

    public function __construct()
    {
        $this->ci = &get_instance();
        $this->initClass();
    }

    private function initClass(){
        $this->ci->load->config('external_service');
        $adsReport = $this->ci->config->item('adsReport', 'external_service');
        $this->host = $adsReport['host'];
        $this->password_access = $adsReport['password_access'];
    }

    public function put($url, $data = []){
        $default_data = [
            'password' => $this->password_access,
        ];
        $data = wp_parse_args($data, $default_data);
        $data_json = json_encode($data);

        $headers = [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_json),
            'x-password-access: ' . $this->password_access
        ];

        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
		curl_close($ch);

        if(isset($error_msg) && !empty($error_msg)){
            throw new Exception($error_msg);
        }

        try{
            $result = json_decode($result);
        }
        catch(Exception $e){
            throw $e;
        }

        return $result;
    }

    public function get($url, $params = []){
        $url = $url . '?' . http_build_query($params);

        $headers = [
            'Content-Type: application/json',
            'x-password-access: ' . $this->password_access
        ];

        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
		curl_close($ch);

        if(isset($error_msg) && !empty($error_msg)){
            throw new Exception($error_msg);
        }

        try{
            $result = json_decode($result, TRUE);
        }
        catch(Exception $e){
            throw $e;
        }

        return $result;
    }
    
    /**
     * download
     *
     * @param  mixed $url
     * @return void
     */
    public function download($url, $file_name)
    {
        $tmp_file = 'tmp/' . $file_name;

        $headers = [
            'x-password-access: ' . $this->password_access
        ];
        
        $fp = fopen (FCPATH . $tmp_file, 'w+');
        $ch = curl_init(str_replace(" ","%20", $url));
        curl_setopt($ch, CURLOPT_TIMEOUT, 600);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FILE, $fp); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
        curl_close($ch);
        fclose($fp);

        if(isset($error_msg) && !empty($error_msg)){
            throw new Exception($error_msg);
        }

        return $tmp_file;
    }
}
