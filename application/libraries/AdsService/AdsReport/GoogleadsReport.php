<?php

namespace AdsService\AdsReport;

use AdsService\AdsReport\Base;
use Exception;

class GoogleadsReport extends Base{
    private $base_url = '';

    public function __construct()
    {
        parent::__construct();

        $api = $this->ci->config->item('adsReport', 'api');
        $this->base_url = $api['googleads'];
    }
    
    /**
     * get_report_files
     *
     * @param  int|string $contract_id
     * @return array|boolean
     */
    final public function get_report_files($contract_id)
    {
        $url = $this->base_url . "/report/file/$contract_id";

        try{
            $data = parent::get($url);
        }
        catch(Exception $e){
            return FALSE;
        }

        if(empty($data) || 200 != $data['code']){
            return FALSE;
        }
        
        return $data['data'];
    }
    
    /**
     * generate_report_file
     *
     * @param  int|string $contract_id
     * @param  array $params
     * @return array|boolean
     */
    final public function generate_report_file($contract_id, $params = [])
    {
        $url = $this->base_url . "/report/create/$contract_id";

        try{
            $data = parent::get($url, $params);
        }
        catch(Exception $e){
            return FALSE;
        }

        if(empty($data) || 200 != $data['code']){
            return FALSE;
        }
        
        return $data['data'];
    }
        
    /**
     * download_report_file
     *
     * @param  int|string $contract_id
     * @param  string $file_name
     * @return void
     */
    final public function download_report_file($contract_id, $file_name)
    {
        $url = $this->base_url . "/report/download/$contract_id?file_name=$file_name";

        try{
            $file_path = parent::download($url, $file_name);
        }
        catch(Exception $e){
            return FALSE;
        }
        
        return $file_path;
    }
}
