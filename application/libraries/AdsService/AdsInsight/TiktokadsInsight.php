<?php

namespace AdsService\AdsInsight;

use AdsService\AdsInsight\Base;
use Exception;

class TiktokadsInsight extends Base
{
    private $base_url = '';

    public function __construct()
    {
        parent::__construct();

        $api = $this->ci->config->item('adsInsight', 'api');
        $this->base_url = $api['tiktokads'];
    }

    final public function syncInsight($contract_id)
    {
        $url = $this->base_url . "/sync/$contract_id";

        try {
            $data = parent::put($url);
        } catch (Exception $e) {
            return FALSE;
        }

        if (200 != $data->code) {
            return FALSE;
        }

        return TRUE;
    }
}
