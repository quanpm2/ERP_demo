<?php

namespace AdsService\Behaviours;

class TiktokAdsFactory
{

    public static $CI       = null;
    public static $contract = null;
    public static $contractId = null;

    public static function build($idOrObject)
    {
        self::$CI =&get_instance();
        $contract = self::$CI->contract_m->set_term_type()->get($idOrObject);

        $service_type                   = get_term_meta_value($contract->term_id, 'service_type') ?: 'account_type';
        'account_rent_type' == $service_type AND $service_type = 'rent';

        $contract_budget_payment_type   = get_term_meta_value($contract->term_id, 'contract_budget_payment_type') ?: 'normal';

        $isAccountForRent               = (bool) get_term_meta_value($contract->term_id, 'isAccountForRent');
        $isAccountForRent AND $service_type = 'rent';

        $enums = [
            'account_type' => [
                'normal' => __NAMESPACE__. '\\Tiktokads\\AccountManagement\\FullVat',
                'customer' => __NAMESPACE__. '\\Tiktokads\\AccountManagement\\CustomerPayment',
            ],
            'rent' => [
                'normal' => __NAMESPACE__. '\\Tiktokads\\AccountRent\\FullVat',
                'customer' => __NAMESPACE__. '\\Tiktokads\\AccountRent\\CustomerPayment',
            ]
        ];

        $decorator = $enums[$service_type][$contract_budget_payment_type];
        return new $decorator($contract);
    }
}