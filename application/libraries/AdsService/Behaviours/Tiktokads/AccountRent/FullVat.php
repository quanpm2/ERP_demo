<?php

namespace AdsService\Behaviours\Tiktokads\AccountRent;

class FullVat extends \AdsService\Behaviours\Tiktokads\Base
{
    /**
     * Constructs a new instance.
     *
     * @param      <type>  $endpoint  The endpoint
     * @param      <type>  $token     The token
     */
    function __construct($idOrContract)  
    {
        parent::__construct($idOrContract);
    }

    /**
     * Prepare the contract data for printable version
     *
     * @return     Array  The result data
     */
    public function prepare_preview()
    {
        parent::prepare_preview();
        $this->printableData['data_service'] = array_merge($this->printableData['data_service'], [
            'service_name'  => 'Cho thuê tài khoản',
            'isAccountForRent' => (int) get_term_meta_value($this->contractId, 'isAccountForRent')
        ]);

        $this->printableData['view_file']           = "tiktokads/contract/preview/account_rent_fullvat";

        return $this->printableData;
    }
}