<?php

namespace AdsService\Behaviours\Tiktokads\AccountRent;

class CustomerPayment extends Base
{
    use \AdsService\Behaviours\Tiktokads\CustomerPaymentTrait;
    
    /**
     * Constructs a new instance.
     *
     * @param      <type>  $endpoint  The endpoint
     * @param      <type>  $token     The token
     */
    function __construct($idOrContract)  
    {
        parent::__construct($idOrContract);
    }

    /**
     * Prepare the contract data for printable version
     *
     * @return     Array  The result data
     */
    public function prepare_preview()
    {
        parent::prepare_preview();

        $contract_id = $this->contractId;

        $this->printableData['data_service'] = array(
			'service_name' 	=> 'Cho thuê tài khoản',
			'budget'		=> (int) get_term_meta_value($contract_id, 'contract_budget'),
			'currency' 		=> get_term_meta_value($contract_id, 'currency'),
			'service_fee' 	=> (int) get_term_meta_value($contract_id, 'service_fee'),
            'isAccountForRent' => (int) get_term_meta_value($contract_id, 'isAccountForRent')
		);

		$discount_amount 	= (double) get_term_meta_value($contract_id, 'discount_amount');
		$promotions 		= get_term_meta_value($contract_id, 'promotions') ?: [];

		if( !empty($discount_amount) && ! empty($promotions))
		{
			$promotions = unserialize($promotions);

			$this->printableData['data_service']['discount_amount'] = $discount_amount;
			$this->printableData['data_service']['promotions'] = array_map(function($promotion) {

				$promotion['text'] 	= "CTKM - {$promotion['name']}";
				$_value 	= $promotion['value'];

				if( 'percentage' == $promotion['unit'])
				{
					$_value = $this->printableData['data_service']['service_fee'] * div($promotion['value'], 100);
					$promotion['text'].= "({$promotion['value']}% phí dịch vụ)";
				}

				$promotion['amount'] = $_value;
				return $promotion;
			}, $promotions);
		}

		$this->printableData['contract_budget_customer_payment_type'] = (string) get_term_meta_value($contract_id, 'contract_budget_customer_payment_type');
		$this->printableData['deposit_amount'] = (int) get_term_meta_value($contract_id, 'deposit_amount');
		$this->printableData['has_deposit'] 	= ! empty($this->printableData['deposit_amount']);

		$this->printableData['view_file'] = 'tiktokads/contract/preview/account_rent_customer_payment';
		$this->printableData['has_deposit'] AND $this->printableData['view_file'] = 'tiktokads/contract/preview/rent_account_behaft_with_deposit';

        return $this->printableData;
    }
}