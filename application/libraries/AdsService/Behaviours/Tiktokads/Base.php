<?php

namespace AdsService\Behaviours\Tiktokads;

use \AdsService\Behaviours\Base as Behaviours;

class Base extends Behaviours
{
    public $term_type   = 'tiktok-ads';

    const FCT_TAX               = 0.05;
    const FCT_PERCENTAGE_MIN    = 0;
    const FCT_PERCENTAGE_MAX    = 0.05;

    /**
     * Constructs a new instance.
     *
     * @param      <type>  $endpoint  The endpoint
     * @param      <type>  $token     The token
     */
    function __construct($idOrContract)  
    {
        parent::__construct($idOrContract);

        $this->CI->load->model('term_posts_m');
        $this->CI->load->model('tiktokads/adaccount_m');
        $this->CI->load->model('tiktokads/tiktokads_m');
        
        $this->CI->load->config('tiktokads/tiktokads');
        $this->CI->load->config('tiktokads/contract');
        
        $this->CI->load->helper('date');
    }

    /**
     * Prepare the contract data for printable version
     *
     * @return     Array  The result data
     */
    public function prepare_preview()
    {
        parent::prepare_preview();

        $this->printableData['data_service'] = array(
            'service_name'  => 'Quản lý tài khoản',
            'budget'        => (int) get_term_meta_value($this->contractId, 'contract_budget'),
            'service_fee'   => (int) get_term_meta_value($this->contractId, 'service_fee')
        );

        /* Chi tiết các đợt thanh toán */
        $invoices = $this->CI->term_posts_m->get_term_posts($this->contractId, $this->CI->invoice_m->post_type, ['orderby'=>'posts.end_date']) ?: [];
        $number_of_payments = count($invoices);
        $payments_stack     = [];

        switch (TRUE)
        {
            case $number_of_payments == 1:
                $payments_stack[] = "<p>Bên A thanh toán cho Bên B 100% giá trị vào ngày hai bên ký kết hợp đồng này.</p>";
                break;
            
            default:
                $payments_stack[] = "<p>Bên A thanh toán cho Bên B làm {$number_of_payments} đợt:</p>";
                $payments_stack[] = '<ul>';

                $i      = 1;
                $_vat   = $this->printableData['vat'];

                foreach ($invoices as $inv)
                {
                    if( ! empty($fct))
                    {
                        
                    }

                    $_total     = numberformat(cal_vat($this->CI->invoice_item_m->get_total($inv->post_id, 'total'), $_vat), 0, '.', '');
                    $_total_f   = currency_numberformat($_total,'đ');
                    $_total_w   = currency_number_to_words($_total);
                    $_end_date  = my_date($inv->end_date,'d/m/Y');

                    $payments_stack[] = '<li> Đợt '.($i++).": thanh toán {$_total_f} ($_total_w), chậm nhất là ngày {$_end_date}.</li>";
                }

                $payments_stack[] = '</ul>';
                break;
        }
        
        $this->printableData['fct']      = (int) get_term_meta_value($this->contractId, 'fct'); // % thuế nhà thầu (đánh trên ngân sách quảng cáo)

        $discount_amount = (double) get_term_meta_value($this->contractId, 'discount_amount');

        $promotions = get_term_meta_value($this->contractId, 'promotions') ?: [];

        if( !empty($discount_amount) && ! empty($promotions))
        {

            $promotions = unserialize($promotions);

            $this->printableData['data_service']['discount_amount'] = $discount_amount;
            $this->printableData['data_service']['promotions'] = array_map(function($promotion) {

                $promotion['text']  = "CTKM - {$promotion['name']}";
                $_value     = $promotion['value'];

                if( 'percentage' == $promotion['unit'])
                {
                    $_value = $this->printableData['data_service']['service_fee'] * div($promotion['value'], 100);
                    $promotion['text'].= "({$promotion['value']}% phí dịch vụ)";
                }

                $promotion['amount'] = $_value;
                return $promotion;
            }, $promotions);
        }

        $this->printableData['payments'] = implode('', $payments_stack);

        /* BEGIN::CONTRACT CURATORS */
        $contract_curators = get_term_meta_value($this->contractId, 'contract_curators');
        $contract_curators = is_serialized($contract_curators) ? unserialize($contract_curators) : [];
        if( ! empty($contract_curators)) $this->printableData['contract_curators'] = $contract_curators;
        /* END::CONTRACT CURATORS */

        return $this->printableData;
    }

    /**
     * Calculates the actual service fee rate.
     *
     * @param      int   $budget  The budget
     *
     * @return     bool  The service fee rate.
     */
    public function calc_service_fee_rate_actual()
	{
		$discount_amount 	= (double) $this->calc_disacount_amount();
		$service_fee 		= (double) get_term_meta_value($this->contractId, 'service_fee');
		return div(max([$service_fee - $discount_amount, 0]), $this->calc_budget());
	}


    /**
     * Calculates the service fee rate.
     *
     * @param      int   $budget  The budget
     *
     * @return     bool  The service fee rate.
     */
    public function calc_service_fee_rate($budget = 0)
    {
        $budget OR $budget = (double) $this->calc_budget();

        $service_fee_rules = $this->CI->config->item('service_fee_rule');

        $rule = array_filter($service_fee_rules, function($x) use ($budget){
            if( ! is_array($x)) return false;
            return $x['min'] <= $budget && $budget <= $x['max'];
        });

        $rule AND $rule = reset($rule);

        if(empty($rule)) return false;

        return $rule['rate'];
    }

    /**
     * Calculates the service fee.
     *
     * @param      int   $budget  The budget
     *
     * @return     int   The service fee.
     */
    public function calc_service_fee($budget = 0)
    {
        parent::is_exist_contract();

        $budget OR $budget = (double) $this->calc_budget();
        $rate   = $this->calc_service_fee_rate($budget);
        return $budget * $rate;
    }

    /**
     * Calculates the disacount amount.
     *
     * @return     <type>  The disacount amount.
     */
    public function calc_disacount_amount()
    {
        parent::is_exist_contract();

        $promotions = get_term_meta_value($this->contractId, 'promotions') ?: [];
        $promotions AND $promotions = unserialize($promotions);

        $service_fee = (double) get_term_meta_value($this->contractId, 'service_fee');

        $values = array_map(function($x) use ($service_fee){

            $value  = (double) $x['value'];

            if('percentage' === $x['unit'])
            {
                $_rate = div($x['value'], 100);
                $value = $service_fee*$_rate;
            }

            return $value;

        }, $promotions);

        return array_sum($values);
    }

    /**
     * Calc the fct tax.
     */
    public function get_fct_tax()
    {
        parent::is_exist_contract();

        return $fct = div((int) get_term_meta_value($this->contractId, 'fct'), 100);
    }

    /**
     * Get the VAT amount
     *
     * @return     integer  ( description_of_the_return_value )
     */
    public function cacl_vat_amount()
    {
        parent::is_exist_contract();
        $payment_amount     = (int) get_term_meta_value($this->contractId, 'payment_amount');
        $vat                = div((int) get_term_meta_value($this->contractId, 'vat'), 100);
        return div($payment_amount, 1+ $vat)*$vat;
    }

    /**
     * Calc payment's service fee
     */
    public function cacl_payment_service_fee()
    {
        parent::is_exist_contract();

        $payment_amount     = (int) get_term_meta_value($this->contractId, 'payment_amount');
        $vat                = div((int) get_term_meta_value($this->contractId, 'vat'), 100);
        $payment_amount     = div($payment_amount, 1 + $vat); /* Giá trị sau khi đã trừ VAT */
        $fct                = $this->get_fct_tax();
        $budget             = $this->calc_actual_budget();

        return $payment_amount - $this->calc_actual_budget() - $this->calc_fct($fct, $budget);
    }


    /**
     * Calculates the payment service fee.
     *
     * @throws     Exception  (description)
     *
     * @return     integer    The payment service fee.
     */
    public function calc_payment_service_fee()
    {
        parent::is_exist_contract();
    
        $payment_amount     = (int) get_term_meta_value($this->contractId, 'payment_amount');
        $vat                = div((int) get_term_meta_value($this->contractId, 'vat'), 100);
        $payment_amount     = div($payment_amount, 1 + $vat); /* Giá trị sau khi đã trừ VAT */
        $fct                = $this->get_fct_tax();
        $budget             = $this->calc_actual_budget();

        return $payment_amount - $this->calc_actual_budget() - $this->calc_fct($fct, $budget);
    }

    /**
     * Calculates the fct.
     *
     * @param      float|integer  $percent  The percent
     *
     * @return     <type>         The fct.
     */
    public function calc_fct($percent = 0.05, $amount = 0)
    {
        parent::is_exist_contract(); // Determines if exist contract.
        empty($amount) AND $amount = (double) get_term_meta_value($this->contractId, 'contract_budget');
        return div($amount, 1-$percent)*$percent;
    }


    /**
     * Sync All amount value of this contract
     *
     * @param      integer  $this->contractId  The term identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function sync_all_amount()
    {
        parent::sync_all_amount();
        $this->sync_all_progress();
        return TRUE;
    }


    /**
     * Đồng bộ các chỉ số bị ảnh thưởng khi có phát sinh chi tiêu
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function sync_all_progress()
    {
        parent::is_exist_contract();

        try
        {
            $actual_budget = max($this->calc_actual_budget(), 0);
            update_term_meta($this->contractId, 'actual_budget', $actual_budget);
        }
        catch (\Exception $e) {}

        try
        {
            $expected_end_time = $this->calc_expected_end_time();
            update_term_meta($this->contractId, 'expected_end_time', $expected_end_time);
        }
        catch (\Exception $e) {}

        try 
        {
            $real_progress = $this->calc_real_progress();
        }
        catch (\Exception $e) {}

        try
        {
            $payment_expected_end_time = $this->calc_payment_expected_end_time();
            update_term_meta($this->contractId, 'payment_expected_end_time', $payment_expected_end_time);
        }
        catch (\Exception $e) {}

        try 
        {
            $real_progress = $this->calc_real_progress_by_payment();
        }
        catch (\Exception $e) {}

        return TRUE;
    }

    /**
     * Calculates the contract value.
     *
     * @throws     Exception  (description)
     *
     * @return     integer    The contract value.
     */
    public function calc_contract_value()
    {
        parent::is_exist_contract(); // Determines if exist contract.

        $contract_budget    = (int) get_term_meta_value($this->contractId, 'contract_budget');
        $service_fee        = (int) get_term_meta_value($this->contractId, 'service_fee');

        return max(0, $contract_budget + $service_fee - $this->calc_disacount_amount());
    }

    /**
     * Calculates the Actual budget Money from Payment money.
     *
     * @return     integer  The actual budget.
     */
    public function calc_actual_budget()
    {
        parent::is_exist_contract();
        
        $actual_budget  = 0;
        $posts  = $this->CI->term_posts_m->get_term_posts($this->contractId, [ 'receipt_payment', 'receipt_payment_on_behaft' ]);
		if( ! empty($posts))
		{
			$posts AND $posts = array_filter($posts, function($x){ return $x->post_status == 'paid'; });
			$posts AND $posts = array_map(function($x){
				return (double) get_post_meta_value($x->post_id, 'actual_budget');
			}, $posts);

            $actual_budget = (double) array_sum($posts);
		}
        
        return $actual_budget;
    }


    /**
     * Tính tổng số ngày đã chạy dịch vụ dựa theo ngày kích hoạt thực hiện dịch vụ
     *
     * @param      <type>  $this->contractId  The term identifier
     *
     * @return     double  The real progress.
     */
        
    function calc_real_days()
    {
        parent::is_exist_contract();

        $start_service_time = get_term_meta_value($this->contractId,'start_service_time');
        if(empty($start_service_time)) throw new \Exception('Dịch vụ chưa được kích hoạt thực hiên !');
        
        $start_time         = start_of_day(get_term_meta_value($this->contractId, 'googleads-begin_time'));
        $end_service_time   = get_term_meta_value($this->contractId, 'end_service_time') ?: start_of_day();

        return diffInDates($start_time,$end_service_time);
    }


    /**
     * Tính tổng ngân sách tiến độ (phải chạy) cho đến thời điểm kết thúc dịch vụ
     *
     * Tổng NS phải chạy = Ngân sách HĐ / Số ngày HĐ * số ngày chạy thực tế
     *
     * @param      int  $this->contractId  The term identifier
     *
     * @return     double  The real progress.
     */
    function calc_budget_progress()
    {
        parent::is_exist_contract();

        $start_time = get_term_meta_value($this->contractId,'start_service_time');
        if(empty($start_time)) throw new \Exception('Dịch vụ chưa được kích hoạt thực hiên !');

        $contract_budget    = $this->calc_budget();
        $contract_days      = parent::calc_contract_days(); // Tính tổng số ngày chạy dịch vụ dựa theo ngày hợp đồng
        $real_days          = $this->calc_real_days();

        return div($contract_budget, $contract_days)*$real_days;
    }


    /**
     * Tính tổng ngân sách tiến độ (phải chạy) cho đến thời điểm kết thúc dịch vụ
     *
     * Tổng NS thực thu phải chạy = Ngân sách HĐ / Số ngày HĐ * số ngày chạy thực tế
     *
     * @param      int  $this->contractId  The term identifier
     *
     * @return     double  The real progress.
     */
    function calc_actual_budget_progress()
    {
        parent::is_exist_contract();

        if( ! is_service_proc($this->contractId)) throw new \Exception('Dịch vụ chưa được kích hoạt thực hiên !');

        $actual_budget      = $this->calc_actual_budget(); // Ngân sách thực thu
        $actual_budget_days = $this->calc_actual_budget_days(); // Số ngày thực hiện hợp đồng dựa trên ngân sách thực thu
        $real_days          = $this->calc_real_days(); // Số ngày đã thực hiện dịch vụ

        return div($actual_budget, $actual_budget_days)*$real_days;
    }


    /**
     * Calculates the budget.
     *
     * @throws     Exception  (description)
     *
     * @return     <type>     The budget.
     */
    public function calc_budget()
    {
        parent::is_exist_contract(); // Determines if exist contract.
        return (int) get_term_meta_value($this->contractId, 'contract_budget');
    }


    /**
     * Creates invoices.
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function create_invoices()
    {
        parent::is_exist_contract();

        if('customer' == get_term_meta_value($this->contractId, 'contract_budget_payment_type')) return $this->get_instance()->create_invoices();

        $contract_begin     = get_term_meta_value($this->contractId,'contract_begin');
        $contract_end       = get_term_meta_value($this->contractId,'contract_end');
        $num_dates          = diffInDates($contract_begin,$contract_end);

        $service_type   = get_term_meta_value($this->contractId,'service_type') ?: 'cpc_type';
        $start_date     = $contract_begin;
        $total_amount   = 0;

        $number_of_payments = get_term_meta_value($this->contractId,'number_of_payments') ?: 1;

        $budget             = $this->calc_budget(); // Ngân sách QC
        $budget_per_payment = div($budget, $number_of_payments); // Ngân sách QC mỗi đợt thanh toán

        $service_fee        = (int) get_term_meta_value($this->contractId, 'service_fee'); // Phí dịch vụ
        $sfee_per_payment   = div($service_fee, $number_of_payments); // Phí dịch vụ mỗi đợt thanh toán

        $service_fee_payment_type = get_term_meta_value($this->contractId, 'service_fee_payment_type');
        if( empty($service_fee_payment_type)) // Khởi tạo giá trị mặc định cho loại thanh toán phí dịch vụ dựa trên file config
        {
            $this->CI->config->load('googleads/contract');
            $service_fee_payment_type_def   = $this->CI->config->item('service_fee_payment_type');
            $service_fee_payment_type_def   = array_keys($service_fee_payment_type_def);
            $service_fee_payment_type       = reset($service_fee_payment_type_def);
        }

        $fct                = (int) get_term_meta_value($this->contractId, 'fct');
        $fct_per_payment    = !empty($fct) ? div(div($budget, 0.95)*div($fct, 100), $number_of_payments) : 0;

        $apply_discount_type            = get_term_meta_value($this->contractId, 'apply_discount_type') ?: 'last_invoice';
        $discount_amount                = $this->calc_disacount_amount();
        $discount_amount_per_payment    = div($discount_amount, $number_of_payments);

        $num_days4inv   = ceil(div($num_dates, $number_of_payments));
        $i              = 0;
        for($i ; $i < $number_of_payments; $i++)
        {   
            if($num_days4inv == 0) break;

            /* Khởi tạo đợt thanh toán */
            $end_date = strtotime('+'.$num_days4inv.' day -1 second', $start_date);
            $end_date = end_of_day($end_date);

            $insert_data = array(
                'post_title'    => 'Thu tiền đợt '. ($i + 1),
                'post_content'  => '',
                'start_date'    => $start_date,
                'end_date'      => $end_date,
                'post_type'     => $this->CI->invoice_m->post_type );

            $inv_id = $this->CI->invoice_m->insert($insert_data);
            if(empty($inv_id)) continue;

            $this->CI->term_posts_m->set_post_terms($inv_id, $this->contractId, $this->contract->term_type);

            $invi_insert_data = array();

            // Khởi tạo hạng mục "Ngân sách QC"
            $invi_insert_data[] = array(
                'invi_title'    => 'Ngân sách quảng cáo Google',
                'invi_status'   => 'publish',
                'invi_description' => '',
                'quantity'  => 1,
                'invi_rate' => 100,
                'inv_id'    => $inv_id,
                'price'     => $budget_per_payment,
                'total'     => $this->CI->invoice_item_m->calc_total_price($budget_per_payment, 1, 100)
            );

            // Khởi tạo hạng mục "Phí nhà thầu"
            if( ! empty($fct_per_payment))
            {
                $invi_insert_data[] = array(
                    'invi_title'    => 'Thuế nhà thầu ('.currency_numberformat($fct, '%').')', 
                    'invi_status'   => 'publish',
                    'invi_description' => '',
                    'quantity'  => 1,
                    'invi_rate' => 100,
                    'inv_id'    => $inv_id,
                    'price'     => $fct_per_payment,
                    'total'     => $this->CI->invoice_item_m->calc_total_price($fct_per_payment, 1, 100)
                );
            }

            /* Khởi tạo với hạng mục trong đợt thanh toán */
            if($i == 0 && 'full' == $service_fee_payment_type)
            {
                $invi_insert_data[] = array(
                    'invi_title'        => 'Phí dịch vụ quảng cáo (thanh toán 100% đợt đầu)',
                    'invi_status'       => 'publish',
                    'invi_description'  => '',
                    'quantity'          => 1,
                    'invi_rate'         => 100,
                    'inv_id'            => $inv_id,
                    'price'             => $service_fee,
                    'total'             => $this->CI->invoice_item_m->calc_total_price($service_fee, 1, 100)
                );

                $sfee_per_payment = 0;
            }

            if( ! empty($sfee_per_payment))
            {
                $invi_insert_data[] = array(
                    'invi_title'    => 'Phí dịch vụ quảng cáo',
                    'invi_status'   => 'publish',
                    'invi_description' => '',
                    'quantity'  => 1,
                    'invi_rate' => 100,
                    'inv_id'    => $inv_id,
                    'price'     => $sfee_per_payment,
                    'total'     => $this->CI->invoice_item_m->calc_total_price($sfee_per_payment, 1, 100)
                );
            }

            if( ! empty($discount_amount))
            {
                switch ($apply_discount_type)
                {
                    case 'last_invoice':
                        if($i != $number_of_payments - 1) break;
                        $invi_insert_data[] = array(
                            'invi_title'    => 'Chương trình giảm giá',
                            'invi_status'   => 'publish',
                            'invi_description' => '',
                            'quantity'  => 1,
                            'invi_rate' => 100,
                            'inv_id'    => $inv_id,
                            'price'     => $discount_amount,
                            'total'     => $this->CI->invoice_item_m->calc_total_price(-$discount_amount, 1, 100)
                        );
                        break;
                    
                    default:
                        $invi_insert_data[] = array(
                            'invi_title'    => 'Chương trình giảm giá',
                            'invi_status'   => 'publish',
                            'invi_description' => '',
                            'quantity'  => 1,
                            'invi_rate' => 100,
                            'inv_id'    => $inv_id,
                            'price'     => $discount_amount_per_payment,
                            'total'     => $this->CI->invoice_item_m->calc_total_price(-$discount_amount_per_payment, 1, 100)
                        );
                        break;
                }
            }

            foreach ($invi_insert_data as $_invi)
            {
                $this->CI->invoice_item_m->insert($_invi);
            }

            $start_date = strtotime('+1 second', $end_date);
            $day_end = $num_dates - $num_days4inv;
            if($day_end < $num_days4inv) $num_days4inv = $day_end;
        }

        update_term_meta($this->contractId, 'contract_value', $this->calc_contract_value());
        return TRUE;
    }

    /**
     * Tính tiến độ thực hiện thực tế đến thời điểm kết thúc dịch vụ
     * hoặc thời gian tính nếu hợp đồng vẫn đang thực hiện
     *
     * Tiến độ = Tổng NS đã chạy / Tổng NS phải chạy
     *
     * @param      int  $this->contractId  The term identifier
     *
     * @return     double  The real progress.
     */
    function calc_real_progress()
    {
        parent::is_exist_contract();

        if( ! is_service_proc($this->contractId)) return 0; // Nếu HĐ chưa được kích hoạt thực hiện thì return 0

        $result = numberformat(div($this->get_actual_result(),$this->calc_budget_progress())*100, 2);

        update_term_meta($this->contractId, 'real_progress', $result);

        return $result;
    }

    /**
     * Tính tiến độ thực hiện thực tế đến thời điểm kết thúc dịch vụ
     * hoặc thời gian tính nếu hợp đồng vẫn đang thực hiện
     *
     * Tiến độ = Tổng NS đã chạy / Tổng NS phải chạy
     *
     * @param      int  $this->contractId  The term identifier
     *
     * @return     double  The real progress.
     */
    function calc_real_progress_by_payment()
    {
        parent::is_exist_contract();

        if( ! is_service_proc($this->contractId)) return 0; // Nếu HĐ chưa được kích hoạt thực hiện thì return 0

        $result = numberformat(div($this->get_actual_result(),$this->calc_actual_budget_progress())*100, 2);

        update_term_meta($this->contractId, 'payment_real_progress', $result);

        return $result;
    }


    /**
      * Tính thời gian dự kiến kết thúc
      *
      * Dự kiến kết thúc = [Tổng NS tiến độ]/[Tổng NS đã chạy]*[Số ngày HD] - 1 + Ngày kích hoạt thực hiện dịch vụ
      *
      * @throws     Exception  Error , stop and return
      *
      * @return     int        Thời gian dự kiến kết thúc dịch vụ dựa theo tiến độ thực tế
      */
    public function calc_expected_end_time()
    {
        parent::is_exist_contract(); // Determines if exist contract.
        if( ! is_service_proc($this->contractId)) throw new \Exception('Dịch vụ chưa được thực hiện');

        $budget_progress    = $this->calc_budget_progress(); //Tính tổng ngân sách tiến độ (phải chạy) cho đến thời điểm kết thúc dịch vụ
        $actual_result      = $this->get_actual_result(); // Tính tổng ngân sách thực tế đã chi
        $contract_days      = $this->calc_contract_days(); // Tính tổng số ngày thực hiện trên hợp đồng
        
        $expected_day   = round(div($budget_progress, $actual_result)*$contract_days);
        $start_time     = start_of_day(get_term_meta_value($this->contractId, 'advertise_start_time'));

        return strtotime("+{$expected_day} days -1 day",$start_time);
    }


    /**
      * Tính thời gian dự kiến kết thúc
      *
      * Dự kiến kết thúc = [Tổng NS tiến độ]/[Tổng NS đã chạy]*[Số ngày HD] - 1 + Ngày kích hoạt thực hiện dịch vụ
      *
      * @throws     Exception  Error , stop and return
      *
      * @return     int        Thời gian dự kiến kết thúc dịch vụ dựa theo tiến độ thực tế
      */
    public function calc_payment_expected_end_time()
    {
        parent::is_exist_contract(); // Determines if exist contract.

        if( ! is_service_proc($this->contractId)) throw new \Exception('Dịch vụ chưa được thực hiện');

        $budget_progress    = $this->calc_actual_budget_progress(); //Tính tổng ngân sách tiến độ (phải chạy) cho đến thời điểm kết thúc dịch vụ
        $actual_result      = $this->get_actual_result(); // Tính tổng ngân sách thực tế đã chi
        $actual_budget_days = $this->calc_actual_budget_days(); // Số ngày thực hiện hợp đồng dựa trên ngân sách thực thu

        $expected_day   = round(div($budget_progress, $actual_result)*$actual_budget_days);
        $start_time     = start_of_day(get_term_meta_value($this->contractId,'googleads-begin_time'));

        return strtotime("+{$expected_day} days -1 day",$start_time);
    }


    /**
     * Gets the actual result.
     */
    public function get_actual_result()
    {
        if( ! $this->contract) return 0;

        $this->contractId        = $this->contractId;
        $actual_result  = (float) get_term_meta_value($this->contractId, 'actual_result') ?: 0;

        $account_currency_code = get_term_meta_value($this->contractId, 'account_currency_code');
        
        $exchange_rate = get_exchange_rate($account_currency_code, $this->contractId);
        $actual_result = $actual_result * $exchange_rate;

        return $actual_result;
    }

    /**
     * Calculates the cost per day left.
     *
     * @param      string     $budget_type  The budget type
     *
     * @throws     Exception  (description)
     *
     * @return     <type>     The cost per day left.
     */
    public function calc_cost_per_day_left($budget_type = 'commit')
    {
        parent::is_exist_contract();

        if( ! is_service_proc($this->contractId)) throw new \Exception('Dịch vụ chưa được thực hiện');

        $left_days      = $this->calc_contract_days() - $this->calc_real_days();
        $left_days      = $left_days > 0 ? $left_days : 1;
        $actual_result  = $this->get_actual_result();

        $budget         = 0;
        switch ($budget_type)
        {
            case 'commit':  $budget = $this->calc_budget(); break;
            case 'payment': $budget = $this->calc_actual_budget(); break;
            default: break;
        }

        return div(($budget - $actual_result), $left_days);
    }

    /**
     * Tính số ngày thực hiện hợp đồng dựa trên ngân sách thực thu
     *
     * @throws     Exception  (description)
     *
     * @return     integer    The actual budget days.
     */
    public function calc_actual_budget_days()
    {
        parent::is_exist_contract();

        if( ! is_service_proc($this->contractId)) throw new \Exception('Dịch vụ chưa được thực hiện');

        $actual_budget      = $this->calc_actual_budget(); // Ngân sách thực thu
        $contract_budget    = $this->calc_budget(); // Ngân sách cam kết trên hợp đồng
        $rate               = div($actual_budget, $contract_budget); // Tỉ lệ ngân sách thực thu so với cam kết
        $contract_days      = $this->calc_contract_days(); // Số ngày thực hiện hợp đồng

        return $contract_days * $rate; 
    }


    /**
     * Clone Contract
     */
    public function clone()
    {
        $insert_id = parent::clone();
        if( ! $insert_id) throw new \Exception('Quá trình thêm mới không thành công');
        
        $insert_data = (array) $this->contract;
        $insert_data['term_status'] = 'unverified';
        unset($insert_data['term_id']);

        /* Clone các trường mặc định của hợp đồng */
        $metadata = get_term_meta_value($this->contract->term_id);
        $metadata = key_value($metadata, 'meta_key', 'meta_value');

        $account_currency_code = $metadata['account_currency_code'];

        $meta_key = strtolower("exchange_rate_{$account_currency_code}_to_vnd");
        $metadata[$meta_key] = get_exchange_rate($account_currency_code, $this->contractId);

        $fields = array_intersect(array_keys($metadata), $this->CI->config->item('metadata_googleads'));
        if( ! empty($fields))
        {
            foreach ($fields as $f)
            {
                if(empty($metadata[$f])) continue;
                update_term_meta($insert_id, $f, $metadata[$f]);
            }
        }
        
        return $insert_id;
    }

    /**
     * Sets the technician identifier.
     */
    public function setTechnicianId()
    {
        parent::is_exist_contract();

        $this->CI->load->model('googleads/googleads_kpi_m');

        $kpis = $this->CI->googleads_kpi_m->select('user_id')->distinct('user_id')->where('term_id', $this->contractId)->get_all();
        if( ! $kpis)
        {
            update_term_meta($this->contractId, 'technicianId', 0);
            return true;
        }

        $roleNameEnums  = [ 
            $this->CI->option_m->get_value('ads_role_id') => 'gat',
            $this->CI->option_m->get_value('leader_ads_role_id') => 'am',
            $this->CI->option_m->get_value('manager_ads_role_id') => 'manager'
        ];

        $staffs = array_map(function($x) use ($roleNameEnums){
            $_staff = $this->CI->admin_m->get_field_by_id($x->user_id);
            $_staff['roleName'] = $roleNameEnums[$_staff['role_id']] ?? 'unknown';
            return $_staff;
        }, $kpis);
        
        if( 1 == count($staffs))
        {
            $_staff = reset($staffs);
            update_term_meta($this->contractId, 'technicianId', $_staff['user_id']);
            return true;
        }
        $staffsGrouped = array_group_by($staffs, 'roleName');

        foreach (array_values($roleNameEnums) as $roleName)
        {
            if(empty($staffsGrouped[$roleName])) continue;

            $_staff = reset($staffsGrouped[$roleName]);
            update_term_meta($this->contractId, 'technicianId', $_staff['user_id']);
            return true;
        }

        $_staff = reset($staffs);
        update_term_meta($this->contractId, 'technicianId', $_staff['user_id']);
        return true;
    }

    public function get_adaccount(){
        $adaccounts = $this->CI->term_posts_m
        ->join('posts AS ads_segment', 'ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = "ads_segment"')
        ->join('term_posts AS tp_adaccount', 'tp_adaccount.post_id = ads_segment.post_id')
        ->join('term AS adaccount', 'adaccount.term_id = tp_adaccount.term_id AND adaccount.term_type = "' . $this->CI->adaccount_m->term_type . '"')

        ->where('term_posts.term_id', $this->contractId)

        ->select('
            adaccount.term_id AS term_id, 
            adaccount.term_slug AS account_id,
            ads_segment.start_date AS start_date,
            ads_segment.end_date AS end_date
        ')
        ->as_array()
        ->get_all();
        if(empty($adaccounts)) return [];

        $adaccounts = array_map(function($item){
            $adaccount_id = $item['term_id'];

            $adaccount_meta = get_term_meta_value($adaccount_id);
            $adaccount_meta = key_value($adaccount_meta, 'meta_key', 'meta_value');
            
            $item = array_merge($item, $adaccount_meta);
            
            return $item;
        }, $adaccounts);

        return $adaccounts;
    }

    /**
     * Retrieves segments based on the given contract ID.
     *
     * @param array $extend_selects An array of additional select columns to include in the query.
     * @return array An array of segments matching the contract ID.
     */
    public function get_segments(array $extend_selects = []){
        $segments = $this->CI->term_posts_m
        ->join('posts AS ads_segment', 'ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = "ads_segment"')
        ->join('term_posts AS tp_adaccount', 'tp_adaccount.post_id = ads_segment.post_id')
        ->join('term AS adaccount', 'adaccount.term_id = tp_adaccount.term_id AND adaccount.term_type = "' . $this->CI->adaccount_m->term_type . '"')

        ->where('term_posts.term_id', $this->contractId)

        ->select('
            ads_segment.post_id AS post_id,
            adaccount.term_id AS adaccount_id,
            term_posts.term_id AS contract_id, 
            ads_segment.start_date AS start_date,
            ads_segment.end_date AS end_date
        ');

        $allow_select = [
            'assigned_technician_id',
            'assigned_technician_rate'
        ];
        array_walk($extend_selects, function($extend_select) use ($allow_select, $segments){
            if(!in_array($extend_select, $allow_select))
            {
                return;
            }

            switch($extend_select)
            {
                case 'assigned_technician_id':
                    $segments->select('ads_segment.post_author AS assigned_technician_id');
                    return;
                case 'assigned_technician_rate':
                    $segments->select('ads_segment.post_content AS assigned_technician_rate');
                    return;
                default:
                    return;
            }
        });

        $segments = $segments->as_array()->get_all();
        if(empty($segments)) return [];

        return $segments;
    }
    
    /**
     * getTypeOfService
     *
     * @return void
     */
    public function getTypeOfService()
    {
        /**
         * Section for deprecated service type CPC_TYPE
         */
        if('cpc_type' == get_term_meta_value($this->contractId, 'service_type'))
        {
            return 'cpc_type';
        }

        $isBehaft = 'customer' == get_term_meta_value($this->contractId, 'contract_budget_payment_type');
        $isAccountForRent = (bool) get_term_meta_value($this->contractId, 'isAccountForRent');

        $parent_type	= $isBehaft ? 'behaft' : 'fullvat';
        $child_type		= $isAccountForRent ? 'rent' : 'default';
        return "{$parent_type}_{$child_type}";
    }
        
    /**
     * calc_service_provider_tax
     *
     * @param  mixed $budget
     * @param  mixed $rate
     * @return void
     */
    public function calc_service_provider_tax($budget = 0, $rate = 0.05)
    {
        return 0;
    }

    /**
     * Delete All Ads Segments
     *
     * @return     boolean  ( description_of_the_return_value )
     */
    public function delete_segments()
    {
        parent::is_exist_contract(); // Determines if exist contract.

        $this->CI->load->model('term_posts_m');
        $this->CI->load->model('ads_segment_m');
        $this->CI->load->model('tiktokads/adaccount_m');

        $adssegments 	= $this->CI->term_posts_m->get_term_posts($this->contractId, $this->CI->ads_segment_m->post_type);
        if(empty($adssegments)) return true;

        $adsSegmentsIds = array_filter(array_column($adssegments, 'post_id'));
        array_walk($adsSegmentsIds, function($x){
            $adaccounts = $this->CI->term_posts_m->get_post_terms($x, $this->CI->adaccount_m->term_type);
            if( ! empty($adaccounts))
            {
                $adaccountIds = array_column($adaccounts, 'term_id');
                $this->CI->ads_segment_m->detach_ad_account($x, $adaccountIds, $this->CI->adaccount_m->term_type);
            }
        });

        $this->CI->ads_segment_m->delete_many($adsSegmentsIds);
        $this->CI->term_posts_m->delete_term_posts($this->contractId, $adsSegmentsIds);
        return true;
    }

    /**
	 * Gets the progress.
	 *
	 * @param      <type>   $start_time    The start time
	 * @param      <type>   $end_time      The end time
	 *
	 * @return     integer  The progress.
	 */
	public function get_the_progress($start_time = 0, $end_time = 0)
    {
    	$all_time 	= empty($start_time) && empty($end_time);
    	$actual 	= 0;
    	$progress 	= 0;
    	$target 	= get_term_meta_value($this->contractId, 'contract_budget');

    	if(!empty($start_time))
        {
            $start_time = start_of_day($start_time);
        }

        if(!empty($end_time))
        {
            $end_time = end_of_day($end_time);
        }

        $this->CI->tiktokads_m->set_contract($this->contractId);

		$insight = $this->CI->tiktokads_m->get_insights(['start_time' => $start_time, 'end_time' => $end_time, 'summary' => TRUE]);
		$insight AND $actual = $insight['spend']; 
 		
    	$progress  	= number_format(div($actual, $target)*100, 3);
    	if( ! $all_time) return $progress;

    	/* Store last adword account data updated time then update result updated on by current time excuted */
    	$actual != get_term_meta_value($this->contractId, 'actual_result') 
    	AND update_term_meta($this->contractId, 'result_updated_on', time());

        /* update actual_result */
        update_term_meta($this->contractId, 'actual_result', $actual);

        $actual_progress_percent = number_format((double)get_term_meta_value($this->contractId, 'actual_progress_percent'), 3);
        if($actual_progress_percent != $progress) update_term_meta($this->contractId,'actual_progress_percent',$progress);

        /* Cập nhật ngân sách */
        $actual_budget                  = (int) get_term_meta_value($this->contractId, 'actual_budget');
        $actual_progress_percent_net    = number_format(div($actual, $actual_budget), 3);
        if($actual_progress_percent_net != number_format( (double) get_term_meta_value($this->contractId,'actual_progress_percent_net'),3))  
        update_term_meta($this->contractId, 'actual_progress_percent_net', $actual_progress_percent_net);

        return $progress;
    }
}