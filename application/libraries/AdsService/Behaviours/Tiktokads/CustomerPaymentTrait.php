<?php

namespace AdsService\Behaviours\Tiktokads;

trait CustomerPaymentTrait {
    

    /**
	 * Calculates the total Payment On BeHaft amount.
	 *
	 * @param      <type>     $start_time  The start time
	 * @param      <type>     $end_time    The end time
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The total payment amount.
	 */
	public function calc_payment_on_behaft_amount($start_time = FALSE,$end_time = FALSE)
	{
		parent::is_exist_contract();

		$this->CI->load->model('contract/receipt_m');

		if(!empty($start_time)) $this->CI->receipt_m->where('posts.end_date >=', $start_time);

		$receipts = $this->CI->receipt_m
		->select('posts.post_id')
		->join('term_posts', 'term_posts.post_id = posts.post_id', 'LEFT')
		->where('term_posts.term_id', $this->contractId)
		->where('posts.post_status', 'paid')
		->where_in('posts.post_type', 'receipt_payment_on_behaft')
		->order_by('posts.end_date', 'desc')
		->get_many_by();

		if(empty($receipts)) return 0;

		return array_sum(array_map(function($x){ 
			return (double) get_post_meta_value($x->post_id,'amount');
		}, $receipts));
	}


    /**
     * Đồng bộ các chỉ số bị ảnh thưởng khi có phát sinh chi tiêu
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function sync_all_progress()
    {
        parent::is_exist_contract(); // Determines if exist contract.
        
        // Tính giá trị đã khách hàng đã chuyển khoản & nhờ thanh toán hộ
        if('behalf' == get_term_meta_value($this->contractId, 'contract_budget_customer_payment_type')) 
            update_term_meta($this->contractId, 'payment_on_behaft_amount', (int) $this->calc_payment_on_behaft_amount());

        return parent::sync_all_progress();
    }

    /**
     * Calculates the payment service fee.
     *
     * @throws     Exception  (description)
     *
     * @return     integer    The payment service fee.
     */
    public function calc_payment_service_fee()
    {
        parent::is_exist_contract(); // Determines if exist contract.

        $payment_amount             = (int) get_term_meta_value($this->contractId, 'payment_amount');
        $vat                        = div((int) get_term_meta_value($this->contractId, 'vat'), 100);
        $payment_amount_after_vat   = div($payment_amount, 1 + $vat); /* Giá trị sau khi đã trừ VAT */

        return $payment_amount_after_vat;
    }

    /**
     * Calculates the fct.
     *
     * @param      float|integer  $percent  The percent
     *
     * @return     <type>         The fct.
     */
    public function calc_fct($percent = 0.05, $amount = 0)
    {
        parent::is_exist_contract(); // Determines if exist contract.
        return 0;
    }


    /**
     * Creates invoices.
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function create_invoices()
    {
        parent::is_exist_contract(); // Determines if exist contract.
        
        $contract_begin     = get_term_meta_value($this->contractId, 'contract_begin');
        $contract_end       = get_term_meta_value($this->contractId, 'contract_end');
        $num_dates          = diffInDates($contract_begin, $contract_end);

        $service_type   = get_term_meta_value($this->contractId, 'service_type') ?: 'cpc_type';
        $start_date     = $contract_begin;

        $number_of_payments = get_term_meta_value($this->contractId, 'number_of_payments') ?: 1;

        $service_fee        = (int) get_term_meta_value($this->contractId, 'service_fee'); // Phí dịch vụ
        $sfee_per_payment   = div($service_fee, $number_of_payments); // Phí dịch vụ mỗi đợt thanh toán

        $service_fee_payment_type = get_term_meta_value($this->contractId, 'service_fee_payment_type');
        if( empty($service_fee_payment_type)) // Khởi tạo giá trị mặc định cho loại thanh toán phí dịch vụ dựa trên file config
        {
            $service_fee_payment_type_def   = $this->CI->config->item('service_fee_payment_type');
            $service_fee_payment_type_def   = array_keys($service_fee_payment_type_def);
            $service_fee_payment_type       = reset($service_fee_payment_type_def);
        }

        $apply_discount_type 			= get_term_meta_value($this->contractId, 'apply_discount_type') ?: 'last_invoice';
		$discount_amount 				= $this->calc_disacount_amount();
	    $discount_amount_per_payment 	= div($discount_amount, $number_of_payments);

        $num_days4inv   = ceil(div($num_dates, $number_of_payments));
        $i              = 0;
        for($i ; $i < $number_of_payments; $i++)
        {   
            if($num_days4inv == 0) break;

            /* Khởi tạo đợt thanh toán */
            $end_date = strtotime('+'.$num_days4inv.' day -1 second', $start_date);
            $end_date = end_of_day($end_date);

            $insert_data = array(
                'post_title'    => 'Thu tiền đợt '. ($i + 1),
                'post_content'  => '',
                'start_date'    => $start_date,
                'end_date'      => $end_date,
                'post_type'     => $this->CI->invoice_m->post_type );

            $inv_id = $this->CI->invoice_m->insert($insert_data);
            if(empty($inv_id)) continue;

            $this->CI->term_posts_m->set_post_terms($inv_id, $this->contractId, $this->contract->term_type);

            $invi_insert_data = array();

            /* Khởi tạo với hạng mục trong đợt thanh toán */
            if($i == 0 && 'full' == $service_fee_payment_type)
            {
                $invi_insert_data[] = array(
                    'invi_title'    => 'Phí dịch vụ quảng cáo (thanh toán 100% đợt đầu)',
                    'invi_status'   => 'publish',
                    'invi_description' => '',
                    'quantity'  => 1,
                    'invi_rate' => 100,
                    'inv_id'    => $inv_id,
                    'price'     => $service_fee,
                    'total'     => $this->CI->invoice_item_m->calc_total_price($service_fee, 1, 100)
                );

                $sfee_per_payment = 0;
            }

            if( ! empty($sfee_per_payment))
            {
                $invi_insert_data[] = array(
                    'invi_title'    => 'Phí dịch vụ quảng cáo',
                    'invi_status'   => 'publish',
                    'invi_description' => '',
                    'quantity'  => 1,
                    'invi_rate' => 100,
                    'inv_id'    => $inv_id,
                    'price'     => $sfee_per_payment,
                    'total'     => $this->CI->invoice_item_m->calc_total_price($sfee_per_payment, 1, 100)
                );
            }

            if( ! empty($discount_amount))
			{
				switch ($apply_discount_type)
				{
					case 'last_invoice':
						if($i != $number_of_payments - 1) break;
						$invi_insert_data[] = array(
							'invi_title' 	=> 'Chương trình giảm giá',
							'invi_status' 	=> 'publish',
							'invi_description' => '',
							'quantity' 	=> 1,
							'invi_rate' => 100,
							'inv_id' 	=> $inv_id,
							'price' 	=> $discount_amount,
							'total' 	=> $this->CI->invoice_item_m->calc_total_price(-$discount_amount, 1, 100)
						);
						break;
					
					default:
						$invi_insert_data[] = array(
							'invi_title' 	=> 'Chương trình giảm giá',
							'invi_status' 	=> 'publish',
							'invi_description' => '',
							'quantity' 	=> 1,
							'invi_rate' => 100,
							'inv_id' 	=> $inv_id,
							'price' 	=> $discount_amount_per_payment,
							'total' 	=> $this->CI->invoice_item_m->calc_total_price(-$discount_amount_per_payment, 1, 100)
						);
						break;
				}
			}

            foreach ($invi_insert_data as $_invi)
            {
                $this->CI->invoice_item_m->insert($_invi);
            }

            $start_date = strtotime('+1 second', $end_date);
            $day_end    = $num_dates - $num_days4inv;
            if($day_end < $num_days4inv) $num_days4inv = $day_end;
        }

        update_term_meta($this->contractId, 'contract_value', $this->calc_contract_value());
        return TRUE;
    }

    /**
     * Calculates the Actual budget Money from Payment money.
     *
     * @return     integer  The actual budget.
     */
    public function calc_actual_budget()
    {
        parent::is_exist_contract(); // Determines if exist contract.

        /* 
         * Trường hợp khách ủy quyền chi hộ
         * Thì Ngân sách Quảng Cáo sẽ là Ngân sách mà Khách đã chuyển nhờ chi hộ
         */
        if('behalf' == get_term_meta_value($this->contractId, 'contract_budget_customer_payment_type'))
        {
            return (int) get_term_meta_value($this->contractId, 'payment_on_behaft_amount');
        }

        /* 
         * Trường hợp khách tự thanh toán ngân sách
         * Thì Ngân sách Quảng Cáo sẽ ánh xạ dựa theo tỷ lệ Phí dịch vụ Khách hàng đã thanh toán
         */
        
        /* Số tiền đã thanh toán */
        $vat = div((int) get_term_meta_value($this->contractId, 'vat'), 100);
        
        /* Số tiền đã thanh toán đã trừ VAT */
        $payment_amount = div((int) get_term_meta_value($this->contractId, 'payment_amount'), 1+$vat);

        /* Phí dịch vụ cam kết */
        $service_fee = (int) get_term_meta_value($this->contractId, 'service_fee');
        $discount_amount = (int) $this->calc_disacount_amount();
        $discount_amount > 0 AND $service_fee-= $discount_amount;

        /* Tỷ lệ phần trăm phí dịch vụ thực tế so với cam kết*/ 
        $payment_service_fee_rate = div($payment_amount, max($service_fee, 0));

        /* Ngân sách ánh xạ theo tỷ lệ phần trăm phí dịch vụ thực tế */
        return $this->calc_budget()*$payment_service_fee_rate;
    }

    /**
     * Calculates the contract value.
     *
     * @throws     Exception  (description)
     *
     * @return     integer    The contract value.
     */
    public function calc_contract_value()
    {
        parent::is_exist_contract(); // Determines if exist contract

        $total      = (int) get_term_meta_value($this->contractId, 'service_fee');
        $isBehaft   = 'behalf' == get_term_meta_value($this->contractId, 'contract_budget_customer_payment_type');
        $isBehaft AND $total+= (int) get_term_meta_value($this->contractId, 'contract_budget');

        $total-= $this->calc_disacount_amount();
        
        return $total;
    }
}