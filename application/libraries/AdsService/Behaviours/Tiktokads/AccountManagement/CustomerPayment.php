<?php

namespace AdsService\Behaviours\Tiktokads\AccountManagement;

class CustomerPayment extends Base
{
    use \AdsService\Behaviours\Tiktokads\CustomerPaymentTrait;

    /**
     * Constructs a new instance.
     *
     * @param      <type>  $endpoint  The endpoint
     * @param      <type>  $token     The token
     */
    function __construct($idOrContract)  
    {
        parent::__construct($idOrContract);
    }

    /**
     * Prepare the contract data for printable version
     *
     * @return     Array  The result data
     */
    public function prepare_preview()
    {
        parent::prepare_preview();

        $contract_id = $this->contractId;

        $this->printableData['data_service'] = array_merge($this->printableData['data_service'], [
            'service_name'  => 'Quản lý tài khoản',
        ]);

        $discount_amount 	= (double) get_term_meta_value($contract_id, 'discount_amount');
		$promotions 		= get_term_meta_value($contract_id, 'promotions') ?: [];

		if( !empty($discount_amount) && ! empty($promotions))
		{
			$promotions = unserialize($promotions);

			$this->printableData['data_service']['discount_amount'] = $discount_amount;
			$this->printableData['data_service']['promotions'] = array_map(function($promotion) {

				$promotion['text'] 	= "CTKM - {$promotion['name']}";
				$_value 	= $promotion['value'];

				if( 'percentage' == $promotion['unit'])
				{
					$_value = $this->printableData['data_service']['service_fee'] * div($promotion['value'], 100);
					$promotion['text'].= "({$promotion['value']}% phí dịch vụ)";
				}

				$promotion['amount'] = $_value;
				return $promotion;
			}, $promotions);
		}
        
        $this->printableData['view_file'] = 'tiktokads/contract/preview/behaft_account_after_2021_jan';

        return $this->printableData;
    }
}