<?php

namespace AdsService\Behaviours\Tiktokads\AccountManagement;

class FullVat extends \AdsService\Behaviours\Tiktokads\Base
{
    /**
     * Constructs a new instance.
     *
     * @param      <type>  $endpoint  The endpoint
     * @param      <type>  $token     The token
     */
    function __construct($idOrContract)  
    {
        parent::__construct($idOrContract);
    }

    /**
     * Prepare the contract data for printable version
     *
     * @return     Array  The result data
     */
    public function prepare_preview()
    {
        parent::prepare_preview();
        $this->printableData['data_service'] = array_merge($this->printableData['data_service'], [
            'service_name'  => 'Quản lý tài khoản',
        ]);


        $this->printableData['view_file']    = 'tiktokads/contract/preview/account';
        $verified_on    = get_term_meta_value($this->contractId, 'verified_on') ?: time();
        $verified_on > strtotime('2021/01/20') AND $this->printableData['view_file'] = "tiktokads/contract/preview/account_after_2021_jan";

        return $this->printableData;
    }
}