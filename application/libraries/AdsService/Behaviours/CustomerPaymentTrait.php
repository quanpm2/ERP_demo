<?php

namespace AdsService\Behaviours;

trait CustomerPaymentTrait {

    /**
     * Compute Actual Budget of Paid Payments
     * @return double Actual Budget
     */
    public function calc_actual_budget()
    {
        if( ! $this->contract) throw new \Exception('Property Contract undefined.');

        $result = 0;
        $posts  = $this->term_posts_m->get_term_posts($this->contract->term_id, [ 'receipt_payment', 'receipt_payment_on_behaft' ]);
		if( ! empty($posts))
		{
			$posts AND $posts = array_filter($posts, function($x){ return $x->post_status == 'paid'; });
			$posts AND $posts = array_map(function($x){
				return (double) get_post_meta_value($x->post_id, 'actual_budget');
			}, $posts);

            $result = (double) array_sum($posts);
		}

        return $result;
    }

    /**
     * Compute Actual Budget of Paid Payments
     * @return double Actual Budget
     */
    public function calc_payment_service_fee()
    {
        if( ! $this->contract) throw new \Exception('Property Contract undefined.');

        $result = 0;
        $posts  = $this->term_posts_m->get_term_posts($this->contract->term_id, [ 'receipt_payment', 'receipt_payment_on_behaft' ]);
		if( ! empty($posts))
		{
			$posts AND $posts = array_filter($posts, function($x){ return $x->post_status == 'paid'; });
			$posts AND $posts = array_map(function($x){
				return (double) get_post_meta_value($x->post_id, 'service_fee');
			}, $posts);

            $result = (double) array_sum($posts);
		} 

        return $result;
    }
    
    
    /**
     * Compute VAT of Paid Payments
     * @return double Actual VAT Amount
     */
    public function compute_vat_amount()
    {
        if( ! $this->contract) throw new \Exception('Property Contract undefined.');

        $result = 0;
        $posts  = $this->term_posts_m->get_term_posts($this->contract->term_id, [ 'receipt_payment', 'receipt_payment_on_behaft' ]);
		if( ! empty($posts))
		{
			$posts AND $posts = array_filter($posts, function($x){ return $x->post_status == 'paid'; });
			$posts AND $posts = array_map(function($x){
				return (double) get_post_meta_value($x->post_id, 'vat_amount');
			}, $posts);

            $result = (double) array_sum($posts);
		} 

        return $result;
    }
    
    /**
     * Compute Foreign Contractor Tax of Paid Payments
     * @return double Actual Foreign Contractor Tax
     */
    public function compute_fct_amount()
    {
        if( ! $this->contract) throw new \Exception('Property Contract undefined.');

        $result = 0;
        $posts  = $this->term_posts_m->get_term_posts($this->contract->term_id, [ 'receipt_payment', 'receipt_payment_on_behaft' ]);
		if( ! empty($posts))
		{
			$posts AND $posts = array_filter($posts, function($x){ return $x->post_status == 'paid'; });
			$posts AND $posts = array_map(function($x){
				return (double) get_post_meta_value($x->post_id, 'fct_amount');
			}, $posts);

            $result = (double) array_sum($posts);
		} 

        return $result;
    }
}