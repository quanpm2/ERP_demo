<?php

namespace AdsService\Behaviours;

class AdsFactory
{

    protected static $CI       = null;
    protected static $contract = null;
    protected static $contractId = null;

    public static function build($idOrObject)
    {
        self::$CI =&get_instance();
        self::$CI->contract_m->set_term_type()->get($idOrObject);
    }
}