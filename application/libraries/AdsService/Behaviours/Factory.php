<?php

namespace AdsService\Behaviours;

class Factory
{

    public static $CI       = null;
    public static $contract = null;
    public static $contractId = null;

    public static function build($idOrObject)
    {
        self::$CI =&get_instance();
        $contract = self::$CI->contract_m->set_term_type()->get($idOrObject);

        $enumsDecorators = [
            'webdoctor'     => 'WebFactory', 
            'seo-traffic'   => 'WebFactory', 
            'seo-top'       => 'WebFactory', 
            'webgeneral'    => 'WebFactory',
            'webdesign'     => 'WebFactory',
            'oneweb'        => 'WebFactory',
            'hosting'       => 'WebFactory',
            'domain'        => 'WebFactory',
            'weboptimize'   => 'WebFactory',
            'banner'        => 'WebFactory',
            'fbcontent'     => 'WebFactory',
            'courseads'     => 'WebFactory',
            'onead'         => 'WebFactory',
            'google-ads'    => 'GoogleAdsFactory',
            'facebook-ads'  => 'FacebookAdsFactory',
            'tiktok-ads'    => 'TiktokAdsFactory',
        ];

        $decorator = __NAMESPACE__. '\\' . $enumsDecorators[$contract->term_type];

        if( ! static::isValidDecorator($decorator)) throw new \Exception("Error Processing Request", 1);
        return $decorator::build($idOrObject);
    }

    /**
     * Creates a filter decorator.
     *
     * @param      <type>  $name   The name
     *
     * @return     string  ( description_of_the_return_value )
     */
    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\Filters\\' . str_replace(' ', '', ucwords(str_replace('_', '', $name)));
    }

    /**
     * Determines if valid decorator.
     *
     * @param      <type>   $decorator  The decorator
     *
     * @return     boolean  True if valid decorator, False otherwise.
     */
    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }
}