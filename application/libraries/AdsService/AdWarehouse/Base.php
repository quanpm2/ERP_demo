<?php

namespace AdsService\AdWarehouse;

use Exception;

class Base{
    public $ci = NULL;

    protected $host = NULL;

    public function __construct()
    {
        $this->ci = &get_instance();
        $this->initClass();
    }
    
    /**
     * initClass
     *
     * @return void
     */
    private function initClass(){
        $this->ci->load->config('external_service');
        $adWarehouse = $this->ci->config->item('adWarehouse', 'external_service');
        $this->host = $adWarehouse['host'];
    }
    
    /**
     * get
     *
     * @param  string $url
     * @param  array $data query params
     * @return void
     */
    public function get($url, $data = []){
        $data_json = http_build_query($data);

        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
		curl_close($ch);

        if(isset($error_msg)){
            throw $error_msg;
        }

        $result = @json_decode($result, true) ?? FALSE;

        return $result;
    }

    /**
     * post
     *
     * @param  string $url
     * @param  array $data data update
     * @return void
     */
    public function post($url, $data = []){
        $data_json = json_encode($data);

        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
		curl_close($ch);

        if(isset($error_msg)){
            throw $error_msg;
        }

        $result = @json_decode($result, true) ?? FALSE;

        return $result;
    }

    /**
     * put
     *
     * @param  string $url
     * @param  array $data data update
     * @return void
     */
    public function put($url, $data = []){
        $data_json = json_encode($data);

        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
		curl_close($ch);

        if(isset($error_msg)){
            throw $error_msg;
        }

        $result = @json_decode($result, true) ?? FALSE;

        return $result;
    }
}