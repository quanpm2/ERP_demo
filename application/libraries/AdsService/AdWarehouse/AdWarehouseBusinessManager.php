<?php

namespace AdsService\AdWarehouse;

use AdsService\AdWarehouse\Base;
use Exception;

class AdWarehouseBusinessManager extends Base {
    private $base_url = '';

    public function __construct()
    {
        parent::__construct();

        $api = $this->ci->config->item('adWarehouse', 'api');
        $this->base_url = $api;
    }
    
    /**
     * fetch_bm
     *
     * @param  mixed $system_user_id
     * @return void
     */
    final public function fetch_bm($system_user_id){
        $endpoint = "{$this->base_url}/facebookads/accounts/${system_user_id}/bm/fetch";

        try
        {
            $response = parent::get($endpoint);
        }
        catch(Exception $e)
        {
            return FALSE;
        }

        if(empty($response)) return FALSE;
        
        return $response;
    }
    
    /**
     * store_bm
     *
     * @param  mixed $payload
     * @return void
     */
    final public function store_bm($system_user_id, $payload){
        $endpoint = "{$this->base_url}/facebookads/accounts/${system_user_id}/bm/store";

        try
        {
            $response = parent::post($endpoint, $payload);
        }
        catch(Exception $e)
        {
            return FALSE;
        }

        if(empty($response)) return FALSE;
        
        return $response;
    }
}