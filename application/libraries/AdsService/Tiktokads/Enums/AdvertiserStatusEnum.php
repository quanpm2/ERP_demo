<?php

namespace AdsService\Tiktokads\Enums;

use UnexpectedValueException;

/**
 * Possible statuses of a link.
 */
class AdvertiserStatusEnum
{
    // Disabled
    const STATUS_DISABLE = 0;

    // Application pending review
    const STATUS_PENDING_CONFIRM = 1;

    // Pending verification
    const STATUS_PENDING_VERIFIED = 2;

    // Review failed
    const STATUS_CONFIRM_FAIL = 3;

    // Approved
    const STATUS_ENABLE = 4;

    // CRM system review failed
    const STATUS_CONFIRM_FAIL_END = 5;

    // Modifications pending review
    const STATUS_PENDING_CONFIRM_MODIFY = 6;

    // Review of modifications failed
    const STATUS_CONFIRM_MODIFY_FAIL = 7;

    // Restricted
    const STATUS_LIMIT = 8;

    // Pending CRM system review
    const STATUS_WAIT_FOR_BPM_AUDIT = 9;

    // Pending corporate bank account authentication
    const STATUS_WAIT_FOR_PUBLIC_AUTH = 10;

    // Pending verification of qualifications for self-service account
    const STATUS_SELF_SERVICE_UNAUDITED = 11;

    // Contract has not taken effect
    const STATUS_CONTRACT_PENDING = 12;

    // Unspecified
    const STATUS_UNSPECIFIED = 13;

    // Unknown
    const STATUS_UNKNOWN = -1;

    private static $valueToName = [
        self::STATUS_DISABLE => 'Mất liên kết',
        self::STATUS_PENDING_CONFIRM => 'Đang xem xét',
        self::STATUS_PENDING_VERIFIED => 'Đang xác minh',
        self::STATUS_CONFIRM_FAIL => 'Xem xét thất bại',
        self::STATUS_ENABLE => 'Đang hoạt động',
        self::STATUS_CONFIRM_FAIL_END => 'Tiktok không chấp nhận',
        self::STATUS_PENDING_CONFIRM_MODIFY => 'Chờ xác nhận thay đổi',
        self::STATUS_CONFIRM_MODIFY_FAIL => 'Xác nhận thay đổi thất bại',
        self::STATUS_LIMIT => 'Vượt giới hạn',
        self::STATUS_WAIT_FOR_BPM_AUDIT => 'Chờ Tiktok xem xét',
        self::STATUS_WAIT_FOR_PUBLIC_AUTH => 'Chờ kết nối ngân hàng',
        self::STATUS_SELF_SERVICE_UNAUDITED => 'Chờ kiểm tra tiêu chuẩn',
        self::STATUS_CONTRACT_PENDING => 'Chờ kiểm tra tiêu chuẩn',
        self::STATUS_UNSPECIFIED => 'Chưa có hiệu lực',
        self::STATUS_UNKNOWN => 'Không xác định',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new UnexpectedValueException(sprintf(
                'Enum %s has no name defined for value %s',
                __CLASS__,
                $value
            ));
        }
        return self::$valueToName[$value];
    }

    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new UnexpectedValueException(sprintf(
                'Enum %s has no value defined for name %s',
                __CLASS__,
                $name
            ));
        }
        return constant($const);
    }
}
