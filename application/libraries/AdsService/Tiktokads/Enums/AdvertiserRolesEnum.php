<?php

namespace AdsService\Tiktokads\Enums;

use UnexpectedValueException;

/**
 * Possible statuses of a link.
 */
class AdvertiserRolesEnum
{
    // Standard advertiser (direct customer)
    const ROLE_ADVERTISER = 1;

    // Standard advertiser (agency sub-account)
    const ROLE_CHILD_ADVERTISER = 2;

    // Secondary agency
    const ROLE_CHILD_AGENT = 3;

    // Primary agency
    const ROLE_AGENT = 4;

    private static $valueToName = [
        self::ROLE_ADVERTISER => 'Tài khoản chính',
        self::ROLE_CHILD_ADVERTISER => 'Tài khoản phụ',
        self::ROLE_CHILD_AGENT => 'Tài khoản thứ cấp',
        self::ROLE_AGENT => 'Tài khoản sơ cấp',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new UnexpectedValueException(sprintf(
                'Enum %s has no name defined for value %s',
                __CLASS__,
                $value
            ));
        }
        return self::$valueToName[$value];
    }

    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new UnexpectedValueException(sprintf(
                'Enum %s has no value defined for name %s',
                __CLASS__,
                $name
            ));
        }
        return constant($const);
    }
}
