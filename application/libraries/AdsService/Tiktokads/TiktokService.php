<?php 

namespace AdsService\Tiktokads;

use Exception;
use Requests;

class TiktokService {
    public $CI       = null;

    private $app_id = NULL;
    private $secret = NULL;
    private $version = NULL;
    private $is_sandbox = NULL;

    private $baseEndpoint = NULL;

    public function __construct($arg = [])
    {
        $this->CI = &get_instance();

        $this->CI->config->load('tiktokads/tiktok_app');

        $credential = $this->CI->config->item('credential', 'tiktok_app');
        $default = [
            'app_id' => $credential['app_id'] ?? NULL,
            'secret' => $credential['app_secret'] ?? NULL,
            'version' => $credential['app_version'] ?? NULL,
            'is_sandbox' => false
        ];
        $args = wp_parse_args($arg, $default);

        $this->app_id = $args['app_id'];
        $this->secret = $args['secret'];
        $this->version = $args['version'];
        $this->is_sandbox = $args['is_sandbox'];

        if($this->is_sandbox){
            $this->baseEndpoint = "https://sandbox-ads.tiktok.com/open_api/{$this->version}";
        }
        else{
            $this->baseEndpoint = "https://business-api.tiktok.com/open_api/{$this->version}";
        }
    }

    /**
     * { function_description }
     *
     * @param      int   $contractId  The contract identifier
     */
    public function buildAuthorizationUri($redirect_uri, $state = ''){
        $params = [
            'app_id' => $this->app_id,
            'state' => $state,
            'redirect_uri' => $redirect_uri,
        ];
        $params = http_build_query($params);
        $redirect_url = "https://ads.tiktok.com/marketing_api/auth?" . $params;

        return $redirect_url;
    }

    /**
     * { function_description }
     *
     * @param      int   $contractId  The contract identifier
     */
    public function fetchAccessToken($auth_code){
        $endpointUrl = $this->baseEndpoint . '/oauth2/access_token';
        $header = [
            'Content-Type' => 'application/json',
        ];
        $payload = [
            'app_id' => $this->app_id,
            'secret' => $this->secret,
            'auth_code' => $auth_code
        ];
        
        $request_id = md5(json_encode($payload) . time());

        log_message('debug', "[{$request_id}] Call TiktokService::fetchAccessToken");

        try
        {
            $request = Requests::post($endpointUrl, $header, json_encode($payload));
        }
        catch(Exception $e)
        {
            log_message('error', "[{$request_id}] {$e->getMessage()}");

            return FALSE;
        }

        $response = json_decode($request->body, TRUE);
        
        if(empty($response)) 
        {
            log_message('error', "[{$request_id}] Empty response");

            return FALSE;
        }

        if(0 != $response['code']) 
        {
            $error = json_encode($response);
            log_message('error', "[{$request_id}] Response error. " . $error);

            return FALSE;
        };
        
        $access_token = $response['data']['access_token'];

        return $access_token;
    }

    /**
     * { function_description }
     *
     * @param      int   $contractId  The contract identifier
     */
    public function getUserInfo($access_token){
        $endpointUrl = $this->baseEndpoint . '/user/info';
        $header = [
            'Content-Type' => 'application/json',
            'Access-Token' => $access_token
        ];

        $request_id = md5(json_encode($header) . time());
        log_message('debug', "[{$request_id}] Call TiktokService::getUserInfo");

        try
        {
            $request = Requests::get($endpointUrl, $header);
        }
        catch(Exception $e)
        {
            log_message('error', "[{$request_id}] {$e->getMessage()}");

            return FALSE;
        }

        $response = json_decode($request->body, TRUE);
        
        if(empty($response)) 
        {
            log_message('error', "[{$request_id}] Empty response");

            return FALSE;
        }

        if(0 != $response['code']) 
        {
            $error = json_encode($response);
            log_message('error', "[{$request_id}] Response error. " . $error);

            return FALSE;
        };

        log_message('debug', "[{$request_id}] " . json_encode($response));
        
        $data = $response['data'];

        return $data;
    }

    /**
     * { function_description }
     *
     * @param      int   $contractId  The contract identifier
     */
    public function listAdvertiser($access_token){
        $header = [
            'Content-Type' => 'application/json',
            'Access-Token' => $access_token
        ];
        $params = [
            'app_id' => $this->app_id,
            'secret' => $this->secret,
        ];
        $params = http_build_query($params);

        $request_id = md5($params . time());
        log_message('debug', "[{$request_id}] Call TiktokService::listAdvertiser");

        $endpointUrl = $this->baseEndpoint . "/oauth2/advertiser/get?{$params}";
        try
        {
            $request = Requests::get($endpointUrl, $header);
        }
        catch(Exception $e)
        {
            log_message('error', "[{$request_id}] Call {$e->getMessage()}");

            return FALSE;
        }

        $response = json_decode($request->body, TRUE);

        if(empty($response)) 
        {
            log_message('error', "[{$request_id}] Empty response");

            return FALSE;
        }

        if(0 != $response['code']) 
        {
            $error = json_encode($response);
            log_message('error', "[{$request_id}] Response error. " . $error);

            return FALSE;
        };

        log_message('debug', "[{$request_id}] " . json_encode($response));
        
        $advertisers = $response['data']['list'] ?? FALSE;
        return $advertisers;
    }

    /**
     * { function_description }
     *
     * @param      int   $contractId  The contract identifier
     */
    public function listAdvertiserDetail($access_token, $advertiser_ids){
        $header = [
            'Access-Token' => $access_token
        ];
        $params = [
            'advertiser_ids' => json_encode($advertiser_ids),
        ];
        $params = http_build_query($params);

        $request_id = md5($params . time());
        log_message('debug', "[{$request_id}] Call TiktokService::listAdvertiserDetail");

        $endpointUrl = $this->baseEndpoint . "/advertiser/info?{$params}";
        try
        {
            $request = Requests::get($endpointUrl, $header);
        }
        catch(Exception $e)
        {
            log_message('error', "[{$request_id}] {$e->getMessage()}");

            return FALSE;
        }

        $response = json_decode($request->body, TRUE);
        
        if(empty($response)) 
        {
            log_message('error', "[{$request_id}] Empty response");

            return FALSE;
        }

        if(0 != $response['code']) 
        {
            $error = json_encode($response);
            log_message('error', "[{$request_id}] Response error. " . $error);

            return FALSE;
        };


        log_message('debug', "[{$request_id}] " . json_encode($response));
        
        $data = $response['data']['list'] ?? FALSE;
        return $data;
    }
}