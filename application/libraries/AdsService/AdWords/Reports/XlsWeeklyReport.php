<?php

/**
 * Notifications Lead Created Internal Event
 *
 * @package Notifications/ACN
 * @author  thonh@webdoctor.vn
 * @since   Version 1.0.0
 */

namespace AdsService\AdWords\Reports;

use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\ReportSettingsBuilder;

use Google\AdsApi\AdWords\Reporting\v201809\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinitionDateRangeType;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloader;

use Google\AdsApi\AdWords\v201809\cm\LocationCriterionService;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201809\cm\ReportDefinitionReportType;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\DateRange;
use Google\AdsApi\Common\OAuth2TokenBuilder;

class XlsWeeklyReport
{
    /**
     * { var_description }
     *
     * @var        <type>
     */
    public $contractId = null;

    /**
     * { var_description }
     *
     * @var        <type>
     */
    public $CI       = null;

    /**
     * Report Insight data Start-Point
     *
     * @var        Integer
     */
    public $start_time  = null;


    /**
     * Report Insight data End-Point
     *
     * @var        Integer
     */
    public $end_time    = null;


    private $objPHPExcel = null;

    /**
     * Constructs a new instance.
     *
     * @param      array  $args   The arguments
     */
    function __construct($contractId, $start, $end)
    {
        $this->CI = &get_instance();
        $this->CI->load->models([
            'user_m',
            'option_m',
            'contract/contract_m',
            'term_users_m',
            'message/message_m',
            'user_m',
            'option_m',
            'contract/contract_m',
            'term_users_m',
            'message/message_m',
            'ads_segment_m',
            'googleads/base_adwords_m'
        ]);

        $this->CI->load->library('googleads/adwords');
        $this->CI->load->library('excel');
        $this->CI->load->config('googleads/adwords');
        $this->CI->load->model('googleads/mcm_client_m');
        $this->CI->load->model('googleads/mcm_account_m');
        $this->CI->load->model('googleads/mcm_account_clients_m');

        $this->CI->googleads_m->set_contract($contractId);
        $this->contract = $this->CI->googleads_m->get_contract();

        $this->start_time = $start;
        $this->end_time = $end;
    }

    public function build()
    {
        $adsSegments = $this->CI->term_posts_m->get_term_posts($this->contract->term_id, $this->CI->ads_segment_m->post_type);
        if(empty($adsSegments)) throw new \Exception('Hợp đồng chưa được cấu hình phân đoạn.');

        $campaigns = $adgroups = $keywords = $geo = [];

        foreach ($adsSegments as $adsSegment)
        {
            $adaccount = $this->CI->term_posts_m->get_post_terms($adsSegment->post_id, $this->CI->mcm_account_m->term_type);
            if(empty($adaccount)) continue;

            $adaccount      = reset($adaccount);

            $_start_time    = max($adsSegment->start_date, $this->start_time);
            $_segment_end   = $adsSegment->end_date ?: time();
            $_end_time      = min($_segment_end, $this->end_time);
            if($_start_time > $_segment_end) continue;

            $this->CI->base_adwords_m->set_mcm_account($adaccount->term_id);
            $client_customer_id = $this->CI->base_adwords_m->get_client_customer_id();

            $this->CI->base_adwords_m->download('CAMPAIGN_PERFORMANCE_REPORT', $_start_time, $_end_time);
            $_campaigns = $this->CI->base_adwords_m->get_cache('CAMPAIGN_PERFORMANCE_REPORT', $_start_time, $_end_time);
            $_campaigns AND $_campaigns = array_filter($_campaigns);
            if( ! empty($_campaigns))
            {
                foreach ($_campaigns as $timestamp => $_items)
                {
                    if(empty($_items)) continue;
                    $campaigns = array_merge($campaigns, $_items);
                }    
            }

            $_adgroups = $this->CI->base_adwords_m->get_range_cache('ADGROUP_PERFORMANCE_REPORT', $_start_time, $_end_time);
            if(empty($_adgroups))
            {
                $selectorFields = $this->CI->base_adwords_m->init_selector_fields('ADGROUP_PERFORMANCE_REPORT')->get_selector_fields();
                if (($key = array_search('Date', $selectorFields)) !== false) unset($selectorFields[$key]);
                $this->CI->base_adwords_m->set_selector_fields($selectorFields)->download('ADGROUP_PERFORMANCE_REPORT', $_start_time, $_end_time);
                $_adgroups = $this->CI->base_adwords_m->get_range_cache('ADGROUP_PERFORMANCE_REPORT', $_start_time, $_end_time);
            }
            $_adgroups AND $adgroups = array_merge($_adgroups, $adgroups);


            $_keywords = $this->CI->base_adwords_m->get_range_cache('KEYWORDS_PERFORMANCE_REPORT', $_start_time, $_end_time);
            if(empty($_keywords))
            {
                $selectorFields = $this->CI->base_adwords_m->init_selector_fields('KEYWORDS_PERFORMANCE_REPORT')->get_selector_fields();
                if (($key = array_search('Date', $selectorFields)) !== false) unset($selectorFields[$key]);
                $_keywords = $this->CI->base_adwords_m->set_selector_fields($selectorFields)->download('KEYWORDS_PERFORMANCE_REPORT', $_start_time, $_end_time);
                $_keywords = $this->CI->base_adwords_m->get_range_cache('KEYWORDS_PERFORMANCE_REPORT', $_start_time, $_end_time);
            }
            $_keywords AND $keywords = array_merge($_keywords, $keywords);


            $this->CI->base_adwords_m->download('GEO_PERFORMANCE_REPORT', $_start_time, $_end_time);
            $_geo = $this->CI->base_adwords_m->get_cache('GEO_PERFORMANCE_REPORT', $_start_time, $_end_time);
            $_geo AND $_geo = array_filter($_geo);
            if( ! empty($_geo))
            {
                foreach ($_geo as $timestamp => $_items)
                {
                    if(empty($_items)) continue;
                    $geo = array_merge($geo, $_items);
                }    
            }
        }

        $cacheMethod = \PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '512MB');
        \PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $this->objPHPExcel = new \PHPExcel();
        $this->objPHPExcel->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle('Campaign Report');
        $this->objPHPExcel = \PHPExcel_IOFactory::load('./files/report_by_account_template_2.xlsx');

        $this->renderCampaignSheet($campaigns);
        $this->renderAdgroupSheet($adgroups);
        $this->renderKeywordsSheet($keywords);
        $this->renderGeoSheet($geo);

        $objWriter = \PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
        $objWriter->setIncludeCharts(TRUE);

        $folder_upload = "files/google_adword/account_type/{$this->contract->term_id}";
        if(!is_dir($folder_upload))
        {
            try 
            {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            }
            catch (\Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $ext = 'xlsx';
        $filename = $folder_upload.'/';
        $filename.= my_date($this->start_time,'Y-m-d').'_to_'.my_date($this->end_time,'Y-m-d').'_created_on_'.my_date(0,'Y-m-d-H-i-s').'.'.$ext;
        
        try 
        {
            $objWriter->save($filename);
        }
        catch (\Exception $e) 
        {
            trigger_error($e->getMessage());
            return FALSE; 
        }

        return $filename;
    }


    /**
     * Build campaign-performance-report sheet
     *
     * @param      <type>  $this->objPHPExcel  The object php excel
     * @param      <type>  $start_time   The start time
     * @param      <type>  $end_time     The end time
     *
     * @return     $this->objPHPExcel
     */
    public function renderCampaignSheet($data)
    {
        $output_array   = array();
        $chart_data     = array();

        $data = array_group_by($data, 'day');
        foreach ($data as $day => $campaigns) 
        {
            $timestamp = start_of_day($day);
            if(empty($campaigns)) continue;

            $day = my_date($timestamp,'d/m/Y');

            # init data for chart
            $clicks = array_sum(array_column($campaigns,'clicks'));
            $invalidClicks = array_sum(array_column($campaigns,'invalidClicks'));
            $impressions = array_sum(array_column($campaigns,'impressions'));
            $ctr = $impressions ? number_format(div($clicks,$impressions), 2) : 0;
            // $avg_pos_index = array_sum(array_map(function($x){return $x['impressions']*$x['avgPosition'];}, $campaigns));
            // $avgPosition = $impressions ? number_format(div($avg_pos_index,$impressions),1) : 0;
            $cost = array_sum(array_column($campaigns,'cost'));
            $avgCPC = div($cost,$clicks);

            $chart_data[] = array(
                'day' => my_date($timestamp,'d/m'),
                'impressions' => $impressions,
                'clicks' => $clicks,
                'invalidClicks' => $invalidClicks,
                'ctr' => $ctr,
                // 'avgPosition' => $avgPosition,
                'avgCPC' => $avgCPC,
                'cost' => $cost
                );

            $campaigns = array_group_by($campaigns,'campaignID');
            foreach ($campaigns as $campaignID => $camps)
            {
                if(empty($camps)) continue;

                $base_camp = reset($camps);

                # init data for chart
                $clicks = array_sum(array_column($camps,'clicks'));
                $invalidClicks = array_sum(array_column($camps,'invalidClicks'));
                $impressions = array_sum(array_column($camps,'impressions'));
                $ctr = $impressions ? number_format(div($clicks,$impressions), 2) : 0;
                // $avg_pos_index = array_sum(array_map(function($x){return $x['impressions']*$x['avgPosition'];}, $camps));
                // $avgPosition = $impressions ? number_format(div($avg_pos_index,$impressions),1) : 0;
                $cost = array_sum(array_column($camps,'cost'));
                $avgCPC = div($cost,$clicks);

                $imprAbsTop = 0;
                empty($base_camp['imprAbsTop']) OR $imprAbsTop = (double) $base_camp['imprAbsTop'];

                $imprTop = 0;
                empty($base_camp['imprTop']) OR $imprTop = (double) $base_camp['imprTop'];

                $output_array[] = array(
                    my_date($timestamp,'d/m/Y'),
                    $base_camp['campaign'],
                    $impressions,
                    $clicks,
                    $invalidClicks,
                    $ctr,
                    // $avgPosition,
                    $imprAbsTop,
                    $imprTop,
                    $base_camp['campaignState'],
                    $this->format_money($avgCPC),
                    $this->format_money($cost)
                );
            }
        }

        if(empty($output_array))
        {
            trigger_error('EMPTY CAMPAIGNS !');
            return FALSE;
        }

        $start_date = my_date($this->start_time,'d/m/Y');
        $end_date = my_date($this->end_time,'d/m/Y');
        $total = array();
        $total['clicks'] = array_sum(array_column($chart_data,'clicks'));
        $total['invalidClicks'] = array_sum(array_column($chart_data,'invalidClicks'));
        $total['impressions'] = array_sum(array_column($chart_data,'impressions'));

        $total['ctr'] = $total['impressions'] ? number_format(div($total['clicks'],$total['impressions']) * 100, 2) : 0;
        $total['cost'] = array_sum(array_column($chart_data,'cost'));
        $total['avgCPC'] = $total['clicks'] ? div($total['cost'],$total['clicks']) : 0;

        $total_and_overall = array(
            'Totals and Overall Averages:',
            '',
            $total['impressions'],
            $total['clicks'],
            $total['invalidClicks'],
            $total['ctr'].'%',
            null,
            null,
            '', // campaign status
            $this->format_money($total['avgCPC']),
            $this->format_money($total['cost'])
        );

        $output_array[] = $total_and_overall;

        $headings = array('Date','Campaign','Impressions','Clicks','Invalid Clicks','CTR', 'Impr Abs Top', 'impr Top', 'Campaign Status', 'Avg.CPC', 'Cost');

        /* Create a copy raw data of report */
        $raw_cache = array();
        $raw_cache['headings'] = $headings;
        $raw_cache['rows'] = array_map(function($x){return array_values($x);}, $output_array);

        $client_customer_id = $this->CI->base_adwords_m->get_client_customer_id();
        $week_data_cache_key = $client_customer_id.'/campaign_'.my_date($this->start_time,'Y-m-d').'_to_'.my_date($this->end_time,'Y-m-d');
        $this->CI->scache->write($raw_cache,'google-adword/week_data/'.$week_data_cache_key);
        /* Finished create the coppy raw data*/

        array_unshift($output_array,$headings);

        $this->objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $this->objPHPExcel->getActiveSheet();

        $start_coordinate = array('x' => 1,'y' => 13);

        $objWorksheet->fromArray($output_array, NULL, 'A' . $start_coordinate['y'],TRUE);
        $objWorksheet->fromArray($chart_data,NULL,'L14',TRUE);
        unset($output_array[count($output_array)-1]);

        $style['borders'] = array('borders' => array('allborders' => array('style' => \PHPExcel_Style_Border::BORDER_THIN)));

        $this->objPHPExcel
        ->getActiveSheet()
        ->getStyle('A' . $start_coordinate['y'] . ':K' . ($start_coordinate['y'] + count($output_array)))
        ->applyFromArray($style['borders']);

        $this->objPHPExcel
        ->getActiveSheet()
        ->getStyle('A' . $start_coordinate['y'] . ':K' . ($start_coordinate['y'] + count($output_array)))
        ->getFont()
        ->setSize(10);
   
        $this->objPHPExcel
        ->getActiveSheet()
        ->getStyle('F' . $start_coordinate['y'] . ':H' . ($start_coordinate['y'] + count($output_array)))
        ->getNumberFormat()
        ->applyFromArray(array('code' => \PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00));

        $this->objPHPExcel
        ->getActiveSheet()
        ->getStyle('P' . $start_coordinate['y'] . ':P' . (14 + count($chart_data)))         
        ->getNumberFormat()
        ->applyFromArray(array( 'code' => \PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00 ));

        $this->objPHPExcel
        ->getActiveSheet()
        ->getStyle('L' . $start_coordinate['y'] . ':R' . (14 + count($chart_data)))
        ->applyFromArray(array('font'  => array('color' => array('rgb' => 'FFFFFF'))));

        $objWorksheet->setCellValue('B10', $start_date.'-'.$end_date);

        $dataseriesLabels1 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'campaign!$C$' . $start_coordinate['y'], NULL, 1),   # Impressions
            new \PHPExcel_Chart_DataSeriesValues('String', 'campaign!$D$' . $start_coordinate['y'], NULL, 1)   # Clicks
            );

        $xAxisTickValues1 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'campaign!$L$14:$L$'.(14 + count($chart_data)), NULL,(14 + count($chart_data)))
            );

        $dataSeriesValues1 = array(
            new \PHPExcel_Chart_DataSeriesValues('Number', 'campaign!$M$14:$M$'.(14 + count($chart_data)), NULL, (14 + count($chart_data))),
            new \PHPExcel_Chart_DataSeriesValues('Number', 'campaign!$N$14:$N$'.(14 + count($chart_data)), NULL, (14 + count($chart_data)))
            );

        $series1 = new \PHPExcel_Chart_DataSeries(
            \PHPExcel_Chart_DataSeries::TYPE_LINECHART,
            \PHPExcel_Chart_DataSeries::GROUPING_STANDARD,
            range(0, count($dataSeriesValues1)-1),
            $dataseriesLabels1,
            $xAxisTickValues1,
            $dataSeriesValues1
            );

        $plotarea1 = new \PHPExcel_Chart_PlotArea(NULL, array($series1));
        $legend1 = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_TOPRIGHT, NULL, false);
        $title1 = new \PHPExcel_Chart_Title('Impressions & Clicks');
        $yAxisLabel1 = new \PHPExcel_Chart_Title('');

        # Create the chart
        $chart1 = new \PHPExcel_Chart(
            'chart1',       # name
            $title1,        # title
            $legend1,       # legend
            $plotarea1,     # plotArea
            true,           # plotVisibleOnly
            0,              # displayBlanksAs
            NULL,           # xAxisLabel
            $yAxisLabel1    # yAxisLabel
            );

        # Set the position where the chart should appear in the worksheet
        $yAxisChart = ($start_coordinate['y'] + count($output_array) + 17);
        $xAxisChart = 1;
        $chart1->setTopLeftPosition('A' . ($yAxisChart - 15));
        $chart1->setBottomRightPosition('F' . $yAxisChart);

        // Add the chart to the worksheet
        $objWorksheet->addChart($chart1);

        $dataseriesLabels2 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'campaign!$E$' . $start_coordinate['y'], NULL, 1)
            );       
        $xAxisTickValues2 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'campaign!$L$14:$L$'.(14 + count($chart_data)), NULL,(14 + count($chart_data)))
            );
        $dataSeriesValues2 = array(
            new \PHPExcel_Chart_DataSeriesValues('Number', 'campaign!$O$14:$O$'.(14 + count($chart_data)), NULL, (14 + count($chart_data)))
            );
        $series2 = new \PHPExcel_Chart_DataSeries(
            \PHPExcel_Chart_DataSeries::TYPE_LINECHART,       
            \PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   
            range(0, count($dataSeriesValues2)-1),          
            $dataseriesLabels2,                             
            $xAxisTickValues2,                             
            $dataSeriesValues2);

        $series2->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_COL);       
        $plotarea2 = new \PHPExcel_Chart_PlotArea(NULL, array($series2));  

        $legend2 = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
        $title2 = new \PHPExcel_Chart_Title('Invalid clicks');
        $yAxisLabel2 = new \PHPExcel_Chart_Title('');
        $chart2 = new \PHPExcel_Chart(
            'chart2',       // name
            $title2,        // title
            $legend2,       // legend
            $plotarea2,     // plotArea
            true,           // plotVisibleOnly
            0,              // displayBlanksAs
            NULL,           // xAxisLabel
            $yAxisLabel2    // yAxisLabel
            );

        # Set the position where the chart should appear in the worksheet
        $yAxisChart = ($start_coordinate['y'] + count($output_array) + 17);
        $xAxisChart = 1;
        $chart2->setTopLeftPosition('F' . ($yAxisChart - 15));
        $chart2->setBottomRightPosition('L' . $yAxisChart);

        # Add the chart to the worksheet
        $objWorksheet->addChart($chart2);

        # Save Excel 2007 file
        # Chart for CTR
        $dataseriesLabels3 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'campaign!$F$' . $start_coordinate['y'], NULL, 1)
            );
        $xAxisTickValues3 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'campaign!$L$14:$L$'.(14 + count($chart_data)), NULL,(14 + count($chart_data)))
            );
        $dataSeriesValues3 = array(
            new \PHPExcel_Chart_DataSeriesValues('Number', 'campaign!$P$14:$P$'.(14 + count($chart_data)), NULL, (14 + count($chart_data)))
            );
        $series3 = new \PHPExcel_Chart_DataSeries(
            \PHPExcel_Chart_DataSeries::TYPE_LINECHART,       // plotType
            \PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   // plotGrouping
            range(0, count($dataSeriesValues3)-1),          // plotOrder
            $dataseriesLabels3,                             // plotLabel
            $xAxisTickValues3,                              // plotCategory
            $dataSeriesValues3                              // plotValues
            );
        $series3->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_COL);

        $plotarea3 = new \PHPExcel_Chart_PlotArea(NULL, array($series3));
        $legend3 = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
        $title3 = new \PHPExcel_Chart_Title('Online CTR');
        $yAxisLabel3 = new \PHPExcel_Chart_Title('%');
        $chart3 = new \PHPExcel_Chart(
            'chart3',       // name
            $title3,        // title
            $legend3,       // legend
            $plotarea3,     // plotArea
            true,           // plotVisibleOnly
            0,              // displayBlanksAs
            NULL,           // xAxisLabel
            $yAxisLabel3    // yAxisLabel
            );

        $xAxisChart = $yAxisChart + 2;
        $chart3->setTopLeftPosition('A' . $xAxisChart);
        $chart3->setBottomRightPosition('F' . ($xAxisChart + 15));
        $objWorksheet->addChart($chart3);

        return $this->objPHPExcel;
    }

    /**
     * Build adgroup-performance-report sheet
     *
     * @param      <type>  $this->objPHPExcel  The object php excel
     * @param      <type>  $start_time   The start time
     * @param      <type>  $end_time     The end time
     *
     * @return     $this->objPHPExcel
     */
    public function renderAdgroupSheet($data)
    {
        $output_array = array_map(function($x){
            return array(
                'campaign'      => $x['campaign'],
                'adGroup'       => $x['adGroup'],
                'impressions'   => $x['impressions'],
                'clicks'        => $x['clicks'],
                'ctr'           => $x['ctr'],
                'imprAbsTop'    => $x['imprAbsTop'],
                'imprTop'       => $x['imprTop'],
                'avgCPC'        => $x['avgCPC'],
                'cost'          => $x['cost']
            );
        }, $data);
        
        if(empty($output_array))
        {
            trigger_error('EMPTY ADGROUPS !');
            return FALSE;
        }

        $start_date = my_date($this->start_time, 'd/m/Y');
        $end_date = my_date($this->end_time, 'd/m/Y');
        
        $total = array();
        $total['clicks'] = array_sum(array_column($output_array,'clicks'));
        $total['impressions'] = array_sum(array_column($output_array,'impressions'));
        $total['ctr'] = $total['impressions'] ? number_format(div($total['clicks'],$total['impressions']) * 100, 2) : 0;
        // $total['avg_pos_index'] = array_sum(array_map(function($x){return $x['impressions']*$x['avgPosition'];}, $output_array));
        // $total['avgPosition'] = $total['impressions'] ? number_format(div($total['avg_pos_index'],$total['impressions']),1) : 0;

        $total['cost'] = array_sum(array_column($output_array,'cost'));
        $total['avgCPC'] = $total['clicks'] ? div($total['cost'],$total['clicks']) : 0;

        $total_and_overall = array(
            'Totals and Overall Averages:',
            '',
            $total['impressions'],
            $total['clicks'],
            $total['ctr'].'%',
            null,
            null,
            $this->format_money($total['avgCPC']),
            $this->format_money($total['cost'])
        );

        $output_array = array_map(function($x){
            $x['avgCPC'] = $this->format_money($x['avgCPC']);
            $x['cost'] = $this->format_money($x['cost']);
            return $x;
        }, $output_array);

        usort($output_array, function($a, $b){
            if(strcasecmp($a['campaign'], $b['campaign']) == 0)
                return strcasecmp($a['adGroup'], $b['adGroup']);
            return strcasecmp($a['campaign'], $b['campaign']);
        });

        $output_array[] = $total_and_overall;
        $headings = array('Campaign','Adgroup', 'Impressions', 'Clicks' , 'CTR', 'Impr Abs Top', 'impr Top', 'Avg.CPC','Cost');

        /* Create a copy raw data of report */
        $raw_cache = array();
        $raw_cache['headings'] = $headings;
        $raw_cache['rows'] = array_map(function($x){return array_values($x);}, $output_array);

        $client_customer_id = $this->CI->base_adwords_m->get_client_customer_id();
        $week_data_cache_key = $client_customer_id.'/adgroup_'.my_date($this->start_time, 'Y-m-d').'_to_'.my_date($this->end_time, 'Y-m-d');
        $this->CI->scache->write($raw_cache,'google-adword/week_data/'.$week_data_cache_key);
        /* Finished create the coppy raw data*/

        array_unshift($output_array,$headings);

        $this->objPHPExcel->setActiveSheetIndex(1); 
        $objWorksheet = $this->objPHPExcel->getActiveSheet(1);
        $start_coordinate = array('y' => 13,'x' => '1');
        $objWorksheet->fromArray($output_array, NULL, 'A' . 13,true);

        $style['borders'] = array('borders' => array('allborders' => array('style' => \PHPExcel_Style_Border::BORDER_THIN)));

        $this->objPHPExcel
        ->getActiveSheet()
        ->getStyle('A' . $start_coordinate['y'] . ':I' . ($start_coordinate['y'] + count($output_array)))
        ->applyFromArray($style['borders']);

        $this->objPHPExcel
        ->getActiveSheet()
        ->getStyle('A' . $start_coordinate['y'] . ':I' . ($start_coordinate['y'] + count($output_array)))
        ->getFont()
        ->setSize(10);

        $this->objPHPExcel
        ->getActiveSheet()
        ->getStyle('A' . $start_coordinate['y'] . ':I' . ($start_coordinate['y'] + count($output_array)))
        ->getAlignment()
        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $this->objPHPExcel
        ->getActiveSheet()
        ->getStyle('F' . $start_coordinate['y'] . ':G' . ($start_coordinate['y'] + count($output_array)))
        ->getNumberFormat()
        ->applyFromArray(array( 'code' => \PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00 ));

        $objWorksheet->setCellValue('B10', $start_date.'-'.$end_date);

        $dataseriesLabels1 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'adgroup!$C$' . $start_coordinate['y'], NULL, 1),
            new \PHPExcel_Chart_DataSeriesValues('String', 'adgroup!$D$' . $start_coordinate['y'], NULL, 1)
            );

        $xAxisTickValues1 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'adgroup!$B$' . ($start_coordinate['y'] + 1) . ':$B$'.($start_coordinate['y'] + count($output_array) -2), NULL, ($start_coordinate['y'] + count($output_array) -2))
            );

        $dataSeriesValues1 = array(
            new \PHPExcel_Chart_DataSeriesValues('Number', 'adgroup!$C$' . ($start_coordinate['y'] + 1) . ':$C$'.($start_coordinate['y'] + count($output_array) -2), NULL, ($start_coordinate['y'] + count($output_array)-2)),
            new \PHPExcel_Chart_DataSeriesValues('Number', 'adgroup!$D$' . ($start_coordinate['y'] + 1) . ':$D$'.($start_coordinate['y'] + count($output_array) -2), NULL, ($start_coordinate['y'] + count($output_array) -2))
            );

        //  Build the dataseries
        $series1 = new \PHPExcel_Chart_DataSeries(
            \PHPExcel_Chart_DataSeries::TYPE_BARCHART,
            \PHPExcel_Chart_DataSeries::GROUPING_STANDARD,
            range(0, count($dataSeriesValues1)-1),
            $dataseriesLabels1,
            $xAxisTickValues1,
            $dataSeriesValues1
            );

        $series1->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_COL);

        # Set the series in the plot area
        $plotarea1 = new \PHPExcel_Chart_PlotArea(NULL, array($series1));

        # Set the chart legend
        $legend1 = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_TOPRIGHT, NULL, false);
        $title1 = new \PHPExcel_Chart_Title('Impressions & Clicks');
        $yAxisLabel1 = new \PHPExcel_Chart_Title('');

        # Create the chart
        $chart1 = new \PHPExcel_Chart(
            'chart1',
            $title1, 
            $legend1, 
            $plotarea1, 
            true, 
            0, 
            NULL, 
            $yAxisLabel1  
            );

        # Set the position where the chart should appear in the worksheet
        $yAxisChart = ($start_coordinate['y'] + count($output_array) + 17);
        $xAxisChart = 1;
        $chart1->setTopLeftPosition('A' . ($yAxisChart - 15));
        $chart1->setBottomRightPosition('I' . ($yAxisChart + 10));
        $objWorksheet->addchart($chart1);
        return $this->objPHPExcel;
    }

    /**
     * Build keywords-performance-report sheet
     *
     * @param      <type>  $this->objPHPExcel  The object php excel
     * @param      <type>  $start_time   The start time
     * @param      <type>  $end_time     The end time
     *
     * @return     $this->objPHPExcel
     */
    public function renderKeywordsSheet($data)
    {
        $output_array = array_map(function($x){
            return array(
                'campaign'      => $x['campaign'],
                'adGroup'       => $x['adGroup'],
                'keyword'       => $x['keyword'],
                'impressions'   => $x['impressions'],
                'clicks'        => $x['clicks'],
                'ctr'           => $x['ctr'],    
                'imprAbsTop'    => $x['imprAbsTop'],
                'imprTop'       => $x['imprTop'],
                'avgCPC'        => $x['avgCPC'],
                'cost'          => $x['cost'],
            );
        }, $data);

        if(empty($output_array))
        {
            trigger_error('EMPTY KEYWORDS !');
            return FALSE;
        }

        $start_date = my_date($this->start_time,'d/m/Y');
        $end_date = my_date($this->end_time,'d/m/Y');
        
        $total = array();
        $total['clicks'] = array_sum(array_column($output_array,'clicks'));
        $total['impressions'] = array_sum(array_column($output_array,'impressions'));
        $total['ctr'] = $total['impressions'] ? number_format(div($total['clicks'],$total['impressions']) * 100, 2) : 0;
        $total['cost'] = array_sum(array_column($output_array,'cost'));
        $total['avgCPC'] = $total['clicks'] ? div($total['cost'],$total['clicks']) : 0;

        $total_and_overall = array(
            'Totals and Overall Averages:',
            '',
            '',
            $total['impressions'],
            $total['clicks'],
            $total['ctr'].'%',
            null,
            null,
            $this->format_money($total['avgCPC']),
            $this->format_money($total['cost'])
            );

        $output_array = array_map(function($x){
            $x['avgCPC'] = $this->format_money($x['avgCPC']);
            $x['cost'] = $this->format_money($x['cost']);
            return $x;
        }, $output_array);

        usort($output_array, function($a, $b)
        {
            if(strcasecmp($a['campaign'], $b['campaign']) == 0)
            {
                if(strcasecmp($a['adGroup'], $b['adGroup']) == 0)
                {
                    return strcasecmp($a['keyword'], $b['keyword']);  
                }
                return strcasecmp($a['adGroup'], $b['adGroup']);  
            }
            return strcasecmp($a['campaign'], $b['campaign']);
        });

        $output_array[] = $total_and_overall;
        $headings = array('Campaign','Adgroup', 'Keyword', 'Impressions', 'Clicks' , 'CTR', 'Impr Abs Top', 'impr Top','Avg.CPC','Cost');

        /* Create a copy raw data of report */
        $raw_cache = array();
        $raw_cache['headings'] = $headings;
        $raw_cache['rows'] = array_map(function($x){return array_values($x);}, $output_array);

        $client_customer_id = $this->CI->base_adwords_m->get_client_customer_id();
        $week_data_cache_key = $client_customer_id.'/keyword_'.my_date($this->start_time,'Y-m-d').'_to_'.my_date($this->end_time,'Y-m-d');
        $this->CI->scache->write($raw_cache,'google-adword/week_data/'.$week_data_cache_key);
        /* Finished create the coppy raw data*/

        array_unshift($output_array,$headings);

        $this->objPHPExcel->setActiveSheetIndex(2); 
        $objWorksheet = $this->objPHPExcel->getActiveSheet();
        $objWorksheet->fromArray($output_array, NULL, 'A' . 13,true);

        $style['borders'] = array('borders' => array('allborders' => array('style' => \PHPExcel_Style_Border::BORDER_THIN)));
        $this->objPHPExcel->getActiveSheet()->getStyle('A13:J' . (13 + count($output_array)))->applyFromArray($style['borders']);
        $this->objPHPExcel->getActiveSheet()->getStyle('A13:J' . (13 + count($output_array)))->getFont()->setSize(10);
        $objWorksheet->setCellValue('B10', $start_date.'-'.$end_date);

        $this->objPHPExcel->getActiveSheet()
        ->getStyle('G13:H' . (13 + count($output_array)))
        ->getNumberFormat()
        ->applyFromArray(array('code' => \PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00));

        return $this->objPHPExcel;
    }

    /**
     * Build geo-performance-report sheet
     *
     * @param      <type>  $this->objPHPExcel  The object php excel
     * @param      <type>  $start_time   The start time
     * @param      <type>  $end_time     The end time
     *
     * @return     $this->objPHPExcel
     */
    public function renderGeoSheet($data)
    {
        $geo_report         = array_group_by($data, 'day');
        $output_array       = array();
        $chart_data         = array();
        $location_places    = array();

        # group by mostSpecificLocationId per day
        foreach ($geo_report as $day => $locations) 
        {
            if(empty($locations)) continue;

            $timestamp = start_of_day(convert_time($day));

            $mostSpecificLocations = array();
            foreach ($locations as $location)
            {
                $mostSpecificLocationId = $location['mostSpecificLocation'];
                if(!isset($mostSpecificLocations[$mostSpecificLocationId]))
                    $mostSpecificLocations[$mostSpecificLocationId] = array();

                $mostSpecificLocations[$mostSpecificLocationId][] = $location;
                
                $countryTerritory = $location['countryTerritory'];
                if(!isset($location_places[$countryTerritory]))
                    $location_places[$countryTerritory] = $countryTerritory;

                if(!isset($location_places[$mostSpecificLocationId]))
                    $location_places[$mostSpecificLocationId] = $mostSpecificLocationId;
            }

            $geo_report[$day] = array();

            foreach ($mostSpecificLocations as $key => $items) 
            {
                if(empty($items)) continue;

                $basic_instance = reset($items);

                $impressions = array_sum(array_column($items,'impressions'));
                $clicks = array_sum(array_column($items,'clicks'));
                $ctr = $impressions ? number_format(div($clicks,$impressions) * 100, 2) : 0;
                $cost = array_sum(array_column($items,'cost'));
                $avgCPC = div($cost,$clicks);

                $geo_report[$day][$key] = array(
                    'campaign' => $basic_instance['campaign'],
                    'city' => $basic_instance['city'],
                    'countryTerritory' => $basic_instance['countryTerritory'],
                    'region' => $basic_instance['region'],
                    'adGroup' => $basic_instance['adGroup'],
                    'network' => $basic_instance['network'],
                    'mostSpecificLocation' => $key,
                    'day' => my_date($timestamp, 'd/m/Y'),
                    'impressions' => $impressions,
                    'clicks' => $clicks,
                    'cost' => $cost,
                    'ctr' => $ctr,
                    'avgCPC' => $avgCPC,
                );
            }
        }

        $location_criterions = $this->CI->base_adwords_m->get_location_criterions([
            'critetiaIds' => array_values($location_places)
        ]);

        foreach ($geo_report as $rows) 
        {
            foreach ($rows as $row) 
            {
                $countryTerritory   = $location_criterions[$row['countryTerritory']] ?? '--';
                $placement          = $location_criterions[$row['mostSpecificLocation']] ?? '--';
                $output_array[] = array(
                    'day' => $row['day'],
                    'campaign' => $row['campaign'],
                    'countryTerritory' => $countryTerritory,
                    'mostSpecificLocation' => $placement,
                    'impressions' => $row['impressions'],
                    'clicks' => $row['clicks'],
                    'ctr' => $row['ctr'].'%',
                    'avgCPC' => $row['avgCPC'],
                    'cost' => $row['cost'],
                    );
            }
        }

        if(empty($output_array))
        {
            trigger_error('EMPTY GEO !');
            return FALSE;
        }

        $start_date = my_date($this->start_time,'d/m/Y');
        $end_date = my_date($this->end_time,'d/m/Y');
        
        $total = array();
        $total['clicks'] = array_sum(array_column($output_array,'clicks'));
        $total['impressions'] = array_sum(array_column($output_array,'impressions'));
        $total['ctr'] = $total['impressions'] ? number_format(div($total['clicks'],$total['impressions']) * 100, 2) : 0;
        $total['cost'] = array_sum(array_column($output_array,'cost'));
        $total['avgCPC'] = $total['clicks'] ? div($total['cost'],$total['clicks']) : 0;
        $total_and_overall = array(
            'Totals and Overall Averages:',
            '',
            '',
            '',
            $total['impressions'],
            $total['clicks'],
            $total['ctr'].'%',
            $this->format_money($total['avgCPC']),
            $this->format_money($total['cost'])
            );

        $output_array = array_map(function($x){
            $x['avgCPC'] = $this->format_money($x['avgCPC']);
            $x['cost'] = $this->format_money($x['cost']);
            return $x;
        }, $output_array);

        $output_array[] = $total_and_overall;
        $headings = array('Date','Campaign','Country/Territory', 'Location', 'Impressions' , 'Clicks','CTR','Avg.CPC','Cost');

        /* Create a copy raw data of report */
        $raw_cache = array();
        $raw_cache['headings'] = $headings;
        $raw_cache['rows'] = array_map(function($x){return array_values($x);}, $output_array);

        $client_customer_id = $this->CI->base_adwords_m->get_client_customer_id();
        $week_data_cache_key = $client_customer_id.'/geo_'.my_date($this->start_time,'Y-m-d').'_to_'.my_date($this->end_time,'Y-m-d');
        $this->CI->scache->write($raw_cache,'google-adword/week_data/'.$week_data_cache_key);
        /* Finished create the coppy raw data*/

        array_unshift($output_array,$headings);

        $this->objPHPExcel->setActiveSheetIndex(3); 
        $objWorksheet = $this->objPHPExcel->getActiveSheet();
        $start_coordinate = array('y' => 13,'x' => '1');
        $objWorksheet->fromArray($output_array, NULL, 'A' . 13,true);

        $style['borders'] = array('borders' => array('allborders' => array('style' => \PHPExcel_Style_Border::BORDER_THIN)));
        $this->objPHPExcel->getActiveSheet()->getStyle('A13:I' . (13 + count($output_array)))->applyFromArray($style['borders']);
        $this->objPHPExcel->getActiveSheet()->getStyle('A13:I' . (13 + count($output_array)))->getFont()->setSize(10);
        $objWorksheet->setCellValue('B10', $start_date.'-'.$end_date);

        return $this->objPHPExcel;
    }

    /**
     * Format Micro-Cost
     *
     * @param      int     $microAmount    The micro amount
     * @param      int     $decimals       The decimals
     * @param      string  $dec_point      The decrement point
     * @param      string  $thousands_sep  The thousands separator
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function format_money($microAmount, $decimals = 2 , $dec_point = "," ,  $thousands_sep = ".")
    {
        return number_format($microAmount/1000000, $decimals, $dec_point ,  $thousands_sep);
    }
}