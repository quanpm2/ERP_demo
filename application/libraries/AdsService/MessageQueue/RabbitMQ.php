<?php

namespace AdsService\MessageQueue;

class RabbitMQ
{
    protected $CI = null;
    
    protected $connection = null;
    protected $channel = null;

    /**
     * Constructs a new instance.
     *
     * @param      <type>  $endpoint  The endpoint
     * @param      <type>  $token     The token
     */
    public function __construct()
    {
        $this->CI = &get_instance();

        $this->CI->load->model('term_posts_m');
        $this->CI->load->config('amqps');

        $this->init();
    }
    
    /**
     * init
     *
     * @return self
     */
    public function init(){
        $amqps_host 	= $this->CI->config->item('host', 'amqps');
        $amqps_port 	= $this->CI->config->item('port', 'amqps');
        $amqps_user 	= $this->CI->config->item('user', 'amqps');
        $amqps_password = $this->CI->config->item('password', 'amqps');

        $this->connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $this->channel = $this->connection->channel();

        return $this;
    }
    
    /**
     * publish
     * 
     * Dispatch message to RabbitMQ with defined structure payload
     * 
     * @param  string $executor
     * @param  string|array $payload
     * @return boolean
     */
    public function publish($executor, $payload = [])
    {
        $external_queue = $this->CI->config->item('external_queue', 'amqps_queues');

        $queue_name = $external_queue[$executor]['queue_name'] ?? NULL;
        if(empty($queue_name)) return FALSE;

        $this->channel->queue_declare($queue_name, false, true, false, false);

        $queue_payload = [
            '__dispatch_job_class' => $external_queue[$executor]['job_class'],
            '__dispatch_job_queue' => $external_queue[$executor]['job_queue'],
            'payload' => $payload,
        ];

        $message = new \PhpAmqpLib\Message\AMQPMessage(
            json_encode($queue_payload),
            [
                'delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT
            ]
        );
        $this->channel->basic_publish($message, '', $queue_name);

        $this->channel->close();
        $this->connection->close();

        return TRUE;
    }
}
