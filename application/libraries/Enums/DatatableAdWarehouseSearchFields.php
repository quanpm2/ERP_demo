<?php

namespace  Enums;

use Exception;

class DatatableAdWarehouseSearchFields
{
    private static $valueToName = [
        
        'fact_warehouse_exchange_id' => 'fact_warehouse_exchange.id',
        'fact_warehouse_exchange_begin_balance' => 'fact_warehouse_exchange.begin_balance',
        'fact_warehouse_exchange_import' => 'fact_warehouse_exchange.import',
        'fact_warehouse_exchange_export_use' => 'fact_warehouse_exchange.export_use',
        'fact_warehouse_exchange_export_suspend' => 'fact_warehouse_exchange.export_suspend',
        'fact_warehouse_exchange_balance' => 'fact_warehouse_exchange.balance',
        'fact_warehouse_exchange_time_id' => 'fact_warehouse_exchange.time_id',
        'fact_warehouse_exchange_ad_warehouse_id' => 'fact_warehouse_exchange.ad_warehouse_id',
        'fact_warehouse_exchange_ad_bm_id' => 'fact_warehouse_exchange.ad_bm_id',
        'fact_warehouse_exchange_ad_account_id' => 'fact_warehouse_exchange.ad_account_id',
        'fact_warehouse_exchange_contract_id' => 'fact_warehouse_exchange.contract_id',
        'ad_warehouse_id' => 'ad_warehouse.id',
        'ad_warehouse_name' => 'ad_warehouse.name',
        'ad_warehouse_status' => 'ad_warehouse.status',
        'ad_warehouse_quantity' => 'ad_warehouse.quantity',
        'ad_warehouse_created_at' => 'ad_warehouse.created_at',
        'ad_warehouse_updated_at' => 'ad_warehouse.updated_at',
        'ad_warehouse_bm_id' => 'ad_warehouse.bm_id',
        'ad_warehouse_original_bm_term_id' => 'ad_warehouse.original_bm_term_id',
        'ad_business_manager_id' => 'ad_business_manager.id',
        'ad_business_manager_name' => 'ad_business_manager.name',
        'ad_business_manager_created_by' => 'ad_business_manager.created_by',
        'ad_business_manager_created_time' => 'ad_business_manager.created_time',
        'ad_business_manager_extended_updated_time' => 'ad_business_manager.extended_updated_time',
        'ad_business_manager_is_hidden' => 'ad_business_manager.is_hidden',
        'ad_business_manager_verification_status' => 'ad_business_manager.verification_status',
        'ad_business_manager_payment_account_id' => 'ad_business_manager.payment_account_id',
        'ad_business_manager_timezone_id' => 'ad_business_manager.timezone_id',
        'ad_business_manager_created_at' => 'ad_business_manager.created_at',
        'ad_business_manager_updated_at' => 'ad_business_manager.updated_at',
        'ad_business_manager_original_term_id' => 'ad_business_manager.original_term_id',
        'ad_business_manager_business_id' => 'ad_business_manager.business_id',
        'ad_account_id' => 'ad_account.id',
        'ad_account_created_at' => 'ad_account.created_at',
        'ad_account_updated_at' => 'ad_account.updated_at',
        'ad_account_name' => 'ad_account.name',
        'ad_account_account_id' => 'ad_account.account_id',
        'ad_account_status' => 'ad_account.status',
        'ad_account_amount_spent' => 'ad_account.amount_spent',
        'ad_account_age' => 'ad_account.age',
        'ad_account_created_time' => 'ad_account.created_time',
        'ad_account_stock_status' => 'ad_account.stock_status',
        'ad_account_type_of_ownership' => 'ad_account.type_of_ownership',
        'ad_account_creation_type' => 'ad_account.creation_type',
        'ad_account_currency' => 'ad_account.currency',
        'ad_account_funding_source' => 'ad_account.funding_source',
        'ad_account_funding_source_details' => 'ad_account.funding_source_details',
        'ad_account_spend_cap' => 'ad_account.spend_cap',
        'ad_account_min_daily_budget' => 'ad_account.min_daily_budget',
        'ad_account_balance' => 'ad_account.balance',
        'ad_account_disable_reason' => 'ad_account.disable_reason',
        'ad_account_original_term_id' => 'ad_account.original_term_id',
        'ad_account_bank_id' => 'ad_account.bank_id',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new Exception(sprintf(
                    'Enum %s has no name defined for value %s', __CLASS__, $value));
        }
        return self::$valueToName[$value];
    }

    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new Exception(sprintf(
                    'Enum %s has no value defined for name %s', __CLASS__, $name));
        }
        return constant($const);
    }
}