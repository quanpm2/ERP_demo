<?php

namespace  Enums;

use Exception;

class DatatableAdWarehouseSummarySearchFields
{
    private static $valueToName = [

        'fact_warehouse_summary_id' => 'fact_warehouse_summary.id',
        'ad_warehouse_id' => 'ad_warehouse.id',
        'ad_warehouse_name' => 'ad_warehouse.name',
        'ad_warehouse_status' => 'ad_warehouse.status',
        'ad_warehouse_quantity' => 'ad_warehouse.quantity',
        'ad_warehouse_created_at' => 'ad_warehouse.created_at',
        'ad_warehouse_updated_at' => 'ad_warehouse.updated_at',
        'ad_warehouse_bm_id' => 'ad_warehouse.bm_id',
        'ad_warehouse_original_bm_term_id' => 'ad_warehouse.original_bm_term_id',
        'ad_business_manager_id' => 'ad_business_manager.id',
        'ad_business_manager_name' => 'ad_business_manager.name',
        'ad_business_manager_created_by' => 'ad_business_manager.created_by',
        'ad_business_manager_created_time' => 'ad_business_manager.created_time',
        'ad_business_manager_extended_updated_time' => 'ad_business_manager.extended_updated_time',
        'ad_business_manager_is_hidden' => 'ad_business_manager.is_hidden',
        'ad_business_manager_verification_status' => 'ad_business_manager.verification_status',
        'ad_business_manager_payment_account_id' => 'ad_business_manager.payment_account_id',
        'ad_business_manager_timezone_id' => 'ad_business_manager.timezone_id',
        'ad_business_manager_created_at' => 'ad_business_manager.created_at',
        'ad_business_manager_updated_at' => 'ad_business_manager.updated_at',
        'ad_business_manager_original_term_id' => 'ad_business_manager.original_term_id',
        'ad_business_manager_business_id' => 'ad_business_manager.business_id',
        'fact_warehouse_summary_time_id' => 'fact_warehouse_summary.time_id',
        'fact_warehouse_summary_ad_warehouse_id' => 'fact_warehouse_summary.ad_warehouse_id',
        'fact_warehouse_summary_ad_bm_id' => 'fact_warehouse_summary.ad_bm_id',
        'fact_warehouse_summary_import' => 'fact_warehouse_summary.import',
        'fact_warehouse_summary_export_use' => 'fact_warehouse_summary.export_use',
        'fact_warehouse_summary_export_suspend' => 'fact_warehouse_summary.export_suspend',
        'fact_warehouse_summary_ad_accounts_count' => 'fact_warehouse_summary.ad_accounts_count',
        'fact_warehouse_summary_ad_accounts_status_active_count' => 'fact_warehouse_summary.ad_accounts_status_active_count',
        'fact_warehouse_summary_ad_accounts_status_disabled_count' => 'fact_warehouse_summary.ad_accounts_status_disabled_count',
        'fact_warehouse_summary_ad_accounts_status_unsettled_count' => 'fact_warehouse_summary.ad_accounts_status_unsettled_count',
        'fact_warehouse_summary_ad_accounts_status_pending_risk_review_count' => 'fact_warehouse_summary.ad_accounts_status_pending_risk_review_count',
        'fact_warehouse_summary_ad_accounts_status_pending_settlement_count' => 'fact_warehouse_summary.ad_accounts_status_pending_settlement_count',
        'fact_warehouse_summary_ad_accounts_status_in_grace_period_count' => 'fact_warehouse_summary.ad_accounts_status_in_grace_period_count',
        'fact_warehouse_summary_ad_accounts_status_pending_closure_count' => 'fact_warehouse_summary.ad_accounts_status_pending_closure_count',
        'fact_warehouse_summary_ad_accounts_status_closed_count' => 'fact_warehouse_summary.ad_accounts_status_closed_count',
        'fact_warehouse_summary_ad_accounts_status_any_active_count' => 'fact_warehouse_summary.ad_accounts_status_any_active_count',
        'fact_warehouse_summary_ad_accounts_status_any_closed_count' => 'fact_warehouse_summary.ad_accounts_status_any_closed_count',
        'fact_warehouse_summary_ad_accounts_status_unspecified_count' => 'fact_warehouse_summary.ad_accounts_status_unspecified_count',
        'fact_warehouse_summary_ad_accounts_disable_none_count' => 'fact_warehouse_summary.ad_accounts_disable_none_count',
        'fact_warehouse_summary_ad_accounts_disable_ads_integrity_policy_count' => 'fact_warehouse_summary.ad_accounts_disable_ads_integrity_policy_count',
        'fact_warehouse_summary_ad_accounts_disable_ads_ip_review_count' => 'fact_warehouse_summary.ad_accounts_disable_ads_ip_review_count',
        'fact_warehouse_summary_ad_accounts_disable_risk_payment_count' => 'fact_warehouse_summary.ad_accounts_disable_risk_payment_count',
        'fact_warehouse_summary_ad_accounts_disable_gray_account_shut_down_count' => 'fact_warehouse_summary.ad_accounts_disable_gray_account_shut_down_count',
        'fact_warehouse_summary_ad_accounts_disable_ads_afc_review_count' => 'fact_warehouse_summary.ad_accounts_disable_ads_afc_review_count',
        'fact_warehouse_summary_ad_accounts_disable_business_integrity_rar_count' => 'fact_warehouse_summary.ad_accounts_disable_business_integrity_rar_count',
        'fact_warehouse_summary_ad_accounts_disable_permanent_close_count' => 'fact_warehouse_summary.ad_accounts_disable_permanent_close_count',
        'fact_warehouse_summary_ad_accounts_disable_unused_reseller_account_count' => 'fact_warehouse_summary.ad_accounts_disable_unused_reseller_account_count',
        'fact_warehouse_summary_ad_accounts_disable_unused_account_count' => 'fact_warehouse_summary.ad_accounts_disable_unused_account_count',
        'fact_warehouse_summary_ad_accounts_disable_umbrella_ad_account_count' => 'fact_warehouse_summary.ad_accounts_disable_umbrella_ad_account_count',
        'fact_warehouse_summary_ad_accounts_disable_business_manager_integrity_policy_count' => 'fact_warehouse_summary.ad_accounts_disable_business_manager_integrity_policy_count',
        'fact_warehouse_summary_ad_accounts_disable_misrepresented_ad_account_count' => 'fact_warehouse_summary.ad_accounts_disable_misrepresented_ad_account_count',
        'fact_warehouse_summary_ad_accounts_disable_aoab_deshare_legal_entity_count' => 'fact_warehouse_summary.ad_accounts_disable_aoab_deshare_legal_entity_count',
        'fact_warehouse_summary_ad_accounts_disable_ctx_thread_review_count' => 'fact_warehouse_summary.ad_accounts_disable_ctx_thread_review_count',
        'fact_warehouse_summary_ad_accounts_disable_compromised_ad_account_count' => 'fact_warehouse_summary.ad_accounts_disable_compromised_ad_account_count',
        'fact_warehouse_summary_ad_accounts_stock_status_inuse_count' => 'fact_warehouse_summary.ad_accounts_stock_status_inuse_count',
        'fact_warehouse_summary_ad_accounts_stock_status_instock_count' => 'fact_warehouse_summary.ad_accounts_stock_status_instock_count',
        'fact_warehouse_summary_ad_accounts_stock_status_pending_io_review_count' => 'fact_warehouse_summary.ad_accounts_stock_status_pending_io_review_count',
        'fact_warehouse_summary_ad_accounts_stock_status_closed_count' => 'fact_warehouse_summary.ad_accounts_stock_status_closed_count',
        'fact_warehouse_summary_ad_accounts_stock_status_unspecified_count' => 'fact_warehouse_summary.ad_accounts_stock_status_unspecified_count',
        'fact_warehouse_summary_ad_accounts_type_of_ownership_internal' => 'fact_warehouse_summary.ad_accounts_type_of_ownership_internal',
        'fact_warehouse_summary_ad_accounts_type_of_ownership_external' => 'fact_warehouse_summary.ad_accounts_type_of_ownership_external',
        'fact_warehouse_summary_ad_accounts_type_of_ownership_unspecified' => 'fact_warehouse_summary.ad_accounts_type_of_ownership_unspecified',
        'fact_warehouse_summary_ad_accounts_creation_type_purchase_count' => 'fact_warehouse_summary.ad_accounts_creation_type_purchase_count',
        'fact_warehouse_summary_ad_accounts_creation_type_create_count' => 'fact_warehouse_summary.ad_accounts_creation_type_create_count',
        'fact_warehouse_summary_ad_accounts_creation_type_default_count' => 'fact_warehouse_summary.ad_accounts_creation_type_default_count',
        'fact_warehouse_summary_ad_accounts_creation_type_unknown_count' => 'fact_warehouse_summary.ad_accounts_creation_type_unknown_count',
        'fact_warehouse_summary_ad_accounts_creation_type_unspecified_count' => 'fact_warehouse_summary.ad_accounts_creation_type_unspecified_count',
        'fact_warehouse_summary_ad_accounts_creation_type_partner_count' => 'fact_warehouse_summary.ad_accounts_creation_type_partner_count',
        'fact_warehouse_summary_ad_accounts_amount_spent' => 'fact_warehouse_summary.ad_accounts_amount_spent',
        'fact_warehouse_summary_ad_accounts_spend_cap_avg' => 'fact_warehouse_summary.ad_accounts_spend_cap_avg',
        'fact_warehouse_summary_ad_accounts_min_daily_budget_avg' => 'fact_warehouse_summary.ad_accounts_min_daily_budget_avg',
        'fact_warehouse_summary_ad_accounts_min_balance_total' => 'fact_warehouse_summary.ad_accounts_min_balance_total',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new Exception(sprintf(
                    'Enum %s has no name defined for value %s', __CLASS__, $value));
        }
        return self::$valueToName[$value];
    }

    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new Exception(sprintf(
                    'Enum %s has no value defined for name %s', __CLASS__, $name));
        }
        return constant($const);
    }
}