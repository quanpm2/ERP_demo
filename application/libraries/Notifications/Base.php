<?php

namespace Notifications;

class Base
{
    public $CI       = null;
    public $customer = NULL;
    public $template = NULL;

    public $data     = array();
    public $sale     = array();
    public $owner    = array();
    public $workers  = array();

    public $autogen_msg     = FALSE;

    public $autoload   = array(
        'models' => [
            'user_m',
            'admin_m',
            'option_m',
            'staffs/department_m',
            'staffs/user_group_m',
            'contract/contract_m',
            'term_users_m',
            'message/message_m'
        ],
        'libraries' => [],
        'helpers' => [] 
    );

    function __construct($args = [])
    {
        $this->CI = &get_instance();
        $this->autoload_classes();
    }

    /**
     * Autoloads any class-specific files that are needed throughout the
     * controller. This is often used by base controllers, but can easily be
     * used to autoload models, etc.
     *
     * @return void
     */
    public function autoload_classes()
    {
        // Using ! empty() because count() returns 1 for certain error conditions

        if ( !empty($this->autoload['libraries']) && is_array($this->autoload['libraries']))
        {
            foreach ($this->autoload['libraries'] as $library)
            {
                $this->CI->load->library($library);
            }
        }

        if ( !empty($this->autoload['helpers']) && is_array($this->autoload['helpers']))
        {
            foreach ($this->autoload['helpers'] as $helper)
            {
                $this->CI->load->helper($helper);
            }
        }

        if ( !empty($this->autoload['models']) && is_array($this->autoload['models']))
        {
            foreach ($this->autoload['models'] as $model)
            {
                $this->CI->load->model($model);
            }
        }
    }


    /**
     * Fire Events
     *
     * @param      array  $args   The arguments
     *
     * @return     self   ( description_of_the_return_value )
     */
    public function send($args = [])
    {
        return $this;
    }

    /**
     * Initializes the object.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function init()
    {
        $this->init_people_belongs();
        return $this;
    }

    /**
     * Load sales|staffs|customer belongs to contract
     *
     * @return     <type>
     */
    public function init_people_belongs()
    {
        $this->load_sale();
        $this->load_owner();
        $this->load_workers();
        $this->load_customer();

        return $this;
    }

    /**
     * Loads a sale.
     *
     * @return     self
     */
    public function load_sale()
    {
        if(empty($this->contract)) return $this;

        /* Load thông tin nhân viên kinh doanh phụ trách */
        $staff_business = (int) get_term_meta_value($this->contract->term_id, 'staff_business');
        if($staff_business) $this->sale = $this->CI->admin_m->get_field_by_id($staff_business);

        $this->load_user_groups();
        return $this;
    }

    /**
     * Loads a sale.
     *
     * @return     self
     */
    public function load_owner()
    {
        return $this;
    }


    /**
     * Loads user groups.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_user_groups()
    {
        if(empty($this->sale)) return FALSE;

        $taxonomies = array( $this->CI->department_m->term_type, $this->CI->user_group_m->term_type );
        $groups = $this->CI->term_users_m->get_user_terms($this->sale['user_id'], $taxonomies);
        if(empty($groups)) return FALSE;

        $manager_role_id    = $this->CI->option_m->get_value('manager_role_id');
        $leader_role_id     = $this->CI->option_m->get_value('leader_role_id');
        $groups = array_group_by($groups, 'term_type');

        if( ! empty($groups['department']))
        {
            $this->departments      = $groups['department'];
            $head_of_departments    = array();

            foreach ($groups['department'] as $_department)
            {
                $_h_users = $this->CI->term_users_m->get_the_users($_department->term_id, 'admin', ['fields'=>'user.user_id, term_id, role_id, display_name, user_email','where'=> ['role_id'=>$this->CI->option_m->get_value('manager_role_id')]]);
                if(empty($_h_users)) continue;

                $head_of_departments = array_merge($_h_users, $head_of_departments);
            }

            if( ! empty($head_of_departments)) $this->head_of_departments = $head_of_departments;
        }

        if( ! empty($groups['user_group']))
        {
            $this->user_groups  = $groups['user_group'];
            $head_of_ugroups    = array();
            foreach ($groups['user_group'] as $_ugroup)
            {
                $_ug_users = $this->CI->term_users_m->get_the_users($_ugroup->term_id, 'admin', ['fields'=>'user.user_id, term_id, role_id, display_name, user_email','where'=> ['role_id'=>$this->CI->option_m->get_value('leader_role_id')]]);
                if(empty($_ug_users)) continue;
                
                $head_of_ugroups = array_merge($_ug_users, $head_of_ugroups);
            }

            if( ! empty($head_of_ugroups)) $this->head_of_ugroups = $head_of_ugroups;
        }

        return $this;
    }

    /**
     * Loads a customer.
     *
     * @return     self
     */
    public function load_customer()
    {
        return $this;
    }

    /**
     * Loads workers.
     *
     * @return     self
     */
    public function load_workers()
    {
        return $this;
    }

    /**
     * Creates a message.
     *
     * @param      array   $data   The data
     * @param      array  $recipients  The recipients
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    protected function create_message()
    {
        if (!$this->autogen_msg || empty($this->data)) return FALSE;
        return $this;
    }
}
