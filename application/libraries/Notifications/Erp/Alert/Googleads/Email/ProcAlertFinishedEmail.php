<?php

/**
 * Notifications SMS for Customer Daily Event
 *
 * @package Notifications\Erp
 * @author  thonh@webdoctor.vn
 * @since   Version 1.0.0
 */

namespace Notifications\Erp\Alert\Googleads\Email;

use Notifications\Erp\Email as Base;
use AdsService\AdWords\Reports\XlsWeeklyReport;

class ProcAlertFinishedEmail extends Base
{
    public $defaultEmailFrom = 'warning';

    /**
     * Contact|Lead Object
     *
     * @var        <object>
     */
    public $contract = null;

    /**
     * Customer|Curators
     *
     * @var        array
     */
    public $recipients = [];

    /**
     * Message Template
     *
     * @var        <type>
     */
    public $messageTemplate = null;

    /**
     * Flag make send is only return true
     *
     * @var        bool
     */
    public $fake_send = true;

    /**
     * Cc Email
     *
     * @var        <array>
     */
    public $cc = [
        'nd@adsplus.vn'
    ];

    /**
     * Sender
     *
     * @var        <array>
     */
    public $from = [
        'email' => 'support@adsplus.vn',
        'name' => 'Adsplus.vn'
    ];

    /**
     * Constructs a new instance.
     *
     * @param      array  $args   The arguments
     */
    function __construct($args = [])
    {
        $this->autoload['models'] = array_merge([
            'googleads/googleads_m',
            'googleads/base_adwords_m',
            'googleads/adword_calculation_m',
            'googleads/googleads_kpi_m',
            'googleads/googleads_log_m'

        ], $this->autoload['models']);

        $this->autoload['helpers'][] = 'text';

        parent::__construct($args);

        !empty($args['config']) and $this->CI->config->load($args['config']);

        $this->CI->config->load('googleads/googleads');
        // $this->CI

        if (!empty($args['contract'])) {
            $this->CI->googleads_m->set_contract($args['contract']);
            $this->contract = $this->CI->googleads_m->get_contract();
            $this->loadRecipients();
            $this->load_workers();
        }

        ENVIRONMENT == 'production' and $this->fake_send = false;
    }


    // /**
    //  * { function_description }
    //  *
    //  * @throws     Exception  (description)
    //  *
    //  * @return     self       ( description_of_the_return_value )
    //  */
    // public function send($args = [])
    // {
    //     $this->recipients = array_filter($this->recipients, function ($x) {
    //         return  !empty($x['phone']);
    //     });
    //     if (empty($this->recipients)) throw new \Exception("Empty Recipients !");

    //     try {
    //         foreach ($this->recipients as $recipient) {
    //             $this->_send($recipient);
    //         }
    //     } catch (\Exception $e) {
    //         throw new \Exception($e->getMessage());
    //     }

    //     return true;
    // }

    /**
     * { function_description }
     *
     * @throws     Exception  (description)
     *
     * @return     self       ( description_of_the_return_value )
     */
    public function send($args = [])
    {
        $result = false;
        try {
            $result = parent::send($args);
        } catch (\Exception $e) {
            // var_dump($e->getMessage());
            $result = false;
        }

        $log = array(
            'user_id'       => 0,
            'term_id'       => $this->contract->term_id,
            'log_type'      => "{$this->CI->googleads_log_m->prefix_log_type}email",
            'log_title'     => $this->subject,
            'log_status'    =>  (int) !empty($result),
            'log_time_create'   => my_date(time(), 'Y-m-d H:i:s'),
            'log_content'       => serialize(array(
                'attachment' => serialize($this->attachments),
                'recipients' => serialize($this->recipients)
            ))
        );

        $this->CI->googleads_log_m->insert($log);

        return $result;
    }

    /**
     * Set From
     */
    public function setFrom($args = [])
    {
        parent::setFrom($args);
        $this->CI->config->load('googleads/email');
        $group_name     = strtolower(trim($this->defaultEmailFrom));
        $conf_emails    = $this->CI->config->item($group_name, 'email-groups');
        if (empty($conf_emails)) return $this;

        $rand_key = array_rand($conf_emails);
        $this->CI->email->initialize($conf_emails[$rand_key]);

        return $this;
    }

    /**
     * Initializing
     */
    public function setUp()
    {
        parent::setUp();

        $this->setFrom();

        // $curators = get_term_meta_value($this->contract->term_id, 'contract_curators');
        // $curators = @unserialize($curators);
        // if (!empty($curators)) {
        //     foreach ($curators as $item) {
        //         $email = trim($item['email']);
        //         if (empty($email)) continue;
        //         $mail_to[] = $email;
        //     }
        // }

        // $this->workers and $this->cc = array_merge(array_column($this->workers, 'user_email'), $this->cc);


        // if (!empty($this->recipients)) {
        //     $recipients = $this->recipients;
        //     $recipients = array_filter($recipients, function ($x) {
        //         return !empty($x['email']);
        //     });
        //     $recipients and $this->to = array_merge(array_column($this->recipients, 'email'), $this->to);
        // }




        // $this->setRecipients([$recipient['email']]);
        // $this->setRecipients(['tanhn@gmail.com']);

        // $data = [];

        // $data['term'] = $this->contract;
        // $data['title'] = 'Thông báo kết thúc dịch vụ';
        // $data['kpis'] = $this->workers;
        // $data['email_source'] = $subject;

        // $data['isJoinedContract']   = false;
        // $data['balanceBudgetAddTo'] = (int) get_term_meta_value($this->contract->term_id, 'balanceBudgetAddTo');
        // if (!empty($data['balanceBudgetAddTo'])) {
        //     $data['isJoinedContract']   = true;
        //     $data['nextContractId']     = (int) get_term_meta_value($this->contract->term_id, 'nextContractId');
        //     $data['nextContractCode']   = get_term_meta_value($data['nextContractId'], 'contract_code');
        // }
        // $this->setData($data);

        // $start_time = get_term_meta_value($this->contract->term_id, 'googleads-begin_time');
        // $start_time or $start_time = get_term_meta_value($this->contract->term_id, 'advertise_start_time');

        // $end_time = get_term_meta_value($this->contract->term_id, 'googleads-end_time');
        // $end_time or $end_time = get_term_meta_value($this->contract->term_id, 'advertise_start_time');
        // $end_time or $end_time = get_term_meta_value($this->contract->term_id, 'end_service_time');
        // $end_time or $end_time = time();

        // $attachments = [
        //     'quan-ly-tai-khoan-bao-cao-tuan.xlsx' => (new XlsWeeklyReport($this->contract->term_id, $start_time, $end_time))->build()
        // ];
        // $this->setAttachments($attachments);

        if ($this->fake_send) {
            $this->setCc([]);
            $this->setBcc([]);

            dd($this->cc, $this->bcc, $this->to);

            // parent::send();
        } else {
            dd(2);
            // parent::send();
        };





        $mail_id = 'ID:' . time();

        $title = "[ADSPLUS.VN] Thông báo kết thúc dịch vụ Adsplus của website  {$this->contract->term_name} [{$mail_id}]";
        $this->setSubject($title);

        // $this->buildXlsFile();
    }

    /**
     * Gets the message.
     */
    public function render()
    {
        $data = $files = array();

        // $service_type = get_term_meta_value($this->contract->term_id, 'service_type');
        // $files['quan-ly-tai-khoan-bao-cao-tuan'] = $this->buildXlsFile();

        $data['term']       = $this->contract;
        $data['start_time'] = $this->start_time;
        $data['end_time']   = $this->end_time;
        $data['title']      = '[ADSPLUS.VN] Báo cáo quảng cáo tuần';

        $data['kpis'] = $this->CI->googleads_kpi_m
            ->where('term_id', $this->contract->term_id)
            ->order_by('kpi_type')
            ->as_array()
            ->get_many_by();

        // $direct_downlink = array();
        // $i = 0;
        // foreach ($this->attachments as $file) {
        //     $segments = explode('/', $file);
        //     $file_name = end($segments);
        //     $direct_downlink[] = anchor(base_url('report/adsplus/download/' . $this->contract->term_id . '/' . $file_name), ' Báo cáo ' . (++$i));
        // }
        // $data['direct_downlink'] = $direct_downlink;

        $data['email_source'] = $this->subject;

        $data['content'] = $this->CI->load->view('googleads/report/finish_email', $data, TRUE);
        $content = $this->CI->load->view('email/yellow_tpl', $data, TRUE);

        return $content;
    }

    /**
     * Builds a Xls file.
     *
     * @throws     Exception  (description)
     */
    public function buildXlsFile()
    {
        $xslFilePath = (new XlsWeeklyReport($this->contract->term_id, $this->start_time, $this->end_time))->build();
        // $xslFilePath = 'files/google_adword/account_type/5158802545/2021-06-30_to_2021-07-07_created_on_2021-07-07-13-55-35.xlsx';

        $this->attachments = [
            'quan-ly-tai-khoan-bao-cao-tuan' => $xslFilePath
        ];

        return $this;
    }


    /**
     * Loads workers.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_workers()
    {
        $kpiTypesEnums = $this->CI->config->item('googleads_service_type') ?: [];
        if (empty($kpiTypesEnums)) return $this;

        $kpiTypesEnums and $kpiTypesEnums = array_keys($kpiTypesEnums);

        $kpis = $this->CI->googleads_kpi_m
            ->where_in('kpi_type', $kpiTypesEnums)
            ->order_by('kpi_type')
            ->where('term_id', $this->contract->term_id)
            ->get_all();

        if (empty($kpis)) return $this;

        $this->workers = array_map(function ($x) {

            $admin          = $this->CI->admin_m->get_field_by_id($x->user_id);
            $_display_name  = explode(' ', $admin['display_name']);

            $admin['last_name']         = ucfirst(convert_accented_characters(end($_display_name)));
            $admin['gender']            = get_user_meta_value($x->user_id, 'gender');
            $admin['gender_f']          = !empty($gender) ? 'Mr.' : 'Ms.';
            $admin['name']              = "{$admin['gender_f']}{$admin['last_name']}";
            $admin['user_phone']        = get_user_meta_value($x->user_id, 'user_phone');

            return $admin;
        }, $kpis);

        return $this;
    }

    /**
     * Loads recipients.
     *
     * @throws     Exception  (description)
     */
    public function loadRecipients()
    {
        $contract = $this->contract;
        $curators = get_term_meta_value($contract->term_id, 'contract_curators');
        $curators = @unserialize($curators);

        if (empty($curators)) {
            throw new \Exception('Không tìm thấy thông tin người nhận báo cáo SMS.');
        }

        $_recipients = array_map(function ($x) {
            return array(
                'name' => $x['name'] ?: '',
                'email' => $x['email'] ?: '',
                'phone' => preg_replace('/\s+/', '', $x['phone'])
            );
        }, $curators);

        if (empty($_recipients)) {
            throw new \Exception('Thông tin người nhận SMS không hợp lệ.');
        }

        $this->recipients = $_recipients;
    }
}
