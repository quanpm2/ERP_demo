<?php

/**
 * Notification Slack Instance
 *
 * @package Notifications/ACN
 * @author  thonh@webdoctor.vn
 * @since   Version 1.0.0
 */

namespace Notifications\Acn;

use Notifications\Base;

class Slack extends Base
{
    /**
     * Contact|Lead Object
     *
     * @var        <object>
     */
    public $contact = null;

    /**
     * Direct Slack's Users
     *
     * @var        array
     */
    public $recipients = [];


    /**
     * Slack Channels
     *
     * @var        array
     */
    public $channels = ['general'];


    /**
     * contact|Lead Owner
     *
     * @var        User_m
     */
    public $contactOwner = null;

    /**
     * Message Template Args
     *
     * @var        array
     */
    public $messageTemplate = [
        'text'          => 'sample text !',
        'blocks'        => null,
        'icon_emoji'    => ':flying-money:',
        'username'      => 'tèn tén ten'
    ];

    function __construct($contact = null, $config = null)
    {

        parent::__construct();

        $this->CI->config->load('slack');

        if (!empty($config)) {
            $this->CI->config->load($config);
        }

        empty($contact) or $this->contact = $contact;
    }

    /**
     * { function_description }
     *
     * @throws     Exception  (description)
     *
     * @return     self       ( description_of_the_return_value )
     */
    public function send()
    {
        if (!$this->channels) throw new \Exception("Channel unknown !");

        try {
            foreach ($this->channels as $channel) {
                $message            = $this->getMessage();
                $message['channel'] = $channel;

                // $result = (new \wrapi\slack\slack($this->CI->config->item('token', 'slack')));
                $slack = new \wrapi\slack\slack($this->CI->config->item('token', 'slack'));
                $result = $slack->chat->postMessage($message);

                if (empty($result['ok'])) {
                    throw new \Exception($return['error'] ?? 'Slack configuration incorrect');
                }
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return true;
    }

    /**
     * Builds messages.
     *
     * @return     self  The messages.
     */
    public function getMessage()
    {
        $this->loadMemberOwner();

        return $this;
    }

    /**
     * Loads a member owner.
     *
     * @throws     Exception  (description)
     *
     * @return     self       ( description_of_the_return_value )
     */
    protected function loadMemberOwner()
    {
        if (!$this->contact) throw new \Exception('Contact/Lead is unknown');

        $ownerId            = (int) get_user_meta_value($this->contact->user_id, 'created_by');
        $this->contactOwner = $this->CI->user_m->select('user_id, user_email, user_time_create, user_type, user_status')->get($ownerId);
        return $this;
    }
}
