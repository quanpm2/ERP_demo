<?php

/**
 * Notification Email Instance
 *
 * @package Notifications/Erp
 * @author  thonh@webdoctor.vn
 * @since   Version 1.0.0
 */

namespace Notifications\Erp;

use Notifications\Base;

class Email extends Base
{
    /**
     * Provider email
     *
     * @var        <string>
     */
    public $provider = 'sendgrid';


    /**
     * Subject email
     *
     * @var        <string>
     */
    public $subject = '';


    /**
     * Sender
     *
     * @var        <array>
     */
    public $from = [];


    /**
     * Direct Email's Users
     *
     * @var        <array>
     */
    public $recipients = [];

    /**
     * Email To
     *
     * @var        <array>
     */
    public $to = [];

    /**
     * Email CC
     *
     * @var        <array>
     */
    public $cc = [];


    /**
     * Email Bcc
     *
     * @var        <array>
     */
    public $bcc = [];

    /**
     * Template Email
     *
     * @var        <string>
     */
    public $template = '';

    /**
     * Data Email
     *
     * @var        <string>
     */
    public $data = '';


    /**
     * contact|Lead Owner
     *
     * @var        User_m
     */
    public $contactOwner = null;


    /**
     * Email Template Args
     *
     * @var        array
     */
    public $emailTemplate = [];

    function __construct( $args = [] ){

        $this->autoload['libraries'][] = 'template';
        $this->autoload['libraries'][] = 'email';

        parent::__construct($args);

        $this->CI->email->clear(TRUE);
    }

    /**
     * Set provider
     */
    public function setProvider($provider = 'sendgrid')
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Set From
     */
    public function setFrom($args = [])
    {
        $this->from = array_merge($args , $this->from);
        return $this;
    }

    /**
     * Set Subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Set recipients
     */
    public function setRecipients(array $recipients)
    {
        $this->recipients = $recipients;

        return $this;
    }

    /**
     * Set Cc
     */
    public function setCc(array $cc)
    {
        $this->cc = $cc;

        return $this;
    }

    /**
     * Set Bcc
     */
    public function setBcc(array $bcc)
    {
        $this->bcc = $bcc;

        return $this;
    }

    /**
     * Set Template
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Set Data
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    // $content = $this->_ci->load->view($view, $data, true);

    /**
     * { function_description }
     *
     * @throws     Exception  (description)
     *
     * @return     self       ( description_of_the_return_value )
     */
    public function send($args = [])
    {
        $this->setUp();

        if( ! empty($this->attachments))
        {
            foreach ($this->attachments as $file_name => $path)
            {
                $this->CI->email->attach($path ,'attachment', "{$file_name}.xlsx");
            }
        }

        // $this->CI->email->set_provider($this->provider);
        $this->CI->email->subject($this->subject);
        $this->CI->email->from($this->from['email'], $this->from['name']);
        $this->CI->email->to($this->to);
        $this->CI->email->cc($this->cc);
        $this->CI->email->bcc($this->bcc);

        $content = $this->render();
        $this->CI->email->message($content);

        try
        {
            $result = $this->CI->email->send();
        }
        catch (\Exception $e)
        {
            throw new \Exception($e->getMessage());
        }

        return $result;
    }

    /**
     * Setup email
     */
    public function setUp()
    {
        return TRUE;
    }

    /**
     * Render email
     */
    public function render()
    {
        if (!$this->template) throw new \Exception("Template unknown !");

        $content = $this->CI->load->view($this->template, $this->data, TRUE);

        return $content;
    }
}
