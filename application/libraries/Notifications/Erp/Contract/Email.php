<?php

/**
 * Notification Email Instance
 *
 * @package Notifications/Erp
 * @author  thonh@webdoctor.vn
 * @since   Version 1.0.0
 */

namespace Notifications\Erp\Contract;

use Notifications\Erp\Email as Base;

class Email extends Base
{
    /**
     * Email Template Args
     *
     * @var        array
     */
    public $emailTemplate = [];

    function __construct( $args = [] )
    {
        $this->autoload['helpers'][] = 'text';
        $this->autoload['models'][] = 'contract/contract_m';

        parent::__construct($args);
        $this->CI->email->clear(TRUE);
        $this->CI->config->load('contract/contract');
        if( ! empty($args['contract']))
        {
            $this->CI->contract_m->set_contract($args['contract']);
            $this->contract = $this->CI->contract_m->get_contract();
        }
    }
}
