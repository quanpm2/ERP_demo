<?php

/**
 * Notifications Lead Created Internal Event
 *
 * @package Notifications/ACN
 * @author  thonh@webdoctor.vn
 * @since   Version 1.0.0
 */

namespace Notifications\Erp\Contract\ServiceStarted;

use Notifications\Erp\Contract\Email as Base;

class Email extends Base
{
    /**
     * Cc Email
     *
     * @var        <array>
     */
    public $bcc = [
        'thonh@webdoctor.vn'
    ];

    /**
     * Sender
     *
     * @var        <array>
     */
    public $from = [
        'email' => 'support@adsplus.vn',
        'name' => 'Adsplus.vn'
    ];

    /**
     * Constructs a new instance.
     *
     * @param      array  $args   The arguments
     */
    function __construct($args = [])
    {
        parent::__construct($args);

        $this->init();
        $this->setUp();
    }

    /**
     * { function_description }
     *
     * @throws     Exception  (description)
     *
     * @return     self       ( description_of_the_return_value )
     */
    public function send($args = [])
    {
        dd('bla');
        return parent::send();
    }

    /**
     * Initializing
     */
    public function setUp()
    {
        parent::setUp();

        $this->data = wp_parse_args(array(
            'representative_name'       => get_term_meta_value($this->contract->term_id, 'representative_name'),
            'representative_address'    => get_term_meta_value($this->contract->term_id, 'representative_address'),
            'representative_email'      => get_term_meta_value($this->contract->term_id, 'representative_email'),
            'contract_code'             => get_term_meta_value($this->contract->term_id, 'contract_code'),
            'contract_begin'            => get_term_meta_value($this->contract->term_id, 'contract_begin'),
            'contract_end'              => get_term_meta_value($this->contract->term_id, 'contract_end'),
            'contract_title'            => $this->CI->config->item($this->contract->term_type, 'contract_services'),
        ), $this->data);
    }

    /**
     * Gets the message.
     */
    public function render()
    {
        // $config = $this->config->item('packages');
        // $term = $this->contract;
        // $term_id = $term->term_id;
        // $mail = get_term_meta_value($term_id, 'representative_email');
        // $mail_cc = array();

        // $representative_name    = get_term_meta_value($term_id, 'representative_name');
        // $contract_code          = get_term_meta_value($term_id, 'contract_code');
        // $contract_begin         = get_term_meta_value($term_id, 'contract_begin');
        // $contract_end           = get_term_meta_value($term_id, 'contract_end');
        // $staff_id = get_term_meta_value($term_id, 'staff_business');
        // $staff_name = $this->admin_m->get_field_by_id($staff_id, 'display_name');
        
        $data = $this->data;

        $content = 'Dear team<br>';
        $content.= 'Thông tin hợp đồng mới vừa được khởi tạo như sau: <br>';

        $this->CI->config->load('table_mail');
        $this->CI->table->set_template($this->CI->config->item('mail_template'));

        $this->CI->table->set_caption('Thông tin hợp đồng');
        $this->CI->table->add_row('Mã hợp đồng:', $data['contract_code']);
        $this->CI->table->add_row('Tên dịch vụ:', 'Web tổng hợp');
        $this->CI->table->add_row('Thời gian đăng ký:', my_date($data['contract_begin'], 'd/m/Y'));
        $this->CI->table->add_row('Thời gian hết hạn:', my_date($data['contract_end'], 'd/m/Y'));
        $this->CI->table->add_row('Kinh doanh phụ trách:', implode(' - ', array_filter([ $this->sale['display_name'], $this->sale['user_email'] ])) );
        $content.= $this->CI->table->generate();


        $this->CI->table->set_caption('Thông tin khách hàng');
        $this->CI->table->add_row('Người đại diện:', $data['representative_name']);
        $this->CI->table->add_row('Địa chỉ:', $data['representative_address']);
        $this->contract->term_name AND $this->CI->table->add_row('Domain/Page:',   $this->contract->term_name);
        $this->CI->table->add_row('Mail liên hệ:', $data['representative_email']);

        $title = 'Hợp đồng thiết kế Web '.$contract_code.' đã được khởi tạo.';

        $subject = "Hợp đồng {$data['contract_title']} {$data['contract_code']} đã được khởi tạo và hoàn tất thủ tục hợp đồng (Kế toán)";

        $title = 
        
        $content.= $this->CI->table->generate();

        $content.= '';
        $staff_id = get_term_meta_value($term_id, 'staff_business');
        $staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');


        $this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
        $this->email->to('support@webdoctor.vn');

        if($staff_mail) $mail_cc[] = $staff_mail;
        if(!empty($mail_cc)) $this->email->cc($mail_cc);

        $this->email->subject($title);
        $this->email->message($content);

        $send_status = $this->email->send();
        return $content;

        $data = $files = array();


        $service_type = get_term_meta_value($this->contract->term_id,'service_type');
        $files['quan-ly-tai-khoan-bao-cao-tuan'] = $this->buildXlsFile();

        $data['term']       = $this->contract;
        $data['start_time'] = $this->start_time;
        $data['end_time']   = $this->end_time;
        $data['title']      = '[ADSPLUS.VN] Báo cáo quảng cáo tuần';

        $data['kpis'] = $this->CI->googleads_kpi_m
        ->where('term_id', $this->contract->term_id)
        ->order_by('kpi_type')
        ->as_array()
        ->get_many_by();
        
        $direct_downlink = array();
        $i = 0;
        foreach ($this->attachments as $file) 
        {
            $segments = explode('/', $file);
            $file_name = end($segments);
            $direct_downlink[] = anchor(base_url('report/adsplus/download/'.$this->contract->term_id.'/'.$file_name),' Báo cáo '.(++$i));
        }
        $data['direct_downlink'] = $direct_downlink;

        
        $data['email_source'] = $this->subject;


        $data['content'] = $this->CI->load->view('googleads/report/activation_email', $data, TRUE);
        $content = $this->CI->load->view('email/yellow_tpl', $data, TRUE);
        
        return $content;
    }
}