<?php

/**
 * Notification Sms Instance
 *
 * @package Notifications/Erp
 * @author  thonh@webdoctor.vn
 * @since   Version 1.0.0
 */

namespace Notifications\Erp;

use Notifications\Base;

class Sms extends Base
{
    /**
     * Contact|Lead Object
     *
     * @var        <object>
     */
    public $contract = null;

    /**
     * Direct Sms's Users
     *
     * @var        array
     */
    public $recipients = [];

    /**
     * contact|Lead Owner
     *
     * @var        User_m
     */
    public $contactOwner = null;

    /**
     * Message Template Args
     *
     * @var        array
     */
    public $messageTemplate = [
        'text'          => 'sample text !',
        'blocks'        => null,
        'icon_emoji'    => ':flying-money:',
        'username'      => 'tèn tén ten'
    ];

    function __construct($args = [])
    {
        $this->autoload['libraries'][] = 'sms';

        parent::__construct();
        
        $this->CI->config->load('sms');
    }

    /**
     * { function_description }
     *
     * @throws     Exception  (description)
     *
     * @return     self       ( description_of_the_return_value )
     */
    public function send($args = [])
    {
        return true;
    }

    /**
     * Builds messages.
     *
     * @return     String  The messages.
     */
    public function getMessage()
    {
        $this->loadMemberOwner();
        return "";
    }

    /**
     * Loads a member owner.
     *
     * @throws     Exception  (description)
     *
     * @return     self       ( description_of_the_return_value )
     */
    protected function loadMemberOwner()
    {
        if (!$this->contact) throw new \Exception('Contact/Lead is unknown');

        $ownerId            = (int) get_user_meta_value($this->contact->user_id, 'created_by');
        $this->contactOwner = $this->CI->user_m->select('user_id, user_email, user_time_create, user_type, user_status')->get($ownerId);
        return $this;
    }
}
