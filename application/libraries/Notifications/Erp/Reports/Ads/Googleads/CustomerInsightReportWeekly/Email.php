<?php

/**
 * Notifications Lead Created Internal Event
 *
 * @package Notifications/ACN
 * @author  thonh@webdoctor.vn
 * @since   Version 1.0.0
 */

namespace Notifications\Erp\Reports\Ads\Googleads\CustomerInsightReportWeekly;

use Notifications\Erp\Reports\Ads\Googleads\Email as Base;
use AdsService\AdWords\Reports\XlsWeeklyReport;

class Email extends Base
{
    public $defaultEmailFrom = 'reporting';

    /**
     * Cc Email
     *
     * @var        <array>
     */
    public $cc = [
        'nd@adsplus.vn'
    ];

    /**
     * Sender
     *
     * @var        <array>
     */
    public $from = [
        'email' => 'support@adsplus.vn',
        'name' => 'Adsplus.vn'
    ];

    /**
     * Report Insight data Start-Point
     *
     * @var        Integer
     */
    public $start_time  = null;


    /**
     * Report Insight data End-Point
     *
     * @var        Integer
     */
    public $end_time    = null;

    /**
     * File Attachments
     *
     * @var        array
     */
    public $attachments = [];

    /**
     * Constructs a new instance.
     *
     * @param      array  $args   The arguments
     */
    function __construct($args = [])
    {
        parent::__construct($args);

        $this->start_time = $args['start_time'] ?: start_of_day(strtotime('-1 week'));
        $this->end_time = $args['end_time'] ?: end_of_day();
    }

    /**
     * { function_description }
     *
     * @throws     Exception  (description)
     *
     * @return     self       ( description_of_the_return_value )
     */
    public function send($args = [])
    {
        $result = false;
        try
        {
            $result = parent::send($args);
        }
        catch (\Exception $e)
        {
            // var_dump($e->getMessage());
            $result = false;
        }

        $log = array(
            'user_id'       => 0,
            'term_id'       => $this->contract->term_id,
            'log_type'      => "{$this->CI->googleads_log_m->prefix_log_type}email",
            'log_title'     => $this->subject,
            'log_status'    =>  (int) ! empty($result),
            'log_time_create'   => my_date(time(), 'Y-m-d H:i:s'),
            'log_content'       => serialize(array(
                'attachment' => serialize($this->attachments),
                'recipients' => serialize($this->recipients)
            ))  
        );

        $this->CI->googleads_log_m->insert($log);
        
        return $result;
    }

    /**
     * Set From
     */
    public function setFrom($args = [])
    {
        parent::setFrom($args);
        $this->CI->config->load('googleads/email');
        $group_name     = strtolower(trim($this->defaultEmailFrom));
        $conf_emails    = $this->CI->config->item($group_name, 'email-groups');
        if(empty($conf_emails)) return $this;

        $rand_key = array_rand($conf_emails);
        $this->CI->email->initialize($conf_emails[$rand_key]);

        return $this;
    }

    /**
     * Initializing
     */
    public function setUp()
    {
        parent::setUp();

        $this->setFrom();

        $curators = get_term_meta_value($this->contract->term_id,'contract_curators');
        $curators = @unserialize($curators);
        if(!empty($curators))
        {
            foreach ($curators as $item)
            {
                $email = trim($item['email']);
                if(empty($email)) continue;
                $mail_to[] = $email;
            }
        }

        $this->workers AND $this->cc = array_merge(array_column($this->workers, 'user_email'), $this->cc);

        
        if( ! empty($this->recipients))
        {
            $recipients = $this->recipients;
            $recipients = array_filter($recipients, function($x){ return !empty($x['email']); });
            $recipients AND $this->to = array_merge( array_column($this->recipients, 'email'), $this->to);
        }

        $mail_id = 'ID:'.time();
        $title = "[ADSPLUS.VN] Báo cáo quảng cáo tuần website {$this->contract->term_name} [{$mail_id}]";
        $this->setSubject($title);

        $this->buildXlsFile();
    }

    /**
     * Gets the message.
     */
    public function render()
    {
        $data = $files = array();

        $service_type = get_term_meta_value($this->contract->term_id,'service_type');
        $files['quan-ly-tai-khoan-bao-cao-tuan'] = $this->buildXlsFile();

        $data['term']       = $this->contract;
        $data['start_time'] = $this->start_time;
        $data['end_time']   = $this->end_time;
        $data['title']      = '[ADSPLUS.VN] Báo cáo quảng cáo tuần';

        $data['kpis'] = $this->CI->googleads_kpi_m
        ->where('term_id', $this->contract->term_id)
        ->order_by('kpi_type')
        ->as_array()
        ->get_many_by();
        
        $direct_downlink = array();
        $i = 0;
        foreach ($this->attachments as $file) 
        {
            $segments = explode('/', $file);
            $file_name = end($segments);
            $direct_downlink[] = anchor(base_url('report/adsplus/download/'.$this->contract->term_id.'/'.$file_name),' Báo cáo '.(++$i));
        }
        $data['direct_downlink'] = $direct_downlink;

        
        $data['email_source'] = $this->subject;


        $data['content'] = $this->CI->load->view('googleads/report/activation_email', $data, TRUE);
        $content = $this->CI->load->view('email/yellow_tpl', $data, TRUE);
        
        return $content;
    }

    /**
     * Builds a Xls file.
     *
     * @throws     Exception  (description)
     */
    public function buildXlsFile()
    {
        $xslFilePath = (new XlsWeeklyReport($this->contract->term_id, $this->start_time, $this->end_time))->build();
        // $xslFilePath = 'files/google_adword/account_type/5158802545/2021-06-30_to_2021-07-07_created_on_2021-07-07-13-55-35.xlsx';

        $this->attachments = [
            'quan-ly-tai-khoan-bao-cao-tuan' => $xslFilePath    
        ];
        
        return $this;
    }

    /**
     * Loads recipients.
     *
     * @throws     Exception  (description)
     */
    public function loadRecipients()
    {
        parent::loadRecipients();
        
    }
}