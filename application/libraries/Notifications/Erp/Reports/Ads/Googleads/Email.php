<?php

/**
 * Notifications Lead Created Internal Event
 *
 * @package Notifications/ACN
 * @author  thonh@webdoctor.vn
 * @since   Version 1.0.0
 */

namespace Notifications\Erp\Reports\Ads\Googleads;

use Notifications\Erp\Email as Base;
use AdsService\AdWords\Reports\XlsWeeklyReport;

class Email extends Base
{
    /**
     * Constructs a new instance.
     *
     * @param      array  $args   The arguments
     */
    function __construct($args = [])
    {
        $this->autoload['models'] = array_merge([
            'googleads/googleads_m',
            'googleads/base_adwords_m',
            'googleads/adword_calculation_m',
            'googleads/googleads_kpi_m',
            'googleads/googleads_log_m',
            'googleads/mcm_account_m',
            'ads_segment_m'
        ], $this->autoload['models']);

        $this->autoload['helpers'][] = 'text';

        parent::__construct($args);

        ! empty($args['config']) AND $this->CI->config->load($args['config']);

        $this->CI->config->load('googleads/googleads');
        if( ! empty($args['contract']))
        {
            $this->CI->googleads_m->set_contract($args['contract']);
            $this->contract = $this->CI->googleads_m->get_contract();
        }
    }

    

    public function setUp()
    {
        $this->loadRecipients();
        $this->load_workers();
        return $this;
    }

    /**
     * Gets the message.
     */
    public function getMessage()
    {
        $data = $files = array();

        $service_type = get_term_meta_value($this->contract->term_id,'service_type');
        $files['quan-ly-tai-khoan-bao-cao-tuan'] = $this->buildXlsFile();

        $data['term']       = $this->contract;
        $data['start_time'] = $this->start_time;
        $data['end_time']   = $this->end_time;
        $data['title']      = '[ADSPLUS.VN] Báo cáo quảng cáo tuần';

        $data['kpis'] = $this->CI->googleads_kpi_m
        ->where('term_id', $this->term_id)
        ->order_by('kpi_type')
        ->as_array()
        ->get_many_by();
        
        $direct_downlink = array();
        $i = 0;
        foreach ($files as $file) 
        {
            $segments = explode('/', $file);
            $file_name = end($segments);
            $direct_downlink[] = anchor(base_url('report/adsplus/download/'.$this->term_id.'/'.$file_name),' Báo cáo '.(++$i));
        }
        $data['direct_downlink'] = $direct_downlink;

        $mail_id    = 'ID:'.time();
        $title      = "[ADSPLUS.VN] Báo cáo quảng cáo tuần website {$this->contract->term_name} [{$mail_id}]";
        $data['email_source'] = $title;

        $this->setSubject($title);

        $data['content'] = $this->CI->load->view('googleads/report/activation_email', $data, TRUE);
        $content = $this->CI->load->view('email/yellow_tpl', $data, TRUE);
        return $content;
    }

    /**
     * Builds a Xls file.
     *
     * @throws     Exception  (description)
     */
    public function buildXlsFile()
    {
        $xslFilePath = new XlsWeeklyReport($this->contract->term_id, $this->start_time, $this->end_time);
        return $xslFilePath;
    }

    /**
     * Loads recipients.
     *
     * @throws     Exception  (description)
     */
    public function loadRecipients()
    {
        $contract = $this->contract;
        $curators = get_term_meta_value($contract->term_id, 'contract_curators');
        $curators = @unserialize($curators);


        if(empty($curators)) return $this;
        
        $_recipients = array_map(function($x){
            return array(
                'name' => $x['name'] ?: '',
                'email' => $x['email'] ?: '',
                'phone' => preg_replace('/\s+/', '', $x['phone'])
            );
        }, $curators);

        if(empty($_recipients)) return $this;

        $this->recipients = $_recipients;
    }

    /**
     * Loads workers.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_workers()
    {
        $kpiTypesEnums = $this->CI->config->item('googleads_service_type') ?: [];
        if(empty($kpiTypesEnums)) return $this;

        $kpiTypesEnums AND $kpiTypesEnums = array_keys($kpiTypesEnums);

        $kpis = $this->CI->googleads_kpi_m
        ->where_in('kpi_type', $kpiTypesEnums)
        ->order_by('kpi_type')
        ->where('term_id', $this->contract->term_id)
        ->get_all();

        if(empty($kpis)) return $this;

        $this->workers = array_map(function($x){

            $admin          = $this->CI->admin_m->get_field_by_id($x->user_id);
            $_display_name  = explode(' ', $admin['display_name']);

            $admin['last_name']         = ucfirst(convert_accented_characters(end($_display_name)));
            $admin['gender']            = get_user_meta_value($x->user_id, 'gender');
            $admin['gender_f']          = !empty($gender) ? 'Mr.' : 'Ms.';
            $admin['name']              = "{$admin['gender_f']}{$admin['last_name']}";
            $admin['user_phone']        = get_user_meta_value($x->user_id, 'user_phone');

            return $admin;

        }, $kpis);

        return $this;
    }
}
