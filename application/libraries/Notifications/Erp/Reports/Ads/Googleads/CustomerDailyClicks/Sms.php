<?php

/**
 * Notifications SMS for Customer Daily Event
 *
 * @package Notifications\Erp
 * @author  thonh@webdoctor.vn
 * @since   Version 1.0.0
 */

namespace Notifications\Erp\Reports\Ads\Googleads\CustomerDailyClicks;

use Notifications\Erp\Sms as Base;

class Sms extends Base
{
    /**
     * Contact|Lead Object
     *
     * @var        <object>
     */
    public $contract = null;

    /**
     * Customer|Curators
     *
     * @var        array
     */
    public $recipients = [];

    /**
     * Message Template
     *
     * @var        <type>
     */
    public $messageTemplate = null;

    /**
     * Flag make send is only return true
     *
     * @var        bool
     */
    public $fake_send = true;

    /**
     * Constructs a new instance.
     *
     * @param      array  $args   The arguments
     */
    function __construct($args = [])
    {
        $this->autoload['models'] = array_merge([
            'googleads/googleads_m',
            'googleads/base_adwords_m',
            'googleads/adword_calculation_m',
            'googleads/googleads_kpi_m',
            'googleads/googleads_log_m'

        ], $this->autoload['models']);

        $this->autoload['helpers'][] = 'text';

        parent::__construct($args);

        ! empty($args['config']) AND $this->CI->config->load($args['config']);

        $this->CI->config->load('googleads/googleads');
        // $this->CI

        if( ! empty($args['contract']))
        {
            $this->CI->googleads_m->set_contract($args['contract']);
            $this->contract = $this->CI->googleads_m->get_contract();
            $this->loadRecipients();
            $this->load_workers();
        }

        ENVIRONMENT == 'production' AND $this->fake_send = false;
    }


    /**
     * { function_description }
     *
     * @throws     Exception  (description)
     *
     * @return     self       ( description_of_the_return_value )
     */
        public function send($args = [])
    {
        $this->recipients = array_filter($this->recipients, function($x){ return  !empty($x['phone']); });
        if(empty($this->recipients)) throw new \Exception("Empty Recipients !");
    
        try
        {
            foreach ($this->recipients as $recipient)
            {
                $this->_send($recipient);
            }
        }
        catch (\Exception $e)
        {
            throw new \Exception($e->getMessage());
        }

        return true;
    }

    /**
     * Send Message
     *
     * @param      <type>  $recipient  The recipient
     */
    public function _send($recipient)
    {
        $numOfSucesss = $this->CI->googleads_log_m
        ->select('log_id')
        ->like('log_content', $recipient['phone'])
        ->count_by([
            'term_id' => $this->contract->term_id,
            'log_type' => 'googleads-report-sms',
            'log_status' => 1,
            'log_time_create >=' => my_date(start_of_day(), 'Y-m-d H:i:s'),
            'log_time_create <=' => my_date(end_of_day(), 'Y-m-d H:i:s'),
        ]);

        if($numOfSucesss > 0) return true;

        $result = false;

        $search_replace = array(
            '{ten_khach_hang}' => $recipient['name'],
            '{ten_nguoi_nhan_sms}' => $recipient['name']
        );
        
        try
        {
            $message    = $this->getMessage();
            $message    = str_replace(array_keys($search_replace), array_values($search_replace), $message);
            $result     = null;

            if($this->fake_send) $result = $this->CI->sms->fake_send($recipient['phone'], $message);
            else $result = $this->CI->sms->send($recipient['phone'], $message);

            if(!empty($result))
            {
                $result = array_shift($result);
                unset($result['config']);
            }

            if(is_array($result))
            {
                $result['customer_name'] = $recipient['name'];
                $result['day'] = my_date(strtotime('yesterday'), 'd/m');
            }

            $this->CI->googleads_log_m->insert(array(
                'term_id' => $this->contract->term_id,
                'log_type' => 'googleads-report-sms',
                'log_title' => 'Send SMS '.my_date(0, 'Y-m-d H:i:s'),
                'log_status' => ( empty($result) ? 0 : 1 ),
                'log_content' => serialize($result),
                'log_time_create' => my_date(0, 'Y-m-d H:i:s'),
            ));

            // (new \Money24h\Notifications())->send("Adsplus báo cáo số liệu quảng cáo hàng ngày", $message, $recipient['phone']);
        }
        catch (\Exception $e)
        {
            throw new \Exception($e->getMessage());
        }

        return true;
    }

    /**
     * Gets the message.
     *
     * @throws     Exception  (description)
     *
     * @return     <type>     The message.
     */
    public function getMessage()
    {
        if( ! empty($this->messageTemplate)) return $this->messageTemplate;

        $this->loadRecipients();
        $this->load_workers();

        $start_date = start_of_day(strtotime('yesterday'));
        $end_date = end_of_day(strtotime('yesterday'));

        $contract   = $this->CI->googleads_m->set_contract($this->contract->term_id);
        $insights = $contract->get_accounts([
            'start_time' => $start_date,
            'end_time' => $end_date,
        ]);

        if(empty($insights)) throw new \Exception('Không có dữ liệu hoặc dữ liệu chưa được cập nhật đầy đủ');

        $clicks         = (int) array_sum(array_column($insights, 'clicks'));
        $invalidClicks  = (int) array_sum(array_column($insights, 'invalid_clicks'));
        if(empty($clicks) && empty($invalidClicks))
        {
            throw new \Exception("Không có dữ liệu cho #{$this->contract->term_id} ngày " . my_date($start_date));
        }

        $sms_template   = 'KQ:{date} - {website}: click: {clicks}, click ko hop le: {invalidclicks}.{extra}';

        $smstpl = get_term_meta_value($this->contract->term_id, 'sms_template');
        $smstpl AND $sms_template = strip_tags(html_entity_decode($smstpl));

        $extra_data     = 'Thong tin chi tiet xin lh: 0909109394 (Mr.Tri)';

        $worker = null;
        $this->workers AND $worker = reset($this->workers);

        if( ! empty($worker))
        {
            $text = [];
            
            empty($worker['name']) OR $text['name'] = $worker['name'];
            empty($worker['user_email']) OR $text['mail'] = $worker['user_email'];
            empty($worker['user_phone']) OR $text['phone'] = $worker['user_phone'];

            $text AND $extra_data = 'Thong tin vui long LH-Ky Thuat: ' . implode(', ', $text);
        }

        $page = $this->contract->term_name;

        /* Thay đổi từ khóa nhạy cảm - không cho phép hiển thị */
        if($blacklist_keywords = $this->CI->option_m->get_value('blacklist_keywords', TRUE))
        {
            $replaces = array_map(function($k){ 
                return substr($k, 0, 2).str_repeat('*', strlen(substr($k, 2, strlen($k))));
            }, $blacklist_keywords);


            $page = str_replace($blacklist_keywords, $replaces, $page);
        }

        $search_replace = array(
            '{clicks}' => $clicks,
            '{invalidclicks}' => $invalidClicks,
            '{website}' => $page,
            '{date}' => my_date($start_date,'d/m'),
            '{extra}' => $extra_data
        );

        $message = str_replace(array_keys($search_replace), array_values($search_replace), $sms_template);

        $this->messageTemplate = $message;

        return $message;
    }


    /**
     * Loads workers.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_workers()
    {
        $kpiTypesEnums = $this->CI->config->item('googleads_service_type') ?: [];

        if(empty($kpiTypesEnums)) return $this;

        $kpiTypesEnums AND $kpiTypesEnums = array_keys($kpiTypesEnums);
        $kpiTypesEnums[] = 'tech';
        
        $kpis = $this->CI->googleads_kpi_m
        ->where_in('kpi_type', $kpiTypesEnums)
        ->order_by('kpi_type')
        ->where('term_id', $this->contract->term_id)
        ->get_all();

        if(empty($kpis)) return $this;

        $this->workers = array_map(function($x){

            $admin          = $this->CI->admin_m->get_field_by_id($x->user_id);
            $_display_name  = explode(' ', $admin['display_name']);

            $admin['last_name']         = ucfirst(convert_accented_characters(end($_display_name)));
            $admin['gender']            = get_user_meta_value($x->user_id, 'gender');
            $admin['gender_f']          = !empty($gender) ? 'Mr.' : 'Ms.';
            $admin['name']              = "{$admin['gender_f']}{$admin['last_name']}";
            $admin['user_phone']        = get_user_meta_value($x->user_id, 'user_phone');

            return $admin;

        }, $kpis);

        return $this;
    }

    /**
     * Loads recipients.
     *
     * @throws     Exception  (description)
     */
    public function loadRecipients()
    {
        $contract = $this->contract;
        $curators = get_term_meta_value($contract->term_id, 'contract_curators');
        $curators = @unserialize($curators);
        
        if(empty($curators)) 
        {
            throw new \Exception('Không tìm thấy thông tin người nhận báo cáo SMS.');
        }        

        $_recipients = array_map(function($x){
            return array(
                'name' => $x['name'] ?: '',
                'email' => $x['email'] ?: '',
                'phone' => preg_replace('/\s+/', '', $x['phone'])
            );
        }, $curators);

        if(empty($_recipients)) 
        {
            throw new \Exception('Thông tin người nhận SMS không hợp lệ.');
        }

        $this->recipients = $_recipients;
    }
}