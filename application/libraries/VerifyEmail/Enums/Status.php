<?php

namespace VerifyEmail\Enums;

use Exception;

class Status
{
    const SUCCESS = 0;
    const INVALID_EMAIL = 1;
    const INVALID_DOMAIN = 2;
    const NONEXIST_DOMAIN = 3;
    const TIMEOUT = 4;

    private static $valueToName = [
        self::SUCCESS => 'Xác thực thành công',
        self::INVALID_EMAIL => 'Email không hợp lệ',
        self::INVALID_DOMAIN => 'Domain email không hợp lệ',
        self::NONEXIST_DOMAIN => 'Domain email chưa được đăng ký hoặc không tồn tại',
        self::TIMEOUT => 'Tiến trình xác thực quá thời hạn',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new Exception(sprintf(
                    'Enum %s has no name defined for value %s', __CLASS__, $value));
        }
        return self::$valueToName[$value];
    }

    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new Exception(sprintf(
                    'Enum %s has no value defined for name %s', __CLASS__, $name));
        }
        return constant($const);
    }
}