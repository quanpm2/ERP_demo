<?php

namespace VerifyEmail;

use VerifyEmail\Enums\Status;

class VerifyEmail
{
    const CRLF = "\r\n";

    private $ci = NULL;
    private $email = null;

    protected $protocol = 'tcp';
    protected $port = 25;
    protected $from = '';
    protected $max_connection_timeout = 30;
    protected $stream_timeout = 30; // Set timeout period on a stream (expressed in the sum of seconds)
    protected $stream_timeout_wait = 0;
    protected $stream_blocking_mode = TRUE; // This affects calls like fgets() and fread() that read from the stream
    protected $stream = false;

    /**
     * __construct
     *
     * @param  mixed $email
     * @return void
     */
    public function __construct($email = null)
    {
        $this->ci = &get_instance();

        $this->email = $email;

        $this->initClass();
    }

    /**
     * initClass
     *
     * @return void
     */
    private function initClass()
    {
        $this->ci->load->model('option_m');

        $this->from = 'tanhn@adsplus.vn';
    }

    /**
     * parse_email
     *
     * @param  mixed $only_domain
     * @return string
     */
    protected function parse_email($email = '', $only_domain = TRUE): string
    {
        $email = $email ?: $this->email;

        sscanf($email, "%[^@]@%s", $user, $domain);
        return ($only_domain) ? mb_strtolower($domain) : array($user, mb_strtolower($domain));
    }

    /**
     * getMXrecords
     *
     * @param  mixed $hostname
     * @return array
     */
    protected function getMXrecords($hostname): array
    {
        $mxhosts = array();
        $mxweights = array();
        if (getmxrr($hostname, $mxhosts, $mxweights) !== FALSE) {
            array_multisort($mxweights, $mxhosts);
        }

        if (empty($mxhosts)) {
            $mxhosts[] = $hostname;
        }
        return $mxhosts;
    }

    /**
     * setEmail
     *
     * @param  mixed $email
     * @return self
     */
    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * setFrom
     *
     * @param  mixed $from
     * @return self
     */
    public function setFrom($from): self
    {
        $this->from = $from;

        return $this;
    }

    /**
     * _streamCode
     *
     * @param  mixed $str
     * @return void
     */
    protected function _streamCode($str)
    {
        preg_match('/^(?<code>[0-9]{3})(\s|-)(.*)$/ims', $str, $matches);
        $code = isset($matches['code']) ? $matches['code'] : false;
        return $code;
    }

    /**
     * _streamResponse
     *
     * @param  mixed $timed
     * @return void
     */
    protected function _streamResponse($timed = 0)
    {
        $reply = stream_get_line($this->stream, 1);
        $status = stream_get_meta_data($this->stream);

        if ($reply === FALSE && $status['timed_out'] && $timed < $this->stream_timeout_wait) {
            $reply = $this->_streamResponse($timed + $this->stream_timeout);
            log_message('DEBUG', "[VerifyEmail_{$this->email}] \$reply is FALSE. Stream response: {$reply}");

            return $this->_streamResponse($timed + $this->stream_timeout);
        }

        if ($reply !== FALSE && $status['unread_bytes'] > 0) {
            $reply .= stream_get_line($this->stream, $status['unread_bytes'], self::CRLF);
        }

        log_message('DEBUG', "[VerifyEmail_{$this->email}] Stream response: {$reply}");

        return $reply;
    }

    /**
     * _streamQuery
     *
     * @param  mixed $query
     * @return void
     */
    protected function _streamQuery($query)
    {
        log_message('DEBUG', "[VerifyEmail_{$this->email}] Stream query: {$query}");
        return stream_socket_sendto($this->stream, $query . self::CRLF);
    }

    /**
     * verify
     *
     * @param  mixed $email
     * @return Status
     */
    final public function verify($email = null)
    {
        $this->setEmail($email ?: $this->email);
        if (empty($this->email)) return FALSE;

        // Check valid email
        if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)) return Status::INVALID_EMAIL;

        $domain = $this->parse_email();

        // Check black list
        $blacklist_email_domain = $this->ci->option_m->get_value('blacklist_email_domain', TRUE) ?: [];
        if (in_array($domain, $blacklist_email_domain)) return Status::INVALID_DOMAIN;

        // Set stream
        $mxs = $this->getMXrecords($domain);
        log_message('DEBUG', "[VerifyEmail_{$this->email}] MX record list: " . implode(', ', $mxs));

        $timeout = ceil($this->max_connection_timeout / count($mxs));
        foreach ($mxs as $host) {
            $address = "{$this->protocol}://{$host}:{$this->port}";
            log_message('DEBUG', "[VerifyEmail_{$this->email}] Opening stream {$address}");

            $this->stream = @stream_socket_client($address, $err_code, $err_message, $timeout);

            if ($this->stream === FALSE) {
                log_message('DEBUG', "[VerifyEmail_{$this->email}] Code: {$err_code}. Reason: {$err_message}. Ignore stream {$address}");

                continue;
            } else {
                log_message('DEBUG', "[VerifyEmail_{$this->email}] Code: {$err_code}. Choose stream {$address}");

                log_message('DEBUG', "[VerifyEmail_{$this->email}] Setting stream");
                stream_set_timeout($this->stream, $this->stream_timeout);
                stream_set_blocking($this->stream, $this->stream_blocking_mode);

                $stream_code = $this->_streamCode($this->_streamResponse());
                if ('220' == $stream_code) {
                    break;
                } else {
                    log_message('DEBUG', "[VerifyEmail_{$this->email}] Stream code: {$stream_code}");

                    fclose($this->stream);
                    $this->stream = FALSE;
                }
            }
        }

        if ($this->stream === FALSE) {
            log_message('ERROR', "[VerifyEmail_{$this->email}] Cannot open steam.");

            return Status::NONEXIST_DOMAIN;
        }

        // Query for validate email
        set_time_limit(1); // 1 second

        $this->_streamQuery("HELO " . $this->parse_email($this->from));
        $this->_streamResponse();
        $this->_streamQuery("MAIL FROM: <{$this->from}>");
        $this->_streamResponse();
        $this->_streamQuery("RCPT TO: <{$this->email}>");
        $code = $this->_streamCode($this->_streamResponse());

        $this->_streamResponse();
        $this->_streamQuery("RSET");
        $this->_streamResponse();
        $code2 = $this->_streamCode($this->_streamResponse());

        log_message('DEBUG', "[VerifyEmail_{$this->email}] Close stream");
        $this->_streamQuery("QUIT");
        fclose($this->stream);

        $code = !empty($code2) ? $code2 : $code;
        if(empty($code)) return Status::TIMEOUT;

        switch ($code) {
            case '250':
            case '251':
            case '252':
                return Status::SUCCESS;
            default:
                return Status::NONEXIST_DOMAIN;
        }

        return Status::NONEXIST_DOMAIN;
    }
}
