<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_ui{

    private $ci;
    private $table;
    private $distinct;
    private $group_by       = array();
    private $select         = array();
    private $joins          = array();
    private $columns        = array();
    public  $where          = array();
    public  $limit          = array();
    private $or_where       = array();
    private $where_in       = array();
    private $where_not_in   = array();
    private $like           = array();
    private $order_by           = array();
    private $filter         = array();
    private $add_columns    = array();
    private $edit_columns   = array();
    private $unset_columns   = array();
    private $option_columns   = array();
    private $column_callback   = array();
    private $add_search   = array();
    private $paging_config   = array();
    private $table_template   = array();
    private $set_order = TRUE;
    public $last_query = '';
    public $filter_position = 1;

    public function __construct()
    {
        $this->ci =& get_instance();

        defined('FILTER_TOP_OUTTER') OR define('FILTER_TOP_OUTTER', 2);
        defined('FILTER_TOP_INNER') OR define('FILTER_TOP_INNER', 1);
    }

    private function get_display_result()
    {
        return $this->ci->db->get($this->table);
    }

    public function from($table)
    {
        $this->table = $table;
        return $this;
    }

    public function select($columns, $backtick_protect = TRUE)
    {
        foreach($this->explode(',', $columns) as $val)
        {
            $column = trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$2', $val));
            $column = preg_replace('/.*\.(.*)/i', '$1', $column); // get name after `.`
            if(!in_array($column, $this->columns))
                $this->columns[] =  $column;

            $this->select[$column] = $val;
            // $this->select[$column] =  trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$1', $val));
        }

        $this->ci->db->select($columns, $backtick_protect);
        return $this;
    }

    public function join($table, $fk, $type = NULL)
    {
        $this->joins[] = array($table, $fk, $type);
        // $this->ci->db->join($table, $fk, $type);
        return $this;
    }

    public function or_where($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->or_where[] = array($key_condition, $val, $backtick_protect);
        $this->ci->db->or_where($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function where($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->where[] = array($key_condition, $val, $backtick_protect);
        //$this->ci->db->where($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function limit($value, $offset = 0)
    {
        $this->limit = array($value, $offset);
        //$this->ci->db->where($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function group_by($val)
    {
        $this->group_by[] = $val;
        //$this->ci->db->group_by($val);
        return $this;
    }

    public function distinct($column)
    {
        $this->distinct = $column;
        $this->ci->db->distinct($column);
        return $this;
    }

    public function like($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->like[] = array($key_condition, $val, $backtick_protect);
        $this->ci->db->like($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function or_like($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->or_like[] = array($key_condition, $val, $backtick_protect);
        $this->ci->db->or_like($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function order_by($key_condition, $val = NULL)
    {
        $this->order_by[] = array($key_condition, $val);
        return $this;
    }

    public function where_in($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->where_in[] = array($key_condition, $val, $backtick_protect);
        return $this;
    }

    public function where_not_in($key_condition, $val, $backtick_protect = TRUE)
    {
        $this->where_not_in[] = array($key_condition, $val, $backtick_protect);
        return $this;
    }

    private function balanceChars($str, $open, $close)
    {
        $openCount = substr_count($str, $open);
        $closeCount = substr_count($str, $close);
        $retval = $openCount - $closeCount;
        return $retval;
    }


    private function explode($delimiter, $str, $open = '(', $close=')')
    {
        $retval = array();
        $hold = array();
        $balance = 0;
        $parts = explode($delimiter, $str);

        foreach($parts as $part)
        {
            $hold[] = $part;
            $balance += $this->balanceChars($part, $open, $close);
            if($balance < 1)
            {
                $retval[] = implode($delimiter, $hold);
                $hold = array();
                $balance = 0;
            }
        }

        if(count($hold) > 0)
        {
            $retval[] = implode($delimiter, $hold);
        }

        return $retval;
    }

    private function get_ordering()
    {
        return TRUE;
    }

    private function get_filtering()
    {
        if(!empty($this->filter)) foreach ($this->filter as $key => $value) 
        {

            if(empty($value))
            {

                $this->ci->db->where($key);

                continue;
            }

            $this->ci->db->like($key, $value);
        }

        return $this->ci->db;
    }

    public function add_filter($key, $value)
    {
        $this->filter[$key] = $value;
    }

    private function get_paging()
    {
        $page = $this->paging_config['cur_page'];

        $page = (isset($page) && $page >0) ? $page : 1;

        $per_page = (isset($this->paging_config['per_page']) ? $this->paging_config['per_page'] : 1);

        $iStart = ($page - 1) * $per_page;

        $iLength = $per_page;

        if($iLength != '' && $iLength > 0)
            $this->ci->db->limit($iLength, ($iStart)? $iStart : 0);
    }

    private function exec_replace($custom_val, $row_data)
    {
        $replace_string = '';

        if(isset($custom_val['replacement']) && is_array($custom_val['replacement']))
        {
            //Added this line because when the replacement has over 10 elements replaced the variable "$1" first by the "$10"
            $custom_val['replacement'] = array_reverse($custom_val['replacement'], true);

            foreach($custom_val['replacement'] as $key => $val)
            {
                $sval = preg_replace("/(?<!\w)([\'\"])(.*)\\1(?!\w)/i", '$2', trim($val));

                if(preg_match('/(\w+::\w+|\w+)\((.*)\)/i', $val, $matches) && is_callable($matches[1]))
                {
                    $func = $matches[1];

                    $args = preg_split("/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[,]+/", $matches[2], 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

                    foreach($args as $args_key => $args_val){

                        $args_val = preg_replace("/(?<!\w)([\'\"])(.*)\\1(?!\w)/i", '$2', trim($args_val));

                        $args[$args_key] = (in_array($args_val, $this->columns))? ($row_data[($this->check_cType())? $args_val : array_search($args_val, $this->columns)]) : $args_val;
                    }

                    $replace_string = call_user_func_array($func, $args);
                }
                elseif(in_array($sval, $this->columns)){

                    $replace_string = $row_data[($this->check_cType())? $sval : array_search($sval, $this->columns)];
                }
                else{

                    $replace_string = $sval;
                }

                $custom_val['content'] = str_ireplace('$' . ($key + 1), $replace_string, $custom_val['content']);
            }
        }
        return $custom_val['content'];
    }


    private function check_cType()
    {
        $column = $this->ci->input->post('columns');

        if(is_numeric(@$column[0]['data'])) return FALSE;
        else return TRUE;
    }

    public function add_column($column, $options = array(), $content = NULL, $match_replacement = NULL, $row_attrs =array())
    {
        if(is_array($column)){

            $options = array_merge($column, $options);

            return $this->add_column($column['data'], $options, $content, $match_replacement, $row_attrs);
        }

        if(!is_array($options)){

            $title = $options;

            $options = array();

            $options['title'] = $title;
        }

        $column_v = trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$2', $column));

        $column_v = preg_replace('/.*\.(.*)/i', '$1', $column_v); // get name after `.`

        if(!empty($this->unset_columns) && (in_array($column_v,$this->unset_columns) || $column_v == 'action')) 
        {
            return $this;
        }

        $options['data'] = $column_v;

        $options['row_attrs'] = $row_attrs;

        $this->columns[] =  $column_v;

        if(!isset($options['set_select'])  || $options['set_select'] != FALSE){

        // $this->select[$column_v] =  trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$1', $column));
            $this->select[$column_v] =  trim($column);
            $this->select(trim($column));
        }

        unset($options['set_select']);

        if(!isset($options['set_order'])  || $options['set_order'] != FALSE){

            $options['row_attrs']['order'] = TRUE;
        }

        unset($options['set_order']);

        if($content !== NULL){

            $this->add_columns[$column] = array('content' => $content, 'replacement' => $this->explode(',', $match_replacement));
        }

        $this->option_columns[] = $options;

        return $this;
    }

    public function get_query($filtering = FALSE)
    {
        if($filtering) $this->get_filtering();

        if(!empty($this->joins))
        {
            $this->joins = arrayUnique($this->joins);
            foreach($this->joins as $val)
            {
                $this->ci->db->join($val[0], $val[1], $val[2]);
            }
        }

        foreach($this->where as $val)
        {
            $this->ci->db->where($val[0], $val[1], $val[2]);
        }

        foreach($this->or_where as $val)
        {
            $this->ci->db->or_where($val[0], $val[1], $val[2]);
        }

        foreach($this->where_in as $val)
        {
            $this->ci->db->where_in($val[0], $val[1], $val[2]);
        }

        foreach($this->where_not_in as $val)
        {
            $this->ci->db->where_not_in($val[0], $val[1], $val[2]);
        }

        foreach($this->group_by as $val)
        {
            $this->ci->db->group_by($val);
        }

        foreach($this->like as $val)
        {
            $this->ci->db->like($val[0], $val[1], $val[2]);
        }

        if(!empty($this->limit))
        {
            $this->ci->db->limit($this->limit[0], $this->limit[1]);
        }

        if(!empty($this->order_by))
        {
            $order_by = @array_unique($this->order_by);

            $has_joined = !empty($this->joins);

            foreach ($order_by as $item)
            {
                $criteria = $item[0];
                $order = $item[1];
                if(!$has_joined)
                {
                    $this->ci->db->order_by($criteria,$order);
                    continue;
                }

                // If order criteria is custom alias (SUM|COUNT|ETC.. WITH ALIAS IN SELECT)
                if(FALSE !== strpos($criteria,'[custom-select].'))
                {
                    $criteria = str_replace('[custom-select].', '', $criteria);
                    $this->ci->db->order_by($criteria,$order);
                    continue;
                }

                // Auto prepend alias table to order criteria
                if(FALSE === strpos($criteria,'.'))
                {
                    $this->ci->db->order_by("{$this->table}.{$criteria}",$order);
                    continue;
                }

                $this->ci->db->order_by($criteria,$order);
            }
        }


        if(strlen($this->distinct) > 0){

            $this->ci->db->distinct($this->distinct);
            $this->ci->db->select($this->columns);
        }

        $result = $this->ci->db->get($this->table, NULL, NULL, FALSE);
        $this->last_query = $this->ci->db->last_query();

        return $result;
    }

    private function get_total_results($filtering = FALSE)
    {
        $result = $this->get_query($filtering);
        if(!$result) return 0;

        return $result->num_rows();
    }

    public function produce_output($results)
    {
        $this->ci->load->library('table');

        if($this->table_template)
        {
            $this->ci->table->set_template($this->table_template);
        }  

        $columns = array();

        // Setting heading for datatable
        $heading_row = $this->get_heading_components();
        if($heading_row) $this->ci->table->set_heading($heading_row);

        // Setting filter row for datatable
        $filter_row = $this->get_filter_row();
        if($filter_row) $this->ci->table->add_row($filter_row);

        // Render datatable
        if(empty($this->columns) || empty($results))
        {
            $this->ci->table->add_row(array('data'=>'Không có dữ liệu', 'colspan'=> count($this->columns), 'class'=>'text-center'));
            return $this->ci->table->generate();
        }

        $option_columns = $this->option_columns;

        // Build datatable
        foreach($results as $result)
        {
            $data = $this->build_callback_cells($this->build_row($result));

            if( ! is_array($data)) continue;

            $row = array();
            foreach ($option_columns as $opt_col)
            {
                $key = $opt_col['data'];
                $row[] = $data[$key] ?? '';
            }

            $this->ci->table->add_row($row);
        }

        return $this->ci->table->generate();
    }

    /**
    * Gets the heading components.
    *
    * @return     array  The heading components.
    */
    protected function get_heading_components()
    {
        $result = array();

        $option_columns = $this->option_columns;
        if(empty($option_columns)) return $result;

        // Initializing headings datatable
        $get_order_by = $this->ci->input->get('order_by') ?: $this->set_default_order_by();
        $order = !empty($get_order_by) ? key($get_order_by) : ''; 
        $order_by = !empty($get_order_by) ? end($get_order_by) : ''; 
        $is_sortable = $this->set_order;

        foreach ($option_columns as $col)
        {
            $col_name = $col['data'];
            $result[$col_name] = $col['title'];

            if( ! $is_sortable) continue;

            if(empty($col['row_attrs']['order'])) continue;

        // init heading + sortable
            $field = str_replace('.','-', $col_name);
            $attrs = array(
                'name'   => "order_by[{$field}]",
                'type'   => 'submit',
                'class'  => 'button-simulate',
                'value'  => 'desc',
                );

            $_icon_sort = '<i class="fa fa-fw fa-sort pull-right"></i>';
            if(strpos($order, $col['data']) !== FALSE)
            {
                $_icon_sort = '<i class="fa fa-fw fa-sort-' . $order_by . ' pull-right"></i>';
                $attrs['value'] = $order_by == 'asc' ? 'desc' : 'asc';
            }

            $result[$col_name] = form_button($attrs, $col['title'] . $_icon_sort);
        }

        return $result;
    }

    /**
    * Gets the filter row.
    *
    * @return     array  The filter row.
    */
    protected function get_filter_row()
    {
        $result = array();

        $option_columns = $this->option_columns;
        if(empty($option_columns)) return $result;

        // check filters is allowed in table
        if($this->filter_position != FILTER_TOP_INNER) return $result;

        $filter_components = $this->build_filter_components();
        if(empty($filter_components)) return $result;

        foreach($option_columns as $col)
        {
            $result[] = $filter_components[$col['data']] ?? ''; 
        }

        return $result;
    }

    /**
    * Render filter box when filter_possition is being set '2'
    *
    * @return     string  filter box HTML
    */
    protected function render_filter_box()
    {
        $html = '';

        // Check filters box is allowed render
        if($this->filter_position != FILTER_TOP_OUTTER) return $html;

        $filter_components = $this->build_filter_components();
        if(empty($filter_components)) return $html;

        return '
        <div class="clearfix"></div>
        <div class="">
            <div id="filter-panel" class="collapse in filter-panel">
                <div class="panel panel-default">
                    <div class="panel-body">'
                        .implode('', array_map(function($x){ return "<div class='col-xs-2 form-group'>{$x}</div>" ;}, $filter_components)).
                    '</div> 
                </div> 
            </div> 
        </div>';
    }

    /**
    * Builds filter components.
    *
    * @param      <type>  $out_of_table  The out of table
    *
    * @return     array   The filter components.
    */
    protected function build_filter_components($out_of_table = FALSE)
    {
        $result = array();
        $filters = $this->add_search;
        if(empty($filters)) return $result;

        foreach ($filters as $key => $filter)
        {
            if(!empty($filter['content']))
            {
                $result[$key] = $filter['content'];
                continue;
            }

            $field = strpos($key,'.') !== FALSE ? str_replace('.','-',$key) : $key;
            $name = "where[{$field}]";
            $value = $this->ci->input->get($name);
            $default = ['name' => $name,'value' => $value,'class'=>'form-control'];
            $filter['placeholder'] = $filter['placeholder'] ?? 'Search..';

            $pos = strpos($key,'.');
            if($pos !== FALSE)
            {
                $key = substr($key,($pos+1));
            }

            $result[$key] = form_input(array_merge($default, $filter));
        }

        return $result;
    }

    /**
    * Builds a row for table by db's data.
    *
    * @param      <type>  $result  The result
    *
    * @return     array   The row.
    */
    private function build_row($result)
    {
        $row = array();

        $columns = $this->columns;
        if(empty($columns)) return $row;

        $add_columns = $this->add_columns;

        foreach($columns as $col)
        {
            if(!empty($add_columns[$col]))
            {
                $row[$col] = $this->exec_replace($add_columns[$col],$result);
                continue;
            }

            if(empty($result[$col]))
            {
                $row[$col] = '';
                continue;
            }

            $row[$col] = $result[$col];
        }

        return $row;
    }

    /**
    * Builds callback cells.
    *
    * @param      <type>  $row    The row
    *
    * @return     <type>  The row.
    */
    private function build_callback_cells($row)
    {
        if(empty($row)) return $row;

        $column_callback = $this->column_callback;
        if(empty($column_callback)) return $row;

        foreach($row as $key => $val)
        {
            if(empty($column_callback[$key])) continue;

            foreach($column_callback[$key] as $callback)
            {
                $r = NULL;

                if($callback['row_data'] === TRUE)
                {
                    $r = $row[$key] = call_user_func_array($callback['function'], array($row[$key], $key, $callback['args']));
                }
                else
                {
                    $r = $row = call_user_func_array($callback['function'], array($row, $key, $callback['args']));
                }

                //if result == false then no process callback
                if($r === FALSE)
                    break 2;
            }
        }

        return $row;
    }

    private function set_default_order_by()
    {
        if( ! $this->set_order) return FALSE;

        $get_order_by = $this->ci->input->get('order_by');

        if(!empty($get_order_by) || empty($this->columns)) return FALSE;

        $primary_key = $this->ci->{$this->table.'_m'}->primary_key ?? FALSE;
        $primary_key = $primary_key ?: ($this->ci->{substr($this->table,0,-1).'_m'}->primary_key ?? FALSE);

        if($primary_key)
        {
            $field = "{$this->table}.{$primary_key}";
            $this->order_by($field, 'desc');
            return array($field=>'desc');
        }

        $primary_key = $this->ci->scache->get('primary_key-'.$this->table);
        if(!$primary_key)
        {
            $fields = $this->ci->db->field_data($this->table);

            foreach ($fields as $field)
            {
                if(empty($field->primary_key)) continue;

                $primary_key = $field->name;
                $this->ci->scache->write($primary_key, 'primary_key-'.$this->table);
                break;
            }
        }

        $field = "{$this->table}.{$primary_key}";
        $this->order_by($field, 'desc');
        return array($field=>'desc');
    }

    public function parse_relations_searches($args = array(),$replace = '.', $search = '-'){

        if(empty($args) || !is_array($args)) return FALSE;

        foreach($args as $condition => $params){

            if(!empty($params) && is_array($params)) foreach ($params as $field => $value) {

                if(!strpos($field, '-')) continue;

                $args[$condition][str_replace('-', '.', $field)] = $value;

                unset($args[$condition][$field]);
            }
        }

        return $args;
    }

    function generate_pagination($custom_config = array()){

        $this->ci->load->library('pagination');

        $total = $this->get_total_results(TRUE);

        $config = array();

    //auto detect base_url
        $this->ci->load->config('pagination');
        $prefix = $this->ci->config->item('prefix');
        $suffix = $this->ci->config->item('suffix');
        $config['base_url'] = preg_replace('/'.$prefix.'(\d+)'.$suffix.'/i','',  current_url());
        $config['base_url'] = preg_replace('/'.$suffix.'/i','', $config['base_url']);
        $config['base_url'] = rtrim($config['base_url'], '/').'/';
        $segs = $this->ci->uri->segment_array();

        $cur_page = preg_match('/'.$prefix.'(\d+)'.$suffix.'/i', current_url(), $matches) ? $matches[1] : '';

        if($this->ci->router->fetch_method() == 'index')
        {
            $tmp_segs = $segs;
    //remove page number
            array_pop($tmp_segs);
            if(end($segs) != 'index' && array_pop($tmp_segs) !='index')
            {
                $config['base_url'] = $config['base_url'].'index/';
            }
        }

        if(!$cur_page)
        {
            $config['uri_segment'] = count($segs) +1;
        }

        $config['total_rows'] = $total;

    // $config['reuse_query_string'] = TRUE;

        $config['per_page'] = $this->ci->pagination->per_page;

        if(!empty($custom_config)){

            $config = array_merge($config,$custom_config);
        }

        $this->paging_config = $config;
        $this->ci->pagination->initialize($config);
        $links = $this->ci->pagination->create_links();

        if(empty($this->ci->pagination->cur_page))
            $this->pagination_desc = '<span style="font-weight: bold;font-style: italic;float: right;">Hiển thị 1 đến '.$config['total_rows'].' trong tổng số '.$config['total_rows'].' kết quả</span>'.$links;
        else{
            $cur_page = ($this->ci->pagination->cur_page - 1 )  * $config['per_page'];
            $max_page = ($cur_page + $config['per_page']);
            $max_page = $max_page > $config['total_rows'] ? $config['total_rows'] : $max_page;
            $this->pagination_desc = '<span style="font-weight: bold;font-style: italic;float: right;">Hiển thị '.$cur_page.' đến '.$max_page.' trong tổng số '.$config['total_rows'].' kết quả</span>';
        }

        $this->paging_config['cur_page'] = $this->ci->pagination->cur_page;

        return $links;
    }

    public function add_search($column, $options = array(), $content = NULL, $match_replacement = NULL){

        if(is_array($column)){

            $options = array_merge($column, $options);

            return $this->add_search($column['data'], $options, $content, $match_replacement);
        }

        if(!is_array($options)){

            $title = $options;

            $options = array();

            $options['title'] = $title;
        }

        $this->add_search[$column] = $options;

        return $this;
    }


    public function generate($config_pagination = array(), $output = 'json', $charset = 'UTF-8'){

        $result = array();

        $result['pagination'] = $this->generate_pagination($config_pagination);

        $content = $this->ci->admin_form->form_open('',['method'=>'get','id'=>'datatable-frm']);
        $content.= form_hidden('search','1');
        $content.= $this->render_filter_box();
        $content.= $this->pagination_desc ?? '';
        $content.= $this->generate_data();
        $content.= $this->ci->admin_form->submit('','submit','submit','',array('style'=>'display:none'));
        $content.= $this->ci->admin_form->form_close();

        $result['table'] = $content;

        return $result;
    }

    public function generate_data(){

        $this->get_paging();

        $this->get_ordering();

        $this->get_filtering();

        $this->set_default_order_by();
        $this->select(implode(',',$this->select));

        $result = $this->get_query(TRUE);
        if(!$result) return FALSE;

        $results = $result->result_array();
        return $this->produce_output($results);
    }

    public function get_columns(){

        return $this->columns;
    }

    public function get_option_columns(){

        return $this->option_columns;
    }

    public function add_column_callback($column, $function, $row_data = TRUE, $args = array()){

        if (is_callable($function)){

            if(is_array($column)) {

                foreach($column as $c) {

                    $this->add_column_callback($c,$function, $row_data);
                }
            }
            else{

                $this->column_callback[$column][] = array('function'=>$function, 'row_data' =>$row_data, 'args' =>$args);
            }
        }

        return $this;
    }

    function set_table_template($template){

        $this->table_template = $template;

        return $this;
    }

    function set_ordering($condition = FALSE){
        $this->set_order = is_bool($condition) ? $condition : TRUE;
        return $this;
    }

    /**
    * Unset column
    *
    * @param string $column
    * @return mixed
    */
    public function unset_column($column)
    {
        $this->unset_columns[] = $column;
        return $this;
    }

    public function set_filter_position($position = FILTER_TOP_INNER)
    {
        $this->filter_position = $position;
        return $this;
    }
}
/* End of file Admin_ui.php */
/* Location: ./application/libraries/Admin_ui.php */