<?php
class Oneweb_Package extends Package
{
	private $website_id;

	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Get Name of Package
	 *
	 * @return     string  ( description_of_the_return_value )
	 */
	public function name()
	{
		return 'Oneweb';
	}

	/**
	 * Init Package
	 */
	public function init()
	{
		$this->_load_menu();
	}

	public function _load_menu()
	{
		if(!is_module_active('oneweb')) return FALSE;	

		$order = 1;
		$itemId = 'admin-oneweb';
		if(has_permission('oneweb.Index.access'))
		{
			$this->menu->add_item(array(
				'id' => $itemId,
				'name' => '1WEB',
				'parent' => null,
				'slug' => admin_url('oneweb'),
				'order' => $order++,
				'icon' => 'fa fa-code',
				'is_active' => is_module('oneweb')
				),'left-service');
		}

		if(!is_module('oneweb')) return FALSE;

        if(has_permission('oneweb.Index.access'))
		{
            $this->menu->add_item(array(
            'id' => 'oneweb-index',
            'name' => 'Dịch vụ',
            'parent' => $itemId,
            'slug' => admin_url('oneweb'),
            'order' => $order++,
            'icon' => 'fa fa-fw fa-xs fa-circle-o'
            ), 'left-service');
		}

		if(has_permission('oneweb.staffs.manage'))
		{
			$this->menu->add_item(array(
			'id' => 'oneweb-staffs',
			'name' => 'Phân công/Phụ trách',
			'parent' => $itemId,
			'slug' => admin_url('oneweb/staffs'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-group'
			), 'left-service');
		}

		$term_id = $this->uri->segment(4);

		$this->website_id = $term_id;

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		$left_navs = array(
			'overview'=> array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-xs fa-tachometer',
				),
			'task' => array(
				'name' =>  'Công việc',
				'icon' => 'fa fa-fw fa-xs fa-tasks',
				),
			'kpi' => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-xs fa-heartbeat',
				),
			'setting'  => array(
				'name' => 'Cấu hình',
				'icon' => 'fa fa-fw fa-xs fa-cogs',
				)
			);

		// Load menu left view-msg + count messages
		$this->load->model('message/message_m') ;
		$this->load->model('message/note_contract_m') ;
		$count_note_contract = count($this->note_contract_m->count_contract($term_id)) ;
		$left_navs['view_msg'] = array('name' => 'Ghi chú' . '<span class="label pull-right" id="count-msg">'.(int)$count_note_contract.'</span>','icon'=>'fa fa-comments-o');

		if(!empty($count_note_contract))
		{
		}

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("Oneweb.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "Oneweb-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
				), 'left-service');
		}

		
	}

	private function _update_permissions()
	{
		$permissions = array();
		
		if(!permission_exists('Oneweb.Kpi'))
		{
			$permissions['Oneweb.Kpi'] = array(
				'description' => 'Quản lý KPI',
				'actions' => array('manage','access','add','delete','update'));
		}

		if(!permission_exists('Oneweb.staffs'))
		{
			$permissions['Oneweb.staffs'] = array(
				'description' => 'Bảng phân công',
				'actions' => array('manage','access','add','delete','update'));
		}

		if(!permission_exists('Oneweb.view_msg')) 
		{
			$permissions['Oneweb.view_msg'] = array(
				'description' => 'Ghi chú',
				'actions' 	  => array('manage', 'access', 'add','delete','update')
			);
		}

		if(!$permissions) return false;
		
		foreach($permissions as $name => $value){
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function title()
	{
		return '1WEB';
	}

	public function author()
	{
		return 'thonh';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return '1WEB';
	}

	private function init_permissions()
	{
		$permissions = array();

		$permissions['Oneweb.Staffs'] = array(
			'description' => 'Bảng phân công KPI',
			'actions' => array('manage','access'));

		$permissions['Oneweb.Done'] = array(
			'description' => 'Dịch vụ đã hoàn thành',
			'actions' => array('manage','access'));

		$permissions['Oneweb.Index'] = array(
			'description' => 'Trang chính',
			'actions' => array('manage','access'));

		$permissions['Oneweb.Overview'] = array(
			'description' => 'Trang tổng quan',
			'actions' => array('manage','access'));

		$permissions['Oneweb.Task'] = array(
			'description' => 'Quản lý công việc',
			'actions' => array('manage','access','add','delete','update'));

		$permissions['Oneweb.Setting'] = array(
			'description' => 'Quản lý cấu hình',
			'actions' => array('manage','access','add','delete','update'));

		$permissions['Oneweb.Kpi'] = array(
			'description' => 'Quản lý cấu hình',
			'actions' => array('manage','access','add','delete','update'));

		$permissions['Oneweb.Start_service'] = array(
			'description' => 'Thực hiện dịch vụ',
			'actions' => array('manage'));

		$permissions['Oneweb.Stop_service'] = array(
			'description' => 'Kết thúc dịch vụ',
			'actions' 	  => array('manage'));

		$permissions['Oneweb.View_msg'] = array(
			'description' => 'Ghi chú',
			'actions' 	  => array('manage', 'access', 'add','delete','update')
		);

		return $permissions;

	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);

			$this->role_permission_m
			->insert(array(
					'role_id' => 1, //admin role
					'permission_id' => $permission_id,
					'action' => serialize($actions)
					));
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		$pers = $this->permission_m->like('name','Oneweb')->get_many_by();

		if($pers)
		foreach($pers as $per)
		{
			$this->permission_m->delete_by_name($per->name);
		}
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}