<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends API_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('api_webbuild_m');
	}

	function list()
	{
		if(!$this->authenticate)
			return parent::render();

		$user_services = $this->api_user_m->get_user_services();
		$token = $this->get('token');

		$list = array();
		$has_service = !empty($user_services['webbuild']);
		if($has_service)
		{
			$list = $this->api_webbuild_m->get_contract_list_by($token);
		}

		$data = array();
		$data['status'] = 1;
		$data['lists'] = $list;
		
		$this->default_response['auth'] = 1;

		return parent::render($data);
	}

	function contract_info()
	{
		if(!$this->authenticate)
			return parent::render();

		$data = array('status'=>0);
		$id = $this->post('id');
		$data['data'] = $this->api_webbuild_m->get_contract_info($id);
		$data['status'] = 1;

		$this->default_response['auth'] = 1;
		return parent::render($data);
	}
}