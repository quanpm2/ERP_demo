<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends REST_Controller
{
	function __construct()
	{
		parent::__construct('oneweb/rest');
		$this->load->model('usermeta_m');
		$this->load->model('termmeta_m');
		$this->load->model('permission_m');
		$this->load->model('staffs/admin_m');
		$this->load->model('oneweb/oneweb_m');

		$this->load->add_package_path(APPPATH.'third_party/erp-core/');
	}

	/**
	 * Excuse the set of start services process
	 * 
	 * 1. Create default tasklist by service package setting
	 * 2. Send activation email to all responsibilty users
	 * 3. Log trace action 
	 * 4. Detect if first contract
	 * 5. Send SMS notify to contract's customer
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     JSON
	 */
	public function proc_put()
	{
		$args = wp_parse_args(parent::put(NULL, TRUE), array('contract_id' => parent::get('contract_id', NULL)));

		if(empty($args['contract_id'])) parent::response('Tham số truyền vào không hợp lệ [0]', parent::HTTP_NOT_ACCEPTABLE);

		$is_contract = $this->oneweb_m->set_contract($args['contract_id']);
		if( ! $is_contract) parent::response('Tham số truyền vào không hợp lệ [1]', parent::HTTP_NOT_ACCEPTABLE);

		if( ! $this->oneweb_m->has_permission($args['contract_id'], 'Oneweb.start_service.manage')) parent::response('Quyền truy cập không hợp lệ. [0]', parent::HTTP_UNAUTHORIZED);

		try
		{
			if( $this->oneweb_m->is_running()) throw new Exception('Dịch vụ đã được thực hiện');
		} 
		catch (Exception $e)
		{
			parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
		}

		$this->config->load('oneweb/oneweb');

		if(empty($args['service_package']))
		{
			$args['service_package'] = get_term_meta_value($args['contract_id'], 'service_package') ?: $this->config->item('default', 'packages');
		}

		if(empty($args['tasks']))
		{
			$tasks = $this->oneweb_m->get_behaviour_m()->get_default_tasks(strtotime('+1 days', start_of_day()), $args['service_package']);
			if( ! $tasks) parent::response('Tác vụ không tìm thấy hoặc đã bị xóa', parent::HTTP_NOT_FOUND);

			$args['tasks'] 		= array_map(function($x){
				$x['post_title'] 	= $x['title'];
				$x['post_author'] 	= $this->admin_m->id;
				$x['post_type'] 	= $this->oneweb_task_m->post_type;
				return $x;
			}, $tasks);
		}

		update_term_meta($args['contract_id'], 'service_package', $args['service_package']);

		$inserted_ids = $this->oneweb_task_m->insert_many($args['tasks']);
		if(empty($inserted_ids)) parent::response('Quá trình xử lý không thành công.');
		$this->term_posts_m->set_term_posts($args['contract_id'], $inserted_ids, $this->oneweb_task_m->post_type);

		update_term_meta($args['contract_id'], 'start_service_time', time());

		if( ! $this->oneweb_m->get_behaviour_m()->send_activation_email('customer'))
			parent::response('Quá trình gửi Email không thành công.', parent::HTTP_NOT_ACCEPTABLE);

		// Detect if contract is the first signature (tái ký | ký mới)
		$this->load->model('contract/base_contract_m');
		$this->base_contract_m->detect_first_contract($args['contract_id']);

		// Send SMS to customer
		$this->load->model('contract/contract_report_m');
		$this->contract_report_m->send_sms_activation_2customer($args['contract_id']);

		parent::response('Dịch vụ đã được kích hoạt thực hiện thành công.');
	}
}
/* End of file Contract.php */
/* Location: ./application/modules/wservice/controllers/api/Contract.php */