<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class Resource extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'form_validation';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'oneweb_m';
        $this->autoload['models'][] = 'admin_m';
        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'webgeneral/webgeneral_m';
        $this->autoload['models'][] = 'webgeneral/webgeneral_kpi_m';
        $this->autoload['models'][] = 'oneweb_contract_m';
        $this->autoload['models'][] = 'log_m';

        parent::__construct();

        $this->load->config('oneweb/oneweb');
        $this->load->config('oneweb/kpi');
        $this->load->config('webgeneral/webgeneral');
    }

    public function config_get()
    {
        if (!has_permission('oneweb.overview.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $data = [];

        $kpi_type = $this->config->item('kpi_type');
        $data['kpi_type'] = array_values($kpi_type);

        return parent::responseHandler($data, 'Lấy thông tin thành công.');
    }

    public function tech_staffs_get($contractId = 0)
    {
        if (!has_permission('oneweb.kpi.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        if (empty($contractId)) {
            return $this->get_many_tech_staffs();
        } else {
            $inputs = wp_parse_args(parent::post(), [
                'contractId' => $contractId,
            ]);

            // Validate
            $rules = array(
                [
                    'field' => 'contractId',
                    'label' => 'contractId',
                    'rules' => [
                        'required',
                        'integer',
                        array('existed_check', array($this->oneweb_m, 'existed_check'))
                    ]
                ],
            );

            $this->form_validation->set_data($inputs);
            $this->form_validation->set_rules($rules);
            if (FALSE == $this->form_validation->run()) {
                return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
            }

            return  $this->get_tech_staffs_by_contract_id($inputs['contractId']);
        }
    }

    protected function get_many_tech_staffs()
    {
        $roles = $this->option_m->get_value('group_oneweb_ids', TRUE);
        $users = $this->admin_m
            ->select('user_id,display_name,user_email')
            ->set_role($roles)->set_get_active()
            ->order_by('display_name')
            ->as_array()
            ->get_all();

        if (empty($users)) {
            return parent::responseHandler(NULL, 'Không có thông tin kỹ thuật.', 'success', 204);
        }

        return parent::responseHandler($users, 'Lấy thông tin thành công.');
    }

    protected function get_tech_staffs_by_contract_id($contract_id)
    {
        $kpis = $this->webgeneral_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(['term_id' => $contract_id]);
        if (empty($kpis)) {
            return parent::responseHandler(NULL, 'Không có thông tin kỹ thuật.', 'success', 204);
        }

        $kpi_type = $this->config->item('kpi_type');

        $data = [];
        foreach ($kpis as $kpi) {
            $staff_name = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');
            $staff_email = $this->admin_m->get_field_by_id($kpi['user_id'], 'user_email');
            $kpi_label = !empty($kpi_type[$kpi['kpi_type']]) ? $kpi_type[$kpi['kpi_type']]['label'] : '--';

            $data[] = [
                'kpi_id' => $kpi['kpi_id'],
                'display_name' => $staff_name,
                'user_email' => $staff_email,
                'kpi_type' => $kpi_label
            ];
        }

        return parent::responseHandler($data, 'Lấy thông tin thành công.');
    }

    public function kpi_post($contract_id)
    {
        if (!has_permission('oneweb.kpi.add')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::post(), ['contract_id' => $contract_id]);

        // Validate
        $kpi_type_config = array_keys($this->config->item('kpi_type'));

        $rules = array(
            [
                'field' => 'contract_id',
                'label' => 'contract_id',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->oneweb_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'staff_id',
                'label' => 'staff_id',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', function ($value) {
                        return $this->admin_m->existed_check($value);
                    })
                ]
            ],
            [
                'field' => 'kpi_type',
                'label' => 'kpi_type',
                'rules' => [
                    'required',
                    'in_list[' . implode(',', $kpi_type_config) . ']'
                ]
            ]
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::response(['code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array()]);
        }

        // Process
        // ??? Hop dong trang thai nao duoc add tech staff
        $kpi_datetime = time();
        $this->webgeneral_kpi_m->update_kpi_value($inputs['contract_id'], $inputs['kpi_type'], 1, $kpi_datetime, $inputs['staff_id']);

        $this->term_users_m->set_relations_by_term($inputs['contract_id'], [$inputs['staff_id']], 'admin');


        return parent::responseHandler([], 'Thêm kỹ thuật thành công.');
    }

    /**
     * @return json
     */
    public function kpi_delete($contractId, $kpiId)
    {
        if (!has_permission('oneweb.kpi.delete')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::delete(), [
            'contractId' => $contractId,
            'kpiId' => $kpiId
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->oneweb_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'kpiId',
                'label' => 'kpiId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->webgeneral_kpi_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $this->webgeneral_kpi_m->delete_cache($kpiId);
        $this->webgeneral_kpi_m->delete($kpiId);

        return parent::responseHandler([], 'Xoá kỹ thuật thành công.');
    }

    public function contracts_get($contract_id)
    {
        if (!has_permission('oneweb.overview.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), [
            'contract_id' => $contract_id,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contract_id',
                'label' => 'contract_id',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->oneweb_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        // Process
        $data = [];

        $data['term_id'] = $inputs['contract_id'];

        $contract_code = get_term_meta_value($inputs['contract_id'], 'contract_code');
        $data['contract_code'] = $contract_code ?: '';

        $contract_begin = get_term_meta_value($inputs['contract_id'], 'contract_begin');
        $data['contract_begin'] = isset($contract_begin) ? my_date($contract_begin, 'd/m/Y') : '';

        $contract_end = get_term_meta_value($inputs['contract_id'], 'contract_end');
        $data['contract_end'] = isset($contract_end) ? my_date($contract_end, 'd/m/Y') : '';

        $contract_region = get_term_meta_value($inputs['contract_id'], 'contract_region');
        $data['contract_region'] = $contract_region ?: '';

        $contract_value = get_term_meta_value($inputs['contract_id'], 'contract_value');
        $data['contract_value'] = (int) $contract_value ?: 0;

        $start_server_time = get_term_meta_value($inputs['contract_id'], 'start_server_time');
        $data['start_server_time'] = isset($start_server_time) ? my_date($start_server_time, 'd/m/Y') : '';

        $representative_gender = get_term_meta_value($inputs['contract_id'], 'representative_gender');
        $data['representative_gender'] = $representative_gender ?: '';

        $representative_name = get_term_meta_value($inputs['contract_id'], 'representative_name');
        $data['representative_name'] = $representative_name ?: '';

        $representative_email = get_term_meta_value($inputs['contract_id'], 'representative_email');
        $data['representative_email'] = $representative_email ?: '';

        $representative_phone = get_term_meta_value($inputs['contract_id'], 'representative_phone');
        $data['representative_phone'] = $representative_phone ?: '';

        $sale_id = get_term_meta_value($inputs['contract_id'], 'staff_business');
        $data['staff_business_id'] = $sale_id ?: '';
        $data['staff_business_name'] = $this->admin_m->get_field_by_id($sale_id, 'display_name') ?: '';
        $data['staff_business_email'] = $this->admin_m->get_field_by_id($sale_id, 'user_email') ?: '';
        $data['staff_business_avatar'] = $this->admin_m->get_field_by_id($sale_id, 'user_avatar') ?: '';

        $data['technical_staffs'] = [];
        $kpis = $this->webgeneral_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(['term_id' => $inputs['contract_id']]);
        if (empty($kpis)) {
            $data['technical_staffs'] = [];
        } else {
            $kpi_type = $this->config->item('kpi_type');

            foreach ($kpis as $kpi) {
                $tech_name = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');
                $tech_email = $this->admin_m->get_field_by_id($kpi['user_id'], 'user_email');
                $tech_avatar = $this->admin_m->get_field_by_id($kpi['user_id'], 'user_avatar');

                $kpi_label = !empty($kpi_type[$kpi['kpi_type']]) ? $kpi_type[$kpi['kpi_type']]['label'] : '';

                $staff = [
                    'id' => $kpi['user_id'],
                    'name' => $tech_name,
                    'email' => $tech_email,
                    'avatar' => $tech_avatar,
                    'type' => $kpi_label,
                ];
                $data['technical_staffs'][] = $staff;
            }
        }

        return parent::responseHandler($data, 'Lấy thông tin thành công');
    }


    public function start_service_post($contractId)
    {
        if (!has_permission('oneweb.start_service.manage')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::post(), [
            'contractId' => $contractId,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->oneweb_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        // Process
        $this->oneweb_m->set_contract($inputs['contractId']);

        try {
            if ($this->oneweb_m->is_running()) throw new Exception('Dịch vụ đã được thực hiện');
        } catch (Exception $e) {
            return parent::responseHandler(NULL, $e->getMessage(), 'error', 400);
        }

        $this->config->load('oneweb/oneweb');

        if (empty($inputs['service_package'])) {
            $inputs['service_package'] = get_term_meta_value($inputs['contractId'], 'service_package') ?: $this->config->item('default', 'packages');
        }

        update_term_meta($inputs['contractId'], 'start_service_time', time());

        if (!$this->oneweb_m->get_behaviour_m()->send_activation_email('customer'))
            return parent::responseHandler(NULL, 'Quá trình gửi Email không thành công.', 'error', 400);

        // Detect if contract is the first signature (tái ký | ký mới)
        $this->load->model('contract/base_contract_m');
        $this->base_contract_m->detect_first_contract($inputs['contractId']);

        // Send SMS to customer
        $this->load->model('contract/contract_report_m');
        $this->contract_report_m->send_sms_activation_2customer($inputs['contractId']);

        return parent::responseHandler(NULL, 'Kích hoạt thành công.');
    }

    public function stop_service_post($contractId)
    {
        if (!has_permission('oneweb.stop_service.manage')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 400);
        }

        $inputs = wp_parse_args(parent::post(), [
            'contractId' => $contractId,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->oneweb_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        // Process
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);

        $term = $this->oneweb_m->get($inputs['contractId']);
        $status = $this->oneweb_contract_m->stop_service($term);
        if (FALSE === $status) {
            return parent::responseHandler(null, 'Có lỗi xảy ra trong quá trình kích hoạt.', 'error', 400);
        }

        $response['msg']    = 'Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".';
        $response['status']    = TRUE;

        return parent::responseHandler(NULL, 'Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
    }
}
/* End of file Resource.php */
/* Location: ./application/modules/oneweb/controllers/api_v2/Resource.php */