<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class OnewebDataset extends MREST_Controller
{
    protected $permission = 'oneweb.index.access';

    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'oneweb_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'admin_m';
        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'webgeneral/webgeneral_kpi_m';

        parent::__construct();

        $this->load->config('onead/onead');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response   = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        $relate_users       = $this->admin_m->get_all_by_permissions($this->permission);

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        if($relate_users === FALSE)
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        if(is_array($relate_users))
        {
            $this->datatable_builder->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        // Prepare search
        $courseTypeConfig = $this->config->item('course_type');

        $courseStatusConfig = $this->config->item('course_status');

        $service_packages_config = $this->config->item('service', 'packages');
        $service_packages_config = key_value($service_packages_config, 'name', 'label');

        // Build datatable
        $data = $this->data;
        $this->search_filter_receipt();

        $data['content'] = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')
            ->select('term.term_id, term.term_name')

            ->add_search('contract_code', ['placeholder' => 'Hợp đồng'])
            ->add_search('contract_begin', ['placeholder' => 'T/G bắt đầu', 'class' => 'form-control set-datepicker'])
            ->add_search('contract_end', ['placeholder' => 'T/G kết thúc', 'class' => 'form-control set-datepicker'])
            ->add_search('technical_staffs', ['placeholder' => 'Kỹ thuật thực hiện'])
            ->add_search('staff_business', ['placeholder' => 'Nhân viên kinh doanh'])
            ->add_search('years', ['placeholder' => 'Số lượng'])
            ->add_search('service_package_price', ['placeholder' => 'Đơn giá'])
            ->add_search('payment_percentage', ['placeholder' => '% T.Toán'])

            ->add_column('contract_code', array('title' => 'Hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_begin', array('title' => 'T/G bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_end', array('title' => 'T/G kết thúc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('technical_staffs', array('title' => 'Kỹ thuật', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('staff_business', array('title' => 'NVKD', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('years', array('title' => 'Số lượng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_package_price', array('title' => 'Đơn giá', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('oneweb_task', array('title' => 'Tiến độ tác vụ', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('payment_percentage', array('title' => '% T.Toán', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('action', array('title' => 'Hành động', 'set_select' => FALSE, 'set_order' => FALSE))

            ->add_column_callback('term_id', function ($data, $row_name) use ($service_packages_config) {
                $term_id = (int)$data['term_id'];

                $data['term_id'] = $term_id;

                // Get contract_code
                $contract_code = get_term_meta_value($data['term_id'], 'contract_code');
                $data['contract_code'] = $contract_code ?? '--';

                // Get contract_begin
                $contract_begin = get_term_meta_value($data['term_id'], 'contract_begin');
                $data['contract_begin'] = $contract_begin ? my_date($contract_begin, 'd/m/Y') : '--';

                // Get contract_end
                $contract_end = get_term_meta_value($data['term_id'], 'contract_end');
                $data['contract_end'] = $contract_end ? my_date($contract_end, 'd/m/Y') : '--';

                // Get technical_staffs
                $tech_kpis = $this->webgeneral_kpi_m->get_kpis($term_id, 'users', 'tech');
                if (!empty($tech_kpis)) {
                    $tech_ids = array();
                    foreach ($tech_kpis as $item) $tech_ids = array_merge($tech_ids, array_keys($item));

                    $data['technical_staffs'] = implode(', ', array_map(function ($x) {
                        return '<p>' . $this->admin_m->get_field_by_id($x, 'display_name') ?: $this->admin_m->get_field_by_id($x, 'user_mail') . '</p>';
                    }, $tech_ids));

                    // Load user's group
                    if (!empty($tech_ids)) {
                        $user_groups_f = implode(', ', array_unique(array_map(function ($x) {

                            $ugroups = $this->term_users_m->get_user_terms($x, 'user_group');
                            if (empty($ugroups)) return NULL;

                            return implode(', ', array_map(function ($x) {
                                return $x->term_name;
                            }, $ugroups));
                        }, $tech_ids)));

                        $data['technical_staffs'] .= "<br/><small><b><u><i>{$user_groups_f}</i></u></b></small>";
                    }
                } else {
                    $data['technical_staffs'] = '--';
                }

                // Get staff_business
                $sale_id = get_term_meta_value($term_id, 'staff_business');
                $staff_business = $this->admin_m->get_field_by_id($sale_id, 'display_name');
                $data['staff_business'] = $staff_business ?: '--';

                // Get years
                $years = (int)get_term_meta_value($data['term_id'], 'years');
                $data['years'] = $years ?: 0;

                // Get service_package_price
                $service_package_price = (int)get_term_meta_value($data['term_id'], 'service_package_price');
                $data['service_package_price'] = $service_package_price ? currency_numberformat($service_package_price, 'đ') : currency_numberformat(0, 'đ');

                // Get oneweb_task
                $tasks = $this->term_posts_m->get_term_posts($term_id, $this->oneweb_task_m->post_type) ?: [];
                $uncomplete_tasks = array();
                $count_tasks = count($tasks);
                $count_uncomplete_tasks = 0;
                $count_complete_tasks = 0;
                if ($count_tasks > 0 && !empty($tasks)) {
                    $uncomplete_tasks         = array_filter($tasks, function ($task) {
                        return ($task->post_status != 'complete');
                    });
                    $count_uncomplete_tasks = count($uncomplete_tasks);
                    $count_complete_tasks     = $count_tasks - $count_uncomplete_tasks;
                }

                $count_complete_tasks = numberformat($count_complete_tasks);
                $count_tasks = numberformat($count_tasks);
                $tasks_percentage = div($count_complete_tasks, $count_tasks)  * 100;

                $data['oneweb_task'] = [
                    'count_complete_tasks' => $count_complete_tasks,
                    'count_tasks' => $count_tasks,
                    'tasks_percentage' => $tasks_percentage,
                ];

                // Get payment_percentage
                $invs_amount = (float)get_term_meta_value($term_id, 'invs_amount');
                $payment_amount = (float)get_term_meta_value($term_id, 'payment_amount');
                $payment_percentage = 100 * (float) get_term_meta_value($term_id, 'payment_percentage');
                $data['payment_progress'] = [
                    'payment_amount' => $payment_amount,
                    'invs_amount' => $invs_amount,
                    'payment_percentage' => $payment_percentage,
                ];

                // Add on
                $data['is_service_proc'] = is_service_proc($term_id);
                $data['is_service_end'] = is_service_end($term_id);

                return $data;
            }, FALSE)
            ->where('term.term_type', $this->oneweb_m->term_type)
            ->where('term.term_status', 'publish')
            ->from('term')
            ->group_by('term.term_id');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);
        // dd($this->datatable_builder->last_query());


        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export_oneweb($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function search_filter_receipt($args = array())
    {
        restrict($this->permission);

        $args = parent::get();
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $alias = uniqid('contract_code_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_contract_code);
            }
        }

        // contract_begin FILTERING & SORTING
        $filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
        $sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
        if ($filter_contract_begin || $sort_contract_begin) {
            $dates = explode(' - ', $args['where']['contract_begin']);

            $alias = uniqid('contract_begin_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

            if ($filter_contract_begin) {
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_begin) {
                $this->datatable_builder->group_by("{$alias}.meta_value")->order_by("{$alias}.meta_value", $sort_contract_begin);
            }
        }

        // contract_end FILTERING & SORTING
        $filter_contract_end = $args['where']['contract_end'] ?? FALSE;
        $sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
        if ($filter_contract_end || $sort_contract_end) {
            $dates = explode(' - ', $args['where']['contract_end']);

            $alias = uniqid('contract_end_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'contract_end'", 'LEFT');

            if ($filter_contract_end) {
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_end) {
                $this->datatable_builder->group_by("{$alias}.meta_value")->order_by("{$alias}.meta_value", $sort_contract_end);
            }
        }

        // technical_staffs FILTERING & SORTING
        $filter_technical_staffs = $args['where']['technical_staffs'] ?? FALSE;
        $sort_technical_staffs = $args['order_by']['technical_staffs'] ?? FALSE;
        if ($filter_technical_staffs) {
            $alias_webgeneral_kpi = uniqid('webgeneral_kpi_');
            $alias_user = uniqid('technical_staffs');

            $this->datatable_builder->join("webgeneral_kpi {$alias_webgeneral_kpi}", "term.term_id = {$alias_webgeneral_kpi}.term_id", 'LEFT');
            $this->datatable_builder->join("user {$alias_user}", "{$alias_user}.user_id = {$alias_webgeneral_kpi}.user_id", 'LEFT');

            if ($filter_technical_staffs) {
                $this->datatable_builder->like("{$alias_user}.user_email", $filter_technical_staffs)
                    ->or_like("{$alias_user}.display_name", $filter_technical_staffs);
            }

            if ($sort_technical_staffs) {
                $this->datatable_builder->select("{$alias_user}.user_id")
                    ->order_by("{$alias_user}.user_email", $sort_technical_staffs)
                    ->order_by("{$alias_user}.display_name", $sort_technical_staffs)
                    ->group_by("{$alias_user}.user_id");
            }
        }

        // staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if ($filter_staff_business || $sort_staff_business) {
            $alias_staff_business = uniqid('staff_business_');
            $alias_user = uniqid('staff_business_');

            $this->datatable_builder->join("termmeta {$alias_staff_business}", "term.term_id = {$alias_staff_business}.term_id and {$alias_staff_business}.meta_key = 'staff_business'", 'LEFT')
                ->join("user {$alias_user}", "{$alias_staff_business}.meta_value = {$alias_user}.user_id and {$alias_user}.user_type = 'admin'", 'LEFT');

            if ($filter_staff_business) {
                $this->datatable_builder->like("{$alias_user}.display_name", $filter_staff_business);
            }

            if ($sort_staff_business) {
                $this->datatable_builder->order_by("{$alias_user}.display_name", $sort_staff_business)
                    ->group_by("{$alias_user}.user_id");
            }
        }

        // years FILTERING & SORTING
        $filter_years = $args['where']['years'] ?? FALSE;
        $sort_years = $args['order_by']['years'] ?? FALSE;
        if ($filter_years || $sort_years) {
            $alias = uniqid('years_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'years'", 'LEFT');

            if ($filter_years) {
                $this->datatable_builder->where("{$alias}.meta_value >=", (int)$filter_years);
            }

            if ($sort_years) {
                $this->datatable_builder->order_by("cast({$alias}.meta_value as unsigned)", $sort_years)
                    ->group_by("{$alias}.meta_value");
            }
        }

        // service_package_price FILTERING & SORTING
        $filter_service_package_price = $args['where']['service_package_price'] ?? FALSE;
        $sort_service_package_price = $args['order_by']['service_package_price'] ?? FALSE;
        if ($filter_service_package_price || $sort_service_package_price) {
            $alias = uniqid('service_package_price_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'service_package_price'", 'LEFT');

            if ($filter_service_package_price) {
                $this->datatable_builder->where("{$alias}.meta_value >=", (int)$filter_service_package_price);
            }

            if ($sort_service_package_price) {
                $this->datatable_builder->order_by("cast({$alias}.meta_value as unsigned)", $sort_service_package_price)
                    ->group_by("{$alias}.meta_value");
            }
        }

        // payment_percentage FILTERING & SORTING
        $filter_payment_percentage = $args['where']['payment_percentage'] ?? FALSE;
        $sort_payment_percentage = $args['order_by']['payment_percentage'] ?? FALSE;
        if ($filter_payment_percentage || $sort_payment_percentage) {
            $alias = uniqid('payment_percentage_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'payment_percentage'", 'LEFT');

            if ($filter_payment_percentage) {
                $this->datatable_builder->where("{$alias}.meta_value >=", (float)$filter_payment_percentage);
            }

            if ($sort_payment_percentage) {
                $this->datatable_builder->order_by("cast({$alias}.meta_value as unsigned)", $sort_payment_percentage)
                    ->group_by("{$alias}.meta_value");
            }
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function export_oneweb($query = '')
    {
        restrict($this->permission);

        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $onewebs = $this->oneweb_m->query($query)->result();
        if (!$onewebs) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $this->load->library('excel');
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array('memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
            ->getProperties()
            ->setCreator("WEBDOCTOR.VN")
            ->setLastModifiedBy("WEBDOCTOR.VN")
            ->setTitle(uniqid('Danh sách dịch vụ Domain __'));

        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/oneweb/oneweb-service-list-export.xlsx');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $row_index = 3;

        foreach ($onewebs as $key => &$oneweb) {
            // Prepare data
            $term_id = (int)$oneweb->term_id;

            $contract_code = get_term_meta_value($term_id, 'contract_code') ?? '--';

            $contract_begin = get_term_meta_value($term_id, 'contract_begin');
            $contract_begin = $contract_begin ? my_date($contract_begin, 'd/m/Y') : '--';

            $contract_end = get_term_meta_value($term_id, 'contract_end');
            $contract_end = $contract_end ? my_date($contract_end, 'd/m/Y') : '--';

            $technical_staffs = '--';
            $tech_kpis = $this->webgeneral_kpi_m->get_kpis($term_id, 'users', 'tech');
            if (!empty($tech_kpis)) {
                $tech_ids = array();
                foreach ($tech_kpis as $item) $tech_ids = array_merge($tech_ids, array_keys($item));

                $technical_staffs = implode(', ', array_map(function ($x) {
                    return $this->admin_m->get_field_by_id($x, 'display_name') ?: $this->admin_m->get_field_by_id($x, 'user_mail');
                }, $tech_ids));
            }

            $sale_id = get_term_meta_value($term_id, 'staff_business');
            $staff_business = $this->admin_m->get_field_by_id($sale_id, 'display_name') ?: '--';

            $years = (int)get_term_meta_value($term_id, 'years');
            $years = $years ? $years . ' năm' : '--';

            $service_package_price = (int)get_term_meta_value($term_id, 'service_package_price');

            $tasks = $this->term_posts_m->get_term_posts($term_id, $this->oneweb_task_m->post_type) ?: [];
            $uncomplete_tasks = array();
            $count_tasks = count($tasks);
            $count_uncomplete_tasks = 0;
            $count_complete_tasks = 0;
            if ($count_tasks > 0 && !empty($tasks)) {
                $uncomplete_tasks         = array_filter($tasks, function ($task) {
                    return ($task->post_status != 'complete');
                });
                $count_uncomplete_tasks = count($uncomplete_tasks);
                $count_complete_tasks     = $count_tasks - $count_uncomplete_tasks;
            }

            $count_complete_tasks = numberformat($count_complete_tasks);
            $count_tasks = numberformat($count_tasks);
            $tasks_percentage = div($count_complete_tasks, $count_tasks)  * 100;

            $row_number = $row_index + $key;

            $i = 0;

            // Set Cell
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $key + 1);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_code);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_begin);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_end);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $technical_staffs);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $staff_business);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $years);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $service_package_price);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $tasks_percentage);
        }

        $numbers_of_domain = count($onewebs);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $row_index . ':A' . ($numbers_of_domain + $row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $file_name = "tmp/export-oneweb-service.xlsx";
        $objWriter->save($file_name);

        try {
            $objWriter->save($file_name);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        if (file_exists($file_name)) {
            $this->load->helper('download');
            force_download($file_name, NULL);
        }

        return FALSE;
    }
}
/* End of file OnewebDataset.php */
/* Location: ./application/modules/oneweb/controllers/api_v2/OnewebDataset.php */