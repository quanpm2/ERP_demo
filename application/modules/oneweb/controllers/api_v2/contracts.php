<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contracts extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'oneweb/oneweb_m';

        parent::__construct($config);

        $this->load->config('oneweb/oneweb');
    }



    public function service_put($id)
    {
        if(FALSE === $this->oneweb_m->has_permission($id, 'admin.contract.view'))
        {
            parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
        }

        $response = [ 'code' => 400, 'messages' => [] ];

        $this->load->library('form_validation');

        $args                   = parent::put(NULL, TRUE);
        $service_package_config = $this->config->item('service', 'packages');
        $enumsServicePackages   = array_column($service_package_config, 'name');

        $this->form_validation->set_data($args);
        $this->form_validation->set_rules('service_package', 'Gói dịch vụ', 'required|in_list['.implode(',', $enumsServicePackages).']');
        $this->form_validation->set_rules('years', 'Số năm', 'required|integer|greater_than_equal_to[1]|less_than_equal_to[10]');
        $this->form_validation->set_rules('service_package_price', 'Chi phí', 'required|integer');

        if( FALSE == $this->form_validation->run())
        {
            parent::response([ 'code' => 400, 'error' => $this->form_validation->error_array()]);
        }

        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                          && FALSE == (bool)get_term_meta_value($id, 'is_manipulation_locked');
        if($is_manipulation_locked)
        {
            $started_service = (int) get_term_meta_value($id, 'started_service') ?: time();
            $started_service = end_of_day($started_service);
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
            if($manipulation_locked_at > $started_service)
            {
                $manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                parent::response([ 'code' => parent::HTTP_NOT_ACCEPTABLE, 'error' => "Hợp đồng đã khoá thao tác lúc {$manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá."]);
            }
        }

        if( ! empty($service_package_config[$args['service_package']]['items']))
        {
            update_term_meta($id, 'service_package_items', serialize($service_package_config[$args['service_package']]['items']));
        }

        isset($args['service_package'])         AND update_term_meta($id, 'service_package', $args['service_package']);
        isset($args['service_package_price'])   AND update_term_meta($id, 'service_package_price', $args['service_package_price']);
        isset($args['years'])                   AND update_term_meta($id, 'years', $args['years']);
        isset($args['discount_amount'])         AND update_term_meta($id, 'discount_amount', $args['discount_amount']);


        if(isset($args['advanced_functions']))
        {
            $advanced_functions = array_filter($args['advanced_functions'], function($x){
                return !empty($x['name']);
            });

            update_term_meta($id, 'advanced_functions', serialize($advanced_functions));   
        }

        if( isset($args['discount_plan']) && ! empty($args['discount_plan']))
        {
            $this->config->load('oneweb/discount');
            switch ($args['discount_plan'])
            {
                /* CHƯƠNG TRÌNH MUA 2 TẶNG 1 */
                case 'combo':
                    $discount_packages_conf = $this->config->item('packages', 'discount');
                    if( empty($discount_packages_conf[$args['discount_plan']])) 
                    {
                        $response['error'] = 'Chương trình giảm giá không còn khả dụng.';
                        parent::response($response);
                    }

                    $args['discount_amount']    = 0;
                    $_years                     = $args['years'];
                    if($availSaleOpts   = array_filter($discount_packages_conf['combo']['sale'], function($opt) use ($_years){ return $opt['buy'] < $_years; }))
                    {
                        $validSaleOpt   = end($availSaleOpts);
                        $args['discount_amount'] = $validSaleOpt ? ($validSaleOpt['free']*$args['service_package_price']) : 0;
                    }

                    break;
                    
                /* CÁC CHƯƠNG TRÌNH GIẢM GIÁ MẶC ĐỊNH */
                default:

                    $discount_packages_conf = $this->config->item('packages', 'discount');

                    /* VALIDATE CHƯƠNG TRÌNH GIẢM GIÁ THEO CẤU HÌNH */
                    if( empty($discount_packages_conf[$args['discount_plan']])) 
                    {
                        $response['error'] = 'Chương trình giảm giá không còn khả dụng.';
                        parent::response($response);
                    }

                    $this->load->model('contract/discount_m');
                    $discount_m = $this->discount_m->set_contract($id)->get_instance();

                    $service_package_price 	= (int) get_term_meta_value($id, 'service_package_price');
                    $years 					= ((int) get_term_meta_value($id, 'years')) ?: 1;

                    /* Tính năng nâng cao */
                    $advanced_functions_price 	= 0;
                    $advanced_functions 		= unserialize(get_term_meta_value($id, 'advanced_functions'));
                    $advanced_functions AND $advanced_functions_price = array_sum(array_column($advanced_functions, 'value'));

                    $price = ($service_package_price*$years) + $advanced_functions_price;

                    try
                    { 
                        $discount_m->validate($args['discount_amount'], $price, $args['discount_plan']);
                    }
                    catch (Exception $e)
                    {
                        $response['error'] = $e->getMessage();
                        parent::response($response);
                    }

                    break;
            }
        }

        try
        {
            $contract = $this->oneweb_m->set_contract($id);
            update_term_meta($id, 'contract_value', $this->oneweb_m->calc_contract_value());

            $contract->delete_all_invoices(); // Delete all invoices available
            $contract->create_invoices(); // Create new contract's invoices
            $contract->sync_all_amount(); // Update all invoices's amount & receipt's amount

            $response['code']       = 200;
            $response['messages'][] = 'Đợt thanh toán đã đồng bộ thành công !';
            $response['messages'][] = 'Số liệu thu chi đã được đồng bộ thành công !';
        }
        catch (Exception $e)
        {
            parent::response([ 'code' => 400, 'error' => $e->getMessage() ]);
        }

        parent::response($response);
    }
}
/* End of file Contracts.php */
/* Location: ./application/modules/oneweb/controllers/api_v2/Contracts.php */