<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends Admin_Controller {
	public $model = 'seotraffic_m';
	public $term_type = 'webdesign';
	public $website_id = 0;
	public $term = FALSE;
	public $contract_id = 0;

	function __construct()
	{
		// Modules::load('webdoctor');
		parent::__construct();
		$this->load->model('webbuild_m');
		$this->post_type = 'webbuild-task';
		$_table = 'posts';
	}


	public function task($id = 0)
	{
		
	}

	public function ajax_edit($post_id = 0, $post_type = '')
	{
		
	}

	public function ajax_delete($post_id = 0)
	{
		
	}
	function update_status($post_id = 0, $status = '')
	{
		
	}
	public function update($term_id = 0)
	{
		
		// Kiểm tra term có tồn tại
		$check_term = $this->term_m->where('term_id',$term_id)->get_all();
		if(empty($check_term)){
			$result['success'] 	= FALSE;
			$result['msg'] 	= 'Lỗi!!! vui lòng kiểm tra lại Term_id';
			return parent::renderJson($result);
		}
		 // Kiểm tra post(task) có liên kết với term hay không
		$check_term_post = $this->term_posts_m->get_term_posts($term_id,$this->post_type);
		if(!$check_term_post){
			$result['success'] 	= FALSE;
			$result['msg'] 	= 'Lỗi!!! vui lòng kiểm tra lại Term_id';
			return parent::renderJson($result);
		}
		$post  = $this->input->post('edit[meta]');
		// Kiểm tra trạng thái , nếu hoàn thanh => show thông báo không cho chỉnh sửa
		//$post['post_status'] ='process';
		if($post['check_complete']=="complete"){
			$result['success'] 	= FALSE;
			$result['msg'] 	= 'Không thể chỉnh sửa Task đã hoàn thành';
			return parent::renderJson($result);
		}
		  
		$post['post_status']=='on'?$post['post_status']='process':$post['post_status']='complete';
		
		

		$update = array();
		$result = array();
		$meta_key = 'sms_status';

		

		$post['sms_status']=='on' ? $post['sms_status']=1:$post['sms_status']=0;
		$post['start_date'] = str_replace('/', '-',$post['start_date']);
		$post['end_date'] = str_replace('/', '-',$post['end_date']);
		$post['start_date'] = $this->mdate->startOfDay($post['start_date']);
		$post['end_date'] = $this->mdate->endOfDay($post['end_date']);

		// $post['start_date'] = "".$post['start_date'].""." 00:00:00";
		// $post['end_date'] = "".$post['end_date'].""." 23:59:59";
		// $post['start_date'] = strtotime(str_replace('/', '-',$post['start_date']));
		// $post['end_date'] = strtotime(str_replace('/', '-',$post['end_date']));

		foreach ($post as $key => $value) {
			if($key=='sms_status' || $key=='check_complete') continue;
			$update[$key] = $value;
		}
		 
		if(empty($post['post_id']))
		{	
			// Kiểm tra quyền Add
			if(!has_permission('webbuild.task.add')){
				$result['success'] 	= FALSE;
				$result['msg'] 	= 'Tài khoản không có quyền Thêm mới Task.';
				return parent::renderJson($result);
			}
			
			$update['created_on'] = time();
			$update['post_type'] = 'webbuild-task';
			$update['post_author'] = $this->admin_m->id;
			$post_id = $this->post_m->insert($update);
			$this->term_posts_m->set_term_posts($term_id, array($post_id), $this->term_type);
			$this->postmeta_m->add_meta($post_id,$meta_key,$post['sms_status']);
			
			$result['success'] 	= TRUE;
			$result['msg'] 		= "Thêm ".$update['post_title']." thành công.";
			
		}
		else
		{
			// Kiểm tra quyền Update
			if(!has_permission('webbuild.task.update')){
				$result['success'] 	= FALSE;
				$result['msg'] 	= 'Tài khoản không có quyền Chỉnh sửa Task.';
				return parent::renderJson($result);
			}
			$update['updated_on'] = time();
			$this->post_m->update($update['post_id'],$update);
			$this->postmeta_m->update_meta($update['post_id'],$meta_key,$post['sms_status']);
			$this->term_posts_m->set_term_posts($term_id, array($update['post_id']), $this->term_type);
			$result['success'] 	= TRUE;
			$result['msg'] 		= 'Cập nhật thành công';
			
		}
		
		$data = $this->term_posts_m->get_term_posts($term_id,'webbuild-task',array('orderby' => 'posts.end_date', 'order' => 'DESC'));
		foreach ($data as $key => $value) {
			update_term_meta($term_id,'end_service_time',$data[$key]->end_date);
			break;
		}
		
		return parent::renderJson($result);
	}
	
	public function get_all()
	{
		$id = $this->input->post('term_id');

		$data = $this->term_posts_m->get_term_posts($id,'webbuild-task');//->get_compiled_select()
		// foreach ($data as $value) {
		// 	$value->sms_status = $this->postmeta_m->get_meta_value($value->post_id,'sms_status');
		// }
		if(empty($data)){
			$result['success'] 	= FALSE;
			$result['msg'] 	= 'Không tìm thấy dữ liệu';
			return parent::renderJson($result);
		}
		$result['success'] 	= TRUE;
		$result['data'] 	= $data;
		return parent::renderJson($result);
	}
	
	public function delete()
	{
		$term_id = $this->input->post('term_id');
		$post_id = $this->input->post('post_id');
		if(!empty($post_id)){
			if($this->post_m->delete($post_id)){
				$this->term_posts_m->delete_term_posts($term_id,$post_id);
				$result['success'] 	= TRUE;
				$result['msg'] 	= 'Xóa thành công';
				return parent::renderJson($result);
			}
			$result['success'] 	= FALSE;
			$result['msg'] 	= 'Lỗi vui lòng kiểm tra lại';
			return parent::renderJson($result);
		}
		
		
	}
}