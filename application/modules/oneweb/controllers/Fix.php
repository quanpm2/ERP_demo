<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fix extends Admin_Controller {

	/**
	 * Sends an activation email.
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function send_activation_email($term_id = 0)
	{
		$this->load->model('oneweb/oneweb_report_m');
		$this->oneweb_report_m->init($term_id)->send_activation_email();
	}
}