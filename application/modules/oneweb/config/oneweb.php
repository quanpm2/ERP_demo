<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['packages'] = array(
	'service' =>array(

		'minimal'=> array(
			'name' => 'minimal',
			'label'=> 'Gói 1.000.000',
			'icon' => 'fa-paint-brush',
			'jobs' => array(),
			'insert_slot' => 7,
			'price' => 1000000,
			'appendix'=>''
		),

		'basic'=> array(
			'name' => 'basic',
			'label'=> 'Gói 3.000.000',
			'icon' => 'fa-paint-brush',
			'jobs' => array(),
			'insert_slot' => 7,
			'price' => 3000000,
			'appendix'=>''
		),

		'basic2'=> array(
			'name' => 'basic2',
			'label'=> 'Gói 5.000.000',
			'icon' => 'fa-paint-brush',
			'jobs' => array(),
			'insert_slot' => 7,
			'price' => 5000000,
			'appendix'=>''
		),

		'normal'=> array(
			'name' => 'normal',
			'label'=> 'Gói 2tr9 1 WEb (đã không hoạt động)',
			'icon' => 'fa-paint-brush',
			'jobs' => array(
				[
					'title' => 'Hoàn chỉnh Header Thay Logo khách hàng, hoàn chỉnh menu, Email và phone liên hệ nếu có',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Hoàn chỉnh Header Thay Logo khách hàng, hoàn chỉnh menu, Email và phone liên hệ nếu có'
				],
				[
					'title' => 'Hoàn chỉnh Footer - Thay thông tin về địa chỉ Email và phone liên hệ - Các nút liên hệ, livechat, social nếu có',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Hoàn chỉnh Footer - Thay thông tin về địa chỉ Email và phone liên hệ - Các nút liên hệ, livechat, social nếu có'
				],
				[
					'title' => 'Cập nhật bài viết - Ít nhất 05 bài viết Tin tức - Bài giới thiệu về Website/Doanh nghiệp',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Cập nhật bài viết - Ít nhất 05 bài viết Tin tức - Bài giới thiệu về Website/Doanh nghiệp'
				],
				[
					'title' => 'Cập nhật sản phẩm  - Ít nhất 05 Sản phẩm, Giá và hình ảnh',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Cập nhật sản phẩm  - Ít nhất 05 Sản phẩm, Giá và hình ảnh'
				],
				[
					'title' => 'Tích chọn chấp nhận cho Google index site',
					'day' 	=> '+1 day',
					'title_ascii' => 'Tích chọn chấp nhận cho Google index site'
				],
				[
					'title' => 'Trang chủ Hoàn thiện Trang chủ theo Mãu yêu cầu',
					'day' 	=> '+1 day',
					'title_ascii' => 'Trang chủ Hoàn thiện Trang chủ theo Mãu yêu cầu'
				],

				[
					'title' => 'Trang Giới thiệu Cập nhật bài viết theo Mẫu Thiết kế',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Trang Giới thiệu Cập nhật bài viết theo Mẫu Thiết kế'
				],
				[
					'title' => 'Trang Tin tức Show sản phẩm mới cập nhật theo Mẫu yêu cầu',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Trang Tin tức Show sản phẩm mới cập nhật theo Mẫu yêu cầu'
				],
				[
					'title' => 'Trang sản phẩm / Trang chi tiết sản phẩm  Show sản phẩm mới cập nhật theo Mẫu yêu cầu',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Trang sản phẩm / Trang chi tiết sản phẩm  Show sản phẩm mới cập nhật theo Mẫu yêu cầu'
				],
				[
					'title' => 'Trang Liên hệ Kiểm tra Email phản hồi khi nhập form liên hệ',
					'day' 	=> '+1 day',
					'title_ascii' => 'Trang Liên hệ Kiểm tra Email phản hồi khi nhập form liên hệ'
				],

				[
					'title' => 'Trang Giỏ hàng / Thanh toán Kiểm tra tình năng mua hàng và ghi nhận đơn hàng',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Trang Giỏ hàng / Thanh toán Kiểm tra tình năng mua hàng và ghi nhận đơn hàng'
				],
				[
					'title' => 'Trang sản phẩm  Show sản phẩm mới cập nhật theo Mẫu yêu cầu',
					'day' 	=> '+1 day',
					'title_ascii' => 'Trang sản phẩm  Show sản phẩm mới cập nhật theo Mẫu yêu cầu'
				],
				[
					'title' => 'Cấu hình Yoast SEO',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Cấu hình Yoast SEO'
				],
				[
					'title' => 'Cấu hình Sercurity cho website',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Cấu hình Sercurity cho website'
				],
				[
					'title' => 'Cấu hình Cache Web',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Cấu hình Cache Web'
				],
				[
					'title' => 'Cập nhật SMTP Email hệ thống - Kiểm tra chính xác mail đã được gửi đi thành công',
					'day' 	=> '+1 hour',
					'title_ascii' => 'Cập nhật SMTP Email hệ thống - Kiểm tra chính xác mail đã được gửi đi thành công'
				],
				[
					'title' => 'Kiểm tra PageSpeed Chấp nhận điểm số 50/80',
					'day' 	=> '+1 day',
					'title_ascii' => 'Kiểm tra PageSpeed Chấp nhận điểm số 50/80'
				],
			),
			'insert_slot' => 7,
			'price' => 2900000,
			'appendix'=>''
		),
	),
	'default' =>'basic'
);

$config['non-working-days'] = array(
	array('date' => '2019/04/30', 'title' => 'Ngày giải phóng miền Nam, thống nhất đất nước 30/4'),
	array('date' => '2019/05/01', 'title' => 'Ngày Quốc tế Lao động 1/5'),
);

$config['sms_status'] = array(
	0 => 'Không gửi SMS',
	1 => 'Đã đặt lịch gửi SMS',
	2 => 'Đã gửi SMS'
);

$config['webbuild-task-status'] = array(
	'publish' => 'Mới tạo',
	'process' => 'Đang thực hiện',
	'complete' => 'Hoàn thành'
);