<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['discount']['packages'] = array(
	
	'combo' => array(
		'name' => 'combo',
		'label' => 'Chương trình mua nhiều nhận nhiều ưu đãi',
		'field_ref' => 'service_price',
		'sale' => array(
			/* Mua 2 năm tặng thêm 1 năm */
			['buy' => 2, 'free' => 1],
			/* Mua 4 năm tặng thêm 2 năm */
			['buy' => 4, 'free' => 2],
			/* Mua 6 năm tặng thêm 3 năm */
			['buy' => 6, 'free' => 3],
			/* Mua 8 năm tặng thêm 4 năm */
			['buy' => 8, 'free' => 4],
			/* Mua 10 năm tặng thêm 5 năm */
			['buy' => 10, 'free' => 5],
		)
	),
	
	'custom' => array(
		'name' => 'custom',
		'label' => 'Chương trình giảm giá linh hoạt (Giảm tối đa 30% giá trị)',
		'field_ref' => 'service_price',
		'sale' => array('min' => 0, 'max' => 30)
	),
); 