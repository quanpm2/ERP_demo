<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Behaviour_m.php');

class Oneweb_behaviour_m extends Behaviour_m {

	function __construct()
	{
		parent::__construct();
		$this->load->config('oneweb/oneweb');
		$this->load->helper('date');
	}


	/**
	 * Gets the tasks default.
	 *
	 * @throws     Exception  (description)
	 */
	public function get_default_tasks($start_time = FALSE, $package_name = '')
	{	
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$term_id 		= $this->contract->term_id;
		$package_name 	= $package_name ?: (get_term_meta_value($term_id, 'service_package') ?: $this->config->item('default', 'packages'));

		$this->load->model('oneweb/oneweb_config_m');
		$this->load->helper('text');

		$jobs = $this->oneweb_config_m->get_service_value($package_name, 'jobs');
		if( empty($jobs)) return [];

		if($advanced_functions = get_term_meta_value($term_id, 'advanced_functions'))
		{
			$inserted_jobs = array_map(function($x){
				$day = $x['days'] ?? 1;
				return array('title' => $x['name'], 'day' => "+{$day} day", 'title_ascii' => convert_accented_characters($x['name']));
			}, (is_serialized($advanced_functions) ? unserialize($advanced_functions) : []));

			$insert_slot = (int) $this->oneweb_config_m->get_service_value($package_name, 'insert_slot');
			array_splice( $jobs, $insert_slot, 0, $inserted_jobs );
		}

		if( ! $start_time) $start_time = start_of_day('+1 day');
		
		defined('SATURDAY')	OR define('SATURDAY', 6);
        defined('SUNDAY')  	OR define('SUNDAY', 0);

        $non_working_days = array();
        if($_non_working_days = $this->config->item('non-working-days'))
        {
	        $non_working_days = array_unique(array_column(array_map(function($d){ $d['timestamp'] = start_of_day($d['date']); return $d; }, $_non_working_days),'timestamp'));
        }

		foreach($jobs as &$job)
		{
			$start_time = start_of_day($start_time);

			// If start_time is weekends or non-working-days, then set to monday of next week and increments step to next
			if(date('w', $start_time) == SATURDAY) $start_time = strtotime('+2 days', $start_time);
			else if(date('w', $start_time) == SUNDAY) $start_time = strtotime('+1 days', $start_time);
			else if(in_array($start_time, $non_working_days)) $start_time = strtotime('+1 days', $start_time);

			$end_time 	= strtotime($job['day'], $start_time);
			$weekend_days = 0;
			while((count_weekend_days($start_time, $end_time) - $weekend_days) > 0)
			{
				$left = count_weekend_days($start_time, $end_time) - $weekend_days;
				$weekend_days = count_weekend_days($start_time, $end_time);
				$end_time = strtotime("+{$left} days", $end_time);
			}

			/* Assign caculated days to job */
			$job['start_date'] 	= $start_time;	
			$job['end_date'] 	= strtotime('-1 second', $end_time);

			$start_time 		= $end_time;
		}

		return $jobs;
	}

	/**
	 * Calculates the contract value.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     boolean    The contract value.
	 */
	public function calc_contract_value()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$term_id = $this->contract->term_id;

		$service_package_price 	= (int) get_term_meta_value($term_id, 'service_package_price');
		$years 					= ((int) get_term_meta_value($term_id, 'years')) ?: 1;
		$discount_amount		= (int) get_term_meta_value($term_id ,'discount_amount');

		/* Tính năng nâng cao */
		$advanced_functions_price 	= 0;
		$advanced_functions 		= unserialize(get_term_meta_value($term_id, 'advanced_functions'));
		$advanced_functions AND $advanced_functions_price = array_sum(array_column($advanced_functions, 'value'));

		$result = ($service_package_price*$years) + $advanced_functions_price - $discount_amount;

		return max($result, 0);
	}

	/**
	 * Creates invoices.
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function create_invoices()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$term_id 		= $this->contract->term_id;
		$contract_value = get_term_meta_value($term_id, 'contract_value') ?: 0;
		if(empty($contract_value)) throw new Exception("Giá trị hợp đồng ({$contract_value}) không hợp lệ.");

		$number_of_payments = 1;

		$contract_begin = get_term_meta_value($term_id, 'contract_begin');
		$contract_end 	= get_term_meta_value($term_id, 'contract_end');
		$num_dates 		= diffInDates($contract_begin,$contract_end);
		$num_days4inv 	= ceil(div($num_dates,$number_of_payments));

		$amount_per_payments = div($contract_value, $number_of_payments);
		
		$start_date 	= $contract_begin;
		$invoice_items 	= array();

		for($i = 0 ; $i < $number_of_payments; $i++)
		{	
			if($num_days4inv == 0) break;

			$end_date = $this->mdate->endOfDay(strtotime("+{$num_days4inv} day -1 second", $start_date));

			$inv_id = $this->invoice_m
			->insert(array(
				'post_title' => "Thu tiền đợt ". ($i + 1),
				'post_content' => "",
				'start_date' => $start_date,
				'end_date' => $end_date,
				'post_type' => $this->invoice_m->post_type
				));

			if(empty($inv_id)) continue;

			$this->term_posts_m->set_post_terms($inv_id, $term_id, $this->contract->term_type);

			$quantity = $num_days4inv;

			// Đợt 1: 50% giá trị HĐ
			if($number_of_payments == 3) {
				
				if($i == 0) {
					$amount_per_payments_3_count = (50 * $contract_value)/100 ;
				}

				// Đợt 2: 30% giá trị HĐ 
				elseif($i == 1) {
					$amount_per_payments_3_count = (30 * $contract_value)/100 ;
				}

				// Đợt 3: 20% giá trị HĐ
				elseif($i == 2) {
					$amount_per_payments_3_count = (20 * $contract_value)/100 ;
				}
			}

			$this->invoice_item_m->insert(array(
					'invi_title' => 'Giá dịch vụ',
					'inv_id' => $inv_id,
					'invi_description' => '',
					'invi_status' => 'publish',
					//'price' => $amount_per_payments,
					'price' => ($number_of_payments == 3) ? $amount_per_payments_3_count : $amount_per_payments,
					'quantity' => 1,
					'invi_rate' => 100,
					'total' => $this->invoice_item_m->calc_total_price(($number_of_payments == 3) ? $amount_per_payments_3_count : $amount_per_payments, 1, 100)
				));

			$start_date = strtotime('+1 second', $end_date);

			$day_end = $num_dates - $num_days4inv;
			if($day_end < $num_days4inv)
			{
				$num_days4inv = $day_end;
			}
		}
		return TRUE;
	}


	/**
	 * Preview data for printable contract version
	 *
	 * @return     String  HTML content
	 */
	public function prepare_preview()
	{
		if( ! $this->contract) return FALSE;

		parent::prepare_preview();

		$data 		= $this->data;
		$term_id 	= $this->contract->term_id;

		$service_package = get_term_meta_value($term_id,'service_package') ?: 'normal';
		$printable_title = $this->config->item($this->contract->term_type,'printable_title');

		$printable_title = 'HỢP ĐỒNG MUA BÁN PHẦN MỀM LANDINGPAGE';
		
		/* Thông tin người đại diện duyệt sản phẩm */
		$data['qc_confirm_name'] 	= (get_term_meta_value($term_id, 'qc_confirm_gender') == 1 ? 'Ông ' : 'Bà ').str_repeat('&nbsp', 1);
		$data['qc_confirm_name']	.= get_term_meta_value($term_id,'qc_confirm_name');
		$data['qc_confirm_email'] 	= get_term_meta_value($term_id, 'qc_confirm_email');
		$data['qc_confirm_phone'] 	= get_term_meta_value($term_id, 'qc_confirm_phone');

		$data['service_package']		= $service_package;
		$data['printable_title'] 		= $printable_title[$service_package] ?? $printable_title;
		$data['webdesign_price']		= (int) get_term_meta_value($term_id, 'webdesign_price');

		$data['service_package_price'] 	= (int) get_term_meta_value($term_id, 'service_package_price');
		$data['years'] 					= (int) get_term_meta_value($term_id, 'years');
		$data['discount_amount'] 		= (int) get_term_meta_value($term_id, 'discount_amount');
		$data['discount_plan'] 			= get_term_meta_value($term_id, 'discount_plan');

		$data['payment_bank_account'] 	= get_term_meta_value($term_id, 'payment_bank_account') ?: 'company';
		$data['bank_info'] 				= $this->config->item('company', 'bank_infos');

		$representative_zone 	= get_term_meta_value($term_id, 'representative_zone') ?: 'hcm';
		if('person' == $data['payment_bank_account'])
		{
			$bank_info 			= $this->config->item('person', 'bank_infos');
			$data['bank_info'] 	= array_map(function($bankInfo){
				if(isset($bankInfo['representative_zone'])) unset($bankInfo['representative_zone']);
				return $bankInfo;
			}, array_filter($bank_info, function($x) use ($representative_zone){
				return $x['representative_zone'] == $representative_zone;
			}));	
		}

		/* Tính năng nâng cao */
		$advanced_functions = unserialize(get_term_meta_value($term_id, 'advanced_functions'));
		$data['advanced_functions'] 		= $advanced_functions;

		$advanced_functions_price = !empty($advanced_functions) ? array_sum(array_column($advanced_functions, 'value')) : 0;
		$data['advanced_functions_price'] 	= $advanced_functions_price;

		/* Phần trăm giảm giá dành cho dữ liệu cũ */
		$data['discount_percent'] 	= (int) get_term_meta_value($term_id ,'discount_percent');
		$data['discount_amount'] 	= (int) get_term_meta_value($term_id ,'discount_amount');

		$data['working_days'] = ((int) get_term_meta_value($term_id, 'working_days')) ?: $data['contract_days'];

		/* Chi tiết các đợt thanh toán */
		$invoices = 				$this->term_posts_m->get_term_posts($term_id, $this->invoice_m->post_type, ['orderby'=>'posts.end_date']);
		$data['invoices']		 	= $invoices;
		$data['number_of_payments'] = count($invoices);

		$verified_on = get_term_meta_value($this->contract->term_id, 'verified_on') ?: time();

		if($verified_on < start_of_day(strtotime('2021/06/01')))
		{
			$data['package_partial']	= $this->load->view("oneweb/contract/{$service_package}", $data, TRUE);
			$data['content'] 			= $this->load->view('oneweb/contract/preview', $data, TRUE);
			return $data;
		}

		$data['content'] 	= $this->load->view('oneweb/contract/preview_ladipage', $data, TRUE);

		return $data;
	}

	/**
	 * Sends an activation email.
	 *
	 * @param      string     $type_to  The type to
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     ( description_of_the_return_value )
	 */
	public function send_activation_email($type_to = 'admin')
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$this->load->model('oneweb/oneweb_report_m');
		return $this->oneweb_report_m->init($this->contract->term_id)->send_activation_email($type_to);
	}

    /**
     * Compute Actual Budget of Paid Payments
     * @return double Actual Budget
     */
    public function calc_payment_service_fee()
    {
        if( ! $this->contract) throw new \Exception('Property Contract undefined.');

        $stop_status = ['ending', 'liquidation'];
        if(!in_array($this->contract->term_status, $stop_status)) return 0;

        return parent::calc_payment_service_fee();
    }
}
/* End of file Oneweb_behaviour_m.php */
/* Location: ./application/modules/oneweb/models/Oneweb_behaviour_m.php */