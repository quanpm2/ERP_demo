<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Contract_wizard_m.php');

class Oneweb_wizard_m extends Contract_wizard_m {

	function __construct()
	{
		parent::__construct();

		$models = array('oneweb/oneweb_m');
		$this->load->model($models);
	}


	/**
	 * Add Hook when in model change service detail
	 *
	 * @param      Array  $post   The post
	 */
	public function wizard_act_service($post)
	{
		parent::wizard_act_service($post);

		$this->hook->add_filter('ajax_act_service',	function($post){

			$metadata 	= $post['meta'] ?? [];
			$term_id 	= $post['edit']['term_id'] ?? 0;

            // Hợp đồng không cần cấu hình VAT
            update_term_meta($term_id, 'vat', 0);

			// Cập nhật thông số chi tiết dịch vụ
			$this->load->config('oneweb/oneweb');
			$service_package_config = $this->config->item('service', 'packages');
			$enumsServicePackages 	= array_column($service_package_config, 'name');

			$this->load->library('form_validation');

			$this->form_validation->set_data($metadata);
			$this->form_validation->set_rules('service_package', 'Gói dịch vụ', 'required|in_list['.implode(',', $enumsServicePackages).']');
			$this->form_validation->set_rules('years', 'Số năm', 'required|integer|greater_than_equal_to[1]|less_than_equal_to[10]');
			$this->form_validation->set_rules('service_package_price', 'Chi phí', 'required|integer');
			if(FALSE == $this->form_validation->run()) throw new Exception( json_encode($this->form_validation->error_array()));

			if( ! empty($service_package_config[$metadata['service_package']]['items']))
			{
				update_term_meta($term_id, 'service_package_items', serialize($service_package_config[$metadata['service_package']]['items']));
			}

			isset($metadata['service_package']) 		AND update_term_meta($term_id, 'service_package', $metadata['service_package']);
			isset($metadata['service_package_price'])	AND update_term_meta($term_id, 'service_package_price', $metadata['service_package_price']);
			isset($metadata['years']) 					AND update_term_meta($term_id, 'years', $metadata['years']);
			isset($metadata['discount_amount'])			AND update_term_meta($term_id, 'discount_amount', $metadata['discount_amount']);


			if(isset($metadata['advanced_functions']))
			{
				$advanced_functions = array_filter($metadata['advanced_functions'], function($x){
					return !empty($x['name']);
				});

				update_term_meta($term_id, 'advanced_functions', serialize($advanced_functions));	
			}

			try
			{
				$contract = $this->oneweb_m->set_contract($term_id);
				update_term_meta($term_id, 'contract_value', $this->oneweb_m->calc_contract_value());
			}
			catch (Exception $e)
			{
				return $post;
			}
			
			return $post;
			
		}, 9);
	}
}
/* End of file Oneweb_wizard_m.php */
/* Location: ./application/modules/oneweb/models/Oneweb_wizard_m.php */