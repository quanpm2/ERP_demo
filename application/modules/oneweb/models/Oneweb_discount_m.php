<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Discount_m.php');

class Oneweb_discount_m extends Discount_m {

	function __construct() 
	{
		parent::__construct();
		$this->load->config('oneweb/discount');
	}

	/**
	 * Gets the packages.
	 *
	 * @return     array  The packages.
	 */
	public function get_packages()
	{
		$allowed_packages = $this->config->item('packages', 'discount');
		if( empty($allowed_packages)) return [];

		$_packages = array_keys($allowed_packages);
		foreach ($_packages as $_package)
		{
			if($_package == 'combo') continue;
			try
			{
				if($this->check_package($_package)) continue;
				unset($allowed_packages[$_package]);
			}
			catch (Exception $e) { unset($allowed_packages[$_package]); }
		}

		return $allowed_packages;
	}

	public function validate($discount_amount = 0, $amount = 0, $package = 'normal')
	{
		$packages = $this->get_packages();
		if( empty($packages[$package])) throw new Exception("Chương trình giảm giá không khả dụng.");

		if(empty($discount_amount)) return TRUE;

		switch ($package)
		{
			case 'normal':
				$con_value_min 			= 10000000; // 10.000.000 đ
				if($amount <= $con_value_min) throw new Exception("Giá trị thiết kế chưa vượt hạn mức cho phép áp dụng chương trình giảm giá");

				$valid_discount_amount = ($amount - $con_value_min)*div($packages['normal']['sale']['max'],100);
				if($discount_amount > $valid_discount_amount)
				{
					$valid_discount_amount_f = currency_numberformat($valid_discount_amount);
					throw new Exception("Giá trị giảm giá vượt quá mức tối đa cho phép {$valid_discount_amount_f} hạn mức cho phép áp dụng chương trình giảm giá");
				}

				return TRUE;

				break;

			case 'custom':
				$con_value_min = 2900000; // 3.000.000 đ
				if($amount < $con_value_min) throw new Exception("Giá trị thiết kế chưa vượt hạn mức cho phép áp dụng chương trình giảm giá [1]");

				$valid_discount_amount = $amount*div($packages['custom']['sale']['max'],100);
				if($discount_amount > $valid_discount_amount)
				{
					$valid_discount_amount_f = currency_numberformat($valid_discount_amount);
					throw new Exception("Giá trị giảm giá vượt quá mức tối đa cho phép {$valid_discount_amount_f} hạn mức cho phép áp dụng chương trình giảm giá");
				}
				return TRUE;

				break;

			case 'cross-sell':

				try
				{
					if( ! $this->check_package($package)) throw new Exception("Chương trình giảm giá không khả dụng.");
				}
				catch (Exception $e) { throw new Exception($e->getMessage()); }

				$discount_amount_max = $amount * div($packages[$package]['sale']['max'], 100);
				if($discount_amount > $discount_amount_max)
				{
					$discount_amount_max_f = currency_numberformat($discount_amount_max);
					throw new Exception("Giá trị giảm giá vượt quá mức tối đa cho phép {$discount_amount_max_f} hạn mức cho phép áp dụng chương trình giảm giá");
				}

				return TRUE;
				break;
		}

		return parent::validate($discount_amount, $amount, $package);
	}

	public function check_package($package = 'normal')
	{
		if( ! $this->contract) return FALSE;

		$allowed_packages = $this->config->item('packages', 'discount');
		if( ! in_array($package, array_keys($allowed_packages))) return FALSE;

		switch ($package)
		{
			case 'normal': return TRUE; break;
			case 'custom': return TRUE; break;

			case 'cross-sell':
				$this->load->model('term_users_m');
				$customers 	= $this->term_users_m->get_the_users($this->contract->term_id, ['customer_person','customer_company']);
				if( ! $customers) return FALSE;
				$customer 	= $customers ? reset($customers) : NULL;

				$this->config->load('contract/contract');
				$taxonomies = $this->config->item('taxonomy');
				unset($taxonomies[$this->contract->term_type]);
				$taxonomies = array_keys($taxonomies);

				$args  = ['where_in' => array('term.term_status' => array('pending', 'publish', 'ending', 'liquidation'))];
				$terms = $this->term_users_m->get_user_terms($customer->user_id, $taxonomies, $args);
				if( ! $terms)
				{
					throw new Exception("Khách hàng không thuộc phạm vi có thể áp dụng chương trình giảm giá cho Cross-sell.");
					return FALSE;
				}

				return TRUE;
				break;
			
			default: return FALSE; break;
		}

		return FALSE;
	}
}
/* End of file Oneweb_discount_m.php */
/* Location: ./application/modules/contract/models/Oneweb_discount_m.php */