<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Webdoctor_report_m.php');

class Oneweb_report_m extends Webdoctor_report_m {

	protected $from 	= [ 'email' => 'hotro@1web.vn', 'name' => '1web.vn' ];

    function __construct()
    {
        $this->recipients['cc'][]   = 'huyenptt@webdoctor.vn';

        $this->autoload['models'][] = 'oneweb/oneweb_m';
        $this->autoload['models'][] = 'webgeneral/webgeneral_kpi_m';
        $this->autoload['models'][] = 'oneweb/oneweb_task_m';
        $this->autoload['models'][] = 'term_posts_m';

        parent::__construct();

        // 30 ngày trước ngày kết thúc hợp đồng
        defined('NUMDAYS_BEFORE_FINISH') OR define('NUMDAYS_BEFORE_FINISH',30);
		// 7 ngày nhắc một lần
		defined('NUMDAYS_NOTICEFINISH') OR define('NUMDAYS_NOTICEFINISH',7);
    }

    /**
     * Sends an activation email.
     *
     * @param      string  $type_to  The type to
     */
    public function send_activation_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;

        $time_start 		= get_term_meta_value($this->term_id, 'contract_begin');
        $time_end   		= get_term_meta_value($this->term_id, 'contract_end');
        $start_service_time = get_term_meta_value($this->term_id, 'start_service_time');
        $end_service_time   = get_term_meta_value($this->term_id, 'end_service_time');

        if(empty($start_service_time))
        {
            $start_service_time = time();
            update_term_meta($this->term_id, 'start_service_time', $start_service_time);
        }

        if(empty($end_service_time))
        {
            $time_end = ($start_service_time - $time_start) + $time_end;
            update_term_meta($this->term_id, 'end_service_time',$time_end);
        }

        $_args = ['where'=>['start_date >=' => start_of_day($time_start)],'orderby'=>'posts.start_date'];
        $tasks = $this->term_posts_m->get_term_posts($this->term_id, $this->oneweb_task_m->post_type, $_args);

        $default = array(
            'term'      => $this->term,
            'term_id'   => $this->term_id,
            'title'     => 'Thông tin kích hoạt dịch vụ',
            'subject'   => "[1WEB.VN] Thông báo kích hoạt dịch vụ {$this->term->term_name}",
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'sale'      => $this->sale,
            'tasks'     => $tasks,
            'content_tpl' => 'oneweb/report/activation_email',
        );

        $this->data = wp_parse_args($this->data, $default);

        if( empty($this->data['content_tpl'])) return FALSE;

        $this->data['content'] = $this->render_content($this->data['content_tpl']);

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        $result = $this->send_email();
        $result AND $this->log_m->insert(array(
            'log_title'     => 'Gửi mail khi kích hoạt 1WEB bằng email',
            'log_status'    => 1,
            'term_id'       => $this->term_id,
            'user_id'       => $this->admin_m->id,
            'log_content'   => 'Đã gửi mail thành công',
            'log_type'      => 'oneweb-report-email',
            'log_time_create' => date('Y-m-d H:i:s'),
        ));

        return $result;
    }

    /**
	 * Send SMS
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     array    ( description_of_the_return_value )
	 */
	public function send_sms($term_id = 0)
	{
		$result = array('status' => FALSE, 'msg' => '');

		$this->load->library('sms');

		$time 			= time();
		$post_send_id 	= array();
		$phone 			= get_term_meta_value($term_id, 'phone_report');

		if( ! $phone)
		{
			$result['msg'] = 'Số điện thoại chưa được khai báo.';
			return $result;
		}

		$posts = $this->post_m->group_by('posts.post_id')->get_posts(array(
			'select' 		=>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
			'tax_query'		=> array( 'taxonomy' 	=> $this->oneweb_m->term_type, 'terms' 	=> $term_id),
			'where' 		=> array( 'end_date <=' => $time ),
			'post_status' 	=> 'complete',
			'meta_key' 		=> 'sms_status',
			'meta_value' 	=> 1,
			'post_type' 	=> $this->oneweb_task_m->post_type
		));

		if( ! $posts)
		{
			$result['msg'] = 'Không có đầu việc nào được thực hiện';
			return $result;
		}

		$result['status'] = true;

		$title = array();
		foreach($posts as $post)
		{
			$meta_title = get_post_meta_value($post_id, 'title_ascii') ?: $post->post_title;
			if(empty($meta_title)) continue;

			$title[] = convert_utf82ascci(mb_strtolower($meta_title));
			$post_send_id[] = $post->post_id;
		}

		$tpl = 'Kinh gui quy khach! 1WEB da hoan thanh viec ';
		$tpl.= implode(', ', $title);

		$f_posts = $this->post_m->group_by('posts.post_id')->where_in('post_status',array('process'))->get_posts(array(
			'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
			'tax_query'=> array( 'taxonomy' => $this->oneweb_m->term_type, 'terms' => $term_id),
			'where' => array( 'end_date <' => $time ),
			'numberposts' => 1,
			'meta_key' => 'sms_status',
			'meta_value' => 1,
			'orderby' => 'start_date',
			'order' => 'asc',
			'post_type' => $this->oneweb_task_m->post_type
		));

		if( ! $f_posts)
		{
			$f_posts = $this->post_m->group_by('posts.post_id')->where_in('post_status',array('complete','process'))->get_posts(array(
				'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
				'tax_query'=> array(
					'taxonomy' => $this->oneweb_m->term_type,
					'terms' => $term_id,
					),
				'where' => array(
					'end_date >' => $time
					),
				'numberposts' => 1,
				'meta_key' => 'sms_status',
				'meta_value' => 1,
				'orderby' => 'start_date',
				'order' => 'asc',
				'post_type' => $this->oneweb_task_m->post_type
			));
		}

		if($f_posts)
		{
			$tpl.= ' va dang tien hanh viec ';
			$title = array();
			foreach($f_posts as $fpost)
			{
				$meta_title = $this->postmeta_m->get_meta_value($fpost->post_id,'title_ascii');

				$title[] = (!empty($meta_title) ? $meta_title : convert_utf82ascci(mb_strtolower($fpost->post_title)));
			}

			$tpl.= implode(', ', $title);
		}
		$tpl.= '.';

		// $sms_results = $this->sms->fake_send($phone, $tpl);
		$sms_results = $this->sms->send($phone,$tpl);

		$is_passed = FALSE;

		$result['msg'] = array();
		foreach ($sms_results as $sms_result) 
		{
			$log_id = $this->log_m->insert(array(
				'log_title'=>$tpl,
				'log_status'=> $sms_result['response']->SendSMSResult,
				'term_id'=>$term_id,
				'user_id'=> 0,
				'log_type'=> 'oneweb-report-sms',
				'log_time_create'=> date('Y-m-d H:i:s'),
				));
			$result['msg'][] = array('title'=>$tpl, 'status'=>$sms_result['response']->SendSMSResult, 'recipient'=>$sms_result['recipient']);
			$sms = array();
			$sms['sms_recipient'] = $sms_result['recipient'];
			$sms['sms_service_id'] = $sms_result['config']['service_id'];
			$this->log_m->meta->update_meta($log_id, 'sms_result', json_encode($sms));

			$result_code = $sms_result['response']->SendSMSResult;
			if($result_code == 0)
			{
				$is_passed = TRUE;
			}
		}

		if( ! $is_passed) return $result;

		foreach($post_send_id as $post_id) update_post_meta($post_id, 'sms_status', 2);

		return $result;
	}

	/**
	 * Sends a tasks complete mail 2 customer.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     array    ( description_of_the_return_value )
	 */
	public function send_tasks_complete_mail2customer($term_id = 0)
	{
		$mails = get_term_meta_value($term_id,'mail_report');
		if(!$mails) return FALSE;

		$this->load->model('webgeneral/webgeneral_kpi_m');
		$this->load->model('webgeneral/webgeneral_config_m');
		$this->load->model('webgeneral/webgeneral_m');

		$result = ['status'=>FALSE,'msg'=>''];
		$time               = time();

		//  Soạn nội dung để gửi mail : tất cả các công việc đã xong
		$tasks = $this->post_m->group_by('posts.post_id')->where_in('post_status',array('process','complete'))->get_posts(array(
			'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent, posts.post_status',
			'tax_query'=> array(
				'taxonomy' 	 => $this->oneweb_m->term_type,
				'terms'    	 => $term_id,
				),
			'where' => array(
				'end_date <' => $time
			),
			'orderby' 		 => 'start_date',
			'order' 		 => 'asc',
			'post_type' 	 => $this->oneweb_task_m->post_type
		));

		if(empty($tasks)) return FALSE;

		$has_unsent = FALSE;
		foreach ($tasks as $task) 
		{
			if($task->post_status == 'process') continue;

			$task_email_status = get_post_meta_value($task->post_id,'email_status');
			if($task_email_status == 0)
			{
				$has_unsent = TRUE;
				break;
			}
		}

		

		if( ! $has_unsent) return FALSE;
		$data['term_id']        = $term_id ;
		$data['tasks'] = $tasks ;

		$term 					= $this->term_m->get($term_id);
		$data['term'] = $term;
		$mail_id = 'ID:'.time();
		$title 					= "[1WEB.VN] Báo cáo công việc đã hoàn thành của website {$term->term_name} {$mail_id}";

		$data['title']          = $title;	
		$data['email_source']   = $title;

		$data['time_send'] 		= time();

		$content 				= $this->load->view('report/complete_tasklist_daily_tpl', $data, TRUE);

		$mail_cc = array('ketoan@webdoctor.vn');
		$mail_bcc = array('support@webdoctor.vn');

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');
		if($staff_mail)
			$mail_cc[] = $staff_mail;

		// Tên nhân viên phụ trách
		$kpis = $this->webgeneral_kpi_m->get_many_by(['term_id' => $term_id,'kpi_type'=> 'tech']);
		if($kpis)
		{
			foreach($kpis as $kpi)
			{
				$mail_cc[] = $this->admin_m->get_field_by_id($kpi->user_id,"user_email");
			}
		}
		$data['kpis'] = $kpis;

		$this->load->library('email');
		
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to($mails);
		$this->email->cc($mail_cc);
		$this->email->bcc($mail_bcc);

		$this->email->subject($title);
		$this->email->message($content);
		$result['msg'] = $this->email->send() ? 'Đã gửi mail thành công' : 'Gửi mail thất bại';
		
 		foreach($tasks as $task) 
 		{
 			$status_email = get_post_meta_value($task->post_id,'email_status');
 			if($status_email == 1) continue;
 			
 			update_post_meta($task->post_id, 'email_status', 1);
		}

		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Send mail complete task to customer',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> 0,
			'log_content'	=> $mails,
			'log_type'		=> 'oneweb-report-email',
			'log_time_create'=> date('Y-m-d H:i:s'),
		));
		
		return $result;
	}

	/**
	 * Sends an activation mail 2 customer.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function send_activation_mail2customer($term_id = 0)
	{
		$term = $this->term_m->get($term_id);
		if(empty($term)) return FALSE;

		$data = array('term'=>$term);

		$time_start = get_term_meta_value($term_id,'contract_begin');
		$time_end = get_term_meta_value($term_id,'contract_end');

		$start_service_time = get_term_meta_value($term_id,'start_service_time');
		$end_service_time = get_term_meta_value($term_id,'end_service_time');

		if(empty($start_service_time))
		{
			$start_service_time = time();
			update_term_meta($term_id, 'start_service_time', $start_service_time);
		}

		if(empty($end_service_time))
		{
			$time_end = ($start_service_time - $time_start) + $time_end;
			update_term_meta($term_id, 'end_service_time',$time_end);
		}

		$this->load->library('email');

		$title = 'Thông báo kích hoạt dịch vụ 1WEB '.ucfirst($term->term_name);
		$data['title'] = 'Thông tin kích hoạt dịch vụ';
		$data['email_source'] = $title;
		$data['term_id'] = $term_id;

		$data['tasks'] = $this->oneweb_task_m
		->select('posts.post_id, post_author, post_title, post_name, post_type, start_date, end_date')
		->join('term_posts', 'term_posts.post_id = posts.post_id')
		->where('start_date >=', $time_start)
		->where('end_date <=', strtotime('+10 month'))
		->where('term_id', $term_id)
		->order_by('posts.start_date')
		->group_by('posts.post_id')
		->get_all();

		$this->load->model('webgeneral/webgeneral_kpi_m');
		$kpis = $this->webgeneral_kpi_m->get_many_by(array(
                  'term_id'=>$term_id,
                  'kpi_type'=> 'tech',
                  ));
		$data['kpis'] = $kpis;
		$content_view = 'oneweb/admin/tpl_activation_email';
		// $data['content'] = $this->load->view($content_view,$data,TRUE);

		$content = $this->load->view($content_view,$data,TRUE);
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');

		$mail_report = get_term_meta_value($term_id,'mail_report');
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');


		$mail_cc = array();
		$mail_cc[] = 'support@webdoctor.vn';
		if($staff_mail)
			$mail_cc[] = $staff_mail;
		if($kpis)
		{
			foreach($kpis as $kpi)
			{
				$mail_cc[] = $this->admin_m->get_field_by_id($kpi->user_id,"user_email");
			}
		}

		$this->email->to($mail_report);
		$this->email->cc($mail_cc);
		$this->email->subject($title);
		$this->email->message($content);

		$send_status = $this->email->send(TRUE);
		return $send_status;
	}

	/**
	 * Sends a tasklist mail 2 customer.
	 *
	 * @param      integer  $term_id      The term identifier
	 * @param      boolean  $add_mail_cc  The add mail cc
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function send_tasklist_mail2customer($term_id = 0, $add_mail_cc = false)
	{	
		$term = $this->term_m->get($term_id);
		if(empty($term)) return FALSE;

		$data = array('term'=>$term);
		$this->load->model('webgeneral/webgeneral_m');

		$time_start = get_term_meta_value($term_id,'contract_begin');
		$start_date = $this->mdate->startOfDay($time_start);

		$time_end = get_term_meta_value($term_id,'contract_end');
		$end_date = $this->mdate->endOfDay($time_end);

		$args = array('order'=>'posts.start_date');
		$data['tasks'] = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('task'),$args);
		if(empty($data['tasks'])) return FALSE;


		$this->load->library('email');

		$title = '[1WEB.VN] Danh sách công việc dự kiến thực hiện của website '.$term->term_name;
		$layout = 'email/blue_tpl';
		$content_view = 'webgeneral/report/tpl_tasklist';
		$data['title'] = 'Danh sách công việc dự kiến thực hiện';
		$data['email_source'] = $title;
		$data['content'] = $this->load->view($content_view,$data,TRUE);
		$content = $this->load->view($layout,$data,TRUE);
		
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');

		// uncomment real customer
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');

		$mail_report = get_term_meta_value($term_id,'mail_report');
		$this->email->to($mail_report);

		$mail_cc = array('support@webdoctor.vn','quanly@webdoctor.vn');
		if($staff_mail) $mail_cc[] = $staff_mail;
		if($add_mail_cc) $mail_cc[] = $add_mail_cc;
		$this->email->cc($mail_cc);

		$this->email->subject($title);
		$this->email->message($content);

		$send_status = $this->email->send();

		return $send_status;
	}

	/**
	 * Sends a finish mail 2 admin.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function send_finish_mail2admin($term_id = 0)
    {
        $term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

        $contract_code = get_term_meta_value($term_id, 'contract_code');

        $title = '[1WEB.VN] Hợp đồng '.$contract_code.' đã được kết thúc.';
        $content = 'Dear nhóm Webdoctor <br>';

        $end_contract_time = get_term_meta_value($term_id,'end_contract_time');
        $content.= 'Hợp đồng '.$contract_code.' vừa được kết thúc vào lúc <b>'.my_date($end_contract_time,'d-m-Y H:i:s').'</b><br>';

        $this->config->load('table_mail');
        $this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Thông tin hợp đồng');
        $this->table
        ->add_row('Mã hợp đồng:', $contract_code)
        ->add_row('Tên dịch vụ:', 'Dịch vụ 1WEB');

        $vat = get_term_meta_value($term->term_id,'vat');
        $has_vat = ($vat > 0);
        $this->table->add_row('Loại hình:', ($has_vat ? 'V' : 'NV'));

        $staff_id = get_term_meta_value($term->term_id,'staff_business');
        if(!empty($staff_id)){
            $display_name = $this->admin_m->get_field_by_id($staff_id,'display_name');
            $display_name = empty($display_name) ? $this->admin_m->get_field_by_id($staff_id,'display_name') : $display_name;
            $this->table->add_row('Kinh doanh phụ trách:', "#{$staff_id} - {$display_name}");
        }
        $content.= $this->table->generate();

        $this->table->set_caption('Thông tin khách hàng');
        $content.=
        $this->table
        ->add_row('Người đại diện', get_term_meta_value($term->term_id,'representative_name'))
        ->add_row('Địa chỉ', get_term_meta_value($term->term_id,'representative_address'))
        ->add_row('Website', $term->term_name)
        ->add_row('Mail liên hệ', mailto(get_term_meta_value($term->term_id,'representative_email')))
        ->generate();

        $this->load->library('email');
        $this->email->from('support@webdoctor.vn', '1web.vn');

		$mail_report = get_term_meta_value($term_id,'mail_report');
		$this->email->to($mail_report);
		
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');

		$mail_cc = array();
		$mail_cc[] = 'support@webdoctor.vn';
		$mail_cc[] = 'quanly@webdoctor.vn';
		if($staff_mail)
			$mail_cc[] = $staff_mail;
		$this->email->cc($mail_cc);
		$this->email->subject($title);
		$this->email->message($content);

		return $this->email->send(TRUE);
    }

    /**
	 * Sends a mail payment alert.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  TRUE if successfully, otherwise return FALSE
	 */
	public function send_mail_payment_alert($term_id = 0)
	{
		$contract = $this->term_m->where('term_type','webdesign')->get($term_id);
		if( ! $contract) return FALSE;

		$tasks = $this->post_m
		->select('posts.post_id,post_author,post_title,post_type,post_status,start_date,end_date,post_thumb,post_slug,post_content')
		->join('term_posts','term_posts.post_id = posts.post_id')
		->where_in('post_type','oneweb-task')
		->order_by('post_status')
		->as_array()
		->get_many_by(['term_posts.term_id'=>$term_id]);

		$uncomplete_tasks = array();
		$count_tasks = count($tasks);
		$count_uncomplete_tasks = 0;
		$count_complete_tasks = 0;
		if($count_tasks > 0)
		{
			$uncomplete_tasks 		= array_filter($tasks,function($task){return ($task['post_status'] != 'complete');});
			$count_uncomplete_tasks = count($uncomplete_tasks);
			$count_complete_tasks 	= $count_tasks - $count_uncomplete_tasks;
		}

		$count_complete_tasks 	= numberformat($count_complete_tasks);
		$count_tasks 			= numberformat($count_tasks);

		$invs_amount 				= (double) get_term_meta_value($term_id,'invs_amount');
		$payment_amount 			= (double) get_term_meta_value($term_id,'payment_amount');
		$payment_percentage			= ((double) get_term_meta_value($term_id,'payment_percentage'))*100;
		$payment_amount_remaining 	= (double) get_term_meta_value($term_id,'payment_amount_remaining');

		$invs_amount_f 				= currency_numberformat($invs_amount, 'đ');
		$payment_amount_f 			= currency_numberformat($payment_amount, 'đ');
		$payment_percentage_f 		= currency_numberformat($payment_percentage, '%');
		$payment_amount_remaining_f = currency_numberformat($payment_amount_remaining, 'đ');		

		$contract_code = get_term_meta_value($term_id, 'contract_code');

		$this->load->model('webgeneral/webgeneral_kpi_m');

		$mail_to 	= $mail_cc = $mail_bcc = array();
		$title 		= "[1WEB.VN] HĐ {$contract_code} đã hoàn thành {$count_complete_tasks}/{$count_tasks} tác vụ và cần thanh toán {$payment_amount_remaining_f} để hoàn tất hợp đồng [ID:".time().']';

		$this->config->load('table_mail');
		$this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Tiến độ thu tiền và thực hiện tác vụ');

        $this->table
        ->add_row('Mã hợp đồng', anchor(admin_url("oneweb/task/{$term_id}"),$contract_code))
        ->add_row('Tiến độ tác vụ', "{$count_complete_tasks}/{$count_tasks} tác vụ")
        ->add_row('Tiến độ thanh toán', " {$payment_amount_f}/{$invs_amount_f} ({$payment_percentage_f})")
        ->add_row('Cần phải thu', $payment_amount_remaining_f);

        // Thông tin nhân viên kinh doanh phụ trách hợp đồng
		$staff_business_id = get_term_meta_value($term_id, 'staff_business');
		$staff_business = $this->admin_m->get_field_by_id($staff_business_id);
		
        if( ! empty($staff_business))
        {
        	$this->table->add_row('Kinh doanh phụ trách', $staff_business['display_name']);
        	$mail_to[] = $staff_business['user_email'];
        }

        $this->load->model('webgeneral/webgeneral_kpi_m');
		if($kpis = $this->webgeneral_kpi_m->get_many_by(['term_id' => $term_id,'kpi_type'=> 'tech']))
		{
			$staff_techs = array_map(function($x){ return $this->admin_m->get_field_by_id($x->user_id); }, $kpis);

        	$this->table->add_row('Kỹ thuật phụ trách', implode(', ', array_column($staff_techs, 'display_name')));
        	$mail_to = array_merge($mail_to, array_column($staff_techs, 'user_email'));
		}

		$content = $this->table->generate();

		//user_group;
		$user_group = $this->term_users_m->get_user_terms($staff_business_id,'user_group');
		$user_group_id = 0;
		$user_group_email = '';
		$department_id = 0;
		$mail_cc = array('hopdong@adsplus.vn');
		
		//user_group
        if(! empty($user_group)){
            $user_group_id = key($user_group);
            $user_group_email = get_term_meta_value($user_group_id, 'email');
            $department_id = $user_group[$user_group_id]->term_parent;
        }

        //department
        $department_email = get_term_meta_value($department_id,'department_mail');
        if(! empty($user_group_email)){
            array_push($mail_cc,$user_group_email);
        }        
        
        if(!empty($department_email)){
            array_push($mail_cc,$department_email);
        }

		$mail_bcc 	= [];

		$this->load->library('email');

		$result = $this->email->from('support@webdoctor.vn', 'Webdoctor.vn')
		->to($mail_to)->cc($mail_cc)->bcc($mail_bcc)
		->subject($title)->message($content)
		->send();
		
		if( ! $result) return FALSE;

		$log_id = $this->log_m->insert(array(
			'log_title'		=> $title,
			'log_status'	=> $result ? 1 : 0,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id ?? 0,
			'log_type'		=> 'oneweb_send_mail_payment_alert',
			'log_time_create'=> date('Y-m-d H:i:s'),
		));

		return TRUE;
	}

    /**
	 * Sends a mail alert before when off service.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  TRUE if successfully, otherwise return FALSE
	 */
    public function send_mail_alert_before_when_off($term_id = 0)
	{
        $term = $this->term_m->get($term_id);
		if(empty($term)) return FALSE;

        $time 				= time();
		$contract_end		= (int)get_term_meta_value($term_id,'contract_end');
		$contract_end		= $this->mdate->endOfDay($contract_end);
		$start_time_allowed = $this->mdate->startOfDay(strtotime('-'.NUMDAYS_BEFORE_FINISH.' days',$contract_end));

		// Nếu thời điểm gửi mail không nằm trong phạm vi [NUMDAYS_BEFORE_FINISH] đến thời gian kết thúc thì không gửi mail thông báo
		if($time < $start_time_allowed || $contract_end < $time) return FALSE;

		$send_renew_notice_email_next_time = (int) get_term_meta_value($term_id,'send_renew_notice_email_next_time');
		
		// Không hợp lệ nếu 2 lần gửi gần nhau có thời gian thấp hơn [NUMDAYS_NOTICEFINISH] ngày
		if($time < $send_renew_notice_email_next_time) return FALSE;

		update_term_meta($term_id,'send_renew_notice_email_next_time',strtotime('+'.NUMDAYS_NOTICEFINISH.' days'));

        // Main process
		$data = array('term'=>$term);

		$time_start = get_term_meta_value($term_id,'contract_begin');
		$time_end = get_term_meta_value($term_id,'contract_end');

		$start_service_time = get_term_meta_value($term_id,'start_service_time');
		$end_service_time = get_term_meta_value($term_id,'end_service_time');

		if(empty($start_service_time))
		{
			$start_service_time = time();
			update_term_meta($term_id, 'start_service_time', $start_service_time);
		}

		if(empty($end_service_time))
		{
			$time_end = ($start_service_time - $time_start) + $time_end;
			update_term_meta($term_id, 'end_service_time',$time_end);
		}

		$this->load->library('email');

		$title = 'Thông báo gia hạn dịch vụ 1WEB '.ucfirst($term->term_name);
		$data['title'] = 'Thông tin gia hạn dịch vụ';
		$data['email_source'] = $title;
		$data['term_id'] = $term_id;

		$data['tasks'] = $this->oneweb_task_m
		->select('posts.post_id, post_author, post_title, post_name, post_type, start_date, end_date')
		->join('term_posts', 'term_posts.post_id = posts.post_id')
		->where('start_date >=', $time_start)
		->where('end_date <=', strtotime('+10 month'))
		->where('term_id', $term_id)
		->order_by('posts.start_date')
		->group_by('posts.post_id')
		->get_all();

		$this->load->model('webgeneral/webgeneral_kpi_m');
		$kpis = $this->webgeneral_kpi_m->get_many_by(array(
                  'term_id'=>$term_id,
                  'kpi_type'=> 'tech',
                  ));
		$data['kpis'] = $kpis;
		$content_view = 'oneweb/report/renew_notice_email';

        $staff_id = get_term_meta_value($term_id, 'staff_business');
        $staff = [
            'user_id' => $staff_id,
            'display_name' => $this->admin_m->get_field_by_id($staff_id, 'display_name'),
            'user_email' => $this->admin_m->get_field_by_id($staff_id, 'user_email'),
        ];
        $data['staff'] = $staff;

		$content = $this->load->view($content_view,$data,TRUE);
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');

		$mail_report = get_term_meta_value($term_id,'mail_report');
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');


		$mail_cc = array();
		$mail_cc[] = 'support@webdoctor.vn';
		if($staff_mail)
			$mail_cc[] = $staff_mail;
		if($kpis)
		{
			foreach($kpis as $kpi)
			{
				$mail_cc[] = $this->admin_m->get_field_by_id($kpi->user_id,"user_email");
			}
		}

		$this->email->to($mail_report);
		$this->email->cc($mail_cc);
		$this->email->subject($title);
		$this->email->message($content);

		$send_status = $this->email->send(TRUE);
		return $send_status;
	}

    /**
     * Init contract record
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     self     ( description_of_the_return_value )
     */
    public function init($term_id = 0)
    {
    	$term = $this->oneweb_m->set_term_type()->get($term_id);
        if( ! $term) return FALSE;

        $this->term     = $term;
        $this->term_id  = $term_id;

        $this->init_people_belongs();
        return $this;
    }
    
    /**
     * Loads workers.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_workers()
    {
        parent::load_workers();
        $kpis = $this->webgeneral_kpi_m->select('user_id')->get_many_by(array('term_id'=>$this->term_id, 'kpi_type'=> 'tech'));

        if( ! $kpis) return $this;

        $_workers       = array_map(function($x){ return $this->admin_m->get_field_by_id($x->user_id); }, $kpis);
        $this->workers  = wp_parse_args(array_column($_workers, NULL, 'user_email'), $this->workers);

        return $this;
    }

    /**
     * Render Send content;
     *
     * @param      <type>  $content_view  The content view
     * @param      string  $layout        The layout
     *
     * @return     Mixed   Result
     */
    public function render_content($content_view, $layout = 'report/template/email/1web')
    {
    	return parent::render_content($content_view, $layout);
    }
}
/* End of file Oneweb_report_m.php */
/* Location: ./application/modules/googleads/models/Oneweb_report_m.php */