<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'models/Post_m.php');

class Oneweb_task_m extends Post_m {

	public $post_type 		= 'oneweb-task';
	public $before_get 		= array('set_post_type');
	public $before_create	= array('filtering_attributes','parse_timestamp_fields', 'created_on', 'updated_on' ,'post_type');
	public $before_update	= array('filtering_attributes','parse_timestamp_fields', 'updated_on');
	public $after_update 	= array('clean_cache');
	public $after_create 	= array('update_sms_status','clean_cache');

	public function __construct()
	{
		parent::__construct();

		$this->set_attributes();
		$this->set_protected_attributes();
	}

	/**
	* Sets the post type.
	*
	* @return     self
	*/
	public function set_post_type()
	{
		$this->where('posts.post_type', $this->post_type);
		return $this;
	}


	/**
	* Sets the post type.
	*
	* @return     self
	*/
	public function filtering_attributes($row)
	{
        foreach ($row as $key => $value)
        {
            if(in_array($key, $this->attributes)) continue;
            unset($row[$key]);
        }
		return $row;
	}


	/**
	* Parse all fields with timestamp type
	*
	* @param      array  $row    The row
	*
	* @return     array  $row
	*/
	public function parse_timestamp_fields($row)
	{
		if(!empty($row['start_date'])) $row['start_date'] = is_numeric($row['start_date'])?$row['start_date']:start_of_day($row['start_date']);
		if(!empty($row['end_date'])) $row['end_date'] = is_numeric($row['end_date'])?$row['end_date']:end_of_day($row['end_date']);
		
		return $row;
	}

	/**
	* Assigning created time
	*
	* @param      array  $post   The post
	*
	* @return     array  $post
	*/
	public function created_on($post)
	{
		unset($this->protected_attributes[array_search('created_on', $this->protected_attributes)]);
		$post['created_on'] = time();
		return $post;
	}

	/**
	 * Assigning Post type
	 *
	 * @param      array  $post   The post
	 *
	 * @return     array  $post
	 */
	public function post_type($post)
	{
		$post['post_type'] = $this->post_type;
		return $post;
	}

	/**
	* Assigning updated time
	*
	* @param      array  $post   The post
	*
	* @return     array  $post
	*/
	public function updated_on($post)
	{
		unset($this->protected_attributes[array_search('updated_on', $this->protected_attributes)]);
		$post['updated_on'] = time();
		return $post;
	}

	public function update_sms_status($post_id)
	{
		update_post_meta($post_id, 'sms_status', 0);
	}

	/**
	* Assigning updated time
	*
	* @param      array  $post   The post
	*
	* @return     array  $post
	*/
	public function clean_cache($argv = NULL)
	{
        parent::clean_cache($argv);

        if( ! is_array($argv)) return FALSE;

        $primary_value  = $argv[2];
        $result         = $argv[1];
        $data           = $argv[0];

        if( ! $result) return FALSE;

        $this->load->model('term_posts_m');
        $terms = $this->term_posts_m->get_post_terms($primary_value, 'oneweb');
        if(empty($terms)) return TRUE;

        array_walk($terms, function($term){ $this->scache->delete_group("term/{$term->term_id}_posts_"); });
        return TRUE;
	}

	/**
	* Sets the attributes.
	*
	* @param      array  $attributes  The attributes
	*
	* @return     self   ( description_of_the_return_value )
	*/
	protected function set_attributes($attributes = array())
	{
		if(!empty($attributes)) $this->attributes = $attributes;

		$this->attributes = ['post_id','post_author','post_title','post_name','post_content','post_excerpt','created_on','updated_on','post_parent','start_date','end_date','post_type','post_thumb','post_slug','post_status'];

		return $this;
	}

	/**
	* Sets the protected attributes.
	*
	* @param      array  $attributes  The attributes
	*
	* @return     self   ( description_of_the_return_value )
	*/
	protected function set_protected_attributes($attributes = array())
	{
		if(!empty($attributes)) $this->protected_attributes = $attributes;

		$this->protected_attributes = ['post_id','post_author','created_on','updated_on','post_parent','post_type','post_thumb','post_slug'];

		return $this;
	}
}
/* End of file Oneweb_task_m.php */
/* Location: ./application/modules/oneweb/models/Oneweb_task_m.php */