<p><strong>1.1</strong> Bên B tiến hành cung cấp cho Bên A gói phần mềm như sau:</p>
<?php
$this->table->clear();

$this->table->set_template(['table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">']);
$this->table->set_heading(['Tên sản phẩm', 'Thời gian', 'Đơn vị tính', 'Số lượng', 'Đơn giá</br>(VNĐ)', 'Thành tiền</br>(VNĐ)']);

$packageName = "Phần mềm 1WEB gói {$years} năm";
$contractDates = my_date($contract_begin, 'd/m/Y').' đến '.my_date($contract_end, 'd/m/Y');

$_row = array(
    $packageName,
    $contractDates,
    'Gói',
    $years,
    [ 'data' => currency_numberformat($service_package_price, ''), 'align'=>'right'],
    [ 'data' => currency_numberformat($service_package_price*$years, ''), 'align' => 'right' ]
);

$this->table->add_row($_row);

/* Danh sách chức năng nâng cao */
if( ! empty($advanced_functions) && is_array($advanced_functions))
{
    foreach ($advanced_functions as $_func)
    {
        if(empty($_func['value'])) continue;
        $this->table->add_row(['data' => "Chức năng: {$_func['name']}", 'colspan' => 5], ['data'=>currency_numberformat($_func['value'], ''),'align'=>'right']);
    }
}



/* Giảm giá áp dụng theo các chương trình mới hiện hành */
if( ! empty($discount_amount))
{
    $this->load->config('oneweb/discount');
    $discountPlanEnums  = $this->config->item('packages', 'discount');
    $discountPlanName   = !empty($discountPlanEnums[$discount_plan]) ? $discountPlanEnums[$discount_plan]['label'] : 'Giảm giá';
    if('combo' == $discount_plan
        && $availSaleOpts   = array_filter($discountPlanEnums['combo']['sale'], function($opt) use ($years){ return $opt['buy'] < $years; }))
    {
        $validSaleOpt       = end($availSaleOpts);
        $discountPlanName   .= "<i> (Mua {$validSaleOpt['buy']} năm Tặng {$validSaleOpt['free']} năm)</i>";
    }

    $this->table->add_row(['data' => $discountPlanName, 'colspan' => 5], ['data' => currency_numberformat(-1*$discount_amount, ''), 'align' => 'right']);
}

$total      = $contract_value;
$text_total = ( empty($vat) ) ? '<b>Tổng cộng</b>' : 'Tổng cộng' ;
$this->table->add_row(array('data'=> 'Thuế giá trị gia tăng', 'colspan' => 5),array('data'=>'/', 'align'=>'right'));
$this->table->add_row(array('data'=> $text_total, 'colspan' => 5),array('data'=>currency_numberformat($total, ''), 'align'=>'right'));

if( ! empty($vat))
{
    $tax = $total*($vat/100);
    $total = cal_vat($total, $vat);

    $this->table
    ->add_row(array('data'=>'VAT', 'colspan' => 5), array('data'=>'<strong>'.$vat.'&#37; ('.currency_numberformat($tax,'').')</strong>','align'=>'right'))
    ->add_row(array('data'=>"<b>Tổng cộng chi phí (bao gồm VAT {$vat}%)</b>", 'colspan' => 5), array('data'=>currency_numberformat($total, ''),'align'=>'right'));
}

$this->table->add_row(array('data'=> '<p><em>Bằng chữ: ' . ucfirst(mb_strtolower(convert_number_to_words($total))) .' đồng</em></p>', 'colspan'=>6)) ;

echo $this->table->generate();
?>

<br/>
<p><strong>1.2</strong> Bên B kích hoạt gói phần mềm cho Bên A trong vòng tối đa 02 ngày làm việc kể từ ngày Bên B nhận được thanh toán của Bên A</p>
<br/>

<p><strong>1.3</strong> Các bên cam kết thực hiện đúng các điều khoản, điều kiện của Hợp đồng cũng như các Phụ lục Hợp đồng kèm theo.</p>
<br/>

<p><u><strong>ĐIỀU 2: GIÁ TRỊ HỢP ĐỒNG VÀ PHƯƠNG THỨC THANH TOÁN</strong></u></p>
<p><strong>2.1 Giá trị hợp đồng</strong></p>
<p>
    Giá trị Bên A phải thanh toán cho Bên B là: <?php echo currency_numberformat($total, 'VNĐ');?> (Số tiền bằng chữ: <?php echo  ucfirst(mb_strtolower(convert_number_to_words($total)));?>).
    <br/>
    Phí duy trì các năm tiếp theo được thanh toán trước ngày hết hạn (ngày hết hạn được quy định tại Điều 1 của Hợp đồng này). Chính sách gia hạn của Bên B được quy định cụ thể tại Điều 6 của Hợp đồng này.
</p>