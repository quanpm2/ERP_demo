<p style="text-align:right">
    <em>Tp HCM, ngày <?php echo my_date($verified_on, 'd');?> tháng <?php echo my_date($verified_on, 'm');?> năm <?php echo my_date($verified_on, 'Y');?></em>
</p>
<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
    <tr>
        <td width="44%" valign="top" style="text-align:center">
            <p>
                <strong>BÊN A</strong><br />
                <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                <strong><?php echo @mb_strtoupper($customer->display_name);?>  </strong>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
        <td rowspan="2"></td>

        <td width="46%" valign="top" style="text-align:center">
            <p>
                <strong>BÊN B</strong><br />
                <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                <strong><?php echo $company_name; ?></strong>
            </p>
            <p><strong>&nbsp;</strong></p>
            <p><strong>&nbsp;</strong></p>
            <p>&nbsp;</p>
        </td>
    </tr>
    <tr>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_customer['Đại diện'];?></strong><br />
                <?php echo $data_customer['Chức vụ'];?>
            </p>
        </td>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_represent['Đại diện']; ?></strong><br />
                <?php echo $data_represent['Chức vụ']; ?>
            </p>
        </td>
    </tr>

    <tr>
        <td width="44%" valign="top" style="text-align:center">
            <p style="text-align:left">Các phê duyệt về thiết kế website và các trao đổi liên quan được thực hiện thông qua đại điện sau :</p>
            <p style="text-align:left">Đại diện Bên A : <?php echo ($qc_confirm_name ?? '');?></p>
            <p style="text-align:left">Email : <?php echo ($qc_confirm_email ?? '');?></p>
            <p style="text-align:left">Hotline : <?php echo ($qc_confirm_phone ?? '');?></p>
            <p>
                <strong>BÊN A</strong><br />
                <strong>THAY MẶT VÀ ĐẠI DIỆN</strong><br />
                <strong>QUYẾT ĐỊNH QUÁ TRÌNH THỰC HIỆN</strong>
            </p>
            <p style="text-align:left">&nbsp;</p>
            <p style="text-align:left">&nbsp;</p>
            <p style="text-align:left">&nbsp;</p>
            <p style="text-align:left">&nbsp;</p>
            <p style="text-align:left">&nbsp;</p>
            <p style="text-align:left">&nbsp;</p>
        </td>
        <td rowspan="2"></td>
        <td width="46%" valign="top" style="text-align:center"></td>
    </tr>
</table>
<script type="text/javascript">
(function() {
    var beforePrint = function() {
        console.log('Functionality to run before printing.');
    };
    var afterPrint = function() {
        console.log('Functionality to run after printing');
    };

    if (window.matchMedia) {
        var mediaQueryList = window.matchMedia('print');
        mediaQueryList.addListener(function(mql) {
            console.log(mql);
            if (mql.matches) {
                beforePrint();
            } else {
                afterPrint();
            }
        });
    }

    window.onbeforeprint = beforePrint;
    window.onafterprint = afterPrint;
}());
</script>
</body>
</html>