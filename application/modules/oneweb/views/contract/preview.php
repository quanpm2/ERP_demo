<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<br/>
<p><strong>Dựa trên: </strong> Nhu cầu của Bên A và khả năng cung cấp của Bên B về dịch vụ. Hai bên thống nhất ký kết hợp đồng kinh tế với các điều khoản cụ thể như sau:</p>
<br/>
<p><u><strong>ĐIỀU 1: NỘI DUNG MUA BÁN</strong></u></p>
<?php 
/* PRINT CHI CHIẾT HỢP ĐỒNG THEO GÓI */
echo $package_partial ?? '';
?>

<p><strong><u>2.2 Phương thức thanh toán:</u></strong></p>
<p> - Ngay sau khi hợp đồng đươc ký BÊN A thanh toán cho BÊN B 100% chi phí </p>
<p> - Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin tài khoản do Bên B cung cấp, cụ thể:</p>

<?php if( ! empty($bank_info)) :?>
<ul style="padding-left:0;list-style:none">
<?php foreach ($bank_info as $label => $text) :?>
    <?php if (is_array($text)) : ?>
        <?php foreach ($text as $key => $value) :?>
            <li>- <?php echo $key;?>: <?php echo $value;?></li>
        <?php endforeach;?>
    <?php continue; endif;?>
    <li>- <?php echo $label;?>: <?php echo $text;?></li>
<?php endforeach;?>
</ul>
<p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
<?php endif; ?>

<p><u><b><strong>ĐIỀU 3: QUYỀN VÀ NGHĨA VỤ CỦA BÊN A</strong></b></u></p>

<p><b>3.1.</b> Bên A chịu trách nhiệm về bản quyền và sở hữu trí tuệ đối với tất cả thông tin dữ liệu Bên A tải lên hệ thống của Bên B.</p>
<p><b>3.2.</b> Chịu trách nhiệm trước pháp luật nếu sử dụng website trái với quy định của nhà nước về sử dụng phần mềm Facebook, website Internet, quảng cáo và tuân thủ các quy định về việc sử dụng dịch vụ tại Điều 7 của Hợp đồng này. </p>
<p><b>3.3.</b> Theo dõi và yêu cầu Bên B thực hiện đầy đủ và đúng thời hạn các cam kết của Hợp đồng. </p>
<p><b>3.4.</b> Thanh toán đầy đủ và đúng hạn các khoản chi phí cho Bên B như đã nêu tại Điều 2 của Hợp đồng này. Và chủ động thanh toán các khoản chi phí gia hạn theo Điều 6 của Hợp đồng này.</p>
<p><b>3.5.</b> Bên A tuân theo Chính sách sử dụng dịch vụ của Bên B. Bên B có thể cung cấp các ứng dụng, tính năng hoặc chức năng mới cho phần mềm, việc sử dụng có thể tùy thuộc vào sự đồng ý của Bên A đối với các điều khoản bổ sung. Ngoài ra, Bên B sẽ cung cấp các sản phẩm có kết nối hoặc hợp tác với một Bên thứ ba, Bên A phải tuân thủ quy định sử dụng dịch vụ của Bên thứ ba đưa ra.</p>
<p><b>3.6.</b> Có quyền đơn phương chấm dứt Hợp đồng trong các tình huống bất khả kháng hoặc trong các tình huống vi phạm Hợp đồng do lỗi của Bên B.</p>
<p><b>3.7.</b> Có quyền khiếu nại tới các cơ quan chức năng để đòi bồi thường và xử lý theo luật định nếu Bên B vi phạm Hợp đồng.</p>
<p><b>3.8.</b> Gói không giới hạn từ 1WEB.VN chỉ dành riêng cho sản phẩm, dịch vụ của cá nhân/ công ty. Không chia sẻ tài khoản dưới mọi hình thức. Liên hệ hỗ trợ để được tư vấn gói dành cho đơn vị cung cấp dịch vụ.</p>
<br/>

<p><u><b><strong>ĐIỀU 4: QUYỀN VÀ NGHĨA VỤ CỦA BÊN B</strong></b></u></p>

<p><b>4.1.</b> Bên B có trách nhiệm thông báo cho Bên A trước 10 ngày làm việc kể từ ngày hết hạn Hợp đồng căn cứ vào Điều 1.</p>
<p><b>4.2.</b> Tư vấn, hỗ trợ Bên A trong các trường hợp cấu hình và sử dụng sản phẩm. Bảo hành và sửa chữa nhanh chóng khi website gặp vấn đề hư hỏng trong thời hạn Hợp đồng tại Điều 1.</p>
<p><b>4.3.</b> Bên B có nghĩa vụ đảm bảo website của Bên A hoạt động liên tục trong thời gian thực hiện, trừ các trường hợp bất khả kháng.</p>
<p><b>4.4.</b> Bên B cam kết bảo mật các thông tin trên dữ liệu website của Bên A, tuyệt đối không cung cấp thông tin cho bất kỳ bên thứ ba nào. Trừ các trường hợp:</p>
<p><b>4.4.1</b> Có sự đồng ý hoặc yêu cầu từ Bên A hoặc Bên A đồng ý với điều khoản sử dụng dịch vụ của Bên thứ ba có liên kết với Bên B</p>
<p><b>4.4.2</b> Có yêu cầu từ các cơ quan pháp luật.</p>
<p><b>4.5.</b> Có quyền đơn phương chấm dứt Hợp đồng trong các tình huống bất khả kháng hoặc trong các tình huống vi phạm Hợp đồng do lỗi của Bên A.</p>
<p><b>4.6.</b> Có quyền khiếu nại tới các cơ quan chức năng để đòi bồi thường và xử lý theo luật định nếu Bên A vi phạm Hợp đồng.</p>

<br/>
<p><u><b><strong>ĐIỀU 5: BẤT KHẢ KHÁNG</strong></b></u></p>
<p><b>5.1.</b> Hai Bên sẽ được miễn trừ việc thực hiện các nghĩa vụ trong Hợp đồng này, nếu/và trong phạm vi của việc thực hiện nghĩa vụ này bị ngăn cản do Sự Kiện Bất Khả Kháng và bên bị ảnh hưởng đã thi hành mọi biện pháp cần thiết để khắc phục và đã ngay lập tức thông báo bằng văn bản hoặc email cho Bên kia biết về việc bị ảnh hưởng tới trách nhiệm thực hiệc các nghĩa vụ theo Hợp đồng này.</p>
<p><b>5.2.</b> Nếu Sự Kiện Bất Khả Kháng kéo dài hơn ba mươi (30) Ngày làm việc, mỗi Bên đều có quyền chấm dứt Hợp đồng này mà không phải bồi thường cho Bên kia bất cứ thiệt hại trực tiếp nào phát sinh từ Sự Kiện Bất Khả Kháng.</p>
<p><b>5.3.</b> Các Bên sẽ được xem xét giảm trách nhiệm khi xảy ra Sự Kiện Bất Khả Kháng trong những trường hợp sau đây:</p>
<p><b>5.3.1</b> Việc vi phạm Hợp đồng của một Bên là nguyên nhân trực tiếp dẫn tới sự vi phạm Hợp đồng của Bên kia;</p>
<p><b>5.3.2</b> Các trường hợp khác theo quy định của pháp luật.</p>
<br/>

<p><u><b><strong>ĐIỀU 6: NÂNG CẤP VÀ GIA HẠN WEBSITE</strong></b></u></p>
<p><b>6.1.</b> Bên A có quyền nâng cấp gói phần mềm đang sử dụng lên gói dịch vụ cao hơn trong suốt quá trình sử dụng. </p>
<p><b>6.2.</b> Bên A đóng thêm cho Bên B khoản phí chênh lệch giữa hai gói phần mềm nếu có tính từ thời điểm nâng cấp phần mềm đến ngày hết hạn.</p>
<p><b>6.3.</b> Bên B có trách nhiệm tiến hành nâng cấp gói phần mềm lên cho Bên A sau 01 (một) ngày làm việc kể từ khi nhận yêu cầu và tiền thanh toán nếu có của Bên A.</p>
<p><b>6.4.</b> Trong vòng 6 tháng kể từ khi hết hạn sử dụng gói phần mềm, nếu không có hoạt động nào của Bên A trong phần quản trị gói phần mềm, thì gói phần mềm sẽ tự động hủy.</p>
<br/>

<p><u><b><strong>ĐIỀU 7: QUY ĐỊNH SỬ DỤNG PHẦN MỀM</strong></b></u></p>

<p><b>7.1.</b> Sau khi kích hoạt gói phần mềm cho Bên A, Bên B không chịu trách nhiệm và không bảo đảm về tính chính xác của những thông tin từ gói phần mềm của Bên A. Bên B không chịu trách nhiệm pháp lý và bồi thường cho Bên A và Bên thứ ba đối với các thiệt hại trực tiếp, gián tiếp, vô ý, đặc biệt, vô hình, các thiệt hại về lợi nhuận, doanh thu, uy tín phát sinh từ việc sử dụng sản phẩm, gói dịch vụ của Bên A.</p>
<p><b>7.2.</b> Bên B có quyền ngưng cung cấp gói phần mềm mà không hoàn lại bất kỳ một chi phí nào trong các trường hợp sau:</p>
<p><b>7.2.1</b> Bên A dùng gói phần mềm vào bất kỳ mục đích/hình thức nào vi phạm pháp luật Việt Nam, đặc biệt về vấn đề bản quyền phần mềm, ca khúc…;</p>
<p><b>7.2.2</b> Bên A gửi, tạo liên kết hoặc trung chuyển cho các dữ liệu mang tính chất bất hợp pháp, đe doạ, lừa dối, thù hằn, xuyên tạc, nói xấu, tục tĩu, khiêu dâm, xúc phạm… hay các hình thức bị pháp luật Việt Nam ngăn cấm khác dưới bất kỳ cách thức nào;</p>
<p><b>7.2.3</b> Bên A lưu trữ, truyền bá các dữ liệu nào mà cấu thành hoặc khuyến khích các hình thức phạm tội; hoặc các dữ liệu mang tính vi phạm luật sáng chế, nhãn hiệu, quyền thiết kế, bản quyền hay bất kỳ quyền sỡ hữu trí tuệ hoặc các quyền hạn của bất kỳ cá nhân nào;</p>
<p><b>7.2.4</b> Bên A sử dụng gói phần mềm hoặc tài khoản để phá hoại Khách hàng khác của Bên B;</p>
<p><b>7.2.5</b> Bên A sử dụng các chương trình có khả năng làm tắc nghẽn hoặc đình trệ hệ thống, như gây cạn kiệt tài nguyên hệ thống;</p>
<p><b>7.2.6</b> Bên A không thanh toán các chi phí đúng hạn.</p>
<p><b>7.3.</b> Bên A giữ một cách an toàn các thông tin nhận biết, mật khẩu hay những thông tin khác liên quan đến tài khoản của mình và lập tức thông báo cho Bên B khi phát hiện các hình thức truy cập trái phép bằng tài khoản của mình hoặc các sơ hở về bảo mật, bao gồm việc mất mát, đánh cắp hoặc để lộ các thông tin về mật khẩu và các thông tin bảo mật khác. Bên B không chịu trách nhiệm cho các thiệt hại vì lý do Bên A làm mất các thông tin này.</p>
<p><b>7.4.</b> Trong suốt quá trình sử dụng phần mềm, Bên A sẽ được bảo hành sản phẩm theo chính sách bảo hành của Bên B. Bên B sẽ tiếp nhận và phản hồi trong 8 giờ làm việc, và thực hiện bảo hành theo thời gian cam kết trong phản hồi.</p>
<br/>

<p><u><b><strong>ĐIỀU 8: GIẢI QUYẾT TRANH CHẤP HỢP ĐỒNG </strong></b></u></p>
<p><b>8.1.</b> Trong quá trình thực hiện Hợp đồng hai Bên cần chủ động thông báo cho nhau biết tiến độ thực hiện, nếu có vấn đề bất lợi phát sinh hoặc xảy ra tranh chấp, các Bên phải kịp thời thông báo bằng văn bản cho nhau biết và phải chủ động bàn bạc giải quyết trên cơ sở thương lượng, tôn trọng quyền và lợi ích hợp pháp của các Bên. Kết quả hòa giải, thương lượng sẽ được các bên lập thành Biên bản làm việc có xác nhận của đại diện các bên. Biên Bản này có giá trị như Phụ lục Hợp đồng.</p>
<p><b>8.2.</b> Trường hợp sản phẩm - dịch vụ do Bên B cung cấp bị gián đoạn hoặc ngừng hoạt động bởi các lý do bất khả kháng, mà Bên B chưa hỗ trợ khắc phục, trong thời gian quá 48 giờ không thông báo cho Bên A, Bên B có trách nhiệm bồi thường gấp 3 lần thời gian duy trì dịch vụ - sản phẩm bị gián đoạn đó.</p>
<p><b>8.3.</b> Gói phần mềm bị ngưng hoạt động do các lý do tấn công mạng, truy cập bất hợp pháp: 2 Bên sẽ rà soát đánh giá nguyên nhân, Bên gây ra nguyên nhân chịu trách nhiệm.</p>
<p><b>8.4.</b> Trường hợp Bên A đơn phương chấm dứt hợp đồng mà không phải do lỗi vi phạm của Bên B, Bên B không có trách nhiệm hoàn lại số tiền Bên A đã thanh toán chưa sử dụng hết.</p>
<p><b>8.5.</b> Trường hợp Bên B đơn phương chấm dứt hợp đồng mà do lỗi vi phạm hợp đồng của Bên B, Bên B phải hoàn trả cho Bên A toàn bộ số tiền còn lại đã ký tương ứng với sản phẩm – dịch vụ.</p>
<p><b>8.6.</b> Trường hợp không thể giải quyết bằng hòa giải, thương lượng, tranh chấp sẽ được đưa ra giải quyết tại tòa án nhân dân có thẩm quyền theo quy định của pháp luật. Quyết định của Tòa án là quyết định cuối cùng, buộc các Bên phải thực hiện. Bên có lỗi sẽ phải chịu mọi án phí và lệ phí liên quan</p>
<br/>

<p><u><b><strong>ĐIỀU 9: SỬA ĐỔI VÀ CHẤM DỨT HỢP ĐỒNG </strong></b></u></p>
<p><b>9.1.</b> Một bên muốn sửa đổi, bổ sung, chấm dứt Hợp đồng trước thời hạn hết hạn Hợp đồng, phải thông báo cho Bên còn lại bằng văn bản trước 30 ngày. Trong thời hạn 15 ngày kể từ ngày nhận được thông báo (hoặc ngày ghi nhận đã nhận được thông báo (qua chuyển phát, chuyển trực tiếp, hoặc hình thức tương tự), nếu Bên còn lại không có ý kiến trả lời bằng văn bản thì thông báo đó mặc nhiên được chấp nhận; </p>
<p><b>9.2.</b> Các trường hợp chấm dứt Hợp đồng:</p>
<p><b>9.2.1</b> Thỏa thuận chấm dứt Hợp đồng; </p>
<p><b>9.2.2</b> Hợp đồng hết hạn; </p>
<p><b>9.2.3</b> Một bên đơn phương chấm dứt Hợp đồng trong các trường hợp: 
<b>(i) </b> Thông báo chấm dứt Hợp đồng tại Khoản 1 Điều này
<br/>
<b>(ii) </b> Trường hợp một Bên vi phạm Hợp đồng và đã được Bên kia yêu cầu khắc phục mà không khắc phục hoặc không thể khắc phục hoặc tiếp tục tái phạm, Bên bị vi phạm có quyền đơn phương chấm dứt Hợp đồng trước thời hạn và đòi bồi thường thiệt hại theo quy định Hợp đồng và quy định pháp luật liên quan. 
<br/>
<b>(iii) </b> Hợp đồng có thể bị hủy bỏ ngay lập tức bằng fax, email, điện thoại bởi một trong các Bên mà không cần báo trước nếu: Một trong các Bên còn lại bị khiếu nại hay bị khởi kiện theo luật phá sản hoặc không có khả năng thanh toán vì bất cứ lý do nào (không có khả năng thanh toán được hiểu là quá hạn thanh toán 30 ngày mà không thực hiện nghĩa vụ thanh toán). Trường hợp Bên A đã hoàn tất thanh toán gói dịch vụ cho Bên B thì thời gian kết thúc hợp đồng theo thời gian Bên A đã mua gói dịch vụ.
<br/>
<b>(iv) </b> Bên B có thể tạm đình chỉ hoặc đơn phương chấm dứt Hợp đồng mà không phải chịu bất kỳ thiệt hại nào phát sinh nếu Bên A vi phạm hoặc không thực hiện đúng nghĩa vụ của mình được quy định trong Hợp đồng hoặc Bên B nhận định hoạt động của Bên A là không an toàn và có các rủi ro trong thanh toán. 
<br/>
<b>(v) </b> Các trường hợp khác theo quy định của Hợp đồng này. </p>
<p><b>9.3.</b> Xử lý chấm dứt Hợp đồng:</p>
<p><b>9.3.1</b> Việc chấm dứt Hợp đồng này vì bất kỳ lý do gì và việc chấm dứt hoạt động kinh doanh của một Bên không có nghĩa là chấm dứt các nghĩa vụ mà Bên đó chưa hoàn thành theo quy định của Hợp đồng.</p>
<p><b>9.3.2</b> Trong bất kỳ trường hợp nào nêu trên, Hợp đồng được chấm dứt Bên B sẽ không hoàn lại phần tiền còn lại của gói phần mềm Bên A đã đăng ký sử dụng.</p>
<p><b>9.3.3</b> Khi chấm dứt hợp đồng Bên B không chịu trách nhiệm về việc đảm bảo lưu trữ dữ liệu trên gói phần mềm đăng ký của Bên A.</p>
<br/>

<p><u><b><strong>ĐIỀU 10: BẢO HÀNH VÀ BẢO TRÌ</strong></b></u></p>
<p><b>10.1.</b> Bảo hành website trong thời gian Bên A còn sử dụng gói phần mềm đăng ký của Bên B.</p>
<p><b>10.2.</b> Không bảo hành đối với các website sử dụng hosting bên ngoài. </p>
<br/>

<p><u><b><strong>ĐIỀU 11: ĐIỀU KHOẢN CHUNG</strong></b></u></p>
<p><b>11.1.</b> Hai Bên cam kết rằng mọi thông tin từ/bởi/có liên quan đến Hợp đồng này đều được coi là Thông Tin Bảo Mật. Theo đó, hai Bên cam kết bảo mật và không tiết lộ thông tin về việc thỏa thuận ký kết hợp đồng cho Bên thứ ba bất kỳ nào trừ trường hợp theo yêu cầu của cơ quan có thẩm quyền. Điều khoản này sẽ giữ nguyên giá trị hiệu lực 6 (sáu) tháng kể từ thời điểm Hợp đồng này được chấm dứt.</p>
<p><b>11.2.</b> Hai bên đồng ý rằng (các) điều khoản trong Hợp đồng này có giá trị độc lập với nhau trừ trường hợp rõ ràng được dẫn chiếu một cách chính xác và đầy đủ. Việc một hoặc một vài điều khoản trong Hợp đồng này bị vô hiệu sẽ không làm ảnh hưởng đến việc thực thi các điều khoản khác trong Hợp đồng. Trường hợp có bất kỳ điều khoản nào bị vô hiệu thì hai bên cam kết bằng tất cả nỗ lực của mình điều chỉnh sao cho phù hợp với các quy định của pháp luật để thực thi.</p>
<p><b>11.3.</b> Những vấn đề chưa được quy định trong Hợp đồng này sẽ được hai Bên thống nhất áp dụng quy định của Pháp luật của Nước CHXHCN Việt Nam.</p>
<p><b>11.4.</b> Hợp đồng có giá trị thời hạn theo Điều 1 kể từ ngày ký. Trường hợp khi Hợp đồng này hết hạn mà Bên A gia hạn gói dịch vụ landing page của Bên B thì Hợp đồng sẽ được tự động gia hạn thêm đúng bằng số tháng mà Bên A thực hiện gia hạn.</p>
<p><b>11.5.</b> Bất kỳ sự chỉnh sửa và/hoặc bổ sung nào trong Hợp đồng này chỉ có hiệu lực khi được lập thành văn bản (bằng Phụ lục Hợp đồng) và được cả hai Bên ký kết, các Phụ lục Hợp đồng này là phần không thể tách rời của Hợp đồng.</p>
<p><b>11.6.</b> Hợp đồng được lập thành hai (02) bản, Bên A giữ một (01) bản, Bên B giữ (01) bản, cùng có giá trị pháp lý như nhau.</p>
