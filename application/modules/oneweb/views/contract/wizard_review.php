<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->javascript->add('plugins/wizard/jquery.bootstrap.wizard.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');

$this->template->javascript->add(admin_theme_url('modules/component/form.js'));
$this->template->javascript->add(admin_theme_url('modules/webbuild/js/webbuild-service-price-component.js'));
$this->template->javascript->add(admin_theme_url('modules/oneweb/js/oneweb-service-price-component.js'));
?>
<div class="col-md-12" id="review-partial">

  <h2 class="page-header">
    <small class="pull-right">Ngày tạo : <?php echo my_date(time(), 'd-m-Y');?></small>
  </h2>

  <div class="row">
  <?php
  $representative_gender  = force_var(get_term_meta_value($edit->term_id,'representative_gender'),'Bà','Ông');
  $representative_name    = get_term_meta_value($edit->term_id,'representative_name') ?: '';
  $display_name           = "{$representative_gender} {$representative_name}";
  $representative_email   = get_term_meta_value($edit->term_id,'representative_email');
  $representative_address = get_term_meta_value($edit->term_id,'representative_address');
  $representative_phone   = get_term_meta_value($edit->term_id,'representative_phone');

  $contract_begin       = get_term_meta_value($edit->term_id,'contract_begin');
  $contract_begin_date  = my_date($contract_begin,'d/m/Y');
  $contract_end         = get_term_meta_value($edit->term_id,'contract_end');
  $contract_end_date    = my_date($contract_end,'d/m/Y');
  $contract_daterange   = "{$contract_begin_date} đến {$contract_end_date}";
        
  echo $this->admin_form->set_col(6)->box_open('Thông tin khách hàng');
  echo $this->table->clear()
    ->add_row('Người đại diện',$display_name?:'Chưa cập nhật')
    ->add_row('Email',$representative_email?:'Chưa cập nhật')
    ->add_row('Địa chỉ',$representative_address?:'Chưa cập nhật')
    ->add_row('Số điện thoại',$representative_phone?:'Chưa cập nhật')
    ->add_row('Chức vụ',$edit->extra['representative_position']??'Chưa cập nhật')
    ->add_row('Mã Số thuế',$edit->extra['customer_tax']??'Chưa cập nhật')
    ->add_row('Thời gian thực hiện',$contract_daterange?:'Chưa cập nhật')
    ->generate();
  echo $this->admin_form->box_close();
  ?>
    <div class="col-md-6">
      <oneweb-service-price-component :id="<?php echo $edit->term_id;?>"></oneweb-service-price-component>
    </div>
  </div>

</div>
<div class="clearfix"></div>

<?php

$hidden_values = ['edit[term_status]'=>'waitingforapprove','edit[term_id]'=>$edit->term_id,'edit[term_type]'=>$edit->term_type];
echo $this->admin_form->form_open('',[],$hidden_values);
echo $this->admin_form->submit('','confirm_step_finish','confirm_step_finish','', array('style'=>'display:none;','id'=>'confirm_step_finish'));
echo $this->admin_form->form_close();
?>
<script type="text/javascript"> var app_root = new Vue({ el: '#review-partial' }); </script>