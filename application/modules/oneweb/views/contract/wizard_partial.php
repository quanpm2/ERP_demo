<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->javascript->add(base_url("dist/vOnewebConfigurationContractBox.js?v=202208261028"));
?>
<div class="col-md-12" id="service_tab">
	
	<form action="<?php echo admin_url()?>contract/create_wizard/index/<?php echo $edit->term_id?>.html" method="post" accept-charset="utf-8">

		<input type="hidden" name="edit[term_id]" value="<?php echo $edit->term_id?>">
		<div id="vOnewebConfigurationContractBox">
		    <v-oneweb-configuration-contract-box :id="<?php echo $edit->term_id;?>" mode="update"></v-oneweb-configuration-contract-box>
		</div>
		<div class="form-group"><div class="form-group col-md-10"><div class="input-group col-sm-12"><input type="submit" name="confirm_step_service" value="confirm_step_service" id="confirm_step_service" style="display:none;" class="form-control "></div></div>
		</div>
	</form>
</div>
<script type="text/javascript"> var app_root = new Vue({ el: '#service_tab' }); </script>