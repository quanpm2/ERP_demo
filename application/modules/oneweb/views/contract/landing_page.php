<p><strong><u>1.1 Chi tiết các thành phần của website cần xây dựng:</u></strong></p>
<table border="1" cellspacing="0" cellpadding="5" width="100%">
    <tr>  
        <th width="15%" nowrap="nowrap"><strong align="center">STT</strong></th>
        <th width="68%"><strong align="center"><u>Phần tính năng cơ bản</u></strong></th>
        <th width="30%" nowrap="nowrap"><strong align="center">Ghi chú</strong></th>
    </tr>

    <tr>
        <td colspan="2"><b>1. Banner động</b></td>
        <td></td>
    </tr>

    <!-- chi tiết tính năng cơ bản -->
    <tr>
        <td></td>
        <td>

            <p> <strong>-	Thiết kế banner động ấn tượng.</strong></p>
            <p><strong>-	Những hình ảnh trong banner thể hiện những dịch vụ và sản phẩm của Qúy công ty.</strong></p>

        </td>
        <td></td>
    </tr>

    <tr>
        <td colspan="2"><b>2. Trang chức năng (Landing page)</b></td>
        <td></td>
    </tr>

    <!-- chi tiết tính năng cơ bản -->
    <tr>
        <td></td>
        <td>
            <p>Trang Landing page gồm các thành phần chính như bản vẽ đã được 02 bên thống nhất:</p>
            <p>-	Trình đơn (menu): trượt tới các thành phần (module) được chỉ định trên chính trang landing  </p>
            <p>-	Phần giới thiệu dịch vụ, sản phẩm</p>
            <p>-	Phần giới thiệu tiện ích </p>
            <p>-	Phần sơ dồ</p>
            <p>-	Thành phần liên hệ</p>
        </td>
        <td></td>
    </tr>

    <tr>
        <td colspan="2"><b>3. Chat trực tuyến</b></td>
        <td></td>
    </tr>

    <!-- chi tiết tính năng cơ bản -->
    <tr>
        <td></td>
        <td>
            <p>Cho phép khách hàng và người quản lý Website có thể chat trực tiếp với nhau thông qua Website</p>
        </td>
        <td></td>
    </tr>

    <tr>
        <td colspan="2"><b>4. Liên kết mạng xã hội</b></td>
        <td></td>
    </tr>

    <!-- chi tiết tính năng cơ bản -->
    <tr>
        <td></td>
        <td>
            <p>Công cụ giúp Website liên kết với nhiều trang mạng xã hội phổ biến hiện nay như Facebook, Google+, Zing me,… 
                Ngoài việc like, share trực tiếp, khách hàng có thể trực tiếp bình luận bằng mạng xã hội trên trang bài viết.

            </p>

        </td>
        <td></td>
    </tr>

    <tr>
        <td colspan="2"><b>5 .Đăng ký nhận thông tin</b></td>
        <td></td>
    </tr>

    <!-- chi tiết tính năng cơ bản -->
    <tr>
        <td></td>
        <td>
            Đăng ký nhận thông tin là chức năng để khách hàng lưu địa chỉ Email liên hệ của mình lại trên Website để sau này có những chương trình CSKH cho những khách hàng này.
        </td>
        <td></td>
    </tr>

    <tr>
        <td colspan="2"><b>6. Tối ưu hóa Website</b></td>
        <td></td>
    </tr>

    <!-- chi tiết tính năng cơ bản -->
    <tr>
        <td></td>
        <td>
            <p>Tối ưu hóa Website theo tiêu chuẩn SEOquake nhằm hỗ trợ Website SEO tốt hơn cụ thể là các phần:</p>
            <p><strong>URL:</strong> </p>
            <ul>
                <p>- Giải thích: Url là đường link hiển thị trên google khi người dùng tìm kiếm.</p>
                <p> - Cách thức tối ưu:</p>
                <p>URL chứa từ khóa cần SEO. </p>
                <ul>
                    <p>+ Các từ trong url được phân cách bằng dấu gạch ngang "-".</p>
                    <p>+Số lượng ký tự trong url < 60.</p>
                </ul>
            </ul>
            <p><strong>Title:</strong> Là tiêu đề của website (hoặc bài viết) hiển thị trên google khi người dùng tìm kiếm.</p>
            <p><strong>Meta description:</strong> Là thẻ mô tả tổng quan của trang web. Thẻ mô tả được hiển thị bên dưới tiêu đề khi người dùng tìm kiếm.</p>
            <p><strong>Meta keywords:</strong> Từ khóa dành cho các máy tìm kiếm. Tuy nhiên hiện tại thẻ Meta keywords đã không còn giá trị trong SEO hoặc giá trị rất thấp nên mình bỏ qua không tối ưu.</p>
            <p><strong>Heading:</strong> Bao gồm các thẻ từ H1-H6 có tác dụng nhấn mạnh các phần nội dung quan trọng bên trong trang web từ đó tăng cường khả năng Seo cho website.</p>
            <p><strong>Robot.txt:</strong> File điều hướng Google, cho phép Google Bot được index (đánh chỉ mục) nội dung nào bên trong website.</p>
            <p><strong>XML sitemap:</strong> Sitemap là sơ đồ liệt kê tất cả các mục thông tin (đường link) bên trong trang web hỗ trợ cho Google Bot dễ dàng di chuyển và index tất cả các nội dung bên trong.</p>
            <p><strong>Google Analytics:</strong> cài đặt Công cụ phân tích thống kê số lượng truy cập vào website.</p>
            <p><strong>Favicon:</strong> Hình ảnh hiển thị (thông thường là logo) trên thanh tab trình duyệt</p>
        </td>
        <td></td>
    </tr>

    <tr>
        <td colspan="2"><b>7. Quản lý đăng tin - cập nhật sản phẩm</b></td>
        <td></td>
    </tr>

    <!-- chi tiết tính năng cơ bản -->
    <tr>
        <td></td>
        <td>
            <p>Bảng quản trị</p>
            <p>Quản lý thông tin tài khoản đăng nhập, thay đổi mật khẩu </p>
            <p>Quản trị thông tin giới thiệu</p>
            <p>Quản trị banner</p>
            <i>Giao diện tiếng Việt, dễ sử dụng, quản trị</i>
            <i>Tích hợp bộ soạn thảo nội dung WYSIWYG tương tự Microsoft Words</i></br>
            <i>* Quản trị: Tạo, sửa, xóa, cập nhật trạng thái, sắp xếp..</i>


        </td>
        <td></td>
    </tr>

</table>
<br/>
<p><strong><u>1.2 Kế hoạch thực hiện và quy trình phối hợp giữa 2 bên</u></strong></p>
<br/>
<!--<p>Tổng thời gian thực hiện và bàn giao: tối đa từ 30 – 60 ngày phụ thuộc vào sự phối hợp và cung cấp thông tin, duyệt mẫu của Bên A; cụ thể:</p>-->


<table border="1" cellspacing="0" cellpadding="5" width="100%">
    <tbody>
        <tr>
            <td width="30">
                <p align="center">
                    <b>
                        <span>
                            STT
                        </span>
                    </b>
                </p>
            </td>
            <td width="123">
                <p align="center">
                    <b><span>NỘI DUNG</span></b>
                </p>
            </td>
            <td width="236">
                <p align="center">
                    <b>
                        <span>WEBDOCTOR</span>
                    </b>
                </p>
            </td>
            <td width="288">
                <p align="center">
                    <b>
                        <span>
                            KHÁCH HÀNG
                        </span>
                    </b>
                </p>
            </td>
            <td width="70" style="text-align: center;"><b>SỐ NGÀY THỰC HIỆN</b></td>
        </tr>

        <!-- Thu thập thông tin thiết kế web -->
        <tr>
            <td><p align="center"><span><b>1</b></span></p></td>  
            <td><p><span>Thu thập thông tin thiết kế web</span></p></td>
            <td colspan="2" style="text-align: center;">Trao đổi và thống nhất giao diện và chức năng của web</td>
            <td style="text-align: center;">2</td>
        </tr>

        <!-- Ký hợp đồng -->
        <tr>
            <td width="47" rowspan="3">
                <p align="center">
                    <span>
                        <b>2</b>
                    </span>
                </p>
            </td>
            <td width="123" rowspan="3">
                <p>
                    <span>
                        Ký hợp đồng
                    </span>
                </p>
            </td>
            <td width="236">

            </td>
            <td width="288">
                <p>
                    <b>
                        <span>
                            Thanh toán đợt 1 (100% giá trị hợp đồng)
                        </span>
                    </b>
                </p>
            </td>
            <td rowspan="2" style="text-align: center;">1</td>
        </tr>
        <tr>
            <td width="236">
                <p>
                    <span>
                        Nhận hình ảnh, thông tin của sản phẩm, công ty
                    </span>
                </p>
            </td>
            <td width="288">
                <p>
                    <span>
                        Cung cấp thông tin, nội dung của website (Landing Page)
                    </span>
                </p>
            </td>
        </tr>
        <tr>
        </tr>

        <!-- Thiết kê giao diện -->
        <tr>
            <td width="47" rowspan="3">
                <p align="center">
                    <span>
                        <b>3</b>
                    </span>
                </p>
            </td>
            <td width="123" rowspan="3">
                <p>
                    <span>
                        Thiết kế giao diện
                    </span>
                </p>
            </td>
            <td width="236">
                Triển khai thực hiện
            </td>
            <td width="288">
                <p>
                    <span>
                        Chuẩn bị nội dung chi tiết
                    </span>
                </p>
            </td>
            <td rowspan="2" style="text-align: center;">1</td>
        </tr>


        <tr>
            <td width="236">
                <p>
                    <span>
                        Gửi demo giao diện
                    </span>
                </p>
            </td>
            <td width="288">
                <p>
                    <span>
                        Duyệt demo giao diện
                    </span>
                </p>
            </td>
        </tr>
        <tr>
        </tr>

        <!-- Demo giao diện website -->
        <tr>
            <td width="47" rowspan="3">
                <p align="center">
                    <span>
                        <b>4</b>
                    </span>
                </p>
            </td>
            <td width="123" rowspan="3">
                <p>

                    <span>
                        Demo giao diện website (Landing Page)
                    </span>

                </p>
            </td>
            <td width="236">
                Chỉnh sửa giao diện
            </td>
            <td width="288">
                <p>
                    <span>
                        Yêu cầu chỉnh sửa giao diện
                    </span>
                </p>
            </td>
            <td rowspan="2" style="text-align: center;">1</td>
        </tr>


        <tr>
            <td width="236">
                <p>
                    <span>
                        Thống nhất giao diện của website
                    </span>
                </p>
            </td>
            <td width="288">
                <p>
                    <span>

                    </span>
                </p>
            </td>
        </tr>
        <tr>
        </tr>

        <!-- Lập trình web -->
        <tr>
            <td width="47" rowspan="1">
                <p align="center">
                    <span>
                        <b>5</b>
                    </span>
                </p>
            </td>
            <td width="123" rowspan="1">
                <p>
                    <span>Lập trình web

                    </span>
                </p>
            </td>
            <td width="236">
                <p>
                    <span>
                        Coding, lập trình xây dựng CSDL cho hạng mục chức năng chung
                    </span>
                </p>
            </td>
            <td width="288">
                <p>
                    <span>
                        Theo dõi tiến độ triển khai
                    </span>
                </p>
            </td>
            <td rowspan="1" style="text-align: center;">1</td>
        </tr>



        <tr>
        </tr>


        <!-- Xuất bản (website chạy trên Internet)   -->
        <tr>
            <td width="47">
                <p align="center">
                    <span>
                        <b>6</b>
                    </span>
                </p>
            </td>
            <td width="123">
                <p>

                    <span>Xuất bản (website chạy trên Internet)

                    </span>

                </p>
            </td>
            <td width="288">
                <p>
                    <span>
                        Cho web chạy thử trên Internet, chỉnh sửa nếu cần
                    </span>
                </p>
            </td>
            <td>Vận hành thử và thống nhất các chức năng, phản hồi ý kiến</td>
            <td style="text-align: center;">1</td>
        </tr>

        <!-- Thiết kế banner và thêm hiệu ứng tải trang (Lazyload) -->
        <tr>
            <td width="47" rowspan="3">
                <p align="center">
                    <span>
                        <b>7</b>
                    </span>
                </p>
            </td>
            <td width="123" rowspan="3">
                <p>
                    <span>Thiết kế banner và thêm hiệu ứng tải trang (Lazyload)

                    </span>
                </p>
            </td>
            <td width="236">
                Thêm hiệu ứng website
            </td>
            <td width="288">
                <p>
                    <span>
                        Cung cấp thông tin, hình ảnh thiết kế
                    </span>
                </p>
            </td>
            <td rowspan="2" style="text-align: center;">1</td>
        </tr>


        <tr>
            <td width="236">
                <p>
                    <span>
                        Thống nhất thiết kế banner
                    </span>
                </p>
            </td>
            <td width="288">
                <p>
                    <span>

                    </span>
                </p>
            </td>
        </tr>
        <tr>
        </tr>

        <!-- Bàn giao và hướng dẫn Admin -->
        <tr>
            <td width="47" rowspan="4">
                <p align="center">
                    <span>
                        <b>8</b>
                    </span>
                </p>
            </td>
            <td width="123" rowspan="4">
                <p>

                    <span>Bàn giao và hướng dẫn Admin

                    </span>

                </p>
            </td>
            <td width="236">
                <p>
                    <span>
                        Gửi tài liệu và hướng dẫn sử dụng quản trị website
                    </span>
                </p>
            </td>
            <td width="288">
                <p>
                    <span>
                        Tiếp nhận phần quản trị website (Landing Page)
                    </span>
                </p>
            </td>
            <td rowspan="4" style="text-align: center;">1</td>
        </tr>


        <tr>
            <td width="236">
                <p>
                    <span>
                        Hỗ trợ nhập liệu 
                    </span>
                </p>
            </td>
            <td width="288">
                <p>
                    <span>
                        Cung cấp hình ảnh và nội dung   
                    </span>
                </p>
            </td>
        </tr>
        <tr>
            <td width="236">
                <p>
                    <span>
                        Bàn giao sản phẩm, các thông số quản lý
                    </span>
                </p>
            </td>
            <td width="288">
                <p>
                    <span>
                        Nhận bàn giao sản phẩm, các thông số quản lý  
                    </span>
                </p>
            </td>
        </tr>


        <tr>
        </tr>

        <!-- Bảo hành website -->
        <tr>
            <td width="47">
                <p align="center">
                    <span>
                        <b>9</b>
                    </span>
                </p>
            </td>
            <td width="123">
                <p>

                    <span>
                        Bảo hành website
                    </span>

                </p>
            </td>
            <td width="236" valign="top">
                <p>
                    <span>Bảo hành các chức năng của website (Landing Page) (do Bên B thiết kế ra) trong vòng 02 tháng sau khi bàn giao.</span></br>
                    <span>-	Bảo hành 02 tháng với website (Landing Page) khi thực hiện lưu trữ tại đơn vị khác tính từ ngày nghiệm thu website
                    </span></br>
                    <span>-	Bảo hành trọn đời với website (Landing Page) thực hiện lưu trữ website tại Bên B</span>
                </p>
            </td>
            <td width="288">
            </td>
            <td></td>
        </tr>

        <tr>
            <td colspan="4" style="text-align: center;"><b>TỔNG CỘNG</b></td>
            <td style="text-align: center;"><b>10 ngày</b></td>
        </tr>

    </tbody>

</table>


<p>Ghi chú: Thời gian làm việc được tính không bao gồm ngày thứ bảy, chủ nhật, các ngày nghỉ lễ tại Việt Nam </p>
<p><u><strong>ĐIỀU 2: PHÍ DỊCH VỤ VÀ PHƯƠNG THỨC THANH TOÁN</strong></u></p>
<p><strong><u>2.1 Phí dịch vụ:</u></strong></p>
<p>Phí dịch vụ là khoản chi phí mà Bên A sẽ thanh toán cho Bên B theo hợp đồng này :

<?php
$this->table->clear();
$this->table->set_template(['table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">']);
$this->table->set_heading(array(['data'=>'Dịch vụ'],['data'=>'Chi phí(VNĐ)','style'=>'font-weight:bold','align'=>'center','width'=>'25%']));


$this->table->add_row('Thiết kế Landing page + Reponsive',
    array('data' => currency_numberformat($webdesign_price),'align' => 'center'));
/*$this->table->add_row('Thiết kế web (*)<br/><em>'.$term->term_name.'</em>',
$contract_days.' ngày (từ '.my_date($contract_begin,'d/m/Y').' - '.my_date($contract_end,'d/m/Y').')',
array('data' => currency_numberformat($webdesign_price),'align' => 'right'));*/

$total = $webdesign_price;

/* Danh sách chức năng nâng cao */
if( ! empty($advanced_functions) && is_array($advanced_functions))
{
    foreach ($advanced_functions as $_func)
    {
        if(empty($_func['value'])) continue;
        $this->table->add_row($_func['name'], ['data'=>currency_numberformat($_func['value']),'align'=>'center']);
    }
    $total+= $advanced_functions_price;
}

/* Giảm giá theo phần trăm (hợp đồng cũ) */
if(!empty($discount_percent))
{
    $_discount_amount = $webdesign_price * div($discount_percent,100);
    if($_discount_amount != 0 && $_discount_amount > 0)
    {
        $total-= $_discount_amount;
        $_discount_amount_f = currency_numberformat(-1*$_discount_amount);

        $this->table->add_row(['data'=>"Giảm giá {$discount_percent}&#37"],['data'=>$_discount_amount_f,'align'=>'center']);
    }
}

/* Giảm giá áp dụng theo các chương trình mới hiện hành */
if( ! empty($discount_amount))
{
    $total-= $discount_amount;
    $this->table->add_row(['data'=>"Giảm giá"], ['data'=>currency_numberformat(-1*$discount_amount),'align'=>'center']);
}

//$this->table->add_row(array('data'=>'Tổng cộng','colspan'=>2),array('data'=>currency_numberformat($total),'align'=>'right'));
$text_total    = ( empty($vat) ) ? '<b>Tổng cộng chi phí (bao gồm VAT 0%)</b>' : 'Tổng cộng' ;
$this->table->add_row(array('data'=> $text_total),array('data'=>currency_numberformat($total),'align'=>'center'));

if(!empty($vat))
{
    $tax = $total*div($vat, 100);
    $total = cal_vat($total,$vat);

/*$this->table
->add_row(array('data'=>'VAT','colspan'=>2),
array('data'=>'<strong>'.$vat.'&#37; ('.currency_numberformat($tax).')</strong>','align'=>'right'))
->add_row(array('data'=>'Thành tiền','colspan'=>2),
array('data'=>currency_numberformat($total),'align'=>'right')); */
$this->table
->add_row(array('data'=>'VAT'),
    array('data'=>'<strong>'.$vat.'&#37; ('.currency_numberformat($tax).')</strong>','align'=>'center'))
->add_row(array('data'=>'<b>Tổng cộng chi phí (bao gồm VAT '. $vat .'%)</b>'),
    array('data'=>currency_numberformat($total),'align'=>'center'));
}

$this->table->add_row(array('data'=> '<p><em>Bằng chữ: ' . ucfirst(mb_strtolower(convert_number_to_words($total))) .' đồng.</em></p>', 'colspan'=>2)) ;
echo $this->table->generate();