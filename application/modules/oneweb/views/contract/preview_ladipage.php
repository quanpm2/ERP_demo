<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<br/>
<p><strong>Dựa trên: </strong> Nhu cầu của Bên A và khả năng cung cấp của Bên B về dịch vụ. Hai bên thống nhất ký kết hợp đồng kinh tế với các điều khoản cụ thể như sau:</p>
<br/>
<p><u><strong>ĐIỀU 1: NỘI DUNG MUA BÁN</strong></u></p>

<p>
	<strong>1.1</strong> Bên B tiến hành cung cấp cho Bên A gói phần mềm như sau:
</p>
<p>Đơn vị tính giá: Việt Nam Đồng (VND). </p>
<p>Thanh toán bằng tiền đồng Việt Nam.</p>

<?php
$this->table->clear();

$this->table->set_template(['table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">']);
$this->table->set_heading(['Tên sản phẩm', 'Đơn vị tính', 'Số lượng', 'Đơn giá</br>(VNĐ)', 'Thành tiền</br>(VNĐ)']);

$packageName = "Phần mềm Landing page";
$contractDates = my_date($contract_begin, 'd/m/Y').' đến '.my_date($contract_end, 'd/m/Y');

$_row = array(
    $packageName,
    [ 'data' => 'Gói', 'align' => 'center'],
    [ 'data' =>  $years, 'align' => 'right'],
    [ 'data' => currency_numberformat($service_package_price, ''), 'align'=>'right'],
    [ 'data' => currency_numberformat($service_package_price*$years, ''), 'align' => 'right' ]
);

$this->table->add_row($_row);

/* Danh sách chức năng nâng cao */
if( ! empty($advanced_functions) && is_array($advanced_functions))
{
    foreach ($advanced_functions as $_func)
    {
        if(empty($_func['value'])) continue;
        $this->table->add_row(['data' => "Chức năng: {$_func['name']}", 'colspan' => 4], ['data'=>currency_numberformat($_func['value'], ''),'align'=>'right']);
    }
}



/* Giảm giá áp dụng theo các chương trình mới hiện hành */
if( ! empty($discount_amount))
{
    $this->load->config('oneweb/discount');
    $discountPlanEnums  = $this->config->item('packages', 'discount');
    $discountPlanName   = !empty($discountPlanEnums[$discount_plan]) ? $discountPlanEnums[$discount_plan]['label'] : 'Giảm giá';
    if('combo' == $discount_plan
        && $availSaleOpts   = array_filter($discountPlanEnums['combo']['sale'], function($opt) use ($years){ return $opt['buy'] < $years; }))
    {
        $validSaleOpt       = end($availSaleOpts);
        $discountPlanName   .= "<i> (Mua {$validSaleOpt['buy']} năm Tặng {$validSaleOpt['free']} năm)</i>";
    }

    $this->table->add_row(['data' => $discountPlanName, 'colspan' => 4], ['data' => currency_numberformat(-1*$discount_amount, ''), 'align' => 'right']);
}

$total      = $contract_value;
$text_total = ( empty($vat) ) ? '<b>Tổng cộng</b>' : 'Tổng cộng' ;
$this->table->add_row(array('data'=> 'Thuế giá trị gia tăng', 'colspan' => 4),array('data'=>'/', 'align'=>'right'));
$this->table->add_row(array('data'=> $text_total, 'colspan' => 4),array('data'=>currency_numberformat($total, ''), 'align'=>'right'));

$this->table->add_row(array('data'=> '<p><em>Bằng chữ: ' . ucfirst(mb_strtolower(convert_number_to_words($total))) .' đồng</em></p>', 'colspan'=>6)) ;

echo $this->table->generate();
?>

<br/>
<p><strong>1.2</strong> Thời hạn sử dụng bản quyền Phần Mềm tổng cộng 12 tháng được tính kể từ ngày bên B nhận được thanh toán của bên A.</p>

<p><strong>1.3</strong> Hai bên thống nhất nghiệm thu giao diện thông qua email hoặc biên bản nghiệm thu giao diện.</p>

<p><strong>1.4</strong>Trong vòng 02 ngày làm việc kể từ ngày Bên B hoàn thành thiết kế giao diện website và gửi email nghiệm thu cho bên A  mà Bên A không còn phản hồi gì xem như giao diện website được nghiệm thu.</p>

<p><strong>1.5</strong> Hình thức sử dụng Phần Mềm: Sử dụng trực tuyến trên nền tảng web tại website <a href="https://ladipage.vn/">https://ladipage.vn/</a>  - <b>CÔNG TY CỔ PHẦN CÔNG NGHỆ LADIPAGE VIỆT NAM.</b></p>

<p><strong>1.6</strong> Thời hạn lưu trữ dữ liệu của Bên A đã đăng tải trên Phần Mềm: Trong thời hạn của bản quyền được quy định tại Hợp đồng này và Phụ lục đính kèm Hợp đồng (nếu có). Bên B sẽ tự động xóa dữ liệu Bên A đã đăng tải trên Phần Mềm sau 01 (một) tháng kể từ ngày Hợp đồng này hết hiệu lực và không được mua thêm thời hạn bản quyền. Bên B không chịu bất kỳ trách nhiệm về dữ liệu đã xóa của Bên A đã đăng tải trong Phần Mềm.</p>

<p><strong>1.7</strong> Chuyển nhượng hoặc/và Chuyển giao Hợp đồng: Trong mọi trường hợp Bên A không được quyền chuyển nhượng hoặc/và chuyển giao một phần hoặc/và toàn bộ quyền và nghĩa vụ của mình theo quy định của Hợp đồng này cho bên thứ ba hoặc bên liên quan của mình ngoại trừ trường hợp có sự đồng ý của Bên B bằng văn bản.</p>

<br/>
<p><u><strong>ĐIỀU 2: GIÁ TRỊ HỢP ĐỒNG VÀ PHƯƠNG THỨC THANH TOÁN</strong></u></p>
<p><strong>2.1 Giá trị hợp đồng</strong></p>
<p>
    Giá trị Bên A phải thanh toán cho Bên B là: <?php echo currency_numberformat($total, 'VNĐ');?> (Số tiền bằng chữ: <?php echo  ucfirst(mb_strtolower(convert_number_to_words($total)));?>).
    <br/>
    Phí duy trì các năm tiếp theo được thanh toán trước ngày hết hạn (ngày hết hạn được quy định tại Điều 1 của Hợp đồng này). Chính sách gia hạn của Bên B được quy định cụ thể tại Điều 6 của Hợp đồng này.
</p>

<p><strong><u>2.2 Phương thức thanh toán:</u></strong></p>
<p> - Ngay sau khi hợp đồng đươc ký BÊN A thanh toán cho BÊN B 100% chi phí </p>
<p> - Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin tài khoản do Bên B cung cấp, cụ thể:</p>

<?php if( ! empty($bank_info)) :?>
<ul style="padding-left:0;list-style:none">
<?php foreach ($bank_info as $label => $text) :?>
    <?php if (is_array($text)) : ?>
        <?php foreach ($text as $key => $value) :?>
            <li>- <?php echo $key;?>: <?php echo $value;?></li>
        <?php endforeach;?>
    <?php continue; endif;?>
    <li>- <?php echo $label;?>: <?php echo $text;?></li>
<?php endforeach;?>
</ul>
<p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
<?php endif; ?>

<p><u><b><strong>ĐIỀU 3: QUYỀN VÀ NGHĨA VỤ CỦA BÊN A</strong></b></u></p>

<p><b>3.1.</b> Bên A chịu trách nhiệm về bản quyền và sở hữu trí tuệ đối với tất cả thông tin dữ liệu Bên A tải lên hệ thống của Bên B.</p>
<p><b>3.2.</b> Chịu trách nhiệm trước pháp luật nếu sử dụng website trái với quy định của nhà nước và tuân thủ các quy định về việc sử dụng dịch vụ tại Điều 7 của Hợp đồng này.</p>
<p><b>3.3.</b> Theo dõi và yêu cầu Bên B thực hiện đầy đủ và đúng thời hạn các cam kết của Hợp đồng. </p>
<p><b>3.4.</b> Thanh toán đầy đủ và đúng hạn các khoản chi phí cho Bên B như đã nêu tại Điều 1 của Hợp đồng này. Và chủ động thanh toán các khoản chi phí gia hạn theo Điều 6 của Hợp đồng này.</p>
<p><b>3.5.</b> Bên A tuân theo Chính sách sử dụng dịch vụ của Bên B. Bên B có thể cung cấp các ứng dụng, tính năng hoặc chức năng mới cho phần mềm, việc sử dụng có thể tùy thuộc vào sự đồng ý của Bên A đối với các điều khoản bổ sung. Ngoài ra, Bên B sẽ cung cấp các sản phẩm có kết nối hoặc hợp tác với một Bên thứ ba, Bên A phải tuân thủ quy định sử dụng dịch vụ của Bên thứ ba đưa ra.</p>
<p><b>3.6.</b> Có quyền đơn phương chấm dứt Hợp đồng trong các tình huống bất khả kháng hoặc trong các tình huống vi phạm Hợp đồng do lỗi của Bên B.</p>
<p><b>3.7.</b> Có quyền khiếu nại tới các cơ quan chức năng để đòi bồi thường và xử lý theo luật định nếu Bên B vi phạm Hợp đồng.</p>
<br/>

<p><u><b><strong>ĐIỀU 4: QUYỀN VÀ NGHĨA VỤ CỦA BÊN B</strong></b></u></p>

<p><b>4.1.</b> Bên B có trách nhiệm thông báo cho Bên A trước 10 ngày làm việc kể từ ngày hết hạn Hợp đồng căn cứ vào Điều 1.</p>
<p><b>4.2.</b> Tư vấn, hỗ trợ Bên A trong các trường hợp cấu hình và sử dụng sản phẩm. Bảo hành và sửa chữa nhanh chóng khi website gặp vấn đề hư hỏng trong thời hạn Hợp đồng tại Điều 1.</p>
<p><b>4.3.</b> Bên B có nghĩa vụ đảm bảo website của Bên A hoạt động liên tục trong thời gian thực hiện, trừ các trường hợp bất khả kháng.</p>
<p><b>4.4.</b> Bên B cam kết bảo mật các thông tin trên dữ liệu website của Bên A, tuyệt đối không cung cấp thông tin cho bất kỳ bên thứ ba nào. Trừ các trường hợp:</p>
<p><b>4.4.1</b> Có sự đồng ý hoặc yêu cầu từ Bên A hoặc Bên A đồng ý với điều khoản sử dụng dịch vụ của Bên thứ ba có liên kết với Bên B</p>
<p><b>4.4.2</b> Có yêu cầu từ các cơ quan pháp luật.</p>
<p><b>4.5.</b> Có quyền đơn phương chấm dứt Hợp đồng trong các tình huống bất khả kháng hoặc trong các tình huống vi phạm Hợp đồng do lỗi của Bên A.</p>
<p><b>4.6.</b> Có quyền khiếu nại tới các cơ quan chức năng để đòi bồi thường và xử lý theo luật định nếu Bên A vi phạm Hợp đồng.</p>

<br/>
<p><u><b><strong>ĐIỀU 5: BẤT KHẢ KHÁNG</strong></b></u></p>
<p><b>5.1.</b> Hai Bên sẽ được miễn trừ việc thực hiện các nghĩa vụ trong Hợp đồng này, nếu/và trong phạm vi của việc thực hiện nghĩa vụ này bị ngăn cản do Sự Kiện Bất Khả Kháng và bên bị ảnh hưởng đã thi hành mọi biện pháp cần thiết để khắc phục và đã ngay lập tức thông báo bằng văn bản hoặc email cho Bên kia biết về việc bị ảnh hưởng tới trách nhiệm thực hiện các nghĩa vụ theo Hợp đồng này.</p>
<p><b>5.2.</b> Nếu Sự Kiện Bất Khả Kháng kéo dài hơn ba mươi (30) Ngày làm việc, mỗi Bên đều có quyền chấm dứt Hợp đồng này mà không phải bồi thường cho Bên kia bất cứ thiệt hại trực tiếp nào phát sinh từ Sự Kiện Bất Khả Kháng.</p>

<p><b>5.3.</b>Hệ thống sử dụng nền tảng của bên thứ 3 là <b>CÔNG TY CỔ PHẦN CÔNG NGHỆ LADIPAGE VIỆT NAM</b>. Trong trường hợp bên thứ 3 có sự kiện bất khả kháng xảy ra, bên B có trách nhiệm thông báo cho bên A bằng văn bản và bên B không chịu trách nhiệm trong vấn đề giải quyết khắc phục.</p>

<p><b>5.4.</b> Các Bên sẽ được xem xét giảm trách nhiệm khi xảy ra Sự Kiện Bất Khả Kháng trong những trường hợp sau đây:</p>
<p><b>5.4.1</b> Việc vi phạm Hợp đồng của một Bên là nguyên nhân trực tiếp dẫn tới sự vi phạm Hợp đồng của Bên kia;</p>
<p><b>5.4.2</b> Các trường hợp khác theo quy định của pháp luật.</p>
<br/>

<p><u><b><strong>ĐIỀU 6: NÂNG CẤP VÀ GIA HẠN WEBSITE</strong></b></u></p>
<p><b>6.1.</b> Bên A có quyền nâng cấp gói phần mềm đang sử dụng lên gói dịch vụ cao hơn trong suốt quá trình sử dụng. </p>
<p><b>6.2.</b> Bên A đóng thêm cho Bên B khoản phí chênh lệch giữa hai gói phần mềm nếu có tính từ thời điểm nâng cấp phần mềm đến ngày hết hạn.</p>
<p><b>6.3.</b> Bên B có trách nhiệm tiến hành nâng cấp gói phần mềm lên cho Bên A sau khi nhận được yêu cầu và tiền thanh toán nếu có của Bên A.</p>
<br/>

<p><u><b><strong>ĐIỀU 7: GIẢI QUYẾT TRANH CHẤP HỢP ĐỒNG</strong></b></u></p>

<p><b>7.1.</b> Trong quá trình thực hiện Hợp đồng hai Bên cần chủ động thông báo cho nhau biết tiến độ thực hiện, nếu có vấn đề bất lợi phát sinh hoặc xảy ra tranh chấp, các Bên phải kịp thời thông báo bằng văn bản cho nhau biết và phải chủ động bàn bạc giải quyết trên cơ sở thương lượng, tôn trọng quyền và lợi ích hợp pháp của các Bên. Kết quả hòa giải, thương lượng sẽ được các bên lập thành Biên bản làm việc có xác nhận của đại diện các bên. Biên Bản này có giá trị như Phụ lục Hợp đồng.</p>
<p><b>7.2.</b> Gói phần mềm bị ngưng hoạt động do các lý do tấn công mạng, truy cập bất hợp pháp: 2 Bên sẽ rà soát đánh giá nguyên nhân, Bên gây ra nguyên nhân chịu trách nhiệm.</p>
<p><b>7.5.</b> Trường hợp không thể giải quyết bằng hòa giải, thương lượng, tranh chấp sẽ được đưa ra giải quyết tại tòa án nhân dân có thẩm quyền theo quy định của pháp luật. Quyết định của Tòa án là quyết định cuối cùng, buộc các Bên phải thực hiện. Bên có lỗi sẽ phải chịu mọi án phí và lệ phí liên quan</p>
<br/>

<p><u><b><strong>ĐIỀU 8: SỬA ĐỔI VÀ CHẤM DỨT HỢP ĐỒNG </strong></b></u></p>
<p><b>8.1.</b> Một bên muốn sửa đổi, bổ sung, chấm dứt Hợp đồng trước thời hạn hết hạn Hợp đồng, phải thông báo cho Bên còn lại bằng văn bản trước 30 ngày. Trong thời hạn 15 ngày kể từ ngày nhận được thông báo (hoặc ngày ghi nhận đã nhận được thông báo (qua chuyển phát, chuyển trực tiếp, hoặc hình thức tương tự), nếu Bên còn lại không có ý kiến trả lời bằng văn bản thì thông báo đó mặc nhiên được chấp nhận; </p>
<p><b>8.2.</b> Các trường hợp chấm dứt Hợp đồng:</p>
<p><b>8.2.1</b> Thỏa thuận chấm dứt Hợp đồng; </p>
<p><b>8.2.2</b> Hợp đồng hết hạn; </p>
<p><b>8.2.3</b> Một bên đơn phương chấm dứt Hợp đồng trong các trường hợp: 
<br/>
<b>(i) </b> Thông báo chấm dứt Hợp đồng tại Khoản 1 Điều này
<br/>
<b>(ii) </b> Trường hợp một Bên vi phạm Hợp đồng và đã được Bên kia yêu cầu khắc phục mà không khắc phục hoặc không thể khắc phục hoặc tiếp tục tái phạm, Bên bị vi phạm có quyền đơn phương chấm dứt Hợp đồng trước thời hạn và đòi bồi thường thiệt hại theo quy định Hợp đồng và quy định pháp luật liên quan. 
<br/>
<b>(iii) </b> Hợp đồng có thể bị hủy bỏ ngay lập tức bằng fax, email, điện thoại bởi một trong các Bên mà không cần báo trước nếu: Một trong các Bên còn lại bị khiếu nại hay bị khởi kiện theo luật phá sản hoặc không có khả năng thanh toán vì bất cứ lý do nào (không có khả năng thanh toán được hiểu là quá hạn thanh toán 30 ngày mà không thực hiện nghĩa vụ thanh toán). Trường hợp Bên A đã hoàn tất thanh toán gói dịch vụ cho Bên B thì thời gian kết thúc hợp đồng theo thời gian Bên A đã mua gói dịch vụ.
<br/>
<b>(iv) </b> Bên B có thể tạm đình chỉ hoặc đơn phương chấm dứt Hợp đồng mà không phải chịu bất kỳ thiệt hại nào phát sinh nếu Bên A vi phạm hoặc không thực hiện đúng nghĩa vụ của mình được quy định trong Hợp đồng hoặc Bên B nhận định hoạt động của Bên A là không an toàn và có các rủi ro trong thanh toán. 
<br/>
<b>(v) </b> Các trường hợp khác theo quy định của Hợp đồng này. </p>
<p><b>8.3.</b> Xử lý chấm dứt Hợp đồng:</p>
<p><b>8.3.1</b> Việc chấm dứt Hợp đồng này vì bất kỳ lý do gì và việc chấm dứt hoạt động kinh doanh của một Bên không có nghĩa là chấm dứt các nghĩa vụ mà Bên đó chưa hoàn thành theo quy định của Hợp đồng.</p>
<p><b>8.3.2</b> Trong bất kỳ trường hợp nào nêu trên, Hợp đồng được chấm dứt Bên B sẽ không hoàn lại phần tiền còn lại của gói phần mềm Bên A đã đăng ký sử dụng.</p>
<p><b>8.3.3</b> Khi chấm dứt hợp đồng Bên B không chịu trách nhiệm về việc đảm bảo lưu trữ dữ liệu trên gói phần mềm đăng ký của Bên A.</p>
<br/>

<p><u><b><strong>ĐIỀU 9: BẢO HÀNH VÀ BẢO TRÌ</strong></b></u></p>
<p><b>9.1.</b> Bảo hành website trong thời gian Bên A còn sử dụng gói phần mềm đăng ký của Bên B.</p>
<p><b>9.2.</b> Không bảo hành đối với các website sử dụng hosting bên ngoài. </p>
<br/>

<p><u><b><strong>ĐIỀU 10: ĐIỀU KHOẢN CHUNG</strong></b></u></p>
<p><b>10.1.</b> Hai Bên cam kết rằng mọi thông tin từ/bởi/có liên quan đến Hợp đồng này đều được coi là Thông Tin Bảo Mật. Theo đó, hai Bên cam kết bảo mật và không tiết lộ thông tin về việc thỏa thuận ký kết hợp đồng cho Bên thứ ba bất kỳ nào trừ trường hợp theo yêu cầu của cơ quan có thẩm quyền. Điều khoản này sẽ giữ nguyên giá trị hiệu lực 6 (sáu) tháng kể từ thời điểm Hợp đồng này được chấm dứt.</p>
<p><b>10.2.</b> Hai bên đồng ý rằng (các) điều khoản trong Hợp đồng này có giá trị độc lập với nhau trừ trường hợp rõ ràng được dẫn chiếu một cách chính xác và đầy đủ. Việc một hoặc một vài điều khoản trong Hợp đồng này bị vô hiệu sẽ không làm ảnh hưởng đến việc thực thi các điều khoản khác trong Hợp đồng. Trường hợp có bất kỳ điều khoản nào bị vô hiệu thì hai bên cam kết bằng tất cả nỗ lực của mình điều chỉnh sao cho phù hợp với các quy định của pháp luật để thực thi.</p>
<p><b>10.3.</b> Những vấn đề chưa được quy định trong Hợp đồng này sẽ được hai Bên thống nhất áp dụng quy định của Pháp luật của Nước CHXHCN Việt Nam.</p>
<p><b>10.4.</b> Hợp đồng có giá trị thời hạn theo Điều 1 kể từ ngày ký. Trường hợp khi Hợp đồng này hết hạn mà Bên A gia hạn gói dịch vụ landing page của Bên B thì Hợp đồng sẽ được tự động gia hạn thêm đúng bằng số tháng mà Bên A thực hiện gia hạn.</p>
<p><b>10.5.</b> Bất kỳ sự chỉnh sửa và/hoặc bổ sung nào trong Hợp đồng này chỉ có hiệu lực khi được lập thành văn bản (bằng Phụ lục Hợp đồng) và được cả hai Bên ký kết, các Phụ lục Hợp đồng này là phần không thể tách rời của Hợp đồng.</p>
<p><b>10.6.</b> Hợp đồng được lập thành hai (02) bản, Bên A giữ một (01) bản, Bên B giữ (01) bản, cùng có giá trị pháp lý như nhau.</p>
