<?php
$rows_id = array();
$data = array();
$start_month = $this->mdate->startOfMonth();

$this->table->set_heading(array('STT','Kỹ thuật thực hiện','Hành động'));
$this->table->set_caption('Kỹ thuật thực hiện');

$kpis = [];
empty($targets['tech']) OR $kpis = array_merge($kpis, $targets['tech']);
empty($targets['design']) OR $kpis = array_merge($kpis, $targets['design']);

if( ! empty($kpis))
{
	foreach ($kpis as $key => $kpi) {

		$btn_delete = anchor(module_url('kpi/'.$term_id.'/'.$kpi['kpi_id'].'/delete'), 'Xóa', 'class="btn btn-danger btn-flat btn-xs"');
		$username = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');
		$target_type = 'none';

		switch ($kpi['kpi_type']) {

			case 'tech':
				$target_type = 'Kỹ thuật WEBSITE (Front-end)';
				break;

			case 'design':
				$target_type = 'Thiết kế (Design)';
				break;
		}

		$this->table->add_row(($key+1),$username, $target_type , $btn_delete);
	}
}

echo $this->table->generate();

echo $this->admin_form->form_open();
echo $this->admin_form->dropdown('Nhân viên thực hiện', 'user_id', $users, '');
echo $this->admin_form->hidden('','target_date',time());
echo $this->admin_form->dropdown('Phụ trách', 'target_type', ['tech' => 'Kỹ thuật WEBSITE (Front-end)', 'design' => 'Thiết kế (DESIGN)'], '');
echo $this->admin_form->hidden('','target_post','1');
echo $this->admin_form->submit('submit_kpi_tech','Lưu lại');
echo $this->admin_form->form_close();
?>