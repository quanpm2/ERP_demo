      <script type="text/javascript">
       function count_msg(term_id) {
           $.ajax({
                 url       : '<?php echo base_url() . 'message/ajax_note_contract/count' ;?>',
                 type      : 'post',            
                 dataType  : 'json' ,
                 data      : { term_id : term_id } ,          
                 success   : function(data, status) {
                     if(data.count_msg > 0) {  
                         $('#count-msg').html('<small class="label bg-green" class="badge bg-green">' + data.count_msg + '</small>') ;
                     }
                 }
           }) ;
       }  
       
       function auto_sroll() {
          var height = 0;
          $('#load-direct-chat-messages div.direct-chat-msg').each(function(i, value){
              height += parseInt($(this).height());
          });

          height += '';

          $('#load-direct-chat-messages').animate({scrollTop: height});
       }

       function alert_permissions(message) {
           $('#alert-permissions').empty() ;
           $('#alert-permissions').css('display', 'block') ;
           box_message =  '<p class="bg-green" style="padding:10px">' + message + '</p>' ;
           $('#alert-permissions').append(box_message).hide(7000) ;
        }

       // Send Message  : add, edit, update
       function send_message(term_id, msg_id = '', msg_content = '', action = 'add') {
           $.ajax( {
                     url       : '<?php echo base_url() . 'message/ajax_note_contract/send' ;?>',
                     type      : 'post' ,
                     data      : { msg_id : msg_id , term_id : term_id , msg_content : msg_content, action : action } ,
                     dataType  : 'json' ,
                     success   : function(data) {           
                         if(action == 'update')  {
                            //console.log(data.after) ; 
                            if(data.status == false) alert_permissions(data.alert) ;  
                            if(data.status == true) {                           
                               //before     = data.before.data ;
                               after      = data.after.data ;
                               
                               //before.replace(before, after) ;
                               $('#msg-' + data.msg_id).addClass('update-before-' + data.msg_id).after(after) ;
                               $('.update-before-' + data.msg_id).remove() ;
                               init_note_delete();
                               init_note_update_enter();
                            }
                         }
                         else if(action == 'add') {
                              if(data.status == false) alert_permissions(data.alert) ;  
                              if(data.status == true) { 
                                $('#load-direct-chat-messages').append(data.data) ;
                                    init_note_delete();
                                    init_note_update_enter();
                                    auto_sroll() ;                            
                              }
                         }
                         else if(action == 'delete') {
                              if(data.status == false) alert_permissions(data.alert) ;   
                              if(data.status == true) {
                                  $('#msg-' + data.msg_id ).hide() ;
                                  init_note_delete();
                                  init_note_update_enter();
                              }
                              return false ;
                         }                       
                     }               
           }).done(function() {
               // Đếm số lượng ghi chú   
               $('#count-msg').empty() ;
               count_msg(term_id) ;      
               $('#msg_content').val('') ; 
           }) ;
       }

       function init_note_update_enter()
        {
          $('.icon-edit-update a.update').on('click', function(event) {     
               term_id      = $(this).data('term-id')   ;
               msg_id       = $(this).data('update-id') ;
           
               //$('#msg_content').val($('#msg-' + msg_id + ' .msg-content').text()) ;
           
               // Update
               $('#frm-note-contract').removeClass('action-insert').addClass('action-edit') ;
               $('#frm-note-contract :text[name="msg_content"]').removeAttr('data-action');

               $('#msg_content_update').val($('#msg-' + msg_id + ' .msg-content').text()) ;
               $('#msg_content_update').css('display', 'block') ;
               $('#msg_content').css('display', 'none') ;
                // Enter tự động kích hoạt button send : update
               $('.action-edit #msg_content_update').keypress(function(event){
                  var keycode = event.keyCode;
                  if (keycode == '13') {
                       msg_content  = $('#msg_content_update').val() ;

                       if(msg_content == '') return false ;

                       send_message(term_id, msg_id, msg_content, 'update') ; 
                       $('#msg_content_update').css('display', 'none') ;
                       $('#msg_content').val('') ;
                       $('#msg_content').css('display', 'block').focus() ;
                  }
               });          
               return false ; 
           }) ;
           return false ;
        }

       function init_note_delete() { 
          $('.icon-edit-update a.delete').on('click', function(event) {
                 term_id      = $(this).data('term-id')   ;
                 msg_id       = $(this).data('delete-id') ;
                 send_message(term_id, msg_id, '', 'delete') ;   
              }) ; 
       }  

       function init_note_add_enter(term_id, msg_content = '') {
           var action = $('#frm-note-contract #msg_content').data('action') ;
           if(action == 'add') {
              $('#msg_content').keypress(function(event){
                  var keycode = event.keyCode;
                  if (keycode == '13') {
                      msg_content  = $('#msg_content').val() ; 

                      if(msg_content == '') return false ;

                      send_message(term_id, '' , msg_content, 'add') ;   
                      msg_content  = $('#msg_content').val('') ;
                      $('#msg_content').focus() ;                    
                  }
              });
           }
       }

      $(document).ready(function() {
           
                 // Load ghi chú theo hợp đồng
                 var term_id      = <?php echo $term_id ?> ;
                 count_msg(term_id) ;

                 auto_sroll() ;
                 $('#msg_content').focus() ; 

                 init_note_add_enter(term_id, $('#msg_content').val()) ;

                 init_note_update_enter();                 
                 init_note_delete();        
      }) ;

        
    </script>

    <style type="text/css">
       #count-msg { 
            margin-right: 10px;
       }

       .icon-edit-update a { 
          color: #fff;
       }

       .msg-default .icon-edit-update a { 
          color: #000 !important;
       }

    </style>

    <div id="modal-note-contract" style="width: 80%; margin: 0px auto"  >
       <!-- DIRECT CHAT -->      
       <div class="box box-success direct-chat direct-chat-success action-add">
          <div id="alert-permissions"></div>
          <div class="box-header with-border">
             <h3 class="box-title">Ghi chú cho hợp đồng</h3>
             <div class="box-tools pull-right">
                <div id="count-msg pull-left"></div>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>
                </button>
             </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
             <!-- Conversations are loaded here -->
             <div class="direct-chat-messages" id="load-direct-chat-messages">
                <?php if(!empty($direct_chat_msg)) {
                   foreach ($direct_chat_msg as $note) {
                     if(@$note->user_id == $this->admin_m->id) { ?>
                <!-- Message. Default to the left -->
                <div class="direct-chat-msg msg-default" id="msg-<?php echo $note->msg_id ;?>">
                   <div class="direct-chat-info clearfix">
                      <span class="direct-chat-name pull-left"><?php echo $this->admin_m->get_field_by_id(@$note->user_id,"display_name") ;?></span>
                      <span class="direct-chat-timestamp pull-right"><?php echo date('D h:i A, d-m-Y', $note->created_on) ;?></span>
                   </div>
                   <!-- /.direct-chat-info -->
                   <i class="direct-chat-img fa fa-user-o" aria-hidden="true"></i><!-- /.direct-chat-img -->
                   <div class="direct-chat-text">
                      <?php if(@$note->user_id == $this->admin_m->id) { ?>
                        <ul class="list-inline icon-edit-update pull-right">
                           <li class="icon-edit"><a href="#update" class="update" title="Sửa" data-update-id="<?php echo $note->msg_id ;?>" data-term-id="<?php echo $note->term_id ;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                           <li class="icon-delete"><a href="#delete" class="delete" title="Xoá" data-delete-id="<?php echo $note->msg_id ;?>" data-term-id="<?php echo $note->term_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                        </ul>
                      <?php } ?>  
                      <div class="msg-content"><?php echo $note->msg_content ;?></div>
                   </div>
                   <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg -->
                <?php } else { ?>
                <div class="direct-chat-msg right" id="msg-<?php echo @$note->msg_id ;?>">
                   <div class="direct-chat-info clearfix">
                      <span class="direct-chat-name pull-right"><?php echo $this->admin_m->get_field_by_id(@$note->user_id, "display_name") ;?></span>
                      <span class="direct-chat-timestamp pull-left"><?php echo date('D h:i A, d-m-Y', $note->created_on) ;?></span>                       
                   </div>
                   <!-- /.direct-chat-info -->
                   <i class="direct-chat-img fa fa-user-circle" aria-hidden="true"></i><!-- /.direct-chat-img -->
                   <div class="direct-chat-text">
                      <?php if(@$note->user_id == $this->admin_m->id) { ?>
                        <ul class="list-inline icon-edit-update pull-right">
                           <li class="icon-edit"><a href="#update" class="update" title="Sửa" data-update-id="<?php echo $note->msg_id ;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                           <li class="icon-delete"><a href="#delete" class="delete" title="Xoá" data-delete-id="<?php echo $note->msg_id ;?>" data-term-id="<?php echo $note->term_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                        </ul>
                      <?php } ?>  
                      <div class="msg-content"><?php echo $note->msg_content ;?></div>
                   </div>
                   <!-- /.direct-chat-text -->
                </div>
                <?php } 
                   }
                   }
                   ?>
             </div>
             <!--/.direct-chat-messages-->
             <!-- /.direct-chat-pane -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
                  <div id="frm-note-contract" method="post" class="action-insert">
                    <input type="text" name="msg_content" id="msg_content" placeholder="Nhập ghi chú và enter..." class="form-control" data-action="add">
                    <input type="text" name="msg_content" id="msg_content_update" placeholder="Nhập ghi chú và enter..." class="form-control" data-action="update" style="display: none">
                  </div>
          </div>
          <!-- /.box-footer-->
       </div>
    </div>
