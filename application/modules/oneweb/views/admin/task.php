<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$this->template->stylesheet->add('plugins/fullcalendar/fullcalendar.min.css');
$this->template->stylesheet->add('plugins/fullcalendar/fullcalendar.print.css','media="print"');

$this->template->javascript->add('plugins/input-mask/jquery.inputmask.js');
$this->template->javascript->add('vendors/vuejs/vue.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');
$this->template->javascript->add('plugins/datepicker/locales/bootstrap-datepicker.vi.js');
$this->template->javascript->add('plugins/fullcalendar/fullcalendar.min.js');

array_map(function($x){ $this->template->footer_javascript->add($x); }, array(
	$this->template->trigger_javascript('modules/calender/js/calender.js'),
	$this->template->trigger_javascript('modules/component/form.js'),
	$this->template->trigger_javascript('modules/oneweb/js/oneweb-tasks-calendar-create-update.js'),
	$this->template->trigger_javascript('modules/oneweb/js/oneweb-tasks-calendar.js'),
	$this->template->trigger_javascript('modules/oneweb/js/oneweb-tasks-grid.js'),
	$this->template->trigger_javascript('modules/oneweb/js/oneweb-tasks-page.js'),
	'<script type="text/javascript"> var app_root = new Vue({el: "#app-container"}); </script>'
));
?>
<oneweb-tasks-page :contract_id="<?php echo $term_id;?>"/></oneweb-tasks-page>