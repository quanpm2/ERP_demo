<?php
class Reporting_Package extends Package
{
    public $website_id = null;

	function __construct()
	{
		parent::__construct();
		$this->website_id  = $this->uri->segment(4);
	}

	/**
	 * A private function to load the menu.
	 *
	 * @return boolean Returns FALSE if the user does not have the 'Admin.Reporting' permission.
	 */
	public function name()
	{
		return 'Reporting';
	}

	/**
	 * A private function to load the menu.
	 *
	 * @return boolean Returns FALSE if the user does not have the 'Admin.Reporting' permission.
	 */
	public function init()
	{
		$this->_load_menu();
	}

	/**
	 * A private function to load the menu.
	 *
	 * @return boolean Returns FALSE if the user does not have the 'Admin.Reporting' permission.
	 */
	private function _load_menu()
	{
		if(!has_permission('Admin.Reporting')) return FALSE;
	}

	/**
	 * A description of the entire PHP function.
	 *
	 * @throws Some_Exception_Class description of exception
	 * @return Some_Return_Value
	 */
	public function title()
	{
		return 'Reporting';
	}

	/**
	 * Retrieves the author of the function.
	 *
	 * @return string The name of the author.
	 */
	public function author()
	{
		return 'Tanhn';
	}

	/**
	 * Retrieves the version of the software.
	 *
	 * @return string The version number of the software.
	 */
	public function version()
	{
		return '1.0';
	}

	/**
	 * Get the description of the entire PHP function.
	 *
	 * @return string The description of the function.
	 */
	public function description()
	{
		return 'Module tổng hợp các báo cáo';
	}
	
	/**
	 * Initializes the permissions for the class.
	 *
	 * @return array The permissions array.
	 */
	private function init_permissions()
	{
		$permissions = array(
			'reporting.revenue' => array(
				'description' => 'Báo cáo doanh thu',
				'actions' => array('access', 'manage', 'mdeparment', 'mgroup')
            ),
        );

		return $permissions;
	}
    
    /**
     * Updates the permissions in the system.
     *
     * @return bool False if permissions are empty.
     */
    private function _update_permissions()
	{
		$permissions = $this->init_permissions();
		if(empty($permissions)) return false;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

    /**
	 * Install module
	 *
	 * @return     bool  status of command
	 */
	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	/**
	 * Uninstall module command
	 *
	 * @return     bool  status of command
	 */
	public function uninstall()
	{
		$permissions = $this->init_permissions();

		if(!$permissions) return FALSE;

		if($pers = $this->permission_m->like('name','reporting')->get_many_by())
		{
			foreach($pers as $per)
			{
				$this->permission_m->delete_by_name($per->name);
			}
		}

		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}

		return TRUE;
	}
}