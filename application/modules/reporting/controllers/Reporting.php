<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporting extends Admin_Controller 
{
	public function __construct(){

		parent::__construct();

		$models = [
			'history_m',
			'term_users_m',
			'staffs/staffs_m',
			'customer/customer_m',
		];

		$this->load->model($models);
	}
}
/* End of file reporting.php */
/* Location: ./application/modules/reporting/controllers/reporting.php */