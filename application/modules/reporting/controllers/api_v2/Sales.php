<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends MREST_Controller
{
	/**
     * ENDPOINT: /api-v2/reporting/sales/commissions
     * 
     * Lấy số tiền hoa hồng theo doanh thu cho nhân viên kinh doanh cụ thể trong khoảng thời gian được chỉ định.
     */
    public function commissions_get()
    {
    	$default_inputs = [
            'sale_id' => 0,
            'year' => my_date(time(), 'Y'),
            'month' => my_date(time(), 'm'),
            'day' => my_date(time(), 'd'),
        ];
        $inputs = wp_parse_args(parent::get(), $default_inputs);

        $data = [
            'value' => 0,
            'samePeriodComparison' => [
                'value' => 0,
                'changeRatioPercent' => 0,
            ],
        ];
        parent::responseHandler($data, 'Lấy dữ liệu thành công', true, 0, $inputs);
    }

    /**
     * ENDPOINT: /api-v2/reporting/sales/new_renewal_counts
     * 
     * fact_customer_resource
     * 
     * Lấy số lượng hợp đồng mới và tái ký mà nhân viên kinh doanh cụ thể đã tạo trong khoảng thời gian được chỉ định.
     */
    public function new_renewal_counts_get()
    {
    	$default_inputs = [
            'sale_id' => 0,
            'year' => (int) my_date(time(), 'Y'),
            'month' => (int) my_date(time(), 'm'),
            'day' => (int) my_date(time(), 'd'),
        ];
        $inputs = wp_parse_args(parent::get(), $default_inputs);

        // Validate inputs
        $this->load->library('form_validation');
        $this->form_validation->set_data($inputs);
        $this->form_validation->set_lang('vietnamese');
        $this->form_validation->set_rules('sale_id', 'sale_id', 'required|integer|greater_than[0]');
        $this->form_validation->set_rules('year', 'year', 'integer');
        $this->form_validation->set_rules('month', 'month', 'integer');
        $this->form_validation->set_rules('day', 'day', 'integer');

        if( FALSE === $this->form_validation->run())
        {
            $errors = [
                'errors' => $this->form_validation->error_array()
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        $roles_id = $this->option_m->get_value('group_sales_ids', TRUE);
        $has_staff = $this->admin_m->set_get_active()->set_role($roles_id)
            ->where('user_id', $inputs['sale_id'])
            ->count_by() > 0;
        if(!$has_staff)
        {
            $errors = [
                'sale_id' => 'Nhân viên kinh doanh không tồn tại!'
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        // Prepare input
        $dimension = 'day';
        if(empty($inputs['day']))
        {
            $dimension = 'month';

            $inputs['day'] = 1;
        }
        if(empty($inputs['month']))
        {
            $dimension = 'year';

            $inputs['day'] = 1;
            $inputs['month'] = 1;
        }

        $selected_time = strtotime($inputs['year'] . '-' . $inputs['month'] . '-' . $inputs['day'] . ' 00:00:00');
        $selected_date = my_date($selected_time, 'Y-m-d 00:00:00');

        $previous_time = strtotime("-1 {$dimension}", $selected_time);
        $previous_date = my_date($previous_time, 'Y-m-d 00:00:00');
        $previous_day = (int) my_date($previous_time, 'd');
        $previous_month = (int) my_date($previous_time, 'm');
        $previous_year = (int) my_date($previous_time, 'Y');

        $day = array_unique([(int) $inputs['day'], $previous_day]);
        $month = array_unique([(int) $inputs['month'], $previous_month]);
        $year = array_unique([(int) $inputs['year'], $previous_year]);

        // Build query
        $this->load->model('reporting/fact_customer_resource_metrics_m');
        $query_builder = $this->fact_customer_resource_metrics_m
            ->join('dim_time', 'dim_time.id = fact_customer_resource_metrics.time_id')
            ->where('fact_customer_resource_metrics.user_id', $inputs['sale_id']);

        switch($dimension)
        {
            case 'year':
                $query_builder = $query_builder->where_in('dim_time.year', $year)
                    ->where('dim_time.quarter', 0)
                    ->where('dim_time.month', 0)
                    ->where('dim_time.week', 0)
                    ->where('dim_time.day', 0)
                    ->order_by('dim_time.year', 'DESC');
                break;

            case 'month':
                $query_builder = $query_builder->where('dim_time.year', $inputs['year'])
                    ->where('dim_time.quarter !=', 0)
                    ->where('dim_time.week', 0)
                    ->where('dim_time.day', 0)
                    ->where('dim_time.date >=', $previous_date)
                    ->where('dim_time.date <=', $selected_date)
                    ->order_by('dim_time.year', 'DESC')
                    ->order_by('dim_time.month', 'DESC');

                if(count($month) > 1)
                {
                    $query_builder = $query_builder->where_in('dim_time.month', $month);

                    break;
                }

                $query_builder = $query_builder->where('dim_time.month', $month);
                
                break;

            default:
                $query_builder = $query_builder->where('dim_time.year', $inputs['year'])
                    ->where('dim_time.quarter !=', 0)
                    ->where('dim_time.month =', $inputs['month'])
                    ->where('dim_time.week !=', 0)
                    ->where('dim_time.date >=', $previous_date)
                    ->where('dim_time.date <=', $selected_date)
                    ->order_by('dim_time.year', 'DESC')
                    ->order_by('dim_time.month', 'DESC')
                    ->order_by('dim_time.day', 'DESC');

                if(count($day) > 1)
                {
                    $query_builder = $query_builder->where_in('dim_time.day', $day);

                    break;
                }

                $query_builder = $query_builder->where('dim_time.day', $day);

                break;
        }

        $customer_resource_metrics = $query_builder
            ->select('dim_time.day')
            ->select('dim_time.month')
            ->select('dim_time.year')
            ->select('fact_customer_resource_metrics.type')
            ->select('fact_customer_resource_metrics.number_of_contracts')
            ->as_array()
            ->get_all();
        if(empty($customer_resource_metrics))
        {
            parent::responseHandler([], 'Không tìm thấy dữ liệu theo yêu cầu', true, 204);
        }

        // Compute response
        $data = [
            'new' => 0,
            'renewal' => 0,
            'samePeriodComparison' => [
                'new' => [
                    'value' => 0,
                    'changeRatioPercent' => 0,
                ],
                'renewal' => [
                    'value' => 0,
                    'changeRatioPercent' => 0,
                ],
            ]
        ];

        $current_value = 1;
        $previous_value = 1;
        switch($dimension)
        {
            case 'year':
                $current_value = $inputs['year'];
                $previous_value = $previous_year;

                break;

            case 'month':
                $current_value = $inputs['month'];
                $previous_value = $previous_month;

                break;

            default:
                $current_value = $inputs['day'];
                $previous_value = $previous_day;

                break;
        }

        $this->load->config('reporting/customer_resource_metrics');
        $customer_resource_type = $this->config->item('type');
        
        $customer_resource_metrics_group_by_dimension = array_group_by($customer_resource_metrics, $dimension);

        $current = $customer_resource_metrics_group_by_dimension[$current_value] ?? [];
        if(!empty($current))
        {
            $reducing = array_reduce($current, function($result, $item) use ($customer_resource_type) {
                if(in_array($item['type'], [
                    $customer_resource_type['NEW_FIND_BY_SELF'],
                    $customer_resource_type['NEW_MARKETING'],
                ]))
                {
                    $result['new'] += (int) $item['number_of_contracts'];
                    
                    return $result;
                }

                if(in_array($item['type'], [
                    $customer_resource_type['RENEWAL_FIND_BY_SELF'],
                    $customer_resource_type['RENEWAL_MARKETING'],
                ]))
                {
                    $result['renewal'] += (int) $item['number_of_contracts'];
                    
                    return $result;
                }
                
                return $result;
            }, [
                'new' => 0,
                'renewal' => 0,
            ]);

            $data['new'] = $reducing['new'];
            $data['renewal'] = $reducing['renewal'];
        }

        $previous = $customer_resource_metrics_group_by_dimension[$previous_value] ?? [];
        $reducing = array_reduce($previous, function($result, $item) use ($customer_resource_type) {
            if(in_array($item['type'], [
                $customer_resource_type['NEW_FIND_BY_SELF'],
                $customer_resource_type['NEW_MARKETING'],
            ]))
            {
                $result['new'] += (int) $item['number_of_contracts'];
                
                return $result;
            }

            if(in_array($item['type'], [
                    $customer_resource_type['RENEWAL_FIND_BY_SELF'],
                    $customer_resource_type['RENEWAL_MARKETING'],
                ]))
            {
                $result['renewal'] += (int) $item['number_of_contracts'];
                
                return $result;
            }
            
            return $result;
        }, [
            'new' => 0,
            'renewal' => 0,
        ]);

        $new_change_ratio_percent = (float) round(div(($data['new'] - $reducing['new']), $reducing['new']) * 100, 2);
        $data['samePeriodComparison']['new']['value'] = (int) $reducing['new'];
        $data['samePeriodComparison']['new']['changeRatioPercent'] = $new_change_ratio_percent;

        $renewal_change_ratio_percent = (float) round(div($data['renewal'] - $reducing['renewal'], $reducing['renewal']) * 100, 2);
        $data['samePeriodComparison']['renewal']['value'] = (int) $reducing['renewal'];
        $data['samePeriodComparison']['renewal']['changeRatioPercent'] = $renewal_change_ratio_percent;

        parent::responseHandler($data, 'Lấy dữ liệu thành công', true, 0, $inputs);
    }

    /**
     * ENDPOINT: /api-v2/reporting/sales/rankings
     * 
     * Lấy số tiền hoa hồng theo doanh thu cho nhân viên kinh doanh cụ thể trong khoảng thời gian được chỉ định.
     */
    public function rankings_get()
    {
    	$default_inputs = [
            'sale_id' => 0,
            'year' => my_date(time(), 'Y'),
            'month' => my_date(time(), 'm'),
            'day' => my_date(time(), 'd'),
            'criteria' => 'revenue',
        ];
        $inputs = wp_parse_args(parent::get(), $default_inputs);

        // Validate inputs
        $this->load->library('form_validation');
        $this->form_validation->set_data($inputs);
        $this->form_validation->set_lang('vietnamese');
        $this->form_validation->set_rules('sale_id', 'sale_id', 'required|integer|greater_than[0]');
        $this->form_validation->set_rules('year', 'year', 'integer');
        $this->form_validation->set_rules('month', 'month', 'integer');
        $this->form_validation->set_rules('day', 'day', 'integer');
        $this->form_validation->set_rules('criteria', 'criteria', 'required');

        if( FALSE === $this->form_validation->run())
        {
            $errors = [
                'errors' => $this->form_validation->error_array()
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        $allow_criteria = [
            'revenue',
        ];
        $criteria = explode(',', $inputs['criteria']);
        $not_allow_criteria = array_filter($criteria, function($metric) use ($allow_criteria) { return !in_array($metric, $allow_criteria);});
        if(!empty($not_allow_criteria))
        {
            $not_allow_criteria = implode(', ', $not_allow_criteria);
            $errors = [
                'errors' => [
                    'criteria' => 'Trường criteria chứa dữ liệu không hợp lệ . (' . $not_allow_criteria . ')'
                ]
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        $roles_id = $this->option_m->get_value('group_sales_ids', TRUE);
        $has_staff = $this->admin_m->set_get_active()->set_role($roles_id)
            ->where('user_id', $inputs['sale_id'])
            ->count_by() > 0;
        if(!$has_staff)
        {
            $errors = [
                'sale_id' => 'Nhân viên kinh doanh không tồn tại!'
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        // Prepare input
        $dimension = 'day';
        if(empty($inputs['day']))
        {
            $dimension = 'month';

            $inputs['day'] = 1;
        }
        if(empty($inputs['month']))
        {
            $dimension = 'year';

            $inputs['day'] = 1;
            $inputs['month'] = 1;
        }

        $selected_time = $inputs['year'] . '-' . str_pad($inputs['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($inputs['day'], 2, '0', STR_PAD_LEFT) . ' 00:00:00';
        $selected_time = strtotime($selected_time);
        $selected_date = my_date($selected_time, 'Y-m-d 00:00:00');

        $previous_time = strtotime("-1 {$dimension}", $selected_time);
        $previous_date = my_date($previous_time, 'Y-m-d 00:00:00');
        $previous_day = (int) my_date($previous_time, 'd');
        $previous_month = (int) my_date($previous_time, 'm');
        $previous_year = (int) my_date($previous_time, 'Y');

        $day = array_unique([(int) $inputs['day'], $previous_day]);
        $month = array_unique([(int) $inputs['month'], $previous_month]);
        $year = array_unique([(int) $inputs['year'], $previous_year]);

        // Build query
        $this->load->model('reporting/fact_sales_ranking_m');
        $query_builder = $this->fact_sales_ranking_m
            ->join('dim_time', 'dim_time.id = fact_sales_ranking.time_id')
            ->where('fact_sales_ranking.user_id', $inputs['sale_id']);

        switch($dimension)
        {
            case 'year':
                $query_builder = $query_builder->where_in('dim_time.year', $year)
                    ->where('dim_time.quarter', 0)
                    ->where('dim_time.month', 0)
                    ->where('dim_time.week', 0)
                    ->where('dim_time.day', 0)
                    ->order_by('dim_time.year', 'DESC');
                break;

            case 'month':
                $query_builder = $query_builder->where('dim_time.year', $inputs['year'])
                    ->where('dim_time.quarter !=', 0)
                    ->where('dim_time.week', 0)
                    ->where('dim_time.day', 0)
                    ->where('dim_time.date >=', $previous_date)
                    ->where('dim_time.date <=', $selected_date)
                    ->order_by('dim_time.year', 'DESC')
                    ->order_by('dim_time.month', 'DESC');

                if(count($month) > 1)
                {
                    $query_builder = $query_builder->where_in('dim_time.month', $month);

                    break;
                }

                $query_builder = $query_builder->where('dim_time.month', $month);
                
                break;

            default:
                $query_builder = $query_builder->where('dim_time.year', $inputs['year'])
                    ->where('dim_time.quarter !=', 0)
                    ->where('dim_time.month =', $inputs['month'])
                    ->where('dim_time.week !=', 0)
                    ->where('dim_time.date >=', $previous_date)
                    ->where('dim_time.date <=', $selected_date)
                    ->order_by('dim_time.year', 'DESC')
                    ->order_by('dim_time.month', 'DESC')
                    ->order_by('dim_time.day', 'DESC');

                if(count($day) > 1)
                {
                    $query_builder = $query_builder->where_in('dim_time.day', $day);

                    break;
                }

                $query_builder = $query_builder->where('dim_time.day', $day);

                break;
        }

        $sale_ranking = $query_builder
            ->select('dim_time.day')
            ->select('dim_time.month')
            ->select('dim_time.year')
            ->select('fact_sales_ranking.rank')
            ->select('fact_sales_ranking.total_sales')
            ->as_array()
            ->get_all();
        if(empty($sale_ranking))
        {
            parent::responseHandler([], 'Không tìm thấy dữ liệu theo yêu cầu', true, 204);
        }

        $data = [
            'value' => 0,
            'samePeriodComparison' => [
                'value' => 0,
                'changeRank' => 0,
            ]
        ];

        $sale_ranking_group_by_dimension = array_group_by($sale_ranking, $dimension);
        $current_rank = $sale_ranking_group_by_dimension[$inputs[$dimension]] ?? [];
        $current_rank = reset($current_rank) ?? [];
        $data['value'] = (int) ($current_rank['rank'] ?? 0);

        $previous_dimension = 0;
        eval("\$previous_dimension = \$previous_{$dimension};");
        $previous_rank = $sale_ranking_group_by_dimension[$previous_dimension] ?? [];
        $previous_rank = reset($previous_rank) ?? [];
        if(empty($previous_rank))
        {
            $data['samePeriodComparison']['value'] = 0;
        }
        else 
        {
            $data['samePeriodComparison']['value'] = (int) ($previous_rank['rank'] ?? 0);
        }

        $data['samePeriodComparison']['changeRank'] = $data['value'] - $data['samePeriodComparison']['value'];
        
        parent::responseHandler($data, 'Lấy dữ liệu thành công', true, 0, $inputs);
    }

    /**
     * ENDPOINT: /api-v2/reporting/sales/revenue
     * 
     * Day => d/m/y => in range start_date -> end_date
     * KPI progress
     * type = get all
     * 
     * Lấy doanh thu mà nhân viên kinh doanh cụ thể mang về theo KPI cho công ty trong khoảng thời gian được chỉ định.
     */
    public function revenue_get()
    {
    	$default_inputs = [
            'sale_id' => null,
            'year' => my_date(time(), 'Y'),
            'month' => my_date(time(), 'm'),
            'day' => my_date(time(), 'd'),
        ];
        $inputs = wp_parse_args(parent::get(), $default_inputs);

        // Validate inputs
        $this->load->library('form_validation');
        $this->form_validation->set_data($inputs);
        $this->form_validation->set_lang('vietnamese');
        $this->form_validation->set_rules('sale_id', 'sale_id', 'required|integer|greater_than[0]');
        $this->form_validation->set_rules('year', 'year', 'integer');
        $this->form_validation->set_rules('month', 'month', 'integer');
        $this->form_validation->set_rules('day', 'day', 'integer');

        if( FALSE === $this->form_validation->run())
        {
            $errors = [
                'errors' => $this->form_validation->error_array()
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        $roles_id = $this->option_m->get_value('group_sales_ids', TRUE);
        $has_staff = $this->admin_m->set_get_active()->set_role($roles_id)
            ->where('user_id', $inputs['sale_id'])
            ->count_by() > 0;
        if(!$has_staff)
        {
            $errors = [
                'sale_id' => 'Nhân viên kinh doanh không tồn tại!'
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        // Prepare input
        $dimension = 'month';
        if(empty($inputs['month']))
        {
            $dimension = 'year';

            $inputs['day'] = 1;
            $inputs['month'] = 1;
        }

        if(empty($inputs['day']))
        {
            $dimension = 'month';

            $inputs['day'] = 1;
        }

        $selected_time = $inputs['year'] . '-' . str_pad($inputs['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($inputs['day'], 2, '0', STR_PAD_LEFT) . ' 00:00:00';
        $selected_time = strtotime($selected_time);
        $previous_time = strtotime("-1 {$dimension}", $selected_time);

        $selected_start_time = 0;
        $selected_end_time = 0;
        $previous_start_time = 0;
        $previous_end_time = 0;
        switch($dimension)
        {
            case 'year':
                $selected_start_time = start_of_year($selected_time);
                $selected_end_time = end_of_year($selected_time);
                $previous_start_time = start_of_year($previous_time);
                $previous_end_time = end_of_year($previous_time);
                break;

            default:
                $selected_start_time = start_of_month($selected_time);
                $selected_end_time = end_of_day(end_of_month($selected_time));
                $previous_start_time = start_of_month($previous_time);
                $previous_end_time = end_of_day(end_of_month($previous_time));

                break;
        }

        // Query data
        $this->load->model('kpi_progress_m');
        $selected_kpi = $this->kpi_progress_m
            ->where('user_id', $inputs['sale_id'])
            ->where('start_date >=', $selected_start_time)
            ->where('end_date <=', $selected_end_time)

            ->select('user_id')
            ->select('target_value')
            ->select('current_value')
            ->select('status')

            ->as_array()
            ->get_by();
        
        $previous_kpi = $this->kpi_progress_m
            ->where('user_id', $inputs['sale_id'])
            ->where('start_date >=', $previous_start_time)
            ->where('end_date <=', $previous_end_time)

            ->select('user_id')
            ->select('target_value')
            ->select('current_value')
            ->select('status')

            ->as_array()
            ->get_by();
        
        // Compute response
        if( empty($selected_kpi)
            && empty($previous_kpi))
        {
            parent::responseHandler([], 'Không tìm thấy dữ liệu theo yêu cầu', true, 204);
        }

        $data = [
            'value' => 0,
            'kpi' => 0,
            'status' => null,
            'samePeriodComparison' => [
                'value' => 0,
                'kpi' => 0,
                'status' => null,
                'changeRatioPercent' => 0,
            ]
        ];

        $data['value'] = (int) ($selected_kpi['current_value'] ?? 0);
        $data['kpi'] = (int) ($selected_kpi['target_value'] ?? 0);
        $data['status'] = $selected_kpi['status'] ?? 'FAILED';

        if(!empty($previous_kpi))
        {
            $data['samePeriodComparison']['value'] = (int) ($previous_kpi['current_value'] ?? 0);
            $data['samePeriodComparison']['kpi'] = (int) ($previous_kpi['target_value'] ?? 0);
            $data['samePeriodComparison']['status'] = $previous_kpi['status'] ?? 'FAILED';

            $changeRatioPercent = (float) round(div($data['value'] - $data['samePeriodComparison']['value'], $data['samePeriodComparison']['value']) * 100, 2);

            $data['samePeriodComparison']['changeRatioPercent'] = $changeRatioPercent;
        }
        
        parent::responseHandler($data, 'Lấy dữ liệu thành công', true, 0, $inputs);
    }

    /**
     * ENDPOINT: /api-v2/reporting/sales/aggregate
     * 
     * Lấy số tiền hoa hồng theo doanh thu cho nhân viên kinh doanh cụ thể trong khoảng thời gian được chỉ định.
     */
    public function aggregate_get()
    {
    	$default_inputs = [
            'sale_id' => 0,
            'year' => my_date(time(), 'Y'),
            'month' => my_date(time(), 'm'),
            'day' => my_date(time(), 'd'),
            'metrics' => null
        ];
        $inputs = wp_parse_args(parent::get(), $default_inputs);

        $allow_metrics = [
            'revenue',
            'newContracts',
            'renewalContracts',
            'commission',
            'kpi',
        ];

        // Validate inputs
        $this->load->library('form_validation');
        $this->form_validation->set_data($inputs);
        $this->form_validation->set_lang('vietnamese');
        $this->form_validation->set_rules('sale_id', 'sale_id', 'required|integer|greater_than[0]');
        $this->form_validation->set_rules('year', 'year', 'integer');
        $this->form_validation->set_rules('month', 'month', 'integer');
        $this->form_validation->set_rules('day', 'day', 'integer');
        $this->form_validation->set_rules('metrics', 'metrics', 'required');

        if( FALSE === $this->form_validation->run())
        {
            $errors = [
                'errors' => $this->form_validation->error_array()
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        $metrics = explode(',', $inputs['metrics']);
        $not_allow_metrics = array_filter($metrics, function($metric) use ($allow_metrics) { return !in_array($metric, $allow_metrics);});
        if(!empty($not_allow_metrics))
        {
            $not_allow_metrics = implode(', ', $not_allow_metrics);
            $errors = [
                'errors' => [
                    'metrics' => 'Trường metrics chứa dữ liệu không hợp lệ . (' . $not_allow_metrics . ')'
                ]
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        $roles_id = $this->option_m->get_value('group_sales_ids', TRUE);
        $has_staff = $this->admin_m->set_get_active()->set_role($roles_id)
            ->where('user_id', $inputs['sale_id'])
            ->count_by() > 0;
        if(!$has_staff)
        {
            $errors = [
                'sale_id' => 'Nhân viên kinh doanh không tồn tại!'
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        // Prepare input
        $dimension = 'day';
        if(empty($inputs['day']))
        {
            $dimension = 'month';

            $inputs['day'] = 1;
        }
        if(empty($inputs['month']))
        {
            $dimension = 'year';

            $inputs['day'] = 1;
            $inputs['month'] = 1;
        }

        $selected_time = $inputs['year'] . '-' . str_pad($inputs['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($inputs['day'], 2, '0', STR_PAD_LEFT) . ' 00:00:00';
        $selected_time = strtotime($selected_time);

        $selected_start_time = 0;
        $selected_end_time = 0;
        switch($dimension)
        {
            case 'year':
                $selected_start_time = start_of_year($selected_time);
                $selected_end_time = end_of_day(end_of_year($selected_time));
                
                break;

            case 'month':
                $selected_start_time = start_of_month($selected_time);
                $selected_end_time = end_of_day(end_of_month($selected_time));

                break;

            case 'day':
                $selected_start_time = start_of_day($selected_time);
                $selected_end_time = end_of_day($selected_time);

                break;
        }

        $selected_start_date = my_date($selected_start_time, 'Y-m-d');
        $selected_day = (int) my_date($selected_start_time, 'd');
        $selected_month = (int) my_date($selected_start_time, 'm');
        $selected_year = (int) my_date($selected_start_time, 'Y');

        $selected_end_date = my_date($selected_end_time, 'Y-m-d');

        // Build query
        $this->load->model('dim_time_m');
        $customer_resource = $this->dim_time_m
            ->where('dim_time.year !=', 0)
            ->where('dim_time.quarter !=', 0)
            ->where('dim_time.month !=', 0)
            ->where('dim_time.week !=', 0)
            ->where('dim_time.day !=', 0)
            ->where('dim_time.date >=', $selected_start_date)
            ->where('dim_time.date <=', $selected_end_date)
            
            ->select('dim_time.date AS date_value');

        switch($dimension)
        {
            case 'year':
                $customer_resource = $customer_resource->where_in('dim_time.year', $selected_year)
                    ->order_by('dim_time.year', 'DESC');
                break;

            case 'month':
                $customer_resource = $customer_resource->where('dim_time.year', $selected_year)
                    ->where('dim_time.month', $selected_month)
                    ->order_by('dim_time.year', 'DESC')
                    ->order_by('dim_time.month', 'DESC');
                
                break;

            default:
                $customer_resource = $customer_resource->where('dim_time.year', $selected_year)
                    ->where('dim_time.month', $selected_month)
                    ->where('dim_time.day', $selected_day)
                    ->order_by('dim_time.year', 'DESC')
                    ->order_by('dim_time.month', 'DESC')
                    ->order_by('dim_time.day', 'DESC');

                break;
        }

        // Fetch customer resource data
        $customer_resource_metric_keys = ['revenue', 'newContracts', 'renewalContracts'];
        $customer_resource_metrics = array_filter($metrics, function($metric) use ($customer_resource_metric_keys) { 
            return in_array($metric, $customer_resource_metric_keys);
        });

        $kpi_map_keys = [
            'revenue' => 'revenue',
            'newContracts' => 'type',
            'renewalContracts' => 'type',
        ];
        $customer_resource_metric_keys = array_map(function($key) use ($kpi_map_keys) { return $kpi_map_keys[$key]; }, $customer_resource_metrics);
        $customer_resource_metric_keys = array_unique($customer_resource_metric_keys);
        
        $customer_resource = $customer_resource->join('fact_customer_resource_metrics', 'fact_customer_resource_metrics.time_id = dim_time.id')
            ->where('user_id', $inputs['sale_id']);

        foreach($customer_resource_metric_keys as $metric)
        {
            $customer_resource->select('fact_customer_resource_metrics.' . $metric);
            
            if('type' == $metric)
            {
                $customer_resource->select('fact_customer_resource_metrics.number_of_contracts');
            }
        }

        $customer_resource_data = $customer_resource->as_array()->get_all();
        foreach($customer_resource_data as &$item)
        {
            if(in_array('newContracts', $customer_resource_metrics))
            {
                $newContracts = 0;
                if(in_array($item['type'], ['ký mới tự tìm', 'ký mới marketing']))
                {
                    $newContracts = $item['number_of_contracts'];
                }

                $item['newContracts'] = (int) $newContracts;
            }

            if(in_array('renewalContracts', $customer_resource_metrics))
            {
                $renewalContracts = 0;
                if(in_array($item['type'], ['tái ký tự tìm', 'tái ký marketing']))
                {
                    $renewalContracts = $item['number_of_contracts'];
                }

                $item['renewalContracts'] = (int) $renewalContracts;
            }

            if(array_intersect(['newContracts', 'renewalContracts'], $customer_resource_metrics))
            {
                unset($item['type']);
                unset($item['number_of_contracts']);
            };
        }
        $customer_resource_group_by_date = array_group_by($customer_resource_data, 'date_value');

        // Fetch kpi data
        $kpi_metric_keys = ['kpi'];
        $kpi_metrics = array_filter($metrics, function($metric) use ($kpi_metric_keys) { 
            return in_array($metric, $kpi_metric_keys);
        });
        
        $kpi_map_keys = [
            'kpi' => 'target_value',
        ];

        $kpi_data = [];
        if(!empty($kpi_metrics))
        {
            $this->load->model('kpi_progress_m');
            $kpi_data = $this->kpi_progress_m->where('user_id', $inputs['sale_id'])
                ->where('start_date >=', $selected_start_time)
                ->where('end_date <=', $selected_end_time)
                
                ->select('start_date');

            foreach($kpi_metrics as $column)
            {
                $kpi_data->select($kpi_map_keys[$column]);
            }

            $kpi_data = $kpi_data->as_array()->get_all();

            $kpi_data = array_map(function($item) {
                $item['start_date'] = my_date(start_of_month($item['start_date']), 'Y-m');
                
                return $item;
            }, $kpi_data);
            $kpi_group_by_start_date = array_group_by($kpi_data, 'start_date');
        }

        // Mapping data
        $period = new DatePeriod(
            new DateTime($selected_start_date),
            new DateInterval('P1D'),
            (new DateTime($selected_end_date))->add(new DateInterval('P1D'))
        );
        $period = array_reverse(iterator_to_array($period));

        $data = [];
        foreach($period as $_date)
        {
            $date = $_date->format('Y-m-d');
            $month = $_date->format('Y-m');

            $item = [
                'date' => $date,
            ];

            $customer_resource_item = $customer_resource_group_by_date[$date] ?? [];
            if(empty($customer_resource_item))
            {
                foreach($customer_resource_metrics as $metric)
                {
                    $item[$metric] = 0;
                }
            }
            else
            {

                foreach($customer_resource_metrics as $metric)
                {
                    $item[$metric] = (int) array_sum(array_column($customer_resource_item, $metric));
                }
            }

            $kpi = $kpi_group_by_start_date[$month] ?? [];
            if(empty($kpi))
            {
                
                foreach($kpi_metrics as $metric)
                {
                    $item[$metric] = 0;
                }
            }
            else
            {
                
                foreach($kpi_metrics as $metric)
                {
                    $item[$metric] = (int) array_sum(array_column($kpi, $kpi_map_keys[$metric]));
                }
            }

            $data[] = $item;
        }

        $extra_input = [
            'metrics' => $metrics,
            'startDate' => $selected_start_date,
            'endDate' => $selected_end_date,
        ];

        parent::responseHandler($data, 'Lấy dữ liệu thành công', true, 0, $extra_input);
    }

    /**
     * ENDPOINT: /api-v2/reporting/sales/kpi_revenue
     * 
     * KPI progress
     * type = REVENUE_PAYMENT
     * 
     * Được thiết kế để truy xuất KPI doanh thu cho một nhân viên kinh doanh cụ thể trong một khoảng thời gian nhất định
     */
    public function kpi_revenue_get()
    {
    	$default_inputs = [
            'sale_id' => null,
            'year' => my_date(time(), 'Y'),
            'month' => my_date(time(), 'm'),
            'day' => my_date(time(), 'd'),
        ];
        $inputs = wp_parse_args(parent::get(), $default_inputs);

        // Validate inputs
        $this->load->library('form_validation');
        $this->form_validation->set_data($inputs);
        $this->form_validation->set_lang('vietnamese');
        $this->form_validation->set_rules('sale_id', 'sale_id', 'required|integer|greater_than[0]');
        $this->form_validation->set_rules('year', 'year', 'integer');
        $this->form_validation->set_rules('month', 'month', 'integer');
        $this->form_validation->set_rules('day', 'day', 'integer');

        if( FALSE === $this->form_validation->run())
        {
            $errors = [
                'errors' => $this->form_validation->error_array()
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        $roles_id = $this->option_m->get_value('group_sales_ids', TRUE);
        $has_staff = $this->admin_m->set_get_active()->set_role($roles_id)
            ->where('user_id', $inputs['sale_id'])
            ->count_by() > 0;
        if(!$has_staff)
        {
            $errors = [
                'sale_id' => 'Nhân viên kinh doanh không tồn tại!'
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        // Prepare input
        $dimension = 'month';
        if(empty($inputs['month']))
        {
            $dimension = 'year';

            $inputs['day'] = 1;
            $inputs['month'] = 1;
        }

        if(empty($inputs['day']))
        {
            $dimension = 'month';

            $inputs['day'] = 1;
        }

        $selected_time = $inputs['year'] . '-' . str_pad($inputs['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($inputs['day'], 2, '0', STR_PAD_LEFT) . ' 00:00:00';
        $selected_time = strtotime($selected_time);

        $selected_start_time = 0;
        $selected_end_time = 0;
        switch($dimension)
        {
            case 'year':
                $selected_start_time = start_of_year($selected_time);
                $selected_end_time = end_of_year($selected_time);

                break;

            default:
                $selected_start_time = start_of_month($selected_time);
                $selected_end_time = end_of_day(end_of_month($selected_time));

                break;
        }

        // Query data
        $this->load->model('kpi_progress_m');
        $selected_kpi = $this->kpi_progress_m
            ->where('user_id', $inputs['sale_id'])
            ->where('start_date >=', $selected_start_time)
            ->where('end_date <=', $selected_end_time)

            ->select('user_id')
            ->select('target_value')
            ->select('current_value')
            ->select('status')

            ->as_array()
            ->get_by();
        
        // Compute response
        if( empty($selected_kpi))
        {
            parent::responseHandler([], 'Không tìm thấy dữ liệu theo yêu cầu', true, 204);
        }

        $data = [
            'currentRevenue' => (int) $selected_kpi['current_value'],
            'targetRevenue' => (int) $selected_kpi['target_value'],
            'estimatedCommission' => 0,
            'currentCommission' => 0,
        ];
        
        parent::responseHandler($data, 'Lấy dữ liệu thành công', true, 0, $inputs);

        $data = [
            [
                'currentRevenue' => 48000,
                'targetRevenue' => 50000,
                'estimatedCommission' => 2400,
                'currentCommission' => 2300,
            ]
        ];
        parent::responseHandler($data, 'Lấy dữ liệu thành công', true, 0, $inputs);
    }

    /**
     * ENDPOINT: /api-v2/reporting/sales/revenue_by_source
     * 
     * fact_customer_resource
     * 
     * Endpoint này trả về số liệu doanh thu được phân loại theo nguồn khách hàng trong một kỳ thời gian cụ thể. Dữ liệu này sau đó có thể được sử dụng để hiển thị trên biểu đồ cột hoặc biểu đồ khác.
     */
    public function revenue_by_source_get()
    {
    	$default_inputs = [
            'sale_id' => 0,
            'year' => my_date(time(), 'Y'),
            'month' => my_date(time(), 'm'),
            'day' => my_date(time(), 'd'),
        ];
        $inputs = wp_parse_args(parent::get(), $default_inputs);

        // Validate inputs
        $this->load->library('form_validation');
        $this->form_validation->set_data($inputs);
        $this->form_validation->set_lang('vietnamese');
        $this->form_validation->set_rules('sale_id', 'sale_id', 'required|integer|greater_than[0]');
        $this->form_validation->set_rules('year', 'year', 'integer');
        $this->form_validation->set_rules('month', 'month', 'integer');
        $this->form_validation->set_rules('day', 'day', 'integer');

        if( FALSE === $this->form_validation->run())
        {
            $errors = [
                'errors' => $this->form_validation->error_array()
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        $roles_id = $this->option_m->get_value('group_sales_ids', TRUE);
        $has_staff = $this->admin_m->set_get_active()->set_role($roles_id)
            ->where('user_id', $inputs['sale_id'])
            ->count_by() > 0;
        if(!$has_staff)
        {
            $errors = [
                'sale_id' => 'Nhân viên kinh doanh không tồn tại!'
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        // Prepare input
        $dimension = 'day';
        if(empty($inputs['day']))
        {
            $dimension = 'month';

            $inputs['day'] = 1;
        }
        if(empty($inputs['month']))
        {
            $dimension = 'year';

            $inputs['day'] = 1;
            $inputs['month'] = 1;
        }

        $day = (int) $inputs['day'];
        $month = (int) $inputs['month'];
        $year = (int) $inputs['year'];

        // Build query
        $this->load->model('reporting/fact_customer_resource_metrics_m');
        $query_builder = $this->fact_customer_resource_metrics_m
            ->join('dim_time', 'dim_time.id = fact_customer_resource_metrics.time_id')
            ->where('fact_customer_resource_metrics.user_id', $inputs['sale_id']);

        switch($dimension)
        {
            case 'year':
                $query_builder = $query_builder->where('dim_time.year', $year)
                    ->where('dim_time.quarter', 0)
                    ->where('dim_time.month', 0)
                    ->where('dim_time.week', 0)
                    ->where('dim_time.day', 0)
                    ->order_by('dim_time.year', 'DESC');

                break;

            case 'month':
                $query_builder = $query_builder->where('dim_time.year', $year)
                    ->where('dim_time.quarter !=', 0)
                    ->where('dim_time.month', $month)
                    ->where('dim_time.week', 0)
                    ->where('dim_time.day', 0)
                    ->order_by('dim_time.year', 'DESC')
                    ->order_by('dim_time.month', 'DESC');
                
                break;

            default:
                $query_builder = $query_builder->where('dim_time.year', $year)
                    ->where('dim_time.quarter !=', 0)
                    ->where('dim_time.month', $month)
                    ->where('dim_time.week !=', 0)
                    ->where('dim_time.day', $day)
                    ->order_by('dim_time.year', 'DESC')
                    ->order_by('dim_time.month', 'DESC');

                break;
        }

        $customer_resource_metrics = $query_builder
            ->select('dim_time.day')
            ->select('dim_time.month')
            ->select('dim_time.year')
            ->select('fact_customer_resource_metrics.type')
            ->select('fact_customer_resource_metrics.revenue')
            ->as_array()
            ->get_all();
        if(empty($customer_resource_metrics))
        {
            parent::responseHandler([], 'Không tìm thấy dữ liệu theo yêu cầu', true, 204);
        }

        $data = array_map(function($item) {
            $mapping_data = [
                'source' => $item['type'],
                'revenue' => (int) $item['revenue'],
            ];

            return $mapping_data;
        }, $customer_resource_metrics);
        parent::responseHandler($data, 'Lấy dữ liệu thành công', true, 200, $inputs);
    }

    /**
     * ENDPOINT: /api-v2/reporting/sales/service_revenue_share
     * 
     * fact_service_metrics
     * 
     * Endpoint này trả về thông tin về tỷ trọng doanh thu mà nhân viên kinh doanh đã tạo ra từ mỗi dịch vụ cụ thể trong một kỳ thời gian xác định.
     */
    public function service_revenue_share_get()
    {
    	$default_inputs = [
            'sale_id' => 0,
            'year' => my_date(time(), 'Y'),
            'month' => my_date(time(), 'm'),
            'day' => my_date(time(), 'd'),
        ];
        $inputs = wp_parse_args(parent::get(), $default_inputs);

        // Validate inputs
        $this->load->library('form_validation');
        $this->form_validation->set_data($inputs);
        $this->form_validation->set_lang('vietnamese');
        $this->form_validation->set_rules('sale_id', 'sale_id', 'required|integer|greater_than[0]');
        $this->form_validation->set_rules('year', 'year', 'integer');
        $this->form_validation->set_rules('month', 'month', 'integer');
        $this->form_validation->set_rules('day', 'day', 'integer');

        if( FALSE === $this->form_validation->run())
        {
            $errors = [
                'errors' => $this->form_validation->error_array()
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        $roles_id = $this->option_m->get_value('group_sales_ids', TRUE);
        $has_staff = $this->admin_m->set_get_active()->set_role($roles_id)
            ->where('user_id', $inputs['sale_id'])
            ->count_by() > 0;
        if(!$has_staff)
        {
            $errors = [
                'sale_id' => 'Nhân viên kinh doanh không tồn tại!'
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        // Prepare input
        $dimension = 'day';
        if(empty($inputs['day']))
        {
            $dimension = 'month';

            $inputs['day'] = 1;
        }
        if(empty($inputs['month']))
        {
            $dimension = 'year';

            $inputs['day'] = 1;
            $inputs['month'] = 1;
        }

        $selected_time = $inputs['year'] . '-' . str_pad($inputs['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($inputs['day'], 2, '0', STR_PAD_LEFT) . ' 00:00:00';
        $selected_time = strtotime($selected_time);

        $day = (int) $inputs['day'];
        $month = (int) $inputs['month'];
        $year = (int) $inputs['year'];

        // Build query
        $this->load->model('reporting/fact_services_metrics_m');
        $query_builder = $this->fact_services_metrics_m
            ->join('dim_time', 'dim_time.id = fact_services_metrics.time_id')
            ->where('fact_services_metrics.user_id', $inputs['sale_id']);

        switch($dimension)
        {
            case 'year':
                $query_builder = $query_builder->where('dim_time.year', $year)
                    ->where('dim_time.quarter', 0)
                    ->where('dim_time.month', 0)
                    ->where('dim_time.week', 0)
                    ->where('dim_time.day', 0)
                    ->order_by('dim_time.year', 'DESC');
                break;

            case 'month':
                $query_builder = $query_builder->where('dim_time.year', $inputs['year'])
                    ->where('dim_time.quarter !=', 0)
                    ->where('dim_time.month =', $month)
                    ->where('dim_time.week', 0)
                    ->where('dim_time.day', 0)
                    ->order_by('dim_time.year', 'DESC')
                    ->order_by('dim_time.month', 'DESC');
                
                break;

            default:
                $query_builder = $query_builder->where('dim_time.year', $inputs['year'])
                    ->where('dim_time.quarter !=', 0)
                    ->where('dim_time.month =', $month)
                    ->where('dim_time.week !=', 0)
                    ->where('dim_time.day =', $day)
                    ->order_by('dim_time.year', 'DESC')
                    ->order_by('dim_time.month', 'DESC')
                    ->order_by('dim_time.day', 'DESC');
                break;
        }

        $services_metrics = $query_builder
            ->select('dim_time.day')
            ->select('dim_time.month')
            ->select('dim_time.year')
            ->select('fact_services_metrics.service')
            ->select('fact_services_metrics.revenue')
            ->as_array()
            ->get_all();
        if(empty($services_metrics))
        {
            parent::responseHandler([], 'Không tìm thấy dữ liệu theo yêu cầu', true, 204);
        }

        // Compute response
        $this->load->config('contract/contract');
        $service_type_enums = $this->config->item('services');

        $services_metrics_total_revenue = array_reduce($services_metrics, function($result, $item) {
            $service = $item['service'];
            $revenue = (int) $item['revenue'];

            if(empty($result[$service]))
            {
                $result[$service] = $revenue;

                return $result;
            }

            
            $result[$service] += $revenue;

            return $result;
        }, []);

        $total_revenue = array_sum(array_values($services_metrics_total_revenue));

        $data = [];
        foreach($services_metrics_total_revenue as $service => $revenue)
        {
            $item = [
                'service' => $service_type_enums[$service] ?? $service,
                'revenueShare' => (float) round(div($revenue, $total_revenue) * 100, 2),
            ];

            $data[] = $item;
        }

        parent::responseHandler($data, 'Lấy dữ liệu thành công', true, 200, $inputs);
    }

    /**
     * ENDPOINT: /api-v2/reporting/sales/new_contract_signed
     * 
     * Endpoint Danh sách hợp đồng trong kỳ dữ liệu của kinh doanh
     */
    public function new_contract_signed_get()
    {
        $default_inputs = [
            'sale_id' => 0,
            'year' => my_date(time(), 'Y'),
            'month' => my_date(time(), 'm'),
            'day' => my_date(time(), 'd'),
        ];
        $inputs = wp_parse_args(parent::get(), $default_inputs);

        // Validate inputs
        $this->load->library('form_validation');
        $this->form_validation->set_data($inputs);
        $this->form_validation->set_lang('vietnamese');
        $this->form_validation->set_rules('sale_id', 'sale_id', 'required|integer|greater_than[0]');
        $this->form_validation->set_rules('year', 'year', 'integer');
        $this->form_validation->set_rules('month', 'month', 'integer');
        $this->form_validation->set_rules('day', 'day', 'integer');

        if( FALSE === $this->form_validation->run())
        {
            $errors = [
                'errors' => $this->form_validation->error_array()
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        $roles_id = $this->option_m->get_value('group_sales_ids', TRUE);
        $has_staff = $this->admin_m->set_get_active()->set_role($roles_id)
            ->where('user_id', $inputs['sale_id'])
            ->count_by() > 0;
        if(!$has_staff)
        {
            $errors = [
                'sale_id' => 'Nhân viên kinh doanh không tồn tại!'
            ];
            parent::responseHandler([], 'Dữ liệu đầu vào không hợp lệ', false, 400, $errors);
        }

        // Prepare input
        $dimension = 'month';
        if(empty($inputs['month']))
        {
            $dimension = 'year';

            $inputs['day'] = 1;
            $inputs['month'] = 1;
        }

        if(empty($inputs['day']))
        {
            $dimension = 'month';

            $inputs['day'] = 1;
        }

        $selected_time = $inputs['year'] . '-' . str_pad($inputs['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($inputs['day'], 2, '0', STR_PAD_LEFT) . ' 00:00:00';
        $selected_time = strtotime($selected_time);

        $selected_start_time = 0;
        $selected_end_time = 0;
        switch($dimension)
        {
            case 'year':
                $selected_start_time = start_of_year($selected_time);
                $selected_end_time = end_of_year($selected_time);

                break;

            default:
                $selected_start_time = start_of_month($selected_time);
                $selected_end_time = end_of_day(end_of_month($selected_time));

                break;
        }

        // Query data
        $this->load->model('contract/contract_m');
        $contracts = $this->contract_m->set_term_type()
            ->join('term_users', 'term_users.term_id = term.term_id')
            ->join('termmeta', 'termmeta.term_id = term.term_id')
            
            ->where('term_users.user_id', $inputs['sale_id'])
            ->where_in('term.term_status', [
                'waitingforapprove',
                'pending',
                'publish',
                'ending',
                'liquidation',
                'remove',
            ])
            ->where('termmeta.meta_key', 'started_service')
            ->where('termmeta.meta_value >=', $selected_start_time)
            ->where('termmeta.meta_value <=', $selected_end_time)

            ->select('term.term_id')
            ->select('term.term_type')
            ->select('term.term_status')

            ->group_by('term.term_id')

            ->as_array()
            ->get_all();
        if(empty($contracts))
        {
            $extra_input = [
                'start_date' => my_date($selected_start_time, 'Y-m-d'),
                'end_date' => my_date($selected_end_time, 'Y-m-d'),
            ];
            parent::responseHandler([], 'Không tìm thấy dữ liệu trong kỳ đuọc chọn', true, 204, $extra_input);
        }

        // Mapping data
        $this->load->config('contract/contract');
        $service = $this->config->item('services');
        $contract_status = $this->config->item('contract_status');
        $contracts = array_map(function($contract) use ($service, $contract_status){
            $contract_id = (int) $contract['term_id'];

            $created_on = get_term_meta_value($contract_id, 'created_on');
            $contract_begin = get_term_meta_value($contract_id, 'contract_begin');
            $contract_end = get_term_meta_value($contract_id, 'contract_end');
            $service_fee_percent = round((float) get_term_meta_value($contract_id, 'service_fee_rate_actual') * 100, 2);

            $data_item = [
                'contract_id' => $contract_id,
                'code' => get_term_meta_value($contract_id, 'contract_code'),
                'created_at' => $created_on ? my_date($created_on, 'd/m/Y') : null,
                'contract_begin' => $contract_begin ? my_date($contract_begin, 'd/m/Y') : null,
                'contract_end' => $contract_end ? my_date($contract_end, 'd/m/Y') : null,
                'budget' => (int) get_term_meta_value($contract_id, 'actual_budget'),
                'spend' => (int) get_term_meta_value($contract_id, 'actual_result'),
                'service_fee_percent' => $service_fee_percent,
                'contract_value' => (int) get_term_meta_value($contract_id, 'contract_value'),
                'service' => $service[$contract['term_type']] ?? 'Chưa xác định',
                'is_new_sign' => (bool) get_term_meta_value($contract_id, 'is_first_contract'),
                'status' => $contract_status[$contract['term_status']] ?? 'Chưa xác định',
            ];

            return $data_item;
        }, $contracts);
        
        $extra_input = [
            'start_date' => my_date($selected_start_time, 'Y-m-d'),
            'end_date' => my_date($selected_end_time, 'Y-m-d'),
        ];
        parent::responseHandler($contracts, 'Lấy dữ liệu thành công', true, 200, $extra_input);
    }
}
/* End of file Sales.php */
/* Location: ./application/modules/reporting/controllers/api_v2/Sales.php */