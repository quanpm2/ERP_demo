<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fact_customer_resource_metrics_m extends Base_model
{
	public $primary_key = 'id';
	public $_table = 'fact_customer_resource_metrics';
}
/* End of file Fact_customer_resource_metrics_m.php */
/* Location: ./application/models/reporting/Fact_customer_resource_metrics_m.php */