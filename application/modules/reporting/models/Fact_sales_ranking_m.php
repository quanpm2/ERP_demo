<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fact_sales_ranking_m extends Base_model
{
	public $primary_key = 'id';
	public $_table = 'fact_sales_ranking';
}
/* End of file Fact_sales_ranking_m.php */
/* Location: ./application/models/reporting/Fact_sales_ranking_m.php */