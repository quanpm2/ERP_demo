<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fact_services_metrics_m extends Base_model
{
	public $primary_key = 'id';
	public $_table = 'fact_services_metrics';
}
/* End of file Fact_services_metrics_m.php */
/* Location: ./application/models/reporting/Fact_services_metrics_m.php */