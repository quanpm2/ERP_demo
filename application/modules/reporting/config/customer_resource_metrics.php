<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

$config['type'] = [
    'NEW_FIND_BY_SELF' => 'ký mới tự tìm',
    'NEW_MARKETING' => 'ký mới marketing',
    'RENEWAL_FIND_BY_SELF' => 'tái ký tự tìm',
    'RENEWAL_MARKETING' => 'tái ký marketing',
];