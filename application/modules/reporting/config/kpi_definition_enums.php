<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

$config['type'] = [
    'REVENUE_PAYMENT' => 'REVENUE_PAYMENT',
    'REVENUE_SPEND' => 'REVENUE_SPEND',
];

$config['status'] = [
    'ACTIVE'       => 'ACTIVE',       // KPI đang được áp dụng và hoạt động
    'INACTIVE'     => 'INACTIVE',     // KPI hiện không được sử dụng hoặc tạm thời không áp dụng
    'DEPRECATED'   => 'DEPRECATED',   // KPI không còn được sử dụng nữa, có thể do đã lỗi thời hoặc thay thế bởi KPI khác
    'UNDER_REVIEW' => 'UNDER_REVIEW', // KPI đang trong quá trình xem xét, có thể do cần cập nhật hoặc điều chỉnh
    'PROPOSED'     => 'PROPOSED',     // KPI đang được đề xuất nhưng chưa được chính thức áp dụng
];