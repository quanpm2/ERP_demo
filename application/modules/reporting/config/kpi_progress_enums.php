<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

$config['status'] = [
    'NOT_STARTED'   => 'NOT_STARTED',   // Chưa bắt đầu - KPI chưa được bắt đầu thực hiện
    'IN_PROGRESS'   => 'IN_PROGRESS',   // Đang tiến hành - KPI đang trong quá trình thực hiện
    'ON_TARGET'     => 'ON_TARGET',     // Đạt mục tiêu - KPI đang tiến triển đúng như kế hoạch
    'BEHIND_TARGET' => 'BEHIND_TARGET', // Chậm mục tiêu - KPI đang chậm tiến độ so với mục tiêu đề ra
    'COMPLETED'     => 'COMPLETED',     // Hoàn thành - KPI đã hoàn thành
    'EXCEEDED'      => 'EXCEEDED',      // Vượt mục tiêu - KPI đã hoàn thành và vượt quá mục tiêu đề ra
    'FAILED'        => 'FAILED',        // Không đạt - KPI không đạt được mục tiêu đề ra
];