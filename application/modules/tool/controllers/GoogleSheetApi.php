<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GoogleSheetApi extends Admin_Controller {

	function __construct()
	{
		$this->autoload['models'][] = 'term_users_m';
		parent::__construct();
		$this->config->load('googlesheet_fields');
	}

	public function test()
	{
		$this->load->model('domain/domain_m');
		$this->load->model('hosting/hosting_m');

		define('SPREADSHEET_ID', '1-W_Co3s3txLotqUOuCeVcetyO-dIeAPyJZWqsBK2GaM');

		$nodes = $this->getHostingRaw();

		$deactiveNodes = [];
		$activeNodes = [];

		foreach ($nodes as $node)
		{	
			if( 'active' !== $node['term_status'])
			{
				$deactiveNodes[] = $node;
				continue;
			}

			if( ! $node['isKeepCluster']) continue;

			$activeNodes[] = $node;
		}

		$this->create_suspended_sheet($deactiveNodes);


		

		$this->create_must_transfer_sheet($activeNodes);
	}

	public function create_suspended_sheet($nodes)
	{
		$this->config->load('googlesheet_fields');
		$fields = $this->config->item('columns');
		$controlFields = array(
			'domain',
			'ip',
			'domainContract',
			'domainContractEnd',
			'hostingContract',
			'hostingContractEnd',
			'action',
			'ip_to',
			'system_tech',
			'backup_date',
			'backup_status',
		);

		foreach ($controlFields as $key => &$item)
		{
			if( empty($fields[$item])) continue;
			$item = $fields[$item];
		}

		$headings 	= array_column($controlFields, 'label');
		$values		= array($headings);

		foreach ($nodes as $node)
		{
			$actions = [];

			$backup_status 	= 'Chưa xử lý';
			$dns_transfer_status 	= "Chờ xử lý";

			$ip 				= $node['term_name'];
			$domain 			= $node['term_slug'];

			$contract 			= reset($node['contracts']);

			$isControlDNSNode 	= booleanToStr(TRUE);

			$hasHostingContract = FALSE;
			$hostingContract = NULL;
			$hostingContractEnd = NULL;
			if( ! empty($node['hostings']))
			{
				$hasHostingContract = booleanToStr(TRUE);
				$_hosting 			= reset($node['hostings']);
				$hostingContract 	= get_term_meta_value($_hosting->term_id, 'contract_code');
				$hostingContractEnd = $_hosting->contract_enddate;

				if($_hosting->isExpired)
				{
					$actions[] = "HĐ Hosting hết hạn";
				}

				$contract = $_hosting;
			}			

			$hasDomainContract 	= !empty($node['domains']);
			$domainContract = $domainContractEnd = NULL;
			if( ! empty($node['domains']))
			{
				$hasDomainContract 	= booleanToStr(TRUE);
				$_domain 			= reset($node['domains']);
				$domainContract 	= get_term_meta_value($_domain->term_id, 'contract_code');
				$domainContractEnd 	= $_domain->contract_enddate;

				if( ! $_domain->isValidDNS)
				{
					$_domain_lbl = empty($_domain->realIP) ? "Domain lỗi DNS" : "Domain sử dụng Host khác";
					$actions[] = "IP Không khớp ({$_domain_lbl})";
				}

				if($_domain->isExpired)
				{
					$actions[] = "HĐ Tên miền hết hạn";
				}

				$contract = $_domain;
			}

			$sale 					= "ANHNT";
			$callCustomerState 		= 'Chưa gọi';
			$callCustomerComment 	= 'Chưa liên hệ được';

			$system_tech 		= "TRUNGNT";
			$backup_date 		= '2020/11/17';
			$backup_status 	= 'Chưa xử lý';
			
			$assignee_dns 			= "KHUONGPB";
			$dns_transfer_date 		= '2020/11/18';
			$dns_transfer_status 	= 'Chưa xử lý';

			$action = "Backup & Đưa lên Google-Drive";

			$values[] = array(

				(string) $domain, // 'domain',
				(string) $ip, // 'ip',

				(string) $domainContract, // 'domainContract',
				(string) $domainContractEnd, // 'domainContractEnd',

				(string) $hostingContract, // 'hostingContract',
				(string) $hostingContractEnd, // 'hostingContractEnd',

				(string) $action, // 'sale',
				(string) $node['ip_to'], // 'ip_to',

				(string) $system_tech, // 'system_tech',
				(string) $backup_date, // 'transfer_date',
				(string) $backup_status, // 'transfer_status',

			);
		}

		// Get the API client and construct the service object.
		$client 		= $this->getClient();
		$service 		= new Google_Service_Sheets($client);
		
		define('SUSPENDED_SHEET_ID', '1602803275');
		define('SUSPENDED_RANGE', 'SuspendedHosts!A1:Y');

		define('START_ROW_INDEX', 1);
		define('START_ROW_CONTENT_INDEX', 1);

		$body 			= new Google_Service_Sheets_ValueRange([ 'values' => $values ]);


        $body 	= new Google_Service_Sheets_ValueRange([ 'values' => $values ]);
        $params = [ 'valueInputOption' => 'USER_ENTERED' ];
		$result = $service->spreadsheets_values->update(SPREADSHEET_ID, SUSPENDED_RANGE, $body, $params);

        foreach ($controlFields as $key => $colConfig)
        {
        	if(FALSE == $colConfig['dataValidatation']) continue;

        	$_dataValidatation = $colConfig['dataValidatation'];

        	if( ! in_array($_dataValidatation['type'], ['BOOLEAN', 'ONE_OF_LIST'])) continue;

        	$_conditions = new Google_Service_Sheets_BooleanCondition();
		    $_conditions->setType($_dataValidatation['type']);

        	if('ONE_OF_LIST' === $_dataValidatation['type'])
        	{
        		$_values = array_map(function($x){

        			$_cellData = new Google_Service_Sheets_ConditionValue();
        			$_cellData->setUserEnteredValue($x);
        			return $_cellData;

        		}, $_dataValidatation['values']);

        		$_conditions->setValues($_values);
        	}

        	$_rule = new Google_Service_Sheets_DataValidationRule();
		    $_rule->setCondition($_conditions);
		    $_rule->setInputMessage('Please set correct value');
		    $_rule->setShowCustomUi(true);

		    /* Init Range Model Object */
		    $_endRowIndex 		= count($values);
		    $_startColumnIndex	= $key;
		    $_endColumnIndex 	= $key + 1;

        	$_range = new Google_Service_Sheets_GridRange();
			$_range->setSheetId(SUSPENDED_SHEET_ID);
		    $_range->setStartRowIndex(START_ROW_CONTENT_INDEX);
			$_range->setEndRowIndex($_endRowIndex);
			$_range->setStartColumnIndex($_startColumnIndex);
			$_range->setEndColumnIndex($_endColumnIndex);

		    $_dataValidationReq = new Google_Service_Sheets_SetDataValidationRequest();
		    $_dataValidationReq->setRule($_rule);
		    $_dataValidationReq->setRange($_range);

		    $sheetReq = new Google_Service_Sheets_Request();
		    $sheetReq->setSetDataValidation($_dataValidationReq);

		    $bodyReq = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest();
		    $bodyReq->setRequests($sheetReq);

		    $result = $service->spreadsheets->batchUpdate(SPREADSHEET_ID, $bodyReq);
        }	
	}

	public function create_must_transfer_sheet($nodes)
	{
		$this->config->load('googlesheet_fields');
		$fields = $this->config->item('columns');
		$controlFields = array(

			'domain',
			'ip',
			'provider',
			'domainContract',
			'domainContractEnd',

			'hostingContract',
			'hostingContractEnd',

			'action',
			'ip_to',

			'system_tech',
			'transfer_date',
			'transfer_status',

			'system_tech',
			'backup_date',
			'backup_status',
		);

		foreach ($controlFields as $key => &$item)
		{
			if( empty($fields[$item])) continue;
			$item = $fields[$item];
		}

		$headings 	= array_column($controlFields, 'label');
		$values		= array($headings);

		foreach ($nodes as $node)
		{
			$action = "Clone sang IP đích";

			$dns_transfer_status 	= "Chưa xử lý";

			$ip 				= $node['term_name'];
			$domain 			= $node['term_slug'];

			$contract 			= reset($node['contracts']);


			$hasHostingContract = FALSE;
			$hostingContract = NULL;
			$hostingContractEnd = NULL;
			if( ! empty($node['hostings']))
			{
				$hasHostingContract = booleanToStr(TRUE);
				$_hosting 			= reset($node['hostings']);
				$hostingContract 	= get_term_meta_value($_hosting->term_id, 'contract_code');
				$hostingContractEnd = $_hosting->contract_enddate;
				$contract = $_hosting;
			}			

			$hasDomainContract 	= !empty($node['domains']);
			$domainContract = $domainContractEnd = NULL;
			if( ! empty($node['domains']))
			{
				$hasDomainContract 	= booleanToStr(TRUE);
				$_domain 			= reset($node['domains']);
				$domainContract 	= get_term_meta_value($_domain->term_id, 'contract_code');
				$domainContractEnd 	= $_domain->contract_enddate;
				$contract = $_domain;
			}

			$system_tech 		= "TRUNGNT";
			$transfer_date 		= '2020/11/18';
			$transfer_status 	= 'Chưa xử lý';

			$system_tech 		= "TRUNGNT";
			$backup_date 		= '2020/11/18';
			$backup_status 	= 'Chưa xử lý';

			$values[] = array(

				(string) $domain, // 'domain',
				(string) $ip, // 'ip',

				(string) $node['provider'], // 'provider',

				(string) $domainContract, // 'domainContract',
				(string) $domainContractEnd, // 'domainContractEnd',

				(string) $hostingContract, // 'hostingContract',
				(string) $hostingContractEnd, // 'hostingContractEnd',

				(string) $action, // 'sale',
				(string) $node['ip_to'], // 'ip_to',


				(string) $system_tech, // 'system_tech',
				(string) $transfer_date, // 'transfer_date',
				(string) $transfer_status, // 'transfer_status',

				(string) $system_tech, // 'system_tech',
				(string) $backup_date, // 'transfer_date',
				(string) $backup_status, // 'transfer_status',
			);
		}

		// Get the API client and construct the service object.
		$client 		= $this->getClient();
		$service 		= new Google_Service_Sheets($client);
		
		define('TRANSFER_DNS_SHEET_ID', '2145039736');
		define('TRANSFER_DNS_RANGE', 'MustTransferedHost!A1:Y');

		defined('START_ROW_INDEX') OR define('START_ROW_INDEX', 1);
		defined('START_ROW_CONTENT_INDEX') OR define('START_ROW_CONTENT_INDEX', 1);

		$body 			= new Google_Service_Sheets_ValueRange([ 'values' => $values ]);


        $body 	= new Google_Service_Sheets_ValueRange([ 'values' => $values ]);
        $params = [ 'valueInputOption' => 'USER_ENTERED' ];
		$result = $service->spreadsheets_values->update(SPREADSHEET_ID, TRANSFER_DNS_RANGE, $body, $params);

        foreach ($controlFields as $key => $colConfig)
        {
        	if(FALSE == $colConfig['dataValidatation']) continue;

        	$_dataValidatation = $colConfig['dataValidatation'];

        	if( ! in_array($_dataValidatation['type'], ['BOOLEAN', 'ONE_OF_LIST'])) continue;

        	$_conditions = new Google_Service_Sheets_BooleanCondition();
		    $_conditions->setType($_dataValidatation['type']);

        	if('ONE_OF_LIST' === $_dataValidatation['type'])
        	{
        		$_values = array_map(function($x){

        			$_cellData = new Google_Service_Sheets_ConditionValue();
        			$_cellData->setUserEnteredValue($x);
        			return $_cellData;

        		}, $_dataValidatation['values']);

        		$_conditions->setValues($_values);
        	}

        	$_rule = new Google_Service_Sheets_DataValidationRule();
		    $_rule->setCondition($_conditions);
		    $_rule->setInputMessage('Please set correct value');
		    $_rule->setShowCustomUi(true);

		    /* Init Range Model Object */
		    $_endRowIndex 		= count($values);
		    $_startColumnIndex	= $key;
		    $_endColumnIndex 	= $key + 1;

        	$_range = new Google_Service_Sheets_GridRange();
			$_range->setSheetId(TRANSFER_DNS_SHEET_ID);
		    $_range->setStartRowIndex(START_ROW_CONTENT_INDEX);
			$_range->setEndRowIndex($_endRowIndex);
			$_range->setStartColumnIndex($_startColumnIndex);
			$_range->setEndColumnIndex($_endColumnIndex);

		    $_dataValidationReq = new Google_Service_Sheets_SetDataValidationRequest();
		    $_dataValidationReq->setRule($_rule);
		    $_dataValidationReq->setRange($_range);

		    $sheetReq = new Google_Service_Sheets_Request();
		    $sheetReq->setSetDataValidation($_dataValidationReq);

		    $bodyReq = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest();
		    $bodyReq->setRequests($sheetReq);

		    $result = $service->spreadsheets->batchUpdate(SPREADSHEET_ID, $bodyReq);
        }	
	}

	public function dns_approach($nodes)
	{
		$unKnowNodes = $controlDNSNodes = $uncontrolDNSNodes = [];

		foreach ($nodes as $node)
		{
			/* If Not Had Contracts then push to unkown queue */
			if(empty($node['contracts']))
			{
				array_push($unKnowNodes, $node);
				continue;
			}

			$domains = $node['domains'];
			if(empty($domains)) 
			{
				$uncontrolDNSNodes[] = $node;
				continue;
			}

			$controlDNSNodes[] = $node;
		}

		/* Node Cần liên hệ khách hàng để chuyển DNS */
		// $this->dns_uncontrol_approach($uncontrolDNSNodes);
		/* Node Có thể chủ động chuyển DNS */
		$this->dns_transfer_approach($controlDNSNodes);
		/* Node Không xác định - cần detech lại rõ ràng */
		// $this->dns_unknow_approach($uncontrolDNSNodes);


		dd(count($controlDNSNodes), count($uncontrolDNSNodes), count($unKnowNodes));
	}

	public function dns_uncontrol_approach($nodes)
	{
		return true;
		/*
		 * TRƯỜNG DỮ LIỆU : 
		 * 'domain',
		 * 		'ip',
		 * 		'hasDomainContract',
		 * 		'domainContract',
		 * 		'domainContractEnd',
		 * 		'hasHostingContract',
		 * 		'hostingContract',
		 * 		'hostingContractEnd',
		 * 		'canControlDNS',
		 * 		'customer_name',
		 * 		'canContact',
		 * 		'callCustomerState',
		 * 		'callCustomerComment',
		 */
		$this->config->load('googlesheet_fields');
		$fields = $this->config->item('columns');
		$uncontrolFields = array(
			'domain',
			'ip',
			'hasDomainContract',
			'hasHostingContract',
			'hostingContract',
			'hostingContractEnd',
			'canControlDNS',
			'customer_name',
			'canContact',
			'callCustomerState',
			'callCustomerComment',
			'ip_to',
			'customerDateMustChaged',
			'adsplusDateTransferd',
			'customerDateChaged',
		);

		foreach ($uncontrolFields as $key => &$item)
		{
			if( empty($fields[$item])) continue;
			$item = $fields[$item];
		}

		$uncontrolColumnConfig 	= array_values($fields);
		$uncontrolHeadings 		= array_column($uncontrolFields, 'label');
		$uncontrolValues		= array($uncontrolHeadings);

		foreach ($nodes as $node)
		{
			$sale 				= "Anhnt@adsplus.vn";
			$processState 		= "Chờ xử lý";
			$ip 				= $node['term_name'];
			$domain 			= $node['term_slug'];
			$hasDomainContract 	= booleanToStr(!empty($node['domains']));

			$isControlDNSNode 	= booleanToStr(FALSE);

			$hasHostingContract = FALSE;
			$hostingContract = NULL;
			$hostingContractEnd = NULL;
			if( ! empty($node['hostings']))
			{
				$hasHostingContract = booleanToStr(TRUE);
				$_hosting 			= reset($node['hostings']);
				$hostingContract 	= get_term_meta_value($_hosting->term_id, 'contract_code');
				$hostingContractEnd = $_hosting->contract_enddate;
			}


			$customers = $this->term_users_m->get_the_users($_hosting->term_id, ['customer_person', 'customer_company']);
			$customer  = reset($customers);

			$callCustomerState 		= 'Chưa gọi';
			$callCustomerComment 	= 'Chưa liên hệ được';

			$uncontrolValues[] = array(
				(string) $domain,
				(string) $ip,
				(string) $hasDomainContract,
				(string) $hasHostingContract,
				(string) $hostingContract,
				(string) $hostingContractEnd,
				(string) $isControlDNSNode,
				(string) $customer->display_name ?? "",
				(string) $sale,
				(string) $callCustomerState,
				(string) $callCustomerComment,
				(string) $node['ip_to'],
				(string) $node['customerDateMustChaged'],
				(string) $node['adsplusDateTransferd'],
				(string) $node['customerDateChaged'],
			);
		}

		// Get the API client and construct the service object.
		$client 		= $this->getClient();
		$service 		= new Google_Service_Sheets($client);
		
		define('UNCONTROL_SHEET_ID', '588420862');
		define('UNCONTROL_RANGE', 'UnControlDNS!A1:X');

		defined('START_ROW_INDEX') OR define('START_ROW_INDEX', 1);
		defined('START_ROW_CONTENT_INDEX') OR define('START_ROW_CONTENT_INDEX', 1);

		$body 			= new Google_Service_Sheets_ValueRange([ 'values' => $uncontrolValues ]);


        $body 	= new Google_Service_Sheets_ValueRange([ 'values' => $uncontrolValues ]);
        $params = [ 'valueInputOption' => 'USER_ENTERED' ];
		$result = $service->spreadsheets_values->update(SPREADSHEET_ID, UNCONTROL_RANGE, $body, $params);

        foreach ($uncontrolFields as $key => $colConfig)
        {
        	if(FALSE == $colConfig['dataValidatation']) continue;

        	$_dataValidatation = $colConfig['dataValidatation'];

        	if( ! in_array($_dataValidatation['type'], ['BOOLEAN', 'ONE_OF_LIST'])) continue;

        	$_conditions = new Google_Service_Sheets_BooleanCondition();
		    $_conditions->setType($_dataValidatation['type']);

        	if('ONE_OF_LIST' === $_dataValidatation['type'])
        	{
        		$_values = array_map(function($x){

        			$_cellData = new Google_Service_Sheets_ConditionValue();
        			$_cellData->setUserEnteredValue($x);
        			return $_cellData;

        		}, $_dataValidatation['values']);

        		$_conditions->setValues($_values);
        	}

        	$_rule = new Google_Service_Sheets_DataValidationRule();
		    $_rule->setCondition($_conditions);
		    $_rule->setInputMessage('Please set correct value');
		    $_rule->setShowCustomUi(true);

		    /* Init Range Model Object */
		    $_endRowIndex 		= count($uncontrolValues);
		    $_startColumnIndex	= $key;
		    $_endColumnIndex 	= $key + 1;

        	$_range = new Google_Service_Sheets_GridRange();
			$_range->setSheetId(UNCONTROL_SHEET_ID);
		    $_range->setStartRowIndex(START_ROW_CONTENT_INDEX);
			$_range->setEndRowIndex($_endRowIndex);
			$_range->setStartColumnIndex($_startColumnIndex);
			$_range->setEndColumnIndex($_endColumnIndex);

		    $_dataValidationReq = new Google_Service_Sheets_SetDataValidationRequest();
		    $_dataValidationReq->setRule($_rule);
		    $_dataValidationReq->setRange($_range);

		    $sheetReq = new Google_Service_Sheets_Request();
		    $sheetReq->setSetDataValidation($_dataValidationReq);

		    $bodyReq = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest();
		    $bodyReq->setRequests($sheetReq);

		    $result = $service->spreadsheets->batchUpdate(SPREADSHEET_ID, $bodyReq);
        }	
	}

	public function dns_transfer_approach($nodes)
	{
		$this->config->load('googlesheet_fields');
		$fields = $this->config->item('columns');
		$controlFields = array(
			'domain',
			'ip',

			'hasDomainContract',
			'domainContract',
			'domainContractEnd',

			'hasHostingContract',
			'hostingContract',
			'hostingContractEnd',
			
			'canControlDNS',
			'customer_name',

			'action',

			'ip_to',

			'sale',
			'callCustomerState',
			'callCustomerComment',

			'system_tech',
			'transfer_date',
			'transfer_status',

			'assignee_dns',
			'dns_transfer_date',
			'dns_transfer_status',
			
			'customerDateMustChaged',
			'adsplusDateTransferd',
			'customerDateChaged',
		);

		foreach ($controlFields as $key => &$item)
		{
			if( empty($fields[$item])) continue;
			$item = $fields[$item];
		}

		$headings 	= array_column($controlFields, 'label');
		$values		= array($headings);

		foreach ($nodes as $node)
		{
			$actions = [];

			$processState 			= "Chờ xử lý";
			$transfer_status 		= "Chờ xử lý";
			$dns_transfer_status 	= "Chờ xử lý";

			$ip 				= $node['term_name'];
			$domain 			= $node['term_slug'];

			$contract 			= reset($node['contracts']);

			$isControlDNSNode 	= booleanToStr(TRUE);

			$hasHostingContract = FALSE;
			$hostingContract = NULL;
			$hostingContractEnd = NULL;
			if( ! empty($node['hostings']))
			{
				$hasHostingContract = booleanToStr(TRUE);
				$_hosting 			= reset($node['hostings']);
				$hostingContract 	= get_term_meta_value($_hosting->term_id, 'contract_code');
				$hostingContractEnd = $_hosting->contract_enddate;

				if($_hosting->isExpired)
				{
					$actions[] = "HĐ Hosting hết hạn";
				}

				$contract = $_hosting;
			}			

			$hasDomainContract 	= !empty($node['domains']);
			$domainContract = $domainContractEnd = NULL;
			if( ! empty($node['domains']))
			{
				$hasDomainContract 	= booleanToStr(TRUE);
				$_domain 			= reset($node['domains']);
				$domainContract 	= get_term_meta_value($_domain->term_id, 'contract_code');
				$domainContractEnd 	= $_domain->contract_enddate;

				if( ! $_domain->isValidDNS)
				{
					$_domain_lbl = empty($_domain->realIP) ? "Domain lỗi DNS" : "Domain sử dụng Host khác";
					$actions[] = "IP Không khớp ({$_domain_lbl})";
				}

				if($_domain->isExpired)
				{
					$actions[] = "HĐ Tên miền hết hạn";
				}

				$contract = $_domain;
			}

			$customers = $this->term_users_m->get_the_users($contract->term_id, ['customer_person', 'customer_company']);
			$customer  = reset($customers);

			$sale 					= "ANHNT";
			$callCustomerState 		= 'Chưa gọi';
			$callCustomerComment 	= 'Chưa liên hệ được';

			$system_tech 		= "TRUNGNT";
			$transfer_date 		= '2020/11/17';
			$transfer_status 	= 'Chưa xử lý';
			
			$assignee_dns 			= "KHUONGPB";
			$dns_transfer_date 		= '2020/11/18';
			$dns_transfer_status 	= 'Chưa xử lý';

			$values[] = array(

				(string) $domain, // 'domain',
				(string) $ip, // 'ip',

				(string) $hasDomainContract, // 'hasDomainContract',
				(string) $domainContract, // 'domainContract',
				(string) $domainContractEnd, // 'domainContractEnd',

				(string) $hasHostingContract, // 'hasHostingContract',
				(string) $hostingContract, // 'hostingContract',
				(string) $hostingContractEnd, // 'hostingContractEnd',

				(string) $isControlDNSNode, // 'canControlDNS',
				(string) $customer->display_name ?? "", // 'customer_name',

				(string) (empty($actions) ? 'Cần xử lý' : implode(', ', $actions)) ,

				(string) $node['ip_to'], // 'ip_to',

				(string) $sale, // 'sale',
				(string) $callCustomerState, // 'callCustomerState',
				(string) $callCustomerComment, // 'callCustomerComment',


				(string) $system_tech, // 'system_tech',
				(string) $transfer_date, // 'transfer_date',
				(string) $transfer_status, // 'transfer_status',

				(string) $assignee_dns, // 'assignee_dns',
				(string) $dns_transfer_date, // 'dns_transfer_date',
				(string) $dns_transfer_status, // 'dns_transfer_status',

				(string) $node['customerDateMustChaged'], // 'customerDateMustChaged',
				(string) $node['adsplusDateTransferd'], // 'adsplusDateTransferd',
				(string) $node['customerDateChaged'], // 'customerDateChaged',

			);
		}

		// Get the API client and construct the service object.
		$client 		= $this->getClient();
		$service 		= new Google_Service_Sheets($client);
		
		define('TRANSFER_DNS_SHEET_ID', '236867709');
		define('TRANSFER_DNS_RANGE', 'TransferDNS!A1:Y');

		define('START_ROW_INDEX', 1);
		define('START_ROW_CONTENT_INDEX', 1);

		$body 			= new Google_Service_Sheets_ValueRange([ 'values' => $values ]);


        $body 	= new Google_Service_Sheets_ValueRange([ 'values' => $values ]);
        $params = [ 'valueInputOption' => 'USER_ENTERED' ];
		$result = $service->spreadsheets_values->update(SPREADSHEET_ID, TRANSFER_DNS_RANGE, $body, $params);

        foreach ($controlFields as $key => $colConfig)
        {
        	if(FALSE == $colConfig['dataValidatation']) continue;

        	$_dataValidatation = $colConfig['dataValidatation'];

        	if( ! in_array($_dataValidatation['type'], ['BOOLEAN', 'ONE_OF_LIST'])) continue;

        	$_conditions = new Google_Service_Sheets_BooleanCondition();
		    $_conditions->setType($_dataValidatation['type']);

        	if('ONE_OF_LIST' === $_dataValidatation['type'])
        	{
        		$_values = array_map(function($x){

        			$_cellData = new Google_Service_Sheets_ConditionValue();
        			$_cellData->setUserEnteredValue($x);
        			return $_cellData;

        		}, $_dataValidatation['values']);

        		$_conditions->setValues($_values);
        	}

        	$_rule = new Google_Service_Sheets_DataValidationRule();
		    $_rule->setCondition($_conditions);
		    $_rule->setInputMessage('Please set correct value');
		    $_rule->setShowCustomUi(true);

		    /* Init Range Model Object */
		    $_endRowIndex 		= count($values);
		    $_startColumnIndex	= $key;
		    $_endColumnIndex 	= $key + 1;

        	$_range = new Google_Service_Sheets_GridRange();
			$_range->setSheetId(TRANSFER_DNS_SHEET_ID);
		    $_range->setStartRowIndex(START_ROW_CONTENT_INDEX);
			$_range->setEndRowIndex($_endRowIndex);
			$_range->setStartColumnIndex($_startColumnIndex);
			$_range->setEndColumnIndex($_endColumnIndex);

		    $_dataValidationReq = new Google_Service_Sheets_SetDataValidationRequest();
		    $_dataValidationReq->setRule($_rule);
		    $_dataValidationReq->setRange($_range);

		    $sheetReq = new Google_Service_Sheets_Request();
		    $sheetReq->setSetDataValidation($_dataValidationReq);

		    $bodyReq = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest();
		    $bodyReq->setRequests($sheetReq);

		    $result = $service->spreadsheets->batchUpdate(SPREADSHEET_ID, $bodyReq);
        }	
	}

	public function appendGoogleSheetApi($spreadsheetId, $sheetId, $sheetName, $values)
	{
		// Get the API client and construct the service object.
		$client 		= $this->getClient();
		$service 		= new Google_Service_Sheets($client);
		
		define('SUSPENDED_SHEET_ID', '1683793197');
		define('SUSPENDED_RANGE', 'Hosting không hợp đồng!A1:X');

		define('START_ROW_INDEX', 1);
		define('START_ROW_CONTENT_INDEX', 1);

		$body 			= new Google_Service_Sheets_ValueRange([ 'values' => $suspendedValues ]);
		$rangeSuspended = 'Hosting không hợp đồng!A1:X';


        $body 	= new Google_Service_Sheets_ValueRange([ 'values' => $suspendedValues ]);
        $params = [ 'valueInputOption' => 'USER_ENTERED' ];
		$result = $service->spreadsheets_values->update(SPREADSHEET_ID, SUSPENDED_RANGE, $body, $params);

        foreach ($suspendedColumnConfig as $key => $colConfig)
        {
        	if(FALSE == $colConfig['dataValidatation']) continue;

        	$_dataValidatation = $colConfig['dataValidatation'];

        	if( ! in_array($_dataValidatation['type'], ['BOOLEAN', 'ONE_OF_LIST'])) continue;

        	$_conditions = new Google_Service_Sheets_BooleanCondition();
		    $_conditions->setType($_dataValidatation['type']);

        	if('ONE_OF_LIST' === $_dataValidatation['type'])
        	{
        		$_values = array_map(function($x){

        			$_cellData = new Google_Service_Sheets_ConditionValue();
        			$_cellData->setUserEnteredValue($x);
        			return $_cellData;

        		}, $_dataValidatation['values']);

        		$_conditions->setValues($_values);
        	}

        	$_rule = new Google_Service_Sheets_DataValidationRule();
		    $_rule->setCondition($_conditions);
		    $_rule->setInputMessage('Please set correct value');
		    $_rule->setShowCustomUi(true);

		    /* Init Range Model Object */
		    $_endRowIndex 		= count($suspendedValues);
		    $_startColumnIndex	= $key;
		    $_endColumnIndex 	= $key + 1;

        	$_range = new Google_Service_Sheets_GridRange();
			$_range->setSheetId(SUSPENDED_SHEET_ID);
		    $_range->setStartRowIndex(START_ROW_CONTENT_INDEX);
			$_range->setEndRowIndex($_endRowIndex);
			$_range->setStartColumnIndex($_startColumnIndex);
			$_range->setEndColumnIndex($_endColumnIndex);

		    $_dataValidationReq = new Google_Service_Sheets_SetDataValidationRequest();
		    $_dataValidationReq->setRule($_rule);
		    $_dataValidationReq->setRange($_range);

		    $sheetReq = new Google_Service_Sheets_Request();
		    $sheetReq->setSetDataValidation($_dataValidationReq);

		    $bodyReq = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest();
		    $bodyReq->setRequests($sheetReq);

		    $result = $service->spreadsheets->batchUpdate(SPREADSHEET_ID, $bodyReq);
        }
	}

	/**
	 * Builds a suspended hosting sheet.
	 *
	 * @param      <type>  $hostings  The hostings
	 */
	public function buildKeepHostingSheet($hostings)
	{
		$enumFieldsConfig = array(
			'domain' => [
				'order' => 0,
				'name' => 'domain',				
				'label' => 'Tên miền',
				'dataValidatation' => FALSE
			],
			'ip' => [
				'order' => 0,
				'name' => 'ip',				
				'label' => 'IP',
				'dataValidatation' => FALSE
			],
			'hasDomainContract' => [
				'order' => 0,
				'name' => 'hasDomainContract',
				'label' => 'Có HĐ Tên miền hay không ?',
				'dataValidatation' => [
					'type' => 'BOOLEAN'
				]
			],

			'domainContract' => [
				'order' => 0,
				'name' => 'domainContract',
				'label' => 'Số HĐ Tên miền',
				'dataValidatation' => FALSE
			],

			'domainContractEnd' => [
				'order' => 0,
				'name' => 'domainContractEnd',
				'label' => 'Ngày kết thúc',
				'dataValidatation' => FALSE
			],

			'hasHostingContract' => [
				'order' => 0,
				'name' => 'hasHostingContract',
				'label' => 'Có HĐ Hosting',
				'dataValidatation' => [
					'type' => 'BOOLEAN'
				]
			],

			'hostingContract' => [
				'order' => 0,
				'name' => 'hostingContract',
				'label' => 'Số HĐ Hosting',
				'dataValidatation' => FALSE
			],

			'hostingContractEnd' => [
				'order' => 0,
				'name' => 'hostingContractEnd',
				'label' => 'Ngày kết thúc',
				'dataValidatation' => FALSE
			],

			'canControlDNS' => [
				'order' => 0,
				'name'	=> 'canControlDNS',
				'label'	=> 'Tự cấu hình DNS được không ?',
				'dataValidatation' => [
					'type' => 'BOOLEAN'
				]
			],

			'customer_name' => [
				'order' => 0,
				'name' => 'customer_name',
				'label' => 'Khách hàng',
				'dataValidatation' => FALSE
			],

			'canContact' => [
				'order' => 0,
				'name'	=> 'canContact',
				'label'	=> 'Có thông tin liên hệ không ?',
				'dataValidatation' => [
					'type' => 'BOOLEAN'
				]
			],

			'callCustomerState' => [
				'order' => 0,
				'name' => 'callCustomerState',
				'label' => 'Gọi được KH không ?',
				'dataValidatation' => [
					'type' => 'ONE_OF_LIST',
					'values' => ['Chưa gọi', 'Máy bận', 'Đã gọi']
				]
			],

			'callCustomerComment' => [
				'order' => 0,
				'name' => 'callCustomerComment',
				'label' => 'Kết quả gọiTiến độ',
				'dataValidatation' => [
					'type' => 'ONE_OF_LIST',
					'values' => ['Chưa liên hệ được', 'Khách đồng ý chuyến', 'Khách không đồng ý']
				]
			],
		);



		/* DATA FOR SUSPENDED SHEET */
		$suspendedColumnConfig 	= array_values($enumFieldsConfig);
		$suspendedHeadings 		= array_column($suspendedColumnConfig, 'label');
		$suspendedValues		= array($suspendedHeadings);
		foreach ($hostings as $hosting)
		{
			$domain 				= $hosting['term_name'];
			$ip 					= $hosting['term_slug'];

			$hasDomainContract 		= null;
			if( ! empty($hosting['hostingContracts']))
			{
				$_hostingContracts = $hosting['hostingContracts'];
				$_hostingContracts = array_map(function($x){

					$x->contract_end = get_term_meta_value($x->term_id, 'contract_end');
					!empty($x->contract_end) AND $x->contract_end_date = my_date($x->contract_end);

					return $x;
				}, $_hostingContracts);

				dd($_hostingContracts);
			}

			$domainContract 		= null;
			$domainContractEnd 		= null;
			$hasHostingContract 	= null;
			$hostingContract 		= null;
			$hostingContractEnd 	= null;
			$canControlDNS 			= null;
			$customer_name 			= null;
			$canContact 			= ! empty($hosting['customer']) ? 'TRUE' : 'FALSE';
			$callCustomerState 		= null;
			$callCustomerComment 	= null;

			// // $technician 		= "Anh Trung";
			// // $processState 		= "Chờ xử lý";
			// $ip 				= $hosting['term_slug'];
			// $domain 			= $hosting['term_name'];
			// $hasDomainContract 	= !empty($hosting['domainContractId']) ? 'TRUE' : 'FALSE';

			// /* Get Contract Code If it existed */
			// $domainContractCode  = null;
			// empty($hosting['domainContractId']) OR $domainContractCode = get_term_meta_value($hosting['domainContractId'], 'contract_code');

			// $canControlDNS 	= $domainContractCode ? 'TRUE' : 'FALSE';
			// $canContact 	= ! empty($hosting['customer']) ? 'TRUE' : 'FALSE';

			// $suspendedValues[] = array(
			// 	$technician,
			// 	$processState,
			// 	$ip,
			// 	$domain,
			// 	$hasDomainContract,
			// 	trim($domainContractCode),
			// 	$hosting['customer']->display_name ?? "",
			// 	$canControlDNS,
			// 	$canContact
			// );
		}

		dd();

		// Get the API client and construct the service object.
		$client 		= $this->getClient();
		$service 		= new Google_Service_Sheets($client);
		
		define('SUSPENDED_SHEET_ID', '1683793197');
		define('SUSPENDED_RANGE', 'Hosting không hợp đồng!A1:X');

		define('START_ROW_INDEX', 1);
		define('START_ROW_CONTENT_INDEX', 1);

		$body 			= new Google_Service_Sheets_ValueRange([ 'values' => $suspendedValues ]);
		$rangeSuspended = 'Hosting không hợp đồng!A1:X';


        $body 	= new Google_Service_Sheets_ValueRange([ 'values' => $suspendedValues ]);
        $params = [ 'valueInputOption' => 'USER_ENTERED' ];
		$result = $service->spreadsheets_values->update(SPREADSHEET_ID, SUSPENDED_RANGE, $body, $params);

        foreach ($suspendedColumnConfig as $key => $colConfig)
        {
        	if(FALSE == $colConfig['dataValidatation']) continue;

        	$_dataValidatation = $colConfig['dataValidatation'];

        	if( ! in_array($_dataValidatation['type'], ['BOOLEAN', 'ONE_OF_LIST'])) continue;

        	$_conditions = new Google_Service_Sheets_BooleanCondition();
		    $_conditions->setType($_dataValidatation['type']);

        	if('ONE_OF_LIST' === $_dataValidatation['type'])
        	{
        		$_values = array_map(function($x){

        			$_cellData = new Google_Service_Sheets_ConditionValue();
        			$_cellData->setUserEnteredValue($x);
        			return $_cellData;

        		}, $_dataValidatation['values']);

        		$_conditions->setValues($_values);
        	}

        	$_rule = new Google_Service_Sheets_DataValidationRule();
		    $_rule->setCondition($_conditions);
		    $_rule->setInputMessage('Please set correct value');
		    $_rule->setShowCustomUi(true);

		    /* Init Range Model Object */
		    $_endRowIndex 		= count($suspendedValues);
		    $_startColumnIndex	= $key;
		    $_endColumnIndex 	= $key + 1;

        	$_range = new Google_Service_Sheets_GridRange();
			$_range->setSheetId(SUSPENDED_SHEET_ID);
		    $_range->setStartRowIndex(START_ROW_CONTENT_INDEX);
			$_range->setEndRowIndex($_endRowIndex);
			$_range->setStartColumnIndex($_startColumnIndex);
			$_range->setEndColumnIndex($_endColumnIndex);

		    $_dataValidationReq = new Google_Service_Sheets_SetDataValidationRequest();
		    $_dataValidationReq->setRule($_rule);
		    $_dataValidationReq->setRange($_range);

		    $sheetReq = new Google_Service_Sheets_Request();
		    $sheetReq->setSetDataValidation($_dataValidationReq);

		    $bodyReq = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest();
		    $bodyReq->setRequests($sheetReq);

		    $result = $service->spreadsheets->batchUpdate(SPREADSHEET_ID, $bodyReq);
        }
	}

	/**
	 * Builds a suspended hosting sheet.
	 *
	 * @param      <type>  $hostings  The hostings
	 */
	public function buildSuspendedHostingSheet($hostings)
	{
		$enumFieldsConfig = $this->getEnumFieldsConfig();

		/* DATA FOR SUSPENDED SHEET */
		$suspendedColumnConfig 	= array_values($enumFieldsConfig);
		$suspendedHeadings 		= array_column($suspendedColumnConfig, 'label');
		$suspendedValues		= array($suspendedHeadings);

		if($suspendedContracts = array_filter($hostings, function($x){ return $x['term_status'] == 'suspended'; }))
		{
			foreach ($suspendedContracts as $hosting)
			{
				$technician 		= "Anh Trung";
				$processState 		= "Chờ xử lý";
				$ip 				= $hosting['term_slug'];
				$domain 			= $hosting['term_name'];
				$hasDomainContract 	= !empty($hosting['domainContractId']) ? 'TRUE' : 'FALSE';

				/* Get Contract Code If it existed */
				$domainContractCode  = null;
				empty($hosting['domainContractId']) OR $domainContractCode = get_term_meta_value($hosting['domainContractId'], 'contract_code');

				$canControlDNS 	= $domainContractCode ? 'TRUE' : 'FALSE';
				$canContact 	= ! empty($hosting['customer']) ? 'TRUE' : 'FALSE';

				$suspendedValues[] = array(
					$technician,
					$processState,
					$ip,
					$domain,
					$hasDomainContract,
					trim($domainContractCode),
					$hosting['customer']->display_name ?? "",
					$canControlDNS,
					$canContact
				);
			}
		}

		// Get the API client and construct the service object.
		$client 		= $this->getClient();
		$service 		= new Google_Service_Sheets($client);
		
		define('SUSPENDED_SHEET_ID', '1683793197');
		define('SUSPENDED_RANGE', 'Hosting không hợp đồng!A1:X');

		define('START_ROW_INDEX', 1);
		define('START_ROW_CONTENT_INDEX', 1);

		$body 			= new Google_Service_Sheets_ValueRange([ 'values' => $suspendedValues ]);
		$rangeSuspended = 'Hosting không hợp đồng!A1:X';


        $body 	= new Google_Service_Sheets_ValueRange([ 'values' => $suspendedValues ]);
        $params = [ 'valueInputOption' => 'USER_ENTERED' ];
		$result = $service->spreadsheets_values->update(SPREADSHEET_ID, SUSPENDED_RANGE, $body, $params);

        foreach ($suspendedColumnConfig as $key => $colConfig)
        {
        	if(FALSE == $colConfig['dataValidatation']) continue;

        	$_dataValidatation = $colConfig['dataValidatation'];

        	if( ! in_array($_dataValidatation['type'], ['BOOLEAN', 'ONE_OF_LIST'])) continue;

        	$_conditions = new Google_Service_Sheets_BooleanCondition();
		    $_conditions->setType($_dataValidatation['type']);

        	if('ONE_OF_LIST' === $_dataValidatation['type'])
        	{
        		$_values = array_map(function($x){

        			$_cellData = new Google_Service_Sheets_ConditionValue();
        			$_cellData->setUserEnteredValue($x);
        			return $_cellData;

        		}, $_dataValidatation['values']);

        		$_conditions->setValues($_values);
        	}

        	$_rule = new Google_Service_Sheets_DataValidationRule();
		    $_rule->setCondition($_conditions);
		    $_rule->setInputMessage('Please set correct value');
		    $_rule->setShowCustomUi(true);

		    /* Init Range Model Object */
		    $_endRowIndex 		= count($suspendedValues);
		    $_startColumnIndex	= $key;
		    $_endColumnIndex 	= $key + 1;

        	$_range = new Google_Service_Sheets_GridRange();
			$_range->setSheetId(SUSPENDED_SHEET_ID);
		    $_range->setStartRowIndex(START_ROW_CONTENT_INDEX);
			$_range->setEndRowIndex($_endRowIndex);
			$_range->setStartColumnIndex($_startColumnIndex);
			$_range->setEndColumnIndex($_endColumnIndex);

		    $_dataValidationReq = new Google_Service_Sheets_SetDataValidationRequest();
		    $_dataValidationReq->setRule($_rule);
		    $_dataValidationReq->setRange($_range);

		    $sheetReq = new Google_Service_Sheets_Request();
		    $sheetReq->setSetDataValidation($_dataValidationReq);

		    $bodyReq = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest();
		    $bodyReq->setRequests($sheetReq);

		    $result = $service->spreadsheets->batchUpdate(SPREADSHEET_ID, $bodyReq);
        }
	}



	/**
	 * Gets the enum fields configuration.
	 *
	 * @return     <type>  The enum fields configuration.
	 */
	public function getEnumFieldsConfig()
	{
		return array(
			'technician' => [
				'order' => 0,
				'name' => 'technician',
				'label' => 'Kỹ thuật Phụ trách',
				'dataValidatation' => FALSE
			],
			'processState' => [
				'order' => 0,
				'name' => 'processState',
				'label' => 'Tiến độ',
				'dataValidatation' => [
					'type' => 'ONE_OF_LIST',
					'values' => ['Chờ xử lý', 'Đang xử lý', 'Hoàn thành']
				]
			],
			'domain' => [
				'order' => 0,
				'name' => 'domain',				
				'label' => 'Tên miền',
				'dataValidatation' => FALSE
			],
			'ip' => [
				'order' => 0,
				'name' => 'ip',				
				'label' => 'IP',
				'dataValidatation' => FALSE
			],
			'hasDomainContract' => [
				'order' => 0,
				'name' => 'hasDomainContract',
				'label' => 'Có HĐ Tên miền',
				'dataValidatation' => [
					'type' => 'BOOLEAN'
				]
			],
			'contract_code' => [
				'order' => 0,
				'name' => 'contract_code',
				'label' => 'HĐ Tên miền',
				'dataValidatation' => FALSE
			],
			'customer_name' => [
				'order' => 0,
				'name' => 'customer_name',
				'label' => 'Khách hàng',
				'dataValidatation' => FALSE
			],
			'canControlDNS' => [
				'order' => 0,
				'name'	=> 'canControlDNS',
				'label'	=> 'Control DNS',
				'dataValidatation' => [
					'type' => 'BOOLEAN'
				]
			],
			'canContact' => [
				'order' => 0,
				'name'	=> 'canContact',
				'label'	=> 'Control DNS',
				'dataValidatation' => [
					'type' => 'BOOLEAN'
				]
			]
		);
	}

	/**
	 * Gets the hosting raw.
	 *
	 * @return     array  The hosting raw.
	 */
	public function getHostingRaw()
	{
		$result = $this->scache->get("tool/hosting_raw");
		if( ! empty($result) 
			/*&& 1==2*/
		) return $result;

		$hostingOnServer = [
			[ 'term_name' => "45.124.94.250", 'term_slug' => "academy-tamviethouse.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "aegvietnam.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "alphahil.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "amthucquenha.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "ankyfurni.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "azgold.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "bachviet.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "batdongsangroup.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "bcm.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "binbong.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "bms-vn.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "boodoor.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "captainland.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "charmhouse.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "cms.goldenlotustravel.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "congtymaysaigon1.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "danhviet1.250.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "danhvietholding.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "danhvietholding.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "datquancomputer.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "document.webdoctor.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "dongnamanhompmi.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "dongphuccodoc.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "dons.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "event.buffettfx.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "fioreresort.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "giatamjsc.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "gigotoys.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "giotsuayeuthuong.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "goldenholiday.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "goldenlotustravel11.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "goldenlotustravel.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "grandworldphuquoc.sieuthi-bds.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "haisanhaiau.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "haisanhuongngoc.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "hdtrans.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "hieuanh.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "hoanggiaauto.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "hocvientaichinhbaohiem.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "hs-decor.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "huongquefood.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "idol.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "iwatch.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "jkcometics.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "kenhmuanha.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "kimtingoxanh.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "kimtingroup.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "knp-architects.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "lanhodiepdalat.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "lenze.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "lotusasean.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "mautk.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "maybocgovn.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "miraie.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "monngonvietdalat.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "ngocbach.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "nguyenthanhphat.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "nguyentrungnhan.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "nhalocphat.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "nhangsen.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "nhathongminhsp.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "nhatmsp.250.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "nhattienthanh.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "noithathuybich.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "noithatnhumai.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "nuoclavie.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "oceanzenspadanang.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "optimize.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "phongthuyasian.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "pipes.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "pokids.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "quangcaomaiphong.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "quangtrancenter.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "rivierapoint-quan7.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "roki-trading.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "saigonsolar.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "saoanlac.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "satthephcm.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "satthepxuyenlocphat688.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "setup.adsplus.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "shopphuquoc.net", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "suachuadienlanhdientu.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "summer.aegvietnam.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "sumostore.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "swisstoucheslaluna.org", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "tach.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "thamtutamduc.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "thangmayvolkslift.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "thietbimamnonanhduong.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "thlplongphuoc.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "tienthinhgarden.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "toanthang-steel.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "tranvufire.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "trieunhatgroup.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "trungtamviethan.edu.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "truongtaynama.edu.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "universegroup.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "vanphatriverside.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "vayvon.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "vayvontieudunghcm.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "veros.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "vietanhoptic.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "vietnamrobovac.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "vinhomesgrandpark-hcm.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "viralblog.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "viralcontentsaz.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "viralquiz.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "voxehoanthanh.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "webdoctor.250.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "webmau.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "xaydungtogia.250.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.250", 'term_slug' => "xkldnhatbandailoan.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "acbland.net", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "ahomesaigon.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "anhemgianghiep.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "artwoodvn.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "beautycarexpo.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "bizguruedu.103.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "camranhcitygates.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "canhoquan7.103.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "chuquan.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "congnong.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "datvangsg.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "datnengiarephuquoc.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.15", 'term_slug' => "diaoctrananhland.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "dodongcongvan.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "dongduong-ate.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "drmichaels.103.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "drpluscell.103.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "duanvincity-hcm.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "emigroup.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "europestcontrol.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "fivestarecocity.103.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "foodtech-vn.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "fujiocha.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "giaminhthaoduoc.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "giaydantuongnhaxinh.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "guru.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "heroart.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "hiring.adsplus.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "homelandvietnam.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "huongdan.1web.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "hyundaibinhtan.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "icfairvn.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "icondecor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "ifashionup.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "kdcphumy.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "kimtingoxanh.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "leads.103.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "lmart.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "luckyhometel.103.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "mayhanprotech.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "melodyquinhon.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "nanolift.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "nguyenhaideco.103.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "nhasachtanphu.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "noithathoanggiatrang.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "norijapanshop.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "nuocsuoilavie.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "nuoctayvanphuong.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "nuocuong.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "nuocuongdh.webdoctor.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "nuocvinhhao.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "nuocvinhhao.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "nvi.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "paradoxvn.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "pearlriverside.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "phelieugiaminhphat.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "pnreal.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "quatangdongphong.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "remcuaphuquy.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "rotuven.103.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "safirahcm.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "satthep247.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "semitec.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "sishibaby.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "sonnuocbinhminh.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "sundecor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "swanpark-nhontrach.net", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "tapdoandiaochungthinh.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "test.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "thangmayhoangtrieu.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "thangmayhoangtrieu.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "thangmayhoangtrieu.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "thienhoagroup.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "thietbimamnontiendat.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "thuexedn.103.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "timstudio.org", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "traioidecor.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "tranenpt.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "tranhdantuong.net.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "trivina.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "tuanphongmaps.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "vachnganminhlong.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "vhnthcm.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "victoria.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "vingroupphuquoc.org", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "waterlavie.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "waterlavie.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "watervinhhao.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "watervinhhao.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "watervinhhao.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.157.13", 'term_slug' => "xaydungdonghung.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "academyspa.edu.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "adnlabo.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "adsplus.demo-kei.site", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "adult.amazonlearning.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "aeg.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "alidoor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "alvaplaza.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "amazonlearning.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "androidxehoi.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "anhhieufashion.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "anhnguuniversal.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "bakerland.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "baohiemnhanthohn.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "batdongsanecoland.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "bdskimoanh.info", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "bencami.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "biwase-iongold.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "bookingdalat24h.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "botanica-premier.net", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "bs-home.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "canhoasahi.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "canhopicty.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "canhothemarq.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "cattuonghungphat.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "chambeauty.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "charmandchill.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "cutipa.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "dailyhaiphongford.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "datnenthanhdo.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "dehome.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "diaoctrananhreal.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "dichvudienlanhbachkhoa.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "dichvuketoansaomai.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "dinhbao.net", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "dodahuyhieu.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "dsseducation.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "duanlongthanhcentral.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "duan-nhonhoinewcity.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "ducthanhsteel.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "dulichbienxanhquinhon.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "duoccaoson.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "duoccs.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "eduline.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "edupath.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "ellycosmetics.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "extra.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "fuji-housing.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "gachchiulua.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "gamuda-gardens.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "geengreenled.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "generali.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "giamcanmaxslim7days.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "goldenmall.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "grandpatea.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "grandworldphuquoc.top", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "greenfire.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "groupbatdongsansaigon.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "handicraftsafimex.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "hdcontrol.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "hdrustique.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "hdtrans.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "healthyno1.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "hethonggiaoducucchaumontessori.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "hinode1.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "hinode.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "hugme.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "hyundai-quan5.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "incheng.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "inhopgiaysachoa.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "inhuyhoang.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "ipadpro.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "iphonepro.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "jobway.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "kbsmart.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "kechocolle.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "kimmocsac.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "labtht.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "laimiancity.vip", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "landing.caramelenglish.edu.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "lebordeaux.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "limousinetuananh.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "macbookcu.128.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "maybocsofa.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "maylanhhitachi.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "maylanhnhatban.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "minhthienthanh.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "miocen.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "moxavietnam.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "naikatsu.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "namcung.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "nhadathophung.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "nhadatnhatrangonline.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "nhahangbienrung.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "nhauthaipurtier.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "noithatbinhduong.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "noithatbinhduong.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "noithatbinhduong.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "noithatgiasi.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "noithatgominhtuan.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "noithatnhatvuong.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "noithatxuavanay1368.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "nuocsuoilavie.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "otomazdavietnam.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "otomiennam.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "otomn.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "phasetimes.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "phongkhamsanphukhoacongdong.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "phukienminhhieu.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "phuongminh.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "phuquoc.tourcano.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "picity.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "poki.128.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "preorder.jejucherryblossom.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "ptviet.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "redpineinternational.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "rocomedia.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "ruoungoainhap.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "rusimama.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "saigontaichinh.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "saonamlogistics.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "senhongspa.128.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "skye.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "sonluxsen.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "stage.bencami.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "subarubinhduong.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "subarubinhtrieu.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "sunshinediamondrivers.sieuthi-bds.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "sunshinediamondrivers.thuecanhohcm.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "suzukitaynguyen.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "swp.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "tamhongphuc.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "tatosa.edu.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "tdarch.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "teogiaygiabeo.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "thammyvienmrhyun.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "thaoduoc.128.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "thiepcuoicloud.net", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "thietbicokhitranhuynh.com", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "thuecanhohcm.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "tmdecorvip.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "tramhuongvangia.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "trangtrinoithatbinhduong.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "trangtrinoithatbinhduong.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "trangtrinoithatbinhduong.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "travelmemoirsint.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "tuvanbatdongsan.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "vanphatriverside.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "vatlieumai.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "vaynhanhhn.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "vaytientragop.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "vdc.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "vietphatmachines.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "vongxoay.bgv.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "windowsconcept.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "xeford247.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "yay.toys", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "yensaovietnam-vietnest.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "yepbyalmaz.com", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "zaaz.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "zing2.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "zing.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "zozoielts.128.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "45.124.94.128", 'term_slug' => "4gviettelsieutoc.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "addtechv2.84.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "ads2business.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "adsbestvn.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "adsfreevn.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "adsonl.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "adsonlvn.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "adsprovina.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "akibavietnam.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "almazweddings.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "ancc.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "api.sccom.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "baobivanphat.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "baohiemmanulifehanoi.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "baohiemmanulifeuytin.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bdskhaihung.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bdsthienhuong.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bepro.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "best4digitalmarketing.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bestemailvn.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bestweb4ads.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bestwebvie.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bietthubienhalong.net", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bietthusinhthaikhudong.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bizgift.84.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bluestudioss.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bokhochiba.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bominsurance.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "bonrau.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "buctuong.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "cameragiarebinhdinh.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "canhobienthanhlongbay.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "canhotm.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "cbcworkshop.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "cdn.bizgift.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "charmcity-bd.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "charmeperfumevietnam.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "chauhoaihao.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "chungcu-goldmarkcity.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "chvpro.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "clairexpress.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "continental.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "cuacuontoanphat.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "cuagohaiduong.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "dailygachngoi24h.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "dangtnk032.84.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "dannjin.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "datcuchi.tanphuland.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "dathoaonlinehanoi.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "dautuvungven.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "debasse.co", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "diaocphucloctho.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "dienthoaichinhhang.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "dientubavi.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "diephoa.asia", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "dieutrituki.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "dieutrituky.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "digitalbestvn.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "digitalcare.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "digitalmarb2b.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "digitalmarketing4business.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "dimarketingtool.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "doanthekhiem.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "donghovantrung.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "dongnamvina.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "duan-aquacity.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "duyhungdaonguyen.84.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "emailonlbuilder.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "emailonlbusiness.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "emailonl.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "emailonlfree.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "emailonlmanager.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "emailonlmarket.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "emailonlsignature.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "emailonltemplate.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "facebookadstech.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "felicegroup.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "flashsalesvietnam.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "giamcandongythai.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "goldenunion.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "good4digitalmarketing.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "googleadstech.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "gouttobed.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "hangraoluoithepmakem.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "heradg.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "hikaru.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "hqavietnhat.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "huyenxinhdep1.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "huyenxinhdep2.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "huyenxinhdep.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "inboundmar.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "inboundmarvn.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "kbsmart.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "khoancatbetonghanoigiare.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "khuongpham.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "ksoc.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "lamvinhland.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "lavastone.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "livingartdecor.com.au", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "longhaibaove.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "luatdinhcanh.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "luattriduc.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "luengyueng.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "lugishoes.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "magicalmagnet.net", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "mau.1web.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "maydetpamick.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "maynghienquelam.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "mayphunsuongnd.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "meyhomespq.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "mynaevent.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "ngoctrangdental.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "nhadatmau2.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "nhadatmau.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "nhahangtanhuyen.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "nhakhoashiny.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "nhakhoataman.net", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "nhasachsomot.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "nhathuocminhhong.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "nhatthangaudio.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "nhunghuoutot.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "noithatphongnam.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "nonbaohiemgiare.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "nova-loyalty.net", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "pearl-riverside.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "peer2peervn.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "pgsm.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "phanboncholan.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "phucanbake.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "phuongdai.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "phusang.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "pickme.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "quanaoxuatkhaudunghang.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "saigongardenriversidevilla.net", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "saigonti2s.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "saigontire.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "sfitbodies.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "simsodepvnn.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "sme.adsplus.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "socnauhomedecor.84.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "sonelviss.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "static.bepro.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "sunshinedaotri.84.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "sunshinehorizon.info.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "sunshinesgruop.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "tailieu.doanthekhiem.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "teccogarden.net", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "test.bepro.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thammyviensaigonangel.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thamtulacviet.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thanhlongbay-binhthuan.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thanhlongbaybt.84.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thanhlongbaybt.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thanhmochuongtb.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "theflowerlab.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thegioitamphonghan.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thesailingbayninhchu.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thietbinhapkhaunhatban.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thietbinhapkhaunhatban.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thietbivesinhyamato.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thiphongnb.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thoitrangmia.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thongtaccongnghet.net", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "thuocdongyminhhong.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "tienphongteambuilding.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "tokyohome.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "tpcn-herbalife.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "trambetongvinhsinh.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "trangtraicho.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "tuekhanh.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "tuvinhatnguyet.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "university.guru.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "uradecor.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "vanchuyenhangkhong.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "vaytiennhanhtphcm.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "viendonghomeshopping.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "vietsagri.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "vinfast-chevroletthanglonghanoi.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "vinhthanhgroup.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "xaydunghoaihuy.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "xeotochinhchu.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "yendtn.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "yensaovietnam-vietnest.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "yukiskin.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "1ad.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "1skilldigitalmarketing.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "2snoingoaithat.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "4marketingvn.com", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "4rau.com", 'term_status' => "suspended" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "15162.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "15210.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "15224.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "15371.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "103.56.156.84", 'term_slug' => "15377.84.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "aacyprus.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "adongvietnam.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "akinterior.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "artlands.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "baogiaweb.webdoctor.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "baominh-tatsu.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "baove911.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "bbfurniture.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "bmwphumyhung.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "camranhknparadise.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "canadianpowermfr.ca", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "chvpro.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "cokhimetal.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "ctjamhome.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "dacsannongsantaynguyen.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "denhatthachcao.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "diaockimcuong.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "doanhnhandatviet.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "event.adsplus.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "giaxeoto5s.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "goinuocuong.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "hanoihousing.net", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "hoatuoikimanh.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "hotronguoibenh.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "hyundai-truongchinh.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "iesedu.78.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "i-green.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "kimtingroup.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "minhhoangan.edu.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "muabanlaptopcu.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "myphamthanhlan.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "nhacongnghe.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "nhatquan.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "nhon.78.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "nhuabaominh.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "nissanmotor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "nissan-saigon3s.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "noithat.78.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "noithatanhduc.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "nuocsuoivinhhao.org", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "nuocvinhhao.org", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "nxbphunu.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "phulieuphutungmay.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "polyurethanepaint.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "quangphuong.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "quatangdongphong.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "shoptrongnghia.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "suakhoakhanhdong.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "subaruvietnam.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "sunshinesaigon.78.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "taonhat.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "thachhoavien.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "thanglongluxuryhome.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "thebankplus.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "thephoihanh.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "thuanphuong.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "thumuaxecu.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "tina-ins.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "tramhuongthaigia.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "tulipspa.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "tulipvietnam.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "vaygopngayhcm.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "vietnamhobby.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "vimetechauto.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "webdoctor.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "webmau.78.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "xaydungphongthuy.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "xuansoncamera.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "ziczacleather.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.78", 'term_slug' => "1web.asia", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "abs-vn.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "agabang.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "bancanho24h.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "bangkinhdailoc.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "bangtaixuantruong.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "benhvienmat.182.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "benhvienmat.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "canadian.182.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "caogungtanmo.yornmi.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "catgiacong.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "chovaytiennong24h.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "chuathanhlam.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "dangngocspa.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "deasystem.182.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "donghodaiduong.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "duanecogreenq7.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "duonghieustone.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "dxa.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "ecohoguom.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "eurostorages.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "free.webdoctor.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "giammo.tamspaclinic.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "grlogs.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "grsupplier.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "haphongtech.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "hatgiongthanhnong.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "haushop.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "hoaniencalendar.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "hoatuoidalatthao.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "hocmoingay.org", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "ieslanding.182.webdoctor.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "immica.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "investo.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "job.adsplus.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "keobaychuot.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "khudothiphucancity.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "kovipaint.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "lamasg.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "lamasg.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "lisamedibeauty.182.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "luan.webdanhba.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "mekongplastic.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "midanshop.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "momkidsworld.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "nemdaotominhthao.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "mythuatcodo.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "nichinest.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "noithatvip41.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "osakiglobal.182.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "phannguyenmedela.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "phongkhamlaophoi.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "phuonghoangroup.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "pmac.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "rainbowfurniture.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "readinggate.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "remcuatanphat.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "senvoigiare.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "shanghaiagri.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "steam.aegvietnam.edu.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "suachuanhagiarehuyhoang.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "suzukiphoquang.net", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "taijutsu.182.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "tamviethouse.182.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "tanngocphat.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "thanglongjs.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "the-doanh-nhan-apec.vivabcs.com.vn", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "theducbangtam.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "thietbidienhanoi.182.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "thietkenoithatsunwahpearl.com", 'term_status' => "active" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "tieudungchatluong.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "tramhuongthaigia.com", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "tuan.graphics", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "uudai.guru.edu.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "vinhomegoldenriver.net", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "viva1.182.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "viva.182.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "viva2.182.webdoctor.com.vn", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "vongtronxanh.com.vn", 'term_status' => "active" ],

			[ 'term_name' => "171.244.43.182", 'term_slug' => "zest.academy", 'term_status' => "suspended" ],
			[ 'term_name' => "171.244.43.182", 'term_slug' => "9cloud.site", 'term_status' => "active" ],
		];

		$contracts = $this->contract_m
		->select('term_id, term_name, term_type, term_status')
		->where_in('term_type', [ $this->hosting_m->term_type, $this->domain_m->term_type ])
		->where('term_status', 'publish')
		->order_by('term_id', 'desc')
		->get_all();

		if( ! empty($contracts))
		{
			foreach ($contracts as &$_contract)
			{
				$_contract->contract_end 		= null;
				$_contract->contract_enddate 	= null;

				$contract_end = get_term_meta_value($_contract->term_id, 'contract_end');
				if( ! empty($contract_end))
				{
					$_contract->contract_end = $contract_end;
					$_contract->contract_enddate = my_date($contract_end);
				}

				$_isValidDNS 	= FALSE;
				$_realIP 		= FALSE;

				$_realIP = $this->scache->get("tool/real_ip/{$_contract->term_name}");
				if(empty($_realIP))
				{
					$_realIP = gethostbyname($_contract->term_name);
					$this->scache->write($_realIP, "tool/real_ip/{$_contract->term_name}");
				}

				$_isValidDNS = $_realIP == $_contract->term_name ? FALSE : TRUE;

				$_contract->isValidDNS = $_isValidDNS;
				$_contract->realIP = $_isValidDNS ? $_realIP : null;
				$_contract->isExpired = $_contract->contract_end < time();
			}
		}


		$contracts = array_group_by($contracts, 'term_name');
		foreach ($hostingOnServer as &$item)
		{
			$item['ip_to'] = $this->config->item($item['term_name'], 'transfer_ip');

			$item['customerDateMustChaged'] = $this->config->item('customerDateMustChaged', 'defaultValues');
			$item['adsplusDateTransferd'] = $this->config->item('adsplusDateTransferd', 'defaultValues');
			$item['customerDateChaged'] = $this->config->item('customerDateChaged', 'defaultValues');

			$item['contracts'] 	= [];
			$item['hostings'] 	= [];
			$item['domains'] 	= [];

			$cluster = $this->config->item($item['term_name'], 'clusters');

			$item['isKeepCluster'] = $item['term_name'] == $cluster['ip'];
			$item['provider'] =	$cluster['provider'];

			if(empty($contracts[$item['term_slug']])) continue;

			$item['contracts'] = $contracts[$item['term_slug']];

			$_typeContracts 	= array_group_by($contracts[$item['term_slug']], 'term_type');
			$item['hostings'] 	= $_typeContracts['hosting'] ?? [];
			$item['domains'] 	= $_typeContracts['domain'] ?? [];
		}

		$provider_sorted = array_column($hostingOnServer, 'provider');
		array_multisort($provider_sorted, SORT_DESC, $hostingOnServer);

		$this->scache->write($hostingOnServer, "tool/hosting_raw");
		return $hostingOnServer;
	}

	public function quickStart()
	{
		// Get the API client and construct the service object.
		$client = $this->getClient();
		$service = new Google_Service_Sheets($client);

		$spreadsheetId = '1-W_Co3s3txLotqUOuCeVcetyO-dIeAPyJZWqsBK2GaM';

		$range 	= 'File tổng!A2:E';
		$values = [
	    [
	        // Cell values ...
	    ],
		    // Additional rows ...
		];
		$body = new Google_Service_Sheets_ValueRange([
		    'values' => $values
		]);
		$params = [
		    'valueInputOption' => $valueInputOption
		];
		$result = $service->spreadsheets_values->update($spreadsheetId, $range,
		$body, $params);

		$response = $service->spreadsheets_values->get($spreadsheetId, $range);
		$values = $response->getValues();

		if (empty($values)) {
		    print "No data found.\n";
		}
		else
		{
		    print "Name, Major:\n";
		    foreach ($values as $row) {
		        // Print columns A and E, which correspond to indices 0 and 4.
		        printf("%s, %s\n", $row[0], $row[4]);
		    }
		}
	}

	/**
	* Returns an authorized API client.
	* @return Google_Client the authorized client object
	*/
	protected function getClient()
	{
		$client = new Google_Client();
		$client->setApplicationName('Google Sheets API PHP Quickstart');
		$client->setScopes(Google_Service_Sheets::SPREADSHEETS);
		// $client->setAuthConfig($credentialConfig);
		$client->setAccessType('offline');
		$client->setPrompt('select_account consent');
		$client->setClientId('1066583327968-ecb3354ljlbob8d0q7r5i0h7agrgrl12.apps.googleusercontent.com');
		$client->setClientSecret('VEvq-G2HukvHfWPUhhnGpp1j');
		$client->setRedirectUri('http://dev.erp.webdoctor.vn/test/getClient');

		$client->fetchAccessTokenWithRefreshToken("1//0edra-jIJELSCCgYIARAAGA4SNwF-L9IrorSSEHhhtwf4MJ8H0eGCrWV6AsIqDUJGMzTqXryO-4D_A6J6P2Gm4o8LG_fgp5OBRWc");
		return $client;
	}
}
/* End of file GoogleSheetApi.php */
/* Location: ./application/modules/Tool/controllers/GoogleSheetApi.php */