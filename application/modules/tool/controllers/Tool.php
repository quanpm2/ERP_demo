<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tool extends Admin_Controller {

	public $model = '';

	function __construct()
	{
		parent::__construct();
		// restrict('Admin.Tool');
	}
	
	public function index($tpl_type = 'demo_adsplus_5')
	{
		$this->adsplus_sms_submit();
		$data = $this->data;

		$this->config->load('sms_template');
		$data['default_tpl_type'] = $this->config->item('adwords_workshop_2');

		$data['tpl_type'] = $tpl_type;
		$data['tpl_content'] = $this->config->item($tpl_type,'adwords_workshop_2');

		$log_numbers = $this->scache->get("tool/{$tpl_type}_adwords_workshop_2_sms_logs");
		if(empty($log_numbers)) 
		{
			return parent::render($data,'adsplus_sms');
		}

		usort($log_numbers, function($a,$b){ return ($a['status'] < $b['status'] ? 1 : -1); });

		$this->table->set_heading('#','Số điện thoại','Thời gian','Trạng thái','Log');
		$i = 0;
		foreach ($log_numbers as $row)
		{
			$sms_status = $row['status'];
			switch ($sms_status) {
				case 1:
					$sms_status = 'Đã gửi';
					break;

				case 0:
					$sms_status = 'Chưa gửi được';
					break;
			}

			$send_on = !empty($row['send_on']) ? my_date($row['send_on'],'Y/m/d H:i:s') : '--';

			$this->table->add_row((++$i),$row['phone'],$send_on,$sms_status,$row['message']??'---');		
		}

		$data['logs_content'] = $this->table->generate();

		parent::render($data,'adsplus_sms');
	}

	private function adsplus_sms_submit()
	{
		$post = $this->input->post();
		if(empty($post)) return FALSE;

		if($this->input->post('reload') != NULL)
		{
			$tpl_type = $post['tpl_type'] ?: 'demo_adsplus_5';
			$log_numbers = $this->scache->get("tool/{$tpl_type}_adwords_workshop_2_sms_logs") ?: array();				

			$post['phones'] = explode(';', $post['phones']);
			foreach ($post['phones'] as $p) 
			{
				$p = preg_replace('/[^0-9]/s', '',$p);
				if(empty($p)) continue;

				$numbers[] = $p;
			}

			if(empty($numbers)) 
			{
				$this->messages->error('Dữ liệu nạp vào không hợp lệ !!!');
				redirect(module_url("index/$tpl_type"),'refresh');
			}

			foreach ($numbers as $i => $number) 
			{
				if(isset($log_numbers[$number])) continue;

				$log_numbers[$number] = ['phone'=>$number,'send_on'=>0,'status'=>0,'message'=>'Đang chờ gửi'];
			}

			$this->scache->write($log_numbers, "tool/{$tpl_type}_adwords_workshop_2_sms_logs");
			redirect(module_url("index/$tpl_type"),'refresh');
		}

		if($this->input->post('btn_send') !== NULL)
		{
			$tpl_type = $post['tpl_type'] ?: 'demo_adsplus';
			$log_numbers = $this->scache->get("tool/{$tpl_type}_adwords_workshop_2_sms_logs");
			if(empty($log_numbers))
			{
				$this->messages->error('Danh sách rỗng . Vui lòng "NẠP SỐ ĐIỆN THOẠI" trước khi thực hiện tác vụ này !!!');
				redirect(module_url("index/$tpl_type"),'refresh');
			}

			$unsend_logs = array();
			if($tpl_type == 'demo_adsplus')
			{

				$unsend_logs = array();
				foreach ($log_numbers as $row)
				{
					if($row['status'] == 1) continue;

					$unsend_logs[$row['phone']] = $row;
				}
			}
			else
			{
				$unsend_logs = $log_numbers;
			}


			if(empty($unsend_logs))
			{
				$this->messages->error('Không có số điện thoại nào đang trong trạng thái "CHỜ GỬI" !!!');
				redirect(module_url("index/$tpl_type"),'refresh');
			}

			$this->load->library('sms');
			$sms_content = $post['content'];
			if(empty($sms_content)) 
			{
				$this->messages->error('SMS chưa có nội dung');
			}

			// $sms_content = 'KQ:04/04 - ***anhtai.vn: click: 125, click ko hop le: 4.Thong tin vui long LH-Ky Thuat: Mr.Huy, huyth1@adsplus.vn, 0964445877';

			$default_status_code = array('not found','0','511','304','253','356','357','360','359','392','393','394','395','396','397','398','399','509');

			foreach ($unsend_logs as $phone => $row) 
			{
				// $result = $this->sms->fake_send($phone,$sms_content,$default_status_code[array_rand($default_status_code,1)]);
				$result = $this->sms->send($phone,$sms_content);

				$log_numbers[$phone]['send_on'] = time();
				if($result == 'No recipients')
				{
					unset($log_numbers[$phone]);
					continue;
				}

				$result = reset($result);
				$send_code = $result['response']->SendSMSResult;
				
				$log_numbers[$phone]['status'] = 0;
				if($send_code == 0)
				{
					$log_numbers[$phone]['status'] = 1;
				}

				$log_numbers[$phone]['message'] = $send_code.' : '.$result['config']['sms_status'][$send_code];
			}

			$this->scache->write($log_numbers, "tool/{$tpl_type}_adwords_workshop_2_sms_logs");

			$this->messages->success('Hệ thống xử lý thành công');
			redirect(module_url("index/$tpl_type"),'refresh');
		}

		if($this->input->post('btn_clear') !==  NULL)
		{
			$tpl_type = $post['tpl_type'] ?: 'demo_adsplus';
			$this->scache->delete("tool/{$tpl_type}_sale_in_covid_workshop_sms_logs");
			$this->messages->success('Đã xử lý tác vụ.');
		}
	}
}
/* End of file Tool.php */
/* Location: ./application/modules/Tool/controllers/Tool.php */