<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cache extends REST_Controller
{
    /**
	 * CONSTRUCTION
	 *
	 * @param      string  $config  The configuration
	 */
	function __construct($config = 'rest')
	{
		parent::__construct($config);

		header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") die();

		if($this->input->method() === 'options') parent::response('ok', parent::HTTP_OK);

		require_once(APPPATH.'third_party/cms-core/libraries/Core.php');
        require_once(APPPATH.'third_party/cms-core/libraries/Package.php');
        require_once(APPPATH.'third_party/cms-core/libraries/Post_type.php');
        $this->load->add_package_path(APPPATH.'third_party/erp-core/');

        $this->load->model('term_posts_m');
        $this->load->model('term_users_m');
	}

    public function clear_put()
    {
        $args = parent::put(null, TRUE);
        
        if( ! empty($args['termIds']))
        {
            $termIds = $args['termIds'];
            is_array($termIds) OR $termIds = [ $termIds ];
            array_walk($termIds, function($x){ $this->delete_term_cache($x); });
        }

        if( ! empty($args['postIds']))
        {
            $postIds = $args['postIds'];
            is_array($postIds) OR $postIds = [ $postIds ];
            array_walk($postIds, function($x){ $this->delete_post_cache($x); });
        }

        if( ! empty($args['userIds']))
        {
            $userIds = $args['userIds'];
            is_array($userIds) OR $userIds = [ $userIds ];
            array_walk($userIds, function($x){ $this->delete_user_cache($x); });
        }

        if( ! empty($args['optionKeys']))
        {
            $optionKeys = $args['optionKeys'];
            is_array($optionKeys) OR $optionKeys = [ $optionKeys ];
            array_walk($optionKeys, function($x){ $this->delete_option_cache($x); });
        }

        parent::response('ok', '200');
    }

    /**
     * @param int $termId
     * 
     * @return [type]
     */
    protected function delete_term_cache(int $termId)
    {
        $cache_paths = [
            "term/{$termId}_meta.cache",
            "term/{$termId}_posts",
        ];

        $postIds = $this->term_posts_m->select('post_id')->where('term_id', $termId)->get_all();
        $cache_paths = wp_parse_args(array_map(function($x){
            return "post/{$x->post_id}_terms_";
        }, $postIds), $cache_paths);

        $userIds = $this->term_users_m->select('user_id')->where('term_id', $termId)->get_all();
        $userIds AND $cache_paths = wp_parse_args(array_map(function($x){
            return "user/{$x->user_id}_terms";
        }, $userIds), $cache_paths);

        $cache_paths = array_unique($cache_paths);

        foreach($cache_paths as $key_cache)
        {
            if(strpos('.cache', $key_cache)) $this->scache->delete($key_cache);
            else $this->scache->delete_group($key_cache);
        }
    }

    protected function delete_post_cache($postId)
    {
        $cache_paths = [
            "post/{$postId}_meta.cache",
            "post/{$postId}_terms_"
        ];

        $termIds = $this->term_posts_m->select('term_id')->where('post_id', $postId)->get_all();
        $cache_paths = wp_parse_args(array_map(function($x){
            return "term/{$x->term_id}_posts_";
        }, $termIds), $cache_paths);

        $cache_paths = array_unique($cache_paths);

        foreach($cache_paths as $key_cache)
        {
            if(strpos('.cache', $key_cache)) $this->scache->delete($key_cache);
            else $this->scache->delete_group($key_cache);
        }   
    }

    protected function delete_user_cache($userId)
    {
        $cache_paths = [
            "user/{$userId}_meta.cache",
            "user/{$userId}_terms_"
        ];

        $termIds = $this->term_users_m->select('term_id')->where('user_id', $userId)->get_all();
        $cache_paths = wp_parse_args(array_map(function($x){
            return "user/{$x->term_id}_users_";
        }, $termIds), $cache_paths);

        $cache_paths = array_unique($cache_paths);

        foreach($cache_paths as $key_cache)
        {
            if(strpos('.cache', $key_cache)) $this->scache->delete($key_cache);
            else $this->scache->delete_group($key_cache);
        }   
    }

    protected function delete_option_cache($optionKey)
    {
        $cache_paths = [
            "options/{$optionKey}-" . (bool) TRUE,
            "options/{$optionKey}-" . (bool) FALSE
        ];

        foreach($cache_paths as $key_cache) $this->scache->delete($key_cache);
    }
}
/* End of file Config.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Config.php */