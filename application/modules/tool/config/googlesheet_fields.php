<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

$config['columns'] = array(

	'domain' => [
		'order' => 0,
		'name' => 'domain',				
		'label' => 'Tên miền',
		'dataValidatation' => FALSE
	],
	'ip' => [
		'order' => 0,
		'name' => 'ip',				
		'label' => 'IP Hiện tại',
		'dataValidatation' => FALSE
	],
	'ip_to' => [
		'order' => 0,
		'name' => 'ip_to',				
		'label' => 'IP Mới',
		'dataValidatation' => FALSE
	],
	'hasDomainContract' => [
		'order' => 0,
		'name' => 'hasDomainContract',
		'label' => 'Có HĐ Tên miền hay không ?',
		'dataValidatation' => [
			'type' => 'BOOLEAN'
		]
	],
	
	'domainContract' => [
		'order' => 0,
		'name' => 'domainContract',
		'label' => 'Số HĐ Tên miền',
		'dataValidatation' => FALSE
	],

	'domainContractEnd' => [
		'order' => 0,
		'name' => 'domainContractEnd',
		'label' => 'Ngày kết thúc',
		'dataValidatation' => FALSE
	],

	'hasHostingContract' => [
		'order' => 0,
		'name' => 'hasHostingContract',
		'label' => 'Có HĐ Hosting',
		'dataValidatation' => [
			'type' => 'BOOLEAN'
		]
	],

	'hostingContract' => [
		'order' => 0,
		'name' => 'hostingContract',
		'label' => 'Số HĐ Hosting',
		'dataValidatation' => FALSE
	],

	'hostingContractEnd' => [
		'order' => 0,
		'name' => 'hostingContractEnd',
		'label' => 'Ngày kết thúc',
		'dataValidatation' => FALSE
	],

	'canControlDNS' => [
		'order' => 0,
		'name'	=> 'canControlDNS',
		'label'	=> 'Tự cấu hình DNS được không ?',
		'dataValidatation' => [
			'type' => 'BOOLEAN'
		]
	],

	'customer_name' => [
		'order' => 0,
		'name' => 'customer_name',
		'label' => 'Khách hàng',
		'dataValidatation' => FALSE
	],

	'canContact' => [
		'order' => 0,
		'name'	=> 'canContact',
		'label'	=> 'Có thông tin liên hệ không ?',
		'dataValidatation' => FALSE
	],

	'provider' => [
		'order' => 0,
		'name'	=> 'provider',
		'label'	=> 'Nhà cung cấp',
		'dataValidatation' => FALSE
	],

	'sale' => [
		'order' => 0,
		'name'	=> 'sale',
		'label'	=> 'KD phụ trách',
		'dataValidatation' => FALSE
	],

	'system_tech' => [
		'order' => 0,
		'name'	=> 'system_tech',
		'label'	=> 'Hệ thống phụ trách',
		'dataValidatation' => FALSE
	],

	'backup_date' => [
		'order' => 0,
		'name'	=> 'backup_date',
		'label'	=> 'Ngày Backup (GG-DRIVE)',
		'dataValidatation' => FALSE
	],

	'backup_status' => [
		'order' => 0,
		'name' => 'backup_status',
		'label' => 'Tiến độ Backup',
		'dataValidatation' => [
			'type' => 'ONE_OF_LIST',
			'values' => ['Chưa xử lý', 'Đang chuyển', 'Hoàn thành', 'Hoàn thành & Remove Hosting']
		]
	],

	'transfer_date' => [
		'order' => 0,
		'name'	=> 'transfer_date',
		'label'	=> 'Ngày chuyển Host',
		'dataValidatation' => FALSE
	],

	'transfer_status' => [
		'order' => 0,
		'name' => 'transfer_status',
		'label' => 'Tiến độ chuyển Host',
		'dataValidatation' => [
			'type' => 'ONE_OF_LIST',
			'values' => ['Chưa xử lý', 'Đang chuyển', 'Hoàn thành']
		]
	],

	'assignee_dns' => [
		'order' => 0,
		'name'	=> 'assignee_dns',
		'label'	=> 'Phụ trách đổi DNS',
		'dataValidatation' => FALSE
	],

	'dns_transfer_date' => [
		'order' => 0,
		'name'	=> 'dns_transfer_date',
		'label'	=> 'Ngày đổi DNS',
		'dataValidatation' => FALSE
	],

	'dns_transfer_status' => [
		'order' => 0,
		'name' => 'dns_transfer_status',
		'label' => 'Tiến độ cấu hình DNS',
		'dataValidatation' => [
			'type' => 'ONE_OF_LIST',
			'values' => ['Chưa xử lý', 'Đang chuyển', 'Hoàn thành']
		]
	],

	'callCustomerState' => [
		'order' => 0,
		'name' => 'callCustomerState',
		'label' => 'Gọi được KH không ?',
		'dataValidatation' => [
			'type' => 'ONE_OF_LIST',
			'values' => ['Chưa gọi', 'Máy bận', 'Đã gọi']
		]
	],

	'callCustomerComment' => [
		'order' => 0,
		'name' => 'callCustomerComment',
		'label' => 'Kết quả gọi',
		'dataValidatation' => [
			'type' => 'ONE_OF_LIST',
			'values' => ['Chưa liên hệ được', 'Khách đồng ý chuyến', 'Khách không đồng ý']
		]
	],

	'customerDateMustChaged' => [
		'order' => 0,
		'name' => 'customerDateMustChaged',
		'label' => 'Ngày hẹn thông báo (KD hẹn KH đổi ngày này)',
		'dataValidatation' => FALSE
	],

	'adsplusDateTransferd' => [
		'order' => 0,
		'name' => 'adsplusDateTransferd',
		'label' => 'Ngày deploy lên Server',
		'dataValidatation' => FALSE
	],

	'customerDateChaged' => [
		'order' => 0,
		'name' => 'customerDateChaged',
		'label' => 'Ngày KH đổi IP',
		'dataValidatation' => FALSE
	],

	'action' => [
		'order' => 0,
		'name' => 'action',
		'label' => 'Hành động',
		'dataValidatation' => FALSE
	],
);

$config['clusters'] = array(

	/* VCCLOUD */
	'45.124.94.128' 	=> [
		'ip'		=> '45.124.94.128',
		'provider'	=> 'VCCLOUD',
		'target_ip' => '45.124.94.128'
	],
	'103.56.156.84' 	=> [
		'ip'		=> '103.56.156.84',
		'provider'	=> 'VCCLOUD',
		'target_ip' => '103.56.156.84'
	],
	'45.124.94.250' 	=> [
		'ip'		=> '45.124.94.250',
		'provider'	=> 'VCCLOUD',
		'target_ip' => '103.56.156.84'
	],
	'103.56.157.13'		=> [
		'ip'		=> '103.56.157.13',
		'provider'	=> 'VCCLOUD',
		'target_ip' => '45.124.94.128'
	],

	/* VIETTEL */
	'171.244.43.182'	=> [
		'ip'		=> '171.244.43.182',
		'provider'	=> 'VIETTEL',
		'target_ip' => '171.244.43.78'
	],
	'171.244.43.78' 	=> [
		'ip'		=> '171.244.43.78',
		'provider'	=> 'VIETTEL',
		'target_ip' => '171.244.43.78'
	],
);

$config['transfer_ip'] = array(
	/* VCCLOUD */
	'45.124.94.128' 	=> '45.124.94.128',
	'103.56.156.84' 	=> '103.56.156.84',
	'45.124.94.250' 	=> '103.56.156.84',
	'103.56.157.13'		=> '45.124.94.128',
	/* VIETTEL */
	'171.244.43.182'	=> '171.244.43.78',
	'171.244.43.78' 	=> '171.244.43.78',
);

$config['defaultValues'] = array(
	'customerDateMustChaged' => '2020/11/18',
	'adsplusDateTransferd' => '2020/11/17',
	'customerDateChaged' => '',
);