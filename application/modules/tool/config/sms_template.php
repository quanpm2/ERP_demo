<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

$config['sale_in_covid_workshop'] = array(
	'demo_adsplus' => 'ADSPLUS hen 18:30h hom nay tai Workshop GIAI PHAP KINH DOANH MUA COVID. Nhap vao link tham du workshop: https://bitly.com.vn/vUUNr Hotline:1800.0098 (MP)',
	'say_thank' => 'Da den gio tham gia Workshop GIAI PHAP KINH DOANH MUA COVID. Hay nhap vao link de tham du ngay workshop: https://bitly.com.vn/vUUNr Hotline:1800.0098 (MP)'
);

$config['google_digital_bites'] = array(

	'demo_adsplus' => 'KQ:04/04 - ***anhtai.vn: click: 125, click ko hop le: 4.Thong tin vui long LH-Ky Thuat: Mr.Huy, huyth1@adsplus.vn, 0964445877',

	'say_thank' => 'Cam on QK da dang ky tham du Google Digital Bites. Hen gap QK tai Hotel des Art Saigon MGallery 76-78 NTMKhai, luc 8AM 5/4/2017'
	);

$config['summer_break_conference'] = array(
	'demo_adsplus' => 'KQ:04/04 - ***anhtai.vn: click: 125, click ko hop le: 4.Thong tin vui long LH-Ky Thuat: Mr.Huy, huyth1@adsplus.vn, 0964445877',
	'say_thank' => 'Cam ơn QK da xac nhan tham du But Pha Doanh Thu Mua He. Hen gap QK tai Nha hang Stix - 174A Ng. Dinh Chieu,Q3, luc 8AM 25/5/2017'
	);

$config['adwords_workshop_2'] = array(

	'demo_adsplus_4' => 'Adsplus hen ban 18h30 tai  WORKSHOP:KHOI PHUC QUANG CAO-HOI SINH NGANH LAM DEP. Link Zoom tham gia: https://zoom.us/j/94770589701
	Hotline:1800.0098 (MP)',
	
	'demo_adsplus_3' => 'Adsplus hen ban 18h30 tai WS:DINH HUONG CONTENT,TANG HIEU QUA ADS. Link Zoom tham gia: https://zoom.us/j/98376704901
	Hotline:1800.0098 (MP)',

	'demo_adsplus' => 'Adsplus hen ban 18h30 tai WS: On Dinh Doanh So - Thach Thuc Covid 2021. Link Zoom tham gia: https://hubs.la/H0P-6YX0
Hotline:1800.0098 (MP)',

	'demo_adsplus_1' => 'Adsplus hen ban 18h30 tai WS: Khang Tai Khoan-Phuc Hoi Doanh Thu 2021. Link Zoom tham gia: https://hubs.la/H0N6Whb0
Hotline:1800.0098 (MP)',

	'demo_adsplus_2' => "Dung quen cuoc hen Workshop 3H THUC HANH GOOGLE ADS ngay mai nhe.
Link tham du: https://hubs.la/H0vRQbs0
Hotline:1800.0098 (MP)
",

'demo_adsplus_tonight' => "Dung quen cuoc hen Workshop 3H THUC HANH GOOGLE ADS toi nay nhe.
Link tham du: https://hubs.la/H0vRQbs0
Hotline:1800.0098 (MP)
"
,

	'say_thank' => "Con 1h nua la Workshop 3H THUC HANH GOOGLE ADS dien ra, ban da san sang chua. Link tham du workshop: https://hubs.la/H0vRQbs0
Hotline:1800.0098 (MP)"
,

	'last_time' => "Da den gio tham gia Workshop 3H THUC HANH GOOGLE ADS. Hay nhap vao link de tham du ngay: https://hubs.la/H0vRQbs0
Hotline:1800.0098 (MP)"

);