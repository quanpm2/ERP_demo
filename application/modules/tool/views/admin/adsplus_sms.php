<?php defined('BASEPATH') OR exit('No direct script access allowed');

echo $this->admin_form->form_open();
echo $this->admin_form->set_col(6,12)->box_open('Tool demo báo cáo SMS dịch vụ ADSPLUS');

echo $this->admin_form->dropdown('Template', 'tpl_type', $default_tpl_type,$tpl_type,'',['style'=>"width:100%"]);

echo $this->admin_form->textarea('Mẫu tin nhắn',['rows'=>2,'name'=>'content'],$tpl_content);

echo $this->admin_form->textarea('Số điện thoại',['name'=>'phones','rows'=>4],'','Dãy số điện thoại , cách nhau bởi dấu ; (semicolon)');
echo $this->admin_form->box_close(['reload', 'Load'], ['button', 'Hủy bỏ']);

if(!empty($logs_content))
{
	echo $this->admin_form->set_col(6)->box_open('List Demo SMS');

	echo '<div class="col-md-12 form-group row">';
	echo form_button(['name'=>'btn_send','type'=>'submit','class'=>'btn btn-primary'],'<i class="fa fa-fw fa-send"></i>SMS');
	echo form_button(['name'=>'btn_clear','type'=>'submit','class'=>'btn btn-warning'],'<i class="fa fa-fw fa-trash"></i>Xóa');
	echo '</div>';

	echo $logs_content ?? '';
}

echo $this->admin_form->form_close();
?>

<script type="text/javascript">
$(function(){
$('select[name=tpl_type]').change(function(){

	$content = $(this).find('option:selected').text();
	$('textarea[name=content]').text($content);

});

})
</script>