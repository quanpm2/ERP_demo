<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weboptimize_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name()
	{
		return 'Dịch vụ Tối ưu website';
	}

	public function init()
	{
		$this->_load_menu();
		$this->_update_permissions();
	}

	/**
	 * Init menu LEFT | NAV ITEM FOR MODULE
	 * then check permission before render UI
	 */
	private function _load_menu()
	{
		$order = 1;
 		$itemId = 'admin-weboptimize';

 		if(!is_module_active('weboptimize')) return FALSE;	
		
		if(has_permission('weboptimize.index.access'))
		{
			$this->menu->add_item(array(
				'id'        => $itemId,
				'name' 	    => 'Tối ưu WEB',
				'parent'    => null,
				'slug'      => admin_url('weboptimize'),
				'order'     => $order++,
				'icon' => 'fa fa-flash',
				'is_active' => is_module('weboptimize')
				),'left-service');	
		}

		if(!is_module('weboptimize')) return FALSE;
		
		if(has_permission('weboptimize.index.access'))
		{
			$this->menu->add_item(array(
			'id' => 'weboptimize-service',
			'name' => 'Dịch vụ',
			'parent' => $itemId,
			'slug' => admin_url('weboptimize'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-circle-o'
			), 'left-service');
		}
		
		$left_navs = array(
			'overview'=> array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-xs fa-tachometer',
				),
			'kpi' => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-xs fa-heartbeat',
				),
			'setting'  => array(
				'name' => 'Cấu hình',
				'icon' => 'fa fa-fw fa-xs fa-cogs',
				),
			'task' => array(
				'name' => 'Công việc',
				'icon' => 'fa fa-fw fa-xs fa-tasks'
				)
			) ;

		
		$term_id = $this->uri->segment(4);

		$this->website_id = $term_id;

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("weboptimize.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "weboptimize-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
				), 'left-service');
		}

		
	}

	public function title()
	{
		return 'Tối ưu Website';
	}

	public function author()
	{
		return 'tambt';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Tối ưu website';
	}
	
	/**
	 * Init default permissions for weboptimize module
	 *
	 * @return     array  permissions default array
	 */
	private function init_permissions()
	{
		return array(

			'weboptimize.index' => array(
				'description' => 'Quản lý Tối ưu Website',
				'actions' => ['manage','access','update','add','delete']),

			'weboptimize.done' => array(
				'description' => 'Dịch vụ đã xong',
				'actions' => ['manage','access','add','delete','update']),
		
			'weboptimize.overview' => array(
				'description' => 'Tổng quan',
				'actions' => ['manage','access']),

			'weboptimize.kpi' => array(
				'description' => 'KPI',
				'actions' => ['manage','access','add','delete','update']),
			
			'weboptimize.start_service' => array(
				'description' => 'Kích hoạt Tối ưu Website',
				'actions' => ['manage','access','update']),
			
			'weboptimize.stop_service' => array(
				'description' => 'Ngừng Tối ưu Website',
				'actions' => ['manage','access','update']),

			'weboptimize.setting' => array(
				'description' => 'Cấu hình',
				'actions' => ['manage','access','add','delete','update']) ,

			'weboptimize.task' => array(
				'description' => 'Công việc',
				'actions' => ['manage','access','add','delete','update']),
				
			'weboptimize.acceptance' => array(
				'description' => 'Nghiệm thu',
				'actions' => ['manage','access','add','delete','update']),
			);
	}

	private function _update_permissions()
	{
		$permissions = array();

		if(!permission_exists('weboptimize.acceptance'))
			$permissions['weboptimize.acceptance'] = array(
				'description' => 'Nghiệm thu',
				'actions' 	  => array('manage', 'access', 'add','delete','update'));

		if(empty($permissions)) return false;
		
		foreach($permissions as $name => $value){
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	/**
	 * Install module
	 *
	 * @return     bool  status of command
	 */
	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		foreach($permissions as $name => $value)
		{
			$description   = $value['description'];
			$actions 	   = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);
			$this->role_permission_m->insert(['role_id'=>1,'permission_id'=>$permission_id,'action'=>serialize($actions)]);
		}

		return TRUE;
	}

	/**
	 * Uninstall module command
	 *
	 * @return     bool  status of command
	 */
	public function uninstall()
	{
		$permissions = $this->init_permissions();

		if(!$permissions) return FALSE;

		if($pers = $this->permission_m->like('name','weboptimize')->get_many_by())
		{
			foreach($pers as $per)
			{
				$this->permission_m->delete_by_name($per->name);
			}
		}

		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}

		return TRUE;
	}
}
/* End of file weboptimize.php */
/* Location: ./application/modules/weboptimize/models/weboptimize.php */