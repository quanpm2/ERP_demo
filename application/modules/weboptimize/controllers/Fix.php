<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fix extends Admin_Controller {

	public function __construct(){
		parent::__construct();

		$models = array(
			'weboptimize_m',
			'weboptimize_contract_m',
			'weboptimize_report_m',
			'weboptimize_kpi_m'
		);

		$this->load->model($models);	
	}

	public function send_mail_info_activated($term_id = 0)
	{
		$this->weboptimize_report_m->send_mail_info_activated($term_id);
	}

	public function send_mail_info_finish($term_id) {
		$this->weboptimize_report_m->send_mail_info_finish($term_id) ;
	}

	public function send_mail_info_to_admin($term_id) {
		$this->weboptimize_report_m->send_mail_info_to_admin($term_id);		
	}

	public function send_mail_alert_expiring_soon($term_id) {
		$this->weboptimize_report_m->send_mail_alert_expiring_soon($term_id);		
	}

	public function send_mail_acceptance($term_id) {
		$this->weboptimize_report_m->send_mail_acceptance($term_id);		
	}
}