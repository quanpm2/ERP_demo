<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weboptimize extends Admin_Controller 
{
	private $term;
	private $term_type = 'weboptimize';
	public  $model 	   = 'weboptimize_contract_m';

	public function __construct(){

		parent::__construct();
		
		$this->load->model('term_users_m');
		$this->load->model('weboptimize/weboptimize_kpi_m') ;
		$this->load->model('weboptimize/weboptimize_contract_m');
		$this->load->model('weboptimize/weboptimize_m');
		$this->load->model('weboptimize/weboptimize_report_m');
		$this->load->model('customer/customer_m');
		$this->load->model('contract/contract_m');
		$this->load->model('contract/contract_wizard_m');
		$this->load->model('contract/invoice_m');
		$this->load->model('webgeneral/webgeneral_callback_m') ;
		$this->load->model('webgeneral/webgeneral_kpi_m');
		$this->load->model('staffs/sale_m');

		$this->load->config('contract/contract');
		$this->load->config('staffs/group');
		$this->load->config('contract/invoice');
		$this->load->config('weboptimize/weboptimize');

		// kiểm tra weboptimize có tồn tại và user có quyền truy cập ?
		$this->init_website();
	}

	private function init_website()
	{
		$term_id = $this->uri->segment(4);
		$method  = $this->uri->segment(3);
		$is_allowed_method = (!in_array($method, array('index','done')));
		if(!$is_allowed_method || empty($term_id)) return FALSE;

		$term = $this->term_m
		->where('term_status','publish')		
		->where('term_type',$this->term_type)
		->where('term_id',$term_id)
		->get_by();
		if(empty($term) OR !$this->is_assigned($term_id)) 
		{
			$this->messages->error('Truy cập #'.$term_id.' không hợp lệ !!!');
			redirect(module_url(),'refresh');
		}

		$this->template->title->set(strtoupper(' '.$term->term_name));
		$this->website_id = $this->data['term_id'] = $term_id;
		$this->data['term'] = $this->term = $term;
	}
	public function index()
	{
		restrict('weboptimize.index.access');
		$this->template->is_box_open->set(1);
		$this->template->title->set('Tổng quan dịch vụ "Tối ưu website"');
		parent::render($this->data);
	}

	public function overview($term_id = 0)
	{
		restrict('weboptimize.overview.access');

		$this->scache->delete('term/' .$term_id . '_meta');

		$term = $this->term_m->get($term_id);

		if(empty($term)){
			$this->messages->error('Dịch vụ không tồn tại');
			redirect(module_url(),'refresh');
		}

		$time = time();	
		$day  = date('d',$time) - 1;

		if($day == 0){
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}

		$contract_begin        = get_term_meta_value($term_id, 'contract_begin');
    	$contract_end          = get_term_meta_value($term_id, 'contract_end');

    	$contract_price_weboptimize           = get_term_meta_value($term_id, 'contract_price_weboptimize') ;

    	$date_start  		   = date('d/m/Y', $contract_begin);
		$date_end 	 		   = date('d/m/Y', $contract_end);

		$description = 'Từ '.$date_start.' đến '.$date_end;

		$time_start  = $this->mdate->startOfDay(strtotime($date_start));
		$time_end    = $this->mdate->endOfDay(strtotime($date_end));

		$this->template->description->append($description);
		$this->template->title->prepend('Tổng quan');
		$this->template->javascript->add('plugins/chartjs/Chart.js');

		/* Load danh sách nhân viên kinh doanh */
		$staffs = $this->sale_m->select('user_id,user_email,display_name')->set_user_type()->set_role()->as_array()->get_all();
		$staffs = array_map(function($x){ $x['display_name'] = $x['display_name'] ?: $x['user_email']; return $x; }, $staffs);
		$data['staffs'] = key_value($staffs, 'user_id', 'display_name');

		$data['websites'] = array();
		$data['website']  = $term->term_name ;
		$data['invoice_url'] = module_url('invoices/');
		$data['term_id'] 			 = $term_id ;

		parent::render($data);
	}

	public function kpi($term_id = 0, $delete_id= 0)
	{
		restrict('weboptimize.kpi.access');	
		$this->config->load('weboptimize/group') ;			
		$this->template->title->prepend('KPI');
		$data = $this->data;
		$this->submit_kpi($term_id,$delete_id);

		$targets = $this->weboptimize_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(array(
			'term_id'=> $term_id
			));

		$data['targets'] = array();

		$data['targets']['tech'] = array();
		foreach($targets as $target)
		{
			$data['targets'][$target['kpi_type']][] = $target;
		}
		
		$data['users'] = $this->admin_m->select('user_id,display_name')->where_in('role_id', $this->config->item('group_memberId'))->set_get_active()->order_by('display_name')->get_many_by();

		$data['users'] = key_value($data['users'],'user_id','display_name');

		$this->data = $data;
		parent::render($this->data);
	}

	public function submit_kpi($term_id = 0,$delete_id=0)
	{
		if($delete_id > 0)
		{
			$this->_delete_kpi($term_id,$delete_id);
			redirect(module_url('kpi/'.$term_id.'/'),'refresh');
		}
		$post = $this->input->post();

		if(empty($post)) return FALSE;

		$insert = array();
		$insert['kpi_datetime'] = $this->input->post('target_date');
		$insert['user_id'] 		= $this->input->post('user_id');
		$insert['kpi_value'] 	= (int)$this->input->post('target_post');

		$kpi_type 				= $this->input->post('target_type');
		if($insert['kpi_datetime'] >= $this->mdate->startOfMonth())
		{
			$this->weboptimize_kpi_m->update_kpi_value($term_id, $kpi_type,$insert['kpi_value'], $insert['kpi_datetime'], $insert['user_id']);			
			$this->messages->success('Cập nhật thành công');
		}
		else
		{
			$this->messages->error('Cập nhật không thành công do tháng cập nhật nhỏ hơn tháng hiện tại');
		}
		redirect(current_url(),'refresh');
	}

	private function _delete_kpi($term_id = 0,$delete_id=0)
	{
		restrict('weboptimize.kpi.delete');
		$check = $this->weboptimize_kpi_m->get($delete_id);
		$is_deleted = false;
		if($check)
		{
			if(strtotime($check->kpi_datetime) >= $this->mdate->startOfMonth())
			{
				$this->weboptimize_kpi_m->delete_cache($delete_id);
				$this->weboptimize_kpi_m->delete($delete_id);
				$is_deleted = true;
				$this->messages->success('Xóa thành công');
			}
		}
		($is_deleted) OR $this->messages->error('Xóa không thành công');
	}

	public function task($term_id = 0)
	{
		restrict('weboptimize.Task.Access');
		if($this->input->post())
		{
			Modules::run('weboptimize/tasks/insert_task',$term_id, $this->weboptimize_m->get_post_type('task'));
		} 

		$this->template->title->prepend('Công việc');
		$data = $this->data;
		$this->admin_ui->select('end_date');
		$this->admin_ui->add_column('posts.post_id','#');
		$this->admin_ui->add_column('post_title','Tiêu đề', null,null, array('class' =>'col-md-3'));
		// $this->admin_ui->add_column('post_content','Nội dung', NULL, NULL, array('class' =>'col-md-4'));

		$this->admin_ui->add_column('post_author', 'Người tạo');

		$this->admin_ui->add_column('type', array('set_select'=>FALSE,'set_order'=>FALSE, 'title' =>'Loại'), null,null, array('class' =>'col-md-1'));
		$this->admin_ui->add_column_callback('type',function($data, $row_name){
			$type = get_post_meta_value($data['post_id'],'task_type');
			$data['type'] = $this->config->item($type,'tasks_type');
			return $data;
		}, FALSE);
		//$this->admin_ui->add_column('sms', array('set_select'=>FALSE,'set_order'=>FALSE, 'title' =>'Gửi SMS'), null,null, array('class' =>'col-md-1'));
		$this->admin_ui->add_column('post_status', 'Trạng thái');
		$this->admin_ui->add_column_callback('post_status',function($data, $row_name){
			$status = array();
			$status['process'] = '';
			$status['complete'] = '';

			switch ($data['post_status']) {

				case 'complete':
				$status['complete'] = 'checked disabled';

				case 'process':
				$status['process'] = 'checked disabled';
				break;
			}

			$data['post_status'] = '<span class="checkbox"><label><input type="checkbox" class="minimal" data-taskid="'.$data['post_id'].'" data-status="process" '.$status['process'].'> Đang thực hiện</label></span>
			<span class="checkbox"><label><input type="checkbox" class="minimal" data-taskid="'.$data['post_id'].'" data-status="complete" '.$status['complete'].'> Hoàn thành </label></span>';
			return $data;
		}, FALSE, array('class' =>'col-md-2'));

		// $this->admin_ui->add_column_callback('sms',function($data, $row_name){

		// 	$is_send = $this->postmeta_m->get_meta_value($data['post_id'],'sms_status');

		// 	if($is_send == 2)
		// 	{
		// 		$data['sms'] = '<span class="label label-success">Đã gửi</span>';
		// 		$data['view'] = '';
		// 	}
		// 	else
		// 	{
		// 		$checked = ($is_send) ? 'checked' : '';
		// 		$checked.= ($data['post_status'] =='complete') ? ' disabled' : '';
		// 		$data['sms'] = '
		// 		<span class="checkbox"><label><input type="checkbox" class="minimal" data-taskid="'.$data['post_id'].'"  data-toggle="confirmation" data-status="send_sms" '.$checked.'> SMS</label></span>';
		// 	}

		// 	return $data;
		// }, FALSE);

		$this->admin_ui->add_column('start_date','Ngày thực hiện','$1 - $2','date("d/m/Y",start_date),date("d/m/Y",end_date)',  array('class' =>'col-md-2'));
		// $this->admin_ui->add_column('end_date','Ngày kt','$1','date("d/m/Y",end_date)');

		$this->admin_ui->where('post_type', $this->weboptimize_m->get_post_type('task'));
		$this->admin_ui->where('term_id',$term_id);
		$this->admin_ui->order_by("start_date asc, post_status asc");
		$this->admin_ui->from('posts');
		$this->admin_ui->join('term_posts', 'term_posts.post_id = posts.post_id');

		$this->admin_ui->add_column_callback(array('post_content','post_author'),array($this->webgeneral_callback_m,'post_get_meta'), FALSE);
		$this->admin_ui->add_column('view', array('set_select'=>FALSE, 'title' =>'Xem'),anchor(module_option_url('webdoctor','tasks/edit/$1'),'Edit','class="ajax_edit" data-taskid="$1"').' | '.anchor(module_option_url('webdoctor','tasks/edit/$1'),'Xóa','class="ajax_delete" data-taskid="$1"'),'post_id', array('class' =>'col-md-1'));

		$this->admin_ui->add_column_callback('view',function($data,$row_name){

				$post_id = $data['post_id'];
				$view = '';

				if(has_permission('weboptimize.Task.Update'))
				{
					$view.= anchor(module_option_url('webdoctor',"tasks/edit/{$post_id}"),'<i class="fa fa-fw fa-edit"></i>',"title='Cập nhật' class='btn btn-default btn-xs ajax_edit' data-taskid='{$post_id}' target='_blank'");
				}

				if(has_permission('weboptimize.Task.Delete'))
				{
					$view.= anchor(module_option_url('webdoctor',"tasks/edit/{$post_id}"),'<i class="fa fa-fw fa-trash"></i>',"title='Xóa' class='btn btn-default btn-xs ajax_delete' data-taskid='{$post_id}' data-toggle='confirmation' target='_blank'");
				}

				$data['view'] = $view;
				return $data;
			},FALSE);

		// Add filter for user with no manage permission
		if( ! has_permission('weboptimize.Task.Manage') ) 
		{
			$this->admin_ui->where('posts.post_author',$this->admin_m->id);
			//$this->admin_ui->where_in('posts.post_author',array($this->admin_m->id,1)); // #1- Hệ thống
		}

		$data['content'] = $this->admin_ui->generate();

		$this->data = $data;
		parent::render($this->data);
	}

	public function setting($term_id = 0)
	{
		restrict('weboptimize.setting');
		$this->template->title->prepend('Cấu hình');
		$this->setting_submit($term_id);
		
		$data = $this->data;
		$data['term'] = $this->term;
		$data['term_id'] = $term_id;
		$data['meta']['phone_report'] = get_term_meta_value($term_id, 'phone_report');
		$data['meta']['mail_report']  = get_term_meta_value($term_id, 'mail_report');

		$this->data = $data;
		parent::render($this->data);
	}

	protected function setting_submit($term_id = 0)
	{
		$post = $this->input->post();
		if(empty($post)) return FALSE;

		// KÍCH HOẠT + LƯU ĐIỂM PAGE SPEED TRƯỚC KHI TỐI ƯU + GỬI MAIL
		if( ! empty($post['start_process']) )
		{
			
			if( ! $this->weboptimize_m->has_permission($term_id, 'weboptimize.start_service') )
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}
			$term = $this->term_m->get($term_id);

			// Cập nhật điểm pagespeed trước khi tối ưu web
			if(empty(get_term_meta_value($term_id, 'has_before_check_optimize_pagespeed')))
			{
				$pagespeeds = $this->get_pagespeed($term->term_name);

				if(FALSE == $pagespeeds) return $this->messages->error('Không nhận được điểm số pagespeed');

				$flag_pagespeed = TRUE ; 
				foreach ($pagespeeds as $meta_key => $meta_value) 
				{
					$meta_key = 'before_' . $meta_key ;
					
					$status = update_term_meta($term_id, $meta_key, $meta_value);
					if(! $status ) 
					{
						$flag_pagespeed = FALSE ;
						return $this->messages->error('Cập nhật điểm số pagespeed vào database không thành công') ;
						break;
					}
				}

				if( $flag_pagespeed == TRUE ) update_term_meta($term_id, 'has_before_check_optimize_pagespeed', 'yes') ;
			}

			$status_proc_service  = $this->weboptimize_contract_m->proc_service($term);

			if(FALSE === $status_proc_service) $this->messages->error('Có lỗi xảy ra! Vui lòng kiểm tra cấu hình cài đặt hoặc thêm nhân viên kỹ thuật') ;
			if(TRUE === $status_proc_service) $this->messages->success('Đã gửi mail thành công');

			/* Phân tích hợp đồng ký mới | tái ký */
			$this->load->model('contract/base_contract_m');
			$this->base_contract_m->detect_first_contract($term_id);


			/* Gửi SMS thông báo kích hoạt hợp đồng đến khách hàng */
			$this->load->model('contract/contract_report_m');
			$this->contract_report_m->send_sms_activation_2customer($term_id);

			redirect(module_url("setting/{$term_id}"),'refresh');
		}

		// NGƯNG KÍCH HOẠT + GỬI MAIL
		else if( ! empty($post['end_process']) )
		{
			if( ! $this->weboptimize_m->has_permission($term_id, 'weboptimize.stop_service') )
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$term = $this->term_m->get($term_id);

			$stat = $this->weboptimize_contract_m->stop_service($term);
			if(TRUE === $stat) $this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}

		// NGHIỆM THU
		else if(! empty($post['acceptance_weboptimize']) )
		{
			if( ! $this->weboptimize_m->has_permission($term_id, 'weboptimize.acceptance_weboptimize') )
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			// $term = $this->term_m->get($term_id);			
			$is_send = $this->weboptimize_report_m->send_mail_acceptance($term_id);
			if(TRUE === $is_send) $this->messages->success('Hệ thống đã gửi mail thành công');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}

		else if(!empty($post['submit']))
		{
			$metadatas = $post['meta'];

			if( ! $this->weboptimize_m->has_permission($term_id, 'weboptimize.setting','update'))
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$metadatas = $post['meta'];
			if($metadatas)
			{
				foreach($metadatas as $key=>$value)
				{	
					update_term_meta($term_id,$key,$value);
				}
			}

			$this->messages->success('Cập nhật thành công');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
	}

	protected function is_assigned($term_id = 0,$kpi_type = '')
	{
		// check user has manager role permission
		$class  = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		$result = $this->weboptimize_m->has_permission($term_id,"{$class}.{$method}",'access');
		return $result;
	}

	public function get_pagespeed($website = '') {
		if(empty($website)) return FALSE;

		$time_out           = $this->config->item('timeout', 'request') ;
        $connect_timeout 	= $this->config->item('connect_timeout', 'request') ;
		$request 			= Requests::request(base_url('weboptimize/api/load_pagespeed'), array('Accept' => 'application/json'), array('url' => $website), Requests::POST, array('timeout' => $time_out,
			'connect_timeout' => $connect_timeout));

		if($request->status_code != 200) return FALSE;

		$get_pagespeed  = json_decode($request->body) ;
		if(empty($get_pagespeed)) return FALSE ;

		$pagespeeds 	= $get_pagespeed->data;
		if(empty($pagespeeds)) return FALSE ;

		return $pagespeeds ;
	}
}
/* End of file weboptimize.php */
/* Location: ./application/modules/weboptimize/controllers/weboptimize.php */