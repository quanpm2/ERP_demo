<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crond extends Admin_Controller 
{
	private $term;
	private $term_type;

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('weboptimize_report_m');
	}

	public function delete() {
		$ids = [2257, 2256, 2255, 2254] ;

		foreach($ids as $id) {
			$this->db->where('term_id', $id);
			$this->db->delete('termmeta') ;			
		}

		$this->db->where('term_type', 'weboptimize');
		$this->db->delete('term') ;
	}

	public function index() 
	{
		$terms 					 = $this->term_m->get_many_by(['term_type' =>'weboptimize','term_status' =>'publish']);

		if( empty( $terms ) ) return FALSE ; 

		$this->warning_contract_deadline($terms) ;
	}

	public function warning_contract_deadline($terms) 
	{
		if( empty($terms) ) return FALSE ;

		$time_current          =  time();
		$time_current          =  $this->mdate->startOfDay($time_current) ;

		foreach($terms as $term) 
		{

			// Thời hạn bắt đầu HĐ
			$contract_begin    				= (int)get_term_meta_value($term->term_id, 'contract_begin') ;
			$contract_begin    				= $this->mdate->startOfDay($contract_begin) ;
				
			// Thời hạn kết thúc HĐ
			$contract_end      				= (int)get_term_meta_value($term->term_id, 'contract_end')   ;
			$contract_end      				= $this->mdate->startOfDay($contract_end) ;

			$data                       	= $this->weboptimize_report_m->send_mail_alert_expiring_soon($term->term_id) ;

			// // Kiểm tra thời gian hợp lệ
			// if( $time_current < $contract_begin ) continue ;
			
			// // Trước 15 ngày kết thúc HĐ
			// $before_15day_contract_end  	= strtotime('-15 day', $contract_end);
			// $before_15day_contract_end      = $this->mdate->startOfDay($before_15day_contract_end) ;

			// // Trước 3 ngày kết thúc HĐ
			// $before_3day_contract_end       = strtotime('-3 day', $contract_end);
			// $before_3day_contract_end       = $this->mdate->startOfDay($before_3day_contract_end) ;	
		
			// // Sau 5 ngày kết thúc HĐ 		
			// $after_5day_contract_end        = strtotime('+5 day', $contract_end);
			// $after_5day_contract_end        = $this->mdate->startOfDay($after_5day_contract_end) ;	

			// // Gửi mail trước 15 ngày kết thúc HĐ
			// if( ($before_15day_contract_end <= $time_current) && ($time_current < $before_3day_contract_end )) 
			// {
			// 	$status_log  				= $this->check_log_weboptimize_warning_deadline($term->term_id) ;
			// 	if( $status_log == TRUE ) {
			// 		$status_send_mail       = $this->weboptimize_report_m->send_mail_alert_expiring_soon($term->term_id, 'before' , 15) ;
			// 	}
			// 	continue;			
			// }
			
			// // Gửi mail trước 3 ngày kết thúc HĐ
			// if( ($before_3day_contract_end <= $time_current) && ($time_current < $contract_end ) ) 
			// {
			// 	$status_log  				= $this->check_log_weboptimize_warning_deadline($term->term_id, 'before', 3) ;			
			// 	if( $status_log == TRUE ) 
			// 	{
			// 		$status_send_mail       = $this->weboptimize_report_m->send_mail_alert_expiring_soon($term->term_id, 'before' , 3) ;					
			// 	} 		
			// 	continue;	
			// }

			// // Sau 5 ngày kết thúc HĐ
			// if( ($after_5day_contract_end >= $time_current) && ($time_current > $contract_end ) ) 
			// {
			// 	$status_log  				= $this->check_log_weboptimize_warning_deadline($term->term_id, 'after', 5) ;	
			// 	if( $status_log == TRUE ) 
			// 	{
			// 		$status_send_mail       = $this->weboptimize_report_m->send_mail_alert_expiring_soon($term->term_id, 'after' , 5) ;
			// 	}
			// 	continue; 				
			// }		
		}
	}

	private function check_log_weboptimize_warning_deadline($term_id, $day = 15, $type = 'before') 
	{

		if(empty($term_id)) return FALSE ;

		$log       = $this->log_m->where('term_id'  , $term_id)
								 ->where('log_type' , 'warning_weboptimize_deadline_' . $type . '_' . $day . 'day')
								 ->get_by();

 		if(empty($log)) return TRUE;

 		return ($log->log_status !== 1);
	}
}