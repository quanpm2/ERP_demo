<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'third_party/vendor/autoload.php';
class Api extends API_Controller {
	
	public function __construct()
	{
		parent::__construct();
        $this->load->config('weboptimize/weboptimize') ; 
	}

    public function check_pagespeed($website = '' , $type = 'desktop')
    {
        if(empty($website)) return $website ;

        $key_pagespeed      = "AIzaSyBgnmVtMRLSfT_HoAl93TWh0awj_1i23PI";

        $time_out           = $this->config->item('timeout', 'request') ;
        $connect_timeout    = $this->config->item('connect_timeout', 'request') ;

        if (!preg_match('%http://%',$website) ||!preg_match('%https://%',$website)) 
        {
             $website ='http://'.$website;
        }

        if (preg_match('%http://%',$website) || preg_match('%https://%',$website))
        {
             $website = $website;
        }

        switch ($type) {
            case 'mobile':
                $request_url = 'https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url='.$website.'&screenshot=true&key='.$key_pagespeed .''.'&strategy=mobile';                
                break;
            
            default:
                $request_url = 'https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url='.$website.'&screenshot=true&key='.$key_pagespeed .''.'&strategy=desktop';

                break;
        }

        $request        = Requests::request($request_url, array(), array(), Requests::GET, array('timeout' => $time_out, 'connect_timeout' => $connect_timeout));

        if($request->status_code != 200) return FALSE;

        return $request->body ;
    } 

     public function load_pagespeed()
     {
         $url 	= $this->input->post('url') ?: $this->input->get('url') ;

         if(empty($url)) return FALSE ;

         $results_desk  = $this->check_pagespeed($url);
         $results_desk  = json_decode($results_desk);

         $result_mobile = $this->check_pagespeed($url, 'mobile');
         $result_mobile = json_decode($result_mobile);

         $size      = ($results_desk->pageStats->totalRequestBytes + $results_desk->pageStats->htmlResponseBytes
             + $results_desk->pageStats->cssResponseBytes + $results_desk->pageStats->javascriptResponseBytes
             + $results_desk->pageStats->imageResponseBytes + $results_desk->pageStats->otherResponseBytes);

         $size_web=round($size/1048576,2);

         $json['status']    = 200;
         $json['data']      = array('weboptimize_score_desktop'     => $results_desk->ruleGroups->SPEED->score,
                                    'weboptimize_score_mobile'      => $result_mobile->ruleGroups->SPEED->score,
                                    'weboptimize_score_usability'   => $result_mobile->ruleGroups->USABILITY->score,
                                    'weboptimize_size_web'          => $size_web);
         $json['msg']       ='Load dữ liệu thành công';
         $json['request']   = $url;

         return $this->render($json);
     }
}