<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends Admin_Controller {
	public $model = 'seotraffic_m';
	public $term_type = 'weboptimize';
	public $website_id = 0;
	public $term = FALSE;

	public function __construct()
	{
		// Modules::load('webdoctor');
		parent::__construct();
		$this->load->model('weboptimize/weboptimize_m');
		$this->post_type = 'weboptimize-task';
		$this->load->model('post/post_m') ;
	}

	public function task($id = 0)
	{
		if($this->input->post())
		{
			$this->insert_task($id);
		} 
		$this->admin_ui->add_column('posts.post_id','#');
		$this->admin_ui->add_column('post_title','Tiêu đề');
		$this->admin_ui->add_column('post_content', 'Nội dung');
		$this->admin_ui->add_column('start_date','Ngày bắt đầu','$1','date("d/m/Y",start_date)');

		$this->admin_ui->where('post_type',$this->post_type);
		$this->admin_ui->where('term_id',$id);
		$this->admin_ui->order_by("posts.post_id desc");
		$this->admin_ui->from('posts');
		$this->admin_ui->join('term_posts', 'term_posts.post_id = posts.post_id');

		$this->admin_ui->add_column_callback(array('post_content'),array($this,'callback_post_get_meta'), FALSE);
		$this->admin_ui->add_column('view', array('set_select'=>FALSE, 'title' =>'Xem'),anchor(module_option_url('webdoctor','tasks/edit/$1'),'Edit','class="ajax_edit" data-taskid="$1"').' | '.anchor(module_option_url('webdoctor','tasks/edit/$1'),'Xóa','class="ajax_delete" data-taskid="$1"'),'post_id');
		$data['content'] = $this->admin_ui->generate();
		return $data;
	}

	public function ajax_edit($post_id = 0, $post_type = '')
	{
		if($post_id <= 0) return '';
		$post_type = $this->weboptimize_m->get_post_type($post_type);
		if(!$post_type) return '';
		$post = $this->post_m->get_by(array('post_id'=>$post_id, 'post_type' => $post_type));
		if(!$post) return FALSE;

		$results 				 = array();
		$results['id'] 			 = $post->post_id;
		$results['title'] 		 = $post->post_title;
		$results['content'] 	 = $post->post_content;
		$results['link_website'] = $this->postmeta_m->get_meta_value($post_id,'link_website');
		// $results['content'] = $post->end_date
		$results['start_date'] 	 = my_date($post->start_date);
		$results['end_date'] 	 = my_date($post->end_date);
		$results['type'] 		 = $this->postmeta_m->get_meta_value($post_id,'task_type');
		$results['done_ratio'] 	 = $this->postmeta_m->get_meta($post_id,'webdoctor_done_ratio',TRUE, TRUE);
		return $this->output->set_content_type('application/json', 'utf-8')->set_output(json_encode($results));
	}

	public function ajax_delete($post_id = 0)
	{
		if($post_id <= 0) return '';

		$this->post_m->where('post_type', $this->post_type)->delete_post($post_id);
		if($this->post_m->affected_rows() > 0)
		{
			$this->weboptimize_kpi_m->update_kpi_result(
				$contract_id,
				'content');
			echo 'OK';
		}
		else
		{
			echo 'Có lỗi xảy ra';
		}
	}

	function insert_task($term_id = 0, $post_type = '')
	{
		$title = $this->input->post('title');
		if(empty($post_type) || empty($term_id) || empty($title))
		{	
			$this->messages->error("Nội dung hoặc tiêu đề không được bỏ trống");
			redirect(current_url());
		}

		$start_date = $this->mdate->startOfDay($this->input->post('start_date'));
		$end_date = $this->mdate->endOfDay($this->input->post('end_date'));

		$insert = array();
		$insert['post_title'] = $title;
		$insert['start_date'] = $start_date;
		$insert['end_date']   = $end_date;
		$insert['post_content'] = $this->input->post('content');

		$post_id = $this->input->post('taskid');
		if($post_id == 0)
		{
			$insert['created_on'] = time();
			$insert['post_type'] = $post_type;
			$insert['post_author'] = $this->admin_m->id;
			$post_id = $this->post_m->insert($insert);
			$this->term_posts_m->set_post_terms($post_id, $term_id, $this->term_type);
			$this->messages->add("Thêm ".$title." thành công.",'success');
		}
		else
		{
			$insert['updated_on'] = time();
			$this->post_m->update($post_id,$insert);
			$this->messages->add("Cập nhật ".$title." thành công.",'info');
		}

		$kpi_type = '';
		switch ($post_type) 
		{
			case 'weboptimize-content':	
				$kpi_type = 'content';
				break;

			case 'weboptimize-content-product':
				$kpi_type = 'content_product';
				break;
			
			default: break;
		}

		if(!empty($kpi_type))
		{
			$this->weboptimize_kpi_m->update_kpi_result($term_id,$kpi_type,0,$start_date,$this->admin_m->id);	
		}

		// $done_ratio = (int)$this->input->post('done_ratio');
		$this->postmeta_m->update_meta($post_id, 'task_type', $this->input->post('type'));
		$this->postmeta_m->update_meta($post_id, 'link_website', $this->input->post('link_website'));
		// $this->postmeta_m->update_meta($post_id, 'webdoctor_done_ratio', $done_ratio);

		redirect(current_url());
	}

	public function update_status($post_id = 0, $status = '')
	{
		if($post_id <= 0)
			return false;
		$status_accept = array('process', 'complete', 'send_sms');
		if(empty($status) || !in_array($status, $status_accept))
			return false;

		if($status == 'send_sms')
		{
			$is_send = $this->postmeta_m->get_meta_value($post_id,'sms_status');
			$this->postmeta_m->update_meta($post_id,'sms_status', !$is_send);
			return ;
		}
		$update = array();
		$update['post_status'] = $status;
		$this->post_m->update($post_id, $update);
	}
}