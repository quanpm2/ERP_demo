<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('datatable_builder');
		$this->load->config('weboptimize/weboptimize') ; 
	}

	public function optimize_pagespeed() 
	{
		$website = $this->input->post('website') ?: $this->input->get('website') ;
		if(empty($website)) return FALSE;

		$term_id = $this->input->post('term_id') ?: $this->input->get('term_id') ;
		if(empty($term_id)) return FALSE;

		$type    = $this->input->post('type') ?: $this->input->get('type') ;
		if(empty($type)) $type = 'before' ;

		$response     = array(
								'status' => 0,
								'msg' 	 => 'Load dữ liệu không thành công',
								'data'	 => NULL
							);

		$pagespeeds   =  Modules::run('weboptimize/weboptimize/get_pagespeed', $website);
		
		if($pagespeeds == FALSE) return $this->renderJson($response);
	
		foreach ($pagespeeds as $meta_key => $meta_value) 
		{
			$meta_key = $type . '_' . $meta_key ;
			$status   = update_term_meta($term_id, $meta_key, $meta_value);
			if( !$status ) return $this->renderJson($response);
		}

		return $this->renderJson(array(	'status' => 1, 
            		 					'msg' => 'Cập nhật dữ liệu thành công',
            		 					'data' => NULL
						           )
			   );
	}

	function post_delete($type ='', $post_id = 0)
	{
		$this->load->model('weboptimize/weboptimize_m');
		$this->load->model('post/post_m') ;
		if( ! $this->input->is_ajax_request())
		{
			die('Bài viết không tồn tại hoặc quyền truy cập bị hạn chế.');
		}

		$post_type = $this->weboptimize_m->get_post_type($type);

		if( ! has_permission("weboptimize.{$type}.delete"))
		{
			die('Quyền truy cập bị hạn chế.');	
		}
		
		if(empty($post_type) || empty($post_id))
		{
			die('Loại bài viết xóa không đúng.');
		}

		$args = array('post_type'=>$post_type,'post_id'=>$post_id);
		if( ! has_permission("weboptimize.{$type}.manage"))
		{
			$args['post_author'] = $this->admin_m->id;
		}


		$check = $this->post_m->get_by($args);
		if(empty($check))
			die('Bài viết không tồn tại hoặc quyền truy cập bị hạn chế.');

		$term_ids 	= $this->term_posts_m->get_post_terms($post_id, 'weboptimize');
		$is_delete  = $this->post_m->delete_post($post_id);
		if($is_delete) return $this->output->set_content_type('application/json')
        								   ->set_output(json_encode(['msg' => 'Xóa thành cônng', 'status' => TRUE]));
        return $this->output->set_content_type('application/json')
        								   ->set_output(json_encode(['msg' => 'Xóa không thành cônng', 'status' => FALSE]));								
	}

	/**
	 * Trang tổng quan liệt kê danh sách tất cả các hợp đồng
	 */
	public function dataset()
	{
		$response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

		if( ! has_permission('weboptimize.index.access'))
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		$defaults 	= ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
		$args 		= wp_parse_args( $this->input->get(), $defaults);

		$data = $this->data; // remove-after

		// Authorization for user role
		if(!has_permission('Weboptimize.Index.Manage') && !has_permission('Weboptimize.Sale.Access') )
		{
			$this->datatable_builder->join('webgeneral_kpi','webgeneral_kpi.term_id = term.term_id');
			$this->datatable_builder->where('webgeneral_kpi.user_id', $this->admin_m->id);
		}

		$service_packages = array_map(function($x){ return $x['label']; }, $this->config->item('service','packages'));

		/* Applies get query params for filter */
		$this->search_filter();

		$this->load->model('webgeneral/webgeneral_kpi_m');
		$this->load->model('term_users_m');

		$this->datatable_builder
		->set_filter_position(FILTER_TOP_OUTTER)
		->select('term.term_id,term.term_name')
		->add_search('term.term_id')
		->add_search('term.term_name',['placeholder'=>'Tên miền'])
		->add_search('contract_begin',['placeholder'=>'Ngày bắt đầu','class'=>'form-control input_daterange'])
		->add_search('contract_end',['placeholder'=>'Ngày kết thúc','class'=>'form-control input_daterange'])
		->add_search('technical_staffs', ['class'=>'form-control','placeholder'=>'Kỹ thuật thực hiện'])
		->add_search('staff_business',['placeholder'=>'Kinh doanh phụ trách'])

		->add_column('term.term_id','ID')
		->add_column('term.term_name','Website')
		->add_column('contract_begin', array('set_select'=> FALSE, 'title'=> 'T/G bắt đầu'))
		->add_column('contract_end', array('set_select'=> FALSE, 'title'=> 'T/G kết thúc'))
		->add_column('score_commitment_weboptimize', array('set_select'=> FALSE, 'title'=> 'Điểm cam kết', 'set_order'=>FALSE))
		->add_column('contract_value', array('set_select' => FALSE,'title'=> 'Trước VAT'))
		->add_column('payment_percentage', array('set_select' => FALSE,'title'=> 'T/đ thanh toán'))
		->add_column('technical_staffs', array('set_select'=>FALSE,'title'=>'Kỹ thuật','set_order'=>FALSE))
		->add_column('staff_business', array('set_select'=>FALSE,'title'=>'Kinh Doanh'))
		->add_column('action', array('set_select'=>FALSE,'title'=>'Actions','set_order'=>FALSE))
		
		->add_column_callback('term_name',function($data,$row_name){
			$data['term_name'] = anchor(module_url("overview/{$data['term_id']}"),$data['term_name']);
			return $data;
		},FALSE)

		->add_column_callback('contract_begin',function($data,$row_name){
			$data['contract_begin'] = my_date(get_term_meta_value($data['term_id'], 'contract_begin')) ;
			return $data;
		},FALSE)
		
		->add_column_callback('contract_end',function($data,$row_name){
			$data['contract_end'] = my_date(get_term_meta_value($data['term_id'], 'contract_end')) ;
			return $data;
		},FALSE)

		->add_column_callback('score_commitment_weboptimize',function($data,$row_name){

			$mobile_score 		= get_term_meta_value($data['term_id'], 'score_commitment_mobile_weboptimize') ?: $this->config->item('score_commitment_mobile_weboptimize', 'pagespeed');

			$desktop_score 		= get_term_meta_value($data['term_id'], 'score_commitment_desktop_weboptimize') ?: $this->config->item('score_commitment_desktop_weboptimize', 'pagespeed');

			$data['score_commitment_weboptimize'] = "<p><span class='label label-success'>Desktop: {$desktop_score}/100</span></p><p><span class='label label-success'>Mobile: {$mobile_score}/100</span></p>";

			return $data;
		},FALSE)

		->add_column_callback('contract_value',function($data,$row_name){
			$data['contract_value'] = currency_numberformat(get_term_meta_value($data['term_id'],'contract_value'),'đ');
			return $data;
		},FALSE)

		->add_column_callback('payment_percentage',function($data,$row_name){
			$term_id = $data['term_id'];
			$invs_amount = (double) get_term_meta_value($term_id,'invs_amount');
			$payment_amount = (double) get_term_meta_value($term_id,'payment_amount');

			$payment_percentage = 100 * (double) get_term_meta_value($term_id,'payment_percentage');

			if($payment_percentage >= 50 && $payment_percentage < 80) $text_color = 'text-yellow';
			else if($payment_percentage >= 80 && $payment_percentage < 90) $text_color = 'text-light-blue';
			else if($payment_percentage >= 90) $text_color = 'text-green';
			else $text_color = 'text-red';

			$payment_percentage = currency_numberformat($payment_percentage,' %');
			$data['payment_percentage'] = "<b class='{$text_color}'>{$payment_percentage}</b>";
			return $data;
		},FALSE)

		->add_column_callback('staff_business',function($data,$row_name) {
			$sale_id = get_term_meta_value($data['term_id'],'staff_business');
			$data['staff_business'] = $this->admin_m->get_field_by_id($sale_id,'display_name');


			$term_id = $data['term_id'];
			# Lấy thông tin kỹ thuật
			$data['technical_staffs'] = '<code>Chưa cấu hình</code>';
			/* Lấy thông tin kỹ thuật thực hiện hợp đồng */
			$tech_kpis = $this->webgeneral_kpi_m->get_kpis($term_id, 'users', 'tech');
			if( ! empty($tech_kpis))
			{	
				$tech_ids = array();
				foreach ($tech_kpis as $item) $tech_ids = array_merge($tech_ids, array_keys($item));

				$data['technical_staffs'] = implode(',', array_map(function($x){
					return '<p>'.$this->admin_m->get_field_by_id($x,'display_name') ?: $this->admin_m->get_field_by_id($x,'user_mail').'</p>';
				}, $tech_ids));


				# Load user's group
				if( ! empty($tech_ids))
				{
					$user_groups_f 	= implode(', ',array_map(function($x){
						
						$ugroups 	= $this->term_users_m->get_user_terms($x, 'user_group');
						if(empty($ugroups)) return FALSE;
						return implode(', ',array_map(function($x){return $x->term_name;},$ugroups));

					}, $tech_ids));

					$data['technical_staffs'].= "<br/><small><b><u><i>{$user_groups_f}</i></u></b></small>";
				}
			}

			return $data;
	  	},FALSE)
		
		->add_column_callback('action', function($data, $row_name) {
		
			if(has_permission('weboptimize.overview.access'))
			{
				$data['action'].= anchor(module_url("overview/{$data['term_id']}"),'<i class="fa fa-fw fa-line-chart"></i>','data-toggle="tooltip" title="Tổng quan"');
			}

			$data['action'].= anchor("admin/contract/edit/{$data['term_id']}", '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>','data-toggle="tooltip" title="Hợp đồng"');

			return $data;
		}, FALSE)
		->from('term');

		$status_list = array_keys($this->config->item('contract_status'));
		array_unshift($status_list, 1);

		$this->datatable_builder
		->where_in('term_status', $status_list)
		->where('term_type','weboptimize')
		->group_by('term.term_id');

		$pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

		$data = $this->datatable_builder->generate($pagination_config);

		// OUTPUT : DOWNLOAD XLSX
		if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
		{
			$this->export($last_query);
			return TRUE;
		}

		
		return parent::renderJson($data);

	}

	protected function search_filter($search_args = array())
	{
		$args = $this->input->get();

		if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

		// Contract_begin FILTERING & SORTING
		$filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
		$sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
		if($filter_contract_begin || $sort_contract_begin)
		{
			$alias = uniqid('contract_begin_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

			if($filter_contract_begin)
			{	
				$dates = explode(' - ', $filter_contract_begin);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['contract_begin']);
			}

			if($sort_contract_begin)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_begin);
				unset($args['order_by']['contract_begin']);
			}
		}

		// Contract_end FILTERING & SORTING
		$filter_contract_end = $args['where']['contract_end'] ?? FALSE;
		$sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
		if($filter_contract_end || $sort_contract_end)
		{
			$alias = uniqid('contract_end_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_end'", 'LEFT');

			if($filter_contract_end)
			{	
				$dates = explode(' - ', $filter_contract_end);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['contract_end']);
			}

			if($sort_contract_end)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_end);
				unset($args['order_by']['contract_end']);
			}
		}

		// Staff_business FILTERING & SORTING
		$filter_staff_business = $args['where']['staff_business'] ?? FALSE;
		$sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
		if($filter_staff_business || $sort_staff_business)
		{
			$alias = uniqid('staff_business_');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT')
			->join("user","{$alias}.meta_value = user.user_id", 'LEFT');

			if($filter_staff_business)
			{	
				$this->datatable_builder->like("user.display_name",$filter_staff_business);
				unset($args['where']['staff_business']);
			}

			if($sort_staff_business)
			{
				$this->datatable_builder->order_by('user.display_name',$sort_staff_business);
				unset($args['order_by']['staff_business']);
			}
		}

		// contract_value FILTERING & SORTING
		$filter_contract_value = $args['where']['contract_value'] ?? FALSE;
		$sort_contract_value = $args['order_by']['contract_value'] ?? FALSE;
		if($filter_contract_value || $sort_contract_value)
		{
			$alias = uniqid('contract_value_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_value'", 'LEFT');

			if($filter_contract_value)
			{		
				$this->datatable_builder->where("{$alias}.meta_value", $filter_contract_value);
				unset($args['where']['contract_value']);
			}

			if($sort_contract_value)
			{
				$this->datatable_builder->order_by("({$alias}.meta_value)*1",$sort_contract_value);
				unset($args['order_by']['contract_value']);
			}
		}

		// payment_percentage FILTERING & SORTING
		$filter_payment_percentage = $args['where']['payment_percentage'] ?? FALSE;
		$sort_payment_percentage = $args['order_by']['payment_percentage'] ?? FALSE;
		if($filter_payment_percentage || $sort_payment_percentage)
		{
			$alias = uniqid('payment_percentage_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_percentage'", 'LEFT');

			if($filter_payment_percentage)
			{		
				$this->datatable_builder->where("{$alias}.meta_value", $filter_payment_percentage);
				unset($args['where']['payment_percentage']);
			}

			if($sort_payment_percentage)
			{
				$this->datatable_builder->order_by("({$alias}.meta_value)*1",$sort_payment_percentage);
				unset($args['order_by']['payment_percentage']);
			}
		}

		if(!empty($args['where']['technical_staffs']))
		{
			$this->datatable_builder->join('webgeneral_kpi','term.term_id = webgeneral_kpi.term_id', 'LEFT OUTER');
			$this->datatable_builder->join('user','user.user_id = webgeneral_kpi.user_id', 'LEFT OUTER');
			$this->datatable_builder->like('user.user_email',strtolower($args['where']['technical_staffs']),'both');

			unset($args['where']['technical_staffs']);
		}

		if(!empty($args['order_by']['technical_staffs']))
		{
			$this->datatable_builder->join('googleads_kpi','term.term_id = googleads_kpi.term_id', 'LEFT OUTER');
			$this->datatable_builder->join('user','user.user_id = googleads_kpi.user_id', 'LEFT OUTER');
			$args['order_by']['user.user_email'] = $args['order_by']['technical_staffs'];
			unset($args['order_by']['technical_staffs']);
		}

		// APPLY DEFAULT FILTER BY MUTATE FIELD		
		$args = $this->datatable_builder->parse_relations_searches($args);
		if( ! empty($args['where']))
		{
			foreach ($args['where'] as $key => $value)
			{
				if(empty($value)) continue;

				if(empty($key))
				{
					$this->datatable_builder->add_filter($value, '');
					continue;
				}

				$this->datatable_builder->add_filter($key, $value);
			}
		}

		if(!empty($args['order_by'])) 
		{
			foreach ($args['order_by'] as $key => $value)
			{
				$this->datatable_builder->order_by($key, $value);
			}
		}
	}
}
/* End of file Ajax.php */
/* Location: ./application/modules/weboptimize/controllers/Ajax.php */