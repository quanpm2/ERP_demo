	<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['packages'] = array(
    'service' => array(),
    'default' =>''
);

$config['task_type'] = array(
	'post-write' => 'Tự viết bài', 
	'post-optimize'=>'Tối ưu bài viết', 
	'other' =>'Việc khác', 
	'product-write' =>'Đăng sản phẩm',
	'product-edit' =>'Sửa sản phẩm',
	'review' =>'Review Website',
	'custom-task' =>'Yêu cầu khách hàng'
);

$config['tasks_type'] = array(
	'task-default'		 	=>'Tối ưu website',
	'custom-task' 		 	=>'Yêu cầu khách hàng', 
	/*'task-design-default'	=>'Thiết kế Website ban đầu', 
	'banner' 			 	=>'Thiết kế Banner'*/
	);

$config['post_type'] = array(
	'task'				=>'weboptimize-task',
	'content'		 	=>'weboptimize-content',
	'content_product'	=>'weboptimize-content-product',
	'mail' 				=>'weboptimize-mail'
	);

$config['pagespeed'] = array(
	'score_commitment_mobile_weboptimize' 	=> 80 ,
	'score_commitment_desktop_weboptimize'	=> 80 
);

$config['request'] = array(
	'timeout' 	=> 800 ,
	'connect_timeout'	=> 800 
);

