<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weboptimize_report_m extends Base_Model {

	public function __construct() 
	{
		parent::__construct();

		$this->load->library('email');

		$this->load->model('staffs/admin_m');
		$this->load->model('weboptimize/weboptimize_kpi_m');

		$this->config->load('weboptimize/weboptimize') ;

		defined('SALE_MANAGER_ID') OR define('SALE_MANAGER_ID',18);
	}

	// Thông báo khởi tạo hợp đồng
	public function send_mail_info_activated($term_id = 0)
	{
		$title  = 'Hợp đồng Tối ưu Website ' . get_term_meta_value($term_id, 'contract_code'). ' đã được kích hoạt.';

		$data['term_id']   = $term_id ;
		$data['title']     = $title ; 
		$data['term']      = $this->contract_m->get($term_id);

		// Hiển thị thông tin kinh doanh ra ngoài view
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	   = $sale;

		// Hiển thị thông tin kỹ thuật phụ trách
        $tech_kpi 		= $this->weboptimize_kpi_m
				        ->select('user_id')
				        ->group_by('user_id')
				        ->get_many_by(['term_id'=>$term_id]);

		$data['techs']	= array() ;	        

		if(!empty($tech_kpi))
        {
        		foreach ($tech_kpi as $i)
        		{
        			$mail  = $this->admin_m->get_field_by_id($i->user_id,'user_email');
        			$data['techs']['email'] = $mail;

        			$phone = get_user_meta_value($i->user_id,'user_phone');
        			$data['techs']['phone'] = $phone;

        			$name  = $this->admin_m->get_field_by_id($i->user_id,'display_name') ;
        			$data['techs']['name'] = $name;
        		}
        }

		$this->set_mail_to($term_id,'customer');
		$this->email->subject($title);
		$this->email->message($this->load->view('admin/send-mail/tpl_activation_email', $data, TRUE));
		
		$send_status 			  				= $this->email->send();
		if(!$send_status) return FALSE;

		// Thực hiện ghi log	
		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Gửi mail kích hoạt Hợp đồng Tối ưu website',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id,
			'log_content'	=> 'Đã gửi mail thành công',
			'log_type'		=> 'weboptimize-report-email',
			'log_time_create' => date('Y-m-d H:i:s'),
		));

		return TRUE;
	}

	// Thông báo kết thúc hợp đồng
	public function send_mail_info_finish($term_id = 0)
	{
		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

		//gửi mail kết thúc hợp đồng
		$data = array();
		$data['time'] = time();
	
		$data['term'] = $term;
		$data['term_id'] = $term_id;

		$title 			   = 'Thông báo kết thúc Hợp đồng Tối ưu website ' . get_term_meta_value($term_id, 'contract_code');
		$data['title'] 	   = $title ;

		// Hiển thị thông tin kinh doanh ra ngoài view
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);

		$data['staff'] 	   = $sale;

		// Hiển thị thông tin kỹ thuật phụ trách
        $tech_kpi 		= $this->weboptimize_kpi_m
				        ->select('user_id')
				        ->group_by('user_id')
				        ->get_many_by(['term_id'=>$term_id]);

		$data['techs']	= array() ;	        

		if(!empty($tech_kpi))
        {
        		foreach ($tech_kpi as $i)
        		{
        			$mail  = $this->admin_m->get_field_by_id($i->user_id,'user_email');
        			$data['techs']['email'] = $mail;

        			$phone = get_user_meta_value($i->user_id,'user_phone');
        			$data['techs']['phone'] = $phone;

        			$name  = $this->admin_m->get_field_by_id($i->user_id,'display_name') ;
        			$data['techs']['name'] = $name;
        		}
        }

		// customer-mailreport-admin
		$this->set_mail_to($term_id, 'customer') ;
		     //->set_mail_to($term_id, 'mailreport')
		     //->set_mail_to($term_id, 'admin') ;
		$this->email->subject($title);
		
		$this->email->message($this->load->view('admin/send-mail/end_contract', $data, true));

 		$send_status 		= $this->email->send();
		if ( FALSE === $send_status ) return FALSE ;
		
		// Thực hiện ghi log	
		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Gửi mail kết thúc Hợp đồng Tối ưu website',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id,
			'log_content'	=> 'Đã gửi mail thành công',
			'log_type'		=> 'weboptimize-report-email',
			'log_time_create' => date('Y-m-d H:i:s'),
		));

		return TRUE ;
	} 

	/**
	 * Sets the mail to.
	 *
	 * @param      integer  $term_id  The term identifier
	 * @param      string   $type_to  The type to [admin|customer|mailreport|specified]
	 *
	 * @return     self     ( description_of_the_return_value )
	 */
	public function set_mail_to($term_id = 0,$type_to = 'admin')
    {
        $this->email->clear(TRUE);

        $recipients = array(
            'mail_to' => array(),
            'mail_cc' => array(),
            'mail_bcc' => array()
        );

        if(is_array($type_to) && !empty($type_to)) 
        {
            $recipients = array_merge_recursive($type_to,$recipients);
            $type_to = $type_to['type_to'] ?? 'specified';
        }

        extract($recipients);

        $this->email->from('support@webdoctor.vn', 'Webdoctor.vn');

        // Nếu email được cấu hình phải gửi cho một nhóm người được truyền vào
        if($type_to == 'specified')
        {
        	$this->recipients = $mail_to;
	        $this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
	        return $this;
        }

        // Nếu email được cấu hình gửi cho nhóm quản lý
        if($type_to == 'mailreport')
        {
        	$mail_to = array('kythuat@webdoctor.vn');
        	$this->recipients = $mail_to;
        	$this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
	        return $this;
        }

        // Nếu KD nghỉ, người nhận là người quản lý
        $sale_id = get_term_meta_value($term_id,'staff_business');
        $sale 	 = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID); 
        
        // Lấy thông tin kỹ thuật phụ trách
        $tech_kpi = $this->weboptimize_kpi_m
        ->select('user_id')
        ->group_by('user_id')
        ->get_many_by(['term_id'=>$term_id]);

        // Nếu gửi mail cho 'admin'
        if($type_to == 'admin')
        {
        	if(!empty($tech_kpi))
        	{
        		foreach ($tech_kpi as $i)
        		{
        			$mail = $this->admin_m->get_field_by_id($i->user_id,'user_email');
        			if(empty($mail)) continue;

        			$mail_to[] = $mail;
        		}
        	}

        	if($sale)
        	{
        		$mail_cc[] = $sale->user_email;
        	}

        	$this->recipients = $mail_to;
        	$this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
	        return $this;
        }

        // Nếu gửi mail cho khách hàng
		if($representative_email = get_term_meta_value($term_id,'representative_email'))
		{
			$mail_to[] = $representative_email;
		}

		// Email người nhận báo cáo được cấu hình theo dịch vụ
		if($mail_report = get_term_meta_value($term_id,'mail_report'))
		{
			$mails = explode(',',trim($mail_report));
			if(!empty($mails))
			{
				foreach ($mails as $mail) 
				{
					$mail_to[] = $mail;
				}
			}
		}

		if(!empty($tech_kpi))
    	{
    		foreach ($tech_kpi as $i)
    		{
    			$mail = $this->admin_m->get_field_by_id($i->user_id,'user_email');
    			if(empty($mail)) continue;

    			$mail_cc[] = $mail;
    		}
    	}

    	if($sale)
    	{
    		$mail_cc[] = $sale->user_email;
    	}

        $this->recipients = $mail_to;

        $this->email
	        ->from('support@webdoctor.vn', 'Webdoctor.vn')
	        ->to($mail_to)
	        ->cc($mail_cc)
	        ->bcc($mail_bcc);
        
        return $this;
    }


	// Thông báo chuẩn bị hết hạn hợp đồng
	public function send_mail_alert_expiring_soon($term_id = 0, $type = '' , $day = '')
	{
		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

		$data 			 = array();
		$data['time']    = time();
		$data['title']   = 'Cảnh báo sắp hết hạn hợp đồng ' . get_term_meta_value($term_id, 'contract_code');
		$data['term']    = $term;
		$data['term_id'] = $term_id;

		// Hiển thị thông tin kinh doanh ra ngoài view
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	   = $sale;

		// Hiển thị thông tin kỹ thuật phụ trách
        $tech_kpi 		= $this->weboptimize_kpi_m
				        ->select('user_id')
				        ->group_by('user_id')
				        ->get_many_by(['term_id'=>$term_id]);

		$data['techs']	= array() ;	        

		if(!empty($tech_kpi))
        {
        		foreach ($tech_kpi as $i)
        		{
        			$mail  = $this->admin_m->get_field_by_id($i->user_id,'user_email');
        			$data['techs']['email'] = $mail;

        			$phone = get_user_meta_value($i->user_id,'user_phone');
        			$data['techs']['phone'] = $phone;

        			$name  = $this->admin_m->get_field_by_id($i->user_id,'display_name') ;
        			$data['techs']['name'] = $name;
        		}
        }

		$this->set_mail_to($term_id,'customer');
			 //->set_mail_to($term_id, 'admin');
		$this->email->subject($data['title']);
		$this->email->message($this->load->view('admin/send-mail/warning_expires', $data, true));

		$send_status   = $this->email->send();
		if( FALSE === $send_status ) return FALSE ;

		if( ! empty($type) ) $type =  '_' . $type ; // before || after
		if( ! empty($day) && is_integer($day) )  $day  =  '_' . $day . 'day' ; // day : int 		

		// Thực hiện ghi log
		$log_id = $this->log_m->insert(array(
						'log_title'			=> 'Gửi mail cảnh báo sắp hết hạn Hợp đồng Tối ưu website' ,
						'log_status'		=> 1,
						'term_id'			=> $term_id,
						'user_id'			=> $this->admin_m->id,
						'log_content'		=> 'Đã gửi mail cảnh báo thành công',
						'log_type'			=> 'warning_weboptimize_deadline' . $type . $day,
						'log_time_create' 	=> date('Y-m-d H:i:s')
				  )); 

		if($log_id) return TRUE ;
	}

	// send mail cho kinh doanh, kỹ thuật
	public function send_mail_info_to_admin($term_id)
	{
		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

        $time = time();

		$contract_code 		= get_term_meta_value($term_id, 'contract_code');
		$title 				= 'Hợp đồng tối ưu website ' . $contract_code . ' đã được khởi tạo.';
		$data 				= array();
		$data['title'] 		= $title ;

		$data['term']		= $term;
		$data['term_id'] 	= $term_id;
		$data['time']       = $time ;

	
		// Hiển thị thông tin kinh doanh ra ngoài view
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		
		$data['staff'] 	   = $sale;

		// Hiên thị thông tin kỹ thuật phụ trách
		$tech_kpi 		   = $this->weboptimize_kpi_m->select('user_id')
        											 ->group_by('user_id')
        											 ->get_many_by(['term_id'=>$term_id]);

      	if(!empty($tech_kpi))
        {
    		foreach ($tech_kpi as $i)
    		{
    			$user_mail  = $this->admin_m->get_field_by_id($i->user_id,'user_email');
    		}
        }  											 

		$this->set_mail_to($term_id,'admin');
		$this->email->subject($title);
		$this->email->message($this->load->view('weboptimize/admin/send-mail/send_mail_admin', $data, TRUE));

		$send_status 			  				= $this->email->send();

		if( FALSE === $send_status ) return FALSE ;

		// Thực hiện ghi log	
		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Thông báo khởi tạo Hợp đồng tối ưu website',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id,
			'log_content'	=> 'Đã gửi mail thành công',
			'log_type'		=> 'weboptimize-report-email',
			'log_time_create' => date('Y-m-d H:i:s'),
		));

		return TRUE ;
	}

	// 
	public function send_mail_acceptance($term_id = 0)
	{        
        if(empty($term_id)) return FALSE;
        $term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

		$data 			 = array();

		$data['title']   = '[WEBDOCTOR] Thông báo nghiệm thu Hợp đồng tối ưu website, ngày ' . date('d-m-Y H:i:s', time());
		$data['term']    = $term;
		$data['term_id'] = $term_id;

		// Hiển thị thông tin kinh doanh ra ngoài view
		$sale_id      	 = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		 = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	 = $sale;

		// Hiển thị thông tin kỹ thuật phụ trách
        $tech_kpi 		= $this->weboptimize_kpi_m
				        ->select('user_id')
				        ->group_by('user_id')
				        ->get_many_by(['term_id'=>$term_id]);

		$data['techs']	= array() ;	        

		if(!empty($tech_kpi))
        {
        		foreach ($tech_kpi as $i)
        		{
        			$mail  = $this->admin_m->get_field_by_id($i->user_id,'user_email');
        			$data['techs']['email'] = $mail;

        			$phone = get_user_meta_value($i->user_id,'user_phone');
        			$data['techs']['phone'] = $phone;

        			$name  = $this->admin_m->get_field_by_id($i->user_id,'display_name') ;
        			$data['techs']['name'] = $name;
        		}
        }

		$tasks 			 = $this->post_m->select('`post_title`, `post_content`')
				 	 		   		    ->join('term_posts', '`term_posts`.`post_id` = `posts`.`post_id`')
				 	 		   		    ->get_many_by(array('post_type' => $this->weboptimize_m->get_post_type('task'), 'term_id' => $term_id, 'post_status' => 'complete')) ;

		$data['tasks'] 	 = $tasks ;
		
		$content = $this->load->view('admin/send-mail/acceptance_weboptimize', $data, TRUE);

		$this->log_m->insert(array(
				'log_title'			=> 'Email nghiệm thu dịch vụ tối ưu Web' ,
				'log_status'		=> 1,
				'term_id'			=> $term_id,
				'user_id'			=> $this->admin_m->id,
				'log_content'		=> '',
				'log_type'			=> 'send_mail_acceptance_weboptimize',
				'log_time_create' 	=> date('Y-m-d H:i:s')
			)); 

		$this->set_mail_to($term_id,'customer');
		$this->email->subject($data['title']);
		$this->email->message($content);
		$send_status   = $this->email->send();

		$has_acceptance_weboptimize = get_term_meta_value($term_id,'has_acceptance_weboptimize');
		if(empty($has_acceptance_weboptimize))
		{
			update_term_meta($term_id, 'has_acceptance_weboptimize', time());
		}

		return $send_status;
	}
}
/* End of file weboptimize_report_m.php */
/* Location: ./application/modules/weboptimize/models/weboptimize_report_m.php */