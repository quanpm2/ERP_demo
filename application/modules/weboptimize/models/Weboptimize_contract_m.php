<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weboptimize_contract_m extends Base_contract_m {

	public function __construct() 
	{
		parent::__construct();
	}

	public function calc_contract_value($term_id = 0)
	{
		$term = $this->term_m->where('term_type','weboptimize')->get($term_id);

		if(empty($term)) return FALSE;

		$total 			 		  	  = 0;

		$weboptimize_cost 	 		  = get_term_meta_value($term->term_id,'contract_price_weboptimize') ;
		$weboptimize_discount_percent = get_term_meta_value($term->term_id,'weboptimize_discount_percent');

		if(!empty($weboptimize_discount_percent) && $weboptimize_discount_amound = ($weboptimize_cost * div($weboptimize_discount_percent,100)))
		{
			$weboptimize_cost-=$weboptimize_discount_amound;
		}

		$total+= $weboptimize_cost;
		return $total;
	}
	
	public function has_permission($term_id = 0, $name = '', $action = '',$kpi_type = '',$user_id = 0,$role_id = null)
	{
		$this->load->model('weboptimize/weboptimize_kpi_m');

		$permission = $name.'.'.$action;
		$permission = trim($permission, '.');
		
		if($this->permission_m->has_permission("{$name}.Manage",$role_id)
			&& $this->permission_m->has_permission($permission,$role_id)) 
			return TRUE;

		if(!$this->permission_m->has_permission($permission,$role_id)) return FALSE;

		$user_id = empty($user_id) ? $this->admin_m->id : $user_id;

		if($this->permission_m->has_permission('weboptimize.sale.access',$role_id) 
			&& get_term_meta_value($term_id,'staff_business') == $user_id)
			return TRUE;

		$args = array(
			'term_id' => $term_id,
			'user_id' => $user_id);

		if(!empty($kpi_type))
		{
			$args['kpi_type'] = $kpi_type;
		}

		$kpis = $this->webgeneral_kpi_m->get_by($args);
		if(empty($kpis)) return FALSE;

		return TRUE;
	}

	public function start_service($term = null)
	{
		restrict('weboptimize.start_service');	
		$this->load->model('weboptimize/weboptimize_report_m') ;
		if(empty($term) )
			return false;

		if(!$term || $term->term_type !='weboptimize')
			return false;
		$term_id = $term->term_id;
		$this->weboptimize_report_m->send_mail_info_to_admin($term_id);
		
		/***
		 * Khởi tạo Meta thời gian dịch vụ để thuận tiện cho tính năng sắp xếp và truy vấn
		 */
		$start_service_time = get_term_meta_value($term_id, 'start_service_time');
		empty($start_service_time) AND update_term_meta($term_id, 'start_service_time', 0);
		
		$end_service_time = get_term_meta_value($term_id, 'end_service_time');
		empty($end_service_time) AND update_term_meta($term_id, 'end_service_time', 0);
		
		return true;
	}

	public function proc_service($term = FALSE)
	{
		if(!$term || $term->term_type != 'weboptimize') return FALSE;
		$term_id = $term->term_id;

        $tech_kpis           = $this->weboptimize_kpi_m->select('user_id')
                                                   ->where('term_id',$term_id)
                                                   ->where('kpi_type','tech')
                                                   ->as_array()
                                                   ->get_many_by(); 

        //if(empty($tech_kpis)) $this->messages->info('Chưa có nhân viên kỹ thuật');
        if(empty($tech_kpis)) return FALSE;
  
  		update_term_meta($term_id, 'start_service_time' , time());

		$is_send = $this->weboptimize_report_m->send_mail_info_activated($term_id) ;

		if(FALSE === $is_send) return FALSE ;

		$this->log_m->insert(array(
						'log_title'		  => 'Kích hoạt thời gian bắt đầu Tối ưu website',
						'log_status'	  => 1,
						'term_id'		  => $term_id,
						'log_type' 		  => 'start_service_time',
						'user_id' 		  => $this->admin_m->id,
						'log_content' 	  => 'Đã gửi mail kích hoạt kết thúc Tối ưu website',
						'log_time_create' => date('Y-m-d H:i:s'),
					 ));

		return TRUE;
	}

	public function stop_service($term)
	{
		if(!$term || $term->term_type != 'weboptimize') return FALSE;
		$term_id = $term->term_id;
	
		$is_send = $this->weboptimize_report_m->send_mail_info_finish($term_id);
		if( FALSE === $is_send) return FALSE ;

		update_term_meta($term_id,'end_service_time',time());
		$this->contract_m->update($term_id, array('term_status'=>'ending'));

		$this->log_m->insert(array(
						'log_title'		  => 'Kích hoạt thời gian ngừng Hợp đồng tối ưu website',
						'log_status'	  => 1,
						'term_id'		  => $term_id,
						'log_type' 		  =>'end_service_time',
						'user_id' 		  => $this->admin_m->id,
						'log_content' 	  => 'Đã gửi mail ngừng Hợp đồng tối ưu website',
						'log_time_create' => date('Y-m-d H:i:s'),
					 ));

		return TRUE;
	}

}

/* End of file weboptimize_report_m.php */
/* Location: ./application/modules/weboptimize/models/weboptimize_report_m.php */