<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Behaviour_m.php');

class Weboptimize_behaviour_m extends Behaviour_m {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
	}

	/**
	 * Calculates the contract value.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     integer  The contract value.
	 */
	public function calc_contract_value($term_id = 0)
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$term_id = $this->contract->term_id;

		$total 			 		  	  = 0;
		$weboptimize_cost 	 		  = (int) get_term_meta_value($term_id,'contract_price_weboptimize') ;
		$weboptimize_discount_percent = (int) get_term_meta_value($term_id,'weboptimize_discount_percent');

		if(!empty($weboptimize_discount_percent) && $weboptimize_discount_amound = ($weboptimize_cost * div($weboptimize_discount_percent,100)))
		{
			$weboptimize_cost-=$weboptimize_discount_amound;
		}

		$total+= $weboptimize_cost;
		return $total;
	}

	/**
	 * Preview data for printable contract version
	 *
	 * @return     String  HTML content
	 */
	public function prepare_preview()
	{
		if( ! $this->contract) return FALSE;

		parent::prepare_preview();
		
		$this->data['content'] = $this->load->view('weboptimize/contract/preview', $this->data ,TRUE);

		return $this->data;
	}
}
/* End of file Weboptimize_behaviour_m.php */
/* Location: ./application/modules/weboptimize/models/Weboptimize_behaviour_m.php */