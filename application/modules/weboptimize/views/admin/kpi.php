<?php
$rows_id = array();
$data = array();
$start_month = $this->mdate->startOfMonth();

$this->table->set_heading(array('STT','Kỹ thuật thực hiện','Hành động'));
$this->table->set_caption('Kỹ thuật thực hiện');

if(!empty($targets['tech']))
{
  $i = 0;
  foreach ($targets['tech'] as $kpi) 
  {
    $btn_delete = anchor(module_url('kpi/'.$term_id.'/'.$kpi['kpi_id'].'/delete'), 'Xóa', 'class="btn btn-danger btn-flat btn-xs"');
    $username = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');
    $this->table->add_row(++$i,$username,$btn_delete);
  }
}

echo $this->table->generate();

echo $this->admin_form->form_open();
echo $this->admin_form->dropdown('Nhân viên thực hiện', 'user_id', $users, '');
echo $this->admin_form->hidden('','target_date',time());
echo $this->admin_form->hidden('','target_type','tech');
echo $this->admin_form->hidden('','target_post','1');
echo $this->admin_form->submit('submit_kpi_tech','Lưu lại');
echo $this->admin_form->form_close();
?>