<?php   
    $this->template->javascript->add('plugins/validate/jquery.validate.js');
    $this->template->stylesheet->add('plugins/bootstrap-fileinput/css/fileinput.css');
    $this->template->javascript->add('plugins/bootstrap-fileinput/js/fileinput.min.js');
?>
<?php

    $representative_gender = get_term_meta_value($term_id, 'representative_gender');
    $contract_begin        = get_term_meta_value($term_id, 'contract_begin');
    $contract_end          = get_term_meta_value($term_id, 'contract_end');
    $contract_region       = get_term_meta_value($term_id, 'contract_region');

    $start_service_time      = (!get_term_meta_value($term_id, 'start_service_time')) ? '--' : date('d-m-Y', get_term_meta_value($term_id, 'start_service_time')) ;
    $end_service_time        = (!get_term_meta_value($term_id, 'end_service_time')) ? '--' : date('d-m-Y', get_term_meta_value($term_id, 'end_service_time')) ;

    $contract_price_weboptimize       = get_term_meta_value($term_id, 'contract_price_weboptimize') ;

    $sale_id               = get_term_meta_value($term_id,'staff_business');

    $staff_business        = 'Chưa có' ;
    if($sale_id) $staff_business     = $this->admin_m->get_field_by_id($sale_id,'display_name');

    $tech_kpis             = $this->weboptimize_kpi_m->select('user_id')
                                                     ->where('term_id',$term_id)
                                                     ->where('kpi_type','tech')
                                                     ->as_array()
                                                     ->get_many_by();

    $staffs_ids = array_column($tech_kpis, 'user_id');

    $technical_staffs       = 'Chưa có';
    if(!empty($staffs_ids)) 
    {
        foreach ($staffs_ids as $staff_id)
        {
            $user_name                             = $this->admin_m->get_field_by_id($staff_id,'display_name');        
            if($user_name)  $technical_staffs      = $user_name ?: $this->admin_m->get_field_by_id($staff_id,'user_mail');
        }
    }

    $ajax_segment = "contract/ajax_dipatcher/ajax_edit/{$term_id}" ;
?>
<div class="row">
    <div class="col-sm-6">
        <?php 
            $contract_price_weboptimize = ($contract_price_weboptimize > 0) ? number_format($contract_price_weboptimize) : 0 ; 
            $this->table->set_caption('Thông tin hợp đồng')
                        ->add_row(array('Mã hợp đồng',get_term_meta_value($term_id, 'contract_code')))
                        ->add_row('Ngày bắt đầu', empty($contract_begin) ? $empty_chars : my_date($contract_begin,'d/m/Y'))
                        ->add_row('Ngày kết thúc', empty($contract_end) ? $empty_chars : my_date($contract_end,'d/m/Y'))
                        ->add_row('Phí tối ưu website', $contract_price_weboptimize  . ' VNĐ')
                        ->add_row('Ngày bắt đầu tối ưu', $start_service_time)
                        ->add_row('Ngày kết thúc tối ưu', $end_service_time)
                        ->add_row('Nhân viên kinh doanh', $staff_business)
                        ->add_row('Kỹ thuật thực hiện', @$technical_staffs) ;   
            echo $this->table->generate();
            $this->table->clear();
        ?>

    </div>
    <div class="col-sm-6">
            <input type="hidden" name="term_id" value="<?php echo $term_id ; ?>" />
             <?php $this->table->set_caption('Thông tin khách hàng')
                               ->add_row('Người đại diện',
          '<span id="incline-data">' .($representative_gender == 1 ? 'Ông ' : 'Bà ').str_repeat('&nbsp', 2).get_term_meta_value($term_id,'representative_name').'</span>')
                               ->add_row('Email',get_term_meta_value($term_id, 'representative_email')) ;
                    echo $this->table->generate(); 
                    $this->table->clear();
             ?> 
            <div class="row">
                <div class="col-sm-6">
                    
                    <?php  
                        // Xem điểm dạng Ajax

                        $has_before_check_optimize_pagespeed = get_term_meta_value($term_id, 'has_before_check_optimize_pagespeed') ?: '';
                        if( empty($has_before_check_optimize_pagespeed) ) {
                            $btn_before_check_optimize_pagespeed = form_button(['class' => 'btn bg-olive btn-flat margin btn-xs pull-right', 'id' => 'has-check-before-optimize-pagespeed', 'name' => 'allow-ajax', 'style' => 'visibility: hidden;'],'<i class="fa fa-refresh fa-fw"></i> Xem điểm');
                        }
                        else 
                        {
                            $btn_before_check_optimize_pagespeed = form_button(['class' => 'btn bg-olive btn-flat margin btn-xs disabled pull-right', 'id' => 'has-check-before-optimize-pagespeed', 'style' => 'visibility: hidden;'],'<i class="fa fa-refresh fa-fw"></i> Xem điểm');
                        }

                        $before_weboptimize_score_mobile = (empty(get_term_meta_value($term_id, 'before_weboptimize_score_mobile'))) ? '--' : get_term_meta_value($term_id, 'before_weboptimize_score_mobile') ;

                        $before_weboptimize_score_desktop = (empty(get_term_meta_value($term_id, 'before_weboptimize_score_desktop'))) ? '--' : get_term_meta_value($term_id, 'before_weboptimize_score_desktop') ;

                        $before_weboptimize_score_usability = (empty(get_term_meta_value($term_id, 'before_weboptimize_score_usability'))) ? '--' : get_term_meta_value($term_id, 'before_weboptimize_score_usability') ;

                        $before_weboptimize_size_web = (empty(get_term_meta_value($term_id, 'before_weboptimize_size_web'))) ? '--' : get_term_meta_value($term_id, 'before_weboptimize_size_web') ;

                        $this->table->clear();
                        $this->table->set_caption('Trước tối ưu'. $btn_before_check_optimize_pagespeed)
                                    // Mobile
                                    ->add_row('Điểm Mobile', $before_weboptimize_score_mobile)
                                        
                                    // Desktop
                                    ->add_row('Điểm Desktop', $before_weboptimize_score_desktop)
                                            
                                    // Thân thiện
                                    ->add_row('Điểm thân thiện', $before_weboptimize_score_usability)
                                
                                    // Dung lượng page
                                    ->add_row('Dung lượng trang', $before_weboptimize_size_web);  

                        echo $this->table->generate();        
                    ?>
                </div>
                <div class="col-sm-6">
                    <?php 
                        $score_commitment_mobile_weboptimize = empty(get_term_meta_value($term_id, 'score_commitment_mobile_weboptimize')) ? 0 : get_term_meta_value($term_id, 'score_commitment_mobile_weboptimize') ;
                        $score_commitment_desktop_weboptimize = empty(get_term_meta_value($term_id, 'score_commitment_desktop_weboptimize')) ? 0 : get_term_meta_value($term_id, 'score_commitment_desktop_weboptimize') ;
                        $after_weboptimize_score_mobile  = get_term_meta_value($term_id, 'after_weboptimize_score_mobile') ?: '';
                        $after_weboptimize_score_desktop = get_term_meta_value($term_id, 'after_weboptimize_score_desktop') ?: '' ;
                        $after_weboptimize_score_usability = get_term_meta_value($term_id, 'after_weboptimize_score_usability') ?: '' ;
                        $after_weboptimize_size_web = get_term_meta_value($term_id, 'after_weboptimize_size_web') ?: '' ;
                        $after_weboptimize_response_time = get_term_meta_value($term_id, 'after_weboptimize_response_time') ?: '' ;

                        $this->table->set_caption('Sau tối ưu' . form_button(['class' => 'btn bg-olive btn-flat margin btn-xs pull-right', 'id' => 'has-check-after-optimize-pagespeed', 'type' => 'button'],'<i class="fa fa-refresh fa-fw" id="pagespeed-loading"></i> Xem điểm'))

                                    // Mobile
                        // input($label = '',$name = '',  $value = '', $help = '', $attrs = array())
                                    ->add_row('Điểm mobile', $after_weboptimize_score_mobile)  

                                    // Desktop
                                    ->add_row('Điểm Desktop', $after_weboptimize_score_desktop)
                                    
                                    // Thân thiện
                                    ->add_row('Điểm thân thiện', $after_weboptimize_score_usability)  
                                    // Dung lượng page
                                    ->add_row('Dung lượng trang', $after_weboptimize_size_web) 

                                    // Thời gian phản hồi
                                    ->add_row(array('Thời gian phản hồi',
                                        $this->admin_form->input('','',get_term_meta_value($term_id, 'after_weboptimize_response_time') ,'',
                                                array(
                                                    'data-original-title' => 'Thời gian phản hồi',
                                                    'data-pk'   => $term_id,
                                                    'data-type-data'    => 'meta_data',
                                                    'data-type' => 'number',
                                                    'data-min' => 0 ,
                                                    'data-step' => 'any',
                                                    'data-name' => 'after_weboptimize_response_time',
                                                    'data-value'    => $after_weboptimize_response_time,
                                                    'is_x_editable' => 'true', 
                                                    'none_label'    =>  TRUE,
                                                    'override_jscript'=> TRUE,
                                                )
                                        )
                                        // Response time
                                    ))   ;  
                        echo $this->table->generate();        
                    ?>
                </div>
                <div class="col-sm-12">
                    <div class="progress-group" id="msg"></div>
                    <div class="progress-group">
                        <span class="progress-text">Điểm cam kết trên mobile</span>
                        <span class="progress-number"><b><?php echo $score_commitment_mobile_weboptimize ;?></b>/100</span>
                        <div class="progress progress-md active">
                          <div class="progress-bar progress-bar-success progress-bar-striped" style="width:<?php echo $score_commitment_mobile_weboptimize ;?>%"></div>
                        </div>
                    </div>

                    <div class="progress-group">
                        <span class="progress-text">Điểm cam kết trên desktop</span>
                        <span class="progress-number"><b><?php echo $score_commitment_desktop_weboptimize ;?></b>/100</span>
                        <div class="progress progress-md active">
                          <div class="progress-bar progress-bar-success progress-bar-striped" style="width:<?php echo $score_commitment_desktop_weboptimize ;?>%"></div>
                        </div>
                    </div>

                    <div class="progress-group">
                        <?php
                            if(!empty($after_weboptimize_score_mobile) && !empty($after_weboptimize_score_desktop)) 
                            {
                                $mobile = '<a class="btn bg-olive btn-flat pull-left" href="https://developers.google.com/speed/pagespeed/insights/?url='.$website.'&tab=mobile" target="_blank"><i class="fa fw fa-mobile-phone"></i> Mobile đạt</a>';
                                if($after_weboptimize_score_mobile < $score_commitment_mobile_weboptimize)
                                {
                                    $mobile = '<a class="btn bg-maroon btn-flat pull-left" href="https://developers.google.com/speed/pagespeed/insights/?url='.$website.'&tab=mobile" target="_blank"><i class="fa fw fa-mobile-phone"></i> Mobile không đạt</a>';
                                }


                                $desktop = '<a class="btn bg-olive btn-flat pull-right" href="https://developers.google.com/speed/pagespeed/insights/?url='.$website.'&tab=desktop" target="_blank"><i class="fa fw fa-desktop"></i> Desktop đạt</a>';
                                if($after_weboptimize_score_desktop < $score_commitment_desktop_weboptimize)
                                {
                                    $desktop = '<a class="btn bg-maroon btn-flat pull-right" href="https://developers.google.com/speed/pagespeed/insights/?url='.$website.'&tab=desktop" target="_blank"><i class="fa fw fa-desktop"></i> Desktop không đạt</a>';
                                }

                                echo '<p>' . $mobile . $desktop . '</p>';
                            }

                        ?>
                    </div>
                </div>
            </div>            
    </div>
</div> 
<script type="text/javascript">
    var url_ajax_optimize_pagespeed = "<?php echo base_url('weboptimize/ajax/optimize_pagespeed') ?>";
    var website             = '<?php echo $website ;?>' ;

    $('body').css('opacity', 1);

    $( ".myeditable" ).each(function( index ) {
        $(this).editable({
            url : (function(base_url, el) {

                if(el.data("ajax_call_url") != undefined){

                    base_url += el.data("ajax_call_url");
                }
                else base_url += "<?php echo $ajax_segment;?>";
                
                return base_url; 

            })(admin_url, $(this)),
            params: function(params) {
                params.type = $(this).data("type-data");
                return params;
            },
            success: function(data, newValue) {
                if($.isEmptyObject(data.response)) return;
                if($.isEmptyObject(data.response.response)) return;
                if(data.response.response.hasOwnProperty("jscallback")){
                    $.each(data.response.response.jscallback,function(i,e){
                        var fn = window[e.function_to_call];
                        fn(e.data);
                    });
                }
            },
            defaultValue: "",
        });   
    });

    // Xem điểm trước khi tối ưu
    $('#has-check-before-optimize-pagespeed[name="allow-ajax"]').on('click', function(e) {
        $.ajax({
            url      : url_ajax_optimize_pagespeed,
            type     : 'post',
            dataType : 'json' ,
            data     : { website : website, term_id : <?php echo $term_id; ?>} ,            
        }).done(function(response) {
            if(response.status == 0) return false;
            window.location.reload(); 
        });  
    });

    // Xem điểm sau khi tối ưu
    $('#has-check-after-optimize-pagespeed').on('click', function(e) {
        $.ajax({
            //async: true,
            url      : url_ajax_optimize_pagespeed,
            type     : 'post',
            dataType : 'json' ,
            data     : { website : website, term_id : <?php echo $term_id; ?>, type : 'after'} ,
            beforeSend : function() {
                $('#pagespeed-loading').addClass('fa-spin');
            },
        }).done(function(response) {
            $('#pagespeed-loading').removeClass('fa-spin');
            if(response.status == 0) { 
                var msg = response.msg;
                $('#msg').html('<div class="callout callout-danger"><p>' + msg + '</p></div>');
                return false;
            }
            window.location.reload(); 
          }); 
    });
</script>

    


   