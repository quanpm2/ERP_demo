<?php 
   $this->load->view('header');
   $start_time = get_term_meta_value($term_id,'contract_begin');
   $end_time   = get_term_meta_value($term_id,'contract_end');

   $technical_name      = 'Chưa có' ;
   
   if( ! empty($techs) ) 
   {
      $technical_name                 = $techs['name'];
      $technical_email                = $techs['email'];
      $technical_phone                = $techs['phone'] ;    
   } 
?>

<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td height="15"></td>
   </tr>
   <tr>
      <td align="center">
         <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
               <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                  <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                     <tr>
                        <!--Title-->
                        <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Kích hoạt dịch vụ</td>
                        <!--End title-->
                        <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                     </tr>
                  </table>
                  <!--Space-->
                  <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                     <tr>
                        <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                           <p style="padding-left: 24px;">&nbsp;</p>
                        </td>
                     </tr>
                  </table>
                  <!--End Space-->
                  <!--detail-->
                  <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                     <tr>
                        <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Thông báo kích hoạt dịch vụ</td>
                     </tr>
                  </table>
                  <!--end detail-->
               </td>
            </tr>
            <tr>
               <td bgcolor="#ffffff">
                  <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                     <tbody>
                        <tr>
                           <td height="25"></td>
                        </tr>
                        <!--Content-->
                        <tr>
                           <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000; line-height:28px;">
                              <b>Kính chào Quý khách !</b> WEBDOCTOR.VN cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.
                              <p>
                                 WEBDOCTOR.VN gửi email thông báo kích hoạt & thực hiện Hợp đồng tối ưu website cho <?php echo anchor(prep_url($term->term_name));?>
                              </p>
                           </td>
                        </tr>

                         <tr>
                                 <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000; line-height:28px;">
                                    <p style="font-family: open sans, arial, sans-serif; font-size:15px">Mọi thắc mắc vui lòng liên hệ với chúng tôi theo thông tin dưới đây:</p>
                                    <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <?php if(!empty($staff)) { ?>
                                             <tr>
                                                <!--<td>Kinh doanh phụ trách</td>-->
                                                <td>
                                                      <tr>
                                                         <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Kinh doanh <?= $staff->display_name?>: </td>
                                                         <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php
                                                            $phone = $this->usermeta_m->get_meta_value($staff->user_id,'user_phone');
                                                            if(!$phone) $phone = '(028) 7300. 4488';
                                                            
                                                            echo $phone; ?></td>
                                                      </tr>
                                                      <tr>
                                                         <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                         <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?= $staff->user_email ?></td>
                                                      </tr>
                                                </td>
                                             </tr>
                                          <?php } ?>

                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Hotline Trung tâm Kinh doanh:</td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">(028) 7300. 4488</td>
                                          </tr>
                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">sales@webdoctor.vn</td>
                                          </tr>
                                          <!-- KỸ THUẬT THỰC HIỆN -->
                                          <?php if(!empty($techs)) { ?>
                                             <tr>
                                                <td>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Kỹ thuật <?php echo $technical_name ?>: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $technical_phone ; ?></td>
                                             </tr>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $technical_email ?></td>
                                             </tr>
                                             </td>
                                             </tr>
                                          <?php } ?>
                                          <tr>
                                             <td height="25"></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                        <!--End Content-->
                        <tr>
                           <td align="left" height="30"></td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </table>
      </td>
   </tr>
   <tr>
      <td height="15"></td>
   </tr>
</table>
</tr>
</tbody></table>
<!-- thong tin hop dong -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin hợp đồng</td>
                                 <!--End title-->
                                 <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tbody>
                              <tr>
                                 <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--end detail-->
                     </td>
                  </tr>
                  <!--start Article-->
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <?php
                                 $service_date              = my_date($start_time,'d/m/Y').' - '.my_date($end_time,'d/m/Y');
                                 $contract_date              = my_date(get_term_meta_value($term_id,'contract_begin'),'d/m/Y').' - '.my_date(get_term_meta_value($term_id,'contract_end'),'d/m/Y');
                                 
                                 $contract_price_weboptimize =  get_term_meta_value($term_id,'contract_price_weboptimize') ?: 0 ;
                                 
                                 $rows = array();
                                 $rows[] = array('Mã hợp đồng', get_term_meta_value($term_id,'contract_code'));
                                 $rows[] = array('Gói dịch vụ', 'TỐI ƯU WEBSITE');
                                 $rows[] = array('Phí tối ưu website', number_format($contract_price_weboptimize) . ' VNĐ');
                                 $rows[] = array('Thời gian hợp đồng', $contract_date);

                                 if( !empty($staff_name) ) { 
                                      $rows[] = array('Kinh doanh phụ trách', $staff_name);
                                 }

                                 if( !empty($technical_staffs_username) ) { 
                                      $rows[] = array('Kỹ thuật phụ trách', $technical_staffs_username);
                                 }
                                 
                                 foreach($rows as $i=>$row):
                                    ?>
                              <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                 </td>
                              </tr>
                              <?php endforeach; ?>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <!--end Article-->
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- tong tin hop dong -->
<?php $this->load->view('footer'); ?>