<?php 
   $this->load->view('header');
   $contract_begin = get_term_meta_value($term_id,'contract_begin');
   $contract_end   = get_term_meta_value($term_id,'contract_end');
   $contract_date  = my_date($contract_begin,'d/m/Y').' - '.my_date($contract_end,'d/m/Y');

   $technical_name      = 'Chưa có' ;
   
   if( ! empty($techs) ) 
   {
      $technical_name                 = $techs['name'];
      $technical_email                = $techs['email'];
      $technical_phone                = $techs['phone'] ;    
   } 

?>


<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td height="15"></td>
   </tr>
   <tr>
      <td align="center">
         <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
               <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                  <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                     <tr>
                        <!--Title-->
                        <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin tối ưu</td>
                        <!--End title-->
                        <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                     </tr>
                  </table>
                  <!--Space-->
                  <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                     <tr>
                        <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                           <p style="padding-left: 24px;">&nbsp;</p>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr>
               <td bgcolor="#ffffff">
                  <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                     <tbody>
                        <tr>
                           <td height="25"></td>
                        </tr>
                        <!--Content-->
                        <tr>
                           <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999; line-height:28px;">
                              <b>Kính chào Quý khách !</b> WEBDOCTOR.VN cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.
                              <p>
                                 <b>WEBDOCTOR.VN</b> thông báo email nghiệm thu Hợp đồng tối ưu Website cho <?php echo anchor(prep_url($term->term_name));?>
                              </p>
                              <p style="text-transform: uppercase;"><b>THÔNG TIN CHI TIẾT HỢP ĐỒNG TỐI ƯU WEBSITE: <?php echo anchor(prep_url($term->term_name));?></b></p>
                           </td>
                        </tr>
                        <!--End Content-->
                     </tbody>
                  </table>
               </td>
            </tr>
            <tr>
               <td bgcolor="#ffffff">
                  <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                     <tbody>
                        <!--start Article-->
                        <tr>
                           <td bgcolor="#ffffff">
                              <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                 <tbody>
                                    <?php
                                       $rows = array();

                                       $contract_price_weboptimize          = get_term_meta_value($term_id,'contract_price_weboptimize') ?: 0 ;

                                       $score_commitment_mobile_weboptimize = get_term_meta_value($term_id,'score_commitment_mobile_weboptimize') ?: $this->config->item('score_commitment_mobile_weboptimize', 'pagespeed') ;

                                       $score_commitment_desktop_weboptimize = get_term_meta_value($term_id,'score_commitment_desktop_weboptimize') ?: $this->config->item('score_commitment_desktop_weboptimize', 'pagespeed') ;

                                       $rows[] = array('Mã hợp đồng', get_term_meta_value($term_id,'contract_code'));
                                       $rows[] = array('Thời gian hợp đồng', $contract_date);
                                       $rows[] = array('Phí tối ưu', currency_numberformat($contract_price_weboptimize));
                                       $rows[] = array('Điểm cam kết trên mobile', $score_commitment_mobile_weboptimize . '/100');
                                       $rows[] = array('Điểm cam kết trên desktop', $score_commitment_desktop_weboptimize . '/100');
                                                                                                          
                                       foreach($rows as $i=>$row):
                                          ?>
                                             <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                                             <tr>
                                                <td width="250" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                                </td>
                                             </tr>
                                       <?php endforeach; ?>

                                    <tr>
                                       <td height="25"></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <!--end Article-->
                     </tbody>
                  </table>
               </td>
            </tr>

            <tr>
               <td bgcolor="#ffffff">
                  <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                     <tbody>
                        <!--start Article-->
                        <tr>
                           <td bgcolor="#ffffff">
                              <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000; line-height:28px;">
                                    <p style="font-family: open sans, arial, sans-serif; font-size:15px">Mọi thắc mắc vui lòng liên hệ với chúng tôi theo thông tin dưới đây:</p>
                                    <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <?php if(!empty($staff)) { ?>
                                             <tr>
                                                <!--<td>Kinh doanh phụ trách</td>-->
                                                <td>
                                                      <tr>
                                                         <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Kinh doanh <?= $staff->display_name?>: </td>
                                                         <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php
                                                            $phone = $this->usermeta_m->get_meta_value($staff->user_id,'user_phone');
                                                            if(!$phone) $phone = '(028) 7300. 4488';
                                                            
                                                            echo $phone; ?></td>
                                                      </tr>
                                                      <tr>
                                                         <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                         <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?= $staff->user_email ?></td>
                                                      </tr>
                                                </td>
                                             </tr>
                                          <?php } ?>

                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Hotline Trung tâm Kinh doanh:</td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">(028) 7300. 4488</td>
                                          </tr>
                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">sales@webdoctor.vn</td>
                                          </tr>
                                          <!-- KỸ THUẬT THỰC HIỆN -->
                                          <?php if(!empty($techs)) { ?>
                                             <tr>
                                                <td>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Kỹ thuật <?php echo $technical_name ?>: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $technical_phone ; ?></td>
                                             </tr>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $technical_email ?></td>
                                             </tr>
                                             </td>
                                             </tr>
                                          <?php } ?>
                                          <tr>
                                             <td height="25"></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <!--End Content-->
                              <tr>
                                 <td align="left" height="30"></td>
                              </tr>
                           </tbody>
                        </table>
                           </td>
                        </tr>
                        <!--end Article-->
                     </tbody>
                  </table>
               </td>
            </tr>

         </table>
      </td>
   </tr>
   <tr>
      <td height="15"></td>
   </tr>
</table>
</tr>
</tbody></table>

<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td height="15"></td>
   </tr>
   <tr>
      <td align="center">
         <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
               <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                  <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                     <tr>
                        <!--Title-->
                        <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Trước khi tối ưu</td>
                        <!--End title-->
                        <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                     </tr>
                  </table>
                  <!--Space-->
                  <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                     <tr>
                        <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                           <p style="padding-left: 24px;">&nbsp;</p>
                        </td>
                     </tr>
                  </table>
                  <!--End Space-->
                  <!--detail--
                  <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                     <tr>
                        <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Thông báo trước khi tối ưu</td>
                     </tr>
                  </table>
                  <!--end detail-->
               </td>
            </tr>
            <tr>
               <td bgcolor="#ffffff">
                  <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                     <tbody>
                        <!--start Article-->
                        <tr>
                           <td bgcolor="#ffffff">
                              <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                 <tbody>
                                    <tr>
                                       <td height="25"></td>
                                    </tr>
                                    <?php
                                       $rows = array();
                                       $rows[] = array('Điểm mobile', get_term_meta_value($term_id,'before_weboptimize_score_mobile') . '/100' ?: '--');
                                       $rows[] = array('Điểm desktop', get_term_meta_value($term_id,'before_weboptimize_score_desktop'). '/100' ?: '--');
                                       $rows[] = array('Điểm thân thiện', get_term_meta_value($term_id,'before_weboptimize_score_usability'). '/100' ?: '--');
                                       $rows[] = array('Dung lượng trang', get_term_meta_value($term_id,'before_weboptimize_size_web') . ' MB'?: '--');
                                                                                                          
                                       foreach($rows as $i=>$row):
                                          ?>
                                    <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                                       <tr>
                                          <td width="250" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                          <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                          </td>
                                       </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                       <td height="25"></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <!--end Article-->
                     </tbody>
                  </table>
               </td>
            </tr>
         </table>
      </td>
   </tr>
   <tr>
      <td height="15"></td>
   </tr>
</table>
</tr>
</tbody></table>

<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Sau khi tối ưu</td>
                                 <!--End title-->
                                 <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tbody>
                              <tr>
                                 <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <!--End Space-->
                        <!--detail--
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--end detail-->
                     </td>
                  </tr>
                  <!--start Article-->
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <?php
                                 $service_date              = my_date($contract_begin,'d/m/Y').' - '.my_date($contract_end,'d/m/Y');
                                 $contract_date              = my_date(get_term_meta_value($term_id,'contract_begin'),'d/m/Y').' - '.my_date(get_term_meta_value($term_id,'contract_end'),'d/m/Y');
                                 
                                 $contract_price_weboptimize =  get_term_meta_value($term_id,'contract_price_weboptimize') ?: 0 ;
                                 
                                 $rows = array();
                                 $rows[] = array('Điểm mobile', get_term_meta_value($term_id,'after_weboptimize_score_mobile') . '/100' ?: '--');
                                       $rows[] = array('Điểm desktop', get_term_meta_value($term_id,'after_weboptimize_score_desktop'). '/100' ?: '--');
                                       $rows[] = array('Điểm thân thiện', get_term_meta_value($term_id,'after_weboptimize_score_usability'). '/100' ?: '--');
                                       $rows[] = array('Dung lượng trang', get_term_meta_value($term_id,'after_weboptimize_size_web') . ' MB' ?: '--');
                                 
                                 foreach($rows as $i=>$row):
                                    ?>
                              <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                              <tr>
                                 <td width="250" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                 </td>
                              </tr>
                              <?php endforeach; ?>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              
                              <?php if(!empty($tasks)) :?>
                                 <tr>
                                    <td colspan="2"><h3 style="text-transform: uppercase; font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;">Các công việc thực hiện tối ưu</h3></td>
                                 </tr>
                                 <tr>
                                    <?php $content_style = 'font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;';?>
                                                <?php foreach($tasks as $i=> $task): ?>
                                                   <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                                                   <tr height="25">
                                                      <td align="left" title="<?php echo $task->post_title;?>" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;">
                                                         <?php echo $task->post_title;?>
                                                      </td>
                                                      <td style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;">
                                                         <?php echo $task->post_content;?> 
                                                      </td>
                                                   </tr>
                                                <?php endforeach; ?>
                                 </tr>
                              <?php endif ;?>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <!--end Article-->
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>

<?php $this->load->view('footer'); ?>