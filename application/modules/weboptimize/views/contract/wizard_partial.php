<?php $this->config->load('weboptimize/weboptimize'); ?>
<div class="col-md-12" id="service_tab">
	<?php 
		$contract_price_weboptimize 				= get_term_meta_value($edit->term_id, 'contract_price_weboptimize') ?: '' ;
		$score_commitment_mobile_weboptimize        = get_term_meta_value($edit->term_id, 'score_commitment_mobile_weboptimize') ?: $this->config->item('score_commitment_mobile_weboptimize', 'pagespeed') ;
		$score_commitment_desktop_weboptimize       = get_term_meta_value($edit->term_id, 'score_commitment_desktop_weboptimize') ?: $this->config->item('score_commitment_desktop_weboptimize', 'pagespeed') ;

		echo $this->admin_form->form_open(), form_hidden('edit[term_id]', $edit->term_id), $this->admin_form->submit('', 'confirm_step_service','confirm_step_service','',array('style'=>'display:none;','id'=>'confirm_step_service')) ;

		echo '<div id="load-package-info">' ;
			$this->table->add_row('Đơn giá tối ưu Website', form_input(['type'=>'number', 'name'=>'edit[meta][contract_price_weboptimize]','class'=>'form-control', 'id' => 'contract_price_weboptimize', 'placeholder' => 'Đơn giá tối ưu Website'],$contract_price_weboptimize))
						->add_row('Điểm cam kết tối ưu trên mobile', form_input(['type'=>'number', 'name'=>'edit[meta][score_commitment_mobile_weboptimize]','class'=>'form-control', 'id' => 'score_commitment_mobile_weboptimize', 'placeholder' => 'Điểm cam kết trên mobile', 'min' => 1, 'max' => 100],$score_commitment_mobile_weboptimize))
						->add_row('Điểm cam kết tối ưu trên desktop', form_input(['type'=>'number', 'name'=>'edit[meta][score_commitment_desktop_weboptimize]','class'=>'form-control', 'id' => 'score_commitment_desktop_weboptimize', 'placeholder' => 'Điểm cam kết trên mobile', 'min' => 1, 'max' => 100],$score_commitment_desktop_weboptimize)) ;
			echo $this->table->generate(); 
		echo '</div>' ;

		echo $this->admin_form->form_close();
	?>
</div>

<script type="text/javascript">
	$(function(){
		$('.set-datepicker').datepicker({
			format: 'yyyy/mm/dd',
			todayHighlight: true,
			autoclose: true,	
		});
	});
	$(document).ready(function() {
		var load_package_info = $("#load-package-info").closest('form');
		load_package_info.validate({  
			rules: {
			  'edit[meta][contract_price_weboptimize]': {
			    required: true,
			  },
			  'edit[meta][score_commitment_mobile_weboptimize]': {
			    required: true,
			  },
			  'edit[meta][score_commitment_desktop_weboptimize]': {
			    required: true,
			  },			  			  		  
			},
			messages: {
			  'edit[meta][contract_price_weboptimize]': {
			    required	: 'Phí tối ưu website không được để trống',
			    digits		: 'Kiểu dữ liệu không hợp lệ, Phí tối ưu website phải là kiểu số',
			  },
			  'edit[meta][score_commitment_mobile_weboptimize]': {
			    required	: 'Điểm cam kết tối ưu trên mobile không được để trống',
			    digits		: 'Kiểu dữ liệu không hợp lệ, Điểm cam kết tối ưu trên mobile phải là kiểu số',
			  }	,
			  'edit[meta][score_commitment_desktop_weboptimize]': {
			    required	: 'Điểm cam kết tối ưu trên desktop không được để trống',
			    digits		: 'Kiểu dữ liệu không hợp lệ, Điểm cam kết tối ưu trên desktop phải là kiểu số',
			  }			  		
			}
		});
	}) ;
</script>