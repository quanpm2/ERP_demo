<?php 
$this->load->config('weboptimize/weboptimize');
$this->config->item('packages');
$services = $this->config->item('service','packages');
?> 
<style type="text/css">
    #facebook-ads thead tr{
        text-align: center;
    }
    .row-containter {
        text-align: justify; 
    }
</style>
<div class="content">
    <!-- Điều 2 -->
    <div class="row-containter">

        <br/>
        <br/>
        <?php
        $contract_begin        = get_term_meta_value($term_id, 'contract_begin') ;
        $contract_begin        = $this->mdate->startOfDay($contract_begin) ;

        $contract_end          = get_term_meta_value($term_id, 'contract_end') ;
        $contract_end          = $this->mdate->startOfDay($contract_end) ;

        $contract_price_weboptimize   = get_term_meta_value($term_id, 'contract_price_weboptimize') ;
        $contract_price_weboptimize       = ($contract_price_weboptimize > 0) ? currency_numberformat($contract_price_weboptimize) : 0;

        $total_days_contract   = ($contract_end - $contract_begin)/(24*60*60) ;
        $text_contract_period  = $total_days_contract . ' ngày (từ ' . date('d/m/Y', $contract_begin) . ' đến ' . date('d/m/Y', $contract_end) . ')';
        $this->table->clear();
        $this->table->set_template( ['table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%" id="facebook-ads">'] );
        $this->table->set_heading( array('Gói dịch vụ', 'Thời gian', 'Đơn giá', 'Thành tiền'))
        ->add_row('Tối ưu webiste (*)', $text_contract_period, array('data' => $contract_price_weboptimize, 'align' => 'center') , array('data' => $contract_price_weboptimize, 'align' => 'center'));

        $weboptimize_discount_percent             = get_term_meta_value($term_id, 'weboptimize_discount_percent') ;
        if( !empty($weboptimize_discount_percent) || $weboptimize_discount_percent > 0 ) 
        { 
            $total_contract_price_weboptimize              = get_term_meta_value($term_id, 'contract_price_weboptimize') ;
            $total_service_fee                  = (! get_term_meta_value($term_id, 'service_fee') ) ? 0 : get_term_meta_value($term_id, 'service_fee') ;
            $total_weboptimize_discount_percent = (($total_contract_price_weboptimize + $total_service_fee) *  $weboptimize_discount_percent)/100 ; 
            $this->table->add_row(array('data' => 'Giảm giá ' . $weboptimize_discount_percent . '%', 'colspan' => 3) , array('data' => currency_numberformat($total_weboptimize_discount_percent) , 'align' => 'center')) ;
        }

        $this->table->add_row(array('data' => 'Tổng cộng', 'colspan' => 3) , array('data' => currency_numberformat($contract_value) , 'align' => 'center')) ;

        $total   = (int)$contract_value;

        if(!empty($vat))
        {
            $tax   = $contract_value * ($vat/100);
            $total = cal_vat($contract_value,$vat);

            $this->table->add_row( array('data'=>'VAT ' . '(' . $vat . ' %)' ,'colspan' => 3),
                array('data'=> '<strong>'.$vat.'&#37; ('.currency_numberformat($tax).') </strong>', 'align' => 'center' )
                )         ;      
        }

        $text_vat = ' chưa bao gồm VAT' ;
        if(!empty($vat)) $text_vat = ' đã bao gồm VAT' ;
        $this->table->add_row(array('data'=>'Tổng thanh toán' . $text_vat ,'colspan'=> 3), 
            array('data'=>currency_numberformat($total),'align'=>'center')
            );           

        echo $this->table->generate();              
        ?>
        <p><em>Bằng chữ: <b><i><?php echo ucfirst(mb_strtolower(convert_number_to_words($total)));?> đồng.</i></b></em><br/>
        </div>
        <!-- Điều 4 -->
        <div class="row-containter">
            <br/>      
            <p>- Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin tài khoản do Bên B cung cấp, cụ thể:</p>

            <?php if( ! empty($bank_info)) :?>
            <ul style="padding-left:0;list-style:none">
            <?php foreach ($bank_info as $label => $text) :?>
                <?php if (is_array($text)) : ?>
                    <?php foreach ($text as $key => $value) :?>
                        <li>- <?php echo $key;?>: <?php echo $value;?></li>
                    <?php endforeach;?>
                <?php continue; endif;?>
                <li>- <?php echo $label;?>: <?php echo $text;?></li>
            <?php endforeach;?>
            </ul>
            <p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
            <?php endif; ?>
            
            <p>- Lộ trình thanh toán:  </p>
            <?php

            $args = array(
                'select' => 'posts.post_id, post_title,post_status,start_date,post_content,end_date',
                'tax_query' => array( 'terms'=> $term->term_id),
                'numberposts' =>0,
                'orderby' => 'posts.end_date',
                'order' => 'ASC',
                'post_type' => 'contract-invoices');
            $lists = $this->invoice_m->get_posts($args);
            $is_one_payments = empty($lists) || (!empty($lists) && count($lists) == 1);
            if($is_one_payments)
            {
                echo '<p>Ngay sau khi hợp đồng đươc ký BÊN A thanh toán cho BÊN B 100% chi phí tối ưu website</p>';
            }
            else
            {
                $number_of_payments = count($lists);
                $i=1;
                echo '<p>- Bên A thanh toán cho Bên B làm '.$number_of_payments.' đợt:</p>';
                echo '<ul style="list-style-type: square;">';
                foreach ($lists as $inv) { 
                    $price_total = $this->invoice_item_m->get_total($inv->post_id, 'total');
                    $price_total = cal_vat($price_total, $vat);
                    $price_total = numberformat($price_total,0,'.','');

                    printf('<li> Đợt %s: thanh toán %s (%s), chậm nhất là ngày %s.</li>',
                        $i,
                        currency_numberformat($price_total,'đ'),
                        currency_number_to_words($price_total), 
                        my_date($inv->end_date,'d/m/Y'));
                    $i++;
                }
                echo '</ul>';
            }
            ?>
        </div>

        <div class="row-containter">
            <p><b>CHÚNG TÔI CAM KẾT</b></p>
            <p>- Nhân viên công ty sẽ thực hiện dịch vụ đã cam kết với tinh thần và phong cách chuyên nghiệp nhất. Hỗ trợ và giải đáp các thắc mắc của khách hàng rõ ràng, đầy đủ nhất.</p>
            <p>- Bảo mật các thông tin khách hàng đã cung cấp.</p>
            <p>- Phối hợp trên tinh thần tôn trọng, tương trợ, tin tưởng hướng đến sự phát triển chung, bền vững, đôi bên cùng có lợi.</p> 
            <p>- Không can thiệp vào các lĩnh vực hoạt động, kinh doanh của nhau khi chưa có đề nghị chính thức bằng văn bản có xác nhận của người đại diện mỗi bên.</p>
            <p>- Lấy đối thoại làm căn cứ để giải quyết các vấn đề chưa thông tỏ, không dùng vai trò của bất cứ thành viên thứ ba nào can thiệp.</p>
        </div>
        <br/>
        <br/>
    </div>