<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_note_contract extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('message_m') ;
		$this->load->model('note_contract_m') ;
 	}

	public function load() {
		$response                        = array() ;
		$term_id   					     = $this->input->post('term_id'); 

		if(empty($term_id)) return false ;

		$response['data']     		 	 = $this->note_contract_m->load($term_id) ;

		if(empty($response['data'])) return FALSE ;
		$response['status']              = true;
		$response['alert']               = 'Load dữ liệu thành công' ;

		$this->render_template_message($response) ;
	}

	public function view($msg_id) {
		$response['data']         = $this->note_contract_m->load($msg_id, 'msg_id') ;

		if(empty($view_as_msg_id['data'])) return FALSE ;

		$response['alert']        = 'Load dữ liệu thành công' ;
		$response['status']       = TRUE ;
		$this->render_template_message($response) ;
	}

	public function count() { 
		$term_id                = $this->input->post('term_id') ;

		if(empty($term_id)) return false ;

		$response     			= array() ;
		$response['count_msg']  = count($this->note_contract_m->count_contract($term_id));

		return $this->renderJson($response, 200) ;
	}

	public function render_template_message($option, $json = true) {

		$data['direct_chat_msg']             = $option['data'] ;
		$response  					 		 = array() ;
		$response['data']  		 			 = $this->load->view('note-contract/direct-chat-msg', $data, TRUE) ;
		$response['status']                  = isset($option['status']) ?  $option['status'] : false;
		$response['alert']                   = isset($option['alert']) ?  $option['alert']   : 'Thất bại!';

		if($json == true) return $this->renderJson($response) ;
		else return $response ;
	}

	public function send() {
		$post  				= $this->input->post() ;
		$response           = array() ;

		if(empty($post)) return FALSE ;

		$action              = $post['action'] ;

		$response = array();
		$response['status'] = FALSE ;
		$response['alert']  = 'Xử lý không thành công' ;

		$post['user_id'] 	 = $this->admin_m->id ;

		$post['msg_type']    = $this->note_contract_m->msg_type ;
		$post['msg_status']  = $this->note_contract_m->msg_status ;

		switch ($action) {
			case 'delete':

				if(!has_permission('Note_contract.index.delete'))
				{
					$response['alert']  = 'Bạn không được quyền truy cập chức năng này!';
					$response['status'] = FALSE;
					return $this->renderJson($response, 200);
				}

				$post['msg_status']  = 'delete' ;
				$msg_id  = $post['msg_id'] ;

				unset($post['action']) ;
				unset($post['msg_content']) ;
				unset($post['msg_id']) ;

				$result  = $this->note_contract_m->update($msg_id, $post)  ;

				if($result > 0) {
					$response['status'] = true ;
					$response['alert']  = 'Xoá thành công' ;
					$response['msg_id'] = $msg_id ;
					return $this->renderJson($response) ;
				}
				break;
					
			case 'update':

				if($post['msg_content'] == '') return FALSE ; 

				if(!has_permission('Note_contract.index.update'))
				{
					$response['alert']  = 'Bạn không được quyền truy cập chức năng này!';
					$response['status'] = FALSE;
					return $this->renderJson($response, 200);
				}

				# code...
				$post['msg_status']  = 'update' ;

				$msg_id  = $post['msg_id'] ;

				unset($post['action']) ;
				unset($post['msg_id']) ;
				
				$result  = $this->note_contract_m->update($msg_id, $post)  ;
				if($result > 0) {

					$response['status'] 		 = true ;
					$response['alert']  		 = 'Cập nhật thành công' ; 
					$after['data']               = $this->note_contract_m->load($msg_id, 'msg_id', 'update');
					$response['after']   		 = $this->render_template_message($after, false) ;
					$response['msg_id']          = $msg_id ;
					return $this->renderJson($response) ;
				}
				break;
									
			case 'add' :

				if($post['msg_content'] == '') return false ;  

				if($post['term_id'] == 0) return false ;

				if(!isset($post['term_id'])) return false ;

				$response['status'] = false;

				if(!has_permission('Note_contract.index.add'))
				{
					$this->messages->error('Bạn không được quyền truy cập chức năng này!') ;
					$response['alert']  = 'Bạn không được quyền truy cập chức năng này!';
					$response['status'] = false;
					return $this->renderJson($response, 200);
				}

				unset($post['action']) ;
				unset($post['msg_id']) ;

				$msg_id    = $this->note_contract_m->insert($post);
				if($msg_id > 0) {
					$response['status']     = true ;
					$response['alert']      = 'Thêm ghi chú thành công' ;
					$response['data']  	    = $this->note_contract_m->load($msg_id, 'msg_id') ;
					$this->render_template_message($response) ;
				}

				break;
		}
	}
	
}
/* End of file Ajax_note_contract.php */
/* Location: ./application/modules/message/controllers/Ajax_note_contract.php */