<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('message_m') ;
		$this->load->model('note_contract_m') ;
 	}

 	public function dataset()
 	{
 		$response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

		$defaults	= array('term_id' => 0,'msg_type'=>'note');
		$args 	= wp_parse_args( $this->input->get(), $defaults);

		if( ! empty($args['term_id'])) $this->message_m->where('term_id',$args['term_id']);

		$messages = $this->message_m->where('msg_type',$args['msg_type'])->order_by('created_on')->get_all();
		if( ! $messages)
		{
			$response['msg'] = 'Không tìm thấy dữ liệu.';
			return parent::renderJson($response);
		}

		$messages = array_map(function($x){
			$x->created_date = my_date($x->created_on, 'd/m/Y H:i:s');
			$x->user = array(
				'user_avatar' => $this->admin_m->get_field_by_id($x->user_id,'user_avatar'),
				'display_name' => $this->admin_m->get_field_by_id($x->user_id,'display_name'),
			);
			return $x;
		}, $messages);

		$response['msg'] 	= 'Dữ liệu đã được tải thành công.';
		$response['data'] 	= $messages;
		$response['status'] = TRUE;

    	return parent::renderJson($response);
 	}

 	public function add()
 	{
 		$response = array('status' => FALSE, 'msg' => 'Xử lý không thành công', 'data' => []);

		$defaults	= array('term_id' => 0,'msg_content' => '');
		$args 		= wp_parse_args( $this->input->get(), $defaults);

		if( ! has_permission('Note_contract.index.add'))
		{
			$response['msg'] = 'Không thể thực hiện tác vụ do quyền truy cập bị hạn chế.';
			return parent::renderJson($response);
		}

		if(empty($args['term_id']) || empty($args['msg_content']))
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ.';
			return parent::renderJson($response);
		}

		$insert_data = array(
			'created_on' => time(),
			'term_id' => $args['term_id'],
			'user_id' => $this->admin_m->id,
			'msg_content' => $args['msg_content'],
			'msg_type' => 'note',
			'msg_status' => 'sent',
		);

		$insert_id = $this->message_m->insert($insert_data);
		if( ! $insert_id)
		{
			$response['msg'] = 'Quá trình xử lý không thành công.';
			return parent::renderJson($response);
		}


		$message = (object) $insert_data;
		$message->msg_id = $insert_id;
		$message->created_date = my_date($message->created_on, 'd/m/Y H:i:s');
		$message->user = array(
			'user_avatar' => $this->admin_m->get_field_by_id($this->admin_m->id,'user_avatar'),
			'display_name' => $this->admin_m->get_field_by_id($this->admin_m->id,'display_name'),
		);

		$response['status'] = TRUE;
		$response['msg'] 	= 'Cập nhật thành công';
		$response['data']	= $message;

		return parent::renderJson($response);
	}

	public function load() {
		$response                        = array() ;
		$term_id   					     = $this->input->post('term_id'); 

		if(empty($term_id)) return false ;

		$response['data']     		 	 = $this->note_contract_m->load($term_id) ;

		if(empty($response['data'])) return FALSE ;
		$response['status']              = true;
		$response['alert']               = 'Load dữ liệu thành công' ;

		$this->render_template_message($response) ;
	}

	public function view($msg_id) {
		$response['data']         = $this->note_contract_m->load($msg_id, 'msg_id') ;

		if(empty($view_as_msg_id['data'])) return FALSE ;

		$response['alert']        = 'Load dữ liệu thành công' ;
		$response['status']       = TRUE ;
		$this->render_template_message($response) ;
	}

	public function count() { 
		$term_id                = $this->input->post('term_id') ;

		if(empty($term_id)) return false ;

		$response     			= array() ;
		$response['count_msg']  = count($this->note_contract_m->count_contract($term_id));

		return $this->renderJson($response, 200) ;
	}

	public function render_template_message($option, $json = true) {

		$data['direct_chat_msg']             = $option['data'] ;
		$response  					 		 = array() ;
		$response['data']  		 			 = $this->load->view('note-contract/direct-chat-msg', $data, TRUE) ;
		$response['status']                  = isset($option['status']) ?  $option['status'] : false;
		$response['alert']                   = isset($option['alert']) ?  $option['alert']   : 'Thất bại!';

		if($json == true) return $this->renderJson($response) ;
		else return $response ;
	}

	public function send() {
		$post  				= $this->input->post() ;
		$response           = array() ;

		if(empty($post)) return FALSE ;

		$action              = $post['action'] ;

		$response = array();
		$response['status'] = FALSE ;
		$response['alert']  = 'Xử lý không thành công' ;

		$post['user_id'] 	 = $this->admin_m->id ;

		$post['msg_type']    = $this->note_contract_m->msg_type ;
		$post['msg_status']  = $this->note_contract_m->msg_status ;

		switch ($action) {
			case 'delete':

				if(!has_permission('Note_contract.index.delete'))
				{
					$response['alert']  = 'Bạn không được quyền truy cập chức năng này!';
					$response['status'] = FALSE;
					return $this->renderJson($response, 200);
				}

				$post['msg_status']  = 'delete' ;
				$msg_id  = $post['msg_id'] ;

				unset($post['action']) ;
				unset($post['msg_content']) ;
				unset($post['msg_id']) ;

				$result  = $this->note_contract_m->update($msg_id, $post)  ;

				if($result > 0) {
					$response['status'] = true ;
					$response['alert']  = 'Xoá thành công' ;
					$response['msg_id'] = $msg_id ;
					return $this->renderJson($response) ;
				}
				break;
					
			case 'update':

				if($post['msg_content'] == '') return FALSE ; 

				if(!has_permission('Note_contract.index.update'))
				{
					$response['alert']  = 'Bạn không được quyền truy cập chức năng này!';
					$response['status'] = FALSE;
					return $this->renderJson($response, 200);
				}

				# code...
				$post['msg_status']  = 'update' ;

				$msg_id  = $post['msg_id'] ;

				unset($post['action']) ;
				unset($post['msg_id']) ;
				
				$result  = $this->note_contract_m->update($msg_id, $post)  ;
				if($result > 0) {

					$response['status'] 		 = true ;
					$response['alert']  		 = 'Cập nhật thành công' ; 
					$after['data']               = $this->note_contract_m->load($msg_id, 'msg_id', 'update');
					$response['after']   		 = $this->render_template_message($after, false) ;
					$response['msg_id']          = $msg_id ;
					return $this->renderJson($response) ;
				}
				break;
									
			case 'add' :

				if($post['msg_content'] == '') return false ;  

				if($post['term_id'] == 0) return false ;

				if(!isset($post['term_id'])) return false ;

				$response['status'] = false;

				if(!has_permission('Note_contract.index.add'))
				{
					$this->messages->error('Bạn không được quyền truy cập chức năng này!') ;
					$response['alert']  = 'Bạn không được quyền truy cập chức năng này!';
					$response['status'] = false;
					return $this->renderJson($response, 200);
				}

				unset($post['action']) ;
				unset($post['msg_id']) ;

				$msg_id    = $this->note_contract_m->insert($post);
				if($msg_id > 0) {
					$response['status']     = true ;
					$response['alert']      = 'Thêm ghi chú thành công' ;
					$response['data']  	    = $this->note_contract_m->load($msg_id, 'msg_id') ;
					$this->render_template_message($response) ;
				}

				break;
		}
	}
	
}
/* End of file Ajax.php */
/* Location: ./application/modules/message/controllers/Ajax.php */