<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utils extends API_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('message_m');
		$this->load->model('header_m');
	}

	function message_map_webgeneral()
	{

	}

	function auser_map_term($phone = '',$user_id = '')
	{
		/*
		Phone : 
			+ can detect all messages related with this then can get all contract from this
			+ only find term_id related in messages

		user_id : 
			+ can detect all messages related with this then can get all contract from this
			+ detect all term_id related with this customer 

		Need : 
			+ combine term(contract) and put it with list server
		*/

		# check info and contract of customer by user_id
		$services = array();
		$term_ids = array();

		if($user_id)
		{
			$this->load->model('customer/customer_m');
			$customer = $this->customer_m->set_get_customer()->get($user_id);
			
			$this->load->model('term_users_m');
			$term_ids = $this->term_users_m->get_the_terms($user_id);
		}

		$this->load->model('header_m');

		if(!empty($phone))
		{
			$term_meta = $this->termmeta_m
			->select('term_id')
	        ->where('meta_key','contract_curators')
	        ->like('meta_value',$phone)
	        ->get_many_by();

	        if(!empty($term_meta))
	        {
	        	$term_ids = array_unique(array_merge($term_ids, array_column($term_meta, 'term_id')));
	        }
		}

        $terms = $this->term_m
        ->select('term_id,term_name,term_type')
        ->where_in('term_status',array('pending','publish','ending','liquidation'))
        ->where('term_type !=','website')
        ->where_in('term_id',$term_ids)
        ->get_many_by();

        $api_services = array(
        	'adsplus' => 'google-ads',
        	'webgeneral' => 'webgeneral', 
			'webbuild' => 'webdesign', 
			'hosting'=>'hosting',
			'domain'=>'Web Tổng hợp'
        	);

		$services = array_group_by($terms,'term_type');
		foreach ($api_services as $key => $term_type) 
		{
			$api_services[$key] = empty($services[$term_type]) ? 0 : 1;
		}

		prd($api_services);
	}

	function remap_log_webgeneral()
	{
		$this->load->model('term_users_m');
		$logs = $this->log_m->like('log_type','webgeneral-report','after')
		->join('logmeta','logmeta.log_id = log.log_id')
		->get_many_by();
		
		$messages = array();
		foreach ($logs as $log) 
		{
			$headers = array();
			$msg_insert_data = array();
			$term_id = $log->term_id;
			$msg_title = ucfirst(str_replace('Kinh gui quy khach! ', '', $log->log_title));

			$msg_insert_data['term_id'] = $term_id;
			$msg_insert_data['msg_title'] = $msg_title;
			$msg_insert_data['msg_content'] = $log->log_title;
			$msg_insert_data['msg_type'] = 'default';
			$msg_insert_data['msg_status'] = 'sent';
			$msg_insert_data['created_on'] = strtotime($log->log_time_create);

			$meta_value = json_decode($log->meta_value);

			if(empty($meta_value)) continue;

			$_header = array(
				'hed_type' => 'sms',
				'hed_subject' => $msg_title,
				'hed_status' => 0,
				'hed_to' => $meta_value->sms_recipient,
				'user_id' => $term_users[$term_id]??0,
				'term_id' => $term_id,
				'msg_id' => 0
				);

			$headers[] = $_header;

		}
	}

	function remap_log_for_webgeneral()
	{
		$this->load->model('term_users_m');
		$ads_log = $this->log_m
		->like('log_type','webgeneral-report','after')
		->where('log_title !=','')
		// ->where('log_time_create >=',my_date(strtotime('2016/09/01'),'Y-m-d H:i:s'))
		->order_by('log_time_create','desc')
		->join('logmeta','logmeta.log_id = log.log_id')
		->get_many_by();

		$term_ids = array_unique(array_column($ads_log, 'term_id'));

		$term_users = $this->term_users_m->where_in('term_id',$term_ids)->select('term_id,user_id')->get_many_by();
		$term_users = array_combine(array_column($term_users, 'term_id'), array_column($term_users, 'user_id'));

		$messages = array();

		foreach ($ads_log as $log) 
		{
			$headers = array();
			$msg_insert_data = array();
			$term_id = $log->term_id;
			$msg_title = ucfirst(str_replace('Kinh gui quy khach! ', '', $log->log_title));

			$msg_insert_data['term_id'] = $term_id;
			$msg_insert_data['msg_title'] = $msg_title;
			$msg_insert_data['msg_content'] = $log->log_title;
			$msg_insert_data['msg_type'] = 'default';
			$msg_insert_data['msg_status'] = 'sent';
			$msg_insert_data['created_on'] = strtotime($log->log_time_create);

			$meta_value = json_decode($log->meta_value);

			if(empty($meta_value)) continue;

			$_header = array(
				'hed_type' => 'sms',
				'hed_subject' => $msg_title,
				'hed_status' => 0,
				'hed_to' => $meta_value->sms_recipient,
				'user_id' => $term_users[$term_id]??0,
				'term_id' => $term_id,
				'msg_id' => 0
				);

			$headers[] = $_header;

			$messages[] = array(
				'message' => $msg_insert_data,
				'headers' => $headers
			);
		}

		if(empty($messages))
			return FALSE;

		foreach($messages as $row)
		{
			$message = $row['message'];
			$headers = $row['headers'];

			$this->message_m->create($message,$headers);
		}
	}

	function remap_log()
	{
		
		$this->load->model('term_users_m');
		$ads_log = $this->log_m
		->like('log_type','googleads-report-','after')
		->where('log_status',1)
		->where('log_content !=','')
		// ->where('log_time_create >=',my_date(strtotime('2016/10/24'),'Y-m-d H:i:s'))
		->order_by('log_time_create','desc')
		->get_many_by();

		$term_ids = array_unique(array_column($ads_log, 'term_id'));

		$term_users = $this->term_users_m->where_in('term_id',$term_ids)->select('term_id,user_id')->get_many_by();
		$term_users = array_combine(array_column($term_users, 'term_id'), array_column($term_users, 'user_id'));

		$messages = array();

		foreach ($ads_log as $log) 
		{
			$headers = array();
			$msg_insert_data = array();

			$log_content = unserialize($log->log_content);
			$rtype = str_replace('googleads-report-', '', $log->log_type);

			$term_id = $log->term_id;
			$msg_insert_data['term_id'] = $term_id;
			$msg_insert_data['msg_title'] = '';
			$msg_insert_data['msg_content'] = '';
			$msg_insert_data['msg_type'] = 'default';
			$msg_insert_data['msg_status'] = 'sent';
			$msg_insert_data['created_on'] = strtotime($log->log_time_create);

			switch ($rtype) 
			{
				case 'sms':
					$msg_title = 'SMS thông báo dữ liệu click ngày ' . ($log_content['day'] ?? '');
					$msg_content = $msg_insert_data['msg_content'];

					foreach ($log_content as $row) 
					{
						$msg_content = $msg_content ?: $row['message'];

						$_header = array(
							'hed_type' => 'sms',
							'hed_subject' => $msg_title,
							'hed_status' => 0,
							'hed_to' => $row['recipient'],
							'user_id' => $term_users[$term_id]??0,
							'term_id' => $term_id,
							'msg_id' => 0
							);

						$headers[] = $_header;
					}

					$msg_insert_data['msg_content'] = $msg_content;
					$msg_insert_data['msg_title'] = $msg_title;
					break;
					
				case 'email' :

					$msg_title = $log->log_title;

					$direct_downlink = array();
					$attachments = unserialize($log_content['attachment']);

					$i = 0;
					foreach ($attachments as $file) 
						$direct_downlink[] = anchor(base_url($file),' Báo cáo '.(++$i));

			        $term = $this->term_m->get($log->term_id);
					$msg_content = '
					<p>Kính chào Quý khách<br/>
					Adsplus.vn gửi email báo cáo quảng cáo cho website '.strtoupper($term->term_name).'<br/>
					Adsplus.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>';

					if(!empty($direct_downlink))
					{
			        	$direct_downlink = implode(', ', $direct_downlink);
			        	$msg_content.= '<p>'.$direct_downlink.'</p>';
					}

					$recipients = $log_content['recipients'];
					foreach ($recipients as $recipient)
					{
						$_header = array(
							'hed_type' => 'email',
							'hed_subject' => $msg_title,
							'hed_status' => 0,
							'hed_to' => $recipient,
							'user_id' => $term_users[$term_id]??0,
							'term_id' => $term_id,
							'msg_id' => 0
						);

						$headers[] = $_header;
					}

					$msg_insert_data['msg_content'] = $msg_content;
					$msg_insert_data['msg_title'] = $msg_title;
					$msg_insert_data['metadata'] = serialize($this->parse_excel_data('file_name',$term->term_name));
					break;
			}

			$messages[] = array(
				'message' => $msg_insert_data,
				'headers' => $headers
			);

		}

		if(empty($messages))
			return FALSE;

		foreach($messages as $row)
		{
			$message = $row['message'];
			$headers = $row['headers'];

			$this->message_m->create($message,$headers);
		}
	}

	function parse_excel_data($file_name = '',$term_name = '')
	{
		$result = array(
			'sheets' => array(
				'campaign'=> array(
					'headings' => array('Date','Campaign','Impressions','Clicks','Invalid Clicks','CTR','Avg.Position','Campaign Status','Avg.CPC','Cost') ,
					'rows' => array(
						array('18/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('19/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('20/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('21/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('22/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('23/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('24/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('Totals and overall Averages','','1217','77','2','5.58%','3.2','','10.718,09','117.899,00'),
						)
					),
				'adgroup'=> array(
					'headings' => array('Date','Campaign','Impressions','Clicks','Invalid Clicks','CTR','Avg.Position','Campaign Status','Avg.CPC','Cost') ,
					'rows' => array(
						array('18/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('19/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('20/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('21/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('22/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('23/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('24/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('Totals and overall Averages','','1217','77','2','5.58%','3.2','','10.718,09','117.899,00'),
						)
					),
				'keyword'=> array(
					'headings' => array('Date','Campaign','Impressions','Clicks','Invalid Clicks','CTR','Avg.Position','Campaign Status','Avg.CPC','Cost') ,
					'rows' => array(
						array('18/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('19/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('20/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('21/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('22/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('23/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('24/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('Totals and overall Averages','','1217','77','2','5.58%','3.2','','10.718,09','117.899,00'),
						)
					),
				'geo'=> array(
					'headings' => array('Date','Campaign','Impressions','Clicks','Invalid Clicks','CTR','Avg.Position','Campaign Status','Avg.CPC','Cost') ,
					'rows' => array(
						array('18/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('19/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('20/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('21/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('22/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('23/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('24/10/2016',$term_name,'197','11','0','5.58%','3.2','enabled','10.718,09','117.899,00'),
						array('Totals and overall Averages','','1217','77','2','5.58%','3.2','','10.718,09','117.899,00'),
					)
				)
			)
		);
		return $result;
	}
}