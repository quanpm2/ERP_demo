<?php 

	if(!empty($direct_chat_msg)) {
		foreach ($direct_chat_msg as $note) {
			if($note->user_id == $this->admin_m->id) { ?>
		<!-- <?php echo $this->admin_m->get_field_by_id(@$note->user_id,"display_name") ;?> -->
        <div class="direct-chat-msg msg-default" id="msg-<?php echo $note->msg_id ;?>">
          <div class="direct-chat-info clearfix">
            <span class="direct-chat-name pull-left"><?php echo $this->admin_m->get_field_by_id(@$note->user_id,"display_name") ;?></span>
            <span class="direct-chat-timestamp pull-right"><?php echo date('D h:i A, d-m-Y', $note->created_on) ;?></span>
          </div>
          <!-- /.direct-chat-info -->
          <i class="direct-chat-img fa fa-user-o" aria-hidden="true"></i><!-- /.direct-chat-img -->
          <div class="direct-chat-text">
                <?php if($note->user_id == $this->admin_m->id) { ?>
                    <ul class="list-inline icon-edit-update pull-right">
                       <li class="icon-edit"><a href="#update" class="update" title="Sửa" data-update-id="<?php echo $note->msg_id ;?>" data-term-id="<?php echo $note->term_id ;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                       <li class="icon-delete"><a href="#delete" class="delete" title="Xoá" data-delete-id="<?php echo $note->msg_id ;?>" data-term-id="<?php echo $note->term_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                    </ul>
                <?php } ?>    
            	<div class="msg-content"><?php echo $note->msg_content ;?></div>
          </div>
          <!-- /.direct-chat-text -->
        </div>
        <!-- /.direct-chat-msg -->
			<?php } else { ?>
			    <!-- <?php echo $this->admin_m->get_field_by_id(@$note->user_id,"display_name") ;?> -->
				<div class="direct-chat-msg right" id="msg-<?php echo @$note->msg_id ;?>">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right"><?php echo $this->admin_m->get_field_by_id(@$note->user_id, "display_name") ;?></span>
                        <span class="direct-chat-timestamp pull-left"><?php echo date('D h:i A, d-m-Y', $note->created_on) ;?></span>                       
                      </div>
                      <!-- /.direct-chat-info -->
                      <i class="direct-chat-img fa fa-user-circle" aria-hidden="true"></i><!-- /.direct-chat-img -->
                      <div class="direct-chat-text">
                          <?php if($note->user_id == $this->admin_m->id) { ?>
                            <ul class="list-inline icon-edit-update pull-right">
                               <li class="icon-edit"><a href="#update" class="update" title="Sửa" data-update-id="<?php echo $note->msg_id ;?>" data-term-id="<?php echo $note->term_id ;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                               <li class="icon-delete"><a href="#delete" class="delete" title="Xoá" data-delete-id="<?php echo $note->msg_id ;?>" data-term-id="<?php echo $note->term_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                            </ul>
                          <?php } ?>  
                        	<div class="msg-content"><?php echo $note->msg_content ;?></div>
                      </div>
                      <!-- /.direct-chat-text -->
                </div>
			<?php } 
		}
	}
?>