<?php
class Message_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name()
	{
		return 'Message';
	}

	public function init()
	{
		$this->_load_menu();
	}

	private function _load_menu()
	{
		# load menu
	}

	public function title()
	{
		return 'Message';
	}

	public function author()
	{
		return 'thonh';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Message';
	}
	
	private function init_permissions()
	{
		$permissions = array(
			'Note_contract.Index' => array('description' => '','actions' => array('access','add','delete','update','manage')),
		);

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			// $this->permission_m->delete_by_name($name);
		}
	}
}