<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Note_contract_m extends Message_m {
    public $msg_type    = 'note';

    public $msg_status  = 'sent';



    public function __construct() {
        parent::__construct();
    }

    public function count_contract($term_id) {
    	return $this->message_m->select('term_id')->where_in('msg_status' , array($this->msg_status, 'update'))
                                                  ->where('msg_type' , $this->msg_type)
                                                  ->where('term_id' , $term_id)
					   							  ->get_many_by() ; 

    }

    public function load($id, $type = 'term_id', $msg_status = '') {
        $result     = array() ;

        if($type == 'term_id') {
        	$result = $this->message_m->where('term_id', $id);
        }
        else {
            $result = $this->message_m->where('msg_id', $id)  ;
        }
      
        $result = $this->message_m->where('msg_type' , $this->msg_type) ;
                                  
        if($msg_status != '') {
            $result = $this->message_m->where('msg_status' , $msg_status) ; 
        }
        else {
            $result = $this->message_m->where_in('msg_status' , array($this->msg_status, 'update')) ;
        }     

        $result = $this->message_m->order_by('created_on' , 'asc')
                                  ->get_many_by();    
    	return $result ;					      
    } 

}
/* End of file Note_m.php */
/* Location: ./application/modules/message/models/Note_m.php */