<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Message_m extends Base_model 
{
    public $primary_key = 'msg_id';
    public $_table = 'message';
    public $header = array();
    public $before_create = array('timestamps');

    function __construct()
    {
        parent::__construct();
        $this->load->model('message/header_m');
    }

    protected function timestamps($row)
    {
    	$row['created_on'] = $row['created_on'] ?? time();
    	
        return $row;
    }
   
    /**
     * Create a new row into the table. $data should be an associative array
     * of data to be inserted. Returns newly created ID.
     *
     * @param      array  $data     the message data content
     * @param      array  $headers  The headers (recipients)
     *
     * @return     bool  ( description_of_the_return_value )
     */
    public function create($message = array(), $headers = array())
    {
        if(empty($message) || !is_array($message)) return FALSE;
        $default_message = array(    
            'term_id' => 0,
            'msg_title' => '',
            'msg_content' => '',
            'msg_type' => 'default',
            'msg_status' => 'sent',
            'metadata' => '',
            'created_on' => time());
        $message = array_merge($default_message,$message);

        $insert_id = $this->insert($message);
        if(!$insert_id) return FALSE;

        $default_header = array(
            'hed_type' => 'email',
            'hed_subject' => $message['msg_title'] ?? '',
            'hed_status' => 0,
            'hed_to' => '',
            'user_id' => 0,
            'term_id' => 0,
            'msg_id' => 0
            );

        foreach ($headers as $header) 
        {
            $header             = array_merge($default_header,$header);
            $header['msg_id']   = $insert_id;
            $this->header_m->insert($header);
        }

        return TRUE;
    }

    public function get_all_by($term_id = '',$user_id = '',$phone = '')
    {
        if(!empty($user_id)) $this->where('message_header.user_id',$user_id);
        else $this->where('message_header.hed_to',$phone);

        $messages = $this
        ->join('message_header','message_header.msg_id = message.msg_id','LEFT')
        ->group_by('message.msg_id')
        ->where('message_header.term_id',$term_id)
        ->order_by('message.created_on','desc')
        ->get_many_by();

        if(!$messages) return FALSE;

        $default_term_type = array(
            'webdoctor' => 'webdoctor', 
            'seo-traffic' => 'seo-traffic', 
            'seo-top' => 'seo-top', 
            'google-ads'=>'adsplus',
            'facebook-ads'=>'facebook-ads',
            'webgeneral'=>'webgeneral',
            'webdesign'=>'webbuild'
            );

        $term = $this->term_m->select('term_id,term_type')->get($term_id);
        $data = array();
        foreach($messages as $msg)
        {
            $metadata = !empty($msg->metadata) ? @unserialize($msg->metadata) : '';
            $data[] = array(
                'id' => $msg->hed_id,
                'type' => $default_term_type[$term->term_type],
                'msg' => 'message',
                'msg_type' => 'message',
                'task_status' => 0,
                'title' => $msg->msg_title,
                'content' => $msg->msg_content,
                'icon' => '',
                'status' => $msg->hed_status,
                'day' => $msg->created_on,
                'contract_id' => $msg->term_id,
                'metadata' => $metadata
                );
        }

        return $data;
    }
}
/* End of file Message_m.php */
/* Location: ./application/modules/message/models/Message_m.php */