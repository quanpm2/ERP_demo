<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Header_m extends Base_model 
{
    public $primary_key = 'hed_id';
    public $_table = 'message_header';

    function __construct()
    {
        parent::__construct();
    }
}
/* End of file Header_m.php */
/* Location: ./application/modules/message/models/Header_m.php */