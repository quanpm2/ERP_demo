<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['task_type'] = array(
	'post-write' => 'Tự viết bài', 
	'post-optimize'=>'Tối ưu bài viết', 
	'other' =>'Việc khác', 
	'product-write' =>'Đăng sản phẩm',
	'product-edit' =>'Sửa sản phẩm',
	'review' =>'Review Website',
	'custom-task' =>'Yêu cầu khách hàng'
	);

$config['tasks_type'] = array(
	'custom-task' =>'Yêu cầu khách hàng',
	'task-default'=>'Tối ưu', 
	'task-design-default'=>'Thiết kế Website ban đầu', 
	'banner' =>'Thiết kế Banner'
	);

$config['post_type'] = array(
	'task'				=>'webgeneral-task',
	'content'		 	=>'webgeneral-content',
	'content_product'	=>'webgeneral-content-product',
	'mail' 				=>'webgeneral-mail'
	);

$config['post_status'] = array(
	'publish'	=>	'Khởi tạo',
	'complete'	=>	'Hoàn thành',
	'process'	=>	'Đang xử lý',
	);

$config['services'] = array(
	'seotraffic'		=>'SEO Chặng',
	'content' 			=>'Nội dung',
	'content_product' 	=>'Sản phẩm'
	);

$config['packages'] = array(
	'service' =>array(
		'startup'=> array(
			'label'=>'Gói dịch vụ 500.000 đ',
			'name'=> 'Gói bắt đầu',
			'seotraffic'=> 0,
			'content' => 5,
			'banner' => 0,
			'price' => 500000,
			'gift'=> FALSE,
			'backlink' => FALSE
			),
		'startupplus'=> array(
			'label'=>'Gói dịch vụ 1.500.000 đ',
			'name'=> 'Gói cơ bản',
			'seotraffic'=> 0,
			'content' =>5,
			'banner' => 3,
			'price' => 1500000,
			'gift'=> FALSE,
			'appendix'=>'files/phu_luc/phu-luc-webdoctor-goi-co-ban.pdf',
			'backlink' => FALSE
			),
		'basic'=> array(
			'label'=>'Gói dịch vụ 2.500.000 đ',
			'name'=> 'Gói tiêu chuẩn',
			'seotraffic'=> 0,
			'content' =>15,
			'banner' => 3,
			'price' => 2500000,
			'gift'=> array(
				'webdesign' => array(
					'label' => 'Miễn phí Website theo mẫu',
					'value' => '5000000'
					),
				'hosting' => array(
					'label' => 'Miễn phí Hosting',
					'value' => '2000000'
					),
				'domain' => array(
					'label' => 'Miễn phí Domain',
					'value' => '250000'
					)
				),
			'appendix'=>'files/phu_luc/phu-luc-webdoctor-goi-tieu-chuan.pdf',
			'backlink' => FALSE
		),
		'normal' =>array(
			'label'=>'Gói dịch vụ 6.000.000 đ',
			'name'=> 'Gói chuyên nghiệp',
			'seotraffic'=> 3000,
			'content' =>30,
			'price' => 6000000,
			'banner' => 6,
			'gift'=> array(
				'webdesign' => array(
					'label' => 'Miễn phí Thiết kế Website theo mẫu',
					'value' => '5000000'
					),
				'hosting' => array(
					'label' => 'Miễn phí Hosting',
					'value' => '3000000'
					),
				'domain' => array(
					'label' => 'Miễn phí Domain',
					'value' => '950000'
					)
				),
			'appendix'=>'files/phu_luc/phu-luc-webdoctor-goi-chuyen-nghiep.pdf',
			'backlink' => FALSE
			)
		),
	'default' =>'basic'
	);
