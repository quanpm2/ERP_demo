
<!--cta-->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="15"></td>
  </tr>
  <tr>
    <td align="center" bgcolor="#ffffff" style="border-top:3px solid #3498db;border-bottom:2px solid #e0e0e0;">
      <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="table-inner">
        <tr>
          <td align="center">
            <!-- title -->
            <table class="table-full" align="center" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><img src="http://webdoctor.vn/images/quote-left.png" width="25" height="37" alt="img" /></td>
                <td align="center" bgcolor="#3498db" style="font-family: open sans, arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 25px;padding-right: 25px;"><a style="color: #ffffff;text-decoration: none;" href="#">WEBDOCTOR.VN</a></td>
                <td><img src="http://webdoctor.vn/images/quote-right.png" width="25" height="37" alt="img" /></td>
              </tr>
            </table>
            <!-- end title -->
          </td>
        </tr>
        <tr>
          <td height="25"></td>
        </tr>
        <!-- headline -->
        <tr>
          <td align="center" valign="top" style="font-family: open sans, arial, sans-serif; font-size:30px; color:#4a4a4a; font-weight:bold;"><a style="color: #4a4a4a;text-decoration: none;" href="#">GIẢI PHÁP WEBDOCTOR.VN</a></td>
        </tr>
        <!-- End headline -->
        <tr>
          <td height="15"></td>
        </tr>
        <!-- Content -->
        <tr>
          <td align="center" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px; line-height:28px; text-align: justify;">
            <table class="table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
              <tbody><tr>
                <td align="center" style="line-height: 0px;"><a href="http://webdoctor.vn/" target="_blank"><img src="https://webdoctor.vn/images/webdoctor-logo.png"></a></td>
              </tr>
            </tbody></table>

            <table class="table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
              <tbody><tr> <td align="center" style="line-height: 0px;">
                <a href="http://adsplus.vn/" target="_blank"><img src="https://webdoctor.vn/images/adsplus-logo.png"></a></td>
              </tr>
            </tbody></table>
          </td>

        </tr>

        <tr>
          <td align="center" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px; line-height:28px; text-align: justify;">
            <p><b>Sứ mệnh của Webdoctor.vn</b><br>
              Là sát cánh với Khách hàng; giúp Khách hàng có một website tốt với chi phí vận hành tiết kiệm; và qua đó giúp Khách hàng phát huy được tối đa hiệu quả kinh doanh.
            </p>
            <p><b>Sứ mệnh của Adsplus.vn</b> <br>
             Google và Facebook lần lượt là hai kênh quảng cáo trực tuyến phổ biến nhất Việt Nam về tính hiệu quả và tỷ lệ quy đổi ra đơn hàng. Adsplus.vn nỗ lực giúp Khách hàng sử dụng hai kênh quảng cáo này tốt nhất; đơn giản nhất; minh bạch nhất và qua đó giúp Khách hàng nâng cao doanh số cũng như phát triển thương hiệu.
           </p>
         </td>
       </tr>
       <!-- End Content -->
       <tr>
        <td height="25"></td>
      </tr>
      <!-- Button -->
      <tr>
        <td align="center">
          <table border="0" cellspacing="0" cellpadding="0" style="border:2px solid #3498db;">
            <tr>
              <td align="center" height="40" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px;padding-left: 25px;padding-right: 25px;"><a href="http://webdoctor.vn/" target="_blank">Xem chi tiết</a></td>
            </tr>
          </table>
        </td>
      </tr>
      <!--End Button-->
      <tr>
        <td height="35"></td>
      </tr>
    </table>
  </td>
</tr>
<tr>
  <td height="15"></td>
</tr>
</table>
<!--end cta-->




<!--footer-->
<table width="100%" bgcolor="#ecf0f1" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="15"></td>
  </tr>
  <tr>
    <td bgcolor="#34495e" style="border-top:4px solid #3498db;">
      <table class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="35"></td>
        </tr>
        <tr>
          <td>
            <!--note-->
            <table class="table-full" bgcolor="#405366" width="310" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left">
                  <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#34495e">
                    <tr>
                      <!--Title-->
                      <td align="left" bgcolor="#34495e" style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Tư vấn miễn phí</td>
                      <!--End title-->
                      <td width="25" align="left" bgcolor="#405366" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/note-1.png" width="23" height="30" alt="img" /></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="10"></td>
              </tr>
              <tr>
                <td>
                  <table class="table-inner" align="center" width="260" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="footer-link" align="left" bgcolor="#405366" style="font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7; line-height:28px;">
                        <b>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</b><br>
                        <b>Hotline:</b>(08) 7300. 4488</td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td height="25" align="right" valign="bottom" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/note-2.png" width="25" height="25" alt="img" /></td>
                </tr>
              </table>
              <!--note-->
              <!--Space-->
              <table width="1" border="0" cellpadding="0" cellspacing="0" align="right">
                <tr>
                  <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                    <p style="padding-left:24px;">&nbsp;</p>
                  </td>
                </tr>
              </table>
              <!--End Space-->
              <!--Preference-->
              <table class="table-full" align="left" width="120" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Thông tin</td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td class="footer-link" style="line-height:28px; font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7;">
                    <a href="https://webdoctor.vn/about/" target="_blank">Về chúng tôi</a><br/>
                    <a href="http://webdoctor.vn/bang-gia-dich-vu/" target="_blank">Bảng giá</a>
                    <br/>
                    <a href="https://webdoctor.vn/contact-us/" target="_blank">Liên hệ</a></td>
                  </tr>
                </table>
                <!--End Preference-->
                <!--Space-->
                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                  <tr>
                    <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                      <p style="padding-left:24px;">&nbsp;</p>
                    </td>
                  </tr>
                </table>
                <!--End Space-->
                <!--Social-->
                <table class="table-full" align="left" width="120" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Social</td>
                  </tr>
                  <tr>
                    <td height="10"></td>
                  </tr>
                  <tr>
                    <td class="footer-link" style="line-height:28px; font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7;"><a href="https://www.facebook.com/webdoctor.vn" target="_blank" style="color:#bdc3c7;font-weight: normal;">Facebook</a>
                      <br/>
                      <a href="https://plus.google.com/+SccomVnseo/posts" target="_blank">Google Plus</a>
                      <br/>
                      <a href="#" target="_blank">Youtube</a></td>
                    </tr>
                  </table>
                  <!--End Social-->
                </td>
              </tr>
              <tr>
                <td height="35"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td bgcolor="#2c3e50">
            <table align="center" width="600" class="table-inner" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="20"></td>
              </tr>
              <tr>
                <td class="footer-link">
                  <!--copyright-->
                  <table align="left" class="table-full" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="font-family: 'Open sans', Arial, sans-serif; color:#bdc3c7; font-size:14px;">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không phải reply lại email này. </td>
                    </tr>
                  </table>
                  <!--end copyright-->

                </td>
              </tr>
              <tr>
                <td height="20"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <!--end footer-->
    </body>

    </html>