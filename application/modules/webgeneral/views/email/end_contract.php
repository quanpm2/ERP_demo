<?php $this->load->view('email/header'); ?>

<?php
$start_time = get_term_meta_value($term->term_id,'start_service_time');
$end_time = get_term_meta_value($term->term_id,'end_service_time');


$is_expired = ($end_time < $time);
$bg_color = ($is_expired) ? '#dd4b39' : '#ff9f00';


$title_bar = ($is_expired) ? 'Thông báo hết hạn dịch vụ' : 'Cảnh báo hết hạn dịch vụ';
?>


<?php $content_style = 'font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;';?>
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
	<tbody><tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td align="center">
			<table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
				<tbody><tr>
					<td align="left" bgcolor="#f8f8f8" style="border-top:3px solid  #3498db">
						<table width="600" class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
							<tbody><tr>
								<td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
									<table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
										<tbody><tr>
											<!--Title-->
											<td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Kết thúc hợp đồng</td>
											<!--End title-->
											<td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
										</tr>
									</tbody></table>
									<!--Space-->
									<table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
										<tbody><tr>
											<td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
												<p style="padding-left: 24px;">&nbsp;</p>
											</td>
										</tr>
									</tbody></table>
									<!--End Space-->
									<!--detail-->
									<table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
										<tbody><tr>
											<td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Thông báo kết thúc hợp đồng</td>
										</tr>
									</tbody></table>
									<!--end detail-->
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>


			<tr>
				<td bgcolor="#ffffff">
					<table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
						<tbody>
							<tr>
								<td height="25"></td>
							</tr>
							<tr>
								<td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999; line-height:28px;">Xin chào quý khách.<br>

									<p><b>WebDoctor.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</b></p>

									<p><b>Chúng tôi xin thông báo về việc hết hạn sử dụng dịch vụ, quý khách vui lòng liên hệ công ty để tiến hành gia hạn dịch vụ.</b></p></td>
								</tr>
								<tr>
									<td height="25"></td>
								</tr>
								<tr>
									<td>
										<!-- date -->
										<table style="border-radius:5px;" class="table1-3" bgcolor="#ecf0f1" width="183" border="0" align="left" cellpadding="0" cellspacing="0">
											<tbody><tr>
												<td align="center">
													<table border="0" align="center" cellpadding="0" cellspacing="0">
														<tbody><tr>
															<td height="10"></td>
														</tr>
														<tr>
															<td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:60px; color:#34495e;font-weight: bold;line-height: 48px;">
																<?php
																echo (($end_time < $time) ? '0' : floor(diffInDates($time,$end_time)));

																?></td>
															</tr>
															<tr>
																<td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:30px; color:#3498db; font-weight: bold;">NGÀY</td>
															</tr>
															<tr>
																<td height="10"></td>
															</tr>
														</tbody></table>
													</td>
												</tr>
											</tbody></table>
											<!-- End date -->
											<!--Space-->
											<table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
												<tbody><tr>
													<td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
														<p style="padding-left:24px;">&nbsp;</p>
													</td>
												</tr>
											</tbody></table>
											<!--End Space-->
											<!--Content-->
											<table class="table3-1" width="342" border="0" align="right" cellpadding="0" cellspacing="0">
												<tbody><tr>
													<td align="left" style="font-family: open sans, arial, sans-serif; font-size:15px; color:#999999; line-height:28px;">
														<b>Domain:</b> <?php echo $term->term_name;?><br>
														<?php echo $this->webgeneral_config_m->get_package_label($term_id).'';?> <br>
														<b>Ngày kết thúc:</b> <?php echo my_date($end_time,'d/m/Y');?><br>
													</td>
												</tr>
											</tbody></table>
											<!--End Content-->
										</td>
									</tr>
								</tbody></table>
							</td>
						</tr>


						<tr>
							<td bgcolor="#ffffff">

								<table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
									<tbody>
										<!--Content-->
										<tr>
											<td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999; line-height:28px;">
												<p style="font-family: open sans, arial, sans-serif; font-size:15px">Để thực hiện gia hạn dịch vụ vui lòng liên hệ với chúng tôi theo thông tin dưới đây:</p>

												<table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
													<tbody>
														<tr>
															<td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?= $staff->display_name?>: </td>

															<td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php
																$phone = $this->usermeta_m->get_meta_value($staff->user_id,'user_phone');
																if(!$phone) $phone = '(08) 7300. 4488';

																echo $phone; ?></td>
															</tr>

															<tr>
																<td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>

																<td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?= $staff->user_email ?></td>
															</tr>


															<tr>
																<td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Hotline Trung tâm Kinh doanh:</td>

																<td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">(08) 7300. 4488</td>
															</tr>
															<tr>
																<td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>

																<td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">sales@webdoctor.vn</td>
															</tr>

															<tr>
																<td height="25"></td>
															</tr>
														</tbody>
													</table>

													<p><i>Lưu ý: Email này thông báo rằng hợp đồng dịch vụ đã kết thúc, chúng tôi sẽ tiến hành ngưng dịch vụ sau 1 ngày kể từ thông báo này.</i></p>
												</td>
											</tr>
											<!--End Content-->
											<tr>
												<td align="left" height="30"></td>
											</tr>
										</tbody>
									</table>




								</td>
							</tr>
						</tbody></table>

					</td>
				</tr>
				<tr>
					<td height="15"></td>
				</tr>
			</tbody></table>








			<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
				<tbody><tr>
					<td height="15"></td>
				</tr>
				<tr>
					<td align="center">
						<table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
							<tbody><tr>
								<td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
									<table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
										<tbody><tr>
											<!--Title-->
											<td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Webdoctor đã thực hiện</td>
											<!--End title-->
											<td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
										</tr>
									</tbody></table>
									<!--Space-->
									<table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
										<tbody><tr>
											<td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
												<p style="padding-left: 24px;">&nbsp;</p>
											</td>
										</tr>
									</tbody></table>
									<!--End Space-->
									<!--detail-->
									<table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
										<tbody><tr>
											<td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Các việc Webdoctor đã thực hiện</td>
										</tr>
									</tbody></table>
									<!--end detail-->
								</td>
							</tr>

							<tr>
								<td bgcolor="#ffffff">
									<table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
										<tbody><tr>
											<td height="25"></td>
										</tr>
										<tr>
											<td>
												<!--left-->
												<table class="table-full" width="166" border="0" align="left" cellpadding="0" cellspacing="0">
													<!-- image -->
													<tbody>
														<tr>
															<td align="left" style="font-family: open sans, arial, sans-serif; font-size:16px; color:#999999; text-align: center; line-height:28px;"><b>Đã thực hiện</b></td>
														</tr>
														<tr>
															<td align="center" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" class="img1" src="http://webdoctor.vn/images/tasks.png" width="166" height="150" alt="img"></td>
														</tr>
														<!-- End image -->
														<tr>
															<td height="25"></td>
														</tr>
														<!--content-->
														<tr>
															<td align="left" style="font-family: open sans, arial, sans-serif; font-size:18px; color:#999999; text-align: center; line-height:28px;"><b><?php echo $num_cusomer_task;?> công việc</b></td>
														</tr>
														<!--end content-->
													</tbody></table>
													<!--end left-->
													<!--Space-->
													<table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
														<tbody><tr>
															<td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
																<p style="padding-left:24px;">&nbsp;</p>
															</td>
														</tr>
													</tbody></table>
													<!--End Space-->
													<!--middle-->
													<table class="table-full" width="166" border="0" align="left" cellpadding="0" cellspacing="0">
														<!-- image -->
														<tbody>

															<!--content-->
															<tr>
																<td align="left" style="font-family: open sans, arial, sans-serif; font-size:16px; color:#999999; text-align: center; line-height:28px;"><b>Đã viết</b></td>
															</tr>

															<tr>
																<td align="center" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" class="img1" src="http://webdoctor.vn/images/content.png" width="166" height="150" alt="img"></td>
															</tr>
															<!-- End image -->
															<tr>
																<td height="25"></td>
															</tr>
															<!--content-->
															<tr>
																<td align="left" style="font-family: open sans, arial, sans-serif; font-size:18px; color:#999999; text-align: center; line-height:28px;"><b><?php echo $num_content;?> bài viết</b></td>
															</tr>
															<!--end content-->
														</tbody></table>
														<!--end middle-->
														<!--Space-->
														<table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
															<tbody><tr>
																<td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
																	<p style="padding-left:24px;">&nbsp;</p>
																</td>
															</tr>
														</tbody></table>
														<!--End Space-->
														<!--right-->
														<table class="table-full" width="166" border="0" align="right" cellpadding="0" cellspacing="0">
															<!-- image -->
															<tbody><tr>
																<td align="left" style="font-family: open sans, arial, sans-serif; font-size:16px; color:#999999; text-align: center; line-height:28px;"><b>Đã thiết kế</b></td>
															</tr><tr>
															<td align="center" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" class="img1" src="http://webdoctor.vn/images/banner.png" width="166" height="150" alt="img"></td>
														</tr>
														<!-- End image -->
														<tr>
															<td height="25"></td>
														</tr>
														<!--content-->
														<tr>
															<td align="left" style="font-family: open sans, arial, sans-serif; font-size:18px; color:#999999; text-align: center; line-height:28px;">
																<b> <?php echo $num_banner;?> banner</b>
															</td>
														</tr>
														<!--end content-->
													</tbody></table>
													<!--end right-->
												</td>
											</tr>
											<tr>
												<td height="25"></td>
											</tr>
										</tbody></table>
									</td>
								</tr>



							</tbody></table>

						</td>
					</tr>

				</tbody></table>




  <?php if(!empty($contents)): ?>
    <!--end 2/2 panel full-->
    <!--1/2 right-->
    <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="15"></td>
      </tr>
      <tr>
        <td align="center">
          <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
                    <!--Title-->
                    <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Báo cáo bài viết</td>
                    <!--End title-->
                    <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                  </tr>
                </table>
                <!--Space-->
                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                  <tr>
                    <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                      <p style="padding-left: 24px;">&nbsp;</p>
                    </td>
                  </tr>
                </table>
                <!--End Space-->
                <!--detail-->
                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Danh sách bài viết đã đưa lên Website</td>
                  </tr>
                </table>
                <!--end detail-->
              </td>
            </tr>

            <!--start Article-->
            <tr>
              <td bgcolor="#ffffff">
                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td height="25"></td>
                    </tr>
                    <?php foreach($contents as $i=>$content): ?>
                      <tr>
                        <td align="left" style="<?php echo $content_style;?><?php echo ($i%2 == 0) ?'background: #f2f2ff;': ''; ?>padding-left: 5px;padding-right: 5px;" title="<?php echo $content->post_content;?>"><?php echo $content->post_title;?></td>

                        <td align="left" style="<?php echo $content_style;?><?php echo ($i%2 == 0) ?'background: #f2f2ff;': ''; ?>padding-left: 5px;padding-right: 5px;"><a href="<?php echo $content->post_content;?>" style="text-decoration: none;" title="<?php echo $content->post_content;?>" target="_blank">Xem</a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                    <tr>
                      <td height="25"></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <!--end Article-->

          </table>
        </td>
      </tr>

    </table>
    <!--end 1/2 right-->
  <?php endif; ?>



  <?php if(!empty($tasks)): ?>
    <!--start công việc đã thực hiện task -->
    <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="15"></td>
      </tr>
      <tr>
        <td align="center">
          <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
                    <!--Title-->
                    <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Báo cáo công việc</td>
                    <!--End title-->
                    <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                  </tr>
                </table>
                <!--Space-->
                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                  <tr>
                    <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                      <p style="padding-left: 24px;">&nbsp;</p>
                    </td>
                  </tr>
                </table>
                <!--End Space-->
                <!--detail-->
                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Danh sách công việc đã thực hiện</td>
                  </tr>
                </table>
                <!--end detail-->
              </td>
            </tr>

            <!--start Article-->
            <tr>
              <td bgcolor="#ffffff">
                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td height="25"></td>
                    </tr>
                    <?php
                    foreach($tasks as $post): 
                      $task_type = get_post_meta_value($post->post_id, 'task_type');
                    // if(get_post_meta_value($post->post_id, 'task_type') == 'task-default');

                    $status = $this->config->item($post->post_status, 'post_status');
                    if(!$status)
                      continue;
                    switch ($post->post_status) {
                      case 'process':
                      $status = 'Đang thực hiện';
                      break;
                      case 'publish':
                      $status = 'Chờ thực hiện';
                      break;

                      default:
                          # code...
                      break;
                    } ?>
                    <tr>
                      <td align="left" style="<?php echo $content_style;?>"><?php echo ucfirst($post->post_title);?></td>

                      <td align="right" style="<?php echo $content_style;?>"><span style="color: #3498db;"><?php echo $status;?></span>
                      </td>
                    </tr>
                    <?php 
                    endforeach; ?>
                    <tr>
                      <td height="25"></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <!--end Article-->
          </table>
        </td>
      </tr>
    </table>
    <!--end công việc đã thực hiện task -->
  <?php endif;?>







				<!-- thong tin khach hang -->
				<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
					<tbody><tr>
						<td height="15"></td>
					</tr>
					<tr>
						<td align="center">
							<table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
								<tbody><tr>
									<td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
										<table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
											<tbody><tr>
												<!--Title-->
												<td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin khách hàng</td>
												<!--End title-->
												<td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
											</tr>
										</tbody></table>
										<!--Space-->
										<table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
											<tbody><tr>
												<td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
													<p style="padding-left: 24px;">&nbsp;</p>
												</td>
											</tr>
										</tbody></table>
										<!--End Space-->
										<!--detail-->
										<table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
											<tbody><tr>
												<td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Thông tin khách hàng và hợp đồng</td>
											</tr>
										</tbody></table>
										<!--end detail-->
									</td>
								</tr>

								<!--start Article-->
								<tr>
									<td bgcolor="#ffffff">
										<table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td height="25"></td>
												</tr>
												<?php

												$service_date = my_date($start_time,'d/m/Y').' - '.my_date($end_time,'d/m/Y');
												$contract_date = my_date(get_term_meta_value($term_id,'contract_begin'),'d/m/Y').' - '.my_date(get_term_meta_value($term_id,'contract_end'),'d/m/Y');
												?>
												<?php 
												$rows = array();
												$rows[] = array('Mã hợp đồng', get_term_meta_value($term->term_id,'contract_code'));
												$rows[] = array('Thời gian hợp đồng', $contract_date);
												$rows[] = array('Thời gian thực hiện', $service_date);

												$rows[] = array('Tên khách hàng', 'Anh/Chị '.get_term_meta_value($term->term_id,'representative_name'));
												$rows[] = array('Email',get_term_meta_value($term->term_id, 'representative_email'));
												$rows[] = array('Điện thoại di động', get_term_meta_value($term->term_id, 'representative_phone'));
												$rows[] = array('Địa chỉ', get_term_meta_value($term->term_id, 'representative_address'));

												foreach($rows as $i=>$row):
													?>
												<?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
												<tr>
													<td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>

													<td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
													</td>
												</tr>
											<?php endforeach; ?>
											<tr>
												<td height="25"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<!--end Article-->

						</tbody></table>
					</td>
				</tr>

			</tbody></table>
			<!-- thong tin khach hang -->


			<?php $this->load->view('email/footer'); ?>