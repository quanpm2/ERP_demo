<div class="col-md-12" id="review-partial">
<h2 class="page-header">
  <small class="pull-right">Ngày tạo : <?php echo my_date(time(), 'd-m-Y');?></small>
</h2>
<div class="row">
  <div class="col-xs-12">
      <p class="lead">Thông tin khách hàng</p>
      <div class="table-responsive">
      <?php

        $representative_gender  = force_var(get_term_meta_value($edit->term_id,'representative_gender'),'Bà','Ông');
        $representative_name    = get_term_meta_value($edit->term_id,'representative_name') ?: '';
        $display_name           = "{$representative_gender} {$representative_name}";
        $representative_email   = get_term_meta_value($edit->term_id,'representative_email');
        $representative_address = get_term_meta_value($edit->term_id,'representative_address');
        $representative_phone   = get_term_meta_value($edit->term_id,'representative_phone');

        $contract_begin       = get_term_meta_value($edit->term_id,'contract_begin');
        $contract_begin_date  = my_date($contract_begin,'d/m/Y');
        $contract_end         = get_term_meta_value($edit->term_id,'contract_end');
        $contract_end_date    = my_date($contract_end,'d/m/Y');
        $contract_daterange   = "{$contract_begin_date} đến {$contract_end_date}";

      echo
      $this->table
        ->clear()->set_caption('')
        ->add_row('Người đại diện',$display_name?:'Chưa cập nhật')
        ->add_row('Email',$representative_email?:'Chưa cập nhật')
        ->add_row('Địa chỉ',$representative_address?:'Chưa cập nhật')
        ->add_row('Số điện thoại',$representative_phone?:'Chưa cập nhật')
        ->add_row('Chức vụ',$edit->extra['representative_position']??'Chưa cập nhật')
        ->add_row('Mã Số thuế',$edit->extra['customer_tax']??'Chưa cập nhật')
        ->add_row('Thời gian thực hiện',$contract_daterange?:'Chưa cập nhật')
        ->generate();
      ?>
      </div>
    </div>
    <!-- /.col -->    
  </div>
</div>
<?php

echo '<div class="clearfix"></div>',
$this->admin_form->form_open(),
$this->admin_form->hidden('','edit[term_status]', 'waitingforapprove'),
form_hidden('edit[term_id]',$edit->term_id),
form_hidden('edit[term_type]',$edit->term_type),
$this->admin_form->submit('','confirm_step_finish','confirm_step_finish','', array('style'=>'display:none;','id'=>'confirm_step_finish')),
$this->admin_form->form_close();
?>