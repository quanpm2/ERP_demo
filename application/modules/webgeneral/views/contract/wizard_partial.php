<div class="col-md-12" id="service_tab">
<?php

$service_package = $this->termmeta_m->get_meta_value($edit->term_id,'service_package');
$service_package = empty($service_package) ? 'basic' : $service_package;

$this->load->config('webgeneral/webgeneral');
$this->config->item('packages');
$services = $this->config->item('service','packages');


echo 
$this->admin_form->form_open(),
form_hidden('edit[term_id]', $edit->term_id),
$this->admin_form->submit('', 'confirm_step_service','confirm_step_service','',array('style'=>'display:none;','id'=>'confirm_step_service')),
$this->admin_form->formGroup_begin(0, 'Chọn gói');
if($services)
{
	foreach($services as $sname=>$service)
	{
		echo $this->admin_form->radio('edit[meta][service_package]',$sname, (!$service_package  || $service_package == $sname) ? TRUE : FALSE),
nbs(5),form_label($service['label']),br();
	}
}


echo $this->admin_form->formGroup_end(),
$this->admin_form->submit('', 'confirm_step_service', 'confirm_step_service', '', array('style'=>'display:none;','id'=>'confirm_step_service')),
$this->admin_form->form_close();
?>
</div>

<script type="text/javascript">
$(function(){
  /* init iCheck plugin */
  $('#service_tab input[type="radio"]').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });
})
</script>