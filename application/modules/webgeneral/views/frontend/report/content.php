<div class="col-lg-12 success" >
  <h4 class="text-center"><?php echo $heading;?></h4>
  <div class="clear"></div>
  <div class="head">
    <div class="ordinal">STT</div>
    <div class="issue">Công việc</div>
    <div class="deadline">Cập nhật</div>
    <div class="note">Ghi chú</div>
    <div class="clear"></div>
  </div>
  <?php
  foreach($tasks as $i=>$task):
  ?>
  <div class="row">
    <div class="ordinal"><?php echo ++$i;?></div>
    <div class="issue"><?php echo $task->post_title;?></div>
    <div class="status"><?php echo $this->mdate->date('d/m/Y',$task->start_date);?></div>
    <div class="note"><?php 
      if(valid_url($task->post_content))
      {
        $task->post_content = anchor($task->post_content, 'Link',array('title' => $task->post_content,'target' =>'_blank'));
      }
      echo $task->post_content;?></div>
    </div>
  <?php endforeach;?>
</div>