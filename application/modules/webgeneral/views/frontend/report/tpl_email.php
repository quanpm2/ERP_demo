<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>[WEBDOCTOR.VN] Báo cáo công việc tuần</title>
</head>

<body>
	<div style="padding:0px;margin:0px;background:#e6e6e6">
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody>
				<tr>
					<td bgcolor="#fff" height="50"><table width="800" cellspacing="0" cellpadding="0" border="0" align="center">
						<tbody>
							<tr>
								<td width="30%" height="50"><img alt="logo Webdoctor" src="http://webdoctor.vn/images/webdoctor-logo.png" style="margin-bottom: 4px;"><br></td>
								<td width="50%"><div>
									<p style="font-family:tahoma;font-size:12px;color:#b8b8b8;text-align:right">HOTLINE:<br>
										Email:<br>
									</p>
								</div></td>
								<td width="20%" style="border-left:5px #fff solid"><div>
									<p style="font-family:tahoma;font-size:12px;color:#333;text-align:left">08.7300 4488<br>
										sales@webdoctor.vn<br>
									</p>
								</div></td>
							</tr>
						</tbody>
					</table></td>
				</tr>
			</tbody>
		</table>
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody>
				<tr>
					<td bgcolor="#0072bc" style="border-top:#0072bc 2px solid">
					
						<table width="800" cellspacing="0" cellpadding="0" border="0" align="center" style="border-top:#0072bc 10px solid;border-bottom:#0072bc 10px solid">
							<tbody>
								<tr>
									<td>
										<h1 style="font-family:tahoma;color:#ffffff;font-size:30px;margin:5px 0">Danh sách công việc đã thực hiện</h1>
										<p style="font-family:tahoma;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng: 
											<span style="font-weight:bold"><?php echo get_term_meta_value($term->term_id,'contract_code');?></span>
										</p>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor="#e6e6e6">
						<table width="800" cellspacing="0" cellpadding="0" border="0" align="center">
							<tbody>
								<tr>
									<td valign="top" height="18"><img alt="arrow down" src="http://webdoctor.vn/images/arrowdown.png"></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-bottom:20px #e6e6e6 solid">
			<tbody>
				<tr>
					<td bgcolor="#e6e6e6">
						<table width="800" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:10px #e6e6e6 solid">
							<tbody>
								<tr>
									<td><p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Kính chào Quý khách</p>
										<p style="font-family:tahoma;font-size:12px;color:#363636;line-height:18px">
											<b>WEBDOCTOR.VN</b> kính gửi quý khách báo cáo các công việc đã thực hiện từ ngày <b><?php echo date('d/m/Y',$start_date); ?> đến ngày <?php echo date('d/m/Y',$end_date); ?>.</b><br>
											<?php echo anchor( @$report_link, 'Quý khách vui lòng bấm vào đây để xem chi tiết báo cáo này', 'title="Xem chi tiết báo cáo" target="_blank"'); ?>
										</p>

									</td>
								</tr>
							</tbody>
						</table>

						<table width="800" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:800px;border-bottom:20px #ffffff solid;border-top:20px #ffffff solid;border-left:20px #ffffff solid;border-right:20px #ffffff solid;background:#ffffff">
							<tbody>
								<?php 

								$template = array(
									'table_open'=>'<table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-bottom:20px;">',
									'thead_open' => '<thead><tr><th colspan="5" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">Báo cáo công việc đã thực hiện</th></tr>',
							        'heading_cell_start' => '<th style="border-bottom:1px dotted #999">',
							        'heading_cell_end' => '</th>',
									'row_start' => '<tr bgcolor="#f2f2f8" style="line-height: 20px;">',
									'row_alt_start' => '<tr style="line-height: 20px;">',
									'row_alt_end' => '</tr>',
									'cell_start' => '<td style="border-bottom:1px dotted #999">',
							        'cell_alt_start' => '<td style="border-bottom:1px dotted #999">'
								);

								if(!empty($tasks)){
								?>
								<tr>
									<td bgcolor="#ffffff">
										<?php
										$this->table->clear();
										$this->table->set_template($template);
										$this->table->set_heading(
											array('data'=>'Công việc','align'=>'left'),
											array('data'=>'Cập nhật','align'=>'left'),
											array('data'=>'Ghi chú','align'=>'left'));
										foreach($tasks as $i => $task){
											if(valid_url($task->post_content))
												$task->post_content = anchor($task->post_content, 'Link',array('title' => $task->post_content,'target' =>'_blank'));

											$this->table->add_row($task->post_title,my_date($task->end_date,'d/m/Y'),$task->post_content);
										}
										echo $this->table->generate();
										?>	
									</td>
								</tr>
								<?php
								}
								?>
								<?php 
								if(!empty($contents)){
								?>
								<tr>
									<td bgcolor="#ffffff">
										<p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636"></p>
										<?php
										$this->table->clear();
										$template['thead_open'] = '<thead><tr><th colspan="5" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">Báo cáo bài viết đã thực hiện</th></tr>';
										$this->table->set_template($template);
										$this->table->set_heading(
											array('data'=>'Bài viết','align'=>'left'),
											array('data'=>'Cập nhật','align'=>'left'),
											array('data'=>'Ghi chú','align'=>'left'));
										foreach($contents as $i => $task){
											if(valid_url($task->post_content))
												$task->post_content = anchor($task->post_content, 'Link',array('title' => $task->post_content,'target' =>'_blank'));

											$this->table->add_row($task->post_title,my_date($task->end_date,'d/m/Y'),$task->post_content);
										}
										echo $this->table->generate();
										?>	
									</td>
								</tr>
								<?php
								}
								?>

								<?php 
								if(!empty($ga)){
								?>
								<tr>
									<td bgcolor="#ffffff">
										<p></p>
										<?php
										$this->table->clear();
										$heading = array();
										$heading[] = 'Ngày';
										foreach($ga['total'] as $v=>$r)
										{
											$heading[] = $this->webgeneral_seotraffic_m->get_ga_label($v);
										}
										$colspan = count($heading);
										$template['thead_open'] = '<thead><tr><th colspan="'.$colspan.'" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">Báo cáo truy cập</th></tr>';

										$template['cell_start'] = '<td style="border-bottom:1px dotted #999;font-size:14px" align="center">';	
										$template['cell_alt_start'] = '<td style="border-bottom:1px dotted #999;font-size:14px" align="center">';
										$this->table->set_template($template);
										$this->table->set_heading($heading);
										$time = time();
										$is_show = false;
										foreach($ga['results'] as $day=>$result)
										{
											$row = array();
											$day = strtotime($day);
											if($day > $time)
												break;
											$row[] = date('d/m/Y', $day);
											foreach($ga['total'] as $key => $val)
											{
												$row[] = isset($result[$key]) ? $result[$key] : 0;
											}
											$is_show = true;
											$this->table->add_row($row);
										}

										$row = array('<b>Tổng</b>');
										foreach($ga['total'] as $key => $val)
										{
											$row[] = '<b>'.$val.'</b>';
										}
										$this->table->add_row($row);
										if($is_show)
										{
											echo $this->table->generate();
										}
										?>	
									</td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>

						<table width="800" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:10px #e6e6e6 solid">
							<tbody>
								<tr>
									<td>
										Cảm ơn quý khách đã sử dụng dịch vụ của <b><a href="//webdoctor.vn" target="_blank" style="color:#00F">WWW.WEBDOCTOR.VN</a></b>. <br>

<em style="color:#666; font-size:12px">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không phải reply lại email này.</em> <br>
</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>

		<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody>
				<tr>
					<td bgcolor="#2d2b2b"><table width="800" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:#2d2b2b 10px solid">
						<tbody>
							<tr>
								<td height="30" colspan="5"><p style="font-family:tahoma;font-size:14px;font-weight:bold;color:#b8b8b8">WebDoctor.vn</p></td>
							</tr>
							<tr>
								<td><table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td width="53"><p style="font-family:tahoma;font-size:12px;color:#b8b8b8;line-height:18px">Add<br>
												Tell<br>
												Email</p></td>
												<td width="587"><p style="font-family:tahoma;font-size:12px;color:#b8b8b8;line-height:18px">Tầng 8, 402 Nguyễn Thị Minh Khai , Phường 5, Quận 3, TP.HCM, Việt Nam<br>
													(08) 7300.4488 <br>
													<a target="_blank" href="mailto:sales@webdoctor.vn" style="color:#b8b8b8;text-decoration:none">sales@webdoctor.vn</a></p></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
					</tbody>
				</table>
				<div class="yj6qo"></div>
				<div class="adL"> </div>
			</div>
		</body>
		</html>