<!DOCTYPE html PUBLIC "-//W3C//Ddiv XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Ddiv/xhtml1-transitional.ddiv">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Báo cáo công việc <?php echo $site_name;?> từ ngày <?php echo date('d/m/Y', $start_date);?> đến ngày <?php echo date('d/m/Y', $end_date);?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="<?php echo theme_url();?>webdoctor/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo theme_url();?>webdoctor/css/style.css"/>
  <script type="text/javascript" src="<?php echo theme_url();?>webdoctor/js/Chart.js"></script>
</head>
<body>
  <div class="container">
    <div class="head">
      <div class="col-lg-3"><img src="<?php echo theme_url();?>webdoctor/images/webdoctor_logo.png" /></div>
      <div class="col-md-pull-3" style="float:right">sản phẩm của <img src="<?php echo theme_url();?>webdoctor/images/SC_logo.png"  /></div>
    </div>
    <div class="clear"></div>
    <div class="bg-success">
      <h2 class="text-center">BÁO CÁO CÔNG VIỆC <?php echo strtoupper($site_name);?></h2>
      <h5 class="text-center">(Từ ngày <?php echo date('d/m/Y', $start_date);?> đến ngày <?php echo date('d/m/Y', $end_date);?>)</h5>

      <div class="clear10"></div>
    </div>
    <div class="clear10"></div>
    
    <?php if($contents): ?>
      <?php $this->load->view('frontend/report/content', array('heading'=>'BÁO CÁO BÀI VIẾT ĐÃ THỰC HIỆN', 'tasks' =>$contents));?>
      <div class="clear10"></div>
    <?php endif;?>

    <?php if($tasks): ?>
      <?php $this->load->view('frontend/report/content', array('heading'=>'BÁO CÁO CÔNG VIỆC ĐÃ THỰC HIỆN', 'tasks' =>$tasks));?>
      <div class="clear10"></div>
    <?php endif;?>

    <?php $this->load->view('frontend/report/traffic');?>
  </div>
</body>
</html>
