<tr>
  <td align="center" style="padding:0 20px"><p style="font-family:Roboto;color:#ffffff;font-size:30px; font-weight:300; margin:5px 0;  text-transform:uppercase;"><?php echo @$title;?></p>
  </td>
</tr>
<tr>
  <td align="center"><p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng: <strong><?php echo get_term_meta_value($term->term_id,'contract_code');?></strong></p>
    <?php
    $start_time = get_term_meta_value($term->term_id,'contract_begin');
    $end_time = get_term_meta_value($term->term_id,'contract_end');
    $contract_date = my_date($start_time,'d/m/Y').' - '.my_date($end_time,'d/m/Y');
    ?>
    <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">
      Thời gian thực hiện từ:<strong> <?php echo $contract_date;?></strong></p>
      &nbsp;
    </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;"><p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">Kính chào Quý khách</p>
      <p>WebDoctor.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>
      <p>WebDoctor.vn gửi email thông báo các công việc sẽ thực hiện cho <?php echo anchor(prep_url($term->term_name));?>.</p>
    </td>
  </tr>
  <tr>
    <td align="center" style="border:20px solid #e6e6e6;">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
        <tbody>
          <tr>
            <td bgcolor="#fff">
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                <tbody>
                  <tr>
                    <td bgcolor="#fff"><p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">
                      <strong>Chi tiết công việc</strong></p>
                      <?php
                      $template = array(
                        'table_open'=>'<table border="0" cellspacing="0" cellpadding="5" width="100%" style="font-size:13px">',
                        'row_alt_start' => '<tr bgcolor="#f4f8fb">',
                        'heading_row_start' => '<tr bgcolor="#0074bd" align="left">',
                        'heading_cell_start'  => '<th width="80%" style="color:#fff"><p><strong>',
                        'heading_cell_end'  => '</strong></p></th>');
                      if(!empty($tasks)){
                        $this->table->set_template($template);
                        foreach ($tasks as $task){
                          if(valid_url($task->post_content))
                            $task->post_title = anchor($task->post_content, $task->post_title,array('title' => $task->post_content,'target' =>'_blank'));

                          $dates = my_date($task->start_date,'d/m/Y').' - '.my_date($task->end_date,'d/m/Y');
                          $this->table->add_row($task->post_title,$dates);
                          // $this->table->add_row($task->post_title,my_date($task->end_date,'d/m/Y'));
                        }
                        echo $this->table->generate();
                      }
                      ?>
                    </td>
                  </tr>
                </tbody>
              </table>
              <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">&nbsp;</p>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>

  <tr>
    <td bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6;border-right: 20px solid #e6e6e6;  ">
      <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
          <tr>
            <td>
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                <tbody>
                  <tr>
                    <td bgcolor="#ffffff" style="font-size: 13px;color: #363636;font-family: Arial, Helvetica, sans-serif;" >

                      <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                        <tbody>
                          <tr>
                            <td bgcolor="#ffffff" style="font-size: 13px; color: #363636; font-family: Arial, Helvetica, sans-serif;" ><p>* WebDoctor.vn sẽ gửi báo cáo  SMS đến Quý khách sau khi hoàn tất mỗi đầu việc.</p>
                              <p>* Báo cáo tổng hợp (đầu việc thực hiện, bài viết mới, lượt truy cập, ....) sẽ  gửi Quý khách vào thứ 2 hàng tuần. </p>
                              <p>* Lưu ý: đây chỉ là bản dự kiến ban đầu,  chúng tôi có thể thay đổi theo tình hình và nhu cầu cụ thể. </p>
                            </td>
                          </tr>
                        </tbody>
                      </table>                        
                      <p>&nbsp;</p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>