
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <title><?php echo $title; ?></title>
  <style type="text/css">
    .ReadMsgBody { width: 100%; background-color: #ffffff; }
    .ExternalClass { width: 100%; background-color: #ffffff; }
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
    html { width: 100%; }
    body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
    table { border-spacing: 0; table-layout: fixed; margin: 0 auto; }
    table table table { table-layout: auto; }
    img { display: block !important; over-flow: hidden !important; }
    table td { border-collapse: collapse; }
    .yshortcuts a { border-bottom: none !important; }
    a { color: #3498db; text-decoration: none; }
    .footer-link a { color: #BDC3C7 !important; }

    @media only screen and (max-width: 640px) {
      body { width: auto !important; }
      table[class="table-inner"] { width: 90% !important; text-align: center !important; }
      table[class="table-full"] { width: 100% !important; text-align: center }
      table[class="table1-3"] { width: 30% !important; }
      table[class="table3-1"] { width: 64% !important; text-align: center !important; }
      *[class="hide"] { max-height: 0px !important; max-width: 0px !important; font-size: 0px !important; display: none !important; }
      /*image*/
      img[class="img1"] { width: 100% !important; height: auto !important; }
    }

    @media only screen and (max-width: 479px) {
      body { width: auto !important; }
      table[class="table-inner"] { width: 90% !important; }
      table[class="table-full"] { width: 100% !important; float: none !important; }
      table[class="table1-3"] { width: 100% !important; border: 0px !important; }
      table[class="table3-1"] { width: 100% !important; text-align: center !important; }
      *[class="hide"] { max-height: 0px !important; max-width: 0px !important; font-size: 0px !important; display: none !important; }
      /*image*/
      img[class="img1"] { width: 100% !important; height: auto !important; }
    }
  </style>
</head>
<?php $content_style = 'font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;';?>
<body>
  <!--header bar-->
  <table width="100%" bgcolor="#ecf0f1" align="center" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td bgcolor="#3498db" style="border-top:5px solid #43a8eb;">
        <table align="center" class="table-inner" width="600" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right">
              <!-- date -->
              <table class="table-full" align="right" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="35" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:12px; font-style:italic; color:#ffffff;"><?php echo date('d/m/Y', $time_send);?></td>
                </tr>
              </table>
              <!-- end date -->
            </td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">
              <!-- Logo -->
              <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#3498db" class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;">
                <tr>
                  <td height="55" align="center" bgcolor="#3498db" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;padding-left: 15px;padding-right: 15px;" src="http://webdoctor.vn/images/logo.png" width="200" height="50" alt="Logo" /></td>
                  <td align="left" class="hide" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/header_shape.png" width="25" alt="img" /></td>
                </tr>
              </table>
              <!-- End Logo -->
              <!--Space-->
              <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                <tr>
                  <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                    <p style="padding-left: 24px;">&nbsp;</p>
                  </td>
                </tr>
              </table>
              <!--End Space-->
              <!-- slogan -->
              <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="table-full">
                <tr>
                  <td height="55" style="font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#7f8c8d;padding-left: 25px;padding-right: 25px;">CHĂM SÓC WEBSITE CHUYÊN NGHIỆP</td>
                </tr>
              </table>
              <!-- End slogan-->
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td height="15"></td>
    </tr>
  </table>
  <!--end header bar-->
  <!--1/1 image-->
  <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="15"></td>
    </tr>
    <tr>
      <td>
        <table class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center" style="">
              <img src="http://webdoctor.vn/images/cover.png" height="129" width="600">
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td height="15"></td>
    </tr>
  </table>
  <!--end 1/1 image-->


  <!--date-->
  <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="15"></td>
    </tr>
    <tr>
      <td align="center">
        <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
              <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                  <!--Title-->
                  <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Báo cáo tuần</td>
                  <!--End title-->
                  <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                </tr>
              </table>
              <!--Space-->
              <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                <tr>
                  <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                    <p style="padding-left: 24px;">&nbsp;</p>
                  </td>
                </tr>
              </table>
              <!--End Space-->
              <!--detail-->
              <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Báo cáo tự động</td>
                </tr>
              </table>
              <!--end detail-->
            </td>
          </tr>
          <tr>
            <td bgcolor="#ffffff">
              <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="25"></td>
                </tr>
                <tr>
                  <td>
                    <!-- date -->
                    <table style="border-radius:5px;" class="table1-3" bgcolor="#ecf0f1" width="183" border="0" align="left" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="center">
                          <table border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                              <td height="10"></td>
                            </tr>
                            <tr>
                              <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:60px; color:#34495e;font-weight: bold;line-height: 48px;"><?php echo date('d', $end_date);?></td>
                            </tr>
                            <tr>
                              <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:30px; color:#3498db; font-weight: bold;"><?php echo strtoupper(date('M', $end_date));?></td>
                            </tr>
                            <tr>
                              <td height="10"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <!-- End date -->
                    <!--Space-->
                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                      <tr>
                        <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                          <p style="padding-left:24px;">&nbsp;</p>
                        </td>
                      </tr>
                    </table>
                    <!--End Space-->
                    <!--Content-->
                    <table class="table3-1" width="342" border="0" align="right" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="left" style="font-family: open sans, arial, sans-serif; font-size:15px; color:#999999; line-height:28px;">
                          <b>WEBDOCTOR.VN</b> kính gửi quý khách báo cáo công việc thực hiện từ ngày <b><?php echo date('d/m/Y', $start_date);?></b> đến ngày <b><?php echo date('d/m/Y', $end_date);?></b>.
                        </td>
                      </tr>
                    </table>
                    <!--End Content-->
                  </td>
                </tr>
                <tr>
                  <td height="25"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td height="15"></td>
    </tr>
  </table>



  <?php if(!empty($contents)): ?>
    <!--end 2/2 panel full-->
    <!--1/2 right-->
    <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="15"></td>
      </tr>
      <tr>
        <td align="center">
          <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
                    <!--Title-->
                    <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Báo cáo bài viết tuần</td>
                    <!--End title-->
                    <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                  </tr>
                </table>
                <!--Space-->
                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                  <tr>
                    <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                      <p style="padding-left: 24px;">&nbsp;</p>
                    </td>
                  </tr>
                </table>
                <!--End Space-->
                <!--detail-->
                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Danh sách bài viết đã đưa lên Website trong tuần</td>
                  </tr>
                </table>
                <!--end detail-->
              </td>
            </tr>

            <!--start Article-->
            <tr>
              <td bgcolor="#ffffff">
                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td height="25"></td>
                    </tr>
                    <?php foreach($contents as $i=>$content): ?>
                      <tr>
                        <td align="left" style="<?php echo $content_style;?><?php echo ($i%2 == 0) ?'background: #f2f2ff;': ''; ?>padding-left: 5px;padding-right: 5px;" title="<?php echo $content->post_content;?>"><?php echo $content->post_title;?></td>

                        <td align="left" style="<?php echo $content_style;?><?php echo ($i%2 == 0) ?'background: #f2f2ff;': ''; ?>padding-left: 5px;padding-right: 5px;"><a href="<?php echo $content->post_content;?>" style="text-decoration: none;" title="<?php echo $content->post_content;?>" target="_blank">Xem</a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                    <tr>
                      <td height="25"></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <!--end Article-->

          </table>
        </td>
      </tr>

    </table>
    <!--end 1/2 right-->
  <?php endif; ?>



  <?php if(!empty($tasks)): ?>
    <!--start công việc đã thực hiện task -->
    <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="15"></td>
      </tr>
      <tr>
        <td align="center">
          <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
                    <!--Title-->
                    <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Báo cáo công việc</td>
                    <!--End title-->
                    <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                  </tr>
                </table>
                <!--Space-->
                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                  <tr>
                    <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                      <p style="padding-left: 24px;">&nbsp;</p>
                    </td>
                  </tr>
                </table>
                <!--End Space-->
                <!--detail-->
                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Danh sách công việc đã thực hiện trong trong tuần</td>
                  </tr>
                </table>
                <!--end detail-->
              </td>
            </tr>

            <!--start Article-->
            <tr>
              <td bgcolor="#ffffff">
                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td height="25"></td>
                    </tr>
                    <?php
                    foreach($tasks as $post): 
                      $task_type = get_post_meta_value($post->post_id, 'task_type');
                    // if(get_post_meta_value($post->post_id, 'task_type') == 'task-default');

                    $status = $this->config->item($post->post_status, 'post_status');
                    if(!$status)
                      continue;
                    switch ($post->post_status) {
                      case 'process':
                      $status = 'Đang thực hiện';
                      break;
                      case 'publish':
                      $status = 'Chờ thực hiện';
                      break;

                      default:
                          # code...
                      break;
                    } ?>
                    <tr>
                      <td align="left" style="<?php echo $content_style;?>"><?php echo ucfirst($post->post_title);?></td>

                      <td align="right" style="<?php echo $content_style;?>"><span style="color: #3498db;"><?php echo $status;?></span>
                      </td>
                    </tr>
                    <?php 
                    endforeach; ?>
                    <tr>
                      <td height="25"></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <!--end Article-->
          </table>
        </td>
      </tr>
    </table>
    <!--end công việc đã thực hiện task -->
  <?php endif;?>




  <?php if(!empty($ga['results'])): ?>
    <!--end 2/2 panel full-->
    <!--1/2 right-->
    <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="15"></td>
      </tr>
      <tr>
        <td align="center">
          <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
                    <!--Title-->
                    <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Báo cáo truy cập</td>
                    <!--End title-->
                    <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                  </tr>
                </table>
                <!--Space-->
                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                  <tr>
                    <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                      <p style="padding-left: 24px;">&nbsp;</p>
                    </td>
                  </tr>
                </table>
                <!--End Space-->
                <!--detail-->
                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Báo cáo truy cập theo Google Analytics</td>
                  </tr>
                </table>
                <!--end detail-->
              </td>
            </tr>

            <!--start Article-->
            <tr>
              <td bgcolor="#ffffff">

                <?php 

                $heading = array('Ngày ');
                foreach($ga['total'] as $v=>$r)
                  $heading[] = $this->webgeneral_seotraffic_m->get_ga_label($v);
                $this->table->set_heading($heading);
                $colspan = count($heading);
                $template['table_open'] = '<table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">';
                    // $template['thead_open'] = '<thead><tr><th colspan="'.$colspan.'" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:left; width:100%">Báo cáo truy cập</th></tr>';

                $template['cell_alt_start'] = '<td align="center" style="'.$content_style.'">';
                $template['cell_start'] =  $template['cell_alt_start'];
                $template['heading_row_start'] = '<tr>
                <td height="25"></td>
              </tr><tr>';
                    $template['heading_cell_start'] = '<th align="center" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;">';
                    // $template['row_start'] = '<tr align="center">';
                    // $template['row_alt_start'] = '<tr align="center" bgcolor="#f4f8fb">';

              $this->table->set_template($template);

              $time = time();
              $is_show = false;
              foreach($ga['results'] as $day=>$result) :
                $row = array();
              $day = strtotime($day);
              if($day > $time)
                break;
              $row[] = date('d/m/Y', $day);
              foreach($ga['total'] as $key => $val)
              {
                $row[] = isset($result[$key]) ? $result[$key] : 0;
              }
              $is_show = true;
              $this->table->add_row($row);

              endforeach;
              $row = array('<b>Tổng</b>');
              foreach($ga['total'] as $key => $val)
              {
                $row[] = '<b>'.$val.'</b>';
              }
              $this->table->add_row($row);
              if($is_show)
              {
                echo $this->table->generate().br();
              }


              ?>

            </td>
          </tr>
          <!--end Article-->

        </table>
      </td>
    </tr>

  </table>
  <!--end 1/2 right-->
<?php endif; ?>


<!--báo cáo đầu việc -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="15"></td>
  </tr>
  <tr>
    <td align="center">
      <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
            <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <!--Title-->
                <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Quản trị nội dung & thiết kế</td>
                <!--End title-->
                <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
              </tr>
            </table>
            <!--Space-->
            <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
              <tr>
                <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                  <p style="padding-left: 24px;">&nbsp;</p>
                </td>
              </tr>
            </table>
            <!--End Space-->
            <!--detail-->
            <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Quản trị nội dung & thiết kế Website</td>
              </tr>
            </table>
            <!--end detail-->
          </td>
        </tr>

        <!--start Article-->
        <tr>
          <td bgcolor="#ffffff">
            <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td height="25"></td>
                </tr>
                <?php
                $kpi_content = $this->webgeneral_kpi_m->get_kpi_value($term_id,'content', $start_date);
                $result_kpi_content = $this->webgeneral_kpi_m->get_kpi_result($term_id,'content', $start_date);
                if(!$kpi_content)
                  $kpi_content = 0;

                if(!$result_kpi_content)
                  $result_kpi_content = 0;

                $txt_percent = '';
                if($kpi_content > 0)
                {
                  $txt_percent = ($result_kpi_content / $kpi_content) * 100;
                  $txt_percent = ' (đạt '.numberformat($txt_percent).'%)';
                }

                $num_banner = 0;
                $num_cusomer_task = 0;
                $kpi_banner = $this->webgeneral_config_m->get_service_value($term_id, 'banner', 0);

                

                if($month_tasks)
                 foreach ($month_tasks as $value) {
                  $task_type = get_post_meta_value($value->post_id, 'task_type');
                  if($task_type == 'banner')
                    $num_banner++;
                  if($task_type == 'custom-task')
                    $num_cusomer_task++;
                }

                $txt_banner_percent = '';
                if($kpi_content > 0)
                {
                  $txt_banner_percent = ($num_banner / $kpi_banner) * 100;
                  $txt_banner_percent = ' (đạt '.numberformat($txt_banner_percent).'%)';
                }

                $txt_cusomer_task = $num_cusomer_task.' bài viết';
                if($num_cusomer_task ==0)
                {
                  $txt_cusomer_task = 'Không có yêu cầu';
                }

                $time_range = array('15h55','16h05', '16h10', '16h15', '16h20', '16h25','16h35','16h40','16h45');
                $rand = array_rand($time_range,1);
                $time_check = $time_range[$rand];
                $results = array(
                  array('Cập nhật nội dung, sản phẩm do doanh nghiệp cung cấp', $txt_cusomer_task),
                  array('Biên soạn nội dung tin tức sản phẩm', numberformat($result_kpi_content).'/'.numberformat($kpi_content).' bài viết'.$txt_percent),
                  array('Cập nhật giao diện website theo sự kiện', 'Chưa có sự kiện'),
                  array('Kiểm tra bố cục giao diện website', 'Kiểm tra lúc '.$time_check.' hằng ngày'),
                  array('Thiết kế banner', $num_banner.'/'.$kpi_banner.' banner'.$txt_banner_percent),
                  array('Sửa chữa , vận hành và xử lý các sự cố phát sinh 24/7', 'Kiểm tra hằng ngày'),
                  array('Sao lưu & phục hồi dữ liệu mới nhất khi có sự cố', 'Hằng ngày'),
                  array('Xử lý, khắc phục các sự cố của hosting, domain', 'Hằng ngày'),
                  );
                  foreach($results as $result): ?>
                  <tr>
                    <td align="left" style="<?php echo $content_style;?>"><?php echo $result[0];?></td>

                    <td align="right" style="<?php echo $content_style;?>"><span style="color: #3498db;"><?php echo $result[1];?></span>
                    </td>
                  </tr>
                <?php endforeach; ?>
                <tr>
                  <td height="25"></td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <!--end Article-->

      </table>
    </td>
  </tr>
</table>
<!--end 1/2 right-->
<!--end báo cáo đầu việc -->



<?php 
$default_tasks = array();
if(!empty($all_tasks))
{
  foreach ($all_tasks as $row)
  {
    $task_type = get_post_meta_value($row->post_id, 'task_type');
    if($task_type != 'task-default') continue;

    $default_tasks[] = $row;
  }
}
?>

<?php if (!empty($default_tasks)): ?>
  <!--start tối ưu onpage -->
  <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="15"></td>
    </tr>
    <tr>
      <td align="center">
        <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
              <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                  <!--Title-->
                  <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Tối ưu Onpage Website</td>
                  <!--End title-->
                  <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                </tr>
              </table>
              <!--Space-->
              <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                <tr>
                  <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                    <p style="padding-left: 24px;">&nbsp;</p>
                  </td>
                </tr>
              </table>
              <!--End Space-->
              <!--detail-->
              <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Các đầu việc tối ưu Onpage Website</td>
                </tr>
              </table>
              <!--end detail-->
            </td>
          </tr>

          <!--start Article-->
          <tr>
            <td bgcolor="#ffffff">
              <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                  <tr>
                    <td height="25"></td>
                  </tr>
                  <?php
                  foreach($default_tasks as $post): 

                      $status = $this->config->item($post->post_status, 'post_status');
                      if(!$status)
                        continue;
                      if($post->post_status == 'publish')
                        $status = 'Chờ thực hiện';
                      ?>
                      <tr>
                        <td align="left" style="<?php echo $content_style;?>"><?php echo $post->post_title;?></td>

                        <td align="right" style="<?php echo $content_style;?>"><span style="color: #3498db;"><?php echo $status;?></span>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                      <tr>
                        <td height="25"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <!--end Article-->
            </table>
          </td>
        </tr>
      </table>
      <!--end tối ưu onpage -->
<?php endif; ?>

    <!--nhân viên phụ trách-->
    <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td height="15"></td>
      </tr>
      <tr>
        <td align="center">
          <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody><tr>
              <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tbody><tr>
                    <!--Title-->
                    <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Nhân viên phụ trách</td>
                    <!--End title-->
                    <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                  </tr>
                </tbody></table>
                <!--Space-->
                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                  <tbody><tr>
                    <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                      <p style="padding-left: 24px;">&nbsp;</p>
                    </td>
                  </tr>
                </tbody></table>
                <!--End Space-->
                <!--detail-->
                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tbody><tr>
                    <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Nhân viên thực hiện và phụ trách sản phẩm</td>
                  </tr>
                </tbody></table>
                <!--end detail-->
              </td>
            </tr>
            <tr>
              <td bgcolor="#ffffff">
                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tbody><tr>
                    <td height="25"></td>
                  </tr>
                  <tr>
                    <td>
                      <!-- image -->
                      <table class="table1-3" width="150" border="0" align="left" cellpadding="0" cellspacing="0">
                        <tbody><tr>
                          <td align="center" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" class="img1" src="<?php echo $this->admin_m->get_field_by_id(304,"user_avatar"); ?>" width="150" height="150" alt="img"></td>
                        </tr>
                      </tbody></table>
                      <!-- End image -->
                      <!--Space-->
                      <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                        <tbody><tr>
                          <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                            <p style="padding-left:24px;">&nbsp;</p>
                          </td>
                        </tr>
                      </tbody></table>
                      <!--End Space-->
                      <!--Content-->
                      <table class="table3-1" width="375" border="0" align="right" cellpadding="0" cellspacing="0">
                        <tbody><tr>
                          <td align="left" style="<?php echo $content_style; ?>">
                            <span style="color: #3498db; font-size: 17px;">Thu Phượng</span>
                            <p>Chuyên viên quản trị nội dung và chăm sóc khách hàng</p>
                          <p>Điện thoại: 01662 608 515</p>
                          <p>Email: phuongttt@webdoctor.vn</p>
                          </td>
                        </tr>
                      </tbody></table>
                      <!--End Content-->
                    </td>
                  </tr>
                  <?php
                  $kpis = $this->webgeneral_kpi_m->get_many_by(array(
                    'term_id'=>$term_id,
                    'kpi_type'=> 'tech',
                    )); 
                  if($kpis):
                    foreach($kpis as $kpi):
                      $admin_name = $this->admin_m->get_field_by_id($kpi->user_id,"display_name");
                    $admin_status = $this->admin_m->get_field_by_id($kpi->user_id,"user_status");
                    $admin_avatar = $this->admin_m->get_field_by_id($kpi->user_id,"user_avatar");
                    if($admin_status !=1)
                      continue;

                    $admin_email = $this->admin_m->get_field_by_id($kpi->user_id,"user_email");
                    $admin_phone = $this->usermeta_m->get_meta_value($kpi->user_id,"user_phone");
                    $admin_phone = ($admin_phone) ? $admin_phone : ' (08) 7300. 4488';

                    ?>
                    <tr>
                      <td height="25"></td>
                    </tr>

                    <tr>
                      <td>
                        <!-- image -->
                        <table class="table1-3" width="150" border="0" align="left" cellpadding="0" cellspacing="0">
                          <tbody><tr>
                            <td align="center" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" class="img1" src="<?php echo $admin_avatar; ?>" width="150" height="150" alt="img"></td>
                          </tr>
                        </tbody></table>
                        <!-- End image -->
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                          <tbody><tr>
                            <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                              <p style="padding-left:24px;">&nbsp;</p>
                            </td>
                          </tr>
                        </tbody></table>
                        <!--End Space-->
                        <!--Content-->
                        <table class="table3-1" width="375" border="0" align="right" cellpadding="0" cellspacing="0">
                          <tbody><tr>
                            <td align="left" style="<?php echo $content_style; ?>">

                              <span style="color: #3498db; font-size: 17px;"><?php echo  $admin_name;?></span>
                              <p>Chuyên viên phụ trách kỹ thuật chăm sóc Website</p>
                              <p>Điện thoại: <?php echo $admin_phone; ?></p>
                              <p>Email: <?php echo $admin_email; ?></p>
                            </td>
                          </tr>
                        </tbody></table>
                        <!--End Content-->
                      </td>
                    </tr>
                  <?php endforeach; ?>
                <?php endif; ?>

                <tr>
                  <td height="25"></td>
                </tr>
              </tbody></table>
            </td>
          </tr>
        </tbody></table>
      </td>
    </tr>
    <tr>
      <td height="15"></td>
    </tr>
  </tbody></table>
  <!--end nhân viên phụ trách-->





  <!--end 3/3 panel-->
  <!--cta-->
  <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="15"></td>
    </tr>
    <tr>
      <td align="center" bgcolor="#ffffff" style="border-top:3px solid #3498db;border-bottom:2px solid #e0e0e0;">
        <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="table-inner">
          <tr>
            <td align="center">
              <!-- title -->
              <table class="table-full" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="http://webdoctor.vn/images/quote-left.png" width="25" height="37" alt="img" /></td>
                  <td align="center" bgcolor="#3498db" style="font-family: open sans, arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 25px;padding-right: 25px;"><a style="color: #ffffff;text-decoration: none;" href="#">WEBDOCTOR.VN</a></td>
                  <td><img src="http://webdoctor.vn/images/quote-right.png" width="25" height="37" alt="img" /></td>
                </tr>
              </table>
              <!-- end title -->
            </td>
          </tr>
          <tr>
            <td height="25"></td>
          </tr>
          <!-- headline -->
          <tr>
            <td align="center" valign="top" style="font-family: open sans, arial, sans-serif; font-size:30px; color:#4a4a4a; font-weight:bold;"><a style="color: #4a4a4a;text-decoration: none;" href="#">GIẢI PHÁP WEBDOCTOR.VN</a></td>
          </tr>
          <!-- End headline -->
          <tr>
            <td height="15"></td>
          </tr>
          <!-- Content -->
          <tr>
            <td align="center" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px; line-height:28px; text-align: justify;">
            <table class="table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
                <tbody><tr>
                  <td align="center" style="line-height: 0px;"><a href="http://webdoctor.vn/" target="_blank"><img src="https://webdoctor.vn/images/webdoctor-logo.png"></a></td>
                </tr>
              </tbody></table>
              
              <table class="table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
                <tbody><tr> <td align="center" style="line-height: 0px;">
                <a href="http://adsplus.vn/" target="_blank"><img src="https://webdoctor.vn/images/adsplus-logo.png"></a></td>
                </tr>
              </tbody></table>
            </td>

          </tr>

          <tr>
            <td align="center" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px; line-height:28px; text-align: justify;">
              <p><b>Sứ mệnh của Webdoctor.vn</b><br>
                Là sát cánh với Khách hàng; giúp Khách hàng có một website tốt với chi phí vận hành tiết kiệm; và qua đó giúp Khách hàng phát huy được tối đa hiệu quả kinh doanh.
              </p>
               <p><b>Sứ mệnh của Adsplus.vn</b> <br>
             Google và Facebook lần lượt là hai kênh quảng cáo trực tuyến phổ biến nhất Việt Nam về tính hiệu quả và tỷ lệ quy đổi ra đơn hàng. Adsplus.vn nỗ lực giúp Khách hàng sử dụng hai kênh quảng cáo này tốt nhất; đơn giản nhất; minh bạch nhất và qua đó giúp Khách hàng nâng cao doanh số cũng như phát triển thương hiệu.
              </p>
            </td>
          </tr>
          <!-- End Content -->
          <tr>
            <td height="25"></td>
          </tr>
          <!-- Button -->
          <tr>
            <td align="center">
              <table border="0" cellspacing="0" cellpadding="0" style="border:2px solid #3498db;">
                <tr>
                  <td align="center" height="40" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px;padding-left: 25px;padding-right: 25px;"><a href="http://webdoctor.vn/" target="_blank">Xem chi tiết</a></td>
                </tr>
              </table>
            </td>
          </tr>
          <!--End Button-->
          <tr>
            <td height="35"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td height="15"></td>
    </tr>
  </table>
  <!--end cta-->




  <!--footer-->
  <table width="100%" bgcolor="#ecf0f1" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td height="15"></td>
    </tr>
    <tr>
      <td bgcolor="#34495e" style="border-top:4px solid #3498db;">
        <table class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="35"></td>
          </tr>
          <tr>
            <td>
              <!--note-->
              <table class="table-full" bgcolor="#405366" width="310" border="0" align="right" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="left">
                    <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#34495e">
                      <tr>
                        <!--Title-->
                        <td align="left" bgcolor="#34495e" style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Tư vấn miễn phí</td>
                        <!--End title-->
                        <td width="25" align="left" bgcolor="#405366" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/note-1.png" width="23" height="30" alt="img" /></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td>
                    <table class="table-inner" align="center" width="260" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="footer-link" align="left" bgcolor="#405366" style="font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7; line-height:28px;">
                          <b>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</b><br>
                          <b>Hotline:</b>(08) 7300. 4488</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td height="25" align="right" valign="bottom" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/note-2.png" width="25" height="25" alt="img" /></td>
                  </tr>
                </table>
                <!--note-->
                <!--Space-->
                <table width="1" border="0" cellpadding="0" cellspacing="0" align="right">
                  <tr>
                    <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                      <p style="padding-left:24px;">&nbsp;</p>
                    </td>
                  </tr>
                </table>
                <!--End Space-->
                <!--Preference-->
                <table class="table-full" align="left" width="120" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Thông tin</td>
                  </tr>
                  <tr>
                    <td height="10"></td>
                  </tr>
                  <tr>
                    <td class="footer-link" style="line-height:28px; font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7;">
                      <a href="https://webdoctor.vn/about/" target="_blank">Về chúng tôi</a><br/>
                      <a href="http://webdoctor.vn/bang-gia-dich-vu/" target="_blank">Bảng giá</a>
                      <br/>
                      <a href="https://webdoctor.vn/contact-us/" target="_blank">Liên hệ</a></td>
                    </tr>
                  </table>
                  <!--End Preference-->
                  <!--Space-->
                  <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                    <tr>
                      <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                        <p style="padding-left:24px;">&nbsp;</p>
                      </td>
                    </tr>
                  </table>
                  <!--End Space-->
                  <!--Social-->
                  <table class="table-full" align="left" width="120" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Social</td>
                    </tr>
                    <tr>
                      <td height="10"></td>
                    </tr>
                    <tr>
                      <td class="footer-link" style="line-height:28px; font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7;"><a href="https://www.facebook.com/webdoctor.vn" target="_blank" style="color:#bdc3c7;font-weight: normal;">Facebook</a>
                        <br/>
                        <a href="https://plus.google.com/+SccomVnseo/posts" target="_blank">Google Plus</a>
                        <br/>
                        <a href="#" target="_blank">Youtube</a></td>
                      </tr>
                    </table>
                    <!--End Social-->
                  </td>
                </tr>
                <tr>
                  <td height="35"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#2c3e50">
              <table align="center" width="600" class="table-inner" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="20"></td>
                </tr>
                <tr>
                  <td class="footer-link">
                    <!--copyright-->
                    <table align="left" class="table-full" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-family: 'Open sans', Arial, sans-serif; color:#bdc3c7; font-size:14px;">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không phải reply lại email này. </td>
                      </tr>
                    </table>
                    <!--end copyright-->

                  </td>
                </tr>
                <tr>
                  <td height="20"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <!--end footer-->
      </body>

      </html>