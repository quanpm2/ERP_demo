<tr>
<td align="center" style="padding:0 20px"><p style="font-family:Roboto;color:#ffffff;font-size:30px; font-weight:300; margin:5px 0;  text-transform:uppercase;"><?php echo @$title;?></p>
  </td>
</tr>
<tr>
  <td align="center"><p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng: <strong><?php echo get_term_meta_value($term->term_id,'contract_code');?></strong></p>
    <?php
    $contract_date = my_date($start_date,'d/m/Y').' - '.my_date($end_date,'d/m/Y');
    ?>
    <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">
    Thời gian thực hiện từ:<strong> <?php echo $contract_date;?></strong></p>
    &nbsp;
  </td>
</tr>
<tr>
  <td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;"><p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">Kính chào Quý khách</p>
    <p><strong>WEBDOCTOR.VN</strong> kính gửi quý khách báo cáo  các công việc đã thực hiện từ ngày <strong><?php echo $contract_date;?>.</strong></p>
    <p><?php echo anchor( @$report_link, 'Quý khách vui lòng bấm vào đây để xem chi tiết báo cáo này', 'title="Xem chi tiết báo cáo"  target="_blank"');?></p>
  </td>
</tr>
<tr>
  <td align="center" style="border:20px solid #e6e6e6;">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <tbody>
        <tr>
          <td bgcolor="#fff">
          <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
            <tbody>
              <tr>
                <td bgcolor="#fff"><p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636"><strong>Báo cáo các công việc đã thực hiện</strong></p>
                  <?php
                  $template = array(
                    'table_open'=>'<table border="0" cellspacing="0" cellpadding="5" width="100%" style="font-size:13px">',
                    'row_alt_start' => '<tr bgcolor="#f4f8fb">',
                    'heading_row_start' => '<tr bgcolor="#0074bd" align="left">',
                    'heading_cell_start'  => '<th width="80%" style="color:#fff"><p><strong>',
                    'heading_cell_end'  => '</strong></p></th>'
                    );

                  if(!empty($statistic_data))
                  {
                    $this->table->set_template($template);
                    $this->table->set_heading('Tác vụ','Đã thực hiện');
                    $this->table->add_row('Bài viết',$statistic_data['content_data_result'].'/'.$statistic_data['content_kpi']);
                    $this->table->add_row('Công việc',$statistic_data['task_complete_count'].'/'.$statistic_data['total_task']);
                    $this->table->add_row('Traffic',$statistic_data['ga_data_result'].'/'.$statistic_data['kpi'] .' lượt');
                    echo $this->table->generate().br();
                  }

                  if(!empty($tasks))
                  {
                    $this->table->set_template($template); 
                    $this->table->set_heading('Công việc','Ngày thực hiện');
                    foreach ($tasks as $task){
                      if(valid_url($task->post_content)){
                        $task->post_title = mb_ucfirst($task->post_title,'UTF-8',TRUE);
                        $task->post_title = anchor($task->post_content, $task->post_title,array('title' => $task->post_content,'target' =>'_blank','style'=>'color: black;text-decoration: initial;'));
                      }

                      $this->table->add_row($task->post_title,my_date($task->end_date,'d/m/Y'));
                    }
                    echo $this->table->generate().br();
                  }
                  
                  if(!empty($contents))
                  {
                    $this->table->set_template($template);
                    $this->table->set_heading("Bài viết",'Ngày thực hiện');

                    foreach ($contents as $post){
                      if(valid_url($post->post_content)){
                        $post->post_title = mb_ucfirst($post->post_title,'UTF-8',TRUE);
                        $post->post_title = anchor($post->post_content, $post->post_title,array('title' => $post->post_content,'target' =>'_blank','style'=>'color: black;text-decoration: initial;'));
                      }
                      $this->table->add_row($post->post_title,my_date($post->end_date,'d/m/Y'));
                    }
                    echo $this->table->generate().br();
                  }

                  if(!empty($ga))
                  {
                    $heading = array('Ngày ');
                    foreach($ga['total'] as $v=>$r)
                      $heading[] = $this->webgeneral_seotraffic_m->get_ga_label($v);

                    $colspan = count($heading);
                    $template['thead_open'] = '<thead><tr><th colspan="'.$colspan.'" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:left; width:100%">Báo cáo truy cập</th></tr>';
                    
                    $template['heading_row_start'] = '<tr align="center" >';
                    $template['heading_cell_start'] = '<th align="center" color:"color:black" >';
                    $template['row_start'] = '<tr align="center">';
                    $template['row_alt_start'] = '<tr align="center" bgcolor="#f4f8fb">';

                    $this->table->set_template($template);
                    $this->table->set_heading($heading);

                    $time = time();
                    $is_show = false;
                    foreach($ga['results'] as $day=>$result)
                    {
                      $row = array();
                      $day = strtotime($day);
                      if($day > $time)
                        break;
                      $row[] = date('d/m/Y', $day);
                      foreach($ga['total'] as $key => $val)
                      {
                        $row[] = isset($result[$key]) ? $result[$key] : 0;
                      }
                      $is_show = true;
                      $this->table->add_row($row);
                    }

                    $row = array('<b>Tổng</b>');
                    foreach($ga['total'] as $key => $val)
                    {
                      $row[] = '<b>'.$val.'</b>';
                    }
                    $this->table->add_row($row);
                    if($is_show)
                    {
                      echo $this->table->generate().br();
                    }
                    
                  }

                  if(!empty($backlinks))
                  {
                    $heading = array('Ngày thực hiện','Số lượng backlink','Số lượng từ khóa');

                    $colspan = count($heading);
                    $template['thead_open'] = '<thead><tr><th colspan="'.$colspan.'" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:left; width:100%">Báo cáo backlink đã chạy</th></tr>';
                    $template['heading_row_start'] = '<tr align="center" >';
                    $template['heading_cell_start'] = '<th align="center" color:"color:black">';
                    $template['row_start'] = '<tr align="center">';
                    $template['row_alt_start'] = '<tr align="center" bgcolor="#f4f8fb">';

                    $this->table->set_template($template);
                    $this->table->set_heading($heading);

                    foreach ($backlinks as $bl) 
                      $this->table->add_row($bl);

                    echo $this->table->generate();
                  }
                  ?>
                </td>
              </tr>
            </tbody>
          </table>
          <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">&nbsp;</p>
        </td>
      </tr>
    </tbody>
  </table>
</td>
</tr>
<tr>
  <td bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6;border-right: 20px solid #e6e6e6;  ">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
              <tbody>
               <tr>
                 <td><p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Nhân sự giám sát (<a href="tel:0862736363">HOTLINE: 08.6273.6363</a>)</p>
                  <table width="100%" cellspacing="0" cellpadding="5" border="0" style="font-size:13px;font-family:tahoma;">
                    <tbody>
                      <tr bgcolor="#f4f8fb">
                        <td height="25" width="25%">Phụ trách nội dung</td>
                        <td>: Ms.Hồng<br />
                          - Điện thoại: <strong>0986 202 059<br />
                        </strong>- Email: <a href="mailto:hongvtk@webdoctor.vn">hongvtk@webdoctor.vn</a></td>
                      </tr>
                      <tr>
                        <td height="25">Phụ trách kỹ thuật</td>
                        <td><p>: Mr.Long<br />
                          - Điện thoại: <strong>093 759 7030</strong><br />
                          - Email: <a href="mailto:longnq@webdoctor.vn">longnq@webdoctor.vn</a></p></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                 <td> &nbsp;</td>
               </tr>
               <tr>
                <td bgcolor="#ffffff" style="font-size: 13px;  font-family: Arial, Helvetica, sans-serif;" ><p>Cảm ơn quý khách đã sử dụng dịch vụ của <strong><a href="//webdoctor.vn" target="_blank">WWW.WEBDOCTOR.VN</a></strong>. <br />
                  <em style="color:#999">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không  phải reply lại email này.</em> <br />
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
</td>
</tr>