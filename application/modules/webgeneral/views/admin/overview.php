<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box"> <span class="info-box-icon bg-info"><i class="fa fa-image"></i></span>
        <div class="info-box-content"> 
            <span class="info-box-text">Sản phẩm</span>     
            <span class="info-box-number text-orange">
                <?php echo $content_product_percent ;?><small>%</small>
            </span> 
            <span class="info-box-number text-orange">
                <?php echo $content_product_data_result ;?><small>/<?php echo $content_product_kpi ;?> sản phẩm</small>
            </span> 
        </div>
        <!-- /.info-box-content --> 
    </div>
    <!-- /.info-box --> 
</div>
<!-- /.col -->

<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box"> <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>
        <div class="info-box-content"> 
            <span class="info-box-text">Bài viết</span> 
            <span class="info-box-number text-orange">
                <?php echo $content_percent ;?><small>%</small>
            </span> 
            <span class="info-box-number text-orange">
                <?php echo $content_data_result ;?><small>/<?php echo $content_kpi ;?> bài</small>
            </span> 
        </div>
        <!-- /.info-box-content --> 
    </div>
    <!-- /.info-box --> 
</div>
<!-- /.col --> 

<!-- fix for small devices only -->
<div class="clearfix visible-sm-block"></div>

<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box"> <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
        <div class="info-box-content"> 
            <span class="info-box-text">Task</span> 
            <span class="info-box-number">xong : 
                <span class=""><?php echo $task_complete_count;?><small>/<?php echo $total_task ;?></small></span>
            </span> 
            <span class="info-box-number">Trễ hạn : <?php echo $task_late_count;?></span>  
            <?php
            if(!empty($task_waiting_count))
                echo '<span class="info-box-number">Còn lại : '.$task_waiting_count.'</span>';
            ?>
        </div>
        <!-- /.info-box-content --> 
    </div>
    <!-- /.info-box --> 
</div>
<!-- /.col -->

<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box"> <span class="info-box-icon bg-yellow"><i class="fa fa-google-plus"></i></span>
        <div class="info-box-content"> 
            <span class="info-box-text">Báo cáo</span> 
            <span class="info-box-number">SMS :<small><?php echo $sms_num;?></small></span> 
            <span class="info-box-number">Email :<small><?php echo $email_num;?></small></span> 
        </div>
        <!-- /.info-box-content --> 
    </div>
    <!-- /.info-box --> 
</div>
<!-- /.col -->

<!-- biểu đồ lưu lượng tìm kiếm -->
<div class="col-sm-12" >
    <h4 class="text-center">BIỂU ĐỒ LƯU LƯỢNG TRUY CẬP - LƯỢT TÌM KIẾM</h4>

    <?php if(!empty($charData['kpi']) && !empty($charData['ga:sessions'])) : ?>


        <div class="row">
            <div class="col-sm-9">
                <canvas id="canvas" height="200" width="900"></canvas>
            </div>
            <div class="col-xs-2 col-sm-2" style="margin:50px 0 0 40px">
                <div class="totalvisit">Tổng visit</div>
                <div class="organic">Organic search</div>
                <div class="kpi">KPI</div>
            </div>
            <script async="async" type="text/javascript">

                var lineChartData = {
                    labels : [<?php echo $charDataHeader; ?>],
                    datasets : [
                    {
                        label: "Tổng visit",
                        fillColor : "rgba(220,220,220,0)",
                        strokeColor : "rgba(220,220,220,1)",
                        pointColor : "rgba(220,220,220,1)",
                        pointStrokeColor : "#fff",
                        pointHighlightFill : "#fff",
                        pointHighlightStroke : "rgba(220,220,220,1)",
                        data : [
                        <?php echo $charData['ga:sessions']; ?>

                        ]
                    },
                    {
                        label: "Organic search",
                        fillColor : "rgba(247,70,74,0)",
                        strokeColor : "#FFC870",
                        pointColor : "#FFC870",
                        pointStrokeColor : "#fff",
                        pointHighlightFill : "#fff",
                        pointHighlightStroke : "rgba(247,70,74,1)",
                        data : [<?php echo $charData['Organic Search']; ?>]
                    },
                    {
                        label: "KPI",
                        fillColor : "rgba(247,70,74,0)",
                        strokeColor : "rgba(247,70,74,1)",
                        pointColor : "rgba(247,70,74,1)",
                        pointStrokeColor : "#fff",
                        pointHighlightFill : "#fff",
                        pointHighlightStroke : "rgba(247,70,74,1)",
                        data : [<?php echo $charData['kpi']; ?>]
                    }
                    ]

                }
            </script> 
        </div>


        <script>
            window.onload = function(){

                var ctx2 = document.getElementById("canvas").getContext("2d");
                window.myLine = new Chart(ctx2).Line(lineChartData, {responsive: true});

            }
        </script>
        <style type="text/css">
            .totalvisit::before {
                background: #dcdcdc none repeat scroll 0 0;
                content: "";
                float: left;
                height: 10px;
                margin: 5px 5px 0 0;
                width: 10px;
            }
            .kpi::before {
                background: #F7464A none repeat scroll 0 0;
                content: "";
                float: left;
                height: 10px;
                margin: 5px 5px 0 0;
                width: 10px;
            }
            .organic::before {
                background: #FFC870 none repeat scroll 0 0;
                content: "";
                float: left;
                height: 10px;
                margin: 5px 5px 0 0;
                width: 10px;
            }
        </style>
        <!-- kết thúc biểu đồ lưu lượng tìm kiếm -->

    <?php  else :
    echo 'Chưa cấu hình KPI hoặc add tài khoản GA';

    endif;?>
</div>

<div class="clear10"></div>


<?php 
echo $this->admin_form->box_open('');
echo $customer_table;
echo $this->admin_form->box_close();
?>

<?php

$this->admin_form->set_col(6);
echo $this->admin_form->box_open('Bài viết');
echo $content_table['table'];
echo $this->admin_form->box_close();



$this->admin_form->set_col(6);
echo $this->admin_form->box_open('Sản phẩm');
echo $content_product['content_table']['table'];
echo $this->admin_form->box_close();



$this->admin_form->set_col(6);
echo $this->admin_form->box_open('Task List');
echo $task_table;
echo $this->admin_form->box_close();

$this->admin_form->set_col(6);
echo $this->admin_form->box_open('Email');
echo $email_table;
echo $this->admin_form->box_close();

$this->admin_form->set_col(6);
echo $this->admin_form->box_open('SMS');
echo $sms_table;
echo $this->admin_form->box_close();

?>
<script type="text/javascript">
//notifyMe();
function notifyMe() {
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
}

  // Let's check if the user is okay to get some notification
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    var notification = new Notification("Hi there!");
}

  // Otherwise, we need to ask the user for permission
  // Note, Chrome does not implement the permission static property
  // So we have to check for NOT 'denied' instead of 'default'
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {

      // Whatever the user answers, we make sure we store the information
      if(!('permission' in Notification)) {
        Notification.permission = permission;
    }

      // If the user is okay, let's create a notification
      if (permission === "granted") {
        var notification = new Notification("Hi there!");
    }
});
}

  // At last, if the user already denied any notification, and you 
  // want to be respectful there is no need to bother him any more.
}
</script>