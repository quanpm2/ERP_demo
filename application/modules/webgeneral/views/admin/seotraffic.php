	<div class="col-sm-12" >
	<h4 class="text-center">BIỂU ĐỒ LƯU LƯỢNG TRUY CẬP - LƯỢT TÌM KIẾM</h4>
	<div class="row">
		<div class="col-sm-9">
			<canvas id="canvas" height="200" width="900"></canvas>
		</div>
		<div class="col-xs-2 col-sm-2" style="margin:50px 0 0 40px">
			<div class="totalvisit">Tổng visit</div>
			<div class="organic">Organic search</div>
			<div class="kpi">KPI</div>
		</div>
		<script async="async" type="text/javascript">

			var lineChartData = {
				labels : [<?php echo $charDataHeader; ?>],
				datasets : [
				{
					label: "Tổng visit",
					fillColor : "rgba(220,220,220,0)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [
					<?php echo $charData['ga:sessions']; ?>

					]
				},
				{
					label: "Organic search",
					fillColor : "rgba(247,70,74,0)",
					strokeColor : "#FFC870",
					pointColor : "#FFC870",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(247,70,74,1)",
					data : [<?php echo $charData['Organic Search']; ?>]
				},
				{
					label: "KPI",
					fillColor : "rgba(247,70,74,0)",
					strokeColor : "rgba(247,70,74,1)",
					pointColor : "rgba(247,70,74,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(247,70,74,1)",
					data : [<?php echo $charData['kpi']; ?>]
				}
				]

			}
		</script> 
	</div>
</div>
</div>
<div class="clear10"></div>

<script>
	window.onload = function(){

		var ctx2 = document.getElementById("canvas").getContext("2d");
		window.myLine = new Chart(ctx2).Line(lineChartData, {responsive: true});

	}
</script>
<style type="text/css">
	.totalvisit::before {
		background: #dcdcdc none repeat scroll 0 0;
		content: "";
		float: left;
		height: 10px;
		margin: 5px 5px 0 0;
		width: 10px;
	}
	.kpi::before {
		background: #F7464A none repeat scroll 0 0;
		content: "";
		float: left;
		height: 10px;
		margin: 5px 5px 0 0;
		width: 10px;
	}
	.organic::before {
		background: #FFC870 none repeat scroll 0 0;
		content: "";
		float: left;
		height: 10px;
		margin: 5px 5px 0 0;
		width: 10px;
	}
</style>