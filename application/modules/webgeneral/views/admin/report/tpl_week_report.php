<tr>
<td align="center" style="padding:0 20px"><p style="font-family:Roboto;color:#ffffff;font-size:30px; font-weight:300; margin:5px 0;  text-transform:uppercase;"><?php echo @$title;?></p>
  </td>
</tr>
<tr>
  <td align="center"><p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng: <strong><?php echo get_term_meta_value($term->term_id,'contract_code');?></strong></p>
    <?php
    $start_time = get_term_meta_value($term->term_id,'contract_begin');
    $end_time = get_term_meta_value($term->term_id,'contract_end');
    $contract_date = my_date($start_time,'d/m/Y').' - '.my_date($end_time,'d/m/Y');
    ?>
    <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">
    Thời gian thực hiện từ:<strong> <?php echo $contract_date;?></strong></p>
    &nbsp;
  </td>
</tr>
<tr>
  <td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;"><p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">Kính chào Quý khách</p>
    <p><strong>WEBDOCTOR.VN</strong> kính gửi quý khách báo cáo  các công việc đã thực hiện từ ngày <strong><?php echo $contract_date;?>.</strong></p>
    <p><?php echo anchor( @$report_link, 'Quý khách vui lòng bấm vào đây để xem chi tiết báo cáo này', 'title="Xem chi tiết báo cáo"  target="_blank"');?></p>
  </td>
</tr>
<tr>
  <td align="center" style="border:20px solid #e6e6e6;">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <tbody>
        <tr>
          <td bgcolor="#fff">
          <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
            <tbody>
              <tr>
                <td bgcolor="#fff"><p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636"><strong>Báo cáo bài viết đã thực hiện</strong></p>
                  <?php
                  
                  $template = array(
                    'table_open'=>'<table border="0" cellspacing="0" cellpadding="5" width="100%" style="font-size:13px">',
                    'row_start' => '<tr bgcolor="#0074bd">');

                  $this->add_row(
                    array('data'=>'<p><strong>Bài viết</strong></p>','width'=>'80%','style'=>'color:#fff'),
                    array('data'=>'<p><strong>Cập nhật</strong></p>','style'=>'color:#fff'));

                  echo $this->table->generate();
                  
                  ?>
                  <table border="0" cellspacing="0" cellpadding="5" width="100%" style="font-size:13px">
                    <tr bgcolor="#0074bd">
                      <td width="80%" style="color:#fff"><p><strong>Bài viết</strong></p></td>
                      <td style="color:#fff"><p><strong>Cập nhật</strong></p></td>
                    </tr>
                    <tr>
                      <td><a href="/">Tại sao bạn nên sử dụng sim    Mobifone trả sau?</a></td>
                      <td>14/05/2016</td>
                    </tr>
                    <tr bgcolor="#f4f8fb">
                      <td><a href="/">Tại sao bạn nên sử dụng sim    Mobifone trả sau?</a></td>
                      <td>14/05/2016</td>
                    </tr>
                    <tr>
                      <td><a href="/">Tại sao bạn nên sử dụng sim    Mobifone trả sau?</a></td>
                      <td>14/05/2016</td>
                    </tr>
                    <tr bgcolor="#f4f8fb">
                      <td><a href="/">Tại sao bạn nên sử dụng sim    Mobifone trả sau?</a></td>
                      <td>14/05/2016</td>
                    </tr>
                    <tr>
                      <td><a href="/">Tại sao bạn nên sử dụng sim    Mobifone trả sau?</a></td>
                      <td>14/05/2016</td>
                    </tr>
                    <tr bgcolor="#f4f8fb">
                      <td><a href="/">Tại sao bạn nên sử dụng sim    Mobifone trả sau?</a></td>
                      <td>14/05/2016</td>
                    </tr>
                  </table>
                  <table border="0" cellspacing="0" cellpadding="5" width="100%" style="font-size:13px">
                    <tr bgcolor="#0074bd">
                      <td style="color:#fff" colspan="10"><p><strong>Báo cáo truy cập</strong></p></td>
                    </tr>
                    <tr>
                      <td><p align="center"><strong>Ngày</strong></p></td>
                      <td><p align="center"><strong>Tổng visit</strong></p></td>
                      <td><p align="center"><strong>Bounce Rate</strong></p></td>
                      <td><p align="center"><strong>Time On Site</strong></p></td>
                      <td><p align="center"><strong>Pages / Session</strong></p></td>
                      <td><p align="center"><strong>Nguồn khác</strong></p></td>
                      <td><p align="center"><strong>Direct</strong></p></td>
                      <td><p align="center"><strong>Không xác định</strong></p></td>
                      <td><p align="center"><strong>Organic Search</strong></p></td>
                      <td><p align="center"><strong>Referral</strong></p></td>
                    </tr>
                    <tr bgcolor="#f4f8fb">
                      <td><p align="center">09/05/2016</p></td>
                      <td><p align="center">422</p></td>
                      <td><p align="center">32%</p></td>
                      <td><p align="center">00:06:41</p></td>
                      <td><p align="center">6.39</p></td>
                      <td><p align="center">54</p></td>
                      <td><p align="center">98</p></td>
                      <td><p align="center">38</p></td>
                      <td><p align="center">179</p></td>
                      <td><p align="center">41</p></td>
                    </tr>
                    <tr>
                      <td><p align="center">10/05/2016</p></td>
                      <td><p align="center">485</p></td>
                      <td><p align="center">36%</p></td>
                      <td><p align="center">00:05:49</p></td>
                      <td><p align="center">5.86</p></td>
                      <td><p align="center">98</p></td>
                      <td><p align="center">91</p></td>
                      <td><p align="center">33</p></td>
                      <td><p align="center">183</p></td>
                      <td><p align="center">26</p></td>
                    </tr>
                    <tr bgcolor="#f4f8fb">
                      <td><p align="center">11/05/2016</p></td>
                      <td><p align="center">557</p></td>
                      <td><p align="center">31%</p></td>
                      <td><p align="center">00:04:36</p></td>
                      <td><p align="center">5.26</p></td>
                      <td><p align="center">108</p></td>
                      <td><p align="center">99</p></td>
                      <td><p align="center">39</p></td>
                      <td><p align="center">189</p></td>
                      <td><p align="center">36</p></td>
                    </tr>
                    <tr>
                      <td><p align="center">12/05/2016</p></td>
                      <td><p align="center">671</p></td>
                      <td><p align="center">22%</p></td>
                      <td><p align="center">00:06:20</p></td>
                      <td><p align="center">6.49</p></td>
                      <td><p align="center">86</p></td>
                      <td><p align="center">140</p></td>
                      <td><p align="center">36</p></td>
                      <td><p align="center">175</p></td>
                      <td><p align="center">43</p></td>
                    </tr>
                    <tr bgcolor="#f4f8fb">
                      <td><p align="center">13/05/2016</p></td>
                      <td><p align="center">670</p></td>
                      <td><p align="center">24%</p></td>
                      <td><p align="center">00:06:22</p></td>
                      <td><p align="center">6.54</p></td>
                      <td><p align="center">52</p></td>
                      <td><p align="center">124</p></td>
                      <td><p align="center">32</p></td>
                      <td><p align="center">186</p></td>
                      <td><p align="center">29</p></td>
                    </tr>
                    <tr>
                      <td><p align="center">14/05/2016</p></td>
                      <td><p align="center">622</p></td>
                      <td><p align="center">21%</p></td>
                      <td><p align="center">00:06:40</p></td>
                      <td><p align="center">7</p></td>
                      <td><p align="center">31</p></td>
                      <td><p align="center">171</p></td>
                      <td><p align="center">22</p></td>
                      <td><p align="center">162</p></td>
                      <td><p align="center">41</p></td>
                    </tr>
                    <tr bgcolor="#f4f8fb">
                      <td><p align="center">15/05/2016</p></td>
                      <td><p align="center">569</p></td>
                      <td><p align="center">21%</p></td>
                      <td><p align="center">00:05:48</p></td>
                      <td><p align="center">6.36</p></td>
                      <td><p align="center">19</p></td>
                      <td><p align="center">124</p></td>
                      <td><p align="center">16</p></td>
                      <td><p align="center">172</p></td>
                      <td><p align="center">38</p></td>
                    </tr>
                    <tr>
                      <td><p align="center"><strong>Tổng</strong></p></td>
                      <td><p align="center"><strong>3996</strong></p></td>
                      <td><p align="center"><strong>26%</strong></p></td>
                      <td><p align="center"><strong>00:06:03</strong></p></td>
                      <td><p align="center"><strong>6.3</strong></p></td>
                      <td><p align="center"><strong>448</strong></p></td>
                      <td><p align="center"><strong>847</strong></p></td>
                      <td><p align="center"><strong>216</strong></p></td>
                      <td><p align="center"><strong>1246</strong></p></td>
                      <td><p align="center"><strong>254</strong></p></td>
                    </tr>
                  </table>

                </td>
              </tr>
            </tbody>
          </table>
          <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">&nbsp;</p>
        </td>
      </tr>
    </tbody>
  </table>
</td>
</tr>
<tr>
  <td bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6;border-right: 20px solid #e6e6e6;  ">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
              <tbody>
               <tr>
                 <td><p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Nhân sự giám sát (<a href="tel:0862736363">HOTLINE: 08.6273.6363</a>)</p>
                  <table width="100%" cellspacing="0" cellpadding="5" border="0" style="font-size:13px;font-family:tahoma;">
                    <tbody>
                      <tr bgcolor="#f4f8fb">
                        <td height="25" width="25%">Phụ trách nội dung</td>
                        <td>: Ms.Hồng<br />
                          - Điện thoại: <strong>0986 202 059<br />
                        </strong>- Email: <a href="mailto:hongvtk@webdoctor.vn">hongvtk@webdoctor.vn</a></td>
                      </tr>
                      <tr>
                        <td height="25">Phụ trách kỹ thuật</td>
                        <td><p>: Mr.Long<br />
                          - Điện thoại: <strong>093 759 7030</strong><br />
                          - Email: <a href="mailto:longnq@webdoctor.vn">longnq@webdoctor.vn</a></p></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                 <td> &nbsp;</td>
               </tr>
               <tr>
                <td bgcolor="#ffffff" style="font-size: 13px;  font-family: Arial, Helvetica, sans-serif;" ><p>Cảm ơn quý khách đã sử dụng dịch vụ của <strong><a href="//webdoctor.vn" target="_blank">WWW.WEBDOCTOR.VN</a></strong>. <br />
                  <em style="color:#999">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không  phải reply lại email này.</em> <br />
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
</td>
</tr>