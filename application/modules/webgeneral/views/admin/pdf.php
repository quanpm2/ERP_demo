

<head>

<title>04052016-432-Hợp đồng Web tổng hợp pmac.vn</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<body>

<style>
 
 
body{line-height:1.2em; font-size: 14px; padding: 0 15mm;}
p {margin:10px 0}
.title{background:#eee; font-weight:600; border-top:1px solid #000}
</style>


<style> 
*{ font-family: "dejavu serif condensed";}
body{ font-family: "dejavu serif condensed";}
    *
    {
     margin: 5px;
    }

    body
    {
      font-size:12px;
       margin: 20px;
    }
    table, tr, td
    {
      border-color: grey;
    }
    table td {
    padding-left: 5px;
    padding-right: 5px;
	}

  	html, body {
        height: 100%;
    }

  </style>

<p align="center">
    CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br/>
    <span>Độc lập - Tự do - Hạnh phúc</span><br/>
    <span>--------------oOo--------------</span>
</p>
<p>&nbsp;</p>
<p align="center"><span style="font-size:1.3em; line-height:1.3em">HỢP ĐỒNG CUNG CẤP GIẢI PHÁP PHẦN MỀM ỨNG DỤNG <br />
  <strong>CHĂM SÓC WEBSITE</strong></span><br />
  (WEBDOCTOR/432/0516/PMAC.VN)</p>
<p align="center">&nbsp;</p>
<p>Hôm nay, ngày 04 tháng 05 năm 2016 chúng tôi gồm:</p>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td width="25%"><strong>Bên thuê dịch vụ</strong></td>
    <td>: <strong>CÔNG TY TNHH KIẾN TRÚC PHÚ MỸ</strong></td>
  </tr>
  <tr>
    <td>Địa chỉ</td>
    <td>:  Lầu 6, 155 Hai Bà Trưng, Phường 6, Quận 3, TP HCM</td>
  </tr>
  <tr>
    <td>Điện thoại</td>
    <td>:  0903771482</td>
  </tr>
   
   
  
  

  <tr>
    <td>Đại diện</td>
    <td>:  
    Ông LÊ VĂN LUẬN     </td>
  </tr>
    </tr>
  <tr>
    <td colspan="2"><em>(Sau đây gọi là Bên A)</em></td>
  </tr>
  <tr>
    <td><strong>Bên cung ứng dịch vụ </strong></td>
    <td>: <strong>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</strong></td>
  </tr>
  <tr>
    <td>Địa  chỉ </td>
    <td>: Tầng 8, 402 Nguyễn Thị Minh Khai , Phường 5, Quận 3, TP.HCM, Việt Nam</td>
  </tr>
  <tr>
    <td>Điện  thoại</td>
    <td>:  (08) 66852 888</td>
  </tr>
  <tr>
    <td>Mã  số thuế </td>
    <td>:  0313547231</td>
  </tr>
  <tr>
    <td>Đại  diện</td>
    <td>:  Ông Nguyễn Nhã Luân</td>
  </tr>
  <tr>
    <td>Chức  vụ</td>
    <td>:  Giám Đốc khối doanh nghiệp</td>
  </tr>
  <tr>
    <td colspan="2"><em>(Sau  đây gọi là Bên B)</em></td>
  </tr>
</table>

 

<p><strong>Dựa trên: </strong><br />
  Nhu cầu của  Bên A và khả năng cung cấp của Bên B về dịch vụ. Hai bên đồng ý ký kết Hợp Đồng  này với các điều khoản sau:</p>

<p> <strong>ĐIỀU  1: DỊCH VỤ ĐĂNG KÝ</strong></p>
<table border="1" cellspacing="0" cellpadding="3" width="100%">
  <tr>
    <th width="25%" nowrap="nowrap"><strong align="center">Dịch vụ</strong></th>
    <th  ><strong>Đơn giá </strong><br />
        (vnđ/tháng)</th>
    <th width="35%" nowrap="nowrap"><strong>Thời gian</strong></th>
    <th width="25%" nowrap="nowrap"><strong>Thành tiền (vnđ)</strong></th>
  </tr>
  <tr><td>Chăm sóc website (*)<br/><em>pmac.vn</em></td><td>500.000 đồng <br/><em>gói bắt đầu</em></td><td>3 tháng <br />(từ 01/04/2016 - 01/07/2016)</td><td>1.500.000 đồng</td></tr>  <tr>
    <td colspan="3">Tổng cộng</td>
    <td><strong>1.500.000 đồng</strong></td>
  </tr>
  </table>
<p><em>Bằng chữ: Một triệu năm trăm ngàn đồng.</em><br />
<em>(*) Chi tiết các hạng mục dịch vụ, khách hàng xem ở Phụ lục đính kèm</em></p>
<p> <strong>ĐIỀU 2: PHƯƠNG THỨC THANH TOÁN</strong><br />
  2.1  Bên A thanh toán cho Bên B thành  đợt như sau:<br />

  
    <u>Đợt 1</u>: Bên A  thanh toán cho Bên B 500.000 đồng sau khi ký và trước khi thực hiện hợp đồng, 
    chậm nhất trước ngày 01/04/2016.<br />
    <u>Các đợt tiếp theo</u>:  Bên A thanh toán cho Bên B 500.000 đồng định kỳ, 
    chậm nhất trước ngày 01 hàng tháng.</p>
<p>2.2  Thanh toán bằng hình thức chuyển khoản<br />


<table width="80%" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr>
    <td width="30%">+  Chủ tài khoản</td>
    <td>: NGUYỄN THỊ DIỄM THÚY</td>
  </tr>
  <tr>
    <td>+ Số tài khoản </td>
    <td>: 19023393899019</td>
  </tr>
  <tr>
    <td>+  Mở tại Ngân hàng</td>
    <td>
: TECHCOMBANK -  Phòng giao dịch văn thánh  </td>
  </tr>
</table>

<p> <strong>ĐIỀU 3: TRÁCH NHIỆM CỦA BÊN A</strong><br />
  3.1 Cung cấp đầy đủ các  thông tin để Bên B tiến hành khởi tạo dịch vụ cho Bên A </p>
<p> 3.2 Chịu trách nhiệm trước  pháp luật về các sản phẩm, dịch vụ, thông tin,…mà Bên B cung cấp thông qua  website được nêu ra trong Điều 1 của hợp đồng này. </p>
<p> 3.3 Thanh toán đầy đủ các khoản chi phí cho Bên B như đã nêu tại Điều 2 của hợp đồng này. </p>
<p> 3.4 Bên A có quyền yêu cầu  Bên B hỗ trợ điều chỉnh lại bài viết nếu bài viết chưa đạt yêu cầu dựa trên bài  đã viết và dựa trên thông tin Bên A cung cấp. </p>
<p> 3.5.  Bên A có trách nhiệm duyệt và chấp thuận về nội dung bài viết, thông qua email  hoặc văn bản, để Bên B đăng tải lên website.<strong></strong></p>
<p><strong>ĐIỀU 4: TRÁCH NHIỆM CỦA BÊN B</strong><br />
  4.1. Hoàn thành việc khởi  tạo dịch vụ theo Điều 1 về chi tiết dịch vụ kể từ khi nhận được đầy đủ thông  tin và tiền thanh toán của Bên A. </p>
<p> 4.2. Điều chỉnh dịch vụ  cho Bên A, trong phạm vi thông số dịch vụ, nếu được Bên A yêu cầu điều chỉnh hoặc  được Bên A chấp nhận đề nghị điều chỉnh của Bên B. Các &ldquo;yêu cầu điều chỉnh&rdquo; và  &ldquo;đề nghị điều chỉnh&rdquo; sẽ được gởi và xác nhận bằng văn bản hoặc email của 2 bên. </p>
<p> 4.3.  Bên B không chịu trách nhiệm pháp lý và bồi thường cho Bên A và bên thứ ba đối  với các thiệt hại trực tiếp, gián tiếp, vô ý, đặc biệt, vô hình, các thiệt hại  về lợi nhuận, doanh thu, uy tín phát sinh từ việc sử dụng sản phẩm, dịch vụ của  Bên A.</p>
<p> <strong>ĐIỀU 5: BẢO MẬT VÀ CÔNG BỐ THÔNG TIN</strong><br />
  5.1 Mỗi  bên sẽ bảo mật và sẽ không tiết lộ cho các bên thứ ba trong suốt thời hạn hiệu  lực của hợp đồng này hoặc sau đó, toàn bộ hay một phần nội dung của hợp đồng  này và bất kỳ thông tin nào do một trong hai bên trao đổi bằng văn bản, lời nói  hay bằng bất kỳ hình thức nào ngoại trừ các thông tin được công bố theo yêu cầu  của các cơ quan quản lý chuyên ngành, cơ quan chủ quản và các cơ quan pháp luật.</p>
<p> 5.2  Hai Bên đảm bảo không tiết lộ các thông tin liên quan đến các tài khoản được dùng để Bên B thực hiện dịch vụ cho Bên A, các tài khoản được liệt  kê bao gồm nhưng không giới hạn sau đây: tài khoản quản trị website, tài khoản  quản trị hosting, mã nguồn website.</p>
<p> <strong>ĐIỀU 6: THANH LÝ HỢP ĐỒNG </strong><br />
  Hai bên tiến hành thanh lý  hợp đồng trong các trường hợp sau: <br />
  6.1 Một trong hai bên vi  phạm các điều khoản trong hợp đồng này. </p>

<p> 6.2 Kể từ ngày hết hạn dịch  vụ thì hợp đồng mặc nhiên được thanh lý. </p>
<p> 6.3 Hợp đồng kinh tế bị  đình chỉ, hủy bỏ. </p>
<p> 6.4 Pháp nhân giải thể.</p>


<p> <strong>ĐIỀU 7: ĐIỀU KHOẢN THI HÀNH</strong> <br />
  7.1&nbsp;&nbsp;&nbsp;&nbsp; Hai bên  cam kết thực hiện đúng các điều khoản của hợp đồng, bên nào vi phạm sẽ phải chịu  trách nhiệm theo quy định của pháp luật.</p>
<p> 7.2&nbsp;&nbsp;&nbsp;&nbsp; Trong quá  trình thực hiện, nếu có vướng mắc gì thì 2 bên chủ động thương lượng giải quyết  trên tinh thần hợp tác, tôn trọng lẫn nhau. Nếu hai bên không tự giải quyết được  sẽ thống nhất chuyển vụ việc tới Toà án kinh tế có thẩm quyền để giải quyết.</p>
<p> 7.3&nbsp;&nbsp;&nbsp;&nbsp; Hợp đồng  này có hiệu lực kể từ ngày ký cho đến hết thời hạn đăng ký tại Điều 1. Khi hợp  đồng hết hiệu lực, nếu hai bên tiếp tục gia hạn hợp đồng, hai bên sẽ ký kết phụ  lục gia hạn hợp đồng theo bảng báo giá của thời điểm ký phụ lục. Hợp đồng này  được lập thành 02 bản có giá trị như nhau: Bên A giữ 01 bản, Bên B giữ 01 bản.</p>
<p style="text-align:right"> 
<em>Tp HCM, ngày 04 tháng 05 năm 2016</em>
</p>
<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
  <tr>
    <td width="40%" valign="top" style="text-align:center"><p><strong>BÊN A</strong><br />
      <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
      <strong>CÔNG TY TNHH KIẾN TRÚC PHÚ MỸ</strong></p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p></td>
  	<td rowspan="2"></td>
  
    <td width="50%" valign="top" style="text-align:center"><p><strong>BÊN B</strong><br />
      <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
      <strong>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</strong></p>
      <p><strong>&nbsp;</strong></p>
      <p><strong>&nbsp;</strong></p>
      <p>&nbsp;</p></td>
  </tr>
  
  <tr>
    <td valign="top" style="text-align:center">
      <p><strong>Ông LÊ VĂN LUẬN </strong><br />
    </p></td>
    <td valign="top" style="text-align:center">
      <p><strong>Nguyễn Nhã Luân</strong><br />
    Giám Đốc khối doanh nghiệp</p></td>
  </tr>
</table>
<p>&nbsp; </p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>