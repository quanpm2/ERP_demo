<?php
$heading = array();
$heading[] = 'STT';
$heading[] = 'Từ khóa';
$heading[] = 'Vị trí';
// $heading = array_merge($heading, '');

$this->table->set_heading($heading);
$t = 0;


for($i=0; $i< $kpi; $i++)
{
	if($t >50) break;
	$t++;
	if(!isset($keywords[$i]))
	{
		$keywords[$i] = new stdClass();
		$keywords[$i]->kpi_name = '';
		$keywords[$i]->kpi_id = '';
	}

	$keyword = $keywords[$i]->kpi_name;
	$date = date('Y-m-01', $time_start);
	$cdate = date('m/Y', $time_start);
	$row = array($i);
	$kp_id = $keywords[$i]->kpi_id;
	$row[] = '<a href="#" class="editable" data-type="text" data-pk="'.$kp_id.'" data-placement="right" data-kpitype="keyword" data-placeholder="Required" data-title="Từ khóa">'.$keyword.'</a>';

	$val = $this->webgeneral_kpi_m->get_kpi_value($term_id, '', $time_start);
	$val = ($val == 0) ? '' : $val;
	$text = $val;
	if($text == '')
		$text = '<a href="#" class="editable" data-type="text" data-pk="'.$kp_id.'" data-placement="right" data-kpitype="position" data-placeholder="Required" data-title="Vị trí từ khóa của tuần" trong tháng.">'.$val.'</a>';
	$row[] = $text;


	// foreach($dates as $type_key=>$type)
	// {
	// 	$val = $this->webgeneral_kpi_m->get_kpi_value($term_id, $type_key, $time_start);
	// 	$val = ($val == 0) ? '' : $val;
	// 	$text = $val;
	// 	if($text == '')
	// 		$text = '<a href="#" class="editable" data-type="text" data-pk="'.$date.'" data-placement="right" data-kpitype="'.$type_key.'" data-placeholder="Required" data-title="Vị trí từ khóa của tuần '.$type.'" trong tháng.">'.$val.'</a>';
	// 	$row[] = $text;
	// }
	$this->table->add_row($row);
	$time_start = strtotime('+1 month', strtotime($date));
	
}
echo $this->table->generate();
?>


<script type="text/javascript">
	$(function(){
		$.fn.editable.defaults.url = '<?php echo $module_url;?>ajax/kpi/<?php echo $website_id;?>'; 
		$('.editable').editable({
			type: 'text',
			name: 'change-kpi',
			validate: function(value) {
				if($.isNumeric(value) != true) {
					// return 'Vui lòng nhập KPI là số';
				}
				if(value < 0) {
					// return 'Vui lòng nhập KPI có giá trị > 0';
				}
			},
			params: function(params) {
				params.type = $(this).data('kpitype');
				return params;
			},
			success: function(response, newValue) {
				if(!response.success) return response.msg;
			},
			defaultValue: 0
		});
	});
</script>