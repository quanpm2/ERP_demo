<?php
$services_pricetag = get_term_meta_value($term_id,'services_pricetag');
$services_pricetag = @unserialize($services_pricetag);
if(!is_service_end($term))
{
  $this->admin_form->set_col(12,6);
  echo $this->admin_form->box_open();
  echo $this->admin_form->form_open('', 'id="start_process_submit"');
  // echo form_button(['id'=>'start_process','name'=>'start_process','type'=>'submit','class'=>'btn btn-danger','value'=>'start_process'],'<i class="fa fa-fw fa-play"></i>Thực hiện chăm sóc website');
  $btn_submit_proc = array('id'=>'start_process', 'name'=>'start_process', 'type'=>'submit', 'class'=>'btn btn-danger', 'value'=>'start_process','data-toggle'=>'confirmation');
  $btn_submit_text = '';
  
  $is_design = (!empty($services_pricetag['webdesign']['is_design']));

  if($is_design && empty($services_pricetag['webdesign']['start_time']))
  {
    $btn_submit_proc['class'] = 'btn btn-default';
    $btn_submit_text = '<i class="fa fa-fw fa-play"></i>Thực hiện thiết kế';
  } 
  else if($is_design&& empty($services_pricetag['webdesign']['end_time']))
  {
    $btn_submit_proc['class'] = 'btn btn-primary';
    $btn_submit_text = '<i class="fa fa-fw fa-stop"></i>Hoàn tất thiết kế';
  }
  else if(!is_service_proc($term))
  {
    $btn_submit_proc['class'] = 'btn btn-danger';
    $btn_submit_text = '<i class="fa fa-fw fa-play"></i>Thực hiện chăm sóc website';
  }
  if($btn_submit_text) echo form_button($btn_submit_proc, $btn_submit_text);
  
  echo form_button(array('id'=>'end_process','name'=>'end_process','type'=>'submit','class'=>'btn btn-danger','value'=>'end_process','data-toggle'=>'confirmation'),'<i class="fa fa-fw fa-stop"></i>Kết thúc hợp đồng');
  echo '<p class="help-block">Vui lòng cấu hình SMS, cấu hình Email và thông số để hệ thống gửi Mail kích hoạt và Mail thông báo kết nối khách hàng </p>' ;
  echo $this->admin_form->box_close();
  echo $this->admin_form->form_close();
}

$this->admin_form->set_col(6,6);
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Cấu hình GA');

echo $this->admin_form->dropdown('Loại','meta[seotraffic_type]', array( 'ga:sessions' => 'Tổng visit', 'Organic Search'=>'Organic Search'), $this->termmeta_m->get_meta_value($term_id,'seotraffic_type'));

echo $this->admin_form->dropdown('GA Table ID','meta[ga_profile_id]', $ga_accounts, $this->termmeta_m->get_meta($term_id,'ga_profile_id',TRUE, TRUE),'Add mail này vào GA của khách: <b>google-analytics@erp-webdoctor.iam.gserviceaccount.com</b>',array('addon_end'=>'<span class="btn btn-xs btn-danger" id="btn-ajax-reload"><i class="fa fa-refresh"></i></span>'));

echo $this->admin_form->input('Report Key','meta[report_key]',$this->termmeta_m->get_meta_value($term_id,'report_key'),'Key này dùng để tạo báo cáo, nếu thay đổi báo cáo cũ sẽ bị lỗi khi xem');
echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();

//sms setting
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Cấu hình SMS');

echo $this->admin_form->input('Số điện thoại','meta[phone_report]', @$meta['phone_report'], 'Mỗi số điện thoại cách nhau bởi dấu ,', array('id'=>'phone_report'));
echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();

//email setting
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Cấu hình Email');

echo $this->admin_form->input('Email','meta[mail_report]', @$meta['mail_report'], 'Mỗi mail cách nhau bởi dấu ,', array('id'=>'mail_report'));
echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();

// Thông tin truy cập website
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Thông tin truy cập admin website');

echo $this->admin_form->input('Tài khoản','meta[login_url]', get_term_meta_value($term_id,'login_url'));
echo $this->admin_form->input('Tài khoản','meta[login_account]', get_term_meta_value($term_id,'login_account'));  
echo $this->admin_form->input('Mật Khẩu','meta[login_password]', get_term_meta_value($term_id,'login_password'));  

echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();

if($this->admin_m->role_id == 1)
{
  // Thông tin FTP
  echo $this->admin_form->form_open();
  echo $this->admin_form->box_open('Thông tin FTP');

  echo $this->admin_form->input('Address or URL','meta[ftp_address]', get_term_meta_value($term_id,'ftp_address'));  
  echo $this->admin_form->input('Port','meta[ftp_port]', get_term_meta_value($term_id,'ftp_port'));  
  echo $this->admin_form->input('Username Name','meta[ftp_username]', get_term_meta_value($term_id,'ftp_username'));  
  echo $this->admin_form->input('Password','meta[ftp_password]', get_term_meta_value($term_id,'ftp_password'));  

  echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
  echo $this->admin_form->form_close();


  // Thông tin truy cập Domain
  echo $this->admin_form->form_open();
  echo $this->admin_form->box_open('Thông tin truy cập Domain');

  echo $this->admin_form->input('Tài khoản','meta[domain_url]', get_term_meta_value($term_id,'domain_url'));  
  echo $this->admin_form->input('Tài khoản','meta[domain_account]', get_term_meta_value($term_id,'domain_account'));  
  echo $this->admin_form->input('Mật Khẩu','meta[domain_password]', get_term_meta_value($term_id,'domain_password'));  

  echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
  echo $this->admin_form->form_close();
}

// Thông tin FTP
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Ghi chú');
echo 
'<div class="form-group">',
  form_textarea(array('name'=>'meta[term_note]','class'=>'form-control'), get_term_meta_value($term_id,'term_note'),'rows="3" placeholder="Enter ..."'),
'</div>';

echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();



// Thông tin FTP
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Thông tin WebService');

echo $this->admin_form->input('Service URL','meta[wp_service_url]', get_term_meta_value($term_id,'wp_service_url'));  
echo $this->admin_form->input('Username','meta[wp_user_id]', get_term_meta_value($term_id,'wp_user_id'),'',array('addon_end'=>'<span class="btn btn-xs btn-danger" id="btn-ajax-checkservice"><i class="fa fa-refresh"></i></span>'));  


echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();

?>

<script type="text/javascript">
  $(function(){
    $('[data-toggle=confirmation]').confirmation();
    $('#btn-ajax-reload').click(function(){
      $.ajax({url: '<?php echo module_url();?>ajax/setting/ga-reload',dataType: 'json', success: function(result){
        $('form > .form-group  select').eq(1).select2({
         allowClear: true,
         data: result
       });

        $('form > .form-group .select2-container').eq(1).css('width','100%');
      }});
    });


    $('#btn-ajax-checkservice').click(function(){
      $.ajax({url: '<?php echo module_url();?>ajax/setting/ga-reload',dataType: 'json', success: function(result){
         
alert('12312');
        // $('form > .form-group .select2-container').eq(1).css('width','100%');
      }});
    });


    // To make Pace works on Ajax calls
    $(document).ajaxStart(function() { Pace.restart(); });
    $('#start_process').click(function(){
      $(this).hide();
      $('#start_process_submit').prepend('<p class="help-block label label-info">Hệ thống đang gửi mail, vui lòng đợi trong giây lát.</p>');
        return true;
    });

  });
</script>