<?php if (has_permission('Webgeneral.Task.Add')): ?>
<button type="button" id="add_task" name="add_new" class="btn btn-info btn-add-new" data-toggle="modal" data-target="#myModal" onclick="javascript:set_default_form();"><i class="glyphicon glyphicon-plus"></i>  Thêm mới</button> 
<?php endif ?>
<br/>
<br/>
<?php echo $content['table'];?>
<?php echo $content['pagination'];?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Thêm mới</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<?php  echo $this->admin_form->form_open();
					echo $this->admin_form->hidden('','taskid',0,'', array('id'=>'taskid')); 
					echo $this->admin_form->input('Tiêu đề','title', '', '', array('id'=>'title'));
					echo $this->admin_form->input('Nội dung','content', '', '', array('id'=>'content'));

					echo $this->admin_form->dropdown('Loại công việc', 'type', $this->config->item('tasks_type'), 'custom-task','', array('id'=>'type'));

					echo $this->admin_form->input('Ngày bắt đầu','start_date',my_date(),'', array('class'=>'set-datepicker','id'=>'start_date')); 
					echo $this->admin_form->input('Ngày kết thúc','end_date',my_date(),'', array('class'=>'set-datepicker','id'=>'end_date')); 
					// echo $this->admin_form->input('Tiến độ','done_ratio', '', '', array('id'=>'done_ratio', 'addon_end' => '%'));

					?>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
				<?php echo $this->admin_form->form_close();?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$("select[name='type']").change();
	$('#start_date,#end_date').datepicker({
		format: 'yyyy/mm/dd',
		todayHighlight: true,
		autoclose: true,
  // startDate: '-0d'
});
	$(".datepicker").css("z-index", 9999);

	$('.ajax_edit').click(function(ev){
		var id = $(this).data('taskid');
		var jqxhr = $.getJSON( "<?php echo module_url('tasks/ajax_edit/');?>"+id+'/task', function(data) {
			$('input#taskid').val(data.id);
			$('input#title').val(data.title);
			$('input#content').val(data.content);
			$('input#start_date').val(data.start_date);
			$('input#end_date').val(data.end_date);
			$('select#type').val(data.type).change();
			$('input#done_ratio').val(data.done_ratio);
		})
		.done(function() {
			$('#myModal').modal({
				show: 'true'
			}); 
		})
		.fail(function() {
			$.notify("Có lỗi xảy ra, không kết nối được dữ liệu", "error");
		});
		ev.preventDefault();
		return false;
	});

	$('.ajax_delete').click(function(ev){
		var id = $(this).data('taskid');
		var me = $(this);
		$(me).parents('tr').css('background-color','#FFEB3B');
		var jqxhr = $.get( "<?php echo module_url('ajax/post_delete/task/');?>"+id, function(data) {
			if(data =='OK')
			{
				$(me).parents('tr').fadeOut();
			}
			else
			{
				$(me).parents('tr').css('background-color','#FFF');
				$.notify(data, "error");
			}
		});
		ev.preventDefault();
		return false;
	});

	function set_default_form(){
		$('input#title').val('');
		$('input#taskid').val(0);
		$('input#content').val('');
		$('input#start_date').val('<?php echo my_date();?>');
		$('input#end_date').val('<?php echo my_date();?>');
       // $('input#end_date').val(data.end_date);
       $('select#type').val('post-optimize');
       $('input#done_ratio').val(100);
   };


   //iCheck for checkbox and radio inputs
   $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
   	checkboxClass: 'icheckbox_minimal-blue',
   	radioClass: 'iradio_minimal-blue'
   });


   $('input').on('ifToggled', function(event){
   	var id = $(this).data('taskid');
   	var status = $(this).data('status');
   	var me = $(this);
   	// $(this).attr('disabled','disabled');
   	$(this).parents('.checkbox label').css('font-weight','900');
   	var jqxhr = $.get( "<?php echo module_url('tasks/update_status/');?>"+id+'/'+status, function(data) {
   		$.notify("Cập nhật thành công", "success");
   	});

   	return true;
   });
      //Date range picker
      // $('#reservation').daterangepicker();
  </script>
  <style type="text/css">
  	.checkbox, .radio {
  		display: initial;
  	}
  </style>