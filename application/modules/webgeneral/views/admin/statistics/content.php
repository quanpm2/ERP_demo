<?php echo $this->admin_form->form_open(); ?>
<div class="form-group col-md-3">
  <label>Năm / Tháng / Ngày:</label>
  <div class="input-group date">
    <div class="input-group-addon">
      <i class="fa fa-calendar"></i>
    </div>
    <input type="text" name="time" class="form-control pull-right" id="datepicker" value="<?php echo my_date($time); ?>"/>
  </div>
  <!-- /.input group -->
</div>
<?php echo $this->admin_form->form_close(); ?>
<?php echo $content ?? ''; ?>
<script type="text/javascript">
$(function(){

  $('#datepicker').datepicker({
    format: "yyyy/mm/dd",
  })
  .on('changeDate',function(ev){
    $(this).closest('form').submit();
  });
})
</script>