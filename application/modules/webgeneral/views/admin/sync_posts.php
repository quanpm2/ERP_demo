<?php

echo $content; ?>



<script type="text/javascript">

	$(document).ajaxStart(function() { Pace.restart(); });

	$('.approval-post').click(function(ev){
		var id = $(this).data('id');
		var type = $(this).data('type');

		var me = $(this);
		$(me).parents('tr').css('background-color','#FFEB3B');

		var term_id = "<?php echo $term_id;?>";
		$.ajax({url: '<?php echo module_url();?>wordpress/approval/'+id+'/'+term_id+'/'+type,dataType: 'json', success: function(result){
			if(result.success =='OK')
			{
				$.notify('Duyệt thành công', "success");
				$(me).parents('tr').fadeOut();
			}
			else
			{
				$(me).parents('tr').css('background-color','#FFF');
				$.notify(result.success, "error");
			}
			ev.preventDefault();
    }});
	});

</script>