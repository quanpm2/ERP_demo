<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wp_service
{
	protected $ci;
	private $serviceUrl;
	private $term_id;

	public function __construct()
	{
		$this->ci =& get_instance();
	}

	public function setServiceUrl($serviceUrl = '')
	{
		$this->serviceUrl = $serviceUrl;
		return $this;
	}
	
	public function getServiceUrl()
	{
		return $this->serviceUrl;
	}
	
	public function connect($serviceUrl = '', $term_id = 0)
	{
		if($serviceUrl != '')
		{
			$this->setServiceUrl($serviceUrl);
		}
		$this->term_id = $term_id;
		$status = $this->curl_get($this->serviceUrl.'?wdt_login=1');
		if($status && $status == 'Webdoctor')
		{
			return $this;
		}
		return false;
	}

	public function listPosts($user_id = 0, $query = array())
	{
		$default = array('meta_key'=>'wdt_id','post_type' => 'post');
		$query = array_merge($default,$query);
		if($user_id >0)
		{
			$query['meta_value'] = $user_id;
		}
		$posts = $this->getData('get_posts', $query);
		
		return $posts;
	}
	
	public function getData()
	{
		$data = array();
		$args = func_get_args();
		$function = array_shift($args);

		$data['function'] = $function;
		$data['args'] = serialize($args);
		$r = $this->curl_post($this->serviceUrl.'?callback',$data);
		return json_decode($r);
	}
	public function curl_post($url, $data = array())
	{
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($data));
		curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

	public function curl_get($url)
	{
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
}

/* End of file Wp_service.php */
/* Location: ./application/modules/webgeneral/libraries/Wp_service.php */
