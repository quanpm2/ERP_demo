<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webgeneral_contract_m extends Base_contract_m {

	function __construct() 
	{
		// Modules::load('webgeneral');
		parent::__construct();
		$this->load->model('webgeneral/webgeneral_m');
		$this->load->model('webgeneral/webgeneral_config_m');
		$this->load->config('webgeneral/webgeneral');
		$this->load->model('webgeneral/webgeneral_report_m');
	}

	function start_service($term = null)
	{
		// Modules::load('webgeneral');
		if(empty($term) )
			return false;

		// $term = $this->term_m->get($term_id);
		if(!$term || $term->term_type !='webgeneral')
			return false;
		$term_id = $term->term_id;

		$term_meta = 'service_package';
		$package = $this->termmeta_m->get_meta_value($term_id, $term_meta);
		if(!$package)
		{
			//nếu không tồn tại sẽ set mặc định là gói 1
			$package = 'basic';
			$this->termmeta_m->update_meta($term_id, $term_meta, $package);
		}

		if($package != 'startup')
		{
			//init KPI
			$this->init_kpi_default($term_id, $package);

			//init Setting
			$this->init_setting_default($term_id);
		}

		$this->send_mail2admin($term_id);
		update_term_meta($term_id, 'start_service_time',0);

		return TRUE;
	}

	public function stop_contract($term)
	{
		$time = time();
		update_term_meta($term->term_id,'end_contract_time',$time);
		$this->log_m->insert(array(
			'log_type' =>'end_contract_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => my_date($time,'Y/m/d H:i:s')
		));
		$this->stop_service($term);
		$this->load->model('webgeneral/webgeneral_report_m');
		return $this->webgeneral_report_m->send_finish_mail2admin($term->term_id);
	}

	public function stop_service($term)
	{
		$time = time();
		$this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');

		$end_service_time = get_term_meta_value($term_id, 'end_service_time');
		if(!$end_service_time) update_term_meta($term->term_id,'end_service_time',$time);
		
		$this->contract_m->update($term->term_id, array('term_status'=>'ending'));

		//gửi mail kết thúc hợp đồng
		$this->mail_end_contract($term->term_id);

		$this->log_m->insert(array(
			'log_type' =>'end_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => date('Y/m/d H:i:s',$time)
			));
		return TRUE;
	}

	function proc_service($term = FALSE)
	{
		if(!$term || $term->term_type !='webgeneral') return FALSE;

		$term_id = $term->term_id;
		$package = $this->termmeta_m->get_meta_value($term->term_id, 'service_package');

		$is_service_start = get_term_meta_value($term->term_id,'start_service_time');

		$is_design = $this->proc_service_design($term->term_id);
		if($is_design == 1)
		{
			$this->init_job_default($term->term_id, strtotime('+1 day'), $package);

			$this->load->model('webgeneral/webgeneral_report_m');
			$this->webgeneral_report_m->send_activation_mail2customer($term_id);
			$this->webgeneral_report_m->send_tasklist_mail2customer($term_id);

			$this->log_m->insert(array(
				'log_type' =>'start_service_time',
				'user_id' => $this->admin_m->id,
				'term_id' => $term->term_id,
				'log_content' => date('Y/m/d H:i:s').' - Send task list'
				));
		}

		return TRUE;
	}

	function proc_service_design($term_id = FALSE)
	{
		$services_pricetag = get_term_meta_value($term_id,'services_pricetag');
		$services_pricetag = @unserialize($services_pricetag);
		if(empty($services_pricetag['webdesign']['is_design']))
			return 1;
		$result = 0;
		$type = '';
		// chưa bắt đầu thiết kế
		if(empty($services_pricetag['webdesign']['start_time']))
		{
			$this->insert_tasklist_design($term_id);
			$services_pricetag['webdesign']['start_time'] = time();
			$services_pricetag['webdesign']['end_time'] = 0;
			update_term_meta($term_id, 'services_pricetag', @serialize($services_pricetag));

			$type = 'Start design website';
			$result = 2;

			$this->webgeneral_report_m->send_tasklist_mail2customer($term_id, 'kythuat@webdoctor.vn');
		}
		else if(empty($services_pricetag['webdesign']['end_time']))
		{
			$services_pricetag['webdesign']['end_time'] = time();
			$type = 'End design website';
			update_term_meta($term_id, 'services_pricetag', @serialize($services_pricetag));
			$result = 3;
		}
		else
		{
			//đã thiết kế hoàn tất
			$result = 1;
		}
		if($result > 1)
		{
			$this->log_m->insert(array(
				'log_type' =>'start_service_time',
				'user_id' => $this->admin_m->id,
				'term_id' => $term_id,
				'log_content' => date('Y/m/d H:i:s').' - '.$type
				));
		}
		return $result;
	}


	public function init_setting_default($term_id = 0)
	{
		$this->load->model('webgeneral/webgeneral_seotraffic_m');
		//phone_report
		$phone = get_term_meta_value($term_id, 'representative_phone');
		if($phone) $this->termmeta_m->update_meta($term_id, 'phone_report',$phone);

		//phone_report
		$mail = get_term_meta_value($term_id, 'representative_email');
		if($mail) $this->termmeta_m->update_meta($term_id, 'mail_report',$mail);

		$value = $this->webgeneral_seotraffic_m->encrypt('',$term_id);
		$this->termmeta_m->update_meta($term_id, 'report_key', $value);

	}


	//Gửi mail thông báo cho Admin khi hoàn tất hợp đồng
	//Thông báo khởi tạo hợp đồng
	public function send_mail2admin($term_id = 0)
	{
		$this->load->library('email');
		$config = $this->config->item('packages');
		$term = $this->term_m->get($term_id);
		$mail = get_term_meta_value($term_id, 'representative_email');
		$mail_cc = array();

		$representative_name = get_term_meta_value($term_id, 'representative_name');
		$contract_code = get_term_meta_value($term_id, 'contract_code');
		$contract_begin = get_term_meta_value($term_id, 'contract_begin');
		$contract_end = get_term_meta_value($term_id, 'contract_end');

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_name = $this->admin_m->get_field_by_id($staff_id, 'display_name');
		$package_name = $this->webgeneral_config_m->get_package_name($term_id);

		$content = 'Dear nhóm Webdoctor <br>';
		$content.= 'Thông tin hợp đồng mới vừa được khởi tạo như sau: <br>';
		$content.= anchor(admin_url('webgeneral/setting/'.$term->term_id), 'Xin vui lòng vào link sau để cấu hình GA cho Website <br>', 'target="_blank"');

		$this->config->load('table_mail');
		$this->table->set_template($this->config->item('mail_template'));

		$this->table->set_caption('Thông tin hợp đồng');
		$this->table->add_row('Mã hợp đồng:', $contract_code);
		$this->table->add_row('Tên dịch vụ:', 'Web tổng hợp');
		$this->table->add_row('Gói dịch vụ:', $package_name);
		$this->table->add_row('Thời gian đăng ký:', my_date($contract_begin,'d/m/Y'));
		$this->table->add_row('Thời gian hết hạn:', my_date($contract_end,'d/m/Y'));
		$this->table->add_row('Kinh doanh phụ trách:', '#'.$staff_id.' - '.$staff_name);
		$content.= $this->table->generate();

		$services_pricetag = get_term_meta_value($term->term_id, 'services_pricetag');
		$services_pricetag = @unserialize($services_pricetag);
		if(!empty($services_pricetag)){
			$this->table->set_caption('Thông tin dịch vụ');
			$ignore_table = TRUE;
			$has_webdesign = !empty($services_pricetag['webdesign']['is_design']);
			if($has_webdesign){
				$ignore_table = FALSE;
				$label = 'Thiết kế website ';
				if(!empty($services_pricetag['webdesign']['theme_id']))
					$label.= '(ID:'.$services_pricetag['webdesign']['theme_id'].')';
				$value = force_var(@$services_pricetag['webdesign']['price'],'Miễn phí','Tính phí');
				$this->table->add_row($label,$value);

				$mail_cc[] = 'longnq@webdoctor.vn';
			}

			$has_hosting = !empty($services_pricetag['hosting']['has_hosting']);
			if($has_hosting){
				$ignore_table = FALSE;
				$label = 'Hosting ';
				$value = force_var(@$services_pricetag['hosting']['price'],'Miễn phí','Tính phí');
				$this->table->add_row($label,$value);
			}

			$has_domain = !empty($services_pricetag['domain']['is_free']);
			if($has_domain){
				$ignore_table = FALSE;
				$label = 'Domain ';
				if(!empty($services_pricetag['domain']['identity']))
					$label.= '('.$services_pricetag['domain']['identity'].')';
				$this->table->add_row($label,'Miễn phí');			
			}

			if(!$ignore_table)
				$content.= $this->table->generate();
		}

		$this->table->set_caption('Thông tin khách hàng');
		$this->table->add_row('Người đại diện:', $representative_name);
		$this->table->add_row('Địa chỉ:', get_term_meta_value($term_id, 'representative_address'));
		$this->table->add_row('Website:', $term->term_name);
		$this->table->add_row('Mail liên hệ:', $mail);
		
		$title = 'Hợp đồng '.$contract_code.' đã được khởi tạo.';
		
		$content.= $this->table->generate();

		$content.= '';
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');


		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to('support@webdoctor.vn');

		if($staff_mail) $mail_cc[] = $staff_mail;
		if(!empty($mail_cc)) $this->email->cc($mail_cc);

		$this->email->subject($title);
		$this->email->message($content);

		$send_status = $this->email->send();
		return $content;
	}

	//Code cứng tạm thời, sẽ thay bằng config sau
	function init_kpi_default($term_id = '', $package ='')
	{
		$insert = array();

		switch ($package) {
			case 'basic':
			$insert['seotraffic'] = 0;
			$insert['content'] = 15;
			break;
			case 'normal':
			$insert['seotraffic'] = 3000;
			$insert['content'] = 30;
			break;
			default:
			$insert['seotraffic'] = 0;
			$insert['content'] = 0;
			break;
		}

		$time_begin = $this->termmeta_m->get_meta_value($term_id, 'contract_begin');
		$contract_end = $this->termmeta_m->get_meta_value($term_id, 'contract_end');
		$contract_end = strtotime('+1 month',$contract_end);
		$this->load->model('webgeneral/webgeneral_kpi_m');

		while($time_begin < $contract_end)
		{
			$this->webgeneral_kpi_m->update_kpi_value($term_id,'seotraffic',$insert['seotraffic'], $time_begin, 1);
			// $this->webgeneral_kpi_m->update_kpi_value($term_id,'content',$insert['content'], $time_begin);
			$time_begin = strtotime('+1 month',$time_begin);
		}
	}

	function init_job_default($term_id =0, $start_time = 0, $package = '')
	{
		if(!$start_time)
		{
			$start_time = strtotime('+1 day');
		}
		if(in_array($package, array('startup','startupplus')))
			return false;

		$start_time = $this->mdate->startOfDay($start_time);
		$post_type = $this->webgeneral_m->get_post_type('task');
		$admin_id = 1;
		$jobs = array();
		$array_insert = array();

		// $jobs[] = array('title'=>'Chúc mừng tạo dự án thành công', 'day' =>'+1 day');
			$jobs[] = array('title'=>'Tối ưu và chỉnh sửa title, description', 'day' =>'+4 day','title_ascii'=>'toi uu va chinh sua title, description');
			$jobs[] = array('title'=>'Phân tích nội dung trùng lặp', 'day' =>'+3 day','title_ascii'=>'phan tich noi dung trung lap');
			$jobs[] = array('title'=>'Phân tích lượng truy cập Google Analatics', 'day' =>'+2 day','title_ascii'=>'phan tich luong truy cap Google Analatics');
			// $jobs[] = array('title'=>'Tối ưu các bài viết trên web thân thiện với Google', 'day' =>'+5 day','title_ascii'=>'toi uu cac bai viet tren web than thien voi Google');

			if($package == 'normal')
			{
				$jobs[] = array('title'=>'Tối ưu các thẻ Meta Robots', 'day' =>'+2 day','title_ascii'=>'toi uu the Meta Robots');
			}

			$jobs[] = array('title'=>'Đăng ký Google Maps', 'day' =>'+2 day','title_ascii'=>'dang ky Google Maps');
			$jobs[] = array('title'=>'Khởi tạo chỉnh sửa Sitemaps', 'day' =>'+2 day','title_ascii'=>'khoi tao chinh sua Sitemaps');


		//các đầu việc thêm dành cho gói tiêu chuẩn
		if($package == 'normal')
		{
			$jobs[] = array('title'=>'Kiểm tra các link liên kết đến website', 'day' =>'+6 day','title_ascii'=>'kiem tra cac link lien ket den website');
			$jobs[] = array('title'=>'Kiểm tra lỗi 404', 'day' =>'+3 day','title_ascii'=>'kiem tra loi 404');
			$jobs[] = array('title'=>'Tối ưu hình ảnh website chuẩn SEO', 'day' =>'+5 day','title_ascii'=>'toi uu hinh anh website chuan SEO');
			$jobs[] = array('title'=>'Kiểm tra tối ưu phiên bản mobile', 'day' =>'+15 day','title_ascii'=>'kiem tra toi uu phien ban mobile');
		}

		$array_insert['post_type'] = $post_type;
		$array_insert['post_author'] = $admin_id;
		$title = '';
		foreach($jobs as $job)
		{
			$end_date = strtotime($job['day'], $start_time);
			$array_insert['start_date'] = $start_time;
			$array_insert['end_date'] = $end_date;
			$array_insert['post_title'] = $job['title'];
			$start_time = $end_date;

			$post_id = $this->post_m->insert($array_insert);
			$title.= $job['title'].'<br>';

			$this->postmeta_m->update_meta($post_id, 'task_type', 'task-default');
			$this->postmeta_m->update_meta($post_id, 'sms_status', 1);

			$this->postmeta_m->update_meta($post_id, 'title_ascii', $job['title_ascii']);
			$this->term_posts_m->set_post_terms($post_id, array($term_id), 'webgeneral');
		}
		return $title;
	}

	public function calc_contract_value($term = null)
	{
		$contract_begin = get_term_meta_value($term->term_id, 'contract_begin');
		$contract_end = get_term_meta_value($term->term_id, 'contract_end');
		$months = diffInMonths($contract_begin,$contract_end);

		$this->load->config('webgeneral/webgeneral'); 
		$services_pricetag = get_term_meta_value($term->term_id,'services_pricetag');
		$services_pricetag = @unserialize($services_pricetag);

		if(empty($services_pricetag)) return 0;

		$service_package = get_term_meta_value($term->term_id, 'service_package');
		$service_price = get_term_meta_value($term->term_id,'service_package_price');
		if(empty($service_price))
			$service_price = $this->webgeneral_config_m->get_price($term->term_id);
		
		$total = $service_price * $months;

		if(@$services_pricetag['discount_month']){
			$discount_month_price = (int) @$services_pricetag['discount_month'] * $service_price;
			$total += $discount_month_price * (-1);
		}

		if(@$services_pricetag['discount']['is_discount']){
			$discount_percent = ((int) @$services_pricetag['discount']['percent']) / 100;
			$discount_amound = $total*$discount_percent;
			$total-= $discount_amound;
		}

		if(@$services_pricetag['webdesign']['is_design'])
			$total += (int) @$services_pricetag['webdesign']['price'];

		if(@$services_pricetag['hosting']['has_hosting'])
			$total += (int) @$services_pricetag['hosting']['price'];

		return $total;
	}

	function insert_tasklist_design($term_id =0, $start_time = 0, $package = '')
	{
		if(!$start_time)
		{
			$start_time = strtotime('+1 day');
		}
		$start_time = $this->mdate->startOfDay($start_time);
		$post_type = $this->webgeneral_m->get_post_type('task');
		$admin_id = 1;
		$jobs = array();
		$array_insert = array();

		$jobs[] = array('title'=>'Thiết kế giao diện ban đầu', 'day' =>'+3 day','title_ascii'=>'thiet ke giao dien ban dau');
		$jobs[] = array('title'=>'Viết CSS và HTML cho giao diện', 'day' =>'+3 day','title_ascii'=>'viet CSS va HTML cho giao dien');
		$jobs[] = array('title'=>'Code website theo giao diện', 'day' =>'+4 day','title_ascii'=>'code website theo giao dien');
		$jobs[] = array('title'=>'Dựng website chạy thử nghiệm', 'day' =>'+1 day','title_ascii'=>'dung website chay thu nghiem');
		$jobs[] = array('title'=>'Cập nhật nội dung ban đầu', 'day' =>'+3 day','title_ascii'=>'cap nhat noi dung ban dau');
		$jobs[] = array('title'=>'Chỉnh sửa giao diện', 'day' =>'+1 day','title_ascii'=>'chinh sua noi dung giao dien');
		$jobs[] = array('title'=>'Chuyển tên miền và chạy thực tế', 'day' =>'+1 day','title_ascii'=>'chuyen ten mien va chay thuc te');

		$array_insert['post_type'] = $post_type;
		$array_insert['post_author'] = $admin_id;
		$title = '';
		foreach($jobs as $job)
		{
			$end_date = strtotime($job['day'], $start_time);
			$array_insert['start_date'] = $start_time;
			$array_insert['end_date'] = $end_date;
			$array_insert['post_title'] = $job['title'];
			$start_time = $end_date;

			$post_id = $this->post_m->insert($array_insert);
			$title.= $job['title'].'<br>';

			$this->postmeta_m->update_meta($post_id, 'task_type', 'task-design-default');
			$this->postmeta_m->update_meta($post_id, 'sms_status', 1);

			$this->postmeta_m->update_meta($post_id, 'title_ascii', $job['title_ascii']);
			$this->term_posts_m->set_post_terms($post_id, array($term_id), 'webgeneral');
		}
		return $title;
	}

	/**
	 * Creates a default invoice for webgeneral contract.
	 * Required numbers_of_payment and contract_value
	 *
	 * @param      term_m  $term   The term
	 *
	 * @return     boolean  status of process
	 */
	public function create_default_invoice($term = false)
	{
		if(empty($term)) return FALSE;

		$contract_begin = get_term_meta_value($term->term_id,'contract_begin');
		$contract_end = get_term_meta_value($term->term_id,'contract_end');

		$services_pricetag = @unserialize(get_term_meta_value($term->term_id, 'services_pricetag'));

		$service_price = get_term_meta_value($term->term_id,'service_package_price');
		if(empty($service_price))
		{
			$this->load->config('webgeneral/webgeneral'); 
			$package_config = $this->config->item('service','packages');

			$service_package = get_term_meta_value($term->term_id,'service_package');
			$service_package = isset($package_config[$service_package]) ? $service_package : $this->config->item('default','packages');
			$service_price = $package_config[$service_package]['price'];
			$this->termmeta_m->update_meta($term->term_id, 'service_package_price',$service_price);
		}

		$num_month = diffInMonths($contract_begin,$contract_end);
		$services_pricetag['number_of_payments'] = $services_pricetag['number_of_payments'] ?? $num_month;

		//số tháng cách nhau cho mỗi hóa đơn thu tiền
		$num_month4inv = ceil(div($num_month,$services_pricetag['number_of_payments']));

		//insert inv 1
		$start_date = $contract_begin;

		$total_amount =  0;
		$rate =  (!empty($services_pricetag['discount']['is_discount']) &&!empty($services_pricetag['discount']['percent'])) ? $services_pricetag['discount']['percent'] : 0;

		$rate = 100 - $rate;
		// $price = $service_price + $service_price * $discount;
 
		$invoice_items = array();
		
		for($i = 0 ; $i < $services_pricetag['number_of_payments']; $i++)
		{
			if($num_month4inv == 0)
				break;
			$end_date = strtotime('+'.$num_month4inv.' month -1 day', $start_date);
			$end_date = $this->mdate->endOfDay($end_date);

			//tạo 1 hóa đơn mới
			$inv_id = $this->invoice_m
			->insert(array(
				'post_title' => "Thu tiền đợt ". ($i + 1),
				'post_content' => "",
				'start_date' => $start_date,
				'end_date' => $end_date,
				'post_type' => $this->invoice_m->post_type
				));

			if(empty($inv_id)) continue;

			$this->term_posts_m->set_post_terms($inv_id, $term->term_id, $term->term_type);
			$invoice_items = array();

			$quantity = $num_month4inv;
			$invoice_items[] = array(
				'invi_title' => 'Chăm sóc website',
				'inv_id' => $inv_id,
				'invi_description' => '',
				'invi_status' => 'publish',
				'price' => $service_price,
				'quantity' => $quantity,
				'invi_rate' => $rate,
				'total' => $this->invoice_item_m->calc_total_price($service_price, $quantity, $rate)
				);

			//thêm các thông tin cho hóa đơn đầu tiên
			if($i == 0)
			{
				if(!empty($services_pricetag['webdesign']['is_design']))
				{
					$tmp_price = (int) @$services_pricetag['webdesign']['price'];
					$invoice_items[] = array(
						'invi_title' => 'Thiết kế website',
						'inv_id' => $inv_id,
						'invi_description' => '',
						'invi_status' => 'publish',
						'price' => $tmp_price,
						'quantity' => 1,
						'invi_rate' => 100,
						'total' => $this->invoice_item_m->calc_total_price($tmp_price, 1, 100)
						);
				}

				if(!empty($services_pricetag['hosting']['has_hosting']))
				{
					$tmp_price = (int) @$services_pricetag['hosting']['price'];
					$invoice_items[] = array(
						'invi_title' => 'Hosting',
						'inv_id' => $inv_id,
						'invi_description' => '',
						'invi_status' => 'publish',
						'price' => $tmp_price,
						'quantity' => 1,
						'invi_rate' => 100,
						'total' => $this->invoice_item_m->calc_total_price($tmp_price, 1, 100)
						);
				}

			}

			//hóa đơn cuối cùng
			if(($i + 1) == $services_pricetag['number_of_payments'])
			{
				if(!empty($services_pricetag['discount_month']))
				{
					$tmp_price = (int) @$services_pricetag['discount_month'] * $service_price * -1;
					$invoice_items[] = array(
						'invi_title' => 'Miễn phí chăm sóc Website',
						'inv_id' => $inv_id,
						'invi_description' => '',
						'invi_status' => 'publish',
						'price' => $tmp_price,
						'quantity' => $services_pricetag['discount_month'],
						'invi_rate' => $rate,
						'total' => $this->invoice_item_m->calc_total_price($tmp_price, $services_pricetag['discount_month'], $rate)
						);
				}
			}

			$this->invoice_item_m->insert_many($invoice_items);

			$start_date = strtotime('+1 day', $end_date);

			//tháng còn lại
			$month_end = $num_month - $num_month4inv;
			if($month_end < $num_month4inv)
			{
				$num_month4inv = $month_end;
			}

			$total_amount+= $this->invoice_item_m->get_total($inv_id,'total');
		}

		$contract_value = get_term_meta_value($term->term_id,'contract_value');
		$invs_amount = $this->base_contract_m->calc_invs_value($term->term_id);
		if($invs_amount < $contract_value)
		{
			return parent::create_default_invoice($term);
		}

		return TRUE;
	}

	function mail_warning_expires($term_id = 0)
	{
		//dd4b39 - màu đỏ
		$data = array();
		$data['time'] = time();
		$data['title'] = 'Cảnh báo hết hạn dịch vụ';
		$term = $this->term_m->get($term_id);
		$data['term'] = $term;
		$data['term_id'] = $term_id;

		$tasks = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('task'));
		$data['num_banner'] = 0;
		$data['num_cusomer_task'] = 0;
		$contents = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('content'));
		$data['num_content'] = count($contents);
		
		if($tasks)
		{
			foreach ($tasks as $task) {
				if($task->post_status != 'complete')
				{
					continue;
				}
				$task_type = get_post_meta_value($task->post_id, 'task_type');
				if($task_type == 'banner')
					$data['num_banner']++;
				else
					$data['num_cusomer_task']++;
			}
		}

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff = $this->admin_m->set_get_active()->get($staff_id);
		if(!$staff)
			$staff = $this->admin_m->get(18); //anh Hải

		$data['staff'] = $staff;
		$content = $this->load->view('email/warning_expires', $data, true);
		$title = 'Thông báo thời hạn thực hiện dịch vụ Webdoctor website '.ucfirst($term->term_name);
		return $this->set_send_mail($term_id, $title, $content);
	}

	function mail_end_contract($term_id = 0)
	{
		$data = array();
		$data['time'] = time();
		$data['title'] = 'Kết thúc hợp đồng';
		$term = $this->term_m->get($term_id);
		if(!$term)
			return false;
		$data['term'] = $term;
		$data['term_id'] = $term_id;

		$tasks = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('task'));
		$data['num_banner'] = 0;
		$data['num_cusomer_task'] = 0;
		$contents = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('content'));
		$data['num_content'] = count($contents);
		
		if($tasks)
		{
			foreach ($tasks as $task) {
				if($task->post_status != 'complete')
				{
					continue;
				}
				$task_type = get_post_meta_value($task->post_id, 'task_type');
				if($task_type == 'banner')
					$data['num_banner']++;
				else
					$data['num_cusomer_task']++;
			}
		}

		$data['tasks'] = $tasks;
		$data['contents'] = $contents;

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff = $this->admin_m->set_get_active()->get($staff_id);
		if(!$staff)
			$staff = $this->admin_m->get(18); //anh Hải

		$data['staff'] = $staff;

		$content = $this->load->view('email/end_contract', $data, true);
		$title = 'Thông báo kết thúc hợp đồng Webdoctor';
		return $this->set_send_mail($term_id, $title, $content);
	}

	function set_send_mail($term_id, $title, $content, $mail_cc = array())
	{

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff = $this->admin_m->set_get_active()->get($staff_id);
		$mail_report = get_term_meta_value($term_id,'mail_report');
		if(empty($mail_report))
			$mail_report = 'support@webdoctor.vn';
		
		$this->load->library('email');
		$this->email->from('support@webdoctor.vn','Webdoctor.vn');

		if($staff) $mail_cc[] = $staff->user_email;
		$mail_cc[] = 'thonh@webdoctor.vn';
		$mail_cc[] = 'support@webdoctor.vn';

		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to($mail_report);
		if(!empty($mail_cc)) $this->email->cc($mail_cc);

		$this->email->subject($title);
		$this->email->message($content);

		return $this->email->send();
	}
}

/* End of file Webgeneral_contract_m.php */
/* Location: ./application/modules/webgeneral/models/Webgeneral_contract_m.php */