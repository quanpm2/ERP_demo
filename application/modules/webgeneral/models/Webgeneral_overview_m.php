<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webgeneral_overview_m extends Base_Model {

	function __construct() {
		parent::__construct();
		$this->load->model('webgeneral/webgeneral_kpi_m');
	}

	public function get_statistic_index_data($term = false,$time_start=0,$time_end=0)
	{
		$data = array();
		$traffic_data = $this->get_statistic_traffic_data($term->term_id,$time_start,$time_end);
		$content_data = $this->get_statistic_content_data($term->term_id,$time_start,$time_end);
		$task_data = $this->get_statistic_task_data($term->term_id,$time_start,$time_end);
		$report_data = $this->get_statistic_report_data($term->term_id,$time_start,$time_end);
		
		$data = array_merge_recursive($traffic_data, $content_data, $task_data, $report_data);

		return $data;
	}

	public function get_statistic_report_data($term_id = 0,$time_start=0,$time_end=0){

		$this->table->set_heading('Tiêu đề','Thời gian gửi');

		$emails = $this->webgeneral_m
			->get_posts(array('where' => array('term_posts.term_id' => $term_id,
								'posts.created_on <=' => $time_end,
								'posts.created_on >=' => $time_start),
							'select' => 'posts.post_id',
							'tax_query' => array('taxonomy'=>'webgeneral', 'field'=> 'term_status', 'terms' =>'publish'),
							'orderby' => 'posts.post_status,posts.end_date',
							'order' => 'DESC',
							'post_type' => 'webgeneral-mail-report'));

		$this->load->model('log_m');

		$data = array();
		$data['email_num'] = count($emails);
		$data['sms_num'] = $this->log_m
			->where('log_time_create >=',my_date($time_start))
			->where('log_time_create <=',my_date($time_end))
			->where('log_type','webgeneral-report-sms')
			->where('term_id',$term_id)
			->count_by();

		return $data;
	}

	protected function get_statistic_traffic_data($term_id,$time_start = 0,$time_end = 0)
	{
		$data = array(
			'ga_percent'=>0,
			'ga_data_result'=>0,
			'kpi'=>0);

		$kpi = $this->webgeneral_kpi_m->get_kpi_value($term_id,'seotraffic', $time_start);
		if($kpi && $kpi >0){
			
			$ga_profile_id = get_term_meta_value($term_id, 'ga_profile_id');
			$ga = $this->webgeneral_seotraffic_m->get_ga(my_date($time_start,'Y-m-d'),my_date($time_end,'Y-m-d'),$ga_profile_id);
			$ga_data_result =  (isset($ga['total']['ga:sessions'])) ? $ga['total']['ga:sessions'] : 0;
			$percent = ($kpi == 0) ? 0 : (($ga_data_result / $kpi) * 100);

			$data['ga_percent'] = numberformat($percent);
			$data['ga_data_result'] = $ga_data_result;	
			$data['kpi'] = numberformat($kpi);
		}

		return $data;
	}

	protected function get_statistic_content_data($term_id = 0,$time_start=0,$time_end=0)
	{
		$data = array('content_percent'=>0,
					  'content_data_result'=>0,
					  'content_kpi'=>0,
					  'content_product_percent' => 0,
					  'content_product_data_result'=>0,
					  'content_product_kpi'=>0,
					  );

		if($kpi_content = $this->webgeneral_kpi_m->get_kpi_value($term_id,'content', $time_end))
		{
			$content_data_result = $this->webgeneral_kpi_m->get_kpi_result($term_id,'content',$time_end);
			$percent = ($kpi_content == 0) ? 0 : (($content_data_result / $kpi_content) * 100);

			$data['content_percent'] = numberformat($percent);
			$data['content_data_result'] = numberformat($content_data_result);	
			$data['content_kpi'] = numberformat($kpi_content);
		}

		if($kpi_content_product = $this->webgeneral_kpi_m->get_kpi_value($term_id,'content_product', $time_end))
		{
			$content_product_data_result = $this->webgeneral_kpi_m->get_kpi_result($term_id,'content_product',$time_end);
			$percent_content_product = ($kpi_content_product == 0) ? 0 : (($content_product_data_result / $kpi_content_product) * 100);
			
			$data['content_product_percent'] = numberformat($percent_content_product);
			$data['content_product_data_result'] = numberformat($content_product_data_result);	
			$data['content_product_kpi'] = numberformat($kpi_content_product);
		}

		return $data;
	}

	protected function get_statistic_task_data($term_id = 0,$time_start = 0,$time_end = 0){

		$data = array('task_late_count' => 0,
			'task_complete_count' => 0,
			'task_waiting_count' => 0,
			'total_task' => 0);
	
		$tasks = $this->webgeneral_m
				->or_where("((posts.end_date <= {$time_end} AND posts.end_date >= {$time_start}) OR (posts.post_status != 'complete' AND posts.end_date <= {$time_start}))")
				->get_posts(array('where' => array('term_posts.term_id' => $term_id),
							'select' => 'posts.post_id,post_title,end_date,post_status',
							'tax_query' => array('taxonomy'=>'webgeneral', 'field'=> 'term_status', 'terms' =>'publish'),
							'orderby' => 'posts.post_status,posts.end_date',
							'order' => 'DESC',
							'post_type' => $this->webgeneral_m->get_post_type('task')));

		if(empty($tasks)) return $data;

		$data['total_task'] = count($tasks);
		
		$time = time();
		foreach ($tasks as $task) {
			if($task->post_status == 'complete')
				$data['task_complete_count']++;
			else if($task->end_date < $time) 
				$data['task_late_count']++;
			else $data['task_waiting_count']++; 
		}

		return $data;
	}

	public function build_seotraffic_chart($term_id = 0,$start_date = '',$end_date = '')
	{
		$data = array();
		$dayEndOfMonth = date("Y-m-t", strtotime($end_date));
		$dayEndOfMonth = $this->mdate->endOfDay($dayEndOfMonth);
		$ga_profile_id = get_term_meta_value($term_id, 'ga_profile_id');
		$kpi = $this->webgeneral_kpi_m->get_kpi_value($term_id,'seotraffic', strtotime($end_date));

		$kpi = $kpi / date('d',$dayEndOfMonth);
		$kpi = numberformat($kpi);
		$ga_data = $this->webgeneral_seotraffic_m->get_ga($start_date, $end_date,$ga_profile_id);

		if(!$ga_data) 
			return $data;

		$charData = array();
		$charData['ga:sessions'] = array();
		$charData['Organic Search'] = array();
		$charData['kpi'] = array();

		foreach($ga_data['headers'] as $header) {
			$charData['ga:sessions'][] = (isset($ga_data['results'][$header]['ga:sessions']) ? $ga_data['results'][$header]['ga:sessions'] : 0);

			$charData['Organic Search'][] = (isset($ga_data['results'][$header]['Organic Search']) ? $ga_data['results'][$header]['Organic Search'] : 0);
			$charData['kpi'][] = $kpi;

		}
		$charData['ga:sessions'] = implode(',', $charData['ga:sessions']);
		$charData['Organic Search'] = implode(',', $charData['Organic Search']);
		$charData['kpi'] = implode(',', $charData['kpi']);
		
		$charDataHeader = array();
		foreach($ga_data['headers'] as $header)
		{
			$charDataHeader[] = date('d/m', strtotime($header));
		}

		$data['charDataHeader'] = implode('","', $charDataHeader);
		$data['charDataHeader'] = '"'.$data['charDataHeader'].'"';

		$data['charData'] = $charData;
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;

		return $data;
	}

	public function get_content_table($term_id = 0,$time_start = 0,$time_end = 0, $type = 'content')
	{
		$this->admin_ui = new admin_ui();
		$data['content_table'] = $this->admin_ui
												->set_ordering(FALSE)
												->select('posts.post_content')
												->add_column('posts.post_id','#')
												->add_column('post_title','Tiêu đề')
												->add_column('start_date','Ngày thêm','$1','date("d/m/Y",start_date)')
												->add_column_callback('post_title',function($data,$row_name){
													if(valid_url($data['post_content']))
														$data['post_title'] = anchor($data['post_content'],$data['post_title'],array('target' =>'_blank'));
													return $data;
												},FALSE) 
												->where('term_id',$term_id)
												->where('posts.start_date >=', $time_start)
												->where('posts.start_date <', $time_end)
												->where('post_type', $this->webgeneral_m->get_post_type($type))
												->order_by("posts.post_id desc")
												->from('posts')
												->join('term_posts', 'term_posts.post_id = posts.post_id')
												->generate();
		return $data;
	}

	public function get_tasklist_table($term_id = 0,$time_start = 0,$time_end = 0)
	{
		$tasks = 
			$this->webgeneral_m
				->or_where("((posts.end_date <= {$time_end} AND posts.end_date >= {$time_start}) OR (posts.post_status != 'complete' AND posts.end_date <= {$time_start}))")
				->get_posts(array('where' => array('term_posts.term_id' => $term_id),
							'select' => 'posts.post_id,post_title,end_date,post_status',
							'tax_query' => array('taxonomy'=>'webgeneral', 'field'=> 'term_status', 'terms' =>'publish'),
							'orderby' => 'posts.post_status,posts.end_date',
							'order' => 'DESC',
							'post_type' => $this->webgeneral_m->get_post_type('task')));

		if(!empty($tasks)){
			$time = time();
			foreach ($tasks as $key => $task) {

				if($task->post_status == 'complete')
					$task->post_title = '<span class="text-gray">'.$task->post_title.'</span>';
				else if($task->end_date < $time) 
					$task->post_title = '<span class="text-red">'.$task->post_title.'</span>';

				$row_data = array($task->post_title,my_date($task->end_date,'d/m/Y'));

				$this->table->add_row($row_data);
			}
		}
		else $this->table->add_row(array('data'=>'Không có dữ liệu', 'colspan'=> 2, 'class'=>'text-center'));

		$data['task_table'] = $this->table->generate();

		return $data;
	}

	public function get_email_log_table($term_id = 0,$time_start = 0,$time_end = 0)
	{
		$data = array();
		$this->table->set_heading('Tiêu đề','Thời gian gửi');
		$emails = $this->webgeneral_m
			->get_posts(array('where' => array('term_posts.term_id' => $term_id,
								'posts.created_on <=' => $time_end,
								'posts.created_on >=' => $time_start),
							'select' => 'posts.post_id,post_title,posts.created_on',
							'tax_query' => array('taxonomy'=>'webgeneral', 'field'=> 'term_status', 'terms' =>'publish'),
							'orderby' => 'posts.post_status,posts.end_date',
							'order' => 'DESC',
							'post_type' => 'webgeneral-mail-report'));

		if(!empty($emails))
			foreach ($emails as $email)
				$this->table->add_row($email->post_title,my_date($email->created_on,'d/m/Y'));
		else $this->table->add_row(array('data'=>'Không có dữ liệu', 'colspan'=> 2, 'class'=>'text-center'));

		$data['email_table'] = $this->table->generate();

		return $data;
	}

	public function get_sms_log_table($term_id = 0,$time_start = '',$time_end = ''){

		$data = array();
		
		$this->load->model('log_m');
		$this->table->set_heading('Nội dung','Thời gian');
		$logs = $this->log_m
			->where('log_time_create >=',my_date($time_start))
			->where('log_time_create <=',my_date($time_end))
			->where('log_type','webgeneral-report-sms')
			->where('term_id',$term_id)
			->get_many_by();

		if(!empty($logs))
			foreach ($logs as $log)
				$this->table->add_row($log->log_title,$log->log_time_create);

		else $this->table->add_row(array('data'=>'Không có dữ liệu', 'colspan'=> 2, 'class'=>'text-center'));

		$data['sms_table'] = $this->table->generate();

		return $data;
	}

	function get_customer_info($term_id = 0)
	{
		$data = array();

		$this->table->set_caption('Thông tin hợp đồng');
		$this->admin_form->set_col(6);
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_name = $this->admin_m->get_field_by_id($staff_id, 'display_name');

		$this->table->add_row('Mã hợp đồng', get_term_meta_value($term_id, 'contract_code'));
		$this->table->add_row('Thời gian thực hiện', my_date(@get_term_meta_value($term_id,'start_service_time'), 'd/m/Y'));
		$this->table->add_row('Thời gian kết thúc', my_date(@get_term_meta_value($term_id,'end_service_time'), 'd/m/Y'));
		$this->table->add_row('Gói dịch vụ', $this->webgeneral_config_m->get_package_name($term_id).' ('.$this->webgeneral_config_m->get_package_label($term_id).')');
		$this->table->add_row('Kinh doanh phụ trách', $staff_name);
		$info_contract = $this->table->generate();

		$this->table->set_caption('Thông tin khách hàng');
		$this->table->add_row('Người đại diện', get_term_meta_value($term_id,'representative_name'));
		$this->table->add_row('Mail liên hệ', mailto(get_term_meta_value($term_id,'representative_email')));
		$this->table->add_row('Địa chỉ', get_term_meta_value($term_id,'representative_address'));
		$data['customer_table'] = $info_contract.$this->table->generate();
		return $data;
	}
}
/* End of file Webgeneral_overview_m.php */
/* Location: ./application/modules/webgeneral/models/Webgeneral_overview_m.php */