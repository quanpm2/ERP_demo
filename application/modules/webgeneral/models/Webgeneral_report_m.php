<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webgeneral_report_m extends Base_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->model('webgeneral_m');
		$this->load->model('webgeneral/webgeneral_seotraffic_m');
		$this->load->config('webgeneral');
	}

	//send sms báo cáo task cho khách
	public function send_sms($term_id)
	{
		$result = array();
		$result['status'] = false;
		$result['msg'] = '';
		$this->load->library('sms');
		$time = time();
		$post_send_id = array();
		$phone = $this->termmeta_m->get_meta_value($term_id, 'phone_report');
		if(!$phone)
		{
			$result['msg'] = 'Số điện thoại chưa được khai báo.';
			return $result;
		}
		// $time = strtotime('2016/03/17');
		// echo $time;
		$posts = $this->post_m->get_posts(array(
			'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
			'tax_query'=> array(
				'taxonomy' => $this->webgeneral_m->term_type,
				'terms' => $term_id,
				),
			'where' => array(
				'end_date <=' => $time
				),
			'post_status' => 'complete',
			'meta_key' => 'sms_status',
			'meta_value' => '1',
			'post_type' => $this->webgeneral_m->get_post_type('task')
			));

		if(!$posts)
		{
			$result['msg'] = 'Không có đầu việc nào được thực hiện';
			return $result;
		}
		$result['status'] = true;
		$title = array();
		foreach($posts as $post)
		{
			$meta_title = $this->postmeta_m->get_meta_value($post->post_id,'title_ascii');

			$meta_title = (!empty($meta_title) ? $meta_title : convert_utf82ascci(mb_strtolower($post->post_title)));
			if(empty($meta_title))
				continue;
			$title[] = $meta_title;

			$post_send_id[] = $post->post_id;
		}

		$tpl = 'Kinh gui quy khach! Webdoctor da hoan thanh viec ';
		$tpl.= implode(', ', $title);


		$f_posts = $this->post_m->where_in('post_status',array('process'))->get_posts(array(
			'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
			'tax_query'=> array(
				'taxonomy' => $this->webgeneral_m->term_type,
				'terms' => $term_id,
				),
			'where' => array(
				'end_date <' => $time
				),
			'numberposts' => 1,
			'meta_key' => 'sms_status',
			'meta_value' => 1,
			'orderby' => 'start_date',
			'order' => 'asc',
			'post_type' => $this->webgeneral_m->get_post_type('task')
			));
		if(!$f_posts)
			$f_posts = $this->post_m->where_in('post_status',array('complete','process'))->get_posts(array(
				'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
				'tax_query'=> array(
					'taxonomy' => $this->webgeneral_m->term_type,
					'terms' => $term_id,
					),
				'where' => array(
					'end_date >' => $time
					),
				'numberposts' => 1,
				'meta_key' => 'sms_status',
				'meta_value' => 1,
				'orderby' => 'start_date',
				'order' => 'asc',
				'post_type' => $this->webgeneral_m->get_post_type('task')
				));

		if($f_posts)
		{
			$tpl.= ' va dang tien hanh viec ';
			$title = array();
			foreach($f_posts as $fpost)
			{
				$meta_title = $this->postmeta_m->get_meta_value($fpost->post_id,'title_ascii');

				$title[] = (!empty($meta_title) ? $meta_title : convert_utf82ascci(mb_strtolower($fpost->post_title)));
			}
			$tpl.= implode(', ', $title);
		}
		$tpl.= '.';

		// $sms_results = $this->sms->fake_send($phone, $tpl);
		$sms_results = $this->sms->send($phone,$tpl);

		$result['msg'] = array();
		foreach ($sms_results as $sms_result) {
			$log_id = $this->log_m->insert(array(
				'log_title'=>$tpl,
				'log_status'=> $sms_result['response']->SendSMSResult,
				'term_id'=>$term_id,
				'user_id'=> 0,
				'log_type'=> 'webgeneral-report-sms',
				'log_time_create'=> date('Y-m-d H:i:s'),
				));
			$result['msg'][] = array('title'=>$tpl, 'status'=>$sms_result['response']->SendSMSResult, 'recipient'=>$sms_result['recipient']);
			$sms = array();
			$sms['sms_recipient'] = $sms_result['recipient'];
			$sms['sms_service_id'] = $sms_result['config']['service_id'];
			$this->log_m->meta->update_meta($log_id, 'sms_result', json_encode($sms));
		}
		if(isset($sms_results[0]))
		{
			$result_code = $sms_results[0]['response']->SendSMSResult;
			if($result_code == 0)
			{
				foreach($post_send_id as $post_id)
				{
					//set status cho post để không gửi lại
					$this->postmeta_m->update_meta($post_id, 'sms_status',2);
				}
			}
			
		}
		return $result;
	}

	function copy($post_or_id)
	{
		if(is_object($post_or_id) || is_array($post_or_id))
			$post = $post_or_id;
		else
			$post = $this->post_m->get($post_or_id);
		if(!$post)
			return 'No post';
		$post_id = $post->post_id;
		unset($post->post_id);
		unset($post->post_status);
		unset($post->post_content);
		unset($post->post_excerpt);
		$post->created_on = time();

		$post->start_date = strtotime('+7 day',$post->start_date);
		$post->end_date = strtotime('+7 day',$post->start_date);

		$post->post_title = '[BÁO CÁO {website_name_up}] HIỆN TRẠNG DOANH NGHIỆP TỪ NGÀY {start_date} ĐẾN NGÀY {end_date}';
		$new_post_id = $this->post_m->insert($post);
		$post_terms = $this->term_posts_m->get_the_terms($post_id);
		if($post_terms){
			foreach($post_terms as $pt){
				$this->term_posts_m->insert(array('term_id' => $pt, 'post_id'=> $new_post_id));
			}
		}

		//insert post meta
		$post_metas = $this->postmeta_m->get_many_by('post_id',$post_id);
		if($post_metas)
		{
			foreach($post_metas as $meta)
			{
				$this->postmeta_m->update_meta($new_post_id, $meta->meta_key, $meta->meta_value);
			}
		}
		return $new_post_id;
	}

	function get_report_link($term_id, $term_name, $post_id)
	{
		$key = $this->termmeta_m->get_meta_value($term_id, 'report_key');
		if(!$key)
		{
			$key = $this->webgeneral_seotraffic_m->encrypt('',$term_id);
			$this->termmeta_m->update_meta($term_id,'report_key',$key);
		}

		$q = array(
			'domain' => $key,
			'p' => $post_id
			);
		return 'http://report.webdoctor.vn/website-tong-hop/'.$term_name.'?'.http_build_query($q);
		return base_url().'report/website-tong-hop/'.$term_name.'?'.http_build_query($q);
	// return 'http://report.webdoctor.vn/website-tong-hop/'.$term_name.'?'.http_build_query($q);
	}

	//gửi mail báo cáo tuần cho khách hàng
	//Báo cáo sẽ được gủi từ thứ 2 tuần trước đến 23h59p ngày Chủ nhật
	public function send_mail($term_id = 0)
	{
		$time_send = $this->termmeta_m->get_meta_value($term_id, 'mail_time_next_send');
		if(!$time_send)
		{
			$time_send = strtotime('last monday');
		}
		
		if($time_send > time())
			return false;
		//update next time send mail
		$this->termmeta_m->update_meta($term_id, 'mail_time_next_send', strtotime('next monday',$time_send));
		$mails = $this->termmeta_m->get_meta_value($term_id, 'mail_report');
		if(!$mails)
		{
			return array('status'=>false, 'msg' =>'Chưa cấu hình email người nhận');
		}

		$term = $this->term_m->get($term_id);
		$end_date = $time_send - 1;
		$start_date = strtotime('-7 day',$time_send);
		$start_date = $this->mdate->startOfDay($start_date);
		$end_date = $this->mdate->endOfDay($end_date);

		$data = array();
		$data['term'] = $term;
		$data['tasks'] = $this->webgeneral_m
		->get_posts_from_date($start_date, $end_date,$term_id, $this->webgeneral_m->get_post_type('task'));
		$data['contents'] = $this->webgeneral_m
		->get_posts_from_date($start_date, $end_date,$term_id, $this->webgeneral_m->get_post_type('content'));
		
		$this->load->model('webgeneral_overview_m');
		$data['statistic_data'] = $this->webgeneral_overview_m
		->get_statistic_index_data($term,$this->mdate->startOfMonth(),$this->mdate->endOfMonth());

		if(!$data['tasks'] && !$data['contents'])
		{
			return array('status'=>false, 'msg' =>'Không có dữ liệu gửi đi', 
				'to'=>$mails);
		}

		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;

		$post_id = $this->webgeneral_m->insert(array(
			'post_status' =>'pending',
			'post_type' =>'webgeneral-mail-report',
			'start_date' =>$start_date,
			'end_date' =>$end_date,
			'created_on' => time(),
			));

		$this->term_posts_m->set_post_terms($post_id, array($term_id), 'webgeneral');

		$data['report_link'] = $this->get_report_link($term_id, $term->term_name, $post_id);

		$ga_profile_id = $this->termmeta_m->get_meta_value($term_id, 'ga_profile_id');
		$data['ga'] = $this->webgeneral_seotraffic_m->get_ga(date('Y-m-d',$start_date), date('Y-m-d',$end_date),$ga_profile_id);

		$this->load->model('backlink/backlink_m');
		$data['backlinks'] = $this->backlink_m
		->select('FROM_UNIXTIME(created_on,"%d-%m-%Y") AS date')
		->select('SUM(BLINK_STATUS = "active") AS backlink')
		->select('COUNT(DISTINCT blink_keyword) AS keyword')
		->where('created_on >=',$start_date)
		->where('created_on <=',$end_date)
		->group_by('date','asc')
		->as_array()
		->get_many_by();

		$layout = 'email/blue_tpl';
		$content_view = 'webgeneral/report/tpl_week_report';

		$data['title'] = 'Webdoctor.vn báo cáo công việc website '.ucfirst($term->term_name);
		$data['content'] = $this->load->view($content_view,$data,TRUE);

		$content = $this->load->view($layout,$data,TRUE);
		
		$title = $data['title'].' từ ngày '.my_date($start_date,'d/m/Y').' đến '.my_date($end_date,'d/m/Y').'';

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');

		$this->load->library('email');
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to($mails);

		$mail_cc = array();
		$mail_cc[] = 'support@webdoctor.vn';
		$mail_cc[] = 'quanly@webdoctor.vn';
		if($staff_mail)
		{
			$mail_cc[] = $staff_mail;
		}
		$this->email->cc($mail_cc);
		$post_update = array(
			'post_title'=>$title,
			'post_content'=>$content
			);

		$this->email->subject($title);
		$this->email->message($content);
		// $send_status = true;
		$send_status = $this->email->send();

		if($send_status)
		{
			$post_update['post_status'] = 'publish';
		}

		$this->webgeneral_m->update($post_id, $post_update);
		$this->email->clear();

		return array('status'=> $send_status, 
			'msg' =>$post_update, 
			'to'=>$mails);
	}

	public function send_mail_custom($term_id = 0,$start_date,$end_date, $isDebug = false)
	{
		$this->load->model('webgeneral/webgeneral_kpi_m');
		$this->load->model('webgeneral/webgeneral_config_m');
		$mails = $this->termmeta_m->get_meta_value($term_id, 'mail_report');
		if(!$mails)
		{
			return array('status'=>false, 'msg' =>'Chưa cấu hình email người nhận');
		}

		$term = $this->term_m->get($term_id);
		$start_date = $this->mdate->startOfDay($start_date);
		$end_date = $this->mdate->endOfDay($end_date);

		$data = array();
		$data['term'] = $term;
		$data['tasks'] = $this->webgeneral_m->get_posts_from_date($start_date, $end_date,$term_id, $this->webgeneral_m->get_post_type('task'));
		$data['contents'] = $this->webgeneral_m->get_posts_from_date($start_date, $end_date,$term_id, $this->webgeneral_m->get_post_type('content'));

		$data['month_tasks'] = $this->webgeneral_m->get_posts_from_date($this->mdate->startOfMonth($start_date), $this->mdate->endOfMonth($start_date),$term_id, $this->webgeneral_m->get_post_type('task'));

		$data['all_tasks'] = $this->webgeneral_m->get_posts_from_date(0, $this->mdate->endOfMonth($start_date),$term_id, $this->webgeneral_m->get_post_type('task'));


		// prd($term_id, $data['full_tasks']);


		// if(!$data['tasks'] && !$data['contents'])
		// {
		// 	return array('status'=>false, 'msg' =>'Không có dữ liệu gửi đi', 
		// 		'to'=>$mails);
		// }

		$data['start_date'] = $start_date;
		$data['term_id'] = $term_id;
		$data['end_date'] = $end_date;
		$data['time_send'] = time();

		
		$ga_profile_id = $this->termmeta_m->get_meta_value($term_id, 'ga_profile_id');
		$data['ga'] = $this->webgeneral_seotraffic_m->get_ga(date('Y-m-d',$start_date), date('Y-m-d',$end_date),$ga_profile_id);
		// prd($data['ga']);
		// prd($kpi);
		// prd($data['contents']);

		$data['title'] = 'Webdoctor.vn báo cáo công việc website '.ucfirst($term->term_name);
		$content = $this->load->view('webgeneral/report/tpl_mail',$data,TRUE);
		if($isDebug == true)
		{
			return $content;
		}
		$post_id = $this->webgeneral_m->insert(array(
			'post_status' =>'pending',
			'post_type' =>'webgeneral-mail-report',
			'start_date' =>$start_date,
			'end_date' =>$end_date,
			'created_on' => time(),
			));

		$this->term_posts_m->set_post_terms($post_id, array($term_id), 'webgeneral');
		$data['report_link'] = $this->get_report_link($term_id, $term->term_name, $post_id);
		
 
		$layout = 'email/blue_tpl';
		$content_view = 'webgeneral/report/tpl_week_report';

		
		$data['content'] = $content;

		
		$title = $data['title'].' từ ngày '.my_date($start_date,'d/m/Y').' đến '.my_date($end_date,'d/m/Y').'';

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');

		$this->load->library('email');
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to($mails);

		$mail_cc = array();
		$mail_cc[] = 'support@webdoctor.vn';
		// $mail_cc[] = 'quanly@webdoctor.vn';
		if($staff_mail)
		{
			$mail_cc[] = $staff_mail;
		}
		$this->email->cc($mail_cc);
		$post_update = array(
			'post_title'=>$title,
			'post_content'=>$content
			);

		$this->email->subject($title);
		$this->email->message($content);
		// $send_status = true;
		$send_status = $this->email->send();

		if($send_status)
		{
			$post_update['post_status'] = 'publish';
		}

		$this->webgeneral_m->update($post_id, $post_update);
		$this->email->clear();

		return array('status'=> $send_status, 
			'msg' =>$post_update, 
			'to'=>$mails);
	}
	public function send_activation_mail2customer($term_id = 0)
	{
		$term = $this->term_m->get($term_id);
		if(empty($term)) return FALSE;

		$data = array('term'=>$term);

		$time_start = get_term_meta_value($term_id,'contract_begin');
		$time_end = get_term_meta_value($term_id,'contract_end');

		$start_service_time = get_term_meta_value($term_id,'start_service_time');
		$end_service_time = get_term_meta_value($term_id,'end_service_time');

		if(empty($start_service_time)){
			$start_service_time = time();
			update_term_meta($term_id, 'start_service_time', $start_service_time);
		}

		if(empty($end_service_time)){
			$time_end = ($start_service_time - $time_start) + $time_end;
			update_term_meta($term_id, 'end_service_time',$time_end);
		}

		$time_start = get_term_meta_value($term_id,'start_service_time');
		$time_end = get_term_meta_value($term_id,'end_service_time');

		$data['duration_time'] = sprintf('%s - %s',my_date($time_start,'d/m/Y'),my_date($time_end,'d/m/Y'));

		$this->load->library('email');

		$title = 'Thông báo kích hoạt dịch vụ Webdoctor của website '.ucfirst($term->term_name);
		$data['title'] = 'Thông tin kích hoạt dịch vụ';
		$data['term_id'] = $term_id;


		$content_view = 'webgeneral/report/tpl_activation_email';
		// $data['content'] = $this->load->view($content_view,$data,TRUE);

		$content = $this->load->view($content_view,$data,TRUE);
		 
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');

		$mail_report = get_term_meta_value($term_id,'mail_report');
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');



		$mail_cc = array();
		$mail_cc[] = 'support@webdoctor.vn';
		$mail_cc[] = 'quanly@webdoctor.vn';
		if($staff_mail)
			$mail_cc[] = $staff_mail;
		
		$this->email->to($mail_report);
		// $this->email->to('hieupt@webdoctor.vn');
		$this->email->cc($mail_cc);
		$this->email->subject($title);
		$this->email->message($content);

		$send_status = $this->email->send(TRUE);
		return $send_status;
	}

	public function send_tasklist_mail2customer($term_id = 0, $add_mail_cc = false)
	{	
		$term = $this->term_m->get($term_id);
		if(empty($term)) return FALSE;

		$data = array('term'=>$term);

		$time_start = get_term_meta_value($term_id,'contract_begin');
		$start_date = $this->mdate->startOfDay($time_start);

		$time_end = get_term_meta_value($term_id,'contract_end');
		$end_date = $this->mdate->endOfDay($time_end);

		$args = array('order'=>'posts.start_date');
		$data['tasks'] = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('task'),$args);
		if(empty($data['tasks'])) return FALSE;


		$this->load->library('email');

		$title = 'Danh sách công việc dự kiến thực hiện của website '.ucfirst($term->term_name);
		$layout = 'email/blue_tpl';
		$content_view = 'webgeneral/report/tpl_tasklist';
		$data['title'] = 'Danh sách công việc dự kiến thực hiện';
		$data['content'] = $this->load->view($content_view,$data,TRUE);
		$content = $this->load->view($layout,$data,TRUE);
		
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');

		// uncomment real customer
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');

		$mail_report = get_term_meta_value($term_id,'mail_report');
		$this->email->to($mail_report);

		$mail_cc = array('support@webdoctor.vn','quanly@webdoctor.vn');
		if($staff_mail) $mail_cc[] = $staff_mail;
		if($add_mail_cc) $mail_cc[] = $add_mail_cc;
		$this->email->cc($mail_cc);

		$this->email->subject($title);
		$this->email->message($content);

		$send_status = $this->email->send();

		return $send_status;
	}

	public function send_finish_mail2admin($term_id = 0)
    {
        $term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

        $contract_code = get_term_meta_value($term_id, 'contract_code');

        $title = '[WEBDOCTOR.VN] Hợp đồng '.$contract_code.' đã được kết thúc.';
        $content = 'Dear nhóm Webdoctor <br>';

        $end_contract_time = get_term_meta_value($term_id,'end_contract_time');
        $content.= 'Hợp đồng '.$contract_code.' vừa được kết thúc vào lúc <b>'.my_date($end_contract_time,'d-m-Y H:i:s').'</b><br>';

        $this->config->load('table_mail');
        $this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Thông tin hợp đồng');
        $this->table
        ->add_row('Mã hợp đồng:', $contract_code)
        ->add_row('Tên dịch vụ:', 'Dịch vụ Webdoctor');

        $vat = get_term_meta_value($term->term_id,'vat');
        $has_vat = ($vat > 0);
        $this->table->add_row('Loại hình:', ($has_vat ? 'V' : 'NV'));

        $staff_id = get_term_meta_value($term->term_id,'staff_business');
        if(!empty($staff_id)){
            $display_name = $this->admin_m->get_field_by_id($staff_id,'display_name');
            $display_name = empty($display_name) ? $this->admin_m->get_field_by_id($staff_id,'display_name') : $display_name;
            $this->table->add_row('Kinh doanh phụ trách:', "#{$staff_id} - {$display_name}");
        }
        $content.= $this->table->generate();

        $this->table->set_caption('Thông tin khách hàng');
        $content.=
        $this->table
        ->add_row('Người đại diện', get_term_meta_value($term->term_id,'representative_name'))
        ->add_row('Địa chỉ', get_term_meta_value($term->term_id,'representative_address'))
        ->add_row('Website', $term->term_name)
        ->add_row('Mail liên hệ', mailto(get_term_meta_value($term->term_id,'representative_email')))
        ->generate();

        $this->load->library('email');
        $this->email->from('support@webdoctor.vn', 'Webdoctor.vn');

		$mail_report = get_term_meta_value($term_id,'mail_report');
		$this->email->to($mail_report);
		
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');

		$mail_cc = array();
		$mail_cc[] = 'support@webdoctor.vn';
		$mail_cc[] = 'quanly@webdoctor.vn';
		if($staff_mail)
			$mail_cc[] = $staff_mail;
		$this->email->cc($mail_cc);
		$this->email->subject($title);
		$this->email->message($content);

		return $this->email->send(TRUE);
    }

    public function send_tech_kpi_report()
    {
    	$start_time = $this->mdate->startOfDay(strtotime('last monday'));
    	$end_time = $this->mdate->endOfDay(time());

    	$this->load->model('webgeneral/webgeneral_kpi_m');
    	$kpis = $this->webgeneral_kpi_m
    	->select('user_id,term.term_name,term.term_id,term.term_status	')
    	->join('term','term.term_id = webgeneral_kpi.term_id')
    	->where('webgeneral_kpi.kpi_type','tech')
    	->where_in('term.term_status',array('publish','ending','liquidation'))
    	->group_by('user_id,term_name')
    	->get_many_by();

    	if(empty($kpis)) return FALSE;

		$data['total'] = array('terms'=>array());
		foreach ($kpis as $key => $value)
		{
			if(!isset($data[$value->user_id])) 
				$data[$value->user_id] = array('terms'=>array());

			$contract_value = get_term_meta_value($value->term_id,'contract_value');
			$end_service_time = get_term_meta_value($value->term_id,'end_service_time');
			$term = array(
				'term_id' => $value->term_id,
				'term_name' => $value->term_name,
				'contract_value' => $contract_value,
				'package_name' => $this->webgeneral_config_m->get_package_name($value->term_id),	
				'package_label' => $this->webgeneral_config_m->get_package_label($value->term_id)
				);

			$is_term_valid = ($value->term_status == 'publish' || 
				(!empty($end_service_time) && $end_service_time <= $end_time && $end_service_time >= $start_time));

			if($is_term_valid)
				$data[$value->user_id]['terms'][$value->term_id] = $term;

			$data['total']['terms'][$value->term_id] = $term;
    	}

		$this->table->set_heading('STT','Thành viên','Gói','Dự án','Tổng','Tổng giá trị hợp đồng');
		$package_total = array();
		$rows = array();
		$i = 1;	
		foreach ($data as $user_id => $row)
		{
			$user_name = is_numeric($user_id) ? $this->admin_m->get_field_by_id($user_id, 'display_name') : 'Tổng kết';

			$packages = array();
			$count = 0;
			$total = 0;

			foreach ($row['terms'] as $term) 
			{
				$count++;
				$total+= $term['contract_value'];
				$package_name = $this->webgeneral_config_m->get_package_name($term['term_id']);

				if(!isset($packages[$term['package_name']]))
				{
					$packages[$term['package_name']] = 1;
					continue;
				}

				$packages[$term['package_name']]++;
			}

			$row_span = count($packages);
			$split_rowspan_package = array();
			$split_rowspan_number = array();

			foreach ($packages as $key => $value) 
			{
				$split_rowspan_package[] = $key;
				$split_rowspan_number[] = $value;
			}

			$rows[] = array(
					array('rowspan'=>$row_span,'data'=>$i++),
					array('rowspan'=>$row_span,'data'=>$user_name),
					$split_rowspan_package,
					$split_rowspan_number,
					array('rowspan'=>$row_span,'data'=>$count),
					array('rowspan'=>$row_span,'data'=>$total)
				);
		}

		$total_row = array_shift($rows);
		usort($rows, function ($a, $b){
		    if($a[5]['data'] == $b[5]['data']) return 0;
		    return ($a[5]['data'] < $b[5]['data']) ? 1 : -1;
		});
		$rows[] = $total_row;
		
		$no = 0;
		foreach ($rows as $key => $row) 
		{
			$split_count = count($row[2]);
			$row[5]['data'] = currency_numberformat($row[5]['data']);
			$row[0]['data'] = ++$no;
			if($split_count == 1)
			{
				$this->table->add_row($row[0],$row[1],$row[2][0],$row[3][0],$row[4],$row[5]);
				continue;
			}

			$this->table->add_row($row[0],$row[1],$row[2][0],$row[3][0],$row[4],$row[5]);
			for ($i=1; $i < $split_count; $i++)
			{ 
				$this->table->add_row($row[2][$i],$row[3][$i]);
			}
		}

		$title = '[WEBDOCTOR.VN] Thống kế bảng xếp hạng support webdoctor tuần qua từ ngày '.my_date($start_time,'d/m/Y').' đến '.my_date($end_time,'d/m/Y');

        $this->config->load('table_mail');
        $this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Thống kế bảng xếp hạng support webdoctor tuần qua');
        $content = $this->table->generate();
		$this->load->library('email');
		$this->email->from('support@webdoctor.vn','Webdoctor.vn');
		$mail_to = array('luannn@webdoctor.vn');
		$mail_cc = array('kythuat@webdoctor.vn');
		$mail_bcc = array();
		$this->email->to($mail_to);
		$this->email->cc($mail_cc);
		$this->email->bcc($mail_bcc);
		$this->email->subject($title);
		$this->email->message($content);
		$send_status = $this->email->send(TRUE);
    }

    public function send_daily_content_kpi_report($time = FALSE)
    {
		$time = $time ?: $this->mdate->convert_time();

		$kpis = $this->webgeneral_kpi_m
		->where('kpi_datetime',my_date($time,'Y-m-1'))
		->group_by('user_id,term_id,kpi_type')
		->where_in('kpi_type',['content','content_product'])
		->get_many_by(); 

		$month = my_date($time,'m');

		$title = "Báo cáo thống kê KPI team nội dung ngày ".my_date($time)." trong tháng {$month}";

		$this->config->load('table_mail');
		$this->table->set_template($this->config->item('mail_template'));
		$this->table->set_caption("<h2>{$title}</h2>");
		$this->table->set_heading('STT','Thành viên','Số HĐ','Bài viết trong ngày','Tác vụ trong ngày',array('colspan'=>3,'data'=>"KPI bài viết tháng {$month}"),array('colspan'=>3,'data'=>"KPI tác vụ tháng {$month}"),array('colspan'=>3,'data'=>"KPI tổng tháng {$month}"));

		$this->table->add_row(array('colspan'=>3,'data'=>''),array('colspan'=>2,'align'=>'center','data'=>my_date($time)),'Thực hiện','KPI','Tiến độ','Thực hiện','KPI','Tiến độ','Thực hiện','KPI','Tiến độ');

		$i = 0;
		$datatable = array();
		$user_kpis = array_group_by($kpis,'user_id');
		foreach ($user_kpis as $user_id => $items)
		{
			$display_name = $this->admin_m->get_field_by_id($user_id,'display_name');

			$posts = $this->post_m
			->select('post_id,post_type')
			->where('start_date >=',$this->mdate->startOfDay($time))
			->where('start_date <',$this->mdate->endOfDay($time))
			->where('post_author',$user_id)
			->where('post_status','publish')
			->where_in('post_type',[$this->webgeneral_m->get_post_type('content'),$this->webgeneral_m->get_post_type('content_product')])
			->get_many_by();

			$count_posts = array_group_by($posts,'post_type');

			$count_content = count($count_posts[$this->webgeneral_m->get_post_type('content')]??NULL);
			$count_content_product = count($count_posts[$this->webgeneral_m->get_post_type('content_product')]??NULL);

			// Tổng số hợp động đang thực hiện
			$count_terms = count(array_group_by($items,'term_id'));

			$kpi_types = array_group_by($items,'kpi_type');

			// total kpi value each type
			$article_kpi_value = (!(empty($kpi_types['content'])) ? array_sum(array_column($kpi_types['content'],'kpi_value')) : 0);
			$product_kpi_value = (!(empty($kpi_types['content_product'])) ? array_sum(array_column($kpi_types['content_product'],'kpi_value')) : 0);

			// total kpi result each type
			$article_kpi_result = (!(empty($kpi_types['content'])) ? array_sum(array_column($kpi_types['content'],'result_value')) : 0);
			$product_kpi_result = (!(empty($kpi_types['content_product'])) ? array_sum(array_column($kpi_types['content_product'],'result_value')) : 0);

			// progress kpi each type
			$article_progress = div($article_kpi_result,$article_kpi_value);
			$product_progress = div($product_kpi_result,$product_kpi_value);

			$kpi_value = $article_kpi_value + $product_kpi_value;
			$kpi_result = $article_kpi_result + $product_kpi_result;
			$kpi_progress = div($kpi_result,$kpi_value);

			$datatable[] = array(
				'serial'=>++$i,
				'display_name'=>$display_name,
				'count_terms'=>$count_terms,

				'count_content'=>$count_content,
				'count_content_product'=>$count_content_product,

				'article_kpi_result'=>$article_kpi_result,
				'article_kpi_value'=>$article_kpi_value,
				'article_progress'=>currency_numberformat($article_progress*100,'%',2),
				
				'product_kpi_result'=>$product_kpi_result,
				'product_kpi_value'=>$product_kpi_value,
				'product_progress'=>currency_numberformat($product_progress*100,'%',2),
				
				'kpi_result'=>$kpi_result,
				'kpi_value'=>$kpi_value,
				'kpi_progress'=>currency_numberformat($kpi_progress*100,'%',2),
				);
		}

		$total_content_product = array_sum(array_column($datatable, 'count_content'));
		$total_count_content_product = array_sum(array_column($datatable, 'count_content_product'));

		$total_article_kpi_result = array_sum(array_column($datatable, 'article_kpi_result'));
		$total_article_kpi_value = array_sum(array_column($datatable, 'article_kpi_value'));
		$total_article_progress = currency_numberformat(div($total_article_kpi_result,$total_article_kpi_value)*100,'%',2);

		$total_product_kpi_result = array_sum(array_column($datatable, 'product_kpi_result'));
		$total_product_kpi_value = array_sum(array_column($datatable, 'product_kpi_value'));
		$total_product_progress = currency_numberformat(div($total_product_kpi_result,$total_product_kpi_value)*100,'%',2);

		$total_kpi_result = array_sum(array_column($datatable, 'kpi_result'));
		$total_kpi_value = array_sum(array_column($datatable, 'kpi_value'));
		$total_progress = currency_numberformat(div($total_kpi_result,$total_kpi_value)*100,'%',2);

		$datatable[] = array(['colspan'=>3,'data'=>'Tổng'],$total_content_product,$total_count_content_product,$total_article_kpi_result,$total_article_kpi_value,$total_article_progress,$total_product_kpi_result,$total_product_kpi_value,$total_product_progress,$total_kpi_result,$total_kpi_value,$total_progress);

		$content = $this->table->generate($datatable);

		$this->load->library('email');
		$this->email
		->from('support@webdoctor.vn', 'Webdoctor.vn')
		->to('thonh@webdoctor.vn')
		->subject("[WEBDOCTOR.VN]{$title}")
		->message($content)
		->send();
    }
}

/* End of file Webgeneral_report_m.php */
/* Location: ./application/models/webgeneral_report_m.php */