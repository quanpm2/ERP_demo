<?php

if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Webgeneral_config_m extends Post_m {

	public $term_type = 'webgeneral';

	function __construct() 
	{
		parent::__construct();
		$this->load->config('webgeneral/webgeneral');
	}


	function get_package_name($termid_or_name = 0)
	{
		return $this->get_service_value($termid_or_name, 'name', 'Chưa xác định');
	}

	function get_package_label($termid_or_name = 0)
	{
		return $this->get_service_value($termid_or_name, 'label', 'Chưa xác định');
	}

	public function get_price($termid_or_name = 0)
	{
		return $this->get_service_value($termid_or_name, 'price', 0);
	}

	public function get_gift($termid_or_name = 0){
		return $this->get_service_value($termid_or_name, 'gift', 0);
	}

	public function get_appendix($termid_or_name = 0){	
		return $this->get_service_value($termid_or_name, 'appendix', '');
	}

	public function get_service_value($termid_or_name = 0, $key = '', $text_key_none = '')
	{
		if(empty($key))
			return false;
		$term_id = (int)$termid_or_name;

		$package_name = ($term_id == 0) ? $termid_or_name : get_term_meta_value($termid_or_name, 'service_package');

		$config = $this->config->item('packages');
		return isset($config['service'][$package_name][$key]) ? $config['service'][$package_name][$key] : $text_key_none;
	}
}