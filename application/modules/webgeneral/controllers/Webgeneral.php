<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webgeneral extends Admin_Controller {

	public $model = '';
	private $website_id;
	private $term;
	private $term_type;

	function __construct()
	{
		parent::__construct();
			
		$models = array(
			'webgeneral_m',
			'webgeneral_kpi_m',
			'webgeneral_seotraffic_m',
			'webgeneral_callback_m',
			'webgeneral_seotop_m',
			'webgeneral_config_m',
			'webgeneral_overview_m',
			'contract/contract_m');

		$this->load->model($models);
		$this->load->config('webgeneral');
		$this->load->add_package_path(APPPATH.'third_party/google-analytics/');

		$this->term_type = $this->webgeneral_m->term_type;
		$this->init_website();
	}

	private function init_website()
	{
		$term_id = $this->uri->segment(4);
		$method  = $this->uri->segment(3);
		
		$is_allowed_method = (!in_array($method, array('index','done','ajax','statistic','statistics')));
		if(!$is_allowed_method || empty($term_id)) return FALSE;

		$term = $this->term_m
		->where('term_status','publish')
		->where('term_type',$this->term_type)
		->where('term_id',$term_id)
		->get_by();

		if(empty($term) OR !$this->is_assigned($term_id)) 
		{
			$this->messages->error('Truy cập #'.$term_id.' không hợp lệ !!!');
			redirect(module_url(),'refresh');
		}

		$this->template->title->set(strtoupper(' '.$term->term_name));
		$this->website_id = $this->data['term_id'] = $term_id;
		$this->data['term'] = $this->term = $term;
	}

	public function index()
	{
		restrict('Webgeneral.Index.Access');

		$this->search_filter();
		$data = array();
		$time = time();
		$day = date('d',$time) - 1;
		if($day == 0){
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}
		$time = strtotime(date('Y-m-'.$day, $time));
		$day = str_pad($day, 2, "0", STR_PAD_LEFT);
		$start_date = strtotime(date('Y/m/01',$time));
		$end_date = strtotime(date('Y/m/d',$time));

		$this->template->title->append('Tổng quan dịch vụ từ ngày '.date('d/m/Y',$start_date).' đến ngày '.date('d/m/Y',$end_date));

		// Authorization for user role
		if(!has_permission('Webgeneral.Index.Manage') && !has_permission('Webgeneral.Sale.Access') )
		{
			$terms_belongs_user = $this->webgeneral_kpi_m
			->select('term_id,kpi_type')
			->group_start()
				->where('kpi_type','tech')
				->where('user_id',$this->admin_m->id)
			->group_end()
			->or_group_start()
				->where('user_id',$this->admin_m->id)
				->where('kpi_type','content')
				->where('kpi_datetime',my_date($time,'Y-m-1'))
			->group_end()
			->or_group_start()
				->where('user_id',$this->admin_m->id)
				->where('kpi_type','content_product')
				->where('kpi_datetime',my_date($time,'Y-m-1'))
			->group_end()
			// ->group_by('term_id')
			->get_many_by();
 
			if($terms_belongs_user)
			{
				$this->admin_ui->where_in('term_id',array_column($terms_belongs_user, 'term_id'));
				$user_kpi_types = array_group_by($terms_belongs_user,'kpi_type');

				if(!isset($user_kpi_types['content'])) $this->admin_ui->unset_column('content');

				if(!isset($user_kpi_types['content_product'])) $this->admin_ui->unset_column('content_product') ;

				if(!isset($user_kpi_types['banner'])) $this->admin_ui->unset_column('tech');
			}
			else $this->admin_ui->where(1,2);
		}

		$serv_packages = array_map(function($x){ return $x['name']; }, $this->config->item('service','packages'));

		$this->admin_ui
			->set_filter_position(FILTER_TOP_OUTTER)
			->select('term.term_id')

			->add_search('term.term_name',['placeholder'=>'Website'])
			->add_search('service_package',array('content'=> form_dropdown(array('name'=>'where[service_package]','class'=>'form-control select2'),prepare_dropdown($serv_packages,'Gói dịch vụ'),$this->input->get('where[service_package]'))))

			->add_search('action',array('content'=> $this->admin_form->submit('search','Tìm kiếm')))

			->add_column('stt', array('set_select'=> FALSE, 'title'=> 'STT','set_order'=> FALSE))
			->add_column('term.term_name','Website')
			->add_column('content_product', array('set_select'=>FALSE,'title'=>'Sản phẩm','set_order'=>FALSE))
			->add_column('content', array('set_select'=>FALSE,'title'=>'Bài viết','set_order'=>FALSE))
			->add_column('banner', array('set_select'=>FALSE,'title'=>'Thiết kế Banner','set_order'=>FALSE))
			->add_column('service_package', array('set_select'=>FALSE,'title'=>'Gói dịch vụ'))
			->add_column('payment_percentage', array('set_select' => FALSE,'title'=> 'T/đ thanh toán'))
			->add_column('action', array('set_select'=>FALSE, 'title'=>'Actions', 'set_order'=>FALSE))

			->add_column_callback('stt',function($data,$row_name){

				global $stt;
				$data['stt'] = (++$stt);

				$term_id = $data['term_id'];
				
				// Check permission for sale role
				if(has_permission('Webgeneral.Sale.access'))
				{
					$sale_id = get_term_meta_value($term_id,'staff_business');
					if($sale_id != $this->admin_m->id) 
					{
						return FALSE;
					}
				}

				// CALLBACK FOR WEBSITE COLUMN
				$domain = $data['term_name'];

				$data['term_name'] = anchor(module_url("overview/{$term_id}"),$domain,'data-toggle="tooltip" title="Chi tiết '.$domain.'"');

				$package_name = $this->webgeneral_config_m->get_package_label($data['term_id']);
				$data['term_name'].= empty($package_name) ? '' : '<br/>'.$package_name;

				$has_start_service = get_term_meta_value($data['term_id'],'start_service_time');
				$data['term_name'].= empty($has_start_service)?'<br/><span class="label label-danger">Chưa gửi mail kết nối</span>': '' ;

				if(!empty($has_start_service))
				{
					$contract_begin = get_term_meta_value($data['term_id'],'start_service_time');
					$contract_end = get_term_meta_value($data['term_id'],'end_service_time');
					$diff = diffInMonths(time(),$contract_end);

					$label_color = 'label-default';
					($diff == 1 || $diff == 0 ) AND $label_color ='label-warning';
					(time() > $contract_end) AND $label_color ='label-danger';
					
					$data['term_name'].= (($label_color) ? '<br/><span class="label '.$label_color.'">Kết thúc HĐ: '.date('d/m/Y',$contract_end).'</span>' :'');
				}

				return $data;

			},FALSE)

			->add_column_callback('content_product',function($data,$row_name) use ($time,$day,$start_date,$end_date) {

					$term_id = $data['term_id'];

					$dateEnfOfMonth = date('d',$this->mdate->endOfMonth($time));
					$kpi_content = $this->webgeneral_kpi_m->get_kpi_value($term_id,'content_product', $time);
					if(empty($kpi_content)) 
					{
						$data['content_product'] = '<code>Chưa cấu hình KPI</code>';
						return $data;
					}
					
					$content_data_result = $this->webgeneral_kpi_m->get_kpi_result($term_id,'content_product', $time);
					$content = $this->admin_form->progress_bar($content_data_result,$kpi_content);

					$users_kpi = $this->webgeneral_kpi_m->get_kpis($term_id,'users', 'content_product', $time);
					foreach($users_kpi as $user_id => $user_kpi)
					{
						$user_name = $this->admin_m->get_field_by_id($user_id,"display_name");
						$kpi_user = $this->webgeneral_kpi_m->get_kpi_value($term_id,'content_product', $time,$user_id);
						$kpi_user_result = $this->webgeneral_kpi_m->get_kpi_result($term_id,'content_product', $time,$user_id);

						$content.= ''.$user_name.': <span class="pull-right">'.$kpi_user_result.'/'.$kpi_user.'</span><br>';
					}

					$data['content_product'] = $content;

					return $data;
				},FALSE)

			->add_column_callback('content',function($data,$row_name) use ($time,$day,$start_date,$end_date) {

					$term_id = $data['term_id'];

					$dateEnfOfMonth = date('d',$this->mdate->endOfMonth($time));
					$kpi_content = $this->webgeneral_kpi_m->get_kpi_value($term_id,'content', $time);
					if(empty($kpi_content)) 
					{
						$data['content'] = '<code>Chưa cấu hình KPI</code>';
						return $data;
					}
					
					$content_data_result = $this->webgeneral_kpi_m->get_kpi_result($term_id,'content', $time);
					$content = $this->admin_form->progress_bar($content_data_result,$kpi_content);

					$users_kpi = $this->webgeneral_kpi_m->get_kpis($term_id,'users', 'content', $time);
					foreach($users_kpi as $user_id => $user_kpi)
					{
						$user_name = $this->admin_m->get_field_by_id($user_id,"display_name");
						$kpi_user = $this->webgeneral_kpi_m->get_kpi_value($term_id,'content', $time,$user_id);
						$kpi_user_result = $this->webgeneral_kpi_m->get_kpi_result($term_id,'content', $time,$user_id);

						$content.= ''.$user_name.': <span class="pull-right">'.$kpi_user_result.'/'.$kpi_user.'</span><br>';
					}

					$data['content'] = $content;

					return $data;
				},FALSE)

			->add_column_callback('banner',function($data,$row_name){

					$term_id = $data['term_id'];
					$start_time = $this->mdate->startOfMonth();
					$end_time = $this->mdate->endOfMonth();

					$service_package = get_term_meta_value($term_id, 'service_package');

					$author_banners = $this->webgeneral_m
					->select('term_id,post_author,posts.post_title,count(posts.post_id) as number_of_banners')
					->where('posts.post_status','complete')
					->as_array()
					->group_by('post_author')
					->get_posts_from_date($start_time, $end_time,$term_id,'webgeneral-task',array('meta_key'=>'task_type','meta_value'=>'banner'));

					$content_data_result = $author_banners ? array_sum(array_column($author_banners,'number_of_banners')) : 0;

					$kpi_banner = $this->webgeneral_config_m->get_service_value($service_package, 'banner');

					$data['banner'] = $this->admin_form->progress_bar($content_data_result,$kpi_banner);

					if(!empty($author_banners))
					{
						foreach ($author_banners as $author) 
						{
							$user_name = $this->admin_m->get_field_by_id($author['post_author'],"display_name");
							$data['banner'].= ''.$user_name.': <span class="pull-right">'.$author['number_of_banners'].'</span><br>';
						}
					}

					return $data;
				},FALSE)

			->add_column_callback('service_package',function($data,$row_name){
				$data['service_package'] = $this->webgeneral_config_m->get_package_name($data['term_id']);
				$kpis = $this->webgeneral_kpi_m->get_kpis($data['term_id']);

				if(isset($kpis['users']['tech']))
				{
					$data['service_package'].= '<br>';
					foreach($kpis['users']['tech'] as $users)
					{
						foreach($users as $user_id => $vl)
						{
							$data['service_package'].= '<span class="label label-default">'.$this->admin_m->get_field_by_id($user_id,"display_name").'</span><br>';
						}
					}
				}
				return $data;
				},FALSE)

			->add_column_callback('payment_percentage',function($data,$row_name){

				$term_id = $data['term_id'];
				$invs_amount = (double)get_term_meta_value($term_id,'invs_amount');
				$payment_amount = (double)get_term_meta_value($term_id,'payment_amount');

				$payment_percentage = 100 * (double) get_term_meta_value($term_id,'payment_percentage');

				if($payment_percentage >= 50 && $payment_percentage < 80)
					$text_color = 'text-yellow';
				else if($payment_percentage >= 80 && $payment_percentage < 90)
					$text_color = 'text-light-blue';
				else if($payment_percentage >= 90)
					$text_color = 'text-green';
				else $text_color = 'text-red';

				$payment_percentage = currency_numberformat($payment_percentage,' %');
				$data['payment_percentage'] = "<b class='{$text_color}'>{$payment_percentage}</b>";
				
				return $data;

			},FALSE)

			->add_column_callback('action', function($data, $row_name){
				
				$data['action'] = anchor(module_url('overview/'.$data['term_id']),'<i class="fa fa-fw fa-line-chart"></i>','data-toggle="tooltip" title="Chi tiết"');
				
				return $data;	

			}, FALSE)

			->where('term_status','publish')
			->where('term.term_type', $this->term_type)
			->from('term')
			->group_by('term.term_id');

		$data['content'] = $this->admin_ui->generate(array('per_page' => 100,'uri_segment' => 4,'base_url'=> module_url('index/')));

		parent::render($data);
	}

	public function done()
	{
		restrict('Webgeneral.Done.Access');

		$this->search_filter();
		$this->template->title->append('Dịch vụ đã hoàn thành');
		$data = $this->data;

		$this->admin_ui
			->select('term.term_id')
			->add_search('term_name')
			->add_search('service_package',array(
				'content'=> form_dropdown(array('name'=>'where[service_package]',
					'class'=>'form-control select2'), 
					prepare_dropdown(array_map(function($x){ return $x['name']; }, $this->config->item('service','packages'))),
					$this->input->get('where[service_package]'))))		
			->add_search('action',array('content'=> $this->admin_form->submit('search','Tìm kiếm')))
			->add_column('stt', array('set_select'=> FALSE, 'title'=> 'STT','set_order'=> FALSE))
			->add_column_callback('stt',function($data,$row_name){
				global $stt;
				$data['stt'] = (++$stt);
				return $data;
			},FALSE)
			->add_column('term_name','Website')
			->add_column_callback('term_id',function($data,$row_name){

				if(!has_permission('Webgeneral.Sale.access')) return $data;
				$sale_id = get_term_meta_value($data['term_id'],'staff_business');
				if($sale_id != $this->admin_m->id) return FALSE;
				
				return $data;
			},FALSE)
			->add_column('content', array('set_select'=>FALSE,'title'=>'Bài viết','set_order'=>FALSE))
			->add_column('banner', array('set_select'=>FALSE,'title'=>'Thiết kế Banner','set_order'=>FALSE))
			->add_column('service_package', array('set_select'=>FALSE,'title'=>'Gói dịch vụ'))
			->add_column('payment_percentage', array('set_select' => FALSE,'title'=> 'T/đ thanh toán'))
			// ->add_column('action', array('set_select'=>FALSE, 'title'=>'Actions', 'set_order'=>FALSE))
			
			->add_column_callback('term_name',function($data,$row_name){
				
				$term_id = $data['term_id'];
				$text = anchor(module_url("overview/{$term_id}"),$data['term_name'],"data-toggle='tooltip' title='Chi tiết'");
				$data['term_name'] = $text;

				return $data;
			},FALSE)
			->add_column_callback('content',function($data,$row_name){
					
					$term_id = $data['term_id'];
					$start_service_time = get_term_meta_value($term_id,'start_service_time');
					$end_service_time = get_term_meta_value($term_id,'end_service_time');

					$start_service_month = $this->mdate->startOfMonth($start_service_time);
					$end_service_month = $this->mdate->startOfMonth($end_service_time);

					$kpi_content = 0;
					$kpi_content_result = 0;


					while($start_service_month < $end_service_month)
					{
						$kpi_month = (int) $this->webgeneral_kpi_m->get_kpi_value($term_id,'content', $start_service_month);
						$kpi_content+= $kpi_month;

						$kpi_month_result = (int) $this->webgeneral_kpi_m->get_kpi_result($term_id,'content', $start_service_month);
						$kpi_content_result+= $kpi_month_result;

						$start_service_month = strtotime('+1 month',$start_service_month);
					}

					if(empty($kpi_content)) return $data;

					$content = '';
					$percent = div($kpi_content_result,$kpi_content)*100;
					$percent_predict =  $this->webgeneral_seotraffic_m->predict_traffic($kpi_content_result,$start_service_time,$end_service_time,$kpi_content);

					if($percent_predict >= 50 && $percent_predict < 80)
						$progress_color = 'progress-bar-yellow';
					else if($percent_predict >= 80 && $percent_predict < 90)
						$progress_color = 'progress-bar-aqua';
					else if($percent_predict >= 90)
						$progress_color = 'progress-bar-green';
					else $progress_color = 'progress-bar-red';

					$kpi_content_result = numberformat($kpi_content_result);
					$kpi_content = numberformat($kpi_content);
					$data['content'] = '<div class="progress-group">
						<span class="progress-text" data-toggle="tooltip" title="Dự đoán đạt '.numberformat($percent_predict).'%">'.numberformat($percent).'%</span>
						<span class="progress-number"><b><span data-toggle="tooltip" title="Bài viết đã thực hiện">'.$kpi_content_result.'</span></b>/<span data-toggle="tooltip" title="KPI">'.$kpi_content.'</span></span>
						<div class="progress sm">
							<div class="progress-bar '.$progress_color.' progress-bar-striped" style="width: '.$percent.'%"></div>
						</div>
					</div>';

					return $data;
				},FALSE)
			->add_column_callback('banner',function($data,$row_name){

					$term_id = $data['term_id'];
					$start_time = get_term_meta_value($term_id,'start_service_time');
					$end_time = get_term_meta_value($term_id,'end_service_time');
					
					$service_package = get_term_meta_value($term_id, 'service_package');
					
					$author_banners = $this->webgeneral_m
					->select('term_id,post_author,posts.post_title,count(posts.post_id) as number_of_banners')
					->where('posts.post_status','complete')
					->as_array()
					->group_by('post_author')
					->get_posts_from_date($start_time, $end_time,$term_id,'webgeneral-task',array('meta_key'=>'task_type','meta_value'=>'banner'));

					$content_data_result = $author_banners ? array_sum(array_column($author_banners,'number_of_banners')) : 0;

					$kpi_banner = $this->webgeneral_config_m->get_service_value($service_package, 'banner');
					$percent = ($kpi_banner == 0) ? 0 : (($content_data_result / $kpi_banner) * 100);
					$percent_predict =  $this->webgeneral_seotraffic_m->predict_traffic($content_data_result,$start_time,$end_time,$kpi_banner);

					if($percent_predict >= 50 && $percent_predict < 80)
						$progress_color = 'progress-bar-yellow';
					else if($percent_predict >= 80 && $percent_predict < 90)
						$progress_color = 'progress-bar-aqua';
					else if($percent_predict >= 90)
						$progress_color = 'progress-bar-green';
					else $progress_color = 'progress-bar-red';

					$content_data_result = numberformat($content_data_result);
					$kpi_banner = numberformat($kpi_banner);
					$banner_content = '<div class="progress-group">
						<span class="progress-text" data-toggle="tooltip" title="Dự đoán đạt '.numberformat($percent_predict).'%">'.numberformat($percent).'%</span>
						<span class="progress-number"><b><span data-toggle="tooltip" title="Banner đã thực hiện">'.$content_data_result.'</span></b>/<span data-toggle="tooltip" title="KPI">'.$kpi_banner.'</span></span>
						<div class="progress sm">
							<div class="progress-bar '.$progress_color.' progress-bar-striped" style="width: '.$percent.'%"></div>
						</div>
					</div>';

					if(!empty($author_banners))
					{
						foreach ($author_banners as $author) 
						{
							$user_name = $this->admin_m->get_field_by_id($author['post_author'],"display_name");
							$banner_content.= ''.$user_name.': <span class="pull-right">'.$author['number_of_banners'].'</span><br>';
						}
					}
					
					$data['banner'] = $banner_content;

					return $data;
				},FALSE)
			->add_column_callback('service_package',function($data,$row_name){
				$data['service_package'] = $this->webgeneral_config_m->get_package_name($data['term_id']);
				$kpis = $this->webgeneral_kpi_m->get_kpis($data['term_id']);

				if(isset($kpis['users']['tech']))
				{
					$data['service_package'].= '<br>';
					foreach($kpis['users']['tech'] as $users)
					{
						foreach($users as $user_id => $vl)
						{
							$data['service_package'].= '<span class="label label-default">'.$this->admin_m->get_field_by_id($user_id,"display_name").'</span><br>';
						}
					}
				}
				return $data;
				},FALSE)
			->add_column_callback('payment_percentage',function($data,$row_name){
				$term_id = $data['term_id'];
				$invs_amount = (double)get_term_meta_value($term_id,'invs_amount');
				$payment_amount = (double)get_term_meta_value($term_id,'payment_amount');

				$payment_percentage = 100 * (double) get_term_meta_value($term_id,'payment_percentage');

				if($payment_percentage >= 50 && $payment_percentage < 80)
					$text_color = 'text-yellow';
				else if($payment_percentage >= 80 && $payment_percentage < 90)
					$text_color = 'text-light-blue';
				else if($payment_percentage >= 90)
					$text_color = 'text-green';
				else $text_color = 'text-red';

				$payment_percentage = currency_numberformat($payment_percentage,' %');
				$data['payment_percentage'] = "<b class='{$text_color}'>{$payment_percentage}</b>";
				return $data;
			},FALSE)
			// ->add_column_callback('action', function($data, $row_name){
			// 	$data['action'] = anchor(module_url('overview/'.$data['term_id']),'<i class="fa fa-fw fa-line-chart"></i>','data-toggle="tooltip" title="Xem chi tiết"');
			// 	return $data;	
			// 	}, FALSE)
			->where('term_status','ending')
			->where('term.term_type', $this->term_type)
			->from('term')
			->group_by('term.term_id');

		$data['content'] = $this->admin_ui->generate(['per_page' => 100,'uri_segment' => 4,'base_url'=> module_url('all_done')]);

		parent::render($data,'blank');
	}

	public function kpi($term_id = 0, $delete_id= 0)
	{
		restrict('Webgeneral.Kpi.Access');
		
		$this->template->title->prepend('KPI');
		$this->load->config('group');
		$data = $this->data;
		$this->submit_kpi($term_id,$delete_id);

		$data['kpi_type'] = $this->config->item('services');

		$data['kpi_desc'] = array('seotraffic'=>'Điền KPI cần đạt', 'content'=>'Số lượng bài viết');
		$data['term_id']  = $term_id;

		$data['time'] 	  = $this->mdate->startOfMonth(time());
		$time_start 	  = get_term_meta_value($term_id, 'contract_begin');
		$time_end 		  = get_term_meta_value($term_id, 'contract_end');

		//+2 month
		$time_start = strtotime('-1 month');
		$time_end = strtotime('+2 month', $time_end);

		$data['time_start'] = $time_start;
		$data['time_end'] = $time_end;
		$data['time_start'] = $this->mdate->startOfDay($data['time_start']);
		$data['time_end'] = $this->mdate->endOfDay($data['time_end']);

		$targets = $this->webgeneral_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(array(
			'term_id'=>$term_id
			));

		$data['targets'] 					= array();
		$data['targets']['content'] 		= array();
		$data['targets']['content_product'] = array();
		$data['targets']['seotraffic'] 		= array();
		$data['targets']['tech'] 			= array();
		$data['targets']['backlink'] 		= array();
		$data['targets']['keyword'] 		= array();
		foreach($targets as $target)
		{
			$data['targets'][$target['kpi_type']][] = $target;
		}

		if(is_module_active('backlink'))
		{	
			$this->load->model('backlink/backlink_kpi_m');
			$this->load->model('backlink/keyword_kpi_m');
			$backlink_targets = $this->backlink_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(array('term_id'=>$term_id));
			if(!empty($backlink_targets))
				foreach ($backlink_targets as $target)
					$data['targets'][$target['kpi_type']][] = $target;
		}
		
		$data['date_contract'] = array();
		for($i=$data['time_start'];$i< $data['time_end']; $i++)
		{
			$data['date_contract'][$time_start]  = date('Y-m', $time_start);
			$date = $data['date_contract'][$time_start];

			$time_start = strtotime('+1 month', strtotime($date));
			if($time_start > $time_end)
				break;
		}

		$data['users'] = $this->admin_m->select('user_id,display_name')->where_in('role_id', $this->config->item('group_memberId'))->set_get_active()->order_by('display_name')->get_many_by();
		$data['users'] = key_value($data['users'],'user_id','display_name');

		$this->data = $data;

		parent::render($this->data);
	}

	public function submit_kpi($term_id = 0,$delete_id=0)
	{
		
		if($delete_id > 0)
		{
			$this->_delete_kpi($term_id,$delete_id);
			redirect(module_url("kpi/{$term_id}"),'refresh');
		}

		$post = $this->input->post();

		if(empty($post)) return FALSE;

		if($this->input->post('submit_kpi_backlink') !== NULL)
		{
			$kpi_datetime 		= $this->input->post('target_date');
			$user_id = $this->input->post('user_id');
			$kpi_value = (int)$this->input->post('target_post');
			$kpi_type = $this->input->post('target_type');

			Modules::run('backlink/update_kpi',$term_id,$kpi_datetime,$user_id,$kpi_value,$kpi_type);
			redirect(current_url(),'refresh');
		}

		if( ! $this->webgeneral_m->has_permission($term_id, 'Webgeneral.kpi','add'))
		{	
			$this->messages->error('Không có quyền thực hiện tác vụ này.');
			redirect(module_url("kpi/{$term_id}"),'refresh');
		}

		$insert = array();
		$insert['kpi_datetime'] = $this->input->post('target_date');
		$insert['user_id'] = $this->input->post('user_id');
		$insert['kpi_value'] = (int)$this->input->post('target_post');

		$kpi_type = $this->input->post('target_type');
		if(empty($kpi_type))
		{
			$is_submit_content 			= $this->input->post('submit_kpi_content');

			$is_submit_content_product  = $this->input->post('submit_kpi_content_product');

			if(!empty($is_submit_content_product)) { 
				$kpi_type               = 'content_product' ;
			}
			
			if(!empty($is_submit_content)) { 
				$kpi_type = ($is_submit_content) ? 'content' : 'seotraffic';
			}			
		}

		if($insert['kpi_datetime'] >= $this->mdate->startOfMonth())
		{
			$this->webgeneral_kpi_m->update_kpi_value($term_id, $kpi_type, $insert['kpi_value'], $insert['kpi_datetime'], $insert['user_id']);

			$this->webgeneral_kpi_m->delete_cache($delete_id);
			$this->messages->success('Cập nhật thành công');
		}
		else
		{
			$this->messages->error('Cập nhật không thành công do tháng cập nhật nhỏ hơn tháng hiện tại');
		}

		redirect(current_url(),'refresh');
	}

	private function _delete_kpi($term_id = 0,$delete_id=0)
	{
		if( ! $this->webgeneral_m->has_permission($term_id, 'Webgeneral.kpi','delete',$kpi_type = ''))
		{	
			$this->messages->error('Không có quyền thực hiện tác vụ này.');
			return FALSE;
		}

		$_tmp_model = 'webgeneral_kpi_m';
		$is_backlink_kpi = $this->input->get('kpi_type') == 'backlink';
		if($is_backlink_kpi){
			$_tmp_model = 'backlink_kpi_m';
			$this->load->model('backlink/backlink_kpi_m');
		}

		$check = $this->{$_tmp_model}->get($delete_id);
		$is_deleted = false;
		if($check)
		{
			if(strtotime($check->kpi_datetime) >= $this->mdate->startOfMonth())
			{
				$this->{$_tmp_model}->delete_cache($delete_id);
				$this->{$_tmp_model}->delete($delete_id);
				$is_deleted = true;
				$this->messages->success('Xóa thành công');
			}
		}
		($is_deleted) OR $this->messages->error('Xóa không thành công');
	}

	/**
	 * The page mapping contract with staffs
	 * @return void
	 */
	public function staffs()
	{
		restrict('Webgeneral.staffs');
		$this->template->title->prepend('Bảng phân công phụ trách thực hiện dịch vụ Web Tổng Hợp');
		$data = $this->data;

		$data['content'] =
		$this->admin_ui	
		->select('term.term_id')
		->add_column('stt', array('set_select'=> FALSE, 'title'=> 'STT', 'set_order'=> FALSE))
		->add_column_callback('stt',function($data,$row_name){
			global $stt;
			$data['stt'] = (++$stt);
			return $data;
		},FALSE)
		->add_column('term.term_name',array('title'=>'Website','set_order'=>FALSE))
		->add_column_callback('term_name',function($data,$row_name){

			$term_id = $data['term_id'];
			$domain = $data['term_name'] ?: '<code>Chưa có website</code>';
			$data['term_name'] = anchor(module_url("kpi/{$term_id}"),$domain);

			return $data;
		},FALSE)
		->add_column('start_service_time', array('set_select'=>FALSE,'title'=>'T/G kích hoạt','set_order'=>FALSE))
		->add_column_callback('start_service_time',function($data,$row_name){
			$start_service_time = get_term_meta_value($data['term_id'],'start_service_time');
			$data['start_service_time'] = my_date($start_service_time);
			return $data;
		},FALSE)
		->add_column('tech_staff', array('set_select'=>FALSE,'title'=>'Phụ trách','set_order'=>FALSE))
		->add_column_callback('tech_staff',function($data,$row_name){

			$term_id = $data['term_id'];
			$kpis = $this->webgeneral_kpi_m->get_kpis($term_id,'users','tech');
			if(empty($kpis)) return $data;

			$tech_staff = '';
			$user_ids = array();

			foreach ($kpis as $uids) 
			{
				foreach ($uids as $i => $val) 
				{
					$user_ids[$i] = $i;
				}
			}

			$data['tech_staff'] = implode(br(), array_map(function($user_id){
				$display_name = $this->admin_m->get_field_by_id($user_id,'display_name');
				return $display_name;

				$email_parts = explode('@', $email);
				return reset($email_parts);

			}, $user_ids));

			return $data;
		},FALSE)
		->add_column('kpi_content', array('set_select'=>FALSE,'title'=>'Nội dung','set_order'=>FALSE))
		->add_column_callback('kpi_content',function($data,$row_name){

			$term_id = $data['term_id'];
			$kpis = $this->webgeneral_kpi_m->get_kpis($term_id,'users','content');
			if(empty($kpis)) return $data;
			$kpi_content = '';
			$user_ids = array();

			foreach ($kpis as $uids) 
			{
				foreach ($uids as $i => $val) 
				{
					$user_ids[$i] = $i;
				}
			}

			$data['kpi_content'] = implode(br(), array_map(function($user_id){
				$display_name = $this->admin_m->get_field_by_id($user_id,'display_name');
				return $display_name;	

			}, $user_ids));

			return $data;
		},FALSE)
		->add_column('sale_staff', array('set_select'=>FALSE,'title'=>'Kinh doanh','set_order'=>FALSE))
		->add_column_callback('sale_staff',function($data,$row_name){
			$staff_id = get_term_meta_value($data['term_id'],'staff_business');
            $sale_staff = $this->admin_m->get_field_by_id($staff_id,'display_name') ?: $this->admin_m->get_field_by_id($staff_id,'user_email');
            $data['sale_staff'] = $sale_staff ?: '<code>---</code>';
			return $data;
		},FALSE)
		->where('term.term_type', 'webgeneral')
		->where_in('term.term_status',array('publish'))
		->from('term')
		->generate(array('per_page' => 10000,'uri_segment' => 4,'base_url'=> module_url('statistic')));

		$this->data = $data;
		parent::render($this->data, 'blank');
	}

	public function setting($term_id = 0)
	{
		restrict('Webgeneral.setting');
		$this->template->title->prepend('Cấu hình');
		$this->setting_submit($term_id);

		$data = $this->data;
		$data['term'] = $this->term;
		$data['term_id'] = $term_id;
		$data['meta']['phone_report'] = get_term_meta_value($term_id, 'phone_report');
		$data['meta']['mail_report'] = get_term_meta_value($term_id, 'mail_report');
		$data['ga_accounts'] = $this->webgeneral_seotraffic_m->reload_account_ga();

		$this->data = $data;
		parent::render($this->data);
	}

	protected function setting_submit($term_id = 0)
	{
		$post = $this->input->post();
		if(empty($post)) return FALSE;

		if( ! empty($post['start_process']))
		{
			if( ! $this->webgeneral_m->has_permission($term_id, 'Webgeneral.start_service'))
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$term = $this->term_m->get($term_id);

			$this->load->model('webgeneral_contract_m');
			$this->webgeneral_contract_m->proc_service($term);
			$this->messages->success('Cập nhật thành công');

			/* Phân tích hợp đồng ký mới | tái ký */
			$this->load->model('contract/base_contract_m');
			$this->base_contract_m->detect_first_contract($term_id);


			/* Gửi SMS thông báo kích hoạt hợp đồng đến khách hàng */
			$this->load->model('contract/contract_report_m');
			$this->contract_report_m->send_sms_activation_2customer($term_id);


			redirect(module_url("setting/{$term_id}"),'refresh');
		}
		else if( ! empty($post['end_process']))
		{
			if( ! $this->webgeneral_m->has_permission($term_id, 'Webgeneral.stop_service'))
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$term = $this->term_m->get($term_id);

			$this->load->model('webgeneral_contract_m');
			$stat = $this->webgeneral_contract_m->stop_service($term);
			if($stat) $this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
		else if(!empty($post['submit']))
		{
			$metadatas = $post['meta'];

			if( ! $this->webgeneral_m->has_permission($term_id, 'Webgeneral.setting','update'))
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$metadatas = $post['meta'];
			if($metadatas)
			{
				foreach($metadatas as $key=>$value)
				{
					if($key == 'report_key' && $value == '')
					{
						$value = $this->webgeneral_seotraffic_m->encrypt('',$term_id);
					}

					update_term_meta($term_id,$key,$value);
				}
			}

			$this->messages->success('Cập nhật thành công');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
	}

	function content($term_id = 0)
	{
		restrict('Webgeneral.Content');

		if($this->input->post())
		{
			Modules::run('webgeneral/tasks/insert_task',$term_id, $this->webgeneral_m->get_post_type('content'));
		}

		$this->template->title->prepend('Bài viết');
		$data = $this->data;

		$this->admin_ui
		->add_column('posts.post_id','#')
		->add_column('post_title','Tiêu đề')
		->add_column('post_content', 'Nội dung')
		->add_column_callback(array('post_content','post_author','type'),array($this->webgeneral_callback_m,'post_get_meta'), FALSE)
		->add_column('type', array('set_select'=>FALSE,'title'=>'Loại','set_order'=>FALSE))
		->add_column('post_author', 'Người tạo')
		->add_column('start_date','Ngày thêm','$1','date("d/m/Y",start_date)')
		->add_column('view', array('set_select'=>FALSE, 'title' =>'', 'set_order'=>FALSE))
		->add_column_callback('view',function($data,$row_name){

				$post_id = $data['post_id'];
				$view = '';

				if(has_permission('Webgeneral.Content.Update'))
				{
					$view.= anchor(module_option_url('webdoctor',"tasks/edit/{$post_id}"),'<i class="fa fa-fw fa-edit"></i>',"title='Cập nhật' class='btn btn-default btn-xs ajax_edit' data-taskid='{$post_id}'");
				}

				if(has_permission('Webgeneral.Content.Delete'))
				{
					$view.= form_button('','<i class="fa fa-fw fa-trash"></i>',"title='Xóa' class='btn btn-default btn-xs ajax_delete' data-taskid='{$post_id}' data-toggle='confirmation'");
				}

				$data['view'] = $view;
				return $data;
			},FALSE)
		->where('post_type', $this->webgeneral_m->get_post_type('content'))
		->where('term_id',$term_id)
		->order_by("posts.post_id desc")
		->from('posts')
		->join('term_posts', 'term_posts.post_id = posts.post_id');

		// Add filter for user with no manage permission
		if( ! has_permission('Webgeneral.Content.Manage')) 
		{
			$this->admin_ui->where('posts.post_author',$this->admin_m->id);
		}

		$data['content'] = $this->admin_ui->generate();

		$this->data = $data;
		parent::render($this->data);
	}

	// code by tambt@webdoctor.vn
	function content_product($term_id = 0)
	{
		restrict('Webgeneral.content_product');
		
		if($this->input->post())
		{
			Modules::run('webgeneral/tasks/insert_task', $term_id, $this->webgeneral_m->get_post_type('content_product'));
		}

		$this->template->title->prepend('Sản phẩm');
		$data = $this->data;

		$this->admin_ui
		->add_column('posts.post_id','#')
		->add_column('post_title','Tiêu đề')
		->add_column('post_content', 'Nội dung')
		->add_column_callback(array('post_content','post_author','type'),array($this->webgeneral_callback_m,'post_get_meta'), FALSE)
		->add_column('type', array('set_select'=>FALSE,'title'=>'Loại','set_order'=>FALSE))
		->add_column('post_author', 'Người tạo')
		->add_column('start_date','Ngày thêm','$1','date("d/m/Y",start_date)')
		->add_column('view', array('set_select'=>FALSE, 'title' =>'', 'set_order'=>FALSE))
		->add_column_callback('view',function($data,$row_name){

				$post_id = $data['post_id'];
				$view = '';

				if(has_permission('Webgeneral.Content_product.Update'))
				{
					$view.= anchor(module_option_url('webdoctor',"tasks/edit/{$post_id}"),'<i class="fa fa-fw fa-edit"></i>',"title='Cập nhật' class='btn btn-default btn-xs ajax_edit' data-taskid='{$post_id}' target='_blank'");
				}

				if(has_permission('Webgeneral.Content_product.Delete'))
				{
					$view.= form_button('','<i class="fa fa-fw fa-trash"></i>',"title='Xóa' class='btn btn-default btn-xs ajax_delete' data-taskid='{$post_id}' data-toggle='confirmation'");
				}

				$data['view'] = $view;
				return $data;
			},FALSE)
		->where('post_type', $this->webgeneral_m->get_post_type('content_product'))
		->where('term_id',$term_id)
		->order_by("posts.post_id desc")
		->from('posts')
		->join('term_posts', 'term_posts.post_id = posts.post_id');
		
		// Add filter for user with no manage permission
		if( ! has_permission('Webgeneral.Content_product.Manage')) 
		{
			$this->admin_ui->where('posts.post_author',$this->admin_m->id);
		}

		$data['content'] = $this->admin_ui->generate();

		$this->data = $data;
		parent::render($this->data);
	}

	public function seotraffic($term_id = 0)
	{
		$this->template->javascript->add('plugins/chartjs/Chart.js');
		$data = array();
		
		$time = time();
		$day = date('d',$time) - 1;
		if($day == 0)
		{
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}
		$start_date = date('Y-m-01',$time);

		$day = str_pad($day, 2, "0", STR_PAD_LEFT);
		$end_date = date('Y-m-'.$day, $time);
		$dayEndOfMonth = date("Y-m-t", $time);
		$dayEndOfMonth = $this->mdate->endOfDay($dayEndOfMonth);
		$ga_profile_id = $this->termmeta_m->get_meta_value($term_id, 'ga_profile_id');
		$kpi = $this->webgeneral_kpi_m->get_kpi_value($term_id,'seotraffic', $time);

		$kpi = $kpi / date('d',$dayEndOfMonth);
		$kpi = numberformat($kpi);
		$ga_data = $this->webgeneral_seotraffic_m->get_ga($start_date, $end_date,$ga_profile_id);
		if(!$ga_data)
		{
			parent::render($data,'blank');
			return true;
		}
		$charData = array();
		$charData['ga:sessions'] = array();
		$charData['Organic Search'] = array();
		$charData['kpi'] = array();

		foreach($ga_data['headers'] as $header) {
			$charData['ga:sessions'][] = (isset($ga_data['results'][$header]['ga:sessions']) ? $ga_data['results'][$header]['ga:sessions'] : 0);

			$charData['Organic Search'][] = (isset($ga_data['results'][$header]['Organic Search']) ? $ga_data['results'][$header]['Organic Search'] : 0);
			$charData['kpi'][] = $kpi;

		}
		$charData['ga:sessions'] = implode(',', $charData['ga:sessions']);
		$charData['Organic Search'] = implode(',', $charData['Organic Search']);
		$charData['kpi'] = implode(',', $charData['kpi']);
		
		$charDataHeader = array();
		foreach($ga_data['headers'] as $header)
		{
			$charDataHeader[] = date('d/m', strtotime($header));
		}

		$data['charDataHeader'] = implode('","', $charDataHeader);
		$data['charDataHeader'] = '"'.$data['charDataHeader'].'"';

		$data['charData'] = $charData;
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		$this->template->title->prepend('Tổng quan ');
		$this->template->title->append(' tháng '.date('m/Y',$time));
		parent::render($data);
	}

	public function seotop($term_id = 0)
	{
		$this->template->title->prepend('SEO từ khóa');
		$data = $this->data;
		$this->load->model('webgeneral_keyword_rank_m');
		$data['term_id'] = $term_id;

		$time_start = '2016/03/01';
		$time_end = '2016/03/30';

		$data['time_start'] = strtotime('2016/02/26');
		$data['time_end'] = $this->mdate->endOfMonth();
		$data['kpi'] = $this->webgeneral_kpi_m->get_kpi_value($term_id, 'seotop',time());

		$data['keywords'] = $this->webgeneral_kpi_m->where(array(
			'term_id' =>$term_id,
			'kpi_datetime >=' =>$time_start,
			'kpi_datetime <=' =>$time_end,
			'kpi_type' =>'keyword'
			))->get_many_by();

		$data['dates'] = array('week-1'=>'Tuần 1', 'week-2'=>'Tuần 2', 'week-3'=> 'Tuần 3', 'week-4'=>'Tuần 4');

		$this->data = $data;
		$this->render($this->data);
	}

	public function task($term_id = 0)
	{
		restrict('Webgeneral.Task.Access');

		if($this->input->post())
		{
			Modules::run('webgeneral/tasks/insert_task',$term_id, $this->webgeneral_m->get_post_type('task'));
		} 

		$this->template->title->prepend('Công việc');
		$data = $this->data;
		$this->admin_ui->select('end_date');
		$this->admin_ui->add_column('posts.post_id','#');
		$this->admin_ui->add_column('post_title','Tiêu đề', null,null, array('class' =>'col-md-3'));
		// $this->admin_ui->add_column('post_content','Nội dung', NULL, NULL, array('class' =>'col-md-4'));
		$this->admin_ui->add_column('post_author', 'Người tạo');
		$this->admin_ui->add_column('type', array('set_select'=>FALSE,'set_order'=>FALSE, 'title' =>'Loại'), null,null, array('class' =>'col-md-1'));
		$this->admin_ui->add_column_callback('type',function($data, $row_name){
			$type = get_post_meta_value($data['post_id'],'task_type');
			$data['type'] = $this->config->item($type,'tasks_type');
			return $data;
		}, FALSE);
		$this->admin_ui->add_column('sms', array('set_select'=>FALSE,'set_order'=>FALSE, 'title' =>'Gửi SMS'), null,null, array('class' =>'col-md-1'));
		$this->admin_ui->add_column('post_status', 'Trạng thái');
		$this->admin_ui->add_column_callback('post_status',function($data, $row_name){
			$status = array();
			$status['process'] = '';
			$status['complete'] = '';

			switch ($data['post_status']) {

				case 'complete':
				$status['complete'] = 'checked disabled';

				case 'process':
				$status['process'] = 'checked disabled';
				break;
			}

			$data['post_status'] = '<span class="checkbox"><label><input type="checkbox" class="minimal" data-taskid="'.$data['post_id'].'" data-status="process" '.$status['process'].'> Đang thực hiện</label></span>
			<span class="checkbox"><label><input type="checkbox" class="minimal" data-taskid="'.$data['post_id'].'" data-status="complete" '.$status['complete'].'> Hoàn thành </label></span>';
			return $data;
		}, FALSE, array('class' =>'col-md-2'));

		$this->admin_ui->add_column_callback('sms',function($data, $row_name){

			$is_send = $this->postmeta_m->get_meta_value($data['post_id'],'sms_status');

			if($is_send == 2)
			{
				$data['sms'] = '<span class="label label-success">Đã gửi</span>';
				$data['view'] = '';
			}
			else
			{
				$checked = ($is_send) ? 'checked' : '';
				$checked.= ($data['post_status'] =='complete') ? ' disabled' : '';
				$data['sms'] = '
				<span class="checkbox"><label><input type="checkbox" class="minimal" data-taskid="'.$data['post_id'].'"  data-toggle="confirmation" data-status="send_sms" '.$checked.'> SMS</label></span>';
			}

			return $data;
		}, FALSE);

		$this->admin_ui->add_column('start_date','Ngày thực hiện','$1 - $2','date("d/m/Y",start_date),date("d/m/Y",end_date)',  array('class' =>'col-md-2'));
		// $this->admin_ui->add_column('end_date','Ngày kt','$1','date("d/m/Y",end_date)');

		$this->admin_ui->where('post_type', $this->webgeneral_m->get_post_type('task'));
		$this->admin_ui->where('term_id',$term_id);
		$this->admin_ui->order_by("start_date asc, post_status asc");
		$this->admin_ui->from('posts');
		$this->admin_ui->join('term_posts', 'term_posts.post_id = posts.post_id');

		$this->admin_ui->add_column_callback(array('post_content','post_author'),array($this->webgeneral_callback_m,'post_get_meta'), FALSE);
		$this->admin_ui->add_column('view', array('set_select'=>FALSE, 'title' =>'Xem'),anchor(module_option_url('webdoctor','tasks/edit/$1'),'Edit','class="ajax_edit" data-taskid="$1"').' | '.anchor(module_option_url('webdoctor','tasks/edit/$1'),'Xóa','class="ajax_delete" data-taskid="$1"'),'post_id', array('class' =>'col-md-1'));

		$this->admin_ui->add_column_callback('view',function($data,$row_name){

				$post_id = $data['post_id'];
				$view = '';

				if(has_permission('Webgeneral.Task.Update'))
				{
					$view.= anchor(module_option_url('webdoctor',"tasks/edit/{$post_id}"),'<i class="fa fa-fw fa-edit"></i>',"title='Cập nhật' class='btn btn-default btn-xs ajax_edit' data-taskid='{$post_id}' target='_blank'");
				}

				if(has_permission('Webgeneral.Task.Delete'))
				{
					$view.= anchor(module_option_url('webdoctor',"tasks/edit/{$post_id}"),'<i class="fa fa-fw fa-trash"></i>',"title='Xóa' class='btn btn-default btn-xs ajax_delete' data-taskid='{$post_id}' data-toggle='confirmation' target='_blank'");
				}

				$data['view'] = $view;
				return $data;
			},FALSE);

		// Add filter for user with no manage permission
		if( ! has_permission('Webgeneral.Task.Manage')) 
		{
			// $this->admin_ui->where('posts.post_author',$this->admin_m->id);
			$this->admin_ui->where_in('posts.post_author',array($this->admin_m->id,1)); // #1- Hệ thống
		}

		$data['content'] = $this->admin_ui->generate();

		$this->data = $data;
		parent::render($this->data);
	}
	
	function insert_demo($term_id = 0)
	{
		$time = strtotime('-1 month');
		$this->webgeneral_m->init_job_default($term_id,$time);
		$this->data['content'] = 'Thêm thành công';
		parent::render($this->data,'blank');
	}

	public function clear($term_id = 0)
	{
		$posts = $this->post_m->where_in('post_status',array('publish','complete','process'))->get_posts(array(
			'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
			'tax_query'=> array(
				'taxonomy' => $this->webgeneral_m->term_type,
				'terms' => $term_id,
				),
			'where' => array(
				'end_date >' => 1
				),
			'post_type' => $this->webgeneral_m->get_post_type('task')
			));

		$title = '';
		if($posts)
		{
			foreach($posts as $post)
			{

				$title.=$post->post_title.'<br>';
				$this->post_m->delete_post($post->post_id);
			}
		}


		$this->data['content'] = 'Xóa thành công<br>'.$title;
		parent::render($this->data,'blank');
	}

	public function email($term_id = 0)
	{
		$this->_construct();
	}

	private function _construct()
	{
		$this->template->title->prepend('Chức năng đang được xây dựng');
		$this->data['content'] = 'Chức năng này đang được xây dựng';
		$data = $this->data;
		$this->data = $data;
		parent::render($this->data,'blank');
	}

	public function logreport($term_id = 0, $is_send = 0)
	{
		$this->template->title->prepend('Các báo cáo đã gửi');
		$data = $this->data;
		$this->load->model('webgeneral_contract_m');

		// $this->config->load('table_mail');
		// $this->table->set_template($this->config->item('mail_template'));
		// // prd($this->config->item('mail_template'));
		// $this->table->set_heading('xxx');
		// $this->table->set_caption('<b>HTlove</b>');
		// $this->table->add_row('xxx');
		// $this->table->add_row('yyy');

		// echo  $this->webgeneral_contract_m->send_mail2admin($term_id,$is_send);
		// $this->load->view();
		$this->data = $data;
		parent::render($this->data, 'blank');
	}

	function asendmail()
	{

		$this->load->library('email');
		$this->email->set_debug(false);
		$this->email->from('hieupt@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to('hieupt@webdoctor.vn');
		$this->email->cc('luannn@webdoctor.vn');
		$this->email->subject('Send mail'.date('d/m/Y H:i:s'));
		$this->load->model('webgeneral_report_m');
		$content = $this->webgeneral_report_m->send_mail_custom(532,strtotime('2016/08/01'), strtotime('2016/08/10'), true);
		// prd();
		// $content = '123';
		$this->email->message($content);
		$send_status = $this->email->send();
		echo $content;die();
		// var_dump($send_status);
		prd();
		echo $this->email->print_debugger();
	}
	
	public function overview($term_id = 0)
	{
		restrict('Webgeneral.Overview.Access');

		$term = $this->term_m->get($term_id);
		if(empty($term)){
			$this->messages->error('Dịch vụ không tồn tại');
			redirect(module_url(),'refresh');
		}

		$time = time();	
		$day = date('d',$time) - 1;
		if($day == 0){
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}
		$date_start = date('01-m-Y',$time);
		$day = str_pad($day, 2, "0", STR_PAD_LEFT);
		$date_end = date($day.'-m-Y', $time);

		$description = 'Từ '.$date_start.' đến '.$date_end;

		$time_start  = $this->mdate->startOfDay(strtotime($date_start));
		$time_end    = $this->mdate->endOfDay(strtotime($date_end));
	

		$this->template->description->append($description);
		$this->template->title->prepend('Tổng quan');
		$this->template->javascript->add('plugins/chartjs/Chart.js');

		$statistic_index_data = $this->webgeneral_overview_m->get_statistic_index_data($term,$time_start,$time_end);

		$char_data = $this->webgeneral_overview_m->build_seotraffic_chart($term_id,my_date($time_start,'Y-m-d'),my_date($time_end,'Y-m-d'));
		$content_data = $this->webgeneral_overview_m->get_content_table($term_id,$time_start,$time_end);
		$content_product_data = $this->webgeneral_overview_m->get_content_table($term_id,$time_start,$time_end, 'content_product');

		$task_data = $this->webgeneral_overview_m->get_tasklist_table($term_id,$time_start,$time_end);
		$email_report_log = $this->webgeneral_overview_m->get_email_log_table($term_id,$time_start,$time_end);
		$sms_report_log = $this->webgeneral_overview_m->get_sms_log_table($term_id,$time_start,$time_end);
		$customer_table = $this->webgeneral_overview_m->get_customer_info($term_id);

		$data = array_merge_recursive($statistic_index_data,$char_data,$content_data,$task_data,$email_report_log,$sms_report_log,$customer_table);
		$data['content_product'] = $content_product_data ;

		parent::render($data);
	}

	protected function search_filter($search_args = array()){	

		if(empty($search_args) && $this->input->get('search')) $search_args = $this->input->get();

		if(empty($search_args)) return FALSE;

		$this->hook->add_filter('search_filter', function($args){

			if(!empty($args['where']['service_package'])){

				$this->admin_ui->join('termmeta AS tm_package', 'tm_package.term_id = term.term_id', 'INNER');

				$this->admin_ui->where('tm_package.meta_key', 'service_package');

				$this->admin_ui->like('tm_package.meta_value', $args['where']['service_package']);

				unset($args['where']['service_package']);
			}

			if(!empty($args['order_by']['service_package'])){

				$this->admin_ui->join('termmeta AS tm_package', 'tm_package.term_id = term.term_id', 'INNER');

				$this->admin_ui->like('tm_package.meta_key', 'service_package');

				$args['order_by']['tm_package.meta_value'] = $args['order_by']['service_package'];

				unset($args['order_by']['service_package']);
			}

			// search filter for method backlink
			if(!empty($args['where']['created_on']))
			{
				$explode = explode(' - ', $args['where']['created_on']);
				$start_time = $this->mdate->startOfDay(strtotime(reset($explode)));
				$end_time = $this->mdate->endOfDay(strtotime(end($explode)));
				
				unset($args['where']['created_on']);

				if(!empty($explode)){
					$this->admin_ui
					->where('backlink.created_on >=', $start_time)
					->where('backlink.created_on <=', $end_time);
				}
			}

			if(!empty($args['order_by']['count_domain_link_from'])){

				$args['order_by']['COUNT(DISTINCT blink_domain)'] = $args['order_by']['count_domain_link_from'];

				unset($args['order_by']['count_domain_link_from']);
			}

			if(!empty($args['order_by']['count_link_from'])){

				$args['order_by']['COUNT(DISTINCT blink_referral)'] = $args['order_by']['count_link_from'];

				unset($args['order_by']['count_link_from']);
			}

			if(!empty($args['order_by']['count_link_to'])){

				$args['order_by']['COUNT(DISTINCT blink_source)'] = $args['order_by']['count_link_to'];

				unset($args['order_by']['count_link_to']);
			}

			if(!empty($args['order_by']['count_active_status'])){

				$args['order_by']["SUM(blink_status = 'active')"] = $args['order_by']['count_active_status'];

				unset($args['order_by']['count_active_status']);
			}

			if(!empty($args['order_by']['count_idle_status'])){

				$args['order_by']["SUM(blink_status = 'idle')"] = $args['order_by']['count_idle_status'];

				unset($args['order_by']['count_idle_status']);
			}

			if(!empty($args['order_by']['count_queue_status'])){

				$args['order_by']["SUM(blink_status = 'queue' OR blink_status = 'draft')"] = $args['order_by']['count_queue_status'];

				unset($args['order_by']['count_queue_status']);
			}

			if(!empty($args['order_by']['index_status'])){

				$args['order_by']['indexed'] = $args['order_by']['index_status'];

				unset($args['order_by']['index_status']);
			}

			if(!empty($args['order_by']['payment_percentage']))
			{
				$this->admin_ui
				->join('termmeta AS tm1', 'tm1.term_id = term.term_id', 'INNER')
				->where('tm1.meta_key', 'payment_percentage');
				$args['order_by']['tm1.meta_value'] = $args['order_by']['payment_percentage'];
				unset($args['order_by']['payment_percentage']);
			}

			return $args;			
		});

		parent::search_filter($search_args);
	}

	protected function is_assigned($term_id = 0,$kpi_type = '')
	{
		// check user has manager role permission
		$class = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		$result = $this->webgeneral_m->has_permission($term_id,"{$class}.{$method}",'access');
		return $result;
	}

	private function has_permission($name ='', $action ='', $term_id = 0)
	{
		if($term_id == 0)
			$term_id = $this->website_id;
		return $this->webgeneral_m->has_permission($term_id,$name, $action);
	}

	function ve()
	{
		
		$this->load->library('email');
		$status = $this->email->verify('duhoc@minhhoangan.edu.vn', 'support@webdoctor.vn');
		var_dump($status);
	}

	function testsms()
	{
		$this->load->library('sms');
		$sms_results = $this->sms->send('0974681379', 'Test sms');
		var_dump($sms_results);
	}

	function active($term_id = 0)
	{
		$this->load->model('webgeneral_report_m');
		$this->webgeneral_report_m->send_activation_mail2customer($term_id);
	}

	function warning_expires($term_id = 0)
    {
        $this->load->model('webgeneral_contract_m');
        // $this->webgeneral_contract_m->mail_warning_expires($term_id );
        $this->webgeneral_contract_m->mail_end_contract($term_id);
    }
}