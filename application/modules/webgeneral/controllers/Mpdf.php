<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpdf extends Public_Controller {

	function __construct()
	{
		parent::__construct();
		// $this->load->add_package_path(APPPATH.'third_party/dompdf/');
		$this->load->add_package_path(APPPATH.'third_party/mypdf/');
		$this->load->library('html2pdf');
	}

	function index()
	{
		 //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./files/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('test.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $data = array(
	    	'title' => 'PDF Created',
	    	'message' => 'Hello World!'
	    );
	    
	    $html = $this->load->view('admin/pdf', $data, true);
	    $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
	     // $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
	    //Load html view
	    $this->html2pdf->html($html);
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	// echo 'PDF saved';
	    	$this->show();
	    }

	}	

	function show()
	{
		header('Content-type: application/pdf');
readfile('./files/test.pdf');
	}
	function view()
	{
		$this->load->view('admin/pdf');
	}
}
