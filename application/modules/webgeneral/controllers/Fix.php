<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fix extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('webgeneral_m');
		$this->load->model(array('webgeneral_kpi_m', 'webgeneral_seotraffic_m','webgeneral_callback_m','webgeneral_seotop_m','webgeneral_config_m'));
	}

	function index()
	{
		// $time_send = strtotime('next monday');
		// $time_send = $this->termmeta_m->get_meta_value(269, 'mail_time_next_send');
		// prd(my_date($time_send));
		// $this->update_kpi_result();
	}

	function update_start_time($term_id = 0)
	{
		if(!$term_id)
			return ;
		update_term_meta($term_id,'start_service_time',0);
		update_term_meta($term_id,'end_service_time',0);
		die('update OK');
	}
	function test_log()
	{
		$config = array();
		require_once APPPATH . '/libraries/logentries.php';
		// $t = $log->Info("Hello Logentries".date('d/m/Y H:i:s'));
		$log->Error("Lỗi CMNR ".date('d/m/Y H:i:s'));

		// $this->load->library('LeLogger', $config);

	}

	function resend($term_id = 0)
	{
		$this->load->model('webgeneral_contract_m');
		$this->load->model('webgeneral_report_m');
		// $package = $this->termmeta_m->get_meta_value($term_id, 'service_package');
		// $this->webgeneral_contract_m->init_job_default($term_id, strtotime('2016/06/01'), $package);
		// $r = $this->webgeneral_report_m->send_tasklist_mail2customer($term_id);
		$r = $this->webgeneral_report_m->send_tasklist_mail2customer($term_id, 'kythuat@webdoctor.vn');
		var_dump($r);
	}
	function ga($term_id = 0)
	{
		$this->load->model('webgeneral_report_m');
		$this->load->model('webgeneral_seotraffic_m');
		// $start_date = str
		// $ga_profile_id = $this->termmeta_m->get_meta_value($term_id, 'ga_profile_id');
		$ga = $this->webgeneral_report_m->send_mail_custom($term_id, strtotime('2016-06-01'), strtotime('2016-06-19'));
		prd($ga);

	}
	function test_send($term_id = 0)
	{
		$this->load->model('webgeneral_contract_m');
		$r = $this->webgeneral_contract_m->send_activation_mail2customer($term_id);
		var_dump($r);
	}

	function resend_report($term_id = 0)
	{
		$this->load->model('webgeneral_report_m');
		$r = $this->webgeneral_report_m->send_mail($term_id);
		var_dump($r);
	}
	function update_time_send()
	{
		$term_id = 269;
		$time_send = strtotime('2016/05/09');;
		$this->termmeta_m->update_meta($term_id, 'mail_time_next_send',$time_send);
	}

	function insert_task_default($term_id = 0)
	{
		$this->load->model('webgeneral_contract_m');
		$package = $this->termmeta_m->get_meta_value($term_id, 'service_package');

		$this->data['content'] = $this->webgeneral_contract_m->init_job_default($term_id, strtotime('+1 day'), $package);

		$this->webgeneral_contract_m->send_tasklist_mail2customer($term_id);
		parent::render($this->data,'blank');
	}
	function list_time_send()
	{
		$this->load->model('webgeneral_report_m');

		$terms = $this->term_m->get_many_by(array(
			'term_type' =>'webgeneral',
			'term_status' =>'publish'
			));
		$data = array();
		foreach($terms as $term)
		{
			// $time_send = strtotime('2016/05/02');
			// $this->termmeta_m->update_meta($term->term_id, 'mail_time_next_send',$time_send);

			// $time_send = strtotime('next monday',$time_send);
			$time_send = $this->termmeta_m->get_meta_value($term->term_id, 'mail_time_next_send');
			$this->table->add_row($term->term_id,$term->term_name, my_date($time_send,'d/m/Y H:i:s'));
		}
		$data['content'] = $this->table->generate();
		parent::render($data, 'blank');
	}
	function update_kpi_result()
	{
		$kpis = $this->webgeneral_kpi_m->order_by('term_id')->get_many_by(array(
			'kpi_type' =>'content'
			));

		$this->table->set_heading(array('Term ID', 'User','Ngày', 'KPI cũ', 'KPI mới'));
		if($kpis)
		{
			foreach($kpis as $kpi)
			{
				$admin_name = $this->admin_m->get_field_by_id($kpi->user_id, 'display_name');
				
				$n_kpi = $this->webgeneral_kpi_m->update_kpi_result($kpi->term_id, 'content', 0, strtotime($kpi->kpi_datetime), $kpi->user_id);
				$this->table->add_row(array($kpi->term_id, $admin_name,$kpi->kpi_datetime, $kpi->result_value, $n_kpi));

			}
		}

		$this->scache->delete_group('modules/webgeneral/kpi-');
		$data['content'] = $this->table->generate();
		parent::render($data, 'blank');
	}

	function t()
	{
		$this->load->model('webgeneral_report_m');
		$this->webgeneral_report_m->send_sms(280);

		
	}
	function update_sms_status($term_id = 0)
	{
		if($term_id ==0)
		{
			return '';
		}
		$posts = $this->post_m->get_posts(array(
			'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
			'tax_query'=> array(
				'taxonomy' => $this->webgeneral_m->term_type,
				'terms' => $term_id,
				),
			'post_status' => 'complete',
			'post_type' => $this->webgeneral_m->get_post_type('task')
			));
		if($posts)
		{

			$this->table->set_heading(array('Term ID','Tiêu đề'));
			foreach($posts as $post)
			{
				$this->postmeta_m->update_meta($post->post_id, 'sms_status', 1);
				$this->table->add_row(array($post->post_id, $post->post_title));
			}

			$data['content'] = $this->table->generate();
			parent::render($data, 'blank');
		}
	}
	
	public function send_week_report($term_id = 0)
	{
		$this->load->model('webgeneral_report_m');
		$this->webgeneral_report_m->send_mail($term_id);
	}

	public function send_activation_mail($term_id = 0)
	{
		$this->load->model('webgeneral_report_m');
		$this->webgeneral_report_m->send_activation_mail2customer($term_id);
	}

	private function send_tasklist_mail($term_id = 0)
	{
		$this->load->model('webgeneral_report_m');
		$this->webgeneral_report_m->send_tasklist_mail2customer($term_id);
	}

	public function load_backlink_sample()
	{
		$this->load->model('backlink_m');
		$this->load->library('excel');

		$file_path = 'files/sample_backlinks.xlsx';

		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel = PHPExcel_IOFactory::createReaderForFile($file_path);
        $objPHPExcel->setReadDataOnly(true); // set this, to not read all excel properties, just data
        $objXLS = $objPHPExcel->load($file_path);

        $total_record = $objXLS->getSheet(0)->getHighestRow();
        $data = array();
        $i=1;
        for($i;$i<=$total_record;$i++)
        {
        	$data[] = array(
				// '' => $objXLS->getSheet(0)->getCell('A' . $i)->getValue(),
				'blink_referral' => $objXLS->getSheet(0)->getCell('B' . $i)->getValue(),
				'blink_keyword' => $objXLS->getSheet(0)->getCell('C' . $i)->getValue(),
				'blink_source' => $objXLS->getSheet(0)->getCell('D' . $i)->getValue(),
				'term_id' => 448,
				'user_id' => 12,
				'created_on' => time(),
				'updated_on' => time()
        		);
        }

        $this->backlink_m->insert_many($data);
	}

	public function send_tech_kpi_report()
	{
		$this->load->model('webgeneral_contract_m');
		$this->webgeneral_report_m->send_tech_kpi_report();
	}

	function convert_content_to_product()
	{
		$posts = $this->post_m
		->where('posts.post_type',$this->webgeneral_m->get_post_type('content'))
		->join('term_posts', 'term_posts.post_id = posts.post_id')
		->where('posts.start_date >=', $this->mdate->startOfMonth(strtotime('2017/05/01')))
		->where('posts.start_date <', $this->mdate->endOfMonth())
		->where('posts.post_status','publish')
		->order_by("posts.post_id")
		->get_many_by();

		if(empty($posts)) prd('404');

		$product_posts = array();
		foreach ($posts as $post)
		{
			$task_type = get_post_meta_value($post->post_id,'task_type');
			if(!in_array($task_type, ['product-write','product-edit'])) continue;

			$product_posts[] = $post;
		}

		if(empty($product_posts)) prd('product not found!');

		foreach ($product_posts as $post)
		{
			$this->post_m->update($post->post_id,['post_type'=>'webgeneral-content-product']);
		}

		$term_ids = array_unique(array_column($product_posts, 'term_id'));
		foreach ($term_ids as $term_id) 
		{
			$this->scache->delete_group('modules/webgeneral/kpi-'.$term_id);
			$this->scache->delete_group('modules/webgeneral/kpi-result-'.$term_id);
			$this->scache->delete_group('modules/webgeneral/get_kpi-'.$term_id);

			$this->webgeneral_kpi_m->update_kpi_result($term_id, 'content');
			$this->webgeneral_kpi_m->update_kpi_result($term_id, 'content_product');
		}
	}

	public function send_daily_content_kpi_report()
	{
		$this->load->model('webgeneral_report_m');
		$this->webgeneral_report_m->send_daily_content_kpi_report();
	}
}