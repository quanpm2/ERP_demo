<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
		restrict('Webgeneral.Index');
		$this->load->model('webgeneral_m');
		$this->load->model('webgeneral_kpi_m');
		$this->term_type = $this->webgeneral_m->term_type;
		// $this->permission();
		// $this->init_website();
	}

	public function index()
	{
		
	}
	public function setting($type ='', $term_id =0)
	{
		$this->load->model('webgeneral_seotraffic_m');
		$this->load->add_package_path(APPPATH.'third_party/google-analytics/');
		switch ($type) {
			case 'ga-reload':
			$results = $this->webgeneral_seotraffic_m->reload_account_ga(true);
			$data = array();
			foreach($results as $id =>$result)
			{
				$data[] = array("id"=> $id, 'text'=>$result);
			}
			return $this->renderJson($data);
			break;
			
			default:
				# code...
			break;
		}
	}
	public function kpi($term_id = 0)
	{
		$result = array('success'=>false, 'msg'=>'Có lỗi xảy ra');
		if($type = $this->input->post('name'))
		{
			$pk = $this->input->post('pk');
			$val = $this->input->post('value');
			switch ($type) {
				case 'change-kpi':
				$type = $this->input->post('type');
				$update = array('kpi_value'=>$val);
				$where = array(
					'term_id'=>$term_id,
					'kpi_type'=>$type,
					'kpi_datetime'=>$pk
					);
				$update['kpi_datetime'] = $pk;
				$count_row = 0;
				
				if(!empty($pk))
				{
					$this->webgeneral_kpi_m->where($where)->update_by($update);
					$count_row = $this->db->affected_rows();
				}
				$result['msg'] = 'Thêm thành công';
				if($count_row == 0)
				{
					$update['term_id'] = $term_id;
					
					$update['kpi_type'] = $type;
					$this->webgeneral_kpi_m->insert($update);
					$result['msg'] = 'Cập nhật thành công';
				}
				$this->webgeneral_kpi_m->delete_cache($term_id);
				$result['success'] = true;
				
				break;
				
				default:
					# code...
				break;
			}
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	function post_delete($type ='', $post_id = 0)
	{
		if( ! $this->input->is_ajax_request())
		{
			die('Bài viết không tồn tại hoặc quyền truy cập bị hạn chế.');
		}

		$post_type = $this->webgeneral_m->get_post_type($type);

		if( ! has_permission("Webgeneral.{$type}.delete"))
		{
			die('Quyền truy cập bị hạn chế.');	
		}
		
		if(empty($post_type) || empty($post_id))
		{
			die('Loại bài viết xóa không đúng.');
		}
		
		$args = array('post_type'=>$post_type,'post_id'=>$post_id);
		if( ! has_permission("Webgeneral.{$type}.manage"))
		{
			$args['post_author'] = $this->admin_m->id;
		}

		$check = $this->webgeneral_m->get_by($args);
		if(empty($check))
			die('Bài viết không tồn tại hoặc quyền truy cập bị hạn chế.');

		$term_ids = $this->term_posts_m->get_post_terms($post_id, 'webgeneral');

		$this->webgeneral_m->delete_post($post_id);
		if(!in_array($type, ['content','content_product']) || empty($term_ids)) die('OK');

		// Recalculates KPI result
		foreach($term_ids as $term)
		{
			$this->webgeneral_kpi_m->update_kpi_result($term->term_id, $type, 0, '', $check->post_author) ;
		}
		
		die('OK');
	}

	function upload_backlinks()
	{
		$result = array('msg'=>'File upload không hợp lệ !','success'=>FALSE);
		$this->output->set_content_type('application/json');

		$post = $this->input->post();
		if(empty($post)) return $result;

		$upload_config = array();
		$upload_config['upload_path'] = './files/backlinks/';
		$upload_config['max_size'] = '4096';
		$upload_config['allowed_types'] = 'xls|xlsx|txt';
		$upload_config['file_name'] = $post['term_name'].'-'.$post['term_id'].'-'. time();
		$upload_config['file_name'] = str_replace('.', '-', $upload_config['file_name']);
		$upload_config['upload_path'].= date('Y').'/'.date('m').'/';

		if(!is_dir($upload_config['upload_path'])) 
			mkdir($upload_config['upload_path'],0777,TRUE);

		$this->load->library('upload');
		$this->upload->initialize($upload_config);	
		if(!$this->upload->do_upload('fileinput'))
			return $this->output->set_output(json_encode($result));

		$data = $this->upload->data();
		$result['msg'] = 'Upload file hợp đồng thành công !';
		$result['success'] = TRUE;
		$result['file_name'] = $upload_config['upload_path'].$data['file_name'];

		return $this->output->set_output(json_encode($result));
	}

	function pingomatic($blink_id = 0)
	{
		$this->load->model('backlink/backlink_m');
		$result = $this->backlink_m->pingomatic($blink_id);
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}
}
/* End of file kpi.php */
/* Location: ./application/controllers/kpi.php */