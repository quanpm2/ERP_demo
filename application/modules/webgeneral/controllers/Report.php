<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Public_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('mdate');
		$this->load->model('webgeneral_m');
		$this->load->model('webgeneral_seotraffic_m');
		$this->load->model('webgeneral_kpi_m');
	}

	public function _remap($method, $seg = '')
	{
		return $this->index($method);
	}

	public function index($site_name = '')
	{
		$data = array();
		$site_name = strtolower($site_name);
		$post_id = $this->input->get('p');
		$post = $this->webgeneral_m->get($post_id);
		$term = $this->term_posts_m->get_post_terms($post_id, 'webgeneral');

		if(!$term || !$post_id || !$post)
		{
			show_frontend_404();
		}

		$start_date = date('Y-m-d',$post->start_date);
		$end_date = strtotime('+7 day', $post->start_date);
		$end_date = date('Y-m-d',$end_date);

		$code = rawurldecode($this->input->get('domain'));

 		if(isset($term[0]))
 			$term = $term[0];

		$term_id = $term->term_id;
		$decrypted = $this->webgeneral_seotraffic_m->encrypt($code,$term, 'decode');

		if(strtolower($decrypted) != $term_id)
			show_frontend_404();

		$time = time();

		$start_date = $this->mdate->startOfDay($start_date);
		$end_date = $this->mdate->endOfDay($end_date);

		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		$data['site_name'] = $term->term_name;
		$ga_profile_id = $this->termmeta_m->get_meta_value($term_id, 'ga_profile_id');

		$data['tasks'] = $this->content($term_id, $start_date, $end_date, 'task');
		$data['contents'] = $this->content($term_id,$start_date, $end_date, 'content');

		$traffic = (array)$this->traffic($term_id, $time,$start_date, $end_date);
		if($traffic && is_array($traffic))
			$data  = array_merge($data,$traffic);
		$this->load->view('frontend/report', $data);

	}
	private function content($term_id, $start_date,$end_date, $post_type = 'content')
	{
		$post_type = $this->webgeneral_m->get_post_type($post_type);
		if(!$post_type)
			return false;

		return $this->webgeneral_m->get_posts_from_date($start_date, $end_date,$term_id, $post_type);
	}
	private function traffic($term_id, $time, $start_date,$end_date)
	{
		$start_date = date('Y-m-d',$start_date);
		$end_date = date('Y-m-d',$end_date);
		$dayEndOfMonth = date("Y-m-t", $time);
		$dayEndOfMonth = $this->mdate->endOfDay($dayEndOfMonth);
		$kpi = $this->webgeneral_kpi_m->get_kpi_value($term_id,'seotraffic', $time);

		$kpi = $kpi / date('d',$dayEndOfMonth);
		$kpi = numberformat($kpi);
		$ga_profile_id = $this->termmeta_m->get_meta_value($term_id, 'ga_profile_id');
		$ga_data = $this->webgeneral_seotraffic_m->get_ga($start_date, $end_date,$ga_profile_id);
		if(!$ga_data)
		{
			return false;
		}
		$charData = array();
		$charData['ga:sessions'] = array();
		$charData['Organic Search'] = array();
		$charData['kpi'] = array();

		foreach($ga_data['headers'] as $header) {
			$charData['ga:sessions'][] = (isset($ga_data['results'][$header]['ga:sessions']) ? $ga_data['results'][$header]['ga:sessions'] : 0);

			$charData['Organic Search'][] = (isset($ga_data['results'][$header]['Organic Search']) ? $ga_data['results'][$header]['Organic Search'] : 0);
			$charData['kpi'][] = $kpi;

		}
		$charData['ga:sessions'] = implode(',', $charData['ga:sessions']);
		$charData['Organic Search'] = implode(',', $charData['Organic Search']);
		$charData['kpi'] = implode(',', $charData['kpi']);
		
		$charDataHeader = array();
		foreach($ga_data['headers'] as $header)
		{
			$charDataHeader[] = date('d/m', strtotime($header));
		}

		$data['charDataHeader'] = implode('","', $charDataHeader);
		$data['charDataHeader'] = '"'.$data['charDataHeader'].'"';

		$data['charData'] = $charData;
		return $data;
	}

}

/* End of file report.php */
/* Location: ./application/controllers/report.php */