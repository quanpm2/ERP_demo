<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends Public_Controller {

	public $model = '';
	private $website_id;
	private $term;
	private $term_type;

	function __construct()
	{
		parent::__construct();
		// restrict('Admin.Webgeneral');
		$this->load->model('webgeneral_m');
		$this->load->model(array('webgeneral_kpi_m', 'webgeneral_seotraffic_m','webgeneral_callback_m','webgeneral_seotop_m','webgeneral_config_m'));
		$this->term_type = $this->webgeneral_m->term_type;
		$this->load->config('webgeneral');
		$this->load->add_package_path(APPPATH.'third_party/google-gmail/');
		$this->load->library('gmail');
	}

	function index()
	{
		$service = $this->gmail->get_service();
		if(!$service)
			return 'xx';
		$lists = $this->gmail->listMessagesUnRead($service);
		$result = array();
		if($lists)
		{
			foreach($lists as $list)
			{
				$message_id = $list->getId();
				$message = $this->gmail->getMessage($service, 'me',$message_id);

				$subject = $this->gmail->get_subject($message);
				$rawData = $this->gmail->get_body($message);
				$result[] = array(
					
					'id'=>$message_id,
					'subject' =>$subject,
					'body' =>$rawData,
					);
			
			}
		}
		prd($result);

	}
	
}
