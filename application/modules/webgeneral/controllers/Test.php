<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(-1);
ini_set('display_errors', 1);
class Test extends Admin_Controller {

	public $model = '';
	private $website_id;
	private $term;
	private $term_type;

	function __construct()
	{
		parent::__construct();
		// restrict('Admin.Webgeneral');
		$this->load->model('webgeneral_m');
		$this->load->model(array('webgeneral_kpi_m','webgeneral_seotraffic_m','webgeneral_callback_m','webgeneral_seotop_m','webgeneral_config_m','webgeneral_overview_m','contract/contract_m'));
		$this->term_type = $this->webgeneral_m->term_type;
		$this->load->config('webgeneral');
	}

	function has_permission()
	{
		$this->load->library('unit_test');
		$this->load->model('webgeneral_m');

		// $this->role_has_permission_webbuild(1820,'access','',157,13);
		$this->role_has_permission(1821,'access','',12,6);
		
		// var_dump('-------------------------------------------------------------------------');
		// $this->role_has_permission(1820,'access','tech',185,3);

		// var_dump('-------------------------------------------------------------------------');
		// $this->role_has_permission(1820,'access','content',185,6);

		// var_dump('-------------------------------------------------------------------------');
		// $this->role_has_permission(1820,'access','tech',185,6);

		// var_dump('-------------------------------------------------------------------------');
		// $this->role_has_permission(1820,'access','tech',47,1);

		// var_dump('-------------------------------------------------------------------------');
		// $this->role_has_permission(1820,'access','content',12,6);
	}

	function role_has_permission_webbuild($term_id,$action,$kpi_type,$user_id,$role_id)
	{
		$this->load->library('unit_test');
		$this->load->model('webgeneral_m');

		$test = $this->webgeneral_m->has_permission($term_id, 'Webbuild.overview',$action,$kpi_type,$user_id,$role_id);
		$expected_result = TRUE;
		$test_name = "role({$role_id}) - user({$user_id}) - term_id ({$term_id}) - Webbuild.overview - {$action} - {$kpi_type}";
		$notes = 'ROLE WEBDOCTOR MEMBER';
		echo $this->unit->run($test,$expected_result,$test_name,$notes);

		$test = $this->webgeneral_m->has_permission($term_id, 'Webbuild.task',$action,$kpi_type,$user_id,$role_id);
		$expected_result = TRUE;
		$test_name = "role({$role_id}) - user({$user_id}) - term_id ({$term_id}) - Webbuild.task - {$action} - {$kpi_type}";
		$notes = 'ROLE WEBDOCTOR MEMBER';
		echo $this->unit->run($test,$expected_result,$test_name,$notes);

		$test = $this->webgeneral_m->has_permission($term_id, 'Webbuild.kpi',$action,$kpi_type,$user_id,$role_id);
		$expected_result = TRUE;
		$test_name = "role({$role_id}) - user({$user_id}) - term_id ({$term_id}) - Webbuild.kpi - {$action} - {$kpi_type}";
		$notes = 'ROLE WEBDOCTOR MEMBER';
		echo $this->unit->run($test,$expected_result,$test_name,$notes);

		$test = $this->webgeneral_m->has_permission($term_id, 'Webbuild.setting',$action,$kpi_type,$user_id,$role_id);
		$expected_result = TRUE;
		$test_name = "role({$role_id}) - user({$user_id}) - term_id ({$term_id}) - Webbuild.setting - {$action} - {$kpi_type}";
		$notes = 'ROLE WEBDOCTOR MEMBER';
		echo $this->unit->run($test,$expected_result,$test_name,$notes);

		$test = $this->webgeneral_m->has_permission($term_id, 'Webbuild.sale',$action,$kpi_type,$user_id,$role_id);
		$expected_result = TRUE;
		$test_name = "role({$role_id}) - user({$user_id}) - term_id ({$term_id}) - Webbuild.sale - {$action} - {$kpi_type}";
		$notes = 'ROLE WEBDOCTOR MEMBER';
		echo $this->unit->run($test,$expected_result,$test_name,$notes);
	}

	function role_has_permission($term_id,$action,$kpi_type,$user_id,$role_id)
	{
		$this->load->library('unit_test');
		$this->load->model('webgeneral_m');

		$test = $this->webgeneral_m->has_permission($term_id, 'Webgeneral.overview',$action,$kpi_type,$user_id,$role_id);
		$expected_result = TRUE;
		$test_name = "role({$role_id}) - user({$user_id}) - term_id ({$term_id}) - Webgeneral.overview - {$action} - {$kpi_type}";
		$notes = 'ROLE WEBDOCTOR MEMBER';
		echo $this->unit->run($test,$expected_result,$test_name,$notes);

		$test = $this->webgeneral_m->has_permission($term_id, 'Webgeneral.seotraffic',$action,$kpi_type,$user_id,$role_id);
		$expected_result = TRUE;
		$test_name = "role({$role_id}) - user({$user_id}) - term_id ({$term_id}) - Webgeneral.seotraffic - {$action} - {$kpi_type}";
		$notes = 'ROLE WEBDOCTOR MEMBER';
		echo $this->unit->run($test,$expected_result,$test_name,$notes);

		$test = $this->webgeneral_m->has_permission($term_id, 'Webgeneral.task',$action,$kpi_type,$user_id,$role_id);
		$expected_result = TRUE;
		$test_name = "role({$role_id}) - user({$user_id}) - term_id ({$term_id}) - Webgeneral.task - {$action} - {$kpi_type}";
		$notes = 'ROLE WEBDOCTOR MEMBER';
		echo $this->unit->run($test,$expected_result,$test_name,$notes);

		$test = $this->webgeneral_m->has_permission($term_id, 'Webgeneral.content',$action,$kpi_type,$user_id,$role_id);
		$expected_result = TRUE;
		$test_name = "role({$role_id}) - user({$user_id}) - term_id ({$term_id}) - Webgeneral.content - {$action} - {$kpi_type}";
		$notes = 'ROLE WEBDOCTOR MEMBER';
		echo $this->unit->run($test,$expected_result,$test_name,$notes);

		$test = $this->webgeneral_m->has_permission($term_id, 'Webgeneral.kpi',$action,$kpi_type,$user_id,$role_id);
		$expected_result = TRUE;
		$test_name = "role({$role_id}) - user({$user_id}) - term_id ({$term_id}) - Webgeneral.kpi - {$action} - {$kpi_type}";
		$notes = 'ROLE WEBDOCTOR MEMBER';
		echo $this->unit->run($test,$expected_result,$test_name,$notes);

		$test = $this->webgeneral_m->has_permission($term_id, 'Webgeneral.setting',$action,$kpi_type,$user_id,$role_id);
		$expected_result = TRUE;
		$test_name = "role({$role_id}) - user({$user_id}) - term_id ({$term_id}) - Webgeneral.setting - {$action} - {$kpi_type}";
		$notes = 'ROLE WEBDOCTOR MEMBER';
		echo $this->unit->run($test,$expected_result,$test_name,$notes);
	}

	function error()
	{
		// break;
	}

	function mail_warning_expires($term_id = 0)
	{
		//dd4b39 - màu đỏ
		$data = array();
		$data['time'] = time();
		$data['title'] = 'Cảnh báo hết hạn dịch vụ';
		$term = $this->term_m->get($term_id);
		$data['term'] = $term;
		$data['term_id'] = $term_id;

		$tasks = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('task'));
		$data['num_banner'] = 0;
		$data['num_cusomer_task'] = 0;
		$contents = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('content'));
		$data['num_content'] = count($contents);
		
		if($tasks)
		{
			foreach ($tasks as $task) {
				if($task->post_status != 'complete')
				{
					continue;
				}
				$task_type = get_post_meta_value($task->post_id, 'task_type');
				if($task_type == 'banner')
					$data['num_banner']++;
				else
					$data['num_cusomer_task']++;
			}
		}

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff = $this->admin_m->set_get_active()->get($staff_id);
		if(!$staff)
			$staff = $this->admin_m->get(18); //anh Hải

		$data['staff'] = $staff;
		$content = $this->load->view('email/warning_expires', $data, true);
		$title = 'Thông báo dịch vụ Webdoctor website '.ucfirst($term->term_name).' hết hạn';
		$this->send_mail($title, $content);
	}

	function mail_end_contract($term_id = 0)
	{
		$data = array();
		$data['time'] = time();
		$data['title'] = 'Kết thúc hợp đồng';
		$term = $this->term_m->get($term_id);
		$data['term'] = $term;
		$data['term_id'] = $term_id;

		$tasks = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('task'));
		$data['num_banner'] = 0;
		$data['num_cusomer_task'] = 0;
		$contents = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('content'));
		$data['num_content'] = count($contents);
		
		if($tasks)
		{
			foreach ($tasks as $task) {
				if($task->post_status != 'complete')
				{
					continue;
				}
				$task_type = get_post_meta_value($task->post_id, 'task_type');
				if($task_type == 'banner')
					$data['num_banner']++;
				else
					$data['num_cusomer_task']++;
			}
		}

		$data['tasks'] = $tasks;
		$data['contents'] = $contents;

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff = $this->admin_m->set_get_active()->get($staff_id);
		if(!$staff)
			$staff = $this->admin_m->get(18); //anh Hải

		$data['staff'] = $staff;

		$content = $this->load->view('email/end_contract', $data, true);
		$title = 'Thông báo kết thúc hợp đồng Webdoctor';
		$this->send_mail($title, $content);
	}

	function send_mail($title = '', $content = '')
	{
		$this->load->library('email');
		$this->email->from('support@webdoctor.vn','Webdoctor.vn');
		$this->email->set_debug(false);
		$this->email->to('thonh@webdoctor.vn');
		$this->email->subject($title);
		$this->email->message($content);
		return $this->email->send();
	}
}