<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wordpress extends Admin_Controller {

	public $api_url = '';
	private $term_type;
	function __construct()
	{
		parent::__construct();
		$this->load->library('wp_service');
		$this->load->model('webgeneral_m');
		$this->term_type = $this->webgeneral_m->term_type;
		$this->init_website();
	}

	private function init_website()
	{

		$term_id = $this->uri->segment(5);
		$method  = $this->uri->segment(4);
		$is_allowed_method = (!in_array($method, array('index','','ajax','statistic', 'approval')));
		if(!$is_allowed_method || empty($term_id)) return FALSE;

		$term = $this->term_m
		->where('term_status','publish')
		->where('term_type',$this->term_type)
		->where('term_id',$term_id)
		->get_by();
		if(empty($term)){
		// if(empty($term) || is_service_end($term)){
			$this->messages->error('#'.$term_id.' không hợp lệ !!!');
			redirect(module_url(),'refresh');
		}

		$this->template->title->set(strtoupper(' '.$term->term_name));
		$this->website_id = $this->data['term_id'] = $term_id;
		$this->data['term'] = $this->term = $term;
	}
	function index()
	{
		$data = array();
		$data['function'] = 'get_posts';
		$data['args'] = array('meta_key'=>'webdoctor_userId','meta_value'=>'15','post_status' => 'post');
		$r = $this->curl_post('http://localhost:9090/wordpress/?callback',$data);
		$r = json_decode($r, false);
		// var_dump($r);
		prd($r);
	}
	function login($term_id = 0)
	{
		$token = md5('hash');

		$term = $this->term_m->get($term_id);
		$hash = $this->session->session_id.'-'.$term_id;
		$token = md5($hash);
		$check = $this->log_m->get_by(array(
			'log_type' =>'webgeneral-wp-login',
			'user_id' => $this->admin_m->id,
			'term_id' => $term_id,
			'log_title' => 'User hash login',
			'log_content' => $token
			));
		if(!$check)
		{
			$this->log_m->insert(array(
			'log_type' =>'webgeneral-wp-login',
			'user_id' => $this->admin_m->id,
			'term_id' => $term_id,
			'log_title' => 'User hash login',
			'log_content' => $token,
			'log_time_create' => date('Y-m-d H:i:s'),
				));
		}
		$url = get_term_meta_value($term_id, 'wp_service_url');
		$url = $url.'?wdt_login='.$token;
		redirect($url,'refresh');
	}
	private function curl_post($url, $data = array())
	{
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($data));
		curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

	private function curl_get($url)
	{
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

	function sync_posts($term_id = 0)
	{
		$this->api_url = get_term_meta_value($term_id, 'wp_service_url');

		//connect client
		$service = $this->wp_service->connect($this->api_url, $term_id);
		if(!$service)
		{
			echo 'Không kết nối được đến website';
			return 'Không kết nối được đến website';
		}

		$posts = $service->listPosts(0, array('post_status'=>'pending'));

		if($posts)
		foreach($posts as $post)
		{
			$link = $service->getData('get_permalink',$post);
			$meta = $service->getData('get_post_meta', $post->ID, 'wdt_id', true);

			$btn_approval = '';
			$btn_approval.= anchor($link, 'Xem trước', 'target="_blank" class="btn btn-xs "');
			$btn_approval.= ' <button name="approval" type="submit" id="" class="btn btn-success btn-xs  approval-post" value="approval" data-toggle="confirmation" data-original-title="" title="" data-id="'.$post->ID.'" data-type="approval"><i class="fa fa-check"></i> Duyệt</button>';

			$btn_approval.= ' <button name="approval" type="submit" id="" class="btn btn-danger btn-xs  approval-post" value="no-approval" data-toggle="confirmation" data-original-title="" title="" data-id="'.$post->ID.'" data-type="no"><i class="fa fa-times"></i> Không duyệt</button>';
			$user = explode('|', $meta);
			$post->post_modified = strtotime($post->post_modified);
			$post->post_modified = strtotime('+7 hours',$post->post_modified);
			$author_name = $this->admin_m->get_field_by_id($user[0], 'display_name');
			$this->table->add_row($post->ID,
				$post->post_title,
				date('d/m/Y H:i:s', $post->post_modified),
				$author_name,
				$post->post_status,
				array('data'=>$btn_approval, 'class'=>'col-md-3'));
		}

		$this->table->set_heading('#','title','Ngày viết','Người viết', 'Trạng thái', 'Hành động');

		parent::render(array('content'=>$this->table->generate(), 'term_id'=>$term_id));
	}

	function approval($post_id = 0, $term_id = 0,  $type = '')
	{
		$this->api_url = get_term_meta_value($term_id, 'wp_service_url');
		$service = $this->wp_service->connect($this->api_url);

		$remote_post = array();
		$remote_post['ID'] = $post_id;

		switch ($type) {
			case 'approval':
			$remote_post['post_status'] = 'publish';
			$this->load->model('webgeneral_m');
			$this->load->model('webgeneral_kpi_m');

			$post = $service->getData('get_post',$post_id);
			$meta = $service->getData('get_post_meta', $post_id, 'wdt_id', true);
			$user = explode('|', $meta);

			$insert = array();
			$insert['post_title'] = $post->post_title;
			$post->post_modified = strtotime($post->post_modified);
			$post->post_modified = strtotime('+7 hours',$post->post_modified);
			
			$insert['start_date'] = $post->post_modified;
			$insert['end_date'] = $insert['start_date'];
			$insert['post_content'] = $service->getData('get_permalink',$post);
			$insert['created_on'] = time();
			$insert['post_type'] = $this->webgeneral_m->get_post_type('content');
			$insert['post_author'] = $user[0];
			$post_id = $this->webgeneral_m->insert($insert);
			$this->term_posts_m->set_post_terms($post_id, $term_id, $this->webgeneral_m->term_type);

			$this->webgeneral_kpi_m->update_kpi_result($term_id, 'content',0,'', $user[0]);

			$this->postmeta_m->update_meta($post_id, 'task_type', 'post-write');
			$this->postmeta_m->update_meta($post_id, 'done_ratio', 100);

			break;
			case 'no':
			$remote_post['post_status'] = 'draft';

			break;
			
			default:
				return ;
			break;
		}

		$r = $service->getData('wp_update_post',$remote_post);
		$result = array();
		$result['success'] = 'OK';
		return parent::renderJson($result);
		
	}

}