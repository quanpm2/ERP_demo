<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends Public_Controller
{  
    public $model = '';
    private $website_id;
    private $term;
    private $term_type;
    	
    function __construct()
    {
        parent::__construct();
        // restrict('Admin.Webgeneral');
        $this->load->model(array(
            'webgeneral_m',
            'contract/base_contract_m',
            'webgeneral_kpi_m',
            'webgeneral_seotraffic_m',
            'webgeneral_callback_m',
            'webgeneral_seotop_m',
            'webgeneral_config_m'
        ));
        $this->load->model('webgeneral_contract_m');
        $this->term_type = $this->webgeneral_m->term_type;
        $this->load->config('webgeneral');
    }

    function index()
    {
        $time      = time();
        $is_monday = (date('w', $time) == '1');
        $h         = date('H', $time);
        
        $this->send();
        
        // $this->send_daily_content_kpi_report();
    }
    
    function warning_expires()
    {
        $terms = $this->term_m->get_many_by(array(
            'term_type' => 'webgeneral',
            'term_status' => 'publish'
        ));
        $time  = time();
        
        foreach ($terms as $term) {
            $end_time     = get_term_meta_value($term->term_id, 'end_service_time');
            // $date_expired = (($end_time < $time) ? 0 : round(diffInDates($end_time, $time)));
            $date_expired = floor(diffInDates($time, $end_time));
            if ($date_expired > 20)
                continue;
            
            //cách 5 ngày gửi 1 lần
            if ($date_expired % 5 == 0) {
                echo $term->term_id . '<br>';
                $this->webgeneral_contract_m->mail_warning_expires($term->term_id);
            }
            
        }
    }
    
    function send()
    {
        $this->load->model('webgeneral_report_m');
        
        $terms      = $this->term_m->get_many_by(array(
            'term_type' => 'webgeneral',
            'term_status' => 'publish',
        ));

        $time       = time();
        $phone_send = array();
        $mail_send  = array();
        $is_monday  = (date('w', $time) == '1');
        $term_cache = array();
        
        /*
        Báo cáo sẽ gửi định kỳ 4 lần / tháng
        Ngày 1, 8, 15, 22
        Ngày 8:  gửi từ 1 -> 7
        Ngày 15: gửi từ 7 -> 14
        Ngày 22: gửi từ 14 -> 21
        Ngày 01: gửi từ 21 -> cuối cùng của tháng (t)
        
        Giả sử:
        - Nếu ngày 1 ko gửi, ngày 2 gửi lại
        - Ngày 2 sẽ gửi lại cho ngày 1
        meta: mail_time_next_send <- đây là ngày end_date sẽ gửi báo cáo
        
        */
        
        if ($terms) 
        {
            foreach ($terms as $term) 
            {
                $time_send = $this->termmeta_m->get_meta_value($term->term_id, 'mail_time_next_send');
                if (!$time_send) 
                {
                    $time_send = $this->get_next_time_send($time);
                    $this->termmeta_m->update_meta($term->term_id, 'mail_time_next_send', $time_send);
                    continue;
                }
                
                if ($time_send < $time) {
                    
                    $date       = $this->general_date($time_send);
                    $start_date = $date['start_date'];
                    $end_date   = $date['end_date'];
                    $time_send  = $date['time_next_send'];
                    
                    $start_date = $this->mdate->startOfDay($start_date);
                    $end_date   = $this->mdate->endOfDay($end_date);
                    
                    //send mail
                    $result                    = $this->webgeneral_report_m->send_mail_custom($term->term_id, $start_date, $end_date);
                    $mail_send[$term->term_id] = (isset($result['status']) ? $result['status'] : false);
                    
                    if ($mail_send[$term->term_id] === false)
                        unset($mail_send[$term->term_id]);
                    else
                    {
                    	$this->termmeta_m->update_meta($term->term_id, 'mail_time_next_send', $time_send);
                    }
                }
                
                //send sms
                $phone_send[$term->term_id] = $this->webgeneral_report_m->send_sms($term->term_id);
                $term_cache[$term->term_id] = $term->term_name;
                echo 'sended: ' . $term->term_name . '<br>';
            }

            $content = '';
            $this->config->load('table_mail');
            $this->table->set_template($this->config->item('mail_template'));
            $i = 1;
            if (!empty($mail_send)) {
                
                $this->table->set_caption('Báo cáo gửi mail định kỳ');
                foreach ($mail_send as $term_id => $r) {
                    if (empty($r))
                        continue;
                    
                    $status = ($r['status']) ? 'Thành công' : 'Không thành công';
                    
                    $title = (isset($r['msg']['post_title'])) ? $r['msg']['post_title'] : '';
                    $title = ($title) ? $title : $r['msg'];
                    
                    $this->table->add_row($i++, @$term_cache[$term_id], $r['to'], $title, $status);
                }
                $content .= $this->table->generate();
            }
            $i = 1;
            if ($phone_send) {
                $this->table->set_caption('Báo cáo gửi SMS định kỳ');
                foreach ($phone_send as $term_id => $sms) {
                    if (is_array($sms['msg'])) {
                        foreach ($sms['msg'] as $r) {
                            $msg = $r['recipient'] . ' - ' . $r['title'];
                            $this->table->add_row($i++, @$term_cache[$term_id], $msg);
                        }
                    } else {
                        $this->table->add_row($i++, @$term_cache[$term_id], $sms['msg']);
                    }
                }
                $content .= $this->table->generate();
            }
            
            $this->load->library('email');
            
            
            $this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
            $this->email->to('support@webdoctor.vn');
            $title = 'Báo cáo gửi báo cáo Web tổng hợp ngày ' . date('d/m/Y H:i:s');
            
            $this->email->subject($title);
            $this->email->message($content);
            $send_status = true;
            $send_status = $this->email->send();
        }
        
        $is_friday = (date('w', $time) == '6');
        if ($is_friday) {
            // $this->webgeneral_report_m->send_tech_kpi_report();
        }
    }
    
    private function get_next_time_send($time_send)
    {
        
        $next_date   = false;
        $first_month = $this->mdate->startOfMonth($time_send);
        $next_month  = strtotime('+1 month', $first_month);
        
        $weeks = array(
            date('Y-m-01', $time_send),
            date('Y-m-08', $time_send),
            date('Y-m-15', $time_send),
            date('Y-m-22', $time_send),
            date('Y-m-01', $next_month),
            date('Y-m-08', $next_month),
            date('Y-m-15', $next_month),
            date('Y-m-22', $next_month)
        );
        foreach ($weeks as $week) {
            if (strtotime($week) > $time_send) {
                $next_date = $week;
                break;
            }
        }


        if ($next_date == false)
            return false;
        return strtotime($next_date);
    }
    
    function general_date($time_send)
    {
        $end_date = strtotime('-7 day', $time_send);
        $end_date = $this->get_next_time_send($end_date);
        
        $start_date = strtotime('-7 day', $end_date);
        // $start_date = $this->get_next_time_send($start_date);
        
        $end_date = strtotime('-1 day', $end_date);
        
        if (date('j', $time_send) <= 7) {
            $start_date = strtotime(date('Y-m-21', $start_date));
            $end_date   = strtotime(date('Y-m-t', $start_date));
        }
        
        if (date('j', $time_send) > 23) {
            $start_date = strtotime(date('Y-m-15', $start_date));
            $end_date   = strtotime(date('Y-m-21', $start_date));
        }
        
        $start_date = $this->mdate->startOfDay($start_date);
        $end_date   = $this->mdate->endOfDay($end_date);
        
        $time_send = $this->get_next_time_send($time_send);
        return array(
            'start_date' => $start_date,
            'end_date' => $end_date,
            'time_next_send' => $time_send
        );
    }
    function test_send()
    {
        $this->load->library('unit_test');
        $time_send = strtotime('2016/10/01');
        $time      = $time_send;
        
        for ($i = 1; $i <= 31; $i++) {
            $test                   = $this->general_date($time);
            $test['start_date']     = date('Y-m-d', $test['start_date']);
            $test['end_date']       = date('Y-m-d', $test['end_date']);
            $test['time_next_send'] = date('Y-m-d', $test['time_next_send']);
            
            if ($i <= 7) {
                $expected_result = array(
                    'start_date' => '2016-09-21',
                    'end_date' => '2016-09-30',
                    'time_next_send' => '2016-10-08'
                );
            } else if ($i >= 8 && $i < 15) {
                $expected_result = array(
                    'start_date' => '2016-10-01',
                    'end_date' => '2016-10-07',
                    'time_next_send' => '2016-10-15'
                );
                
            } else if ($i >= 15 && $i <= 21) {
                $expected_result = array(
                    'start_date' => '2016-10-08',
                    'end_date' => '2016-10-14',
                    'time_next_send' => '2016-10-22'
                );
            } elseif ($i >= 22 && $i <= 31) {
                $expected_result = array(
                    'start_date' => '2016-10-15',
                    'end_date' => '2016-10-21',
                    'time_next_send' => '2016-11-01'
                );
                if ($i == 31) {
                    var_dump($i, $test);
                }
            }
            
            $this->unit->run($test, $expected_result, date('Y-m-d', $time));
            $time = strtotime('+1 day', $time);
        }
        echo $this->unit->report();
    }
    function test()
    {
        /*
        Báo cáo sẽ gửi định kỳ 4 lần / tháng
        Ngày 1, 8, 15, 22
        Ngày 8:  gửi từ 1 -> 7
        Ngày 15: gửi từ 7 -> 14
        Ngày 22: gửi từ 14 -> 21
        Ngày 01: gửi từ 21 -> cuối cùng của tháng (t)
        
        
        //Next time: 
        22 - t: ngày 01
        01-07: ngày 8
        08-14: ngày 15
        15-21: ngày 22
        Giả sử:
        - Nếu ngày 1 ko gửi, ngày 2 gửi lại
        - Ngày 2 sẽ gửi lại cho ngày 1
        meta: mail_time_next_send <- đây là ngày end_date sẽ gửi báo cáo
        
        */
        $this->load->library('unit_test');
        $time = strtotime('2016/09/01');
        
        for ($i = 1; $i <= 60; $i++) {
            $test = $this->get_next_time_send($time);
            $test = date('Y-m-d', $test);
            if ($i <= 7) {
                $expected_result = '2016-09-08';
            } else if ($i >= 8 && $i < 15) {
                $expected_result = '2016-09-15';
            } else if ($i >= 15 && $i <= 21) {
                $expected_result = '2016-09-22';
            } else {
                $expected_result = '2016-10-01';
            }
            $this->unit->run($test, $expected_result, date('Y-m-d', $time) . ' - ' . $test . ' - ' . $expected_result);
            $time = strtotime('+1 day', $time);
        }
        echo $this->unit->report();
    }

    function send_daily_content_kpi_report()
    {
        $this->load->model('webgeneral_report_m');

        $day_of_week = date('w');
        if($day_of_week == 0) return FALSE;
        $time = ($day_of_week == 1) ? strtotime('-2 days') : strtotime('yesterday');

        $this->webgeneral_report_m->send_daily_content_kpi_report($time);
    }
}
/* End of file Cron.php */
/* Location: ./application/modules/webgeneral/controllers/Cron.php */