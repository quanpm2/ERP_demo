<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics extends Admin_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('webgeneral_m');
		$this->load->model('webgeneral_kpi_m');
		$this->load->config('webgeneral');
	}

	function banner($month = '', $year = '')
	{
		$this->template->title->prepend('Thống kê banner');
		
		$data = $this->data;
		$this->config->load('group');

		$month = ((int)$month && $month > 0 && $month < 13) ? $month : $this->mdate->date('m');
		$year = ((int)$year && $year > 0) ? $year : $this->mdate->date('Y');
		$time = $this->mdate->convert_time("{$year}/{$month}/1");

		$start_time = $this->mdate->startOfMonth($time);
		$end_time = $this->mdate->endOfMonth($time);

		$author_banners = $this->webgeneral_m
		->select('term_posts.term_id,post_author,posts.post_title,count(posts.post_id) as number_of_banners')
		->where('posts.post_status','complete')
		->as_array()
		->group_by('post_author,term_posts.term_id')
		->join('term_posts', 'term_posts.post_id = posts.post_id')
		->get_posts_from_date($start_time, $end_time,0,'webgeneral-task',array('meta_key'=>'task_type','meta_value'=>'banner'));

		$termIds = array_column($author_banners, 'term_id');
		if(!empty($termIds))
			$this->term_m->where_in('term_id',$termIds);

		$terms = $this->term_m->select('term_id,term_name')->as_array()->get_many_by();
		$terms = array_column($terms,NULL,'term_id');

		$groupMemberIds = $this->config->item('group_memberId');
		$userIds = array_column($author_banners,'post_author');
		if(!empty($userIds))
			$this->admin_m->where_in('user_id',$userIds);
		
		$users = $this->admin_m
		->select('user_id,display_name')
		->set_get_active()->where_in('role_id',$groupMemberIds)
		->order_by('display_name')
		->as_array()
		->get_many_by();
		$users = array_column($users,NULL,'user_id');

		$headings = ['#','Nhân viên thực hiện','Website','Đã thực hiện'];
		$this->table->set_heading($headings);

		$author_banners = array_group_by($author_banners, 'post_author' );
		$index = 0;
		foreach ($author_banners as $post_author => $rows) 
		{
			$index++;
			foreach ($rows as $i => $row) 
			{
				$website = $terms[$row['term_id']]['term_name'];
				
				if($i == 0) 
				{
					$display_name = $users[$post_author]['display_name'];
					$this->table->add_row(
						array('data'=> $index,'rowspan'=>count($rows)),
						array('data'=> $display_name,'rowspan'=>count($rows)),
						$website,
						$row['number_of_banners']
						);

					continue;
				}
					
				$this->table->add_row($website,$row['number_of_banners']);
			}

			$bannerTotal = array_sum(array_column($rows, 'number_of_banners'));
			$termTotal = count(array_column($rows, 'term_id'));
			$this->table->add_row('','<b>Tổng</b>',"<b>{$termTotal} HĐ</b>","<b>{$bannerTotal}</b>");
		}

		$data['content'] = $this->table->generate();
		$this->data = $data;
		parent::render($this->data, 'blank');
	}

	function task(){

		$this->template->title->prepend('Thống kê công việc');
		$this->load->config('group');
		$data = $this->data;

		$users = $this->admin_m->select('user_id,display_name')
		->where_in('role_id', $this->config->item('group_memberId'))->set_get_active()->order_by('display_name')->get_many_by();

		$time = time();

		$this->table->set_heading('#','Công việc','Phân công cho','Dự án', 'Thời gian thực hiện','Thời hạn');

		$tasks = $this->webgeneral_m->get_posts(
			array(
				'where' => array(
					'posts.end_date <=' => $time + 604800,
					'posts.post_status !=' => 'complete',
					),
				'tax_query' => array('taxonomy'=>'webgeneral', 'field'=> 'term_status', 'terms' =>'publish'),
				'orderby' => 'posts.end_date',
				'order' => 'asc',
				'post_type' => $this->webgeneral_m->get_post_type('task'),
				)
			);

		$no = 1;

		if(!empty($tasks)){

			$post_terms = $this->term_posts_m
			->where_in('post_id', array_map(function($x){return $x->post_id;}, $tasks))->get_many_by();

			$post_terms = $this->term_m
			->distinct()
			->select('term_posts.post_id,term.term_id,term.term_name,term.term_type')
			->where_in('term.term_id', array_map(function($x){return $x->term_id;}, $post_terms))
			->join('term_posts', 'term_posts.term_id = term.term_id', 'RIGHT')	
			->as_array()
			->get_many_by();

			foreach ($tasks as $task) {

				$term = null;

				foreach ($post_terms as $pt) {

					if($pt['post_id'] == $task->post_id){
						$term = $pt;
						break;
					}
				}

				$message_status = '';

				$diff_timestamp  = $task->end_date - $time;

				$past = FALSE;

				if($diff_timestamp < 0){

					$past = TRUE;   

					$diff_timestamp = absint($diff_timestamp);
				} 

				$time_rounds = array (
					31536000 => 'năm',
					2592000 => 'tháng',
					604800 => 'tuần',
					86400 => 'ngày',
					3600 => 'giờ',
					60 => 'phút'
					);

				if($past) $message_status = '<span class="label label-danger">Đã quá hạn {left_time}</span>';
				else{

					$label = $diff_timestamp - (3*24*60*60) > 0 
					? 'label-info'
					:'label-warning';

					$message_status = 
					'<span class="label '.$label.'">Còn lại {left_time}</span>';
				}

				$left_time = '';

				foreach ($time_rounds as $key => $value) {

					if($diff_timestamp < $key) continue;

					$var = floor($diff_timestamp / $key);

					$diff_timestamp = $diff_timestamp - $key*$var;

					$left_time .= $var . ' ' . $value . ' ';
				}

				$message_status = str_replace('{left_time}', $left_time, $message_status);

				$this->table->add_row(array(
					$no,
					anchor(module_url('task/' . $term['term_id']), $task->post_title),
					$this->admin_m->get_field_by_id($task->post_author, 'display_name'),
					$term['term_name'],
					my_date($task->start_date,'d/m/Y') . ' - ' . my_date($task->end_date,'d/m/Y'),
					$message_status
					));

				$no++;
			}
		}

		$data['content'] = $this->table->generate();
		$this->data = $data;
		parent::render($this->data, 'blank');
	}

	function kpi($month = '', $year = '')
	{
		restrict('Webgeneral.Content.Manage,Webgeneral.Content.Access');

		if($time = $this->input->post('time'))
		{
			$month = date('m',$time);
			$year = date('Y',$time);
			redirect(module_url("statistics/kpi/{$month}/{$year}"),'refresh');
		}

		$data = $this->data;
		$this->template->title->prepend('Thống kê bài viết theo nhân viên');

		$year = $year ?: date('Y');
		$month = $month ?: date('m');
		$data['time'] = strtotime("{$year}/{$month}/01");
		$kpi_datetime = date("{$year}/{$month}/01");
		$kpi_content = $this->webgeneral_kpi_m
		->order_by(['user_id'=>'asc','term_id'=>'desc'])
		->get_many_by(['kpi_type'=>'content','kpi_datetime'=>$kpi_datetime]);

		if(empty($kpi_content)) return parent::render($data,'statistics/kpi');

		$i = 1;
		$user_kpis = array_group_by($kpi_content,'user_id');
		$this->table->set_heading('#','Nhân viên','Dự án','KPI', 'Hoàn thành','Còn lại');

		foreach ($user_kpis as $user_id => $kpis) 
		{
			$rowspan = count($kpis) + 1;
			$display_name = $this->admin_m->get_field_by_id($user_id, 'display_name');

			$kpi_total = array_sum(array_column($kpis,'kpi_value'));
			$kpi_total_result = array_sum(array_column($kpis,'result_value'));
			$kpi_total_progress = $this->admin_form->progress_bar($kpi_total_result,$kpi_total);

			$this->table->add_row(['data'=>$i++,'rowspan'=>$rowspan],['data'=>$display_name.br().$kpi_total_progress,'rowspan'=>$rowspan]);

			foreach ($kpis as $kpi) 
			{
				$kpi_term = $this->term_m->get($kpi->term_id);
				$display_name = $this->admin_m->get_field_by_id($kpi->user_id, 'display_name');
				$remain = $kpi->kpi_value - $kpi->result_value;
				$remain = $remain > 0 ? $remain : 0;

				$kpi_progress = $this->admin_form->progress_bar($kpi->result_value,$kpi->kpi_value);

				$this->table->add_row($kpi_term->term_name.br().$kpi_progress,$kpi->kpi_value,$kpi->result_value,$remain);
			}
		}

		$data['content'] = $this->table->generate();
		parent::render($data,'statistics/kpi');
	}

	function content_product($month = '', $year = '')
	{
		restrict('Webgeneral.Content_product.Manage,Webgeneral.Content_product.Access');

		if($time = $this->input->post('time'))
		{
			$month = date('m',$time);
			$year = date('Y',$time);
			redirect(module_url("statistics/content_product/{$month}/{$year}"),'refresh');
		}

		$data = $this->data;
		$this->template->title->prepend('Thống kê sản phẩm theo nhân viên');

		$year = $year ?: date('Y');
		$month = $month ?: date('m');
		$data['time'] = strtotime("{$year}/{$month}/01");
		$kpi_datetime = date("{$year}/{$month}/01");
		$kpi_content = $this->webgeneral_kpi_m
		->order_by(['user_id'=>'asc','term_id'=>'desc'])
		->get_many_by(['kpi_type'=>'content_product','kpi_datetime'=>$kpi_datetime]);

		if(empty($kpi_content)) return parent::render($data,'statistics/content_product');

		$i = 1;
		$user_kpis = array_group_by($kpi_content,'user_id');
		$this->table->set_heading('#','Nhân viên','Dự án','KPI', 'Hoàn thành','Còn lại');

		foreach ($user_kpis as $user_id => $kpis) 
		{
			$rowspan = count($kpis) + 1;
			$display_name = $this->admin_m->get_field_by_id($user_id, 'display_name');

			$kpi_total = array_sum(array_column($kpis,'kpi_value'));
			$kpi_total_result = array_sum(array_column($kpis,'result_value'));
			$kpi_total_progress = $this->admin_form->progress_bar($kpi_total_result,$kpi_total);

			$this->table->add_row(['data'=>$i++,'rowspan'=>$rowspan],['data'=>$display_name.br().$kpi_total_progress,'rowspan'=>$rowspan]);

			foreach ($kpis as $kpi) 
			{
				$kpi_term = $this->term_m->get($kpi->term_id);
				$display_name = $this->admin_m->get_field_by_id($kpi->user_id, 'display_name');
				$remain = $kpi->kpi_value - $kpi->result_value;
				$remain = $remain > 0 ? $remain : 0;

				$kpi_progress = $this->admin_form->progress_bar($kpi->result_value,$kpi->kpi_value);

				$this->table->add_row($kpi_term->term_name.br().$kpi_progress,$kpi->kpi_value,$kpi->result_value,$remain);
			}
		}

		$data['content'] = $this->table->generate();
		parent::render($data,'statistics/content_product');
	}

	function content($year = '',$month = '',$day = '')
	{
		if($this->input->post('time') != NULL)
		{
			$time = $this->mdate->convert_time($this->input->post('time'));
			redirect(module_url('statistics/content/'.date('Y',$time).'/'.date('m',$time).'/'.date('d',$time)),'refresh');
		}

		$year = $year ?: date('Y');
		$month = $month ?: date('m');
		$day = $day ?: date('d');

		$time = $this->mdate->convert_time("{$year}/{$month}/{$day}");

		$kpis = $this->webgeneral_kpi_m
		->where('kpi_datetime',"{$year}-{$month}-01")
		->group_by('user_id,term_id,kpi_type')
		->where_in('kpi_type',['content','content_product'])
		->get_many_by(); 

		$this->table->set_heading('STT','Thành viên','Số HĐ','Bài viết trong ngày','Tác vụ trong ngày',array('colspan'=>3,'data'=>"KPI bài viết tháng {$month}"),array('colspan'=>3,'data'=>"KPI tác vụ tháng {$month}"),array('colspan'=>3,'data'=>"KPI tổng tháng {$month}"));

		$this->table->add_row(array('colspan'=>3,'data'=>''),array('colspan'=>2,'align'=>'center','data'=>my_date($time)),'Thực hiện','KPI','Tiến độ','Thực hiện','KPI','Tiến độ','Thực hiện','KPI','Tiến độ');

		$i = 0;
		$datatable = array();
		$user_kpis = array_group_by($kpis,'user_id');
		foreach ($user_kpis as $user_id => $items)
		{
			$display_name = $this->admin_m->get_field_by_id($user_id,'display_name');

			$posts = $this->post_m
			->select('post_id,post_type')
			->where('start_date >=',$this->mdate->startOfDay($time))
			->where('start_date <',$this->mdate->endOfDay($time))
			->where('post_author',$user_id)
			->where('post_status','publish')
			->where_in('post_type',[$this->webgeneral_m->get_post_type('content'),$this->webgeneral_m->get_post_type('content_product')])
			->get_many_by();

			$count_posts = array_group_by($posts,'post_type');

			$count_content = count($count_posts[$this->webgeneral_m->get_post_type('content')]??NULL);
			$count_content_product = count($count_posts[$this->webgeneral_m->get_post_type('content_product')]??NULL);

			// Tổng số hợp động đang thực hiện
			$count_terms = count(array_group_by($items,'term_id'));

			$kpi_types = array_group_by($items,'kpi_type');

			// total kpi value each type
			$article_kpi_value = (!(empty($kpi_types['content'])) ? array_sum(array_column($kpi_types['content'],'kpi_value')) : 0);
			$product_kpi_value = (!(empty($kpi_types['content_product'])) ? array_sum(array_column($kpi_types['content_product'],'kpi_value')) : 0);

			// total kpi result each type
			$article_kpi_result = (!(empty($kpi_types['content'])) ? array_sum(array_column($kpi_types['content'],'result_value')) : 0);
			$product_kpi_result = (!(empty($kpi_types['content_product'])) ? array_sum(array_column($kpi_types['content_product'],'result_value')) : 0);

			// progress kpi each type
			$article_progress = div($article_kpi_result,$article_kpi_value);
			$product_progress = div($product_kpi_result,$product_kpi_value);

			$kpi_value = $article_kpi_value + $product_kpi_value;
			$kpi_result = $article_kpi_result + $product_kpi_result;
			$kpi_progress = div($kpi_result,$kpi_value);

			$datatable[] = array(
				'serial'=>++$i,
				'display_name'=>$display_name,
				'count_terms'=>$count_terms,

				'count_content'=>$count_content,
				'count_content_product'=>$count_content_product,

				'article_kpi_result'=>$article_kpi_result,
				'article_kpi_value'=>$article_kpi_value,
				'article_progress'=>currency_numberformat($article_progress*100,'%',2),
				
				'product_kpi_result'=>$product_kpi_result,
				'product_kpi_value'=>$product_kpi_value,
				'product_progress'=>currency_numberformat($product_progress*100,'%',2),
				
				'kpi_result'=>$kpi_result,
				'kpi_value'=>$kpi_value,
				'kpi_progress'=>currency_numberformat($kpi_progress*100,'%',2),
				);
		}

		$total_content_product = array_sum(array_column($datatable, 'count_content'));
		$total_count_content_product = array_sum(array_column($datatable, 'count_content_product'));

		$total_article_kpi_result = array_sum(array_column($datatable, 'article_kpi_result'));
		$total_article_kpi_value = array_sum(array_column($datatable, 'article_kpi_value'));
		$total_article_progress = currency_numberformat(div($total_article_kpi_result,$total_article_kpi_value)*100,'%',2);

		$total_product_kpi_result = array_sum(array_column($datatable, 'product_kpi_result'));
		$total_product_kpi_value = array_sum(array_column($datatable, 'product_kpi_value'));
		$total_product_progress = currency_numberformat(div($total_product_kpi_result,$total_product_kpi_value)*100,'%',2);

		$total_kpi_result = array_sum(array_column($datatable, 'kpi_result'));
		$total_kpi_value = array_sum(array_column($datatable, 'kpi_value'));
		$total_progress = currency_numberformat(div($total_kpi_result,$total_kpi_value)*100,'%',2);

		$datatable[] = array(['colspan'=>3,'data'=>'Tổng'],$total_content_product,$total_count_content_product,$total_article_kpi_result,$total_article_kpi_value,$total_article_progress,$total_product_kpi_result,$total_product_kpi_value,$total_product_progress,$total_kpi_result,$total_kpi_value,$total_progress);

		$data['content'] = $this->table->generate($datatable);
		$data['time'] = $time;
		parent::render($data,'statistics/content');
	}
}
/* End of file Statistics.php */
/* Location: ./application/modules/webgeneral/controllers/Statistics.php */