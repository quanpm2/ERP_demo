<?php
class Webgeneral_Package extends Package
{
	private $website_id;
	function __construct()
	{
		parent::__construct();
	}

	public function name(){
		
		return 'Webgeneral';
	}

	public function init(){	

		$this->_load_menu();
		// $this->_update_permissions();
	}

	private function _load_menu()
	{
		if(!is_module_active('webgeneral')) return FALSE;	

		$order = 1;
		$itemId = 'admin-webgeneral';

		if(has_permission('Webgeneral.Index.access'))
		{
			$this->menu->add_item(array(
				'id' => $itemId,
				'name' => 'Web tổng hợp',
				'parent' => null,
				'slug' => admin_url('webgeneral'),
				'order' => $order++,
				'icon' => 'fa fa-krw',
				'is_active' => is_module('webgeneral')
				),'left-service');
		}

		if(!is_module('webgeneral')) return FALSE;

		$this->menu->add_item(array(
		'id' => 'webgeneral-index',
		'name' => 'Dịch vụ',
		'parent' => $itemId,
		'slug' => admin_url('webgeneral'),
		'order' => $order++,
		'icon' => 'fa fa-fw fa-xs fa-database'
		), 'left-service');

		if(has_permission('webgeneral.Index.Access'))
		{
			$this->menu->add_item(array(
			'id' => 'webgeneral-index-on',
			'name' => 'Đang thực hiện',
			'parent' => 'webgeneral-index',
			'slug' => admin_url('webgeneral'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-toggle-on'
			), 'left-service');
		}

		if(has_permission('webgeneral.Done.Access'))
		{
			$this->menu->add_item(array(
			'id' => 'webgeneral-done',
			'name' => 'Hoàn thành',
			'parent' => 'webgeneral-index',
			'slug' => admin_url('webgeneral/done'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-toggle-off'
			), 'left-service');
		}


		$has_content_manage = has_permission('Webgeneral.Content.Manage');
		$has_content_product_manage = has_permission('Webgeneral.Content_product.Manage');
		$is_administrator = ($this->admin_m->role_id == 1);
		$has_task_manage = has_permission('Webgeneral.Task.Manage');
		if($has_content_manage OR $is_administrator OR $has_task_manage OR $has_content_product_manage)
		{
			$this->menu->add_item(array(
			'id' => 'webgeneral-statistics',
			'name' => 'Thống kê',
			'parent' => $itemId,
			'slug' => admin_url('#'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-bar-chart-o'
			), 'left-service');

			if($has_content_manage)
			{
				$this->menu->add_item(array(
					'id' => 'webgeneral-statistics-kpi',
					'name' => 'Nội dung',
					'parent' => 'webgeneral-statistics',
					'slug' => admin_url('webgeneral/statistics/kpi'),
					'order' => $order++,
					'icon' => 'fa fa-fw fa-xs fa-pencil-square-o'
					), 'left-service');
			}

			if($has_content_product_manage)
			{
				$this->menu->add_item(array(
					'id' => 'webgeneral-statistics-content-product',
					'name' => 'Sản phẩm',
					'parent' => 'webgeneral-statistics',
					'slug' => admin_url('webgeneral/statistics/content_product'),
					'order' => $order++,
					'icon' => 'fa fa-fw fa-xs fa-money'
					), 'left-service');
			}

			if($is_administrator)
			{
				$this->menu->add_item(array(
					'id' => 'webgeneral-statistic-banner',
					'name' => 'Banner',
					'parent' => 'webgeneral-statistics',
					'slug' => admin_url('webgeneral/statistics/banner'),
					'order' => $order++,
					'icon' => 'fa fa-fw fa-xs fa-file-photo-o'
					), 'left-service');
			}

			if($has_task_manage)
			{
				$this->menu->add_item(array(
					'id' => 'webgeneral-statistic-task',
					'name' => 'Công việc',
					'parent' => 'webgeneral-statistics',
					'slug' => admin_url('webgeneral/statistics/task'),
					'order' => $order++,
					'icon' => 'fa fa-fw fa-xs fa-tasks'
					), 'left-service');
			}
		}

		if(has_permission('webgeneral.staffs.manage'))
		{

			$this->menu->add_item(array(
			'id' => 'webgeneral-staffs',
			'name' => 'Phân công/Phụ trách',
			'parent' => $itemId,
			'slug' => admin_url('webgeneral/staffs'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-group'
			), 'left-service');
		}

		$term_id = $this->uri->segment(4);
		$check_approve = $this->uri->segment(3);
		if($check_approve == 'wordpress')
		{
			$term_id = $this->uri->segment(5);
		}

		$this->website_id = $term_id;

		$left_navs = array(
			'overview' => array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-xs fa-tachometer',
				),
			'seotraffic'  => array(
				'name' => 'SEO Chặng',
				'icon' => 'fa fa-fw fa-xs fa-share-alt-square',
				),
			'content'  => array(
				'name' => 'Bài viết',
				'icon' => 'fa fa-fw fa-xs fa-pencil-square-o',
				),
			
			'content_product'  => array(
				'name' => 'Sản phẩm',
				'icon' => 'fa fa-fw fa-xs fa-pencil-square-o',
				),
			'task'  => array(
				'name' => 'Công việc',
				'icon' => 'fa fa-fw fa-xs fa-tasks',
				),
			'kpi'  => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-xs fa-heartbeat',
				),
			'setting'  => array(
				'name' => 'Cấu hình',
				'icon' => 'fa fa-fw fa-xs fa-cogs',
				)
		);

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("Webgeneral.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "webgeneral-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
				), 'left-service');
		}

		if(has_permission('Webgeneral.backlink'))
			$this->menu->add_item(array(
				'id' => 'webgeneral-backlink-index',
				'name' => 'Backlink',
				'parent' => $itemId,
				'slug' => module_url("backlink/{$term_id}"),
				'order' => $order++,
				'icon' => ''
				), 'left-service');

		if(has_permission('Webgeneral.ApprovePost'))
		{
			$service_url = get_term_meta_value($term_id, 'wp_service_url');
			$service_user_id = get_term_meta_value($term_id, 'wp_user_id');
			if($service_url && $service_user_id)
			{
				$this->menu->add_item(array(
				'id' => 'webgeneral-sync_posts-index',
				'name' => 'Duyệt bài',
				'parent' => $itemId,
				'slug' => module_url("wordpress/sync_posts/{$term_id}"),
				'order' => $order++,
				'icon' => ''
				), 'left-service');
			}
			
		}

		if(has_permission('Webgeneral.backlink'))
			$this->menu->add_item(array(
				'id' => 'webgeneral-backlink',
				'name' => 'Backlink',
				'parent' => 'webgeneral-backlink-index',
				'slug' => module_url("backlink/{$term_id}"),
				'order' => $order++,
				'icon' => ''
				), 'left-service');

		if(has_permission('Webgeneral.backlink'))
			$this->menu->add_item(array(
				'id' => 'webgeneral-keywords',
				'name' => 'Keywords',
				'parent' => 'webgeneral-backlink-index',
				'slug' => module_url("keywords/{$term_id}"),
				'order' => $order++,
				'icon' => ''
				), 'left-service');
	}

	private function _update_permissions()
	{
		$permissions = array();

		if(!permission_exists('Webgeneral.staffs'))
			$permissions['Webgeneral.staffs'] = array(
				'description' => 'Bảng phân công',
				'actions' => array('manage','access','add','delete','update'));

		if(!permission_exists('Webgeneral.content_product'))
			$permissions['Webgeneral.content_product'] = array(
				'description' => 'Quản lý nội dung - Sản phẩm',
				'actions' => array('manage','access','add','delete','update'));
		
		// if(!permission_exists('Webgeneral.backlink'))
		// 	$permissions['Webgeneral.backlink'] = array(
		// 		'description' => '',
		// 		'actions' => array('manage','access','add','delete','update'));

		// if(!permission_exists('Webgeneral.backlink_keywords'))
		// 	$permissions['Webgeneral.backlink_keywords'] = array(
		// 		'description' => '',
		// 		'actions' => array('manage','access','add','delete','update'));
		
		if(!$permissions) return false;
		foreach($permissions as $name => $value){
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function title()
	{
		return 'Web General';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Dịch vụ Website tổng hợp';
	}
	private function init_permissions()
	{
		$permissions = array();
		
		$permissions['Webgeneral.Staffs'] = array(
			'description' => 'Bảng phân công KPI',
			'actions' => array('manage','access'));

		$permissions['Webgeneral.Done'] = array(
			'description' => 'Dịch vụ đã hoàn thành',
			'actions' => array('manage','access'));

		$permissions['Webgeneral.Index'] = array(
			'description' => 'Trang chính Web tổng hợp',
			'actions' => array('manage','access'));

		$permissions['Webgeneral.Overview'] = array(
			'description' => 'Trang tổng quan',
			'actions' => array('manage','access'));

		$permissions['Webgeneral.Seotraffic'] = array(
			'description' => 'Quản lý SEO chặng',
			'actions' => array('manage','access'));

		$permissions['Webgeneral.Content'] = array(
			'description' => 'Quản lý nội dung',
			'actions' => array('manage','access','add','delete','update'));

		$permissions['Webgeneral.Content_product'] = array(
			'description' => 'Quản lý sản phẩm',
			'actions' => array('manage','access','add','delete','update'));

		$permissions['Webgeneral.Task'] = array(
			'description' => 'Quản lý công việc',
			'actions' => array('manage','access','add','delete','update'));

		$permissions['Webgeneral.Kpi'] = array(
			'description' => 'Quản lý KPI',
			'actions' => array('manage','access','add','delete','update'));

		$permissions['Webgeneral.Setting'] = array(
			'description' => 'Quản lý cấu hình',
			'actions' => array('manage','access','add','delete','update'));

		$permissions['Webgeneral.start_service'] = array(
			'description' => 'Thực hiện dịch vụ',
			'actions' => array('manage'));

		$permissions['Webgeneral.stop_service'] = array(
			'description' => 'Kết thúc dịch vụ',
			'actions' => array('manage'));

		$permissions['Webgeneral.Logreport'] = array(
			'description' => 'Quản lý báo cáo',
			'actions' => array('access','add','delete','update'));

		$permissions['Webgeneral.Sale'] = array(
			'description' => 'Kinh doanh phụ trách',
			'actions' => array('access'));

		return $permissions;

	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);

			$this->role_permission_m
			->insert(array(
					'role_id' => 1, //admin role
					'permission_id' => $permission_id,
					'action' => serialize($actions)
					));
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		$pers = $this->permission_m->like('name','Webgeneral')->get_many_by();

		if($pers)
		foreach($pers as $per)
		{
			$this->permission_m->delete_by_name($per->name);
		}
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}