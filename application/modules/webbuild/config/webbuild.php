<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['packages'] = array(
	'service' =>array(

		'landing_page'=> array(
			'name' => 'landing_page',
			'label'=> 'Gói Thiết Kế Landing Page',
			'icon' => 'fa-scissors',
			'jobs' => array(
				
				['title'=>'Lên bố cục và chức năng','day'=>'+1 days','title_ascii'=>'len bo cuc va chuc nang'],
				['title'=>'Duyệt bố cục và chức năng','day'=>'+1 days','title_ascii'=>'duyet bo cuc va chuc nang'],

				['title'=>'Thiết kế giao diện ban đầu','day'=>'+2 days','title_ascii'=>'thiet ke giao dien ban dau'],
				['title'=>'Duyệt giao diện lần 1','day'=>'+1 days','title_ascii'=>'duyet giao dien lan 1'],
				['title'=>'Chỉnh sửa giao diện lần 1','day'=>'+1 days','title_ascii'=>'chinh sua giao dien lan 1'],
				['title'=>'Tiến hành lập trình động cho Website','day'=>'+2 days','title_ascii'=>'tien hanh lap trinh dong cho website'],
				// In one day
				['title'=>'Tối ưu phiên bản Mobile','day'=>'+1 hour','title_ascii'=>'toi uu phien ban mobile'],
				['title'=>'Đưa Website lên Server chạy thực tế','day'=>'+1 hour','title_ascii'=>'dua website len server chay thuc te'],
				['title'=>'Nghiệm thu và bàn giao','day'=>'+1 day','title_ascii'=>'nghiem thu va ban giao'],

				// In one day
				['title'=>'Chuyển tên miền và chạy thực tế','day'=>'+1 hour','title_ascii'=>'chuyen ten mien va chay thuc te'],
				['title'=>'Đăng ký cài đặt Google Analytics','day'=>'+1 hour','title_ascii'=>'dang ky cai dat google analytics'],
				['title'=>'Đăng ký cài đặt Google Webmaster Tool','day'=>'+1 hour','title_ascii'=>'dang ky cai dat google webmaster tool'],
				['title'=>'Kiểm tra Google PageSpeed Insights','day'=>'+1 hour','title_ascii'=>'kiem tra google pagespeed insights'],
				['title'=>'Kiểm tra và cấu hình Robots','day'=>'+1 hour','title_ascii'=>'kiem tra va cau  hinh robots'],
				['title'=>'Đăng ký Google Analytics','day'=>'+1 hour','title_ascii'=>'dang ky google analytics'],
				['title'=>'Lock cài đặt Plugins, cấu hình bảo mật file config','day'=>'+1 hour','title_ascii'=>'lock cai dat plugin, cau hinh bao mat file config']
			),
			'insert_slot' => 6,
			'price' => 3000000,
			'appendix'=>''
		),

		'clone_web'=> array(
			'name' => 'clone_web',
			'label'=> 'Gói theo mẫu (Clone Website)',
			'icon' => 'fa-cubes',
			'jobs' => array(
				['title'=>'Tiến hành lấy thông tin website (màu sắc, logo, nội dung demo)','day'=>'+3 day','title_ascii'=>'tien hanh lay thong tin website mau sac logo noi dung demo'],
				['title'=>'Đưa Website theo mẫu lên Server chạy thực tế','day'=>'+1 day','title_ascii'=>'dua website theo mau len server chay thuc te'],
				['title'=>'Chỉnh sửa thông tin lần cuối','day'=>'+1 day','title_ascii'=>'chinh sua thong tin lan cuoi'],
				['title'=>'Duyệt Website lần sau cùng','day'=>'+2 day','title_ascii'=>'duyet website lan sau cung'],
				['title'=>'Nghiệm thu và bàn giao','day'=>'+1 hour','title_ascii'=>'nghiem thu va ban giao'],
				['title'=>'Chuyển tên miền và chạy thực tế','day'=>'+1 hour','title_ascii'=>'chuyen ten mien va chay thuc te'],
				['title'=>'Đăng ký cài đặt Google Analytics','day'=>'+1 hour','title_ascii'=>'dang ky cai dat google analytics'],
				['title'=>'Đăng ký cài đặt Google Webmaster Tool','day'=>'+1 hour','title_ascii'=>'dang ky cai dat google webmaster tool'],
				['title'=>'Kiểm tra Google PageSpeed Insights','day'=>'+1 day','title_ascii'=>'kiem tra google pagespeed insights'],
			),
			'insert_slot' => 1,
			'price' => 7000000,
			'appendix'=>''
		),

		'normal'=> array(
			'name' => 'normal',
			'label'=> 'Gói Thiết Kế Website Theo Yêu Cầu',
			'icon' => 'fa-paint-brush',
			'jobs' => array(
				['title'=>'Lên demo bố cục và chức năng', 'day' =>'+1 day','title_ascii'=>'len demo bo cuc va chuc nang'],
				['title'=>'Duyệt demo bố cục và chức năng', 'day' =>'+1 day','title_ascii'=>'duyet demo bo cuc va chuc nang'],
				['title'=>'Thiết kế giao diện lần đầu', 'day' =>'+3 day','title_ascii'=>'thiet ke giao dien lan dau'],
				['title'=>'Duyệt giao diện lần 1', 'day' =>'+1 day','title_ascii'=>'duyet giao dien lan 1'],
				['title'=>'Chỉnh sửa giao diện lần 1', 'day' =>'+2 day','title_ascii'=>'chinh sua giao dien lan 1'],
				['title'=>'Tiến hành lập trình tĩnh theo mẫu', 'day' =>'+2 day','title_ascii'=>'tien hanh lap trinh tinh theo mau'],
				['title'=>'Tiến hành lập trình động cho Website', 'day' =>'+5 day','title_ascii'=>'tien hanh lap trinh dong cho website'],
				['title'=>'Tối ưu phiên bản Mobile', 'day' =>'+2 day','title_ascii'=>'toi uu phien ban Mobile'],

				['title'=>'Đưa Website lên Server chạy thực tế', 'day' =>'+1 day','title_ascii'=>'dua website len Server chay thuc te'],
				['title'=>'Đưa dữ liệu mẫu lên Website', 'day' =>'+1 hour','title_ascii'=>'dua du lieu mau len Website'],
				
				['title'=>'Duyệt Website lần sau cùng', 'day' =>'+1 day','title_ascii'=>'duyet Website lan sau cung'],
				['title'=>'Nghiệm thu và bàn giao', 'day' =>'+1 day','title_ascii'=>'nghiem thu va ban giao'],
				
				['title'=>'Chuyển tên miền và chạy thực tế', 'day' =>'+1 day','title_ascii'=>'chuyen ten mien va chay thuc te'],
				['title'=>'Đăng ký cài đặt Google Analytics', 'day' =>'+1 hour','title_ascii'=>'dang ky Google Analytics'],
				['title'=>'Đăng ký cài đặt Google Webmaster Tool', 'day' =>'+1 hour','title_ascii'=>'dang ky cai dat Google Webmaster Tool'],
				['title'=>'Kiểm tra Google PageSpeed Insights ', 'day' =>'+1 hour','title_ascii'=>'kiem tra Google PageSpeed Insights'],
				['title'=>'Kiểm tra và cấu hình Robots', 'day' =>'+1 hour','title_ascii'=>'kiem tra va cau hinh Robots'],
				['title'=>'Đăng ký Google Analytics', 'day' =>'+1 hour','title_ascii'=>'dang ky Google Analytics'],
				['title'=>'Lock cài đặt Plugins, cấu hình bảo mật file config', 'day' =>'+1 hour','title_ascii'=>'Lock cai dat Plugins va cau hinh bao mat file config']

			),
			'insert_slot' => 7,
			'price' => 9000000,
			'appendix'=>''
		),
	),
	'default' =>'normal'
);

$config['non-working-days'] = array(
	array('date' => '2019/04/30', 'title' => 'Ngày giải phóng miền Nam, thống nhất đất nước 30/4'),
	array('date' => '2019/05/01', 'title' => 'Ngày Quốc tế Lao động 1/5'),
);

$config['sms_status'] = array(
	0 => 'Không gửi SMS',
	1 => 'Đã đặt lịch gửi SMS',
	2 => 'Đã gửi SMS'
);

$config['webbuild-task-status'] = array(
	'publish' => 'Mới tạo',
	'process' => 'Đang thực hiện',
	'complete' => 'Hoàn thành'
);