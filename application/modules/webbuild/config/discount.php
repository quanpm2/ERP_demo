<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['discount']['packages'] = array(
	'normal' => array(
		'name' => 'normal',
		'label' => 'Chương trình giảm giá bình thường (tối đa 20% của giá TKW sau khi đã trừ đi 10tr giá cơ bản)',
		'field_ref' => 'service_price',
		'sale' => array('min' => 0, 'max' => 20),
	),
	'cross-sell' => array(
		'name' => 'cross-sell',
		'label' => 'Chương trình Cross-sell (Giảm tối đa 20% giá trị TWK dành cho KH đã sử dụng các dịch vụ khác.)',
		'field_ref' => 'service_price',
		'sale' => array('min' => 0, 'max' => 20)
	),

	'custom' => array(
		'name' => 'custom',
		'label' => 'Chương trình giảm giá linh hoạt (Giảm tối đa 30% giá trị TKW)',
		'field_ref' => 'service_price',
		'sale' => array('min' => 0, 'max' => 30)
	),
); 