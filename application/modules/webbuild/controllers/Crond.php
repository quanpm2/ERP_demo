<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crond extends Public_Controller 
{
	function __construct()
	{
		parent::__construct();
		$models = array(
			'webbuild/webbuild_report_m'
			);
		$this->load->model($models);
	}

	public function index()
	{
		// Gửi tin nhắn hàng loạt
		$terms = $this->term_m->get_many_by(['term_type' =>'webdesign','term_status' =>'publish']);
		if(empty($terms)) return FALSE;
		
		// $this->send_sms_tasks_2customer($terms);			
		$this->send_email_complete_tasks_2customer($terms);

		//HieuPT: test send mail
		// $this->send_email_remind_sale_close_project($terms);

		$hour = date('G');
		$day_of_week = date('w');
		if($hour == 10 && $day_of_week == 1)
		{
			$this->webbuild_report_m->send_new_themes_mail2admin();
		}
	}

	private function send_sms_tasks_2customer($terms = array())
	{
		if(empty($terms)) return FALSE;

		$phone_send = array();
		$term_cache = array();

		foreach($terms as $term)
		{
			$phone_send[$term->term_id] = $this->webbuild_report_m->send_sms($term->term_id);
			$term_cache[$term->term_id] = $term->term_name;
		}

		$i = 1;
		$content = '';
		if($phone_send)
		{
			$this->table->set_caption('Báo cáo gửi SMS định kỳ');
			foreach($phone_send as $term_id => $sms)
			{
				if(is_array($sms['msg']))
				{
					foreach($sms['msg'] as $r)
					{
						$msg = $r['recipient'].' - '.$r['title'];
						$this->table->add_row($i++, @$term_cache[$term_id], $msg);
					}
				}
				else
				{
					$this->table->add_row($i++, @$term_cache[$term_id], $sms['msg']);
				}
			}
			$content.= $this->table->generate();
		}

		$this->load->library('email');
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to('support@webdoctor.vn');
		$title = 'Báo cáo gửi báo cáo SMS thiết kế Web ngày '.date('d/m/Y H:i:s');

		$this->email->subject($title);
		$this->email->message($content);
		$send_status = true;
		$send_status = $this->email->send();
		echo $content;
	}

	private function send_email_complete_tasks_2customer($terms = array())
	{
		if(empty($terms)) return FALSE;

		// Gửi mail hàng loạt với nhiều hợp đồng đã hoàn thành
		foreach($terms as $term) 
		{
			$send_mail = $this->webbuild_report_m->send_tasks_complete_mail2customer($term->term_id);
		}

		return TRUE;
	}

	private function send_email_remind_sale_close_project($terms = array())
	{
		if(empty($terms)) return FALSE;

		foreach($terms as $term) 
		{
			$send_mail = $this->webbuild_report_m->send_email_remind_sale_close_project($term->term_id);
		}
		return true;
	}
}
