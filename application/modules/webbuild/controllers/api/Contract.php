<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends REST_Controller
{
	function __construct()
	{
		parent::__construct('webbuild/rest');
		$this->load->model('usermeta_m');
		$this->load->model('termmeta_m');
		$this->load->model('permission_m');
		$this->load->model('staffs/admin_m');
		$this->load->model('webbuild/webbuild_m');
	}

	public function index_get()
	{
		$default 	= array('id' => '', 'meta' => '', 'field' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if( empty($args['id']) || empty($args['meta'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		$data 			= array('msg' => 'Xử lý không thành công', 'data' => []);
		$model_names 	= array('term_m','termmeta_m', 'staffs/admin_m', 'contract/contract_m', 'webbuild/webbuild_m', 'term_users_m');
		$this->load->model($model_names);

		if(FALSE === $this->contract_m->has_permission($args['id'], 'admin.contract.view'))
		{
			$data['msg'] = 'Không có quyền truy xuất hoặc hợp đồng không khả dụng !';
			parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
		}

		$data = array('id' => $args['id']);
		$args['meta'] = is_string($args['meta']) ? explode(',', $args['meta']) : $args['meta'];
		foreach ($args['meta'] as $meta)
		{
			$_value = get_term_meta_value($args['id'], $meta);
			$data[$meta] = is_serialized($_value) ? unserialize($_value) : $_value;
		}		

		$response['data'] = $data;
		parent::response($response);
	}

	public function config_get()
	{
		$default 	= array('name' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if( ! has_permission('admin.contract.view')) parent::response(['msg'=>'Quyền hạn không hợp lệ.'], parent::HTTP_UNAUTHORIZED);

		$this->config->load('webbuild/webbuild');
		$this->config->load('webbuild/discount');

		$response = array(
			'msg' => 'Dữ liệu tải thành công',
			'data' => array(
				'services' => $this->config->item('service', 'packages'),
				'discount' => $this->config->item('discount')
			)
		);

		if( ! empty($args['id']))
		{
			$this->load->model('contract/discount_m');
			$discount_m = $this->discount_m->set_contract($args['id'])->get_instance();
			$response['data']['discount']['packages'] = $discount_m->get_packages();
		}

		parent::response($response);
	}

	public function service_post()
	{
		$default 	= array();
		$args 		= wp_parse_args(parent::post(NULL, TRUE), $default);
		$model_names 	= array('term_m','termmeta_m', 'staffs/admin_m', 'contract/contract_m', 'webbuild/webbuild_m', 'term_users_m');
		$this->load->model($model_names);

		if( empty($args) || empty($args['id']) || empty($args['meta']))
		{	
			parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);
		}

		if(FALSE === $this->contract_m->has_permission($args['id'], 'admin.contract.view'))
		{
			parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
		}
		
		$contract 	= $this->contract_m->get($args['id']);
		$meta 		= $args['meta'];

		$this->config->load('webbuild/webbuild');
		$config_packages = $this->config->item('service', 'packages');
		
		/* VALIDATE : GIÁ TRỊ GÓI THIẾT KẾ WEBSITE*/
		if( empty($meta['webdesign_price']) 
			|| empty($config_packages[$meta['service_package']]) 
			|| $config_packages[$meta['service_package']]['price'] != $meta['webdesign_price'])
			parent::response('Gói dịch vụ không hợp lệ hoặc không còn khả dụng.', parent::HTTP_UNAUTHORIZED);

		$total = $meta['webdesign_price'];
		/* CHỨC NĂNG NÂNG CAO */
		if( ! empty($meta['advanced_functions'])) $total+= array_sum(array_column($meta['advanced_functions'], 'value'));

		/* CHƯƠNG TRÌNH GIẢM GIÁ */ 
		if( ! empty($meta['discount_plan']))
		{
			$this->config->load('webbuild/discount');
			$discount_packages_conf = $this->config->item('packages', 'discount');

			/* VALIDATE CHƯƠNG TRÌNH GIẢM GIÁ THEO CẤU HÌNH */
			if( empty($discount_packages_conf[$meta['discount_plan']]))
			{
				parent::response('Chương trình giảm giá không còn khả dụng.', parent::HTTP_NOT_ACCEPTABLE);
			}

			$this->load->model('contract/discount_m');
			$discount_m = $this->discount_m->set_contract($contract)->get_instance();
			try
			{
				$discount_m->validate($meta['discount_amount'], $total, $meta['discount_plan']);
			}
			catch (Exception $e)
			{
				parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
			}
		}

		$messages = array();

		/* AFTER ALL VALIDATION */
		foreach ($meta as $key => $value)
		{
			$value = (is_array($value) || is_object($value)) ? serialize($value) : $value;
			update_term_meta($args['id'], $key, $value);
		}

		/* Init webbuild contract model instance */
		$this->webbuild_m->set_contract($args['id']);

		// SYNC CONTRACT-VALUE
		$this->load->model('webbuild/webbuild_contract_m');
		update_term_meta($args['id'], 'contract_value', (int) $this->webbuild_m->calc_contract_value());
		$messages[] = 'Giá trị hợp đồng đã được cập nhật mới theo chi tiết dịch vụ thành công !';


		// ASYNC CONTRACT DAYS TO MAP WITH SERVICE-PACKAGE TASKLIST
		$working_days = (int) $this->webbuild_m->get_working_days(); // Get Mandays (exclude holiday , weekends, non-working days)
		update_term_meta($args['id'], 'working_days', $this->webbuild_m->get_working_days()); // Update meta "working days (man-days)"

		$contract_begin = get_term_meta_value($args['id'], 'contract_begin');
		$contract_begin = start_of_day($contract_begin);

		$tasks 		= $this->webbuild_m->get_behaviour_m()->get_default_tasks($contract_begin);
		$last_task 	= end($tasks);
		update_term_meta($args['id'], 'contract_end', start_of_day($last_task['end_date']));// Update meta "contract_end" computed from tasks
		$messages[] = 'Thời gian hợp đồng đã được đồng bộ theo tasklist';


		// RE-CREATED NEW INVOICES
		$this->load->model('term_posts_m');
		if($invoices = $this->term_posts_m->get_term_posts($args['id'], 'contract-invoices'))
		{
			$model_names = array('contract/invoice_m','contract/invoice_item_m','post_m');
			$this->load->model($model_names);

			$invoice_ids = array_column($invoices, 'post_id');
			$this->post_m->delete_many($invoice_ids);// Xóa tất cả invoice cũ		
			$this->invoice_item_m->where_in('inv_id', $invoice_ids)->delete_by(); // Xóa tất cả invoice items cũ
			$this->term_posts_m->delete_term_posts($args['id'], $invoice_ids); // Xóa liên kết hợp đồng - đợt thanh toán
			
			$this->webbuild_m->create_invoices(); // Tạo mới đợt thanh toán
			$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

			$this->webbuild_m->sync_all_amount(); // Đồng bộ số liệu thu chi
			$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
		}

		parent::response(['msg' => $messages]);
	}

	public function task_get()
	{
		$default 	= array('contract_id' => '', 'id' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if(empty($args['contract_id'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);


		$is_contract = $this->webbuild_m->set_contract($args['contract_id']);
		if( ! $is_contract) parent::response('Tham số truyền vào không hợp lệ [0]', parent::HTTP_NOT_ACCEPTABLE);


		if( ! $this->webbuild_m->has_permission($args['contract_id'], 'webbuild.task.access')) parent::response('Quyền truy cập không hợp lệ. [0]', parent::HTTP_UNAUTHORIZED);

		$data = NULL;

		if( ! empty($args['package'])) // List Default tasklist by package
		{
			$tasklist = $this->webbuild_m->get_behaviour_m()->get_default_tasks(strtotime('+1 days', start_of_day()), $args['package']);
			if( ! $tasklist) parent::response('Tác vụ không tìm thấy hoặc đã bị xóa', parent::HTTP_NOT_FOUND);

			$tasklist = array_map(function($x){
				$x['post_title'] 	= $x['title'];
				$x['post_author'] 	= $this->admin_m->id;
				$x['post_type'] 	= $this->webbuild_task_m->post_type;
				return $x;
			}, $tasklist);

			parent::response(['data' => $tasklist]);
		}

		if(!empty($args['id'])) // Get first
		{
			$tasks = $this->term_posts_m->get_term_posts($args['contract_id'], $this->webbuild_task_m->post_type, ['where' => ['term_id' => $args['contract_id'], 'posts.post_id' => $args['id']], 'fields' => 'posts.post_id, post_author, post_title, post_status, created_on, start_date, end_date', 'orderby' => 'posts.start_date']);
			if( ! $tasks) parent::response('Tác vụ không tìm thấy hoặc đã bị xóa', parent::HTTP_NOT_FOUND);

			$task = reset($tasks);
			parent::response(array('data' => $task));
		}

		// GET MANY BY
		$tasks = $this->term_posts_m->get_term_posts($args['contract_id'], $this->webbuild_task_m->post_type, ['fields' => 'posts.post_id, post_author, post_title, post_status, created_on, start_date, end_date', 'orderby' => 'posts.start_date']);

		if(empty($tasks)) parent::response('Không tìm thấy kêt quả phù hợp.', parent::HTTP_NOT_FOUND);

		$tasks = array_map(function($x){ $x->sms_status = (int) get_post_meta_value($x->post_id, 'sms_status'); return $x;}, $tasks);
		parent::response(array('data' => array_values($tasks)));
	}


	/**
	 * API UPDATE TASK
	 */
	public function task_put()
	{
		$default 	= array('contract_id' => parent::get('contract_id', NULL), 'id' => parent::get('id', NULL));
		$args 		= wp_parse_args(parent::put(NULL, TRUE), $default);

		if(empty($args['contract_id'])) parent::response('Tham số truyền vào không hợp lệ [0]', parent::HTTP_NOT_ACCEPTABLE);

		$is_contract = $this->webbuild_m->set_contract($args['contract_id']);
		if( ! $is_contract) parent::response('Tham số truyền vào không hợp lệ [1]', parent::HTTP_NOT_ACCEPTABLE);

		if( ! $this->webbuild_m->has_permission($args['contract_id'], 'webbuild.task.update')) parent::response('Quyền truy cập không hợp lệ. [0]', parent::HTTP_UNAUTHORIZED);
		$tasks = $this->term_posts_m->get_term_posts($args['contract_id'], $this->webbuild_task_m->post_type, ['where' => ['term_id' => $args['contract_id'], 'posts.post_id' => $args['id']], 'fields' => 'posts.post_id, post_author, post_title, post_status, created_on, start_date, end_date', 'orderby' => 'posts.start_date']);
		if(empty($tasks)) parent::response('Tác vụ không được tìm thấy hoặc đã bị xóa [0]', parent::HTTP_NOT_FOUND);

		if( ! $this->webbuild_task_m->update($args['id'], $args['edit'])) 
			parent::response('Quá trình xử lý không thành công .', parent::HTTP_SERVICE_UNAVAILABLE);

		$this->config->load('webbuild/webbuild');
		if($this->config->item(($args['meta']['sms_status']??''), 'sms_status') && 2 != get_post_meta_value($args['id'], 'sms_status')) 
		{
			update_post_meta($args['id'], 'sms_status', ($args['meta']['sms_status'] ?? 0));
		}

		$this->webbuild_m->get_behaviour_m()->sync_all_progress();
		parent::response('Tác vụ đã được cập nhật thành công .');
	}

	/**
	 * API CREATE TASK
	 */
	public function task_post()
	{
		$default 	= array('contract_id' => parent::get('contract_id', NULL));
		$args 		= wp_parse_args(parent::post(NULL, TRUE), $default);

		if(empty($args['contract_id'])) parent::response('Tham số truyền vào không hợp lệ [0]', parent::HTTP_NOT_ACCEPTABLE);
		$is_contract = $this->webbuild_m->set_contract($args['contract_id']);
		if( ! $is_contract) parent::response('Tham số truyền vào không hợp lệ [1]', parent::HTTP_NOT_ACCEPTABLE);

		if( ! $this->webbuild_m->has_permission($args['contract_id'], 'webbuild.task.add')) parent::response('Quyền truy cập không hợp lệ. [0]', parent::HTTP_UNAUTHORIZED);

		if(empty($args['edit']['start_date']) || empty($args['edit']['start_date']))
		{
			parent::response('Thời gian thực hiện tác vụ không được để trống', parent::HTTP_NOT_ACCEPTABLE);
		}

		$args['edit']['start_date'] = start_of_day($args['edit']['start_date']);
		$args['edit']['end_date'] 	= end_of_day($args['edit']['end_date']);

		if($args['edit']['start_date'] > $args['edit']['end_date'])
			parent::response('Thời gian tác vụ không hợp lệ', parent::HTTP_NOT_ACCEPTABLE);

		$this->config->load('webbuild/webbuild');

		$args['edit']['post_status'] = $args['edit']['post_status'] ?? 'publish';

		if( ! $this->config->item('publish', 'webbuild-task-status'))
			parent::response('Tham số truyền vào không hợp lệ [1]', parent::HTTP_NOT_ACCEPTABLE);

		if( ! $this->config->item( ($args['meta']['sms_status'] ?? 0), 'sms_status'))
			parent::response('Tham số truyền vào không hợp lệ [2]', parent::HTTP_NOT_ACCEPTABLE);

		$args['edit']['post_author'] = $this->admin_m->id;
		$inserted_id = $this->webbuild_task_m->insert($args['edit']);
		if( ! $inserted_id) parent::response('Quá trình xử lý không thành công [0] .', parent::HTTP_SERVICE_UNAVAILABLE);

		update_post_meta($inserted_id, 'sms_status', ($args['meta']['sms_status'] ?? 0));
		$this->term_posts_m->set_term_posts($args['contract_id'], array($inserted_id), $this->webbuild_task_m->post_type);

		$this->webbuild_m->get_behaviour_m()->sync_all_progress();

		parent::response('Tác vụ đã được cập nhật thành công .');
	}


	/**
	 * API DELETE TASK
	 */
	public function task_delete()
	{
		$default 	= array('contract_id' => parent::get('contract_id', NULL), 'id' => parent::get('id', NULL));
		$args 		= wp_parse_args(parent::put(NULL, TRUE), $default);
		if(empty($args['contract_id'])) parent::response('Tham số truyền vào không hợp lệ [0]', parent::HTTP_NOT_ACCEPTABLE);

		if(FALSE === $this->webbuild_m->set_contract($args['contract_id']))
		{
			parent::response('Tham số truyền vào không hợp lệ [1]', parent::HTTP_NOT_ACCEPTABLE);
		}
		
		$this->webbuild_m->get_behaviour_m()->sync_all_progress();

		if( ! $this->webbuild_m->has_permission($args['contract_id'], 'webbuild.task.delete'))
			parent::response('Quyền truy cập không hợp lệ. [0]', parent::HTTP_UNAUTHORIZED);
		try
		{
			$this->webbuild_m->delete_task_by($args['id']);
			parent::response('Tác vụ đã xóa thành công.');
		}
		catch (Exception $e) 
		{
			parent::response('Tham số truyền vào không hợp lệ [1]', parent::HTTP_NOT_ACCEPTABLE);	
		}
	}
}
/* End of file Contract.php */
/* Location: ./application/modules/wservice/controllers/api/Contract.php */