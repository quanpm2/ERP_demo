<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Webbuild extends Admin_Controller {

	public $model = '';
	private $website_id;
	private $term;
	private $term_type = 'webdesign';

	function __construct()
	{
		$this->autoload['models'][] = 'contract/contract_m';
		$this->autoload['models'][] = 'webbuild/webbuild_m';
		$this->autoload['models'][] = 'webbuild/webbuild_config_m';
		$this->autoload['models'][] = 'webbuild/webbuild_task_m';
		$this->autoload['models'][] = 'webgeneral/webgeneral_kpi_m';
		$this->autoload['models'][] = 'webgeneral/webgeneral_seotraffic_m';
		$this->autoload['models'][] = 'webbuild/webbuild_callback_m';
		$this->autoload['models'][] = 'message/message_m';
		$this->autoload['models'][] = 'message/note_contract_m';

		parent::__construct();

		$this->load->config('webbuild/webbuild');
		$this->init_website();
	}

	private function init_website()
	{
		$term_id = $this->uri->segment(4);
		$method  = $this->uri->segment(3);
		$is_allowed_method = (!in_array($method, array('index','done','ajax','statistic')));
		if(!$is_allowed_method || empty($term_id)) return FALSE;

		$term = $this->term_m
		->where_in('term_status', ['publish', 'ending'])
		->where('term_type', $this->term_type)
		->where('term_id',$term_id)
		->get_by();

		if(empty($term) OR !$this->is_assigned($term_id)) 
		{
			$this->messages->error('Truy cập #'.$term_id.' không hợp lệ !!!');
			redirect(module_url(),'refresh');
		}

		$this->template->title->set(strtoupper(' '.$term->term_name));
		$this->website_id = $this->data['term_id'] = $term_id;
		$this->data['term'] = $this->term = $term;
	}


	public function dashboard()
	{
		$data = $this->data;
		$this->template->is_box_open->set(1);
		parent::render($data);
	}

	/**
	 * PAGE LIST
	 */
	public function index()
	{
		restrict('Webbuild.Index.Access');
		
		$this->template->is_box_open->set(1);
		$this->template->title->set('Tổng quan dịch vụ thiết kế Website');
		parent::render($this->data);
	}

	public function task($term_id = 0)
	{
		if(FALSE === $this->webbuild_m->has_permission($term_id, 'webbuild.task.access'))
		{
			$this->messages->error('Không có quyền thực hiện tác vụ này.');
			redirect(module_url(),'refresh');
		}
		
		$this->template->title->prepend('Công việc');
		$this->data['term_id'] = $term_id;
		$this->template->is_box_open->set(1);
		
		parent::render($this->data);
	}
	
	public function logreport($term_id = 0, $is_send = 0)
	{
		$this->template->title->prepend('Các báo cáo đã gửi');
		$data = $this->data;
		$this->load->model('webgeneral_contract_m');

		// $this->config->load('table_mail');
		// $this->table->set_template($this->config->item('mail_template'));
		// // prd($this->config->item('mail_template'));
		// $this->table->set_heading('xxx');
		// $this->table->set_caption('<b>HTlove</b>');
		// $this->table->add_row('xxx');
		// $this->table->add_row('yyy');

		// echo  $this->webgeneral_contract_m->send_mail2admin($term_id,$is_send);
		// $this->load->view();
		$this->data = $data;
		parent::render($this->data, 'blank');
	}

	function asendmail()
	{
		$this->load->library('email');
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to('hieupt@webdoctor.vn');
		$this->email->subject('Mail test '.date('d/m/Y'));
		$this->email->message('Hi');
		$send_status = $this->email->send();
		prd($send_status);
	}

	// tambt@webdoctor.vn	
	public function view_msg($term_id = 0) {
		restrict('Webbuild.View_msg.Access');
		
		$this->load->model('message_m') ;
		$this->load->model('note_contract_m') ;

		$data['term_id'] 			 = $term_id ;

		$data['direct_chat_msg']     = $this->note_contract_m->load($term_id) ;

		parent::render($data);
	}

	
	public function overview($term_id = 0)
	{
		redirect(admin_url("webbuild/task/{$term_id}"), 'refresh');
		restrict('Webbuild.Overview.Access');

		$term = $this->term_m->get($term_id);
		if(empty($term)){
			$this->messages->error('Dịch vụ không tồn tại');
			redirect(module_url(),'refresh');
		}


		$time = time();	
		$day = date('d',$time) - 1;
		if($day == 0){
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}
		$date_start = date('01-m-Y',$time);
		$day = str_pad($day, 2, "0", STR_PAD_LEFT);
		$date_end = date($day.'-m-Y', $time);

		$description = 'Từ '.$date_start.' đến '.$date_end;

		$time_start = $this->mdate->startOfDay(strtotime($date_start));
		$time_end   = $this->mdate->endOfDay(strtotime($date_end));

		$this->template->description->append($description);
		$this->template->title->prepend('Tổng quan');
		$this->template->javascript->add('plugins/chartjs/Chart.js');

		// $statistic_index_data = $technical_staffs->webgeneral_overview_m->get_statistic_index_data($term,$time_start,$time_end);
		// $char_data = $this->webgeneral_overview_m->build_seotraffic_chart($term_id,my_date($time_start,'Y-m-d'),my_date($time_end,'Y-m-d'));
		// $content_data = $this->webgeneral_overview_m->get_content_table($term_id,$time_start,$time_end);
		// $task_data = $this->webgeneral_overview_m->get_tasklist_table($term_id,$time_start,$time_end);
		// $email_report_log = $this->webgeneral_overview_m->get_email_log_table($term_id,$time_start,$time_end);
		// $sms_report_log = $this->webgeneral_overview_m->get_sms_log_table($term_id,$time_start,$time_end);
		// $customer_table = $this->webgeneral_overview_m->get_customer_info($term_id);

		// // $data = array_merge_recursive($sms_report_log,$customer_table);
		// $data = array();
		// parent::render($data);

		$this->load->model('message_m');
		$this->load->model('note_contract_m');

		$data['term_id'] 			 = $term_id ;
		
		$data['direct_chat_msg']     = $this->note_contract_m->load($term_id) ;

		$data['has_permission'] 	 = true ;

		parent::render($data);
	}

	public function kpi($term_id = 0, $delete_id= 0)
	{
		restrict('Webbuild.Kpi.Access');
		
		$this->template->title->prepend('KPI');
		// $this->load->config('group');
		$data = $this->data;
		$this->submit_kpi($term_id,$delete_id);

		$data['kpi_type'] = $this->config->item('services');
		$data['kpi_desc'] = array('seotraffic'=>'Điền KPI cần đạt', 'content'=>'Số lượng bài viết');
		$data['term_id'] = $term_id;

		$data['time'] = $this->mdate->startOfMonth(time());
		$time_start = get_term_meta_value($term_id, 'contract_begin');
		$time_end = get_term_meta_value($term_id, 'contract_end');

		//+2 month
		$time_start = strtotime('-1 month');
		$time_end = strtotime('+2 month', $time_end);

		$data['time_start'] = $time_start;
		$data['time_end'] = $time_end;
		$data['time_start'] = $this->mdate->startOfDay($data['time_start']);
		$data['time_end'] = $this->mdate->endOfDay($data['time_end']);

		$targets = $this->webgeneral_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by([ 'term_id' => $term_id ]);
		
		$data['targets'] = array();
		$data['targets']['content'] = array();
		$data['targets']['seotraffic'] = array();
		$data['targets']['tech'] = array();
		$data['targets']['design'] = array();
		$data['targets']['backlink'] = array();
		$data['targets']['keyword'] = array();
		foreach($targets as $target)
		{
			$data['targets'][$target['kpi_type']][] = $target;
		}
		
		$data['date_contract'] = array();
		for($i=$data['time_start'];$i< $data['time_end']; $i++)
		{
			$data['date_contract'][$time_start]  = date('Y-m', $time_start);
			$date = $data['date_contract'][$time_start];

			$time_start = strtotime('+1 month', strtotime($date));
			if($time_start > $time_end)
				break;
		}

		$roles = $this->option_m->get_value('group_webbuild_ids', TRUE);
		$users = $this->admin_m
		->select('user_id,display_name,user_email')
		->set_role($roles)->set_get_active()
		->order_by('display_name')->get_all();

		$users = array_map(function($x){ $x->display_name = "{$x->display_name} - {$x->user_email}"; return $x; }, $users);
		$data['users']	= key_value($users, 'user_id', 'display_name');

		$this->data = $data;
		parent::render($this->data);
	}
	public function submit_kpi($term_id = 0,$delete_id=0)
	{
		if($delete_id >0)
		{
			$this->_delete_kpi($term_id,$delete_id);
			redirect(module_url('kpi/'.$term_id.'/'),'refresh');
		}

		$post = $this->input->post();
		if(empty($post)) return FALSE;

		if( ! $this->webbuild_m->has_permission($term_id, 'Webbuild.kpi.add'))
		{	
			$this->messages->error('Không có quyền thực hiện tác vụ này.');
			redirect(module_url("kpi/{$term_id}"),'refresh');
		}

		$insert = array();
		$insert['kpi_datetime'] = $this->input->post('target_date');
		$insert['user_id'] = $this->input->post('user_id');
		$insert['kpi_value'] = (int)$this->input->post('target_post');
		$insert['target_type'] = $this->input->post('target_type');

		$kpi_type = $this->input->post('target_type');
		if(empty($kpi_type))
		{
			$is_submit_content = $this->input->post('submit_kpi_content');
			$kpi_type = ($is_submit_content) ? 'content' : 'seotraffic';
		}

		if($insert['kpi_datetime'] >= $this->mdate->startOfMonth())
		{
			$this->webgeneral_kpi_m->update_kpi_value($term_id, $kpi_type,$insert['kpi_value'], $insert['kpi_datetime'], $insert['user_id']);
			$this->webgeneral_kpi_m->delete_cache($delete_id);
			
			# Update relation of term's user in term_users table
			$this->term_users_m->set_relations_by_term($term_id,[$post['user_id']], 'admin');

			$this->messages->success('Cập nhật thành công');
		}
		else
		{
			$this->messages->error('Cập nhật không thành công do tháng cập nhật nhỏ hơn tháng hiện tại');
		}
		redirect(current_url(),'refresh');
	}

	private function _delete_kpi($term_id = 0,$delete_id=0)
	{
		restrict('webbuild.kpi.Delete');
		if(FALSE === $this->webbuild_m->has_permission($term_id, 'webbuild.kpi.delete'))
		{
			$this->messages->error('Không có quyền thực hiện tác vụ này.');
			return FALSE;
		}

		$check = $this->webgeneral_kpi_m->get($delete_id);
		$is_deleted = FALSE;
		if($check)
		{
			if(strtotime($check->kpi_datetime) >= $this->mdate->startOfMonth())
			{
				$this->webgeneral_kpi_m->delete_cache($delete_id);
				$this->webgeneral_kpi_m->delete($delete_id);
				$is_deleted = TRUE;
				
				// Force delete relation of user vs term
				$this->term_users_m->delete_term_users($term_id, $check->user_id);

				$this->messages->success('Xóa thành công');
			}
		}
		($is_deleted) OR $this->messages->error('Xóa không thành công');
	}


	/**
	 * Determines if it has permission.
	 *
	 * @param      string   $name     The name
	 * @param      string   $action   The action
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  True if has permission, False otherwise.
	 */
	private function has_permission($name ='', $action ='', $term_id = 0)
	{
		if($term_id == 0)
			$term_id = $this->website_id;
		return $this->webbuild_m->has_permission($term_id,"{$name}.{$action}");
	}

	
	/**
	 * CONFIGURATION SERVICE PAGE
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function setting($term_id = 0)
	{
		
		restrict('Webbuild.setting');

		$this->setting_submit($term_id);

		$data = $this->data;
		
		$data['term'] 		= $this->term;
		$data['term_id'] 	= $term_id;

		$this->load->config('webbuild/webbuild');
		$service_packages 			= $this->config->item('service','packages');

		$service_package 	= get_term_meta_value($term_id, 'package_service');
		if(empty($service_packages[$service_package]))
		{
			$service_package = $this->config->item('default','packages');
		}

		$data['meta'] = array(
			'phone_report'		=> get_term_meta_value($term_id, 'phone_report'),
			'mail_report'		=> get_term_meta_value($term_id, 'mail_report'),
			// 'domains_report'    => get_term_meta_value($term_id, 'domains_report'),
			'package_service'	=> $service_package,
		);

		$data['service_package']	= $service_package;
		$data['service_packages'] 	= array_values($service_packages);

		$data['jsdataobject'] = json_encode($data);

		$this->template->title->prepend('Cấu hình');
		$this->template->is_box_open->set(1);

		parent::render($data);
	}

	protected function setting_submit($term_id = 0)
	{
		$post = $this->input->post();
		if(empty($post)) return FALSE;

		if( ! empty($post['start_process']))
		{
			if( ! $this->webbuild_m->has_permission($term_id, 'Webbuild.start_service'))
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$term = $this->term_m->get($term_id);

			$this->load->model('webbuild_contract_m');
			$this->webbuild_contract_m->proc_service($term);
			$this->messages->success('Cập nhật thành công');

			/* Phân tích hợp đồng ký mới | tái ký */
			$this->load->model('contract/base_contract_m');
			$this->base_contract_m->detect_first_contract($term_id);


			/* Gửi SMS thông báo kích hoạt hợp đồng đến khách hàng */
			$this->load->model('contract/contract_report_m');
			$this->contract_report_m->send_sms_activation_2customer($term_id);
			

			redirect(module_url("setting/{$term_id}"),'refresh');
		}
		else if( ! empty($post['end_process']))
		{
			if( ! $this->webbuild_m->has_permission($term_id, 'Webbuild.stop_service'))
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$term = $this->term_m->get($term_id);

			$this->load->model('webbuild_contract_m');
			$stat = $this->webbuild_contract_m->stop_service($term);
			if($stat) $this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
		else if(!empty($post['submit']))
		{
			$metadatas = $post['meta'];

			if( ! $this->webbuild_m->has_permission($term_id, 'Webbuild.setting.update'))
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$metadatas = $post['meta'];
			$result =array('success'=>true,'msg'=>'Cập nhật thành công');
			if($metadatas)
			{
				
				foreach($metadatas as $key=>$value)
				{
					
					if($key == 'report_key' && $value == '')
					{
						$value = $this->webgeneral_seotraffic_m->encrypt('',$term_id);
					}
					
					if($key == 'phone_report'){
						$phones = explode(",",$value);
						
						foreach ($phones as &$phone) {
							$phone = $this->vn_to_en($phone);
							$phone = preg_replace('([a-zA-Z]+)', '', $phone);
						}
						$value = implode(',',$phones);
					}

					if($key =='mail_report' && $value !=''){
						$emails = explode(",",$value);
						
						foreach ($emails as &$email) {
							if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
								$result  = array('success'=>false,'msg'=>'Email không hợp lệ !!');
								break;
							}
						}
					}
					if($result['success']){
						$value = str_replace(' ','',$value);
						update_term_meta($term_id,$key,$value);
					}
				}
			}
			if($result['success']){
				$this->messages->success($result['msg']);
				redirect(module_url("setting/{$term_id}"),'refresh');
			}
			$this->messages->error($result['msg']);
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
	}
	function vn_to_en ($str){
 
		$unicode = array(
		 
		'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
		 
		'd'=>'đ',
		 
		'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
		 
		'i'=>'í|ì|ỉ|ĩ|ị',
		 
		'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
		 
		'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
		 
		'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
		 
		'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
		 
		'D'=>'Đ',
		 
		'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
		 
		'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
		 
		'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
		 
		'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
		 
		'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
		 
		);
	 
		foreach($unicode as $nonUnicode=>$uni){
		 
		$str = preg_replace("/($uni)/i", $nonUnicode, $str);
		 
		}
		//$str = str_replace(' ','_',$str);
		 
		return $str;
	 
	}

	/**
	 * Determines if user's logged assigned.
	 *
	 * @param      integer  $term_id   The term identifier
	 * @param      string   $kpi_type  The kpi type
	 *
	 * @return     boolean  True if assigned, False otherwise.
	 */
	protected function is_assigned($term_id = 0,$kpi_type = '')
	{
		// check user has manager role permission
		$class 	= $this->router->fetch_class();
		$method = $this->router->fetch_method();
		$result = $this->webbuild_m->has_permission($term_id, "{$class}.{$method}.access");
		return $result;
	}


	/**
	 * THE PAGE MAPPING CONTRACT WITH TECHNICAL STAFFS
	 */
	public function staffs()
	{
		// restrict('Webgeneral.staffs');
		$this->template->title->prepend('Bảng phân công phụ trách thực hiện dịch vụ TKW');
		$data = $this->data;

		$data['content'] =
		$this->admin_ui	
		->select('term.term_id')
		->add_column('stt', array('set_select'=> FALSE, 'title'=> 'STT', 'set_order'=> FALSE))
		->add_column_callback('stt',function($data,$row_name){
			global $stt;
			$data['stt'] = (++$stt);
			return $data;
		},FALSE)
		->add_column('term.term_name',array('title'=>'Website','set_order'=>FALSE))
		->add_column_callback('term_name',function($data,$row_name){

			$term_id = $data['term_id'];
			$domain = $data['term_name'] ?: '<code>Chưa có website</code>';
			$data['term_name'] = anchor(module_url("kpi/{$term_id}"),$domain);

			return $data;
		},FALSE)
		->add_column('start_service_time', array('set_select'=>FALSE,'title'=>'T/G kích hoạt','set_order'=>FALSE))
		->add_column_callback('start_service_time',function($data,$row_name){
			$start_service_time = get_term_meta_value($data['term_id'],'start_service_time');
			$data['start_service_time'] = my_date($start_service_time);
			return $data;
		},FALSE)
		->add_column('tech_staff', array('set_select'=>FALSE,'title'=>'Phụ trách','set_order'=>FALSE))
		->add_column_callback('tech_staff',function($data,$row_name){

			$term_id = $data['term_id'];
			$kpis = $this->webgeneral_kpi_m->get_kpis($term_id,'users','tech');
			if(empty($kpis)) return $data;

			$tech_staff = '';
			$user_ids = array();

			foreach ($kpis as $uids) 
			{
				foreach ($uids as $i => $val) 
				{
					$user_ids[$i] = $i;
				}
			}

			$data['tech_staff'] = implode(br(), array_map(function($user_id){
				$display_name = $this->admin_m->get_field_by_id($user_id,'display_name');
				return $display_name;

				$email_parts = explode('@', $email);
				return reset($email_parts);

			}, $user_ids));

			return $data;
		},FALSE)
		->add_column('sale_staff', array('set_select'=>FALSE,'title'=>'Kinh doanh','set_order'=>FALSE))
		->add_column_callback('sale_staff',function($data,$row_name){
			$staff_id = get_term_meta_value($data['term_id'],'staff_business');
            $sale_staff = $this->admin_m->get_field_by_id($staff_id,'display_name') ?: $this->admin_m->get_field_by_id($staff_id,'user_email');
            $data['sale_staff'] = $sale_staff ?: '<code>---</code>';
			return $data;
		},FALSE)
		->where('term.term_type', 'webdesign')
		->where_in('term.term_status',array('publish'))
		->from('term')
		->generate(array('per_page' => 10000,'uri_segment' => 4,'base_url'=> module_url('statistic')));

		$this->data = $data;
		parent::render($this->data, 'blank');
	}
}
/* End of file Webbuild.php */
/* Location: ./application/modules/webbuild/controllers/Webbuild.php */