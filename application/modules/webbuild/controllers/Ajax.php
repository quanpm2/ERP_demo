<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Admin_Controller {

	function __construct()
	{
		$this->autoload['libraries'][] 	= 'datatable_builder';
        $this->autoload['models'][] 	= 'webbuild/webbuild_m';
        $this->autoload['models'][] 	= 'webbuild/webbuild_task_m';
        $this->autoload['helpers'][] 	= 'date';

		parent::__construct();
		$this->term_type = $this->webbuild_m->term_type;
	}

	public function setting($type ='', $term_id =0)
	{
		$this->load->model('webgeneral_seotraffic_m');
		$this->load->add_package_path(APPPATH.'third_party/google-analytics/');
		switch ($type) {
			case 'ga-reload':
			$results = $this->webgeneral_seotraffic_m->reload_account_ga(true);
			$data = array();
			foreach($results as $id =>$result)
			{
				$data[] = array("id"=> $id, 'text'=>$result);
			}
			return $this->renderJson($data);
			break;
			
			default:
				# code...
			break;
		}
	}
	
	public function kpi($term_id = 0)
	{
		$result = array('success'=>false, 'msg'=>'Có lỗi xảy ra');
		if($type = $this->input->post('name'))
		{
			$pk = $this->input->post('pk');
			$val = $this->input->post('value');
			switch ($type) {
				case 'change-kpi':
				$type = $this->input->post('type');
				$update = array('kpi_value'=>$val);
				$where = array(
					'term_id'=>$term_id,
					'kpi_type'=>$type,
					'kpi_datetime'=>$pk
					);
				$update['kpi_datetime'] = $pk;
				$count_row = 0;
				
				if(!empty($pk))
				{
					$this->webgeneral_kpi_m->where($where)->update_by($update);
					$count_row = $this->db->affected_rows();
				}
				$result['msg'] = 'Thêm thành công';
				if($count_row == 0)
				{
					$update['term_id'] = $term_id;
					
					$update['kpi_type'] = $type;
					$this->webgeneral_kpi_m->insert($update);
					$result['msg'] = 'Cập nhật thành công';
				}
				$this->webgeneral_kpi_m->delete_cache($term_id);
				$result['success'] = true;
				
				break;
				
				default:
					# code...
				break;
			}
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	function upload_backlinks()
	{
		$result = array('msg'=>'File upload không hợp lệ !','success'=>FALSE);
		$this->output->set_content_type('application/json');

		$post = $this->input->post();
		if(empty($post)) return $result;

		$upload_config = array();
		$upload_config['upload_path'] = './files/backlinks/';
		$upload_config['max_size'] = '4096';
		$upload_config['allowed_types'] = 'xls|xlsx|txt';
		$upload_config['file_name'] = $post['term_name'].'-'.$post['term_id'].'-'. time();
		$upload_config['file_name'] = str_replace('.', '-', $upload_config['file_name']);
		$upload_config['upload_path'].= date('Y').'/'.date('m').'/';

		if(!is_dir($upload_config['upload_path'])) 
			mkdir($upload_config['upload_path'],0777,TRUE);

		$this->load->library('upload');
		$this->upload->initialize($upload_config);	
		if(!$this->upload->do_upload('fileinput'))
			return $this->output->set_output(json_encode($result));

		$data = $this->upload->data();
		$result['msg'] = 'Upload file hợp đồng thành công !';
		$result['success'] = TRUE;
		$result['file_name'] = $upload_config['upload_path'].$data['file_name'];

		return $this->output->set_output(json_encode($result));
	}

	function pingomatic($blink_id = 0)
	{
		$this->load->model('backlink/backlink_m');
		$result = $this->backlink_m->pingomatic($blink_id);
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}
	
	/**
	 * Excuse the set of stop services process
	 *
	 * 1. Update end_service_time metadata
	 * 2. Change term_status to "ending"
	 * 3. Log trace
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     JSON
	 */
	public function stop_service($term_id = 0)
	{
		$response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

		// Restrict permission of current user
		$has_permission = $this->webbuild_m->has_permission($term_id, 'Webbuild.stop_service.manage');
		if( ! $has_permission)
		{	
			$response['msg'] = 'Không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}

		$this->load->model('contract/contract_m');
		$term = $this->contract_m->select('term_id,term_name,term_status,term_type')->set_term_type()->where('term_type','webdesign')->get($term_id);
		if( ! $term)
		{
			$response['msg'] = 'Không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}

		// Check if the service is running then stop excute and return
		if( is_service_end($term))
		{
			$response['msg'] = 'Trạng thái hiện tại của dịch vụ đã là kết thúc.';
			return parent::renderJson($response);
		}

		// Excute main function
		$this->load->model('webbuild_contract_m');
		$result = $this->webbuild_contract_m->stop_service($term);
		if( ! $result)
		{
			$response['msg'] = 'Quá trình xử lý không thành công. Xin thử lại';
			return parent::renderJson($response);
		}

		$response['msg']	= 'Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}


	public function dataset()
	{
		$response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

		$permission = 'Webbuild.Index.Access';
		if( ! has_permission($permission))
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		$defaults	= ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => TRUE, 'is_ordering' => TRUE];
		$args 		= wp_parse_args( $this->input->get(NULL, TRUE), $defaults);

		$relate_users 		= $this->admin_m->get_all_by_permissions($permission);
		// LOGGED USER NOT HAS PERMISSION TO ACCESS
		if($relate_users === FALSE)
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		if(is_array($relate_users))
		{
			$this->datatable_builder->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
		}

		# LOAD ALL MESSAGE OF WEBBUILD CONTRACT
		$this->load->model('message/message_m');
		$this->load->model('webgeneral/webgeneral_kpi_m');
		$this->load->model('term_users_m');
		$this->load->model('webbuild_config_m');

		// $messages = $this->message_m
		// ->select('term.term_id')->select('COUNT(msg_id) as msg_count')
		// ->join('term','term.term_id = message.term_id')->group_by('term.term_id')
		// ->get_many_by(['term_type'=>$this->webbuild_m->term_type ]);
  //       $messages =	key_value($messages,'term_id','msg_count');
        $messages = [];

		// Authorization for user role
		if(is_array($relate_users) && count($relate_users) == 1)
		{
			$this->datatable_builder->unset_column('staff_business');
			$this->datatable_builder->unset_column('staff_advertise');
		}

		$data = $this->data;
		$this->search_filter();
		$this->load->config('contract/contract');	

		$this->datatable_builder
		->set_filter_position(FILTER_TOP_OUTTER)
		->select('term.term_id')

		->add_search('term.term_name', ['placeholder'=>'Website'])
		->add_search('start_service_time', ['class'=>'form-control input_daterange','placeholder'=>'Thời gian thực hiện'])
		->add_search('end_service_time', ['class'=>'form-control input_daterange','placeholder'=>'Thời gian kết thúc'])

		->add_search('technical_staffs', ['class'=>'form-control','placeholder'=>'Kỹ thuật thực hiện'])
		->add_search('technical_group', ['class'=>'form-control','placeholder'=>'Nhóm Kỹ thuật'])

		->add_search('service_package',array('content'=> form_dropdown(array('name'=>'where[service_package]','class'=>'form-control select2'),prepare_dropdown(key_value($this->config->item('service','packages'),'name','label'),'ALL gói thiết kế'),$this->input->get('where[service_package]'))))

		->add_column('stt', array('set_select'=> FALSE, 'title'=> 'STT','set_order'=> FALSE))
		->add_column('term.term_name','Website')
		->add_column('term.start_service_time', array('set_select'=> FALSE, 'title'=> 'T/G kích hoạt'))
		->add_column('end_service_time', array('set_select'=> FALSE, 'title'=> 'T/G kết thúc'))
		
		->add_column('technical_staffs', array('set_select'=>FALSE,'title'=>'Kỹ thuật','set_order'=>FALSE))
		->add_column('staff_business', array('set_select'=>FALSE,'title'=>'NVKD','set_order'=>FALSE))

		->add_column('service_package', array('set_select'=>FALSE,'title'=>'Gói dịch vụ','set_order'=>FALSE))

		->add_column('webbuild_task', array('set_select'=>FALSE,'title'=>'Tiến độ tác vụ','set_order'=>FALSE))
		->add_column('payment_percentage', array('set_select'=>FALSE,'title'=>'% T.Toán'))
		->add_column('action', array('set_select'=>FALSE, 'title'=>'Actions', 'set_order'=>FALSE))

		->add_column_callback('term_id',function($data,$row_name) use($messages) {

			$sale_id = get_term_meta_value($data['term_id'],'staff_business');

			# Nếu user đang đăng nhập là kinh doanh
			if(has_permission('webbuild.sale.access') && $sale_id != $this->admin_m->id) return FALSE;

			$term_id = $data['term_id'];

			global $stt;
			$data['stt'] = (++$stt);

			// Kiểm tra các ghi chú của user về hợp đồng
			if(has_permission('Note_contract.Index.access'))
			{
				$data['term_name'] = anchor(module_url("overview/{$term_id}"),$data['term_name'],'data-toggle="tooltip" title="Thống kê chi tiết '.$data['term_name'].'"');
			}

			$start_service_time = get_term_meta_value($term_id,'start_service_time');
			$end_service_time 	= get_term_meta_value($term_id,'end_service_time');

			// Kiểm tra và hiển thị ghi chú 'Chưa gửi mail kết nối nội bộ'
			if( ! empty($start_service_time))
			{
				$diff = diffInMonths(time(),$end_service_time);
				( ! $end_service_time) AND $end_service_time = 0;
				$label_color = 'label-default';
				($diff == 1 || $diff == 0 ) AND $label_color ='label-warning';
				(time() > $end_service_time) AND $label_color ='label-danger';

				$data['term_name'].= (($label_color) ? '<br/><span class="label '.$label_color.' pull-left">Kết thúc HĐ: '.date('d/m/Y',$end_service_time).'</span>' : '');
			}
			else
			{
				$data['term_name'].= '<br/><span class="label label-danger pull-left">Chưa gửi mail kết nối</span>';	
			}

			// Số lượng ghi chú cho hợp đồng
			if(has_permission('Note_contract.Index.access') || 1==1)
			{
				if(array_key_exists($term_id, $messages) || 1==1) 
				{
					$data['term_name'].= '<span class="label pull-left messages-list"><a href="#" class="btn-note count-note" data-term_id="'.$term_id.'"><small class="label bg-green">'.($messages[$term_id]??0).'</small></a></span>' ;
				}
			}

			$data['start_service_time'] = $start_service_time ? my_date($start_service_time,'d/m/Y') : '--';
			$data['end_service_time'] = $end_service_time ? my_date($end_service_time,'d/m/Y') : '--';

			# Lấy thông tin kỹ thuật
			$data['technical_staffs'] = '<code>Chưa cấu hình</code>';
			/* Lấy thông tin kỹ thuật thực hiện hợp đồng */
			$tech_kpis = $this->webgeneral_kpi_m->get_kpis($term_id, 'users', 'tech');
			if( ! empty($tech_kpis))
			{	
				$tech_ids = array();
				foreach ($tech_kpis as $item) $tech_ids = array_merge($tech_ids, array_keys($item));

				$data['technical_staffs'] = implode(', ', array_map(function($x){
					return '<p>'.$this->admin_m->get_field_by_id($x,'display_name') ?: $this->admin_m->get_field_by_id($x,'user_mail').'</p>';
				}, $tech_ids));


				# Load user's group
				if( ! empty($tech_ids))
				{
					$user_groups_f = implode(', ', array_unique(array_map(function($x){

						$ugroups = $this->term_users_m->get_user_terms($x, 'user_group');
						if(empty($ugroups)) return NULL;
						
						return implode(', ', array_map(function($x){return $x->term_name;},$ugroups));

					}, $tech_ids)));

					$data['technical_staffs'].= "<br/><small><b><u><i>{$user_groups_f}</i></u></b></small>";
				}
			}

			# THÔNG TIN KINH DOANH PHỤ TRÁCH
			$data['staff_business'] = ! empty($sale_id) ? $this->admin_m->get_field_by_id($sale_id,'display_name') : '';
			
			# Load user's group
			if( ! empty($sale_id) && $user_groups = $this->term_users_m->get_user_terms($sale_id, 'user_group'))
			{
				$user_groups_f = implode(', ',array_map(function($x){return$x->term_name;},$user_groups));
				$data['staff_business'].= "<br/><small><b><u><i>{$user_groups_f}</i></u></b></small>";
			}

			# GÓI DỊCH VỤ
			$service_package 		= get_term_meta_value($term_id, 'service_package') ?: $this->config->item('default','packages');
			$service_package_icon	= $this->webbuild_config_m->get_service_value($service_package,'icon');
			//$service_package_label	= $this->webbuild_config_m->get_service_value($service_package,'label');
			
			$this->load->config('contract');
			$package_config 			= $this->config->item('package');
			$service_package_label		= unserialize(get_term_meta_value($term_id,'packages_new'));
			$service_package_label 		= $service_package_label['package'] ?? $service_package;
			
			$data['service_package']	= "<u><i class='fa {$service_package_icon}'></i> {$package_config[$service_package_label]}</u>";

			// Lấy thông tin "TIẾN ĐỘ THỰC HIỆN"
			$data['webbuild_task'] = $this->admin_form->progress_bar((int) get_term_meta_value($term_id, 'numOfCompletedTasks'), (int) get_term_meta_value($term_id, 'numOfTasks'));

			$invs_amount 	= (double)get_term_meta_value($term_id,'invs_amount');
			$invs_amount_f	= currency_numberformat($invs_amount, 'đ');

			$payment_amount 	= (double)get_term_meta_value($term_id,'payment_amount');
			$payment_amount_f	= currency_numberformat($payment_amount, '');

			# calculate payment percentage
			$payment_percentage 	= 100 * (double) get_term_meta_value($term_id,'payment_percentage');
			$payment_percentage_f	= currency_numberformat($payment_percentage,' %');

			if($payment_percentage >= 50 && $payment_percentage < 80) $text_color = 'text-yellow';
			else if($payment_percentage >= 80 && $payment_percentage < 90) $text_color = 'text-light-blue';
			else if($payment_percentage >= 90) $text_color = 'text-green';
			else $text_color = 'text-red';

			$data['payment_percentage']	= "<span class='{$text_color}'><b>{$payment_percentage_f}</b></span><br/><span class='{$text_color}'><small>{$payment_amount_f} / {$invs_amount_f} </small><span>";

			return $data;
		},FALSE)
		
		->add_column_callback('action', function($data, $row_name){

			if(has_permission('webbuild.overview.access'))
			{
				$data['action'].= anchor(module_url("overview/{$data['term_id']}"),'<i class="fa fa-fw fa-line-chart"></i>', 'class="btn btn-default btn-xs" data-toggle="tooltip" title="Thống kê chi tiết"');
			}
			$data['action'].= "<a class='btn btn-default btn-xs payment-alert' data-toggle='tooltip' title='Gửi mail' term_id='{$data['term_id']}'><i class='fa fa-paper-plane'></i></a>";
			return $data;

		}, FALSE)

		->from('term')
		->where('term.term_type', $this->webbuild_m->term_type)
		->group_by('term.term_id');

		$pagination_config = array(
			'per_page' 		=> $args['per_page'],
			'cur_page' 		=> $args['cur_page'],
		);

		$data = $this->datatable_builder->generate($pagination_config);
		return parent::renderJson($data);
	}

	protected function search_filter($search_filter = array())
	{
		$args = $this->input->get();

		if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

		// start_service_time FILTERING & SORTING
		$filter_start_service_time 	= $args['where']['start_service_time'] ?? FALSE;
		$sort_start_service_time 	= $args['order_by']['start_service_time'] ?? FALSE;
		if($filter_start_service_time || $sort_start_service_time)
		{
			$alias = uniqid('start_service_time_');
			$this->datatable_builder
			->join("termmeta {$alias}", "{$alias}.term_id = term.term_id AND {$alias}.meta_key = 'start_service_time'",'LEFT OUTER');

			// FILTERING
			if($filter_start_service_time && $explode = explode(' - ', $filter_start_service_time))
			{
				$this->datatable_builder
				->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($explode)))
				->where("{$alias}.meta_value <=", $this->mdate->startOfDay(end($explode)));
			}

			// SORTING
			if($sort_start_service_time)
			{
				$this->datatable_builder->order_by("({$alias}.meta_value)*1", $sort_start_service_time);
			}
		}
		unset($args['where']['start_service_time'],$args['order_by']['start_service_time']);


		// started_service FILTERING & SORTING
		$filter_started_service 	= $args['where']['started_service'] ?? FALSE;
		$sort_started_service 	= $args['order_by']['started_service'] ?? FALSE;
		if($filter_started_service || $sort_started_service)
		{
			$alias = uniqid('started_service_');
			$this->datatable_builder
			->join("termmeta {$alias}", "{$alias}.term_id = term.term_id AND {$alias}.meta_key = 'started_service'",'LEFT OUTER');

			// FILTERING
			if($filter_started_service && $explode = explode(' - ', $filter_started_service))
			{
				$this->datatable_builder
				->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($explode)))
				->where("{$alias}.meta_value <=", $this->mdate->startOfDay(end($explode)));
			}

			// SORTING
			if($sort_started_service)
			{
				$this->datatable_builder->order_by("({$alias}.meta_value)*1", $sort_started_service);
			}
			unset($args['where']['started_service'],$args['order_by']['started_service']);
		}


		// end_service_time FILTERING & SORTING
		$filter_end_service_time 	= $args['where']['end_service_time'] ?? FALSE;
		$sort_end_service_time 	= $args['order_by']['end_service_time'] ?? FALSE;
		if($filter_end_service_time || $sort_end_service_time)
		{
			$alias = uniqid('end_service_time_');
			$this->datatable_builder
			->join("termmeta {$alias}", "{$alias}.term_id = term.term_id AND {$alias}.meta_key = 'end_service_time'",'LEFT OUTER');

			// FILTERING
			if($filter_end_service_time && $explode = explode(' - ', $filter_end_service_time))
			{
				$this->datatable_builder
				->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($explode)))
				->where("{$alias}.meta_value <=", $this->mdate->startOfDay(end($explode)));
			}

			// SORTING
			if($sort_end_service_time)
			{
				$this->datatable_builder->order_by("({$alias}.meta_value)*1", $sort_end_service_time);
			}

		}
		unset($args['where']['end_service_time'],$args['order_by']['end_service_time']);


		if(!empty($args['order_by']['payment_percentage']))
		{
			$this->datatable_builder->join('termmeta AS payment_percentage_termmeta', 'payment_percentage_termmeta.term_id = term.term_id', 'INNER');
			$args['where']['payment_percentage_termmeta.meta_key'] = 'payment_percentage';

			$args['order_by']['payment_percentage_termmeta.meta_value'] = $args['order_by']['payment_percentage'];

			unset($args['order_by']['payment_percentage']);
		}

		if(!empty($args['where']['staff_business']))
		{
			$this->datatable_builder
			->join('termmeta AS tm_sb', 'tm_sb.term_id = term.term_id', 'INNER')
			->where('tm_sb.meta_key', 'staff_business')
			->where('tm_sb.meta_value', $args['where']['staff_business']);
			unset($args['where']['staff_business']);
		}

		if(!empty($args['order_by']['staff_business']))
		{
			$this->datatable_builder->join('termmeta AS tm_sb', 'tm_sb.term_id = term.term_id', 'INNER');
			$args['order_by']['tm_sb.meta_value'] = $args['order_by']['staff_business'];
			unset($args['order_by']['staff_business']);
		};


		# FILTER BY TECHINAL USER-GROUP
		$filter_technical_group = $args['where']['technical_group'] ?? FALSE;
		$sort_technical_group 	= $args['order_by']['technical_group'] ?? FALSE;
		if($filter_technical_group || $sort_technical_group)
		{
			$alias = uniqid('technical_group_');
			$this->datatable_builder
			->join('webgeneral_kpi','term.term_id = webgeneral_kpi.term_id', 'LEFT OUTER')
			->join('term_users','term_users.user_id = webgeneral_kpi.user_id', 'LEFT OUTER')
			->join('term user_group','user_group.term_id = term_users.term_id', 'LEFT OUTER');

			if($filter_technical_group)
			{
				$this->datatable_builder->like('user_group.term_name', trim($filter_technical_group), 'both');
			}

			if($sort_technical_group)
			{
				$this->datatable_builder->order_by('user_group.term_name',$sort_technical_group);
			}

			unset($args['where']['technical_group'], $args['order_by']['technical_group']);
		}

		# FILTER BY SERVICE PACKAGE
		$filter_service_package = $args['where']['service_package'] ?? FALSE;
		$sort_service_package 	= $args['order_by']['service_package'] ?? FALSE;
		if($filter_service_package || $sort_service_package)
		{
			$alias = uniqid('service_package_');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id AND {$alias}.meta_key = 'service_package'", 'LEFT OUTER');

			if($filter_service_package)
			{
				$this->datatable_builder->where("{$alias}.meta_value",trim($filter_service_package));
			}

			if($sort_service_package)
			{
				$this->datatable_builder->order_by('{$alias}.meta_value',$sort_service_package);
			}

			unset($args['where']['service_package'], $args['order_by']['service_package']);
		}


		// term_status FILTERING & SORTING
		$filter_term_status 	= $args['where']['term_status'] ?? FALSE;
		$sort_term_status 	= $args['order_by']['term_status'] ?? FALSE;
		if($filter_term_status || $sort_term_status)
		{
			// FILTERING
			if($filter_term_status)
			{
				$this->datatable_builder->where('term.term_status', $filter_term_status);
			}

			// SORTING
			if($sort_term_status)
			{
				$this->datatable_builder->order_by('term.term_status', $sort_term_status);
			}

			unset($args['where']['term_status'],$args['order_by']['term_status']);
		}


		if(!empty($args['order_by']['payment_percentage']))
		{
			$this->datatable_builder
			->join('termmeta AS tm1', 'tm1.term_id = term.term_id', 'INNER')
			->where('tm1.meta_key', 'payment_percentage');
			$args['order_by']['tm1.meta_value'] = $args['order_by']['payment_percentage'];
			unset($args['order_by']['payment_percentage']);
		}


		if(!empty($args['where']['technical_staffs']))
		{
			$this->datatable_builder->join('webgeneral_kpi','term.term_id = webgeneral_kpi.term_id', 'LEFT OUTER');
			$this->datatable_builder->join('user','user.user_id = webgeneral_kpi.user_id', 'LEFT OUTER');
			$this->datatable_builder->like('user.user_email',strtolower($args['where']['technical_staffs']),'both');

			unset($args['where']['technical_staffs']);
		}

		if(!empty($args['order_by']['technical_staffs']))
		{
			$this->datatable_builder->join('googleads_kpi','term.term_id = googleads_kpi.term_id', 'LEFT OUTER');
			$this->datatable_builder->join('user','user.user_id = googleads_kpi.user_id', 'LEFT OUTER');
			$args['order_by']['user.user_email'] = $args['order_by']['technical_staffs'];
			unset($args['order_by']['technical_staffs']);
		}
		
		// APPLY DEFAULT FILTER BY MUTATE FIELD		
		$args = $this->datatable_builder->parse_relations_searches($args);
		if( ! empty($args['where']))
		{
			foreach ($args['where'] as $key => $value)
			{
				if(empty($value)) continue;

				if(empty($key))
				{
					$this->datatable_builder->add_filter($value, '');
					continue;
				}

				$this->datatable_builder->add_filter($key, $value);
			}
		}

		if(!empty($args['order_by'])) 
		{
			foreach ($args['order_by'] as $key => $value)
			{
				$this->datatable_builder->order_by($key, $value);
			}
		}
	}


	public function send_mail_payment_alert($term_id)
	{
		$response = array('msg' => 'Không thể thực hiện tác vụ này', 'status' => FALSE);

		$this->load->model('webbuild/webbuild_report_m');
		$result = $this->webbuild_report_m->send_mail_payment_alert($term_id);
		if( ! $result) return parent::renderJson($response);

		$response['msg'] = 'Xử lý tác vụ thành công';
		$response['status'] = TRUE;

		return parent::renderJson($response);
	}
}
/* End of file kpi.php */
/* Location: ./application/controllers/kpi.php */