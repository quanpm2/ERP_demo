<?php
class Webbuild_Package extends Package
{
	private $website_id;
	function __construct()
	{
		parent::__construct();
	}

	public function name(){
		
		return 'Webbuild';
	}

	public function init(){	

		$this->_load_menu();
		// $this->_update_permissions();
	}

	public function _load_menu()
	{
		if(!is_module_active('webbuild')) return FALSE;	

		$order 	= 1;
		$itemId = 'admin-webbuild';
		if(has_permission('webbuild.Index.access'))
		{
			$this->menu->add_item(array(
				'id' => $itemId,
				'name' => 'TK Web',
				'parent' => null,
				'slug' => admin_url('webbuild'),
				'order' => $order++,
				'icon' => 'fa fa-code',
				'is_active' => is_module('webbuild')
				),'left-service');
		}

		if(!is_module('webbuild')) return FALSE;

		$this->menu->add_item(array(
		'id' => 'webbuild-index',
		'name' => 'Dịch vụ',
		'parent' => $itemId,
		'slug' => admin_url('webbuild'),
		'order' => $order++,
		'icon' => 'fa fa-fw fa-xs fa-circle-o'
		), 'left-service');

		if(has_permission('webbuild.staffs.manage'))
		{
			$this->menu->add_item(array(
			'id' => 'webbuild-staffs',
			'name' => 'Phân công/Phụ trách',
			'parent' => $itemId,
			'slug' => admin_url('webbuild/staffs'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-group'
			), 'left-service');
		}

		$term_id = $this->uri->segment(4);

		$this->website_id = $term_id;

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		$left_navs = array(
			'overview'=> array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-xs fa-tachometer',
				),
			'task' => array(
				'name' =>  'Công việc',
				'icon' => 'fa fa-fw fa-xs fa-tasks',
				),
			'kpi' => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-xs fa-heartbeat',
				),
			'setting'  => array(
				'name' => 'Cấu hình',
				'icon' => 'fa fa-fw fa-xs fa-cogs',
				)
			);

		// Load menu left view-msg + count messages
		$this->load->model('message/message_m') ;
		$this->load->model('message/note_contract_m') ;
		$count_note_contract = count($this->note_contract_m->count_contract($term_id)) ;
		$left_navs['view_msg'] = array('name' => 'Ghi chú' . '<span class="label pull-right" id="count-msg">'.(int)$count_note_contract.'</span>','icon'=>'fa fa-comments-o');

		if(!empty($count_note_contract))
		{
		}

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("Webbuild.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "Webbuild-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
				), 'left-service');
		}

		
	}

	private function _update_permissions()
	{
		$permissions = array();
		
		if(!permission_exists('Webbuild.Kpi'))
		{
			$permissions['Webbuild.Kpi'] = array(
				'description' => 'Quản lý KPI',
				'actions' => array('manage','access','add','delete','update'));
		}

		if(!permission_exists('Webbuild.staffs'))
		{
			$permissions['Webbuild.staffs'] = array(
				'description' => 'Bảng phân công',
				'actions' => array('manage','access','add','delete','update'));
		}

		if(!permission_exists('Webbuild.view_msg')) 
		{
			$permissions['Webbuild.view_msg'] = array(
				'description' => 'Ghi chú',
				'actions' 	  => array('manage', 'access', 'add','delete','update')
			);
		}

		if(!$permissions) return false;
		
		foreach($permissions as $name => $value){
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function title()
	{
		return 'Thiết kế Website';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Thiết kế Website';
	}
	private function init_permissions()
	{
		$permissions = array();

		$permissions['Webbuild.Staffs'] = array(
			'description' => 'Bảng phân công KPI',
			'actions' => array('manage','access'));

		$permissions['Webbuild.Done'] = array(
			'description' => 'Dịch vụ đã hoàn thành',
			'actions' => array('manage','access'));

		$permissions['Webbuild.Index'] = array(
			'description' => 'Trang chính',
			'actions' => array('manage','access'));

		$permissions['Webbuild.Overview'] = array(
			'description' => 'Trang tổng quan',
			'actions' => array('manage','access'));

		$permissions['Webbuild.Task'] = array(
			'description' => 'Quản lý công việc',
			'actions' => array('manage','access','add','delete','update'));

		$permissions['Webbuild.Setting'] = array(
			'description' => 'Quản lý cấu hình',
			'actions' => array('manage','access','add','delete','update'));

		$permissions['Webbuild.Start_service'] = array(
			'description' => 'Thực hiện dịch vụ',
			'actions' => array('manage'));

		$permissions['Webbuild.Stop_service'] = array(
			'description' => 'Kết thúc dịch vụ',
			'actions' 	  => array('manage'));

		$permissions['Webbuild.View_msg'] = array(
			'description' => 'Ghi chú',
			'actions' 	  => array('manage', 'access', 'add','delete','update')
		);

		return $permissions;

	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);

			$this->role_permission_m
			->insert(array(
					'role_id' => 1, //admin role
					'permission_id' => $permission_id,
					'action' => serialize($actions)
					));
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		$pers = $this->permission_m->like('name','Webbuild')->get_many_by();

		if($pers)
		foreach($pers as $per)
		{
			$this->permission_m->delete_by_name($per->name);
		}
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}