<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webbuild_callback_m extends Base_Model {

	
	function post_get_meta($post, $row_name, $arg = array())
	{
		switch ($row_name) {
			case 'post_content':
			if(valid_url($post['post_content']))
			{
				$post['post_content'] = anchor($post['post_content'], 'Link',array('title' => $post['post_content'],'target' =>'_blank'));
			}
			break;

			case 'post_author':
			$post['post_author'] = $this->admin_m->get_field_by_id($post['post_author'],'display_name');
			break;

			case 'type':
			$type = $this->postmeta_m->get_meta_value($post['post_id'],'task_type');
			if(!$type)
			{
				$type = 'post-write';
				$this->postmeta_m->update_meta($post['post_id'], 'task_type', $type);
			}
			$post['type'] =$this->config->item($type,'task_type');
			break;
			
			default:
			break;
		}
		return $post;
	}
}

/* End of file Webbuild_callback_m.php */