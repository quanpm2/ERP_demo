<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Webbuild_report_m extends Base_Model {

	function __construct()
	{
		parent::__construct();
		$models = array(
			'staffs/user_group_m',
			'term_users_m',
			'webbuild_m',
			'webgeneral/webgeneral_seotraffic_m',
			'webgeneral/webgeneral_m',
			'webbuild/webbuild_task_m'
		);
		
		$this->load->model($models);
		$this->load->config('webgeneral');
	}

	/**
	 * Send SMS
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     array    ( description_of_the_return_value )
	 */
	public function send_sms($term_id = 0)
	{
		$result = array('status' => FALSE, 'msg' => '');

		$this->load->library('sms');

		$time 			= time();
		$post_send_id 	= array();
		$phone 			= get_term_meta_value($term_id, 'phone_report');

		if( ! $phone)
		{
			$result['msg'] = 'Số điện thoại chưa được khai báo.';
			return $result;
		}

		$posts = $this->post_m->group_by('posts.post_id')->get_posts(array(
			'select' 		=>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
			'tax_query'		=> array( 'taxonomy' 	=> $this->webbuild_m->term_type, 'terms' 	=> $term_id),
			'where' 		=> array( 'end_date <=' => $time ),
			'post_status' 	=> 'complete',
			'meta_key' 		=> 'sms_status',
			'meta_value' 	=> '1',
			'post_type' 	=> $this->webbuild_task_m->post_type
		));

		if(!$posts)
		{
			$result['msg'] = 'Không có đầu việc nào được thực hiện';
			return $result;
		}

		$result['status'] = true;
		$title = array();
		foreach($posts as $post)
		{
			$meta_title = $this->postmeta_m->get_meta_value($post->post_id,'title_ascii');

			$meta_title = (!empty($meta_title) ? $meta_title : convert_utf82ascci(mb_strtolower($post->post_title)));
			if(empty($meta_title))
				continue;
			$title[] = $meta_title;

			$post_send_id[] = $post->post_id;
		}

		$tpl = 'Kinh gui quy khach! Webdoctor da hoan thanh viec ';
		$tpl.= implode(', ', $title);


		$f_posts = $this->post_m->group_by('posts.post_id')->where_in('post_status',array('process'))->get_posts(array(
			'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
			'tax_query'=> array(
				'taxonomy' => $this->webbuild_m->term_type,
				'terms' => $term_id,
				),
			'where' => array(
				'end_date <' => $time
				),
			'numberposts' => 1,
			'meta_key' => 'sms_status',
			'meta_value' => 1,
			'orderby' => 'start_date',
			'order' => 'asc',
			'post_type' => $this->webbuild_task_m->post_type
			));

		if(!$f_posts)
			$f_posts = $this->post_m->group_by('posts.post_id')->where_in('post_status',array('complete','process'))->get_posts(array(
				'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
				'tax_query'=> array(
					'taxonomy' => $this->webbuild_m->term_type,
					'terms' => $term_id,
					),
				'where' => array(
					'end_date >' => $time
					),
				'numberposts' => 1,
				'meta_key' => 'sms_status',
				'meta_value' => 1,
				'orderby' => 'start_date',
				'order' => 'asc',
				'post_type' => $this->webbuild_task_m->post_type
				));

		if($f_posts)
		{
			$tpl.= ' va dang tien hanh viec ';
			$title = array();
			foreach($f_posts as $fpost)
			{
				$meta_title = $this->postmeta_m->get_meta_value($fpost->post_id,'title_ascii');

				$title[] = (!empty($meta_title) ? $meta_title : convert_utf82ascci(mb_strtolower($fpost->post_title)));
			}
			$tpl.= implode(', ', $title);
		}
		$tpl.= '.';

		// $sms_results = $this->sms->fake_send($phone, $tpl);
		$sms_results = $this->sms->send($phone,$tpl);

		$is_passed = FALSE;

		$result['msg'] = array();
		foreach ($sms_results as $sms_result) 
		{
			$log_id = $this->log_m->insert(array(
				'log_title'=>$tpl,
				'log_status'=> $sms_result['response']->SendSMSResult,
				'term_id'=>$term_id,
				'user_id'=> 0,
				'log_type'=> 'webbuild-report-sms',
				'log_time_create'=> date('Y-m-d H:i:s'),
				));
			$result['msg'][] = array('title'=>$tpl, 'status'=>$sms_result['response']->SendSMSResult, 'recipient'=>$sms_result['recipient']);
			$sms = array();
			$sms['sms_recipient'] = $sms_result['recipient'];
			$sms['sms_service_id'] = $sms_result['config']['service_id'];
			$this->log_m->meta->update_meta($log_id, 'sms_result', json_encode($sms));

			$result_code = $sms_result['response']->SendSMSResult;
			if($result_code == 0)
			{
				$is_passed = TRUE;
			}
		}

		if($is_passed)
		{
			foreach($post_send_id as $post_id)
			{
				//set status cho post để không gửi lại
				$this->postmeta_m->update_meta($post_id, 'sms_status',2);
			}
		}
		
		return $result;
	}

	public function send_tasks_complete_mail2customer($term_id = 0)
	{
		$mails = get_term_meta_value($term_id,'mail_report');
		if(!$mails) return FALSE;

		$this->load->model('webgeneral/webgeneral_kpi_m');
		$this->load->model('webgeneral/webgeneral_config_m');
		$this->load->model('webgeneral/webgeneral_m');

		$result = ['status'=>FALSE,'msg'=>''];
		$time               = time();

		//  Soạn nội dung để gửi mail : tất cả các công việc đã xong
		$tasks = $this->post_m->group_by('posts.post_id')->where_in('post_status',array('process','complete'))->get_posts(array(
				'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent, posts.post_status',
				'tax_query'=> array(
					'taxonomy' 	 => $this->webbuild_m->term_type,
					'terms'    	 => $term_id,
					),
				'where' => array(
					'end_date <' => $time
				),
				'orderby' 		 => 'start_date',
				'order' 		 => 'asc',
				'post_type' 	 => $this->webbuild_task_m->post_type
				));

		if(empty($tasks)) return FALSE;

		$has_unsent = FALSE;
		foreach ($tasks as $task) 
		{
			if($task->post_status == 'process') continue;

			$task_email_status = get_post_meta_value($task->post_id,'email_status');
			if($task_email_status == 0)
			{
				$has_unsent = TRUE;
				break;
			}
		}

		

		if( ! $has_unsent) return FALSE;
		$data['term_id']        = $term_id ;
		$data['tasks'] = $tasks ;

		$term 					= $this->term_m->get($term_id);
		$data['term'] = $term;
		$mail_id = 'ID:'.time();
		$title 					= "[WEBDOCTOR.VN] Báo cáo công việc đã hoàn thành của website {$term->term_name} {$mail_id}";

		$data['title']          = $title;	
		$data['email_source']   = $title;

		$data['time_send'] 		= time();

		$content 				= $this->load->view('report/complete_tasklist_daily_tpl', $data, TRUE);

		$mail_cc = array('ketoan@webdoctor.vn');
		$mail_bcc = array('support@webdoctor.vn');

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');
		if($staff_mail)
			$mail_cc[] = $staff_mail;

		// Tên nhân viên phụ trách
		$kpis = $this->webgeneral_kpi_m->get_many_by(['term_id' => $term_id,'kpi_type'=> 'tech']);
		if($kpis)
		{
			foreach($kpis as $kpi)
			{
				$mail_cc[] = $this->admin_m->get_field_by_id($kpi->user_id,"user_email");
			}
		}
		$data['kpis'] = $kpis;

		$this->load->library('email');
		
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to($mails);
		$this->email->cc($mail_cc);
		$this->email->bcc($mail_bcc);

		$this->email->subject($title);
		$this->email->message($content);
		$result['msg'] = $this->email->send() ? 'Đã gửi mail thành công' : 'Gửi mail thất bại';
		
 		foreach($tasks as $task) 
 		{
 			$status_email = get_post_meta_value($task->post_id,'email_status');
 			if($status_email == 1) continue;
 			
 			update_post_meta($task->post_id, 'email_status', 1);
		}

		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Send mail complete task to customer',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> 0,
			'log_content'	=> $mails,
			'log_type'		=> 'webbuild-report-email',
			'log_time_create'=> date('Y-m-d H:i:s'),
		));
		
		return $result;
	}

	function copy($post_or_id)
	{
		if(is_object($post_or_id) || is_array($post_or_id))
			$post = $post_or_id;
		else
			$post = $this->post_m->get($post_or_id);
		if(!$post)
			return 'No post';
		$post_id = $post->post_id;
		unset($post->post_id);
		unset($post->post_status);
		unset($post->post_content);
		unset($post->post_excerpt);
		$post->created_on = time();

		$post->start_date = strtotime('+7 day',$post->start_date);
		$post->end_date = strtotime('+7 day',$post->start_date);

		$post->post_title = '[BÁO CÁO {website_name_up}] HIỆN TRẠNG DOANH NGHIỆP TỪ NGÀY {start_date} ĐẾN NGÀY {end_date}';
		$new_post_id = $this->post_m->insert($post);
		$post_terms = $this->term_posts_m->get_the_terms($post_id);
		if($post_terms){
			foreach($post_terms as $pt){
				$this->term_posts_m->insert(array('term_id' => $pt, 'post_id'=> $new_post_id));
			}
		}

		//insert post meta
		$post_metas = $this->postmeta_m->get_many_by('post_id',$post_id);
		if($post_metas)
		{
			foreach($post_metas as $meta)
			{
				$this->postmeta_m->update_meta($new_post_id, $meta->meta_key, $meta->meta_value);
			}
		}
		return $new_post_id;
	}


	public function send_mail_custom($term_id = 0,$start_date,$end_date, $isDebug = false)
	{
		$this->load->model('webgeneral/webgeneral_kpi_m');
		$this->load->model('webgeneral/webgeneral_config_m');
		$mails = $this->termmeta_m->get_meta_value($term_id, 'mail_report');
		if(!$mails)
		{
			return array('status'=>false, 'msg' =>'Chưa cấu hình email người nhận');
		}

		$term = $this->term_m->get($term_id);
		$start_date = $this->mdate->startOfDay($start_date);
		$end_date = $this->mdate->endOfDay($end_date);

		$data = array();
		$data['term'] = $term;
		$data['tasks'] = $this->webgeneral_m->get_posts_from_date($start_date, $end_date,$term_id, $this->webgeneral_m->get_post_type('task'));
		$data['contents'] = $this->webgeneral_m->get_posts_from_date($start_date, $end_date,$term_id, $this->webgeneral_m->get_post_type('content'));

		$data['month_tasks'] = $this->webgeneral_m->get_posts_from_date($this->mdate->startOfMonth($start_date), $this->mdate->endOfMonth($start_date),$term_id, $this->webgeneral_m->get_post_type('task'));

		$data['all_tasks'] = $this->webgeneral_m->get_posts_from_date(0, $this->mdate->endOfMonth($start_date),$term_id, $this->webgeneral_m->get_post_type('task'));


		// prd($term_id, $data['full_tasks']);


		if(!$data['tasks'] && !$data['contents'])
		{
			return array('status'=>false, 'msg' =>'Không có dữ liệu gửi đi', 
				'to'=>$mails);
		}

		$data['start_date'] = $start_date;
		$data['term_id'] = $term_id;
		$data['end_date'] = $end_date;
		$data['time_send'] = time();

		
		$ga_profile_id = $this->termmeta_m->get_meta_value($term_id, 'ga_profile_id');
		$data['ga'] = $this->webgeneral_seotraffic_m->get_ga(date('Y-m-d',$start_date), date('Y-m-d',$end_date),$ga_profile_id);
		// prd($data['ga']);
		// prd($kpi);
		// prd($data['contents']);

		$data['title'] = 'Webdoctor.vn báo cáo công việc website '.ucfirst($term->term_name);
		$content = $this->load->view('webgeneral/report/tpl_mail', $data,TRUE);
		if($isDebug == true)
		{
			return $content;
		}
		$post_id = $this->webgeneral_m->insert(array(
			'post_status' =>'pending',
			'post_type' =>'webgeneral-mail-report',
			'start_date' =>$start_date,
			'end_date' =>$end_date,
			'created_on' => time(),
			));

		$this->term_posts_m->set_post_terms($post_id, array($term_id), 'webgeneral');
		$data['report_link'] = $this->get_report_link($term_id, $term->term_name, $post_id);
		
 
		$layout = 'email/blue_tpl';
		$content_view = 'webgeneral/report/tpl_week_report';

		
		$data['content'] = $content;

		
		$title = $data['title'].' từ ngày '.my_date($start_date,'d/m/Y').' đến '.my_date($end_date,'d/m/Y').'';

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');

		$this->load->library('email');
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to($mails);

		$mail_cc = array();
		$mail_cc[] = 'support@webdoctor.vn';
		if($staff_mail)
		{
			$mail_cc[] = $staff_mail;
		}
		$this->email->cc($mail_cc);
		$post_update = array(
			'post_title'=>$title,
			'post_content'=>$content
		);

		$this->email->subject($title);
		$this->email->message($content);
		// $send_status = true;
		$send_status = $this->email->send();

		if($send_status)
		{
			$post_update['post_status'] = 'publish';
		}

		$this->webgeneral_m->update($post_id, $post_update);
		$this->email->clear();

		return array('status'=> $send_status, 
			'msg' =>$post_update, 
			'to'=>$mails);
	}

	public function send_activation_mail2customer($term_id = 0)
	{
		$term = $this->term_m->get($term_id);
		if(empty($term)) return FALSE;

		$data = array('term'=>$term);

		$time_start = get_term_meta_value($term_id,'contract_begin');
		$time_end = get_term_meta_value($term_id,'contract_end');

		$start_service_time = get_term_meta_value($term_id,'start_service_time');
		$end_service_time = get_term_meta_value($term_id,'end_service_time');

		if(empty($start_service_time)){
			$start_service_time = time();
			update_term_meta($term_id, 'start_service_time', $start_service_time);
		}

		if(empty($end_service_time)){
			$time_end = ($start_service_time - $time_start) + $time_end;
			update_term_meta($term_id, 'end_service_time',$time_end);
		}

		$this->load->library('email');

		$title = 'Thông báo kích hoạt dịch vụ thiết kế Website '.ucfirst($term->term_name);
		$data['title'] = 'Thông tin kích hoạt dịch vụ';
		$data['email_source'] = $title;
		$data['term_id'] = $term_id;

		$data['tasks'] = $this->webbuild_task_m
		->select('posts.post_id, post_author, post_title, post_name, post_type, start_date, end_date')
		->join('term_posts', 'term_posts.post_id = posts.post_id')
		->where('start_date >=', $time_start)
		->where('end_date <=', strtotime('+10 month'))
		->where('term_id', $term_id)
		->order_by('posts.start_date')
		->group_by('posts.post_id')
		->get_all();

		$this->load->model('webgeneral/webgeneral_kpi_m');
		$kpis = $this->webgeneral_kpi_m->get_many_by(array(
                  'term_id'=>$term_id,
                  'kpi_type'=> 'tech',
                  ));
		$data['kpis'] = $kpis;
		$content_view = 'webbuild/admin/tpl_activation_email';
		// $data['content'] = $this->load->view($content_view,$data,TRUE);

		$content = $this->load->view($content_view,$data,TRUE);
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');

		$mail_report = get_term_meta_value($term_id,'mail_report');
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');


		$mail_cc = array();
		$mail_cc[] = 'support@webdoctor.vn';
		if($staff_mail)
			$mail_cc[] = $staff_mail;
		if($kpis)
		{
			foreach($kpis as $kpi)
			{
				$mail_cc[] = $this->admin_m->get_field_by_id($kpi->user_id,"user_email");
			}
		}

		$this->email->to($mail_report);
		// $this->email->to('hieupt@webdoctor.vn');
		$this->email->cc($mail_cc);
		$this->email->subject($title);
		$this->email->message($content);

		$send_status = $this->email->send(TRUE);
		return $send_status;
	}

	public function send_tasklist_mail2customer($term_id = 0, $add_mail_cc = false)
	{	
		$term = $this->term_m->get($term_id);
		if(empty($term)) return FALSE;

		$data = array('term'=>$term);
		$this->load->model('webgeneral/webgeneral_m');

		$time_start = get_term_meta_value($term_id,'contract_begin');
		$start_date = $this->mdate->startOfDay($time_start);

		$time_end = get_term_meta_value($term_id,'contract_end');
		$end_date = $this->mdate->endOfDay($time_end);

		$args = array('order'=>'posts.start_date');
		$data['tasks'] = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('task'),$args);
		if(empty($data['tasks'])) return FALSE;


		$this->load->library('email');

		$title = '[WEBDOCTOR.VN] Danh sách công việc dự kiến thực hiện của website '.$term->term_name;
		$layout = 'email/blue_tpl';
		$content_view = 'webgeneral/report/tpl_tasklist';
		$data['title'] = 'Danh sách công việc dự kiến thực hiện';
		$data['email_source'] = $title;
		$data['content'] = $this->load->view($content_view,$data,TRUE);
		$content = $this->load->view($layout,$data,TRUE);
		
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');

		// uncomment real customer
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');

		$mail_report = get_term_meta_value($term_id,'mail_report');
		$this->email->to($mail_report);

		$mail_cc = array('support@webdoctor.vn','quanly@webdoctor.vn');
		if($staff_mail) $mail_cc[] = $staff_mail;
		if($add_mail_cc) $mail_cc[] = $add_mail_cc;
		$this->email->cc($mail_cc);

		$this->email->subject($title);
		$this->email->message($content);

		$send_status = $this->email->send();

		return $send_status;
	}

	public function send_finish_mail2admin($term_id = 0)
    {
        $term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

        $contract_code = get_term_meta_value($term_id, 'contract_code');

        $title = '[WEBDOCTOR.VN] Hợp đồng '.$contract_code.' đã được kết thúc.';
        $content = 'Dear nhóm Webdoctor <br>';

        $end_contract_time = get_term_meta_value($term_id,'end_contract_time');
        $content.= 'Hợp đồng '.$contract_code.' vừa được kết thúc vào lúc <b>'.my_date($end_contract_time,'d-m-Y H:i:s').'</b><br>';

        $this->config->load('table_mail');
        $this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Thông tin hợp đồng');
        $this->table
        ->add_row('Mã hợp đồng:', $contract_code)
        ->add_row('Tên dịch vụ:', 'Dịch vụ Webdoctor');

        $vat = get_term_meta_value($term->term_id,'vat');
        $has_vat = ($vat > 0);
        $this->table->add_row('Loại hình:', ($has_vat ? 'V' : 'NV'));

        $staff_id = get_term_meta_value($term->term_id,'staff_business');
        if(!empty($staff_id)){
            $display_name = $this->admin_m->get_field_by_id($staff_id,'display_name');
            $display_name = empty($display_name) ? $this->admin_m->get_field_by_id($staff_id,'display_name') : $display_name;
            $this->table->add_row('Kinh doanh phụ trách:', "#{$staff_id} - {$display_name}");
        }
        $content.= $this->table->generate();

        $this->table->set_caption('Thông tin khách hàng');
        $content.=
        $this->table
        ->add_row('Người đại diện', get_term_meta_value($term->term_id,'representative_name'))
        ->add_row('Địa chỉ', get_term_meta_value($term->term_id,'representative_address'))
        ->add_row('Website', $term->term_name)
        ->add_row('Mail liên hệ', mailto(get_term_meta_value($term->term_id,'representative_email')))
        ->generate();

        $this->load->library('email');
        $this->email->from('support@webdoctor.vn', 'Webdoctor.vn');

		$mail_report = get_term_meta_value($term_id,'mail_report');
		$this->email->to($mail_report);
		
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');

		$mail_cc = array();
		$mail_cc[] = 'support@webdoctor.vn';
		$mail_cc[] = 'quanly@webdoctor.vn';
		if($staff_mail)
			$mail_cc[] = $staff_mail;
		$this->email->cc($mail_cc);
		$this->email->subject($title);
		$this->email->message($content);

		return $this->email->send(TRUE);
    }

    public function send_tech_kpi_report()
    {
    	$start_time = $this->mdate->startOfDay(strtotime('last monday'));
    	$end_time = $this->mdate->endOfDay(time());

    	$this->load->model('webgeneral/webgeneral_kpi_m');
    	$kpis = $this->webgeneral_kpi_m
    	->select('user_id,term.term_name,term.term_id,term.term_status	')
    	->join('term','term.term_id = webgeneral_kpi.term_id')
    	->where('webgeneral_kpi.kpi_type','tech')
    	->where_in('term.term_status',array('publish','ending','liquidation'))
    	->group_by('user_id,term_name')
    	->get_many_by();

    	if(empty($kpis)) return FALSE;

		$data['total'] = array('terms'=>array());
		foreach ($kpis as $key => $value)
		{
			if(!isset($data[$value->user_id])) 
				$data[$value->user_id] = array('terms'=>array());

			$contract_value = get_term_meta_value($value->term_id,'contract_value');
			$end_service_time = get_term_meta_value($value->term_id,'end_service_time');
			$term = array(
				'term_id' => $value->term_id,
				'term_name' => $value->term_name,
				'contract_value' => $contract_value,
				'package_name' => $this->webgeneral_config_m->get_package_name($value->term_id),	
				'package_label' => $this->webgeneral_config_m->get_package_label($value->term_id)
				);

			$is_term_valid = ($value->term_status == 'publish' || 
				(!empty($end_service_time) && $end_service_time <= $end_time && $end_service_time >= $start_time));

			if($is_term_valid)
				$data[$value->user_id]['terms'][$value->term_id] = $term;

			$data['total']['terms'][$value->term_id] = $term;
    	}

		$this->table->set_heading('STT','Thành viên','Gói','Dự án','Tổng','Tổng giá trị hợp đồng');
		$package_total = array();
		$rows = array();
		$i = 1;	
		foreach ($data as $user_id => $row)
		{
			$user_name = is_numeric($user_id) ? $this->admin_m->get_field_by_id($user_id, 'display_name') : 'Tổng kết';

			$packages = array();
			$count = 0;
			$total = 0;

			foreach ($row['terms'] as $term) 
			{
				$count++;
				$total+= $term['contract_value'];
				$package_name = $this->webgeneral_config_m->get_package_name($term['term_id']);

				if(!isset($packages[$term['package_name']]))
				{
					$packages[$term['package_name']] = 1;
					continue;
				}

				$packages[$term['package_name']]++;
			}

			$row_span = count($packages);
			$split_rowspan_package = array();
			$split_rowspan_number = array();

			foreach ($packages as $key => $value) 
			{
				$split_rowspan_package[] = $key;
				$split_rowspan_number[] = $value;
			}

			$rows[] = array(
					array('rowspan'=>$row_span,'data'=>$i++),
					array('rowspan'=>$row_span,'data'=>$user_name),
					$split_rowspan_package,
					$split_rowspan_number,
					array('rowspan'=>$row_span,'data'=>$count),
					array('rowspan'=>$row_span,'data'=>$total)
				);
		}

		$total_row = array_shift($rows);
		usort($rows, function ($a, $b){
		    if($a[5]['data'] == $b[5]['data']) return 0;
		    return ($a[5]['data'] < $b[5]['data']) ? 1 : -1;
		});
		$rows[] = $total_row;
		
		$no = 0;
		foreach ($rows as $key => $row) 
		{
			$split_count = count($row[2]);
			$row[5]['data'] = currency_numberformat($row[5]['data']);
			$row[0]['data'] = ++$no;
			if($split_count == 1)
			{
				$this->table->add_row($row[0],$row[1],$row[2][0],$row[3][0],$row[4],$row[5]);
				continue;
			}

			$this->table->add_row($row[0],$row[1],$row[2][0],$row[3][0],$row[4],$row[5]);
			for ($i=1; $i < $split_count; $i++)
			{ 
				$this->table->add_row($row[2][$i],$row[3][$i]);
			}
		}

		$title = '[WEBDOCTOR.VN] Thống kế bảng xếp hạng support webdoctor tuần qua từ ngày '.my_date($start_time,'d/m/Y').' đến '.my_date($end_time,'d/m/Y');

        $this->config->load('table_mail');
        $this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Thống kế bảng xếp hạng support webdoctor tuần qua');
        $content = $this->table->generate();
		$this->load->library('email');
		$this->email->from('support@webdoctor.vn','Webdoctor.vn');
		$mail_to = array('luannn@webdoctor.vn');
		$mail_cc = array('kythuat@webdoctor.vn');
		$this->email->to($mail_to);
		$this->email->cc($mail_cc);
		$this->email->bcc($mail_bcc);
		$this->email->subject($title);
		$this->email->message($content);
		$send_status = $this->email->send(TRUE);
    }

    public function send_new_themes_mail2admin($start_time  = FALSE, $end_time = FALSE)
    {
    	$this->load->helper('text');
    	$end_time   = !$end_time 	  ? $this->mdate->startOfDay() : $end_time;
    	$start_time = !$start_time ? $this->mdate->startOfDay(strtotime("-1 week",$end_time)) : $start_time;

    	$link          = 'http://themes.webdoctor.vn/api/theme_latest_as_time/' . $start_time . '/' . $end_time ;
    	$theme_latest  = Requests::get($link) ;

    	if($theme_latest->status_code != 200)  return FALSE;

    	if($theme_latest->success != 1) 	   return FALSE; 

		$theme_latest  = json_decode($theme_latest->body) ;
		$theme_latest  = $theme_latest->data ;

		if(empty($theme_latest)) return FALSE ; 

		$data['list_templates'] = $theme_latest ;
		$data['start_time'] 	= $start_time ;
		$data['end_time']   	= $end_time   ;
		$data['title']        	= '[WEBDOCTOR.VN] Giới thiệu mẫu theme mới nhất trong tuần từ ' . date('d/m/Y', $start_time) . ' đến ' . date('d/m/Y', $end_time) . ' ID:'.time();

		$content 				= $this->load->view('report/week_latest_themes_tpl', $data, TRUE);

		$this->load->library('email');
		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to('sales@adsplus.vn,kdhn@adsplus.vn');
		$this->email->cc('kythuat@webdoctor.vn');
		$this->email->subject($data['title']);
		$this->email->message($content);

		$result['msg'] = $this->email->send() ? 'Đã gửi mail thành công' : 'Gửi mail thất bại';	

		return $result;
    }

    /**
	*	Send mail remind to sale and customer when design web complete
    */
    public function send_email_remind_sale_close_project($term_id = 0, $add_mail_cc = false)
	{
		$term = $this->term_m->get($term_id);
		if(empty($term)) return FALSE;

		$this->load->model('webgeneral/webgeneral_m');

		$data = array('term'=>$term);

		$args = array('order'=>'posts.start_date');
		$data['tasks'] = $this->webgeneral_m->get_posts_from_date(1, false,$term_id, $this->webgeneral_m->get_post_type('task'),$args);
		if(empty($data['tasks'])) return FALSE;

		foreach($data['tasks'] as $task)
		{
			// check task is complete, if not complete then not process send mail
			if($task->post_status != 'complete')
			{
				return false;
			}
		}

		//when task 100% complete then send to sale
		$time_start = get_term_meta_value($term_id,'contract_begin');
		$start_date = $this->mdate->startOfDay($time_start);

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff = $this->admin_m->set_get_active()->get($staff_id);
		if(!$staff)
			$staff = $this->admin_m->get(18); //anh Hải

		$data['staff'] = $staff;

		$data['time'] = time();
		$data['start_time'] = get_term_meta_value($term->term_id,'start_service_time');

		$this->load->library('email');

		$title = '[WEBDOCTOR.VN] Thông báo hoàn thành công việc và nghiệm thu hợp đồng website '.$term->term_name;
		$layout = 'email/blue_tpl';
		$content_view = 'report/remind_sale_close_project_tpl';
		$data['title']          = $title;	
		$data['time_send'] 		= time();

		$data['content'] = $this->load->view($content_view,$data,TRUE);
		$content = $this->load->view($layout,$data,TRUE);

		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');

		// uncomment real customer
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');

		$mail_report = get_term_meta_value($term_id,'mail_report');
		$this->email->to($mail_report);
		$this->email->to('st.trunghieu@gmail.com');

		$mail_cc = array('support@webdoctor.vn','quanly@webdoctor.vn');
		if($staff_mail) $mail_cc[] = $staff_mail;
		if($add_mail_cc) $mail_cc[] = $add_mail_cc;
		$this->email->cc($mail_cc);

		$this->email->subject($title);
		$this->email->message($content);
		$send_status = $this->email->send();
		return $send_status;
	}

	
	/**
	 * Sends a mail payment alert.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  TRUE if successfully, otherwise return FALSE
	 */
	public function send_mail_payment_alert($term_id = 0)
	{
		$contract = $this->term_m->where('term_type','webdesign')->get($term_id);
		if( ! $contract) return FALSE;

		$tasks = $this->post_m
		->select('posts.post_id,post_author,post_title,post_type,post_status,start_date,end_date,post_thumb,post_slug,post_content')
		->join('term_posts','term_posts.post_id = posts.post_id')
		->where_in('post_type','webbuild-task')
		->order_by('post_status')
		->as_array()
		->get_many_by(['term_posts.term_id'=>$term_id]);

		$uncomplete_tasks = array();
		$count_tasks = count($tasks);
		$count_uncomplete_tasks = 0;
		$count_complete_tasks = 0;
		if($count_tasks > 0)
		{
			$uncomplete_tasks 		= array_filter($tasks,function($task){return ($task['post_status'] != 'complete');});
			$count_uncomplete_tasks = count($uncomplete_tasks);
			$count_complete_tasks 	= $count_tasks - $count_uncomplete_tasks;
		}

		$count_complete_tasks 	= numberformat($count_complete_tasks);
		$count_tasks 			= numberformat($count_tasks);

		$invs_amount 				= (double) get_term_meta_value($term_id,'invs_amount');
		$payment_amount 			= (double) get_term_meta_value($term_id,'payment_amount');
		$payment_percentage			= ((double) get_term_meta_value($term_id,'payment_percentage'))*100;
		$payment_amount_remaining 	= (double) get_term_meta_value($term_id,'payment_amount_remaining');

		$invs_amount_f 				= currency_numberformat($invs_amount, 'đ');
		$payment_amount_f 			= currency_numberformat($payment_amount, 'đ');
		$payment_percentage_f 		= currency_numberformat($payment_percentage, '%');
		$payment_amount_remaining_f = currency_numberformat($payment_amount_remaining, 'đ');		

		$contract_code = get_term_meta_value($term_id, 'contract_code');

		$this->load->model('webgeneral/webgeneral_kpi_m');

		$mail_to 	= $mail_cc = $mail_bcc = array();
		$title 		= "[WEBDOCTOR.VN][Thiết kế webite] HĐ {$contract_code} đã hoàn thành {$count_complete_tasks}/{$count_tasks} tác vụ và cần thanh toán {$payment_amount_remaining_f} để hoàn tất hợp đồng [ID:".time().']';

		$this->config->load('table_mail');
		$this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Tiến độ thu tiền và thực hiện tác vụ');

        $this->table
        ->add_row('Mã hợp đồng', anchor(admin_url("webbuild/task/{$term_id}"),$contract_code))
        ->add_row('Tiến độ tác vụ', "{$count_complete_tasks}/{$count_tasks} tác vụ")
        ->add_row('Tiến độ thanh toán', " {$payment_amount_f}/{$invs_amount_f} ({$payment_percentage_f})")
        ->add_row('Cần phải thu', $payment_amount_remaining_f);

        // Thông tin nhân viên kinh doanh phụ trách hợp đồng
		$staff_business_id = get_term_meta_value($term_id, 'staff_business');
		$staff_business = $this->admin_m->get_field_by_id($staff_business_id);
		
        if( ! empty($staff_business))
        {
        	$this->table->add_row('Kinh doanh phụ trách', $staff_business['display_name']);
        	$mail_to[] = $staff_business['user_email'];
        }

        $this->load->model('webgeneral/webgeneral_kpi_m');
		if($kpis = $this->webgeneral_kpi_m->get_many_by(['term_id' => $term_id,'kpi_type'=> 'tech']))
		{
			$staff_techs = array_map(function($x){ return $this->admin_m->get_field_by_id($x->user_id); }, $kpis);

        	$this->table->add_row('Kỹ thuật phụ trách', implode(', ', array_column($staff_techs, 'display_name')));
        	$mail_to = array_merge($mail_to, array_column($staff_techs, 'user_email'));
		}

		$content = $this->table->generate();

		//user_group;
		$user_group = $this->term_users_m->get_user_terms($staff_business_id,'user_group');
		$user_group_id = 0;
		$user_group_email = '';
		$department_id = 0;
		$mail_cc = array('hopdong@adsplus.vn');
		
		//user_group
        if(! empty($user_group)){
            $user_group_id = key($user_group);
            $user_group_email = get_term_meta_value($user_group_id, 'email');
            $department_id = $user_group[$user_group_id]->term_parent;
        }

        //department
        $department_email = get_term_meta_value($department_id,'department_mail');
        if(! empty($user_group_email)){
            array_push($mail_cc,$user_group_email);
        }        
        
        if(!empty($department_email)){
            array_push($mail_cc,$department_email);
        }

		$mail_bcc 	= [];

		$this->load->library('email');

		$result = $this->email->from('support@webdoctor.vn', 'Webdoctor.vn')
		->to($mail_to)->cc($mail_cc)->bcc($mail_bcc)
		->subject($title)->message($content)
		->send();
		
		if( ! $result) return FALSE;

		$log_id = $this->log_m->insert(array(
			'log_title'		=> $title,
			'log_status'	=> $result ? 1 : 0,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id ?? 0,
			'log_type'		=> 'webbuild_send_mail_payment_alert',
			'log_time_create'=> date('Y-m-d H:i:s'),
		));

		return TRUE;
	}
}
/* End of file Webgeneral_report_m.php */
/* Location: ./application/models/webgeneral_report_m.php */