<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Contract_m.php');

class Webbuild_m extends Contract_m {

	public $term_type = 'webdesign';

	function __construct() 
	{
		parent::__construct();
		$this->load->model('webbuild/webbuild_task_m');
	}

	/**
	 * Gets the working days.
	 *
	 * @return     integer  The working days.
	 */
	public function get_working_days()
	{
		if( ! $this->contract) return FALSE;

		$this->config->load('webbuild/webbuild');
		$term_id 			= $this->contract->term_id;
		$service_package 	= get_term_meta_value($term_id, 'service_package');
		$default_packages 	= $this->config->item('service', 'packages');
		$default_tasklist 	= $default_packages[$service_package]['jobs'] ?? [];

		$days = array_column($default_tasklist, 'day');

		if($advanced_functions = get_term_meta_value($term_id, 'advanced_functions'))
		{
			$advanced_functions = is_serialized($advanced_functions) ? unserialize($advanced_functions) : [];
			foreach ($advanced_functions as $func)
			{
				if(empty($func['days'])) continue;
				
				array_unshift($days, "+{$func['days']} day");
			}
		}

		$last_value = end($days);
		if(FALSE !== strpos($last_value, 'hour')) $days[count($days)-1] = str_replace('hour', 'day', $last_value);

		$days = array_filter($days, function($x){ return (FALSE !== strpos($x, 'day'));});

		$start_time = start_of_day();
		$end_time 	= strtotime(implode(' ', $days), $start_time);
		$interval 	= date_diff(date_create(my_date($start_time, 'Y-m-d H:i:s')), date_create(my_date($end_time, 'Y-m-d H:i:s')));
		return $interval->days;
	}

	/**
	 * Delete tasks by contract id and tasks ids
	 *
	 * @param      integer  $term_id  The term identifier
	 * @param      integer  $tasks    The tasks
	 */
	public function delete_task_by($tasks = 0)
	{
		if( ! $this->contract || empty($tasks)) throw new Exception('ID hợp đồng & ID tasks không hợp lệ');
		switch (TRUE)
		{
			case TRUE === $tasks: /* Delete all tasks */
				$contract_tasks = $this->term_posts_m->get_term_posts($this->contract->term_id, $this->webbuild_task_m->post_type);
				if(empty($contract_tasks)) throw new Exception('Tác vụ không tồn tại hoặc đã bị xóa');
				
				$tasks 			= $contract_tasks ? array_column($contract_tasks, 'post_id') : [];
				break;

			case is_numeric($tasks): /* Cast tasks id type to array */
				$tasks = [$tasks];
				break;
		}

		$_tasks = $this->term_posts_m->get_term_posts($this->contract->term_id, $this->webbuild_task_m->post_type, ['where_in' => $tasks]);
		if(!$_tasks) return TRUE;

		$this->term_posts_m->delete_term_posts($this->contract->term_id, $tasks);
		$this->webbuild_task_m->delete_many($tasks);
		return TRUE;
	}
}
/* End of file Webbuild_m.php */
/* Location: ./application/modules/webbuild/models/Webbuild_m.php */