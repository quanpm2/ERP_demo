<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Webdoctor_report_m.php');

class Webdesign_report_m extends Webdoctor_report_m {

    function __construct()
    {
        
        $this->recipients['cc'][]   = 'huyenptt@webdoctor.vn';

        $this->autoload['models'][] = 'webbuild/webbuild_m';
        $this->autoload['models'][] = 'webgeneral/webgeneral_kpi_m';
        $this->autoload['models'][] = 'webbuild/webbuild_task_m';
        $this->autoload['models'][] = 'term_posts_m';
        
        parent::__construct();
    }

    /**
     * Sends an activation email.
     *
     * @param      string  $type_to  The type to
     */
    public function send_activation_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;

        $time_start = get_term_meta_value($this->term_id, 'contract_begin');
        $time_end   = get_term_meta_value($this->term_id, 'contract_end');

        $start_service_time = get_term_meta_value($this->term_id, 'start_service_time');
        $end_service_time   = get_term_meta_value($this->term_id, 'end_service_time');

        if(empty($start_service_time))
        {
            $start_service_time = time();
            update_term_meta($this->term_id, 'start_service_time', $start_service_time);
        }

        if(empty($end_service_time))
        {
            $time_end = ($start_service_time - $time_start) + $time_end;
            update_term_meta($this->term_id, 'end_service_time',$time_end);
        }

        $_args = ['where'=>['start_date >=' => start_of_day($time_start)],'orderby'=>'posts.start_date'];
        $tasks = $this->term_posts_m->get_term_posts($this->term_id, $this->webbuild_task_m->post_type, $_args);

        $this->data['tasks']        = $tasks;
        $this->data['subject']      = "[WEBDOCTOR.VN] Thông báo kích hoạt dịch vụ Thiết kế Website {$this->term->term_name}";
        $this->data['content_tpl']  = 'webbuild/report/activation_email';
        
        return parent::send_activation_email($type_to);
    }

    public function init($term_id = 0)
    {
        $term = $this->contract_m->set_term_type()->where('term_type', 'webdesign')->get($term_id);
        if( ! $term) return FALSE;

        $this->term     = $term;
        $this->term_id  = $term_id;

        $this->init_people_belongs();
        return $this;
    }

    /**
     * Loads workers.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_workers()
    {
        parent::load_workers();
        $kpis = $this->webgeneral_kpi_m->select('user_id')->get_many_by(array('term_id'=>$this->term_id, 'kpi_type'=> 'tech'));

        if( ! $kpis) return $this;

        $_workers       = array_map(function($x){ return $this->admin_m->get_field_by_id($x->user_id); }, $kpis);
        $this->workers  = wp_parse_args(array_column($_workers, NULL, 'user_email'), $this->workers);

        return $this;
    }
}
/* End of file Webdesign_report_m.php */
/* Location: ./application/modules/googleads/models/Webdesign_report_m.php */