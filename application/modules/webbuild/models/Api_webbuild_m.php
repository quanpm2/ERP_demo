<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_webbuild_m extends Base_model 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('api_user_m');
    }

    function get_contract_list_by($token = '')
    {
        $data = array();
        $token = $token ?: $this->api_user_m->auser_token ?? '';

        $services = array();
        $term_ids = array();

        $user_id = $this->api_user_m->user_id ?? 0;
        if($user_id)
        {
            $this->load->model('customer/customer_m');
            $customer = $this->customer_m->set_get_customer()->get($user_id);
            
            $this->load->model('term_users_m');
            $term_ids = $this->term_users_m
            ->where_in('term_status',array('pending','publish','ending','liquidation'))
            ->get_the_terms($user_id,'webdesign');
        }

        if(empty($term_ids)) return FALSE;

        $terms = $this->term_m
        ->select('term_id,term_name,term_status')
        ->where_in('term_status',array('pending','publish','ending','liquidation'))
        ->where('term_type','webdesign')
        ->where_in('term_id',$term_ids)
        ->get_many_by();

        if(empty($terms)) return FALSE;

        foreach ($terms as $term)
        {
            $term_id = $term->term_id;
            $status = 0;
            $description = 'HĐ đã hết hạn';
            $contract_code = get_term_meta_value($term->term_id,'contract_code');

            switch ($term->term_status) 
            {
                case 'ending':
                    $status = 2;
                    $description = 'HĐ bị đã hết hạn';
                    break;

                case 'liquidation':
                    $status = 0;
                    $description = 'HĐ bị khóa';
                    break;
                
                default:
                    $status = 1;
                    $description = 'HĐ còn hạn';
                    break;
            }

            $data[$term_id] = array(
                'id' => $term_id,
                'name' => $term->term_name.'-HĐ'.$term_id,
                'status' => $status,
                'description' => $description,
                'contract_code' => $contract_code,
                );
        }

        return $data;
    }

    function get_tasks_by($term_id)
    {
    	$data = array();

    	switch ($term_id) 
    	{
    		case 1:
    			# code...
    			break;
    		
    		case 2:
				# code...
    			break;
    	}

    	return $data;
    }
    
    function get_contract_info($term_id,$token)
    {
        $data = array('sale'=>array(),'tech'=>array());

        $term = $this->term_m->get($term_id);
        if(!$term) return $data;

        $sale = array(
            'name' => 'Triệu Minh Hải',
            'phone' => '0972899723',
            'email' => 'haitm@webdoctor.vn'
        );

        if($staff_business = get_term_meta_value($term_id,'staff_business'))
        {
            if($phone = $this->usermeta_m->get_meta_value($staff_business, 'user_phone'))
            {
                $sale['phone'] = $this->usermeta_m->get_meta_value($staff_business, 'user_phone');
            }
            
            if($display_name = $this->admin_m->get_field_by_id($staff_business, 'display_name'))
            {
                $sale['name'] = $display_name ?: 'NVKD';
            }

            if($email = $this->admin_m->get_field_by_id($staff_business, 'user_email'))
            {
                $sale['email'] = $email;
            }
        }

        $data['sale'] = $sale;


        $tech = array(
            'name' => 'Triệu Minh Hải',
            'phone' => '0972899723',
            'email' => 'haitm@webdoctor.vn'
        );

        $this->load->model('webgeneral/webgeneral_kpi_m');
        if($kpi = $this->webgeneral_kpi_m->where('kpi_type','tech')->order_by('kpi_type')->where('term_id',$term_id)->get_by())
        {
            if($admin = $this->admin_m->set_get_admin()->limit(1)->get($kpi->user_id))
            {
                if($phone = $this->usermeta_m->get_meta_value($admin->user_id, 'user_phone'))
                {
                    $tech['phone'] = $this->usermeta_m->get_meta_value($admin->user_id, 'user_phone');
                }
                
                if($display_name = $this->admin_m->get_field_by_id($admin->user_id, 'display_name'))
                {
                    $tech['name'] = $display_name ?: 'Kỹ Thuật';
                }

                if($email = $this->admin_m->get_field_by_id($admin->user_id, 'user_email'))
                {
                    $tech['email'] = $email;
                }
            }
        }

        $data['tech'] = $tech;

        return $data;
    }
}