<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Base_contract_m.php');

class Webbuild_contract_m extends Base_contract_m {

	function __construct() 
	{
		parent::__construct();

		$this->load->model('webbuild/webbuild_task_m');
		$this->load->config('webbuild/webbuild');
		$this->load->helper('date');
	}

	public function start_service($term = null)
	{
		// Modules::load('webgeneral');
		if(empty($term) )
			return false;

		if(!$term || $term->term_type !='webdesign')
			return false;
		$term_id = $term->term_id;

		$this->init_setting_default($term_id);

		$this->send_mail2admin($term_id);

		update_term_meta($term_id, 'start_service_time',0);

		return true;
	}

	public function stop_contract($term)
	{
		$time = time();
		update_term_meta($term->term_id,'end_contract_time',$time);
		$this->log_m->insert(array(
			'log_type' =>'end_contract_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => my_date($time,'Y/m/d H:i:s')
			));
		$this->stop_service($term);
	}

	public function stop_service($term)
	{
		$time = time();
		$this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
		update_term_meta($term->term_id,'end_service_time',$time);
		$this->contract_m->update($term->term_id, array('term_status'=>'ending'));
		$this->log_m->insert(array(
			'log_type' =>'end_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => date('Y/m/d H:i:s',$time)
			));
		return TRUE;
	}

	/**
	 * Technical confirm excute service
	 *
	 * @param      term_m  $term   The term
	 *
	 * @return     bool  status of process
	 */
	public function proc_service($term = FALSE,$taskPackge=array())
	{
		if( ! $term || $term->term_type !='webdesign') return FALSE;
		$term_id = $term->term_id;

		if(is_service_proc($term)) return FALSE;

		// Create default tasklist for contract by package_name
		
		$this->insert_tasklist_design($term_id,0,'',$taskPackge);
		update_term_meta($term_id,'start_service_time',time());

		// Send Notification's email to users
		$this->load->model('webbuild_report_m');
		$this->webbuild_report_m->send_activation_mail2customer($term_id);

		// CREATE LOG
		$this->log_m->insert(array(
			'log_type' =>'start_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => date('Y/m/d H:i:s').' - Send task list'
		));

		return TRUE;
	}
	public function load_default_tasklist ($start_time = 0, $package_name = '')
	{
		$arr = array();
		# LOAD DEFAULT TASKLIST
		
		$this->load->model('webbuild/webbuild_config_m');
		$jobs = $this->webbuild_config_m->get_service_value($package_name, 'jobs');
		if( empty($jobs)) return FALSE;

		defined('SATURDAY')	OR define('SATURDAY', 6);
        defined('SUNDAY')  	OR define('SUNDAY', 0);

		# If start_time is not set , default is next day from now
		if( ! $start_time) $start_time = strtotime('+1 day');

		$array_insert	= array('post_type' => $this->webbuild_task_m->post_type,'post_author' => 1);
		$id = 0;
		foreach($jobs as $job)
		{
			$id++;
			$start_time = $this->mdate->startOfDay($start_time);

			// If start_time is weekends , then set to monday of next week
			if(date('w', $start_time) == SATURDAY) $start_time = strtotime('+2 days', $start_time);
			else if(date('w', $start_time) == SUNDAY) $start_time = strtotime('+1 days', $start_time);

			$end_time 	= strtotime($job['day'], $start_time);

			$weekend_days = 0;
			while((count_weekend_days($start_time, $end_time) - $weekend_days) > 0)
			{
				$left = count_weekend_days($start_time, $end_time) - $weekend_days;
				$weekend_days = count_weekend_days($start_time, $end_time);
				$end_time = strtotime("+{$left} days", $end_time);
			}

			$array_insert['post_title'] = $job['title'];
			$array_insert['start_date'] = $start_time;
			$array_insert['end_date'] 	= strtotime('-1 second', $end_time);
			$array_insert['title_ascii'] = $job['title_ascii'];
			$array_insert['id'] 	= $id;

			array_push($arr,$array_insert);

			$start_time = $end_time;
		}
		return $arr;
	}

	public function create_default_invoice($term)
	{
		$contract_value = get_term_meta_value($term->term_id,'contract_value') ?: 0;
		if(empty($contract_value)) return FALSE; 

		$number_of_payments = get_term_meta_value($term->term_id,'number_of_payments') ?: 3;
		//if($number_of_payments <> 3) return $this->messages->warn('Số lần thanh toán phải là 3 đợt') ;

		$contract_begin = get_term_meta_value($term->term_id,'contract_begin');
		$contract_end = get_term_meta_value($term->term_id,'contract_end');
		$num_dates = diffInDates($contract_begin,$contract_end);

		$num_days4inv = ceil(div($num_dates,$number_of_payments));

		$amount_per_payments = div($contract_value,$number_of_payments);
		
		$start_date = $contract_begin;
		$invoice_items = array();

		for($i = 0 ; $i < $number_of_payments; $i++)
		{	
			if($num_days4inv == 0) break;

			$end_date = $this->mdate->endOfDay(strtotime("+{$num_days4inv} day -1 second", $start_date));

			$inv_id = $this->invoice_m
			->insert(array(
				'post_title' => "Thu tiền đợt ". ($i + 1),
				'post_content' => "",
				'start_date' => $start_date,
				'end_date' => $end_date,
				'post_type' => $this->invoice_m->post_type
				));

			if(empty($inv_id)) continue;

			$this->term_posts_m->set_post_terms($inv_id, $term->term_id, $term->term_type);

			$quantity = $num_days4inv;

			// Đợt 1: 50% giá trị HĐ
			if($number_of_payments == 3) {
				if($i == 0) {
					$amount_per_payments_3_count = (50 * $contract_value)/100 ;
				}

				// Đợt 2: 30% giá trị HĐ 
				elseif($i == 1) {
					$amount_per_payments_3_count = (30 * $contract_value)/100 ;
				}

				// Đợt 3: 20% giá trị HĐ
				elseif($i == 2) {
					$amount_per_payments_3_count = (20 * $contract_value)/100 ;
				}
			}

			$this->invoice_item_m->insert(array(
					'invi_title' => 'Giá dịch vụ',
					'inv_id' => $inv_id,
					'invi_description' => '',
					'invi_status' => 'publish',
					//'price' => $amount_per_payments,
					'price' => ($number_of_payments == 3) ? $amount_per_payments_3_count : $amount_per_payments,
					'quantity' => 1,
					'invi_rate' => 100,
					'total' => $this->invoice_item_m->calc_total_price(($number_of_payments == 3) ? $amount_per_payments_3_count : $amount_per_payments, 1, 100)
				));

			$start_date = strtotime('+1 second', $end_date);

			$day_end = $num_dates - $num_days4inv;
			if($day_end < $num_days4inv)
			{
				$num_days4inv = $day_end;
			}
		}
		return TRUE;
	}

	public function init_setting_default($term_id = 0)
	{
		//phone_report
		$phone = get_term_meta_value($term_id, 'representative_phone');
		if($phone) $this->termmeta_m->update_meta($term_id, 'phone_report',$phone);

		//phone_report
		$mail = get_term_meta_value($term_id, 'representative_email');
		if($mail) $this->termmeta_m->update_meta($term_id, 'mail_report',$mail);
	}


	//Gửi mail thông báo cho Admin khi hoàn tất hợp đồng
	//Thông báo khởi tạo hợp đồng
	public function send_mail2admin($term_id = 0)
	{
		$this->load->library('email');
		$config = $this->config->item('packages');
		$term = $this->term_m->get($term_id);
		$mail = get_term_meta_value($term_id, 'representative_email');
		$mail_cc = array();

		$representative_name = get_term_meta_value($term_id, 'representative_name');
		$contract_code = get_term_meta_value($term_id, 'contract_code');
		$contract_begin = get_term_meta_value($term_id, 'contract_begin');
		$contract_end = get_term_meta_value($term_id, 'contract_end');

		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_name = $this->admin_m->get_field_by_id($staff_id, 'display_name');
		$content = 'Dear nhóm Webdoctor <br>';
		$content.= 'Thông tin hợp đồng mới vừa được khởi tạo như sau: <br>';

		$this->config->load('table_mail');
		$this->table->set_template($this->config->item('mail_template'));

		$this->table->set_caption('Thông tin hợp đồng');
		$this->table->add_row('Mã hợp đồng:', $contract_code);
		$this->table->add_row('Tên dịch vụ:', 'Web tổng hợp');
		$this->table->add_row('Thời gian đăng ký:', my_date($contract_begin,'d/m/Y'));
		$this->table->add_row('Thời gian hết hạn:', my_date($contract_end,'d/m/Y'));
		$this->table->add_row('Kinh doanh phụ trách:', '#'.$staff_id.' - '.$staff_name);
		$content.= $this->table->generate();


		$this->table->set_caption('Thông tin khách hàng');
		$this->table->add_row('Người đại diện:', $representative_name);
		$this->table->add_row('Địa chỉ:', get_term_meta_value($term_id, 'representative_address'));
		$this->table->add_row('Website:', $term->term_name);
		$this->table->add_row('Mail liên hệ:', $mail);
		
		$title = 'Hợp đồng thiết kế Web '.$contract_code.' đã được khởi tạo.';
		
		$content.= $this->table->generate();

		$content.= '';
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');


		$this->email->from('support@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to('support@webdoctor.vn');

		if($staff_mail) $mail_cc[] = $staff_mail;
		if(!empty($mail_cc)) $this->email->cc($mail_cc);

		$this->email->subject($title);
		$this->email->message($content);

		$send_status = $this->email->send();
		return $content;
	}


	/**
	 * Calculates the contract value.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  The contract value.
	 */
	public function calc_contract_value($term_id = 0)
	{
		$term = $this->term_m->where('term_type','webdesign')->get($term_id);
		if(empty($term)) return FALSE;

		/* Giá dịch vụ theo gói thiết kế website */
		$webdesign_price = (int) get_term_meta_value($term_id,'webdesign_price');

		/* Giá giảm */
		$discount_amount = (int) get_term_meta_value($term_id, 'discount_amount');

		/* Giá các chức năng nâng cao thiết kế website */
		$advanced_functions 		= unserialize(get_term_meta_value($term_id, 'advanced_functions'));
		$advanced_functions_price 	= $advanced_functions ? array_sum(array_column($advanced_functions, 'value')) : 0;
		
		$result = $webdesign_price + $advanced_functions_price - $discount_amount;

		return $result > 0 ? $result : 0;
	}


	/**
	 * Insert tasklist default of Webbuild contract
	 *
	 * @param      integer  $term_id       The term identifier
	 * @param      integer  $start_time    The start time
	 * @param      string   $package_name  The package name
	 *
	 * @return     string   job titles created
	 */
 	public function insert_tasklist_design($term_id = 0, $start_time = 0, $package_name = '',$taskPackges=array())
	{
		if(!empty($taskPackges)){
			$array_insert	= array('post_type' => $this->webbuild_task_m->post_type,'post_author' => 1);
			$endTask =0;
			foreach ($taskPackges as $taskPackge) {
				$array_insert['post_title'] = $taskPackge['post_title'];
				$array_insert['start_date'] = $taskPackge['start_date'];
				$array_insert['end_date'] 	= $taskPackge['end_date'];
				if($endTask < $taskPackge['end_date']){
					$endTask = $taskPackge['end_date'];
				}
				
				$insert_id 	= $this->post_m->insert($array_insert);

				update_post_meta($insert_id, 'task_type', 'task-design-default');
				update_post_meta($insert_id, 'title_ascii', $taskPackge['title_ascii']);
				update_post_meta($insert_id, 'sms_status', 1);
				
				//$this->term_posts_m->set_post_terms($insert_id, array($term_id), 'webdesign');
				$this->term_posts_m->set_term_posts($term_id, array($insert_id), 'webdesign');
			}
			
			update_term_meta($term_id,'end_service_time',$endTask);
			return TRUE;
		}
		# LOAD DEFAULT TASKLIST
		$package_name = $package_name ?: (get_term_meta_value($term_id,'service_package') ?: 'normal');

		$this->load->model('webbuild/webbuild_config_m');
		$jobs = $this->webbuild_config_m->get_service_value($package_name, 'jobs');
		if( empty($jobs)) return FALSE;

		defined('SATURDAY')	OR define('SATURDAY', 6);
        defined('SUNDAY')  	OR define('SUNDAY', 0);

		# If start_time is not set , default is next day from now
		if( ! $start_time) $start_time = strtotime('+1 day');

		$array_insert	= array('post_type' => $this->webbuild_task_m->post_type,'post_author' => 1);
		foreach($jobs as $job)
		{
			$start_time = $this->mdate->startOfDay($start_time);

			// If start_time is weekends , then set to monday of next week
			if(date('w', $start_time) == SATURDAY) $start_time = strtotime('+2 days', $start_time);
			else if(date('w', $start_time) == SUNDAY) $start_time = strtotime('+1 days', $start_time);

			$end_time 	= strtotime($job['day'], $start_time);

			$weekend_days = 0;
			while((count_weekend_days($start_time, $end_time) - $weekend_days) > 0)
			{
				$left = count_weekend_days($start_time, $end_time) - $weekend_days;
				$weekend_days = count_weekend_days($start_time, $end_time);
				$end_time = strtotime("+{$left} days", $end_time);
			}

			$array_insert['post_title'] = $job['title'];
			$array_insert['start_date'] = $start_time;
			$array_insert['end_date'] 	= strtotime('-1 second', $end_time);

			$insert_id 	= $this->post_m->insert($array_insert);

			update_post_meta($insert_id, 'task_type', 'task-design-default');
			update_post_meta($insert_id, 'title_ascii', $job['title_ascii']);
			update_post_meta($insert_id, 'sms_status', 1);

			$this->term_posts_m->set_post_terms($insert_id, array($term_id), 'webdesign');

			$start_time = $end_time;
		}

		return TRUE;
	}
}
/* End of file Webbuild_contract_m.php */
/* Location: ./application/modules/webbuild/models/Webbuild_contract_m.php */