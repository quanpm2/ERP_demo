<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Webbuild_config_m extends Post_m {

	function __construct() 
	{
		parent::__construct();
		$this->load->config('webbuild/webbuild');
	}


	/**
	 * Gets the service value.
	 *
	 * @param      integer  $termid_or_name  The termid or name
	 * @param      string   $key             The key
	 * @param      string   $text_key_none   The text key none
	 *
	 * @return     array  The service value.
	 * 
	 */
	public function get_service_value($termid_or_name = 0, $key = '', $text_key_none = '')
	{
		if(empty($key)) return FALSE;
		
		$term_id = (int)$termid_or_name;

		$package_name = ($term_id == 0) ? $termid_or_name : get_term_meta_value($termid_or_name, 'service_package');

		$config = $this->config->item('packages');
		return isset($config['service'][$package_name][$key]) ? $config['service'][$package_name][$key] : $text_key_none;
	}
}
/* End of file Webbuild_config_m.php */
/* Location: ./application/modules/webbuild/models/Webbuild_config_m.php */