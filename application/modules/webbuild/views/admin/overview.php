    <script type="text/javascript">
       function count_msg(term_id) {
           $.ajax({
                 url       : '<?php echo base_url() . 'message/ajax_note_contract/count' ;?>',
                 type      : 'post',            
                 dataType  : 'json' ,
                 data      : { term_id : term_id } ,          
                 success   : function(data, status) {
                     if(data.count_msg > 0) {  
                         $('#count-msg').html('<small class="label bg-green" class="badge bg-green">' + data.count_msg + '</small>') ;
                     }
                 }
           }) ;
       }  


        $(document).ready(function() {
           
                 // Load ghi chú theo hợp đồng
                 //$('.btn-note-contract').on('click', function(e) {
                 var term_id      = <?php echo $term_id ?> ;
                 count_msg(term_id) ;
        }) ;

    </script>

    <style type="text/css">
       #count-msg { 
            margin-right: 10px;
       }

       .icon-edit-update a { 
          color: #fff;
       }

       .msg-default .icon-edit-update a { 
          color: #000 !important;
       }

    </style>

    <div id="modal-note-contract" style="width: 80%; margin: 0px auto"  >
        
    </div>
