<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->template->stylesheet->add('plugins/tagsinput/tagsinput.css');

$this->template->javascript->add('plugins/tagsinput/tagsinput.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.js');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/fullcalendar/fullcalendar.min.css');
$this->template->stylesheet->add('plugins/fullcalendar/fullcalendar.print.css','media="print"');
$this->template->javascript->add('plugins/fullcalendar/fullcalendar.min.js');
$this->template->javascript->add('plugins/jQueryUI/jquery-ui.min.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');

$is_service_proc    = is_service_proc($term);
$is_service_end     = is_service_end($term);

if( ! $is_service_proc || ! $is_service_end)
{
    echo $this->admin_form->set_col(12,6)->box_open();

    if( ! $is_service_proc)  echo "<proc-service-component term_id='{$term_id}'></proc-service-component>";
    if( ! $is_service_end && $is_service_proc) echo "<stop-service-component term_id='{$term_id}'></stop-service-component>";
    echo"<alert-payment-component term_id='{$term_id}'></alert-payment-component>";
    echo '<div class="col-md-offset-2"><p class="help-block" style="margin-left: 32%;">Vui lòng cấu hình SMS, cấu hình Email và thông số để hệ thống gửi E-Mail kích hoạt và E-Mail thông báo kết nối khách hàng </p></div>' ;

    echo $this->admin_form->box_close();
}


$this->admin_form->set_col(6,6);

//sms setting
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Cấu hình SMS');

echo $this->admin_form->input('Số điện thoại','meta[phone_report]', @$meta['phone_report'], 'Tab hoặc enter hoặc space để nhập thêm', array('id'=>'phone_report','data-role'=>'tagsinput'));
echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();

//email setting
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Cấu hình Email');

echo $this->admin_form->input('Email','meta[mail_report]', @$meta['mail_report'], 'Tab hoặc enter hoặc space để nhập thêm', array('id'=>'mail_report','data-role'=>'tagsinput'));
echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();
?>

<script type="text/javascript"> var jsobjectdata = <?php echo $jsdataobject;?>; </script>

<?php
$version = 201810091332;
echo $this->template->trigger_javascript(admin_theme_url("modules/component/form.js?v={$version}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/webbuild/js/setting.js?v={$version}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/calender/js/calender.js?v={$version}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/webbuild/js/app.js?v={$version}"));