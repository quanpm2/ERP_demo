<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

echo '<webbuild-index-component></webbuild-index-component>';

echo $this->template->trigger_javascript(admin_theme_url('modules/component/ui.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/webbuild/js/note-messages.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/webbuild/js/app.js'));

/* End of file index.php */
/* Location: ./application/modules/googleads/views/admin/index.php */