<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<br/><p><u><strong>XÉT RẰNG :</strong></u></p>
<p> &#10003; Bên A là chủ sở hữu trang web <i><b><?php echo prep_url($term->term_name);?></b></i> (sau đây gọi là “website”) có chức năng giới thiệu thông tin,sản phẩm và dịch vụ của Bên A. Bên A có nhu cầu thuê dịch vụ thiết kế phần mềm ứng dụng website theo đúng mô tả đã được phác thảo.</p>
<p> &#10003; Bên B là doanh nghiệp có kinh nghiệm trong việc thực hiện phần mềm ứng dụng website và có khả năng đáp ứng đủ điều kiện nhu cầu của Bên A </p>
<p> &#10003; Hợp đồng này có hiệu lực trên tên miền <b><i><?php echo prep_url($term->term_name);?></i></b> và không có hiệu lực trên các sản phẩm khác của Bên A</p>
<br/>
<p><u><strong>ĐIỀU 1: NỘI DUNG HỢP ĐỒNG</strong></u></p>

<p>
    Bên A đồng ý thuê Bên B thực hiện cung ứng dịch vụ thiết kế phần mềm ứng dụng website theo đúng bản mô tả giao diện đã được Bên A duyệt trong vòng <?php echo $working_days; ?> ngày làm việc kể từ ngày <?php echo my_date($contract_begin,'d/m/Y'); ?> đến ngày <?php echo my_date($contract_end,'d/m/Y'); ?>
</p>

<p>- Chi tiết phần việc thực hiện trong hợp đồng này bao gồm: </p>

<?php 
/* PRINT CHI CHIẾT HỢP ĐỒNG THEO GÓI */
echo $package_partial ?? '';
?>

<!-- Phương thức thanh toán -->
<br/>
<p>Phí dịch vụ này là toàn bộ khoản thanh toán mà Bên A có nghĩa vụ thanh toán cho Bên B để thực hiện công việc như đã thỏa thuận trong hợp đồng này</p>
<br/>
<p><strong><u>2.2 Phương thức thanh toán:</u></strong></p>
<p> - Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin tài khoản do Bên B cung cấp, cụ thể:</p>

<?php if( ! empty($bank_info)) :?>
<ul style="padding-left:0;list-style:none">
<?php foreach ($bank_info as $label => $text) :?>
    <?php if (is_array($text)) : ?>
        <?php foreach ($text as $key => $value) :?>
            <li>- <?php echo $key;?>: <?php echo $value;?></li>
        <?php endforeach;?>
    <?php continue; endif;?>
    <li>- <?php echo $label;?>: <?php echo $text;?></li>
<?php endforeach;?>
</ul>
<p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
<?php endif; ?>

<?php
/* PRINT ĐỢT THANH TOÁN */
if($number_of_payments > 0 && $invoices)
{
    $stack  = array();
    $index  = 0;

    foreach ($invoices as $invoice) 
    {
        $_total     = (int) $this->invoice_item_m->get_total($invoice->post_id, 'total');
        $_percent   = div($_total, $contract_value)*100;

        if($vat > 0 && $_total > 0) $_total*=(1+div($vat, 100));

        $number_day     = diffInDates(start_of_day($invoice->start_date),start_of_day($invoice->end_date));
        $_total_f       = currency_numberformat($_total, ' đ');
        $_total_words_f = currency_number_to_words($_total, ' đồng');

        if($number_of_payments > 3)
        {
            $stack[] = '<li> Đợt '.(++$index).': thanh toán '.$_total_f.' ('.$_total_words_f.'), chậm nhất là ngày '.my_date($invoice->end_date,'d/m/Y').'.</li>';
            continue;
        }

        /* Thanh toán chính xác làm 3 đợt */

        
        if($index == 0) // thanh toán đợt 1
        { 
            $stack[] = "<li>Đợt 1: Bên A thanh toán cho Bên B <b>$_percent%</b> giá trị hợp đồng tương đương với số tiền <b>$_total_f</b> (<i>$_total_words_f</i>) sau khi ký và trước khi thực hiện hợp đồng.</li>" ;
            $index++;
            continue;
        }
        
        if($index == 1) // thanh toán đợt 2
        {
            $stack[] = "<li>Đợt 2: Bên A thanh toán cho Bên B <b>$_percent%</b> giá trị hợp đồng tương đương với số tiền <b>$_total_f</b> (<i>$_total_words_f</i>) trong vòng $number_day ngày sau khi Bên A ký duyệt demo giao diện và đặc tả chức năng website bằng bản cứng hoặc xác nhận duyệt bản mềm qua mail.</li>" ;
            $index++;
            continue;
        }

        // thanh toán đợt 3
        $stack[] = "<li>Đợt 3: Bên A thanh toán cho Bên B <b>$_percent%</b> giá trị hợp đồng còn lại tương đương với số tiền <b>$_total_f</b> (<i>$_total_words_f</i>) trong vòng $number_day ngày, sau khi Bên A đồng ý và xác nhận đồng mẫu thiết kế website mà Bên B đã hoàn thiện chuyển sang và trước khi Bên A nhận bàn giao Website từ Bên B.</li>" ;
    }

    $stack_f = implode('', $stack);
    echo "<p>Bên A thanh toán cho Bên B làm {$number_of_payments} đợt:</p><ul>{$stack_f}</ul>";
}
else echo '<p>Bên A thanh toán cho Bên B 100% giá trị vào ngày hai bên ký kết hợp đồng này.</p>';
?>

<p><u><b><strong>ĐIỀU 3: QUYỀN VÀ NGHĨA VỤ CỦA BÊN A</strong></b></u></p>
<p><strong><b><u>3.1 Trách nhiệm của Bên A:</u></b></strong></p>
<p> - Cung cấp đầy đủ, kịp thời các tài liệu, cần thiết phục vụ cho việc thiết kế website cho Bên B.</p>

<p> - Bên A cử người có thẩm quyền phối hợp với Bên B trong việc cung cấp thông tin, duyệt giao diện website, nghiệm thu website theo đúng tiến độ quy định ở điều 1. </p>

<p> - Nếu Bên A có thay đổi người thì những văn bản duyệt giao diện, ý kiến chỉnh sửa website <i><b><?php echo prep_url($term->term_name);?></b></i>. do người trước ký vẫn còn hiệu lực thực hiện. Bên A gửi email xác nhận thông tin của người liên hệ làm việc trực tiếp với Bên B (Họ tên, Email, Điện thoại)</p>

<p> - Việc thay đổi nhân sự phụ trách phối hợp thiết kế web, Bên A có trách nhiệm gửi công văn xác nhận việc thay đổi nhân sự.</p>
<p> - Bên A có trách nhiệm thanh toán đúng tiến độ hợp đồng. Nếu Bên A chậm trễ thanh toán thì Bên B có quyền tạm ngưng thiết kế website cho đến khi Bên A hoàn tất nghĩa vụ thanh toán. </p>
<p> -  Bên A chịu trách nhiệm về thông tin đã cung cấp để Bên B đưa lên website.</p>
<p> -  Bên A có trách nhiệm cử nhân viên quản trị website của Bên A sang Bên B để tham dự buổi hướng dẫn sử dụng trang quản trị (do Bên B tổ chức).</p>

<p><strong><u><b>3.2 Quyền lợi của Bên A:</b></u></strong></p>
<p> Yêu cầu Bên B thực hiện đúng tiến độ và chất lượng công việc đã nêu trên.</p>
<p>-  Khi Bên B gửi yêu cầu nghiệm thu, nếu Bên A thấy website chưa đúng theo các chi tiết đã thỏa thuận giữa hai bên thì Bên A gửi văn bản yêu cầu chỉnh sửa, Bên B sẽ thực hiện chỉnh sửa cho đúng theo các chi tiết đã thỏa thuận.  </p>
<p>-  Bên A giữ quyền sở hữu toàn bộ mã nguồn cũng như thông tin, cơ sở dữ liệu của website ngay sau khi Bên A hoàn tất nghĩa vụ thanh toán toàn bộ chi phí thiết kế website.</p>
<p>-  Bên A có quyền lợi được tham gia buổi hướng dẫn quản trị website trực tiếp do nhân viên của Bên B hướng dẫn bao gồm quản lý bài viết, thay đổi banner, thay đổi thông tin liên hệ, module hỗ trợ trực tuyến (Tối đa 2 buổi hướng dẫn tại văn phòng của Bên B).</p>
<br/>

<p><u><b><strong>ĐIỀU 4: QUYỀN VÀ NGHĨA VỤ CỦA BÊN B</strong></b></u></p>
<p><strong><u><b>4.1 Trách nhiệm của Bên B:</b></u></strong></p>
<p>-  Bên B có trách nhiệm cung cấp dịch vụ đúng theo các chi tiết nêu trong Điều 1 của hợp đồng này.</p>
<p>-  Bảo quản và bàn giao lại cho Bên A các tài liệu, dữ liệu được giao để thực hiện công việc, sau khi công việc hoàn thành.</p>
<p>-  Hoàn thành công việc đúng thời hạn hợp đồng, trừ trường hợp chậm trễ do Bên A gây ra.</p>
<p>-  Báo cáo bằng email để Bên A theo dõi tiến độ thực hiện hợp đồng.</p>
<p>-  Thực hiện sửa lại các lỗi phát sinh cho hoàn chỉnh trong quá trình Bên A tiến hành chạy thử và trong thời hạn bảo hành.</p>
<p>-  Hỗ trợ và phối hợp với Bên A trong suốt quá trình thực hiện.</p>
<p>-  Bên B có trách nhiệm sẽ chuyển giao công nghệ quản lý nội dung website lại cho Bên A khi có yêu cầu, gồm: </p>
<ul>
    <li>Hướng dẫn trực tiếp cho nhân viên quản trị website bao gồm quản lý bài viết, thay đổi banner, thay đổi thông tin liên hệ, module hỗ trợ trực tuyến (Tối đa 2 buổi hướng dẫn tại văn phòng của Bên B).</li>
    <li>Cung cấp tài liệu hướng dẫn quản trị website</li>
    <li>Đường dẫn quản trị website, user, mật khẩu sử dụng quản trị web</li>
    <li>Tệp toàn bộ mã nguồn website</li>
</ul>
<br />

<p><strong><u><b>4.2 Quyền lợi của Bên B:</b></u></strong></p>
<p>-  Yêu cầu Bên A cung cấp đầy đủ thông tin, tài liệu đúng hạn.</p>
<p>- Có quyền tạm đình chỉ hoặc đơn phương chấm dứt cung cấp dịch vụ cho Bên A nếu Bên A vi phạm trách nhiệm thanh toán được qui định tại điều 2 mà không phải hoàn trả các đợt thanh toán mà Bên A đã thực hiện cho Bên B trước đó và không chịu bất cứ trách nhiệm nào về các thiệt hại nếu có.</p>
<p>- Có quyền tạm đình chỉ hoặc đơn phương chấm dứt cung cấp dịch vụ cho Bên A nếu Bên A vi phạm, hoặc không phối hợp theo đúng quy trình thực hiện dự án theo điều 1.2 mà không phải hoàn trả các đợt thanh toán mà Bên A đã thực hiện cho Bên B trước đó và không chịu bất cứ trách nhiệm nào về các thiệt hại nếu có.</p>
<p>- Tạm đình chỉ hoặc chấm dứt cung cấp dịch vụ nếu website của Bên A có biểu hiện hoạt động vi phạm pháp luật nước CHXHCN Việt Nam mà không phải bồi thường hoặc hoàn trả lại phí dịch vụ mà Bên A đã thanh toán.</p>
<br />

<p><u><b><strong>ĐIỀU 5: BÀN GIAO</strong></b></u></p>
<p>-  Trong vòng 7 ngày kể từ khi Bên B bàn giao website hoàn chỉnh cho Bên A, Bên B phải hướng dẫn cho nhân viên của Bên A phương pháp sử dụng, khai thác và quản lý thành thạo website.</p>
<p>-  Bàn giao đầy đủ: quyền quản trị website & quyền FTP</p>
<p>- Triển khai & Bảo trì hệ thống miễn phí 02 tháng sau bàn giao</p>
<p>-  Bên B có trách nhiệm bảo hành website (sửa chữa các lỗi lập trình, kỹ thuật nếu có) </p>
<ul>
    <li>Bảo hành 02 tháng với website thực hiện lưu trữ tại đơn vị khác tính từ ngày nghiệm thu website</li>
    <li>Bảo hành trọn đời với website thực hiện lưu trữ website tại Bên B</li>
</ul>
<br />

<p><u><b><strong>ĐIỀU 6: HỦY/ THANH LÝ HỢP ĐỒNG</strong></b></u></p>
<p>-  Sau thời hạn 10 (mười) ngày kể từ ngày Bên B gửi biên bản nghiệm thu hợp đồng mà Bên A không có phản hồi thì hợp đồng mặc định được nghiệm thu, miễn trừ trách nhiệm của 2 bên trong mọi trường hợp mà không cần sự đồng ý của 2 bên bằng văn bản.</p>
<p>-  Sau thời hạn 10 (mười) ngày kể từ ngày nghiệm thu hợp đồng, nếu Bên A chưa thanh toán đợt cuối hợp đồng cho Bên B thì hợp đồng sẽ mặc định được thanh lý và Bên B sẽ không bàn giao sản phẩm cũng như không hoàn trả khoản tiền mà Bên A đã thanh toán trước đó.</p>

<br />

<p><u><b><strong>ĐIỀU 7: ĐIỀU KHOẢN CHUNG</strong></b></u></p>
<p>Hai bên cam kết thực hiện đúng và đầy đủ các điều khoản trên đây. Trong quá trình thực hiện nếu có vấn đề phát sinh, hai bên sẽ cùng nhau bàn bạc và giải quyết trên tinh thần hợp tác hữu nghị.</p>
<p>Hợp đồng này được lập thành 02 bản gốc, mỗi bên giữ một bản, có giá trị pháp lý như nhau kể từ ngày ký.</p>