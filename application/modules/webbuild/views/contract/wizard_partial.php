<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->javascript->add('plugins/wizard/jquery.bootstrap.wizard.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');

$this->template->javascript->add(admin_theme_url('modules/component/form.js'));
$this->template->javascript->add(admin_theme_url('modules/webbuild/js/webbuild-service-price-component.js'));

?>
<div class="col-md-12" id="service_tab">
	<webbuild-service-price-component :id="<?php echo $edit->term_id;?>"></webbuild-service-price-component>
	<form action="<?php echo admin_url()?>contract/create_wizard/index/<?php echo $edit->term_id?>.html" method="post" accept-charset="utf-8">
		<input type="hidden" name="edit[term_id]" value="<?php echo $edit->term_id?>">
		<div class="form-group"><div class="form-group col-md-10"><div class="input-group col-sm-12"><input type="submit" name="confirm_step_service" value="confirm_step_service" id="confirm_step_service" style="display:none;" class="form-control "></div></div>
		</div>
	</form>
</div>
<script type="text/javascript"> var app_root = new Vue({ el: '#service_tab' }); </script>