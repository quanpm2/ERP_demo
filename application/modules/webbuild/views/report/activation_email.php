<!--date-->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="15"></td>
    </tr>
    <tr>
        <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <!--Title-->
                                <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">
                                    Kích hoạt dịch vụ
                                </td>
                                <!--End title-->
                                <td class="hide" align="left" bgcolor="#f8f8f8">
                                    <img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" />
                                </td>
                            </tr>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                            <tr>
                                <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                </td>
                            </tr>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Thông báo kích hoạt dịch vụ</td>
                            </tr>
                        </table>
                        <!--end detail-->
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff">

                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td height="25"></td>
                                </tr>
                                <!--Content-->
                                <tr>
                                    <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999; line-height:28px;"><b>Kính chào Quý khách !</b> WEBDOCTOR.VN cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.

                                        <p>
                                            WEBDOCTOR.VN gửi email thông báo kích hoạt & thực hiện hợp đồng thiết kế Website cho <?php echo anchor(prep_url($term->term_name));?>
                                        </p>
                                    </td>
                                </tr>
                                <!--End Content-->
                                <tr>
                                    <td align="left" height="30"></td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="15"></td>
    </tr>
</table>

<!-- thong tin khach hang -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td height="15"></td>
        </tr>
        <tr>
            <td align="center">
                <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <!--Title-->
                                            <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin khách hàng</td>
                                            <!--End title-->
                                            <td class="hide" align="left" bgcolor="#f8f8f8">
                                                <img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--Space-->
                                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                    <tbody>
                                        <tr>
                                            <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left: 24px;">&nbsp;</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--End Space-->
                                <!--detail-->
                                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Thông tin khách hàng và hợp đồng</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--end detail-->
                            </td>
                        </tr>

                        <!--start Article-->
                        <tr>
                            <td bgcolor="#ffffff">
                                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <?php
                                        $start_time = get_term_meta_value($term->term_id,'contract_begin');
                                        $end_time = get_term_meta_value($term->term_id,'contract_end');
                                        $time_start = (isset($tasks[0]) ? $tasks[0]->start_date : time());
                                        $end_task = (!empty($tasks) ? end($tasks) : false);
                                        $time_end = ($end_task ? $end_task->end_date : strtotime('+1 month'));

                                        // $contract_date = my_date($start_time,'d/m/Y').' - '.my_date($end_time,'d/m/Y');
                                        $contract_date = my_date($time_start,'d/m/Y').' - '.my_date($time_end,'d/m/Y');
                                        ?>
                                        <?php 
                                        $rows = array();
                                        $rows[] = array('Mã hợp đồng', get_term_meta_value($term->term_id,'contract_code'));
                                        $rows[] = array('Thời gian thực hiện', $contract_date);

                                        $rows[] = array('Tên khách hàng', 'Anh/Chị '.get_term_meta_value($term->term_id,'representative_name'));
                                        $rows[] = array('Email',get_term_meta_value($term->term_id, 'representative_email'));
                                        $rows[] = array('Điện thoại di động', get_term_meta_value($term->term_id, 'representative_phone'));
                                        $rows[] = array('Địa chỉ', get_term_meta_value($term->term_id, 'representative_address'));

                                        foreach($rows as $i=>$row):
                                            ?>
                                        <tr>
                                            <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                                            <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>

                                            <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg;?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                        <td height="25"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><i> Xin Quý khách vui lòng kiểm tra lại thông tin, nếu có sai sót hãy liên hệ với nhân viên chăm sóc để thay đổi trong thời gian sớm nhất.</i></td>
                                    </tr>
                                    <tr>
                                        <td height="25"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <!--end Article-->

                </tbody>
            </table>
        </td>
    </tr>

    </tbody>
</table>
<!-- thong tin khach hang -->

<?php if(!empty($tasks)): ?>
<!--start công việc đã thực hiện task -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="15"></td>
    </tr>
    <tr>
        <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <!--Title-->
                                <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Công việc dự kiến</td>
                                <!--End title-->
                                <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                            </tr>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                            <tr>
                                <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                </td>
                            </tr>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Danh sách công việc dự kiến thực hiện thiết kế Website</td>
                            </tr>
                        </table>
                        <!--end detail-->
                    </td>
                </tr>

                <!--start Article-->
                <tr>
                    <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td height="25"></td>
                                </tr>
                                <?php foreach($tasks as $post): ?>
                                <tr>
                                    <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;"><?php echo ucfirst($post->post_title);?></td>
                                    <td align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;"><span style="color: #3498db;"><?php echo date('d/m/Y',$post->start_date);?> - <?php echo date('d/m/Y',$post->end_date);?></span>    </td>
                                </tr>
                                <?php endforeach;?>
                                <tr>
                                    <td height="25"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><i>Xin lưu ý: Thời gian trên có thể thay đổi nếu có phát sinh thêm.</i></td>
                                </tr>
                                <tr>
                                    <td height="25"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <!--end Article-->
            </table>
        </td>
    </tr>
</table>
<!--end công việc đã thực hiện task -->
<?php endif;?>