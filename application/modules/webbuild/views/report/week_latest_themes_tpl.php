<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
      <title><?php echo $title; ?></title>
      <style type="text/css">
         .ReadMsgBody { width: 100%; background-color: #ffffff; }
         .ExternalClass { width: 100%; background-color: #ffffff; }
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
         html { width: 100%; }
         body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
         table { border-spacing: 0; table-layout: fixed; margin: 0 auto; }
         table table table { table-layout: auto; }
         img { display: block !important; over-flow: hidden !important; }
         table td { border-collapse: collapse; }
         .yshortcuts a { border-bottom: none !important; }
         a { color: #3498db; text-decoration: none; }
         .footer-link a { color: #BDC3C7 !important; }
         @media only screen and (max-width: 640px) {
         body { width: auto !important; }
         table[class="table-inner"] { width: 90% !important; text-align: center !important; }
         table[class="table-full"] { width: 100% !important; text-align: center }
         table[class="table1-3"] { width: 30% !important; }
         table[class="table3-1"] { width: 64% !important; text-align: center !important; }
         *[class="hide"] { max-height: 0px !important; max-width: 0px !important; font-size: 0px !important; display: none !important; }
         /*image*/
         img[class="img1"] { width: 100% !important; height: auto !important; }
         }
         @media only screen and (max-width: 479px) {
         body { width: auto !important; }
         table[class="table-inner"] { width: 90% !important; }
         table[class="table-full"] { width: 100% !important; float: none !important; }
         table[class="table1-3"] { width: 100% !important; border: 0px !important; }
         table[class="table3-1"] { width: 100% !important; text-align: center !important; }
         *[class="hide"] { max-height: 0px !important; max-width: 0px !important; font-size: 0px !important; display: none !important; }
         /*image*/
         img[class="img1"] { width: 100% !important; height: auto !important; }
         }

         .table-inner .tbody .table-full{
            width: 49% !important;
            float:left;
         }
      </style>
   </head>
   <?php $content_style = 'font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;';?>
   <body>
      <!--header bar-->
      <table width="100%" bgcolor="#ecf0f1" align="center" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td bgcolor="#3498db" style="border-top:5px solid #43a8eb;">
               <table align="center" class="table-inner" width="600" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td align="right">
                        <!-- date -->
                        <table class="table-full" align="right" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                              <td height="35" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:12px; font-style:italic; color:#ffffff;"><?php echo date('d/m/Y', time());?></td>
                           </tr>
                        </table>
                        <!-- end date -->
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#FFFFFF">
                        <!-- Logo -->
                        <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#3498db" class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;">
                           <tr>
                              <td height="55" align="center" bgcolor="#3498db" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;padding-left: 15px;padding-right: 15px;" src="http://webdoctor.vn/images/logo.png" width="200" height="50" alt="Logo" /></td>
                              <td align="left" class="hide" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/header_shape.png" width="25" alt="img" /></td>
                           </tr>
                        </table>
                        <!-- End Logo -->
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tr>
                              <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                 <p style="padding-left: 24px;">&nbsp;</p>
                              </td>
                           </tr>
                        </table>
                        <!--End Space-->
                        <!-- slogan -->
                        <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="table-full">
                           <tr>
                              <td height="55" style="font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#7f8c8d;padding-left: 25px;padding-right: 25px;">CHĂM SÓC WEBSITE CHUYÊN NGHIỆP</td>
                           </tr>
                        </table>
                        <!-- End slogan-->
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td height="15"></td>
         </tr>
      </table>
      <!--end header bar-->
      <!--1/1 image-->
      <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td height="15"></td>
         </tr>
         <tr>
            <td>
               <table class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                     <td align="center" style="background:#AE7B1C; padding:20px;text-align:center; font-family:arial;font-weight:bold;color:#fff">
                        WEBDOCTOR.VN GIỚI THIỆU MẪU TEMPLATE WEB MỚI TRÊN KHO <A style="color:#ADF" href="//themes.webdoctor.vn">THEMES.WEBDOCTOR.VN</A><br />
						Chúc các bạn một tuần kinh doanh thuận lợi ^.^ 
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td height="15"></td>
         </tr>
      </table>
      <!--end 1/1 image-->
      <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
         <tbody>
            <tr>
               <td height="15"></td>
            </tr>
            <tr>
               <td align="center">
                  <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                     <tbody>
                        <tr>
                           <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                              <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                                 <tbody>
                                    <tr>
                                       <!--Title-->
                                       <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Webdoctor đã thực hiện</td>
                                       <!--End title-->
                                       <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                                    </tr>
                                 </tbody>
                              </table>
                              <!--Space-->
                              <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                 <tbody>
                                    <tr>
                                       <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                          <p style="padding-left: 24px;">&nbsp;</p>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <!--End Space-->
                              <!--detail-->
                              <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                                 <tbody>
                                    <tr>
                                       <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Theme trong tuần <?php echo date('d/m/Y', $start_time) . ' - ' . date('d/m/Y', $end_time); ?></td>
                                    </tr>
                                 </tbody>
                              </table>
                              <!--end detail-->
                           </td>
                        </tr>
                        <tr>
                           <td bgcolor="#ffffff">
                              <table class="table-inner" width="560" border="0" align="center" cellpadding="0" cellspacing="0">
                                 <tbody>
                                    <tr>
                                       <td height="25"></td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <?php if(!empty($list_templates)) { ?>
                                             <?php foreach($list_templates as $theme) { ?>
                                             <table align="left" cellpadding="0" cellspacing="0" border="0" width="48%">
                                             <tr>
                                             	<td style="text-align:center"><a href="<?php echo $theme->post_slug ;?>" title="<?php echo $theme->post_title ;?>"><img style="line-height:0px; font-size:0px; border:0px;display: inline !important" class="img1 img-responsive" src="<?php echo $theme->post_thumb ;?>" alt="<?php echo convert_accented_characters(url_title($theme->post_title)) ;?>" title="<?php echo $theme->post_title ;?>" width="250" height="315"></a><br/><a href="<?php echo $theme->post_slug ;?>" title="<?php echo $theme->post_title ;?>" style="padding: 10px 0;display: block;text-transform: uppercase; font-family:arial"><strong><?php echo $theme->post_title?></strong></a><br/>&nbsp;</td>
                                             </tr>
                                             </table>
                                             <?php } ?>   
                                          <?php  } ?>
                                          
                                       </td>
                                    </tr>
                                    <tr>
                                       <td height="25"></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      <!--end 3/3 panel-->
      <!--cta-->
      <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td height="15"></td>
         </tr>
         <tr>
            <td align="center" bgcolor="#ffffff" style="border-top:3px solid #3498db;border-bottom:2px solid #e0e0e0;">
               <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="table-inner">
                  <tr>
                     <td align="center">
                        <!-- title -->
                        <table class="table-full" align="center" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                              <td><img src="http://webdoctor.vn/images/quote-left.png" width="25" height="37" alt="img" /></td>
                              <td align="center" bgcolor="#3498db" style="font-family: open sans, arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 25px;padding-right: 25px;"><a style="color: #ffffff;text-decoration: none;" href="#">WEBDOCTOR.VN</a></td>
                              <td><img src="http://webdoctor.vn/images/quote-right.png" width="25" height="37" alt="img" /></td>
                           </tr>
                        </table>
                        <!-- end title -->
                     </td>
                  </tr>
                  <tr>
                     <td height="25"></td>
                  </tr>
                  <!-- headline -->
                  <tr>
                     <td align="center" valign="top" style="font-family: open sans, arial, sans-serif; font-size:30px; color:#4a4a4a; font-weight:bold;"><a style="color: #4a4a4a;text-decoration: none;" href="#">GIẢI PHÁP WEBDOCTOR.VN</a></td>
                  </tr>
                  <!-- End headline -->
                  <tr>
                     <td height="15"></td>
                  </tr>
                  <!-- Content -->
                  <tr>
                     <td align="center" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px; line-height:28px; text-align: justify;">
                        <table class="table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td align="center" style="line-height: 0px;"><a href="http://webdoctor.vn/" target="_blank"><img src="https://webdoctor.vn/images/webdoctor-logo.png"></a></td>
                              </tr>
                           </tbody>
                        </table>
                        <table class="table1-3" width="260" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td align="center" style="line-height: 0px;">
                                    <a href="http://adsplus.vn/" target="_blank"><img src="https://webdoctor.vn/images/adsplus-logo.png"></a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td align="center" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px; line-height:28px; text-align: justify;">
                        <p><b>Sứ mệnh của Webdoctor.vn</b><br>
                           Là sát cánh với Khách hàng; giúp Khách hàng có một website tốt với chi phí vận hành tiết kiệm; và qua đó giúp Khách hàng phát huy được tối đa hiệu quả kinh doanh.
                        </p>
                        <p><b>Sứ mệnh của Adsplus.vn</b> <br>
                           Google và Facebook lần lượt là hai kênh quảng cáo trực tuyến phổ biến nhất Việt Nam về tính hiệu quả và tỷ lệ quy đổi ra đơn hàng. Adsplus.vn nỗ lực giúp Khách hàng sử dụng hai kênh quảng cáo này tốt nhất; đơn giản nhất; minh bạch nhất và qua đó giúp Khách hàng nâng cao doanh số cũng như phát triển thương hiệu.
                        </p>
                     </td>
                  </tr>
                  <!-- End Content -->
                  <tr>
                     <td height="25"></td>
                  </tr>
                  <!-- Button -->
                  <tr>
                     <td align="center">
                        <table border="0" cellspacing="0" cellpadding="0" style="border:2px solid #3498db;">
                           <tr>
                              <td align="center" height="40" style="font-family: open sans, arial, sans-serif; color:#999999; font-size:14px;padding-left: 25px;padding-right: 25px;"><a href="http://webdoctor.vn/" target="_blank">Xem chi tiết</a></td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <!--End Button-->
                  <tr>
                     <td height="35"></td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td height="15"></td>
         </tr>
      </table>
      <!--end cta-->
      <!--footer-->
      <table width="100%" bgcolor="#ecf0f1" border="0" align="center" cellpadding="0" cellspacing="0">
         <tr>
            <td height="15"></td>
         </tr>
         <tr>
            <td bgcolor="#34495e" style="border-top:4px solid #3498db;">
               <table class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                     <td height="35"></td>
                  </tr>
                  <tr>
                     <td>
                        <!--note-->
                        <table class="table-full" bgcolor="#405366" width="310" border="0" align="right" cellpadding="0" cellspacing="0">
                           <tr>
                              <td align="left">
                                 <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#34495e">
                                    <tr>
                                       <!--Title-->
                                       <td align="left" bgcolor="#34495e" style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Tư vấn miễn phí</td>
                                       <!--End title-->
                                       <td width="25" align="left" bgcolor="#405366" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/note-1.png" width="23" height="30" alt="img" /></td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td height="10"></td>
                           </tr>
                           <tr>
                              <td>
                                 <table class="table-inner" align="center" width="260" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                       <td class="footer-link" align="left" bgcolor="#405366" style="font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7; line-height:28px;">
                                          <b>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</b><br>
                                          <b>Hotline:</b>(028) 7300. 4488
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td height="25" align="right" valign="bottom" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/note-2.png" width="25" height="25" alt="img" /></td>
                           </tr>
                        </table>
                        <!--note-->
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="right">
                           <tr>
                              <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                 <p style="padding-left:24px;">&nbsp;</p>
                              </td>
                           </tr>
                        </table>
                        <!--End Space-->
                        <!--Preference-->
                        <table class="table-full" align="left" width="120" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                              <td style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Thông tin</td>
                           </tr>
                           <tr>
                              <td height="10"></td>
                           </tr>
                           <tr>
                              <td class="footer-link" style="line-height:28px; font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7;">
                                 <a href="https://webdoctor.vn/about/" target="_blank">Về chúng tôi</a><br/>
                                 <a href="http://webdoctor.vn/bang-gia-dich-vu/" target="_blank">Bảng giá</a>
                                 <br/>
                                 <a href="https://webdoctor.vn/contact-us/" target="_blank">Liên hệ</a>
                              </td>
                           </tr>
                        </table>
                        <!--End Preference-->
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tr>
                              <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                 <p style="padding-left:24px;">&nbsp;</p>
                              </td>
                           </tr>
                        </table>
                        <!--End Space-->
                        <!--Social-->
                        <table class="table-full" align="left" width="120" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                              <td style="font-family:Open sans,  Arial, sans-serif; font-size:18px; color:#ffffff; font-weight:bold;">Social</td>
                           </tr>
                           <tr>
                              <td height="10"></td>
                           </tr>
                           <tr>
                              <td class="footer-link" style="line-height:28px; font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#bdc3c7;"><a href="https://www.facebook.com/webdoctor.vn" target="_blank" style="color:#bdc3c7;font-weight: normal;">Facebook</a>
                                 <br/>
                                 <a href="https://plus.google.com/+SccomVnseo/posts" target="_blank">Google Plus</a>
                                 <br/>
                                 <a href="#" target="_blank">Youtube</a>
                              </td>
                           </tr>
                        </table>
                        <!--End Social-->
                     </td>
                  </tr>
                  <tr>
                     <td height="35"></td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td bgcolor="#2c3e50">
               <table align="center" width="600" class="table-inner" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td height="20"></td>
                  </tr>
                  <tr>
                     <td class="footer-link">
                        <!--copyright-->
                        <table align="left" class="table-full" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                              <td style="font-family: 'Open sans', Arial, sans-serif; color:#bdc3c7; font-size:14px;">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không phải reply lại email này. </td>
                           </tr>
                        </table>
                        <!--end copyright-->
                     </td>
                  </tr>
                  <tr>
                     <td height="20"></td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <!--end footer-->
   </body>
</html>