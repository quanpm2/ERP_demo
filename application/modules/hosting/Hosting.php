<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hosting_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name()
	{
		return 'Hosting';
	}

	public function init()
	{
		$this->_load_menu();
		// $this->_update_permissions();
	}

	/**
	 * Init menu LEFT | NAV ITEM FOR MODULE
	 * then check permission before render UI
	 */
	private function _load_menu()
	{
		$order = 1;
 		$itemId = 'admin-hosting';
 		if(!is_module_active('hosting')) return FALSE;	
 		
		if(has_permission('Hosting.Index.access'))
		{
			$this->menu->add_item(array(
				'id'        => $itemId,
				'name' 	    => 'Hosting',
				'parent'    => null,
				'slug'      => admin_url('hosting'),
				'order'     => $order++,
				'icon' => 'fa fa-server',
				'is_active' => is_module('hosting')
				),'left-service');	
		}

		if(!is_module('hosting')) return FALSE;
		if(has_permission('Hosting.Index.access'))
		{
			$this->menu->add_item(array(
				'id' => 'hosting-service',
				'name' => 'Dịch vụ',
				'parent' => $itemId,
				'slug' => admin_url('hosting'),
				'order' => $order++,
				'icon' => 'fa fa-fw fa-xs fa-circle-o'
			), 'left-service');
		}
		
		$left_navs = array(
			'overview'=> array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-xs fa-tachometer',
				),
			'kpi' => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-xs fa-heartbeat',
				),
			'setting'  => array(
				'name' => 'Cấu hình',
				'icon' => 'fa fa-fw fa-xs fa-cogs',
				)
			) ;

		$term_id = $this->uri->segment(4);

		$this->website_id = $term_id;

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("hosting.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "hosting-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
				), 'left-service');
		}

		
	}

	private function _update_permissions()
	{
		$permissions = array();
		if(!permission_exists('Hosting.renew'))
			$permissions['Hosting.renew'] = array(
				'description' => 'Bảng phân công phụ trách thực hiện',
				'actions' => array('manage','update'));

		if(!$permissions) return TRUE;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function title()
	{
		return 'Hosting';
	}

	public function author()
	{
		return 'tambt';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Hosting';
	}
	
	/**
	 * Init default permissions for hosting module
	 *
	 * @return     array  permissions default array
	 */
	private function init_permissions()
	{
		return array(

			'Hosting.index' => array(
				'description' => 'Quản lý Hosting',
				'actions' => ['manage','access','update','add','delete']),

			'Hosting.done' => array(
				'description' => 'Dịch vụ đã xong',
				'actions' => ['manage','access','add','delete','update']),
		
			'Hosting.overview' => array(
				'description' => 'Tổng quan',
				'actions' => ['manage','access','add','delete','update']),

			'Hosting.kpi' => array(
				'description' => 'KPI',
				'actions' => ['manage','access','add','delete','update']),
			
			'Hosting.start_service' => array(
				'description' => 'Kích hoạt hosting',
				'actions' => ['manage','access','update']),
			
			'Hosting.stop_service' => array(
				'description' => 'Ngừng hosting',
				'actions' => ['manage','access','update']),
			
			'Hosting.setting' => array(
				'description' => 'Cấu hình',
				'actions' => ['manage','access','add','delete','update']),

			'Hosting.security' => array(
				'description' => 'Bảo mật thông tin Hosting',
				'actions' => ['manage','access', 'update']),

			'Hosting.renew' => array(
				'description' => 'Gia hạn hợp đồng cho thuê hosting',
				'actions' => ['manage','update'])
			);
	}

	/**
	 * Install module
	 *
	 * @return     bool  status of command
	 */
	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);
			$this->role_permission_m->insert(['role_id'=>1,'permission_id'=>$permission_id,'action'=>serialize($actions)]);
		}

		return TRUE;
	}

	/**
	 * Uninstall module command
	 *
	 * @return     bool  status of command
	 */
	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		if($pers = $this->permission_m->like('name','Hosting')->get_many_by())
		{
			foreach($pers as $per)
			{
				$this->permission_m->delete_by_name($per->name);
			}
		}

		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}

		return TRUE;
	}
}
/* End of file Hosting.php */
/* Location: ./application/modules/hosting/models/Hosting.php */