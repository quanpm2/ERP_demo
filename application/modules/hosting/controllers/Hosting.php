<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hosting extends Admin_Controller 
{
	private $term;
	private $term_type = 'hosting';
	public  $model = 'hosting_contract_m';

	public function __construct(){

		parent::__construct();
		
		$this->load->model('term_users_m');
		$this->load->model('hosting_kpi_m') ;
		$this->load->model('hosting_contract_m');
		$this->load->model('hosting_m');
		$this->load->model('hosting_report_m');
		$this->load->model('customer/customer_m');
		$this->load->model('contract/contract_m');
		$this->load->model('contract/contract_wizard_m');
		$this->load->model('contract/invoice_m');
		$this->load->model('webgeneral/webgeneral_kpi_m');
		$this->load->model('staffs/sale_m');

		$this->load->config('contract/contract');
		$this->load->config('staffs/group');
		$this->load->config('contract/invoice');

		// kiểm tra hosting có tồn tại và user có quyền truy cập ?
		$this->init_website();
	}

	private function init_website()
	{
		$term_id = $this->uri->segment(4);
		$method  = $this->uri->segment(3);
		$is_allowed_method = (!in_array($method, array('index','done')));
		if(!$is_allowed_method || empty($term_id)) return FALSE;

		$term = $this->term_m
		->where('term_status','publish')
		->where('term_type',$this->term_type)
		->where('term_id',$term_id)
		->get_by();

		if(empty($term) OR !$this->is_assigned($term_id)) 
		{
			$this->messages->error('Truy cập #'.$term_id.' không hợp lệ !!!');
			redirect(module_url(),'refresh');
		}

		$this->template->title->set(strtoupper(' '.$term->term_name));
		$this->website_id = $this->data['term_id'] = $term_id;
		$this->data['term'] = $this->term = $term;
	}

    public function index()
	{
		restrict('hosting.index.access');

        $this->template->is_box_open->set(1);
        $this->template->title->set('Tổng quan dịch vụ Hosting');
        $this->template->javascript->add(base_url("dist/vHostingIndex.js?v=3"));
        $this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
        $this->template->javascript->add('plugins/daterangepicker/moment.min.js');
        $this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
        $this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
        $this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
        $this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
        $this->template->stylesheet->add(base_url('node_modules/vue2-dropzone/dist/vue2Dropzone.min.css'));
        $this->template->publish();
	}


	public function overview($term_id = 0)
	{
		restrict('Hosting.Overview.Access');

		$term = $this->term_m->get($term_id);

		if(empty($term)){
			$this->messages->error('Dịch vụ không tồn tại');
			redirect(module_url(),'refresh');
		}

		$time = time();	
		$day = date('d',$time) - 1;

		if($day == 0){
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}

		$contract_begin        = get_term_meta_value($term_id, 'contract_begin');
    	$contract_end          = get_term_meta_value($term_id, 'contract_end');

    	$date_start  		   = date('d/m/Y', $contract_begin);
		$date_end 	 		   = date('d/m/Y', $contract_end);


		$description = 'Từ '.$date_start.' đến '.$date_end;

		$time_start  = $this->mdate->startOfDay(strtotime($date_start));
		$time_end    = $this->mdate->endOfDay(strtotime($date_end));

		$this->template->description->append($description);
		$this->template->title->prepend('Tổng quan');
		$this->template->javascript->add('plugins/chartjs/Chart.js');

		/* Load danh sách nhân viên kinh doanh */
		$staffs = $this->sale_m->select('user_id,user_email,display_name')->set_user_type()->set_role()->as_array()->get_all();
		$staffs = array_map(function($x){ $x['display_name'] = $x['display_name'] ?: $x['user_email']; return $x; }, $staffs);
		$data['staffs'] = key_value($staffs, 'user_id', 'display_name');

		$data['websites'] = array();

		if(!empty($data['cus_belongs'])){
			$_webs = $this->term_users_m
			->where(array('term_users.user_id' => reset($data['cus_belongs']),'term.term_type'=>'website'))
			->join('term','term.term_id = term_users.term_id')
			->get_many_by();
			if(!empty($_webs))
				foreach ($_webs as $w)
					$data['websites'][$w->term_id] = $w->term_name;
		}

		if(!empty($data['edit']->term_parent)){
			$bonus_website = $this->contract_m->get($data['edit']->term_parent);
			if(!empty($bonus_website))
				$data['websites'][$bonus_website->term_id] = $bonus_website->term_name;
		}

		//$data['ajax_segment'] = "contract/ajax_dipatcher/ajax_edit/{$data['edit']->term_id}";

		$data['invoice_url'] = module_url('invoices/');


		$data['term_id'] 			 = $term_id ;
		
		parent::render($data);
	}


	public function kpi($term_id = 0, $delete_id= 0)
	{
		$this->config->load('hosting/group') ;
		restrict('Hosting.Kpi.Access');
		
		$this->template->title->prepend('KPI');
		$data = $this->data;
		$this->submit_kpi($term_id,$delete_id);

		$data['kpi_type'] = 'tech';
		$data['term_id']  = $term_id;

		$data['time'] 	  = $this->mdate->startOfMonth(time());
		$time_start 	  = get_term_meta_value($term_id, 'contract_begin');
		$time_end 		  = get_term_meta_value($term_id, 'contract_end');

		//+2 month
		$time_start = strtotime('-1 month');
		$time_end = strtotime('+2 month', $time_end);

		$data['time_start'] = $time_start;
		$data['time_end'] = $time_end;
		$data['time_start'] = $this->mdate->startOfDay($data['time_start']);
		$data['time_end'] = $this->mdate->endOfDay($data['time_end']);

		$targets = $this->hosting_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(array(
			'term_id'=> $term_id
			));

		$data['targets'] = array();

		$data['targets']['tech'] = array();
		foreach($targets as $target)
		{
			$data['targets'][$target['kpi_type']][] = $target;
		}
		
		$data['date_contract'] = array();
		for($i=$data['time_start'];$i< $data['time_end']; $i++)
		{
			$data['date_contract'][$time_start]  = date('Y-m', $time_start);
			$date = $data['date_contract'][$time_start];

			$time_start = strtotime('+1 month', strtotime($date));
			if($time_start > $time_end)
				break;
		}

		$data['users'] = $this->admin_m->select('user_id,display_name')->where_in('role_id', $this->config->item('group_memberId'))->set_get_active()->order_by('display_name')->get_many_by();

		$data['users'] = key_value($data['users'],'user_id','display_name');

		$this->data = $data;
		parent::render($this->data);
	}
	public function submit_kpi($term_id = 0,$delete_id=0)
	{
		if($delete_id > 0)
		{
			$this->_delete_kpi($term_id,$delete_id);
			redirect(module_url('kpi/'.$term_id.'/'),'refresh');
		}

		restrict('hosting.kpi.add');

		$post = $this->input->post();
		if(empty($post)) return FALSE;

		$insert = array();
		$insert['kpi_datetime'] = $this->input->post('target_date');
		$insert['user_id'] 		= $this->input->post('user_id');
		$insert['kpi_value'] 	= (int)$this->input->post('target_post');

		$kpi_type 				= $this->input->post('target_type');
		if($insert['kpi_datetime'] >= $this->mdate->startOfMonth())
		{
			$this->hosting_kpi_m->update_kpi_value($term_id, $kpi_type,$insert['kpi_value'], $insert['kpi_datetime'], $insert['user_id']);
			$this->hosting_kpi_m->delete_cache($delete_id);
			$this->messages->success('Cập nhật thành công');
		}
		else
		{
			$this->messages->error('Cập nhật không thành công do tháng cập nhật nhỏ hơn tháng hiện tại');
		}
		redirect(current_url(),'refresh');
	}

	private function _delete_kpi($term_id = 0,$delete_id=0)
	{
		restrict('Hosting.Kpi.Delete');
		$check = $this->hosting_kpi_m->get($delete_id);
		$is_deleted = FALSE;
		if($check)
		{
			if(strtotime($check->kpi_datetime) >= $this->mdate->startOfMonth())
			{
				$this->hosting_kpi_m->delete_cache($delete_id);
				$this->hosting_kpi_m->delete($delete_id);
				$is_deleted = TRUE;
				$this->messages->success('Xóa thành công');
			}
		}
		($is_deleted) OR $this->messages->error('Xóa không thành công');
	}

	public function setting($term_id = 0)
	{
		restrict('Hosting.setting');
		$this->template->title->prepend('Cấu hình');
		$this->setting_submit($term_id);
		
		$data = $this->data;
		$data['term'] = $this->term;
		$data['term_id'] = $term_id;
		$data['meta']['phone_report'] = get_term_meta_value($term_id, 'phone_report');
		$data['meta']['mail_report']  = get_term_meta_value($term_id, 'mail_report');
		$data['meta']['domains_report']  = get_term_meta_value($term_id, 'domains_report');

		$this->data = $data;
		parent::render($this->data);
	}

	protected function setting_submit($term_id = 0)
	{
		$this->load->model('hosting_report_m') ;
		$post = $this->input->post();
		if(empty($post)) return FALSE;

		if( ! empty($post['start_process']) )
		{
			if( ! $this->hosting_m->has_permission($term_id, 'Hosting.start_service') )
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}
		
			$term 				  = $this->term_m->get($term_id);
			$status_proc_service  = $this->hosting_contract_m->proc_service($term);

			if(FALSE === $status_proc_service) $this->messages->error('Có lỗi xảy ra') ;
			if(TRUE === $status_proc_service) $this->messages->success('Cập nhật thành công');


			/* Phân tích hợp đồng ký mới | tái ký */
			$this->load->model('contract/base_contract_m');
			$this->base_contract_m->detect_first_contract($term_id);


			/* Gửi SMS thông báo kích hoạt hợp đồng đến khách hàng */
			$this->load->model('contract/contract_report_m');
			$this->contract_report_m->send_sms_activation_2customer($term_id);
			
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
		else if( ! empty($post['end_process']))
		{
			if( ! $this->hosting_m->has_permission($term_id, 'Hosting.stop_service') )
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$term = $this->term_m->get($term_id);

			$stat = $this->hosting_contract_m->stop_service($term);
			if(TRUE === $stat) $this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
		else if(!empty($post['submit']))
		{
			$metadatas = $post['meta'];

			if( ! $this->hosting_m->has_permission($term_id, 'Hosting.setting','update'))
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}
			$result =array('success'=>true,'msg'=>'Cập nhật thành công');
			$metadatas = $post['meta'];
			if($metadatas)
			{
				foreach($metadatas as $key=>$value)
				{	
					if($key == 'report_key' && $value == '')
					{
						$value = $this->webgeneral_seotraffic_m->encrypt('',$term_id);
						
					}
					if($key == 'phone_report'){
						$phones = explode(",",$value);
						
						foreach ($phones as &$phone) {
							$phone = $this->vn_to_en($phone);
							$phone = preg_replace('([a-zA-Z]+)', '', $phone);
						}
						$value = implode(',',$phones);
						if(empty($value)){
							$result  = array('success'=>false,'msg'=>'Số điện thoại không hợp lệ !!');
						}
					}
					if($key=='domains_report'){
						$domains = explode(",",$value);
						
						foreach ($domains as &$domain) {
							$this->load->helper('util');
							if(!checkdomain(strtolower($domain))){
								$result  = array('success'=>false,'msg'=>'Tên miền không hợp lệ !!');
								break;								
							}	
						}
					}
					if($key =='mail_report' && $value !=''){
						$emails = explode(",",$value);
						
						foreach ($emails as &$email) {
							if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
								$result  = array('success'=>false,'msg'=>'Email không hợp lệ !!'); ;
								break;
							}
						}
						
					}
					if($result['success']){
						$value = str_replace(' ','',$value);
						update_term_meta($term_id,$key,$value);
					}
				}
			}
			if($result['success']){
				$this->messages->success($result['msg']);
				redirect(module_url("setting/{$term_id}"),'refresh');
			}
			$this->messages->error($result['msg']);
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
	}
	function vn_to_en ($str){
 
		$unicode = array(
		 
		'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
		 
		'd'=>'đ',
		 
		'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
		 
		'i'=>'í|ì|ỉ|ĩ|ị',
		 
		'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
		 
		'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
		 
		'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
		 
		'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
		 
		'D'=>'Đ',
		 
		'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
		 
		'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
		 
		'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
		 
		'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
		 
		'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
		 
		);
	 
		foreach($unicode as $nonUnicode=>$uni){
		 
		$str = preg_replace("/($uni)/i", $nonUnicode, $str);
		 
		}
		//$str = str_replace(' ','_',$str);
		 
		return $str;
	 
	}
	protected function is_assigned($term_id = 0,$kpi_type = '')
	{
		// check user has manager role permission
		$class = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		$result = $this->hosting_m->has_permission($term_id,"{$class}.{$method}",'access');
		return $result;
	}
}
/* End of file Hosting.php */
/* Location: ./application/modules/hosting/controllers/Hosting.php */