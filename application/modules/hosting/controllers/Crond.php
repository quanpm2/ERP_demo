<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crond extends Public_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('hosting/hosting_report_m');
	}

	public function delete_log() {
		$this->db->where(['log_type' => 'warning_hosting_extend'])->delete('log');
	}

	public function update_hosting() 
	{
		$term_ids   = [2210, 2209] ;

		foreach($term_ids as $term_id) 
		{
			$this->db->update('term', ['term_status' => 'publish']) ;
		}
	}

	public function index() 
	{
		$terms 					 = $this->term_m->get_many_by(['term_type' =>'hosting','term_status' =>'publish']);		
		if( empty( $terms ) ) return FALSE ; 

		$this->extend_contract($terms) ;
		$this->warning_contract_deadline($terms) ;
	}

	public function warning_contract_deadline($terms) 
	{
		if( empty($terms) ) return FALSE ;

		$time_current          =  time();
		$time_current          =  $this->mdate->startOfDay($time_current) ;

		foreach($terms as $term) 
		{

			// Thời hạn bắt đầu HĐ
			$contract_begin    				= (int)get_term_meta_value($term->term_id, 'contract_begin') ;
			$contract_begin    				= $this->mdate->startOfDay($contract_begin) ;
				
			// Thời hạn kết thúc HĐ
			$contract_end      				= (int)get_term_meta_value($term->term_id, 'contract_end')   ;
			$contract_end      				= $this->mdate->startOfDay($contract_end) ;

			// Kiểm tra thời gian hợp lệ
			if( $time_current < $contract_begin ) continue ;
			
			// Trước 15 ngày kết thúc HĐ
			$before_15day_contract_end  	= strtotime('-15 day', $contract_end);
			$before_15day_contract_end      = $this->mdate->startOfDay($before_15day_contract_end) ;

			// Trước 3 ngày kết thúc HĐ
			$before_3day_contract_end       = strtotime('-3 day', $contract_end);
			$before_3day_contract_end       = $this->mdate->startOfDay($before_3day_contract_end) ;	
		
			// Sau 5 ngày kết thúc HĐ 		
			$after_5day_contract_end        = strtotime('+5 day', $contract_end);
			$after_5day_contract_end        = $this->mdate->startOfDay($after_5day_contract_end) ;	

			// Gửi mail trước 15 ngày kết thúc HĐ
			if( ($before_15day_contract_end <= $time_current) && ($time_current < $before_3day_contract_end )) 
			{
				$status_log  				= $this->check_log_hosting_warning_deadline($term->term_id) ;
				if( $status_log == TRUE ) {
					$status_send_mail       = $this->hosting_report_m->send_mail_alert_expiring_soon($term->term_id, 'before' , 15) ;
				}
				continue;			
			}
			
			// Gửi mail trước 3 ngày kết thúc HĐ
			if( ($before_3day_contract_end <= $time_current) && ($time_current < $contract_end ) ) 
			{
				$status_log  				= $this->check_log_hosting_warning_deadline($term->term_id, 'before', 3) ;			
				if( $status_log == TRUE ) 
				{
					$status_send_mail       = $this->hosting_report_m->send_mail_alert_expiring_soon($term->term_id, 'before' , 3) ;					
				} 		
				continue;	
			}

			// Sau 5 ngày kết thúc HĐ
			if( ($after_5day_contract_end >= $time_current) && ($time_current > $contract_end ) ) 
			{
				$status_log  				= $this->check_log_hosting_warning_deadline($term->term_id, 'after', 5) ;	
				if( $status_log == TRUE ) 
				{
					$status_send_mail       = $this->hosting_report_m->send_mail_alert_expiring_soon($term->term_id, 'after' , 5) ;
				}
				continue; 				
			}
	
		}
	}

	private function check_log_hosting_warning_deadline($term_id, $type = 'before', $day = 15) 
	{

		if(empty($term_id)) return FALSE ;

		$log       = $this->log_m->where('term_id'  , $term_id)
								 ->where('log_type' , 'warning_hosting_deadline_' . $type . '_' . $day . 'day')
								 ->get_by();	
								 
 		if(empty($log)) return TRUE;
 		return ( (int) $log->log_status !== 1 ) ;
	}

	public function extend_contract($terms) 
	{
		if( empty($terms) ) return FALSE ;

		$time_current          =  $this->mdate->startOfDay();

		foreach($terms as $term) 
		{

			// Thời hạn bắt đầu HĐ
			$contract_begin    				= (int)get_term_meta_value($term->term_id, 'contract_begin') ;
			$contract_begin    				= $this->mdate->startOfDay($contract_begin) ;
				
			// Thời hạn kết thúc HĐ
			$contract_end      				= (int)get_term_meta_value($term->term_id, 'contract_end')   ;
			$contract_end      				= $this->mdate->startOfDay($contract_end) ;

			// Kiểm tra thời gian hợp lệ
			if($time_current < $contract_begin ) continue ;
			if($time_current < $contract_end) continue ;
			
			// Số ngày gia hạn
			$number_of_extension_days  		= strtotime('+30 day', $contract_end);
			$number_of_extension_days       = $this->mdate->startOfDay($number_of_extension_days) ;
			//echo date('d-m-Y', $number_of_extension_days) ;

			// 7 ngày gia hạn hợp đồng
			if( ($number_of_extension_days >= $time_current) && ($time_current >= $contract_end ) ) 
			{
				$status_log  				= $this->check_log_hosting_extend_contract($term->term_id) ;	
				if( $status_log == TRUE ) 
				{
					$status_send_mail       = $this->hosting_report_m->send_mail_extend($term->term_id) ;
				}
				continue; 				
			}		
		}
	}

	private function check_log_hosting_extend_contract($term_id) 
	{
		if(empty($term_id)) return FALSE ;

		$log       = $this->log_m->where('term_id'  , $term_id)
								 ->where('log_type' , 'warning_hosting_extend')
								 ->get_by();	
								 
 		if(empty($log)) return TRUE;
 		return ( (int) $log->log_status !== 1 ) ;
	}
}