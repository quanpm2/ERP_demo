<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends Public_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$models = array(
			'hosting/hosting_m',
			'hosting/hosting_report_m');

		$this->load->model($models);
	}

	public function index() 
	{
        if( ! parent::check_token())
        {
            log_message('error',"#hosting/cron/index has been illegal access to");
            return FALSE;
        }

		if(ENVIRONMENT != 'production') return FALSE;

		$hour			= date('G');
		$token 			= parent::build_token();
		$curl_options	= array('timeout' => 1,'connect_timeout' => 10);

		switch ($hour)
		{
			case 8:

				// Gửi email thông báo sắp hết hạn hợp đồng HOSTING
				// try{ Requests::get(base_url("admin/hosting/cron/send_noticefinish_mail?token={$token}"),[],$curl_options); }
	   //      	catch(Exception $e) { /*do nothing*/ }

	   //      	// Gửi email thông báo hợp đồng đã hết hạn và cần được gia hạn để tiếp tục.
				// try{ Requests::get(base_url("admin/hosting/cron/send_mail_before_suspend?token={$token}"),[],$curl_options); }
	   //      	catch(Exception $e) { /*do nothing*/ }

				break;
			
			default: break;
		}

		return TRUE;
	}


	/**
	 * Send daily sms and weekly report email
	 */
	public function send_mail_before_suspend()
	{
		if( ! parent::check_token())
        {
            log_message('error','#hosting/cron/send_mail_before_suspend has been illegal access to');
            return FALSE;
        }

		$terms = $this->hosting_m->select('term_id')->set_term_type()->get_many_by(['term_status'=>'publish']);
		if(empty($terms)) return FALSE;

		foreach ($terms as $term)
		{
			$this->hosting_report_m->send_mail_before_suspend($term->term_id);
		}

		return TRUE;
	}

	/**
	 * Send daily sms and weekly report email
	 */
	public function send_noticefinish_mail()
	{
		if( ! parent::check_token())
        {
            log_message('error','#hosting/cron/send_mail_before_suspend has been illegal access to');
            return FALSE;
        }

		$terms = $this->hosting_m->select('term_id')->set_term_type()->get_many_by(['term_status'=>'publish']);
		if(empty($terms)) return FALSE;

		foreach ($terms as $term)
		{
			$this->hosting_report_m->send_noticefinish_mail($term->term_id);
		}

		return TRUE;
	}
}
/* End of file Crond.php */
/* Location: ./application/modules/facebookads/controllers/Crond.php */