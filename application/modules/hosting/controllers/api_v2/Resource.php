<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Resource extends MREST_Controller
{
    function __construct()
    {
        $this->autoload['libraries'][] = 'form_validation';

        $this->autoload['models'][] = 'log_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'hosting/hosting_m';
        $this->autoload['models'][] = 'hosting/hosting_report_m';
        $this->autoload['models'][] = 'hosting/hosting_kpi_m';

        $this->autoload['helpers'][] = 'date';

        parent::__construct();

        $this->load->config('hosting/group');
        $this->load->config('hosting/hosting');
    }

    public function contracts_get($contract_id)
    {
        if (!has_permission('hosting.overview.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), [
            'contract_id' => $contract_id,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contract_id',
                'label' => 'contract_id',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->hosting_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        // Prepare
        $service_packages = $this->config->item('service', 'packages');
        $service_packages = array_filter($service_packages, function ($item) {
            return $item['status'] == 'active';
        });
        $service_packages = key_value($service_packages, 'name', 'label');

        // Process
        $data = [];

        $data['term_id'] = $inputs['contract_id'];

        $contract_code = get_term_meta_value($inputs['contract_id'], 'contract_code');
        $data['contract_code'] = $contract_code ?: '';

        $contract_begin = get_term_meta_value($inputs['contract_id'], 'contract_begin');
        $data['contract_begin'] = isset($contract_begin) ? my_date($contract_begin, 'd/m/Y') : '';

        $contract_end = get_term_meta_value($inputs['contract_id'], 'contract_end');
        $data['contract_end'] = isset($contract_end) ? my_date($contract_end, 'd/m/Y') : '';

        $contract_region = get_term_meta_value($inputs['contract_id'], 'contract_region');
        $data['contract_region'] = $contract_region ?: '';

        $contract_value = get_term_meta_value($inputs['contract_id'], 'contract_value');
        $data['contract_value'] = (int) $contract_value ?: 0;

        $start_server_time = get_term_meta_value($inputs['contract_id'], 'start_server_time');
        $data['start_server_time'] = isset($start_server_time) ? my_date($start_server_time, 'd/m/Y') : '';

        $service_packages = $this->config->item('service', 'packages');
        $service_packages = array_filter($service_packages, function ($item) {
            return $item['status'] == 'active';
        });
        $service_packages = key_value($service_packages, 'name', 'label');
        $hosting_service_package = get_term_meta_value($inputs['contract_id'], 'hosting_service_package');
        $data['hosting_service_package'] = $service_packages[$hosting_service_package] ?? '';

        $representative_gender = get_term_meta_value($inputs['contract_id'], 'representative_gender');
        $data['representative_gender'] = $representative_gender ?: '';

        $representative_name = get_term_meta_value($inputs['contract_id'], 'representative_name');
        $data['representative_name'] = $representative_name ?: '';

        $representative_email = get_term_meta_value($inputs['contract_id'], 'representative_email');
        $data['representative_email'] = $representative_email ?: '';

        $representative_phone = get_term_meta_value($inputs['contract_id'], 'representative_phone');
        $data['representative_phone'] = $representative_phone ?: '';

        $sale_id = get_term_meta_value($inputs['contract_id'], 'staff_business');
        $data['staff_business_id'] = $sale_id ?: '';
        $data['staff_business_name'] = $this->admin_m->get_field_by_id($sale_id, 'display_name') ?: '';
        $data['staff_business_email'] = $this->admin_m->get_field_by_id($sale_id, 'user_email') ?: '';
        $data['staff_business_avatar'] = $this->admin_m->get_field_by_id($sale_id, 'user_avatar') ?: '';

        $tech_kpis = $this->hosting_kpi_m->select('user_id')
            ->where('term_id', $inputs['contract_id'])
            ->where('kpi_type', 'tech')
            ->as_array()
            ->get_many_by();
        $staffs_ids = array_column($tech_kpis, 'user_id');

        $data['technical_staffs'] = [];
        if (!empty($staffs_ids)) {
            foreach ($staffs_ids as $staff_id) {
                $user_name = $this->admin_m->get_field_by_id($staff_id, 'display_name');
                if ($user_name) $technical_staffs[] = $user_name ?: $this->admin_m->get_field_by_id($staff_id, 'user_mail');
            }

            foreach ($staffs_ids as $staff_id) {
                $tech_name = $this->admin_m->get_field_by_id($staff_id, 'display_name');
                $tech_email = $this->admin_m->get_field_by_id($staff_id, 'user_email');
                $tech_avatar = $this->admin_m->get_field_by_id($staff_id, 'user_avatar');
                $staff = [
                    'id' => $staff_id,
                    'name' => $tech_name,
                    'email' => $tech_email,
                    'avatar' => $tech_avatar,
                ];
                $data['technical_staffs'][] = $staff;
            }
        }

        return parent::responseHandler($data, 'Lấy thông tin thành công');
    }

    public function customer_get($contract_id)
    {
        if (!has_permission('hosting.overview.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), [
            'contract_id' => $contract_id,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contract_id',
                'label' => 'contract_id',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->hosting_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        // Process
        $data = [];

        $representative_gender = get_term_meta_value($inputs['contract_id'], 'representative_gender');
        $representative_name = get_term_meta_value($inputs['contract_id'], 'representative_name');
        $data['name'] = ($representative_gender == 1 ? 'Ông' : 'Bà') . ' ' . $representative_name;

        $representative_email = get_term_meta_value($inputs['contract_id'], 'representative_email');
        $data['email'] = $representative_email;

        return parent::responseHandler($data, 'Lấy thông tin thành công');
    }

    public function tech_staffs_get($contractId = 0)
    {
        if (!has_permission('hosting.kpi.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        if (empty($contractId)) {
            return $this->get_many_tech_staffs();
        } else {
            $inputs = wp_parse_args(parent::post(), [
                'contractId' => $contractId,
            ]);

            // Validate
            $rules = array(
                [
                    'field' => 'contractId',
                    'label' => 'contractId',
                    'rules' => [
                        'required',
                        'integer',
                        array('existed_check', array($this->hosting_m, 'existed_check'))
                    ]
                ],
            );

            $this->form_validation->set_data($inputs);
            $this->form_validation->set_rules($rules);
            if (FALSE == $this->form_validation->run()) {
                return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
            }

            return  $this->get_tech_staffs_by_contract_id($inputs['contractId']);
        }
    }

    protected function get_many_tech_staffs()
    {
        $users = $this->admin_m->select('user_id, display_name, user_email')
            ->where_in('role_id', $this->config->item('group_memberId'))
            ->set_get_active()
            ->order_by('display_name')
            ->as_array()
            ->get_many_by();

        if (empty($users)) {
            return parent::responseHandler(NULL, 'Không có thông tin kỹ thuật.', 'success', 204);
        }

        return parent::responseHandler($users, 'Lấy thông tin thành công.');
    }

    protected function get_tech_staffs_by_contract_id($contract_id)
    {
        $kpis = $this->hosting_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(['term_id' => $contract_id]);
        if (empty($kpis)) {
            return parent::responseHandler(NULL, 'Không có thông tin kỹ thuật.', 'success', 204);
        }

        $data = [];
        foreach ($kpis as $kpi) {
            $staff_name = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');
            $staff_email = $this->admin_m->get_field_by_id($kpi['user_id'], 'user_email');

            $data[] = [
                'kpi_id' => $kpi['kpi_id'],
                'display_name' => $staff_name,
                'user_email' => $staff_email,
            ];
        }

        return parent::responseHandler($data, 'Lấy thông tin thành công.');
    }

    public function kpi_post($contract_id)
    {
        if (!has_permission('hosting.kpi.add')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::post(), ['contract_id' => $contract_id]);

        // Validate
        $rules = array(
            [
                'field' => 'contract_id',
                'label' => 'contract_id',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->hosting_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'staff_id',
                'label' => 'staff_id',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', function ($value) {
                        return $this->admin_m->existed_check($value);
                    })
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::response(['code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array()]);
        }

        // Process
        // ??? Hop dong trang thai nao duoc add tech staff
        $kpi_datetime = time();
        $this->hosting_kpi_m->update_kpi_value($inputs['contract_id'], 'tech', 1, $kpi_datetime, $inputs['staff_id']);

        $this->term_users_m->set_relations_by_term($inputs['contract_id'], [$inputs['staff_id']], 'admin');

        return parent::responseHandler([], 'Thêm kỹ thuật thành công.');
    }

    /**
     * @return json
     */
    public function kpi_delete($contractId, $kpiId)
    {
        if (!has_permission('hosting.kpi.delete')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::delete(), [
            'contractId' => $contractId,
            'kpiId' => $kpiId
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->hosting_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'kpiId',
                'label' => 'kpiId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->hosting_kpi_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $kpi = $this->hosting_kpi_m->get($kpiId);
        if (strtotime($kpi->kpi_datetime) < $this->mdate->startOfMonth()) {
            return parent::responseHandler(NULL, 'Kpi không hợp lệ', 'error', 400);
        }

        $this->hosting_kpi_m->delete_cache($kpiId);
        $this->hosting_kpi_m->delete($kpiId);


        return parent::responseHandler(NULL, 'Xoá kỹ thuật thành công.');
    }

    public function start_service_post($contractId)
    {
        if (!has_permission('hosting.start_service.update')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::post(), [
            'contractId' => $contractId,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->hosting_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $term = $this->hosting_m->get($inputs['contractId']);
        $status_proc_service  = $this->hosting_contract_m->proc_service($term);

        if (FALSE === $status_proc_service) {
            return parent::responseHandler(null, 'Có lỗi xảy ra trong quá trình kích hoạt.', 'error', 403);
        }

        /* Phân tích hợp đồng ký mới | tái ký */
        $this->load->model('contract/base_contract_m');
        $this->base_contract_m->detect_first_contract($inputs['contractId']);


        /* Gửi SMS thông báo kích hoạt hợp đồng đến khách hàng */
        $this->load->model('contract/contract_report_m');
        $this->contract_report_m->send_sms_activation_2customer($inputs['contractId']);

        return parent::responseHandler(NULL, 'Kích hoạt thành công.');
    }

    public function stop_service_post($contractId)
    {
        if (!has_permission('hosting.stop_service.update')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 400);
        }

        $inputs = wp_parse_args(parent::post(), [
            'contractId' => $contractId,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->hosting_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $term = $this->hosting_m->get($inputs['contractId']);
        $status = $this->hosting_contract_m->stop_service($term);
        if (FALSE === $status) {
            return parent::responseHandler(null, 'Có lỗi xảy ra trong quá trình kích hoạt.', 'error', 400);
        }

        return parent::responseHandler(NULL, 'Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
    }
}
/* End of file Resource.php */
/* Location: ./application/modules/hosting/controllers/api_v2/Resource.php */