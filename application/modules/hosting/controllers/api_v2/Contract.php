<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends MREST_Controller
{
	function __construct()
	{
		$this->autoload['libraries'][] 	= 'form_validation';
        $this->autoload['models'][] 	= 'contract/contract_m';
        $this->autoload['models'][] 	= 'hosting/hosting_m';
        $this->autoload['helpers'][] 	= 'date';

		parent::__construct();
	}

	public function index_get()
	{
		$default 	= array('id' => '', 'meta' => '', 'field' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if( empty($args['id']) || empty($args['meta'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		$data = array('msg' => 'Xử lý không thành công', 'data' => []);

		if(FALSE === $this->hosting_m->has_permission($args['id'], 'admin.contract.view'))
		{
			$data['msg'] = 'Không có quyền truy xuất hoặc hợp đồng không khả dụng !';
			parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
		}

		$data = array('id' => $args['id']);
		$args['meta'] = is_string($args['meta']) ? explode(',', $args['meta']) : $args['meta'];
		foreach ($args['meta'] as $meta)
		{
			$_value = get_term_meta_value($args['id'], $meta);
			$data[$meta] = is_serialized($_value) ? unserialize($_value) : $_value;
		}		

		$response['data'] = $data;
		parent::response($response);
	}

	/**
	 * Update Actions
	 */
	public function index_post()
	{
		$args = wp_parse_args(parent::post(NULL, TRUE), ['id' => 0]);
		if( empty($args['pk']) || empty($args['name'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		if(FALSE === $this->contract_m->has_permission($args['pk'], 'admin.contract.update'))
		{
			parent::response('Không thể thực hiện tác vụ, quyền truy cập bị hạn chế', parent::HTTP_NOT_ACCEPTABLE);
		}

        empty($args['id']) AND $args['id'] = $args['pk'] ?? 0;
        $this->form_validation->set_data($args);

		$configRules = array(
            [
                'field' => 'id',
                'label' => 'Contract Id',
                'rules' => 'required|integer'
            ],
            [
                'field' => 'type',
                'label' => 'Contract Id',
                'rules' => 'in_list[meta]'
            ],
            [
                'field' => 'name',
                'label' => 'name',
                'rules' => 'in_list[hosting_months,discount_amount]'
            ]
        );

        switch ($args['name'])
		{
			case 'hosting_months':
				array_push($configRules, [ 'field' => 'value', 'label' => 'Số tháng đăng ký', 'rules' => 'required|integer|greater_than_equal_to[12]' ]);
				break;

			case 'discount_amount':
				array_push($configRules, [ 'field' => 'value', 'label' => 'Giá trị khuyến mãi', 'rules' => 'required|integer|greater_than_equal_to[0]' ]);
				break;
			
			default:
				break;
		}


        $this->form_validation->set_rules($configRules);
        if(FALSE == $this->form_validation->run()) parent::response($this->form_validation->error_array(), parent::HTTP_NOT_ACCEPTABLE);

		$messages = ['Cập nhật dữ liệu thành công'];

		update_term_meta($args['id'], $args['name'], $args['value']);

		try
		{
			$this->hosting_m->set_contract($args['pk']); // Init & dectect contract specified
			$contract = $this->hosting_m->get_contract();

			update_term_meta($args['pk'], 'contract_value', $this->hosting_m->calc_contract_value());

			$this->hosting_m->delete_all_invoices(); //  Delete all invoices available
			$this->hosting_m->create_invoices(); // Create new contract's invoices

			$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

			$this->hosting_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
			$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
		}
		catch (Exception $e)
		{
			parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
		}

		parent::response(['msg' => $messages]);
	}
}
/* End of file Contract.php */
/* Location: ./application/modules/hosting/controllers/api_v2/Contract.php */