<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'vendor/autoload.php';

class Config extends MREST_Controller
{
    /**
     * API dữ liệu các đợt thanh toán
     */
    public function index_get()
    {
        $this->config->load('hosting/hosting', TRUE);
    	parent::response([
    		'status' 	=> TRUE,
    		'data' 		=> $this->config->item('hosting')
    	]);
    }
}
/* End of file Index.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Index.php */