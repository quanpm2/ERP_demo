<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="col-md-12" id="service_tab">
<?php
$this->table->set_caption('Chi tiết dịch vụ');
$this->config->load('hosting/hosting');

$packages = $this->config->item('service', 'packages');
$headings = [''];
$spackage = get_term_meta_value($edit->term_id, 'hosting_service_package');
$packages = array_filter($packages, function($x) use($spackage){ return ($x['status'] == 'active' || $x['name'] == $spackage); });

usort($packages, function($a, $b){ if(empty($a['disk']) || empty($b['disk'])) return FALSE; return $a['disk'] >= $b['disk']; });

foreach ($packages as $key => $value) 
{
    $headings[] = array(    
        'data' => '<label>'.form_radio('hosting_service_package', $value['name'],  $spackage == $value['name'], ['class'=>'minimal']).br(2).$value['label'].($value['status']!='active'?'<br><small><i>(Đã hủy bỏ)</i></small>':'').'</label>',
        'class' => 'text-center',
        'style' => 'vertical-align: top;'
	);
}

$this->table->set_heading($headings);
$service_package = get_term_meta_value($edit->term_id, 'service_package');
$service_package = $service_package ? unserialize($service_package) : [];
$package_config  = $this->config->item('package_config');
foreach ($package_config as $key => $cfg)
{
	$_row = array();
	array_push($_row, $cfg['text']);
	foreach ($packages as $p)
	{
		if(array_key_exists($key, $p) && is_null($p[$key]) && $_config = $this->config->item($key, 'package_config'))
    	{
    		$_input = '';
    		$value = ($p['name'] == ($service_package['name']??'')) ? $service_package[$key] : '';
    		switch ($_config['type'])
    		{
    			case 'number':
    				$_input = form_input(['name'=>"meta[service_package][{$p['name']}][{$key}]",'type'=>'number'], $value, ['class'=>'form-control','placeholder'=>'Tùy chỉnh']);
    				break;

    			case 'bool':
    				$_input = form_dropdown("meta[service_package][{$p['name']}][{$key}]",array(FALSE => 'Không',TRUE => 'Có'), $value, ['class'=>'form-control','placeholder'=>'Tùy chỉnh']);
    				break;

    			case 'enum':
    				$_input = form_dropdown("meta[service_package][{$p['name']}][{$key}]", ($_config['list']??[]), $value, ['class'=>'form-control', 'style'=>'text-align:center','placeholder'=>'Số năm đăng ký']);
    				break;

    			default:
    				$_input = form_input("meta[service_package][{$p['name']}][{$key}]", $value, ['class'=>'form-control','placeholder'=>'Tùy chỉnh']);	
    				break;
    		}

    		$_row[] = $_input;
    		continue;
    	}

        if('price_year' == $key) $p[$key] = $p['price']*12;

        if( ! isset($p[$key])) $p[$key] = '--';

        if( ! empty($field['divide_by'])) $p[$key]/=$field['divide_by'];


        if(is_bool($p[$key])) $p[$key] = $p[$key] ? '<i class="fa fa-fw fa-check text-green"></i>' : '--';
        else if(is_numeric($p[$key])) $p[$key] = currency_numberformat($p[$key], ($field['unit']??''));

        $_row[] = array('data' => $p[$key], 'class' => 'text-center');
	}

	$this->table->add_row($_row);
}


echo $this->admin_form->form_open();
echo $this->admin_form->input_numberic('Giá trị khuyến mãi', 'meta[discount_amount]', (double) get_term_meta_value($edit->term_id, 'discount_amount'));
echo $this->table->generate();
echo form_hidden('edit[term_id]', $edit->term_id);
echo $this->admin_form->submit('','confirm_step_service','confirm_step_service','',array('style'=>'display:none;','id'=>'confirm_step_service'));
echo $this->admin_form->form_close();
?>
</div>

<script type="text/javascript">
$(function(){
  /* init iCheck plugin */
  $('#service_tab input[type="radio"]').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });
}) ;

$(document).ready(function() {
	
	$('#service_tab :input').on('ifChecked', function(event) {
	   	    var package  		 		 = $(this).val() ;

	   	    $('#load-package-info input').val('');
	   	    
	   	    var hosting_disk             = $(this).data('disk') ;
	   	    var hosting_bandwidth        = $(this).data('bandwidth') ;
	   	    var hosting_email_webmail    = $(this).data('email-webmail') ;
	   	    var hosting_price            = $(this).data('price') ;
	   	    
	   	   	$('#hosting_disk').val(hosting_disk) ; 
	   	   	$('#hosting_bandwidth').val(hosting_bandwidth) ; 
	   	    $('#hosting_email_webmail').val(hosting_email_webmail) ; 
	   	   	$('#hosting_price').val(hosting_price) ;
	});

	// Khi chọn Hosting khác
	$(':input[value="hosting_orther"]').on('ifChecked', function(event) {
		 $('#load-package-info input').removeAttr('readonly');
	});

	// Không chọn hosting khác : readonly	
	$(':input[value!="hosting_orther"]').on('ifChecked', function(event) {
		 $('#load-package-info input').attr('readonly', 'true');
		 //$('#load-package-info').css('display', 'none') ;	
	});
  
	var load_package_info = $("#load-package-info").closest('form');
	load_package_info.validate({  
		rules: {
		  'meta[hosting_disk]': {
		    required: true,
		  },
		  'meta[hosting_bandwidth]': {
		    required: true,
		  },
		  'meta[hosting_price]': {
		    required: true,
		  }		  
		},
		messages:{
		  'meta[hosting_disk]': {
		    required: 'Dung lượng không được để trống',
		    digits: 'Kiểu dữ liệu không hợp lệ , dung lượng phải là kiểu số',
		  },
		  'meta[hosting_bandwidth]': {
		    required: 'Băng thông không được để trống',
		    digits: 'Kiểu dữ liệu không hợp lệ , băng thông phải là kiểu số',
		  },
		  'meta[hosting_price]': {
		    required: 'Giá không được để trống',
		    digits  : 'Kiểu dữ liệu không hợp lệ , chi phí tháng phải là kiểu số',
		  }
		}
	});
}) ;
</script>

<?php
/* End of file wizard_partial.php */
/* Location: ./application/modules/hosting/views/contract/wizard_partial.php */