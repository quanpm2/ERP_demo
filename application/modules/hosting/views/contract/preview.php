<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>  
<div class="content">
	<div class="row-containter">
		<p><b>Dựa trên:</b> Nhu cầu của Bên A và khả năng cung cấp của Bên B về dịch vụ. Hai bên thống nhất ký kết hợp đồng kinh tế với các điều khoản cụ thể như sau:</p> 
		<p><b><u>Điều 1:</u> Nội dung hợp đồng</b>
			<p><b>- Đề xuất mua hosting:</b></p>
			<div>
				<p>+ Hosting ( Duy trì hàng năm)</p> 
				<?php if ($price_list): ?>
				<ul class="list-unstyled" style="list-style-type: none;">
					<?php foreach ($price_list as $item): ?>
						<li><?php echo $item; ?></li>
					<?php endforeach ?>	
				</ul>
				<?php endif ?>
			</div>
		</div>
		<div class="row-containter">
			<p>( Giá trên chưa bao gồm thuế VAT )</p>     
			<p><b><u>Điều 2:</u> Gía trị hợp đồng và phương thức thanh toán</b></p>
			<p>Đơn vị tính giá: Việt Nam Đồng (VND).</p>
			<p>Thanh toán bằng tiền đồng Việt Nam.</p>
		</div>
		<div class="row-containter">
			<?php
			$this->table->clear();
			$this->table->set_template(['table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">']);
			$this->table->set_heading('STT', 'Khoản mục', 'Gói dịch vụ', 'Thời gian', 'Thành tiền');

			$total = money_round($month*$price);
			$price_cell = array('data'=>currency_numberformat($total),'align'=>'right');
			$this->table->add_row(1,'Lệ phí đăng ký hosting',"{$service_package['month']} tháng",'Từ ngày ký kết hợp đồng',$price_cell);

			$buy2get1 = (bool) get_term_meta_value($term_id, 'buy2get1');

			if(!empty($service_package['buy2get1']) && div($month, 12) >= 3)
			{
				$discount_percent 	= ((int) get_term_meta_value($term_id, 'hosting_discount_percent')) ?: div(100, 3);
				$_discount_amount = $total* div($discount_percent, 100);
				$this->table->add_row(['data'=>'<b>Chương trình khuyến mãi mua 2 tặng 1</b>','colspan'=>2],NULL,NULL,array('data'=>currency_numberformat($_discount_amount),'align'=>'right'));
				$total-= $_discount_amount;
			}

			$discount_percentage = (int) get_term_meta_value($term_id, 'hosting_discount_percent');
			if($discount_percentage) 
			{ 
				$_discount_amount = $total*div($discount_percentage,100);
				$this->table->add_row(['data'=>"Giảm giá {$discount_percentage}%",'colspan'=>2],'','',array('data'=>currency_numberformat($_discount_amount),'align'=>'right'));

				$total-= $_discount_amount;
			}

			if($discount_amount) 
			{ 
				$this->table->add_row(['data'=>"Giảm giá trực tiếp",'colspan'=>2],'','',array('data'=>currency_numberformat($discount_amount),'align'=>'right'));
				$total-= $discount_amount;
			}

			$this->table->add_row(['data'=>'Tổng cộng','colspan'=>2],'','',['data'=>currency_numberformat($total),'align'=>'right']);

			if(!empty($vat))
			{
				$tax_amount   = $total*div($vat,100);
				$tax_amount_text = currency_numberformat($tax_amount);
				$this->table->add_row(['data'=>"VAT {$vat}%",'colspan'=>2],'','',['data'=>"<strong>{$vat}&#37; ({$tax_amount_text}) </strong>",'align'=>'right']);      

				$total = cal_vat($total,$vat);
			}

			$this->table->add_row(array('data'=>'Tổng thanh toán','colspan'=>2),'','',array('data'=>currency_numberformat($total),'align'=>'right'));
			echo $this->table->generate();              
			?>
			<p><em>Bằng chữ: <b><i><?php echo ucfirst(mb_strtolower(convert_number_to_words($total)));?> đồng.</i></b></em><br/>
			</div>
			<div class="row-containter">
				<p>Phương thức thanh toán</p>
				<?php
				$args = array(
					'select' => 'posts.post_id, post_title,post_status,start_date,post_content,end_date',
					'tax_query' => array( 'terms'=>$term->term_id),
					'numberposts' =>0,
					'orderby' => 'posts.end_date',
					'order' => 'ASC',
					'post_type' => 'contract-invoices');
				$lists = $this->invoice_m->get_posts($args);

				$is_one_payments = empty($lists) || (!empty($lists) && count($lists) == 1);

				if($is_one_payments)
				{
					echo '<p>- Ngay sau khi hợp đồng đươc ký BÊN A thanh toán cho BÊN B 100% chi phí Host ( vì host Công ty phải thanh toán 100% cho nhà cung cấp trước khi khởi tạo domain và chia hosting )</p>';
				}
				else
				{
					$number_of_payments = count($lists);
					$i=1;
					echo '<p>- Bên A thanh toán cho Bên B làm '.$number_of_payments.' đợt:</p>';
					echo '<ul style="list-style-type: square;">';
					foreach ($lists as $inv) { 
						$price_total = $this->invoice_item_m->get_total($inv->post_id, 'total');
						$price_total = cal_vat($price_total, $vat);
						$price_total = numberformat($price_total,0,'.','');

						printf('<li> Đợt %s: thanh toán %s (%s), chậm nhất là ngày %s.</li>',
							$i,
							currency_numberformat($price_total,'đ'),
							currency_number_to_words($price_total), 
							my_date($inv->end_date,'d/m/Y'));
						$i++;
					}
					echo '</ul>';
				}
				?>

				<p>- Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin tài khoản do Bên B cung cấp, cụ thể:</p>

				<?php if( ! empty($bank_info)) :?>
			    <ul style="padding-left:0;list-style:none">
			    <?php foreach ($bank_info as $label => $text) :?>
			        <?php if (is_array($text)) : ?>
			            <?php foreach ($text as $key => $value) :?>
			                <li>- <?php echo $key;?>: <?php echo $value;?></li>
			            <?php endforeach;?>
			        <?php continue; endif;?>
			        <li>- <?php echo $label;?>: <?php echo $text;?></li>
			    <?php endforeach;?>
			    </ul>
			    <p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
			    <?php endif; ?>

				<p><b><u>Điều 3:</u> Trách nhiệm và quyền hạn của mỗi bên</b></p>
				<p><u>3.1 Trách nhiệm và quyền hạn của Bên A:</u></p>
				<ul class="list-unstyled" style="list-style-type: none; text-align: justify-all;">
					<li>- Cung cấp nội dung thông tin đảm bảo chính xác, trung thực trong khuôn khổ luật pháp quy định.</li>
					<li>- Có trách nhiệm thanh toán các khoản chi phí theo như quy định tại Điều 2 trên.</li>
					<li>- Có quyền khiếu nại về chất lượng thông tin, chất lượng dịch vụ do Bên B cung cấp. Mọi khiếu nại phải được gửi cho Bên B dưới dạng văn bản trong vòng 05 ngày kể từ ngày phát sinh vấn đề và Bên B trả lời khiếu nại cho Bên A trong vòng 05 ngày kể từ ngày Bên B nhận được công văn của Bên A.</li>
				</ul>
				<p><u>3.2 Trách nhiệm và quyền hạn của Bên B:</u></p>
				<ul class="list-unstyled" style="list-style-type: none; text-align: justify-all;">
					<li>- Cung cấp các dịch vụ theo Điều 1 của Hợp đồng này.</li>
					<li>- Trước khi hết hạn sử dụng dịch vụ 15 ngày  Bên B sẽ thông báo cho khách hàng lần 1, và sẽ tiếp tục thông báo lần 2 trước khi hết hạn 03 ngày. </li>
					<li>- Bên B sẽ thông báo lần 3 trong kỳ gia hạn 05 ngày và sau thời gian gia hạn dịch vụ sẽ tạm ngừng nếu Bên A vẫn chưa thanh tóan cho Bên B. Dịch vụ chỉ được khôi phục trong vòng 30 ngày kể từ ngày  tạm ngừng cung cấp dịch vụ.</li>
				</ul>    
				<p><b><u>Điều 4:</u> Bất khả kháng</b></p>
				<ul class="list-unstyled" style="list-style-type: none; text-align: justify-all;">
					<li>4.1 Nếu có bất kỳ sự kiện nào như thiên tai,dịch họa, lũ lụt,bão, hỏa hoạn , động đất hoặc các hiểm họa thiên tai khác hoặc việc can thiệp của nhà nước, hay bất kỳ sự kiện nào khác xảy ra ngoài tầm kiểm soát  của bất kỳ bên nào và không thể lường trước được , thì Bên bị  sự kiện bất khả kháng làm ảnh hưởng được tạm hoãn thực hiện nghĩa vụ,với điều kiện là Bên bị ảnh hưởng đó đã áp dụng mọi biện pháp cần thiết và có thể để ngăn ngừa ,hạn chế hoặc khắc phục hậu quả của sự kiện đó.
					</li>
					<li>4.2 Bên bị ảnh hưởng bởi sự kiện bất khả kháng đó có nghĩa vụ thông báo cho bên còn lại. Trong trường hợp sự kiện bất khả kháng xảy ra , các bên được miễn trách nhiệm bồi thường thiệt hại.</li>
					<li>4.3 Nếu sự kiện không chấm dứt trong vòng 30 (ba mươi) ngày làm việc hoặc một một khoản thời gian lâu hơn và vẫn tiếp tục ảnh hưởng đến việc thực hiện hợp đồng thì bên nào cũng có quyền đơn phương chấm dứt hợp đồng và thông báo cho bên còn lại bằng văn bản trong vòng 03 (ba) ngày làm việc kể từ ngày dự định chấm dứt.</li>
					<li>4.4 Khi sự kiện bất khả kháng chấm dứt , các bên sẽ tiếp tục thực hiện hợp đồng nếu việc tiếp tục thưc hiện hợp đồng được sự  đồng ý của 2 bên và có thể thực hiện được.</li>
				</ul> 
				<p><b><u>Điều 5:</u> Điều khoản chung</b></p>
				<ul class="list-unstyled" style="list-style-type: none; text-align: justify-all;">
					<li>5.1 Hai Bên cam kết thực hiện đúng nghĩa vụ của mình theo các điều khoản đã ghi trong Hợp đồng.</li>
					<li>5.2 Trong quá trình thực hiện Hợp đồng, nếu có bất kỳ vấn đề nào phát sinh, hai Bên cùng trao đổi, giải quyết trên tinh thần hợp tác và giúp đỡ lẫn nhau.</li>
					<li>5.3 Nếu có tranh chấp xảy ra thì mọi vấn đề không thống nhất sẽ được giải quyết theo quy định của pháp luật.</li>
					<li>5.4 Hợp đồng này được lập thành 02 bản có giá trị pháp lý như nhau, mỗi Bên giữ 01 bản và có hiệu lực kể từ ngày ký.</li> 
				</ul>   
			</div>
		</div>
