<?php 
$service_package = get_term_meta_value($term_id, 'service_package');
$service_package = unserialize($service_package);
?>

<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #ff9f00;">
                        <table width="600" class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" bgcolor="#ff0000" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông báo hợp đồng "Hosting - lưu trữ website" sắp kết thúc</td>
                                 <!--End title-->
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <tr>
                                 <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                    Xin chào quý khách.<br>
                                    <p><b>WebDoctor.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</b></p>
                                    <p><b>Chúng tôi xin thông báo về việc sắp đến hạn dịch vụ Lưu trữ Website (Hosting), quý khách vui lòng liên hệ nhân viên Kinh doanh của Webdoctor.vn để tiến hành gia hạn dịch vụ.</b></p>
                                 </td>
                              </tr>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <tr>
                                 <td>
                                    <!-- date -->
                                    <table style="border-radius:5px;" class="table1-3" bgcolor="#ecf0f1" width="183" border="0" align="left" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <tr>
                                             <td align="center">
                                                <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                   <tbody>
                                                      <tr>
                                                         <td align="center">
                                                            <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                               <tbody>
                                                                 <tr>
                                                                    <td height="10"></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:30px; color:#333333; font-weight: bold;">CÒN</td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:60px; color:#34495e;font-weight: bold;line-height: 48px;">
                                                                        <?php echo $remain_days ?: '0';?></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:30px; color:#3498db; font-weight: bold;">NGÀY</td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td height="10"></td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- End date -->
                                    <!--Space-->
                                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                       <tbody>
                                          <tr>
                                             <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left:24px;">&nbsp;</p>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!--End Space-->
                                    <!--Content-->
                                    <table class="table3-1" width="342" border="0" align="right" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <tr>
                                             <td align="left" style="font-family: open sans, arial, sans-serif; font-size:15px; color:#444444; line-height:28px;">
                                                <b>Website:</b> <?php echo $term->term_name;?><br>
                                                
                                                <b>Gói Hosting:</b> <?php echo number_format((float)$service_package['disk'] ?? 0, 0, '', '.') ; ?> Mb<br>
                                                <b>Ngày kết thúc:</b> <?php echo my_date($contract_end,'d/m/Y');?><br>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!--End Content-->
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <!--Content-->
                              <tr>
                                 <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                    <p style="font-family: open sans, arial, sans-serif; font-size:15px">Để thực hiện gia hạn dịch vụ vui lòng liên hệ với chúng tôi theo thông tin dưới đây:</p>
                                    <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                       <tbody>

                                          <?php if (!empty($sale)): ?>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Kinh doanh <?= $sale->display_name?>: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php
                                                   $phone = get_user_meta_value($sale->user_id,'user_phone');
                                                   if(!$phone) $phone = '(028) 7300. 4488';
                                                   
                                                   echo $phone; ?></td>
                                             </tr>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?= $sale->user_email ?></td>
                                             </tr>
                                          <?php endif ?>

                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Hotline Trung tâm Kinh doanh:</td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">(028) 7300. 4488</td>
                                          </tr>
                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">sales@webdoctor.vn</td>
                                          </tr>


                                          <!-- KỸ THUẬT THỰC HIỆN -->
                                          <?php if (!empty($technical_staff)): ?>
                                             <tr>
                                                <td>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Kỹ thuật <?php echo $technical_staff['name']; ?>: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $technical_staff['phone']; ?></td>
                                             </tr>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $technical_staff['email']; ?></td>
                                             </tr>
                                             </td>
                                             </tr>
                                          <?php endif ?>
                                          <tr>
                                             <td height="25"></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <p><b>Việc dịch vụ quá hạn không được gia hạn kịp thời có thể dẫn đến gián đoạn sử dụng dịch vụ của quý khách.</b></p>
                                    <p><i>Quý khách vui lòng bỏ qua thông báo này nếu đã thực hiện gia hạn dịch vụ.</i></p>
                                    <p style="text-align:right">CHÂN THÀNH CẢM ƠN</p>
                                 </td>
                              </tr>
                              <!--End Content-->
                              <tr>
                                 <td align="left" height="30"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td height="15"></td>
      </tr>
   </tbody>
</table>
<!-- thong tin hop dong -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin hợp đồng</td>
                                 <!--End title-->
                                 <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tbody>
                              <tr>
                                 <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--end detail-->
                     </td>
                  </tr>
                  <!--start Article-->
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Mã hợp đồng: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id,'contract_code') ;?>
                                 </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Thời gian hợp đồng: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo $contract_date ;?></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Gói: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $service_package['label'] ?? '--' ?> MB                                 </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Dung lượng: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo number_format((float)$service_package['disk'] ?? 0, 0, '', '.') ?? '--' ?> MB                                 </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Băng thông: </td>
                                 <td width="300" align="left" style="font-family:o pen sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo number_format((float)$service_package['bandwidth'] ?? 0, 0, '', '.') ;?> GB                                 </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email/Webmail: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo ($service_package['email_webmail'] ?: '--'); ?>                                </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Chi phí tháng: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo number_format((float)$service_package['price'] ?? 0, 0, '', '.') ;?> VNĐ                                 </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Chi phí: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo number_format(((float)$service_package['month'] ?? 0) * ((float)$service_package['price'] ?? 0), 0, '', '.') . 'VNĐ/' . ($service_package['month'] ?? 0) . ' tháng' ;?></td>
                              </tr>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <!--end Article-->
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- ong tin hop dong -->
<!-- thong tin khach hang -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin khách hàng</td>
                                 <!--End title-->
                                 <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tbody>
                              <tr>
                                 <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--end detail-->
                     </td>
                  </tr>
                  <!--start Article-->
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Tên khách hàng: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Anh/Chị <?php echo get_term_meta_value($term->term_id,'representative_name')?></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id, 'representative_email') ;?></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Điện thoại di động: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id, 'representative_phone') ;?></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Địa chỉ: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id, 'representative_address') ;?></td>
                              </tr>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <!--end Article-->
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- thong tin khach hang -->