<?php
$this->template->stylesheet->add('plugins/tagsinput/tagsinput.css');

$this->template->javascript->add('plugins/tagsinput/tagsinput.js');

if(!is_service_end($term))
{
  $this->admin_form->set_col(12,6);
  echo $this->admin_form->box_open();
  echo $this->admin_form->form_open('', 'id="start_process_submit"');
  // echo form_button(['id'=>'start_process','name'=>'start_process','type'=>'submit','class'=>'btn btn-danger','value'=>'start_process'],'<i class="fa fa-fw fa-play"></i>Thực hiện chăm sóc website');
  $btn_submit_proc = array('id'=>'start_process', 'name'=>'start_process', 'type'=>'submit', 'class'=>'btn btn-danger', 'value'=>'start_process','data-toggle'=>'confirmation');
  $btn_submit_text = '';
  
  $start_service_time       =  get_term_meta_value($term_id,'start_service_time');
  if( ! $start_service_time )
  {
      $btn_submit_proc['class'] = 'btn btn-default';
      $btn_submit_text          = '<i class="fa fa-fw fa-play"></i>Kích hoạt';
      echo form_button($btn_submit_proc, $btn_submit_text);
  }

  echo '<p class="help-block">Vui lòng cấu hình SMS, cấu hình Email và thông số để hệ thống gửi Mail kích hoạt và Mail thông báo kết nối khách hàng </p>' ;
  echo $this->admin_form->box_close();
  echo $this->admin_form->form_close();
}
$this->admin_form->set_col(6,6);

// sms setting
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Cấu hình SMS');

echo $this->admin_form->input('Số điện thoại','meta[phone_report]', @$meta['phone_report'], 'Mỗi số điện thoại cách nhau bởi dấu ,', array('id'=>'phone_report','data-role'=>'tagsinput'));
echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();

// email setting
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Cấu hình Email');

echo $this->admin_form->input('Email','meta[mail_report]', @$meta['mail_report'], 'Mỗi mail cách nhau bởi dấu ,', array('id'=>'mail_report','data-role'=>'tagsinput'));
echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));

//domains setting
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Thêm Domains');

echo $this->admin_form->input('Domains','meta[domains_report]',@$meta['domains_report'],'Tab hoặc enter hoặc space để nhập thêm', array('id'=>'domains_report','data-role'=>'tagsinput'));
echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();

// Thông tin FTP
if(has_permission('Hosting.security.manage')) 
{
    echo $this->admin_form->form_open();
    echo $this->admin_form->box_open('Thông tin Hosting');

    //echo $this->admin_form->input('Hosting Admin Control Panel', 'meta[hosting_admin]' , get_term_meta_value($term_id, 'hosting_admin'))     ;
    echo $this->admin_form->input('Cpanel Username', 'meta[hosting_username]' , get_term_meta_value($term_id, 'hosting_username'))     ;
    echo $this->admin_form->input('Cpanel Password', 'meta[hosting_password]', get_term_meta_value($term_id, 'hosting_password'))      ;
    echo $this->admin_form->input('FTP Hostname', 'meta[hosting_ftp_hostname]', get_term_meta_value($term_id, 'hosting_ftp_hostname')) ;
    echo $this->admin_form->input('FTP Port', 'meta[hosting_ftp_port]', get_term_meta_value($term_id, 'hosting_ftp_port') ?: 21)       ;
    echo $this->admin_form->input('FTP Username', 'meta[hosting_ftp_username]', get_term_meta_value($term_id, 'hosting_ftp_username')) ;
    echo $this->admin_form->input('FTP Password', 'meta[hosting_ftp_password]', get_term_meta_value($term_id, 'hosting_ftp_password')) ;
    
    echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
    echo $this->admin_form->form_close();
}

?>

<script type="text/javascript">
  $(function(){
    $('[data-toggle=confirmation]').confirmation();
    $('#btn-ajax-reload').click(function(){
      $.ajax({url: '<?php echo module_url();?>ajax/setting/ga-reload',dataType: 'json', success: function(result){
        $('form > .form-group  select').eq(1).select2({
         allowClear: true,
         data: result
       });

        $('form > .form-group .select2-container').eq(1).css('width','100%');
      }});
    });


    $('#btn-ajax-checkservice').click(function(){
      $.ajax({url: '<?php echo module_url();?>ajax/setting/ga-reload',dataType: 'json', success: function(result){
         
alert('12312');
        // $('form > .form-group .select2-container').eq(1).css('width','100%');
      }});
    });


    // To make Pace works on Ajax calls
    $(document).ajaxStart(function() { Pace.restart(); });
    $('#start_process').click(function(){
      $(this).hide();
      $('#start_process_submit').prepend('<p class="help-block label label-info">Hệ thống đang gửi mail, vui lòng đợi trong giây lát.</p>');
        return true;
    });

  });
</script>