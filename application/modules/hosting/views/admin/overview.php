<?php
    
    $representative_gender = get_term_meta_value($term_id, 'representative_gender');
    $contract_begin        = get_term_meta_value($term_id, 'contract_begin');
    $contract_end          = get_term_meta_value($term_id, 'contract_end');
    $contract_region       = get_term_meta_value($term_id, 'contract_region');
    $contract_type         = get_term_meta_value($term_id, 'contract_type');
    $start_server_time     = get_term_meta_value($term_id, 'start_server_time') ;
    $start_server_time     = (!isset($start_server_time)) ? 'Chưa kích hoạt' : my_date($start_server_time, 'd/m/Y') ;

    $hosting_disk          = get_term_meta_value($term_id, 'hosting_disk');
    $hosting_bandwidth     = get_term_meta_value($term_id, 'hosting_bandwidth');
    $hosting_price         = get_term_meta_value($term_id, 'hosting_price') ;
    $hosting_email_webmail = get_term_meta_value($term_id, 'hosting_email_webmail') ;

    $sale_id               = get_term_meta_value($term_id,'staff_business');

    $staff_business        = 'Chưa có' ;
    if($sale_id) $staff_business        = $this->admin_m->get_field_by_id($sale_id,'display_name');

    $tech_kpis             = $this->hosting_kpi_m->select('user_id')
                                                 ->where('term_id',$term_id)
                                                 ->where('kpi_type','tech')
                                                 ->as_array()
                                                 ->get_many_by();
    $staffs_ids = array_column($tech_kpis, 'user_id');

    $technical_staffs       = 'Chưa có';
    if(!empty($staffs_ids)) 
    {
        foreach ($staffs_ids as $staff_id)
        {
            $user_name                             = $this->admin_m->get_field_by_id($staff_id,'display_name');        
            if($user_name)  $technical_staffs      = $user_name ?: $this->admin_m->get_field_by_id($staff_id,'user_mail');
        }
    }
?>
<div class="row">
    <div class="col-sm-6">
        <?php   $this->table->set_caption('Thông tin hợp đồng');
                $this->table->add_row(array('Mã hợp đồng',get_term_meta_value($term_id, 'contract_code')))
                            ->add_row('Gói:', '<b>'          . get_term_meta_value($term_id, 'hosting_disk')         . ' MB</b>')
                            ->add_row('Dung lượng', '<b>'    . get_term_meta_value($term_id, 'hosting_disk')         . ' MB</b>')
                            ->add_row('Băng thông', '<b>'    . get_term_meta_value($term_id, 'hosting_bandwidth')     . ' GB</b>')
                            ->add_row('Email/Webmail', '<b>' . get_term_meta_value($term_id, 'hosting_email_webmail') . '</b>')
                            ->add_row('Chi phí tháng', '<b>' . number_format(get_term_meta_value($term_id, 'hosting_price')) . ' VNĐ</b>')
                            ->add_row('Ngày bắt đầu', empty($contract_begin) ? $empty_chars : my_date($contract_begin,'d/m/Y'))
                            ->add_row('Ngày kết thúc', empty($contract_end) ? $empty_chars : my_date($contract_end,'d/m/Y'))
                            ->add_row('Ngày kích hoạt', $start_server_time)
                            ->add_row('Nhân viên kinh doanh', $staff_business)
                            ->add_row('Kỹ thuật thực hiện', @$technical_staffs) ;   
    echo $this->table->generate();
    ?>

    </div>
    <div class="col-sm-6">
         <?php $this->table->set_caption('Thông tin khách hàng');
                echo $this->table->add_row('Người đại diện',
      '<span id="incline-data">' .($representative_gender == 1 ? 'Ông ' : 'Bà ').str_repeat('&nbsp', 2).get_term_meta_value($term_id,'representative_name').'</span>')
                                ->add_row('Email',get_term_meta_value($term_id, 'representative_email'))
                                ->generate(); 
         ?>       
    </div>
</div> 

    


   