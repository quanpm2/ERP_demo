<?php $this->load->view('header'); ?>

<?php
   $start_time = get_term_meta_value($term->term_id,'contract_begin');
   $end_time   = get_term_meta_value($term->term_id,'contract_end');
   
   
   $is_expired = ($end_time < $time);
   $bg_color = ($is_expired) ? '#dd4b39' : '#ff9f00';

?>

<?php $content_style = 'font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;';?>
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid  #3498db">
                        <table width="600" class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                                    <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <tr>
                                             <!--Title-->
                                             <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Khởi chạy dịch vụ</td>
                                             <!--End title-->
                                             <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!--Space-->
                                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                       <tbody>
                                          <tr>
                                             <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left: 24px;">&nbsp;</p>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!--End Space-->
                                    <!--detail-->
                                    <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <tr>
                                             
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!--end detail-->
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <tr>
                                 <td>
                                    <!-- date -->
                                    <table style="border-radius:5px;" class="table1-3" bgcolor="#ecf0f1" width="183" border="0" align="left" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <tr>
                                             <td align="center">
                                                <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                   <tbody>
                                                      <tr>
                                                         <td height="10"></td>
                                                      </tr>
                                                      <tr>
                                                         <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:60px; color:#34495e;font-weight: bold;line-height: 48px;">
                                                            <?php //echo (($end_time < $time) ? '0' : floor(diffInDates($time,$end_time))); ?>
                                                            <?php echo get_term_meta_value($term->term_id, 'hosting_months') ; ?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:30px; color:#3498db; font-weight: bold;">THÁNG</td>
                                                      </tr>
                                                      <tr>
                                                         <td height="10"></td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- End date -->
                                    <!--Space-->
                                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                       <tbody>
                                          <tr>
                                             <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left:24px;">&nbsp;</p>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!--End Space-->
                                    <!--Content-->
                                    <table class="table3-1" width="342" border="0" align="right" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <tr>
                                             <td align="left" style="font-family: open sans, arial, sans-serif; font-size:15px; color:#999999; line-height:28px;">
                                                <b>Website:</b> <?php echo $term->term_name;?><br>
                                                <b>Ngày kết thúc:</b> <?php echo my_date($end_time,'d/m/Y');?><br>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!--End Content-->
                                 </td>
                              </tr>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <!--Content-->
                              <tr>
                                 <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999; line-height:28px;">                            
                                    <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          
                                          <?php if(!empty($staff)) { ?>
                                             <tr>
                                                <td>
                                                      <tr>
                                                         <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><span style="text-align: left">Kinh doanh   </span> <?php if(!empty($staff->display_name)) echo $staff->display_name; ?>: </td>
                                                         <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">
                                                         <?php
                                                            $phone  = '' ;
                                                            if(!empty($staff->user_id)) $phone = $this->usermeta_m->get_meta_value($staff->user_id,'user_phone');

                                                            if(!$phone) $phone = '(028) 7300. 4488';
                                                            
                                                            echo $phone; ?></td>
                                                      </tr>
                                                      <tr>
                                                         <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                         <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php if( !empty($staff->user_email)) echo $staff->user_email ?></td>
                                                      </tr>
                                                </td>
                                             </tr>
                                          <?php } ?>

                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Hotline Trung tâm Kinh doanh:</td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">(028) 7300. 4488</td>
                                          </tr>
                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">sales@webdoctor.vn</td>
                                          </tr>
                                          <tr>
                                             <td height="25"></td>
                                          </tr>
                                       </tbody>
                                    </table>                                  
                                 </td>
                              </tr>
                              <!--End Content-->
                              <tr>
                                 <td align="left" height="30"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td height="15"></td>
      </tr>
   </tbody>
</table>

<!-- thong tin hop dong -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin hợp đồng</td>
                                 <!--End title-->
                                 <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tbody>
                              <tr>
                                 <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--end detail-->
                     </td>
                  </tr>
                  <!--start Article-->
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <?php
                                 $service_date = my_date($start_time,'d/m/Y').' - '.my_date($end_time,'d/m/Y');
                                 $contract_date = my_date(get_term_meta_value($term->term_id,'contract_begin'),'d/m/Y').' - '.my_date(get_term_meta_value($term->term_id,'contract_end'),'d/m/Y');
                                 ?>
                              <?php 
                                 $rows = array();
                                 $rows[] = array('Mã hợp đồng', get_term_meta_value($term->term_id,'contract_code'));
                                 $rows[] = array('Thời gian hợp đồng', $contract_date);
                                 $rows[] = array('Thời gian thực hiện', $service_date);
                                 $rows[] = array('Gói', get_term_meta_value($term->term_id, 'hosting_disk')         . ' MB');
                                   $rows[] = array('Dung lượng',  get_term_meta_value($term->term_id, 'hosting_disk')         . ' MB');
                                   $rows[] = array('Băng thông',  get_term_meta_value($term->term_id, 'hosting_bandwidth')     . ' GB');
                                   $rows[] = array('Email/Webmail', get_term_meta_value($term->term_id, 'hosting_email_webmail'));
                                   $rows[] = array('Chi phí tháng',  number_format(get_term_meta_value($term->term_id, 'hosting_price')) . ' VNĐ');
                                   $rows[] = array('Chi phí', number_format( get_term_meta_value($term->term_id, 'hosting_months') * get_term_meta_value($term->term_id, 'hosting_price')) . 'VNĐ/' . get_term_meta_value($term->term_id, 'hosting_months') . ' tháng');
                                 
                                 foreach($rows as $i=>$row):
                                    ?>
                              <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                 </td>
                              </tr>
                              <?php endforeach; ?>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <!--end Article-->
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- ong tin hop dong -->

<!-- thong tin khach hang -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin khách hàng</td>
                                 <!--End title-->
                                 <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tbody>
                              <tr>
                                 <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--end detail-->
                     </td>
                  </tr>
                  <!--start Article-->
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <?php
                                 $service_date = my_date($start_time,'d/m/Y').' - '.my_date($end_time,'d/m/Y');
                                 $contract_date = my_date(get_term_meta_value($term->term_id,'contract_begin'),'d/m/Y').' - '.my_date(get_term_meta_value($term->term_id,'contract_end'),'d/m/Y');
                                 ?>
                              <?php 
                                 $rows = array();
                                 // $rows[] = array('Mã hợp đồng', get_term_meta_value($term->term_id,'contract_code'));
                                 // $rows[] = array('Thời gian hợp đồng', $contract_date);
                                 // $rows[] = array('Thời gian thực hiện', $service_date);
                                 
                                 $rows[] = array('Tên khách hàng', 'Anh/Chị '.get_term_meta_value($term->term_id,'representative_name'));
                                 $rows[] = array('Email',get_term_meta_value($term->term_id, 'representative_email'));
                                 $rows[] = array('Điện thoại di động', get_term_meta_value($term->term_id, 'representative_phone'));
                                 $rows[] = array('Địa chỉ', get_term_meta_value($term->term_id, 'representative_address'));
                                 
                                 foreach($rows as $i=>$row):
                                 	?>
                              <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                 </td>
                              </tr>
                              <?php endforeach; ?>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <!--end Article-->
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- thong tin khach hang -->
<?php $this->load->view('footer'); ?>