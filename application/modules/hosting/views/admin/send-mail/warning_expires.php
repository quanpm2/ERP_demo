<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <title><?php echo $title; ?></title>
  <style type="text/css">
    .ReadMsgBody { width: 100%; background-color: #ffffff; }
    .ExternalClass { width: 100%; background-color: #ffffff; }
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
    html { width: 100%; }
    body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
    table { border-spacing: 0; table-layout: fixed; margin: 0 auto; }
    table table table { table-layout: auto; }
    img { display: block !important; over-flow: hidden !important; }
    table td { border-collapse: collapse; }
    .yshortcuts a { border-bottom: none !important; }
    a { color: #3498db; text-decoration: none; }
    .footer-link a { color: #BDC3C7 !important; }

    @media only screen and (max-width: 640px) {
      body { width: auto !important; }
      table[class="table-inner"] { width: 90% !important; text-align: center !important; }
      table[class="table-full"] { width: 100% !important; text-align: center }
      table[class="table1-3"] { width: 30% !important; }
      table[class="table3-1"] { width: 64% !important; text-align: center !important; }
      *[class="hide"] { max-height: 0px !important; max-width: 0px !important; font-size: 0px !important; display: none !important; }
      /*image*/
      img[class="img1"] { width: 100% !important; height: auto !important; }
    }

    @media only screen and (max-width: 479px) {
      body { width: auto !important; }
      table[class="table-inner"] { width: 90% !important; }
      table[class="table-full"] { width: 100% !important; float: none !important; }
      table[class="table1-3"] { width: 100% !important; border: 0px !important; }
      table[class="table3-1"] { width: 100% !important; text-align: center !important; }
      *[class="hide"] { max-height: 0px !important; max-width: 0px !important; font-size: 0px !important; display: none !important; }
      /*image*/
      img[class="img1"] { width: 100% !important; height: auto !important; }
    }
  </style>
</head>
<?php $content_style = 'font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;';?>
<body>
  <!--header bar-->
  <table width="100%" bgcolor="#ecf0f1" align="center" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td bgcolor="#3498db" style="border-top:5px solid #43a8eb;">
        <table align="center" class="table-inner" width="600" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right">
              <!-- date -->
              <table class="table-full" align="right" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="35" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:12px; font-style:italic; color:#ffffff;"><?php echo date('d/m/Y');?></td>
                </tr>
              </table>
              <!-- end date -->
            </td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">
              <!-- Logo -->
              <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#3498db" class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;">
                <tr>
                  <td height="55" align="center" bgcolor="#3498db" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;padding-left: 15px;padding-right: 15px;" src="http://webdoctor.vn/images/logo.png" width="200" height="50" alt="Logo" /></td>
                  <td align="left" class="hide" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://webdoctor.vn/images/header_shape.png" width="25" alt="img" /></td>
                </tr>
              </table>
              <!-- End Logo -->
              <!--Space-->
              <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                <tr>
                  <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                    <p style="padding-left: 24px;">&nbsp;</p>
                  </td>
                </tr>
              </table>
              <!--End Space-->
              <!-- slogan -->
              <table border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="table-full">
                <tr>
                  <td height="55" style="font-family:Open sans,  Arial, sans-serif; font-size:14px; color:#7f8c8d;padding-left: 25px;padding-right: 25px;">CHĂM SÓC WEBSITE CHUYÊN NGHIỆP</td>
                </tr>
              </table>
              <!-- End slogan-->
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td height="15"></td>
    </tr>
  </table>
  <!--end header bar-->
  
<?php
   $start_time     = get_term_meta_value($term->term_id,'contract_begin');
   $end_time       = get_term_meta_value($term->term_id,'contract_end');
   
   $time_current                   = time() ;
   $time_current                   = $this->mdate->startOfDay($time_current) ;
   
   $extension_day                  = strtotime('+7 day', $end_time);
   $extension_day                   = $this->mdate->startOfDay($extension_day) ;
   
   $number_of_extension_days       = ($extension_day - $time_current)/(24*60*60) ;
   
   $is_expired = ($end_time < $time);
   $bg_color = ($is_expired) ? '#dd4b39' : '#ff9f00';

   $technical_name      = 'Chưa có' ;
   
   if( ! empty($techs) ) 
   {
      $technical_name                 = $techs['name'];
      $technical_email                = $techs['email'];
      $technical_phone                = $techs['phone'] ;    
   } 
   
   
   // $title_bar = ($is_expired) ? 'Thông báo thời hạn dịch vụ' : 'Cảnh báo thời hạn dịch vụ Cảnh báo hết hạn dịch vụ';
   $title_bar = 'Cảnh báo sắp đến thời gian gia hạn dịch vụ';
   ?>
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid <?php echo $bg_color;?>;">
                        <table width="600" class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" bgcolor="#ff0000" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;"><?php echo $title_bar ;?></td>
                                 <!--End title-->
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <tr>
                                 <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                    Xin chào quý khách.<br>
                                    <p><b>WebDoctor.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</b></p>
                                    <p><b>Chúng tôi xin thông báo về việc sắp đến hạn dịch vụ Lưu trữ Website (Hosting), quý khách vui lòng liên hệ nhân viên Kinh doanh của Webdoctor.vn để tiến hành gia hạn dịch vụ.</b></p>
                                 </td>
                              </tr>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <tr>
                                 <td>
                                    <!-- date -->
                                    <table style="border-radius:5px;" class="table1-3" bgcolor="#ecf0f1" width="183" border="0" align="left" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <tr>
                                             <td align="center">
                                                <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                   <tbody>
                                                      <tr>
                                                         <td align="center">
                                                            <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                               <tbody>
                                                                  <tr>
                                                                     <td height="10"></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:60px; color:#34495e;font-weight: bold;line-height: 48px;">
                                                                        <?php
                                                                        //echo (($end_time < $time) ? '0' : floor(diffInDates($time, $end_time)));
                                                                        echo get_term_meta_value($term_id, 'hosting_months') ;
                                                                        ?></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:30px; color:#3498db; font-weight: bold;">THÁNG</td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td height="10"></td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- End date -->
                                    <!--Space-->
                                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                       <tbody>
                                          <tr>
                                             <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left:24px;">&nbsp;</p>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!--End Space-->
                                    <!--Content-->
                                    <table class="table3-1" width="342" border="0" align="right" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <tr>
                                             <td align="left" style="font-family: open sans, arial, sans-serif; font-size:15px; color:#444444; line-height:28px;">
                                                <b>Website:</b> <?php echo $term->term_name;?><br>
                                                <b>Gói Hosting:</b> <?php echo number_format(get_term_meta_value($term->term_id, 'hosting_disk'), 0, '', '.') ; ?> Mb<br>
                                                <b>Ngày kết thúc:</b> <?php echo my_date($end_time,'d/m/Y');?><br>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!--End Content-->
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <!--Content-->
                              <tr>
                                 <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                    <p style="font-family: open sans, arial, sans-serif; font-size:15px">Để thực hiện gia hạn dịch vụ vui lòng liên hệ với chúng tôi theo thông tin dưới đây:</p>
                                    <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <?php if(!empty($staff)) { ?>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Kinh doanh <?= $staff->display_name?>: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php
                                                   $phone = $this->usermeta_m->get_meta_value($staff->user_id,'user_phone');
                                                   if(!$phone) $phone = '(028) 7300. 4488';
                                                   
                                                   echo $phone; ?></td>
                                             </tr>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?= $staff->user_email ?></td>
                                             </tr>
                                          <?php } ?>
                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Hotline Trung tâm Kinh doanh:</td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">(028) 7300. 4488</td>
                                          </tr>
                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">sales@webdoctor.vn</td>
                                          </tr>
                                          <!-- KỸ THUẬT THỰC HIỆN -->
                                          <?php if(!empty($techs)) { ?>
                                             <tr>
                                                <td>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Kỹ thuật <?php echo $technical_name ?>: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $technical_phone ; ?></td>
                                             </tr>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $technical_email ?></td>
                                             </tr>
                                             </td>
                                             </tr>
                                          <?php } ?>
                                          <tr>
                                             <td height="25"></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <p><b>Việc dịch vụ quá hạn không được gia hạn kịp thời có thể dẫn đến gián đoạn sử dụng dịch vụ của quý khách.</b></p>
                                    <p><i>Quý khách vui lòng bỏ qua thông báo này nếu đã thực hiện gia hạn dịch vụ.</i></p>
                                    <p style="text-align:right">CHÂN THÀNH CẢM ƠN</p>
                                 </td>
                              </tr>
                              <!--End Content-->
                              <tr>
                                 <td align="left" height="30"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td height="15"></td>
      </tr>
   </tbody>
</table>
<!-- thong tin hop dong -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin hợp đồng</td>
                                 <!--End title-->
                                 <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tbody>
                              <tr>
                                 <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--end detail-->
                     </td>
                  </tr>
                  <!--start Article-->
                  <tr>
                     <td bgcolor="#ffffff">
                        <!-- temp -->
                        <?php
                           $service_date = my_date($start_time,'d/m/Y').' - '.my_date($end_time,'d/m/Y');
                           $contract_date = my_date(get_term_meta_value($term->term_id,'contract_begin'),'d/m/Y').' - '.my_date(get_term_meta_value($term->term_id,'contract_end'),'d/m/Y');                         
                                        $rows[] = array('Chi phí tháng',  number_format(get_term_meta_value($term->term_id, 'hosting_price')) . ' VNĐ');
                                        $rows[] = array('Chi phí', number_format( get_term_meta_value($term->term_id, 'hosting_months') * get_term_meta_value($term->term_id, 'hosting_price')) . 'VNĐ/' . get_term_meta_value($term->term_id, 'hosting_months') . ' tháng');
                           
                           ?>
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Mã hợp đồng: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id,'contract_code') ;?>
                                 </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Thời gian hợp đồng: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo $contract_date ;?></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Gói: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id, 'hosting_disk') ?> MB                                 </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Dung lượng: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id, 'hosting_disk')?> MB                                 </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Băng thông: </td>
                                 <td width="300" align="left" style="font-family:o pen sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id, 'hosting_bandwidth') ;?> GB                                 </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email/Webmail: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id, 'hosting_email_webmail') ?>                                </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Chi phí tháng: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo number_format(get_term_meta_value($term->term_id, 'hosting_price')) ;?> VNĐ                                 </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Chi phí: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo number_format( get_term_meta_value($term->term_id, 'hosting_months') * get_term_meta_value($term->term_id, 'hosting_price')) . 'VNĐ/' . get_term_meta_value($term->term_id, 'hosting_months') . ' tháng' ;?></td>
                              </tr>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <!--end Article-->
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- ong tin hop dong -->
<!-- thong tin khach hang -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin khách hàng</td>
                                 <!--End title-->
                                 <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tbody>
                              <tr>
                                 <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--end detail-->
                     </td>
                  </tr>
                  <!--start Article-->
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Tên khách hàng: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Anh/Chị <?php echo get_term_meta_value($term->term_id,'representative_name')?></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id, 'representative_email') ;?></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Điện thoại di động: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id, 'representative_phone') ;?></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Địa chỉ: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo get_term_meta_value($term->term_id, 'representative_address') ;?></td>
                              </tr>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <!--end Article-->
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- thong tin khach hang -->
