<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Behaviour_m.php');

class Hosting_behaviour_m extends Behaviour_m {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
	}

	/**
	 * Preview data for printable contract version
	 *
	 * @return     String  HTML content
	 */
	public function prepare_preview()
	{
		if( ! $this->contract) return FALSE;

		parent::prepare_preview();

		$this->config->load('hosting/hosting');

		$data = $this->data;

		$contract = $this->get_contract();
		$service_package = get_term_meta_value($contract->term_id, 'service_package');
		$service_package = is_serialized($service_package) ? unserialize($service_package) : [];

		$data['price'] = $service_package['price'] ?? get_term_meta_value($contract->term_id, 'hosting_price');
		$data['month'] = $service_package['month'] ?? get_term_meta_value($contract->term_id, 'hosting_month');

		$data['service_package'] = $service_package;
		$data['discount_amount'] = (double) get_term_meta_value($contract->term_id, 'discount_amount');

		$spackage = get_term_meta_value($contract->term_id, 'hosting_service_package');

		$packages = $this->config->item('service','packages');
		$packages = array_filter($packages, function($x) use($spackage){ return ($x['status'] == 'active' || $x['name'] == $spackage); });
		$packages = wp_parse_args([$spackage=>$service_package], $packages);

		usort($packages, function($a, $b){ if(empty($a['disk']) || empty($b['disk'])) return FALSE; return $a['disk'] >= $b['disk']; });
		$data['spackage'] 	= $spackage;
		$data['packages'] 	= $packages;

		/* LIST SERVICE PACKAGE */
		$price_list = array();
		foreach ($packages as $key => $_package)
		{
			if(empty($_package['disk']) && $_package['name'] != $spackage) continue;

			$checked 	= $_package['name'] == $spackage ? '&#x2611;' : '&#x2610;';

			$disk 		= is_numeric($_package['disk']) ? currency_numberformat($_package['disk'], ' MB') : $_package['disk'];
			$bandwidth 	= $_package['bandwidth'] ?? '...';
			$bandwidth 	= is_numeric($bandwidth) ? currency_numberformat($bandwidth, ' lượt/tháng') : $bandwidth;
			$subdomain 	= $_package['sub_domain'] ?? '...';
			$cpm 		= currency_numberformat(($_package['price'] > 0 ? money_round($_package['price'],100) : 0), ' vnđ/tháng');
			$cpy 		= currency_numberformat(($_package['price'] > 0 ? money_round($_package['price']*12) : 0), ' vnđ/năm');

			$price_list[] = "{$checked} Dung lượng: {$disk} - Băng thông: {$bandwidth} - Subdomain: {$subdomain} - Chi phí: {$cpm} ({$cpy})";
		}
		
		$data['price_list'] = $price_list;
		$data['content'] 	= $this->load->view('hosting/contract/preview', $data ,TRUE);

		return $data;
	}
	
	/**
	 * Calculates the contract value.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The contract value.
	 */
	public function calc_contract_value()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$service_package = get_term_meta_value($this->get_contract()->term_id, 'service_package');
		$service_package = is_serialized($service_package) ? unserialize($service_package) : [];
		$price 	= $service_package['price'] ?? 0;
		$months = $service_package['month'] ?? 0;
		$total 	= $price*$months;

		$discount_percent 	= (int) get_term_meta_value($this->get_contract()->term_id, 'hosting_discount_percent');
		$buy2get1 			= (bool) get_term_meta_value($this->get_contract()->term_id, 'buy2get1');
		if( !empty($service_package['buy2get1']) && div($months, 12) >= 3) $discount_percent = div(100, 3);

		return ($total * (1-div($discount_percent, 100))) - (double) get_term_meta_value($this->get_contract()->term_id, 'discount_amount');
	}

    /**
     * Compute Actual Budget of Paid Payments
     * @return double Actual Budget
     */
    public function calc_payment_service_fee()
    {
        if( ! $this->contract) throw new \Exception('Property Contract undefined.');

        $stop_status = ['ending', 'liquidation'];
        if(!in_array($this->contract->term_status, $stop_status)) return 0;

        return parent::calc_payment_service_fee();
    }
}
/* End of file Hosting_behaviour_m.php */
/* Location: ./application/modules/hosting/models/Hosting_behaviour_m.php */