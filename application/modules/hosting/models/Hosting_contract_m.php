<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hosting_contract_m extends Base_contract_m {

	function __construct() 
	{
		parent::__construct();
		$this->load->model('hosting/hosting_m');
	}

	public function calc_contract_value($term_id = 0)
	{
		$term = $this->term_m->where('term_type','hosting')->get($term_id);

		if(empty($term)) return FALSE;

		$total 			 		  = 0;

		$hosting_price            = (int)get_term_meta_value($term->term_id,'hosting_price') ;
		$hosting_months           = (int)get_term_meta_value($term->term_id,'hosting_months');

		$hosting_cost 	 		  = $hosting_price * $hosting_months;
		$hosting_discount_percent = (int)get_term_meta_value($term->term_id,'hosting_discount_percent');

		if(!empty($hosting_discount_percent) && $hosting_discount_amound = ($hosting_cost * div($hosting_discount_percent,100)))
		{
			$hosting_cost-=$hosting_discount_amound;
		}

		$total+= $hosting_cost;
		return $total;
	}
	
	public function has_permission($term_id = 0, $name = '', $action = '',$kpi_type = '',$user_id = 0,$role_id = null)
	{
		$this->load->model('hosting_kpi_m');

		$permission = $name.'.'.$action;
		$permission = trim($permission, '.');
		
		if($this->permission_m->has_permission("{$name}.Manage",$role_id)
			&& $this->permission_m->has_permission($permission,$role_id)) 
			return TRUE;

		if(!$this->permission_m->has_permission($permission,$role_id)) return FALSE;

		$user_id = empty($user_id) ? $this->admin_m->id : $user_id;

		if($this->permission_m->has_permission('Hosting.sale.access',$role_id) 
			&& get_term_meta_value($term_id,'staff_business') == $user_id)
			return TRUE;

		$args = array(
			'term_id' => $term_id,
			'user_id' => $user_id);

		if(!empty($kpi_type))
		{
			$args['kpi_type'] = $kpi_type;
		}

		$kpis = $this->webgeneral_kpi_m->get_by($args);
		if(empty($kpis)) return FALSE;

		return TRUE;
	}

	public function start_service($term = null)
	{
		restrict('Hosting.start_service');	
		$this->load->model('hosting/hosting_report_m') ;
		if(empty($term) )
			return false;

		if(!$term || $term->term_type !='hosting')
			return false;
		$term_id = $term->term_id;
		$this->hosting_report_m->send_mail_info_to_admin($term_id);

		/***
		 * Khởi tạo Meta thời gian dịch vụ để thuận tiện cho tính năng sắp xếp và truy vấn
		 */
		$start_service_time = get_term_meta_value($term_id, 'start_service_time');
		empty($start_service_time) AND update_term_meta($term_id, 'start_service_time', 0);

		return true;
	}

	public function proc_service($term = FALSE)
	{
		if(!$term || $term->term_type != 'hosting') return FALSE;
		$term_id = $term->term_id;

		$staff_id            = get_term_meta_value($term_id, 'staff_business');    
        if( ! $staff_id ) $this->messages->info('Chưa có nhân viên kinh doanh');
     
        $tech_kpis           = $this->hosting_kpi_m->select('user_id')
                                                   ->where('term_id',$term_id)
                                                   ->where('kpi_type','tech')
                                                   ->as_array()
                                                   ->get_many_by(); 

        if(empty($tech_kpis)) $this->messages->info('Chưa có nhân viên kỹ thuật');
  
		$is_send = $this->hosting_report_m->send_mail_info_activated($term_id) ;

		if(FALSE === $is_send) return FALSE ;

		update_term_meta($term_id, 'start_service_time' , time());

		$this->log_m->insert(array(
						'log_type' =>'start_service_time',
						'user_id' => $this->admin_m->id,
						'term_id' => $term->term_id,
						'log_content' => date('Y/m/d H:i:s', time()). ' - Start Service Time'
					 ));

		return TRUE;
	}

	public function stop_service($term)
	{
		if(!$term || $term->term_type != 'hosting') return FALSE;
		$term_id = $term->term_id;
		
		$is_send = $this->hosting_report_m->send_mail_info_finish($term_id);
		if( FALSE === $is_send) return FALSE ;

		update_term_meta($term_id,'end_service_time', time());
		$this->contract_m->update($term_id, array('term_status'=>'ending'));
		$this->log_m->insert(array(
			'log_type' =>'end_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term_id,
			'log_content' => date('Y/m/d H:i:s', time()) . ' - End Service Time'
			));

        // Push queue sync service fee
        $this->load->config('amqps');
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        
        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];

        $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $channel     = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $payload = [
            'event' 	=> 'contract_payment.sync.service_fee',
            'contract_id' => $term_id
        ];
        $message = new \PhpAmqpLib\Message\AMQPMessage(
            json_encode($payload),
            array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $channel->basic_publish($message, '', $queue);	

        $channel->close();
        $connection->close();

		return TRUE;
	}


	public function renew($term_id = 0, $data = null)
	{
		$this->load->model('contract/contract_m') ;
		$this->load->model('hosting/hosting_kpi_m') ;
		$this->load->model('hosting/hosting_report_m');
		$this->config->load('hosting/hosting') ;
		$this->config->load('contract/contract');
		$hosting_contract_end = $data['hosting_contract_end'] ;
		$hosting_months 	  = $data['hosting_months'] ?? 12;		
		
		# Nhân bản hosting
		$term_renew_id = $this->clone($term_id);
		if(empty($term_renew_id)) return FALSE ;

		# Thông tin hợp đồng vừa clone
		$term 				  = $this->term_m->get($term_renew_id);
		
		# Cập nhật KPI cho HĐ mới
		$tech_kpis = $this->hosting_kpi_m->select('`term_id`, `user_id`, `kpi_type`, `kpi_value`, `kpi_datetime`, `result_value`')
										 ->where('term_id', $term_id)
										 ->where('kpi_type','tech')
										 ->as_array()
										 ->get_many_by();
		if(!empty($tech_kpis))
		{
			foreach($tech_kpis as $kpi)
			{
				$kpi['term_id'] = $term_renew_id;
				$this->hosting_kpi_m->insert($kpi);
			}
		}					  

		# Cập nhật lại thời gian
		$hosting_contract_end = $data['hosting_contract_end'] ;
		update_term_meta($term_renew_id, 'contract_begin', $hosting_contract_end);
		update_term_meta($term_renew_id, 'contract_end', strtotime( "+$hosting_months month", $hosting_contract_end)) ;
		update_term_meta($term_renew_id,'end_contract_time', '');

		# Cập nhật lại thông số

		$_contract = new contract_m();
		$_contract->set_contract($term);

		// contract codes
		update_term_meta($term_renew_id,'contract_code', $_contract->gen_contract_code());

		// % giảm giá hosting
		update_term_meta($term_renew_id,'hosting_discount_percent', 0);
		
		// cập nhật thông tin lại gói hosting
		$hosting_service_package = get_term_meta_value($term_renew_id, 'hosting_service_package') ;
		$services = $this->config->item('service','packages');
		if(!empty($services) && $hosting_service_package)
		{
			foreach ($services as $key => $service) 
			{
				if($key == 'hosting_orther') continue;

				if($key == $hosting_service_package)
				{
					// disk
					update_term_meta($term_renew_id,'hosting_disk', $service['disk']);

					// email_webmail
					update_term_meta($term_renew_id,'hosting_email_webmail', $service['email_webmail']);

					// price
					update_term_meta($term_renew_id,'hosting_price', $service['price']);

					// bandwidth
					update_term_meta($term_renew_id,'hosting_bandwidth', $service['bandwidth']);

					// bandwidth
					update_term_meta($term_renew_id,'hosting_months', $hosting_months);  

					// contract value 
					update_term_meta($term_renew_id,'contract_value', $hosting_months * $service['price']) ; 
				}
			}
		}

		// trạng thái
		$this->term_m->update($term_renew_id, array('term_status' => 'waitingforapprove'));
			
		# Lưu log
		$is_log = $this->log_m->insert(array(
					'log_type' => "clone_contract_hosting",
					'user_id'  => $this->admin_m->id,
					'term_id'  => $term_renew_id,
					'log_content' => date('Y/m/d H:i:s', time()) . '- Clone Hosting'
				  ));
		if($is_log == FALSE) return FALSE;

		# Kết thúc HĐ cũ
 		$result = $this->contract_m->stop_contract($term_id);
 		if(!$result) return FALSE ;

		// Tự động tạo các đợt thanh toán cho hợp đồng mới gia hạn
 		$term_renew = $this->hosting_m->set_term_type()->get($term_renew_id);
 		if(!empty($term_renew))
 		{
	 		parent::create_default_invoice($term_renew);
	 		parent::sync_all_amount($term_renew_id);
 		}

		return $term_renew_id;
	}

    /**
	 * Gửi email nhắc khách hàng gia hạn dịch vụ
	 *
	 * @param      <type>  $term   The term
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function notice_renew_service($term = FALSE)
	{
        if(!$term || $term->term_type != 'hosting') return FALSE;
		$term_id = $term->term_id;

		$this->load->model('hosting/hosting_report_m') ;
		$is_send = $this->hosting_report_m->send_noticefinish_mail($term_id);

		if(FALSE === $is_send) return FALSE ;

		return TRUE;
	}
}

/* End of file Hosting_contract_m.php */
/* Location: ./application/modules/hosting/models/Hosting_contract_m.php */