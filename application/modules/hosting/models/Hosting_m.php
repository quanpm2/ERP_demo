<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
require_once(APPPATH. 'modules/contract/models/Contract_m.php');

class Hosting_m extends Contract_m {

	public $term_type = 'hosting';

	function __construct() 
	{
		parent::__construct();
	}

	/**
	 * Sets the term type defined in model.
	 *
	 * @return     this
	 */
	public function set_term_type()
	{
		return $this->where('term_type',$this->term_type);
	}

	/**
	 * Determines if it has permission.
	 *
	 * @param      integer  $term_id   The term identifier
	 * @param      string   $name      The name
	 * @param      string   $action    The action
	 * @param      string   $kpi_type  The kpi type
	 * @param      integer  $user_id   The user identifier
	 * @param      <type>   $role_id   The role identifier
	 *
	 * @return     boolean  True if has permission, False otherwise.
	 */
	public function has_permission($term_id = 0, $name = '', $action = '',$kpi_type = '',$user_id = 0,$role_id = null)
	{
		$permission = $name.'.'.$action;
		$permission = trim($permission, '.');
		
		if($this->permission_m->has_permission("{$name}.Manage",$role_id)
			&& $this->permission_m->has_permission($permission,$role_id)) 
			return TRUE;

		if(!$this->permission_m->has_permission($permission,$role_id)) return FALSE;

		$user_id = empty($user_id) ? $this->admin_m->id : $user_id;

		if($this->permission_m->has_permission('Hosting.sale.access',$role_id) 
			&& get_term_meta_value($term_id,'staff_business') == $user_id)
			return TRUE;

		$args = array(
			'term_id' => $term_id,
			'user_id' => $user_id);

		if(!empty($kpi_type))
		{
			$args['kpi_type'] = $kpi_type;
		}

		$this->load->model('hosting_kpi_m');
		$kpis = $this->hosting_kpi_m->get_by($args);
		if(empty($kpis)) return FALSE;

		return TRUE;
	}

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function existed_check($value)
    {
        if (empty($value)) {
            return true;
        }
        
        $isExisted = $this->set_term_type()->where('term.term_id', (int) $value)->count_by() > 0;
        if (!$isExisted) {
            $this->form_validation->set_message('existed_check', 'Hợp đồng không tồn tại.');
            return false;
        }

        return true;
    }
}
/* End of file Hosting_m.php */
/* Location: ./application/modules/hosting/models/Hosting_m.php */