<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_hosting_m extends Base_model 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('api_user_m');
    }

    function get_contract_list_by($token = '')
    {
        $data = array();
        $token = $token ?: $this->api_user_m->auser_token ?? '';

        $services = array();
        $term_ids = array();

        $user_id = $this->api_user_m->user_id ?? 0;
        if($user_id)
        {
            $this->load->model('customer/customer_m');
            $customer = $this->customer_m->set_get_customer()->get($user_id);
            
            $this->load->model('term_users_m');
            $term_ids = $this->term_users_m
            ->where_in('term_status',array('pending','publish','ending','liquidation'))
            ->get_the_terms($user_id,'hosting');
        }

        if(empty($term_ids)) return FALSE;

        $terms = $this->term_m
        ->select('term_id,term_name,term_status')
        ->where_in('term_status',array('pending','publish','ending','liquidation'))
        ->where('term_type','hosting')
        ->where_in('term_id',$term_ids)
        ->get_many_by();

        if(empty($terms)) return FALSE;

        foreach ($terms as $term)
        {
            $term_id = $term->term_id;
            $status = 0;
            $description = 'HĐ đã hết hạn';
            $contract_code = get_term_meta_value($term->term_id,'contract_code');

            switch ($term->term_status) 
            {
                case 'ending':
                    $status = 2;
                    $description = 'HĐ đã hết hạn';
                    break;

                case 'liquidation':
                    $status = 0;
                    $description = 'HĐ bị khóa';
                    break;
                
                default:
                    $status = 1;
                    $description = 'HĐ còn hạn';
                    break;
            }

            $data[$term_id] = array(
                'id' => $term_id,
                'name' => $term->term_name.'-HĐ'.$term_id,
                'status' => $status,
                'description' => $description,
                'contract_code' => $contract_code,
                );
        }

        return $data;
    }

    function get_tasks_by($term_id)
    {
    	$data = array();

    	switch ($term_id) 
    	{
    		case 1:
    			# code...
    			break;
    		
    		case 2:
				# code...
    			break;
    	}

    	return $data;
    }

    function get_contract_info($term_id)
    {
        $data = array('sale'=>array(),'tech'=>array());

        switch ($term_id) {
            case 21:
                $data['sale'] = array('name' => 'hotline','phone' => '0873004488','email' => 'haitm@webdoctor.vn');
                $data['tech'] = array('name' => 'Triệu Minh Hải','phone' => '0873004488','email' => 'haitm@webdoctor.vn');
                break;
           
            default:
                # code...
                break;
        }

        return $data;
    }
}