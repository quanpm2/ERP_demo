<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Contract_wizard_m.php');

class Hosting_wizard_m extends Contract_wizard_m {

	function __construct()
	{
		parent::__construct();

		$models = array('hosting/hosting_m');
		$this->load->model($models);
	}


	/**
	 * Add Hook when in model change service detail
	 *
	 * @param      Array  $post   The post
	 */
	public function wizard_act_service($post)
	{
		parent::wizard_act_service($post);

		// Cập nhật các thông số chi tiết của hợp đồng hosting
		$this->hook->add_filter('ajax_act_service',	function($post){

			$metadata 	= $post['meta'] ?? [];
			$term_id 	= $post['edit']['term_id'] ?? 0;

			$this->config->load('hosting/hosting');
			$service_package_cfg 	= $this->config->item('service', 'packages');
			$spackage 				= $post['hosting_service_package'] ?? '';
			$service_package 		= $service_package_cfg[$spackage] ?? [];
			if( ! empty($metadata['service_package'][$spackage]))
			{
				$service_package = wp_parse_args($metadata['service_package'][$spackage], $service_package);
			}
			
			// Cập nhật thông số chi tiết dịch vụ
			update_term_meta($term_id, 'service_package', serialize($service_package));
			update_term_meta($term_id, 'hosting_service_package', $spackage);
			update_term_meta($term_id, 'hosting_months', $service_package['month']);
			update_term_meta($term_id, 'hosting_price', $service_package['price']);
			update_term_meta($term_id, 'discount_amount', $metadata['discount_amount']);

			// Cập nhật thời gian hợp đồng theo số năm đăng ký sử dụng dịch vụ
			$contract_begin = (int) get_term_meta_value($term_id, 'contract_begin');
			update_term_meta($term_id, 'contract_end', strtotime("+{$service_package['month']} months", $contract_begin));

			unset($post['hosting_service_package'], $post['meta']['service_package']);

			return $post;
		}, 9);

		// Cập nhật giá trị hợp đồng
		$this->hook->add_filter('ajax_act_service',	function($post){

			$term_id = $post['edit']['term_id'] ?? 0;

			try
			{
				$contract 		= $this->hosting_m->set_contract($term_id);
				$contract_value = $this->hosting_m->calc_contract_value();
				update_term_meta($term_id, 'contract_value', $contract_value);
			} 
			catch (Exception $e) { return $post; }

			return $post;
		}, 9);
	}
}
/* End of file Hosting_wizard_m.php */
/* Location: ./application/modules/contract/models/Hosting_wizard_m.php */