<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hosting_kpi_m extends Base_Model {

	public $_table = 'webgeneral_kpi';
	public $primary_key = 'kpi_id';
	public $before_create = array('delete_cache');
	public $after_update = array('delete_cache');
	public $before_delete = array('delete_cache');


	function __construct()
	{
		parent::__construct() ;
		$this->load->model('contract/base_contract_m');
		$this->load->model('hosting/hosting_contract_m')  ;
		$this->load->model('hosting/hosting_m')  ;		
	}
	function get_kpis($term_id = 0, $type = 'all', $kpi_type = 'content', $time = 0, $user_id = 0)
	{
		$key_cache = 'modules/hosting/get_kpi-'.$term_id;
		if($time == 0)
			$date = date('Y-m-01');
		else
			$date = date('Y-m-01',$time);


		if(($kpis = $this->scache->get($key_cache)) === FALSE)
		{
			$results = $this->order_by('kpi_datetime','asc')->get_many_by(array(
				'term_id' => $term_id
				));
			$kpis = array();
			$kpis['user_id'] = array();
			$kpis['users'] = array();
			$kpis['kpi_type'] = array();
			$kpis['kpi_date'] = array();
			foreach($results as $r)
			{
				if(!isset($kpis['user_id'][$r->user_id]))
				{
					$kpis['user_id'][$r->user_id] = array();
					$kpis['user_id'][$r->user_id]['total'] = 0;
				}
				if(!isset($kpis['user_id'][$r->user_id][$r->kpi_type][$r->kpi_datetime]))
				{
					if(!isset($kpis['user_id'][$r->user_id][$r->kpi_type]['total']))
					{
						$kpis['user_id'][$r->user_id][$r->kpi_type]['total'] = 0;
					}
					$kpis['user_id'][$r->user_id][$r->kpi_type][$r->kpi_datetime] = 0;
				}

				$kpis['user_id'][$r->user_id][$r->kpi_type][$r->kpi_datetime]+= $r->kpi_value;
				$kpis['user_id'][$r->user_id][$r->kpi_type]['total']+= $r->kpi_value;

				$kpis['users'][$r->kpi_type][$r->kpi_datetime][$r->user_id] = $r->kpi_value;
			}

			$this->scache->write($kpis,$key_cache);
		}
		$result = array();

		switch ($type) {
			case 'users':
				$result = $kpis['users'];
				(isset($result[$kpi_type])) AND $result = $result[$kpi_type];
				($time > 0 && isset($result[$date])) AND $result = $result[$date];
				($user_id > 0) AND $result = @$result[$user_id];

				break;
			
			default:
				$result = $kpis;
				break;
		}
	
		return $result;		
	}
	function get_kpi_value($term_id = 0, $type = 'all', $time = 0, $user_id = 0)
	{
		if($time == 0)
			$date = date('Y-m-01');
		else
			$date = date('Y-m-01',$time);

		$key_cache = 'modules/hosting/kpi-'.$term_id;
		if(($kpis = $this->scache->get($key_cache)) === FALSE)
		{
			$results = $this->order_by('kpi_datetime','asc')->get_many_by(array(
				'term_id' => $term_id
				));
			$kpis = array();
			$kpi_all = array();
			$kpis['all'] = array();
			$kpis['users'] = array();
			if($results)
			{
				foreach($results as $result)
				{
					if(!isset($kpis[$result->kpi_type]))
						$kpis[$result->kpi_type] = array();
					if(!isset($kpis[$result->kpi_type][$result->kpi_datetime]))
						$kpis[$result->kpi_type][$result->kpi_datetime] = 0;
					if(!isset($kpis['all'][$result->kpi_datetime]))
						$kpis['all'][$result->kpi_datetime] = 0;
	
					if(!isset($kpis['users'][$result->user_id][$result->kpi_type]))
					{
						$kpis['users'][$result->user_id][$result->kpi_type] = array();
						$kpis['users'][$result->user_id][$result->kpi_type]['total'] = 0;
					}
					
					!isset($kpis['users'][$result->user_id][$result->kpi_type][$result->kpi_datetime]) AND $kpis['users'][$result->user_id][$result->kpi_type][$result->kpi_datetime] = 0;

					$kpis[$result->kpi_type][$result->kpi_datetime]+= $result->kpi_value;
					$kpis['all'][$result->kpi_datetime]+= $result->kpi_value;

					$kpis['users'][$result->user_id][$result->kpi_type][$result->kpi_datetime]+= $result->kpi_value;
					$kpis['users'][$result->user_id][$result->kpi_type]['total']+= $result->kpi_value;
				}
			}
			$this->scache->write($kpis,$key_cache);
		}

		if($type == 'users')
		{
			return $kpis['users'];
		}
		if($user_id > 0)
		{
			if($time == 0)
				$date = 'total';
			return isset($kpis['users'][$user_id][$type][$date]) ? $kpis['users'][$user_id][$type][$date] : false;
		}

		if($time == 0 && isset($kpis[$type][$date]))
			return $kpis[$type];

		if($time > 0)
		{
			return isset($kpis[$type][$date]) ? $kpis[$type][$date] : 0;
		}

		return $kpis;
	}

	//các hàm này chưa có viết tối ưu
	function get_kpi_result($term_id = 0, $type = 'all', $time = 0,  $user_id = 0)
	{
		if($time == 0)
			$date = date('Y-m-01');
		else
			$date = date('Y-m-01',$time);

		$key_cache = 'modules/hosting/kpi-result-'.$term_id.'-'.$user_id;

		if(($kpis = $this->scache->get($key_cache)) === FALSE)
		{
			$where = array('term_id' => $term_id);
			if($user_id > 0)
			{
				$where['user_id'] = $user_id;
			}
			$results = $this->order_by('kpi_datetime','asc')->get_many_by($where);
			$kpis = array();
			$kpi_all = array();
			$kpis['all'] = array();
			if($results)
			{
				foreach($results as $result)
				{
					if(!isset($kpis[$result->kpi_type]))
						$kpis[$result->kpi_type] = array();

					if(!isset($kpis[$result->kpi_type]['total']))
						$kpis[$result->kpi_type]['total'] = 0;

					if(!isset($kpis[$result->kpi_type][$result->kpi_datetime]))
						$kpis[$result->kpi_type][$result->kpi_datetime] = 0;
					if(!isset($kpis['all'][$result->kpi_datetime]))
						$kpis['all'][$result->kpi_datetime] = 0;

					$kpis[$result->kpi_type]['total']+= $result->result_value;
					$kpis[$result->kpi_type][$result->kpi_datetime]+= $result->result_value;
					$kpis['all'][$result->kpi_datetime]+= $result->result_value;
				}
			}
			
			$this->scache->write($kpis,$key_cache);
		}

		if($time == 0 && isset($kpis[$type][$date]))
			return $kpis[$type];

		if($time > 0)
		{
			return isset($kpis[$type][$date]) ? $kpis[$type][$date] : 0;
		}

		return $kpis;
	}

	function update_kpi_value($term_id, $kpi_type, $kpi_value, $kpi_datetime, $user_id = 0)
	{
		$this->delete_cache($term_id);
		if($kpi_value <= 0)
			return false;
		if($user_id == 0)
		{
			$user_id = $this->admin_m->id;
		}
		$kpi_datetime = date('Y-m-01',$kpi_datetime);
		$update =array();
		$update['term_id'] = $term_id;
		$update['kpi_type'] = $kpi_type;
		$update['kpi_datetime'] = $kpi_datetime;
		$update['kpi_value'] = $kpi_value;
		$update['user_id'] = $user_id;
		$check = $this->where($update)->get_by();

		if(!$check)
		{
			$this->insert($update);
			$this->update_kpi_result($term_id, $kpi_type, 0, strtotime($kpi_datetime), $user_id);
			return true;
		}
		return true;
	}

	function update_kpi_result($term_id, $kpi_type, $kpi_result = 0, $kpi_datetime = '', $user_id = 0)
	{
		if(empty($kpi_datetime))
			$kpi_datetime = time();

		$kpi_datetime = $kpi_datetime;
		$start_month  = $this->mdate->startOfMonth($kpi_datetime);
		$end_month    = $this->mdate->endOfMonth($kpi_datetime);
		$end_date     = $this->mdate->endOfDay($end_month);

		$where = array(
			'kpi_type' => $kpi_type,
			'term_id' => $term_id,
			'kpi_datetime' => date('Y-m-01',$kpi_datetime)
			);

		if($user_id > 0)
		{
			$where['user_id'] = $user_id;
		} 

		$this->where($where);
		$this->update_by('term_id', $term_id, array('result_value' => $kpi_result));

		$key_cache = 'modules/hosting/kpi-result-'.$term_id.'-'. $user_id;
		$this->scache->delete($key_cache);
		return $kpi_result;
	}

	function delete_cache($term)
	{
		//trigger after_update
		$term_id = (isset($term[2][0]) && $term[2][0] == 'term_id') ? $term[2][1] : 0;

		if(is_array($term) && isset($term['term_id']))
			$term_id = $term['term_id'];
		else if(is_object($term) && isset($term->term_id))
			$term_id = $term->term_id;
		if(is_string($term) || is_numeric($term))
			$term_id = $term;
		$this->scache->delete_group('modules/hosting/kpi-'.$term_id);
		$this->scache->delete_group('modules/hosting/kpi-result-'.$term_id);
		$this->scache->delete_group('modules/hosting/get_kpi-'.$term_id);
		return $term;
	}

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function existed_check($value)
    {
        if (empty($value)) {
            return true;
        }
        
        $isExisted = $this->get($value);
        if (empty($isExisted)) {
            $this->form_validation->set_message('existed_check', 'Kpi không tồn tại.');
            return false;
        }

        return true;
    }
}

/* End of file Hosting_kpi_m.php */
/* Location: ./application/modules/hosting/models/Hosting_kpi_m.php */