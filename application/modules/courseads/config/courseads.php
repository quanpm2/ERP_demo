<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['course_status'] = array(
    'draft'         => 'Đang soạn',
    'pending'         => 'Chưa khai giảng',
    'publish'         => 'Đã khai giảng',
    'ending'        => 'Đã kết thúc',
    'remove'         => 'Hủy bỏ'
);

$config['course_type'] = array(
    'demo'         => 'Khóa học demo',
    'publish'     => 'Khóa học chính thức'
);

$config['register_status'] = array(
    'pending'             => 'Chưa thanh toán',
    'waitingforapprove'     => 'Chờ thanh toán',
    'publish'             => 'Đã thanh toán',
    'removing'             => 'Hủy bỏ',
);

$config['courses_list'] = [
    [
        'name'  => 'googleads-search',
        'title' => 'Khóa học Google Search Ads',
    ],
    [
        'name'  => 'facebookads',
        'title' => 'Khóa học Facebook Ads',
    ],
    [
        'name'  => 'googleads-search-chuyen-sau',
        'title' => 'Khóa học Google Search Ads Chuyên Sâu',
    ],
    [
        'name'  => 'facebookads-chuyen-sau',
        'title' => 'Khóa học Facebook Ads Chuyên Sâu',
    ],
    [
        'name'  => 'combo',
        'title' => 'Combo khoá học cơ bản'
    ],
    [
        'name'  => 'combo-chuyen-sau',
        'title' => 'Combo khoá học chuyên sâu'
    ],
    [
        'name'  => 'googleads-search-co-ban-chuyen-sau',
        'title' => 'Combo Google Search ads Ads Cơ bản và Chuyên sâu'
    ],
    [
        'name'  => 'combo',
        'title' => 'Combo Facebook Ads Cơ bản và Chuyên sâu',
    ],
];

$config['schedulers'] = array(
    'googleads-search' => array(
        'name'        => 'googleads-search',
        'title'       => 'Khóa học Google Search Ads',
        'description' => 'Thời lượng: 5 buổi học và 1 buổi thi',
        'items'       => [
            'Buổi 1: CẤU TRÚC TÀI KHOẢN VÀ XÂY DỰNG ĐỊNH HƯỚNG MỤC TIÊU BÁN HÀNG',
            'Buổi 2: XÂY DỰNG BỘ TỪ KHÓA VÀ MẪU QUẢNG CÁO HÚT CLICK',
            'Buổi 3: CÀI ĐẶT QUẢNG CÁO VÀ CHUYỂN ĐỔI ĐỂ ĐO LƯỜNG HIỆU QUẢ',
            'Buổi 4: ĐỌC BÁO CÁO VÀ TỐI ƯU CHIẾN DỊCH',
            'Buổi 5: TỐI ƯU HÓA TÀI KHOẢN QUẢNG CÁO',
            'Buổi 6: THI LẤY CHỨNG CHỈ QUỐC TẾ CỦA GOOGLE',
        ],
        'type'        => 'product'
    ),

    'facebookads' => array(
        'name'        => 'facebookads',
        'title'       => 'Khóa học Facebook Ads',
        'description' => 'Thời lượng: 5 buổi học',
        'items'       => [
            'Buổi 1: TỔNG QUAN FACEBOOK MARKETING',
            'Buổi 2: QUẢN LÝ VÀ BÁN HÀNG TRÊN FACEBOOK',
            'Buổi 3: THỰC HÀNH TẠO CHIẾN DỊCH QUẢNG CÁO',
            'Buổi 4: PHÂN TÍCH SỐ LIỆU VÀ TỐI ƯU QUẢNG CÁO',
            'Buổi 5: BÁO CÁO ĐỀ ÁN VÀ CHIA SẺ KINH NGHIỆM',
        ],
        'type'        => 'product'
    ),

    'googleads-search-chuyen-sau'     => array(
        'name'        => 'googleads-search-chuyen-sau',
        'title'       => 'Khóa học Google Search Ads Chuyên Sâu',
        'description' => 'Thời lượng: 3 buổi học',
        'items'       => [
            'Buổi 1: ÔN TẬP LẠI KIẾN THỨC VÀ CÀI ĐẶT CHUYỂN ĐỔI CHUYÊN SÂU',
            'Buổi 2: GOOGLE SHOPPING / GDN',
            'Buổi 3: ĐỌC BÁO CÁO VÀ TỐI ƯU CHIẾN DỊCH'
        ],
        'type'        => 'product'
    ),

    'facebookads-chuyen-sau'     => array(
        'name'        => 'facebookads-chuyen-sau',
        'title'       => 'Khóa học Facebook Ads Chuyên Sâu',
        'description' => 'Thời lượng: 3 buổi học',
        'items' => [
            'Buổi 1: THỰC HÀNH TẠO CHIẾN DỊCH QUẢNG CÁO',
            'Buổi 2: PHÂN TÍCH SỐ LIỆU VÀ TỐI ƯU QUẢNG CÁO',
            'Buổi 3: BÁO CÁO ĐỀ ÁN VÀ CHIA SẺ KINH NGHIỆM'
        ],
        'type'        => 'product'
    ),

    'combo' => array(
        'name'  => 'combo',
        'title' => 'Combo Khóa học cơ bản',
        'items' => ['googleads-search', 'facebookads'],
        'type'  => 'combo'
    ),

    'combo-chuyen-sau' => array(
        'name'  => 'combo-chuyen-sau',
        'title' => 'Combo Khóa học Chuyên sâu',
        'items' => ['googleads-search-chuyen-sau', 'facebookads-chuyen-sau'],
        'type'  => 'combo'
    ),

    'googleads-search-co-ban-chuyen-sau' => array(
        'name'  => 'combo',
        'title' => 'Khóa học Google Search Ads Cơ bản và Chuyên sâu',
        'items' => ['googleads-search', 'googleads-search-chuyen-sau'],
        'type'  => 'combo'
    ),

    'facebookads-co-ban-chuyen-sau' => array(
        'title' => 'Khóa học Facebook Ads Cơ bản và Chuyên sâu',
        'name'  => 'combo',
        'items' => ['facebookads', 'facebookads-chuyen-sau'],
        'type'  => 'combo'
    ),
);
