<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



$config['register_status'] = array(
  'pending'     => 'Chưa thanh toán',
  'publish'     => 'Đã thanh toán',
  'removing'     => 'Hủy bỏ',
);
