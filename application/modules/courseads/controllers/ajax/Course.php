<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends Admin_Controller {

	public $model = '';

	function __construct()
	{
		parent::__construct();
		$this->load->model('courseads/course_m');
		$this->load->model('courseads/courseads_contract_m');
		$this->load->model('courseads/courseads_m');
		$this->load->model('term_users_m');
	}

	public function create()
	{
		$response = array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		if( ! has_permission('Courseads.course.Add'))
		{
			$response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
			return parent::renderJson($response,500);
		}

		$edit = $this->input->post();
		if(empty($edit))
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ';
			return parent::renderJson($response,500);
		}

		$form_data = $this->course_m->validate($edit);
		if( ! $form_data)
		{
			$errors = $this->form_validation->error_array();
			$response['msg'] = implode('<br/>', $errors);
			$response['data']['errors'] = $errors;

			return parent::renderJson($response,500);
		}

		$form_data['edit']['post_type'] = 'course';
		$form_data['edit']['post_status'] = 'pending';

		$form_data['edit']['post_author'] 	= $this->admin_m->id;
		$form_data['edit']['created_on'] 	= time();

		// startBlock check thời gian khóa học có hợp lệ không
		$course_time = explode(' - ',$form_data['meta']['course_time']);
		if(count($course_time) == 1){
			$response['msg'] = 'Ngày tháng không hợp lệ !';
			return parent::renderJson($response,500);
		}
		if($this->checkValidateDate($course_time[0]) == 0)
		{
			$response['msg'] = 'Thời gian khóa học không hợp lệ !';
			return parent::renderJson($response,500);
		}
		if($this->checkValidateDate($course_time[1]) == 0)
		{
			$response['msg'] = 'Thời gian khóa học không hợp lệ !';
			return parent::renderJson($response,500);
		}
		$form_data['edit']['start_date'] 	= $this->mdate->startOfDay($course_time[0]);
		$form_data['edit']['end_date'] 	= $this->mdate->endOfDay($course_time[1]);
		//endBlock
		// startBlock check thời gian giảm giá
		$flag_voucher_time = FALSE; // flag = TRUE nếu tồn tại thời gian giảm giá
		if($form_data['meta']['voucher_time'])
		{
			$flag_voucher_time = TRUE;
			$voucher_time = explode(' - ',$form_data['meta']['voucher_time']);

			if(count($voucher_time) == 1){
				$response['msg'] = 'Ngày tháng không hợp lệ !';
				return parent::renderJson($response,500);
			}

			if($this->checkValidateDate($voucher_time[0]) == 0)
			{
				$response['msg'] = 'Ngày giảm giá không hợp lệ !';
				return parent::renderJson($response,500);
			}
			if($this->checkValidateDate($voucher_time[1]) == 0)
			{
				$response['msg'] = 'Ngày giảm giá không hợp lệ !';
				return parent::renderJson($response,500);
			}

			$form_data['meta']['voucher_start'] = $this->mdate->startOfDay($voucher_time[0]);
			$form_data['meta']['voucher_end'] = $this->mdate->endOfDay($voucher_time[1]);

			if($form_data['meta']['voucher_end'] > $form_data['edit']['start_date']){
				$response['msg'] = 'Ngày giảm giá phải kết thúc trước khi khóa học bắt đầu !';
				return parent::renderJson($response,500);
			}
		}
		// endBlock

		//	startBlock check giá trị giảm giá
		$flag_voucher_value = FALSE; // flag = TRUE nếu tồn tại giá trị giảm giá
		if($flag_voucher_time == TRUE){
			if($form_data['meta']['voucher_value']){
				$flag_voucher_value = TRUE;

				// Check giá trị giảm giá nhỏ hơn 100%
				if($form_data['meta']['voucher_value'] > $form_data['meta']['course_value']){
					$response['msg'] = 'Giá trị giảm giá không hợp lệ !';
					return parent::renderJson($response,500);
				}
			}

			if($flag_voucher_value ==FALSE){
				$response['msg'] = 'Giá trị giảm giá không được để trống !';
				return parent::renderJson($response,500);
			}
		}
		// endBlock

		// startBlock check só lượng giảm giá
		$flag_voucher_qty = FALSE; // flag = TRUE nếu tồn tại số lượng giảm giá
		if($flag_voucher_value == TRUE){
			if($form_data['meta']['voucher_qty']){
				$flag_voucher_qty = TRUE;
			}

			if($flag_voucher_qty == FALSE){
				$response['msg'] = 'Số lượng giảm giá không được để trống !';
				return parent::renderJson($response,500);
			}
		}
		// endBlock

		// $time_course = $form_data['meta']['day_in_week'];
		// $time_start = $form_data['meta']['course_start_day_time'];
		// $time_end = $form_data['meta']['course_end_day_time'];
		// $time_array = array();
		// foreach ($time_course as $key => $value) {
		// 	if(strtotime($time_start[$value]) + 1800 > strtotime($time_end[$value])){
		// 		$response['msg'] = 'Giờ bắt đầu học không hợp lệ !';
		// 		return parent::renderJson($response,500);
		// 	}
		// 	$time_array[$value]['time_start'] = $time_start[$value];
		// 	$time_array[$value]['time_end'] = $time_end[$value];
		// }
		// $days = $this->generateCourseDays($form_data['edit']['start_date'],$form_data['edit']['end_date'],$form_data['meta']['day_in_week'],$form_data['meta']['total_course_time'],$time_array);

		// $form_data['meta']['course_days'] = serialize($days);
		// $form_data['meta']['day_in_week'] = serialize($time_array);
		$insert_id = $this->post_m->insert($form_data['edit']);
		// unset($form_data['meta']['course_start_day_time']);
		// unset($form_data['meta']['course_end_day_time']);
		if(!empty($form_data['meta']))
		{
			foreach ($form_data['meta'] as $key  => $value)
			{
				if($key != 'course_time' && $key != 'voucher_time')
				{
					update_post_meta($insert_id,$key,$value);
				}

			}
		}

		// $form_data['term_users']['user_id'] = $this->admin_m->id;
		// $form_data['term_users']['term_id'] = $insert_id;
		// $this->term_users_m->insert($form_data['term_users']);
		$response['data']['insert_id']		= $insert_id;
		$response['data']['display_name']	= $form_data['edit']['post_title'] ?: '';
		$response['msg'] 		= 'Thêm mới dữ liệu thành công !';
		$response['success'] 	= TRUE;

		return parent::renderJson($response);
	}

	public function update()
	{
		$response = array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		if( ! has_permission('Courseads.course.Update'))
		{
			$response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
			return parent::renderJson($response,500);
		}

		$edit = $this->input->post();
		if(empty($edit))
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ';
			return parent::renderJson($response,500);
		}

		$form_data = $this->course_m->validate($edit['edit']);
		if( ! $form_data)
		{
			$errors = $this->form_validation->error_array();
			$response['msg'] = implode('<br/>', $errors);
			$response['data']['errors'] = $errors;

			return parent::renderJson($response,500);
		}

		if($form_data['edit']['post_status'] == 'remove'){
			if( ! has_permission('Courseads.course.Delete'))
			{
				$response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
				return parent::renderJson($response,500);
			}
		}

		// startBlock check thời gian khóa học có hợp lệ không
		$course_time = explode(' - ',$form_data['meta']['course_time']);
		if(count($course_time) == 1){
			$response['msg'] = 'Ngày tháng không hợp lệ !';
			return parent::renderJson($response,500);
		}
		if($this->checkValidateDate($course_time[0]) == 0)
		{
			$response['msg'] = 'Thời gian khóa học không hợp lệ !';
			return parent::renderJson($response,500);
		}
		if($this->checkValidateDate($course_time[1]) == 0)
		{
			$response['msg'] = 'Thời gian khóa học không hợp lệ !';
			return parent::renderJson($response,500);
		}
		$form_data['edit']['start_date'] 	= $this->mdate->startOfDay($course_time[0]);
		$form_data['edit']['end_date'] 	= $this->mdate->endOfDay($course_time[1]);
		//endBlock

		$form_data['edit']['updated_on'] 	= time();

		// startBlock check thời gian giảm giá
		$flag_voucher_time = FALSE; // flag = TRUE nếu tồn tại thời gian giảm giá
		if($form_data['meta']['voucher_time'])
		{
			$flag_voucher_time = TRUE;
			$voucher_time = explode(' - ',$form_data['meta']['voucher_time']);

			if(count($voucher_time) == 1){
				$response['msg'] = 'Ngày tháng không hợp lệ !';
				return parent::renderJson($response,500);
			}

			if($this->checkValidateDate($voucher_time[0]) == 0)
			{
				$response['msg'] = 'Ngày giảm giá không hợp lệ !';
				return parent::renderJson($response,500);
			}
			if($this->checkValidateDate($voucher_time[1]) == 0)
			{
				$response['msg'] = 'Ngày giảm giá không hợp lệ !';
				return parent::renderJson($response,500);
			}

			$form_data['meta']['voucher_start'] = $this->mdate->startOfDay($voucher_time[0]);
			$form_data['meta']['voucher_end'] = $this->mdate->endOfDay($voucher_time[1]);

			if($form_data['meta']['voucher_end'] > $form_data['edit']['start_date']){
				$response['msg'] = 'Ngày giảm giá phải kết thúc trước khi khóa học bắt đầu !';
				return parent::renderJson($response,500);
			}
		}
		// endBlock

		//	startBlock check giá trị giảm giá
		$flag_voucher_value = FALSE; // flag = TRUE nếu tồn tại giá trị giảm giá
		if($flag_voucher_time == TRUE){
			if($form_data['meta']['voucher_value']){
				$flag_voucher_value = TRUE;

				// Check giá trị giảm giá nhỏ hơn 100%
				if($form_data['meta']['voucher_value'] > $form_data['meta']['course_value']){
					$response['msg'] = 'Giá trị giảm giá không hợp lệ !';
					return parent::renderJson($response,500);
				}
			}

			if($flag_voucher_value ==FALSE){
				$response['msg'] = 'Giá trị giảm giá không được để trống !';
				return parent::renderJson($response,500);
			}
		}
		// endBlock

		// startBlock check só lượng giảm giá
		$flag_voucher_qty = FALSE; // flag = TRUE nếu tồn tại số lượng giảm giá
		if($flag_voucher_value == TRUE){
			if($form_data['meta']['voucher_qty']){
				$flag_voucher_qty = TRUE;
			}

			if($flag_voucher_qty == FALSE){
				$response['msg'] = 'Số lượng giảm giá không được để trống !';
				return parent::renderJson($response,500);
			}
		}
		// endBlock
		$time_course = $form_data['meta']['day_in_week'];
		$time_start = $form_data['meta']['course_start_day_time'];
		$time_end = $form_data['meta']['course_end_day_time'];
		$time_array = array();
		foreach ($time_course as $key => $value) {
			if(strtotime($time_start[$value]) + 1800 > strtotime($time_end[$value])){
				$response['msg'] = 'Giờ bắt đầu học không hợp lệ !';
				return parent::renderJson($response,500);
			}
			$time_array[$value]['time_start'] = $time_start[$value];
			$time_array[$value]['time_end'] = $time_end[$value];
		}
		$days = $this->generateCourseDays($form_data['edit']['start_date'],$form_data['edit']['end_date'],$form_data['meta']['day_in_week'],$form_data['meta']['total_course_time'],$time_array);

		$form_data['meta']['course_days'] = serialize($days);
		$form_data['meta']['day_in_week'] = serialize($time_array);
		unset($form_data['meta']['course_start_day_time']);
		unset($form_data['meta']['course_end_day_time']);
		$this->post_m->update($form_data['meta']['course_id'],$form_data['edit']);
		// unset($form_data['meta']['course_start_day_time']);
		// unset($form_data['meta']['course_end_day_time']);
		if(!empty($form_data['meta']))
		{
			foreach ($form_data['meta'] as $key  => $value)
			{
				if($key!='course_name' && $key != 'course_time' && $key != 'voucher_time')
				{
					update_post_meta($form_data['meta']['course_id'],$key,$value);
				}

			}
		}
		$response['data']['insert_id']		= $form_data['meta']['course_id'];
		$response['data']['display_name']	= $form_data['edit']['post_title'] ?: '';
		$response['msg'] 		= 'Cập nhập dữ liệu thành công !';
		$response['success'] 	= TRUE;

		return parent::renderJson($response);
	}

	public function getVoucher(){
		$id = $this->input->post('course_id');
		$term = $this->course_m->set_post_type()->get($id);
		$data['course_code'] = $term->post_name;
		$created_display_name = $this->admin_m->get_field_by_id($this->admin_m->id,'display_name');
		$name = explode(' ',$created_display_name);
		$data['name'] = create_slug(strtolower($name[count($name)-1]));
		$code = $data['name'] . '-'. $data['course_code'];
		$voucher[$code]['created_by'] = $this->admin_m->id;
		$voucher[$code]['created_on'] = time();
		$voucher[$code]['qty'] = get_post_meta_value($id, 'voucher_qty');
		$list_vouchers = get_post_meta_value($id, 'voucher_by_user');
		if($list_vouchers == null)
		{
				update_post_meta($id,'voucher_by_user',serialize($voucher));
		}else{
				$list_vouchers = unserialize($list_vouchers);
				if(array_key_exists($code,$list_vouchers) == 0){
					$list_vouchers[$code] = $voucher[$code];
					update_post_meta($id,'voucher_by_user',serialize($list_vouchers));
				}else{
					$response['code'] = $code;
					$response['qty'] = $list_vouchers[$code]['qty'];
					$response['msg'] 		= 'Bạn đã lấy code khóa học này !';
					$response['success'] 	= FALSE;
					return parent::renderJson($response);
				}
		}

		$response['code'] = $code;
		$response['qty'] = $voucher[$code]['qty'];
		$response['msg'] 		= 'Lấy mã giảm giá thành công !';
		$response['success'] 	= TRUE;
		return parent::renderJson($response);
	}

	/*
	*	function generate course days
	*   @param (string) $course_start is  course start day
	*   @param (string) $course_end is course end day
	*   @param (array) $day_in_week is day in week
	*   @param (int) $total_course_days is total course days
	*   @param (string) $time_start is course start time in course day
	*   @param (string) $time_end is course end time in course day
	*
	*   return (array) days is date of course
	*/
	private function generateCourseDays($course_start,$course_end,$day_in_week,$total_course_days,$time_array){
		$i=0;
		$temp_time = array();
		while ($course_start < $course_end) {
			foreach ($day_in_week as $key => $value) {
				if($i<$total_course_days*2){
					if(strtotime(date("Y-m-d",strtotime(date("Y-m-d",$course_start).' '.$value))) < $course_end){
						$temp_time[strtotime(date("Y-m-d",$course_start).' '.$value)] = $time_array[$value];
						$i++;
					}
				}
				else{
					break;
				}
			}
			$course_start = strtotime('+1 week',$course_start);
		}
		ksort($temp_time);

		$j=0;
		$days = array();
		foreach ($temp_time as $key1 => $value1) {
			if($j<$total_course_days){
				$day = array();
				$day['day'] = $key1;
				$day['status'] = 0;
				$day['time_start'] = $value1['time_start'];
				$day['time_end'] = $value1['time_end'];
				$days[] = $day;
				$j++;
			}
		}
		return $days;
	}

	/*
	*	function check validate date
	*   @param (string) $date is  date
	*
	*   return (boolean):  0 not is date, 1 is date
	*/

	private function checkValidateDate($date){
		$check_voucher_time = explode('-',$date);

		if(count($check_voucher_time) != 3) return 0;

		if(is_numeric($check_voucher_time[0]) != TRUE && is_numeric($check_voucher_time[1]) != TRUE && is_numeric($check_voucher_time[2]) != TRUE ) return 0;

		if(checkdate($check_voucher_time[1],$check_voucher_time[0],$check_voucher_time[2]) == FALSE) return 0;

		return 1;
	}

	public function index()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);

        if (!has_permission('Courseads.course.Access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $defaults = ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args = wp_parse_args($this->input->get(), $defaults);

        $data = $this->data; // remove-after

        $relate_users = $this->admin_m->get_all_by_permissions('Courseads.course.Access');

        $this->load->library('datatable_builder');
        $this->load->config('courseads');
        $this->load->config('course_contract');
        $this->load->model('courseads/courseads_contract_m');
        $this->load->model('term_posts_m');
        /* Applies get query params for filter */
        $this->search_filter();

        $this->datatable_builder->set_filter_position(FILTER_TOP_OUTTER)
            ->select('posts.post_id,posts.post_name,posts.post_author,posts.created_on,posts.start_date,posts.end_date,posts.post_status')
            ->add_search('post_name', ['placeholder' => 'Mã khóa học'])
            ->add_search('posts.post_title', ['placeholder' => 'Tên khóa học'])
            ->add_search('created_on', ['placeholder' => 'Ngày tạo', 'class' => 'form-control input_daterange'])
            ->add_search('start_date', ['placeholder' => 'Ngày bắt đầu khóa học', 'class' => 'form-control input_daterange'])
            ->add_search('end_date', ['placeholder' => 'Ngày kết thúc khóa học', 'class' => 'form-control input_daterange'])
            ->add_search('teacher_id', ['placeholder' => 'Giảng viên phụ trách', 'class' => 'form-control'])
            ->add_search('post_author', ['placeholder' => 'Người tạo'])
            ->add_search('course_type', array('content' => form_dropdown(array('name' => 'where[course_type]', 'class' => 'form-control'), prepare_dropdown($this->config->item('course_type'), 'Loại khóa học'), $this->input->get('where[course_type]'))))
            ->add_search('post_status', array('content' => form_dropdown(array('name' => 'where[post_status]', 'class' => 'form-control'), prepare_dropdown($this->config->item('course_status'), 'Trạng thái khóa học'), $this->input->get('where[post_status]'))))
            ->add_column('post_name', array('set_select' => FALSE, 'title' => 'Mã Khóa học', 'set_order' => TRUE))
            ->add_column('course_daterange', array('set_select' => FALSE, 'title' => 'T/G HĐ'))
            ->add_column('course_value', array('set_select' => FALSE, 'title' => 'GTKH'))
            ->add_column('total_student', array('set_select' => FALSE, 'title' => 'Tỷ lệ học viên'))
            ->add_column('post_status', 'Trạng thái khóa học')
            ->add_column('course_type', array('set_select' => FALSE, 'title' => 'Loại khóa học'))
            ->add_column('teacher_id', array('set_select' => FALSE, 'title' => 'Giảng viên phụ trách', 'set_order' => FALSE))
            ->add_column('action', array('set_select' => FALSE, 'title' => 'Actions', 'set_order' => FALSE))
            ->add_column_callback('post_id', function ($data, $row_name) {

                $post_id = $data['post_id'];
                $course_code = $data['post_name'];
                $created_by = $data['post_author'];
                /*
                * Mã khóa học
                */
                // $course_code = get_term_meta_value($term_id, 'course_code') ? : '<code>Không có mã khóa học</code>';

                if (has_permission('Courseads.course.Update') || has_permission('Courseads.course.Add')) {
                    $course_code = '<a title="Cập nhật khóa học" style="cursor: pointer;" onclick="showEdit(' . $post_id . ')">' . $course_code . '</a>';
                }
                $course_code = "<b>{$course_code}</b></br/>";
                /*
                * Người tạo
                */
                if ($created_by) {
                    $created_display_name = $this->admin_m->get_field_by_id($created_by, 'display_name');
                    $course_code .= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Người tạo'><i class='fa fa-fw fa-user'></i>Tạo bởi :{$created_display_name}</span>";
                }
                /*
                * Ngày tạo
                */
                if ($data['created_on']) {
                    $created_datetime = my_date($data['created_on'], 'd/m/Y');
                    $course_code .= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Ngày tạo'><i class='fa fa-fw  fa-clock-o'></i>Ngày tạo :{$created_datetime}</span>";
                }
                $data['post_name'] = $course_code;
                /*
                * Thời gian khóa học(từ ngày - đến ngày)
                */
                $course_daterange = '<i class="fa fa-fw fa-play" style="color:#72d072;font-size:0.8em"></i> ' . ($data['start_date'] ? my_date($data['start_date']) : '--') . br();
                $course_daterange .= '<i class="fa fa-fw fa-stop" style="color: #bb7171;font-size:0.8em"></i> ' . ($data['end_date'] ? my_date($data['end_date']) : '--');
                $data['course_daterange'] = $course_daterange;
                /*
                * Giá trị khóa học
                */
                $course_value = get_post_meta_value($post_id, 'course_value');
                $data['course_value'] = currency_numberformat($course_value, 'đ');

                /*
                * Loại khóa học
                */
                $data['_term_type'] = get_post_meta_value($post_id, 'course_type');
                $data['course_type'] = $this->config->item($data['_term_type'], 'course_type');
                $class_status ='';
                if($data['_term_type'] == 'demo') $class_status = 'label label-info';
                if($data['_term_type'] == 'publish') $class_status = 'label label-success';
                $data['course_type'] = "<span class='{$class_status}'>{$data['course_type']}</span>";
                /*
                * Trạng thái khóa học
                */
                $data['_term_status'] = $data['post_status'];
                $class_status ='';
                if($data['_term_status'] == 'pending') $class_status = 'label label-info';
                if($data['_term_status'] == 'publish') $class_status = 'label label-success';
                if($data['_term_status'] == 'ending') $class_status = 'label label-default';
                if($data['_term_status'] == 'remove') $class_status = 'label label-danger';
                $data['term_status'] = $this->config->item($data['post_status'], 'course_status');
                $data['post_status'] = "<span class='{$class_status}'>{$data['term_status']}</span>";
               
               	/*
                * Tỷ lệ học viên
                */
               	$student_qty = (int) get_post_meta_value($post_id, 'student_qty');
               	$num_customers = $this->course_m->count_customers($post_id);

               	$register_list = (int) get_post_meta_value($post_id, 'register_list');
               	$num_contracts = $this->course_m->count_contracts($post_id);

           		$total_student = "<span class='text-muted' data-toggle='tooltip' title='Tỷ lệ học viên'><i class='fa fa-fw fa-user'></i>Học viên : {$num_customers}/{$student_qty}</span>";
                $total_student .= "<br/><span class='text-muted' data-toggle='tooltip' title='Số lượng đăng ký'><i class='fa fa-fw fa-user'></i>Đăng ký : {$num_contracts}</span>";
                $data['total_student'] = $total_student;

                /*
                * Giảng viên phụ trấch
                */
                if ($teacher_id = get_post_meta_value($post_id, 'teacher_id'))
                {
                    $data['teacher_id'] = $this->admin_m->get_field_by_id($teacher_id, 'display_name');
                }

                /*
                * Hành động
                */
                if (has_permission('Courseads.course.Update')) {
                    $data['action'] = '<a title="Cập nhật khóa học" class="btn btn-default btn-xs" onclick="showEdit(' . $post_id . ')"><i class="fa fa-fw fa-edit"></i></a>';
                }
                if (has_permission('Courseads.contract.Access')) {
                    $data['action'] .= '<a title="Trang danh sách phiếu đăng ký" class="btn btn-default btn-xs" href="' . admin_url('courseads?course_id=' . $post_id) . '"><i class="fa fa-fw fa-book"></i></a>';
                }
                
                return $data;
            }, FALSE)
            ->from('posts')
            ->where('post_type', 'course');

        $status_list = array_keys($this->config->item('course_status'));
        array_unshift($status_list, 1);

        $this->datatable_builder
            ->where_in('post_status', $status_list)
            ->where_in('post_type', 'course')
            ->group_by('posts.post_id');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        $this->template->title->set('Quản lý khóa học');
        $this->template->description->set('Trang danh sách tất cả các khóa học');
        return parent::renderJson($data);
    }

    protected function search_filter($search_args = array())
    {
        $args = $this->input->get();

        if (!empty($args['where']))
            $args['where'] = array_map(function ($x) {
                return trim($x);
            }, $args['where']);


        // course_daterange FILTERING & SORTING

        $filter_course_daterange = $args['where']['course_daterange'] ?? FALSE;
        $sort_course_daterange = $args['order_by']['course_daterange'] ?? FALSE;
        if ($filter_course_daterange || $sort_course_daterange) {
            $alias = uniqid('course_daterange_');
            if ($sort_course_daterange) {
                $this->datatable_builder->order_by("start_date", $sort_course_daterange);
                unset($args['order_by']['course_daterange'],$args['where']['course_daterange']);
            }
        }

        // course_code FILTERING & SORTING
        $filter_course_code = $args['where']['post_name'] ?? FALSE;
        $sort_course_code = $args['order_by']['course_code'] ?? FALSE;
        if ($filter_course_code || $sort_course_code) {
            if ($filter_course_code) {
                $this->datatable_builder->like("post_name", $filter_course_code);
            }

            if ($sort_course_code) {
                $this->datatable_builder->order_by("(post_name * 1)", $sort_course_code);
            }

            unset($args['where']['post_name'], $args['order_by']['post_name']);
        }

        // start_date FILTERING & SORTING
        $filter_course_start = $args['where']['start_date'] ?? FALSE;
        $sort_course_start = $args['order_by']['start_date'] ?? FALSE;
        if ($filter_course_start || $sort_course_start) {
            if ($filter_course_start) {
                $dates = explode(' - ', $filter_course_start);
                $this->datatable_builder->where("start_date >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("start_date <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['start_date']);
            }

            if ($sort_course_start) {
                $this->datatable_builder->order_by("start_date", $sort_course_start);
                unset($args['order_by']['start_date']);
            }
        }

        // course_end FILTERING & SORTING
        $filter_course_end = $args['where']['end_date'] ?? FALSE;
        $sort_course_end = $args['order_by']['end_date'] ?? FALSE;
        if ($filter_course_end || $sort_course_end) {
            if ($filter_course_end) {
                $dates = explode(' - ', $filter_course_end);
                $this->datatable_builder->where("end_date >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("end_date <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['end_date']);
            }

            if ($sort_course_end) {
                $this->datatable_builder->order_by("end_date", $sort_course_end);
                unset($args['order_by']['end_date']);
            }
        }
        
        // Created_on FILTERING & SORTING
        $filter_created_on = $args['where']['created_on'] ?? FALSE;
        $sort_created_on = $args['order_by']['created_on'] ?? FALSE;
        if ($filter_created_on || $sort_created_on) {
            $alias = uniqid('created_on_');

            if ($filter_created_on) {
                $dates = explode(' - ', $filter_created_on);
                $this->datatable_builder->where("created_on >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("created_on <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['created_on']);
            }

            if ($sort_created_on) {
                $this->datatable_builder->order_by("created_on", $sort_created_on);
                unset($args['order_by']['created_on']);
            }
        }
        // created_by FILTERING & SORTING
        $filter_created_by = $args['where']['post_author'] ?? FALSE;
        $sort_created_by = $args['order_by']['post_author'] ?? FALSE;
        if ($filter_created_by || $sort_created_by) {
            $alias = uniqid('created_by_');
            $this->datatable_builder
                ->join("user tblcustomer", "post_author = tblcustomer.user_id", 'LEFT');

            if ($filter_created_by) {
                $this->datatable_builder->like("tblcustomer.display_name", $filter_created_by);
                unset($args['where']['post_author']);
            }

            if ($sort_created_by) {
                $this->datatable_builder->order_by('tblcustomer.display_name', $filter_created_by);
                unset($args['order_by']['post_author']);
            }
        }
        // teacher_id FILTERING & SORTING
        $filter_teacher_id = $args['where']['teacher_id'] ?? FALSE;
        $sort_teacher_id = $args['order_by']['teacher_id'] ?? FALSE;
        if ($filter_teacher_id || $sort_teacher_id) {
            $alias = uniqid('teacher_id_');
            $this->datatable_builder
                ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id and {$alias}.meta_key = 'teacher_id'", 'LEFT')
                ->join("user tblcustomer", "{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

            if ($filter_teacher_id) {
                $this->datatable_builder->like("tblcustomer.display_name", $filter_teacher_id);
                unset($args['where']['teacher_id']);
            }

            if ($sort_teacher_id) {
                $this->datatable_builder->order_by('tblcustomer.display_name', $filter_teacher_id);
                unset($args['order_by']['teacher_id']);
            }
        }

        // Created_on COURSE TYPE
        $filter_course_type = $args['where']['course_type'] ?? FALSE;
        $sort_course_type = $args['order_by']['course_type'] ?? FALSE;
        if ($filter_course_type || $sort_course_type) {
            $alias = uniqid('teacher_id_');
            $this->datatable_builder
                ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id and {$alias}.meta_key = 'course_type'", 'LEFT');

            if ($filter_course_type) {
                $this->datatable_builder->where("{$alias}.meta_value =", $filter_course_type);
                unset($args['where']['course_type']);
            }

            if ($sort_course_type) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_course_type);
                unset($args['order_by']['course_type']);
            }
        }

        // course_daterange FILTERING & SORTING

        $filter_course_value = $args['where']['course_value'] ?? FALSE;
        $sort_course_value = $args['order_by']['course_value'] ?? FALSE;
        if ($filter_course_value || $sort_course_value) {
            $alias = uniqid('course_value_');
            $this->datatable_builder
                ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id and {$alias}.meta_key = 'course_value'", 'LEFT');
            if ($sort_course_value) {
                $this->datatable_builder->order_by("CAST({$alias}.meta_value as UNSIGNED)",$sort_course_value);
                unset($args['order_by']['course_value'],$args['where']['course_value']);
            }
        }

        $filter_total_student = $args['where']['total_student'] ?? FALSE;
        $sort_total_student = $args['order_by']['total_student'] ?? FALSE;
        if ($filter_total_student || $sort_total_student) {
            $alias = uniqid('total_student_');
            $this->datatable_builder
                ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id and {$alias}.meta_key = 'student_qty'", 'LEFT');
            if ($sort_total_student) {
                $this->datatable_builder->order_by("CAST({$alias}.meta_value as UNSIGNED)",$sort_total_student);
                unset($args['order_by']['total_student'],$args['where']['total_student']);
            }
        }

        $filter_term_status = $args['where']['post_status'] ?? FALSE;
        $sort_term_status = $args['order_by']['post_status'] ?? FALSE;

        if ($filter_term_status || $sort_term_status) {
            if ($filter_term_status) {
                $this->datatable_builder->where('post_status', $filter_term_status);
                unset($args['where']['post_status']);
            }
            if ($sort_term_status) {
                $this->datatable_builder->order_by("post_status", $sort_term_status);
                unset($args['order_by']['post_status']);
            }
        }

        $args = $this->datatable_builder->parse_relations_searches($args);
        if (!empty($args['where'])) {
            foreach ($args['where'] as $key => $value) {
                if (empty($value)) continue;

                if (empty($key)) {
                    $this->datatable_builder->add_filter($value, '');
                    continue;
                }

                $this->datatable_builder->add_filter($key, $value);
            }
        }

        if (!empty($args['order_by'])) {
            foreach ($args['order_by'] as $key => $value) {
                $this->datatable_builder->order_by($key, $value);
            }
        }
    }
}