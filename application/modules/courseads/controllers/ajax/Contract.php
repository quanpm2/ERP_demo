<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends Admin_Controller {

	public $model = '';

	function __construct()
	{
		parent::__construct();
		$this->load->model('courseads/course_m');
		$this->load->model('courseads/courseads_contract_m');
		$this->load->model('courseads/courseads_m');
		$this->load->model('courseads/student_m');
		$this->load->model('term_users_m');

		$this->load->config('courseads/courseads');
	}


	/**
	 * { function_description }
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function index()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);

        // if ( ! $this->courseads_m->has_permission('Courseads.contract.access'))
        if ( ! has_permission('Courseads.contract.access'))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $defaults 	= ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args 		= wp_parse_args($this->input->get(), $defaults);
        $data 		= $this->data;
        
        $courses 	= $this->course_m->set_post_type()->get_all();
        $courses 	= $courses ? key_value($courses, 'post_id', 'post_title') : array();

        $this->load->library('datatable_builder');
        /* Applies get query params for filter */
        $this->search_filter();

		// LOGGED USER NOT HAS PERMISSION TO ACCESS
        $relate_users = $this->admin_m->get_all_by_permissions('Courseads.course.access');
		if($relate_users === FALSE)
		{
			$response['msg'] = 'Quyền truy cập bị hạn chế.';
			return parent::renderJson($response);
		}

		if(is_array($relate_users))
		{
			$this->datatable_builder->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
		}


        $this->datatable_builder->set_filter_position(FILTER_TOP_OUTTER)
        ->select('term.term_id,term.term_name,term.term_type')
        
        ->add_search('contract_code', ['placeholder' => 'Mã HĐ'])
        ->add_search('customer', ['placeholder' => 'Khách hàng', 'class' => 'form-control'])
        ->add_search('course_id', array('content' => form_dropdown(array('name' => 'where[course_id]', 'class' => 'form-control select2 where_course_code'), prepare_dropdown($courses, 'Khóa học : All'), $this->input->get('where[course_id]'))))
        ->add_search('created_on', ['placeholder' => 'Ngày tạo', 'class' => 'form-control input_daterange'])
        ->add_search('staff_business', ['placeholder' => 'Kinh doanh phụ trách', 'class' => 'form-control'])

        ->add_search('term_status', array('content' => form_dropdown(array('name' => 'where[term_status]', 'class' => 'form-control select2'), prepare_dropdown($this->config->item('register_status'), 'Trạng thái : All'), $this->input->get('where[term_status]'))))
        ->add_search('action',['content'=>form_button(['name'=>'notification_of_payment','class'=>'btn btn-info', 'id' => 'send-mail-payment','type'=>'submit'],'<i class="fa fa-fw fa-send"></i> Email')])

        ->add_column('all_checkbox',array('title'=> '<input type="checkbox" name="all-checkbox" value="" id="all-checkbox" class="minimal"/>','set_order'=> FALSE, 'set_select' => FALSE))
        ->add_column('contract_code', array('set_select' => FALSE, 'title' => 'Mã HĐ', 'set_order' => TRUE))
        ->add_column('customer', array('set_select' => FALSE, 'title' => 'Khách hàng', 'set_order' => TRUE))
        ->add_column('course_id', array('set_select' => FALSE, 'title' => 'Khóa học', 'set_order' => TRUE))
        ->add_column('staff_business', array('set_select' => FALSE, 'title' => 'Kinh doanh phụ trách'))
        ->add_column('contract_value', array('set_select' => FALSE, 'title' => 'Giá trị HĐ'))
        ->add_column('term_status', 'Trạng thái')
        ->add_column('action', array('set_select' => FALSE, 'title' => 'Actions', 'set_order' => FALSE))

        ->add_column_callback('term_id', function ($data, $row_name) {

            $term_id = $data['term_id'];
            $data['all_checkbox'] = '<input type="checkbox"  name="post_ids[]" value="'.$term_id.'" class="minimal post_ids"/>' ;
            $has_send_mail_payment = get_term_meta_value($term_id, 'start_service_time');
			if($has_send_mail_payment != "")
			{
				$data['all_checkbox'] = '<input type="checkbox" name="nocheck-post_ids[]" value="" class="minimal" checked="checked" disabled="disabled"/>' ;
			}

			$contract_code = get_term_meta_value($term_id,'contract_code') ?: '<code>HĐ chưa được cấp số</code>';
			$contract_link = anchor(admin_url("courseads/overview/{$term_id}"),$contract_code,['target'=>'_blank']);
			$contract_code = "<b>{$contract_link}</b>";
			$contract_code.= '<small><em> ('.$this->config->item($data['term_status'],'contract_status').')</em></small>';

			// Get contract date range
			$contract_begin = get_term_meta_value($term_id,'contract_begin');
			$contract_end = get_term_meta_value($term_id,'contract_end');
			$contract_daterange = my_date($contract_begin,'d/m/Y').' - '.my_date($contract_end,'d/m/Y');

			$contract_code.= br()."<span class='text-muted col-xs-12' data-toggle='tooltip' title='Thời gian trên HĐ'><i class='fa fa-fw fa-adjust'></i>{$contract_daterange}</span>";

			$data['contract_code'] = $contract_code;

			/* Thông tin khách hàng */
			$customer = $this->term_users_m->get_the_users($term_id, array('customer_person','customer_company'));
			if($customer)
			{
				$customer 			= $customer ? reset($customer) : NULL;
				$data['customer'] 	= $this->admin_m->get_field_by_id($customer->user_id, 'display_name');
			}

			/* Danh sách khóa học */
			$courses_items 		= unserialize(get_term_meta_value($term_id, 'courses_items'));
			$data['course_id'] 	= $courses_items ? implode(br(), array_column($courses_items, 'post_title')) : '';

			/* Giá trị hợp đồng */
			$courses_items 		= unserialize(get_term_meta_value($term_id, 'courses_items'));
			$data['course_id'] 	= $courses_items ? implode(br(), array_column($courses_items, 'post_title')) : '';

           	/* Giá trị hợp đồng */
           	$contract_value = get_term_meta_value($term_id, 'contract_value');
           	$data['contract_value'] = currency_numberformat($contract_value);

			/*
            * Nhân viên kinh doanh
            */
           	$staff_business = (int) get_term_meta_value($term_id, 'staff_business');
           	$data['staff_business'] = $staff_business ? $this->admin_m->get_field_by_id($staff_business, 'display_name') : '';

           	
           	$class_status ='';
            $data['_term_status'] = $data['term_status'];
            if($data['_term_status'] == 'pending') $class_status = 'label label-info';
            if($data['_term_status'] == 'publish') $class_status = 'label label-success';
            if($data['_term_status'] == 'removing') $class_status = 'label label-danger';
            $data['term_status'] = "<span class='{$class_status}'>{$this->config->item($data['term_status'], 'register_status')}</span>";

            /*
            * Hành động
            */
                      
            if(has_permission('Courseads.contract.access'))
			{
				$data['action'].= anchor(module_url("overview/{$term_id}"),'<i class="fa fa-fw fa-line-chart"></i>', 'class="btn btn-default btn-xs" data-toggle="tooltip" title="Thống kê chi tiết"');
			}

            if (has_permission('Courseads.Setting.Access')) {
                $data['action'] .= '<a title="Setting khóa học" class="btn btn-default btn-xs" href="' . admin_url('courseads/setting/' . $term_id) . '"><i class="fa fa-fw fa-cogs"></i></a>';
            }
            return $data;
        }, FALSE)
        ->from('term')
        ->where('term_type', 'courseads')
        ->where_in('term_status', ['pending', 'publish','waitingforapprove','ending'])
        ->group_by('term.term_id');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        $this->template->title->set('Quản lý khóa học');
        $this->template->description->set('Trang danh sách tất cả các khóa học');
        return parent::renderJson($data);
    }

	public function create(){
		$response = array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		if( ! has_permission('Courseads.contract.Add'))
		{
			$response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
			return parent::renderJson($response,500);
		}

		$edit = $this->input->post();
		if(empty($edit))
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ';
			return parent::renderJson($response,500);
		}

		$form_data = $this->courseads_m->validate($this->input->post());
		if( ! $form_data)
		{
			$errors = $this->form_validation->error_array();
			$response['msg'] = implode('<br/>', $errors);
			$response['data']['errors'] = $errors;

			return parent::renderJson($response,500);
		}

		$user_type = $form_data['edit']['user_type'];
		$is_user = $this->admin_m->select('user.user_id')
		->join('usermeta','user.user_id = usermeta.user_id')
		->where('user.user_type','student')
		->where('usermeta.meta_key','student_phone')
		->where('usermeta.meta_value',$form_data['meta']['contract_phone'])
		->where('user.user_email',$form_data['meta']['contract_email'])->get_many_by();


		$form_data['user']['display_name'] = $form_data['meta']['contract_name'];
		$form_data['user']['user_email'] = $form_data['meta']['contract_email'];
		$form_data['usermeta']['user_phone'] = $form_data['meta']['contract_phone'];
		$form_data['user']['user_type'] = 'student';
		$form_data['user']['user_time_create'] = time();

		// startBlock check số lượng học viên đã đủ chưa
		$student_list = get_post_meta_value($form_data['meta']['course'], 'student_list');
		$student_qty = get_post_meta_value($form_data['meta']['course'], 'student_qty');
		$list_student = explode(',',$student_list);
		if(count($list_student) == $student_qty){
			$response['msg'] = "Khóa học đã đủ số lượng học viên!";
			return parent::renderJson($response,500);
		}
		// endBlock
		$post = $this->course_m->set_post_type()->get($form_data['meta']['course']);
		$form_data['termmeta_register']['contract_value1'] = get_post_meta_value($form_data['meta']['course'], 'course_value');
		$form_data['termmeta_register']['contract_begin'] = $post->start_date;
		$form_data['termmeta_register']['contract_end'] = $post->end_date;

		// startBlock check mã giảm giá
		if($form_data['meta']['voucher_code'] == null) $form_data['termmeta_register']['contract_value'] = $form_data['termmeta_register']['contract_value1'];
		if($form_data['meta']['voucher_code'] != null)
		{
			$check_voucher = $this->checkVoucher($form_data['meta']['course'],$form_data['meta']['voucher_code']);
			$check_voucher = json_decode($check_voucher->final_output);
			if($check_voucher->success == FALSE)
			{
				$response['msg'] = $check_voucher->msg;
				return parent::renderJson($response,500);
			}else{
				$form_data['termmeta_register']['voucher_code'] 	= $form_data['meta']['voucher_code'];
				$form_data['termmeta_register']['voucher_value'] 	= get_post_meta_value($form_data['meta']['course'], 'voucher_value');
				$form_data['termmeta_register']['contract_value'] 	= $form_data['termmeta_register']['contract_value1'] - $form_data['termmeta_register']['voucher_value'];
			}
		}
		// endBlock

		$flag = false;
		if(count($is_user) == 0){
			$is_email = $this->admin_m->select('user_id')
			->where('user.user_type','student')
			->where('user.user_email',$form_data['user']['user_email'])->get_many_by();
			if(count($is_email)==1){
				if($flag == false){
					$this->user_m->update($is_email[0]->user_id, $form_data['user']);
					update_user_meta($is_email[0]->user_id, 'student_phone', $form_data['usermeta']['user_phone']);
					$student_id = $is_email[0]->user_id;
					$flag = true;
				}
			}
			$is_phone = $this->admin_m->select('user.user_id')
				->join('usermeta','user.user_id = usermeta.user_id')
				->where('usermeta.meta_key','student_phone')
				->where('usermeta.meta_value',$form_data['usermeta']['user_phone'])->get_many_by();
			if(count($is_phone)==1){
				if($flag == false){
					$this->user_m->update($is_email[0]->user_id, $form_data['user']);
					update_user_meta($is_phone[0]->user_id, 'student_phone', $form_data['usermeta']['user_phone']);
					$student_id = $is_phone[0]->user_id;
					$flag = true;
				}
			}
			if($flag == false){
				$student_id = $this->user_m->insert($form_data['user']);
				update_user_meta($student_id, 'student_phone', $form_data['usermeta']['user_phone']);
			}
		}else{
			$student_id = $is_user[0]->user_id;
		}

		// startBlock check học viên đã đăng ký
		$register_list = get_post_meta_value($form_data['meta']['course'], 'register_list');
		$list_register = explode(',',$register_list);
		if(in_array($student_id,$list_register) == true){
			$response['msg'] = "Học viên đã đăng ký khóa học này!";
			return parent::renderJson($response,500);
		}
		// endBlock

		$form_data['usermeta']['created_by'] 	= $this->admin_m->id;
		$form_data['usermeta']['created_on'] 	= time();
		$form_data['usermeta']['course_id'] 	= $form_data['meta']['course'];
		$list_courses = $this->usermeta_m->get_meta_value($student_id, 'course_lists');
		if($list_courses != null){
			$form_data['usermeta']['course_lists'] = $list_courses .','.$form_data['usermeta']['course_id'];
		}else{
			$form_data['usermeta']['course_lists'] = $form_data['usermeta']['course_id'];
		}
		if(!empty($form_data['usermeta']))
		{
			foreach ($form_data['usermeta'] as $key  => $value)
			{
				if($key!='course_id' && $key != 'student_phone' && $key != 'user_phone')
				{
					update_user_meta($student_id,$key,$value);
				}

			}
		}

		// startBlock insert term register
		$form_data['term_register']['term_name'] = $form_data['meta']['contract_name'].'-'.get_term_meta_value($form_data['meta']['course'], 'course_code');
		$form_data['term_register']['term_type'] = 'course_contract';
		$form_data['term_register']['term_status'] = 'pending';
		$register_id = $this->term_m->insert($form_data['term_register']);
		// endBlock

		// startBlock insert term meta register
		$register_qty = intval(get_post_meta_value($form_data['meta']['course'], 'register_qty'))+1 ?: 1;
		$form_data['termmeta_register']['register_code'] 	= $post->post_name.'/'.$register_qty;
		$form_data['termmeta_register']['created_by'] 	= $this->admin_m->id;
		$form_data['termmeta_register']['created_on'] 	= time();
		$form_data['termmeta_register']['course_id'] 	= $form_data['meta']['course'];
		$form_data['termmeta_register']['student_id'] 	= $student_id;

		if(!empty($form_data['termmeta_register']))
		{
			foreach ($form_data['termmeta_register'] as $key  => $value)
			{
				update_term_meta($register_id,$key,$value);
			}
		}
		// endBlock

		// startBlock insert user meta register
		$form_data['termmeta']['register_qty'] 	= $register_qty;
		if($register_list== null){
			$form_data['termmeta']['register_list'] 	= $student_id;
		}else{
			$form_data['termmeta']['register_list'] 	= $register_list. ',' . $student_id;
		}

		if(!empty($form_data['termmeta']))
		{
			foreach ($form_data['termmeta'] as $key  => $value)
			{
				update_post_meta($form_data['meta']['course'],$key,$value);
			}
		}
		// endBlock

		$list_vouchers = get_post_meta_value($form_data['meta']['course'], 'voucher_by_user');
        $list_vouchers = unserialize($list_vouchers);
		$list_vouchers[$form_data['meta']['voucher_code']]['qty'] = $list_vouchers[$form_data['meta']['voucher_code']]['qty'] -1;
		update_post_meta($form_data['meta']['course'],'voucher_by_user',serialize($list_vouchers));
		// startBlock insert term user
		$form_data['term_users']['user_id'] = $this->admin_m->id;
		$form_data['term_users']['term_id'] = $register_id;
		$this->term_users_m->insert($form_data['term_users']);
		// endBlock
		$response['msg'] = 'Thêm mới dữ liệu thành công !';
		return parent::renderJson($response);
	}

	public function update(){
		$response = array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		if( ! has_permission('Courseads.contract.Add'))
		{
			$response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
			return parent::renderJson($response,500);
		}

		$edit = $this->input->post('edit');
		if(empty($edit))
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ';
			return parent::renderJson($response,500);
		}
		$temp = $this->input->post();
		$form_data = $this->courseads_m->validate($temp['edit']);
		if( ! $form_data)
		{
			$errors = $this->form_validation->error_array();
			$response['msg'] = implode('<br/>', $errors);
			$response['data']['errors'] = $errors;

			return parent::renderJson($response,500);
		}
		$term = $this->courseads_m->set_term_type()->get($form_data['meta']['contract_id']);
		$student_id = get_term_meta_value($term->term_id, 'student_id');
		$form_data['user']['display_name'] = $form_data['meta']['contract_name'];
		$form_data['user']['user_email'] = $form_data['meta']['contract_email'];
		$form_data['usermeta']['student_phone'] = $form_data['meta']['contract_phone'];
		$this->user_m->update($student_id, $form_data['user']);
		update_user_meta($student_id,'student_phone',$form_data['usermeta']['student_phone']);
		if($form_data['meta']['register_status'] == 'publish'){
			if($term->term_status != 'publish'){
				$response['msg'] = 'Bạn không thể cập nhập trạng thái đã thanh toán !';
				return parent::renderJson($response,500);
			}
		}else{
			$form_data['term']['term_status'] = $form_data['meta']['register_status'];
			$this->db->where('term_id', $term->term_id);
			$this->db->update('term', $form_data['term']);
		}
		$response['msg'] = 'Cập nhập dữ liệu thành công !';
		return parent::renderJson($response);
	}

	public function checkEmail()
	{
		$contract_email = $this->input->post('contract_email');
		$register = $this->admin_m->select('user_id,display_name,user_email')
		->where('user.user_type','student')
		->where('user.user_email',$contract_email)->get_many_by();

		if(count($register) == 0){
			$response['msg'] 		= 'Không có thông tin học viên. Mời bạn tạo mới !';
			$response['success'] 	= false;
			return parent::renderJson($response);
		}
		$response['student'] = $register[0];
		$response['student']->phone = $this->usermeta_m->get_meta_value($response['student']->user_id, 'student_phone');
		$response['msg'] 		= 'Thông tin học viên đã có. Tự động load học viên !';
		$response['success'] 	= true;

		return parent::renderJson($response);
	}

	public function checkPhone()
	{
		$contract_phone = $this->input->post('contract_phone');
		$register = $this->admin_m->select('user.user_id,display_name,user_email,meta_value as phone')
			->join('usermeta','user.user_id = usermeta.user_id')
			->where('usermeta.meta_key','student_phone')
			->where('usermeta.meta_value',$contract_phone)->get_many_by();
		if(count($register) == 0){
			$response['msg'] 		= 'Không có thông tin học viên. Mời bạn tạo mới !';
			$response['success'] 	= false;
			return parent::renderJson($response);
		}
		$response['msg'] 		= 'Thông tin học viên đã có. Tự động load học viên !';
		$response['success'] 	= true;
		$response['student'] = $register[0];
		return parent::renderJson($response);
	}

	public function checkVoucher($id = null,$voucher_code = null){
        $data = array();
		if($this->input->post('course_id') != null || $this->input->post('voucher_code')  != null ){
			$data['id'] = $this->input->post('course_id');
			$code = $this->input->post('voucher_code');
		}else{
			$data['id'] = $id;
			$code = $voucher_code;
		}
        $term = $this->course_m->set_post_type()->get($data['id']);
        $list_vouchers = get_post_meta_value($data['id'], 'voucher_by_user');
		if($list_vouchers == null){
			$data['msg'] = 'Mã giảm giá không tồn tại hoặc không có chương trình giảm giá !';
            $data['success'] 	= FALSE;
            return parent::renderJson($data);
		}
        $list_vouchers = unserialize($list_vouchers);
        $voucher_start = get_post_meta_value($data['id'], 'voucher_start');
        if($data['id'] == ''){
            $data['msg'] = 'Mời chọn khóa học !';
            $data['success'] 	= FALSE;
            return parent::renderJson($data);
        }
        if($code == ''){
            $data['msg'] = 'Mời nhập mã giảm giá !';
            $data['success'] 	= FALSE;
            return parent::renderJson($data);
        }
		if(array_key_exists($code,$list_vouchers) == 1){
			if($list_vouchers[$code]['qty'] <= 0){
				$data['msg'] = 'Số lượng giảm giá đã hết !';
				$data['success'] 	= FALSE;
				return parent::renderJson($data);
			}
			$data['code'] = $code;
			$data['voucher'] = $list_vouchers[$code];
			$data['success'] = TRUE;
			$data['voucher_value'] = get_post_meta_value($data['id'], 'voucher_value');
		}else{
			$data['msg'] = 'Mã giảm giá không tồn tại !';
			$data['success'] = FALSE;
			return parent::renderJson($data);
		}
        if(time() < $voucher_start){
            $data['msg'] = 'Chương trình giảm giá chưa bắt đầu !';
            $data['success'] = FALSE;
            return parent::renderJson($data);
        }
        $voucher_end = get_post_meta_value($data['id'], 'voucher_end');
        if(time() > $voucher_end){
            $data['msg'] = 'Chương trình giảm giá đã kết thúc !';
            $data['success'] = FALSE;
            return parent::renderJson($data);
        }

        return parent::renderJson($data);
    }

    /**
	 * Excuse the set of start services process
	 *
	 * 1. Update Money Exchange Rate from VIETCOMBANK API
	 * 2. Update Advertise start time if is not set
	 * 3. Log trace action
	 * 4. Send activation email to all responsibilty users
	 * 5. Detect if first contract
	 * 6. Send SMS notify to contract's customer
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     JSON
	 */
	public function proc_service($term_id = 0)
	{

		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
		$args 		= wp_parse_args( $this->input->post(), ['begin_date' => my_date(time(), 'd-m-Y')]);

		// Restrict permission of current user
		if( ! has_permission('Courseads.Start_service.Manage')) /* Check permission sau "Courseads.Start_service" > cần update permissions ở file courseads.php */
		{
			$response['msg'] = 'Không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}

		// Check if the service is running then stop excute and return
		if($this->courseads_m->is_service_proc($term_id))
		{
			$response['msg'] = 'Dịch vụ đã được thực hiện.';
			return parent::renderJson($response);
		}
		$term = $this->courseads_m->set_term_type()->get($term_id);
		if( ! $term)
		{
			$response['msg'] = 'Không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}
		$list_student = unserialize(get_term_meta_value($term_id,'guru_students'));
		if( ! $list_student){
			$response['msg'] = 'Không có dữ liệu học viên!!';
			return parent::renderJson($response);
		}
		// Update Adwords begin time
		update_term_meta($term_id, 'googleads-begin_time', $this->mdate->startOfDay($args['begin_date']));

		# code
		$result = $this->courseads_contract_m->proc_service($term);
		if( ! $result)
		{
			$response['msg'] = 'Quá trình xử lý không thành công.';
			return parent::renderJson($response);
		}

		// Detect if contract is the first signature (tái ký | ký mới)
		// $this->load->model('contract/base_contract_m');
		// $this->base_contract_m->detect_first_contract($term_id);

		// Send SMS to customer
		// $this->load->model('contract/contract_report_m');
		// $this->contract_report_m->send_sms_activation_2customer($term_id);

		$response['msg']	= 'Dịch vụ đã được kích hoạt thực hiện thành công.';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}

	protected function search_filter($search_args = array())
    {
        $args = $this->input->get(NULL, TRUE);

        if (!empty($args['where'])) $args['where'] = array_map(function ($x) { return trim($x); }, $args['where']);

        // contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code)
        {
            $alias = uniqid('contract_code_');
            $this->datatable_builder
            ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

            if($filter_contract_code) $this->datatable_builder->like("{$alias}.meta_value", $filter_contract_code);
            if($sort_contract_code) $this->datatable_builder->order_by("{$alias}.meta_value", $sort_contract_code);

            unset($args['where']['contract_code'], $args['order_by']['contract_code']);
        }

        // course_id FILTERING & SORTING
        $filter_course_id = $args['where']['course_id'] ?? FALSE;
        $sort_course_id = $args['order_by']['course_id'] ?? FALSE;
        if($filter_course_id || $sort_course_id)
        {
            $this->datatable_builder->join('term_posts', 'term.term_id = term_posts.term_id', 'LEFT');
            $this->datatable_builder->join('posts', 'term_posts.post_id = posts.post_id AND posts.post_type = "course"');

            if($filter_course_id) $this->datatable_builder->where('posts.post_id', $filter_course_id);
            if($sort_course_id) $this->datatable_builder->order_by('posts.post_title', $sort_course_id);

            unset($args['where']['course_id'], $args['order_by']['course_id']);
        }

        // Created_on FILTERING & SORTING
        $filter_created_on = $args['where']['created_on'] ?? FALSE;
        $sort_created_on = $args['order_by']['created_on'] ?? FALSE;
        if ($filter_created_on || $sort_created_on) {
            $alias = uniqid('created_on_');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'created_on'", 'LEFT');

            if ($filter_created_on) {
                $dates = explode(' - ', $filter_created_on);
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['created_on']);
            }

            if ($sort_created_on) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_created_on);
                unset($args['order_by']['created_on']);
            }
        }

        // Contract_code FILTERING & SORTING
		$filter_staff_business = $args['where']['staff_business'] ?? FALSE;
		$sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
		if($filter_staff_business || $sort_staff_business)
		{
			$alias = uniqid('staff_business_');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT')
			->join("user tblcustomer","{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

			if($filter_staff_business)
			{	
				$this->datatable_builder->like("tblcustomer.user_email",$filter_staff_business);
				unset($args['where']['staff_business']);
			}

			if($sort_staff_business)
			{
				$this->datatable_builder->order_by('tblcustomer.user_email',$sort_staff_business);
				unset($args['order_by']['staff_business']);
			}
		}

		// customer FILTERING & SORTING
		$filter_customer = $args['where']['customer'] ?? FALSE;
		$sort_customer = $args['order_by']['customer'] ?? FALSE;
		if($filter_customer || $sort_customer)
		{
			$alias = uniqid('contract_customers_');
			$alias_customer_tbl = uniqid("{$alias}_tbl_");
			$this->datatable_builder
			->join("term_users {$alias}","{$alias}.term_id = term.term_id", 'LEFT')
			->join("user {$alias_customer_tbl}","{$alias_customer_tbl}.user_id = {$alias}.user_id", 'LEFT');

			if($filter_customer)
			{	
				$this->datatable_builder->like("{$alias_customer_tbl}.display_name", $filter_customer);
				unset($args['where']['customer']);
			}

			if($sort_customer)
			{
				$this->datatable_builder->order_by("{$alias_customer_tbl}.display_name", $sort_customer);
				unset($args['order_by']['customer']);
			}
		}

        // term_status FILTERING & SORTING
        $filter_term_status = $args['where']['term_status'] ?? FALSE;
        $sort_term_status = $args['order_by']['term_status'] ?? FALSE;

        if ($filter_term_status || $sort_term_status) {
            if ($filter_term_status) {
                $this->datatable_builder->where('term.term_status', $filter_term_status);
                unset($args['where']['term_status']);
            }
            if ($sort_term_status) {
                $this->datatable_builder->order_by("term.term_status", $sort_term_status);
                unset($args['order_by']['term_status']);
            }
        }

        $args = $this->datatable_builder->parse_relations_searches($args);
        if (!empty($args['where'])) {
            foreach ($args['where'] as $key => $value) {
                if (empty($value)) continue;

                if (empty($key)) {
                    $this->datatable_builder->add_filter($value, '');
                    continue;
                }

                $this->datatable_builder->add_filter($key, $value);
            }
        }

        if (!empty($args['order_by'])) {
            foreach ($args['order_by'] as $key => $value) {
                $this->datatable_builder->order_by($key, $value);
            }
        }
    }
}
/* End of file Contract.php */
/* Location: ./application/modules/courseads/controllers/ajax/Contract.php */