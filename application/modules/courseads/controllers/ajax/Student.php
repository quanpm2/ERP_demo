<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends Admin_Controller {

	public $model = '';

	function __construct()
	{
		parent::__construct();

		$this->load->model('course_m');
		$this->load->model('courseads_contract_m');
		$this->load->model('student_m');
	}


	public function index()
	{
		$data = array();
		parent::render($data);
	}

	public function create()
	{
		$response = array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		if( ! has_permission('Courseads.contract.Add'))
		{
			$response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
			return parent::renderJson($response,500);
		}

		$edit = $this->input->post('edit');
		if(empty($edit))
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ';
			return parent::renderJson($response,500);
		}

		$form_data = $this->student_m->validate($this->input->post());
		if( ! $form_data)
		{
			$errors = $this->form_validation->error_array();
			$response['msg'] = implode('<br/>', $errors);
			$response['data']['errors'] = $errors;

			return parent::renderJson($response,500);
		}
		$is_email = $this->admin_m->select('user_id')
		->where('user.user_type','student')
		->where('user.user_email',$form_data['meta']['student_email'])->get_many_by();
		if(count($is_email)==1){
			$response['msg'] = 'Email đã được sử dụng !';
			return parent::renderJson($response,500);
		}
		$is_phone = $this->admin_m->select('user.user_id')
			->join('usermeta','user.user_id = usermeta.user_id')
			->where('usermeta.meta_key','student_phone')
			->where('usermeta.meta_value',$form_data['meta']['student_phone'])->get_many_by();
		if(count($is_phone)==1){
			$response['msg'] = 'Số điện thoại đã được sử dụng !';
			return parent::renderJson($response,500);
		}
		$form_data['user']['display_name'] = $form_data['meta']['student_name'];
		$form_data['user']['user_email'] = $form_data['meta']['student_email'];
		$form_data['user']['user_type'] = 'student';
		$form_data['user']['user_time_create'] = time();
		$form_data['usermeta']['student_phone'] = $form_data['meta']['student_phone'];
		$form_data['usermeta']['created_by'] 	= $this->admin_m->id;
		$form_data['usermeta']['created_on'] 	= time();
		$student_id = $this->user_m->insert($form_data['user']);
		if(!empty($form_data['usermeta']))
		{
			foreach ($form_data['usermeta'] as $key  => $value)
			{
				update_user_meta($student_id,$key,$value);
			}
		}

		$response['msg'] = 'Thêm mới dữ liệu thành công !';
		return parent::renderJson($response);
	}

	public function update()
	{
		$response = array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		if( ! has_permission('Courseads.contract.Add'))
		{
			$response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
			return parent::renderJson($response,500);
		}

		$edit = $this->input->post('edit');
		if(empty($edit))
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ';
			return parent::renderJson($response,500);
		}
		$temp = $this->input->post();
		$form_data = $this->student_m->validate($temp['edit']);
		if( ! $form_data)
		{
			$errors = $this->form_validation->error_array();
			$response['msg'] = implode('<br/>', $errors);
			$response['data']['errors'] = $errors;

			return parent::renderJson($response,500);
		}
		$form_data['user']['display_name'] = $form_data['meta']['student_name'];
		$form_data['user']['user_email'] = $form_data['meta']['student_email'];
		$form_data['usermeta']['student_phone'] = $form_data['meta']['student_phone'];
		$this->user_m->update($form_data['meta']['student_id'], $form_data['user']);
		update_user_meta($form_data['meta']['student_id'],'student_phone',$form_data['usermeta']['student_phone']);

		$response['msg'] = 'Thêm mới dữ liệu thành công !';
		return parent::renderJson($response);
	}
}
