<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataset extends Admin_Controller
{
    public function __construct()
    {
        $models = array(
            'term_users_m',
            'courseads/course_m',
            'courseads/courseads_m',
        );
        parent::__construct();
        $this->load->model($models);
        $this->load->library('datatable_builder');
        $this->load->config('courseads');
        $this->load->config('course_contract');
    }

    public function index()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);

        if (!has_permission('Courseads.course.Access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $defaults = ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args = wp_parse_args($this->input->get(), $defaults);

        $data = $this->data; // remove-after

        $relate_users = $this->admin_m->get_all_by_permissions('Courseads.course.Access');

        /* Applies get query params for filter */
        $this->search_filter();

        $this->datatable_builder->set_filter_position(FILTER_TOP_OUTTER)
            ->select('posts.post_id,posts.post_name,posts.post_author,posts.created_on,posts.start_date,posts.end_date,posts.post_status')
            ->add_search('post_name', ['placeholder' => 'Mã khóa học'])
            ->add_search('posts.post_title', ['placeholder' => 'Tên khóa học'])
            ->add_search('created_on', ['placeholder' => 'Ngày tạo', 'class' => 'form-control input_daterange'])
            ->add_search('start_date', ['placeholder' => 'Ngày bắt đầu khóa học', 'class' => 'form-control input_daterange'])
            ->add_search('end_date', ['placeholder' => 'Ngày kết thúc khóa học', 'class' => 'form-control input_daterange'])
            ->add_search('teacher_id', ['placeholder' => 'Giảng viên phụ trách', 'class' => 'form-control'])
            ->add_search('post_author', ['placeholder' => 'Người tạo'])
            ->add_search('course_type', array('content' => form_dropdown(array('name' => 'where[course_type]', 'class' => 'form-control'), prepare_dropdown($this->config->item('course_type'), 'Loại khóa học'), $this->input->get('where[course_type]'))))
            ->add_search('post_status', array('content' => form_dropdown(array('name' => 'where[post_status]', 'class' => 'form-control'), prepare_dropdown($this->config->item('course_status'), 'Trạng thái khóa học'), $this->input->get('where[post_status]'))))
            ->add_column('post_name', array('set_select' => FALSE, 'title' => 'Mã Khóa học', 'set_order' => TRUE))
            ->add_column('course_daterange', array('set_select' => FALSE, 'title' => 'T/G HĐ'))
            ->add_column('course_value', array('set_select' => FALSE, 'title' => 'GTKH'))
            ->add_column('total_student', array('set_select' => FALSE, 'title' => 'Tỷ lệ học viên'))
            ->add_column('post_status', 'Trạng thái khóa học')
            ->add_column('course_type', array('set_select' => FALSE, 'title' => 'Loại khóa học'))
            ->add_column('teacher_id', array('set_select' => FALSE, 'title' => 'Giảng viên phụ trách', 'set_order' => FALSE))
            ->add_column('action', array('set_select' => FALSE, 'title' => 'Actions', 'set_order' => FALSE))
            ->add_column_callback('post_id', function ($data, $row_name) {

                $post_id = $data['post_id'];
                $course_code = $data['post_name'];
                $created_by = $data['post_author'];
                /*
                * Mã khóa học
                */
                // $course_code = get_term_meta_value($term_id, 'course_code') ? : '<code>Không có mã khóa học</code>';

                if (has_permission('Courseads.course.Update') || has_permission('Courseads.course.Add')) {
                    $course_code = '<a title="Cập nhật khóa học" style="cursor: pointer;" onclick="showEdit(' . $post_id . ')">' . $course_code . '</a>';
                }
                $course_code = "<b>{$course_code}</b></br/>";
                /*
                * Người tạo
                */
                if ($created_by) {
                    $created_display_name = $this->admin_m->get_field_by_id($created_by, 'display_name');
                    $course_code .= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Người tạo'><i class='fa fa-fw fa-user'></i>Tạo bởi :{$created_display_name}</span>";
                }
                /*
                * Ngày tạo
                */
                if ($data['created_on']) {
                    $created_datetime = my_date($data['created_on'], 'd/m/Y');
                    $course_code .= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Ngày tạo'><i class='fa fa-fw  fa-clock-o'></i>Ngày tạo :{$created_datetime}</span>";
                }
                $data['post_name'] = $course_code;
                /*
                * Thời gian khóa học(từ ngày - đến ngày)
                */
                $course_daterange = '<i class="fa fa-fw fa-play" style="color:#72d072;font-size:0.8em"></i> ' . ($data['start_date'] ? my_date($data['start_date']) : '--') . br();
                $course_daterange .= '<i class="fa fa-fw fa-stop" style="color: #bb7171;font-size:0.8em"></i> ' . ($data['end_date'] ? my_date($data['end_date']) : '--');
                $data['course_daterange'] = $course_daterange;
                /*
                * Giá trị khóa học
                */
                $course_value = get_post_meta_value($post_id, 'course_value');
                $data['course_value'] = currency_numberformat($course_value, 'đ');

                /*
                * Loại khóa học
                */
                $data['_term_type'] = get_post_meta_value($post_id, 'course_type');
                $data['course_type'] = $this->config->item($data['_term_type'], 'course_type');
                $class_status ='';
                if($data['_term_type'] == 'demo') $class_status = 'label label-info';
                if($data['_term_type'] == 'publish') $class_status = 'label label-success';
                $data['course_type'] = "<span class='{$class_status}'>{$data['course_type']}</span>";
                /*
                * Trạng thái khóa học
                */
                $data['_term_status'] = $data['post_status'];
                $class_status ='';
                if($data['_term_status'] == 'pending') $class_status = 'label label-info';
                if($data['_term_status'] == 'publish') $class_status = 'label label-success';
                if($data['_term_status'] == 'ending') $class_status = 'label label-default';
                if($data['_term_status'] == 'remove') $class_status = 'label label-danger';
                $data['term_status'] = $this->config->item($data['post_status'], 'course_status');
                $data['post_status'] = "<span class='{$class_status}'>{$data['term_status']}</span>";
                /*
                * Tỷ lệ học viên
                */
                if ($student_qty = get_post_meta_value($post_id, 'student_qty')) {
                    $student_list = get_post_meta_value($post_id, 'student_list');
                    if ($student_list != null) {
                        $list_student = explode(',', $student_list);
                        $qty_stu = count($list_student);
                    } else {
                        $qty_stu = 0;
                    }
                    $register_list = get_post_meta_value($post_id, 'register_list');
                    if ($register_list != null) {
                        $list_register = explode(',', $register_list);
                        $qty_reg = count($list_register);
                    } else {
                        $qty_reg = 0;
                    }

                    $total_student = "<span class='text-muted' data-toggle='tooltip' title='Tỷ lệ học viên'><i class='fa fa-fw fa-user'></i>Tỷ lệ học viên :{$qty_stu}/{$student_qty}</span>";
                    $total_student .= "<br/><span class='text-muted' data-toggle='tooltip' title='Số lượng đăng ký'><i class='fa fa-fw fa-user'></i>Số lượng đăng ký :{$qty_reg}</span>";
                    $data['total_student'] = $total_student;
                }

                /*
                * Giảng viên phụ trấch
                */
                if ($teacher_id = get_post_meta_value($post_id, 'teacher_id')) {
                    $teacher_display_name = $this->admin_m->get_field_by_id($teacher_id, 'display_name');
                    $teacher_id = "<span class='text-muted' data-toggle='tooltip' title='Giảng viên phụ trách'><i class='fa fa-fw fa-user'></i>Giảng viên phụ trách :{$teacher_display_name}</span>";
                    $data['teacher_id'] = $teacher_id;
                }

                /*
                * Hành động
                */
                if (has_permission('Courseads.course.Update')) {
                    $data['action'] = '<a title="Cập nhật khóa học" class="btn btn-default btn-xs" onclick="showEdit(' . $post_id . ')"><i class="fa fa-fw fa-edit"></i></a>';
                }
                if (has_permission('Courseads.contract.Access')) {
                    $data['action'] .= '<a title="Trang danh sách phiếu đăng ký" class="btn btn-default btn-xs" href="' . admin_url('courseads/contract?id=' . $post_id) . '"><i class="fa fa-fw fa-book"></i></a>';
                }
                $data['action'] .= '<a title="Lấy mã giảm giá" class="btn btn-default btn-xs" onclick="getVoucher(' . $post_id . ')"><i class="fa fa-fw fa-gift"></i></a>';
                return $data;
            }, FALSE)
            ->from('posts')
            ->where('post_type', 'course');

        $status_list = array_keys($this->config->item('course_status'));
        array_unshift($status_list, 1);

        $this->datatable_builder
            ->where_in('post_status', $status_list)
            ->where_in('post_type', 'course')
            ->group_by('posts.post_id');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        $this->template->title->set('Quản lý khóa học');
        $this->template->description->set('Trang danh sách tất cả các khóa học');
        return parent::renderJson($data);
    }

    protected function search_filter($search_args = array())
    {
        $args = $this->input->get();

        if (!empty($args['where']))
            $args['where'] = array_map(function ($x) {
                return trim($x);
            }, $args['where']);


        // course_daterange FILTERING & SORTING

        $filter_course_daterange = $args['where']['course_daterange'] ?? FALSE;
        $sort_course_daterange = $args['order_by']['course_daterange'] ?? FALSE;
        if ($filter_course_daterange || $sort_course_daterange) {
            $alias = uniqid('course_daterange_');
            if ($sort_course_daterange) {
                $this->datatable_builder->order_by("start_date", $sort_course_daterange);
                unset($args['order_by']['course_daterange'],$args['where']['course_daterange']);
            }
        }

        // course_code FILTERING & SORTING
        $filter_course_code = $args['where']['post_name'] ?? FALSE;
        $sort_course_code = $args['order_by']['course_code'] ?? FALSE;
        if ($filter_course_code || $sort_course_code) {
            if ($filter_course_code) {
                $this->datatable_builder->like("post_name", $filter_course_code);
            }

            if ($sort_course_code) {
                $this->datatable_builder->order_by("(post_name * 1)", $sort_course_code);
            }

            unset($args['where']['post_name'], $args['order_by']['post_name']);
        }

        // start_date FILTERING & SORTING
        $filter_course_start = $args['where']['start_date'] ?? FALSE;
        $sort_course_start = $args['order_by']['start_date'] ?? FALSE;
        if ($filter_course_start || $sort_course_start) {
            if ($filter_course_start) {
                $dates = explode(' - ', $filter_course_start);
                $this->datatable_builder->where("start_date >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("start_date <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['start_date']);
            }

            if ($sort_course_start) {
                $this->datatable_builder->order_by("start_date", $sort_course_start);
                unset($args['order_by']['start_date']);
            }
        }

        // course_end FILTERING & SORTING
        $filter_course_end = $args['where']['end_date'] ?? FALSE;
        $sort_course_end = $args['order_by']['end_date'] ?? FALSE;
        if ($filter_course_end || $sort_course_end) {
            if ($filter_course_end) {
                $dates = explode(' - ', $filter_course_end);
                $this->datatable_builder->where("end_date >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("end_date <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['end_date']);
            }

            if ($sort_course_end) {
                $this->datatable_builder->order_by("end_date", $sort_course_end);
                unset($args['order_by']['end_date']);
            }
        }
        
        // Created_on FILTERING & SORTING
        $filter_created_on = $args['where']['created_on'] ?? FALSE;
        $sort_created_on = $args['order_by']['created_on'] ?? FALSE;
        if ($filter_created_on || $sort_created_on) {
            $alias = uniqid('created_on_');

            if ($filter_created_on) {
                $dates = explode(' - ', $filter_created_on);
                $this->datatable_builder->where("created_on >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("created_on <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['created_on']);
            }

            if ($sort_created_on) {
                $this->datatable_builder->order_by("created_on", $sort_created_on);
                unset($args['order_by']['created_on']);
            }
        }
        // created_by FILTERING & SORTING
        $filter_created_by = $args['where']['post_author'] ?? FALSE;
        $sort_created_by = $args['order_by']['post_author'] ?? FALSE;
        if ($filter_created_by || $sort_created_by) {
            $alias = uniqid('created_by_');
            $this->datatable_builder
                ->join("user tblcustomer", "post_author = tblcustomer.user_id", 'LEFT');

            if ($filter_created_by) {
                $this->datatable_builder->like("tblcustomer.display_name", $filter_created_by);
                unset($args['where']['post_author']);
            }

            if ($sort_created_by) {
                $this->datatable_builder->order_by('tblcustomer.display_name', $filter_created_by);
                unset($args['order_by']['post_author']);
            }
        }
        // teacher_id FILTERING & SORTING
        $filter_teacher_id = $args['where']['teacher_id'] ?? FALSE;
        $sort_teacher_id = $args['order_by']['teacher_id'] ?? FALSE;
        if ($filter_teacher_id || $sort_teacher_id) {
            $alias = uniqid('teacher_id_');
            $this->datatable_builder
                ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id and {$alias}.meta_key = 'teacher_id'", 'LEFT')
                ->join("user tblcustomer", "{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

            if ($filter_teacher_id) {
                $this->datatable_builder->like("tblcustomer.display_name", $filter_teacher_id);
                unset($args['where']['teacher_id']);
            }

            if ($sort_teacher_id) {
                $this->datatable_builder->order_by('tblcustomer.display_name', $filter_teacher_id);
                unset($args['order_by']['teacher_id']);
            }
        }

        // Created_on COURSE TYPE
        $filter_course_type = $args['where']['course_type'] ?? FALSE;
        $sort_course_type = $args['order_by']['course_type'] ?? FALSE;
        if ($filter_course_type || $sort_course_type) {
            $alias = uniqid('teacher_id_');
            $this->datatable_builder
                ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id and {$alias}.meta_key = 'course_type'", 'LEFT');

            if ($filter_course_type) {
                $this->datatable_builder->where("{$alias}.meta_value =", $filter_course_type);
                unset($args['where']['course_type']);
            }

            if ($sort_course_type) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_course_type);
                unset($args['order_by']['course_type']);
            }
        }

        // course_daterange FILTERING & SORTING

        $filter_course_value = $args['where']['course_value'] ?? FALSE;
        $sort_course_value = $args['order_by']['course_value'] ?? FALSE;
        if ($filter_course_value || $sort_course_value) {
            $alias = uniqid('course_value_');
            $this->datatable_builder
                ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id and {$alias}.meta_key = 'course_value'", 'LEFT');
            if ($sort_course_value) {
                $this->datatable_builder->order_by("CAST({$alias}.meta_value as UNSIGNED)",$sort_course_value);
                unset($args['order_by']['course_value'],$args['where']['course_value']);
            }
        }

        $filter_total_student = $args['where']['total_student'] ?? FALSE;
        $sort_total_student = $args['order_by']['total_student'] ?? FALSE;
        if ($filter_total_student || $sort_total_student) {
            $alias = uniqid('total_student_');
            $this->datatable_builder
                ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id and {$alias}.meta_key = 'student_qty'", 'LEFT');
            if ($sort_total_student) {
                $this->datatable_builder->order_by("CAST({$alias}.meta_value as UNSIGNED)",$sort_total_student);
                unset($args['order_by']['total_student'],$args['where']['total_student']);
            }
        }

        $filter_term_status = $args['where']['post_status'] ?? FALSE;
        $sort_term_status = $args['order_by']['post_status'] ?? FALSE;

        if ($filter_term_status || $sort_term_status) {
            if ($filter_term_status) {
                $this->datatable_builder->where('post_status', $filter_term_status);
                unset($args['where']['post_status']);
            }
            if ($sort_term_status) {
                $this->datatable_builder->order_by("post_status", $sort_term_status);
                unset($args['order_by']['post_status']);
            }
        }

        $args = $this->datatable_builder->parse_relations_searches($args);
        if (!empty($args['where'])) {
            foreach ($args['where'] as $key => $value) {
                if (empty($value)) continue;

                if (empty($key)) {
                    $this->datatable_builder->add_filter($value, '');
                    continue;
                }

                $this->datatable_builder->add_filter($key, $value);
            }
        }

        if (!empty($args['order_by'])) {
            foreach ($args['order_by'] as $key => $value) {
                $this->datatable_builder->order_by($key, $value);
            }
        }
    }

    protected function contract_search_filter($search_args = array())
    {
        $args = $this->input->get();

        if (!empty($args['where']))
            $args['where'] = array_map(function ($x) {
                return trim($x);
            }, $args['where']);

        // register_code FILTERING & SORTING
        $filter_register_code = $args['where']['register_code'] ?? FALSE;
        $sort_register_code = $args['order_by']['register_code'] ?? FALSE;
        if ($filter_register_code || $sort_register_code) {
            $alias = uniqid('register_code_');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'register_code'", 'LEFT');

            if ($filter_register_code) {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_register_code);
            }

            if ($sort_register_code) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_register_code);
            }

            unset($args['where']['register_code'], $args['order_by']['register_code']);
        }

        // course_code FILTERING & SORTING
        $filter_course_code = $args['where']['course_code'] ?? FALSE;
        $sort_course_code = $args['order_by']['course_code'] ?? FALSE;
        if ($filter_course_code || $sort_course_code) {
            $alias = uniqid('course_code_');
            $alias_po = uniqid('posts_');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'course_id'", 'LEFT');
            $this->datatable_builder->join("posts {$alias_po}", "{$alias_po}.post_id = {$alias}.meta_value");
            if ($filter_course_code) {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_course_code);
            }

            if ($sort_course_code) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_course_code);
            }

            unset($args['where']['course_code'], $args['order_by']['course_code']);
        }

        // Created_on FILTERING & SORTING
        $filter_created_on = $args['where']['created_on'] ?? FALSE;
        $sort_created_on = $args['order_by']['created_on'] ?? FALSE;
        if ($filter_created_on || $sort_created_on) {
            $alias = uniqid('created_on_');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'created_on'", 'LEFT');

            if ($filter_created_on) {
                $dates = explode(' - ', $filter_created_on);
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['created_on']);
            }

            if ($sort_created_on) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_created_on);
                unset($args['order_by']['created_on']);
            }
        }

        // sale_id FILTERING & SORTING
        $filter_sale_id = $args['where']['sale_id'] ?? FALSE;
        $sort_sale_id = $args['order_by']['sale_id'] ?? FALSE;
        if ($filter_sale_id || $sort_sale_id) {
            $alias = uniqid('sale_id_');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'created_by'", 'LEFT')
                ->join("user tblcustomer", "{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

            if ($filter_sale_id) {
                $this->datatable_builder->like("tblcustomer.display_name", $filter_sale_id);
                unset($args['where']['sale_id']);
            }

            if ($sort_sale_id) {
                $this->datatable_builder->order_by('tblcustomer.display_name', $filter_sale_id);
                unset($args['order_by']['sale_id']);
            }
        }

        // term_status FILTERING & SORTING
        $filter_term_status = $args['where']['term_status'] ?? FALSE;
        $sort_term_status = $args['order_by']['term_status'] ?? FALSE;

        if ($filter_term_status || $sort_term_status) {
            if ($filter_term_status) {
                $this->datatable_builder->where('term.term_status', $filter_term_status);
                unset($args['where']['term_status']);
            }
            if ($sort_term_status) {
                $this->datatable_builder->order_by("term.term_status", $sort_term_status);
                unset($args['order_by']['term_status']);
            }
        }

        $args = $this->datatable_builder->parse_relations_searches($args);
        if (!empty($args['where'])) {
            foreach ($args['where'] as $key => $value) {
                if (empty($value)) continue;

                if (empty($key)) {
                    $this->datatable_builder->add_filter($value, '');
                    continue;
                }

                $this->datatable_builder->add_filter($key, $value);
            }
        }

        if (!empty($args['order_by'])) {
            foreach ($args['order_by'] as $key => $value) {
                $this->datatable_builder->order_by($key, $value);
            }
        }
    }


    public function contract()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);

        if (!has_permission('Courseads.contract.Access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $defaults = ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args = wp_parse_args($this->input->get(), $defaults);

        $data = $this->data; // remove-after

        $term = $this->course_m->set_post_type()->where('post_type', 'course')->get_many_by();
        if ($term) {
            foreach ($term as $value) {
                $data_course[$value->post_id] = $value->post_title;
            }
        }
        
        /* Applies get query params for filter */
        $this->contract_search_filter();

        $this->datatable_builder->set_filter_position(FILTER_TOP_OUTTER)
            ->select('term.term_id,term.term_name')
            ->add_search('register_code', ['placeholder' => 'Mã phiếu đăng ký'])
            // ->add_search('course_code',['placeholder'=>'Mã khóa học'])
            ->add_search('course_code', array('content' => form_dropdown(array('name' => 'where[course_code]', 'class' => 'form-control select2 where_course_code'), prepare_dropdown($data_course, 'Chọn khóa học'), $this->input->get('where[course_code]'))))
            ->add_search('created_on', ['placeholder' => 'Ngày tạo', 'class' => 'form-control input_daterange'])
            ->add_search('sale_id', ['placeholder' => 'Nhân viên kinh doanh', 'class' => 'form-control'])
            ->add_search('term_status', array('content' => form_dropdown(array('name' => 'where[term_status]', 'class' => 'form-control select2'), prepare_dropdown($this->config->item('register_status'), 'Trạng thái phiếu đăng ký'), $this->input->get('where[term_status]'))))
            ->add_search('action',['content'=>form_button(['name'=>'notification_of_payment','class'=>'btn btn-info', 'id' => 'send-mail-payment','type'=>'submit'],'<i class="fa fa-fw fa-send"></i> Email')])
            ->add_column('all_checkbox',array('title'=> '<input type="checkbox" name="all-checkbox" value="" id="all-checkbox" class="minimal"/>','set_order'=> FALSE, 'set_select' => FALSE))
            ->add_column('register_code', array('set_select' => FALSE, 'title' => 'Mã phiếu đăng ký', 'set_order' => TRUE))
            ->add_column('course_code', array('set_select' => FALSE, 'title' => 'Khóa học', 'set_order' => TRUE))
            ->add_column('sale_id', array('set_select' => FALSE, 'title' => 'Nhân viên kinh doanh'))
            ->add_column('register_value', array('set_select' => FALSE, 'title' => 'GTKH'))
            ->add_column('term_status', 'Trạng thái phiếu đăng ký')
            ->add_column('action', array('set_select' => FALSE, 'title' => 'Actions', 'set_order' => FALSE))
            ->add_column_callback('term_id', function ($data, $row_name) {

                $term_id = $data['term_id'];
                $data['all_checkbox'] = '<input type="checkbox"  name="post_ids[]" value="'.$term_id.'" class="minimal post_ids"/>' ;
                $has_send_mail_payment = get_term_meta_value($term_id, 'start_service_time');
    			if($has_send_mail_payment != "")
    			{
    				$data['all_checkbox'] = '<input type="checkbox" name="nocheck-post_ids[]" value="" class="minimal" checked="checked" disabled="disabled"/>' ;
    			}
                /*
                * Mã phiếu đăng ký
                */
                $course_code = get_term_meta_value($term_id, 'contract_code') ?: '<code>Không có mã phiếu đăng ký</code>';

                if (has_permission('Courseads.contract.Update') || has_permission('Courseads.contract.Add')) {
                    $course_code = '<a title="Cập nhật phiếu đăng ký" style="cursor: pointer;" onclick="showEdit(' . $term_id . ')">' . $course_code . '</a>';
                }
                $course_code = "<b>{$course_code}</b></br/>";
                /*
                * Người tạo
                */
                if ($created_by = get_term_meta_value($term_id, 'created_by')) {
                    $created_display_name = $this->admin_m->get_field_by_id($created_by, 'display_name');
                    $course_code .= "<span class='text-muted' data-toggle='tooltip' title='Người tạo'><i class='fa fa-fw fa-user'></i>Tạo bởi :{$created_display_name}</span>";
                }
                /*
                * Ngày tạo
                */
                if ($created_on = get_term_meta_value($term_id, 'created_on')) {
                    $created_datetime = my_date($created_on, 'd/m/Y');
                    $course_code .= "<span class='text-muted' data-toggle='tooltip' title='Ngày tạo'><i class='fa fa-fw  fa-clock-o'></i>Ngày tạo :{$created_datetime}</span>";
                }
                /*
                * Khóa học
                */
                $data['register_code'] = $course_code;
                if ($course_id = get_term_meta_value($term_id, 'course_id')) {
                    $term = $this->course_m->set_post_type()->get($course_id);
                    $data['course_code'] = $term->post_title ?? null;
                }
                /*
                * Giá trị phiếu đăng ký
                */
                $course_value = get_term_meta_value($term_id, 'contract_value');
                $data['register_value'] = currency_numberformat($course_value, 'đ');

                $class_status ='';
                $data['_term_status'] = $data['term_status'];
                if($data['_term_status'] == 'pending') $class_status = 'label label-info';
                if($data['_term_status'] == 'publish') $class_status = 'label label-success';
                if($data['_term_status'] == 'removing') $class_status = 'label label-danger';
                $data['term_status'] = "<span class='{$class_status}'>{$this->config->item($data['term_status'], 'register_status')}</span>";

                /*
                * Nhân viên kinh doanh
                */
                if ($sale_id = get_term_meta_value($term_id, 'created_by')) {
                    $sale_display_name = $this->admin_m->get_field_by_id($sale_id, 'display_name');
                    $sale_id = "<span class='text-muted' data-toggle='tooltip' title='Nhân viên kinh doanh'><i class='fa fa-fw fa-user'></i>Nhân viên kinh doanh :{$sale_display_name}</span>";
                    $data['sale_id'] = $sale_id;
                }

                /*
                * Hành động
                */
                $data['action'] ='';
                if (has_permission('Courseads.contract.Update')) {
                    $data['action'] .= '<a title="Cập nhật khóa học" class="btn btn-default btn-xs" onclick="showEdit(' . $term_id . ')"><i class="fa fa-fw fa-edit"></i></a>';
                }

                if (has_permission('Courseads.Kpi.Access')) {
                    $data['action'] .= '<a title="KPi khóa học" class="btn btn-default btn-xs" href="' . admin_url('courseads/kpi/' . $term_id) . '"><i class="fa fa-fw fa-external-link"></i></a>';
                }
                if (has_permission('Courseads.Setting.Access')) {
                    $data['action'] .= '<a title="Setting khóa học" class="btn btn-default btn-xs" href="' . admin_url('courseads/setting/' . $term_id) . '"><i class="fa fa-fw fa-cogs"></i></a>';
                }
                return $data;
            }, FALSE)
            ->from('term')
            ->where('term_type', 'courseads');
        // if(!has_permission('Courseads.contract.Manage'))
        // {
        //     $alias = uniqid('sale_id_');
        //     $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'created_by'", 'LEFT');
        //     $this->datatable_builder->where('meta_value =',$this->admin_m->id);
        // }
        $this->datatable_builder
            ->where_in('term_type', 'courseads')
            ->group_by('term.term_id');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);


        $this->template->title->set('Quản lý khóa học');
        $this->template->description->set('Trang danh sách tất cả các khóa học');
        return parent::renderJson($data);
    }

    protected function student_search_filter($search_args = array())
    {
        $args = $this->input->get();

        if (!empty($args['where']))
            $args['where'] = array_map(function ($x) {
                return trim($x);
            }, $args['where']);

        // student_name FILTERING & SORTING
        $filter_display_name = $args['where']['display_name'] ?? FALSE;
        $sort_display_name = $args['order_by']['display_name'] ?? FALSE;
        if ($filter_display_name || $sort_display_name) {
            $alias = uniqid('display_name_');

            if ($filter_display_name) {
                $this->datatable_builder->like("display_name", $filter_display_name);
                unset($args['where']['display_name']);
            }

            if ($sort_display_name) {
                $this->datatable_builder->order_by('display_name', $filter_display_name);
                unset($args['order_by']['display_name']);
            }
        }
        // student_email FILTERING & SORTING
        $filter_student_email = $args['where']['student_email'] ?? FALSE;
        $sort_student_email = $args['order_by']['student_email'] ?? FALSE;
        if ($filter_student_email || $sort_student_email) {
            $alias = uniqid('student_email_');

            if ($filter_student_email) {
                $this->datatable_builder->like("user_email", $filter_student_email);
                unset($args['where']['student_email']);
            }

            if ($sort_student_email) {
                $this->datatable_builder->order_by('user_email', $filter_student_email);
                unset($args['order_by']['student_email']);
            }
        }

        // student_phone FILTERING & SORTING
        $filter_student_phone = $args['where']['student_phone'] ?? FALSE;
        $sort_student_phone = $args['order_by']['student_phone'] ?? FALSE;
        if ($filter_student_phone || $sort_student_phone) {
            $alias = uniqid('student_phone_');
            $this->datatable_builder->join("usermeta {$alias}", "{$alias}.user_id = user.user_id and {$alias}.meta_key = 'student_phone'", 'LEFT');

            if ($filter_student_phone) {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_student_phone);
            }

            if ($sort_student_phone) {
                $this->datatable_builder->order_by("({$alias}.meta_value * 1)", $sort_student_phone);
            }

            unset($args['where']['student_phone'], $args['order_by']['student_phone']);
        }

        // course_code FILTERING & SORTING
        $filter_course_code = $args['where']['course_code'] ?? FALSE;
        $sort_course_code = $args['order_by']['course_code'] ?? FALSE;
        if ($filter_course_code || $sort_course_code) {
            $alias = uniqid('course_code_');
            $list_students = get_post_meta_value($filter_course_code, 'register_list');
            $list = explode(',', $list_students);
            if (count($list) > 0) {
                $this->datatable_builder->where_in("user.user_id", $list);
            } else {
                $this->datatable_builder->where_in("user.user_id", ' ');
            }

            unset($args['where']['course_code'], $args['order_by']['course_code']);
        }

        // Created_on FILTERING & SORTING
        $filter_created_on = $args['where']['created_on'] ?? FALSE;
        $sort_created_on = $args['order_by']['created_on'] ?? FALSE;
        if ($filter_created_on || $sort_created_on) {
            $alias = uniqid('created_on_');
            $this->datatable_builder->join("usermeta {$alias}", "{$alias}.user_id = user.user_id and {$alias}.meta_key = 'created_on'", 'LEFT');

            if ($filter_created_on) {
                $dates = explode(' - ', $filter_created_on);
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['created_on']);
            }

            if ($sort_created_on) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_created_on);
                unset($args['order_by']['created_on']);
            }
        }

        // created_by FILTERING & SORTING
        $filter_created_by = $args['where']['created_by'] ?? FALSE;
        $sort_created_by = $args['order_by']['created_by'] ?? FALSE;
        if ($filter_created_by || $sort_created_by) {
            $alias = uniqid('created_by_');
            $sale_staffs = $this->admin_m->select('user_id')
                ->like('user.display_name', $filter_created_by)->get_many_by();
            $this->datatable_builder->join("usermeta {$alias}", "{$alias}.user_id = user.user_id and {$alias}.meta_key = 'created_by'", 'LEFT');
            $list_sale = array_column($sale_staffs, 'user_id');
            if (count($list_sale) > 0) {
                $this->datatable_builder->where_in("{$alias}.meta_value", $list_sale);
            } else {
                $this->datatable_builder->where_in("{$alias}.meta_value", ' ');
            }
            unset($args['where']['created_by']);
            unset($args['order_by']['created_by']);
        }

        // term_status FILTERING & SORTING
        $filter_term_status = $args['where']['term_status'] ?? FALSE;
        $sort_term_status = $args['order_by']['term_status'] ?? FALSE;
        if ($filter_term_status || $sort_term_status) {
            $alias = uniqid('course_code_');
            $term = $this->course_m->set_post_type()->where('term.term_type', 'courseads')->where('term.term_status', $filter_term_status)->get_many_by();
            $term_id = array_column($term, 'term_id');
            $list_student = array();
            foreach ($term_id as $key => $value) {
                $temp_students = get_term_meta_value($value, 'student_list');
                if ($temp_students != null) {
                    $temp_students = explode(',', $temp_students);
                    $list_student = array_merge($list_student, $temp_students);
                }
            }
            if (count($list_student) > 0) {
                $this->datatable_builder->where_in("user.user_id", $list_student);
            } else {
                $this->datatable_builder->where_in("user.user_id", ' ');
            }

            unset($args['where']['term_status'], $args['order_by']['term_status']);
        }

        $args = $this->datatable_builder->parse_relations_searches($args);
        if (!empty($args['where'])) {
            foreach ($args['where'] as $key => $value) {
                if (empty($value)) continue;

                if (empty($key)) {
                    $this->datatable_builder->add_filter($value, '');
                    continue;
                }

                $this->datatable_builder->add_filter($key, $value);
            }
        }

        if (!empty($args['order_by'])) {
            foreach ($args['order_by'] as $key => $value) {
                $this->datatable_builder->order_by($key, $value);
            }
        }
    }

    public function students()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);

        if (!has_permission('Courseads.contract.Access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $defaults = ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args = wp_parse_args($this->input->get(), $defaults);

        $data = $this->data; // remove-after

        $relate_users = $this->admin_m->get_all_by_permissions('Courseads.contract.Access');

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        if ($relate_users === FALSE) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        if (is_array($relate_users)) {
            $this->datatable_builder->join('term_users', 'term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
        }
        $term = $this->course_m->set_post_type()->where('post_type', 'course')->get_many_by();
        if ($term) {
            foreach ($term as $value) {
                $data_course[$value->post_id] = $value->post_title;
            }
        }
        /* Applies get query params for filter */
        $this->student_search_filter();

        $this->datatable_builder->set_filter_position(FILTER_TOP_OUTTER)
            ->select('user.user_id,user.display_name')
            ->add_search('display_name', ['placeholder' => 'Tên học viên'])
            ->add_search('student_email', ['placeholder' => 'Email học viên'])
            ->add_search('student_phone', ['placeholder' => 'Số điện thoại'])
            ->add_search('course_code', array('content' => form_dropdown(array('name' => 'where[course_code]', 'class' => 'form-control select2 where_course_code'), prepare_dropdown($data_course, 'Chọn khóa học'), $this->input->get('where[course_code]'))))
            ->add_search('created_on', ['placeholder' => 'Ngày tạo', 'class' => 'form-control input_daterange'])
            ->add_search('created_by', ['placeholder' => 'Nhân viên tạo', 'class' => 'form-control'])
            ->add_search('term_status', array('content' => form_dropdown(array('name' => 'where[term_status]', 'class' => 'form-control select2'), prepare_dropdown($this->config->item('course_status'), 'Tình trạng khóa học'), $this->input->get('where[term_status]'))))
            ->add_column('student_id', array('set_select' => FALSE, 'title' => 'Học viên', 'set_order' => TRUE))
            ->add_column('user_email', array('title' => 'Email học viên'))
            ->add_column('user_phone', array('set_select' => FALSE, 'title' => 'Số điện thoại học viên'))
            ->add_column('course_code', array('set_select' => FALSE, 'title' => 'Khóa học', 'set_order' => TRUE))
            ->add_column('action', array('set_select' => FALSE, 'title' => 'Actions', 'set_order' => FALSE))
            ->add_column_callback('user_id', function ($data, $row_name) {

                $user_id = $data['user_id'];
                /*
                * Mã phiếu đăng ký
                */
                $student = $data['display_name'] ?: '<code>Không có họ tên</code>';

                if (has_permission('Courseads.contract.Update') || has_permission('Courseads.contract.Add')) {
                    $student = '<a title="Cập nhật thông tin học viên" style="cursor: pointer;" onclick="showEdit(' . $user_id . ')">' . $student . '</a>';
                }
                $student = "<b>{$student}</b></br/>";
                /*
                * Người tạo
                */
                if ($created_by = $this->usermeta_m->get_meta_value($user_id, 'created_by')) {
                    $created_display_name = $this->admin_m->get_field_by_id($created_by, 'display_name');
                    $student .= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Người tạo'><i class='fa fa-fw fa-user'></i>Tạo bởi :{$created_display_name}</span>";
                }
                /*
                * Ngày tạo
                */
                if ($created_on = $this->usermeta_m->get_meta_value($user_id, 'created_on')) {
                    $created_datetime = my_date($created_on, 'd/m/Y');
                    $student .= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Ngày tạo'><i class='fa fa-fw  fa-clock-o'></i>Ngày tạo :{$created_datetime}</span>";
                }
                /*
                * Số điện thoại
                */
                if ($student_phone = $this->usermeta_m->get_meta_value($user_id, 'student_phone')) {
                    $student_phone = "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Số điện thoại'><i class='fa fa-fw  fa-phone'></i>Số điện thoại :{$student_phone}</span>";
                }
                $data['user_phone'] = $student_phone;
                /*
                * Khóa học
                */
                $data['student_id'] = $student;
                $courses = '';
                if ($course_lists = $this->usermeta_m->get_meta_value($user_id, 'course_lists')) {
                    $course_lists = explode(',', $course_lists);
                    foreach ($course_lists as $key => $value) {
                        $term = $this->course_m->set_post_type()->get(intval($value));
                        $name = $term->post_title ?? null;
                        $courses .= "<span class='text-muted col-xs-6' data-toggle='tooltip'><i class='fa fa-fw fa-book'></i>" . $name . "</span>";
                    }
                    $data['course_code'] = $courses;
                }

                /*
                * Nhân viên kinh doanh
                */
                if ($sale_id = $this->admin_m->get_field_by_id($user_id, 'created_by')) {
                    $sale_display_name = $this->admin_m->get_field_by_id($sale_id, 'display_name');
                    $sale_id = "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Nhân viên kinh doanh'><i class='fa fa-fw fa-user'></i>Nhân viên kinh doanh :{$sale_display_name}</span>";
                    $data['sale_id'] = $sale_id;
                }

                /*
                * Hành động->where_in('user_id',"1471,1471,1471,1472")
                */
                if (has_permission('Courseads.contract.Update')) {
                    $data['action'] = '<a title="Cập nhật học viên" class="btn btn-default btn-xs" onclick="showEdit(' . $user_id . ')"><i class="fa fa-fw fa-edit"></i></a>';
                }
                return $data;
            }, FALSE)
            ->from('user')
            ->where('user_type', 'student');

        $this->datatable_builder
            ->where('user_type', 'student')
            ->group_by('user.user_id');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];
        $data = $this->datatable_builder->generate($pagination_config);

        $this->template->title->set('Quản lý khóa học');
        $this->template->description->set('Trang danh sách tất cả các khóa học');
        return parent::renderJson($data);
    }

    public function getCourse()
    {
        $data = array();
        $data['id'] = $this->input->post('course_id');
        $term = $this->course_m->set_post_type()->get($data['id']);
        $data['course_id'] = $term->post_id;
        $data['course_code'] = $term->post_name;
        $data['course_name'] = $term->post_title;
        $data['course_status'] = $term->post_status;
        $data['course_type'] = get_post_meta_value($data['id'], 'course_type');
        $data['teacher_id'] = get_post_meta_value($data['id'], 'teacher_id');
        $data['student_qty'] = get_post_meta_value($data['id'], 'student_qty');
        $data['course_value'] = get_post_meta_value($data['id'], 'course_value');
        $data['course_start'] = $term->start_date;
        $data['course_end'] = $term->end_date;
        $data['course_time'] = date('d-m-Y', $data['course_start']) . ' - ' . date('d-m-Y', $data['course_end']);
        $data['total_course_time'] = get_post_meta_value($data['id'], 'total_course_time');

        $data['days'] = array();
        $data['day_in_week'] = unserialize(get_post_meta_value($data['id'], 'day_in_week'));
        if( ! empty($data['day_in_week']))
        {
            foreach ($data['day_in_week'] as $key => $value) {
                $data['days'][] = $key;
            }
        }

        $data['voucher_start'] = get_post_meta_value($data['id'], 'voucher_start');
        $data['voucher_end'] = get_post_meta_value($data['id'], 'voucher_end');
        if ($data['voucher_start'] || $data['voucher_end']) {
            $data['voucher_time'] = date('d-m-Y', $data['voucher_start']) . ' - ' . date('d-m-Y', $data['voucher_end']);
        }

        $data['voucher_qty'] = get_post_meta_value($data['id'], 'voucher_qty');
        $data['voucher_value'] = get_post_meta_value($data['id'], 'voucher_value');

        return parent::renderJson($data);
    }

    public function getStudent()
    {
        $data['id'] = $this->input->post('student_id');
        $user = $this->user_m->get($data['id']);
        $data['display_name'] = $user->display_name;
        $data['user_email'] = $user->user_email;
        $data['student_phone'] = $this->usermeta_m->get_meta_value($data['id'], 'student_phone');
        return parent::renderJson($data);
    }

    public function getResgister()
    {
        $data = array();
        $data['id'] = $this->input->post('resgister_id');
        $term = $this->courseads_m->set_term_type()->get($data['id']);
        $data['resgister_id'] = $term->term_id;
        $student_id = get_term_meta_value($data['resgister_id'], 'student_id');
        $data['contract_name'] = $this->admin_m->get_field_by_id($student_id, 'display_name');
        $data['contract_email'] = $this->admin_m->get_field_by_id($student_id, 'user_email');
        $data['contract_phone'] = $this->usermeta_m->get_meta_value($student_id, 'student_phone');
        $data['course_id'] = get_term_meta_value($data['resgister_id'], 'course_id');
        $course = $this->course_m->set_post_type()->get($data['course_id']);
        $data['course_name'] = $course->post_title;
        $data['contract_value1'] = get_term_meta_value($data['resgister_id'], 'contract_value1');
        $data['voucher_code'] = get_term_meta_value($data['resgister_id'], 'voucher_code');
        $data['voucher_value'] = get_term_meta_value($data['resgister_id'], 'voucher_value');
        $data['contract_value'] = get_term_meta_value($data['resgister_id'], 'contract_value');
        $data['term_status'] = $term->term_status;

        return parent::renderJson($data);
    }

    public function create()
    {
        $response = array('success' => FALSE, 'msg' => 'Quá trình xử lý thất bại', 'data' => []);

        if (!has_permission('admin.course.add')) {
            $response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            return parent::renderJson($response, 500);
        }
    }
}
