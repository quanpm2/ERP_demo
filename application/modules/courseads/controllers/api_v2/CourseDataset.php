<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class CourseDataset extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'admin_m';

        parent::__construct();

        $this->load->config('courseads/courseads');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('courseads.course.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        // Prepare search
        $courseTypeConfig = $this->config->item('course_type');

        $courseStatusConfig = $this->config->item('course_status');

        // Build datatable
        $data = $this->data;
        $this->search_filter_receipt();

        $data['content'] = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')
            ->select('posts.post_id, posts.post_name, posts.post_author, posts.post_title, posts.post_status, posts.created_on, posts.start_date, posts.end_date')

            ->add_search('post_name', ['placeholder' => 'Mã khoá học'])
            ->add_search('post_title', ['placeholder' => 'Tên khoá học'])
            ->add_search('teacher_name', ['placeholder' => 'Giảng viên phụ trách'])
            ->add_search('course_type', ['content' => form_dropdown(['name' => 'where[course_type]', 'class' => 'form-control select2'], prepare_dropdown($courseTypeConfig, 'Loại'), parent::get('where[course_type]'))])
            ->add_search('post_status', ['content' => form_dropdown(['name' => 'where[post_status]', 'class' => 'form-control select2'], prepare_dropdown($courseStatusConfig, 'Trạng thái'), parent::get('where[post_status]'))])
            ->add_search('course_value', ['placeholder' => 'Giá trị khoá học'])
            ->add_search('start_date', ['placeholder' => 'T/G bắt đầu', 'class' => 'form-control set-datepicker'])
            ->add_search('end_date', ['placeholder' => 'T/G kết thúc', 'class' => 'form-control set-datepicker'])
            ->add_search('total_course_time', ['placeholder' => 'Tổng số buổi học'])
            ->add_search('student_qty', ['placeholder' => 'Số lượng học viên'])
            ->add_search('voucher_value', ['placeholder' => 'Giá trị giảm giá'])
            ->add_search('voucher_qty', ['placeholder' => 'Số lượng giảm giá'])
            ->add_search('post_author', ['placeholder' => 'Người tạo'])
            ->add_search('created_on', ['placeholder' => 'Ngày tạo', 'class' => 'form-control set-datepicker'])

            ->add_column('post_name', array('title' => 'Mã khoá học', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('post_title', array('title' => 'Tên khoá học', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('teacher_name', array('title' => 'Giảng viên phụ trách', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('course_type', array('title' => 'Loại', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('post_status', array('title' => 'Trạng thái', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('course_value', array('title' => 'Giá trị khoá học', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('start_date', array('title' => 'T/G bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('end_date', array('title' => 'T/G kết thúc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('total_course_time', array('title' => 'Tổng số buổi học', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('student_qty', array('title' => 'Số lượng học viên', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('voucher_value', array('title' => 'Giá trị giảm giá', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('voucher_qty', array('title' => 'Số lượng giảm giá', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('post_author', array('title' => 'Người tạo', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('created_on', array('title' => 'Ngày tạo', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('action', array('title' => 'Hành động', 'set_select' => FALSE, 'set_order' => FALSE))

            ->add_column_callback('post_id', function ($data, $row_name) {
                $post_id = (int)$data['post_id'];

                // Get teacher_name
                $teacher_id = get_post_meta_value($post_id, 'teacher_id');
                $teacher = $this->admin_m->set_get_admin()->select('display_name, user_avatar')->where('user_id', $teacher_id)->get_by();
                $data['teacher_id'] = $teacher_id ?: '';
                $data['teacher_name'] = $teacher ? $teacher->display_name : '';
                $data['teacher_avt'] = $teacher ? $teacher->user_avatar : '';

                // Get course_type
                $courseTypeConfig = $this->config->item('course_type');
                $data['course_type_raw'] = get_post_meta_value($post_id, 'course_type');
                $data['course_type'] = $courseTypeConfig[$data['course_type_raw']] ?? '';

                // Get post_status
                $courseStatusConfig = $this->config->item('course_status');
                $data['post_status_raw'] = $data['post_status'];
                $data['post_status'] = $courseStatusConfig[$data['post_status_raw']] ?? '';

                // Get teacher_name
                $data['course_value_raw'] = get_post_meta_value($post_id, 'course_value') ?? 0;
                $data['course_value'] = currency_numberformat($data['course_value_raw'], '');

                // Get start_date
                $data['start_date_raw'] = $data['start_date'];
                $data['start_date'] = '' . $data['start_date_raw'] ? date('d/m/Y', (int)$data['start_date_raw']) : '';

                // Get start_date
                $data['end_date_raw'] = $data['end_date'];
                $data['end_date'] = '' . $data['end_date_raw'] ? date('d/m/Y', (int)$data['end_date_raw']) : '';

                // Get total_course_time
                $data['total_course_time_raw'] = get_post_meta_value($post_id, 'total_course_time');
                $data['total_course_time'] = $data['total_course_time_raw'] ?: '';

                // Get student_qty
                $data['student_qty_raw'] = get_post_meta_value($post_id, 'student_qty');
                $data['student_qty'] = $data['student_qty_raw'] ?: '';

                // Get voucher_value
                $data['voucher_value_raw'] = get_post_meta_value($post_id, 'voucher_value');
                $data['voucher_value'] = $data['voucher_value_raw'] ? currency_numberformat($data['voucher_value_raw'], '') : '';

                // Get voucher_qty
                $data['voucher_qty_raw'] = get_post_meta_value($post_id, 'voucher_qty');
                $data['voucher_qty'] = $data['voucher_qty_raw'];

                // Get post_author
                $data['post_author_raw'] = $data['post_author'];
                $author = $this->admin_m->set_get_admin()->select('display_name')->where('user_id', $data['post_author_raw'])->get_by();
                $data['post_author'] = $author ? $author->display_name : '';

                // Get created_on
                $data['created_on_raw'] = $data['created_on'];
                $data['created_on'] = date('d/m/Y', (int)$data['created_on_raw']);

                return $data;
            }, FALSE)

            ->from('posts')
            ->where('posts.post_type', 'course');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);
        // dd($this->datatable_builder->last_query());

        // // OUTPUT : DOWNLOAD XLSX
        // if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
        //     $this->export_receipt($last_query);
        //     return TRUE;
        // }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function search_filter_receipt($args = array())
    {
        restrict('courseads.course.access');

        $args = parent::get();
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // post_name FILTERING & SORTING
        $filter_post_name = $args['where']['post_name'] ?? FALSE;
        if (!empty($filter_post_name)) {
            $this->datatable_builder->like('posts.post_name', $filter_post_name);
        }

        $sort_post_name = $args['order_by']['post_name'] ?? FALSE;
        if (!empty($sort_post_name)) {
            $this->datatable_builder->order_by('posts.post_name', $sort_post_name);
        }

        // post_title FILTERING & SORTING
        $filter_post_title = $args['where']['post_title'] ?? FALSE;
        if (!empty($filter_post_title)) {
            $this->datatable_builder->like('posts.post_title', $filter_post_title);
        }

        $sort_post_title = $args['order_by']['post_title'] ?? FALSE;
        if (!empty($sort_post_title)) {
            $this->datatable_builder->order_by('posts.post_title', $sort_post_title);
        }

        // teacher_name FILTERING & SORTING
        $filter_teacher_name = $args['where']['teacher_name'] ?? FALSE;
        $sort_teacher_name = $args['order_by']['teacher_name'] ?? FALSE;
        if ($filter_teacher_name || $sort_teacher_name) {
            $aliasMetaTeacher = uniqid('meta_teacher_name_');
            $aliasTeacher = uniqid('user_teacher_');
            $this->datatable_builder->join("postmeta {$aliasMetaTeacher}", "{$aliasMetaTeacher}.post_id = posts.post_id and {$aliasMetaTeacher}.meta_key = 'teacher_id'", 'LEFT')
                ->join("user {$aliasTeacher}", "{$aliasTeacher}.user_id = {$aliasMetaTeacher}.meta_value", 'LEFT');

            if ($filter_teacher_name) {
                $this->datatable_builder->like("{$aliasTeacher}.display_name", $filter_teacher_name);
            }

            if ($sort_teacher_name) {
                $this->datatable_builder->select("{$aliasTeacher}.display_name")
                    ->order_by("{$aliasTeacher}.display_name", $sort_teacher_name);
            }
        }

        // course_type FILTERING & SORTING
        $filter_course_type = $args['where']['course_type'] ?? FALSE;
        $sort_course_type = $args['order_by']['course_type'] ?? FALSE;
        if ($filter_course_type || $sort_course_type) {
            $aliasCourseType = uniqid('meta_course_type_');
            $this->datatable_builder->join("postmeta {$aliasCourseType}", "{$aliasCourseType}.post_id = posts.post_id and {$aliasCourseType}.meta_key = 'course_type'", 'LEFT');

            if ($filter_course_type) {
                $this->datatable_builder->where("{$aliasCourseType}.meta_value", $filter_course_type);
            }

            if ($sort_course_type) {
                $this->datatable_builder->select("{$aliasCourseType}.meta_value")
                    ->order_by("{$aliasCourseType}.meta_value", $sort_course_type);
            }
        }

        // post_status FILTERING & SORTING
        $filter_post_status = $args['where']['post_status'] ?? FALSE;
        if (!empty($filter_post_status)) {
            $this->datatable_builder->like('posts.post_status', $filter_post_status);
        }

        $sort_post_status = $args['order_by']['post_status'] ?? FALSE;
        if (!empty($sort_post_status)) {
            $this->datatable_builder->order_by('posts.post_status', $sort_post_status);
        }

        // course_value FILTERING & SORTING
        $filter_course_value = $args['where']['course_value'] ?? FALSE;
        $sort_course_value = $args['order_by']['course_value'] ?? FALSE;
        if ($filter_course_value || $sort_course_value) {
            $aliasCourseValue = uniqid('meta_course_value_');
            $this->datatable_builder->join("postmeta {$aliasCourseValue}", "{$aliasCourseValue}.post_id = posts.post_id and {$aliasCourseValue}.meta_key = 'course_value'", 'LEFT');

            if ($filter_course_value) {
                $this->datatable_builder->where("{$aliasCourseValue}.meta_value >=", (int)$filter_course_value);
            }

            if ($sort_course_value) {
                $this->datatable_builder->select("{$aliasCourseValue}.meta_value")
                    ->order_by("cast({$aliasCourseValue}.meta_value as unsigned)", $sort_course_value);
            }
        }

        // start_date FILTERING & SORTING
        $filter_start_date = $args['where']['start_date'] ?? FALSE;
        if (!empty($filter_start_date)) {
            $dates = explode(' - ', $args['where']['start_date']);

            $this->datatable_builder
                ->where("posts.start_date >=", $this->mdate->startOfDay(reset($dates)))
                ->where("posts.start_date <=", $this->mdate->endOfDay(end($dates)));
        }

        $sort_start_date = $args['order_by']['start_date'] ?? FALSE;
        if (!empty($sort_start_date)) {
            $this->datatable_builder->order_by('posts.start_date', $sort_start_date);
        }

        // end_date FILTERING & SORTING
        $filter_end_date = $args['where']['end_date'] ?? FALSE;
        if (!empty($filter_end_date)) {
            $dates = explode(' - ', $args['where']['end_date']);

            $this->datatable_builder
                ->where("posts.end_date >=", $this->mdate->startOfDay(reset($dates)))
                ->where("posts.end_date <=", $this->mdate->endOfDay(end($dates)));
        }

        $sort_end_date = $args['order_by']['end_date'] ?? FALSE;
        if (!empty($sort_end_date)) {
            $this->datatable_builder->order_by('posts.end_date', $sort_end_date);
        }

        // total_course_time FILTERING & SORTING
        $filter_total_course_time = $args['where']['total_course_time'] ?? FALSE;
        $sort_total_course_time = $args['order_by']['total_course_time'] ?? FALSE;
        if ($filter_total_course_time || $sort_total_course_time) {
            $aliasCourseValue = uniqid('meta_total_course_time_');
            $this->datatable_builder->join("postmeta {$aliasCourseValue}", "{$aliasCourseValue}.post_id = posts.post_id and {$aliasCourseValue}.meta_key = 'total_course_time'", 'LEFT');

            if ($filter_total_course_time) {
                $this->datatable_builder->where("{$aliasCourseValue}.meta_value >=", (int)$filter_total_course_time);
            }

            if ($sort_total_course_time) {
                $this->datatable_builder->select("{$aliasCourseValue}.meta_value")
                    ->order_by("cast({$aliasCourseValue}.meta_value as unsigned)", $sort_total_course_time);
            }
        }

        // student_qty FILTERING & SORTING
        $filter_student_qty = $args['where']['student_qty'] ?? FALSE;
        $sort_student_qty = $args['order_by']['student_qty'] ?? FALSE;
        if ($filter_student_qty || $sort_student_qty) {
            $aliasCourseValue = uniqid('meta_student_qty_');
            $this->datatable_builder->join("postmeta {$aliasCourseValue}", "{$aliasCourseValue}.post_id = posts.post_id and {$aliasCourseValue}.meta_key = 'student_qty'", 'LEFT');

            if ($filter_student_qty) {
                $this->datatable_builder->where("{$aliasCourseValue}.meta_value >=", (int)$filter_student_qty);
            }

            if ($sort_student_qty) {
                $this->datatable_builder->select("{$aliasCourseValue}.meta_value")
                    ->order_by("cast({$aliasCourseValue}.meta_value as unsigned)", $sort_student_qty);
            }
        }

        // voucher_value FILTERING & SORTING
        $filter_voucher_value = $args['where']['voucher_value'] ?? FALSE;
        $sort_voucher_value = $args['order_by']['voucher_value'] ?? FALSE;
        if ($filter_voucher_value || $sort_voucher_value) {
            $aliasCourseValue = uniqid('meta_voucher_value_');
            $this->datatable_builder->join("postmeta {$aliasCourseValue}", "{$aliasCourseValue}.post_id = posts.post_id and {$aliasCourseValue}.meta_key = 'voucher_value'", 'LEFT');

            if ($filter_voucher_value) {
                $this->datatable_builder->where("{$aliasCourseValue}.meta_value >=", (int)$filter_voucher_value);
            }

            if ($sort_voucher_value) {
                $this->datatable_builder->select("{$aliasCourseValue}.meta_value")
                    ->order_by("cast({$aliasCourseValue}.meta_value as unsigned)", $sort_voucher_value);
            }
        }

        // voucher_qty FILTERING & SORTING
        $filter_voucher_qty = $args['where']['voucher_qty'] ?? FALSE;
        $sort_voucher_qty = $args['order_by']['voucher_qty'] ?? FALSE;
        if ($filter_voucher_qty || $sort_voucher_qty) {
            $aliasCourseValue = uniqid('meta_voucher_qty_');
            $this->datatable_builder->join("postmeta {$aliasCourseValue}", "{$aliasCourseValue}.post_id = posts.post_id and {$aliasCourseValue}.meta_key = 'voucher_qty'", 'LEFT');

            if ($filter_voucher_qty) {
                $this->datatable_builder->where("{$aliasCourseValue}.meta_value >=", (int)$filter_voucher_qty);
            }

            if ($sort_voucher_qty) {
                $this->datatable_builder->select("{$aliasCourseValue}.meta_value")
                    ->order_by("cast({$aliasCourseValue}.meta_value as unsigned)", $sort_voucher_qty);
            }
        }



        // post_author FILTERING & SORTING
        $filter_post_author = $args['where']['post_author'] ?? FALSE;
        $sort_post_author = $args['order_by']['post_author'] ?? FALSE;
        if ($filter_post_author || $sort_post_author) {
            $aliasPostAuthor = uniqid('user_post_author_');
            $this->datatable_builder->join("user {$aliasPostAuthor}", "{$aliasPostAuthor}.user_id = posts.post_author", 'LEFT');

            if ($filter_post_author) {
                $this->datatable_builder->like("{$aliasPostAuthor}.display_name", $filter_post_author);
            }

            if ($sort_post_author) {
                $this->datatable_builder->select("{$aliasPostAuthor}.display_name")
                    ->order_by("{$aliasPostAuthor}.display_name", $sort_post_author);
            }
        }

        // created_on FILTERING & SORTING
        $filter_created_on = $args['where']['created_on'] ?? FALSE;
        if (!empty($filter_created_on)) {
            $dates = explode(' - ', $args['where']['created_on']);

            $this->datatable_builder
                ->where("posts.created_on >=", $this->mdate->startOfDay(reset($dates)))
                ->where("posts.created_on <=", $this->mdate->endOfDay(end($dates)));
        }

        $sort_created_on = $args['order_by']['created_on'] ?? FALSE;
        if (!empty($sort_created_on)) {
            $this->datatable_builder->order_by('posts.created_on', $sort_created_on);
        }
    }
}
/* End of file Dataset.php */
/* Location: ./application/modules/staffs/controllers/api_v2/Dataset.php */