<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contracts extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'courseads/course_m';
        $this->autoload['models'][] = 'courseads/courseads_m';

        parent::__construct($config);
    }

    /**
     * /GET
     *
     * @param      string  $start  The start
     * @param      string  $end    The end
     */
    public function  courses_get()
    {
        $courses = $this->course_m
        ->select('post_id, post_title, post_status')
        ->set_post_type()->get_many_by('post_status', ['pending', 'publish']);

        $courses AND $courses = array_map(function($x){ 
            $x->course_value = (int) get_post_meta_value($x->post_id, 'course_value'); 
            return $x;
        }, $courses);

        parent::response([
            'code' => REST_Controller::HTTP_OK,
            'data' => $courses
        ]);
    }

    /**
     * { function_description }
     *
     * @param      int   $id     The identifier
     */
    public function index_get($id = 0)
    {
        $contract = $this->courseads_m->select('term_id, term_name, term_type, term_status')->set_term_type()->get((int) $id);
        if(empty($contract)) parent::response(['status' => false, 'data' => null]);

        $contractCourses = $this->term_posts_m->get_term_posts($contract->term_id, $this->course_m->post_type);
        if($contractCourses)
        {
            $courses_items = get_term_meta_value($id, 'courses_items');
            $courses_items && is_serialized($courses_items) AND $courses_items = unserialize($courses_items);

            $contractCourses = array_map(function($x) use ($courses_items){

                return [
                    'post_id'       => (int) $x->post_id,
                    'post_author'   => $x->post_author,
                    'post_title'    => $x->post_title,
                    'post_status'   => $x->post_status,
                    'start_date'    => (int) $x->start_date,
                    'end_date'      => (int) $x->end_date,
                    'quantity'      => (int) $courses_items[$x->post_id]['quantity'] ?? 1,
                    'course_value'  => (double) $courses_items[$x->post_id]['course_value'] ?? 0,
                ];

            }, $contractCourses); 
        }

        $contract->courses          = $contractCourses ? array_values($contractCourses) : [];
        $contract->contract_begin   = (int) get_term_meta_value($id, 'contract_begin');
        $contract->contract_end     = (int) get_term_meta_value($id, 'contract_end');
        $contract->discount_amount  = (int) get_term_meta_value($id, 'discount_amount');
        $contract->vat              = (int) get_term_meta_value($id, 'vat');
        
        parent::response([ 'status' => true, 'data' => $contract ]);
    }
}
/* End of file Contracts.php */
/* Location: ./application/modules/courseads/controllers/api_v2/Contracts.php */