<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class CourseContractDataset extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'admin_m';
        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'courseads_contract_m';
        $this->autoload['models'][] = 'course_m';

        parent::__construct();

        $this->load->config('courseads/courseads');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('courseads.contract.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        $relate_users = $this->admin_m->get_all_by_permissions('courseads.index.access');
        if (!$relate_users) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }
        if (is_array($relate_users)) {
            $this->datatable_builder
                ->join('term_users','term_users.term_id = term.term_id')
                ->where_in('term_users.user_id', $relate_users);
        }

        // Prepare search
        $courses = $this->course_m->set_post_type()->get_all();
        $courses = $courses ? key_value($courses, 'post_id', 'post_title') : array();

        $register_status = $this->config->item('register_status');

        // Build datatable
        $data = $this->data;
        $this->search_filter_receipt();

        $data['content'] = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')
            ->select('term.term_id, term.term_status')

            ->add_search('contract_code', ['placeholder' => 'Mã HĐ'])
            ->add_search('customer', ['placeholder' => 'Khách hàng', 'class' => 'form-control'])
            ->add_search('course_name', array('content' => form_dropdown(array('name' => 'where[course_name]', 'class' => 'form-control select2 where_course_code'), prepare_dropdown($courses, 'Khóa học : All'), $this->input->get('where[course_name]'))))
            ->add_search('staff_business', ['placeholder' => 'Kinh doanh phụ trách', 'class' => 'form-control'])
            ->add_search('contract_value', ['placeholder' => 'Giá trị HĐ', 'class' => 'form-control'])
            ->add_search('term_status', array('content' => form_dropdown(array('name' => 'where[term_status]', 'class' => 'form-control select2'), prepare_dropdown($register_status, 'Trạng thái : All'), $this->input->get('where[term_status]'))))

            // ->add_search('action', ['content' => form_button(['name' => 'notification_of_payment', 'class' => 'btn btn-info', 'id' => 'send-mail-payment', 'type' => 'submit'], '<i class="fa fa-fw fa-send"></i> Email')])

            ->add_column('contract_code', array('title' => 'Mã HĐ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('customer', array('title' => 'Khách hàng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('course_name', array('title' => 'Khóa học', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('staff_business', array('title' => 'Kinh doanh phụ trách', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_value', array('title' => 'Giá trị HĐ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('term_status', array('title' => 'Trạng thái', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('action', array('title' => 'Hành động', 'set_select' => FALSE,  'set_order' => FALSE))

            ->add_column_callback('term_id', function ($data, $row_name) use ($register_status) {
                $term_id = (int)$data['term_id'];

                $data['term_id'] = $term_id;

                // Get contract_code
                $contract_code = get_term_meta_value($term_id, 'contract_code') ?? '';
                $data['contract_code'] = $contract_code;

                // Get customer
                $customer = $this->term_users_m->get_the_users($term_id, array('customer_person', 'customer_company'));
                if ($customer) {
                    $customer             = reset($customer);
                    $data['customer']     = $this->admin_m->get_field_by_id($customer->user_id, 'display_name');
                } else {
                    $data['customer'] = '';
                }

                // Get course_name
                $courses_items = unserialize(get_term_meta_value($term_id, 'courses_items'));
                $data['course_name'] = $courses_items ? implode(array_column($courses_items, 'post_title')) : '';

                // Get staff_business
                $staff_business = (int) get_term_meta_value($term_id, 'staff_business');
                $data['staff_business_id'] = $staff_business ? $this->admin_m->get_field_by_id($staff_business, 'user_id') : '';
                $data['staff_business_name'] = $staff_business ? $this->admin_m->get_field_by_id($staff_business, 'display_name') : '';
                $data['staff_business_avt'] = $staff_business ? $this->admin_m->get_field_by_id($staff_business, 'user_avatar') : '';

                // Get contract_value
                $contract_value = get_term_meta_value($term_id, 'contract_value');
                $data['contract_value'] = currency_numberformat($contract_value, '');

                // Get term_status
                $data['term_status_raw'] = $data['term_status'];
                $data['term_status'] = $register_status[$data['term_status_raw']] ?? '';

                // Add on
                $data['is_service_proc'] = is_service_proc($term_id);
                $data['is_service_end'] = is_service_end($term_id);

                return $data;
            }, FALSE)
            ->from('term')
            ->where('term_type', 'courseads')
            ->where('term_status', 'publish')
            ->group_by('term.term_id');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);
        // dd($this->datatable_builder->last_query());

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export_courseads($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function search_filter_receipt($args = array())
    {
        restrict('courseads.contract.access');

        $args = parent::get();
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $aliasCourseType = uniqid('meta_contract_code_');
            $this->datatable_builder->join("termmeta {$aliasCourseType}", "{$aliasCourseType}.term_id = term.term_id and {$aliasCourseType}.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("{$aliasCourseType}.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder->select("{$aliasCourseType}.meta_value")
                    ->order_by("{$aliasCourseType}.meta_value", $sort_contract_code);
            }
        }

        // customer FILTERING & SORTING
        $filter_customer = $args['where']['customer'] ?? FALSE;
        $sort_customer = $args['order_by']['customer'] ?? FALSE;
        if ($filter_customer || $sort_customer) {
            $alias = uniqid('contract_customers_');
            $alias_customer_tbl = uniqid("{$alias}_tbl_");

            $this->datatable_builder
                ->join("term_users {$alias}", "{$alias}.term_id = term.term_id")
                ->join("user {$alias_customer_tbl}", "{$alias_customer_tbl}.user_id = {$alias}.user_id and {$alias_customer_tbl}.user_type in ('customer_person', 'customer_company')");

            if ($filter_customer) {
                $this->datatable_builder->like("{$alias_customer_tbl}.display_name", $filter_customer);
            }

            if ($sort_customer) {
                $this->datatable_builder
                    ->select("{$alias_customer_tbl}.user_id")
                    ->order_by("{$alias_customer_tbl}.display_name", $sort_customer)
                    ->group_by("{$alias_customer_tbl}.user_id");
            }
        }

        // contract_value FILTERING & SORTING
        $filter_contract_value = $args['where']['contract_value'] ?? FALSE;
        $sort_contract_value = $args['order_by']['contract_value'] ?? FALSE;
        if ($filter_contract_value || $sort_contract_value) {
            $alias_contract_value = uniqid('meta_contract_value_');

            $this->datatable_builder->join("termmeta {$alias_contract_value}", "{$alias_contract_value}.term_id = term.term_id and {$alias_contract_value}.meta_key = 'contract_value'", 'LEFT');

            if ($filter_contract_value) {
                $this->datatable_builder->where("{$alias_contract_value}.meta_value >=", (int)$filter_contract_value);
            }

            if ($sort_contract_value) {
                $this->datatable_builder->select("{$alias_contract_value}.meta_value")
                    ->order_by("cast({$alias_contract_value}.meta_value as unsigned)", $sort_contract_value)
                    ->group_by("{$alias_contract_value}.meta_value");
            }
        }

        // course_name FILTERING & SORTING
        $filter_course_name = $args['where']['course_name'] ?? FALSE;
        $sort_course_name = $args['order_by']['course_name'] ?? FALSE;
        if ($filter_course_name || $sort_course_name) {
            $alias_term_posts = uniqid('term_posts_');
            $alias_posts = uniqid('posts_');

            $this->datatable_builder->join("term_posts {$alias_term_posts}", "term.term_id = {$alias_term_posts}.term_id")
                ->join("posts {$alias_posts}", "{$alias_posts}.post_id = {$alias_term_posts}.post_id")
                ->where("{$alias_posts}.post_type = 'course'");

            if ($filter_course_name) {
                $this->datatable_builder->where("{$alias_posts}.post_id", $filter_course_name);
            }

            if ($sort_course_name) {
                $this->datatable_builder->order_by("{$alias_posts}.post_id", $sort_course_name)
                    ->group_by("{$alias_posts}.post_id");
            }
        }

        // staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if ($filter_staff_business || $sort_staff_business) {
            $alias = uniqid('staff_business_');
            $alias_staff = uniqid('staff_business_');

            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id AND {$alias}.meta_key = 'staff_business'", 'LEFT')
                ->join("user {$alias_staff}", "{$alias}.meta_value = {$alias_staff}.user_id AND {$alias_staff}.user_type = 'admin'");

            if ($filter_staff_business) {
                $this->datatable_builder->like("{$alias_staff}.display_name", $filter_staff_business);
            }

            if ($sort_staff_business) {
                $this->datatable_builder->order_by("{$alias_staff}.display_name", $sort_staff_business)
                    ->group_by("{$alias_staff}.user_id");
            }
        }

        // term_status FILTERING & SORTING
        $filter_term_status = $args['where']['term_status'] ?? FALSE;
        if ($filter_term_status) {
            $this->datatable_builder->where('term.term_status', $filter_term_status);
        }

        $sort_term_status = $args['order_by']['term_status'] ?? FALSE;
        if ($sort_term_status) {
            $this->datatable_builder->order_by('term.term_status', $sort_term_status);
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function export_courseads($query = '')
    {
        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $courses = $this->courseads_contract_m->query($query)->result();
        if (!$courses) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $this->load->library('excel');
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array('memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
            ->getProperties()
            ->setCreator("WEBDOCTOR.VN")
            ->setLastModifiedBy("WEBDOCTOR.VN")
            ->setTitle(uniqid('Danh sách dịch vụ Khoá học __'));

        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/courseads/course-contract-list-export.xlsx');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $row_index = 3;

        foreach ($courses as $key => &$course) {
            // Prepare data
            $term_id = (int)$course->term_id;

            $contract_code = get_term_meta_value($term_id, 'contract_code') ?? '--';

            $customer_display_name = '--';
            $customer = $this->term_users_m->get_the_users($term_id, array('customer_person', 'customer_company'));
            if ($customer) {
                $customer = $customer ? reset($customer) : NULL;
                $customer_display_name = $this->admin_m->get_field_by_id($customer->user_id, 'display_name');
            }

            $courses_items = unserialize(get_term_meta_value($term_id, 'courses_items'));
            $course_name = $courses_items ? implode(array_column($courses_items, 'post_title')) : '';

            $staff_business = (int) get_term_meta_value($term_id, 'staff_business');
            $staff_business_name = $staff_business ? $this->admin_m->get_field_by_id($staff_business, 'display_name') : '--';

            $contract_end = get_term_meta_value($course->term_id, 'contract_end');
            $contract_end = $contract_end ? my_date($contract_end, 'd/m/Y') : '--';

            $quantity = get_term_meta_value($course->term_id, 'quantity');
            $quantity = '' . $quantity ? (int)$quantity : '--';

            $contract_value = get_term_meta_value($course->term_id, 'contract_value');
            $contract_value = $contract_value ?? '--';

            $register_status = $this->config->item('register_status');
            $status = $course->term_status;
            $status_label = $register_status[$status] ?? '--';

            $row_number = $row_index + $key;

            $i = 0;

            // Set Cell
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $key + 1);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_code);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $course_name);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $customer_display_name);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $staff_business_name);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_value);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $status_label);
        }

        $numbers_of_course = count($courses);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $row_index . ':A' . ($numbers_of_course + $row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

        $objPHPExcel->getActiveSheet()
            ->getStyle('F' . $row_index . ':F' . ($numbers_of_course + $row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $file_name = "tmp/export-courseads-service.xlsx";
        $objWriter->save($file_name);

        try {
            $objWriter->save($file_name);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        if (file_exists($file_name)) {
            $this->load->helper('download');
            force_download($file_name, NULL);
        }

        return FALSE;
    }
}
/* End of file Dataset.php */
/* Location: ./application/modules/staffs/controllers/api_v2/Dataset.php */