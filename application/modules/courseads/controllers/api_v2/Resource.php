<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class Resource extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'form_validation';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'log_m';
        $this->autoload['models'][] = 'admin_m';
        $this->autoload['models'][] = 'staffs/staffs_m';
        $this->autoload['models'][] = 'courseads_m';
        $this->autoload['models'][] = 'courseads_contract_m';
        $this->autoload['models'][] = 'course_m';

        parent::__construct();

        $this->load->config('courseads/courseads');
    }

    /**
     * @return json
     */
    public function configs_get()
    {
        if (!has_permission('courseads.course.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $course_type = $this->config->item('course_type');
        $courses_list = $this->config->item('courses_list');

        $data = [
            'course_type' => $course_type,
            'courses_list' => $courses_list
        ];

        return parent::responseHandler($data, 'success');
    }

    /**
     * @return json
     */
    public function teachers_get()
    {
        if (!has_permission('courseads.course.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $teachers = $this->admin_m->select('user_id, display_name, user_email')
            ->where('user.user_type', 'admin')
            ->as_array()
            ->get_many_by();
        if (empty($teachers)) {
            return parent::responseHandler([], 'success', 'Không tìm thấy danh sách giảng viên.', 204);
        }

        return parent::responseHandler($teachers, 'success');
    }

    /**
     * @return json
     */
    public function create_course_post()
    {

        if (!has_permission('courseads.course.add')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $default = [
            'course_code' => '',
            'course_name' => '',
            'course_type' => '',
            'teacher' => '',
            'student_qty' => '',
            'course_value' => '',
            'course_time_begin' => '',
            'course_time_end' => '',
            'total_course_time' => '',
            'voucher_time_begin' => '',
            'voucher_time_end' => '',
            'voucher_value' => '',
            'voucher_qty' => '',
        ];
        $inputs = wp_parse_args(parent::post(), $default);

        // Validate
        $course_type = implode(',', array_keys($this->config->item('course_type')));
        $schedulers = implode(',', array_keys($this->config->item('schedulers')));

        $rules = array(
            [
                'field' => 'course_code',
                'label' => 'course_code',
                'rules' => [
                    'required',
                    "in_list[$schedulers]"
                ]
            ],
            [
                'field' => 'course_type',
                'label' => 'course_type',
                'rules' => [
                    'required',
                    "in_list[$course_type]"
                ]
            ],
            [
                'field' => 'teacher',
                'label' => 'teacher',
                'rules' => [
                    'required',
                    array('existed_check', array($this->staffs_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'student_qty',
                'label' => 'student_qty',
                'rules' => [
                    'required',
                    'is_natural_no_zero',
                ]
            ],
            [
                'field' => 'course_value',
                'label' => 'course_value',
                'rules' => [
                    'required',
                    'is_natural_no_zero',
                ]
            ],
            [
                'field' => 'course_time_begin',
                'label' => 'course_time_begin',
                'rules' => [
                    'required',
                    'is_natural_no_zero',
                    array('check_date', function ($value) {
                        if (!$this->checkValidateTime((int)$value)) {
                            $this->form_validation->set_message('check_date', 'Định dạng ngày tháng không đúng');
                            return FALSE;
                        }

                        return TRUE;
                    })
                ]
            ],
            [
                'field' => 'course_time_end',
                'label' => 'course_time_end',
                'rules' => [
                    'required',
                    'is_natural_no_zero',
                    array('check_date', function ($value) {
                        if (!$this->checkValidateTime((int)$value)) {
                            $this->form_validation->set_message('check_date', 'Định dạng ngày tháng không đúng');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                    array('course_time_end_callable', function ($value) use ($inputs) {
                        $course_time_begin = (int)$inputs['course_time_begin'];
                        $course_time_end = (int)$value;
                        $time_diff = $course_time_end - $course_time_begin;

                        if ($time_diff < 0) {
                            $this->form_validation->set_message('course_time_end_callable', 'Ngày kết thúc là ngày hiện tại hoặc sau đó.');
                            return FALSE;
                        }

                        return TRUE;
                    })
                ]
            ],
            [
                'field' => 'total_course_time',
                'label' => 'total_course_time',
                'rules' => [
                    'required',
                    'is_natural_no_zero',
                ]
            ],
            [
                'field' => 'voucher_time_begin',
                'label' => 'voucher_time_begin',
                'rules' => [
                    'is_natural_no_zero',
                    array('check_empty_voucher_time_end', function ($value) use ($inputs) {
                        if (empty($value)) {
                            return TRUE;
                        }

                        if (empty($inputs['voucher_time_end'])) {
                            $this->form_validation->set_message('check_empty_voucher_time_end', 'Thời gian kết thúc giảm giá không được bỏ trống');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                    array('check_date', function ($value) {
                        if (empty($value)) {
                            return TRUE;
                        }

                        if (!$this->checkValidateTime((int)$value)) {
                            $this->form_validation->set_message('check_date', 'Định dạng ngày tháng không đúng');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                ]
            ],
            [
                'field' => 'voucher_time_end',
                'label' => 'voucher_time_end',
                'rules' => [
                    'is_natural_no_zero',
                    array('check_date', function ($value) {
                        if (empty($value)) {
                            return TRUE;
                        }

                        if (!$this->checkValidateTime((int)$value)) {
                            $this->form_validation->set_message('check_date', 'Định dạng ngày tháng không đúng');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                    array('check_empty_voucher_time_begin', function ($value) use ($inputs) {
                        if (empty($value)) {
                            return TRUE;
                        }

                        if (empty($inputs['voucher_time_begin'])) {
                            $this->form_validation->set_message('check_empty_voucher_time_begin', 'Thời gian bắt đầu giảm giá không được bỏ trống');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                    array('voucher_time_end_callable', function ($value) use ($inputs) {
                        if (empty($value)) {
                            return TRUE;
                        }

                        $course_time_begin = (int)$inputs['course_time_begin'];
                        $voucher_time_end = (int)$value;
                        $time_diff = $voucher_time_end - $course_time_begin;

                        if ($time_diff < 0) {
                            $this->form_validation->set_message('voucher_time_end_callable', 'Thời gian kết thúc giảm giá là ngày hiện tại hoặc sau đó.');
                            return FALSE;
                        }

                        return TRUE;
                    })
                ]
            ],
            [
                'field' => 'voucher_value',
                'label' => 'voucher_value',
                'rules' => [
                    'is_natural_no_zero',
                    array('check_invalid', function ($value) use ($inputs) {
                        if (empty($inputs['voucher_time_begin']) || empty($inputs['voucher_time_end'])) {
                            return TRUE;
                        }

                        if (empty($value)) {
                            $this->form_validation->set_message('check_invalid', 'Giá trị giảm giá không được để trống.');
                            return FALSE;
                        }

                        if ((int)$value > (int)$inputs['course_value']) {
                            $this->form_validation->set_message('check_invalid', 'Giá trị giảm giá phải nhỏ hơn hoặc bằng giá trị khoá học.');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                ]
            ],
            [
                'field' => 'voucher_qty',
                'label' => 'voucher_qty',
                'rules' => [
                    'is_natural_no_zero',
                    array('check_invalid', function ($value) use ($inputs) {
                        if (empty($inputs['voucher_value'])) {
                            return TRUE;
                        }

                        if (empty($value)) {
                            $this->form_validation->set_message('check_invalid', 'Số lượng giảm giá không được để trống');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $post_data = [
            'post_title' => $inputs['course_name'],
            'post_name' => $inputs['course_code'],
            'start_date' => $this->mdate->startOfDay($inputs['course_time_begin']),
            'end_date' => $this->mdate->endOfDay($inputs['course_time_end']),
            'post_type' => 'course',
            'created_on' => time(),
            'post_status' => 'pending',
        ];

        $post_meta = [
            'teacher_id' => $inputs['teacher'],
            'student_qty' => $inputs['student_qty'],
            'course_value' => $inputs['course_value'],
            'total_course_time' => $inputs['total_course_time'],
            'course_type' => $inputs['course_type'],
            'voucher_start' => $inputs['voucher_time_begin'],
            'voucher_end' => $inputs['voucher_time_end'],
            'voucher_value' => $inputs['voucher_value'],
            'voucher_qty' => $inputs['voucher_qty']
        ];

        $insert_id = $this->course_m->insert($post_data);
        foreach ($post_meta as $key  => $value) {
            update_post_meta($insert_id, $key, $value);
        }

        $response = array_merge($post_data, $post_meta, ['id' => $insert_id]);
        return parent::responseHandler($response, 'success');
    }

    /**
     * @return json
     */
    public function course_get($course_id)
    {
        if (!has_permission('courseads.course.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), ['course_id' => $course_id]);

        // Validate
        $rules = array(
            [
                'field' => 'course_id',
                'label' => 'course_id',
                'rules' => [
                    'required',
                    array('existed_check', array($this->course_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $course = $this->course_m->set_post_type()
            ->where('posts.post_id', $course_id)
            ->as_array()
            ->get_by();

        $response = [
            'course_id' => (int)$course['post_id'],
            'course_code' => $course['post_name'],
            'course_name' => $course['post_title'],
            'course_type' => get_post_meta_value($course['post_id'], 'course_type'),
            'teacher' => (int)get_post_meta_value($course['post_id'], 'teacher_id'),
            'student_qty' => (int)get_post_meta_value($course['post_id'], 'student_qty'),
            'course_value' => (int)get_post_meta_value($course['post_id'], 'course_value'),
            'course_time_begin' => (int)$course['start_date'],
            'course_time_end' => (int)$course['end_date'],
            'total_course_time' => (int)get_post_meta_value($course['post_id'], 'total_course_time'),
            'voucher_time_begin' => (int)get_post_meta_value($course['post_id'], 'voucher_start'),
            'voucher_time_end' => (int)get_post_meta_value($course['post_id'], 'voucher_end'),
            'voucher_value' => (int)get_post_meta_value($course['post_id'], 'voucher_value'),
            'voucher_qty' => (int)get_post_meta_value($course['post_id'], 'voucher_qty'),
        ];

        return parent::responseHandler($response, 'success');
    }

    /**
     * @return json
     */
    public function course_put($post_id)
    {

        if (!has_permission('courseads.course.update')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $default = [
            'post_id' => $post_id,
            'course_type' => '',
            'teacher' => '',
            'student_qty' => '',
            'course_value' => '',
            'course_time_begin' => '',
            'course_time_end' => '',
            'total_course_time' => '',
            'voucher_time_begin' => '',
            'voucher_time_end' => '',
            'voucher_value' => '',
            'voucher_qty' => '',
        ];
        $inputs = wp_parse_args(parent::put(), $default);

        // Validate
        $rules = array(
            [
                'field' => 'post_id',
                'label' => 'post_id',
                'rules' => [
                    'required',
                    array('existed_check', array($this->course_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'teacher',
                'label' => 'teacher',
                'rules' => [
                    'required',
                    array('existed_check', array($this->staffs_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'student_qty',
                'label' => 'student_qty',
                'rules' => [
                    'required',
                    'is_natural_no_zero',
                ]
            ],
            [
                'field' => 'course_value',
                'label' => 'course_value',
                'rules' => [
                    'required',
                    'is_natural_no_zero',
                ]
            ],
            [
                'field' => 'course_time_begin',
                'label' => 'course_time_begin',
                'rules' => [
                    'required',
                    'is_natural_no_zero',
                    array('check_date', function ($value) {
                        if (!$this->checkValidateTime((int)$value)) {
                            $this->form_validation->set_message('check_date', 'Định dạng ngày tháng không đúng');
                            return FALSE;
                        }

                        return TRUE;
                    })
                ]
            ],
            [
                'field' => 'course_time_end',
                'label' => 'course_time_end',
                'rules' => [
                    'required',
                    'is_natural_no_zero',
                    array('check_date', function ($value) {
                        if (!$this->checkValidateTime((int)$value)) {
                            $this->form_validation->set_message('check_date', 'Định dạng ngày tháng không đúng');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                    array('course_time_end_callable', function ($value) use ($inputs) {
                        $course_time_begin = (int)$inputs['course_time_begin'];
                        $course_time_end = (int)$value;
                        $time_diff = $course_time_end - $course_time_begin;

                        if ($time_diff < 0) {
                            $this->form_validation->set_message('course_time_end_callable', 'Ngày kết thúc là ngày hiện tại hoặc sau đó.');
                            return FALSE;
                        }

                        return TRUE;
                    })
                ]
            ],
            [
                'field' => 'total_course_time',
                'label' => 'total_course_time',
                'rules' => [
                    'required',
                    'is_natural_no_zero',
                ]
            ],
            [
                'field' => 'voucher_time_begin',
                'label' => 'voucher_time_begin',
                'rules' => [
                    'is_natural_no_zero',
                    array('check_empty_voucher_time_end', function ($value) use ($inputs) {
                        if (empty($value)) {
                            return TRUE;
                        }

                        if (empty($inputs['voucher_time_end'])) {
                            $this->form_validation->set_message('check_empty_voucher_time_end', 'Thời gian kết thúc giảm giá không được bỏ trống');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                    array('check_date', function ($value) {
                        if (empty($value)) {
                            return TRUE;
                        }

                        if (!$this->checkValidateTime((int)$value)) {
                            $this->form_validation->set_message('check_date', 'Định dạng ngày tháng không đúng');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                ]
            ],
            [
                'field' => 'voucher_time_end',
                'label' => 'voucher_time_end',
                'rules' => [
                    'is_natural_no_zero',
                    array('check_date', function ($value) {
                        if (empty($value)) {
                            return TRUE;
                        }

                        if (!$this->checkValidateTime((int)$value)) {
                            $this->form_validation->set_message('check_date', 'Định dạng ngày tháng không đúng');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                    array('check_empty_voucher_time_begin', function ($value) use ($inputs) {
                        if (empty($value)) {
                            return TRUE;
                        }

                        if (empty($inputs['voucher_time_begin'])) {
                            $this->form_validation->set_message('check_empty_voucher_time_begin', 'Thời gian bắt đầu giảm giá không được bỏ trống');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                    array('voucher_time_end_callable', function ($value) use ($inputs) {
                        if (empty($value)) {
                            return TRUE;
                        }

                        $course_time_begin = (int)$inputs['course_time_begin'];
                        $voucher_time_end = (int)$value;
                        $time_diff = $voucher_time_end - $course_time_begin;

                        if ($time_diff < 0) {
                            $this->form_validation->set_message('voucher_time_end_callable', 'Thời gian kết thúc giảm giá là ngày hiện tại hoặc sau đó.');
                            return FALSE;
                        }

                        return TRUE;
                    })
                ]
            ],
            [
                'field' => 'voucher_value',
                'label' => 'voucher_value',
                'rules' => [
                    'is_natural_no_zero',
                    array('check_invalid', function ($value) use ($inputs) {
                        if (empty($inputs['voucher_time_begin']) || empty($inputs['voucher_time_end'])) {
                            return TRUE;
                        }

                        if (empty($value)) {
                            $this->form_validation->set_message('check_invalid', 'Giá trị giảm giá không được để trống.');
                            return FALSE;
                        }

                        if ((int)$value > (int)$inputs['course_value']) {
                            $this->form_validation->set_message('check_invalid', 'Giá trị giảm giá phải nhỏ hơn hoặc bằng giá trị khoá học.');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                ]
            ],
            [
                'field' => 'voucher_qty',
                'label' => 'voucher_qty',
                'rules' => [
                    'is_natural_no_zero',
                    array('check_invalid', function ($value) use ($inputs) {
                        if (empty($inputs['voucher_value'])) {
                            return TRUE;
                        }

                        if (empty($value)) {
                            $this->form_validation->set_message('check_invalid', 'Số lượng giảm giá không được để trống');
                            return FALSE;
                        }

                        return TRUE;
                    }),
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $post_data = [
            'start_date' => $this->mdate->startOfDay($inputs['course_time_begin']),
            'end_date' => $this->mdate->endOfDay($inputs['course_time_end']),
            'post_type' => 'course',
            'created_on' => time(),
            'post_status' => 'pending',
        ];

        $post_meta = [
            'teacher_id' => $inputs['teacher'],
            'student_qty' => $inputs['student_qty'],
            'course_value' => $inputs['course_value'],
            'total_course_time' => $inputs['total_course_time'],
            'course_type' => $inputs['course_type'],
            'voucher_start' => $inputs['voucher_time_begin'],
            'voucher_end' => $inputs['voucher_time_end'],
            'voucher_value' => $inputs['voucher_value'],
            'voucher_qty' => $inputs['voucher_qty']
        ];

        $this->course_m->update($post_id, $post_data);
        foreach ($post_meta as $key  => $value) {
            update_post_meta($post_id, $key, $value);
        }

        $response = array_merge($post_data, $post_meta, ['id' => $post_id]);
        return parent::responseHandler($response, 'success');
    }

    /**
     * @return json
     */
    public function student_get($contractId)
    {
        if (!has_permission('courseads.setting.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), ['contractId' => $contractId]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->courseads_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $guru_students = unserialize(get_term_meta_value($contractId, 'guru_students'));
        if (empty($guru_students)) {
            return parent::responseHandler(null, 'Không tìm thấy học viên.', 'success', 204);
        }

        return parent::responseHandler($guru_students, 'Không tìm thấy học viên.', 'success', 204);
    }

    /**
     * @return json
     */
    public function student_put($contractId)
    {
        if (!has_permission('courseads.setting.update')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        if (!(has_permission('courseads.setting.manage') || has_permission('courseads.setting.mdeparment') || has_permission('courseads.setting.mgroup'))) 
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);

        $inputs = wp_parse_args(parent::put(), ['contractId' => $contractId]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->courseads_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'students[]',
                'label' => 'students',
                'rules' => [
                    'required',
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $count = count($inputs['students']);
        if ($count) {
            $i = 0;
            $curators = array();
            for ($i; $i < $count; $i++) {
                if (empty($inputs['students'][$i]['name']) || empty($inputs['students'][$i]['phone']) || empty($inputs['students'][$i]['email'])) {
                    return parent::responseHandler(null, 'Thông tin học viên chưa đầy đủ', 'error', 400);
                }

                $curators[] = array(
                    'name' => $inputs['students'][$i]['name'],
                    'phone' => $inputs['students'][$i]['phone'],
                    'email' => $inputs['students'][$i]['email']
                );
            }

            $meta_value = serialize($curators);
            update_term_meta($inputs['contractId'], 'guru_students', $meta_value);

            $this->courseads_contract_m->count_customers($inputs['contractId']);
        }

        return parent::responseHandler(null, 'Lưu thông tin thành công');
    }

    /**
     * @return json
     */
    public function detail_get($contractId)
    {
        if (!has_permission('courseads.overview.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::put(), ['contractId' => $contractId]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->courseads_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        // Process
        $data = [
            'contract' => [],
            'customers' => [],
            'courseads' => [],
        ];

        // Contract detail
        $contract_code = get_term_meta_value($inputs['contractId'], 'contract_code');
        $contract_begin = get_term_meta_value($inputs['contractId'], 'contract_begin');
        $contract_end = get_term_meta_value($inputs['contractId'], 'contract_end');
        $contract_start_service = get_term_meta_value($inputs['contractId'], 'start_service_time');
        $contract_start_service = get_term_meta_value($inputs['contractId'], 'start_service_time');
        $staff_business = get_term_meta_value($inputs['contractId'], 'staff_business');
        $is_service_proc = is_service_proc($inputs['contractId']);
        $is_service_end = is_service_end($inputs['contractId']);

        $representative_gender = get_term_meta_value($inputs['contractId'], 'representative_gender');
        $representative_name = get_term_meta_value($inputs['contractId'], 'representative_name');
        $representative_email = get_term_meta_value($inputs['contractId'], 'representative_email');
        $representative_phone = get_term_meta_value($inputs['contractId'], 'representative_phone');

        $data['contract'] = [
            'contract_code' => $contract_code,
            'contract_start_service' => $contract_start_service ? my_date($contract_start_service, 'h:i:s d/m/Y') : '--',
            'contract_begin' => $contract_begin ? my_date($contract_begin, 'd/m/Y') : '--',
            'contract_end' => $contract_end ? my_date($contract_end, 'd/m/Y') : '--',
            'staff_business' => $staff_business ? $this->admin_m->get_field_by_id($staff_business, 'display_name') : '--',
            'representative_gender' => (bool) $representative_gender ? 'Ông' : 'Bà',
            'representative_name' => $representative_name,
            'representative_email' => $representative_email,
            'representative_phone' => $representative_phone,
        ];

        if (has_permission('courseads.start_service') && !$is_service_proc) {
            $data['contract']['is_allow_start'] = TRUE;
        }

        if (has_permission('courseads.stop_service') && $is_service_proc && !$is_service_end) {
            $data['contract']['is_allow_stop'] = TRUE;
        }

        // courseads detail
        $term_courses = $this->term_posts_m->get_term_posts($inputs['contractId'], $this->course_m->post_type);
        foreach ($term_courses as $item) {
            $title = $item->post_title;
            $start_date = $item->start_date;
            $end_date = $item->end_date;
            $total_course_time = (int)get_post_meta_value($item->post_id, 'total_course_time');

            $data['courseads'][] = [
                'title' => $title,
                'start_date' => '' . $start_date ? my_date($start_date, 'd/m/Y') : '--',
                'end_date' => '' . $end_date ? my_date($end_date, 'd/m/Y') : '--',
                'total_course_time' => $total_course_time ?: 0,
            ];
        }

        return parent::responseHandler($data, 'Lấy thông tin thành công');
    }

    /**
     * @return json
     */
    public function start_contract_put($contractId)
    {
        if (!has_permission('courseads.start_service.update')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        if (!(has_permission('courseads.start_service.manage') || has_permission('courseads.start_service.mdeparment') || has_permission('courseads.start_service.mgroup'))) 
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);

        $inputs = wp_parse_args(parent::put(), [
            'contractId' => $contractId,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->courseads_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $term = $this->courseads_m->set_term_type()->where('term_id', $inputs['contractId'])->get_by();

        $is_proc_service = $this->courseads_contract_m->proc_service($term);
        if (!$is_proc_service) {
            return parent::responseHandler([], 'Có lỗi trong quá trình khởi chạy.', 'error', 400);
        }

        return parent::responseHandler([], 'Dịch vụ khởi chạy thành công.');
    }

    /**
     * @return json
     */
    public function stop_contract_put($contractId)
    {
        if (!has_permission('courseads.stop_service.update')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        if (!(has_permission('courseads.stop_service.manage') || has_permission('courseads.stop_service.mdeparment') || has_permission('courseads.stop_service.mgroup'))) 
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);

        $inputs = wp_parse_args(parent::put(), [
            'contractId' => $contractId,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->courseads_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $term = $this->courseads_m->set_term_type()->where('term_id', $inputs['contractId'])->get_by();

        $is_stop_service = $this->courseads_contract_m->stop_service($term);
        if (!$is_stop_service) {
            return parent::responseHandler([], 'Có lỗi trong quá trình kết thúc hợp đồng.', 'error', 400);
        }

        return parent::responseHandler([], 'Hợp đồng đã chuyển sang trạng thái "Đã kết thúc".');
    }

    protected function checkValidateTime($time)
    {
        $day = (int)date('d', $time);
        $month = (int)date('m', $time);
        $year = (int)date('Y', $time);

        return checkdate($month, $day, $year);
    }

    /**
     * @return json
     */
    public function courseads_code_get($contractId)
    {
        if (!has_permission('courseads.setting.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), ['contractId' => $contractId]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->courseads_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $allow_meta_key = ['gg', 'fb', 'gg_advance', 'fb_advance', 'relearn'];
        $data = [];
        foreach($allow_meta_key as $meta_key){
            $data[$meta_key] = get_term_meta_value($contractId, 'coursead_' . $meta_key) ?: '';
        }

        return parent::responseHandler($data, 'Lấy dữ liệu thành công');
    }

    /**
     * @return json
     */
    public function courseads_code_put($contractId)
    {
        if (!has_permission('courseads.setting.update')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        if (!(has_permission('courseads.setting.manage') || has_permission('courseads.setting.mdeparment') || has_permission('courseads.setting.mgroup'))) 
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);

        $inputs = wp_parse_args(parent::put(), [
            'contractId' => $contractId,
            'gg' => '',
            'fb' => '',
            'gg_advance' => '',
            'fb_advance' => '',
            'relearn' => ''
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->courseads_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $allow_meta_key = ['gg', 'fb', 'gg_advance', 'fb_advance', 'relearn'];
        foreach($inputs as $meta_key => $meta_value)
        {
            if(in_array($meta_key, $allow_meta_key)) update_term_meta($contractId, 'coursead_' . $meta_key, $meta_value);
        }

        return parent::responseHandler(null, 'Lưu thông tin thành công');
    }
}
/* End of file Resource.php */
/* Location: ./application/modules/staffs/controllers/api_v2/Resource.php */