<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courseads extends Admin_Controller {

	function __construct()
	{
		$models = array(
			'term_users_m',
			'courseads/course_m',
      		'courseads/courseads_m',
      		'courseads/courseads_kpi_m'
		);

	    parent::__construct();
	    $this->load->model($models);
		$this->load->library('datatable_builder');
		$this->load->config('courseads');
	}

	public function index()
	{
		restrict('Courseads.contract.access');

		$data = $this->data;
		
		$default_remote_url = admin_url('courseads/ajax/contract?');
        $args = array(
        	'where[course_id]' => $this->input->get('course_id', TRUE)
        );
        $default_remote_url.= http_build_query($args);
        $data['default_remote_url'] = $default_remote_url;

        // Get list teachers
		$teacher_lists = array();
		$teachers = $this->admin_m->select('user_id,display_name,user_email')
		->where('user.user_type', 'admin')->get_many_by();
		if($teachers)
		{
			foreach ($teachers as $user)
			{
				$teacher_lists[$user->user_id] = $user->display_name . ' - ' . $user->user_email;
			}
		}
		$data['teachers'] = $teacher_lists;

		$this->template->is_box_open->set(1);
		$this->template->title->set('Dịch vụ GURU');
		$this->template->description->set('Trang danh sách hợp đồng');

		parent::render($data);
	}

    public function course()
	{
		restrict('Courseads.Course.Access');

        $this->template->is_box_open->set(1);
		$this->template->title->set('Quản lý khóa học');
        $this->template->description->set('Trang danh sách tất cả các khóa học');
		$this->template->javascript->add(base_url("dist/vCourseadsIndex.js?v=4"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		$this->template->stylesheet->add(base_url('node_modules/vue2-dropzone/dist/vue2Dropzone.min.css'));
		$this->template->publish();
	}

	public function contract()
	{
		restrict('Courseads.contract.Access');
		
		$terms = $this->course_m->set_post_type()->where('post_status','pending')->get_many_by();
		$data['courses'] = key_value($terms,'post_id','post_title');

		$this->template->is_box_open->set(1);
		$this->template->title->set('Quản lý phiếu đăng ký');
		$this->template->description->set('Trang danh sách phiếu đăng ký');
		parent::render($data);
	}

	public function students()
	{
		restrict('Courseads.contract.Access');

		$data = $this->data;
		$this->template->is_box_open->set(1);
		$this->template->title->set('Quản lý học viên');
		$this->template->description->set('Trang danh sách học viên');
		parent::render($data);
	}

	public function kpi($term_id = 0, $delete_id= 0)
	{
		restrict('Courseads.Kpi.Access');

		$this->template->title->prepend('KPI');
		$data = $this->data;
		$this->submit_kpi($term_id,$delete_id);

		$data['users'] = $this->admin_m->select('user_id,display_name')->where_in('role_id', $this->config->item('group_memberId'))->set_get_active()->order_by('display_name')->get_many_by();
		$data['users'] = key_value($data['users'],'user_id','display_name');

		$data['kpis'] = $this->courseads_kpi_m
		->order_by('kpi_type')
		->where('object_id',$term_id)
		->as_array()
		->get_many_by();

		if(!empty($data['kpis']))
		{
			$exclude_user_ids = key_value($data['kpis'],'user_id','kpi_id');
			$data['users'] = array_diff_key($data['users'],$exclude_user_ids);
		}

		$data['term_id'] = $term_id;

		parent::render($data);
	}
	public function submit_kpi($term_id = 0,$delete_id=0)
	{
		if($delete_id >0)
		{
			if( ! $this->courseads_m->has_permission($term_id, 'Courseads.kpi.delete'))
			{
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("kpi/{$term_id}"),'refresh');
			}
			$this->courseads_kpi_m->delete($delete_id);
			redirect(module_url('kpi/'.$term_id.'/'),'refresh');
		}

		$post = $this->input->post();
		if(empty($post)) return FALSE;
		if( ! $this->courseads_m->has_permission($term_id, 'Courseads.kpi.update'))
		{
			$this->messages->error('Không có quyền thực hiện tác vụ này.');
			redirect(module_url("kpi/{$term_id}"),'refresh');
		}


		$insert_data = array(
			'object_id' 	=> $term_id,
			'object_type' 	=> 'term',
			'user_id'		=> $this->input->post('user_id'),
			'kpi_type' 	    => 'teacher',
			'kpi_value' 	=> 0,
			'kpi_datetime'  => date('Y-m-d H:i:s',$this->mdate->startOfMonth()),
			'result_value'  => 0
		);

		if($this->mdate->startOfMonth($insert_data['kpi_datetime']) >= $this->mdate->startOfMonth())
		{
			$this->courseads_kpi_m->insert($insert_data);
			
			$this->load->model('term_users_m');
			$this->term_users_m->set_term_users($term_id, $this->input->post('user_id'), 'admin');

			$this->messages->success('Cập nhật thành công');
		}
		else
		{
			$this->messages->error('Cập nhật không thành công do tháng cập nhật nhỏ hơn tháng hiện tại');
		}
		redirect(current_url(),'refresh');
	}

	/**
	 * Adwords Setting Page
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function setting($term_id = 0)
	{
		restrict('Courseads.setting');
		$this->submit($term_id);
		$data = $this->data;
		$data['term_id'] = $term_id;


		parent::render($data);
	}

	/**
	 * Submit handler for function Overview|Setting
	 * Process and redirect to parent page
	 * @return     <type>  Process
	 */
	private function submit($term_id = 0)
	{
		$post = $this->input->post();
		if(empty($post)) return FALSE;

		if( ! $this->courseads_m->has_permission($term_id, 'Courseads.setting.update'))
		{
			$this->messages->error('Không có quyền thực hiện tác vụ này.');
			redirect(module_url("kpi/{$term_id}"),'refresh');
		}

		if(empty($post['edit']['student_name']) || empty($post['edit']['student_phone']) || empty($post['edit']['student_email'])) 
		{
			$this->messages->error('Dữ liệu đầu vào không tồn tại.');
			redirect(module_url("kpi/{$term_id}"),'refresh');
		}

		//validate phone
		foreach ($post['edit']['student_phone'] as &$phone) {
			$phone = create_slug($phone);

			$phone = preg_replace('([a-zA-Z]+)', '', $phone);
			if(empty($phone)) return $this->messages->error('Số điện thoại không được rỗng !');
			if(strlen($phone)<10) return $this->messages->error('Số điện thoại < 10. Vui lòng nhập lại !');
		}

		//validate email

		foreach ($post['edit']['student_email'] as &$email) {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return $this->messages->error('Email không hợp lệ !');
			}

		}

		if(!empty($post['curator_submit'])) // Update curator infomation reciepents for system reporting
		{

			$count = count($post['edit']['student_name']);
			if($count)
			{
				$i = 0;
				$curators = array();
				for ($i; $i < $count; $i++)
				{
					if(empty($post['edit']['student_phone'][$i]) || empty($post['edit']['student_name'][$i]) ||  empty($post['edit']['student_email'][$i])) return $this->messages->error('Có lỗi xảy ra, vui lòng kiểm tra lại !');;

					$curators[] = array(
						'name'=>$post['edit']['student_name'][$i],
						'phone'=>$post['edit']['student_phone'][$i],
						'email'=>$post['edit']['student_email'][$i]);
				}

				$meta_value = serialize($curators);
				update_term_meta($term_id,'guru_students',$meta_value);

				$this->load->model('courseads/courseads_contract_m');
				$this->courseads_contract_m->count_customers($term_id);

				$this->messages->success('Cập nhật thành công !');
			}
		}

		redirect(current_url(), 'refresh');
	}

	public function overview($term_id = 0){
		restrict('Courseads.overview');
		$data['term_id'] = $term_id;

		 
		
		parent::render($data);
	}

}
/* End of file Courseads.php */
/* Location: ./application/modules/courseads/controllers/Courseads.php */
