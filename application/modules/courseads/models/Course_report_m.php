<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course_report_m extends Base_Model
{
	function __construct()
	{
		parent::__construct();

		$this->load->config('contract/contract');
		$this->load->helper('date');
		$this->load->model('courseads/course_m');
	}

	/**
	 * Sends an activation mail 2 customer.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function send_activation_mail2customer($term_id = 0)
	{
		$term = $this->term_m->get($term_id);
		if(empty($term)) return FALSE;

		$data = array('term'=>$term);

		$time_start = get_term_meta_value($term_id,'contract_begin');
		$time_end 	= get_term_meta_value($term_id,'contract_end');


		$this->load->library('email');

		$this->load->model('term_posts_m');
		$this->load->model('courseads/course_m');
		$courses = $this->term_posts_m->get_term_posts($term_id, $this->course_m->post_type);
		foreach ($courses as $key => $value) {
			$data['course'][$key]['start_date'] = $value->start_date;
			$data['course'][$key]['total_course_time'] = get_post_meta_value($value->post_id,'total_course_time');
			$data['course'][$key]['day_in_week'] = unserialize(get_post_meta_value($value->post_id,'day_in_week')) ?: [];
			$data['course'][$key]['contract_value'] = get_term_meta_value($term_id,'contract_value');
			$data['course'][$key]['voucher_value'] = get_term_meta_value($term_id,'voucher_value');

			$data['course'][$key]['title'] = 'Thông tin đăng ký khóa học '.ucfirst($value->post_title);
			$data['course'][$key]['term_id'] = $term_id;
		}

		$title = 'Kính gửi Quý học viên Guru';
		$content_view = 'courseads/admin/tpl_email';
		$content = $this->load->view($content_view,$data,TRUE);
		$this->email->from('support@adsplus.vn', 'Adsplus.vn');
		$representative_email = get_term_meta_value($term_id, 'representative_email');
		$staff_id = get_term_meta_value($term_id, 'staff_business');
		$staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');
		$mail_cc = array();
		$students = unserialize(get_term_meta_value($term_id, 'guru_students'));

		// foreach ($students as $key => $value) {
		// 	$mail_cc[] = $value['email'];
		// }

		if($staff_mail)
			$mail_cc[] = $staff_mail;
		$this->email->to($representative_email);
		$this->email->cc($mail_cc);
		$this->email->subject($title);
		$this->email->message($content);

		$send_status = $this->email->send(TRUE);
		return $send_status;
	}
}
/* End of file Course_report_m.php */
/* Location: ./application/modules/courseads/models/Course_report_m.php */
