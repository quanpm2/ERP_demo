<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Contract_m.php');

class Courseads_m extends Contract_m
{
	public $term_type 	= 'courseads';

	/**
	 * Prepare data for clone contract
	 *
	 * @param      void  $primary_value  The primary value
	 */
	public $validate = array(
 			array(
 					'field' => 'meta[contract_name]',
 					'label' => 'Thông tin khách hàng',
 					'rules' => 'required'
 			),
 			array(
 					'field' => 'meta[contract_email]',
 					'label' => 'Email khách hàng',
 					'rules' => 'required'
 			),
 			array(
 					'field' => 'meta[contract_phone]',
 					'label' => 'Số điện thoại khách hàng',
 					'rules' => 'required|is_natural'
 			),

 			array(
 					'field' => 'meta[course]',
 					'label' => 'Khóa học',
 					'rules' => 'required'
 			),
 	);

	function __construct()
	{
		parent::__construct();
		$this->load->config('contract/contract');
	}


	public function set_term_type()
	{
		return $this->where_in('term.term_type',$this->term_type);
	}

	/**
	* Determines if it has permission.
	*
	* @param      integer  $term_id     The term identifier
	* @param      string   $permission  The permission
	* @param      integer  $user_id     The user identifier
	*
	* @return     boolean  True if has permission, False otherwise.
	*/
	public function has_permission($term_id = 0, $permission = '', $user_id = 0)
	{
		if(empty($permission)) $permission = $this->router->fetch_class().'.'.$this->router->fetch_method().'.access';
		if(empty($user_id)) $user_id = $this->admin_m->id;

		$relate_users = $this->admin_m->get_all_by_permissions($permission, $user_id);
		if(is_bool($relate_users)) return $relate_users;

		$this->load->model('term_users_m');
		$users = $this->term_users_m->get_the_users($term_id, 'admin');
		if(empty($users)) return FALSE;

		$user_intersect = array_intersect($relate_users, array_column($users, 'user_id'));
		if(empty($user_intersect)) return FALSE;

		return TRUE;
	}

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function existed_check($value)
    {
        if (empty($value)) {
            return true;
        }
        
        $isExisted = $this->set_term_type()->where('term.term_id', (int) $value)->count_by() > 0;
        if (!$isExisted) {
            $this->form_validation->set_message('existed_check', 'Hợp đồng không tồn tại.');
            return false;
        }

        return true;
    }
}
/* End of file Courseads_m.php */
/* Location: ./application/modules/courseads/models/Courseads_m.php */
