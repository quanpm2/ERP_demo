<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Courseads_kpi_m extends Base_model {

	public $_table 			= 'kpi';
	public $primary_key 	= 'kpi_id';
	public $kpi_type 		= 'teacher';
	protected $object_type	= 'term';

	public $before_create 	= array('delete_cache');
	public $after_update 	= array('delete_cache');
	public $before_delete 	= array('delete_cache');

	function __construct()
	{
		parent::__construct();
	}


	function get_kpis($object_id = 0, $type = 'all', $kpi_type = 'content', $time = 0, $user_id = 0)
	{
		$key_cache = 'modules/phonebook/get_kpi-'.$object_id;
		if($time == 0)
			$date = date('Y-m-01');
		else
			$date = date('Y-m-01',$time);


		if(($kpis = $this->scache->get($key_cache)) === FALSE)
		{
			$results = $this->order_by('kpi_datetime','asc')->get_many_by(array('object_id' => $object_id,'object_type'=>$this->object_type));
			$kpis = array();
			$kpis['user_id'] = array();
			$kpis['users'] = array();
			$kpis['kpi_type'] = array();
			$kpis['kpi_date'] = array();
			foreach($results as $r)
			{
				if(!isset($kpis['user_id'][$r->user_id]))
				{
					$kpis['user_id'][$r->user_id] = array();
					$kpis['user_id'][$r->user_id]['total'] = 0;
				}
				if(!isset($kpis['user_id'][$r->user_id][$r->kpi_type][$r->kpi_datetime]))
				{
					if(!isset($kpis['user_id'][$r->user_id][$r->kpi_type]['total']))
					{
						$kpis['user_id'][$r->user_id][$r->kpi_type]['total'] = 0;
					}
					$kpis['user_id'][$r->user_id][$r->kpi_type][$r->kpi_datetime] = 0;
				}

				$kpis['user_id'][$r->user_id][$r->kpi_type][$r->kpi_datetime]+= $r->kpi_value;
				$kpis['user_id'][$r->user_id][$r->kpi_type]['total']+= $r->kpi_value;

				$kpis['users'][$r->kpi_type][$r->kpi_datetime][$r->user_id] = $r->kpi_value;
			}

			$this->scache->write($kpis,$key_cache);
		}
		$result = array();

		switch ($type) {
			case 'users':
				$result = $kpis['users'];
				(isset($result[$kpi_type])) AND $result = $result[$kpi_type];
				($time > 0 && isset($result[$date])) AND $result = $result[$date];
				($user_id > 0) AND $result = @$result[$user_id];

				break;
			
			default:
				$result = $kpis;
				break;
		}
	
		return $result;		
	}


	function get_kpi_value($object_id = 0, $type = 'all', $time = 0, $user_id = 0)
	{
		if($time == 0)
			$date = date('Y-m-01');
		else
			$date = date('Y-m-01',$time);

		$key_cache = 'modules/phonebook/kpi-'.$object_id;
		if(($kpis = $this->scache->get($key_cache)) === FALSE)
		{
			$results = $this->order_by('kpi_datetime','asc')->get_many_by(array(
				'object_id' => $object_id,'object_type'=>$this->object_type
				));
			$kpis = array();
			$kpi_all = array();
			$kpis['all'] = array();
			$kpis['users'] = array();
			if($results)
			{
				foreach($results as $result)
				{
					if(!isset($kpis[$result->kpi_type]))
						$kpis[$result->kpi_type] = array();
					if(!isset($kpis[$result->kpi_type][$result->kpi_datetime]))
						$kpis[$result->kpi_type][$result->kpi_datetime] = 0;
					if(!isset($kpis['all'][$result->kpi_datetime]))
						$kpis['all'][$result->kpi_datetime] = 0;
	
					if(!isset($kpis['users'][$result->user_id][$result->kpi_type]))
					{
						$kpis['users'][$result->user_id][$result->kpi_type] = array();
						$kpis['users'][$result->user_id][$result->kpi_type]['total'] = 0;
					}
					
					!isset($kpis['users'][$result->user_id][$result->kpi_type][$result->kpi_datetime]) AND $kpis['users'][$result->user_id][$result->kpi_type][$result->kpi_datetime] = 0;

					$kpis[$result->kpi_type][$result->kpi_datetime]+= $result->kpi_value;
					$kpis['all'][$result->kpi_datetime]+= $result->kpi_value;

					$kpis['users'][$result->user_id][$result->kpi_type][$result->kpi_datetime]+= $result->kpi_value;
					$kpis['users'][$result->user_id][$result->kpi_type]['total']+= $result->kpi_value;
				}
			}
			$this->scache->write($kpis,$key_cache);
		}

		if($type == 'users')
		{
			return $kpis['users'];
		}
		if($user_id > 0)
		{
			if($time == 0)
				$date = 'total';
			return isset($kpis['users'][$user_id][$type][$date]) ? $kpis['users'][$user_id][$type][$date] : false;
		}

		if($time == 0 && isset($kpis[$type][$date]))
			return $kpis[$type];

		if($time > 0)
		{
			return isset($kpis[$type][$date]) ? $kpis[$type][$date] : 0;
		}

		return $kpis;
	}

	//các hàm này chưa có viết tối ưu
	function get_kpi_result($type = 'all', $time = 0,  $user_id = 0)
	{
		if($time == 0)
			$date = date('Y-m-01');
		else
			$date = date('Y-m-01',$time);

		$key_cache = 'modules/phonebook/kpi-result-'.$user_id;

		if(($kpis = $this->scache->get($key_cache)) === FALSE)
		{
			if($user_id > 0)
			{
				$where['user_id'] = $user_id;
			}
			$results = $this->order_by('kpi_datetime','asc')->get_many_by($where);

			$kpis = array();
			$kpi_all = array();
			$kpis['all'] = array();
			if($results)
			{
				foreach($results as $result)
				{
					if(!isset($kpis[$result->kpi_type]))
						$kpis[$result->kpi_type] = array();

					if(!isset($kpis[$result->kpi_type]['total']))
						$kpis[$result->kpi_type]['total'] = 0;

					if(!isset($kpis[$result->kpi_type][$result->kpi_datetime]))
						$kpis[$result->kpi_type][$result->kpi_datetime] = 0;
					if(!isset($kpis['all'][$result->kpi_datetime]))
						$kpis['all'][$result->kpi_datetime] = 0;

					$kpis[$result->kpi_type]['total']+= $result->result_value;
					$kpis[$result->kpi_type][$result->kpi_datetime]+= $result->result_value;
					$kpis['all'][$result->kpi_datetime]+= $result->result_value;
				}
			}
			
			$this->scache->write($kpis,$key_cache);
		}

		if($time == 0 && isset($kpis[$type][$date]))
			return $kpis[$type];

		if($time > 0)
		{
			return isset($kpis[$type][$date]) ? $kpis[$type][$date] : 0;
		}

		return $kpis;
	}

	function update_kpi_value($object_id, $kpi_type, $kpi_value, $time, $user_id = 0)
	{
		$this->delete_cache($object_id);
		if($kpi_value <= 0) return false;

		if($user_id == 0)
		{
			$user_id = $this->admin_m->id;
		}

		$time = $this->mdate->convert_time($time);

		$kpi_datetime = date('Y-m-d',$time);
		$update =array();
		$update['object_id'] = $object_id;
		$update['object_type'] = $this->object_type;
		$update['kpi_type'] = $kpi_type;
		$update['kpi_datetime'] = $kpi_datetime;
		$update['user_id'] = $user_id;
		$check = $this->where($update)->get_by();
		
		$update['kpi_value'] = $kpi_value;

		if(!$check)
		{
			if(in_array(my_date($time,'D'), ['Sat','Sun'])) // 2 ngày cuối tuần sẽ không tính KPI
			{
				$update['kpi_value'] = 0;
			}

			$this->insert($update);
			$this->update_kpi_result($object_id, $kpi_type, 0, $time, $user_id);
			return true;
		}

		return true;
	}

	function update_kpi_result($ext, $kpi_type, $kpi_result = 0, $kpi_datetime = '', $user_id = 0)
	{
		if(empty($kpi_datetime))
			$kpi_datetime = time();

		$kpi_datetime = $kpi_datetime;

		// If kpi result is empty or zero , auto request call center for get data from ext
  		if(empty($kpi_result))
  		{
			$this->load->model('phonebook/report_phonebook_m');
			$results = $this->report_phonebook_m
			->get_called_result(date('Y-m-d 00:00:00',$kpi_datetime), date('Y-m-d 23:59:59',$kpi_datetime),$ext);

	  		$calleds = $this->report_phonebook_m->build_tree_called($results);
	  		$result_ext = $this->report_phonebook_m
	  		->calc_total_from_ext($calleds,date('Y-m-d 00:00:00',$kpi_datetime), date('Y-m-d 23:59:59',$kpi_datetime));

	  		$kpi_result = $result_ext[$ext]['callKPI'] ?? 0;
  		}

		$where = array(
			'kpi_type' => $kpi_type,
			'object_id' => $ext,
			'object_type' => $this->object_type,
			'kpi_datetime' => date('Y-m-d',$kpi_datetime)
			);

		if($user_id > 0)
		{
			$where['user_id'] = $user_id;
		} 

		$this->where($where);
		$this->update_by('object_id', $ext, array('result_value' => $kpi_result));
		$key_cache = 'modules/phonebook/kpi-result-'.$ext.'-'. $user_id;
		$this->scache->delete($key_cache);
		return $kpi_result;
	}

	function delete_cache($term)
	{
		//trigger after_update
		$object_id = (isset($term[2][0]) && $term[2][0] == 'object_id') ? $term[2][1] : 0;

		if(is_array($term) && isset($term['object_id']))
			$object_id = $term['object_id'];
		else if(is_object($term) && isset($term->object_id))
			$object_id = $term->object_id;
		if(is_string($term) || is_numeric($term))
			$object_id = $term;
		$this->scache->delete_group('modules/phonebook/kpi-'.$object_id);
		$this->scache->delete_group('modules/phonebook/kpi-result-'.$object_id);
		$this->scache->delete_group('modules/phonebook/get_kpi-'.$object_id);
		return $term;
	}
}
/* End of file Call_kpi_m.php */
/* Location: ./application/modules/staffs/models/Call_kpi_m.php */