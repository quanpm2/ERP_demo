<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courseads_contract_m extends Base_contract_m
{
	function __construct()
	{
		parent::__construct();

		$this->load->config('contract/contract');
		$this->load->helper('date');
	}

	/**
	 * Calculates the contract value.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     integer  The contract value.
	 */
	public function calc_contract_value($term_id = 0)
	{
		$this->load->model('courseads/courseads_m');
		$contract = $this->courseads_m->set_term_type()->set_term_type()->get($term_id);
		if( ! $contract) return FALSE;

		/* Chi tiết các khóa học đã đăng ký của hợp đồng */
		$courses_items = get_term_meta_value($term_id, 'courses_items');
		$courses_items = unserialize($courses_items);
		if( ! $courses_items) return FALSE;

		$contract_value = array_sum(array_map(function($x){ return $x['course_value']*$x['quantity']; }, $courses_items));
		return $contract_value;
	}


	/**
	 * Cập nhật thời gian hợp đồng dựa trên tất cả các khóa học được khởi tạo
	 * trong hợp đồng
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function remap_contract_dates($term_id = 0)
	{
		$this->load->model('courseads/courseads_m');

		$contract = $this->courseads_m->select('term_id')->set_term_type()->get($term_id);
		if( ! $contract) return FALSE;

		$this->load->model('term_posts_m');
		$this->load->model('courseads/course_m');
		$term_courses 	= $this->term_posts_m->get_term_posts($term_id, $this->course_m->post_type);
		if(empty($term_courses)) return FALSE;

		/* Parse thời gian bắt đầu khóa học sớm nhất */
		$c_start_dates 	= array_column($term_courses, 'start_date');
		$contract_begin = reset($c_start_dates);
		update_term_meta($term_id, 'contract_begin', $contract_begin);

		/* Parse thời gian kết thúc khóa học trễ nhất */
		$c_end_dates 	= array_column($term_courses, 'end_date');
		rsort($c_end_dates);
		$contract_end 	= reset($c_end_dates);
		update_term_meta($term_id, 'contract_end', $contract_end);
		return TRUE;
	}


	/**
	 * { function_description }
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function map_course_kpi($term_id = 0)
	{
		$this->load->model('courseads/courseads_m');

		$contract = $this->courseads_m->select('term_id')->set_term_type()->get($term_id);
		if( ! $contract) return FALSE;

		$courses_items = unserialize(get_term_meta_value($term_id, 'courses_items'));
		if(empty($courses_items)) return FALSE;

		$teachers = array_map(function($x){
			return get_post_meta_value($x['post_id'], 'teacher_id');
		}, $courses_items);

		if( ! $teachers) return FALSE;

		$this->load->model('courseads/courseads_kpi_m');
		$kpis = $this->courseads_kpi_m
		->where('object_type', 'term')
		->where('kpi_type', 'teacher')
		->where_in('user_id', $teachers)
		->get_all();

		$kpis = $kpis ? array_column($kpis, 'user_id') : [];

		foreach ($teachers as $_teacher_id)
		{
			if(in_array($_teacher_id, $kpis)) continue;

			$insert_data = array(
				'object_id' 	=> $term_id,
				'object_type' 	=> 'term',
				'user_id'		=> $_teacher_id,
				'kpi_type' 	    => 'teacher',
				'kpi_value' 	=> 0,
				'kpi_datetime'  => date('Y-m-d H:i:s',$this->mdate->startOfMonth()),
				'result_value'  => 0 );

			$this->courseads_kpi_m->insert($insert_data);	
		}

		$this->load->model('term_users_m');
		$this->term_users_m->set_term_users($term_id, $teachers, 'admin');
		return TRUE;
	}

	
	/**
	 * Counts the number of customers.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   Number of customers.
	 */
	public function count_customers($term_id = 0)
	{
		$this->load->model('courseads/courseads_m');

		$contract = $this->courseads_m->select('term_id')->set_term_type()->get($term_id);
		if( ! $contract) return FALSE;

		$guru_students = unserialize(get_term_meta_value($term_id,'guru_students'));
		$num = !empty($guru_students) ? count($guru_students) : 0;

		$this->load->model('term_users_m');
		$customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company']);
		if( ! empty($customers))
		{
			$num += array_sum(array_map(function($x){ return (int) $x->user_type == 'customer_person'; }, $customers));
		}

		update_term_meta($term_id, 'num_customers', $num);
		return TRUE;
	}


	/**
	 * Kích hoạt hợp đồng 
	 * 1. thay đổi trạng thái sang đang thực hiện
	 * 2. Gửi email thông báo -> khách hàng , học viên, kinh doanh phụ trách
	 * 3. log
	 *
	 * @param      <type>  $term   The term
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	function proc_service($term = FALSE)
	{
		$now = time();
		update_term_meta($term->term_id, 'start_service_time', $now);

		$this->log_m->insert(array(
			'log_type' =>'start_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => my_date($now,'Y/m/d H:i:s')
			));

		// $this->load->model('courseads/course_report_m');
		// $stat = $this->course_report_m->send_activation_mail2customer($term->term_id);
		// return $stat;

        return TRUE;
	}

    /**
	 * Stops a service.
	 *
	 * @param      <type>  $term   The term
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function stop_service($term)
	{
		if(!$term || $term->term_type != 'courseads') return FALSE;
		$term_id = $term->term_id;

		update_term_meta($term_id,'end_service_time', time());
		$this->contract_m->update($term_id, array('term_status'=>'ending'));
		$this->log_m->insert(array(
			'log_type' =>'end_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term_id,
			'log_content' => date('Y/m/d H:i:s', time()) . ' - End Service Time'
			));

        // Push queue sync service fee
        $this->load->config('amqps');
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        
        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];

        $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $channel     = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $payload = [
            'event' 	=> 'contract_payment.sync.service_fee',
            'contract_id' => $term_id
        ];
        $message = new \PhpAmqpLib\Message\AMQPMessage(
            json_encode($payload),
            array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $channel->basic_publish($message, '', $queue);	

        $channel->close();
        $connection->close();

		return TRUE;
	}

	/**
	 * Creates a default invoice for courseads contract.
	 * Required numbers_of_payment and contract_value
	 *
	 * @param      term_m  $term   The term
	 *
	 * @return     boolean  status of process
	 */
	public function create_default_invoice($term)
	{
		$contract_value = get_term_meta_value($term->term_id,'contract_value') ?: 0;
		if(empty($contract_value)) return FALSE; 

		$number_of_payments = get_term_meta_value($term->term_id,'number_of_payments') ?: 1;
		$contract_begin 	= get_term_meta_value($term->term_id,'contract_begin');
		$contract_end 		= get_term_meta_value($term->term_id,'contract_end');
		$num_dates 			= diffInDates($contract_begin,$contract_end);
		
		$num_days4inv 			= ceil(div($num_dates,$number_of_payments));

		$discount_amount = (int) get_term_meta_value($term->term_id, 'discount_amount');
		if($discount_amount >= 0)
		{
			$contract_value-= $discount_amount;
		}

		$amount_per_payments 	= div($contract_value,$number_of_payments);

		$start_date = $contract_begin;
		$invoice_items = array();

		for($i = 0 ; $i < $number_of_payments; $i++)
		{	
			if($num_days4inv == 0) break;

			$end_date = $this->mdate->endOfDay(strtotime("+{$num_days4inv} day -1 second", $start_date));

			$inv_id = $this->invoice_m
			->insert(array(
				'post_title' => "Thu tiền đợt ". ($i + 1),
				'post_content' => "",
				'start_date' => $start_date,
				'end_date' => $end_date,
				'post_type' => $this->invoice_m->post_type
				));

			if(empty($inv_id)) continue;

			$this->term_posts_m->set_post_terms($inv_id, $term->term_id, $term->term_type);

			$quantity = $num_days4inv;

			$this->invoice_item_m->insert(array(
					'invi_title' => 'Giá dịch vụ',
					'inv_id' => $inv_id,
					'invi_description' => '',
					'invi_status' => 'publish',
					'price' => $amount_per_payments,
					'quantity' => 1,
					'invi_rate' => 100,
					'total' => $this->invoice_item_m->calc_total_price($amount_per_payments, 1, 100)
				));

			$start_date = strtotime('+1 second', $end_date);

			$day_end = $num_dates - $num_days4inv;
			if($day_end < $num_days4inv)
			{
				$num_days4inv = $day_end;
			}
		}

		return TRUE;
	}

	public function start_service($term = null)
	{
		$is_proc_service = $this->proc_service($term);
		if (!$is_proc_service)
		{
			$this->messages->error('Có lỗi trong quá trình khởi chạy.');
		}
		
		return true;
	}
}
/* End of file Courseads_contract_m.php */
/* Location: ./application/modules/courseads/models/Courseads_contract_m.php */