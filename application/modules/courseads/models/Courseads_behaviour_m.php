<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Behaviour_m.php');

class Courseads_behaviour_m extends Behaviour_m {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('date');
		$this->load->model('term_posts_m');
		$this->load->model('courseads/course_m');
		$this->load->model('courseads/courseads_m');
	}

	/**
	 * Calculates the contract value.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The contract value.
	 */
	public function calc_contract_value()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$value = 0;

		$courses_items = get_term_meta_value($this->contract->term_id, 'courses_items');
		if($courses_items)
		{
			$courses_items = unserialize($courses_items);
			$value = array_sum(array_map(function($x){
				return (int) $x['course_value'] * (int) $x['quantity'];
			}, $courses_items));
		}

		$discount_amount = (int) get_term_meta_value($this->contract->term_id, 'discount_amount');
		
		return max([0, $value - $discount_amount]);
	}

	/**
	 * Generate contract code by taxonomy
	 *
	 * @param      term_m  $term   The term
	 *
	 * @return     string  contract code
	 */
	public function gen_contract_code()
	{
		if( ! $this->contract) return FALSE;

		$contract 		= $this->contract;
		$contract_begin = get_term_meta_value($contract->term_id, 'contract_begin');
		$contract_begin = empty($contract_begin) ? my_date(time(), 'my') : my_date($contract_begin, 'my');

		$this->load->config('contract/contract');
		$taxonomy = $this->config->item($contract->term_type, 'taxonomy');

		$contract->term_name = strtoupper($contract->term_name);

		$courses_items = get_term_meta_value($this->contract->term_id, 'courses_items');
		$courses_items AND $courses_items = unserialize($courses_items);

		$this->load->helper('text');

		$courses_items = array_map(function($x){
			return mb_strtoupper(convert_accented_characters(url_title($x['post_title'])));
		}, $courses_items);
		
		$result = sprintf("%s/%s/%s/%s",$taxonomy ,$contract->term_id, $contract_begin, implode('-', $courses_items));
		return $result;
	}

	/**
	 * { function_description }
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     ( description_of_the_return_value )
	 */
	public function remap_contract_dates()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");
		return TRUE;
	}


	/**
	 * Counts the number of customers.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   Number of customers.
	 */
	public function count_customers()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");
		$term_id = $this->contract->term_id;

		$guru_students 	= unserialize(get_term_meta_value($term_id, 'guru_students'));
		$num 			= !empty($guru_students) ? count($guru_students) : 0;

		$customers 		= $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company']);
		if( ! empty($customers))
		{
			$num += count(array_filter($customers, function($x){
				$x->user_type == 'customer_person';
			}));
		}

		update_term_meta($term_id, 'num_customers', $num);
		
		return TRUE;
	}

	/**
	 * Preview data for printable contract version
	 *
	 * @return     String  HTML content
	 */
	public function prepare_preview()
	{
		if( ! $this->contract) return FALSE;

		parent::prepare_preview();

		$data = $this->data;

		$data['printable_title'] 	= $this->config->item($this->contract->term_type,'printable_title');
		$data['contract_code']		= get_term_meta_value($this->contract->term_id, 'contract_code');
		$data['company_name'] 		= $this->config->item('company','contract_represent');

		/* Contract date time duration */
		$contract_begin = get_term_meta_value($this->contract->term_id, 'contract_begin');
		$contract_end 	= get_term_meta_value($this->contract->term_id, 'contract_end');
		// $data['contract_begin'] = my_date($contract_begin, '\n\g\à\y d \t\h\á\n\g m \n\ă\m Y');
		// $data['contract_end'] 	= my_date($contract_end, '\n\g\à\y d \t\h\á\n\g m \n\ă\m Y');

		$this->load->model('courseads/course_m');

		$term_courses 	= $this->term_posts_m->get_term_posts($this->contract->term_id, $this->course_m->post_type);
		$courses_items  = unserialize(get_term_meta_value($this->contract->term_id, 'courses_items'));
		$courses_types  = [];

		foreach ($term_courses as &$term_course)
		{
			$term_course->quantity 		= $courses_items[$term_course->post_id]['quantity'] ?? 0;
			$term_course->course_value 	= $courses_items[$term_course->post_id]['course_value'] ?? 0;
			$term_course->start_date 	= my_date($term_course->start_date, 'd/m/Y');
			$term_course->end_date 		= my_date($term_course->end_date, 'd/m/Y');
			$term_course->total 		= $term_course->course_value * $term_course->quantity;
			
			$data['total_course_time'][$term_course->post_title] = get_post_meta_value($term_course->post_id,'total_course_time');
			$data['day_in_week'][$term_course->post_title] = unserialize(get_post_meta_value($term_course->post_id,'day_in_week'));
		}

		$this->load->config('courseads/courseads');
		$courses_types = array_map(function($x){
			return trim(mb_strtolower($x));
		}, array_column($term_courses, 'post_name'));

		$courses_types AND $courses_types = array_unique($courses_types);

		// $courses_types[] = 'combo-chuyen-sau';

		if( ! empty($courses_types))
		{
			$courses_types = array_map(function($x){
				return $this->config->item($x, 'schedulers');
			}, $courses_types);

			$courses_types = array_column($courses_types, null, 'name');
			foreach($courses_types as $key => $course)
			{
				if('product' == $course['type']) continue;
				if(empty($course['items'])) continue;

				foreach($course['items'] as $_item)
				{
					if( ! empty($courses_types[$_item])) continue;

					$_config = $this->config->item($_item, 'schedulers');
					if(empty($_config)) continue;
					
					$courses_types[$_item] = $_config;
				}

				unset($courses_types[$key]);
			}
		}	
		
		$data['courses_types'] 		= array_values($courses_types);
		$data['term_courses'] 		= $term_courses;
		$data['discount_amount'] 	= (int) get_term_meta_value($this->contract->term_id, 'discount_amount');
		$data['view_file'] 			= 'courseads/contract/preview';

		$created_on = (int) get_term_meta_value($this->contract->term_id, 'created_on');
		$created_on > start_of_day('2021/10/10') AND $data['view_file'] = 'courseads/contract/preview_after_20_oct_2021';

		return $data;
	}

    /**
     * Compute Actual Budget of Paid Payments
     * @return double Actual Budget
     */
    public function calc_payment_service_fee()
    {
        if( ! $this->contract) throw new \Exception('Property Contract undefined.');

        $stop_status = ['ending', 'liquidation'];
        if(!in_array($this->contract->term_status, $stop_status)) return 0;

        return parent::calc_payment_service_fee();
    }
}
/* End of file Courseads_behaviour_m.php */
/* Location: ./application/modules/courseads/models/Courseads_behaviour_m.php */