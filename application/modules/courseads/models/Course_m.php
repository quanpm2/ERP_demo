<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Course_m extends Post_m {

	public $post_type = 'course';

	public $validate = array(
			array(
					'field' => 'edit[post_name]',
					'label' => 'Mã khóa học',
					'rules' => 'required'
			),
			array(
					'field' => 'edit[post_title]',
					'label' => 'Tên khóa học',
					'rules' => 'required'
			),
			array(
					'field' => 'meta[course_type]',
					'label' => 'Loại khóa học',
					'rules' => 'required'
			),

			array(
					'field' => 'meta[teacher_id]',
					'label' => 'Giảng viên',
					'rules' => 'required'
			),

			array(
					'field' => 'meta[student_qty]',
					'label' => 'Số lượng học viên',
					'rules' => 'required|is_natural'
			),

			array(
					'field' => 'meta[course_value]',
					'label' => 'Giá trị khóa học',
					'rules' => 'required|is_natural'
			),
			array(
					'field' => 'meta[course_time]',
					'label' => 'Thời gian học',
					'rules' => 'required'
			),
			array(
					'field' => 'meta[total_course_time]',
					'label' => 'Tổng số buổi học',
					'rules' => 'required|is_natural'
			),
			// array(
			// 		'field' => 'meta[day_in_week]',
			// 		'label' => 'Các ngày học trong tuần',
			// 		'rules' => 'required'
			// ),
			// array(
			// 		'field' => 'meta[course_start_day_time]',
			// 		'label' => 'Thời gian bắt đầu học',
			// 		'rules' => 'required'
			// ),
			// array(
			// 		'field' => 'meta[course_end_day_time]',
			// 		'label' => 'Thời gian kết thúc học',
			// 		'rules' => 'required'
			// ),
			array(
					'field' => 'meta[voucher_value]',
					'label' => 'Giá trị giảm giá',
					'rules' => 'is_natural'
			)
			,
			array(
					'field' => 'meta[voucher_qty]',
					'label' => 'Số lượng giảm giá',
					'rules' => 'is_natural'
			)
	);
	
	/**
	 * Sets the post type.
	 *
	 * @return     Object seft
	 */
	public function set_post_type()
	{
		return $this->where_in('posts.post_type', $this->post_type);
	}

	/**
	 * Counts the number of customers.
	 *
	 * @param      integer  $post_id  The post identifier
	 *
	 * @return     integer  Number of customers.
	 */
	public function count_customers($post_id = 0)
	{
		$this->load->model('term_posts_m');
		$this->load->model('courseads/courseads_m');

		$num = 0;
		$terms = $this->term_posts_m->get_post_terms($post_id, $this->courseads_m->term_type);
		if( empty($terms)) return $num;

		$num = array_sum(array_map(function($x){ return (int) get_term_meta_value($x->term_id, 'num_customers'); }, $terms));
		return count($terms);
	}

	/**
	 * Counts the number of contracts.
	 *
	 * @param      integer  $post_id  The post identifier
	 *
	 * @return     integer  Number of contracts.
	 */
	public function count_contracts($post_id = 0)
	{
		$this->load->model('term_posts_m');
		$this->load->model('courseads/courseads_m');
		
		$num = 0;
		$terms = $this->term_posts_m->get_post_terms($post_id, $this->courseads_m->term_type);
		if( empty($terms)) return $num;

		return count($terms);
	}

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function existed_check($value)
    {
        if (empty($value)) {
            return true;
        }
        
        $isExisted = $this->set_post_type()->where('posts.post_id', (int) $value)->count_by() > 0;
        if (!$isExisted) {
            $this->form_validation->set_message('existed_check', 'Khoá học không tồn tại.');
            return false;
        }

        return true;
    }
}
/* End of file Course.php */
/* Location: ./application/modules/courseads/models/Course_m.php */