<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'modules/customer/models/Customer_m.php');

class Student_m extends Customer_m {

	public $validate = array(
			array(
					'field' => 'meta[student_name]',
					'label' => 'Tên học viên',
					'rules' => 'required'
			),
			array(
					'field' => 'meta[student_email]',
					'label' => 'Email học viên',
					'rules' => 'required'
			),
			array(
					'field' => 'meta[student_phone]',
					'label' => 'Số điện thoại học viên',
					'rules' => 'required|is_natural'
			),
	);

	function __construct()
	{
		parent::__construct();
	}
}
/* End of file Student_m.php */
/* Location: ./application/modules/courseads/models/Student_m.php */
