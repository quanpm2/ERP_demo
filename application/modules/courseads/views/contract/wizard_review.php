<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php 
    $term_id = $edit->term_id;
?>

<div class="col-md-12" id="review-partial">
    <h2 class="page-header"><small class="pull-right">Ngày tạo : <?php echo my_date(time(), 'd-m-Y');?></small></h2>

    <div class="row">
        <div class="col-xs-6">
            <p class="lead">Thông tin khách hàng</p>
            <div class="table-responsive">
                <?php
                    $representative_gender  = force_var(get_term_meta_value($edit->term_id,'representative_gender'),'Bà','Ông');
                    $representative_name    = get_term_meta_value($edit->term_id,'representative_name') ?: '';
                    $display_name           = "{$representative_gender} {$representative_name}";
                    $representative_email   = get_term_meta_value($edit->term_id,'representative_email');
                    $representative_address = get_term_meta_value($edit->term_id,'representative_address');
                    $representative_phone   = get_term_meta_value($edit->term_id,'representative_phone');

                    $contract_begin       = get_term_meta_value($edit->term_id,'contract_begin');
                    $contract_begin_date  = my_date($contract_begin,'d/m/Y');
                    $contract_end         = get_term_meta_value($edit->term_id,'contract_end');
                    $contract_end_date    = my_date($contract_end,'d/m/Y');
                    $contract_daterange   = "{$contract_begin_date} đến {$contract_end_date}";

                    echo $this->table->clear()
                    ->add_row('Người đại diện',$display_name?:'Chưa cập nhật')
                    ->add_row('Email',$representative_email?:'Chưa cập nhật')
                    ->add_row('Địa chỉ',$representative_address?:'Chưa cập nhật')
                    ->add_row('Số điện thoại',$representative_phone?:'Chưa cập nhật')
                    ->add_row('Chức vụ',$edit->extra['representative_position']??'Chưa cập nhật')
                    ->add_row('Mã Số thuế',$edit->extra['customer_tax']??'Chưa cập nhật')
                    ->add_row('Thời gian thực hiện',$contract_daterange?:'Chưa cập nhật')
                    ->generate();
                ?>
            </div>
        </div>
        <div class="col-xs-6">
            <p class="lead">Thông số dịch vụ</p>
            <?php 
                $hubspot_link = get_term_meta_value($term_id, 'hubspot_link');
                echo $this->table->clear()
                    ->add_row('Link Hubspot', $hubspot_link)
                    ->generate();
            ?>

            <p class="lead">Thông tin chi khóa học đã đăng ký</p>
            <?php 
                $total = 0;

                $this->load->model('courseads/course_m');
                $term_courses   = $this->term_posts_m->get_term_posts($term_id, $this->course_m->post_type);
                $courses_items  = unserialize(get_term_meta_value($term_id, 'courses_items'));
                if($courses_items)
                {
                    $this->table->clear();
                    $this->table->set_heading('Khóa học','Đơn giá','Số lượng HV','Thành tiền');
                
                    foreach ($courses_items as $_term_id => $course)
                    {
                        if( empty($term_courses[$_term_id]))
                        {
                            unset($courses_items[$_term_id]);
                            continue;
                        }
                    
                        $_name          = $term_courses[$course['post_id']]->post_title ?? '--';
                        $_course_value  = $course['course_value'];
                        $_quantity      = $course['quantity'];
                        $_total         = $_course_value * $_quantity;
                        $total          += $_total;
                    
                        $this->table->add_row($_name, currency_numberformat($_course_value), $_quantity, ['data' => currency_numberformat($_total, ' đ'), 'align' => 'right']);
                    }
                
                    $this->table->add_row(['data'=>'<i>Tổng giá trị khóa học</i>','colspan'=>3], ['data' => currency_numberformat($total, ' đ'), 'align' => 'right']);
                }
            
                $discount_amount = (int) get_term_meta_value($term_id, 'discount_amount');
                if($discount_amount > 0)
                {
                    $this->table->add_row(['data'=>'Giảm giá','colspan'=>3], ['data' => currency_numberformat($discount_amount, ' đ'), 'align' => 'right']);
                }
            
                $amount_not_vat = $total - $discount_amount;
            
                $this->table->add_row(['data'=>'<b>Thành tiền (chưa tính VAT)</b>','colspan'=>3], ['data' => currency_numberformat($amount_not_vat, ' đ'), 'align' => 'right']);
            
                $amount     = 0;
                $vat_amount = 0;
                $vat        = (int) get_term_meta_value($term_id, 'vat');
            
                if($vat > 0)
                {
                    $vat_amount = $amount_not_vat * div($vat, 100);
                    $amount = $amount_not_vat + $vat_amount;
                }
            
                $this->table->add_row(['data'=>"<b>VAT {$vat} %</b>",'colspan'=>3], ['data' => currency_numberformat($vat_amount, ' đ'), 'align' => 'right']);
                $this->table->add_row(['data'=>'<b>Thành tiền</b>','colspan'=>3], ['data' => currency_numberformat($amount, ' đ'), 'align' => 'right']);
            
                echo $this->table->generate();
            ?>
        </div>
    </div>

</div>
<div class="clearfix"></div>

<?php

$hidden_values = ['edit[term_status]'=>'waitingforapprove','edit[term_id]'=>$edit->term_id,'edit[term_type]'=>$edit->term_type];
echo $this->admin_form->form_open('',[],$hidden_values);
echo $this->admin_form->submit('','confirm_step_finish','confirm_step_finish','', array('style'=>'display:none;','id'=>'confirm_step_finish'));
echo $this->admin_form->form_close();