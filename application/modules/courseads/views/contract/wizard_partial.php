<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$this->template->stylesheet->add('plugins/tagsinput/tagsinput.css');
$this->template->javascript->add('plugins/tagsinput/tagsinput.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.js');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');
$this->template->javascript->add(base_url("dist/vCourseadsConfigurationBox.js"));
?>
<div class="col-md-12" id="service_tab">
	
<?php

$this->load->model('courseads/course_m');

$term_courses   = $this->term_posts_m->get_term_posts($edit->term_id, $this->course_m->post_type);
$courses_items  = unserialize(get_term_meta_value($edit->term_id, 'courses_items'));
if($courses_items)
{
    foreach ($courses_items as $_term_id => $course)
    {
        if( ! empty($term_courses[$_term_id])) continue;
        unset($courses_items[$_term_id]);
    }
}

$courses = $this->course_m->set_post_type()->get_many_by('post_status', ['pending', 'publish']);

$courses = array_map(function($x){ $x->course_value = (int) get_post_meta_value($x->post_id, 'course_value'); return $x; }, $courses);

echo $this->admin_form->form_open();
echo form_hidden('edit[term_id]', $edit->term_id);
?>
<div id="app-process-course-setting-container">
	<v-courseads-setting term_id="<?php echo $edit->term_id;?>"></v-courseads-setting>
</div>
<?php
echo $this->admin_form->submit('', 'confirm_step_service', 'confirm_step_service', '', ['style'=>'display:none;','id'=>'confirm_step_service']);
echo $this->admin_form->form_close();
?>
</div>
<script type="text/javascript">
var courses             = <?php echo json_encode($courses);?>;
var contract_courses    = <?php echo json_encode(array_values(($courses_items?:[]))); ?>;
var app_root 			= new Vue({ el: '#service_tab' });
</script>