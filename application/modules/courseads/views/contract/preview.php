<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ADSPLUS-<?php echo date('mY');?>-<?php echo $term->term_id;?>-<?php echo $term->term_name;?>.pdf</title>
    <style>
    body{line-height:1.2em; font-size: 14px; padding: 0 20mm;}
        p {margin:8px 0}
        .title{background:#eee; font-weight:600; border-top:1px solid #000}
        ul{margin: 0}
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <script type="text/javascript">
    window.onload = function () {

        var imgNum = Math.ceil(document.body.scrollHeight/750) 
        var imgTags = '<img src="<?php echo base_url('template/admin/img/watermark-adplus-2.png');?>" width="100%" class="watermark" style="opacity:0.8">'
        var innerHTML = ""
        for(var i = 0;i < imgNum;i++){
            innerHTML +=imgTags
        }
        document.getElementById('img-holder').innerHTML = innerHTML;
    }
    </script>
</head>

<body style="font-family: 'Times New Roman', serif;">

    <div id="img-holder" style="position: absolute;top: 0;left:0"></div>
    
    <p align="center">
        CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br/>
        <span>Độc lập - Tự do - Hạnh phúc</span><br/>
        <span>--------------oOo--------------</span>
    </p>

    <p>&nbsp;</p>

    <p align="center">
        <b>
            <span style="font-size:1.3em; line-height:1.3em"><?php echo ($printable_title?:'HỢP ĐỒNG'); ?></span><br>
            Số: (<?php echo get_term_meta_value($term_id,'contract_code');?>)
        </b>
    </p>

    <p align="center">&nbsp;</p>

    <p>Căn cứ Luật Thương mại nước Cộng hoà xã hội chủ nghĩa Việt Nam</p>
    <p>Hôm nay, ngày <?php echo my_date($verified_on, 'd');?> tháng <?php echo my_date($verified_on, 'm');?> năm <?php echo my_date($verified_on, 'Y');?> chúng tôi gồm:</p>

    <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td width="35%"><strong>BÊN THUÊ DỊCH VỤ (BÊN A)</strong></td>
            <td>: <strong><?php echo mb_strtoupper($customer->display_name);?></strong></td>
        </tr>
        <?php foreach ($data_customer as $label => $val) :?>
        <?php if (empty($val)) continue;?>
        <tr>
            <td width="130px" valign="top"><?php echo $label;?></td>
            <td>: <?php echo $val;?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td><strong>BÊN CUNG ỨNG DỊCH VỤ (BÊN B)</strong></td>
            <td>: <strong><?php echo $company_name; ?></strong></td>
        </tr>

        <?php foreach ($data_represent as $label => $val) :?>
        <?php if (empty($val)) continue;?>
        <tr>
            <td width="130px" valign="top"><?php echo $label;?></td>
            <td>: <?php echo $val;?></td>
        </tr>
        <?php endforeach; ?>

    </table>


    <i>Bên A thống nhất đề nghị Bên B cung cấp khóa học quảng cáo như sau</i>

    <br/>
    <br/>
    <p><u><strong>ĐIỀU I: ĐIỀU KHOẢN CHUNG</strong></u></p>
    <p>Bên B chịu trách nhiệm cung cấp khóa học cho Bên A như sau:</p>

    <?php foreach ($courses_types as $index => $_course): ?>
        <p><b>1.<?php echo $index+1;?> <?php echo $_course['title'];?></b></p>
        <p><?php echo $_course['description'] ;?></p>
        
        <ul>
            <?php foreach ($_course['items'] as $item): ?>
            <li style="padding: 8px;"><?php echo $item;?></li>
            <?php endforeach ?> 
        </ul>

    <?php endforeach ?>

    <p><u><strong>ĐIỀU II: GIÁ TRỊ HỢP ĐỒNG</strong></u></p>
    <p>Đơn vị tính giá: Việt Nam Đồng (VND).</p>
    <p>Thanh toán bằng tiền đồng Việt Nam.</p>
    <table border="1" cellspacing="0" style="text-align: center;" cellpadding="5" width="100%">
        <tr>
            <th width="25%" nowrap="nowrap">
                <strong align="center">Khóa học</strong>
            </th>
            <th width="30%" nowrap="nowrap">
                <strong align="center">Thời gian học</strong>
            </th>
            <th width="10%" nowrap="nowrap">
                <strong align="center">Số lượng</strong>
            </th>
            <th width="20%">
                <strong align="center"><u>Đơn giá</u></strong>
            </th>
            <th width="25%" nowrap="nowrap">
                <strong align="center">Thành tiền <br/> (VND)</strong>
            </th>
        </tr>
        <?php if ($term_courses): ?>
        <?php foreach ($term_courses as $term_course): ?>
        <tr>
            <td><?php echo $term_course->post_title;?></td>
            <td><?php echo  my_date($contract_begin, 'd/m/Y') . ' đến ' . my_date($contract_end, 'd/m/Y');?></td>
            <td><?php echo $term_course->quantity;?></td>
            <td align="right"><?php echo currency_numberformat($term_course->course_value);?></td>
            <td align="right"><?php echo currency_numberformat($term_course->total);?>
        </tr>
        <?php endforeach ?>
        <?php endif ?>


        
        <?php
            $term_courses_arr = array_map(function($x){ return (array)$x; }, $term_courses);
            $total_not_vat = array_sum(array_column($term_courses_arr, 'total'));
            ?>
        <tr>
            <td colspan="4" style="text-align: right;"><strong>Tổng (không bao gồm VAT)</strong></td>
            <td align="right"><strong><?php echo currency_numberformat($total_not_vat);?></strong></td>
        </tr>
        <?php 
            if($discount_amount > 0) :
              $total_not_vat-= $discount_amount;
            ?>
        <tr>
            <td colspan="4" style="text-align: right;"><strong>Giảm giá</strong></td>
            <td align="right"><strong><?php echo currency_numberformat($discount_amount);?></strong></td>
        </tr>
        <?php endif;?>
        <?php if($vat):
            $vat_value = $total_not_vat*div($vat,100);
            ?>
        <tr>
            <td colspan="4" style="text-align: right;"><strong>VAT <?php echo $vat?>%</strong></td>
            <td align="right"><strong><?php echo currency_numberformat($vat_value);?></strong></td>
        </tr>
        <?php endif;?>
        <?php
            $total = $total_not_vat * (1+(div($vat, 100)));
            ?>
        <tr>
            <td colspan="4" style="text-align: right;"><strong>Tổng</strong></td>
            <td align="right"><strong><?php echo currency_numberformat($total);?></strong></td>
        </tr>
    </table>
    <p>(Bằng chữ: <?php   echo ucfirst(mb_strtolower(convert_number_to_words($total)));?>)</p>
    <br/>


    <p><u><strong>ĐIỀU III: PHƯƠNG THỨC THANH TOÁN</strong></u></p>
    <p>3.1 Bên A thanh toán cho Bên B 100% giá trị hợp đồng trước khi thực hiện.</p>
    <p>3.2 Bên B phát hành hóa đơn GTGT trong vòng 5 ngày sau khi 2 bên nghiệm thu hợp đồng.</p>
    <p>3.3 Hình thức thanh toán: Thanh toán bằng tiền mặt hoặc chuyển khoản về tài khoản sau :</p>


    <?php if( ! empty($bank_info)) :?>
    <ul style="padding-left:0;list-style:none">
    <?php foreach ($bank_info as $label => $text) :?>
        <?php if (is_array($text)) : ?>
            <?php foreach ($text as $key => $value) :?>
                <li>- <?php echo $key;?>: <?php echo $value;?></li>
            <?php endforeach;?>
        <?php continue; endif;?>
        <li>- <?php echo $label;?>: <?php echo $text;?></li>
    <?php endforeach;?>
    </ul>
    <p><strong>Cú pháp chuyển khoản thanh toán: Học viện Guru_ [Tên Học viên (Tên đơn vị)]_Tên Khóa học_Số tiền cần chuyển</strong></p>
    <?php endif; ?>

    <p><u><strong>ĐIỀU IV: TRÁCH NHIỆM VÀ QUYỀN HẠN CỦA BÊN A</strong></u></p>

    <p>4.1 Trách nhiệm: Bên A phải làm đầy đủ thủ tục nhập học và đóng học phí theo quy định mới được tham gia lớp học. Trong học tập tuyệt đối chấp hành nội quy của lớp học và quy định của Pháp luật. Nếu vi phạm phải bồi thường toàn bộ học phí của khoá học và những tổn thất gây ra, đồng thời phải chịu trách nhiệm trước pháp luật.</p>
    <p>4.2 Quyền hạn: Bên A có quyền đề xuất, đóng góp ý kiến xây dựng để đảm bảo quyền lợi cho mình. Người nhập học hoặc đang học nếu vì lý do nào đó mà chưa tham gia học được thì Bên B sẽ xét để bảo lưu và được học lại.</p>

    <p>4.3 Quyền lợi: Bên A được học tập lý thuyết và thực hành đảm bảo đủ thời gian, nội dung chất lượng theo chương trình của Bên B đã đề ra. Bên A được kiểm tra trình độ lý thuyết, tay nghề và được cấp chứng chỉ theo quy định. </p>
    <p>4.4 Không được sử dụng nội dung đăng tải thông tin doanh nghiệp vào mục đích gây rối trật tự xã hội, phá hoại an ninh quốc gia, làm tổn hại thuần phong mỹ tục hay kinh doanh bất hợp pháp.</p>
    <p>4.5 Chịu trách nhiệm về tính xác thực của các thông tin trong thông tin doanh nghiệp đăng.</p>
    <p>4.6 Tuân thủ theo các quy định pháp luật về quyền sở hữu công nghiệp bản quyền.</p>
    <p>4.7 Tuân thủ theo các quy định của Nhà nước về sử dụng dịch vụ Internet và các điều khoản quy định trong hợp đồng này. Trong trường hợp có vi phạm, Bên A sẽ chịu trách nhiệm hoàn toàn trước pháp luật.</p>
    <br/>

    <p><u><strong>ĐIỀU V: TRÁCH NHIỆM VÀ QUYỀN HẠN CỦA BÊN A</strong></u></p>

    <p>5.1 Bên B có quyền yêu cầu Bên A cung cấp thông tin quảng cáo đầy đủ, chính xác theo đúng thời gian của hợp đồng này.</p>

    <p>5.2 Thực hiện đầy đủ những điều kiện cần thiết đã cam kết trong hợp đồng đào tạo  để Bên A học tập đạt hiệu quả, bảo đảm theo hợp đồng đã ký.</p>

    <p>5.3 Nhanh chóng giải quyết các khiếu nại của Bên A về chất lượng dịch vụ trong phạm vi trách nhiệm của Bên B</p>

    <p>5.4 Bên B có quyền điều chuyển Bên A giữa các lớp và thay đổi, tạm hoãn, kỷ luật và chấm dứt hợp đồng với các trường hợp Bên A vi phạm hợp đồng theo quy định của pháp luật.</p>

    <br/>

    <p><u><strong>ĐIỀU VI: TẠM NGƯNG, TẠM CẮT, THAY ĐỔI NỘI DUNG CỦA HỢP ĐỒNG</strong></u></p>

    <p><b>6.1 Tạm ngưng</b></p>
    <p> a. Bên B sẽ tạm ngưng một hoặc tất cả dịch vụ trong hợp đồng này sau khi nhận được yêu cầu bằng văn bản của Bên A.</p>
    <p> b. Bên A phải cung cấp đầy đủ lý do chính đáng cho việc tạm ngưng khóa học </p>

    <p><b>6.2 Tạm cắt</b></p>
    <p>Bên B sẽ tạm cắt dịch vụ khoá học của Bên A nếu Bên A vi phạm một trong các điều khoản quy định trong hợp đồng này.</p>

    <p><b>6.3. Thay đổi</b></p>
    <p>Mọi thay đổi liên quan đến hợp đồng phải có văn bản đề nghị bên kia trước 5 ngày để giải quyết. Mọi chi phí phát sinh cho thay đổi hợp đồng do nguyên nhân từ bên nào thì bên đó có trách nhiệm thanh toán.</p>
    <br/>
    
    <p><u><strong>ĐIỀU VII: ĐIỀU KHOẢN THI HÀNH</strong></u></p>
    <p>7.1. Hai bên cam kết thực hiện đúng các điều khoản của hợp đồng, bên nào vi phạm sẽ phải chịu trách nhiệm theo quy định của pháp luật.</p>
    <p>7.2. Trong trường hợp bất khả kháng: thiên tai, bão lụt, động đất, khủng bố … gây gián đoạn các buổi học, trách nhiệm và thời hạn thực hiện hợp đồng của cả hai bên sẽ được xem xét, đàm phán và quyết định lại.</p>

    <p>7.3. Trong quá trình thực hiện, nếu có vướng mắc gì thì 2 bên chủ động thương lượng giải quyết trên tinh thần hợp tác, tôn trọng lẫn nhau. Nếu hai bên không tự giải quyết được sẽ thống nhất chuyển vụ việc tới Toà án Kinh tế tại Hồ Chí Minh để giải quyết.</p>
    <p>7.4. Hợp đồng này có hiệu lực kể từ ngày ký cho đến hết thời hạn đăng bài tại Điều I. Khi hợp đồng hết hiệu lực, nếu hai bên tiếp tục gia hạn hợp đồng, hai bên sẽ ký kết phụ lục gia hạn hợp đồng theo báo giá tại thời điểm ký phụ lục hợp đồng.</p>

    <br/>
    <p>Hợp đồng này được lập thành 02 bản có giá trị như nhau: Bên A giữ 01 bản, Bên B giữ 01 bản.</p>




<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
    <tr>
        <td width="44%" valign="top" style="text-align:center">
            <p>
                <strong>BÊN A</strong><br />
                <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                <strong><?php echo mb_strtoupper($customer->display_name);?>  </strong>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
            <td rowspan="2"></td>

            <td width="46%" valign="top" style="text-align:center">
                <p><strong>BÊN B</strong><br />
                    <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                    <strong>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</strong>
                </p>
                <p><strong>&nbsp;</strong></p>
                <p><strong>&nbsp;</strong></p>
                <p>&nbsp;</p>
            </td>
    </tr>
    <tr>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_customer['Đại diện'] ;?></strong><br/>
                <?php echo $data_customer['Chức vụ'] ;?>
            </p>
        </td>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_represent['Đại diện'] ?? ''; ?></strong><br />
                <?php echo $data_represent['Chức vụ']; ?>
            </p>
        </td>
    </tr>
</table>

<?php if(!empty($contract_budget_customer_payment_type) && 'behalf' == $contract_budget_customer_payment_type) $this->load->view('googleads/contract/preview/behaft_appendix');?>

<script type="text/javascript">

    (function() {
        var beforePrint = function() {
            console.log('Functionality to run before printing.');
        };
        var afterPrint = function() {
            console.log('Functionality to run after printing');
        };

        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');
            mediaQueryList.addListener(function(mql) {
                console.log(mql);
                if (mql.matches) {
                    beforePrint();
                } else {
                    afterPrint();
                }
            });
        }

        window.onbeforeprint = beforePrint;
        window.onafterprint = afterPrint;
    }());
</script>
</body>
</html>