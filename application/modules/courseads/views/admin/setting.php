<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');



$this->template->javascript->add('plugins/input-mask/jquery.inputmask.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.date.extensions.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.extensions.js');

//  $has_start_permission 	= has_permission('Googleads.start_service');
//  $has_stop_permission	= has_permission('Googleads.stop_service');
// $is_service_proc 		= is_service_proc($term);
// $is_service_end 		= is_service_end($term);

// $this->admin_form->set_col(12,6);
// echo $this->admin_form->box_open();
// if($has_start_permission && !$is_service_proc)
// {
// 	echo "<proc-service-component term_id='{$term_id}' begin_date='{$begin_date}'></proc-service-component>";
// }

// if($has_stop_permission && $is_service_proc && !$is_service_end)
// {
// 	$end_date_suggest = $end_date ?: my_date(time(),'d-m-Y');
// 	echo "<stop-service-component term_id='{$term_id}' end_date='{$end_date_suggest}'></stop-service-component>";
// 	echo "<suspend-service-component term_id='{$term_id}' end_date='{$end_date_suggest}'></suspend-service-component>";
	
// 	echo '<p class="help-block">Vui lòng cấu hình để hệ thống E-mail hoạt động</p>';
// }
// $this->admin_form->set_col(12,6);
// echo $this->admin_form->box_open();
 echo "<proc-service-component isHidden = 'false' term_id='{$term_id}' begin_date=".my_date(time(),'d-m-Y')."></proc-service-component>";


// echo $this->admin_form->box_close();

// $this->admin_form->set_col(6,6);
// echo $this->admin_form->form_open();
// echo $this->admin_form->box_open('Người nhận báo cáo tự động');

// $curators = @unserialize(get_term_meta_value($term->term_id,'guru_students'));


// $max_curator = $this->option_m->get_value('max_number_curators');
// $max_curator = $max_curator ?: $this->config->item('max_input_curators','googleads');
// $max_curator = 1;

// $is_valid_num_curator = (!empty($max_curator) && is_numeric($max_curator));
// if($is_valid_num_curator){
// 	$i = 1;
// 	for ($i; $i <= $max_curator; $i++){
// 		echo '<h4 class="page-header">Người giám sát ' . $i . '</h4>';
// 		echo $this->admin_form->input('Họ và tên','edit[curator_name][]',@$curators[$i-1]['name'],'');
// 		echo $this->admin_form->input('Số điện thoại','edit[curator_phone][]',@$curators[$i-1]['phone'],'Số điện thoại nhận báo cáo tự động');
// 		echo $this->admin_form->input('E-mail','edit[curator_email][]',@$curators[$i-1]['email'],'Địa chỉ nhận báo cáo qua email tự động',array('class'=>'form-control 123123'));
// 	}
// }
//  echo $this->admin_form->box_close(array('curator_submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
//  echo $this->admin_form->form_close();

$guru_students = unserialize(get_term_meta_value($term_id,'guru_students'));

?>
<?php echo $this->admin_form->form_open();?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-theme">
			<div class="box-header with-border">
				<h3 class="box-title">Học viên nhận báo cáo</h3>
				<div class="box-tools pull-right">
					<button type="button" data-widget="collapse" class="btn btn-box-tool">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<table border="1" cellpadding="2" id="auto_report" cellspacing="1" class="table table-responsive table-bordered table-hover">
					<thead>
						<tr><th>#</th><th>Họ và tên</th><th>Số điện thoại</th><th>Email</th></tr>
					</thead>
					<tbody>
						<?php
							if(!$guru_students){

								$str = '<tr><td>1</td><td><input type="text" name="edit[student_name][]" value="" placeholder="Nguyễn văn A" required="required" class="form-control"></td><td><input type="text" name="edit[student_phone][]" value="" placeholder="0123456789" required="required" class="form-control"></td><td><input type="email" name="edit[student_email][]" value="" placeholder="example@gmail.com" required="required" class="form-control" style="width: 80%; display: inline;"></td></tr>';

								echo $str;
							}else{

								$data = '';
								for ($i=0; $i < count($guru_students); $i++) { 
									$index = $i+1;
									$str = '<tr>'.
										'<td>'.$index.'</td>'.
										'<td><input type="text" name="edit[student_name][]" value="'.$guru_students[$i]['name'].'" class="form-control" placeholder="Nguyễn văn A" required></td>'.
										'<td><input type="text" name="edit[student_phone][]" value="'.$guru_students[$i]['phone'].'" class="form-control" placeholder="0123456789" required ></td>'.
										'<td><input type="email" name="edit[student_email][]" value="'.$guru_students[$i]['email'].'" class="form-control" placeholder="example@gmail.com" style="width:80%; display: inline;" required></td></tr>';
									$data .= $str;
								}

								echo $data;

							}
						?>
					<!-- <tr>
						<td><input type="text" name="edit[curator_name][]" value='' class="form-control" placeholder="Nguyễn văn A"></td>
						<td><input type="text" name="edit[curator_phone][]" value='' class="form-control" placeholder="0123456789"></td>
						<td><input type="text" name="edit[curator_email][]" value='' class="form-control" placeholder="example@gmail.com"></td>
					</tr> -->
					<tr class="add-new">
						<td colspan="4"><button type="button" class="btn btn-default" onclick="addRow(this)">Thêm mới</button></td>
					</tr>
					
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer text-center">
			<input type="submit" name="curator_submit" value="Lưu lại" class="btn btn-info"> 
			<input type="button" name="button" value="Hủy bỏ" class="btn btn-default">
		</div>

		
	</div>
</div>
<?php echo $this->admin_form->form_close();?>

<script type="text/javascript">


var baseURL = "<?php echo admin_url('');?>"; 
$(function(){
	$('[data-toggle=confirmation]').confirmation();
	$(".input-mask").inputmask();
	$('.set-datepicker').datepicker({
		format: 'dd-mm-yyyy',
		todayHighlight: true,
		autoclose: true,
	});
	$("#updatefrm").submit(function(){
		var form = $(this);
		$.each(form.find($("input[type='text'],textarea")), function( index, value ) {
			if($(value).prop("defaultValue") == $(value).val())
			$(value).remove();
		});
		$.each(form.find($("select")), function( index, value) {
			if(value.options[value.selectedIndex].defaultSelected)	
				$(value).remove();
		});
		$.each(form.find($("input[type='checkbox'],input[type='radio']")), function( index, value ) {
			if($(value).prop("defaultChecked") == $(value).prop("checked"))	
				$(value).remove();
		});
	});

	addRow = function(e){
		var rowCount = $('table > tbody > tr').length;
		var buttonAdd = '<tr>'+
					'<td>'+rowCount+'</td>'+
					'<td><input type="text" name="edit[student_name][]" value="" class="form-control" placeholder="Nguyễn văn A" required></td>'+
					'<td><input type="text" name="edit[student_phone][]" value="" class="form-control" placeholder="0123456789" required></td>'+
					'<td><input type="email" name="edit[student_email][]" value="" class="form-control" placeholder="example@gmail.com" style="width:80%; display: inline;" required>'+
					'<i class="fa fa-times" onclick="closeNewRow(this)" style="color: red;font-size: 26px;float: right;" aria-hidden="true"></i>'+
					'</td></tr>';
		$(e).parent().parent().before(buttonAdd)
	}
	closeNewRow = function(e){
		$(e).parent().parent().fadeOut( "slow",function(){
			$(this).remove();
		}  ); 
	}
})
</script>

<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/component/form.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/courseads/js/setting.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/courseads/js/app.js'));

/* End of file setting.php */
/* Location: ./application/modules/googleads/views/admin/setting.php */