<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->stylesheet->add('modules/courseads/css/style.css');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');
$this->template->stylesheet->add('plugins/timepicker/bootstrap-timepicker.min.css');
$this->template->javascript->add('plugins/timepicker/bootstrap-timepicker.js');

$this->load->config('courseads');
?>
<div class="row" id="contract-databuilder-container">
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-plus-square"></i> Tất cả khóa học</a></li>
                <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-flag"></i> Khóa học đang mở</a></li>
                <li><a href="#tab_3" data-toggle="tab"><i class="fa fa-pause"></i> Khóa học đã kết thúc</a></li>
                <li class="pull-right">
                    <?php if(has_permission('Courseads.course.Add'))
            		{
            			?>
                        <button type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#create_course"><i class="fa fa-fw fa-plus "></i> Khóa học</button>
                        <?php
            		} ?>

                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <?php echo $this->admin_form->datatable(['remote_url'=>admin_url('courseads/ajax/course'),'is_download'=>TRUE]);?>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <?php echo $this->admin_form->datatable(['remote_url'=>admin_url('courseads/ajax/course?&where%5Bpost_status%5D=publish'),'is_download'=>TRUE]);?>
                </div>
                <div class="tab-pane" id="tab_3">
                    <?php echo $this->admin_form->datatable(['remote_url'=>admin_url('courseads/ajax/course?&where%5Bpost_status%5D=ending'),'is_download'=>TRUE]);?>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<!-- Modal -->
<?php
$course_create_api_url = admin_url('courseads/ajax/course/create');
?>
<div class="modal" id="create_course" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Tạo khóa học mới</h4>
            </div>
            <?php echo $this->admin_form->form_open($course_create_api_url,[],[]); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                    echo $this->admin_form->input('Mã khóa học','edit[post_name]');
                    echo $this->admin_form->input('Tên khóa học','edit[post_title]');
                    echo $this->admin_form->formGroup_begin(0,'Loại khóa học');
                    echo form_dropdown(array('name'=>'meta[course_type]','class'=>'form-control'),['publish'=>'Khóa học chính thức','demo'=>'Khóa học demo']);
                    echo $this->admin_form->formGroup_end();
                    echo $this->admin_form->dropdown('Giảng viên','meta[teacher_id]',prepare_dropdown($teachers, 'Chọn giảng viên phụ trách'));
                    echo $this->admin_form->input_numberic('Số lượng học viên','meta[student_qty]');
                    echo $this->admin_form->input_numberic('Giá trị khóa học','meta[course_value]');
                    echo $this->admin_form->input('Thời gian khóa học','meta[course_time]','','', array('class'=>'form-control','id'=>'course_time'));
                    echo $this->admin_form->input_numberic('Số buổi học','meta[total_course_time]');
                    ?>
                    <div id="div_time"></div>
                    <?php
                    echo "<h3>Thông tin mã giảm giá</h3>";
                    // echo $this->admin_form->input('Mã giảm giá','meta[voucher_code]');

                    echo $this->admin_form->input('Thời gian giảm giá','meta[voucher_time]','','', array('class'=>'form-control','id'=>'voucher_time'));
                    echo $this->admin_form->input_numberic('Giá trị giảm giá :','meta[voucher_value]','','', array('class'=>'form-control','min'=>'0'));
                    echo $this->admin_form->input_numberic('Số lượng giảm giá','meta[voucher_qty]');
                       ?>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?php echo form_submit(array('type'=>'button','class'=>'btn btn-primary create_course_btn'),'Tạo mới khóa học');?>
            </div>
            <?php echo $this->admin_form->form_close(); ?>
        </div>
    </div>
</div>
<?php $course_update_api_url = admin_url('courseads/ajax/course/update'); ?>
<div class="modal" id="edit_course" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Cập nhập khóa học</h4>
            </div>
            <?php echo $this->admin_form->form_open($course_update_api_url,[],['edit[user_type]'=>'customer_person']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                      <input type="hidden" name="edit[meta][course_id]" id="edit_course_id" />
                    <?php
                    echo $this->admin_form->input('Mã khóa học','edit[edit][post_name]','','', array('id'=>'edit_course_code'));
                    echo $this->admin_form->input('Tên khóa học','edit[edit][post_title]','','', array('id'=>'edit_course_name'));
                    echo $this->admin_form->formGroup_begin(0,'Loại khóa học');
                    echo form_dropdown(array('name'=>'edit[meta][course_type]','class'=>'form-control','id'=>'edit_course_type'),['publish'=>'Khóa học chính thức','demo'=>'Khóa học demo']);
                    echo $this->admin_form->formGroup_end();
                    echo $this->admin_form->dropdown('Trạng thái khóa học','edit[edit][post_status]',prepare_dropdown($this->config->item('course_status'),'Trạng thái khóa học'),'','', array('id'=>'edit_course_status'));
                    echo $this->admin_form->dropdown('Giảng viên','edit[meta][teacher_id]',prepare_dropdown($teachers, 'Chọn giảng viên phụ trách'),'','', array('id'=>'edit_teacher_id'));
                    echo $this->admin_form->input_numberic('Số lượng học viên','edit[meta][student_qty]','','', array('id'=>'edit_student_qty'));
                    echo $this->admin_form->input_numberic('Giá trị khóa học','edit[meta][course_value]','','', array('id'=>'edit_course_value'));
                    echo $this->admin_form->input('Thời gian khóa học','edit[meta][course_time]','','', array('class'=>'form-control','id'=>'edit_course_time'));
                    echo $this->admin_form->input_numberic('Số buổi học','edit[meta][total_course_time]','','', array('id'=>'edit_total_course_time'));
                    ?>
                    <div id="div_time_update"></div>
                    <?php
                    echo "<h3>Thông tin mã giảm giá</h3>";
                    // echo $this->admin_form->input('Mã giảm giá','meta[voucher_code]');

                    echo $this->admin_form->input('Thời gian giảm giá','edit[meta][voucher_time]','','', array('class'=>'form-control','id'=>'edit_voucher_time'));
                    echo $this->admin_form->input_numberic('Giá trị giảm giá :','edit[meta][voucher_value]','','', array('class'=>'form-control','min'=>'0','id'=>'edit_voucher_value'));
                    echo $this->admin_form->input_numberic('Số lượng giảm giá','edit[meta][voucher_qty]','','', array('id'=>'edit_voucher_qty'));
                       ?>
                    <script>

                    </script>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?php echo form_submit(array('type'=>'button','class'=>'btn btn-primary update_course_btn'),'Cập nhập khóa học');?>
            </div>
            <?php echo $this->admin_form->form_close(); ?>
        </div>
    </div>
</div>
<div class="modal" id="show_voucher" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Lấy mã giảm giá</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                      <?php
                      echo $this->admin_form->input('Mã giảm giá','','','', array('id'=>'voucher_code_value','disabled'=>'disabled'));
                      echo $this->admin_form->input('Số lượng','','','', array('id'=>'voucher_code_qty','disabled'=>'disabled'));
                       ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function showEdit(course_id){
      $.ajax({
        method: "POST",
        url: '<?php echo admin_url('courseads/ajax/dataset/getCourse');?>',
        data: { course_id: course_id}
      })
        .done(function( data ) {
          // console.log(data);
          // $("#edit_course_time").daterangepicker({
          //   format: 'DD-MM-YYYY',
          // });
          // $("#edit_voucher_time").daterangepicker({});
          $("#edit_course_id").val(data.course_id);
          $("#edit_course_code").val(data.course_code);
          $("#edit_course_name").val(data.course_name);
          $("#edit_course_type").val(data.course_type).trigger('change');
          $("#edit_course_status").val(data.course_status).trigger('change');
          $("#edit_teacher_id").val(data.teacher_id).trigger('change');
          $("#edit_student_qty").val(data.student_qty);
          $("#edit_course_value").val(data.course_value);
          $("#edit_course_time").val(data.course_time);
          $("#edit_total_course_time").val(data.total_course_time);
          $("#edit_day_in_week").val(data.days).trigger('change');
          change_day2();
          jQuery.each( data.day_in_week, function( i, val ) {
              console.log(i);
              console.log(val);
              $("#edit_u_course_start_day_time_" + i).val(val.time_start);
              $("#edit_u_course_end_day_time_" + i).val(val.time_end);
          });

          $("#edit_voucher_time").val(data.voucher_time);
          $("#edit_voucher_value").val(data.voucher_value);
          $("#edit_voucher_qty").val(data.voucher_qty);
          $('#edit_course').modal('show');
          $('#edit_course_time').daterangepicker({
            opens: 'left',format: 'DD-MM-YYYY'
          }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
          });
          $('#edit_voucher_time').daterangepicker({
            opens: 'left',format: 'DD-MM-YYYY'
          }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
          });
        });
    }
    function change_day(){
        $('#day_in_week :selected').each(function(i, sel){
            var date = new Date();
            var hour_next = date.getHours() + 2;
            var time_now = date.getHours() + ':' + date.getMinutes();
            var time_next = hour_next + ':' + date.getMinutes();
            html = "<div class='form-group' id='time_" + $(sel).val() + "'>";
            html += "<label for='inputEmail3' class='col-sm-2 control-label'>Thời gian học thứ " + $(sel).text() + " :</label>";
            html += "<div class='form-group col-md-10' style='padding-left: 0px;padding-right:  0px;'>";
            html += "<div class='input-group col-sm-12'>";
            html += "<div class='col-sm-6'>";
            html += "<div class='input-group bootstrap-timepicker timepicker'>";
            html += "<input id='edit_course_start_day_time_" + $(sel).val() + "' type='text' class='form-control input-small time_input' name='meta[course_start_day_time][" + $(sel).val() + "]' value='" + cover_time(time_now) + "'/>";
            html += "<span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>";
            html += "</div>";
            html += "</div>";
            html += "<div class='col-sm-6'>";
            html += "<div class='input-group bootstrap-timepicker timepicker'>";
            html += "<input id='edit_course_end_day_time_" + $(sel).val() + "' type='text' class='form-control input-small time_input' name='meta[course_end_day_time][" + $(sel).val() + "]' value='" + cover_time(time_next) + "'/>";
            html += "<span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            if($('#time_' + $(sel).val())[0]){
                var a = 1;
            }else{
                $("#div_time").append(html);
            }
        });
        var day_s = $('#day_in_week').val();
        var days = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday'];

        for (var variable in days) {
            if($('#time_' + days[variable])[0]){
                if(jQuery.inArray(days[variable],day_s) == -1){
                    $('#time_' + days[variable]).remove();
                }
            }
        }

        $('.time_input').timepicker();
    }

    function change_day2(){
        $('#edit_day_in_week :selected').each(function(i, sel){
            var date = new Date();
            var hour_next = date.getHours() + 2;
            var time_now = date.getHours() + ':' + date.getMinutes();
            var time_next = hour_next + ':' + date.getMinutes();
            html = "<div class='form-group' id='time_" + $(sel).val() + "'>";
            html += "<label for='inputEmail3' class='col-sm-2 control-label'>Thời gian học thứ " + $(sel).text() + " :</label>";
            html += "<div class='form-group col-md-10' style='padding-left: 0px;padding-right:  0px;'>";
            html += "<div class='input-group col-sm-12'>";
            html += "<div class='col-sm-6'>";
            html += "<div class='input-group bootstrap-timepicker timepicker'>";
            html += "<input id='edit_u_course_start_day_time_" + $(sel).val() + "' type='text' class='form-control input-small time_input' name='edit[meta][course_start_day_time][" + $(sel).val() + "]' value='" + cover_time(time_now) + "'/>";
            html += "<span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>";
            html += "</div>";
            html += "</div>";
            html += "<div class='col-sm-6'>";
            html += "<div class='input-group bootstrap-timepicker timepicker'>";
            html += "<input id='edit_u_course_end_day_time_" + $(sel).val() + "' type='text' class='form-control input-small time_input' name='edit[meta][course_end_day_time][" + $(sel).val() + "]' value='" + cover_time(time_next) + "'/>";
            html += "<span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            if($('#time_' + $(sel).val())[0]){
                var a = 1;
            }else{
                $("#div_time_update").append(html);
            }
        });
        var day_s = $('#edit_day_in_week').val();
        var days = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday'];
        console.log(day_s);
        for (var variable in days) {
            if($('#time_' + days[variable])[0]){
                if(jQuery.inArray(days[variable],day_s) == -1){
                    $('#time_' + days[variable]).remove();
                }
            }
        }
        $('.time_input').timepicker();
    }

    function cover_time(time24) {
        var ts = time24;
        var H = +ts.substr(0, 2);
        var h = (H % 12) || 12;
        h = (h < 10)?("0"+h):h;  // leading 0 at the left for 1 digit hours
        var ampm = H < 12 ? " AM" : " PM";
        ts = h + ts.substr(2, 3) + ampm;
        return ts;
    };

    function getVoucher(course_id){
      $.ajax({
        method: "POST",
        url: '<?php echo admin_url('courseads/ajax/course/getVoucher');?>',
        data: { course_id: course_id}
      })
        .done(function( data ) {
          $("#voucher_code_value").val(data.code);
          $("#voucher_code_qty").val(data.qty);
          $('#show_voucher').modal('show');
          console.log(data);
        });
    }
    $('[data-toggle=confirmation]').confirmation();
    $(function(){
      $("#course_time").daterangepicker({
        format: 'DD-MM-YYYY',
      });
      $("#voucher_time").daterangepicker({
        format: 'DD-MM-YYYY',
      });
      $('.time_input').timepicker();
      // $('.timepicker').on('show.timepicker', function () {
      //     $('.modal').removeAttr('tabindex');
      // });
      $('#day_in_week').select2();

      // $('.timepicker').timepicker({
      //   showInputs: true
      // });
      $('#edit_day_in_week').select2();
      $("#create_course").find('form').validate({
          rules: {
              'edit[post_name]': { required : true },
              'edit[post_title]': { required : true },
              'meta[course_type]': { required : true },
              'meta[teacher_id]': { required : true },
              'meta[student_qty]': {
                  required : true,
                  number: true
             },
              'meta[course_value]': {
                  required : true,
                  number: true
             },
              'meta[course_time]': { required : true },
              'meta[total_course_time]': {
                  required : true,
                  number: true
               },
              'meta[day_in_week][]': { required : true },
              'meta[voucher_value]': { number : true },
              'meta[voucher_qty]': { number : true },
              // 'meta[voucher_code]': { required : true },
          },
          messages:{
              'edit[post_name]': 'Mã khóa học là bắt buộc !',
              'edit[post_title]': 'Tên khóa học là bắt buộc !',
              'meta[course_type]': 'Loại khóa học là bắt buộc !',
              'meta[teacher_id]': 'Giảng viên là bắt buộc !',
              'meta[student_qty]': {
                  required : 'Số lượng học viên là bắt buộc !',
                  number: 'Số lượng học viên là phải là số !'
             },
              'meta[course_value]': {
                  required : 'Giá trị khóa học là bắt buộc !',
                  number: 'Giá trị khóa học là phải là số !'
             },
              'meta[course_time]': 'Thời gian học là bắt buộc !',
              'meta[total_course_time]': {
                  required : 'Tổng các buổi học là bắt buộc !',
                  number: 'Số lượng học viên là phải là số !'
             },
              'meta[day_in_week][]': 'Các ngày học trong tuần là bắt buộc !',
              'meta[voucher_value]': 'Giá trị giảm giá phải là số !',
              'meta[voucher_qty]': 'Số lượng mã giảm giá phải là số !',
              // 'meta[voucher_code]': 'Mã giảm giá là bắt buộc !',
          }
      });

      // Add event submit handler for customer create form
      $('.create_course_btn').click(function(){
          var form = $(this).closest('form');
          var valid = form.valid();
          if(!valid) return false;
          console.log("Thành công");
          $.post( form.attr('action'),form.serializeArray(),
          function( response ) {
            $.notify(response.msg,'success');
            $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='success-handler'>"+response.msg+"</div></div>");
            $('#create_course').modal('hide');
            window.setTimeout(function(){location.reload()},1000);
          },"json").fail(function(response) {
              $.notify(response.responseJSON.msg,'error');
              $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='error-handler'>"+response.responseJSON.msg+"</div></div>");
          });
      });
      $("#edit_course").find('form').validate({
          rules: {
              'edit[meta][post_name]': { required : true },
              'edit[meta][post_title]': { required : true },
              'edit[meta][course_type]': { required : true },
              'edit[meta][teacher_id]': { required : true },
              'edit[meta][student_qty]': {
                  required : true,
                  number: true
             },
              'edit[meta][course_value]': {
                  required : true,
                  number: true
             },
              'edit[meta][course_time]': { required : true },
              'edit[meta][total_course_time]': {
                  required : true,
                  number: true
             },
              'edit[meta][day_in_week]': { required : true },
              'edit[meta][course_status]': { required : true },
              'edit[meta][voucher_value]': { number : true },
              'edit[meta][voucher_qty]': { number : true },

          },
          messages:{
              'edit[meta][post_name]': 'Mã khóa học là bắt buộc !',
              'edit[meta][post_title]': 'Tên khóa học là bắt buộc !',
              'edit[meta][course_type]': 'Loại khóa học là bắt buộc !',
              'edit[meta][teacher_id]': 'Giảng viên là bắt buộc !',
              'edit[meta][student_qty]': {
                  required : 'Số lượng học viên là bắt buộc !',
                  number: 'Số lượng học viên là phải là số !'
             },
              'edit[meta][course_value]': {
                  required : 'Giá trị khóa học là bắt buộc !',
                  number: 'Số lượng học viên là phải là số !'
             },
              'edit[meta][course_time]': 'Thời gian học là bắt buộc !',
              'edit[meta][total_course_time]': {
                  required : 'Tổng các buổi học là bắt buộc !',
                  number: 'Số lượng học viên là phải là số !'
             },
              'edit[meta][day_in_week]': 'Các ngày học trong tuần là bắt buộc !',
              'edit[meta][course_status]': 'Trạng thái khóa học là bắt buộc !',
              'edit[meta][voucher_value]': 'Giá trị giảm giá phải là số !',
              'edit[meta][voucher_qty]': 'Số lượng mã giảm giá phải là số !',
          }
      });

      // Add event submit handler for customer create form
      $('.update_course_btn').click(function(){
          var form = $(this).closest('form');
          var valid = form.valid();
          if(!valid) return false;
          console.log("Thành công");
          $.post( form.attr('action'),form.serializeArray(),
          function( response ) {
            console.log(response);
            $.notify(response.msg,'success');
            $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='success-handler'>"+response.msg+"</div></div>");
            $('#edit_course').modal('hide');
            window.setTimeout(function(){location.reload()},1000);

          },"json").fail(function(response) {
              $.notify(response.responseJSON.msg,'error');
              $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='error-handler'>"+response.responseJSON.msg+"</div></div>");
          });
      });
    });
</script>

<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/component/ui.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/app.js'));
/* End of file index.php */
/* Location: ./application/modules/googleads/views/admin/index.php */
