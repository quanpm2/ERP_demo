<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->stylesheet->add('modules/courseads/css/style.css');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');
$this->template->stylesheet->add('plugins/timepicker/bootstrap-timepicker.min.css');
$this->template->javascript->add('plugins/timepicker/bootstrap-timepicker.js');

$this->load->config('courseads');
?>

<div class="row" id="contract-databuilder-container">
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-plus-square"></i> Tất cả phiếu đăng ký</a></li>
                <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-flag"></i> Chưa thanh toán</a></li>
                <li><a href="#tab_3" data-toggle="tab"><i class="fa fa-flag"></i> Đã thanh toán</a></li>
                <li><a href="#tab_4" data-toggle="tab"><i class="fa fa-flag"></i> Hủy bỏ</a></li>
                <li class="pull-right">
                    <button type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#create_contract"><i class="fa fa-fw fa-plus "></i> Phiếu đăng ký</button>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <?php
                    if($this->input->get('id')!=null){
                        ?>
                        <contract-component remote_url="<?php echo admin_url('courseads/ajax/dataset/contract?&where%5Bcourse_code%5D='.$this->input->get('id'))?>"></contract-component>
                        <?php
                        }else{
                        ?>
                        <contract-component remote_url="<?php echo admin_url('courseads/ajax/dataset/contract')?>"></contract-component>
                        <?php
                    }
                     ?>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <?php
                    if($this->input->get('id')!=null){
                        ?>
                        <contract-component remote_url="<?php echo admin_url('courseads/ajax/dataset/contract?&where%5Bterm_status%5D=pending&where%5Bcourse_code%5D='.$this->input->get('id'))?>"></contract-component>
                        <?php
                        }else{
                        ?>
                        <contract-component remote_url="<?php echo admin_url('courseads/ajax/dataset/contract?&where%5Bterm_status%5D=pending')?>"></contract-component>
                        <?php
                    }
                     ?>
                </div>
                <div class="tab-pane" id="tab_3">
                    <?php
                    if($this->input->get('id')!=null){
                        ?>
                        <contract-component remote_url="<?php echo admin_url('courseads/ajax/dataset/contract?&where%5Bterm_status%5D=publish&where%5Bcourse_code%5D='.$this->input->get('id'))?>"></contract-component>
                        <?php
                        }else{
                        ?>
                        <contract-component remote_url="<?php echo admin_url('courseads/ajax/dataset/contract?&where%5Bterm_status%5D=publish')?>"></contract-component>
                        <?php
                    }
                     ?>
                </div>
                <div class="tab-pane" id="tab_4">
                    <?php
                    if($this->input->get('id')!=null){
                        echo $this->admin_form->datatable(['remote_url'=>admin_url('courseads/ajax/dataset/contract?&where%5Bterm_status%5D=removing&where%5Bcourse_code%5D='.$this->input->get('id'))]);
                    }else{
                        echo $this->admin_form->datatable(['remote_url'=>admin_url('courseads/ajax/dataset/contract?&where%5Bterm_status%5D=removing')]);
                    }
                     ?>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
<?php
$course_contract_create_api_url = admin_url('courseads/ajax/contract/create');
?>
<div class="modal" id="create_contract" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Tạo phiếu đăng ký mới</h4>
            </div>
            <?php echo $this->admin_form->form_open($course_contract_create_api_url,[],['edit[user_type]'=>'customer_person']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                    /*
                     * line 95-138 :
                     * Hạn chế dùng echo đi chung với html nha em , như vậy dễ xảy ra tình trạng
                     * 1 => dễ lỗi , code mất nhiều thời gian
                     * 2 => khó cho người kế thừa chức năng này nếu cần hỗ trợ  upgrade sau vài tháng
                     */
                      echo "<h3>Thông tin phiếu đăng ký</h3>";
                      echo $this->admin_form->input('Họ tên','meta[contract_name]','','', array('id'=>'contract_name'));
                      echo "<div class='form-group'>";
                      echo "<label for='inputEmail3' class='col-sm-2 control-label'>Email</label>";
                      echo "<div class='form-group col-md-10' style='padding-left: 0px;'>";
                      echo "<div class='col-md-9'>";
                      echo form_input(array('name'=>'meta[contract_email]','class'=>'form-control','id'=>'contract_email'));
                      echo "</div>";
                      echo "<div class='col-md-3'>";
                      echo form_button(['id'=>'check_email','class'=>'btn btn-success col-md-12', 'onclick'=>'checkEmail()'],'<i class="fa fa-fw fa-plus"></i> Kiểm tra Email');
                      echo "</div>";
                      echo "</div>";
                      echo "</div>";
                      echo "<div id='div_email'>";
                      echo "</div>";
                      echo "<div class='form-group'>";
                      echo "<label for='inputEmail3' class='col-sm-2 control-label'>Số điện thoại</label>";
                      echo "<div class='form-group col-md-10' style='padding-left: 0px;'>";
                      echo "<div class='col-md-9'>";
                      echo form_input(array('name'=>'meta[contract_phone]','class'=>'form-control','id'=>'contract_phone'));
                      echo "</div>";
                      echo "<div class='col-md-3'>";
                      echo form_button(['id'=>'check_phone','class'=>'btn btn-success col-md-12', 'onclick'=>'checkPhone()'],'<i class="fa fa-fw fa-plus"></i> Kiểm tra phone');
                      echo "</div>";
                      echo "</div>";
                      echo "</div>";
                      echo "<div id='div_phone'>";
                      echo "</div>";
                      echo $this->admin_form->dropdown('Khóa học','meta[course]',prepare_dropdown($courses, 'Chọn khóa học'),'','', array('id'=>'course_id','onchange'=>"getCourse()"));
                      echo $this->admin_form->input('Giá trị khóa học','meta[course_value]','','', array('class'=>'form-control','disabled'=>'disabled','id'=>'course_value'));
                      echo "<div class='form-group'>";
                      echo "<label for='inputEmail3' class='col-sm-2 control-label'>Mã giảm giá</label>";
                      echo "<div class='form-group col-md-10' style='padding-left: 0px;'>";
                      echo "<div class='col-md-9'>";
                      echo form_input(array('name'=>'meta[voucher_code]','class'=>'form-control','id'=>'voucher_code'));
                      echo "</div>";
                      echo "<div class='col-md-3'>";
                      echo form_button(['id'=>'add_total_student','class'=>'btn btn-success col-md-12', 'onclick'=>'checkVoucher()'],'<i class="fa fa-fw fa-plus"></i> Kiểm tra voucher');
                      echo "</div>";
                      echo "</div>";
                      echo "</div>";
                      echo "<div id='div_voucher'>";
                      echo "</div>";
                      echo $this->admin_form->input('Giá trị cuối cùng','meta[contract_value_total]','','', array('class'=>'form-control','disabled'=>'disabled','id'=>'contract_value_total'));

                    ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?php echo form_submit(array('type'=>'button','class'=>'btn btn-primary create_contract_btn'),'Tạo mới phiếu đăng ký');?>
            </div>
            <?php echo $this->admin_form->form_close(); ?>
        </div>
    </div>
</div>
<?php
$course_contract_update_api_url = admin_url('courseads/ajax/contract/update');
/* line 160 - cuối file
 *
 * Em tiến hành code Vue component cho mỗi chức năng => viết ra file riêng theo cú pháp [ten-chuc-nang-component].js
 * Sau khi code xong => chỉ cần gọi component ở file view này và emmbed file js view là chạy
 */
?>
<div class="modal" id="update_contract" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Cập nhập phiếu đăng ký</h4>
            </div>
            <?php echo $this->admin_form->form_open($course_contract_update_api_url,[],['edit[user_type]'=>'customer_person']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                      echo "<h3>Thông tin phiếu đăng ký</h3>";
                      echo "<input name='edit[meta][contract_id]' type='hidden' id='edit_contract_id' />";
                      echo $this->admin_form->input('Họ tên','edit[meta][contract_name]','','', array('id'=>'edit_contract_name'));
                      echo $this->admin_form->input('Email','edit[meta][contract_email]','','', array('id'=>'edit_contract_email'));
                      echo $this->admin_form->input('Số điện thoại','edit[meta][contract_phone]','','', array('id'=>'edit_contract_phone'));
                      // echo "<input name='edit[meta][course]' type='hidden' id='edit_course_id' />";
                      echo $this->admin_form->dropdown('Khóa học','edit[meta][course]',prepare_dropdown($courses, 'Chọn khóa học'),'','', array('id'=>'edit_course_id'));
                      echo $this->admin_form->input('Giá trị khóa học','edit[meta][course_value]','','', array('class'=>'form-control','disabled'=>'disabled','id'=>'edit_course_value'));
                      echo "<div class='form-group'>";
                      echo "<label for='inputEmail3' class='col-sm-2 control-label'>Mã giảm giá</label>";
                      echo "<div class='form-group col-md-10' style='padding-left: 0px;'>";
                      echo "<div class='col-md-9'>";
                      echo form_input(array('name'=>'edit[meta][voucher_code]','class'=>'form-control','id'=>'edit_voucher_code','disabled'=>'disabled'));
                      echo "</div>";
                      echo "<div class='col-md-2'>";
                      // echo form_button(['id'=>'add_total_student','class'=>'btn btn-success', 'onclick'=>'checkVoucher()'],'<i class="fa fa-fw fa-plus"></i> Kiểm tra voucher');
                      echo "</div>";
                      echo "</div>";
                      echo "</div>";
                      echo "<div id='edit_div_voucher'>";
                      echo "</div>";
                      $status_register = $this->config->item('register_status');
                      echo $this->admin_form->input('Giá trị cuối cùng','edit[meta][contract_value_total]','','', array('class'=>'form-control','disabled'=>'disabled','id'=>'edit_contract_value_total'));
                      echo $this->admin_form->dropdown('Trạng thái phiếu đăng ký','edit[meta][register_status]',$status_register,'','', array('id'=>'edit_register_status'));
                    ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?php echo form_submit(array('type'=>'button','class'=>'btn btn-primary update_contract_btn'),'Cập nhập phiếu đăng ký');?>
            </div>
            <?php echo $this->admin_form->form_close(); ?>
        </div>
    </div>
</div>
<script>
function showEdit(resgister_id){
  $.ajax({
    method: "POST",
    url: '<?php echo admin_url('courseads/ajax/dataset/getResgister');?>',
    data: { resgister_id: resgister_id}
  })
    .done(function( data ) {
      console.log(data);

      $("#edit_contract_id").val(data.id);
      $("#edit_contract_name").val(data.contract_name);
      $("#edit_contract_email").val(data.contract_email);
      $("#edit_contract_phone").val(data.contract_phone);
      var newOption = new Option(data.course_name, data.course_id, true, true);
        // Append it to the select
        $('#edit_course_id').append(newOption);
      $("#edit_course_id").val(data.course_id).trigger('change').attr('disabled','disabled');
      $("#edit_course_value").val(formatMoney(parseInt(data.contract_value1),'VNĐ')).attr('disabled','disabled');

      $("#edit_voucher_code").val(data.voucher_code);
      $("#edit_register_status").val(data.term_status).trigger('change');
      $("#edit_register_status option[value='publish']").prop('disabled','disabled');
      if(data.term_status == 'publish'){
          $("#edit_register_status").attr('disabled','disabled');
      }
      if(data.course_value > 0){
          html = "<div class='form-group'>";
          html += "<label for='inputEmail3' class='col-sm-2 control-label'>Giá trị được giảm</label>";
          html += "<div class='form-group col-md-10'><div class='input-group col-sm-12'>";
          html += "<input type='text' name='meta[voucher_value]' value='" + data.voucher_value + " %'  data-value='" + data.voucher_value + "' id='contract_voucher_value' disabled='disabled' class='form-control form-control'>";
          html += "</div>";
          html += "</div>";
          html += "</div>";
          $("#edit_div_voucher").html(html);
      }

      $("#edit_contract_value_total").val(formatMoney(parseInt(data.contract_value),'VNĐ')).attr('disabled','disabled');
      $('#update_contract').modal('show');
    });
}
function getVoucher(course_id) {
  $.ajax({
    method: "POST",
    url: '<?php echo admin_url('courseads/ajax/course/getVoucher');?>',
    data: { course_id: course_id}
  })
    .done(function( data ) {
      $("#voucher_code_value").val(data.code);
      $("#voucher_code_qty").val(data.qty);
      $('#show_voucher').modal('show');
      console.log(data);
    });
}
function getCourse() {
  var course_id = $("#course_id").val();
  $.ajax({
    method: "POST",
    url: '<?php echo admin_url('courseads/ajax/dataset/getCourse');?>',
    data: { course_id: course_id}
  })
    .done(function( data ) {
      $("#course_value").val(formatMoney(parseInt(data.course_value),'VNĐ'));
      $('#course_value').attr('data-value', data.course_value);
      if(parseInt($('#contract_voucher_value').attr('data-value')) > 0)
      {

          var total = parseInt($("#course_value").attr('data-value')) * (100 - parseInt($('#contract_voucher_value').attr('data-value'))) / 100;
          $("#contract_value_total").val(formatMoney(parseInt(total),'VNĐ'));
      }else{
          $("#contract_value_total").val(formatMoney(parseInt($("#course_value").attr('data-value')),'VNĐ'));
      }
      console.log(data);
    });
}
function checkVoucher() {
    var course_id = $("#course_id").val();
    var voucher_code = $("#voucher_code").val();
    var html = '';
    $.ajax({
      method: "POST",
      url: '<?php echo admin_url('courseads/ajax/Contract/checkVoucher');?>',
      data: { course_id: course_id,voucher_code: voucher_code}
    })
      .done(function( data ) {
          if(data.success == true){
              html += "<div class='form-group'>";
              html += "<label for='inputEmail3' class='col-sm-2 control-label'>Giá trị được giảm:</label>";
              html += "<div class='form-group col-md-10'><div class='input-group col-sm-12'>";
              html += "<input type='text' name='meta[voucher_value]' value='" + data.voucher_value + "'  data-value='" + data.voucher_value + "' id='contract_voucher_value' disabled='disabled' class='form-control form-control'>";
              html += "</div>";
              html += "</div>";
              html += "</div>";

          }else{
              html += "<div class='form-group'>";
              html += "<label for='inputEmail3' class='col-sm-2 control-label'> </label>";
              html += "<div class='form-group col-md-10'><div class='input-group col-sm-12'>";
              html += "<label class='error'>" + data.msg + "</label>";
              html += "</div>";
              html += "</div>";
              html += "</div>";
          }
          $("#div_voucher").html(html);
          if(parseInt($('#contract_voucher_value').attr('data-value')) > 0)
          {
              var total = parseInt($("#course_value").attr('data-value')) - parseInt($('#contract_voucher_value').attr('data-value'));
              $("#contract_value_total").val(formatMoney(parseInt(total),'VNĐ'));
          }else{
              $("#contract_value_total").val(formatMoney(parseInt($("#course_value").attr('data-value')),'VNĐ'));
          }
          console.log(data);
      });
}
function checkEmail() {
    var contract_email = $("#contract_email").val();
    var html = '';
    if(contract_email == '')
    {
        html += "<div class='form-group'>";
        html += "<label for='inputEmail3' class='col-sm-2 control-label'> </label>";
        html += "<div class='form-group col-md-10'><div class='input-group col-sm-12'>";
        html += "<label class='error'>Chưa nhập email</label>";
        html += "</div>";
        html += "</div>";
        html += "</div>";
        $("#div_email").html(html);
    }
    else{
        $.ajax({
          method: "POST",
          url: '<?php echo admin_url('courseads/ajax/Contract/checkEmail');?>',
          data: { contract_email: contract_email}
        })
        .done(function( data ) {
            if(data.success == true){
                $("#contract_phone").val(data.student.phone);
                $("#contract_name").val(data.student.display_name);
                html += "<div class='form-group'>";
                html += "<label for='inputEmail3' class='col-sm-2 control-label'> </label>";
                html += "<div class='form-group col-md-10'><div class='input-group col-sm-12'>";
                html += "<label class='success'>" + data.msg + "</label>";
                html += "</div>";
                html += "</div>";
                html += "</div>";
                $("#div_email").html(html);
                window.setTimeout(function(){$("#div_email").empty();},3000);
            }else{
                html += "<div class='form-group'>";
                html += "<label for='inputEmail3' class='col-sm-2 control-label'> </label>";
                html += "<div class='form-group col-md-10'><div class='input-group col-sm-12'>";
                html += "<label class='error'>" + data.msg + "</label>";
                html += "</div>";
                html += "</div>";
                html += "</div>";
                $("#div_email").html(html);
            }
            console.log(data);
        });
    }

}
function checkPhone() {
    var contract_phone = $("#contract_phone").val();
    var html = '';
    if(contract_phone == '')
    {
        html += "<div class='form-group'>";
        html += "<label for='inputEmail3' class='col-sm-2 control-label'> </label>";
        html += "<div class='form-group col-md-10'><div class='input-group col-sm-12'>";
        html += "<label class='error'>Chưa nhập số điện thoại</label>";
        html += "</div>";
        html += "</div>";
        html += "</div>";
        $("#div_phone").html(html);
    }
    else{
        $.ajax({
          method: "POST",
          url: '<?php echo admin_url('courseads/ajax/Contract/checkPhone');?>',
          data: { contract_phone: contract_phone}
        })
        .done(function( data ) {
            if(data.success == true){
                $("#contract_email").val(data.student.user_email);
                $("#contract_name").val(data.student.display_name);
                html += "<div class='form-group'>";
                html += "<label for='inputEmail3' class='col-sm-2 control-label'> </label>";
                html += "<div class='form-group col-md-10'><div class='input-group col-sm-12'>";
                html += "<label class='success'>" + data.msg + "</label>";
                html += "</div>";
                html += "</div>";
                html += "</div>";
                $("#div_phone").html(html);
                window.setTimeout(function(){$("#div_phone").empty();},3000);
            }else{
                html += "<div class='form-group'>";
                html += "<label for='inputEmail3' class='col-sm-2 control-label'> </label>";
                html += "<div class='form-group col-md-10'><div class='input-group col-sm-12'>";
                html += "<label class='error'>" + data.msg + "</label>";
                html += "</div>";
                html += "</div>";
                html += "</div>";
                $("#div_phone").html(html);
            }
            console.log(data);
        });
    }

}
function formatMoney(n, currency) {
    return n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    }) + " " + currency;
}
$(function(){
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    $('#all-checkbox').on('ifChecked', function(e) {
        $('.table input:checkbox').iCheck('check');
    });

    $('#all-checkbox').on('ifUnchecked', function(e) {
        $('.table input:checkbox').iCheck('uncheck');
    });

    $(':input[name|="notification_of_payment"]').parent().removeClass('col-xs-2').addClass('col-xs-3') ;
$("#create_contract").find('form').validate({
    rules: {
        'meta[contract_name]': { required : true },
        'meta[contract_email]': {
            required : true,
            email: true
        },
        'meta[contract_phone]': {
            required : true,
            number: true
         },
        'meta[course]': { required : true },
        'meta[student_name][]': { required : true },
        'meta[student_email][]': {
            required : true,
            email: true
        },
        'meta[student_phone][]': {
            required : true,
            number: true
         },
    },
    messages:{
        'meta[contract_name]': 'Tên khách hàng là bắt buộc !',
        'meta[contract_email]': {
          required: "Email khách hàng là bắt buộc !",
          email: "Email không đúng định dạng !"
        },
        'meta[contract_phone]': {
          required: "Số điện thoại khách hàng là bắt buộc !",
          number: "Số điện thoại không đúng định dạng !"
        },
        'meta[course]': 'Khóa học là bắt buộc !',
        'meta[student_name][]': 'Tên học viên là bắt buộc !',
        'meta[student_email][]': {
          required: "Email học viên là bắt buộc !",
          email: "Email không đúng định dạng !"
        },
        'meta[student_phone][]': {
          required: "Số điện thoại học viên là bắt buộc !",
          number: "Số điện thoại không đúng định dạng !"
        },
    }
});
$('.create_contract_btn').click(function(){
    var form = $(this).closest('form');
    var valid = form.valid();
    if(!valid) return false;
    console.log("Thành công");
    $.post( form.attr('action'),form.serializeArray(),
    function( response ) {
      $.notify(response.msg,'success');
      $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='success-handler'>"+response.msg+"</div></div>");
      $('#create_contract').modal('hide');
      window.setTimeout(function(){location.reload()},1000);
    },"json").fail(function(response) {
        $.notify(response.responseJSON.msg,'error');
        $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='error-handler'>"+response.responseJSON.msg+"</div></div>");
    });
});
$("#update_contract").find('form').validate({
    rules: {
        'edit[meta][contract_name]': { required : true },
        'edit[meta][contract_email]': {
            required : true,
            email: true
        },
        'edit[meta][contract_phone]': {
            required : true,
            number: true
         },
        'edit[meta][course]': { required : true }
    },
    messages:{
        'edit[meta][contract_name]': 'Tên khách hàng là bắt buộc !',
        'edit[meta][contract_email]': {
          required: "Email khách hàng là bắt buộc !",
          email: "Email không đúng định dạng !"
        },
        'edit[meta][contract_phone]': {
          required: "Số điện thoại khách hàng là bắt buộc !",
          number: "Số điện thoại không đúng định dạng !"
        },
        'edit[meta][course]': 'Khóa học là bắt buộc !'
    }
});
$('.update_contract_btn').click(function(){
    var form = $(this).closest('form');
    var valid = form.valid();
    if(!valid) return false;
    console.log("Thành công");
    $(':disabled').each(function(e) {
        $(this).removeAttr('disabled');
    })
    $.post( form.attr('action'),form.serializeArray(),
    function( response ) {
      $.notify(response.msg,'success');
      $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='success-handler'>"+response.msg+"</div></div>");
      $('#update_contract').modal('hide');
      window.setTimeout(function(){location.reload()},1000);
    },"json").fail(function(response) {
        $.notify(response.responseJSON.msg,'error');
        $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='error-handler'>"+response.responseJSON.msg+"</div></div>");
    });
});
window.setTimeout(function(){$('.where_course_code').select2()},3000);

});
</script>
<!-- /.row -->
<!-- Modal -->

<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/courseads/js/contract.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/component/ui.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/app.js'));
/* End of file index.php */
/* Location: ./application/modules/googleads/views/admin/index.php */
