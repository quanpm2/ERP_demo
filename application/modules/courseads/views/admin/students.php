<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->stylesheet->add('modules/contract/css/style.css');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');
$this->template->stylesheet->add('plugins/timepicker/bootstrap-timepicker.min.css');
$this->template->javascript->add('plugins/timepicker/bootstrap-timepicker.js');

$this->load->config('courseads');
?>

<div class="row" id="contract-databuilder-container">
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-plus-square"></i> Tất cả học viên</a></li>
                <li class="pull-right">
                    <button type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#create_student"><i class="fa fa-fw fa-plus "></i> Học viên</button>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <?php echo $this->admin_form->datatable(['remote_url'=>admin_url('courseads/ajax/dataset/students')]);?>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
<?php
$course_student_create_api_url = admin_url('courseads/ajax/student/create');
?>
<div class="modal" id="create_student" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Tạo học viên mới</h4>
            </div>
            <?php echo $this->admin_form->form_open($course_student_create_api_url,[],['edit[user_type]'=>'customer_person']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                      echo "<h3>Thông tin học viên</h3>";
                      echo $this->admin_form->input('Họ tên','meta[student_name]','','', array('id'=>'contract_name'));
                      echo $this->admin_form->input('Số điện thoại','meta[student_phone]','','', array('id'=>'student_phone'));
                      echo $this->admin_form->input('Email','meta[student_email]','','', array('id'=>'student_email'));
                    ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?php echo form_submit(array('type'=>'button','class'=>'btn btn-primary create_student_btn'),'Tạo mới học viên');?>
            </div>
            <?php echo $this->admin_form->form_close(); ?>
        </div>
    </div>
</div>
<?php
$course_student_update_api_url = admin_url('courseads/ajax/student/update');
?>
<div class="modal" id="update_student" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Cập nhập học viên</h4>
            </div>
            <?php echo $this->admin_form->form_open($course_student_update_api_url,[],['edit[user_type]'=>'customer_person']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                      echo "<h3>Thông tin học viên</h3>";
                      echo "<input name='edit[meta][student_id]' id='edit_contract_id' type='hidden'/>";
                      echo $this->admin_form->input('Họ tên','edit[meta][student_name]','','', array('id'=>'edit_contract_name'));
                      echo $this->admin_form->input('Số điện thoại','edit[meta][student_phone]','','', array('id'=>'edit_student_phone'));
                      echo $this->admin_form->input('Email','edit[meta][student_email]','','', array('id'=>'edit_student_email'));
                    ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?php echo form_submit(array('type'=>'button','class'=>'btn btn-primary update_student_btn'),'Cập nhập học viên');?>
            </div>
            <?php echo $this->admin_form->form_close(); ?>
        </div>
    </div>
</div>
<script>
function showEdit(student_id){
  $.ajax({
    method: "POST",
    url: '<?php echo admin_url('courseads/ajax/dataset/getStudent');?>',
    data: { student_id: student_id}
  })
    .done(function( data ) {
      console.log(data);
      $("#edit_contract_id").val(data.id);
      $("#edit_contract_name").val(data.display_name);
      $("#edit_student_phone").val(data.student_phone);
      $("#edit_student_email").val(data.user_email);
      $('#update_student').modal('show');
    });
}
$(function(){
    $("#create_student").find('form').validate({
        rules: {
            'meta[student_name]': { required : true },
            'meta[student_email]': {
                required : true,
                email: true
            },
            'meta[student_phone]': {
                required : true,
                number: true
             },
        },
        messages:{
            'meta[student_name]': 'Tên học viên là bắt buộc !',
            'meta[student_email]': {
              required: "Email học viên là bắt buộc !",
              email: "Email học viên định dạng !"
            },
            'meta[student_phone]': {
              required: "Số điện thoại học viên là bắt buộc !",
              number: "Số điện thoại không đúng định dạng !"
            },
        }
    });
    $('.create_student_btn').click(function(){

        var form = $(this).closest('form');
        var valid = form.valid();
        if(!valid) return false;
        console.log("Thành công");
        $.post( form.attr('action'),form.serializeArray(),
        function( response ) {
          $.notify(response.msg,'success');
          $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='success-handler'>"+response.msg+"</div></div>");
          $('#create_student').modal('hide');
          window.setTimeout(function(){location.reload()},1000);
        },"json").fail(function(response) {
            $.notify(response.responseJSON.msg,'error');
            $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='error-handler'>"+response.responseJSON.msg+"</div></div>");
        });
    });
    $("#update_student").find('form').validate({
        rules: {
            'edit[meta][student_name]': { required : true },
            'edit[meta][student_email]': {
                required : true,
                email: true
            },
            'edit[meta][student_phone]': {
                required : true,
                number: true
             },
        },
        messages:{
            'edit[meta][student_name]': 'Tên học viên là bắt buộc !',
            'edit[meta][student_email]': {
              required: "Email học viên là bắt buộc !",
              email: "Email học viên định dạng !"
            },
            'edit[meta][student_phone]': {
              required: "Số điện thoại học viên là bắt buộc !",
              number: "Số điện thoại không đúng định dạng !"
            },
        }
    });
    $('.update_student_btn').click(function(){

    var form = $(this).closest('form');
    var valid = form.valid();
    if(!valid) return false;
    console.log("Thành công");
    $.post( form.attr('action'),form.serializeArray(),
    function( response ) {
      $.notify(response.msg,'success');
      $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='success-handler'>"+response.msg+"</div></div>");
      $('#update_student').modal('hide');
      window.setTimeout(function(){location.reload()},1000);
    },"json").fail(function(response) {
        $.notify(response.responseJSON.msg,'error');
        $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='error-handler'>"+response.responseJSON.msg+"</div></div>");
    });
});
    window.setTimeout(function(){$('.where_course_code').select2()},3000);
});

</script>
<!-- /.row -->
<!-- Modal -->

<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/component/ui.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/app.js'));
/* End of file index.php */
/* Location: ./application/modules/googleads/views/admin/index.php */
