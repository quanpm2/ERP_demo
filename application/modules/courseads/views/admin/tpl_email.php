
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <title>Kính gửi Quý học viên của Guru</title>
  <style type="text/css">
  </style>
</head>
<body>
    <span>Kính gửi Quý học viên của Guru</span>
    <h4>Guru trân trọng cảm ơn và xác nhận Quý học viên đã chuyển khoản thanh toán thành công học phí Khóa học của Guru</h4>
    <p>Đồng thời, Guru xin gửi đến Quý học viên tất cả thông tin cần thiết về Khóa học của Guru.</p>
    <p>Quý học viên vui lòng đọc hết email này để không để lỡ bất kì thông tin quan trọng nào từ Học viện Guru.</p>
    <?php

    foreach ($course as $key => $value) {
    ?>
    <h4><?php echo $value['title']; ?></h4>
    <ul>
        <li>KHAI GIẢNG: <?php echo  my_date($value['start_date'],'d/m/Y'); ?></li>
        <li>THỜI LƯỢNG: <?php echo $value['total_course_time']; ?> buổi</li>
        <?php foreach ($value['day_in_week'] as $key1 => $value1) {
            switch ($key1) {
                case 'monday':
                    ?>
                    <li>Thứ hai : <?php echo  $value1['time_start'].' - '.$value1['time_end']; ?></li>
                    <?php
                    break;
                case 'tuesday':
                    ?>
                    <li>Thứ ba : <?php echo  $value1['time_start'].' - '.$value1['time_end']; ?></li>
                    <?php
                    break;
                case 'wednesday':
                    ?>
                    <li>Thứ tư : <?php echo  $value1['time_start'].' - '.$value1['time_end']; ?></li>
                    <?php
                    break;
                case 'thursday':
                    ?>
                    <li>Thứ năm : <?php echo  $value1['time_start'].' - '.$value1['time_end']; ?></li>
                    <?php
                    break;
                case 'friday':
                    ?>
                    <li>Thứ sáu : <?php echo  $value1['time_start'].' - '.$value['time_end']; ?></li>
                    <?php
                    break;
                case 'saturday':
                    ?>
                    <li>Thứ bảy : <?php echo  $value1['time_start'].' - '.$value1['time_end']; ?></li>
                    <?php
                    break;
                case 'sunday':
                    ?>
                    <li>Chủ nhật : <?php echo  $value1['time_start'].' - '.$value1['time_end']; ?></li>
                    <?php
                    break;

                default:
                    // code...
                    break;
            }
            }
        ?>

        <li>HỌC PHÍ: Học phí niêm yết 2017: <?php echo  number_format($value['contract_value']); ?> vnđ (học phí <?php if($value['voucher_value'] == 0) echo "chưa"; else echo "đã"?> giảm theo voucher)</li>
        <li>ĐỊA ĐIỂM: <a href="https://www.google.com/maps/dir/''/adsplus/@10.7716061,106.6878069,18.5z/data=!4m8!4m7!1m0!1m5!1m1!1s0x31752f3a60446f2f:0xa2a453088acdd2a1!2m2!1d106.687262!2d10.771795" target="_blank">Tầng 8, Tòa nhà Việt-Úc 402 Nguyễn Thị Minh Khai, P.5, Q.3, TP.HCM</a></li>
        
    </ul>
    <?php
    }
    ?>
    <img src="<?php echo base_url();?>template/admin/img/map.PNG" width="25" height="37" alt="img" />
    <h4 style="color:red">Để đảm bảo Khóa học tương tác tốt, Quý học viên lưu ý chuẩn bị:</h4>
    <ol>
        <li>Laptop để thực hành</li>
        <li>Website (không cần là chủ website) & sản phẩm để thực hành quảng cáo</li>
        <li>Thẻ ATM VISA hoặc Master khi tham gia khóa học để thực hành quảng cáo.</li>
        <li>Bản đồ hướng dẫn tới địa chỉ của lớp học</li>
        <li>Quý học viên vui lòng tham gia Group lớp học để phục vụ cho quá trình học:
            <br/><a href="https://www.facebook.com/groups/1247139258765398/" target="_blank"> Group Facebook</a>
            <br/><b>Và Group chat Skype được Giảng viên yêu cầu trên lớp</b>
        </li>
    </ol>
    <p>Mọi thông tin thắc mắc vui lòng liên hệ Hotline: 0965970024   hoặc Email: <a href="mailto:guru@adsplus.vn">guru@adsplus.vn</a></p>
    <br/>
    <br/>
    <span>Học viên Guru trân trọng cảm ơn!</span>
</body>
</html>
