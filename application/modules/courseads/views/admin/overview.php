<?php   
    $this->template->javascript->add('plugins/validate/jquery.validate.js');
    $this->template->stylesheet->add('plugins/bootstrap-fileinput/css/fileinput.css');
    $this->template->javascript->add('plugins/bootstrap-fileinput/js/fileinput.min.js');
?>
<?php
    // T/G kích hoạt
    $contract_start         = get_term_meta_value($term_id, 'start_service_time');
    // T/G bắt đầu
    $contract_begin        = get_term_meta_value($term_id, 'contract_begin');
    // T/G kết thúc
    $contract_end          = get_term_meta_value($term_id, 'contract_end');
    //Số buổi học
    $term_courses   = $this->term_posts_m->get_term_posts($term_id, $this->course_m->post_type);

    $total_course_time =0;
    $data = array();
    if(!empty($term_courses)){
        foreach ($term_courses as $key => $value) {

                $total_course_time = get_post_meta_value($value->post_id,'total_course_time');
                $data['day_in_week'][$value->post_id]['title'] = $value->post_title;
                $data['day_in_week'][$value->post_id]['week'] = unserialize(get_post_meta_value($value->post_id,'day_in_week'));
                $data['day_in_week'][$value->post_id]['start_date'] = $value->start_date;
                $data['day_in_week'][$value->post_id]['end_date'] = $value->end_date;
            }    
    }
    
    // Nhân viên kinh doanh
     $sale_id               = get_term_meta_value($term_id,'staff_business');

     $staff_business        = 'Chưa có' ;
     if($sale_id) $staff_business     = $this->admin_m->get_field_by_id($sale_id,'display_name');

     // Học viên.... lấy trong setting của phiếu đăng ký
    $guru_students = unserialize(get_term_meta_value($term_id,'guru_students'));
    

?> 
<div class="overlay" style="display: none; opacity: 1 !important; z-index: 9999999 !important">
    <i class="fa fa-refresh fa-spin" style="font-size: 72px"></i>
</div>
<!-- Thông tin hợp đồng -->
<div class="row">
    <div class="col-sm-6">
        <?php 
            
            $this->table->set_caption('Thông tin hợp đồng')
                        ->add_row(array('Mã hợp đồng',get_term_meta_value($term_id, 'contract_code')))
                        ->add_row('Thời gian bắt đầu', empty($contract_begin) ? '' : my_date($contract_begin,'d/m/Y'))
                        ->add_row('Thời gian kết thúc', empty($contract_end) ? '' : my_date($contract_end,'d/m/Y'))
                        ->add_row('Thời gian', $total_course_time . ' Buổi')
                        ->add_row('Kinh doanh phụ trách', $staff_business );
             echo $this->table->generate();
            $this->table->clear();
        ?>
    </div>
    <!-- Thông tin khách hàng -->
    <div class="col-sm-6">
            <input type="hidden" name="term_id" value="<?php echo $term_id ; ?>" />
             <?php 
             $representative_gender = get_term_meta_value($term_id, 'representative_gender');
             $this->table->set_caption('Thông tin khách hàng')
                            ->add_row('Chức vụ','Họ Tên','Email','Số điện thoại')
                            ->add_row('Người đại diện',
          '<span id="incline-data">' .($representative_gender == 1 ? 'Ông ' : 'Bà ').str_repeat('&nbsp', 2).get_term_meta_value($term_id,'representative_name').'</span>',get_term_meta_value($term_id, 'representative_email'),get_term_meta_value($term_id, 'representative_phone'));
                            if(!empty($guru_students)){
                                foreach ($guru_students as $key => $value) {
                                    $this->table->add_row('Học viên',$value['name'],$value['email'],$value['phone']);  
                                }
                            }
                    echo $this->table->generate(); 
                    $this->table->clear();
             ?>            
    </div> 
    <!-- Thông tin khóa học -->
    <div class="col-sm-12">
            <input type="hidden" name="term_id" value="<?php echo $term_id ; ?>" />
             <?php 
             $representative_gender = get_term_meta_value($term_id, 'representative_gender');
             $this->table->set_caption('Thông tin khóa học')
             ->add_row('<strong>Thứ</strong>','<strong>T/G bắt đầu</strong>','<strong>T/G kết thúc</strong>');

             $this->load->model('postmeta_m');

                foreach ($data['day_in_week'] as $post_id => $day_in_week) {
                    
                    $this->table->add_row(array('data'=>'<strong>'.$day_in_week['title'].'</strong>','style'=>'background-color:#ddd','colspan'=>3));
                    
                    $day_start = my_date($day_in_week['start_date'],'d');
                    $day_end = my_date($day_in_week['end_date'],'d');
                    $date=array();
                    for ($i=0; $i < ($day_end - $day_start) +1 ; $i++) {
                       
                        $timestamp = strtotime('+'.$i.'days', $day_in_week['start_date']);

                        $date[date('N', $timestamp)] = my_week_name($timestamp).' ('.my_date($timestamp,'d/m/Y').')' ;
                        //

                        //$this->mdate->startOfDay($course_time[0]);
                    }
                    // $dates=array();
                    // $days = unserialize($this->postmeta_m->get_meta_value($post_id,'course_days'));
                    // foreach ($days as $key => $value) {
                    //      $day = $value['day'];
                    //     $date[date('N', $day)] = my_week_name($day).' ('.my_date($day,'d/m/Y').')' ;

                    //     switch (date('N', $day)) {
                    //         case 1:
                    //             if(!empty($date[1]))
                    //                 $this->table->add_row($date[1],$value['time_start'],$value['time_end']);
                    //             break;
                    //         case 2:
                    //             if(!empty($date[2]))
                    //                 $this->table->add_row($date[2],$value['time_start'],$value['time_end']);
                    //             break;
                    //         case 3:
                    //             if(!empty($date[3]))
                    //                 $this->table->add_row($date[3],$value['time_start'],$value['time_end']);
                    //             break;
                    //         case 4:
                    //             if($date[4])
                    //             $this->table->add_row($date[4],$value['time_start'],$value['time_end']);
                    //             break;
                    //         case 5:
                    //             if(!empty($date[5]))
                    //                 $this->table->add_row($date[5],$value['time_start'],$value['time_end']);
                    //             break;
                    //         case 6:
                    //             if(!empty($date[6]))
                    //                 $this->table->add_row($date[6],$value['time_start'],$value['time_end']);
                    //             break;
                    //         case 7:
                    //             if(!empty($date[7]))
                    //                 $this->table->add_row($date[7],$value['time_start'],$value['time_end']);
                    //             break;

                    //         default:
                    //             // code...
                    //             break;
                    //     }
                    // }
                    
                    //prd( my_date(unserialize($a[0]['meta_value'])[0]['day'],'d/m/Y') );
                    
                   
                    
                        
                }
                                    
                        
                echo $this->table->generate(); 
                $this->table->clear();
             ?>            
    </div> 
</div> 


    


   