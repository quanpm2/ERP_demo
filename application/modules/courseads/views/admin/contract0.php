<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->stylesheet->add('modules/contract/css/style.css');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');
$this->template->stylesheet->add('plugins/timepicker/bootstrap-timepicker.min.css');
$this->template->javascript->add('plugins/timepicker/bootstrap-timepicker.js');

$this->load->config('courseads');
?>

<div class="row" id="contract-databuilder-container">
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-plus-square"></i> Tất cả hợp đồng</a></li>
                <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-flag"></i> Đang chờ duyệt</a></li>
                <li class="pull-right">
                    <button type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#create_contract"><i class="fa fa-fw fa-plus "></i> Hợp đồng</button>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <?php echo $this->admin_form->datatable(['remote_url'=>admin_url('courseads/ajax/dataset/contract'),'is_download'=>TRUE]);?>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <?php echo $this->admin_form->datatable(['remote_url'=>admin_url('courseads/ajax/dataset/contract?&where%5Bterm_status%5D=publish'),'is_download'=>TRUE]);?>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
<?php
$course_contract_create_api_url = admin_url('courseads/ajax/course/create_contract');
?>
<div class="modal" id="create_contract" data-backdrop="static">
    <div class="modal-dialog" style="width: 100%;height: 100%;margin: 0;top: 0;left: 0;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Tạo hợp đồng mới</h4>
            </div>
            <?php echo $this->admin_form->form_open($course_contract_create_api_url,[],['edit[user_type]'=>'customer_person']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                    <?php
                      echo "<h3>Thông tin hợp đồng</h3>";
                      echo $this->admin_form->input('Khách hàng','meta[contract_name]');
                      echo $this->admin_form->input('Email khách hàng','meta[contract_email]');
                      echo $this->admin_form->input('Số điện thoại khách hàng','meta[contract_phone]');
                      echo $this->admin_form->dropdown('Khóa học','meta[course]',prepare_dropdown($courses, 'Chọn khóa học'),'','', array('id'=>'course_id','onchange'=>"getCourse()"));
                      echo $this->admin_form->input('Số lượng học viên','meta[contract_total_student]','','', array('class'=>'form-control','disabled'=>'disabled','id'=>'contract_total_student','value'=>'1'));
                      echo $this->admin_form->input('Giá trị khóa học','meta[course_value]','','', array('class'=>'form-control','disabled'=>'disabled','id'=>'course_value'));
                      echo $this->admin_form->input('Tổng giá trị hợp đồng','meta[contract_value_temp]','','', array('class'=>'form-control','disabled'=>'disabled','id'=>'contract_value_temp'));

                      echo $this->admin_form->formGroup_begin(0,'Mã giảm giá');
                      echo "<div class='col-md-7'>";
                      echo form_input(array('name'=>'meta[voucher_code]','class'=>'form-control','id'=>'voucher_code'));
                      echo "</div>";
                      echo "<div class='col-md-2'>";
                      echo form_button(['id'=>'add_total_student','class'=>'btn btn-primary', 'onclick'=>'checkVoucher()'],'<i class="fa fa-fw fa-plus"></i> Kiểm tra voucher');
                      echo "</div>";
                      echo $this->admin_form->formGroup_end();
                      echo "<div id='div_voucher'>";
                      echo "</div>";
                      echo $this->admin_form->input('Giá trị cuối cùng','meta[contract_value_total]','','', array('class'=>'form-control','disabled'=>'disabled','id'=>'contract_value_total'));

                    ?>
                    </div>
                    <?php
                        echo "<div id='list_students' class='col-md-6'>";
                        echo "<h3>Thông tin học viên</h3>";
                        echo "<input type='hidden' id='temp_student' value='1'>";
                        echo "<div class='row'>";
                        echo "<div class='form-group' id='student_1'>";
                        echo "<label for='inputEmail3' class='col-sm-1 control-label'>Học viên 1 :</label>";
                        echo "<div class='form-group col-md-10'>";
                        echo "<div class='col-sm-4'>";
                        echo "<input type='text' name='meta[student_name][]' class='form-control' placeholder='Họ tên học viên'>";
                        echo "</div>";
                        echo "<div class='col-sm-4'>";
                        echo "<input type='text' name='meta[student_email][]' class='form-control' placeholder='Email học viên'>";
                        echo "</div>";
                        echo "<div class='col-sm-4'>";
                        echo "<input type='text' name='meta[student_phone][]' class='form-control' placeholder='Số điện thoại học viên'>";
                        echo "</div>";
                        echo "</div>";
                        echo "<div class='col-sm-1'>";

                        echo "<button type='button' id='add_total_student' onclick='removeStudent(1)' class='btn btn-danger'><i class='fa fa-fw fa-remove'></i></button>";
                        echo "</div>";
                        echo "</div>";
                        echo "</div>";
                        echo "</div>";
                        echo "<button type='button' onclick='addStudent()' class='btn btn-success  pull-right'><i class='fa fa-fw fa-plus'></i>Thêm học viên</button>";
                     ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?php echo form_submit(array('type'=>'button','class'=>'btn btn-primary create_contract_btn','data-dismiss'=>'modal'),'Tạo mới hợp đồng');?>
            </div>
            <?php echo $this->admin_form->form_close(); ?>
        </div>
    </div>
</div>
<script>
function getVoucher(course_id) {
  $.ajax({
    method: "POST",
    url: '<?php echo admin_url('courseads/ajax/course/getVoucher');?>',
    data: { course_id: course_id}
  })
    .done(function( data ) {
      $("#voucher_code_value").val(data.code);
      $("#voucher_code_qty").val(data.qty);
      $('#show_voucher').modal('show');
      console.log(data);
    });
}
function addStudent() {
    var total_temp = $("#temp_student").val();
    var id = parseInt(total_temp) + 1;
    $("#temp_student").val(id);
    var total_student = $("#contract_total_student").val();
    $("#contract_total_student").val(parseInt(total_student) + 1);
    var html = '';
    var temp = parseInt($("#contract_total_student").val()) * parseInt($("#course_value").attr('data-value'));
    $("#contract_value_temp").val(formatMoney(temp,'VNĐ'));
    if(parseInt($('#contract_voucher_value').attr('data-value')) > 0)
    {
        var total = temp * (100 - parseInt($('#contract_voucher_value').attr('data-value'))) / 100;
        $("#contract_value_total").val(formatMoney(parseInt(total),'VNĐ'));
    }else{
        $("#contract_value_total").val(formatMoney(parseInt(temp),'VNĐ'));
    }
    html += "<div class='row'>";
    html += "<div class='form-group' id='student_"+ id +"'>";
    html += "<label for='inputEmail3' class='col-sm-1 control-label'>Học viên"+ id +" :</label>";
    html += "<div class='form-group col-md-10'>";
    html += "<div class='col-sm-4'>";
    html += "<input type='text' name='meta[student_name][]' class='form-control' placeholder='Họ tên học viên'>";
    html += "</div>";
    html += "<div class='col-sm-4'>";
    html += "<input type='text' name='meta[student_email][]' class='form-control' placeholder='Email học viên'>";
    html += "</div>";
    html += "<div class='col-sm-4'>";
    html += "<input type='text' name='meta[student_phone][]' class='form-control' placeholder='Số điện thoại học viên'>";
    html += "</div>";
    html += "</div>";
    html += "<div class='col-sm-1'>";

    html += "<button type='button' id='add_total_student' onclick='removeStudent("+ id +")' class='btn btn-danger'><i class='fa fa-fw fa-remove'></i></button>";
    html += "</div>";
    html += "</div>";
    html += "</div>";
    $("#list_students").append(html);
}
function getCourse() {
  var course_id = $("#course_id").val();
  $.ajax({
    method: "POST",
    url: '<?php echo admin_url('courseads/ajax/dataset/getCourse');?>',
    data: { course_id: course_id}
  })
    .done(function( data ) {
      $("#course_value").val(formatMoney(parseInt(data.course_value),'VNĐ'));
      $('#course_value').attr('data-value', data.course_value);
      var temp = parseInt($("#contract_total_student").val()) * parseInt(data.course_value);

      $("#contract_value_temp").val(formatMoney(temp,'VNĐ'));
      if(parseInt($('#contract_voucher_value').attr('data-value')) > 0)
      {
          var total = temp * (100 - parseInt($('#contract_voucher_value').attr('data-value'))) / 100;
          $("#contract_value_total").val(formatMoney(parseInt(total),'VNĐ'));
      }else{
          $("#contract_value_total").val(formatMoney(parseInt(temp),'VNĐ'));
      }
      console.log(data);
    });
}
function checkVoucher() {
    var course_id = $("#course_id").val();
    var voucher_code = $("#voucher_code").val();
    var html = '';
    $.ajax({
      method: "POST",
      url: '<?php echo admin_url('courseads/ajax/dataset/checkVoucher');?>',
      data: { course_id: course_id,voucher_code: voucher_code}
    })
      .done(function( data ) {
          if(data.success == true){
              html += "<div class='form-group'>";
              html += "<label for='inputEmail3' class='col-sm-2 control-label'>Giá trị được giảm</label>";
              html += "<div class='form-group col-md-10'><div class='input-group col-sm-12'>";
              html += "<input type='text' name='meta[voucher_value]' value='" + data.voucher_value + " %'  data-value='" + data.voucher_value + "' id='contract_voucher_value' disabled='disabled' class='form-control form-control'>";
              html += "</div>";
              html += "</div>";
              html += "</div>";

          }else{
              html += "<div class='form-group'>";
              html += "<label for='inputEmail3' class='col-sm-2 control-label'> </label>";
              html += "<div class='form-group col-md-10'><div class='input-group col-sm-12'>";
              html += "<label class='error'>" + data.msg + "</label>";
              html += "</div>";
              html += "</div>";
              html += "</div>";
          }
          $("#div_voucher").html(html);
          var temp = parseInt($("#contract_total_student").val()) * parseInt($("#course_value").attr('data-value'));
          if(parseInt($('#contract_voucher_value').attr('data-value')) > 0)
          {
              var total = temp * (100 - parseInt($('#contract_voucher_value').attr('data-value'))) / 100;
              $("#contract_value_total").val(formatMoney(parseInt(total),'VNĐ'));
          }else{
              $("#contract_value_total").val(formatMoney(parseInt(temp),'VNĐ'));
          }
          console.log(data);
      });
}
function removeStudent(div_id) {
    var total = $("#contract_total_student").val();
    if(parseInt(total) > 1)
    {
        $("#student_"+div_id).remove();
        $("#contract_total_student").val(parseInt(total) - 1);
        var temp = parseInt($("#contract_total_student").val()) * parseInt($("#course_value").attr('data-value'));
        $("#contract_value_temp").val(formatMoney(temp,'VNĐ'));
        if(parseInt($('#contract_voucher_value').attr('data-value')) > 0)
        {
            var total = temp * (100 - parseInt($('#contract_voucher_value').attr('data-value'))) / 100;
            $("#contract_value_total").val(formatMoney(parseInt(total),'VNĐ'));
        }else{
            $("#contract_value_total").val(formatMoney(parseInt(temp),'VNĐ'));
        }
    }
}
function formatMoney(n, currency) {
    return n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    }) + " " + currency;
}
$(function(){
$("#create_contract").find('form').validate({
    rules: {
        'meta[contract_name]': { required : true },
        'meta[contract_email]': {
            required : true,
            email: true
        },
        'meta[contract_phone]': {
            required : true,
            number: true
         },
        'meta[course]': { required : true },
        'meta[student_name][]': { required : true },
        'meta[student_email][]': {
            required : true,
            email: true
        },
        'meta[student_phone][]': {
            required : true,
            number: true
         },
    },
    messages:{
        'meta[contract_name]': 'Tên khách hàng là bắt buộc !',
        'meta[contract_email]': {
          required: "Email khách hàng là bắt buộc !",
          email: "Email không đúng định dạng !"
        },
        'meta[contract_phone]': {
          required: "Số điện thoại khách hàng là bắt buộc !",
          number: "Số điện thoại không đúng định dạng !"
        },
        'meta[course]': 'Khóa học là bắt buộc !',
        'meta[student_name][]': 'Tên học viên là bắt buộc !',
        'meta[student_email][]': {
          required: "Email học viên là bắt buộc !",
          email: "Email không đúng định dạng !"
        },
        'meta[student_phone][]': {
          required: "Số điện thoại học viên là bắt buộc !",
          number: "Số điện thoại không đúng định dạng !"
        },
    }
});
$('.create_contract_btn').click(function(){
    var form = $(this).closest('form');
    var valid = form.valid();
    if(!valid) return false;
    console.log("Thành công");
    $.post( form.attr('action'),form.serializeArray(),
    function( response ) {
      $.notify(response.msg,'success');
      $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='success-handler'>"+response.msg+"</div></div>");
      window.setTimeout(function(){location.reload()},3000);
    },"json").fail(function(response) {
        $.notify(response.responseJSON.msg,'error');
        $(form).find('.modal-body').prepend("<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='error-handler'>"+response.responseJSON.msg+"</div></div>");
    });
});
});
</script>
<!-- /.row -->
<!-- Modal -->

<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/component/ui.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/app.js'));
/* End of file index.php */
/* Location: ./application/modules/googleads/views/admin/index.php */
