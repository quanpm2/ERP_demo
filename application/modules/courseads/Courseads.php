<?php
class Courseads_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name(){

		return 'Courseads';
	}

	public function init(){
		$this->_load_menu();
		if(has_permission('Admin.Courseads') && is_module_active('courseads'))
		{

		}
	}

	public function title()
	{
		return 'Courseads';
	}

	public function author()
	{
		return 'thonh';
	}

	public function version()
	{
		return '0.1';
	}

	private function _load_menu()
	{
		if(!is_module_active('courseads')) return FALSE;
		if(!has_permission('Courseads.course.Access')) return FALSE;

		$itemId = 'admin-courseads';
		
		$this->menu->add_item(array(
			'id' => $itemId,
			'name' => 'Khóa học',
			'parent' => null,
			'slug' => admin_url('courseads/course'),
			'icon' => 'fa fa-book',
			'order' => 1,
			),'left-ads-service');

		if(!is_module('courseads')) return FALSE;

		$order = 0;

		if(has_permission('Courseads.course.Access'))
		{
			$this->menu->add_item(array(
			'id' => 'admin-courseads-contract',
			'name' => 'Danh sách hợp đồng',
			'parent' => $itemId,
			'slug' => module_url(),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-database'
			), 'left-ads-service');

		}

		if(has_permission('Courseads.course.Access'))
		{
            $this->menu->add_item(array(
                'id' => 'course',
                'name' => 'Khóa học',
                'parent' => $itemId,
                'slug' => module_url('course'),
                'order' => $order++,
                'icon' => 'fa fa-fw fa-xs fa-book'
                ), 'left-ads-service');

		}

		$term_id = $this->uri->segment(4);

		$this->website_id = $term_id;

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		$left_navs = array(
			'overview'=> array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-xs fa-tachometer',
				),
			'kpi' => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-xs fa-heartbeat',
				),
			'setting'  => array(
				'name' => 'Cấu hình',
				'icon' => 'fa fa-fw fa-xs fa-cogs',
				)
			);

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("Courseads.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "Courseads-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
				), 'left-ads-service');
		}

	}

	public function description()
	{
		return 'Quản lý các khóa học của học viện GURU';
	}
	private function init_permissions()
	{
		
		$permissions = array();
        $permissions['Courseads.index'] = array(
			'description' => 'Quản lý Khóa học',
			'actions' => ['access','manage','mdeparment','mgroup']);

		$permissions['Courseads.course'] = array(
			'description' => 'Quản lý khóa học',
			'actions' => ['access','add','update','delete','manage','mdeparment','mgroup']);

		$permissions['Courseads.contract'] = array(
			'description' => 'Quản lý hợp đồng đăng ký khóa học',
			'actions' => ['access','add','update','delete','manage','mdeparment','mgroup']);

		$permissions['Courseads.Kpi'] = array(
			'description' => 'Quản lý Kpi',
			'actions' => array('access','add','delete','update','manage','mdeparment','mgroup'));

		$permissions['Courseads.Setting'] = array(
			'description' => 'Quản lý cấu hình',
			'actions' => array('access','add','delete','update','manage','mdeparment','mgroup'));

		$permissions['Courseads.overview'] = array(
			'description' => 'Tổng quan',
			'actions' => array('access','add','delete','update','manage','mdeparment','mgroup'));

		$permissions['Courseads.Start_service'] = array(
			'description' => 'Thực hiện dịch vụ',
			'actions' => array('update','manage','mdeparment','mgroup'));

		$permissions['Courseads.Stop_service'] = array(
			'description' => 'Kết thúc dịch vụ',
			'actions' 	  => array('update','manage','mdeparment','mgroup'));

		return $permissions;
	}

	private function _update_permissions()
	{
		$permissions = array();
		
		if(!permission_exists('Courseads.Kpi'))
		{
			$permissions['Courseads.Kpi'] = array(
				'description' => 'Quản lý KPI',
				'actions' => array('manage','access','add','delete','update'));
		}

		if(!permission_exists('Courseads.setting'))
		{
			$permissions['Courseads.setting'] = array(
				'description' => 'Cài đặt',
				'actions' => array('manage','access','add','delete','update'));
		}
		if(!permission_exists('Courseads.overview'))
		{
			$permissions['Courseads.overview'] = array(
				'description' => 'Tổng quan',
				'actions' => array('manage','access','add','delete','update'));
		}

		if(!$permissions) return false;
		
		foreach($permissions as $name => $value){
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if( ! $permissions) return FALSE;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if( ! $permissions) return FALSE;

		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}
