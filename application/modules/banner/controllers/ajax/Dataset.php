<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dataset extends Admin_Controller {

	public $term_type = 'banner';

	function __construct()
	{
		parent::__construct();

		$models = array();

		$this->load->library('datatable_builder');
	}

	/**
	 * Trang tổng quan liệt kê danh sách tất cả các hợp đồng
	 */
	public function index()
	{
		$response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

		if( ! has_permission('banner.index.access'))
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		$defaults 	= ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
		$args 		= wp_parse_args( $this->input->get(), $defaults);

		$data = $this->data; // remove-after

		// Authorization for user role
		if(!has_permission('Banner.Index.Manage') && !has_permission('Banner.Sale.Access') )
		{
			$this->datatable_builder->join('webgeneral_kpi','webgeneral_kpi.term_id = term.term_id');
			$this->datatable_builder->where('webgeneral_kpi.user_id', $this->admin_m->id);
		}

		/* Applies get query params for filter */
		$this->search_filter();

		$this->datatable_builder
		->set_filter_position(FILTER_TOP_OUTTER)
		->select('term.term_id,term.term_name')
		->add_search('term.term_id',['placeholder'=>'ID'])
		->add_search('term.term_name',['placeholder'=>'Website'])
		->add_search('contract_begin',['placeholder'=>'Ngày bắt đầu','class'=>'form-control input_daterange'])
		->add_search('contract_end',['placeholder'=>'Ngày kết thúc','class'=>'form-control input_daterange'])
		->add_search('staff_business',['placeholder'=>'Kinh doanh phụ trách'])

		->add_column('term.term_id','ID')
		->add_column('term.term_name','Website')
		->add_column('contract_begin', array('set_select'=> FALSE, 'title'=> 'T/G bắt đầu'))
		->add_column('contract_end', array('set_select'=> FALSE, 'title'=> 'T/G kết thúc'))
		->add_column('contract_value', array('set_select' => FALSE,'title'=> 'Trước VAT'))
		->add_column('payment_percentage', array('set_select' => FALSE,'title'=> 'T/đ thanh toán'))
		->add_column('staff_business', array('set_select'=>FALSE,'title'=>'Kinh Doanh'))
		->add_column('action', array('set_select'=>FALSE,'title'=>'Actions','set_order'=>FALSE))
		
		->add_column_callback('term_name',function($data,$row_name){

			$website = mb_strlen($data['term_name']) > 25 ? substr($data['term_name'], 0, 25).'...' : $data['term_name'];

			$data['term_name'] = anchor(module_url("overview/{$data['term_id']}"), $website);
			return $data;
		},FALSE)

		->add_column_callback('contract_begin',function($data,$row_name){
			$data['contract_begin'] = my_date(get_term_meta_value($data['term_id'], 'contract_begin')) ;
			return $data;
		},FALSE)
		
		->add_column_callback('contract_end',function($data,$row_name){
			$data['contract_end'] = my_date(get_term_meta_value($data['term_id'], 'contract_end')) ;
			return $data;
		},FALSE)

		->add_column_callback('contract_value',function($data,$row_name){
			$data['contract_value'] = currency_numberformat(get_term_meta_value($data['term_id'],'contract_value'),'đ');
			return $data;
		},FALSE)

		->add_column_callback('payment_percentage',function($data,$row_name){
			$term_id = $data['term_id'];
			$invs_amount = (double) get_term_meta_value($term_id,'invs_amount');
			$payment_amount = (double) get_term_meta_value($term_id,'payment_amount');

			$payment_percentage = 100 * (double) get_term_meta_value($term_id,'payment_percentage');

			if($payment_percentage >= 50 && $payment_percentage < 80) $text_color = 'text-yellow';
			else if($payment_percentage >= 80 && $payment_percentage < 90) $text_color = 'text-light-blue';
			else if($payment_percentage >= 90) $text_color = 'text-green';
			else $text_color = 'text-red';

			$payment_percentage = currency_numberformat($payment_percentage,' %');
			$data['payment_percentage'] = "<b class='{$text_color}'>{$payment_percentage}</b>";
			return $data;
		},FALSE)

		->add_column_callback('staff_business',function($data,$row_name) {
			$sale_id = get_term_meta_value($data['term_id'],'staff_business');
			$data['staff_business'] = $this->admin_m->get_field_by_id($sale_id,'display_name');
			return $data;
	  	},FALSE)
		
		->add_column_callback('action', function($data, $row_name) {
		
			if(has_permission('banner.overview.access'))
			{
				$data['action'].= anchor(module_url("overview/{$data['term_id']}"),'<i class="fa fa-fw fa-line-chart"></i>','data-toggle="tooltip" title="Tổng quan"');
			}

			$data['action'].= anchor("admin/contract/edit/{$data['term_id']}", '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>','data-toggle="tooltip" title="Hợp đồng"');

			return $data;
		}, FALSE)
		->from('term');

		$status_list = array_keys($this->config->item('contract_status'));
		array_unshift($status_list, 1);

		$this->datatable_builder
		->where_in('term_status', $status_list)
		->where('term_type','banner')
		->group_by('term.term_id');

		$pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

		$data = $this->datatable_builder->generate($pagination_config);

		// OUTPUT : DOWNLOAD XLSX
		if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
		{
			$this->export($last_query);
			return TRUE;
		}

		
		return parent::renderJson($data);

	}

	protected function search_filter($search_args = array())
	{
		$args = $this->input->get();

		if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

		// Contract_begin FILTERING & SORTING
		$filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
		$sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
		if($filter_contract_begin || $sort_contract_begin)
		{
			$alias = uniqid('contract_begin_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

			if($filter_contract_begin)
			{	
				$dates = explode(' - ', $filter_contract_begin);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['contract_begin']);
			}

			if($sort_contract_begin)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_begin);
				unset($args['order_by']['contract_begin']);
			}
		}

		// Contract_end FILTERING & SORTING
		$filter_contract_end = $args['where']['contract_end'] ?? FALSE;
		$sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
		if($filter_contract_end || $sort_contract_end)
		{
			$alias = uniqid('contract_end_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_end'", 'LEFT');

			if($filter_contract_end)
			{	
				$dates = explode(' - ', $filter_contract_end);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['contract_end']);
			}

			if($sort_contract_end)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_end);
				unset($args['order_by']['contract_end']);
			}
		}

		// Staff_business FILTERING & SORTING
		$filter_staff_business = $args['where']['staff_business'] ?? FALSE;
		$sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
		if($filter_staff_business || $sort_staff_business)
		{
			$alias = uniqid('staff_business_');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT')
			->join("user","{$alias}.meta_value = user.user_id", 'LEFT');

			if($filter_staff_business)
			{	
				$this->datatable_builder->like("user.display_name",$filter_staff_business);
				unset($args['where']['staff_business']);
			}

			if($sort_staff_business)
			{
				$this->datatable_builder->order_by('user.display_name',$sort_staff_business);
				unset($args['order_by']['staff_business']);
			}
		}

		// contract_value FILTERING & SORTING
		$filter_contract_value = $args['where']['contract_value'] ?? FALSE;
		$sort_contract_value = $args['order_by']['contract_value'] ?? FALSE;
		if($filter_contract_value || $sort_contract_value)
		{
			$alias = uniqid('contract_value_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_value'", 'LEFT');

			if($filter_contract_value)
			{		
				$this->datatable_builder->where("{$alias}.meta_value", $filter_contract_value);
				unset($args['where']['contract_value']);
			}

			if($sort_contract_value)
			{
				$this->datatable_builder->order_by("({$alias}.meta_value)*1",$sort_contract_value);
				unset($args['order_by']['contract_value']);
			}
		}

		// payment_percentage FILTERING & SORTING
		$filter_payment_percentage = $args['where']['payment_percentage'] ?? FALSE;
		$sort_payment_percentage = $args['order_by']['payment_percentage'] ?? FALSE;
		if($filter_payment_percentage || $sort_payment_percentage)
		{
			$alias = uniqid('payment_percentage_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_percentage'", 'LEFT');

			if($filter_payment_percentage)
			{		
				$this->datatable_builder->where("{$alias}.meta_value", $filter_payment_percentage);
				unset($args['where']['payment_percentage']);
			}

			if($sort_payment_percentage)
			{
				$this->datatable_builder->order_by("({$alias}.meta_value)*1",$sort_payment_percentage);
				unset($args['order_by']['payment_percentage']);
			}
		}

		// APPLY DEFAULT FILTER BY MUTATE FIELD		
		$args = $this->datatable_builder->parse_relations_searches($args);
		if( ! empty($args['where']))
		{
			foreach ($args['where'] as $key => $value)
			{
				if(empty($value)) continue;

				if(empty($key))
				{
					$this->datatable_builder->add_filter($value, '');
					continue;
				}

				$this->datatable_builder->add_filter($key, $value);
			}
		}

		if(!empty($args['order_by'])) 
		{
			foreach ($args['order_by'] as $key => $value)
			{
				$this->datatable_builder->order_by($key, $value);
			}
		}
	}
}
/* End of file banner.php */
/* Location: ./application/modules/banner/controllers/ajax/dataset.php */