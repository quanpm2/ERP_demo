<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends Admin_Controller 
{
	private $term;
	private $term_type = 'banner';
	public  $model 	   = 'banner_contract_m';

	public function __construct(){

		parent::__construct();
		
		$this->load->model('term_users_m');
		$this->load->model('banner/banner_kpi_m') ;
		$this->load->model('banner/banner_contract_m');
		$this->load->model('banner/banner_m');
		$this->load->model('banner/banner_report_m');
		$this->load->model('customer/customer_m');
		$this->load->model('contract/contract_m');
		$this->load->model('contract/contract_wizard_m');
		$this->load->model('contract/invoice_m');
		$this->load->model('webgeneral/webgeneral_callback_m') ;
		$this->load->config('contract/contract');
		$this->load->config('staffs/group');
		$this->load->config('contract/invoice');
		$this->load->config('banner/banner');
		$this->load->model('webgeneral/webgeneral_kpi_m');
		$this->load->model('staffs/sale_m');

		// kiểm tra banner có tồn tại và user có quyền truy cập ?
		$this->init_website();
	}

	private function init_website()
	{
		$term_id = $this->uri->segment(4);	
		$method  = $this->uri->segment(3);
		$is_allowed_method = (!in_array($method, array('index','done')));
		if(!$is_allowed_method || empty($term_id)) return FALSE;

		$term = $this->term_m
		->where('term_status','publish')		
		->where('term_type',$this->term_type)

		->where('term_id',$term_id)
		->get_by();
		
		if(empty($term) OR !$this->is_assigned($term_id)) 
		{
			$this->messages->error('Truy cập #'.$term_id.' không hợp lệ !!!');
			redirect(module_url(),'refresh');
		}


		$this->template->title->set(strtoupper(' '.$term->term_name));
		$this->website_id = $this->data['term_id'] = $term_id;
		$this->data['term'] = $this->term = $term;
	}
	public function index()
	{
		restrict('banner.index.access');
		$this->template->is_box_open->set(1);
		$this->template->title->set('HĐ Thiết kế banner');
		$this->template->description->set('đang thực hiện');

		parent::render($this->data);
	}

	public function overview($term_id = 0)
	{
		restrict('banner.overview.access');
		$this->scache->delete('term/' .$term_id . '_meta');

		$term = $this->term_m->get($term_id);

		if(empty($term)){
			$this->messages->error('Dịch vụ không tồn tại');
			redirect(module_url(),'refresh');
		}

		$time = time();	
		$day  = date('d',$time) - 1;

		if($day == 0){
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}

		$contract_begin        = get_term_meta_value($term_id, 'contract_begin');
    	$contract_end          = get_term_meta_value($term_id, 'contract_end');

    	$price_banner           = get_term_meta_value($term_id, 'price_banner') ;
    	$number_banner			= get_term_meta_value($term_id,'number_banner');


    	$date_start  		   = date('d/m/Y', $contract_begin);
		$date_end 	 		   = date('d/m/Y', $contract_end);

		$description = 'Từ '.$date_start.' đến '.$date_end;

		$time_start  = $this->mdate->startOfDay(strtotime($date_start));
		$time_end    = $this->mdate->endOfDay(strtotime($date_end));

		$this->template->description->append($description);
		$this->template->title->prepend('Tổng quan');
		$this->template->javascript->add('plugins/chartjs/Chart.js');


		/* Load danh sách nhân viên kinh doanh */
		$staffs = $this->sale_m->select('user_id,user_email,display_name')->set_user_type()->set_role()->as_array()->get_all();
		$staffs = array_map(function($x){ $x['display_name'] = $x['display_name'] ?: $x['user_email']; return $x; }, $staffs);
		$data['staffs'] = key_value($staffs, 'user_id', 'display_name');

		$data['websites'] = array();
		$data['website']  = $term->term_name ;
		$data['invoice_url'] = module_url('invoices/');
		$data['term_id'] 			 = $term_id;

		parent::render($data);
	}

	public function kpi($term_id = 0, $delete_id= 0)
	{
		restrict('banner.kpi.access');	
		$this->config->load('banner/group') ;			
		$this->template->title->prepend('KPI');
		$data = $this->data;
		$this->submit_kpi($term_id,$delete_id);

		$targets = $this->banner_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(array(
			'term_id'=> $term_id
			));

		$data['targets'] = array();

		$data['targets']['tech'] = array();
		foreach($targets as $target)
		{
			$data['targets'][$target['kpi_type']][] = $target;
		}
		
		$data['users'] = $this->admin_m->select('user_id,display_name')->where_in('role_id', $this->config->item('group_memberId'))->set_get_active()->order_by('display_name')->get_many_by();
		$data['users'] = key_value($data['users'],'user_id','display_name');

		$get_config_banners  		= $this->config->item('packages_banner') ;
		if(empty($get_config_banners)) return FALSE;
			
		foreach ($get_config_banners as $key_group => &$group_banner) 
		{
			foreach ($group_banner as $key => $banner) 
			{	
				$quanties 		= get_term_meta_value($term_id, $banner['number']) ?: 0;
				$group_banner[$key]['quanties'] = $quanties ;
				$has_checked	= (empty(get_term_meta_value($term_id, $key))) ? 0 : 1 ;
				$group_banner[$key]['checked'] = $has_checked ;
			} 
		}

		$data['banner_info']	= $get_config_banners ;

		$this->data = $data;
		parent::render($this->data);
	}

	public function submit_kpi($term_id = 0,$delete_id=0)
	{
		if($delete_id > 0)
		{
			$this->_delete_kpi($term_id,$delete_id);
			redirect(module_url('kpi/'.$term_id.'/'),'refresh');
		}
		$post = $this->input->post();

		if(empty($post)) return FALSE;

		$insert = array();
		$kpi_datetime = $post['target_date'];
		$user_id 		= $post['user_id'];
		$kpi_value 	= (int)$post['target_post'];
		$result_value = $post['total_point'] ;

		$kpi_type 				= $this->input->post('target_type');
		if($kpi_datetime >= $this->mdate->startOfMonth())
		{
			$this->banner_kpi_m->update_kpi_value($term_id, $kpi_type, $kpi_value, $kpi_datetime, $user_id);	
			$this->banner_kpi_m->update_kpi_result($term_id, $kpi_type, $result_value, $kpi_datetime, $user_id)	;	
			$this->messages->success('Cập nhật thành công');
		}
		else
		{
			$this->messages->error('Cập nhật không thành công do tháng cập nhật nhỏ hơn tháng hiện tại');
		}
		redirect(current_url(),'refresh');
	}

	private function _delete_kpi($term_id = 0,$delete_id=0)
	{
		restrict('banner.kpi.delete');
		$check = $this->banner_kpi_m->get($delete_id);
		$is_deleted = false;
		if($check)
		{
			if(strtotime($check->kpi_datetime) >= $this->mdate->startOfMonth())
			{
				$this->banner_kpi_m->delete_cache($delete_id);
				$this->banner_kpi_m->delete($delete_id);
				$is_deleted = true;
				$this->messages->success('Xóa thành công');
			}
		}
		($is_deleted) OR $this->messages->error('Xóa không thành công');
	}

	public function task($term_id = 0)
	{
		restrict('banner.Task.Access');
		if($this->input->post())
		{
			Modules::run('banner/tasks/insert_task',$term_id, $this->banner_m->get_post_type('task'));
		} 

		$this->template->title->prepend('Công việc');
		$data = $this->data;
		$this->admin_ui->select('end_date');
		$this->admin_ui->add_column('posts.post_id','#');
		$this->admin_ui->add_column('post_title','Tiêu đề', null,null, array('class' =>'col-md-3'));
		// $this->admin_ui->add_column('post_content','Nội dung', NULL, NULL, array('class' =>'col-md-4'));

		$this->admin_ui->add_column('post_author', 'Người tạo');

		$this->admin_ui->add_column('type', array('set_select'=>FALSE,'set_order'=>FALSE, 'title' =>'Loại'), null,null, array('class' =>'col-md-1'));
		$this->admin_ui->add_column_callback('type',function($data, $row_name){
			$type = get_post_meta_value($data['post_id'],'task_type');
			$data['type'] = $this->config->item($type,'tasks_type');
			return $data;
		}, FALSE);
		$this->admin_ui->add_column('sms', array('set_select'=>FALSE,'set_order'=>FALSE, 'title' =>'Gửi SMS'), null,null, array('class' =>'col-md-1'));
		$this->admin_ui->add_column('post_status', 'Trạng thái');
		$this->admin_ui->add_column_callback('post_status',function($data, $row_name){
			$status = array();
			$status['process'] = '';
			$status['complete'] = '';

			switch ($data['post_status']) {

				case 'complete':
				$status['complete'] = 'checked disabled';

				case 'process':
				$status['process'] = 'checked disabled';
				break;
			}

			$data['post_status'] = '<span class="checkbox"><label><input type="checkbox" class="minimal" data-taskid="'.$data['post_id'].'" data-status="process" '.$status['process'].'> Đang thực hiện</label></span>
			<span class="checkbox"><label><input type="checkbox" class="minimal" data-taskid="'.$data['post_id'].'" data-status="complete" '.$status['complete'].'> Hoàn thành </label></span>';
			return $data;
		}, FALSE, array('class' =>'col-md-2'));

		$this->admin_ui->add_column_callback('sms',function($data, $row_name){

			$is_send = $this->postmeta_m->get_meta_value($data['post_id'],'sms_status');

			if($is_send == 2)
			{
				$data['sms'] = '<span class="label label-success">Đã gửi</span>';
				$data['view'] = '';
			}
			else
			{
				$checked = ($is_send) ? 'checked' : '';
				$checked.= ($data['post_status'] =='complete') ? ' disabled' : '';
				$data['sms'] = '
				<span class="checkbox"><label><input type="checkbox" class="minimal" data-taskid="'.$data['post_id'].'"  data-toggle="confirmation" data-status="send_sms" '.$checked.'> SMS</label></span>';
			}

			return $data;
		}, FALSE);

		$this->admin_ui->add_column('start_date','Ngày thực hiện','$1 - $2','date("d/m/Y",start_date),date("d/m/Y",end_date)',  array('class' =>'col-md-2'));
		// $this->admin_ui->add_column('end_date','Ngày kt','$1','date("d/m/Y",end_date)');

		$this->admin_ui->where('post_type', $this->banner_m->get_post_type('task'));
		$this->admin_ui->where('term_id',$term_id);
		$this->admin_ui->order_by("start_date asc, post_status asc");
		$this->admin_ui->from('posts');
		$this->admin_ui->join('term_posts', 'term_posts.post_id = posts.post_id');

		$this->admin_ui->add_column_callback(array('post_content','post_author'),array($this->webgeneral_callback_m,'post_get_meta'), FALSE);
		$this->admin_ui->add_column('view', array('set_select'=>FALSE, 'title' =>'Xem'),anchor(module_option_url('webdoctor','tasks/edit/$1'),'Edit','class="ajax_edit" data-taskid="$1"').' | '.anchor(module_option_url('webdoctor','tasks/edit/$1'),'Xóa','class="ajax_delete" data-taskid="$1"'),'post_id', array('class' =>'col-md-1'));

		$this->admin_ui->add_column_callback('view',function($data,$row_name){

				$post_id = $data['post_id'];
				$view = '';

				if(has_permission('banner.Task.Update'))
				{
					$view.= anchor(module_option_url('webdoctor',"tasks/edit/{$post_id}"),'<i class="fa fa-fw fa-edit"></i>',"title='Cập nhật' class='btn btn-default btn-xs ajax_edit' data-taskid='{$post_id}' target='_blank'");
				}

				if(has_permission('banner.Task.Delete'))
				{
					$view.= anchor(module_option_url('webdoctor',"tasks/edit/{$post_id}"),'<i class="fa fa-fw fa-trash"></i>',"title='Xóa' class='btn btn-default btn-xs ajax_delete' data-taskid='{$post_id}' data-toggle='confirmation' target='_blank'");
				}

				$data['view'] = $view;
				return $data;
			},FALSE);

		// Add filter for user with no manage permission
		if( ! has_permission('banner.Task.Manage')) 
		{
			// $this->admin_ui->where('posts.post_author',$this->admin_m->id);
			$this->admin_ui->where_in('posts.post_author',array($this->admin_m->id,1)); // #1- Hệ thống
		}

		$data['content'] = $this->admin_ui->generate();

		$this->data = $data;
		parent::render($this->data);
	}

	public function setting($term_id = 0)
	{
		restrict('banner.setting');
		$this->template->title->prepend('Cấu hình');
		$this->setting_submit($term_id);
		
		$data = $this->data;
		$data['term'] = $this->term;
		$data['term_id'] = $term_id;
		$data['meta']['phone_report'] = get_term_meta_value($term_id, 'phone_report');
		$data['meta']['mail_report']  = get_term_meta_value($term_id, 'mail_report');

		$this->data = $data;
		parent::render($this->data);
	}

	protected function setting_submit($term_id = 0)
	{
		$this->load->model('banner_report_m') ;
		$post = $this->input->post();
		if(empty($post)) return FALSE;

		if( ! empty($post['start_process']) )
		{
			
			if( ! $this->banner_m->has_permission($term_id, 'banner.start_service') )
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}
			$term 				  = $this->term_m->get($term_id);
			
			$status_proc_service  = $this->banner_contract_m->proc_service($term);

			if(FALSE === $status_proc_service) $this->messages->error('Có lỗi xảy ra! Vui lòng kiểm tra cấu hình cài đặt hoặc thêm nhân viên kỹ thuật') ;
			if(TRUE === $status_proc_service) $this->messages->success('Đã gửi mail thành công');

			/* Phân tích hợp đồng ký mới | tái ký */
			$this->load->model('contract/base_contract_m');
			$this->base_contract_m->detect_first_contract($term_id);

			redirect(module_url("setting/{$term_id}"),'refresh');
		}
		else if( ! empty($post['end_process']) )
		{
			if( ! $this->banner_m->has_permission($term_id, 'banner.stop_service') )
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$term = $this->term_m->get($term_id);

			$stat = $this->banner_contract_m->stop_service($term);
			if(TRUE === $stat) $this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}

		else if(! empty($post['acceptance_banner']) )
		{
			if( ! $this->banner_m->has_permission($term_id, 'banner.acceptance_banner') )
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			// $term = $this->term_m->get($term_id);			
			$is_send = $this->banner_report_m->send_mail_acceptance($term_id);
			if(TRUE === $is_send) $this->messages->success('Hệ thống đã gửi mail thành công');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}

		else if(!empty($post['submit']))
		{
			$metadatas = $post['meta'];

			if( ! $this->banner_m->has_permission($term_id, 'banner.setting','update'))
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$metadatas = $post['meta'];
			if($metadatas)
			{
				foreach($metadatas as $key=>$value)
				{	
					update_term_meta($term_id,$key,$value);
				}
			}

			$this->messages->success('Cập nhật thành công');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
	}

	protected function is_assigned($term_id = 0,$kpi_type = '')
	{
		// check user has manager role permission
		$class  = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		$result = $this->banner_m->has_permission($term_id,"{$class}.{$method}",'access');
		return $result;
	}

	public function calc_point($term_id)
	{
	}

	public function view_point($term_id)
	{
		$get_config_banners  		= $this->config->item('packages_banner') ;
		if(empty($get_config_banners)) return FALSE;
			
		foreach ($get_config_banners as $key_group => &$group_banner) 
		{
			foreach ($group_banner as $key => $banner) 
			{	
				$quanties 		= get_term_meta_value($term_id, $banner['number']) ?: 0;				
				$group_banner[$key]['quanties'] = $quanties ;
				$has_checked	= (empty(get_term_meta_value($term_id, $key))) ? 0 : 1 ;
				$group_banner[$key]['checked'] = $has_checked ;
			} 
		}

		$data['banner_info']	= $get_config_banners ;
		parent::render($data);
	}

	public function statistical_kpi()
	{
		
		restrict('banner.statistical_kpi.access');
		
		// Authorization for user role
		if(!has_permission('banner.statistical_kpi.manage') && !has_permission('Banner.Sale.Access'))
		{
		   $this->admin_ui->join('webgeneral_kpi','webgeneral_kpi.term_id = term.term_id')
		   				  ->where('webgeneral_kpi.user_id', $this->admin_m->id);
		}

		// LOAD FILE CONFIG : CONTRACT/CONFIG/CONTRACT.PHP
		$this->load->config('contract/contract') ;	


		// TỔNG KPI ĐỀ RA TRONG THÁNG
		$total_kpi_month = $this->config->item('total_kpi_month');
		
		// TIỀN THƯỞNG BANNER THEO KPI
		$bonus_kpi       = $this->config->item('bonus_kpi');

		// TRẠNG THÁI CỦA HỢP ĐỒNG
		$contract_status = $this->config->item('contract_status') ;

		// HIỂN THỊ DANH SÁCH CHI TIẾT BANNER
		$get_config_banners  		= $this->config->item('packages_banner') ;

		if(empty($get_config_banners)) return FALSE;

		$time = time();
		$day = date('d',$time) - 1;
		if($day == 0){
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}
		$time = strtotime(date('Y-m-'.$day, $time));
		$day = str_pad($day, 2, "0", STR_PAD_LEFT);
		$start_date = strtotime(date('Y/m/01',$time));
		$end_date = $this->mdate->endOfMonth($time);

		// THỜI GIAN ĐẦU THÁNG - CUỐI THÁNG
		$start_time = $this->mdate->startOfMonth();
		$end_time = $this->mdate->endOfMonth();

		$this->template->title->append('Tổng quan dịch vụ từ ngày '.date('d/m/Y',$start_date).' đến ngày '.date('d/m/Y',$end_date));
		
		$terms = $this->term_m->select('*')
							  ->join('termmeta', 'term.term_id = termmeta.term_id', 'left')
							  //->join('webgeneral_kpi', 'webgeneral_kpi.term_id = termmeta.term_id', 'left')
					 		  ->where_in('term_status', array('publish', 'ending'))
					 		  ->where('term_type','banner')
					 		  ->where('meta_key', 'contract_begin')
					 		  ->where('meta_value < ', $end_date)
					 		  ->where('meta_value > ', $start_time)
					 		  //->where('webgeneral_kpi.user_id' , $this->admin_m->id)
					 		  ->group_by('term.term_id')
					 		  ->get_many_by();

		if(empty($terms)) {
			$this->messages->error('Không có KPIs theo tháng');
			redirect(module_url('index'));
		}

		/* =====================================================
		*  CẤU HÌNH TEMPLATE HTML TABLE
		*  =====================================================
		*/
			$template = array('table_open' => '<table border="1" cellpadding="2" cellspacing="1" class="table table-responsive table-bordered table-hover">');
			$this->table->set_template($template);
			$this->table->set_heading('STT', 'Website', 'Loại thiết kế' , 'Số lượng', 'Điểm', 'Tổng điểm', 'Ngày nhận order', 'Deadline', 'Ngày giao', 'Nhân viên kinh doanh', 'Trạng thái', 'Ghi chú');					 		  
		$stt    = 1;
		$overviews   = array() ;
		$total_kpi_point = 0;

		/* =====================================================
		*  CẤU HÌNH XUẤT FILE EXCEL
		*  =====================================================
		*/ 
			$this->load->helper('file');
			$this->load->library('excel');
			$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	        $cacheSettings = array( 'memoryCacheSize' => '512MB');
	        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	        $objPHPExcel = new PHPExcel();
	        $objPHPExcel
	        ->getProperties()
	        ->setCreator("WEBDOCTOR.VN")
	        ->setLastModifiedBy("WEBDOCTOR.VN")
	        ->setTitle(uniqid('Danh sách dự thu __'));

	        $objPHPExcel->setActiveSheetIndex(0);
	        $objWorksheet = $objPHPExcel->getActiveSheet();        
	        $headings = array('STT', 'Website', 'Loại thiết kế' , 'Số lượng', 'Điểm', 'Tổng điểm', 'Ngày nhận order', 'Deadline', 'Ngày giao', 'Nhân viên kinh doanh', 'Trạng thái', 'Ghi chú');
	        $objWorksheet->fromArray($headings, NULL, 'A7');
	       	$row_index = 7;

		foreach ($terms as $key => $term) 
		{
			// ID
			$term_id = $term->term_id;

			// TÊN WEBSITE
			$website = $term->term_name ;

			// NGÀY NHẬN ORDER
			$date_of_order = get_term_meta_value($term_id, 'contract_begin');

			// DEADLINE
			$deadline = get_term_meta_value($term_id, 'contract_end');
			
			// NGÀY GIAO
			$date_delivery = get_term_meta_value($term_id, 'contract_end');

			// NHÂN VIÊN KINH DOANH
			$sale_id = get_term_meta_value($term_id,'staff_business');
			$staff_business = $this->admin_m->get_field_by_id($sale_id,'display_name') ?: '--';

			// TRẠNG THÁI HỢP ĐỒNG
			$status = $contract_status[$term->term_status] ?: '' ;

			// GHI CHÚ
			$contract_note = get_term_meta_value($term_id, 'contract_note') ?: '';

			// XỬ LÝ KHI HIỂN THỊ DANH SÁCH
			foreach ($get_config_banners as $key_group => $group_banner) 
			{
				foreach ($group_banner as $key => &$banner) 
				{	
					$quanties 		= get_term_meta_value($term_id, $banner['number']) ?: 0;
					$overviews[$key]['label'] = $banner['label'] ;
		
					$label = $banner['label'];
					$has_checked	= (empty(get_term_meta_value($term_id, $key))) ? 0 : 1 ;
					if($has_checked == 1)
					{
						$kpi_point  	  = $banner['kpi_point'] ;
						$calc_kpi_point   = $kpi_point * $quanties ;
						$total_kpi_point  += $calc_kpi_point ;
						$overviews[$key]['total_banner'][] = $quanties ;

						/* ====================================================== 
						*  XUẤT DÒNG TRONG EXCEL
						*  ======================================================
						*/
							$row_number = $row_index + $stt;
						
							$objWorksheet->setCellValueByColumnAndRow(0, $row_number, $stt);

				    		// TÊN WEBSITE
				    		$objWorksheet->setCellValueByColumnAndRow(1, $row_number, $website);

							// LOẠI THIẾT KẾ
				    		$objWorksheet->setCellValueByColumnAndRow(2, $row_number, strip_tags($banner['label'], '<br/>'));				    		
				    		
				    		// SỐ LƯỢNG
				    		$objWorksheet->setCellValueByColumnAndRow(3, $row_number, $quanties);

				    		// ĐIỂM
				    		$objWorksheet->setCellValueByColumnAndRow(4, $row_number, $kpi_point);

				    		// TỔNG ĐIỂM
				    		$objWorksheet->setCellValueByColumnAndRow(5, $row_number, $calc_kpi_point);

				    		// NGÀY NHẬN ORDER
				     		if(!empty($date_of_order))
							{
								$objWorksheet->setCellValueByColumnAndRow(6, $row_number, PHPExcel_Shared_Date::PHPToExcel($date_of_order));
							}
							else {
							 	$objWorksheet->setCellValueByColumnAndRow(6, $row_number, '');
							}

				    		// DEALINE
				    		if(!empty($deadline))
							{
								$objWorksheet->setCellValueByColumnAndRow(7, $row_number, PHPExcel_Shared_Date::PHPToExcel($deadline));
							}
							else {
								$objWorksheet->setCellValueByColumnAndRow(7, $row_number, '');
							}

				    		// DATE DELIVARY
				    		if(!empty($date_delivery))
							{
								$objWorksheet->setCellValueByColumnAndRow(8, $row_number, PHPExcel_Shared_Date::PHPToExcel($date_delivery));
							}
							else {
								$objWorksheet->setCellValueByColumnAndRow(8, $row_number, '');
							}

				    		// NHÂN VIÊN KINH DOANH
				    		$objWorksheet->setCellValueByColumnAndRow(9, $row_number, $staff_business);

				    		// TRẠNG THÁI
				    		$objWorksheet->setCellValueByColumnAndRow(10, $row_number, $status);

				    		// GHI CHÚ
				    		$objWorksheet->setCellValueByColumnAndRow(11, $row_number, $contract_note);

				    	/* ====================================================== 
						*  RENDER HTML TR TABLE
						*  ======================================================
						*/
							$this->table->add_row($stt, 
											 $website, 
											 $banner['label'] , 
											 $quanties, 
											 $kpi_point ,
											 $calc_kpi_point, 
											 (!empty($date_of_order)) ? $this->mdate->date('d/m/Y', $date_of_order) : '', 
											 (!empty($deadline)) ? $this->mdate->date('d/m/Y', $deadline) : '', 
											 (!empty($date_delivery)) ? $this->mdate->date('d/m/Y', $date_delivery) : '', 
											 $staff_business,  
											 $status, 
											 $contract_note) ;
						
						$stt++;
					}
				}
			}
			
		}
		
		$num_rows = $stt;

		$data['content']	= $this->table->generate(); 
		$this->table->clear() ;

		// THỐNG KÊ SỐ LƯỢNG BANNER ĐÃ THIẾT KÊ
		$data['overviews'] = '' ;
		if(!empty($overviews))
		{
			$this->table->set_template($template);
			$this->table->set_caption('Số tác vụ thực hiện trong tháng') ;
			$total_banners	= 0;

			// SỐ DÒNG THỨ ? KHI EXPORT 
			$stt2 = 8 ; 

			foreach($overviews as $overview)
			{
				$label = $overview['label'] ;
				
				$total_banner 	= 0;					
				if(isset($overview['total_banner']))
				{
					$total_banner  = array_sum($overview['total_banner']) ;
					$total_banners += $total_banner ;
				}

				// EXPORT EXCEL: NHIỀU DÒNG CHO 'SỐ TÁC VỤ TRONG THÁNG'
				$objWorksheet->setCellValueByColumnAndRow(15, $stt2, $total_banner);
				$objWorksheet->setCellValueByColumnAndRow(16, $stt2, strip_tags($label, '<br/>'));
				$stt2++ ;

				// RENDER HTML TR TABLE
				$this->table->add_row($total_banner, $label) ;			
			}
			$data['overviews'] = $this->table->generate();
		}

		// THỐNG KÊ ĐẠT ĐƯỢC KPI TRONG THÁNG
		$percent_kpi				= (float) ($total_kpi_point/$total_kpi_month * 100);
		$bonus						= 0;

		if($total_kpi_point - $total_kpi_month > 0)
		{
			$bonus = $bonus_kpi * ($total_kpi_point - $total_kpi_month) ;
		}
	
		$this->table->clear() ;
		$this->table->set_template($template) ;
		$this->table->set_caption('') ;
		$this->table->add_row('<b>Kpis/Tháng</b>', $total_kpi_month)
					->add_row('<b>Banner đã thiết kế</b>', $total_banners)
					->add_row('<b>Kpis đạt được</b>', $total_kpi_point)
					->add_row('<b>% đạt được</b>', $percent_kpi)
					->add_row('<b>Thưởng KPIs</b>', currency_numberformat($bonus));
		$data['statistical'] = $this->table->generate();
		
		// KHI NHẤN NÚT IMPORT
		if($this->input->post('export_kpis_excel') !== NULL)
		{   		    
			// Set Cells style for Date
		    $objPHPExcel->getActiveSheet()
	        	->getStyle('G'.$row_index.':G'.($num_rows+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

		    // Set Cells style for Date
		    $objPHPExcel->getActiveSheet()
	        	->getStyle('H'.$row_index.':H'.($num_rows+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
		    	
		    // Set Cells style for Date
		    $objPHPExcel->getActiveSheet()
	        	->getStyle('I'.$row_index.':I'.($num_rows+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);		
		
			// TỔNG THỐNG KÊ
		   	$objWorksheet->setCellValueByColumnAndRow(1, 1, 'Kpis/Tháng');
			$objWorksheet->setCellValueByColumnAndRow(2, 1, $total_kpi_month); 	

			$objWorksheet->setCellValueByColumnAndRow(1, 2, 'Banner đã thiết kế');
			$objWorksheet->setCellValueByColumnAndRow(2, 2, $total_banners); 

			$objWorksheet->setCellValueByColumnAndRow(1, 3, 'Kpis đạt được');
			$objWorksheet->setCellValueByColumnAndRow(2, 3, $total_kpi_point); 

			$objWorksheet->setCellValueByColumnAndRow(1, 4, '% đạt được');
			$objWorksheet->setCellValueByColumnAndRow(2, 4, $percent_kpi); 

			$objWorksheet->setCellValueByColumnAndRow(1, 5, 'Thưởng KPIs');
			$objWorksheet->setCellValueByColumnAndRow(2, 5, $bonus); 
			$objPHPExcel->getActiveSheet()
		        		->getStyle('C5:C5')
		        		->getNumberFormat()
			    		->setFormatCode("#,##0");

			// THỐNG KÊ SỐ LƯỢNG TỪNG LOẠI BANNER
			$objWorksheet->setCellValueByColumnAndRow(15, 7, 'Số tác vụ thực hiện trong tháng');
	        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

	        $folder_upload = "export/banner/";
	        if(!is_dir($folder_upload))
	        {
	            try 
	            {
	                $oldmask = umask(0);
	                mkdir($folder_upload, 0777, TRUE);
	                umask($oldmask);
	            }
	            catch (Exception $e)
	            {
	                trigger_error($e->getMessage());
	                return FALSE;
	            }
	        }

	        $file_name = $folder_upload . 'export-list-kpi-banner-' . my_date(time(), 'd-m-Y') . '.xlsx';

	       	try 
	        {
	        	$this->load->helper('download');
	        	$this->load->helper('file');

				delete_files($file_name);
				
	            $objWriter->save($file_name); 	
	            force_download($file_name,NULL);
	        }
	        catch (Exception $e) 
	        {
	            trigger_error($e->getMessage());
	            return FALSE; 
	        }
	    }

		parent::render($data);			 
	}

}
/* End of file banner.php */
/* Location: ./application/modules/banner/controllers/banner.php */