<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends REST_Controller
{
	function __construct()
	{
		parent::__construct('webbuild/rest');
		$this->load->model('usermeta_m');
		$this->load->model('permission_m');
	}

	public function service_post()
	{
		$default 	= array();
		$args 		= wp_parse_args(parent::post(NULL, TRUE), $default);
		$model_names 	= array('term_m','termmeta_m', 'staffs/admin_m', 'contract/contract_m', 'banner/banner_m', 'term_users_m');
		$this->load->model($model_names);

		$args['id'] = $args['edit']['term_id'] ?? 0;

		if( empty($args) || empty($args['id']))
		{	
			parent::response('Truy cập không hợp lệ', parent::HTTP_NOT_ACCEPTABLE);
		}

		if(FALSE === $this->contract_m->has_permission($args['id'], 'admin.contract.view'))
		{
			parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
		}

		$meta = $args['meta'];

		/* VALIDATE : GIÁ TRỊ GÓI THIẾT KẾ WEBSITE*/
		if( empty($meta['banners_selected'])) parent::response('Gói dịch vụ không hợp lệ hoặc không còn khả dụng.', parent::HTTP_UNAUTHORIZED);

		$banners_selected = array();
		foreach ($meta['banners_selected'] as $key => &$packages)
		{
			foreach ($packages as &$p)
			{
				if(empty($p['checked'])) continue;

				unset($p['checked']);
				
				$banners_selected = wp_parse_args($p, $banners_selected);
			}
		}

		update_term_meta($args['id'], 'banners_selected', serialize($banners_selected));
		$messages = array();

		$contract_value = $this->contract_m->set_contract($args['id'])->calc_contract_value();
		update_term_meta($args['id'], 'contract_value', $contract_value);
		$messages[] 	= 'Giá trị hợp đồng đã được cập nhật mới theo chi tiết dịch vụ thành công !';

		/* CẬP NHẬT ĐỢT THANH TOÁN MỚI */
		$this->load->model('term_posts_m');
		if($invoices = $this->term_posts_m->get_term_posts($args['id'], 'contract-invoices'))
		{
			$model_names = array('contract/invoice_m','contract/invoice_item_m','post_m','contract/base_contract_m');
			$this->load->model($model_names);
			
			$invoice_ids = array_column($invoices, 'post_id');
			$this->post_m->delete_many($invoice_ids);// Xóa tất cả invoice cũ		
			$this->invoice_item_m->where_in('inv_id', $invoice_ids)->delete_by(); // Xóa tất cả invoice items cũ
			$this->term_posts_m->delete_term_posts($args['id'], $invoice_ids); // Xóa liên kết hợp đồng - đợt thanh toán

			$this->contract_m->create_invoices(); // Đồng bộ đợt thanh toán
			$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

			$this->contract_m->sync_all_amount(); // Đồng bộ số liệu thu chi
			$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
		}

		parent::response(['msg' => $messages]);
	}
}
/* End of file Contract.php */
/* Location: ./application/modules/wservice/controllers/api/Contract.php */