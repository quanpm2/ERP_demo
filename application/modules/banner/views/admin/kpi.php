<?php
$rows_id = array();
$data = array();
$start_month = $this->mdate->startOfMonth();

$this->table->set_heading(array('STT','Kỹ thuật thực hiện','Hành động'));
$this->table->set_caption('Kỹ thuật thực hiện');

if(!empty($targets['tech']))
{
  $i = 0;
  foreach ($targets['tech'] as $kpi) 
  {
    $btn_delete = anchor(module_url('kpi/'.$term_id.'/'.$kpi['kpi_id'].'/delete'), 'Xóa', 'class="btn btn-danger btn-flat btn-xs"');
    $username = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');
    $this->table->add_row(++$i,$username,$btn_delete);
  }
}

echo $this->table->generate();

echo $this->admin_form->form_open();
echo $this->admin_form->dropdown('Nhân viên thực hiện', 'user_id', $users, '');
echo $this->admin_form->hidden('','target_date',time());
echo $this->admin_form->hidden('','target_type','tech');
echo $this->admin_form->hidden('','target_post','1');
echo $this->admin_form->submit('submit_kpi_tech','Lưu lại');
$this->table->clear();
?>

<div style="margin-top: 20px">
		<p style="font-size: 19px; border-bottom: 2px solid #00a65a; padding: 5px 0px">THÔNG TIN ĐIỂM KPIs</p>
    	<?php 
    		$banner_info['nomal_banner']['group_name'] 		= 'BANNER TĨNH';
    		$banner_info['moving_banner']['group_name'] 	= 'BANNER ĐỘNG';
    		$banner_info['company_banner']['group_name'] 	= 'BANNER NỘI BỘ CÔNG TY';
    	?>
    	<?php foreach ($banner_info as $key => $banners) : ?>
	    	  <table class="table table-striped table-condensed" style="margin-bottom: 50px">
	    	  	 <thead>
		    	  	 <tr>
	    	  	 		<th style="width: 40%"><?php echo $banners['group_name'] ;?></th>
	    	  	 		<th style="width: 15%">TÌNH TRẠNG</th>
	    	  	 		<th style="width: 15%">SỐ LƯỢNG</th>
	    	  	 		<th style="width: 15%">SỐ ĐIỂM</th>
	    	  	 		<th style="width: 15%">TÍNH ĐIỂM</th>
		    	  	 </tr>
	    	  	 </thead>
	    	  	 <tbody>
		    	  	 <?php foreach ($banners as $key => $banner) : ?>
		    	  	 		<?php 
		    	  	 			if(!is_array($banner)) continue ;

		    	  	 			$checked 	   = '' ;
		    	  	 			$calc_point    = 0;
		    	  	 			$value_checked = 0;
		    	  	 			$disabled		= '' ;
		    	  	 	
		    	  	 			if($banner['checked'] == 1) {
		    	  	 				$checked 		= 'checked' ;
		    	  	 				$calc_point		= $banner['kpi_point'] * $banner['quanties'] ;
		    	  	 				$total_point[] 	= $calc_point ;
		    	  	 				$value_checked 	= 1;
		    	  	 				//$disabled		= 'disabled' ;
		    	  	 			} 
		    	  	 		?>
			    	  	 	<tr class="<?php echo $key;?>">
			    	  	 		<td><?php echo $banner['label'];?></td>
			    	  	 			<td>
			    	  	 				<input type="checkbox" value="<?php echo $value_checked;?>" <?php echo $checked ;?> <?php echo $disabled ;?> />
			    	  	 			</td>				    	  	 		
			    	  	 		<td>
			    	  	 			<?php echo $banner['quanties'] ;?>
			    	  	 			
			    	  	 		</td>
			    	  	 		<td>
			    	  	 			<?php echo $banner['kpi_point'] ;?>
			    	  	 			
			    	  	 		</td>
			    	  	 		<td>
			    	  	 			<?php echo $calc_point; ?>
			    	  	 		</td>
			    	  	 	</tr>
		    	  	 <?php endforeach ?>
	    	  	 </tbody>
	    	  </table>
    	<?php endforeach ?>   
    	<input type="hidden" name="total_point" value="<?php echo array_sum($total_point) ; ?>">
    	<p class="bg-green text-center">Tổng điểm đạt được: <b><?php echo array_sum($total_point) ; ?></b></p>
</div>
<?php echo $this->admin_form->form_close(); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('input[type="checkbox"]').iCheck({
    		checkboxClass: 'icheckbox_flat-blue',
    		radioClass: 'iradio_flat-blue',
		});
	}); 
</script>    