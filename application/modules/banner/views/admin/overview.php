<?php   
    $this->template->javascript->add('plugins/validate/jquery.validate.js');
    $this->template->stylesheet->add('plugins/bootstrap-fileinput/css/fileinput.css');
    $this->template->javascript->add('plugins/bootstrap-fileinput/js/fileinput.min.js');
?>
<?php

    $representative_gender = get_term_meta_value($term_id, 'representative_gender');
    $contract_begin        = get_term_meta_value($term_id, 'contract_begin');
    $contract_end          = get_term_meta_value($term_id, 'contract_end');
    $contract_region       = get_term_meta_value($term_id, 'contract_region');


    $price_banner       = get_term_meta_value($term_id, 'contract_value') ;

    $sale_id               = get_term_meta_value($term_id,'staff_business');

    $staff_business        = 'Chưa có' ;
    if($sale_id) $staff_business     = $this->admin_m->get_field_by_id($sale_id,'display_name');

    $tech_kpis             = $this->banner_kpi_m->select('user_id')
                                                     ->where('term_id',$term_id)
                                                     ->where('kpi_type','tech')
                                                     ->as_array()
                                                     ->get_many_by();

    $staffs_ids = array_column($tech_kpis, 'user_id');

    $technical_staffs       = 'Chưa có';
    if(!empty($staffs_ids)) 
    {
        foreach ($staffs_ids as $staff_id)
        {
            $user_name                             = $this->admin_m->get_field_by_id($staff_id,'display_name');        
            if($user_name)  $technical_staffs      = $user_name ?: $this->admin_m->get_field_by_id($staff_id,'user_mail');
        }
    }

    $ajax_segment = "contract/ajax_dipatcher/ajax_edit/{$term_id}" ;
?>
<div class="overlay" style="display: none; opacity: 1 !important; z-index: 9999999 !important">
    <i class="fa fa-refresh fa-spin" style="font-size: 72px"></i>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php 
            $contract_price_banner = ($price_banner > 0) ? number_format($price_banner) : 0 ; 
            $this->table->set_caption('Thông tin hợp đồng')
                        ->add_row(array('Mã hợp đồng',get_term_meta_value($term_id, 'contract_code')))
                        ->add_row('Ngày bắt đầu', empty($contract_begin) ? $empty_chars : my_date($contract_begin,'d/m/Y'))
                        ->add_row('Ngày kết thúc', empty($contract_end) ? $empty_chars : my_date($contract_end,'d/m/Y'))
                        ->add_row('Phí thiết kế Banner', currency_numberformat($price_banner))
                        ->add_row('Nhân viên kinh doanh', $staff_business)
                        ->add_row('Kỹ thuật thực hiện', @$technical_staffs) ;   
            echo $this->table->generate();
            $this->table->clear();
        ?>
    </div>
    <div class="col-sm-6">
            <input type="hidden" name="term_id" value="<?php echo $term_id ; ?>" />
             <?php $this->table->set_caption('Thông tin khách hàng')
                               ->add_row('Người đại diện',
          '<span id="incline-data">' .($representative_gender == 1 ? 'Ông ' : 'Bà ').str_repeat('&nbsp', 2).get_term_meta_value($term_id,'representative_name').'</span>')
                               ->add_row('Email',get_term_meta_value($term_id, 'representative_email')) ;
                    echo $this->table->generate(); 
                    $this->table->clear();
             ?>            
    </div>
</div> 

    


   