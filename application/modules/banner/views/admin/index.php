<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
?>

<div class="row">
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-list-ul"></i>Đang hoạt động</a></li>
                <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-stop"></i> Đã kết thúc</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                   <?php echo $this->admin_form->datatable(['remote_url'=>admin_url('banner/ajax/dataset?&where%5Bterm_status%5D=publish')]);?>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                   <?php echo $this->admin_form->datatable(['remote_url'=>admin_url('banner/ajax/dataset?&where%5Bterm_status%5D=ending')]);?>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<script type="text/javascript">
$('[data-toggle=confirmation]').confirmation();
</script>

<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/component/ui.js'));  
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/app.js'));
/* End of file index.php */
/* Location: ./application/modules/googleads/views/admin/index.php */