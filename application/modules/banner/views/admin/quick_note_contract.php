<style type="text/css">
   #count-msg { 
        margin-right: 10px;
   }

   .icon-edit-update a { 
      color: #fff;
   }

   .msg-default .icon-edit-update a { 
      color: #000 !important;
   }
</style>
<script type="text/javascript">

   function alert_permissions(message) {
       $('#alert-permissions').empty() ;
       $('#alert-permissions').css('display', 'block') ;
       box_message =  '<p class="bg-green" style="padding:10px">' + message + '</p>' ;
       $('#alert-permissions').append(box_message).hide(7000) ;
   }

   function count_msg(term_id) {
       $.ajax({
             url       : '<?php echo base_url() . 'message/ajax_note_contract/count' ;?>',
             type      : 'post',            
             dataType  : 'json' ,
             data      : { term_id : term_id } ,          
             success   : function(data, status) {
                 if(status == 'success') {                    
                     $('#count-msg').html('<span data-toggle="tooltip" title="' + data.count_msg + ' ghi chú" class="badge bg-green" title="">' + data.count_msg + '</span>') ;
                 }
             }
       }) ;
   }
   

   function auto_sroll() {
    var height = 0;
    $('#load-direct-chat-messages div.direct-chat-msg').each(function(i, value){
        height += parseInt($(this).height());
    });

    height += '';

    $('#load-direct-chat-messages').animate({scrollTop: height});
   }

   // Send Message  : add, edit, update
   function send_message(term_id = 0, msg_id = '', msg_content = '', action = 'add') {
       $.ajax( {
                 url       : '<?php echo base_url() . 'message/ajax_note_contract/send' ;?>',
                 type      : 'post' ,
                 data      : { msg_id : msg_id , term_id : term_id , msg_content : msg_content, action : action } ,
                 dataType  : 'json' ,
                 success   : function(data) {            
                     if(action == 'update')  {
                        if(data.status == false) alert_permissions(data.alert) ;  
                        if(data.status == true) {                           
                           after      = data.after.data ;
                           
                           $('#msg-' + data.msg_id).addClass('update-before-' + data.msg_id).after(after) ;
                           $('.update-before-' + data.msg_id).remove() ;
                           init_note_delete();
                           init_note_update_enter();
                        }
                     }
                     else if(action == 'add') {
                          if(data.status == false) alert_permissions(data.alert) ;  
                          if(data.status == true) { 
                            $('#load-direct-chat-messages').append(data.data) ;
                                init_note_delete();
                                init_note_update_enter();
                                auto_sroll() ;                            
                          }
                     }
                     else if(action == 'delete') {
                          if(data.status == false) alert_permissions(data.alert) ;   
                          if(data.status == true) {
                              $('#msg-' + data.msg_id ).hide() ;
                              init_note_delete();
                              init_note_update_enter();
                          }
                          return false ;
                     }                       
                 }               
       }).done(function() {
           // Đếm số lượng ghi chú   
           $('#count-msg').empty() ;
           count_msg(term_id) ;      
           $('#msg_content').val('') ; 
       }) ;
   }

   function init_note_update_enter()
    {
      $('.icon-edit-update a.update').on('click', function(event) {     
           term_id      = $(this).data('term-id')   ;
           msg_id       = $(this).data('update-id') ;
               
           // Update
           $('#frm-note-contract').removeClass('action-insert').addClass('action-edit') ;
           $('#frm-note-contract :text[name="msg_content"]').removeAttr('data-action');

           $('#msg_content_update').val($('#msg-' + msg_id + ' .msg-content').text()) ;
           $('#msg_content_update').css('display', 'block').focus() ;
           $('#msg_content').css('display', 'none') ;
          
           // Enter tự động kích hoạt button send : update
           $('.action-edit #msg_content_update').keypress(function(event){
              if (event.which == 13) {
                   msg_content  = $('#msg_content_update').val() ;
                   if(msg_content == '') return false ;
                   send_message(term_id, msg_id, msg_content, 'update') ; 
                   $('#msg_content_update').css('display', 'none') ;
                   $('#msg_content').val('') ;
                   $('#msg_content').css('display', 'block').focus() ;
              }
           });          
           return false ; 
       }) ;
       return false ;
    }

   function init_note_delete() { 
      $('.icon-edit-update a.delete').on('click', function(event) {
             term_id      = $(this).data('term-id')   ;
             msg_id       = $(this).data('delete-id') ;
             send_message(term_id, msg_id, '', 'delete') ;   
          }) ; 
   }  

   function init_note_add_enter(term_id, msg_content = '') {
       var action = $('#frm-note-contract #msg_content').data('action') ;
       if(action == 'add') {
          $('#msg_content').keypress(function(event){
              if (event.which == 13) {
                  msg_content  = $('#msg_content').val() ;

                  if(msg_content == '') return false ;

                  send_message(term_id, '' , msg_content, 'add') ;      
                  msg_content  = '' ; 
                  $('#msg_content').css('display', 'block').focus() ;             
              }
          });
       }
   }


   $(document).ready(function() {
      $('.btn-note').on('click', function(event) {
            event.preventDefault();

            $('#term_id').val($(this).data('term-id')) ;

            var term_id  = $('#term_id').val() ;

            $('#load-direct-chat-messages').empty() ;
            $('#count-msg').empty() ;
            $('#msg_content').val('') ;

            $.ajax({
                url       : '<?php echo base_url() . 'message/ajax_note_contract/load' ;?>',
                type      : 'post',            
                dataType  : 'json' ,
                data      : { term_id : term_id } ,          
                success   : function(data) {

                    $('#load-direct-chat-messages').append(data.data) ;

                    auto_sroll() ;
                    init_note_delete();
                    init_note_update_enter();
                },
            }).done(function() {    
                // Đếm số lượng ghi chú      
                count_msg(term_id) ;  
                $('#msg_content').focus() ;    
            }) ;
           
            
             var action = $('#frm-note-contract #msg_content').data('action') ;
             if(action == 'add') {
                $('#msg_content').keypress(function(event){
                    // var keycode = event.keyCode;
                    if (event.which == 13) {
                        msg_content  = $('#msg_content').val() ;

                        if(msg_content == '') return false ;

                        send_message($('#term_id').val(), '' , msg_content, 'add') ;      
                        msg_content  = $('#msg_content').val('') ;
                        $('#msg_content').focus() ;             
                    }
                });
             }

            init_note_update_enter();  
            init_note_delete();         
      }) ;
   }) ;

</script>
<div class="modal" id="modal-note-contract">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
               <!-- DIRECT CHAT -->
              <div class="box box-success direct-chat direct-chat-success action-add">
                <div id="alert-permissions"></div>
                <div class="box-header with-border">
                  <h3 class="box-title">Ghi chú cho hợp đồng</h3>

                  <div class="box-tools pull-right">
                    <div id="count-msg" class="pull-left"></div>
                    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>
                    </button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages" id="load-direct-chat-messages"></div>
                  <!--/.direct-chat-messages-->

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <div id="frm-note-contract" method="post" class="action-insert">
                    <input type="hidden" value="" name="term_id" id="term_id"/> 
                    <input type="text" name="msg_content" id="msg_content" placeholder="Nhập ghi chú và enter..." class="form-control" data-action="add">
                    <input type="text" name="msg_content" id="msg_content_update" placeholder="Nhập ghi chú và enter..." class="form-control" data-action="update" style="display: none">
                  </div>
                </div>
                <!-- /.box-footer-->
              </div>
              <!--/.direct-chat -->
          </div>
        </div>  
      </div>
      <!-- /.modal-dialog -->
</div>