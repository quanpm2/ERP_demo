<?php echo $this->admin_form->form_open() ;?>
	<div class="row">
		<div class="col-xs-2 col-sm-12 form-group">
			<button type="submit" name="export_kpis_excel" class="btn-success btn-flat pull-right btn btn-xs" placeholder="Export Excel .xls" data-toggle="confirmation">Export Excel .xls</button>
		</div>
	</div>
	<div class="row">
	    <div class="col-sm-9">
			<?php echo $content;?>
		</div>
		<div class="col-sm-3">
			<div class="row">
				<div class="col-sm-12"><?php echo $statistical;?></div>
				<div class="col-sm-12"><?php echo $overviews;?></div>
			</div>	
		</div>
	</div>			
<?php echo $this->admin_form->form_close(); ?>