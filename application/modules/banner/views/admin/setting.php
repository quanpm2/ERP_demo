<?php
if(!is_service_end($term))
{
  $this->admin_form->set_col(12,6);
  echo $this->admin_form->box_open();
  echo $this->admin_form->form_open('', 'id="start_process_submit"');
  
  $btn_submit_proc = array('id'=>'start_process', 'name'=>'start_process', 'type'=>'submit', 'class'=>'btn btn-danger', 'value'=>'start_process','data-toggle'=>'confirmation');
  $btn_submit_text = '';
  
  $start_service_time       =  get_term_meta_value($term_id,'start_service_time');
  if( ! $start_service_time )
  {
      $btn_submit_proc['class'] = 'btn btn-default';
      $btn_submit_text          = '<i class="fa fa-fw fa-play"></i>Kích hoạt';
      echo form_button($btn_submit_proc, $btn_submit_text);
  }

  $end_service_time =  get_term_meta_value($term_id,'end_service_time');

  if( ! $end_service_time ) // true 
  {
      echo form_button(array('id'=>'end_process','name'=>'end_process','type'=>'submit','class'=>'btn btn-danger','value'=>'end_process','data-toggle'=>'confirmation'),'<i class="fa fa-fw fa-stop"></i>Kết thúc hợp đồng');
  }

  if( has_permission("banner.acceptance") ) 
  {
    $has_acceptance_banner = get_term_meta_value($term_id, 'has_acceptance_banner');
    if(empty($has_acceptance_banner))
    {
      echo form_button(array('id' => 'acceptance_banner','name' => 'acceptance_banner','type'=>'submit', 'class'=>'btn btn-success','value'=>'acceptance_banner','data-toggle'=>'confirmation'),'<i class="fa fa-fw fa-money"></i> Nghiệm thu');
    }
  }

  echo '<p class="help-block">Vui lòng cấu hình SMS, cấu hình Email và thông số để hệ thống gửi Mail kích hoạt và Mail thông báo kết nối khách hàng </p>' ;
  echo $this->admin_form->box_close();
  echo $this->admin_form->form_close();
}
$this->admin_form->set_col(6,6);
//sms setting
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Cấu hình SMS');

echo $this->admin_form->input('Số điện thoại','meta[phone_report]', @$meta['phone_report'], 'Mỗi số điện thoại cách nhau bởi dấu ,', array('id'=>'phone_report'));
echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();

//email setting
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Cấu hình Email');

echo $this->admin_form->input('Email','meta[mail_report]', @$meta['mail_report'], 'Mỗi mail cách nhau bởi dấu ,', array('id'=>'mail_report'));
echo $this->admin_form->box_close(array('submit', 'Lưu lại'), array('button', 'Hủy bỏ'));
echo $this->admin_form->form_close();

?>

<script type="text/javascript">
  $(function(){
    $('[data-toggle=confirmation]').confirmation();
    $('#btn-ajax-reload').click(function(){
      $.ajax({url: '<?php echo module_url();?>ajax/setting/ga-reload',dataType: 'json', success: function(result){
        $('form > .form-group  select').eq(1).select2({
         allowClear: true,
         data: result
       });

        $('form > .form-group .select2-container').eq(1).css('width','100%');
      }});
    });


    $('#btn-ajax-checkservice').click(function(){
      $.ajax({url: '<?php echo module_url();?>ajax/setting/ga-reload',dataType: 'json', success: function(result){
         
alert('12312');
        // $('form > .form-group .select2-container').eq(1).css('width','100%');
      }});
    });


    // To make Pace works on Ajax calls
    $(document).ajaxStart(function() { Pace.restart(); });
    $('#start_process').click(function(){
      $(this).hide();
      $('#start_process_submit').prepend('<p class="help-block label label-info">Hệ thống đang gửi mail, vui lòng đợi trong giây lát.</p>');
        return true;
    });

  });
</script>