<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $contract_code;?></title>
    <style>
        body{line-height:1.2em; font-size: 14px; padding: 0 20mm;}
        p {margin:8px 0}
        .title{background:#eee; font-weight:600; border-top:1px solid #000}
        ul{margin: 0}
    </style>
</head>
<body style="font-family: 'Times New Roman', serif;">
    <p align="center">
        CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br/>
        <span>Độc lập - Tự do - Hạnh phúc</span><br/>
        <span>--------------oOo--------------</span>
    </p>
    <p>&nbsp;</p>
    <p align="center">  
        <span style="font-size:1.3em; line-height:1.3em"><?php echo ($printable_title?:'HỢP ĐỒNG'); ?></span><br/> <?php echo $contract_code;?>
    </p>

    <table width="100%" id="table" border="1" cellspacing="0" cellpadding="3" class="table" style="margin: auto;">
        <tbody>
            <tr>
                <td colspan="2" style="text-align: left;background-color: #c0c0c0;"><strong> BÊN MUA DỊCH VỤ (BÊN A)</strong></td>
            </tr>
            <?php if(!empty($data_customer)): ?>
                <?php foreach ($data_customer as $label => $val): ?>
                    <?php if (empty($val)) continue;?>
                    <tr>
                        <td style="background-color: #dddddd; width: 20%"><?php echo $label;?></td>
                        <td>  <?php echo $val;?></td>
                    </tr>
                <?php endforeach ?>
            <?php endif ?>
            <tr >
                <td colspan="2" style="text-align: left;background-color: c0c0c0;"><strong>BÊN CUNG CẤP DỊCH VỤ (BÊN B)</strong></td>
            </tr>
            <tr>
                <td style="background-color: #dddddd;">Công ty</td>
                <td><strong><?php echo $company_name; ?></strong></td>
            </tr>
            <?php foreach ($data_represent as $label => $val): ?>
                <?php if (empty($val)) continue;?>
                <tr>
                    <td style="background-color: #dddddd;"><?php echo $label;?></td>
                    <td><?php echo $val;?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    <br/>

    <table width="100%"  border="1" cellspacing="0" cellpadding="3" style="margin: auto;">
        <thead>
            <tr style="background-color: #c0c0c0;">
                <th>Dịch vụ</th>
                <th>Kích thước</th>
                <th>Số lượng (Bộ)</th>
                <th>Đơn giá (VNĐ/Bộ)</th>
                <th>Giảm giá (%)</th>
                <th>Thành tiền</th>
            </tr>
        </thead>
        <tbody >
        <?php if (!empty($banners)): ?>
            <?php $i = 0; ?>
            <?php foreach ($banners as $key => &$banner):
                ++$i;
                $_min   = $banner['saleoff']['min'] ?? FALSE;
                $_sale  = $banner['saleoff']['value'] ?? 0;

                $banner['rate']     = FALSE != $_min && $banner['number'] >= $_min ? $_sale : 0;
                $banner['discount'] = $banner['number']*$banner['price']*div($banner['rate'], 100);
                $banner['total']    = $banner['number']*$banner['price'] - $banner['discount'];
            ?>
            <tr style="text-align: center;">
                <?php if ($i==1): ?>
                <td rowspan="8">Thiết kế banner</td>    
                <?php endif ?>
                <td><?php echo $banner['label']??'';?></td>
                <td><?php echo $banner['number']??0;?></td>
                <td style="text-align: right;"><?php echo currency_numberformat($banner['price']);?></td>
                <td style="text-align: center;"><?php echo currency_numberformat($banner['rate'], '%');?></td>
                <td style="text-align: right;"><?php echo currency_numberformat($banner['total']);?></td>
            </tr>
            <?php endforeach ?>

            <?php $total = array_sum(array_column($banners, 'total')); ?>

            <?php 
                if ($vat > 0): 
                    $vat_amount = $total*(div($vat, 100));
                    $total+= $vat_amount;
            ?>
            <tr>
                <td colspan="4">Thuế VAT</td>
                <td style="text-align: right;"><?php echo currency_numberformat($vat_amount);?></td>
            </tr>
            <?php endif ?>
            <tr>
                <td colspan="4">Tổng cộng</td>
                <td style="text-align: right;"><?php echo currency_numberformat($total);?></td>
            </tr>
            <tr style="text-align: left;">
                <td colspan="5">Bằng chữ: <b><i><?php echo convert_number_to_words($total);?></b></i></td>
            </tr>
        <?php endif ?>

        </tbody>
    </table>
    <br/>

    <div class="row-containter">
        <br/>      
        <p><b>Phương thức thanh toán</b></p>
        <p>Bên A thanh toán 100% giá trị phiếu cho Bên B sau khi ký và trước khi thực hiện . Thanh toán bằng hình thức chuyển khoản <br/>Thông tin tài khoản:</p>

        <?php if( ! empty($bank_info)) :?>
        <ul style="padding-left:0;list-style:none">
        <?php foreach ($bank_info as $label => $text) :?>
            <?php if (is_array($text)) : ?>
                <?php foreach ($text as $key => $value) :?>
                    <li>- <?php echo $key;?>: <?php echo $value;?></li>
                <?php endforeach;?>
            <?php continue; endif;?>
            <li>- <?php echo $label;?>: <?php echo $text;?></li>
        <?php endforeach;?>
        </ul>
        <p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
        <?php endif; ?>
    </div>
    <br/>
    <table width="100%" border="0" cellspacing="0" cellpadding="3" style="margin: auto;">
        <tbody>
            <tr>
                <td colspan="2" >
                    <strong>QUY ĐỊNH CHUNG</strong>
                </td>

            </tr>
            <tr>
                <td>
                    - Nội dung banner: bao gồm phần text (phần chữ) và hình ảnh.
                </td>
            </tr>
            <tr>
                <td>
                    -  Banner demo: là banner do Bên B thiết kế theo yêu cầu của Bên A và có thể được chỉnh sửa bởi Bên A. <strong>Sau khi gửi Demo, Bên A không được chỉnh sửa quá 3 lần so với yêu cầu ban đầu như:  không thay đổi nội dung bố cục, hình ảnh, thông điệp đã cung cấp cho Bên B trước khi thực hiện.</strong>
                </td>
            </tr>
            <tr>
                <td>
                    - Banner chính thức: là banner thuộc một trong hai trường hợp sau:
                </td>
            </tr>
            <tr>
                <td>
                    <p style="margin-left: 5%;">+ Banner demo trở thành banner chính thức khi Bên A không yêu cầu Bên B chỉnh sửa trong thời hạn hai (02) ngày  kể từ ngày nhận được banner demo.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="margin-left: 5%;">+ Banner sau khi Bên B chỉnh sửa lần cuối cùng (không quá 3 lần)</p>
                </td>
            </tr>
        </tbody>  
    </table>

    <?php echo $this->load->view('admin/preview/footer','',TRUE);?>