<?php $this->config->load('banner/banner'); ?>
<div class="col-md-12" id="review-partial">

    <h2 class="page-header">
        <i class="fa fa-globe"></i> Mã hợp đồng : <?php echo get_term_meta_value($edit->term_id, 'contract_code');?> 
        <small class="pull-right">Ngày tạo : <?php echo my_date(time(), 'd-m-Y');?></small>
    </h2>

    <div class="row">
        <?php
        $representative_gender  = force_var(get_term_meta_value($edit->term_id,'representative_gender'),'Bà','Ông');
        $representative_name    = get_term_meta_value($edit->term_id,'representative_name') ?: '';
        $display_name           = "{$representative_gender} {$representative_name}";
        $representative_email   = get_term_meta_value($edit->term_id,'representative_email');
        $representative_address = get_term_meta_value($edit->term_id,'representative_address');
        $representative_phone   = get_term_meta_value($edit->term_id,'representative_phone');
        $contract_daterange     = get_term_meta_value($edit->term_id,'contract_daterange');

        echo $this->admin_form->set_col(6)->box_open('Thông tin khách hàng');
        echo $this->table->clear()
        ->add_row('Người đại diện',$display_name?:'Chưa cập nhật')
        ->add_row('Email',$representative_email?:'Chưa cập nhật')
        ->add_row('Địa chỉ',$representative_address?:'Chưa cập nhật')
        ->add_row('Số điện thoại',$representative_phone?:'Chưa cập nhật')
        ->add_row('Chức vụ',$edit->extra['representative_position']??'Chưa cập nhật')
        ->add_row('Mã Số thuế',$edit->extra['customer_tax']??'Chưa cập nhật')
        ->add_row('Thời gian thực hiện',$contract_daterange?:'Chưa cập nhật')
        ->generate();

        echo $this->admin_form->box_close();

        echo $this->admin_form->set_col(6)->box_open('Thông tin dịch vụ');

        /**/
        $banners_selected = get_term_meta_value($edit->term_id, 'banners_selected');
        $banners_selected = $banners_selected ? unserialize($banners_selected) : [];
        $_banners = array();
        foreach ($banners_selected as $key => $value)
        {
            $items = explode('_', $key);
            $index = array_shift($items);

            if($index == 'saleoff') $value = unserialize($value);

            $_banners[implode('_', $items)][$index] = $value;
        }

        $this->table->clear()->set_heading('Gói Banner','Số lượng Banner','Đơn Giá(VNĐ)','Giảm giá (%)','Tổng tiền');
        
        $banners = $_banners ? array_filter($_banners, function($x){ return !empty($x['number']); }) : [];
        foreach ($banners as $key => &$banner)
        {
            $_min   = $banner['saleoff']['min'] ?? FALSE;
            $_sale  = $banner['saleoff']['value'] ?? 0;

            $rate     = FALSE != $_min && $banner['number'] >= $_min ? $_sale : 0;
            $discount = $banner['number']*$banner['price']*div($rate, 100);
            $total    = $banner['number']*$banner['price'] - $discount;

            $this->table->add_row($banner['label']??'', $banner['number']??0,currency_numberformat($banner['price']),currency_numberformat($rate, '%'),currency_numberformat($total));
        }
        /**/
        

        echo $this->table->generate();
        echo $this->admin_form->box_close();
        ?>
    </div>
</div>
<div class="clearfix"></div>

<?php
$hidden_values = ['edit[term_status]'=>'waitingforapprove','edit[term_id]'=>$edit->term_id,'edit[term_type]'=>$edit->term_type];
echo $this->admin_form->form_open('',[],$hidden_values);
echo $this->admin_form->submit('','confirm_step_finish','confirm_step_finish','', array('style'=>'display:none;','id'=>'confirm_step_finish'));
echo $this->admin_form->form_close();
?>