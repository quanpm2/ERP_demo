<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name()
	{
		return 'Thiết kế Banner';
	}

	public function init()
	{
		$this->_load_menu();
		$this->_update_permissions();
	}


	/**
	 * Init menu LEFT | NAV ITEM FOR MODULE
	 * then check permission before render UI
	 */
	private function _load_menu()
	{
		$order = 1;
		$itemId = 'admin-banner';
 
 		if(!is_module_active('banner')) return FALSE;	
		
		if(has_permission('banner.index.access'))
		{
			$this->menu->add_item(array(
				'id'        => $itemId,
				'name' 	    => 'TK Banner',
				'parent'    => null,
				'slug'      => admin_url('banner'),
				'order'     => $order++,
				'icon' => 'fa fa-picture-o',
				'is_active' => is_module('banner')
				),'left-service');	
		}

		if(!is_module('banner')) return FALSE;
		
		if(has_permission('banner.index.access'))
		{
			$this->menu->add_item(array(
			'id' => 'banner-service',
			'name' => 'Dịch vụ',
			'parent' => $itemId,
			'slug' => admin_url('banner'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-circle-o'
			), 'left-service');
		}
		

		if(has_permission('banner.statistical_kpi.access'))
		{
			$this->menu->add_item(array(
				'id' => 'statistical',
				'name' => 'Thống kê KPIs',
				'parent' => $itemId,
				'slug' => admin_url('banner/statistical_kpi'),
				'order' => $order++,
				'icon' => 'fa fa-fw fa-xs fa-area-chart'
			), 'left-service');
		}


		$left_navs = array(
			'overview'=> array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-xs fa-tachometer',
				),
			'kpi' => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-xs fa-heartbeat',
				),
			'setting'  => array(
				'name' => 'Cấu hình',
				'icon' => 'fa fa-fw fa-xs fa-cogs',
				),
			'task' => array(
				'name' => 'Công việc',
				'icon' => 'fa fa-fw fa-xs fa-tasks'
				),
			
			) ;
		$term_id = $this->uri->segment(4);

		$this->website_id = $term_id;

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("banner.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "banner-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
				), 'left-service');
		}

		
	}

	public function title()
	{
		return 'Thiết kế Banner';
	}

	public function author()
	{
		return 'nhantt';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Thiết kế Banner';
	}
	
	/**
	 * Init default permissions for banner module
	 *
	 * @return     array  permissions default array
	 */
	private function init_permissions()
	{
		return array(

			'banner.index' => array(
				'description' => 'Quản lý thiết kế Banner',
				'actions' => ['manage','access','update','add','delete']),

			'banner.done' => array(
				'description' => 'Dịch vụ đã xong',
				'actions' => ['manage','access','add','delete','update']),
		
			'banner.overview' => array(
				'description' => 'Tổng quan',
				'actions' => ['manage','access']),

			'banner.kpi' => array(
				'description' => 'KPI',
				'actions' => ['manage','access','add','delete','update']),
			
			'banner.start_service' => array(
				'description' => 'Kích hoạt thiết kế Banner',
				'actions' => ['manage','access','update']),
			'banner.sale' => array(
			    'description' => 'Sale',
			    'actions' => ['manage','access','add','delete','update']),
			'banner.stop_service' => array(
				'description' => 'Ngừng thiết kế Banner',
				'actions' => ['manage','access','update']),
			
			'banner.setting' => array(
				'description' => 'Cấu hình',
				'actions' => ['manage','access','add','delete','update']) ,
			'banner.task' => array(
				'description' => 'Công việc',
				'actions' => ['manage','access','add','delete','update']) ,
			'banner.statistical_kpi' => array(
				'description' => 'Thống kê KPIs trong tháng',
				'actions' => ['manage','access'])
			);
	}

	private function _update_permissions()
	{
		$permissions = array();

		if(!permission_exists('banner.statistical_kpi'))
			$permissions['banner.statistical_kpi'] = array(
				'description' => 'Thống kê KPIs trong tháng',
				'actions' => array('manage', 'access'));

		if(empty($permissions)) return false;
		
		foreach($permissions as $name => $value){
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	/**
	 * Install module
	 *
	 * @return     bool  status of command
	 */
	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		foreach($permissions as $name => $value)
		{
			$description   = $value['description'];
			$actions 	   = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);
			$this->role_permission_m->insert(['role_id'=>1,'permission_id'=>$permission_id,'action'=>serialize($actions)]);
		}

		return TRUE;
	}

	/**
	 * Uninstall module command
	 *
	 * @return     bool  status of command
	 */
	public function uninstall()
	{
		$permissions = $this->init_permissions();

		if(!$permissions) return FALSE;

		if($pers = $this->permission_m->like('name','banner')->get_many_by())
		{
			foreach($pers as $per)
			{
				$this->permission_m->delete_by_name($per->name);
			}
		}

		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}

		return TRUE;
	}
}
/* End of file banner.php */
/* Location: ./application/modules/banner/models/banner.php */