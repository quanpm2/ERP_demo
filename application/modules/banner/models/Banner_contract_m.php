<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_contract_m extends Base_contract_m {

	function __construct() 
	{
		parent::__construct();
	}

	public function calc_contract_value($term_id = 0)
	{
		  $term = $this->term_m->where('term_type','banner')->get($term_id);

		  $desigh_nomal_banner= get_term_meta_value($term->term_id, 'desigh_nomal_banner');
	      $resize_nomal_banner= get_term_meta_value($term->term_id, 'resize_nomal_banner');
	      $combo_nomal_banner= get_term_meta_value($term->term_id, 'combo_nomal_banner');

	      $desigh_banner= get_term_meta_value($term->term_id, 'desigh_banner');
	      $resize_banner= get_term_meta_value($term->term_id, 'resize_banner');
	      $combo_banner= get_term_meta_value($term->term_id, 'combo_banner');

	      // TÊN CÁC GÓI BANNER NỘI BỘ CÔNG TY
	      $design_banner_slider_static_company_banner = get_term_meta_value($term->term_id, 'design_banner_slider_static_company_banner') ;
	      $resize_static_company_banner = get_term_meta_value($term->term_id, 'resize_static_company_banner') ;
	      $resize_dynamic_company_banner = get_term_meta_value($term->term_id, 'resize_dynamic_company_banner');
	      $print_ad_company_banner = get_term_meta_value($term->term_id, 'print_ad_company_banner') ;
	      $standee_company_banner = get_term_meta_value($term->term_id, 'standee_company_banner');
		  
		  $banner_packages=array();
	      array_push($banner_packages, $desigh_nomal_banner,$resize_nomal_banner,$combo_nomal_banner,$desigh_banner,$resize_banner,$combo_banner, $design_banner_slider_static_company_banner, $resize_static_company_banner, $resize_dynamic_company_banner, $print_ad_company_banner, $standee_company_banner);
	      $arr=array_filter( $banner_packages);


	      if ($desigh_nomal_banner) {

	         $number_desigh_nomal_banner = get_term_meta_value($term->term_id, 'number_desigh_nomal_banner');
	         $price_desigh_nomal_banner  =  get_term_meta_value($term->term_id, 'price_desigh_nomal_banner');
	      }
	      else
	      {
	        $number_desigh_nomal_banner='';
	        $price_desigh_nomal_banner ='';
	      }

	      if ($resize_nomal_banner) {

	          $number_resize_nomal_banner= get_term_meta_value($term->term_id, 'number_resize_nomal_banner');
	          $price_resize_nomal_banner= get_term_meta_value($term->term_id, 'price_resize_nomal_banner');

	      }
	      else 
	      {
	        $number_resize_nomal_banner='';
	        $price_resize_nomal_banner='';
	      }

	      if ($combo_nomal_banner) {
	        $number_combo_nomal_banner= get_term_meta_value($term->term_id, 'number_combo_nomal_banner');
	        $price_combo_nomal_banner= get_term_meta_value($term->term_id, 'price_combo_nomal_banner');
	      }
	      else
	      {
	       $number_combo_nomal_banner='';
	       $price_combo_nomal_banner='';
	      }

	      if ($desigh_banner) {
	        $number_desigh_moving_banner= get_term_meta_value($term->term_id, 'number_desigh_moving_banner');
	        $price_desigh_moving_banner= get_term_meta_value($term->term_id, 'price_desigh_moving_banner');
	      }
	      else 
	      {
		      $number_desigh_moving_banner='';
		      $price_desigh_moving_banner='';
	      }

	      if ($resize_banner) {
	        $number_resize_moving_banner= get_term_meta_value($term->term_id, 'number_resize_moving_banner');
	       $price_resize_moving_banner= get_term_meta_value($term->term_id, 'price_resize_moving_banner');
	      }
	      else 
	      {
	      $number_resize_moving_banner='';
	      $price_resize_moving_banner=''; 
	     }

	      if ($combo_banner) {
	         $number_combo_moving_banner= get_term_meta_value($term->term_id, 'number_combo_moving_banner');
	         $price_combo_moving_banner= get_term_meta_value($term->term_id, 'price_combo_moving_banner');
	      }
	      else 
	      {
	      $number_combo_moving_banner='';
	      $price_combo_moving_banner='';
	      }


	      $number_design_banner_slider_static_company_banner = '' ;
	      $price_design_banner_slider_static_company_banner  = '' ;
	      if($design_banner_slider_static_company_banner) 
	      {
	      		$number_design_banner_slider_static_company_banner = get_term_meta_value($term->term_id, 'number_design_banner_slider_static_company_banner');
	      		$price_design_banner_slider_static_company_banner  =  get_term_meta_value($term->term_id, 'price_design_banner_slider_static_company_banner') ;	
	      }
	      
	      $number_resize_static_company_banner = '' ;
	      $price_resize_static_company_banner  = '' ;
	      if($resize_static_company_banner) 
	      {
		      $number_resize_static_company_banner = get_term_meta_value($term->term_id, 'number_resize_static_company_banner') ;
		      $price_resize_static_company_banner  =  get_term_meta_value($term->term_id, 'price_resize_static_company_banner') ;
		  }

		  $number_resize_dynamic_company_banner 	= '' ;
	      $price_resize_dynamic_company_banner 		= '' ;
	      if($resize_dynamic_company_banner) 
	      {
		      $number_resize_dynamic_company_banner = get_term_meta_value($term->term_id, 'number_resize_dynamic_company_banner');
		      $price_resize_dynamic_company_banner  =  get_term_meta_value($term->term_id, 'price_resize_dynamic_company_banner');
		  }

		  $number_print_ad_company_banner 	= '' ;
	      $price_print_ad_company_banner 		= '' ;
	      if($print_ad_company_banner) 
	      {
		      $number_print_ad_company_banner = get_term_meta_value($term->term_id, 'number_print_ad_company_banner');
		      $price_print_ad_company_banner  =  get_term_meta_value($term->term_id, 'price_print_ad_company_banner');
	  	  }

	  	  $number_standee_company_banner 	= '' ;
	      $price_standee_company_banner 		= '' ;
	      if($standee_company_banner) 
	      {
		      $number_standee_company_banner = get_term_meta_value($term->term_id, 'number_standee_company_banner');
		      $price_standee_company_banner  =  get_term_meta_value($term->term_id, 'price_standee_company_banner');
		  }

	      $number_banner=array();
	      array_push($number_banner, $number_desigh_nomal_banner,$number_resize_nomal_banner,$number_combo_nomal_banner,$number_desigh_moving_banner,$number_resize_moving_banner,$number_combo_moving_banner, $number_design_banner_slider_static_company_banner, $number_resize_static_company_banner, $number_resize_dynamic_company_banner, $number_print_ad_company_banner, $number_standee_company_banner);
	      $number_banner=array_filter( $number_banner);
	     
	      $price_banner=array();
	      array_push($price_banner, $price_desigh_nomal_banner,$price_resize_nomal_banner,$price_combo_nomal_banner,$price_desigh_moving_banner,$price_resize_moving_banner,$price_combo_moving_banner, $price_design_banner_slider_static_company_banner, $price_resize_static_company_banner, $price_resize_dynamic_company_banner, $price_print_ad_company_banner, $price_standee_company_banner);
	      $price_banner=array_filter( $price_banner); 

	      update_term_meta($term->term_id, 'banner_packages', implode(',', $banner_packages));
	      update_term_meta($term->term_id, 'number_banner',   implode(',',$number_banner));
	      update_term_meta($term->term_id, 'price_banner',  implode(',',$price_banner));

	      $contract_value       = 0;

	        foreach ($price_banner as $key_price=> $value_price) 
	        {
	          foreach ($number_banner as $key_number => $value_number) 
	          {
	            if ($key_price == $key_number)
	             {
	              $contract_value += $value_price*$value_number;
	             }
	          }
	        }
	          update_term_meta($term->term_id, 'contract_value', $contract_value);


		return $contract_value;
	}
	
	public function has_permission($term_id = 0, $name = '', $action = '',$kpi_type = '',$user_id = 0,$role_id = null)
	{
		$this->load->model('banner_kpi_m');

		$permission = $name.'.'.$action;
		$permission = trim($permission, '.');
		
		if($this->permission_m->has_permission("{$name}.Manage",$role_id)
			&& $this->permission_m->has_permission($permission,$role_id)) 
			return TRUE;

		if(!$this->permission_m->has_permission($permission,$role_id)) return FALSE;

		$user_id = empty($user_id) ? $this->admin_m->id : $user_id;

		if($this->permission_m->has_permission('banner.sale.access',$role_id) 
			&& get_term_meta_value($term_id,'staff_business') == $user_id)
			return TRUE;

		$args = array(
			'term_id' => $term_id,
			'user_id' => $user_id);

		if(!empty($kpi_type))
		{
			$args['kpi_type'] = $kpi_type;
		}

		$kpis = $this->webgeneral_kpi_m->get_by($args);
		if(empty($kpis)) return FALSE;

		return TRUE;
	}

	public function start_service($term = null)
	{
		restrict('banner.start_service');	
		$this->load->model('banner/banner_report_m') ;
		if(empty($term) )
			return false;

		if(!$term || $term->term_type !='banner')
			return false;
		$term_id = $term->term_id;
		$this->banner_report_m->send_mail_info_to_admin($term_id);


		/***
		 * Khởi tạo Meta thời gian dịch vụ để thuận tiện cho tính năng sắp xếp và truy vấn
		 */
		$start_service_time = get_term_meta_value($term_id, 'start_service_time');
		empty($start_service_time) AND update_term_meta($term_id, 'start_service_time', 0);
		
		$end_service_time = get_term_meta_value($term_id, 'end_service_time');
		empty($end_service_time) AND update_term_meta($term_id, 'end_service_time', 0);
		
		return true;
	}

	public function proc_service($term = FALSE)
	{
		if(!$term || $term->term_type != 'banner') return FALSE;
		$term_id = $term->term_id;

        $tech_kpis           = $this->banner_kpi_m->select('user_id')
                                                   ->where('term_id',$term_id)
                                                   ->where('kpi_type','tech')
                                                   ->as_array()
                                                   ->get_many_by(); 

        //if(empty($tech_kpis)) $this->messages->info('Chưa có nhân viên kỹ thuật');
        if(empty($tech_kpis)) return FALSE;
  
  		update_term_meta($term_id, 'start_service_time' , time());

		$is_send = $this->banner_report_m->send_mail_info_activated($term_id) ;

		if(FALSE === $is_send) return FALSE ;

		$this->log_m->insert(array(
						'log_title'		  => 'Kích hoạt thời gian bắt đầu thiết kế Bannner',
						'log_status'	  => 1,
						'term_id'		  => $term_id,
						'log_type' 		  => 'start_service_time',
						'user_id' 		  => $this->admin_m->id,
						'log_content' 	  => 'Đã gửi mail kích hoạt bắt đầu thiết kế Bannner',
						'log_time_create' => date('Y-m-d H:i:s'),
					 ));
		restrict('banner.start_service');

		return TRUE;
	}

	public function stop_service($term)
	{
		if(!$term || $term->term_type != 'banner') return FALSE;
		$term_id = $term->term_id;
	
		$is_send = $this->banner_report_m->send_mail_info_finish($term_id);
		if( FALSE === $is_send) return FALSE ;

		update_term_meta($term_id,'end_service_time',time());
		$this->contract_m->update($term_id, array('term_status'=>'ending'));

		$this->log_m->insert(array(
						'log_title'		  => 'Kích hoạt thời gian ngừng Hợp đồng thiết kế bannner',
						'log_status'	  => 1,
						'term_id'		  => $term_id,
						'log_type' 		  =>'end_service_time',
						'user_id' 		  => $this->admin_m->id,
						'log_content' 	  => 'Đã gửi mail ngừng Hợp đồng thiết kế bannner',
						'log_time_create' => date('Y-m-d H:i:s'),
					 ));

		return TRUE;
	}

}

/* End of file banner_report_m.php */
/* Location: ./application/modules/banner/models/banner_report_m.php */