<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Behaviour_m.php');

class Banner_behaviour_m extends Behaviour_m {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
	}

	/**
	 * Preview data for printable contract version
	 *
	 * @return     String  HTML content
	 */
	public function prepare_preview()
	{
		if( ! $this->contract) return FALSE;

		parent::prepare_preview();

		$data 				= $this->data;

		if( !empty($data['customer']) && $data['customer']->user_type == 'customer_company')
		{
			$data['data_customer'] = wp_parse_args($data['data_customer'], ['Công ty' => $data['customer']->display_name]);
		}

		$data['contract_code'] = get_term_meta_value($this->contract->term_id, 'contract_code');

		$banners_selected = get_term_meta_value($this->contract->term_id, 'banners_selected');
		$banners_selected = $banners_selected ? unserialize($banners_selected) : [];
		$_banners = array();
		foreach ($banners_selected as $key => $value)
		{
			$items = explode('_', $key);
			$index = array_shift($items);

			if($index == 'saleoff') $value = unserialize($value);

			$_banners[implode('_', $items)][$index] = $value;
		}

		$data['banners'] = array_filter($_banners, function($x){ return !empty($x['number']); }) ;
		$data['view_file'] 	= 'banner/contract/preview';
		
		return $data;
	}

	/**
	 * Calculates the contract value.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The contract value.
	 */
	public function calc_contract_value()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$banners_selected = get_term_meta_value($this->contract->term_id, 'banners_selected');
		$banners_selected = $banners_selected ? unserialize($banners_selected) : [];

		if(empty($banners_selected)) return 0;

		$data = array();
		foreach ($banners_selected as $key => $value)
		{
			$items = explode('_', $key);
			$index = array_shift($items);
			
			if($index == 'saleoff') $value = unserialize($value);

			$data[implode('_', $items)][$index] = $value;
		}

		$data = array_map(function($x){
			if(empty($x['price']) || empty($x['number'])) return 0;

			if(empty($x['saleoff']['min']) || empty($x['saleoff']['value']) || $x['number'] < $x['saleoff']['min']) return $x['price']*$x['number'];

			return ($x['price']*$x['number']) - ($x['price']*$x['number']*div($x['saleoff']['value'], 100));
		}, $data);

		return array_sum($data);
	}
}
/* End of file Banner_behaviour_m.php */
/* Location: ./application/modules/banner/models/Banner_behaviour_m.php */