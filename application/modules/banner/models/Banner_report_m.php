<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_report_m extends Base_Model {

	public function __construct() 
	{
		parent::__construct();

		$this->load->library('email');

		$this->load->model('staffs/admin_m');
		$this->load->model('banner/banner_contract_m');
		$this->load->model('banner/banner_kpi_m');

		$this->config->load('banner/banner') ;

		defined('SALE_MANAGER_ID') OR define('SALE_MANAGER_ID',18);
	}

	// Thông báo khởi tạo hợp đồng
	public function send_mail_info_activated($term_id = 0)
	{
		$title  = 'Hợp đồng thiết kế Banner ' . get_term_meta_value($term_id, 'contract_code'). ' đã được kích hoạt.';

		$data['term_id']          				= $term_id ;
		$data['title']            			    = $title ;
		$data['email_source']            			    = $title ;
		$data['term'] = $this->term_m->get($term_id);

		$this->set_mail_to($term_id,'customer')
			  ->set_mail_to($term_id, 'admin');
		$this->email->subject($title);
		
		$data['content']	= $this->load->view('banner/admin/send-mail/tpl_activation_email', $data, TRUE);
		$content 			= $this->load->view('email/blue_modern_tpl',$data,TRUE);
		$this->email->message($content);
		
		$send_status 			  				= $this->email->send();
		if(!$send_status) return FALSE;

		// Thực hiện ghi log	
		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Gửi mail kích hoạt Hợp đồng thiết kế Banner.',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id,
			'log_content'	=> 'Đã gửi mail thành công',
			'log_type'		=> 'banner-report-email',
			'log_time_create' => date('Y-m-d H:i:s'),
		));

		return TRUE;
	}

	// Thông báo kết thúc hợp đồng
	public function send_mail_info_finish($term_id = 0)
	{
		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

		//gửi mail kết thúc hợp đồng
		$data = array();
		$data['time'] = time();
	
		$data['term'] = $term;
		$data['term_id'] = $term_id;

		$title 			   = 'Thông báo kết thúc Hợp đồng thiết kế Bannner ' . get_term_meta_value($term_id, 'contract_code');
		$data['title'] 	   = $title ;
		$data['email_source']            			    = $title ;
		$data['term'] = $this->term_m->get($term_id);

		// Hiển thị thông tin kinh doanh ra ngoài view
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);

		$data['staff'] 	   = $sale;

		// customer-mailreport-admin
		$this->set_mail_to($term_id, 'customer') 
		     ->set_mail_to($term_id, 'mailreport')
		     ->set_mail_to($term_id, 'admin') ;
		$this->email->subject($title);

		$data['content']	= $this->load->view('admin/send-mail/end_contract', $data, TRUE);
		$content 			= $this->load->view('email/blue_modern_tpl',$data,TRUE);
		$this->email->message($content);		

 		$send_status 		= $this->email->send();
		if ( FALSE === $send_status ) return FALSE ;
		
		// Thực hiện ghi log	
		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Gửi mail kết thúc Hợp đồng thiết kế Bannner',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id,
			'log_content'	=> 'Đã gửi mail thành công',
			'log_type'		=> 'banner-report-email',
			'log_time_create' => date('Y-m-d H:i:s'),
		));

		return TRUE ;
	} 

	/**
	 * Sets the mail to.
	 *
	 * @param      integer  $term_id  The term identifier
	 * @param      string   $type_to  The type to [admin|customer|mailreport|specified]
	 *
	 * @return     self     ( description_of_the_return_value )
	 */
	public function set_mail_to($term_id = 0,$type_to = 'admin')
    {
        $this->email->clear(TRUE);

        $recipients = array(
            'mail_to' => array(),
            'mail_cc' => array(),
            'mail_bcc' => array('thonh@webdoctor.vn')
        );

        if(is_array($type_to) && !empty($type_to)) 
        {
            $recipients = array_merge_recursive($type_to,$recipients);
            $type_to = $type_to['type_to'] ?? 'specified';
        }

        extract($recipients);

        $this->email->from('support@adsplus.vn', 'Adsplus.vn');

        // Nếu email được cấu hình phải gửi cho một nhóm người được truyền vào
        if($type_to == 'specified')
        {
        	$this->recipients = $mail_to;
	        $this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
	        return $this;
        }

        // Nếu email được cấu hình gửi cho nhóm quản lý
        if($type_to == 'mailreport')
        {
        	$mail_to = array('thonh@webdoctor.vn');
        	$this->recipients = $mail_to;
        	$this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
	        return $this;
        }

        // Nếu KD nghỉ, người nhận là người quản lý
        $sale_id = get_term_meta_value($term_id,'staff_business');
        $sale 	 = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID); 
        
        // Lấy thông tin kỹ thuật phụ trách
        $tech_kpi = $this->banner_kpi_m
        ->select('user_id')
        ->group_by('user_id')
        ->get_many_by(['term_id'=>$term_id]);

        // Nếu gửi mail cho 'admin'
        if($type_to == 'admin')
        {
        	if(!empty($tech_kpi))
        	{
        		foreach ($tech_kpi as $i)
        		{
        			$mail = $this->admin_m->get_field_by_id($i->user_id,'user_email');
        			if(empty($mail)) continue;

        			$mail_to[] = $mail;
        		}
        	}

        	if($sale)
        	{
        		$mail_cc[] = $sale->user_email;
        	}
        	
			$mail_cc[] = 'thonh@webdoctor.vn';

        	$this->recipients = $mail_to;
        	$this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
	        return $this;
        }

        // Nếu gửi mail cho khách hàng
		if($type_to == 'customer')
		{
			$representative_email = get_term_meta_value($term_id,'representative_email');
			$mail_to[] = $representative_email;
		}


		// Email người nhận báo cáo được cấu hình theo dịch vụ
		if($mail_report = get_term_meta_value($term_id,'mail_report'))
		{
			$mails = explode(',',trim($mail_report));
			if(!empty($mails))
			{
				foreach ($mails as $mail) 
				{
					$mail_to[] = $mail;
				}
			}
		}

		if(!empty($tech_kpi))
    	{
    		foreach ($tech_kpi as $i)
    		{
    			$mail = $this->admin_m->get_field_by_id($i->user_id,'user_email');
    			if(empty($mail)) continue;

    			$mail_cc[] = $mail;
    		}
    	}

    	if($sale)
    	{
    		$mail_cc[] = $sale->user_email;
    	}

        $this->recipients = $mail_to;

        $this->email
	        ->from('support@adsplus.vn', 'Adsplus.vn')
	        ->to($mail_to)
	        ->cc($mail_cc)
	        ->bcc($mail_bcc);
        
        return $this;
    }

	// send mail cho kinh doanh, kỹ thuật
	public function send_mail_info_to_admin($term_id)
	{
		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

        $time = time();

		$contract_code 		= get_term_meta_value($term_id, 'contract_code');
		$title 				= 'Hợp đồng thiết kế Bannner ' . $contract_code . ' đã được khởi tạo.';
		$data 				= array();
		$data['title'] 		= $title ;

		$data['term']		= $term;
		$data['term_id'] 	= $term_id;
		$data['time']       = $time ;

	
		// Hiển thị thông tin kinh doanh ra ngoài view
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	   = $sale;

		// Hiên thị thông tin kỹ thuật phụ trách
		$tech_kpi 		   = $this->banner_kpi_m->select('user_id')
        											 ->group_by('user_id')
        											 ->get_many_by(['term_id'=>$term_id]);

      	if(!empty($tech_kpi))
        {
    		foreach ($tech_kpi as $i)
    		{
    			$user_mail  = $this->admin_m->get_field_by_id($i->user_id,'user_email');
    		}
        }  											 

		$this->set_mail_to($term_id,'admin');
		$this->email->subject($title);
		$this->email->message($this->load->view('banner/admin/send-mail/send_mail_admin', $data, TRUE));

		$send_status 			  				= $this->email->send();

		if( FALSE === $send_status ) return FALSE ;
		// Thực hiện ghi log	
		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Thông báo khởi tạo Hợp đồng thiết kế Bannner',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id,
			'log_content'	=> 'Đã gửi mail thành công',
			'log_type'		=> 'banner-report-email',
			'log_time_create' => date('Y-m-d H:i:s'),
		));
		return TRUE ;
	}
}
/* End of file banner_report_m.php */
/* Location: ./application/modules/banner/models/banner_report_m.php */