<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Webdoctor_report_m.php');

class Report_m extends Webdoctor_report_m {

    function __construct()
    {
        $this->recipients['cc'][] = 'luannn@webdoctor.vn';
        $this->recipients['cc'][] = 'quanly@webdoctor.vn';

        $this->autoload['models'][] = 'banner/banner_m';
        $this->autoload['models'][] = 'banner/banner_kpi_m';
        
        parent::__construct();
    }

    /**
     * Sends an activation email.
     *
     * @param      string  $type_to  The type to
     */
    public function send_activation_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;

        $contract_begin = get_term_meta_value($this->term_id, 'contract_begin');
        $contract_end   = get_term_meta_value($this->term_id, 'contract_end');
        
        $data = array(
            'subject'           => '[BANNER] Hợp đồng '.get_term_meta_value($this->term_id, 'contract_code').' đã được kích hoạt.',
            'content_tpl'       => 'banner/report/activation_email',
            'contract_begin'    => $contract_begin,
            'contract_end'      => $contract_end,
            'contract_date'     => my_date($contract_begin, 'd/m/Y') .' - '. my_date($contract_begin, 'd/m/Y'),
            'contract_value'    => (int) get_term_meta_value($this->term_id,'contract_value'),
            'sale'              => $this->sale
        );

        $this->data = wp_parse_args($data, $this->data);
        return parent::send_activation_email($type_to);
    }


    /**
     * Loads workers.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_workers()
    {
        parent::load_workers();

        $kpis = $this->banner_kpi_m->select('user_id')->where('term_id',$this->term_id)->where('kpi_type','tech')->get_all();
        if( ! $kpis) return $this;

        $_workers       = array_map(function($x){ return $this->admin_m->get_field_by_id($x->user_id); }, $kpis);
        $this->workers  = wp_parse_args(array_column($_workers, NULL, 'user_email'), $this->workers);

        return $this;
    }

    public function init($term_id = 0)
    {
        $term = $this->banner_m->set_term_type()->get($term_id);
        if( ! $term) return FALSE;

        $this->term     = $term;
        $this->term_id  = $term_id;

        $this->init_people_belongs();
        return $this;
    }

    /**
     * Sets the mail to.
     *
     * @param      string  $type_to  The type to
     *
     * @return     self    ( description_of_the_return_value )
     */
    public function set_mail_to($type_to = 'admin')
    {
        parent::set_mail_to($type_to);

        $recipients = $this->recipients;

        if($type_to == 'customer')
        {
            // Nếu gửi mail cho khách hàng
			if($representative_email = get_term_meta_value($this->term_id,'representative_email'))
			{
				$this->recipients['to'][] = $representative_email;
			}

			// Email người nhận báo cáo được cấu hình theo dịch vụ
			if($mail_report = get_term_meta_value($this->term_id, 'mail_report'))
			{
				$mails = explode(',',trim($mail_report));
				if(!empty($mails))
				{
					foreach ($mails as $mail) 
					{
						$this->recipients['to'][] = $mail;
					}
				}
			}

            $this->recipients  = $recipients;
            return $this;
        }

        return $this;
    }
}
/* End of file Report_m.php */
/* Location: ./application/modules/googleads/models/Report_m.php */