<?php

if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Banner_m extends Contract_m {

	public $term_type = 'banner';

	function __construct() 
	{
		parent::__construct();
	}

	public function set_term_type()
	{
		return $this->where('term_type', $this->term_type);
	}

	/*
	return post type for type
	*/
	public function get_post_type($type = '')
	{
		$this->load->config('banner');
		$post_type = $this->config->item('post_type');
		return isset($post_type[$type]) ? $post_type[$type] : false;
	}

	function get_posts_from_date($start_date, $end_date, $term_id = 0, $post_type ='', $r = array())
	{
		$defaults = array('order'=>array('posts.post_id'=>'desc'));
		$args = wp_parse_args( $r, $defaults );
		$this->order_by($args['order']);
		$this->where('post_type', $post_type);
		$this->where('start_date >=',$start_date);

		if(!empty($end_date))
			$this->where('end_date <=',$end_date);
		
		if($term_id >0)
		{
			$this->join('term_posts', 'term_posts.post_id = posts.post_id');
			$this->where('term_id',$term_id);
		}
		if(isset($args['meta_key']))
		{
			$this->join('postmeta','postmeta.post_id = posts.post_id');
			$this->where('meta_key',$args['meta_key']);
			if(isset($args['meta_value']))
				$this->where('meta_value',$args['meta_value']);
		}
		return $this->get_many_by();
	}

	function get_favicon($url = '')
	{
		
	}

	function count_post_complete($term_id = 0, $start_date ='', $end_date ='', $post_type='', $user_id = 0)
	{
		if(!$start_date) $start_date = $this->mdate->startOfMonth();
		if(!$end_date) $end_date = $this->mdate->endOfMonth();

		if(!$post_type)
			$post_type = 'content';
		$post_type = $this->get_post_type($post_type);

		$start_date = $this->mdate->startOfDay($start_date);
		$end_date = $this->mdate->endOfDay($end_date);
		if($term_id >0)
		{
			$this->join('term_posts', 'term_posts.post_id = posts.post_id');
			$this->where('term_id',$term_id);
		}
		$this->where('post_type', $post_type);
		$this->where('start_date >=',$start_date);
		$this->where('end_date <=',$end_date);
		if($user_id > 0)
		{
			$this->where('post_author', $user_id);
		}
		return $this->count_by();
	}

	public function has_permission($term_id = 0, $name = '', $action = '',$kpi_type = '',$user_id = 0,$role_id = null)
	{
		$this->load->model('banner_kpi_m');

		$permission = $name.'.'.$action;
		$permission = trim($permission, '.');
		
		if($this->permission_m->has_permission("{$name}.Manage",$role_id)
			&& $this->permission_m->has_permission($permission,$role_id)) 
			return TRUE;

		if(!$this->permission_m->has_permission($permission,$role_id)) return FALSE;

		$user_id = empty($user_id) ? $this->admin_m->id : $user_id;

		if($this->permission_m->has_permission('banner.sale.access',$role_id) 
			&& get_term_meta_value($term_id,'staff_business') == $user_id)
			return TRUE;

		$args = array(
			'term_id' => $term_id,
			'user_id' => $user_id);

		if(!empty($kpi_type))
		{
			$args['kpi_type'] = $kpi_type;
		}

		$kpis = $this->banner_kpi_m->get_by($args);
		if(empty($kpis)) return FALSE;

		return TRUE;
	}
}
/* End of file banner_m.php */
/* Location: ./application/modules/banner/models/banner_m.php */