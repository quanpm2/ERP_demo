<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['packages'] = array(
    'service' => array(),
    'default' =>''
);


$config['tasks_type'] = array(
	'task-default'		 	=>'thiết kế Bannner',
	'custom-task' 		 	=>'Yêu cầu khách hàng'
	);

$config['post_type'] = array(
	'task'				=>'banner-task',
	'content'		 	=>'banner-content',
	'content_product'	=>'banner-content-product',
	'mail' 				=>'banner-mail'
	);

$config['description_banner']='Banner tĩnh: Banner dạng tĩnh,Mục đích đặt trên website, cover Facebook, Facebook Ads, Google Display Network… Tổng dung lượng (kích thước) =< 100kb. Banner động: Banner động tối đa 3 frame, Thời gian trình chiếu mỗi frame tối thiểu 5s,Tổng dung lượng (kích thước) =< 150k.';

$config['packages_banner'] = array(
	'nomal_banner'		=> array(
		'facebook_nomal_banner'=>array(
			'label'			=>'Thiết kế banner Facebook <br/>(bao gồm 4 lựa chọn: <br/>Post tương tác hoặc Click To Web 					1200x900 hoặc 1200x628, <br/>Album (1 hình - 900x603 hoặc 598x900, 3 hình-600x600),<br/>Carousel (600x600) - 5 hình <br/>Fanpage: 851x315 (cover), 618x618 (avatar))',
			'name' 			=>'price_facebook_nomal_banner',
			'number'		=>'number_facebook_nomal_banner',
			'price_banner'	=> 650000,
			'kpi_point'		=> 0,
			'sale_off'		=> array('min' => 3,'value' => 27)),

		'desigh_nomal_banner' => array(
			'label'			=> 'Thiết kế Banner tĩnh',
			'name' 			=> 'price_desigh_nomal_banner',
			'number'		=> 'number_desigh_nomal_banner',
			'price_banner'	=> 150000,
			'kpi_point'		=> 5,
			'sale_off'		=> array('min' => 1,'value' => 0)),

		'resize_nomal_banner' => array(
			'label'			=> 'Resize Banner tĩnh',
			'name' 			=> 'price_resize_nomal_banner',
			'number'		=> 'number_resize_nomal_banner',
			'price_banner'	=> 50000,
			'kpi_point'		=> 1.5,
			'sale_off'		=> array('min' => 1,'value' => 0)),

		'combo_nomal_banner' => array(
			'label'			=>'[COMBO] Thiết kế banner Google Display Network <br/>(bao gồm 06 kích thước: 336x280, 300x250, 728x90, 160x600, 300x600, 1200x628 px)',
			'number'		=> 'number_combo_nomal_banner',
			'name' 			=> 'price_combo_nomal_banner',
			'price_banner'	=> 990000,
			'kpi_point'		=> 5 + (1.5 * 4),
			'sale_off'		=> array('min' => 3,'value' => 27)),
	),
	'moving_banner' 	=> array(
		
		'desigh_banner' => array(
			'label'		=> 'Thiết kế Banner động',
			'number'	=> 'number_desigh_moving_banner',
			'name' 		=> 'price_desigh_moving_banner',
			'price_banner'	=> 300000,
			'kpi_point'		=> 10,
			'sale_off'		=> array('min' => 1,'value' => 0)),

		'resize_banner'	=> array(
			'label' 	=> 'Resize Banner động',
			'number' 	=> 'number_resize_moving_banner',
			'name' 		=> 'price_resize_moving_banner',
			'price_banner' 	=> 100000,
			'kpi_point'		=> 3,
			'sale_off'		=> array('min' => 1,'value' => 0)),

		'combo_banner'	=> array(
			'label' 	=> '[COMBO] Thiết kế banner động Google Display Network <br/>(bao gồm 05 kích thước: 336x280, 300x250, 728x90, 160x600, 300x600 px)',
			'number' 	=> 'number_combo_moving_banner',
			'name' 		=> 'price_combo_moving_banner',
			'price_banner' 	=> 2190000,
			'kpi_point' 	=> 10 + (3 * 4),
			'sale_off'		=> array('min' => 3,'value' => 27)),
	),

	'company_banner' 	=> array(
		'design_banner_slider_static_company_banner' => array(
			'label' 	=> 'Banner tĩnh, Slider tĩnh (*)',
			'name' 		=> 'price_design_banner_slider_static_company_banner',
			'number' 	=> 'number_design_banner_slider_static_company_banner',
			'price_banner' 	=> 50000,
			'kpi_point'		=> 10,
			'sale_off'		=> array('min' => 1,'value' => 0)),

		'resize_static_company_banner' => array(
			'label' 	=> 'Resize Banner tĩnh (*)',
			'name' 		=> 'price_resize_static_company_banner',
			'number' 	=> 'number_resize_static_company_banner',
			'price_banner'	=> 100000,
			'kpi_point'		=> 1.5,
			'sale_off'		=> array('min' => 1,'value' => 0)),

		'resize_dynamic_company_banner'	=> array(
			'label' 	=> 'Resize Banner động (*)',
			'name' 		=> 'price_resize_dynamic_company_banner',
			'number' 	=> 'number_resize_dynamic_company_banner',
			'price_banner' 	=> 150000,
			'kpi_point'		=> 3,
			'sale_off'		=> array('min' => 1,'value' => 0)),

		'print_ad_company_banner' => array(
			'label' 	=> 'Print Ad (*)',
			'name' 		=> 'price_print_ad_company_banner',
			'number' 	=> 'number_print_ad_company_banner',
			'price_banner'	=> 2000000,
			'kpi_point'		=> 20,
			'sale_off'		=> array('min' => 1,'value' => 0)),

		'standee_company_banner'	=> array(
			'label' 	=> 'Standee (*)',
			'name' 		=> 'price_standee_company_banner',
			'number' 	=> 'number_standee_company_banner',
			'price_banner'	=> 2500000,
			'kpi_point'		=> 15,
			'sale_off'		=> array('min' => 1,'value' => 0)),		
		)
);

// Config default designer user id
$config['kpi'] = array(
	'kpi_type' 	=> 'tech',
	'user_id' 	=> 727,
	'kpi_value'	=> 1
);

$config['total_kpi_month']	= 500;
$config['bonus_kpi']		= 10000;