<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Zaloads_Package extends Package
{
    const ROOT_ROLE_ID = 1;

    function __construct()
    {
        parent::__construct();
        $this->website_id  = $this->uri->segment(4);
    }

    public function name()
    {
        return 'Zalo Ads';
    }

    public function init()
    {
        $this->_load_menu();
    }

    /**
     * Init menu LEFT | NAV ITEM FOR MODULE
     * then check permission before render UI
     */
    private function _load_menu()
    {
        $order = 0;

        if (!is_module_active('zaloads')) return FALSE;

        $itemId = 'zaloads';

        if (has_permission('zaloads.Index.access')) {
            $this->menu->add_item(array(
                'id'        => $itemId,
                'name'         => 'Zalo Ads',
                'parent'    => null,
                'slug'      => admin_url('zaloads'),
                'order'     => $order++,
                'icon'        => 'fa fa-lg fa-share-alt-square',
                'is_active' => is_module('zaloads')
            ), 'left-ads-service');
        }

        if (!is_module('zaloads')) return FALSE;

        if (has_permission('zaloads.Index.access')) {
            $this->menu->add_item(array(
                'id' => 'zaloads-service',
                'name' => 'Danh sách',
                'parent' => $itemId,
                'slug' => admin_url('zaloads'),
                'order' => $order++,
                'icon' => 'fa fa-fw fa-xs fa-database'
            ), 'left-ads-service');
        }

        if (has_permission('zaloads.staffs.manage')) {

            $this->menu->add_item(array(
                'id' => 'zaloads-staffs',
                'name' => 'Phân công/Phụ trách',
                'parent' => $itemId,
                'slug' => admin_url('zaloads/staffs'),
                'order' => $order++,
                'icon' => 'fa fa-fw fa-xs fa-group'
            ), 'left-ads-service');
        }

        $left_navs = array(
            'overview' => array(
                'name' => 'Tổng quan',
                'icon' => 'fa fa-fw fa-xs fa-tachometer',
            ),
            'kpi' => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-xs fa-heartbeat',
				),
            'setting'  => array(
                'name' => 'Cấu hình',
                'icon' => 'fa fa-fw fa-xs fa-cogs',
            )
        );

        $term_id = $this->uri->segment(4);

        $this->website_id = $term_id;

        if (empty($term_id) || !is_numeric($term_id)) return FALSE;

        foreach ($left_navs as $method => $name) {
            if (!has_permission("zaloads.{$method}.access")) continue;

            $icon = $name;
            if (is_array($name)) {
                $icon = $name['icon'];
                $name = $name['name'];
            }

            $this->menu->add_item(array(
                'id' => "zaloads-{$method}",
                'name' => $name,
                'parent' => $itemId,
                'slug' => module_url("{$method}/{$term_id}"),
                'order' => $order++,
                'icon' => $icon
            ), 'left-ads-service');
        }
    }

    public function title()
    {
        return 'Zalo Ads';
    }

    public function author()
    {
        return 'tanhn';
    }

    public function version()
    {
        return '1.0';
    }

    public function description()
    {
        return 'Quản lý hợp đồng Zalo';
    }

    /**
     * Init default permissions for zaloads module
     *
     * @return     array  permissions default array
     */
    private function init_permissions()
    {
        return array(

            'zaloads.index' => array(
                'description' => 'Quản lý Zalo Ads',
                'actions' => ['access', 'update', 'add', 'delete', 'manage', 'mdeparment', 'mgroup']
            ),

            'zaloads.done' => array(
                'description' => 'Dịch vụ đã xong',
                'actions' => ['access', 'add', 'delete', 'update', 'manage', 'mdeparment', 'mgroup']
            ),

            'zaloads.overview' => array(
                'description' => 'Tổng quan',
                'actions' => ['access', 'manage', 'mdeparment', 'mgroup']
            ),

            'zaloads.kpi' => array(
				'description' => 'KPI',
				'actions' => ['access','add','delete','update','manage','mdeparment','mgroup']),

            'zaloads.start_service' => array(
                'description' => 'Kích hoạt Zalo Ads',
                'actions' => ['access', 'update', 'manage', 'mdeparment', 'mgroup']
            ),

            'zaloads.stop_service' => array(
                'description' => 'Ngừng Zalo Ads',
                'actions' => ['access', 'update', 'manage', 'mdeparment', 'mgroup']
            ),

            'zaloads.setting' => array(
                'description' => 'Cấu hình',
                'actions' => ['access', 'add', 'delete', 'update', 'manage']
            ),

            'zaloads.staffs' => array(
                'description' => 'Phân công phụ trách',
                'actions' => ['access', 'add', 'delete', 'update', 'manage']
            )
        );
    }

    private function _update_permissions()
    {
        $permissions = $this->init_permissions();
        if (empty($permissions)) return false;

        foreach ($permissions as $name => $value) {
            $description = $value['description'];
            $actions = $value['actions'];
            $this->permission_m->add($name, $actions, $description);
        }
    }

    /**
     * Install module
     *
     * @return     bool  status of command
     */
    public function install()
    {
        $permissions = $this->init_permissions();
        if (!$permissions) return FALSE;

        $rootPermissions = $this->role_permission_m->get_by_role_id(self::ROOT_ROLE_ID);
        $rootPermissions and $rootPermissions = array_column($rootPermissions, 'role_id', 'permission_id');

        foreach ($permissions as $name => $value) {
            $description   = $value['description'];
            $actions        = $value['actions'];
            $permission_id = $this->permission_m->add($name, $actions, $description);

            if (!empty($rootPermissions[$permission_id])) continue;

            $this->role_permission_m->insert([
                'permission_id'    => $permission_id,
                'role_id'        => self::ROOT_ROLE_ID,
                'action'        => serialize($actions)
            ]);
        }

        return TRUE;
    }
    /**
     * Uninstall module command
     *
     * @return     bool  status of command
     */
    public function uninstall()
    {
        $permissions = $this->init_permissions();

        if (!$permissions) return FALSE;

        if ($pers = $this->permission_m->like('name', 'zaloads')->get_many_by()) {
            foreach ($pers as $per) {
                $this->permission_m->delete_by_name($per->name);
            }
        }

        foreach ($permissions as $name => $value) {
            $this->permission_m->delete_by_name($name);
        }

        return TRUE;
    }
}
/* End of file zaloads.php */
/* Location: ./application/modules/zaloads/models/zaloads.php */