<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'modules/contract/models/Contract_m.php');

class Zaloads_m extends Contract_m
{

    public $term_type = 'zalo-ads';

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Gets all active.
     *
     * @return     Array  All active.
     */
    public function get_all_active()
    {
        $terms = $this->set_term_type()->get_many_by(['term_status' => 'publish']);

        if (empty($terms)) return FALSE;

        foreach ($terms as $key => $term) {
            $adaccount_status   = get_term_meta_value($term->term_id, 'adaccount_status');
            $is_service_proc    = parent::is_service_proc($term);
            $begin_time         = get_term_meta_value($term->term_id, 'advertise_start_time');

            if (!$is_service_proc || empty($begin_time) || 'APPROVED' != $adaccount_status) {
                unset($terms[$key]);
                continue;
            }

            $end_time = get_term_meta_value($term->term_id, 'advertise_end_time');
            $last_end_time = strtotime('+1 day', $this->mdate->endOfDay($end_time));

            if (empty($end_time) || $last_end_time > time()) {
                $term->begin_time = $begin_time;
                $term->end_time = $end_time;
                continue;
            }

            unset($terms[$key]);
        }

        return $terms;
    }

    /**
     * Gets the segments.
     *
     * @throws     Exception  (description)
     *
     * @return     array      The segments.
     */
    public function getSegments()
    {
        if (!$this->contract) throw new Exception('CONTRACT UNSPECIFIED');

        $this->load->model('ads_segment_m');

        $segments = $this->term_posts_m->get_term_posts($this->contract->term_id, $this->ads_segment_m->post_type);
        if (empty($segments)) return [];

        $segments = array_map(function ($x) {

            $adaccount = $this->term_posts_m->get_post_terms($x->post_id, $this->adaccount_m->term_type);
            $adaccount and $adaccount = reset($adaccount);

            return array(
                'post_id'         => (int) $x->post_id,
                'start_date'     => (int) $x->start_date,
                'end_date'        => (int) $x->end_date,
                'contract_id'    => (int) $x->term_id,
                'adaccount_id'    => (int) $adaccount->term_id,
                'adaccount' => $adaccount,
                'adaccount' => array(
                    'id'    => (int) $adaccount->term_id,
                    'account_name'    => $adaccount->term_name,
                    'customer_id'    => $adaccount->term_slug,
                    'currency_code'    => get_term_meta_value($adaccount->term_id, 'currency_code') ?: 'VND',
                    'source' => get_term_meta_value($adaccount->term_id, 'source')
                )
            );
        }, $segments);

        $segments = array_values($segments);

        $this->contract->segments = $segments;
        return $segments;
    }

    /**
     * Gets the balance spend.
     *
     * @throws     Exception  (description)
     *
     * @return     array      The balance spend.
     */
    public function getBalanceSpend()
    {
        if (!$this->contract) throw new Exception('CONTRACT UNSPECIFIED');

        $this->load->model('balance_spend_m');

        $balance_spend = $this->term_posts_m->get_term_posts($this->contract->term_id, $this->balance_spend_m->post_type, [
            'where' => [
                'post_status' => 'publish',
                'comment_status' => 'direct'
            ]
        ]);
        if (empty($balance_spend)) return [];

        $balance_spend = array_map(function ($x) {
            return array(
                'post_id'         => (int) $x->post_id,
                'start_date'     => (int) $x->start_date,
                'end_date'        => (int) $x->end_date,
                'contract_id'    => (int) $x->term_id,
                'spend'         => (int) $x->post_content
            );
        }, $balance_spend);

        $balance_spend = array_values($balance_spend);

        $this->contract->balance_spend = $balance_spend;
        return $balance_spend;
    }

    /**
     * Gets the field configuration for datatable.
     *
     * @return     Array  The field configuration.
     */
    public function get_field_config()
    {
        $this->config->load('zaloads/fields');
        $default_columns = $this->config->item('default_columns', 'datasource');
        $columns = $this->config->item('columns', 'datasource');

        $config = parent::get_field_config();
        $config['columns'] = array_merge($config['columns'], $columns);
        $config['default_columns'] = $default_columns;

        return $config;
    }


    /**
     * Gets the field callback.
     *
     * @return     <type>  The field callback.
     */
    public function get_field_callback()
    {
        $default_callbacks = parent::get_field_callback();

        $default_callbacks['contract_code']['func'] = function ($data, $row_name) {

            $term_id                    = $data['term_id'];
            $contract_code              = get_term_meta_value($data['term_id'], 'contract_code');

            $data['contract_code']      = "";
            $data['contract_code_raw']  = $contract_code;

            switch ($data['term_status']) {
                case 'publish':

                    if (!is_service_proc($term_id) || !get_term_meta_value($term_id, 'advertise_start_time')) {
                        $data['contract_code'] = '<i class="fa fa-circle-o text-red pull-right" data-toggle="tooltip" data-placement="top" title="Trạng thái : Chưa cấu hình"></i>';
                        break;
                    }

                    $data['contract_code'] = '<i class="fa fa-circle-o text-green pull-right" data-toggle="tooltip" data-placement="top" title="Trạng thái : Đang hoạt động"></i>';

                    break;

                case 'pending':
                    $data['contract_code'] = '<i class="fa fa-circle-o pull-right" data-toggle="tooltip" data-placement="top" title="Trạng thái : Ngưng hoạt động"></i>';
                    break;

                default:
                    # code...
                    break;
            }
            /* end Icon Trạng thái*/

            $short_name             = mb_strlen($contract_code) > 24 ? (mb_substr($contract_code, 0, 24) . '...') : $contract_code;
            $data['contract_code'] .= anchor(module_url("overview/{$data['term_id']}"), '<i class="fa fw fa-external-link-square"></i>' . nbs(2) . $short_name);

            return $data;
        };

        $default_callbacks['customer_code']['func'] = function ($data, $row_name) {

            $term_id = $data['term_id'];
            $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company']);

            if (empty($customers)) return $data;

            $customer = end($customers);
            $data['customer_code'] = cid($customer->user_id, $customer->user_type);
            return $data;
        };

        $default_callbacks['website']['func'] = function ($data, $row_name) {

            // CALLBACK : WEBSITE TERM_NAME
            $term_id                = $data['term_id'];
            $contract_code          = get_term_meta_value($data['term_id'], 'contract_code');
            $short_name             = mb_strlen($contract_code) > 24 ? (mb_substr($contract_code, 0, 24) . '...') : $contract_code;
            $data['website']        = anchor(module_url("overview/{$data['term_id']}"), '<i class="fa fw fa-external-link-square"></i>' . nbs(2) . ($short_name ?: '<code>Không tồn tại</emtpy>'));
            $data['website_raw']    = $data['term_name'];

            if ($customer = $this->term_users_m->get_the_users($data['term_id'], ['customer_person', 'customer_company'])) {
                $customer = reset($customer);
                $data['website'] .= br() . '<small>MSKH :<b>' . cid($customer->user_id, $customer->user_type) . '</b></small>';
            }

            if ('external' == get_term_meta_value($data['term_id'], 'adaccount_source')) {
                $data['website'] .= '<small><i>  (Tài khoản khách)</i></small>';
            }


            return $data;
        };

        $default_callbacks['contract_budget']['func'] = function ($data, $row_name) {

            $contract_m = (new contract_m())->set_contract((object) $data);
            $contract_budget = $contract_m->get_behaviour_m()->calc_budget();
            $data['contract_budget']        = currency_numberformat($contract_budget, '');
            $data['contract_budget_raw']    = $contract_budget;

            $vat = div((float) get_term_meta_value($data['term_id'], 'vat'), 100);
            if ($vat > 0) $data['contract_budget'] = "<b>{$data['contract_budget']}</b>";

            $fct = (float) get_term_meta_value($data['term_id'], 'fct');
            if ($fct > 0) $data['contract_budget'] = "<u>{$data['contract_budget']}</u>";

            return $data;
        };

        $default_callbacks['status']['func'] = function ($data, $row_name) {

            $stats = $this->config->item('status', 'zaloads');
            $data['status_raw'] = $stats[$data['term_status']] ?? $data['term_status'];

            // CALLBACK : TERM_STATUS
            switch ($data['term_status']) {
                case 'publish':
                    $data['status'] = '<i class="fa fa-toggle-on text-green" data-toggle="tooltip" data-placement="top" title="Trạng thái : Đang hoạt động"></i>';
                    break;

                default:
                    $data['status'] = '<i class="fa fa-toggle-off" data-toggle="tooltip" data-placement="top" title="Trạng thái : Ngưng hoạt động"></i>';
                    break;
            }

            return $data;
        };

        $callbacks = array(

            'actual_result' => array(
                'field' => 'actual_result',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['actual_result'])) return $data;

                    $data['balance_spend'] = (float) get_term_meta_value($data['term_id'], 'balance_spend');

                    $actual_result = (float) get_term_meta_value($data['term_id'], 'actual_result');

                    $balanceBudgetReceived = (int) get_term_meta_value($data['term_id'], 'balanceBudgetReceived');
                    $actual_result = $actual_result + $data['balance_spend'];

                    $data['actual_result_raw']  = $actual_result;
                    $data['actual_result']      = $actual_result > 0 ? currency_numberformat($actual_result, '') : '--';

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'result_updated_on' => array(
                'field' => 'result_updated_on',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['result_updated_on'])) return $data;

                    $result_updated_on              = (int) get_term_meta_value($data['term_id'], 'result_updated_on');
                    $data['result_updated_on_raw']  = $result_updated_on;
                    $data['result_updated_on']      = $result_updated_on ? my_date($result_updated_on, 'd/m/Y H:i:s') : '--';

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'result_pending_days' => array(
                'field' => 'result_pending_days',
                'func'    => function ($data, $row_name) {

                    $result_updated_on = $data['result_updated_on'] ?? get_term_meta_value($data['term_id'], 'result_updated_on');
                    if (empty($result_updated_on)) return 0;

                    $days = round(diffInDates($result_updated_on, time()), 1);

                    $data['result_pending_days_raw']     = $days;
                    $data['result_pending_days']         = $days;
                    return $data;
                },
                'row_data' => FALSE,
            ),

            'adaccount_status' => array(
                'field' => 'adaccount_status',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['adaccount_status'])) return $data;

                    $term_id            = $data['term_id'];
                    $states             = $this->config->item('states', 'adaccount_status');
                    $adaccount_status   = get_term_meta_value($term_id, 'adaccount_status') ?: $this->config->item('default', 'adaccount_status');
                    $text               = $states[$adaccount_status] ?? 'UNKNOWN';

                    $data['adaccount_status']       = $adaccount_status;
                    $data['adaccount_status_raw']   = $text;

                    if ('UNSPECIFIED' == $adaccount_status) {
                        $data['adaccount_status'] = anchor(base_url("admin/zaloads/setting/{$term_id}"), '<span class="btn btn-danger btn-xs"><i class="fa fw fa-times-circle-o"></i>&nbsp;&nbsp;' . $text . '</span>', ['target' => "_blank"]);
                        return $data;
                    }

                    if ('PENDING_APPROVAL' == $adaccount_status) {
                        $data['adaccount_status'] = anchor(base_url("admin/zaloads/setting/{$term_id}"), '<span class="btn btn-primary btn-xs"><i class="fa fw fa-cog"></i>&nbsp;&nbsp;' . $text . '</span>', ['target' => "_blank"]);
                        return $data;
                    }

                    if ('APPROVED' == $adaccount_status) {
                        $data['adaccount_status'] = anchor(base_url("admin/zaloads/setting/{$term_id}"), '<span class="btn btn-success btn-xs"><i class="fa fw  fa-check-circle-o"></i>&nbsp;&nbsp;' . $text . '</span>', ['target' => "_blank"]);
                        return $data;
                    }

                    $data['adaccount_status'] = anchor(base_url("admin/zaloads/setting/{$term_id}"), '<span class="btn btn-default btn-xs"><i class="fa fw fa-times-circle-o"></i>&nbsp;&nbsp;Unknown</span>', ['target' => "_blank"]);

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'actual_budget' => array(
                'field' => 'actual_budget',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['actual_budget'])) return $data;

                    $term_id = $data['term_id'];
                    $actual_budget              = get_term_meta_value($data['term_id'], 'actual_budget');
                    $data['actual_budget_raw']  = (int) $actual_budget;
                    $data['actual_budget']      = $actual_budget > 0 ? currency_numberformat($actual_budget, '') : '--';

                    $payment_percentage             = 100 * (float) get_term_meta_value($term_id, 'payment_percentage');
                    $data['payment_percentage']     = currency_numberformat($payment_percentage, '%');
                    $data['payment_percentage_raw'] = $payment_percentage;

                    $payment_percentage_text = currency_numberformat($payment_percentage, '%');
                    if ('customer' == get_term_meta_value($term_id, 'contract_budget_payment_type')) {
                        $payment_percentage_text = "Khách tự thanh toán ({$payment_percentage_text})";
                    }

                    if ($payment_percentage >= 50 && $payment_percentage < 80) $text_color = 'text-yellow';
                    else if ($payment_percentage >= 80 && $payment_percentage < 90) $text_color = 'text-light-blue';
                    else if ($payment_percentage >= 90) $text_color = 'text-green';
                    else $text_color = 'text-red';

                    if ($actual_budget > 0) {
                        $data['actual_budget'] =
                            "<b data-toggle='tooltip' data-original-title='{$payment_percentage_text}' class='{$text_color}'>" . currency_numberformat($actual_budget, '') . '</b>';
                    }

                    if ('customer' == get_term_meta_value($term_id, 'contract_budget_payment_type')) {
                        $data['actual_budget'] = "<strike>{$data['actual_budget']}</strike>";
                        if ('behalf' == get_term_meta_value($term_id, 'contract_budget_customer_payment_type'))
                            $data['actual_budget'] = '<span class="fa fa-credit-card"></span> &nbsp' . $data['actual_budget'];
                    }

                    $vat = (int) get_term_meta_value($data['term_id'], 'vat');
                    switch (get_term_meta_value($data['term_id'], 'contract_budget_payment_type')) {
                        case 'customer':
                            $data['actual_budget'] .= (empty($vat) ? '<small class="pull-right">(đ)</small>' : '');
                            break;

                        default:
                            $data['actual_budget'] .= (empty($vat) ? '<small class="pull-right">(đ)</small>' : '&nbsp<small class="pull-right">($)</small>');
                            break;
                    }

                    /* Tình trạng bảo lãnh hiện tại của hợp đồng */
                    $bailment_amount    = (int) get_term_meta_value($term_id, 'bailment_amount');
                    $bailment_end_date  = get_term_meta_value($term_id, 'bailment_end_date');
                    if ($bailment_amount > 0) {
                        $bailment_class = $bailment_end_date < time() ? 'text-red' : 'text-yellow';
                        $bailment_decr = $bailment_end_date < time() ? 'Nợ ' : 'Bảo lãnh';

                        $data['actual_budget'] .= '<p class="' . $bailment_class . '"><small>' . $bailment_decr . ' ' . currency_numberformat($bailment_amount, '') . '</small></p>';
                    }

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'payment_expected_end_time' => array(
                'field' => 'payment_expected_end_time',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['payment_expected_end_time'])) return $data;

                    $payment_expected_end_time              = get_term_meta_value($data['term_id'], 'payment_expected_end_time');
                    $data['payment_expected_end_time']      = my_date($payment_expected_end_time, 'd/m/Y');
                    $data['payment_expected_end_time_raw']  = $payment_expected_end_time;

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'payment_real_progress' => array(
                'field' => 'payment_real_progress',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['payment_real_progress'])) return $data;

                    $data['payment_real_progress']  = '--';
                    $term_id                = $data['term_id'];
                    $payment_real_progress  = get_term_meta_value($term_id, 'payment_real_progress');
                    if (empty($payment_real_progress)) return $data;

                    $data['payment_real_progress']      = currency_numberformat((float) $payment_real_progress, '%');
                    $data['payment_real_progress_raw']  = $payment_real_progress;

                    try {
                        $_contract = (new contract_m())->set_contract((object) $data);
                        $expected_end_time  = $_contract->get_behaviour_m()->calc_payment_expected_end_time();
                        $contract_days      = $_contract->get_behaviour_m()->calc_contract_days();

                        $start_time         = start_of_day(get_term_meta_value($term_id, 'advertise_start_time'));
                        $commit_end_time    = strtotime("+{$contract_days} days -1 day", $start_time);
                        $balance_days       = round(div($expected_end_time - $commit_end_time, 86400));

                        $class = ($payment_real_progress < 100) ? 'text-red' : 'text-green';
                        $title = $balance_days > 0 ? "Chậm tiến độ {$balance_days} ngày" : "Vượt tiến độ {$balance_days} ngày";

                        $data['payment_real_progress'] = "<span class='progress-text {$class}' data-toggle='tooltip' data-original-title='{$title}'><b>{$data['payment_real_progress']}</b></span>";
                    } catch (Exception $e) {
                        return $data;
                    }

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'payment_cost_per_day_left' => array(
                'field' => 'payment_cost_per_day_left',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['payment_cost_per_day_left'])) return $data;

                    try {
                        $_contract = (new contract_m())->set_contract((object) $data);
                        $payment_cost_per_day_left  = $_contract->get_behaviour_m()->calc_cost_per_day_left('payment');
                        $data['payment_cost_per_day_left']      = currency_numberformat($payment_cost_per_day_left, '');
                        $data['payment_cost_per_day_left_raw']  = $payment_cost_per_day_left;
                    } catch (Exception $e) {
                        $data['cost_per_day_left'] = '--';
                    }

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'actual_progress_percent_net' => array(
                'field' => 'actual_progress_percent_net',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['actual_progress_percent_net'])) return $data;

                    $value = (float) get_term_meta_value($data['term_id'], 'actual_progress_percent_net') * 100;
                    $class = $value < 50 ? 'text-aqua' : ($value < 80 ? 'text-yellow' : ($value < 90 ? 'text-green' : 'text-red'));

                    $data['actual_progress_percent_net']        = "<p class='{$class}'><b>" . currency_numberformat($value, '%', 2) . '</b></p>';
                    $data['actual_progress_percent_net_raw']    = $value;

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'expected_end_time' => array(
                'field' => 'expected_end_time',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['expected_end_time'])) return $data;

                    $expected_end_time              = get_term_meta_value($data['term_id'], 'expected_end_time');
                    $data['expected_end_time']      = my_date($expected_end_time, 'd/m/Y');
                    $data['expected_end_time_raw']  = $expected_end_time;

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'real_progress' => array(
                'field' => 'real_progress',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['real_progress'])) return $data;

                    $data['real_progress']  = '--';
                    $term_id        = $data['term_id'];
                    $real_progress  = get_term_meta_value($term_id, 'real_progress');
                    if (empty($real_progress)) return $data;

                    $data['real_progress']      = currency_numberformat((float) $real_progress, '%');
                    $data['real_progress_raw']  = $real_progress;

                    try {
                        $_contract          = (new contract_m())->set_contract((object) $data);
                        $expected_end_time  = $_contract->get_behaviour_m()->calc_expected_end_time();
                        $contract_days      = $_contract->get_behaviour_m()->calc_contract_days();

                        $start_time         = start_of_day(get_term_meta_value($term_id, 'advertise_start_time'));
                        $commit_end_time    = strtotime("+{$contract_days} days -1 day", $start_time);
                        $balance_days       = round(div($expected_end_time - $commit_end_time, 86400));

                        $class = ($real_progress < 100) ? 'text-red' : 'text-green';
                        $title = $balance_days > 0 ? "Chậm tiến độ {$balance_days} ngày" : "Vượt tiến độ {$balance_days} ngày";

                        $data['real_progress'] = "<span class='progress-text {$class}' data-toggle='tooltip' data-original-title='{$title}'><b>{$data['real_progress']}</b></span>";
                    } catch (Exception $e) {
                        return $data;
                    }

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'cost_per_day_left' => array(
                'field' => 'cost_per_day_left',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['cost_per_day_left'])) return $data;

                    try {
                        $_contract = (new contract_m())->set_contract((object) $data);
                        $cost_per_day_left  = $_contract->get_behaviour_m()->calc_cost_per_day_left('commit');
                        $data['cost_per_day_left']      = currency_numberformat($cost_per_day_left, '');
                        $data['cost_per_day_left_raw']  = $cost_per_day_left;
                    } catch (Exception $e) {
                        $data['cost_per_day_left'] = '--';
                    }

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'actual_progress_percent' => array(
                'field' => 'actual_progress_percent',
                'func'  => function ($data, $row_name) {

                    if (!empty($data['actual_progress_percent'])) return $data;

                    $value = (float) get_term_meta_value($data['term_id'], 'actual_progress_percent');
                    $class = $value < 50 ? 'text-aqua' : ($value < 80 ? 'text-yellow' : ($value < 90 ? 'text-green' : 'text-red'));

                    $data['actual_progress_percent']        = "<p class='{$class}'><b>" . currency_numberformat($value, '%', 2) . '</b></p>';
                    $data['actual_progress_percent_raw']    = $value;

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'adaccount_id' => array(
                'field' => 'adaccount_id',
                'func'  => function ($data, $row_name) {

                    $data['adaccount_id']       = '--';
                    $data['adaccount_id_raw']   = '--';

                    $term_id        = $data['term_id'];

                    $adaccount_source       = get_term_meta_value($term_id, 'adaccount_source') ?: 'internal';

                    switch ($adaccount_source) {
                        case 'external':

                            $external_bmid          = get_term_meta_value($term_id, 'external_bmid');
                            $external_adaccount_id  = get_term_meta_value($term_id, 'external_adaccount_id');

                            if (empty($external_adaccount_id)) {
                                $data['adaccount_id'] = anchor(base_url("admin/zaloads/setting/{$term_id}"), '<span class="btn btn-danger btn-xs"><i class="fa fw fa-cog"></i>&nbsp;&nbsp;Cấu hình</span>', ['target' => "_blank"]);
                                return $data;
                            }

                            $data['adaccount_id_raw']   = $external_adaccount_id;
                            $data['adaccount_id']       = anchor(base_url("admin/zaloads/setting/{$term_id}"), '<span class="btn btn-primary btn-xs"><i class="fa fw fa-cog"></i>&nbsp;&nbsp;Đã cấu hình</span>', ['target' => "_blank"]);

                            break;

                        default:

                            $adaccount_id   = (int) get_term_meta_value($term_id, 'adaccount_id');
                            if (empty($adaccount_id)) {
                                $data['adaccount_id'] = anchor(base_url("admin/zaloads/setting/{$term_id}"), '<span class="btn btn-danger btn-xs"><i class="fa fw fa-cog"></i>&nbsp;&nbsp;Cấu hình</span>', ['target' => "_blank"]);
                                return $data;
                            }

                            $data['adaccount_id_raw']   = $adaccount_id;
                            $data['adaccount_id']       = anchor(base_url("admin/zaloads/setting/{$term_id}"), '<span class="btn btn-primary btn-xs"><i class="fa fw fa-cog"></i>&nbsp;&nbsp;Đã cấu hình</span>', ['target' => "_blank"]);
                            break;
                    }
                    return $data;
                },
                'row_data' => FALSE,
            ),


            'adaccounts' => array(
                'field' => 'adaccounts',
                'func'  => function ($data, $row_name) {

                    $data['adaccounts']         = '--';
                    $data['adaccounts_raw']   = [];

                    $term_id            = $data['term_id'];
                    $adaccounts = get_term_meta($data['term_id'], 'adaccounts', FALSE, TRUE);
                    $adaccounts and $adaccounts = array_map('intval', $adaccounts);

                    if (empty($adaccounts)) {
                        $data['adaccounts'] = anchor(base_url("admin/zaloads/setting/{$term_id}"), '<span class="btn btn-danger btn-xs"><i class="fa fw fa-times-circle-o"></i>&nbsp;&nbsp;Chưa cấu hình</span>', ['target' => "_blank"]);
                        return $data;
                    }

                    $start_service_time = get_term_meta_value($data['term_id'], 'start_service_time');
                    $is_running = !empty($start_service_time);
                    if ($is_running) {
                        $data['adaccounts'] = anchor(base_url("admin/zaloads/setting/{$term_id}"), '<span class="btn btn-success btn-xs"><i class="fa fw  fa-check-circle-o"></i>&nbsp;&nbsp;Đã kích hoạt</span>', ['target' => "_blank"]);
                        return $data;
                    }

                    $data['adaccounts'] = anchor(base_url("admin/zaloads/setting/{$term_id}"), '<span class="btn btn-primary btn-xs"><i class="fa fw fa-cog"></i>&nbsp;&nbsp;Đã cấu hình</span>', ['target' => "_blank"]);

                    return $data;
                },
                'row_data' => FALSE,
            )
        );

        return wp_parse_args($callbacks, $default_callbacks);
    }


    /**
     * Tính thời gian dự kiến kết thúc
     *
     * Dự kiến kết thúc = [Tổng NS tiến độ]/[Tổng NS đã chạy]*[Số ngày HD] - 1 + Ngày kích hoạt thực hiện dịch vụ
     *
     * @param      int  $term_id  The term identifier
     *
     * @return     int  Thời gian dự kiến kết thúc dịch vụ dựa theo tiến độ thực tế
     */
    public function calc_expected_end_time($term_id)
    {
        $start_service_time = get_term_meta_value($term_id, 'start_service_time');
        if (empty($start_service_time)) return 0;

        $retval = 0;

        $total_budget_progress = $this->calc_budget_progress($term_id);

        // Tính tổng ngân sách thực tế đã chi
        $total_budget_actual = get_term_meta_value($term_id, 'actual_result') ?? 0;
        $contract_days = $this->calc_contract_days($term_id);

        $start_time = $this->mdate->startOfDay(get_term_meta_value($term_id, 'advertise_start_time'));
        $expected_day = round(div($total_budget_progress, $total_budget_actual) * $contract_days);
        $retval = strtotime("+{$expected_day} days -1 day", $start_time);

        return $retval;
    }


    /**
     * Tính tổng số ngày chạy dịch vụ dựa theo ngày hợp đồng
     *
     * @param      <type>  $term_id  The term identifier
     *
     * @return     double  The real progress.
     */
    function calc_contract_days($term_id)
    {
        $retval = 0;

        $contract_begin = get_term_meta_value($term_id, 'contract_begin');
        $contract_end = get_term_meta_value($term_id, 'contract_end');
        $retval = diffInDates($this->mdate->startOfDay($contract_begin), $this->mdate->startOfDay($contract_end));

        return $retval;
    }

    /**
     * Tính tổng số ngày đã chạy dịch vụ dựa theo ngày kích hoạt thực hiện dịch vụ
     *
     * @param      <type>  $term_id  The term identifier
     *
     * @return     double  The real progress.
     */
    function calc_real_days($term_id)
    {
        $start_service_time = get_term_meta_value($term_id, 'start_service_time');
        if (empty($start_service_time)) return 0;

        $retval = 0;

        $start_time = $this->mdate->startOfDay(get_term_meta_value($term_id, 'advertise_start_time'));
        $end_service_time = get_term_meta_value($term_id, 'end_service_time') ?: $this->mdate->startOfDay();
        $retval = diffInDates($start_time, $end_service_time);

        return $retval;
    }


    function widget_complete_percent_bar($real = 0, $total = 0, $colors = array(), $titles = array())
    {
        $percent = ($total == 0) ? 0 : $real / $total;
        $percent = $percent * 100;
        $progress_color = reset($colors);

        $default_titles = array('real' => '', 'percent' => '', 'total' => '');
        $titles = array_merge($default_titles, $titles);
        $real_decimal = is_float($real) ? 2 : 0;
        $total_decimal = is_float($total) ? 2 : 0;
        $percent_decimals = is_float($percent) ? 2 : 0;;

        foreach ($colors as $color_pecent => $class) {
            if ($percent >= $color_pecent)
                $progress_color = $class;
        }
        return
            '<div class="progress-group">
            <span class="progress-text" data-toggle="tooltip" title="" data-original-title="' . $titles['percent'] . '">' . numberformat($percent, $percent_decimals) . '%</span>
            <span class="progress-number"><b><span data-toggle="tooltip" title="' . $titles['real'] . '">' .
            numberformat($real, $real_decimal) . '</span></b>/
                <span data-toggle="tooltip" title="' . $titles['total'] . '">' .
            numberformat($total, $total_decimal) .
            '</span></span>
                    <div class="progress sm" style="width:100%;">
                        <div class="progress-bar ' . $progress_color . ' progress-bar-striped" style="width: ' . $percent . '%"></div>
                    </div>
                </div>';
    }

    /**
     * Sets the behaviour m.
     *
     * @return     self  ( description_of_the_return_value )
     */
    protected function set_behaviour_m()
    {
        if (!$this->contract) return FALSE;
        $this->load->model('zaloads/zaloads_behaviour_m', 'behaviour_tmp');

        $behaviour_tmp = (new Behaviour_m());
        $behaviour_tmp->set_contract($this->contract);

        $this->behaviour_model = $behaviour_tmp->get_instance();
        return $this;
    }

    /**
     * Gets all by_adaccount_id.
     *
     * @param      int     $adaccount_id    The_adaccount_id
     *
     * @return     <type>  All by_adaccount_id.
     */
    public function get_all_by_adaccount_id($adaccount_id = 0)
    {
        if (!empty($this->contract)) $adaccount_id = get_term_meta_value($this->contract->term_id, 'adaccount_id');

        if (empty($adaccount_id)) return false;

        $m_args = array(
            'key' => 'adaccount_id', 'value' => (int) $adaccount_id, 'compare' => '='
        );

        $contracts = $this->select('term.term_id, term_name, term_status, term_type')->m_find($m_args)->set_term_type()->get_all();

        foreach ($contracts as &$contract) {
            $contract->adaccount_id = (int) get_term_meta_value($contract->term_id, 'adaccount_id');
            $contract->advertise_start_time = (int) get_term_meta_value(
                $contract->term_id,
                'advertise_start_time'
            );
            $contract->advertise_end_time = (int) get_term_meta_value(
                $contract->term_id,
                'advertise_end_time'
            );
            $contract->contract_code = get_term_meta_value($contract->term_id, 'contract_code');

            $contract->actual_budget = (int) get_term_meta_value($contract->term_id, 'actual_budget');

            $contract_m = (new contract_m())->set_contract($contract);
            $contract->actual_result = (float) $contract_m->get_behaviour_m()->get_actual_result();
        }

        return $contracts;
    }

    /**
     * Determines if valid start date.
     *
     * @param      int   $adaccount_id         The adaccount_id
     * @param      int   $start_date  The start date
     *
     * @return     bool  True if valid start date, False otherwise.
     */
    public function isValidStartDate($adaccount_id = 0, $start_date = 0)
    {
        if (!$this->contract) return FALSE;

        return empty($this->get_conflict_by_start_date());
    }

    /**
     * Gets the conflict by start date.
     *
     * @param      int     $adaccount_id         The adaccount_id
     * @param      int     $start_date  The start date
     *
     * @return     <type>  The conflict by start date.
     */
    public function get_conflict_by_start_date($adaccount_id = 0, $start_date = 0)
    {
        if (!$this->contract) return FALSE;

        !empty($this->contract) and $this->where('term.term_id !=', (int) $this->contract->term_id);

        $this->contract->advertise_start_time = (int) get_term_meta_value($this->contract->term_id, 'advertise_start_time');

        $m_args = ['key' => 'adaccount_id', 'compare' => '=', 'value' => (int) $adaccount_id];

        $contracts = $this->select('term.term_id, term_name, term_status, term_type')->m_find($m_args)->set_term_type()->where('term.term_id !=', (int) $this->contract->term_id)->get_all();

        if (empty($contracts)) return [];

        $contracts = array_map(function ($x) {

            $x->adaccount_id = (int) get_term_meta_value($x->term_id, 'adaccount_id');;
            $x->advertise_start_time = (int) get_term_meta_value($x->term_id, 'advertise_start_time');
            $x->advertise_end_time = (int) get_term_meta_value($x->term_id, 'advertise_end_time') ?: end_of_day();

            $x->contract_code = get_term_meta_value($x->term_id, 'contract_code');
            return $x;
        }, $contracts);

        $contracts = array_filter($contracts, function ($x) use ($start_date) {
            return ($x->advertise_start_time <= $start_date && $start_date <= $x->advertise_end_time) || ($start_date <= $x->advertise_start_time && $start_date <= $x->advertise_end_time);
        });

        return $contracts;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function existed_check($value)
    {
        if (empty($value)) {
            return true;
        }
        
        $isExisted = $this->set_term_type()->where('term.term_id', (int) $value)->count_by() > 0;
        if (!$isExisted) {
            $this->form_validation->set_message('existed_check', 'Hợp đồng không tồn tại.');
            return false;
        }

        return true;
    }
}
/* End of file Zaloads_m.php */
/* Location: ./application/modules/zaloads/models/Zaloads_m.php */