<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'modules/zaloads/models/Zaloads_behaviour_m.php');

class Zaloads_rent_account_behaft_behaviour_m extends Zaloads_behaviour_m
{

    /**
     * Calculates the fct.
     *
     * @param      float|integer  $percent  The percent
     *
     * @return     <type>         The fct.
     */
    public function calc_fct($percent = 0.05, $amount = 0)
    {
        parent::is_exist_contract(); // Determines if exist contract.
        return 0;
    }

    /**
     * Calculates the contract value.
     *
     * @throws     Exception  (description)
     *
     * @return     integer    The contract value.
     */
    public function calc_contract_value()
    {
        parent::is_exist_contract(); // Determines if exist contract

        $total = (int) get_term_meta_value($this->contract->term_id, 'service_fee');

        'behalf' == get_term_meta_value($this->contract->term_id, 'contract_budget_customer_payment_type')
            and $total += (int) get_term_meta_value($this->contract->term_id, 'contract_budget');

        $total -= parent::calc_disacount_amount();

        return $total;
    }


    /**
     * Calculates the budget.
     *
     * @throws     Exception  (description)
     *
     * @return     <type>     The budget.
     */
    public function calc_budget()
    {
        parent::is_exist_contract(); // Determines if exist contract.

        return (int) get_term_meta_value($this->contract->term_id, 'contract_budget');
    }


    /**
     * Render Pritable contract
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     Array   Data result for Preview Action
     */
    public function prepare_preview($term_id = 0)
    {
        parent::is_exist_contract(); // Determines if exist contract.

        $data = $this->data;

        $data['data_service'] = array(
            'service_name'     => 'Cung cấp tài khoản',
            'budget'        => (int) get_term_meta_value($this->contract->term_id, 'contract_budget'),
            'currency'         => get_term_meta_value($this->contract->term_id, 'currency'),
            'service_fee'     => (int) get_term_meta_value($this->contract->term_id, 'service_fee')
        );

        $discount_amount     = (float) get_term_meta_value($this->contract->term_id, 'discount_amount');
        $promotions         = get_term_meta_value($this->contract->term_id, 'promotions') ?: [];

        if (!empty($discount_amount) && !empty($promotions)) {
            $promotions = unserialize($promotions);

            $data['data_service']['discount_amount'] = $discount_amount;
            $data['data_service']['promotions'] = array_map(function ($promotion) use ($data) {

                $promotion['text']     = "CTKM - {$promotion['name']}";
                $_value     = $promotion['value'];

                if ('percentage' == $promotion['unit']) {
                    $_value = $data['data_service']['service_fee'] * div($promotion['value'], 100);
                    $promotion['text'] .= "({$promotion['value']}% phí dịch vụ)";
                }

                $promotion['amount'] = $_value;
                return $promotion;
            }, $promotions);
        }

        $data['contract_budget_customer_payment_type'] = (string) get_term_meta_value($this->contract->term_id, 'contract_budget_customer_payment_type');
        $data['deposit_amount'] = (int) get_term_meta_value($this->contract->term_id, 'deposit_amount');
        $data['has_deposit']     = !empty($data['deposit_amount']);
        $data['view_file']         = 'zaloads/contract/preview/rent_account_behaft_with_deposit';

        return $data;
    }


    /**
     * Tính thời gian dự kiến kết thúc
     *
     * Dự kiến kết thúc = [Tổng NS tiến độ]/[Tổng NS đã chạy]*[Số ngày HD] - 1 + Ngày kích hoạt thực hiện dịch vụ
     *
     * @throws     Exception  Error , stop and return
     *
     * @return     int        Thời gian dự kiến kết thúc dịch vụ dựa theo tiến độ thực tế
     */
    public function calc_expected_end_time()
    {
        parent::is_exist_contract(); // Determines if exist contract.
        if (!is_service_proc($this->contract->term_id)) throw new Exception('Dịch vụ chưa được thực hiện');

        $budget_progress     = parent::calc_budget_progress(); //Tính tổng ngân sách tiến độ (phải chạy) cho đến thời điểm kết thúc dịch vụ
        $actual_result         = parent::get_actual_result(); // Tính tổng ngân sách thực tế đã chi
        $contract_days         = parent::calc_contract_days(); // Tính tổng số ngày thực hiện trên hợp đồng

        $expected_day     = round(div($budget_progress, $actual_result) * $contract_days);
        $start_time     = start_of_day(get_term_meta_value($this->contract->term_id, 'advertise_begin_time'));

        return strtotime("+{$expected_day} days -1 day", $start_time);
    }

    /**
     * Tính thời gian dự kiến kết thúc
     *
     * Dự kiến kết thúc = [Tổng NS tiến độ]/[Tổng NS đã chạy]*[Số ngày HD] - 1 + Ngày kích hoạt thực hiện dịch vụ
     *
     * @throws     Exception  Error , stop and return
     *
     * @return     int        Thời gian dự kiến kết thúc dịch vụ dựa theo tiến độ thực tế
     */
    public function calc_payment_expected_end_time()
    {
        parent::is_exist_contract(); // Determines if exist contract.
        if (!is_service_proc($this->contract->term_id)) throw new Exception('Dịch vụ chưa được thực hiện');

        $budget_progress     = parent::calc_actual_budget_progress(); //Tính tổng ngân sách tiến độ (phải chạy) cho đến thời điểm kết thúc dịch vụ
        $actual_result         = parent::get_actual_result(); // Tính tổng ngân sách thực tế đã chi
        $actual_budget_days = parent::calc_actual_budget_days(); // Số ngày thực hiện hợp đồng dựa trên ngân sách thực thu

        $expected_day     = round(div($budget_progress, $actual_result) * $actual_budget_days);
        $start_time     = start_of_day(get_term_meta_value($this->contract->term_id, 'advertise_begin_time'));

        return strtotime("+{$expected_day} days -1 day", $start_time);
    }

    /**
     * Calculates the total Payment On BeHaft amount.
     *
     * @param      <type>     $start_time  The start time
     * @param      <type>     $end_time    The end time
     *
     * @throws     Exception  (description)
     *
     * @return     integer    The total payment amount.
     */
    public function calc_payment_on_behaft_amount($start_time = FALSE, $end_time = FALSE)
    {
        if (!$this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

        $term_id = $this->contract->term_id;

        $this->load->model('contract/receipt_m');

        if (!empty($start_time)) $this->receipt_m->where('posts.end_date >=', $start_time);

        $receipts = $this->receipt_m
            ->select('posts.post_id')
            ->join('term_posts', 'term_posts.post_id = posts.post_id', 'LEFT')
            ->where('term_posts.term_id', $term_id)
            ->where('posts.post_status', 'paid')
            ->where_in('posts.post_type', 'receipt_payment_on_behaft')
            ->order_by('posts.end_date', 'desc')
            ->get_many_by();

        if (empty($receipts)) return 0;

        return array_sum(array_map(function ($x) {
            return (float) get_post_meta_value($x->post_id, 'amount');
        }, $receipts));
    }

    /**
     * Creates invoices.
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function create_invoices()
    {
        parent::is_exist_contract(); // Determines if exist contract.

        $term_id = $this->contract->term_id;

        $contract_begin     = get_term_meta_value($term_id, 'contract_begin');
        $contract_end         = get_term_meta_value($term_id, 'contract_end');
        $num_dates             = diffInDates($contract_begin, $contract_end);

        $start_date     = $contract_begin;

        $number_of_payments = get_term_meta_value($term_id, 'number_of_payments') ?: 1;

        $service_fee         = (int) get_term_meta_value($term_id, 'service_fee'); // Phí dịch vụ
        $sfee_per_payment    = div($service_fee, $number_of_payments); // Phí dịch vụ mỗi đợt thanh toán

        $service_fee_payment_type = get_term_meta_value($term_id, 'service_fee_payment_type');
        if (empty($service_fee_payment_type)) // Khởi tạo giá trị mặc định cho loại thanh toán phí dịch vụ dựa trên file config
        {
            $this->config->load('zaloads/contract');
            $service_fee_payment_type_def     = $this->config->item('service_fee_payment_type');
            $service_fee_payment_type_def     = array_keys($service_fee_payment_type_def);
            $service_fee_payment_type         = reset($service_fee_payment_type_def);
        }

        $apply_discount_type             = get_term_meta_value($term_id, 'apply_discount_type') ?: 'last_invoice';
        $discount_amount                 = $this->calc_disacount_amount();
        $discount_amount_per_payment     = div($discount_amount, $number_of_payments);

        $num_days4inv     = ceil(div($num_dates, $number_of_payments));
        $i                 = 0;
        for ($i; $i < $number_of_payments; $i++) {
            if ($num_days4inv == 0) break;

            /* Khởi tạo đợt thanh toán */
            $end_date = strtotime('+' . $num_days4inv . ' day -1 second', $start_date);
            $end_date = $this->mdate->endOfDay($end_date);

            $insert_data = array(
                'post_title'     => 'Thu tiền đợt ' . ($i + 1),
                'post_content'     => '',
                'start_date'     => $start_date,
                'end_date'         => $end_date,
                'post_type'     => $this->invoice_m->post_type
            );

            $inv_id = $this->invoice_m->insert($insert_data);
            if (empty($inv_id)) continue;

            $this->term_posts_m->set_post_terms($inv_id, $term_id, $this->contract->term_type);

            $invi_insert_data = array();

            /* Khởi tạo với hạng mục trong đợt thanh toán */
            if ($i == 0 && 'full' == $service_fee_payment_type) {
                $invi_insert_data[] = array(
                    'invi_title'     => 'Phí dịch vụ quảng cáo (thanh toán 100% đợt đầu)',
                    'invi_status'     => 'publish',
                    'invi_description' => '',
                    'quantity'     => 1,
                    'invi_rate' => 100,
                    'inv_id'     => $inv_id,
                    'price'     => $service_fee,
                    'total'     => $this->invoice_item_m->calc_total_price($service_fee, 1, 100)
                );

                $sfee_per_payment = 0;
            }

            if (!empty($sfee_per_payment)) {
                $invi_insert_data[] = array(
                    'invi_title'     => 'Phí dịch vụ quảng cáo',
                    'invi_status'     => 'publish',
                    'invi_description' => '',
                    'quantity'     => 1,
                    'invi_rate' => 100,
                    'inv_id'     => $inv_id,
                    'price'     => $sfee_per_payment,
                    'total'     => $this->invoice_item_m->calc_total_price($sfee_per_payment, 1, 100)
                );
            }

            if (!empty($discount_amount)) {
                switch ($apply_discount_type) {
                    case 'last_invoice':
                        if ($i != $number_of_payments - 1) break;
                        $invi_insert_data[] = array(
                            'invi_title'     => 'Chương trình giảm giá',
                            'invi_status'     => 'publish',
                            'invi_description' => '',
                            'quantity'     => 1,
                            'invi_rate' => 100,
                            'inv_id'     => $inv_id,
                            'price'     => $discount_amount,
                            'total'     => $this->invoice_item_m->calc_total_price(-$discount_amount, 1, 100)
                        );
                        break;

                    default:
                        $invi_insert_data[] = array(
                            'invi_title'     => 'Chương trình giảm giá',
                            'invi_status'     => 'publish',
                            'invi_description' => '',
                            'quantity'     => 1,
                            'invi_rate' => 100,
                            'inv_id'     => $inv_id,
                            'price'     => $discount_amount_per_payment,
                            'total'     => $this->invoice_item_m->calc_total_price(-$discount_amount_per_payment, 1, 100)
                        );
                        break;
                }
            }

            foreach ($invi_insert_data as $_invi) {
                $this->invoice_item_m->insert($_invi);
            }

            $start_date = strtotime('+1 second', $end_date);
            $day_end = $num_dates - $num_days4inv;
            if ($day_end < $num_days4inv) $num_days4inv = $day_end;
        }

        update_term_meta($term_id, 'contract_value', $this->calc_contract_value());
        return TRUE;
    }

    /**
     * Sync All amount value of this contract
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function sync_all_amount()
    {
        if (!$this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

        return parent::sync_all_amount();
    }

    /**
     * Đồng bộ các chỉ số bị ảnh thưởng khi có phát sinh chi tiêu
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function sync_all_progress()
    {
        parent::is_exist_contract(); // Determines if exist contract.

        // Tính giá trị đã khách hàng đã chuyển khoản & nhờ thanh toán hộ
        if ('behalf' == get_term_meta_value($this->contract->term_id, 'contract_budget_customer_payment_type'))
            update_term_meta($this->contract->term_id, 'payment_on_behaft_amount', (int) $this->calc_payment_on_behaft_amount());

        $metadata = array();

        try {
            $actual_budget = max($this->calc_actual_budget(), 0);
            update_term_meta($this->contract->term_id, 'actual_budget', $actual_budget);
        } catch (Exception $e) {
        }

        try {
            $expected_end_time = $this->calc_expected_end_time();
            update_term_meta($this->contract->term_id, 'expected_end_time', $expected_end_time);
        } catch (Exception $e) {
        }

        try {
            $real_progress = $this->calc_real_progress();
        } catch (Exception $e) {
        }

        try {
            $payment_expected_end_time = $this->calc_payment_expected_end_time();
            update_term_meta($this->contract->term_id, 'payment_expected_end_time', $payment_expected_end_time);
        } catch (Exception $e) {
        }

        try {
            $real_progress = $this->calc_real_progress_by_payment();
        } catch (Exception $e) {
        }

        return TRUE;
    }
}
/* End of file zaloads_rent_account_behaft_behaviour_m.php */
/* Location: ./application/modules/zaloads/models/zaloads_rent_account_behaft_behaviour_m.php */