<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Zaloads_report_m extends Base_Model
{
    public $recipients = NULL;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->helper('text');

        $models = [
            'log_m',
            'term_users_m',
            'zaloads/zaloads_m',
            'zaloads/zaloads_kpi_m',
            'message/message_m',
            'message/header_m',
        ];

        $this->load->model($models);
        defined('SALE_MANAGER_ID') or define('SALE_MANAGER_ID', 18);
    }

    /**
     * Sends a noticefinish mail.
     *
     * @param      integer  $term_id  The term identifier
     * @param      string   $type_to  The type to
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function send_noticefinish_mail($term_id = 0, $type_to = 'admin')
    {
        $term = $this->zaloads_m->set_term_type()->get($term_id);
        if (empty($term)) return FALSE;

        $this->set_send_from('warning');
        $this->set_mail_to($term_id, $type_to);

        $data           = array();
        $data['term']   = $term;
        $data['title']  = 'Thông báo dự kiến kết thúc dịch vụ ADSPLUS';
        $data['kpis']   = $this->zaloads_kpi_m->order_by('kpi_type')->as_array()->get_many_by(['term_id' => $term_id]);

        $data['contract_code']          = get_term_meta_value($term_id, 'contract_code');
        $data['contract_begin']         = get_term_meta_value($term_id, 'contract_begin');
        $data['contract_end']           = get_term_meta_value($term_id, 'contract_end');
        $data['contract_days']          = $this->zaloads_m->calc_contract_days($term_id);
        $data['advertise_start_time']   = get_term_meta_value($term_id, 'advertise_start_time');
        $data['advertise_end_time']       = strtotime("+{$data['contract_days']} days", $data['advertise_start_time']);
        $data['contract_date']          = my_date($data['advertise_start_time'], 'd/m/Y') . ' - ' . my_date($data['advertise_end_time'], 'd/m/Y');
        $data['contract_budget']        = get_term_meta_value($term->term_id, 'contract_budget');
        $data['actual_result']          = get_term_meta_value($term->term_id, 'actual_result');

        $data['expected_end_time'] = $this->zaloads_m->calc_expected_end_time($term_id);

        $title = '[ADSPLUS.VN] Thông báo dự kiến kết thúc dịch vụ Adsplus của website ' . $term->term_name;
        $this->email->subject($title);

        $data['email_source'] = $title;

        $content = $this->render_content($data, 'zaloads/report/notice_finish');
        $this->email->message($content);


        $is_send = $this->email->send(TRUE);

        if ($is_send) {
            $data_message = array(
                'term_id' => $term_id,
                'msg_title' => $title,
                'msg_content' => $content,
                'msg_type' => 'default',
                'msg_status' => 'sent',
                'metadata' => '',
                'created_on' => time(),
                'hed_type' => 'email'
            );

            $mail_headers = $this->email->headers;

            $recipients = array();
            if (!empty($mail_headers['To'])) {
                $_mails = array_map(function ($m) {
                    return trim($m);
                }, explode(',', $mail_headers['To']));
                $recipients = array_merge($recipients, $_mails);
            }

            $user_id = 0;
            $this->load->model('term_users_m');
            if ($term_user = $this->term_users_m->select('term_id,user_id')->get_by(array('term_id' => $term->term_id))) {
                $user_id = $term_user->user_id;
            }
            $data_message['user_id'] = $user_id;

            $this->create_message($data_message, $recipients);
            $this->email->headers = array();
        }

        return $is_send;
    }


    /**
     * Sends a mail alert 2 admin.
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function send_mail_alert_2admin($term_id = 0)
    {
        $term = $this->zaloads_m->set_term_type()->get($term_id);
        if (empty($term)) return FALSE;

        $this->set_send_from('warning');
        $this->set_mail_to($term_id, 'admin');

        $data                       = array();
        $data['term']               = $term;
        $data['kpis']               = $this->zaloads_kpi_m->order_by('kpi_type')->where('term_id', $term_id)->as_array()->get_many_by();
        $data['contract_code']      = get_term_meta_value($term_id, 'contract_code');
        $data['contract_begin']     = get_term_meta_value($term_id, 'contract_begin');
        $data['contract_end']       = get_term_meta_value($term_id, 'contract_end');
        $data['contract_days']      = $this->zaloads_m->calc_contract_days($term_id);
        $data['start_service_time'] = get_term_meta_value($term_id, 'start_service_time');
        $data['end_service_time']   = strtotime("+{$data['contract_days']} days", $data['start_service_time']);
        $data['contract_date']      = my_date($data['start_service_time'], 'd/m/Y') . ' - ' . my_date($data['end_service_time'], 'd/m/Y');
        $data['contract_budget']    = get_term_meta_value($term->term_id, 'contract_budget');

        $data['progress']           = (new zaloads_m())->set_contract($term)->get_behaviour_m()->get_the_progress();
        $data['actual_result']      = get_term_meta_value($term->term_id, 'actual_result');

        $complete_progress          = currency_numberformat($data['progress'], '%', 2);
        $mail_id                    = 'ID:' . time();
        $data['title']              = "{$term->term_name} [{$complete_progress}] tiến độ cam kết {$mail_id}";

        $content = $this->render_content($data, 'zaloads/report/alert_process');
        $this->email->subject('[CẢNH BÁO][FB] ' . $data['title']);
        $this->email->message($content);

        $result = $this->email->send();

        return $result;
    }

    /**
     * Sends an activation mail 2 customer.
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function send_activation_mail2customer($term_id = 0)
    {
        $term = $this->zaloads_m->get($term_id);
        if (empty($term)) return FALSE;

        $this->set_send_from('reporting');
        $this->set_mail_to($term_id, 'customer');

        $data = array();
        $data['term'] = $term;
        $data['title'] = 'Thông tin kích hoạt dịch vụ';
        $data['kpis'] = $this->zaloads_kpi_m->order_by('kpi_type')->where('term_id', $term_id)->as_array()->get_many_by();

        $title = '[ADSPLUS.VN] Thông báo kích hoạt dịch vụ quảng cáo Zalo của website ' . $term->term_name;
        $data['email_source'] = $title;


        $data['isJoinedContract']   = false;
        $data['balanceBudgetReceived'] = (int) get_term_meta_value($term_id, 'balanceBudgetReceived');
        if (!empty($data['balanceBudgetReceived'])) {
            $data['isJoinedContract']       = true;
            $data['previousContractId']     = (int) get_term_meta_value($term_id, 'previousContractId');
            $data['previousContractCode']   = get_term_meta_value($data['previousContractId'], 'contract_code');
        }

        $content = $this->render_content($data, 'zaloads/report/activation_email');
        $this->email->subject($title);
        $this->email->message($content);

        $result = $this->email->send();
        if (!$result) return $result;

        $data_message = array(
            'term_id' => $term_id,
            'msg_title' => $title,
            'msg_content' => $content,
            'msg_type' => 'default',
            'msg_status' => 'sent',
            'metadata' => '',
            'created_on' => time(),
            'hed_type' => 'email'
        );

        $mail_headers = $this->email->headers;

        $recipients = array();
        if (!empty($mail_headers['To'])) {
            $_mails = array_map(function ($m) {
                return trim($m);
            }, explode(',', $mail_headers['To']));
            $recipients = array_merge($recipients, $_mails);
        }

        $user_id = 0;
        $this->load->model('term_users_m');
        if ($term_user = $this->term_users_m->select('term_id,user_id')->get_by(array('term_id' => $term->term_id))) {
            $user_id = $term_user->user_id;
        }
        $data_message['user_id'] = $user_id;

        $this->create_message($data_message, $recipients);
        $this->email->headers = array();

        return $result;
    }

    /**
     * Sends a finished service.
     *
     * @param      integer  $term_id  The term identifier
     * @param      string   $type_to  The type to
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function send_finished_service($term_id = 0, $type_to = 'admin')
    {
        $term = $this->zaloads_m->set_term_type()->get($term_id);
        if (empty($term)) return FALSE;

        $this->set_send_from('reporting');
        $this->set_mail_to($term_id, $type_to);

        $data = array();
        $data['term'] = $term;
        $data['title'] = 'Thông báo kết thúc dịch vụ';
        $data['kpis'] = $this->zaloads_kpi_m
            ->order_by('kpi_type')
            ->as_array()
            ->get_many_by(['term_id' => $term_id]);

        $title = ($type_to != 'customer') ? '[XEM TRƯỚC]' : '';
        $title .= '[ADSPLUS.VN] Thông báo kết thúc dịch vụ qc Zalo của website ' . $term->term_name;
        $this->email->subject($title);
        $data['email_source'] = $title;

        $data['actual_result'] = (int) get_term_meta_value($term_id, 'actual_result');

        $data['isJoinedContract']   = false;
        $data['balanceBudgetAddTo'] = (int) get_term_meta_value($term_id, 'balanceBudgetAddTo');
        if (!empty($data['balanceBudgetAddTo'])) {
            $data['isJoinedContract']   = true;
            $data['nextContractId']     = (int) get_term_meta_value($term_id, 'nextContractId');
            $data['nextContractCode']   = get_term_meta_value($data['nextContractId'], 'contract_code');
        }

        $content = $this->render_content($data, 'zaloads/report/finish_email');
        $this->email->message($content);

        $result = $this->email->send();

        if ($result && $type_to == 'customer') {
            $data_message = array(
                'term_id' => $term->term_id,
                'msg_title' => $title,
                'msg_content' => $content,
                'msg_type' => 'default',
                'msg_status' => 'sent',
                'metadata' => '',
                'created_on' => time(),
                'hed_type' => 'email'
            );

            $mail_headers = $this->email->headers;

            $recipients = array();
            if (!empty($mail_headers['To'])) {
                $_mails = array_map(function ($m) {
                    return trim($m);
                }, explode(',', $mail_headers['To']));
                $recipients = array_merge($recipients, $_mails);
            }

            $user_id = 0;
            $this->load->model('term_users_m');
            if ($term_user = $this->term_users_m->select('term_id,user_id')->get_by(array('term_id' => $term->term_id))) {
                $user_id = $term_user->user_id;
            }
            $data_message['user_id'] = $user_id;

            $this->create_message($data_message, $recipients);
            $this->email->headers = array();
        }

        $this->email->clear(TRUE);

        return $result;
    }


    // send mail cho kinh doanh, kỹ thuật
    public function send_mail_info_to_admin($term_id)
    {
        $term = $this->term_m->get($term_id);
        if (empty($term)) return FALSE;

        $time = time();

        $contract_code         = get_term_meta_value($term_id, 'contract_code');
        $title                 = 'Hợp đồng quảng cáo Zalo trực tuyến ' . $contract_code . ' đã được khởi tạo.';
        $data                 = array();
        $data['title']         = $title;

        $data['term']        = $term;
        $data['term_id']     = $term_id;
        $data['time']       = $time;


        // Hiển thị thông tin kinh doanh ra ngoài view
        $sale_id             = get_term_meta_value($term_id, 'staff_business');
        $sale                 = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);

        $data['staff']        = $sale;

        // Hiên thị thông tin kỹ thuật phụ trách
        $tech_kpi            = $this->zaloads_kpi_m->select('user_id')
            ->group_by('user_id')
            ->get_many_by(['term_id' => $term_id]);

        if (!empty($tech_kpi)) {
            foreach ($tech_kpi as $i) {
                $user_mail  = $this->admin_m->get_field_by_id($i->user_id, 'user_email');
            }
        }

        $this->set_mail_to($term_id, 'admin');
        $this->email->subject($title);
        $this->email->message($this->load->view('zaloads/admin/send-mail/send_mail_admin', $data, TRUE));

        $send_status                               = $this->email->send();

        if (FALSE === $send_status) return FALSE;

        // Thực hiện ghi log	
        $log_id = $this->log_m->insert(array(
            'log_title'        => 'Thông báo khởi tạo hợp đồng Zalo trực tuyến',
            'log_status'    => 1,
            'term_id'        => $term_id,
            'user_id'        => $this->admin_m->id,
            'log_content'    => 'Đã gửi mail thành công',
            'log_type'        => 'zaloads-report-email',
            'log_time_create' => date('Y-m-d H:i:s'),
        ));

        return TRUE;
    }

    protected function render_content($data = array(), $content_view, $layout = '')
    {
        if (!is_array($data))
            $data = array($data);

        if (empty($content_view)) return FALSE;
        if (empty($layout)) $layout = 'email/yellow_tpl';

        $data['content'] = $this->load->view($content_view, $data, TRUE);
        $content = $this->load->view($layout, $data, TRUE);
        if (empty($content)) return FALSE;
        return $content;
    }

    public function create_message($data = array(), $recipients = array())
    {
        if (empty($data)) return FALSE;

        $default = array(
            'term_id' => 0,
            'msg_title' => '',
            'msg_content' => '',
            'msg_type' => 'default',
            'msg_status' => 'sent',
            'metadata' => '',
            'created_on' => time()
        );

        $this->load->model('message/message_m');
        $message = array_merge($default, $data);
        $headers = array();

        $term_id = $message['term_id'] ?? 0;

        $hed_type = 'email';
        if (isset($message['hed_type'])) {
            $hed_type = $message['hed_type'];
            unset($message['hed_type']);
        }

        $user_id = 0;
        if (isset($message['user_id'])) {
            $user_id = $message['user_id'];
            unset($message['user_id']);
        }

        foreach ($recipients as $recipient) {
            $headers[] = array(
                'hed_type' => $hed_type,
                'hed_subject' => $message['msg_title'] ?? '',
                'hed_status' => 0,
                'hed_to' => $recipient,
                'user_id' => $user_id,
                'term_id' => $term_id,
                'msg_id' => 0
            );
        }

        return $this->message_m->create($message, $headers);
    }

    /**
     * Sends daily sms.
     *
     * @param      integer  $term_id    The term identifier
     * @param      <type>   $fake_send  The fake send
     *
     * @return     array    ( description_of_the_return_value )
     */
    public function send_daily_sms($term_id = 0, $fake_send = TRUE)
    {
        $term = $this->term_m->get($term_id);
        if (empty($term)) {
            trigger_error("Hợp đồng #{$term_id} không tồn tại");
            return FALSE;
        }

        $mcm_account_id = get_term_meta_value($term_id, 'mcm_client_id');
        if (empty($mcm_account_id)) {
            trigger_error('Client-customer-id không tồn tại');
            return FALSE;
        }

        $curators = get_term_meta_value($term_id, 'contract_curators');
        $curators = @unserialize($curators);
        if (empty($curators)) {
            trigger_error('Thông tin người nhận SMS không tồn tại');
            return FALSE;
        }

        $updated_on = (int) get_term_meta_value($term_id, 'account_performance_report_download_next_time');
        if ($updated_on < time()) {
            trigger_error('download_performance_daily FAILURE');
            return FALSE;
        }

        $this->load->model('base_adwords_m');
        $this->base_adwords_m->set_mcm_account($mcm_account_id, FALSE);

        $this->load->model('adword_calculation_m');
        $this->adword_calculation_m
            ->set_base_adword_model($this->base_adwords_m)
            ->set_report_type('ACCOUNT_PERFORMANCE_REPORT');

        $yesterday = $this->mdate->startOfDay(strtotime('yesterday'));
        $clicks = $this->adword_calculation_m->calc_clicks($yesterday);
        $invalidClicks = $this->adword_calculation_m->calc_invalidclicks($yesterday);

        if (empty($clicks)) {
            trigger_error("Không có dữ liệu clicks cho #{$term_id} ngày " . my_date($yesterday));
            return FALSE;
        }

        /* adding meta for check term has clicks in current month */
        $activated_until_time = get_term_meta_value($term_id, 'activated_until_time');
        $start_of_month = $this->mdate->startOfMonth();
        $end_of_month = $this->mdate->endOfMonth();
        if (empty($activated_until_time) || ((int) $activated_until_time < $start_of_month)) {
            update_term_meta($term_id, 'activated_until_time', $end_of_month);
        }

        $sms_template = 'KQ:{date} - {website}: click: {clicks}, click ko hop le: {invalidclicks}.{extra}';

        $smstpl = get_term_meta_value($term_id, 'sms_template');
        if (!empty($smstpl)) {
            $smstpl = html_entity_decode($smstpl);
            $smstpl = strip_tags($smstpl);
            $sms_template = $smstpl;
        }

        $extra_data = 'Thong tin chi tiet xin lh: 0972899723 (Mr.Hai)';

        $kpi = $this->zaloads_kpi_m->order_by('kpi_type')->where('term_id', $term_id)->get_by();
        if (!empty($kpi)) {
            $kpi_info = array();

            $admin = $this->admin_m
                ->set_get_admin()
                ->limit(1)
                ->get($kpi->user_id);

            if (!empty($admin)) {
                $this->load->helper('text');
                if (!empty($admin->display_name)) {
                    $display_name   = explode(' ', $admin->display_name);
                    $last_name      = ucfirst(convert_accented_characters(end($display_name)));

                    $gender     = get_user_meta_value($kpi->user_id, 'gender');
                    $gender_f   = !empty($gender) ? 'Mr.' : 'Ms.';

                    $kpi_info['name']   = "{$gender_f}{$last_name}";
                }

                if (!empty($admin->user_email)) {
                    $kpi_info['mail'] = $admin->user_email;
                }

                if ($tech_phone = $this->usermeta_m->get_meta_value($kpi->user_id, 'user_phone')) {
                    $kpi_info['phone'] = $tech_phone;
                }

                $kpi_info = implode(', ', $kpi_info);
                if (!empty($kpi_info)) {
                    $extra_data = 'Thong tin vui long LH-Ky Thuat: ' . $kpi_info;
                }
            }
        }

        $sms_log = array();

        $this->load->library('sms');
        foreach ($curators as $curator) {
            $result = array();
            $phone = preg_replace('/\s+/', '', $curator['phone']);
            if (empty($phone))
                continue;

            $search_replace = array(
                '{clicks}' => $clicks,
                '{invalidclicks}' => $invalidClicks,
                '{website}' => $term->term_name,
                '{ten_khach_hang}' => $curator['name'],
                '{ten_nguoi_nhan_sms}' => $curator['name'],
                '{date}' => my_date($yesterday, 'd/m'),
                '{extra}' => $extra_data
            );

            $message = str_replace(array_keys($search_replace), array_values($search_replace), $sms_template);

            $result = false;
            if ($fake_send) {
                $result = $this->sms->fake_send($phone, $message, 0);
                trigger_error('Fake message has sent');
                continue;
            }

            $result = $this->sms->send($phone, $message);

            if (!empty($result)) {
                $result = array_shift($result);
                unset($result['config']);
            }

            if (is_array($result)) {
                $result['customer_name'] = $curator['name'];
                $result['day'] = my_date($yesterday, 'd/m');
            }

            $sms_log[] = $result;
        }

        if (!empty($sms_log)) {
            $content = '';
            $day = '';
            $recipients = array();
            foreach ($sms_log as $row) {
                $content = $content ?: $row['message'];
                $day = $day ?: $row['day'];
                $recipients[] = $row['recipient'];
            }

            if (!empty($recipients)) {
                $title = 'Thống kê lưu lượng click ngày ' . $day;
                $data_message = array(
                    'term_id' => $term_id,
                    'msg_title' => $title,
                    'msg_content' => $content,
                    'msg_type' => 'default',
                    'msg_status' => 'sent',
                    'metadata' => '',
                    'created_on' => time(),
                    'hed_type' => 'sms'
                );

                $user_id = 0;
                $this->load->model('term_users_m');
                if ($term_user = $this->term_users_m->select('term_id,user_id')->get_by(array('term_id' => $term->term_id))) {
                    $user_id = $term_user->user_id;
                }
                $data_message['user_id'] = $user_id;

                $this->create_message($data_message, $recipients);
            }
        }

        return $sms_log;
    }


    /**
     * Send zaloads sms by date time with campaigns insight data
     *
     * @param      integer  $term_id    The term identifier
     * @param      mixed   $datetime   The datetime
     * @param      bool     $fake_send  The fake send
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    function send_sms($term_id = 0, $datetime = 'yesterday', $fake_send = TRUE)
    {
        $term = $this->zaloads_m->set_term_type()->get($term_id);
        if (empty($term)) return FALSE;

        $this->zaloads_m->set_contract($term);
        $data = $this->zaloads_m->get_accounts([
            'start_time' => start_of_day($datetime),
            'end_time' => end_of_day($datetime)
        ]);

        if (empty($data)) {
            trigger_error("#{$term_id} Data Insight Not Found.");
            return FALSE;
        }

        $data = reset($data);

        // Data for sms content
        $date           = my_date($datetime, 'd/m');
        $spend_text     = currency_numberformat($data['spend'] ?? 0, 'VND');
        $result_text    = currency_numberformat($data['result'], '');

        $curators       = get_term_meta_value($term_id, 'contract_curators');
        $curators       = @unserialize($curators);

        if (empty($curators)) {
            trigger_error("#{$term_id} Thông tin người nhận SMS không tồn tại");
            return FALSE;
        }

        $message = "Zalo Ads:{$date} - Ket qua:{$result_text} (tuong tac,like page,dang ky, xem video ..), Chi phi: {$spend_text}.";
        if ($kpi = $this->zaloads_kpi_m->order_by('kpi_type')->get_by(['term_id' => $term_id])) {
            if ($admin = $this->admin_m->set_get_admin()->get($kpi->user_id)) {
                $tech_email = $admin->user_email;
                $tech_phone = get_user_meta_value($kpi->user_id, 'user_phone');
                $tech_name  = '';
                if (!empty($admin->display_name)) {
                    $gender     = get_user_meta_value($kpi->user_id, 'gender');
                    $gender_f   = !empty($gender) ? 'Mr.' : 'Ms.';

                    $display_name   = explode(' ', $admin->display_name);
                    $last_name      = ucfirst(convert_accented_characters(end($display_name)));
                    $tech_name      = "{$gender_f}{$last_name}";
                }


                $message .= "LH-Ky Thuat: {$tech_name} {$tech_phone}";
            }
        }

        $this->load->library('sms');

        $recipients = array_filter(array_map(function ($x) {
            return  preg_replace('/\s+/', '', $x['phone']);
        }, $curators));
        $result     = $fake_send ? $this->sms->fake_send($recipients, $message, 0) : $this->sms->send($recipients, $message);

        // Log send email system
        if (empty($result)) return FALSE;

        if ($term_user = $this->term_users_m->select('term_id,user_id')->get_by(array('term_id' => $term->term_id))) {
            $customer_id = $term_user->user_id;
        }

        $message_headers = [];
        foreach ($result as $item) {
            $message_headers[] = array(
                'hed_type' => 'sms',
                'hed_subject' => "Thống kê kết quả quảng cáo ngày {$date}",
                'hed_status' => 0,
                'hed_to' => $item['recipient'],
                'user_id' => $customer_id,
                'term_id' => $term_id
            );

            $log_content = $item;
            unset($log_content['config']);

            $this->log_m->insert(array(
                'log_type' => 'zaloads_report_sms',
                'log_status' => ($item['response']->SendSMSResult == 0 ? 1 : 0),
                'term_id' => $term_id,
                'user_id' => $customer_id,
                'log_content' => serialize($log_content),
                'log_time_create' => my_date(0, 'Y-m-d H:i:s'),
                'log_title' => "Thống kê kết quả quảng cáo ngày {$date}"
            ));
        }

        if (empty($message_headers)) {
            trigger_error("#{$term_id} SMS not sent success");
            return FALSE;
        }

        $message_data = array(
            'term_id' => $term_id,
            'msg_title' => "Thống kê kết quả quảng cáo ngày {$date}",
            'msg_content' => $message
        );

        return $this->message_m->create($message_data, $message_headers);
    }


    /**
     * Sends a report email.
     *
     * @param      integer|string  $term_id     The term identifier
     * @param      integer         $start_time  The start time
     * @param      integer         $end_time    The end time
     * @param      string          $type_to     The type to
     *
     * @return     <type>          ( description_of_the_return_value )
     */
    public function send_report_email($term_id = 0, $start_time = 0, $end_time = 0, $type_to = 'admin')
    {
        $term = $this->zaloads_m->set_term_type()->get($term_id);
        if (empty($term)) return FALSE;

        $adaccount_id = (int) get_term_meta_value($term_id, 'adaccount_id');
        if (empty($adaccount_id)) {
            trigger_error("#{$term_id} AdAccount Id not found.");
            return FALSE;
        }

        $this->load->library('excel');
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array('memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
            ->getProperties()
            ->setCreator("ADSPLUS.VN")
            ->setLastModifiedBy("ADSPLUS.VN")
            ->setTitle("Báo cáo chiến dịch quảng cáo Zalo ads");

        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/report/report-Zalo-template.xlsx');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $this->zaloads_m->set_contract($term);
        $data = $this->zaloads_m->get_accounts([
            'start_time' => start_of_day($start_time),
            'end_time' => end_of_day($end_time)
        ]);
        if (empty($data)) {
            trigger_error("#{$term_id} AdAccount data not found.");
            return FALSE;
        }

        $row_number = 6;
        foreach ($data as $key => $adcampaign_id) {
            $adcampaign_term = $this->adcampaign_m->set_term_type()->get($adcampaign_id);
            if (empty($adcampaign_term)) continue;

            $row_number++;
            $currency = $item['account_currency'] ?? 'VND';

            // Set Cell
            $objWorksheet->setCellValueByColumnAndRow(0, $row_number, my_date($start_time));
            $objWorksheet->setCellValueByColumnAndRow(1, $row_number, my_date($end_time));
            $objWorksheet->setCellValueByColumnAndRow(2, $row_number, $item['account_name'] ?? '--');
            $objWorksheet->setCellValueByColumnAndRow(3, $row_number, $item['impressions'] ?? '--');
            $objWorksheet->setCellValueByColumnAndRow(4, $row_number, $item['reach'] ?? '--');
            $objWorksheet->setCellValueByColumnAndRow(5, $row_number, $item['result'] ?? '--');
            $objWorksheet->setCellValueByColumnAndRow(6, $row_number, $item['comment'] ?? '--');
            $objWorksheet->setCellValueByColumnAndRow(7, $row_number, $item['like'] ?? '--');
            $objWorksheet->setCellValueByColumnAndRow(8, $row_number, $item['share'] ?? '--');
            $objWorksheet->setCellValueByColumnAndRow(9, $row_number, $item['clicks'] ?? '--');
            $objWorksheet->setCellValueByColumnAndRow(10, $row_number, currency_numberformat($data['cpc'] ?? 0, " {$currency}"));
            $objWorksheet->setCellValueByColumnAndRow(11, $row_number, currency_numberformat($data['ctr'] ?? 0, " {$currency}"));
            $objWorksheet->setCellValueByColumnAndRow(12, $row_number, currency_numberformat($data['cpp'] ?? 0, " {$currency}"));
            $objWorksheet->setCellValueByColumnAndRow(13, $row_number, currency_numberformat($data['spend'] ?? 0, " {$currency}"));
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $folder_upload = "files/zaloads/{$term_id}";
        if (!is_dir($folder_upload)) {
            try {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $ext = 'xlsx';
        $file_name = my_date($start_time, 'Y-m-d') . '_to_' . my_date($end_time, 'Y-m-d') . '_created_on_' . my_date(0, 'Y-m-d-H-i-s') . '.' . $ext;
        $path_to_file = "{$folder_upload}/{$file_name}";

        try {
            $objWriter->save($path_to_file);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->set_mail_to($term_id, $type_to);
        // $this->set_send_from('reporting');

        $data = array();
        $data['direct_downlink'] = [anchor(base_url("report/zaloads/download/{$term_id}/{$file_name}"), ' Báo cáo tuần')];
        $data['term'] = $term;
        $data['start_time'] = $start_time;
        $data['end_time'] = $end_time;
        $data['title'] = '[ADSPLUS.VN] Báo cáo quảng cáo tuần';
        $data['kpis'] = $this->zaloads_kpi_m->order_by('kpi_type')->where('term_id', $term_id)->as_array()->get_many_by();

        $mail_id = 'ID:' . time();
        $title = "[ADSPLUS.VN] Báo cáo quảng cáo tuần website {$term->term_name} [{$mail_id}]";
        $this->email->subject($title);

        $data['email_source'] = $title;

        $content = $this->render_content($data, 'zaloads/report/weekly');
        $this->email->message($content);

        $this->email->attach($path_to_file, 'attachment', $file_name);

        $result = $this->email->send();
        $this->email->clear(TRUE);

        $log_content = array();
        $log_content['attachment'] = $path_to_file;

        $recipients = $this->recipients;
        if (!empty($recipients)) {
            $log_content['recipients'] = $recipients;
        }

        $log_content = serialize($log_content);
        $time_send = time();
        $is_sent = (int) !empty($result);
        $log = array(
            'user_id' => 0,
            'term_id' => $term->term_id,
            'log_type' => 'zaloads_report_email',
            'log_title' => $title,
            'log_status' => $is_sent,
            'log_content' => $log_content,
            'log_time_create' => my_date($time_send, 'Y-m-d H:i:s')
        );

        $this->log_m->insert($log);

        if ($is_sent) {
            $data_message = array(
                'term_id' => $term->term_id,
                'msg_title' => $title,
                'msg_content' => $content,
                'msg_type' => 'default',
                'msg_status' => 'sent',
                'metadata' => '',
                'created_on' => $time_send,
                'hed_type' => 'email'
            );

            $mail_headers = $this->email->headers;

            $recipients = array();
            if (!empty($mail_headers['To'])) {
                $_mails = array_map(function ($m) {
                    return trim($m);
                }, explode(',', $mail_headers['To']));
                $recipients = array_merge($recipients, $_mails);
            }

            $user_id = 0;
            $this->load->model('term_users_m');
            if ($term_user = $this->term_users_m->select('term_id,user_id')->get_by(array('term_id' => $term->term_id))) {
                $user_id = $term_user->user_id;
            }
            $data_message['user_id'] = $user_id;

            $this->create_message($data_message, $recipients);
            $this->email->headers = array();
        }

        return $result;
    }


    /**
     * Sets the mail to.
     *
     * @param      integer  $term_id  The term identifier
     * @param      string   $type_to  The type to [admin|customer|mailreport|specified]
     *
     * @return     self     ( description_of_the_return_value )
     */
    public function set_mail_to($term_id = 0, $type_to = 'admin')
    {
        $this->email->clear(TRUE);

        $recipients = array(
            'mail_to' => array(),
            'mail_cc' => array(''),
            'mail_bcc' => array('thonh@webdoctor.vn')
        );

        if (is_array($type_to) && !empty($type_to)) {
            $recipients = array_merge_recursive($type_to, $recipients);
            $type_to = $type_to['type_to'] ?? 'specified';
        }

        extract($recipients);

        $staff_mail = '';
        $tech_mail = array();

        if (!empty($term_id)) {
            $this->load->model('staffs/admin_m');
            $staff_id = get_term_meta_value($term_id, 'staff_business');
            $staff_mail = $this->admin_m->get_field_by_id($staff_id, 'user_email');

            $kpis = $this->zaloads_kpi_m
                ->select('user_id')
                ->where('term_id', $term_id)
                ->group_by('user_id')
                ->get_many_by();
            if (!empty($kpis)) {
                foreach ($kpis as $kpi) {
                    $mail = $this->admin_m->get_field_by_id($kpi->user_id, 'user_email');
                    if (empty($mail)) continue;
                    $tech_mail[] = $mail;
                }
            }
        }

        if ($type_to == 'admin') {
            if (!empty($tech_mail)) {
                foreach ($tech_mail as $mail) {
                    $mail_to[] = $mail;
                }
            }

            if (!empty($staff_mail)) {
                $mail_cc[] = $staff_mail;
            }

            $mail_cc[] = 'thonh@webdoctor.vn';
        } else if ($type_to == 'customer') {
            $curators = get_term_meta_value($term_id, 'contract_curators');
            $curators = @unserialize($curators);
            if (!empty($curators)) {
                foreach ($curators as $item) {
                    $email = trim($item['email']);
                    if (empty($email)) continue;
                    $mail_to[] = $email;
                }
            }

            $mail_cc[] = 'thonh@webdoctor.vn';
            if (!empty($staff_mail)) $mail_cc[] = $staff_mail;
            if (!empty($tech_mail))
                foreach ($tech_mail as $mail)
                    $mail_cc[] = $mail;
        } else if ($type_to == 'mailreport') {
            $mail_to = array('thonh@webdoctor.vn');
        }

        $this->recipients = $mail_to;

        $this->email
            ->from('support@adsplus.vn', 'Adsplus.vn')
            ->to($mail_to)
            ->cc($mail_cc)
            ->bcc($mail_bcc);

        return $this;
    }


    /**
     * Sets the send from.
     *
     * @param      <type>  $group_name  The group name
     */
    public function set_send_from($group_name = FALSE)
    {
        if ($group_name == FALSE) return FALSE;

        $this->config->load('googleads/email');
        $group_name     = strtolower(trim($group_name));
        $conf_emails    = $this->config->item($group_name, 'email-groups');
        if (empty($conf_emails)) return FALSE;

        $rand_key = array_rand($conf_emails);
        $this->email->initialize($conf_emails[$rand_key]);

        return TRUE;
    }


    /**
     * Sends a statistical cronmail 2 admin.
     *
     * @param      boolean  $start_time  The start time
     * @param      boolean  $end_time    The end time
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function send_statistical_cronmail2admin($start_time = FALSE, $end_time = FALSE)
    {
        $start_time = $start_time ?: time();
        $start_date = $this->mdate->startOfDay($start_time, FALSE);
        $end_time   = $end_time ?: $start_time;
        $end_date   = $this->mdate->endOfDay($end_time, FALSE);

        $logs       = $this->log_m->order_by('log_id', 'desc')
            ->where_in('log_type', ['zaloads_report_sms', 'zaloads_report_email'])
            ->get_many_by(['log_status' => 1, 'log_time_create >=' => $start_date, 'log_time_create <=' => $end_date]);

        if (empty($logs)) return FALSE;

        $terms = $this->term_m->select('term_id,term_name')->where_in('term_id', array_column($logs, 'term_id'))->get_many_by();
        $terms_key_val = key_value($terms, 'term_id', 'term_name');

        $group_logs = array_group_by($logs, 'log_type');

        // Send datatable for week email report
        if (!empty($group_logs['zaloads_report_email'])) {
            /* GENERATE BODY MAIL */
            $this->table->set_caption('Thống kê gửi EMAIL tự động');

            $template = $this->config->item('mail_template');
            $template['table_open'] = '<table  border="0" cellpadding="4" cellspacing="0" style="font-size:12px; font-family:Arial" width="100%"><tr><th colspan="5" style="background: #138474 none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">';

            $this->table->set_template($template);
            $this->table->set_heading('STT', 'Fanpage', 'Người nhận', 'Tình trạng', 'File báo cáo');

            $i = 0;
            foreach ($group_logs['zaloads_report_email'] as $log) {
                $status_lbl = $log->log_status ? 'Ok' : 'Error';

                $log_content = unserialize($log->log_content);
                if (empty($log_content)) continue;

                $attachments = anchor(base_url($log_content['attachment']), 'email đính kèm');

                $mail_to = '';
                if (!empty($log_content['recipients'])) {
                    $recipients = $log_content['recipients'];
                    $mail_to = is_array($recipients) ? implode(',', $recipients) : $recipients;
                }

                $anchor_link = base_url($log_content['attachment']);
                $file_download_link = anchor($anchor_link, 'Tải xuống');
                $this->table->add_row(++$i, $terms_key_val[$log->term_id], $mail_to, $status_lbl, $attachments);
            }

            $email_datatable = $this->table->generate();
        }

        // Send datatable for SMS report
        if (!empty($group_logs['zaloads_report_sms'])) {
            /* GENERATE BODY MAIL */
            $this->load->library('sms');
            $this->table->set_caption('Thống kê gửi SMS tự động');
            $template['table_open'] = '<table  border="0" cellpadding="4" cellspacing="0" style="font-size:12px; font-family:Arial" width="100%"><tr><th colspan="6" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">';

            $this->table->set_template($template);
            $this->table->set_heading('STT', 'Fanpage', 'Người nhận', 'Tình trạng', 'Nội dung');

            $i = 0;
            foreach ($group_logs['zaloads_report_sms'] as $log) {
                $log_content = @unserialize($log->log_content);
                if (empty($log_content)) continue;

                $this->table->add_row(++$i, $terms_key_val[$log->term_id], $log_content['recipient'], $this->sms->get_status_label($log_content['response']->SendSMSResult), $log_content['message']);
            }

            $sms_datatable = $this->table->generate();
        }

        // Prepare Email reporting
        $this->set_send_from('reporting');
        $this->set_mail_to(0, 'mailreport');

        $title = '[Adsplus.vn][Zalo] thống kê báo cáo tự động ngày ' . my_date(time(), 'd-m-Y') . ' [ID:' . time() . ']';
        $content = ($email_datatable ?? '') . ($sms_datatable ?? '');

        $this->email->subject($title);
        $this->email->message($content);
        $result = $this->email->send();
        return $result;
    }
}
/* End of file Zaloads_report_m.php */
/* Location: ./application/modules/zaloads/models/Zaloads_report_m.php */