<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'modules/zaloads/models/Zaloads_behaviour_m.php');

class Zaloads_rent_account_behaviour_m extends Zaloads_behaviour_m
{


    /**
     * Render Pritable contract
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     Array   Data result for Preview Action
     */
    public function prepare_preview($term_id = 0)
    {
        $data = $this->data;

        $data['data_service'] = array(
            'service_name'     => 'Cung cấp tài khoản',
            'budget'        => (int) get_term_meta_value($this->contract->term_id, 'contract_budget'),
            'currency'         => get_term_meta_value($this->contract->term_id, 'currency'),
            'service_fee'     => (int) get_term_meta_value($this->contract->term_id, 'service_fee')
        );

        $discount_amount     = (float) get_term_meta_value($this->contract->term_id, 'discount_amount');
        $promotions         = get_term_meta_value($this->contract->term_id, 'promotions') ?: [];

        if (!empty($discount_amount) && !empty($promotions)) {
            $promotions = unserialize($promotions);

            $data['data_service']['discount_amount'] = $discount_amount;
            $data['data_service']['promotions'] = array_map(function ($promotion) use ($data) {

                $promotion['text']     = "CTKM - {$promotion['name']}";
                $_value     = $promotion['value'];

                if ('percentage' == $promotion['unit']) {
                    $_value = $data['data_service']['service_fee'] * div($promotion['value'], 100);
                    $promotion['text'] .= "({$promotion['value']}% phí dịch vụ)";
                }

                $promotion['amount'] = $_value;
                return $promotion;
            }, $promotions);
        }

        $data['contract_budget_customer_payment_type'] = (string) get_term_meta_value($this->contract->term_id, 'contract_budget_customer_payment_type');
        $data['deposit_amount'] = (int) get_term_meta_value($this->contract->term_id, 'deposit_amount');
        $data['has_deposit']     = !empty($data['deposit_amount']);

        $data['view_file'] = 'zaloads/contract/preview/rent_account';
        $data['has_deposit'] and $data['view_file'] = 'zaloads/contract/preview/rent_account_with_deposit';
        return $data;
    }
}
/* End of file Account_behaviour_m.php */
/* Location: ./application/modules/zaloads/models/Account_behaviour_m.php */