<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$columns = array(
    'id' => array(
        'name' => 'id',
        'label' => '#ID',
        'set_select' => FALSE,
        'title' => '#ID',
        'type' => 'string',
    ),

    'website' => array(
        'name' => 'website',
        'label' => 'URL Fanpage',
        'set_select' => FALSE,
        'title' => 'URL Fanpage',
        'type' => 'string',
    ),

    'status' => array(
        'name' => 'status',
        'label' => 'Trạng thái dịch vụ',
        'set_select' => FALSE,
        'title' => 'T.Thái',
        'type' => 'string',
    ),

    'contract_code' => array(
        'name' => 'contract_code',
        'label' => 'Mã Hợp đồng',
        'set_select' => FALSE,
        'title' => 'Mã Hợp đồng',
        'set_order' => TRUE,
        'type' => 'string',
    ),

    'start_service_time' => array(
        'name' => 'start_service_time',
        'label' => 'Ngày kích hoạt dịch vụ',
        'set_select' => FALSE,
        'title' => 'Ng.Kích hoạt',
        'type' => 'timestamp',
        'set_order' => TRUE
    ),

    'staff_business' => array(
        'name' => 'staff_business',
        'label' => 'Kinh doanh phụ trách',
        'set_select' => FALSE,
        'title' => 'NVKD',
        'type' => 'string',
        'set_order' => TRUE
    ),

    'actual_result' => array(
        'name' => 'actual_result',
        'label' => 'Đã dùng',
        'set_select' => FALSE,
        'title' => 'Đã dùng',
        'type' => 'number',
        'set_order' => TRUE
    ),

    'actual_budget' => array(
        'name' => 'actual_budget',
        'label' => 'Ngân sách (đã thu)',
        'set_select' => FALSE,
        'title' => 'Ngân sách<br><small><u><i>Đã thu</i></u></small>',
        'type' => 'number',
        'set_order' => TRUE
    ),

    'payment_expected_end_time' => array(
        'name' => 'payment_expected_end_time',
        'label' => 'Dự kiến (NS thực)',
        'set_select' => FALSE,
        'title' => 'Dự kiến<br><small><u><i>(NS thực)</i></u></small>',
        'type' => 'number',
        'set_order' => TRUE
    ),

    'payment_real_progress' => array(
        'name' => 'payment_real_progress',
        'label' => 'Tiến độ (NS thực)',
        'set_select' => FALSE,
        'title' => 'Tiến độ<br/><small><u><i>(NS thực)</i></u></small>',
        'type' => 'number',
        'set_order' => TRUE
    ),

    'payment_cost_per_day_left' => array(
        'name' => 'payment_cost_per_day_left',
        'label' => 'est.NS/Ngày (NS thực)',
        'set_select' => FALSE,
        'title' => 'est.NS/Ngày <br><small><u><i>(NS thực)</i></u></small>',
        'type' => 'number',
        'set_order' => FALSE
    ),

    'actual_progress_percent_net' => array(
        'name' => 'actual_progress_percent_net',
        'label' => '% H.Thành (NS thực)',
        'set_select' => FALSE,
        'title' => 'H.Thành (%)<br><small><u><i>(NS thực)</i></u></small>',
        'type' => 'number',
        'set_order' => TRUE
    ),

    'contract_budget' => array(
        'name' => 'contract_budget',
        'label' => 'Ngân sách (NS HĐ)',
        'set_select' => FALSE,
        'title' => 'Ngân sách<br><small><u><i>(NS HĐ)</i></u></small>',
        'type' => 'number',
        'set_order' => TRUE
    ),

    'expected_end_time' => array(
        'name' => 'expected_end_time',
        'label' => 'Dự kiến (NS HĐ)',
        'set_select' => FALSE,
        'title' => 'Dự kiến<br><small><u><i>(NS HĐ)</i></u></small>',
        'type' => 'number',
        'set_order' => TRUE
    ),

    'real_progress' => array(
        'name' => 'real_progress',
        'label' => 'Tiến độ (NS HĐ)',
        'set_select' => FALSE,
        'title' => 'Tiến độ<br/><small><u><i>(NS HĐ)</i></u></small>',
        'type' => 'number',
        'set_order' => TRUE
    ),

    'cost_per_day_left' => array(
        'name' => 'cost_per_day_left',
        'label' => 'est.NS/Ngày (NS HĐ)',
        'set_select' => FALSE,
        'title' => 'est.NS/Ngày <br><small><u><i>(NS HĐ)</i></u></small>',
        'type' => 'number',
        'set_order' => FALSE
    ),

    'actual_progress_percent' => array(
        'name' => 'actual_progress_percent',
        'label' => '% H.Thành (NS HĐ)',
        'set_select' => FALSE,
        'title' => 'H.Thành (%)<br><small><u><i>(NS HĐ)</i></u></small>',
        'type' => 'number',
        'set_order' => TRUE
    ),

    'service_fee' => array(
        'name' => 'service_fee',
        'label' => 'Phí dịch vụ',
        'set_select' => FALSE,
        'title' => 'Phí dịch vụ',
        'type' => 'number',
        'set_order' => TRUE
    ),

    'vat' => array(
        'name' => 'vat',
        'label' => 'VAT',
        'set_select' => FALSE,
        'title' => 'VAT',
        'type' => 'number',
        'set_order' => TRUE
    ),

    'result_updated_on' => array(
        'name' => 'result_updated_on',
        'label' => 'Thời gian cập nhật dữ liệu mới',
        'set_select' => FALSE,
        'title' => 'Cập nhật lúc',
        'type' => 'timestamp',
        'set_order' => TRUE
    ),

    'result_pending_days' => [
        'name' => 'result_pending_days',
        'title' => 'Số ngày không phát sinh chi phí',
        'type' => 'number'
    ]
);

$config['datasource'] = array(
    'columns' => $columns,
    'default_columns' => array(
        'contract_code',
        'customer_code',
        'actual_result',
        'actual_budget',
        'payment_expected_end_time',
        'payment_real_progress',
        'payment_cost_per_day_left',
        'actual_progress_percent_net',
        'expected_end_time',
        'real_progress',
        'cost_per_day_left',
        'actual_progress_percent',
        'result_updated_on',
        'staff_business',
        'status',
    )
);
unset($columns);
