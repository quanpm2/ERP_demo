<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$config['packages'] = array('service' => array(), 'default' => '');
$config['zaloads'] = array(
    'status' => array('pending' => 'Ngưng hoạt động', 'publish' => 'Đang hoạt động'),
    'max_input_curators' => 2,
);
