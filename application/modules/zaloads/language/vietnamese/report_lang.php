<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Heading labels */
$lang['date_start'] = 'Bắt đầu báo cáo';
$lang['date_stop'] = 'Kết thúc báo cáo';
$lang['campaign_name'] = 'Chiến dịch quảng cáo';
$lang['objective'] = 'Chỉ báo kết quả';
$lang['spend'] = 'Số tiền đã chi tiêu';
$lang['cost_per_result'] = 'Chi phí trên mỗi kết quả';
$lang['cpm'] = 'CPM (Chi phí trên mỗi 1.000 lần hiển thị)';
$lang['cpc'] = 'CPC (Tất cả)';
$lang['clicks'] = 'Số lần nhấp vào liên kết';
$lang['impressions'] = 'Lượt xem';
$lang['reach'] = 'Số người tiếp cận được';

/* Objective language labels*/
$lang['VIDEO_VIEWS'] = 'Số lượt xem video';
$lang['POST_ENGAGEMENT'] = 'Tương tác với bài viết';
$lang['LOCAL_AWARENESS'] = 'Nhận thức tại địa phương';
$lang['CONVERSIONS'] = 'Chuyển đổi';
$lang['PAGE_LIKES'] = 'Số lượt thích Trang';
$lang['LEAD_GENERATION'] = 'Tìm kiếm khách hàng tiềm năng';
$lang['EVENT_RESPONSES'] = 'Phản hồi sự kiện';
$lang['OFFER_CLAIMS'] = 'Số lần nhận ưu đãi';
$lang['LINK_CLICKS'] = 'Lưu lượng truy cập';
$lang['REACH'] = 'REACH';
$lang['APP_INSTALLS'] = 'Actions::APP_INSTALLS';
$lang['BRAND_AWARENESS'] = 'Actions::BRAND_AWARENESS';
$lang['PRODUCT_CATALOG_SALES'] = 'Actions::PRODUCT_CATALOG_SALES';