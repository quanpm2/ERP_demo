<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');

$version            = 20211117171533;
$is_service_proc    = is_service_proc($term);
$is_service_end     = is_service_end($term);

$has_start_permission   = has_permission('zaloads.start_service');
$has_stop_permission    = has_permission('zaloads.stop_service');

# MAIN COMMAND : START SERVICE | STOP SERVICE
if($has_start_permission || $has_stop_permission)
{
    $this->admin_form->set_col(8,6);
    echo $this->admin_form->form_open();
    echo $this->admin_form->box_open('', 'box-theme', "col-md-offset-2");

    /* SHOW START button If service not started and has user has start permission , otherwise HIDE */
    if( ! $is_service_proc && $has_start_permission)
    {
        $this->template->javascript->add(base_url("dist/vZaloadsConfigurationBox.js?v={$version}"));
        echo "<div id='app-process-service-container'><v-zaloads-configuration-box term_id='{$term_id}'/> </div>";
    }

    /* SHOW END button If service started , serviec still running and has user has start permission , otherwise HIDE */

    if( ! $is_service_end && $is_service_proc && $has_stop_permission)
    {
        $this->template->javascript->add(base_url("dist/vZaloadsConfigurationStopBox.js?v={$version}"));
        echo "<div id='app-stop-process-service-container'> <v-zaloads-configuration-stop-box term_id='{$term_id}'/> </div>";
    }

    // echo '<p class="help-block">Vui lòng cấu hình để hệ thống E-mail hoạt động</p>';

    echo $this->admin_form->box_close();
    echo $this->admin_form->form_close();
}
# END MAIN COMMAND : START SERVICE | STOP SERVICE


# END CURATORS SETTING BOX
// $this->template->javascript->add(base_url("dist/vZaloadsConfigurationServiceBox.js?v={$version}"));
// echo "<div id='app-configuration-service-box-container'><v-zaloads-configuration-service-box term_id='{$term_id}'/> </div>";

# CURATORS SETTING BOX
$this->admin_form->set_col(8, 6);
echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Thông tin nhận báo cáo Email', 'box-theme', 'col-md-offset-2');

$curators = @unserialize(get_term_meta_value($term->term_id,'contract_curators'));
$max_curator = $this->option_m->get_value('max_number_curators');
$max_curator = $max_curator ?: $this->config->item('max_input_curators','googleads');

echo '<div class="row">';
for ($i=0; $i < $max_curator; $i++)
{ 
    echo $this->admin_form->formGroup_begin();
    echo '<div class="col-xs-4" style="padding-left: 0;">';
    echo form_input(['name'=>'edit[meta][curator_name][]','class'=>'form-control','placeholder'=>'Người nhận báo cáo '.($i+1)],$curators[$i]['name']??'');
    echo '</div>';
    echo '<div class="col-xs-4">';
    echo form_input(['name'=>'edit[meta][curator_email][]','class'=>'form-control','placeholder'=>'Email '.($i+1)],$curators[$i]['email']??'');
    echo '</div>';

    echo '<div class="col-xs-4">';
    echo form_input(['name'=>'edit[meta][curator_phone][]','class'=>'form-control','placeholder'=>'Phone '.($i+1)],$curators[$i]['phone']??'');
    echo '</div><br/>';
    echo $this->admin_form->formGroup_end();
}

echo '</div>';
echo $this->admin_form->box_close(['submit','Lưu lại'],['button','Hủy bỏ']);
echo $this->admin_form->form_close();
?>
<style type="text/css"> .multiselect--active { z-index: 1000; } </style>