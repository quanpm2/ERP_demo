<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">
            <p style="font-family:Roboto;color:#ffffff;font-size:30px; font-weight:300; margin:5px 0;  text-transform:uppercase;"><?php echo @$title; ?></p>
        </td>
    </tr>
    <tr>
        <td align="center">
            <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng:
                <strong>
                    <?php echo get_term_meta_value($term->term_id, 'contract_code'); ?>
                </strong>
            </p>
            <?php
            $contract_begin = get_term_meta_value($term->term_id, 'contract_begin');
            $contract_end = get_term_meta_value($term->term_id, 'contract_end');

            $time_contract = 0;
            if (!empty($contract_begin) && !empty($contract_end)) {
                $time_contract = $contract_end - $contract_begin;
            }

            $start_time = get_term_meta_value($term->term_id, 'advertise_start_time');

            $end_time = (int)$start_time + (int)$time_contract;

            $contract_date = my_date($start_time, 'd/m/Y') . ' - ' . my_date($end_time, 'd/m/Y');
            ?>
            <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Thời gian thực hiện từ: <span style="font-weight:bold"><?php echo $contract_date; ?></span> .</p> &nbsp;
        </td>
    </tr>
    <tr>
        <td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
            <p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">Kính chào Quý khách</p>
            <p>Adsplus.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>
            <p>Adsplus.vn gửi email thông báo kích hoạt & thực hiện hợp đồng ADSPLUS cho <?php echo anchor(prep_url($term->term_name), $term->term_name); ?></p>
        </td>
    </tr>

    <tr>
        <td align="center" style="border:20px solid #e6e6e6;">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                <tbody>
                    <tr>
                        <td bgcolor="#fff">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                                <tbody>
                                    <tr>
                                        <td bgcolor="#fff">
                                            <?php
                                            $template = array(
                                                'table_open' => '<table border="0" cellspacing="0" cellpadding="5" width="100%" style="font-size:13px">',
                                                'row_alt_start' => '<tr bgcolor="#f4f8fb">'
                                            );

                                            $this->table->set_template($template);
                                            $this->table->add_row(array('data' => 'PHẦN CAM KẾT', 'style' => 'color:#fff;font-weight:bold', 'width' => '80%', 'colspan' => 2, 'bgcolor' => '#f58220'));

                                            $this->table->add_row('Thời gian bắt đầu :', my_date($start_time, 'd/m/Y'));
                                            $this->table->add_row('Thời gian kết thúc dự kiến :', my_date($end_time, 'd/m/Y'));

                                            $contract_budget = (int) get_term_meta_value($term->term_id, 'contract_budget');
                                            $this->table->add_row('Ngân sách :', currency_numberformat($contract_budget));

                                            echo $this->table->generate();
                                            ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>


    <tr>
        <td align="center" style="border:20px solid #e6e6e6;">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                <tbody>
                    <tr>
                        <td bgcolor="#fff">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                                <tbody>
                                    <tr>
                                        <td bgcolor="#fff">
                                            <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Thông tin khách hàng</p>
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                                                <tbody>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td width="25%" height="25">Tên khách hàng</td>
                                                        <td>: <?php echo 'Anh/Chị ' . get_term_meta_value($term->term_id, 'representative_name'); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25">Email</td>
                                                        <td>: <?php echo mailto(get_term_meta_value($term->term_id, 'representative_email')); ?></td>
                                                    </tr>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td height="25">Điện thoại di động</td>
                                                        <td>: <?php echo get_term_meta_value($term->term_id, 'representative_phone'); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25">Địa chỉ</td>
                                                        <td>: <?php echo get_term_meta_value($term->term_id, 'representative_address'); ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">
                                                Thông tin báo cáo công việc website
                                                <?php echo anchor(prep_url($term->term_name), $term->term_name); ?>
                                            </p>
                                            <?php
                                            $curators = get_term_meta_value($term->term_id, 'contract_curators');
                                            $curators = @unserialize($curators);
                                            $names = $emails = $phones = array();
                                            if (!empty($curators)) {
                                                foreach ($curators as $item) {
                                                    if (!empty($item['name'])) $names[] = $item['name'];
                                                    if (!empty($item['phone'])) $phones[] = $item['phone'];
                                                    if (!empty($item['email'])) $emails[] = mailto($item['email']);
                                                }
                                            }
                                            ?>
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                                                <tbody>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td width="25%" height="25">Nhận báo cáo</td>
                                                        <td>: <?php echo empty($names) ? '' : implode(', ', $names); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25">SMS </td>
                                                        <td>: <span style="font-weight:bold"><?php echo empty($phones) ? '' : implode(', ', $phones); ?></span></td>
                                                    </tr>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td height="25">Email</td>
                                                        <td>: <?php echo empty($emails) ? '' : implode(', ', $emails); ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
