<table width="100%" border="0" cellspacing="0" cellpadding="0" >
	<tr>
		<td align="center">
			<p style="font-family:Roboto;color:#ffffff;font-size:30px; font-weight:300; margin:5px 0;  text-transform:uppercase;"><?php echo @$title;?></p>
		</td>
	</tr>
	<tr>
		<td align="center">
			<p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng: 
				<strong>
					<?php echo get_term_meta_value($term->term_id,'contract_code');?>
				</strong>
			</p>
			<?php
			$contract_date = my_date($start_time,'d/m/Y').' đến '.my_date($end_time,'d/m/Y');
			?>
			<p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Thời gian thực hiện từ: <span style="font-weight:bold"><?php echo $contract_date;?></span> .</p> &nbsp;
		</td>
	</tr>
	<tr>
		<td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
			<p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">Kính chào Quý khách</p>
			<p>Adsplus.vn gửi email báo cáo quảng cáo cho website <?php echo anchor(prep_url($term->term_name),$term->term_name);?></p>
			<p>Adsplus.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>
			<p>Quý khách vui lòng tải xuống file báo cáo trong tệp đính kèm</p>
		</td>
	</tr>
	<tr>
		<td bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6;border-right: 20px solid #e6e6e6;  ">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tbody>
					<tr>
						<td bgcolor="#f58220" >
							<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px"   >
								<tbody>
									<tr>
										<td>
											<p style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:16px ; font-weight:bold;margin:10px 20px">Các dịch vụ ADSPLUS.VN cung cấp cho Quý khách : </p>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#e6e6e6">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px">
								<tbody>
									<tr>
										<td valign="top"><img alt="arrow down" src="http://webdoctor.vn/images/arrowdown-1.png"></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
								<tbody>
									<tr>
										<td bgcolor="#ffffff" style="font-size: 13px;
										color: #363636;
										font-family: Arial, Helvetica, sans-serif;" >
										<p ><strong>1. Nhận báo cáo tuần từ hệ thống. </strong> Anh (Chị) sẽ nhận được báo cáo tuần từ hệ thống Adsplus.vn, giúp anh chị theo dõi được tiến độ chạy quảng cáo</p>
										<p ><strong>2. Nhận SMS hàng ngày. </strong> Anh (Chị) sẽ nhận được SMS hàng ngày thông báo số nhấp chuột mỗi ngày.</p>
										<p >Cảm ơn anh/chị ! </p>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</td>
</tr>
</table>