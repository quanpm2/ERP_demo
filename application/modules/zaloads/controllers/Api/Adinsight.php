<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adinsight extends API_Controller {

	function __construct()
	{
		parent::__construct();

		$models = array(
			'facebookads/facebookads_m',
			'facebookads/adcampaign_m',
			'facebookads/adset_m',
			'term_posts_m',
		);

		$this->load->model($models);
	}

	public function adaccount($term_id = 0)
	{
		$defaults = array(
			'orderby'=>'start_date',
			'order'=>'ASC',
			'fields'=>'all',
			'where'=>'',
			'start_time' => $this->mdate->startOfDay(strtotime('2017/08/21')),
			'end_time' => $this->mdate->endOfDay(),
		);
		$args 			= wp_parse_args($this->get(),$defaults);
		$args_encrypt	= md5(json_encode($args));


		/**
		 * Truy xuất dữ liệu hợp đồng input và xác thực thông tin
		 *
		 * @var        facebookads_m
		 */
		$term = $this->facebookads_m->set_term_type()->get($term_id);
		if( ! $term)
		{
			$response['msg'] = 'Hợp đồng không tồn tại hoặc không hợp lệ !';
			return parent::render($response,500);
		}


		/**
		 * Truy xuất thông tin các campaign được kết nối với hợp đồng
		 *
		 * @var        array
		 */
		$adcampaign_ids = get_term_meta_value($term_id,'adcampaign_ids');
        $adcampaign_ids = $adcampaign_ids ? unserialize($adcampaign_ids) : array();
        if( ! $adcampaign_ids)
		{
			$response['msg'] = 'Cấu hình chiến dịch tạm thời không khả dụng !';
			return parent::render($response,500);
		}


		/**
		 * Dữ liệu tất cả các campaign quảng cáo đang thực hiện
		 *
		 * @var        array
		 */
		$adcampaign_insight = array();
		$start_time = $args['start_time'];
		$end_time 	= $args['end_time'];
        foreach ($adcampaign_ids as $adcampaign_id)
        {
        	$adcamp_insight = $this->adcampaign_m->get_data($adcampaign_id,$start_time,$end_time);

        	if(empty($adcamp_insight)) continue;
        	
        	$adcampaign_insight = array_merge($adcampaign_insight,$adcamp_insight);
        }

        if(empty($adcampaign_insight))
		{
			$response['msg'] = 'Không tìm thấy dữ liệu !';
			return parent::render($response,500);
		}

        $adcampaign_insight = array_group_by($adcampaign_insight,'date_start');

        $datatable = array();
        foreach ($adcampaign_insight as $date => $campaigns)
        {
        	$date = my_date($this->mdate->startOfDay($date),'d/m/Y');
        	if(empty($campaigns)) continue;


        	foreach ($campaigns as $campaign)
        	{
				/* Ngày | Tên chiến dịch | Tổng hiển thị | Tiếp Cận | Tổng kết quả | Giá mỗi kết quả | Clicks | Chi phí CLicks | Chi phí | Đối tượng */
        		$datatable[] = array(
        			'date' => $date,
        			'campaign_id' 		=> $campaign['campaign_id'],
        			'campaign_name' 	=> $campaign['campaign_name'],
        			'objective' 		=> $campaign['objective'],
        			'reach' 			=> $campaign['reach'],
        			'impressions' 		=> $campaign['impressions'],
        			'result' 			=> $campaign['result'],
        			'cost_per_result' 	=> $campaign['cost_per_result'],
        			'clicks' 			=> $campaign['clicks'],
        			'cpc' 				=> $campaign['cpc'],
        			'spend' 			=> $campaign['spend']
        		);
        	}
        }

        $response['data'] = $datatable;
        $this->default_response['auth'] = 1;
		return parent::render($response);
	}

	public function adcampaigns($term_id = 0)
	{
		$defaults = array(
			'orderby'=>'start_date',
			'order'=>'ASC',
			'fields'=>'all',
			'where'=>'',
			'start_time' => $this->mdate->startOfDay(strtotime('2017/08/21')),
			'end_time' => $this->mdate->endOfDay(),
		);
		$args 			= wp_parse_args($this->get(),$defaults);
		$args_encrypt	= md5(json_encode($args));


		/**
		 * Truy xuất dữ liệu hợp đồng input và xác thực thông tin
		 *
		 * @var        facebookads_m
		 */
		$term = $this->facebookads_m->set_term_type()->get($term_id);
		if( ! $term)
		{
			$response['msg'] = 'Hợp đồng không tồn tại hoặc không hợp lệ !';
			return parent::render($response,500);
		}

		/**
		 * Truy xuất thông tin các campaign được kết nối với hợp đồng
		 *
		 * @var        array
		 */
		$adcampaign_ids = get_term_meta_value($term_id,'adcampaign_ids');
        $adcampaign_ids = $adcampaign_ids ? unserialize($adcampaign_ids) : array();
        if( ! $adcampaign_ids)
		{
			$response['msg'] = 'Cấu hình chiến dịch tạm thời không khả dụng !';
			return parent::render($response,500);
		}

		/**
		 * Dữ liệu tất cả các campaign quảng cáo đang thực hiện
		 *
		 * @var        array
		 */
		$adcampaign_insight = array();
		$start_time = $args['start_time'];
		$end_time 	= $args['end_time'];
        foreach ($adcampaign_ids as $adcampaign_id)
        {
        	$adcamp_insight = $this->adcampaign_m->get_data($adcampaign_id,$start_time,$end_time);

        	if(empty($adcamp_insight)) continue;
        	
        	$adcampaign_insight = array_merge($adcampaign_insight,$adcamp_insight);
        }

        if(empty($adcampaign_insight))
		{
			$response['msg'] = 'Không tìm thấy dữ liệu !';
			return parent::render($response,500);
		}

        $adcampaign_insight = array_group_by($adcampaign_insight,'date_start');

        $datatable = array();
        foreach ($adcampaign_insight as $date => $campaigns)
        {
        	$date = my_date($this->mdate->startOfDay($date),'d/m/Y');
        	if(empty($campaigns)) continue;


        	foreach ($campaigns as $campaign)
        	{
				/* Ngày | Tên chiến dịch | Tổng hiển thị | Tiếp Cận | Tổng kết quả | Giá mỗi kết quả | Clicks | Chi phí CLicks | Chi phí | Đối tượng */
        		$datatable[] = array(
        			'date' => $date,
        			'campaign_id' 		=> $campaign['campaign_id'],
        			'campaign_name' 	=> $campaign['campaign_name'],
        			'objective' 		=> $campaign['objective'],
        			'reach' 			=> $campaign['reach'],
        			'impressions' 		=> $campaign['impressions'],
        			'result' 			=> $campaign['result'],
        			'cost_per_result' 	=> $campaign['cost_per_result'],
        			'clicks' 			=> $campaign['clicks'],
        			'cpc' 				=> $campaign['cpc'],
        			'spend' 			=> $campaign['spend']
        		);
        	}
        }

        $response['data'] = $datatable;
        $this->default_response['auth'] = 1;
		return parent::render($response);
	}

	public function adsets($term_id = 0)
	{
		$defaults = array(
			'orderby'=>'start_date',
			'order'=>'ASC',
			'fields'=>'all',
			'where'=>'',
			'start_time' => $this->mdate->startOfDay(strtotime('2017/08/21')),
			'end_time' => $this->mdate->endOfDay(),
		);
		$args 			= wp_parse_args($this->get(),$defaults);
		$args_encrypt	= md5(json_encode($args));


		/**
		 * Truy xuất dữ liệu hợp đồng input và xác thực thông tin
		 *
		 * @var        facebookads_m
		 */
		$term = $this->facebookads_m->set_term_type()->get($term_id);
		if( ! $term)
		{
			$response['msg'] = 'Hợp đồng không tồn tại hoặc không hợp lệ !';
			return parent::render($response,500);
		}

		/**
		 * Truy xuất thông tin các campaign được kết nối với hợp đồng
		 *
		 * @var        array
		 */
		$adcampaign_ids = get_term_meta_value($term_id,'adcampaign_ids');
        $adcampaign_ids = $adcampaign_ids ? unserialize($adcampaign_ids) : array();
        if( ! $adcampaign_ids)
		{
			$response['msg'] = 'Cấu hình chiến dịch tạm thời không khả dụng !';
			return parent::render($response,500);
		}

		/**
		 * List adsets
		 *
		 * @var        array
		 */
		$adsets = array();
		foreach ($adcampaign_ids as $adcampaign_id)
		{
			$adset_terms = $this->term_posts_m->get_term_posts($adcampaign_id,$this->adset_m->post_type);
			if(empty($adset_terms)) continue;

			$adsets = array_merge($adsets,$adset_terms);
		}

		if(empty($adsets))
		{
			$response['msg'] = 'Cấu hình nhóm quảng cáo tạm thời không khả dụng !';
			return parent::render($response,500);
		}


		/**
		 * Dữ liệu tất cả các nhóm quảng cáo đang thực hiện
		 *
		 * @var        array
		 */
		$adsets_insight = array();
		$start_time = $args['start_time'];
		$end_time 	= $args['end_time'];
        foreach ($adsets as $adset)
        {
        	$adset_insight = $this->adset_m->get_data($adset->post_id,$start_time,$end_time);
        	if(empty($adset_insight)) continue;

        	$adsets_insight = array_merge($adsets_insight,$adset_insight);
        }

        if(empty($adsets_insight))
		{
			$response['msg'] = 'Không tìm thấy dữ liệu !';
			return parent::render($response,500);
		}

        $adsets_insight = array_group_by($adsets_insight,'date_start');

        $datatable = array();
        foreach ($adsets_insight as $date => $adsets)
        {
        	$date = my_date($this->mdate->startOfDay($date),'d/m/Y');
        	if(empty($adsets)) continue;


        	foreach ($adsets as $adset)
        	{
				/* Ngày | Tên chiến dịch | Tổng hiển thị | Tiếp Cận | Tổng kết quả | Giá mỗi kết quả | Clicks | Chi phí CLicks | Chi phí | Đối tượng */
        		$datatable[] = array(
        			'date' => $date,
        			'adset_id' 			=> $adset['adset_id'],
        			'adset_name' 		=> $adset['adset_name'],
        			'campaign_id' 		=> $adset['campaign_id'],
        			'campaign_name' 	=> $adset['campaign_name'],
        			'objective' 		=> $adset['objective'],
        			'reach' 			=> $adset['reach'],
        			'impressions' 		=> $adset['impressions'],
        			'result' 			=> $adset['result'],
        			'cost_per_result' 	=> $adset['cost_per_result'],
        			'clicks' 			=> $adset['clicks'],
        			'cpc' 				=> $adset['cpc'],
        			'spend' 			=> $adset['spend']
        		);
        	}
        }

        $response['data'] = $datatable;
        $this->default_response['auth'] = 1;
		return parent::render($response);
	}
}
/* End of file Adinsight.php */
/* Location: ./application/modules/facebookads/controllers/Api/Adinsight.php */