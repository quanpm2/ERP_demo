<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends API_Controller {

	function __construct()
	{
		parent::__construct();

		$models = array(
			'api_user_m',
			'facebookads/facebookads_m',
			'message/message_m'
			);

		$this->load->model($models);
		$this->load->config('contract/contract');
	}


	public function list()
	{
		$response = array('status'=>FALSE,'msg'=>'Xử lý không thành công','data'=>array());

		if(!$this->authenticate) return parent::render();
		$token = $this->get('token');

		$services = array();
        $term_ids = array();

		$user_id = $this->api_user_m->user_id ?? 0;
        if($user_id)
        {
            $this->load->model('customer/customer_m');
            $customer = $this->customer_m->set_get_customer()->get($user_id);
            
            $this->load->model('term_users_m');
            $term_ids = $this->term_users_m
            ->where_in('term_status',array('pending','publish'))
            ->get_the_terms($user_id,'facebook-ads');
        }

        if(!empty($this->api_user_m->auser_phone))
        {
            $auser_phone = $this->api_user_m->auser_phone ?? 0;

            $term_meta = $this->termmeta_m
            ->select('term_id')
            ->where('meta_key','contract_curators')
            ->like('meta_value',$auser_phone)
            ->get_many_by();

            if(!empty($term_meta))
            {
                $term_ids = array_unique(array_merge($term_ids, array_column($term_meta, 'term_id')));
            }
        }

		$defaults 			= array('orderby'=>'term.term_id','order'=>'DESC','fields'=>'all','where'=>'');
		$args 				= wp_parse_args($this->get(),$defaults);
		$args_encrypt		= md5(json_encode($args));

		if(!empty($args['orderby'])) $this->facebookads_m->order_by($args['orderby'],$args['order']);
			
		$terms = $this->facebookads_m
		->set_term_type()
		// ->where_in('term.term_status',['pending','publish','ending','liquidation'])
		->where_in('term.term_status',['publish'])
		->where_in('term.term_id',$term_ids)
		->get_all();

		if(empty($terms))
		{
			$response['msg'] = 'Quý khách hàng chưa có hợp đồng nào được khởi tạo !';
			return parent::render($response,500);
		}

		$data = array();
		foreach ($terms as $term)
		{
			$data[] = array(
				'id' => $term->term_id,
				'code' => get_term_meta_value($term->term_id,'contract_code'),
				'status' => $term->term_status,
				'type' => $term->term_type,
				'name' => $term->term_name,
				'msg_count' => 0,
				'start_service_time' => get_term_meta_value($term->term_id,'start_service_time'),
				'expected_end_time' => get_term_meta_value($term->term_id, 'expected_end_time'),
				'budget' => get_term_meta_value($term->term_id,'contract_budget'),
				'spend' => get_term_meta_value($term->term_id,'actual_result'),
				'reach' => (int) get_term_meta_value($term->term_id,'reach'),
				'impressions' => (int) get_term_meta_value($term->term_id,'impressions'),
				'cpc' => (int) get_term_meta_value($term->term_id,'cpc'),
				'clicks' => (int) get_term_meta_value($term->term_id,'clicks'),
				'result' => (int) get_term_meta_value($term->term_id,'result'),
				'cost_per_result' => (int) get_term_meta_value($term->term_id,'cost_per_result')
			);
		}
		
		$response['status'] = 1;
		$response['data'] = $data;
        $this->default_response['auth'] = 1;
		return parent::render($response);
	}


	public function messages($term_id = 0)
	{
		$response = array('status'=>FALSE,'msg'=>'Xử lý không thành công','data'=>array());
		if(!$this->authenticate) return parent::render();
		$token = $this->get('token');

        $user_id = $this->api_user_m->user_id;
        $auser_phone = $this->api_user_m->auser_phone;

		$this->load->model('message/message_m'); 
        $messages = $this->message_m->get_all_by($term_id,$user_id,$auser_phone);

		$response['status'] = 1;
		$response['msg'] = 'Xử lý thành công .';
		$response['data'] = $messages;
        $this->default_response['auth'] = 1;
		return parent::render($response);
	}

	/**
	 * Lấy tất cả các chiến dịch đang thực hiện 
	 * Theo ID hợp đồng truyền vào
	 *
	 * @param      integer  $term_id  mã hợp đồng
	 */
	public function campaigns($term_id = 0)
	{
		$response = array('status'=>FALSE,'msg'=>'Xử lý không thành công','data'=>array());
		// if(!$this->authenticate) return parent::render();
		// $token = $this->get('token');
		
		/* Ngày | Tổng hiển thị | Tiếp Cận | Tổng kết quả | Giá mỗi kết quả | Clicks | Chi phí CLicks | Chi phí | Đối tượng */
		
		$response['data'] = $data;
        $this->default_response['auth'] = 1;
		return parent::render($response);
	}
}
/* End of file Contract.php */
/* Location: ./application/modules/facebookads/controllers/Api/Contract.php */