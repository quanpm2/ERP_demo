<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contract extends REST_Controller
{
    function __construct()
    {
        parent::__construct('zaloads/rest');
        $this->load->model('usermeta_m');
        $this->load->model('permission_m');
        $this->load->config('zaloads/fields');
    }

    public function config_get()
    {
        $default     = array('name' => '');
        $args         = wp_parse_args(parent::get(NULL, TRUE), $default);
        if (!has_permission('zaloads.index.access')) parent::response(['msg' => 'Quyền hạn không hợp lệ.'], parent::HTTP_UNAUTHORIZED);

        $response = array(
            'msg' => 'Dữ liệu tải thành công',
            'data' => array(
                'columns' => $this->config->item('columns', 'datasource'),
                'default_columns' => $this->config->item('default_columns', 'datasource')
            )
        );

        parent::response($response);
    }
}
/* End of file Contract.php */
/* Location: ./application/modules/wservice/controllers/api/Contract.php */