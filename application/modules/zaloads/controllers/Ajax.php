<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajax extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();

        $models = array(
            'zaloads/zaloads_m',
            'zaloads/zaloads_kpi_m',
        );
        $this->load->model($models);

        $this->load->config('zaloads/zaloads');
    }

    /**
     * Render dataset of FB's services
     */
    public function dataset()
    {
        $response     = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        $permission = 'zaloads.index.access';

        if (!has_permission($permission)) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $columns = $this->zaloads_m->get_field_config();
        $default = array('offset' => 0, 'per_page' => 10, 'cur_page' => 1, 'is_filtering' => TRUE, 'is_ordering' => TRUE, 'columns' => implode(',', $columns['default_columns']));
        $args     = wp_parse_args($this->input->get(), $default);
        $args['columns'] = explode(',', $args['columns']);

        $this->load->library('datatable_builder');

        $relate_users         = $this->admin_m->get_all_by_permissions($permission);
        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        if ($relate_users === FALSE) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        if (is_array($relate_users)) {
            $this->datatable_builder->join('term_users', 'term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
        }

        /* Applies get query params for filter */
        $this->search_filter();
        $this->template->title->append('Tổng quan dịch vụ đang thực hiện ADSPLUS');

        $this->datatable_builder
            ->select('term.term_id,term.term_status,term.term_name,term.term_type')
            ->set_filter_position(FILTER_TOP_OUTTER)
            ->add_search('term.term_id', ['placeholder' => '#ID'])
            ->add_search('term.term_name', ['placeholder' => 'Website'])
            ->add_search('term_status', array('content' => form_dropdown(array('name' => 'where[term_status]', 'class' => 'form-control select2'), prepare_dropdown($this->config->item('status', 'zaloads'), 'Trạng thái : All'), $this->input->get('where[term_status]'))))

            ->add_search('term_status', array('content' => form_dropdown(array('name' => 'where[term_status]', 'class' => 'form-control select2'), prepare_dropdown($this->config->item('status', 'zaloads'), 'Trạng thái : All'), $this->input->get('where[term_status]'))))

            ->add_search('staff_advertise', ['placeholder' => 'Kỹ thuật phụ trách'])
            ->add_search('staff_business', ['placeholder' => 'Kinh doanh phụ trách'])

            ->add_search('is_service_proc', array('content' => form_dropdown(array('name' => 'where[is_service_proc]', 'class' => 'form-control select2'), prepare_dropdown(['active' => 'Đã kích hoạt', 'unactive' => 'Chưa kích hoạt'], 'TT Kích hoạt : All'), $this->input->get('where[is_service_proc]'))))
            ->add_search('has_adaccount', array('content' => form_dropdown(array('name' => 'where[has_adaccount]', 'class' => 'form-control select2'), prepare_dropdown(['active' => 'Đã cấu hình Adaccount', 'unactive' => 'Chưa cấu hình AdAccount'], 'TT Cấu hình : All'), $this->input->get('where[has_adaccount]'))));

        foreach ($args['columns'] as $key) {
            if (empty($columns['columns'][$key])) continue;
            $this->datatable_builder->add_column($columns['columns'][$key]['name'], $columns['columns'][$key]);
        }

        foreach ($this->zaloads_m->get_field_callback() as $callback) {
            $this->datatable_builder->add_column_callback($callback['field'], $callback['func'], $callback['row_data']);
        }

        $data = $this->datatable_builder
            ->select('term.*')
            ->from('term')
            ->join('termmeta started_service', 'started_service.term_id = term.term_id', 'INNER')
            ->where('term.term_type', $this->zaloads_m->term_type)
            ->where_in('term.term_status', array_keys($this->config->item('status', 'zaloads')))
            ->where('started_service.meta_key ', 'started_service')
            ->group_by('term.term_id')
            ->generate(array('per_page' => $args['per_page'], 'cur_page' => $args['cur_page']));

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export($last_query);
            return TRUE;
        }

        return parent::renderJson($data);
    }


    /**
     * Export Excel Dataset
     *
     * @param      string  $query  The query
     */
    public function export($query = '')
    {
        if (empty($query)) return FALSE;

        // remove limit in query string
        $query = explode('LIMIT', $query);
        $query = reset($query);

        $terms = $this->contract_m->query($query)->result();

        if (!$terms) return FALSE;

        $this->config->load('contract/fields');
        $default     = array('columns' => implode(',', $this->config->item('default_columns', 'datasource')));
        $args         = wp_parse_args($this->input->get(), $default);
        $args['columns'] = explode(',', $args['columns']);

        $this->load->library('excel');
        $cacheMethod     = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings     = array('memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("WEBDOCTOR.VN")->setLastModifiedBy("WEBDOCTOR.VN")->setTitle(uniqid('Danh sách hợp đồng __'));
        $objPHPExcel->setActiveSheetIndex(0);

        $objWorksheet     = $objPHPExcel->getActiveSheet();
        $callbacks         = $this->zaloads_m->get_field_callback();
        $field_config     = $this->zaloads_m->get_field_config();

        $args['columns'] = array(
            'website',
            'actual_result',
            'actual_budget',
            'payment_expected_end_time',
            'payment_real_progress',
            'payment_cost_per_day_left',
            'actual_progress_percent_net',
            'expected_end_time',
            'real_progress',
            'cost_per_day_left',
            'actual_progress_percent',
            'result_updated_on',
            'fb_staff_advertise',
            'staff_business',
            'status',
            'adaccount_id',
        );

        $headings = array_map(function ($x) use ($field_config) {
            return $field_config['columns'][$x]['title'] ?? $x;
        }, $args['columns']);

        $objWorksheet->fromArray($headings, NULL, 'A1');
        $row_index = 2;
        foreach ($terms as $key => &$term) {
            $term = (array) $term;
            foreach ($args['columns'] as $column) {
                if (empty($field_config['columns'][$column])) continue;
                if (empty($callbacks[$column])) continue;

                $term = call_user_func_array($callbacks[$column]['func'], array($term, $column, []));
            }

            $i = 0;
            $row_number = $row_index + $key;

            foreach ($args['columns'] as $column) {
                if (empty($field_config['columns'][$column])) continue;

                $value = $term["{$column}_raw"] ?? ($term["{$column}"] ?? '');
                $col_number = $i;
                $i++;

                switch ($field_config['columns'][$column]['type']) {
                    case 'string':
                        $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
                        break;

                    case 'number':
                        $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp':
                        $value = PHPExcel_Shared_Date::PHPToExcel($value);
                        $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default:
                        break;
                }

                $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $value);
            }
        }

        $num_rows = count($terms);

        // We'll be outputting an excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $folder_upload = "files/contracts/";
        if (!is_dir($folder_upload)) {
            try {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $created_datetime     = my_date(time(), 'Y-m-d-H-i-s');
        $file_name             = "{$folder_upload}danh-sach-hop-dong-facebook-ads-{$created_datetime}.xlsx";

        try {
            $objWriter->save($file_name);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($file_name, NULL);
        return TRUE;
    }

    protected function search_filter($search_args = array())
    {
        $args = $this->input->get();

        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        if (!empty($args['where']['started_service'])) {
            $explode = explode(' - ', $args['where']['started_service']);
            if (!empty($explode)) {
                $this->datatable_builder
                    ->join('termmeta AS tm_stated_service', 'tm_stated_service.term_id = term.term_id', 'LEFT')
                    ->where('tm_stated_service.meta_key', 'started_service')
                    ->where('tm_stated_service.meta_value >=', start_of_day(reset($explode)))
                    ->where('tm_stated_service.meta_value <=', end_of_day(end($explode)));
            }
            unset($args['where']['started_service']);
        }

        if (!empty($args['order_by']['started_service'])) {
            $this->datatable_builder->join('termmeta AS tm_stated_service', 'tm_stated_service.term_id = term.term_id', 'LEFT');
            $args['where']['tm_stated_service.meta_key'] = 'started_service';
            $args['order_by']['tm_stated_service.meta_value'] = $args['order_by']['started_service'];
            unset($args['order_by']['started_service']);
        }

        // Staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if ($filter_staff_business || $sort_staff_business) {
            $alias = uniqid('staff_business_');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT')
                ->join("user tblcustomer", "{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

            if ($filter_staff_business) {
                $this->datatable_builder->like("tblcustomer.display_name", $filter_staff_business);
                unset($args['where']['staff_business']);
            }

            if ($sort_staff_business) {
                $this->datatable_builder->order_by('tblcustomer.display_name', $sort_staff_business);
                unset($args['order_by']['staff_business']);
            }
        }

        // is_service_proc FILTERING & SORTING
        $filter_is_service_proc = $args['where']['is_service_proc'] ?? FALSE;
        if ($filter_is_service_proc) {
            $alias     = uniqid('start_service_time_');
            $type     = $filter_is_service_proc == 'active' ? '' : 'LEFT';
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'start_service_time'", $type);

            'active' == $filter_is_service_proc
                ? $this->datatable_builder->where("({$alias}.meta_value is not NULL AND {$alias}.meta_value != '' AND {$alias}.meta_value > 0)")
                : $this->datatable_builder->where("({$alias}.meta_value is null or {$alias}.meta_value = '' OR {$alias}.meta_value = 0)");

            unset($args['where']['is_service_proc']);
        }

        // has_adaccount FILTERING & SORTING
        $filter_has_adaccount = $args['where']['has_adaccount'] ?? FALSE;
        if ($filter_has_adaccount) {
            $alias     = uniqid('adaccount_id_');
            $type     = $filter_has_adaccount == 'active' ? '' : 'LEFT';
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'adaccount_id'", $type);

            'active' == $filter_has_adaccount
                ? $this->datatable_builder->where("({$alias}.meta_value != '' OR {$alias}.meta_value > 0 )")
                // : $this->datatable_builder->where("{$alias}.meta_value in (NULL, '', 0)");
                : $this->datatable_builder->where("({$alias}.meta_value is null or {$alias}.meta_value = '' OR {$alias}.meta_value = 0)");

            unset($args['where']['has_adaccount']);
        }

        /* Sort for actual progress goal */
        if (!empty($args['order_by']['actual_progress_percent_net'])) {
            $alias = uniqid('actual_progress_percent_net');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_progress_percent'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['actual_progress_percent_net'];
            unset($args['order_by']['actual_progress_percent_net']);
        }

        /* Sort for actual progress goal */
        if (!empty($args['order_by']['actual_progress_percent'])) {
            $alias = uniqid('actual_progress_percent');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_progress_percent'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['actual_progress_percent'];
            unset($args['order_by']['actual_progress_percent']);
        }

        /* Sort for actual progress goal */
        if (!empty($args['order_by']['real_progress'])) {
            $alias = uniqid('real_progress');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'real_progress'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['real_progress'];
            unset($args['order_by']['real_progress']);
        }

        /* Sort for actual progress goal */
        if (!empty($args['order_by']['payment_real_progress'])) {
            $alias = uniqid('payment_real_progress');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_real_progress'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_real_progress'];
            unset($args['order_by']['payment_real_progress']);
        }

        /* Sort for actual progress goal */
        if (!empty($args['order_by']['result_updated_on'])) {
            $alias = uniqid('result_updated_on');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'result_updated_on'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['result_updated_on'];
            unset($args['order_by']['result_updated_on']);
        }

        if (!empty($args['order_by']['actual_result'])) {
            $alias = uniqid('actual_result');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_result'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['actual_result'];
            unset($args['order_by']['actual_result']);
        }

        // FIND AND SORT STAFF_ADVERTISE WITH FACEBOOK_KPI
        $filter_staff_advertise = $args['where']['staff_advertise'] ?? FALSE;
        $sort_staff_advertise = $args['order_by']['fb_staff_advertise'] ?? FALSE;
        if ($filter_staff_advertise || $sort_staff_advertise) {
            $this->datatable_builder->join('googleads_kpi', 'term.term_id = googleads_kpi.term_id', 'LEFT OUTER');
            $this->datatable_builder->join('user', 'user.user_id = googleads_kpi.user_id', 'LEFT OUTER');

            if ($filter_staff_advertise) {
                $this->datatable_builder->like('user.user_email', strtolower($filter_staff_advertise), 'both');
            }

            if ($sort_staff_advertise) {
                $this->datatable_builder->order_by('user.user_email', $sort_staff_advertise);
            }

            unset($args['where']['staff_advertise'], $args['order_by']['staff_advertise']);
        }

        if (!empty($args['order_by']['payment_percentage'])) {
            $alias = uniqid('payment_percentage');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_percentage'", 'INNER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_percentage'];
            unset($args['order_by']['payment_percentage']);
        }

        if (!empty($args['order_by']['payment_cost_per_day_left'])) {
            $alias = uniqid('payment_cost_per_day_left');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_cost_per_day_left'", 'INNER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_cost_per_day_left'];
            unset($args['order_by']['payment_cost_per_day_left']);
        }

        if (!empty($args['order_by']['payment_expected_end_time'])) {
            $alias = uniqid('payment_expected_end_time');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_expected_end_time'", 'INNER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_expected_end_time'];
            unset($args['order_by']['payment_expected_end_time']);
        }

        if (!empty($args['order_by']['contract_budget'])) {
            $alias = uniqid('contract_budget');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_budget'", 'INNER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['contract_budget'];
            unset($args['order_by']['contract_budget']);
        }

        if (!empty($args['order_by']['actual_budget'])) {
            $alias = uniqid('actual_budget');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_budget'", 'INNER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['actual_budget'];
            unset($args['order_by']['actual_budget']);
        }

        if (!empty($args['order_by']['expected_end_time'])) {
            $this->datatable_builder->join('termmeta AS expected_end_time_termmeta', 'expected_end_time_termmeta.term_id = term.term_id', 'INNER');
            $args['where']['expected_end_time_termmeta.meta_key'] = 'expected_end_time';

            $args['order_by']['(expected_end_time_termmeta.meta_value)*1'] = $args['order_by']['expected_end_time'];

            unset($args['order_by']['expected_end_time']);
        }

        if (!empty($args['order_by']['payment_expected_end_time'])) {
            $this->datatable_builder->join('termmeta AS payment_expected_end_time_termmeta', 'payment_expected_end_time_termmeta.term_id = term.term_id', 'INNER');
            $args['where']['payment_expected_end_time_termmeta.meta_key'] = 'payment_expected_end_time';

            $args['order_by']['(payment_expected_end_time_termmeta.meta_value)*1'] = $args['order_by']['payment_expected_end_time'];

            unset($args['order_by']['payment_expected_end_time']);
        }

        $sort_status = $args['order_by']['status'] ?? FALSE;
        if ($sort_status) {
            $this->datatable_builder->order_by('term.term_status', $sort_status);
            unset($args['order_by']['status']);
        }

        $sort_type = $args['order_by']['type'] ?? FALSE;
        if ($sort_type) {
            $this->datatable_builder->order_by('term.term_type', $sort_type);
            unset($args['order_by']['type']);
        }

        $sort_website = $args['order_by']['website'] ?? FALSE;
        if ($sort_website) {
            $this->datatable_builder->order_by('term.term_name', $sort_website);
            unset($args['order_by']['website']);
        }

        // APPLY DEFAULT FILTER BY MUTATE FIELD		
        $args = $this->datatable_builder->parse_relations_searches($args);
        if (!empty($args['where'])) {
            foreach ($args['where'] as $key => $value) {
                if (empty($value)) continue;

                if (empty($key)) {
                    $this->datatable_builder->add_filter($value, '');
                    continue;
                }

                $this->datatable_builder->add_filter($key, $value);
            }
        }

        if (!empty($args['order_by'])) {
            foreach ($args['order_by'] as $key => $value) {
                $this->datatable_builder->order_by($key, $value);
            }
        }
    }

    /**
     * Đồng bộ toàn bộ dữ liệu của dịch vụ dựa vào ID dịch vụ
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     json   data response
     */
    public function async_service_data($term_id = 0)
    {
        $result = ['success' => 'error', 'msg' => 'Xử lý không thành công', 'data' => ''];
        if (!parent::check_token()) {
            $result['msg'] = 'Request invalid !';
            return parent::renderJson($result);
        }

        $contract         = (new zaloads_m())->set_contract($term_id);
        $behaviour_m     = $contract->get_behaviour_m();

        $behaviour_m->get_the_progress();
        $behaviour_m->sync_all_amount();

        $result['success']     = 'success';
        $result['msg']         = 'Dữ liệu đã được đồng bộ thành công. Vui lòng (F5) để xem sự thay đổi';
        return parent::renderJson($result);
    }
}
/* End of file Ajax.php */
/* Location: ./application/modules/controllers/Ajax.php */
