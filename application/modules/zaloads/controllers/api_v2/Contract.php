<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contract extends MREST_Controller
{
    function __construct()
    {
        $this->autoload['models'][]     = 'zaloads/zaloads_m';
        $this->autoload['models'][]     = 'zaloads/zaloads_kpi_m';
        $this->autoload['helpers'][]     = 'date';

        parent::__construct();
    }

    /**
     * @param int $id Id hợp đồng FB
     * 
     * @return [type]
     */
    public function overview_get(int $id)
    {
        $data = $this->data;

        if (false == $this->zaloads_m->set_contract($id)) return parent::responseHandler([], 'Hợp đồng không tồn tại hoặc đã bị xóa.', 'error', 404);
        if (!$this->zaloads_m->can('zaloads.overview.access')) return parent::responseHandler([], 'Không có quyền truy cập.', 'error', 401);

        $contract = $this->zaloads_m->get_contract();

        $saleId = (int) get_term_meta_value($id, 'staff_business');
        $sale     = $this->admin_m->get_field_by_id($saleId);

        $kpis = $this->zaloads_kpi_m->order_by('kpi_type')->get_many_by(['term_id' => $id]);
        $kpis and $kpis = array_map(function ($kpi) {
            $kpi->display_name = $this->admin_m->get_field_by_id($kpi->user_id, 'display_name');
            return $kpi;
        }, $kpis);

        $data = array(
            'term_id'                => $id,
            'term'                    => $contract,
            'kpis'                    => $kpis,
            'sale'                    => $sale,
            'contract_code'            => get_term_meta_value($id, 'contract_code'),
            'representative_name'    => get_term_meta_value($id, 'representative_name'),
            'representative_email'        => get_term_meta_value($id, 'representative_email'),
            'representative_address'    => get_term_meta_value($id, 'representative_address')
        );

        $start_service_time     = (int) get_term_meta_value($id, 'start_service_time');
        $end_service_time         = (int) get_term_meta_value($id, 'end_service_time');
        $advertise_start_time    = (int) get_term_meta_value($id, 'advertise_start_time');
        $advertise_end_time        = (int) get_term_meta_value($id, 'advertise_end_time');
        $vat                     = div((float) get_term_meta_value($id, 'vat'), 100);
        $contract_budget         = (float) get_term_meta_value($id, 'contract_budget');
        $service_fee             = (float) get_term_meta_value($id, 'service_fee');
        $payment_amount         = (float) get_term_meta_value($id, 'payment_amount');
        $result_updated_on      = get_term_meta_value($id, 'result_updated_on');
        $next_time_get_insight  = get_term_meta_value($id, 'next_time_get_insight');

        if (!empty($vat)) $service_fee += $service_fee * $vat;

        $actual_budget                 = (int) get_term_meta_value($id, 'actual_budget');
        $balanceBudgetReceived         = (int) get_term_meta_value($id, 'balanceBudgetReceived');

        $balance_spend                 = (float) get_term_meta_value($id, 'balance_spend');

        $payment_percentage         = 100 * (float) get_term_meta_value($id, 'payment_percentage');

        $data['actual_budget']                = $actual_budget;
        $data['total_actual_budget']        = $actual_budget;
        $data['balanceBudgetReceived']        = $balanceBudgetReceived;
        $data['balance_spend']                = $balance_spend;
        $data['contract_budget']            = $contract_budget;
        $data['payment_amount']                = $payment_amount;
        $data['payment_percentage']            = $payment_percentage;
        $data['start_service_time']            = $start_service_time;
        $data['end_service_time']            = $end_service_time;
        $data['advertise_start_time']        = $advertise_start_time;
        $data['advertise_end_time']            = $advertise_end_time;
        $data['result_updated_on']          = $result_updated_on ? date('h:i:s d/m/Y', $result_updated_on) : '--';
        $data['next_time_get_insight']      = $next_time_get_insight ? date('h:i:s d/m/Y', $next_time_get_insight) : '--';

        $start_service_time = start_of_day($start_service_time);
        $end_service_time     = end_of_day(($end_service_time ?: time()));

        $data['reach'] = 0;
        $data['result'] = 0;
        $data['impressions'] = 0;
        $data['amount_spend'] = 0;
        $data['amount_spend_percentage'] = 0;

        parent::responseHandler($data, 'ok');
    }

    /**
     * /PUT API-V2/zaloADS/CONTRACT/ADACCOUNT_INSIGHT/:ID
     *
     * @param      int    $term_id  The term identifier
     *
     * @return     Json  Result
     */
    public function adaccount_insight_put(int $term_id = 0)
    {
        $contract     = (new zaloads_m())->set_contract($term_id);
        if (!$contract) {
            return parent::responseHandler([], 'Không tìm thấy dữ liệu của hợp đồng', 'success', 400);
        }

        try {
            $contract->update_insights();
            $behaviour_m     = $contract->get_behaviour_m();
            $progress         = $behaviour_m->get_the_progress();
            $behaviour_m->sync_all_amount();

            update_term_meta($term_id, 'time_next_api_check', strtotime('+30 minutes'));
        } catch (Exception $e) {
            $result = $e->getMessage();
            return parent::responseHandler([], $result, 'error', 400);
        }

        $description = 'Đã đồng bộ lúc ' . date('h:i:s d/m/Y') . '. Kỳ đồng bộ tiếp theo lúc ' . date('h:i:s d/m/Y', get_term_meta_value($term_id, 'time_next_api_check'));
        audit('sync_spend', 'term', $term_id, '', $description, '', $this->admin_m->id, 'erp');

        return parent::responseHandler([], 'Dữ liệu đã được đồng bộ thành công. Vui lòng (F5) để xem sự thay đổi', 'success', 200);
    }

    /**
     * ĐỒNG BỘ TOÀN BỘ SỐ LIỆU THU CHI CỦA HỢP ĐỒNG zaloADS
     * DỰA TRÊN IDS HỢP ĐỐNG ĐƯỢC YÊU CẦU
     *
     * @return     json
     */
    public function sync_spend_put()
    {
        $ids     = parent::put('ids', TRUE);
        if (empty($ids)) return parent::response(['code' => REST_Controller::HTTP_BAD_REQUEST, 'message' => 'HTTP_BAD_REQUEST']);

        is_array($ids) or $ids = [$ids];
        $ids = array_unique(array_filter(array_map('intval', $ids)));

        $contracts = $this->zaloads_m->set_term_type()->where_in('term_id', $ids)->get_all();

        if (empty($contracts)) return parent::response(['code' => REST_Controller::HTTP_NO_CONTENT, 'message' => 'HTTP_NO_CONTENT']);

        $result = ['total' => count($contracts), 'completed' => 0, 'failed' => 0];

        foreach ($contracts as $contract) {
            $contract = (new zaloads_m())->set_contract($contract);
            try {
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_progress();
                $result['completed']++;
            } catch (Exception $e) {
                $result['failed']++;
                continue;
            }
        }

        return parent::response(['code' => REST_Controller::HTTP_OK, 'data' => $result]);
    }
}
/* End of file Adaccount.php */
/* Location: ./application/modules/zaloads/controllers/api_v2/Adaccount.php */