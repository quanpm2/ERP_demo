<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Resource extends MREST_Controller
{
    function __construct()
    {
        $this->autoload['libraries'][] = 'form_validation';
        $this->autoload['models'][]     = 'zaloads/zaloads_m';
        $this->autoload['models'][]     = 'zaloads/zaloads_kpi_m';
        $this->autoload['models'][]     = 'admin_m';
        $this->autoload['helpers'][]     = 'date';

        parent::__construct();
    }

    /**
     * @return json
     */
    public function setting_get($contractId)
    {
        if (!has_permission('zaloads.kpi.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), ['contractId' => $contractId]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->zaloads_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::response(['code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array()]);
        }

        // Process
        $kpis = $this->zaloads_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(['term_id' => $inputs['contractId']]);

        $data = [
            'kpi' => [],
        ];
        foreach ($kpis as $kpi) {
            $item = [];

            $item['kpi_id'] = $kpi['kpi_id'];
            $item['user_id'] = $kpi['user_id'];
            $item['display_name'] = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');
            $item['user_email'] = $this->admin_m->get_field_by_id($kpi['user_id'], 'user_email');

            $data['kpi'][] = $item;
        }

        return parent::responseHandler($data, 'Lấy dữ liệu thành công.');
    }

    public function kpi_staff_get()
    {
        if (!has_permission('zaloads.kpi.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        // $role_ids = $this->option_m->get_value('group_zaloads_ids', TRUE);
        $staffs = $this->admin_m->select('user_id, display_name, user_email')
            // ->where_in('role_id', $role_ids)
            ->set_get_active()
            ->order_by('display_name')
            ->as_array()
            ->get_all();
        if (empty($staffs)) {
            return parent::responseHandler(null, 'Không có dữ liệu kỹ thuật viên.', 'success', 201);
        }

        return parent::responseHandler($staffs, 'Lấy dữ liệu thành công.');
    }

    public function kpi_staff_post($contractId)
    {
        if (!has_permission('zaloads.kpi.add')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::post(), ['contractId' => $contractId]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->zaloads_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'staffId',
                'label' => 'staffId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', function ($value) {
                        return $this->admin_m->existed_check($value);
                    })
                ]
            ]
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::response(['code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array()]);
        }

        // Process
        // ??? Hop dong trang thai nao duoc add tech staff
        $kpi_datetime = time();
        $this->zaloads_kpi_m->update_kpi_value($inputs['contractId'], 'account_type', 1, $kpi_datetime, $inputs['staffId']);
        $this->term_users_m->set_relations_by_term($inputs['contractId'], [$inputs['staffId']], 'admin');
        $this->zaloads_m->set_contract($contractId)->get_behaviour_m()->setTechnicianId();

        return parent::responseHandler([], 'Thêm kỹ thuật thành công.');
    }

    public function kpi_staff_delete($contractId, $staffId)
    {
        if (!has_permission('zaloads.kpi.delete')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::delete(), [
            'contractId' => $contractId,
            'staffId' => $staffId
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->zaloads_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'staffId',
                'label' => 'staffId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->zaloads_kpi_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $this->zaloads_kpi_m->delete_cache($staffId);
        $this->zaloads_kpi_m->delete($staffId);

        return parent::responseHandler([], 'Xoá kỹ thuật thành công.');
    }
}
/* End of file Adaccount.php */
/* Location: ./application/modules/zaloads/controllers/api_v2/Adaccount.php */