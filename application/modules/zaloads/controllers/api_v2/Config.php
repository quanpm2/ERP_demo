<?php defined('BASEPATH') or exit('No direct script access allowed');

class Config extends MREST_Controller
{
    /**
     * API dữ liệu các đợt thanh toán
     */
    public function items_get($key = '')
    {
        $this->config->load('zaloads/zaloads', TRUE);
        $this->config->load('zaloads/contract', TRUE);

        $result = array_merge($this->config->item('zaloads'), $this->config->item('contract'));

        if (!empty($key)) $result = $this->config->item($key, 'zaloads');

        parent::response([
            'status' => TRUE,
            'data' => $result
        ]);
    }

    public function options_get($key = '')
    {
        parent::response([
            'status' => true,
            'data' => $this->option_m->get_value($key)
        ]);
    }
}
/* End of file Config.php */
/* Location: ./application/modules/zaloads/controllers/api_v2/Config.php */