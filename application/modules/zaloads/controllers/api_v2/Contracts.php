<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contracts extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'zaloads/zaloads_m';
        $this->autoload['models'][] = 'zaloads/zaloads_kpi_m';
        // $this->autoload['models'][] = 'zaloads/adaccount_m';
        $this->autoload['models'][] = 'ads_segment_m';
        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'log_m';
        $this->autoload['models'][] = 'balance_spend_m';

        parent::__construct($config);
    }

    public function index_get($id = 0)
    {
        $contract = $this->zaloads_m->set_term_type()->get($id);
        if (empty($contract)) parent::response(['status' => false, 'data' => null]);

        $contract->number_of_payments   = (int)(get_term_meta_value($id, 'number_of_payments') ?: 1);
        $contract->contract_budget_payment_type = get_term_meta_value($id, 'contract_budget_payment_type');
        $contract->contract_budget_customer_payment_type = get_term_meta_value($id, 'contract_budget_customer_payment_type');

        $contract->service_fee_payment_type = get_term_meta_value($id, 'service_fee_payment_type');

        $contract->adaccount_source = get_term_meta_value($id, 'adaccount_source') ?: 'internal';

        $contract->adaccounts = get_term_meta($id, 'adaccounts', FALSE, TRUE);
        $contract->adaccounts and $contract->adaccounts = array_filter(array_map('intval', $contract->adaccounts));

        $contract->advertise_start_time = (int) get_term_meta_value($id, 'advertise_start_time');
        $contract->advertise_end_time = (int) get_term_meta_value($id, 'advertise_end_time');
        $contract->contract_code = get_term_meta_value($id, 'contract_code');
        $contract->actual_budget = (int) get_term_meta_value($id, 'actual_budget');
        $contract->balanceBudgetReceived        = (int) get_term_meta_value($id, 'balanceBudgetReceived');

        $contract->service_fee          = (int) get_term_meta_value($id, 'service_fee');
        $contract->contract_budget      = (int) get_term_meta_value($id, 'contract_budget');

        /**
         * Danh sách chương trình khuyến mãi hợp đồng đang áp dụng
         */
        $_promotions = get_term_meta_value($id, 'promotions');
        if ($_promotions) {
            $_promotions = unserialize($_promotions);
            $_promotions = array_map(function ($x) {
                $x['value'] = (float) $x['value'];
                return $x;
            }, $_promotions);
        }
        $contract->promotions = $_promotions;
        $contract->apply_discount_type = get_term_meta_value($id, 'apply_discount_type') ?: 'last_invoice';

        $contract->is_service_proc    = (bool) is_service_proc($contract);
        $contract->is_service_end     = (bool) is_service_end($contract);

        $contract_m = (new zaloads_m())->set_contract($contract);
        $contract->actual_result = (float) $contract_m->get_behaviour_m()->get_actual_result();

        $contract_curators = get_term_meta_value($id, 'contract_curators');
        $contract_curators = is_serialized($contract_curators) ? unserialize($contract_curators) : [];
        $contract_curators = array_map(function ($item) {
            $item['guid'] = uniqid('contract_curators_');
            return $item;
        }, $contract_curators);
        $contract->curators = $contract_curators;

        $contract->is_display_promotions_discount =  (int) get_term_meta_value($id, 'is_display_promotions_discount');

        $contract->segments = $contract_m->getSegments();
        $contract->balance_spend = $contract_m->getBalanceSpend();

        $contract->deposit_amount = (int) get_term_meta_value($id, 'deposit_amount');

        parent::response(['status' => true, 'data' => $contract]);
    }

    /**
     * Update Zalo ads Contract
     *
     * @param      integer  $id     The identifier
     */
    public function index_put($id)
    {
        $contract = $this->zaloads_m->set_term_type()->get($id);
        if (empty($contract)) parent::response(['status' => false, 'data' => null]);

        if (FALSE === $this->contract_m->has_permission($id, 'admin.contract.update')) {
            parent::responseHandler([], 'Không thể thực hiện tác vụ, quyền truy cập bị hạn chế.', 'error', parent::HTTP_NOT_ACCEPTABLE);
        }

        $this->load->library('form_validation');

        if ($promotions = parent::put('promotions', TRUE)) {
            $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
            $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                            && FALSE == (bool)get_term_meta_value($id, 'is_manipulation_locked');
            if($is_manipulation_locked)
            {
                $started_service = (int) get_term_meta_value($id, 'started_service') ?: time();
                $started_service = end_of_day($started_service);
                $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
                if($manipulation_locked_at > $started_service)
                {
                    $manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                    parent::response([ 'code' => parent::HTTP_NOT_ACCEPTABLE, 'error' => "Hợp đồng đã khoá thao tác lúc {$manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá."]);
                }
            }

            $args = parent::put(null, TRUE);
            $this->form_validation->set_data($args);
            $this->form_validation->set_rules('apply_discount_type', 'apply_discount_type', 'required|in_list[average,last_invoice]');
            if (FALSE == $this->form_validation->run()) {
                parent::responseHandler([], $this->form_validation->error_array(), 'error', 400);
            }

            foreach ($promotions as $promotion) {
                $this->form_validation->set_data($promotion);
                $this->form_validation->set_rules('name', 'name', 'required');
                $this->form_validation->set_rules('value', 'value', 'required|numeric');
                $this->form_validation->set_rules('unit', 'unit', 'required|in_list[percentage,number]');

                if ($this->form_validation->run() != FALSE) continue;

                parent::responseHandler([], $this->form_validation->error_array(), 'error', 400);
            }

            $contract = (new contract_m)->set_contract($id);

            update_term_meta($id, 'apply_discount_type', $args['apply_discount_type']);
            update_term_meta($id, 'promotions', serialize($promotions));
            update_term_meta($id, 'discount_amount', (float) $contract->get_behaviour_m()->calc_disacount_amount());
            update_term_meta($id, 'contract_value', (float) $contract->get_behaviour_m()->calc_contract_value());
            update_term_meta($id, 'service_fee_rate_actual', (float) $contract->get_behaviour_m()->calc_service_fee_rate_actual());

            $contract->delete_all_invoices(); // Delete all invoices available
            $contract->create_invoices(); // Create new contract's invoices
            $contract->sync_all_amount(); // Update all invoices's amount & receipt's amount

            parent::responseHandler([], 'Số liệu thu chi đã được đồng bộ thành công !');
        }


        if ($curators = parent::put('curators', TRUE)) {
            foreach ($curators as $curator) {
                $this->form_validation->set_data($curator);
                $this->form_validation->set_rules('name', 'name', 'required');
                $this->form_validation->set_rules('email', 'value', 'valid_email');
                $this->form_validation->set_rules('phone', 'unit', 'min_length[9]|max_length[12]|numeric');

                if ($this->form_validation->run() != FALSE) continue;

                parent::responseHandler([], $this->form_validation->error_array(), 'error', 400);
            }

            update_term_meta($id, 'contract_curators', serialize($curators));

            parent::responseHandler([], 'Thông tin người nhận báo cáo đã được cập nhật thành công !');
        }

        parent::responseHandler([], 'Cập nhật thành công.');
    }

    /**
     * Get all activated by adaccount_id
     *
     * @param      int   $adaccount_id    The adaccount_id
     */
    public function activated_by_adaccount_id_get($adaccount_id = 0)
    {
        $args = parent::get();

        $m_args = array(
            'key' => 'adaccount_id', 'value' => (int) $adaccount_id, 'compare' => '='
        );

        $contracts = $this->zaloads_m->select('term.term_id, term_name, term_status, term.status')->m_find($m_args)->set_term_type()
            ->where_in('term_status', ['pending', 'publish'])
            ->get_all();

        $contracts and $contracts = array_map(function ($contract) {

            $contract->adaccount_id = (int) get_term_meta_value($contract->term_id, 'adaccount_id');
            $contract->advertise_start_time = (int) get_term_meta_value($contract->term_id, 'advertise_start_time');
            $contract->advertise_end_time = (int) get_term_meta_value($contract->term_id, 'advertise_end_time');
            $contract->contract_code = get_term_meta_value($contract->term_id, 'contract_code');
            $contract->actual_budget = (int) get_term_meta_value($contract->term_id, 'actual_budget');

            $contract_m = (new contract_m())->set_contract($contract);
            $contract->actual_result = (float) $contract_m->get_behaviour_m()->get_actual_result();

            return $contract;
        }, $contracts);

        parent::response([
            'status' => true,
            'data' => [
                'isActive' => !empty($contracts),
                'contracts' => $contracts
            ]
        ]);
    }

    /**
     * Determines if valid start date.
     *
     * @param      int   $term_id  The term identifier
     * @param      int   $adaccount_id      The adaccount_id
     */
    public function is_valid_start_date_get($term_id = 0, $adaccount_id = 0, $start_time = 0)
    {
        $contract   = new zaloads_m();
        $contract->set_contract($term_id);

        $contracts = $contract->get_conflict_by_start_date($adaccount_id, $start_time);

        parent::response([
            'status' => true,
            'data' => [
                'isValid' => empty($contracts),
                'contracts' => $contracts
            ]
        ]);
    }

    /**
     * { function_description }
     *
     * @param      int   $adaccount_id    The adaccount_id
     */
    public function related_by_adaccount_id_get($adaccount_id = 0)
    {
        $args = wp_parse_args(parent::get(null, true), array('adaccount_id' => $adaccount_id,  'term_id' => null));

        !empty($args['term_id']) and $this->zaloads_m->where('term.term_id !=', (int) $args['term_id']);

        $contracts = $this->zaloads_m->get_all_by_adaccount_id($adaccount_id);

        parent::response(['status' => true, 'data' => $contracts]);
    }

    /**
     * Calculates the metrics put.
     *
     * @param      int   $term_id  The term identifier
     */
    public function compute_metrics_put(int $term_id = 0)
    {
        $contract = (new zaloads_m())->set_contract($term_id);
        if (!$contract) parent::response(['code' => 400, 'data' => true]);

        try {
            $behaviour_m = $contract->get_behaviour_m();
            $behaviour_m->get_the_progress();
            $behaviour_m->sync_all_amount();
        } catch (Exception $e) {
            parent::response(['code' => 400, 'error' => $e->getMessage()]);
        }

        parent::response(['code' => 200]);
    }

    /**
     * Excuse the set of stop services process
     *
     * 1. Update advertise_end_time metadata
     * 1. Update end_service_time metadata with now()
     * 2. Change term_status to "ending"
     * 3. Log trace
     * 4. Send Overall E-mail with Adwords Data to someone
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     JSON
     */
    public function stop_service_put($term_id = 0)
    {
        $args       = wp_parse_args(parent::put(null, true), ['end_date' => my_date(time(), 'd-m-Y')]);

        $contract   = new zaloads_m();
        $contract->set_contract($term_id);

        // Restrict permission of current user
        if (!$contract->can('zaloads.stop_service.update')) parent::responseHandler([], 'Không có quyền thực hiện tác vụ này.', 'error', 403);


        // Check if the service is running then stop excute and return
        if ($contract->is_service_end($term_id)) parent::responseHandler([], 'Hợp đồng đã được đóng.');

        $end_time = end_of_day($args['end_date']);

        /* Load danh sách tất cả phân đoạn của hợp đồng */
        // $adssegments = $this->term_posts_m->get_term_posts($term_id, $this->ads_segment_m->post_type); 
        // if(empty($adssegments))
        // {
        //     $response['msg'] = 'Có lỗi xảy ra vì phân đoạn không được tìm thấy.';
        //     parent::response($response);
        // }

        // foreach ($adssegments as $adssegment)
        // {
        //     $_end_date = end_of_day($adssegment->end_date ?: $end_time);
        //     $this->ads_segment_m->set_post_type()->update($adssegment->post_id, [ 'end_date' => $_end_date ]);
        // }

        update_term_meta($term_id, 'advertise_end_time', time());

        // Main process
        $this->load->model('zaloads/zaloads_contract_m');
        $result = $this->zaloads_contract_m->stop_service($contract->get_contract(), $end_time);
        if (!$result) parent::responseHandler([], 'Quá trình xử lý không thành công.', 'error', 400);

        parent::responseHandler([], 'Hợp đồng đã được kết thúc.');
    }

    /**
     * Excuse the set of start services process
     * 
     * 1. Update Money Exchange Rate from VIETCOMBANK API
     * 2. Update Advertise start time if is not set
     * 3. Log trace action 
     * 4. Send activation email to all responsibilty users
     * 5. Detect if first contract
     * 6. Send SMS notify to contract's customer
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     JSON
     */
    public function proc_service_put($term_id = 0)
    {
        $argsDefault    = ['begin_date' => my_date(time(), 'd-m-Y')];
        $args           = wp_parse_args(parent::put(null, true), $argsDefault);

        $contract       = new zaloads_m();
        if (!$contract->set_contract($term_id)) parent::responseHandler([], 'Hợp đồng hết hiệu lực hoặc không tồn tại.', 'error', 400);

        // Restrict permission of current user
        if (!$contract->can('zaloads.start_service.update')) parent::responseHandler([], 'Không có quyền thực hiện tác vụ này.', 'error', 403);

        // Check if the service is running then stop excute and return
        if ($this->zaloads_m->is_service_proc($term_id)) parent::responseHandler([], 'Dịch vụ đã được thực hiện.');

        $kpi = $this->zaloads_kpi_m->get_kpis($term_id);
        $isExistKpi = count($kpi['user_id'] ?? []);
        if($isExistKpi <= 0){
            parent::responseHandler([], 'Dịch vụ chưa được cấu hình kỹ thuật.', 'error', 400);
        }

        // $segments = $this->term_posts_m->get_term_posts($term_id, $this->ads_segment_m->post_type);
        // if(empty($segments))
        // {
        //     $response['msg'] = 'Tài khoản quảng cáo cần phải được cấu hình trước khi thực hiện dịch vụ';
        //     parent::response($response);   
        // }

        $this->load->model('zaloads/zaloads_contract_m');
        $result  = $this->zaloads_contract_m->proc_service($contract->get_contract(), start_of_day($args['begin_date']));
        if (!$result) parent::responseHandler([], 'Quá trình xử lý không thành công.', 'error', 400);

        // Detect if contract is the first signature (tái ký | ký mới)
        $this->base_contract_m->detect_first_contract($term_id);

        // Send SMS to customer
        $this->load->model('contract/contract_report_m');
        $this->contract_report_m->send_sms_activation_2customer($term_id);

        parent::responseHandler([], 'Dịch vụ đã được kích hoạt thực hiện thành công.');
    }

    
}
/* End of file MCCReport.php */
/* Location: ./application/modules/zaloads/controllers/api_v2/MCCReport.php */