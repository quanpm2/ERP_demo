<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gws extends Admin_Controller {
    public $model = 'gws_m';
    public $term_type = 'gws';

    function __construct(){
        parent::__construct();

        $this->load->model('gws_m');
    }
    
    /**
     * index
     *
     * @return void
     */
    public function index(){
        restrict('Gws.Index.Access');

        $this->template->is_box_open->set(1);
        $this->template->title->set('Tổng quan dịch vụ Google Worksapce đang thực hiện');
        
        $this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
        $this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
        $this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
        $this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
        
        $this->template->javascript->add(base_url("dist/vGwsAdminIndexPage.js"));
        $this->template->javascript->add('plugins/daterangepicker/moment.min.js');
        $this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
        
        $this->template->publish(FALSE, ['admin_url' => admin_url()]);
    }
}