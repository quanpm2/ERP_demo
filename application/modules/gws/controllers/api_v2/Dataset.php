<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class Dataset extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'gws/gws_m';

        parent::__construct();
        
        $this->config->load('gws/gws_dataset_fields');
    }
    
    /**
     * config_get
     *
     * @return void
     */
    public function config_get()
    {
		parent::response(array('msg' => 'Dữ liệu tải thành công','data' => $this->config->item('datasource')));        
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('gws.overview.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $args = wp_parse_args(parent::get(), [
            'offset' => 0,
            'per_page' => 20,
            'cur_page' => 1,
            'is_filtering' => true,
            'is_ordering' => true
        ]);

        $this->filtering();

        $this->load->config('contract/contract');
        $contract_status = $this->config->item('contract_status');

        $this->load->config('gws/gws');
        $service_package = $this->config->item('service_package');

        $data['content'] = $this
            ->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')

            ->select("term.term_id AS term_id")
            ->select("term.term_type AS term_type")
            ->select("term.term_status AS term_status")
            ->select("MAX(IF(m_contract.meta_key = 'num_of_licences', m_contract.meta_value, NULL)) AS num_of_licences")
            ->select("MAX(IF(m_contract.meta_key = 'contract_code', m_contract.meta_value, NULL)) AS contract_code")
            ->select("MAX(IF(m_contract.meta_key = 'contract_value', m_contract.meta_value, NULL)) AS contract_value")
            ->select("MAX(IF(m_contract.meta_key = 'service_fee_rate', m_contract.meta_value, NULL)) AS service_fee_rate")
            ->select("MAX(IF(m_contract.meta_key = 'service_fee', m_contract.meta_value, NULL)) AS service_fee")
            ->select("MAX(IF(m_contract.meta_key = 'service_package', m_contract.meta_value, NULL)) AS service_package")
            ->select("MAX(IF(m_contract.meta_key = 'contract_begin', m_contract.meta_value, NULL)) AS contract_begin")
            ->select("MAX(IF(m_contract.meta_key = 'contract_end', m_contract.meta_value, NULL)) AS contract_end")
            ->select("MAX(IF(m_contract.meta_key = 'staff_business', m_contract.meta_value, NULL)) AS staff_business")
            ->select("customer.user_id AS customer_id")
            ->select("customer.user_type AS customer_type")

            ->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])

            ->add_column('contract_code', array('title' => 'Mã hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('cid', array('title' => 'Mã khách hàng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('term_status', array('title' => 'Trạng thái', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_begin', array('title' => 'Ngày bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_end', array('title' => 'Ngày kết thúc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_value', array('title' => 'GTHĐ (-VAT%)', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_fee_rate', array('title' => '% Phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_fee', array('title' => 'Phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_package', array('title' => 'Gói dịch vụ', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('num_of_licences', array('title' => 'Số lượng đăng ký', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('staff_business', array('title' => 'Nhân viên kinh doanh', 'set_select' => FALSE, 'set_order' => FALSE))

            ->add_column_callback('term_id', function ($data, $row_name) use ($contract_status, $service_package) {
                $data['cid'] = cid($data['customer_id'], $data['customer_type']);

                $data['term_status_raw'] = $data['term_status'];
                $data['term_status'] = @$contract_status[$data['term_status']];

                $data['service_package_raw'] = $data['service_package'];
                $data['service_package'] = @$service_package[$data['service_package_raw']]['label'];

                $data['contract_begin_raw'] = $data['contract_begin'];
                $data['contract_begin'] = my_date($data['contract_begin'], 'd-m-Y');

                $data['contract_end_raw'] = $data['contract_end'];
                $data['contract_end'] = my_date($data['contract_end'], 'd-m-Y');
                
                $data['staff_business_raw'] = $data['staff_business'];
                if(!empty($data['staff_business'])){
                    $staff_business = $this->admin_m->get_field_by_id($data['staff_business']);
                    $data['staff_business'] = [
                        'user_id' => $staff_business['user_id'],
                        'display_name' => $staff_business['display_name'],
                        'user_avatar' => $staff_business['user_avatar'],
                        'user_email' => $staff_business['user_email'],
                    ];
                }

                return $data;
            }, FALSE)

            ->from('term')
            ->join('term_users AS tu_customer', 'tu_customer.term_id = term.term_id')
            ->join('user AS customer', 'customer.user_id = tu_customer.user_id AND customer.user_type IN ("customer_company", "customer_person")')
            ->join('termmeta AS m_contract', 'm_contract.term_id = term.term_id AND m_contract.meta_key IN ("staff_business", "num_of_licences", "contract_code", "contract_value", "service_fee_rate", "service_fee",  "service_package", "contract_begin", "contract_end")', 'LEFT')

            ->where('term.term_type', $this->gws_m->term_type)
            ->where_in('term.term_status', $this->config->item('contract_status_on'))
            ->group_by('term.term_id')
            ->order_by('term.term_id', 'DESC');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];
        $data = $this->datatable_builder->generate($pagination_config);

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering()
    {
        restrict('gws.overview.access');

        $args = parent::get(NULL, TRUE);
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $alias = uniqid('contract_code_');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

            if($filter_contract_code)
            {   
                $this->datatable_builder->like("{$alias}.meta_value", $filter_contract_code);
                unset($args['where']['contract_code']);
            }

            if($sort_contract_code)
            {
                $this->datatable_builder->order_by("term.term_id", $sort_contract_code);
                unset($args['order_by']['contract_code']);
            }
        }
    }
}
/* End of file Dataset.php */
/* Location: ./application/modules/gws/controllers/api_v2/Dataset.php */