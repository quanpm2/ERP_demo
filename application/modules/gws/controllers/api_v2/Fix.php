<?php defined('BASEPATH') or exit('No direct script access allowed');

class Fix extends MREST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->config('gws');
        $this->load->model('gws/gws_m');
        $this->load->model('gws/gws_report_m');
    }

    /**
     * start_service_email_get
     *
     * @param  string|int $term_id
     * @return void
     */
    public function start_service_email_get($term_id){
        $status = $this->gws_report_m->send_mail_info_to_admin($term_id);
        dd($status);
    }

    /**
     * proc_service_email_get
     *
     * @param  string|int $term_id
     * @return void
     */
    public function proc_service_email_get($term_id){
        $status = $this->gws_report_m->send_mail_info_activated($term_id);
        dd($status);
    }

    /**
     * stop_service_get
     *
     * @param  string|int $term_id
     * @return void
     */
    public function stop_service_email_get($term_id){
        $status = $this->gws_report_m->send_mail_info_finish($term_id);
        dd($status);
    }
}
/* End of file Fix.php */
/* Location: ./application/modules/gws/controllers/api_v2/Fix.php */