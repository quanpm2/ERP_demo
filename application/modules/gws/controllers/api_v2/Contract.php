<?php defined('BASEPATH') or exit('No direct script access allowed');

class Contract extends MREST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('contract/base_contract_m');
        $this->load->model('gws/gws_m');
        $this->load->model('gws/gws_contract_m');
        $this->load->model('gws/gws_kpi_m');

        $this->load->config('gws');
    }

    /**
     * metadata_put
     *
     * @param  int|string $contract_id
     * @return void
     */
    public function metadata_put($contract_id)
    {
        $data = parent::put();

        $service_package_detail = get_term_meta_value($contract_id, 'service_package_detail');
        $num_of_term = get_term_meta_value($contract_id, 'num_of_term') ?: 1;
        $service_package_detail = @json_decode($service_package_detail, TRUE) ?: [];

        $term_unit = $service_package_detail['term_unit'];
        $month = 1;
        'year' == $term_unit AND $month = 12;

        $num_of_month = $num_of_term * $month;

        // Update contract_begin and contract_end
        $contract_begin = (int)get_term_meta_value($contract_id, 'contract_begin');
        $_contract_end = (int)get_term_meta_value($contract_id, 'contract_begin');;
        $contract_end = strtotime("+{$num_of_month} months", $contract_begin);
        if($_contract_end != $contract_end){
            update_term_meta($contract_id, 'contract_end', $contract_end);
        }

        $email_accounts = $data['email_accounts'] ?? [];
        $this->termmeta_m->delete_meta($contract_id, 'email_accounts');
        if(!empty($email_accounts)){
            array_walk($email_accounts, function($email_account) use ($contract_id){
                $this->termmeta_m->add_meta($contract_id, 'email_accounts', $email_account);
            });
        }
        unset($data['email_accounts']);

        foreach($data as $key => $value){
            if(!is_array($value)){
                update_term_meta($contract_id, $key, $value);
                continue;
            }
            
            update_term_meta($contract_id, $key, json_encode($value));
        }

        parent::responseHandler($data, "Cập nhật dữ liệu thành công");
    }

    /**
     * start_service_put
     *
     * @param  int|string $contract_id
     * @return void
     */
    public function start_service_put($contract_id)
    {
        if(!has_permission('gws.start_service.manage')) return parent::responseHandler([], "Không có quyền thực hiên tác vụ!", 'error', 403);
        
        $contract = $this->gws_m->set_contract($contract_id);
        if(empty($contract)) return parent::responseHandler([], "Không tìm thấy hợp đồng!", 'error', 400);

        $count_tech = $this->gws_kpi_m
            ->select('user_id')
            ->group_by('user_id')
            ->count_by(['term_id' => $contract_id]);
        if($count_tech < 1) return parent::responseHandler([], "Dịch vụ chưa được phân công phục trách!", 'error', 400);

        $contract = $contract->get_contract();
        $is_proc = $this->gws_contract_m->proc_service($contract);
        if(!$is_proc) return parent::responseHandler([], "Tiến trình Kích hoạt hợp đồng bị lỗi! Vui lòng liên hệ bộ phận Công nghệ để xử lý.", 'error', 400);

        parent::responseHandler([], "Kích hoạt thành công ");
    }

    /**
     * stop_service_put
     *
     * @param  int|string $contract_id
     * @return void
     */
    public function stop_service_put($contract_id)
    {
        if(!has_permission('gws.stop_service.manage')) return parent::responseHandler([], "Không có quyền thực hiên tác vụ!", 'error', 403);
        
        $contract = $this->gws_m->set_contract($contract_id);
        if(empty($contract)) return parent::responseHandler([], "Không tìm thấy hợp đồng!", 'error', 400);

        $contract = $contract->get_contract();
        $is_stop = $this->gws_contract_m->stop_service($contract);
        if(!$is_stop) return parent::responseHandler([], "Tiến trình Kết thúc hợp đồng bị lỗi! Vui lòng liên hệ bộ phận Công nghệ để xử lý.", 'error', 400);

        parent::responseHandler([], "Kết thúc thành công ");
    }
    
    /**
     * add_tech_staff_post
     *
     * @param  mixed $contract_id
     * @param  mixed $tech_id
     * @return void
     */
    public function add_tech_staff_post($contract_id, $tech_id){
        if (!has_permission('gws.kpi.add')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);

        $contract = $this->gws_m->set_contract($contract_id);
        if(empty($contract)) return parent::responseHandler([], "Không tìm thấy hợp đồng!", 'error', 400);

        if(!$this->admin_m->existed_check($tech_id)) return parent::responseHandler([], "Không tìm thấy kỹ thuật!", 'error', 400);

        $kpi_datetime = time();
        $this->gws_kpi_m->update_kpi_value($contract_id, 'tech', 1, $kpi_datetime, $tech_id);
        $this->term_users_m->set_relations_by_term($contract_id, [$tech_id], 'admin');
        $this->gws_m->set_contract($contract_id)->get_behaviour_m()->setTechnicianId();

        return parent::responseHandler([], 'Thêm kỹ thuật thành công.');
    }
    
    /**
     * remove_tech_staff_post
     *
     * @param  mixed $contract_id
     * @param  mixed $tech_id
     * @return void
     */
    public function remove_tech_staff_delete($contract_id, $tech_id){
        if (!has_permission('gws.kpi.add')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);

        $contract = $this->gws_m->set_contract($contract_id);
        if(empty($contract)) return parent::responseHandler([], "Không tìm thấy hợp đồng!", 'error', 400);

        if(!$this->admin_m->existed_check($tech_id)) return parent::responseHandler([], "Không tìm thấy kỹ thuật!", 'error', 400);

        $kpis = $this->gws_kpi_m->get_kpis($contract_id, 'tech', 1, 0, $tech_id);
        if(empty($kpis['user_id'][$tech_id])) return parent::responseHandler([], "Kỹ thuật không được gán cho hợp đồng này!", 'error', 400);
        
        $this->gws_kpi_m->delete_by(['term_id' => $contract_id, 'user_id' => $tech_id]);

        return parent::responseHandler([], 'Xoá kỹ thuật thành công.');
    }
}
/* End of file Resource.php */
/* Location: ./application/modules/gws/controllers/api_v2/Resource.php */