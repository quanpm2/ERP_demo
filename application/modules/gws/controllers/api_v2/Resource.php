<?php defined('BASEPATH') or exit('No direct script access allowed');

class Resource extends MREST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->config('gws');
        $this->load->model('gws/gws_m');
        $this->load->model('gws/gws_kpi_m');
    }

    /**
     * config_get
     *
     * @return void
     */
    public function config_get()
    {
        $service_package = $this->config->item('service_package');
        $term_unit = $this->config->item('term_unit');

        parent::responseHandler([
            'service_package' => $service_package,
            'term_unit' => $term_unit,
        ], "Lấy dữ liệu thành công");
    }
    
    /**
     * contract_service_get
     *
     * @param  string|int $term_id
     * @return void
     */
    public function contract_service_get($term_id){
        $contract = (new gws_m())->set_contract($term_id);
        $contract = $contract->get_contract();
        if(empty($contract)) return parent::responseHandler([], 'Hợp đồng không khả dụng', 'error', 403);
        
        $service_package = get_term_meta_value($term_id, 'service_package');
        $service_package_detail = @json_decode(get_term_meta_value($term_id, 'service_package_detail'), TRUE);
        
        $contract_code = get_term_meta_value($term_id, 'contract_code');
        $num_of_term = get_term_meta_value($term_id, 'num_of_term');
        $num_of_licences = get_term_meta_value($term_id, 'num_of_licences');
        $service_fee_rate = (float) get_term_meta_value($term_id, 'service_fee_rate');
        $vat = get_term_meta_value($term_id, 'vat');
        $fct = get_term_meta_value($term_id, 'fct');
        $promotions = @json_decode(get_term_meta_value($term_id, 'promotions'), TRUE) ?: [];
        $apply_discount_type = get_term_meta_value($term_id, 'apply_discount_type') ?: 'last_invoice';
        $contract_begin = get_term_meta_value($term_id, 'contract_begin');
        $contract_end = get_term_meta_value($term_id, 'contract_end');
        $start_service_time = get_term_meta_value($term_id, 'start_service_time');
        $contract_value = get_term_meta_value($term_id, 'contract_value');

        $email_accounts = get_term_meta($term_id, 'email_accounts');
        $email_accounts = array_column($email_accounts, 'meta_value');

        $is_service_start = is_service_start($term_id);
        $is_service_proc = is_service_proc($term_id);
        $is_service_end = is_service_end($term_id);

        $staff_business_id = get_term_meta_value($term_id, 'staff_business');
        $staff_business = NULL;
        if(!empty($staff_business_id)){
            $staff_business = $this->admin_m->get_field_by_id($staff_business_id);
            $staff_business = [
                'user_id' => $staff_business['user_id'],
                'display_name' => $staff_business['display_name'],
                'user_avatar' => $staff_business['user_avatar'],
                'user_email' => $staff_business['user_email'],
            ];
        }

        $techs = [];
        $tech_kpi = $this->gws_kpi_m
            ->select('user_id')
            ->group_by('user_id')
            ->get_many_by(['term_id' => $term_id]);
        if (!empty($tech_kpi)) {
            foreach ($tech_kpi as $i) {
                $tech = [];

                $tech['user_id'] = $i->user_id;

                $name  = $this->admin_m->get_field_by_id($i->user_id, 'display_name');
                $tech['display_name'] = $name;

                $phone = get_user_meta_value($i->user_id, 'user_phone');
                $tech['user_phone'] = $phone;

                $mail  = $this->admin_m->get_field_by_id($i->user_id, 'user_email');
                $tech['user_email'] = $mail;

                $avatar  = $this->admin_m->get_field_by_id($i->user_id, 'user_avatar');
                $tech['user_avatar'] = $avatar;

                $techs[] = $tech;
            }
        }

        $response_data = [
            'term_id' => $term_id,
            'contract_code' => $contract_code,
            'staff_business' => $staff_business,
            'techs' => $techs,
            'domain' => $contract->term_name,
            'service_package' => $service_package,
            'service_package_detail' => $service_package_detail,
            'num_of_term' => $num_of_term,
            'num_of_licences' => $num_of_licences,
            'service_fee_rate' => $service_fee_rate,
            'vat' => $vat,
            'fct' => $fct,
            'email_accounts' => $email_accounts,
            'promotions' => $promotions,
            'apply_discount_type' => $apply_discount_type,
            'contract_begin' => my_date($contract_begin, 'd-m-Y'),
            'contract_end' => my_date($contract_end, 'd-m-Y'),
            'start_service_time' => $start_service_time ? my_date($start_service_time, 'd-m-Y') : null,
            'is_service_start' => $is_service_start,
            'is_service_proc' => $is_service_proc,
            'is_service_end' => $is_service_end,
            'contract_value' => $contract_value,
        ];
        return parent::responseHandler($response_data, 'Lấy dữ liệu hợp đồng thành công');
    }
    
    /**
     * tech_staffs_get
     *
     * @param  mixed $term_id
     * @return void
     */
    public function tech_staffs_get(){
        if (!has_permission('gws.kpi.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $role_ids = $this->option_m->get_value('group_fb_ads_role_ids', TRUE);
        $staffs = $this->admin_m->select('user_id, display_name, user_email')
            ->where_in('role_id', $role_ids)
            ->set_get_active()
            ->order_by('display_name')
            ->as_array()
            ->get_all();
        if (empty($staffs)) {
            return parent::responseHandler(null, 'Không có dữ liệu kỹ thuật viên.', 'success', 201);
        }

        return parent::responseHandler($staffs, 'Lấy dữ liệu thành công.');
    }
}
/* End of file Resource.php */
/* Location: ./application/modules/gws/controllers/api_v2/Resource.php */