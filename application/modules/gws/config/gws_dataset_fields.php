<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$columns = array(
    'contract_code' => array(
        'name' => 'contract_code',
        'label' => 'Mã hợp đồng',
        'set_select' => false,
        'title' => 'Mã hợp đồng',
        'type' => 'string',
        'align' => 'left'
    ),
    'cid' => array(
        'name' => 'cid',
        'label' => 'Mã khách hàng',
        'set_select' => false,
        'title' => 'Mã khách hàng',
        'type' => 'string',
        'align' => 'left'
    ),
    'term_status' => array(
        'name' => 'term_status',
        'label' => 'Trạng thái',
        'set_select' => false,
        'title' => 'Trạng thái',
        'type' => 'string',
        'align' => 'left'
    ),
    'contract_begin' => array(
        'name' => 'contract_begin',
        'label' => 'Ngày bắt đầu',
        'set_select' => false,
        'title' => 'Ngày bắt đầu',
        'type' => 'string',
        'align' => 'right'
    ),
    'contract_end' => array(
        'name' => 'contract_end',
        'label' => 'Ngày kết thúc',
        'set_select' => false,
        'title' => 'Ngày kết thúc',
        'type' => 'string',
        'align' => 'right'
    ),
    'contract_value' => array(
        'name' => 'contract_value',
        'label' => 'GTHĐ (-VAT%)',
        'set_select' => false,
        'title' => 'GTHĐ (-VAT%)',
        'type' => 'number',
        'align' => 'right'
    ),
    'service_fee_rate' => array(
        'name' => 'service_fee_rate',
        'label' => '% Phí dịch vụ',
        'set_select' => false,
        'title' => '% Phí dịch vụ',
        'type' => 'string',
        'align' => 'right'
    ),
    'service_fee' => array(
        'name' => 'service_fee',
        'label' => 'Phí dịch vụ',
        'set_select' => false,
        'title' => 'Phí dịch vụ',
        'type' => 'number',
        'align' => 'right'
    ),
    'service_package' => array(
        'name' => 'service_package',
        'label' => 'Gói dịch vụ',
        'set_select' => false,
        'title' => 'Gói dịch vụ',
        'type' => 'string',
        'align' => 'left'
    ),
    'num_of_licences' => array(
        'name' => 'num_of_licences',
        'label' => 'Số lượng đăng ký',
        'set_select' => false,
        'title' => 'Số lượng đăng ký',
        'type' => 'number',
        'align' => 'right'
    ),
    'staff_business' => array(
        'name' => 'staff_business',
        'label' => 'Nhân viên Kinh doanh',
        'set_select' => false,
        'title' => 'Nhân viên Kinh doanh',
        'type' => 'string',
        'align' => 'left'
    ),
);

$config['datasource'] = array(
    'columns' => $columns,
    'default_columns' => array(
        'contract_code',
        'cid',
        'term_status',
        'contract_begin',
        'contract_end',
        'contract_value',
        'service_fee_rate',
        'service_fee',
        'service_package',
        'num_of_licences',
        'staff_business'
    )
);
unset($columns);