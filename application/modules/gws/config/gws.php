<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['service_package'] = array(
    'starter' => [
        'name' => 'starter',
        'label' => 'GWS Starter',
        'is_active' => true,
        'term_unit' => 'year',
        'img_slug' => '//template/admin/modules/gws/imgs/google_apps_work_64.svg',
        'drive_storage' => 30,
        'price_plan' => [
            '1_20' => [
                'label' => '1 ~ 20 emails',
                'min_email' => 1,
                'max_email' => 20,
                'unit' => 'month',
                'price_per_email' => 111000
            ],
            '21_300' => [
                'label' => '21 ~ 300 emails',
                'min_email' => 21,
                'max_email' => 300,
                'unit' => 'month',
                'price_per_email' => 143000
            ]
        ],
        'min_email' => 1,
        'max_email' => 300,
    ],
    'standard' => [
        'name' => 'standard',
        'label' => 'GWS Standard',
        'is_active' => true,
        'term_unit' => 'year',
        'img_slug' => '//template/admin/modules/gws/imgs/google_apps_work_64.svg',
        'drive_storage' => 30,
        'price_plan' => [
            '1_20' => [
                'label' => '1 ~ 20 emails',
                'min_email' => 1,
                'max_email' => 20,
                'unit' => 'month',
                'price_per_email' => 211000
            ],
            '21_300' => [
                'label' => '21 ~ 300 emails',
                'min_email' => 21,
                'max_email' => 300,
                'unit' => 'month',
                'price_per_email' => 264000
            ]
        ],
        'min_email' => 1,
        'max_email' => 300,
    ],
    'plus' => [
        'name' => 'plus',
        'label' => 'GWS Plus',
        'is_active' => true,
        'term_unit' => 'year',
        'img_slug' => '//template/admin/modules/gws/imgs/google_apps_work_64.svg',
        'drive_storage' => 30,
        'price_plan' => [
            '1_20' => [
                'label' => '1 ~ 20 emails',
                'min_email' => 1,
                'max_email' => 20,
                'unit' => 'month',
                'price_per_email' => 367000
            ],
            '21_300' => [
                'label' => '21 ~ 300 emails',
                'min_email' => 21,
                'max_email' => 300,
                'unit' => 'month',
                'price_per_email' => 421000
            ]
        ],
        'min_email' => 1,
        'max_email' => 300,
    ],
    'enterprise' => [
        'name' => 'enterprise',
        'label' => 'GWS Enterprise',
        'is_active' => true,
        'term_unit' => 'year',
        'img_slug' => '//template/admin/modules/gws/imgs/google_apps_work_64.svg',
        'drive_storage' => 30,
        'price_plan' => [
            '1_20' => [
                'label' => '1 ~ 20 emails',
                'min_email' => 1,
                'max_email' => 20,
                'unit' => 'month',
                'price_per_email' => 0
            ],
            '21_300' => [
                'label' => '21 ~ 300 emails',
                'min_email' => 21,
                'max_email' => 300,
                'unit' => 'month',
                'price_per_email' => 0
            ],
            '300_more' => [
                'label' => '> 300 emails',
                'min_email' => 301,
                'max_email' => PHP_INT_MAX,
                'unit' => 'month',
                'price_per_email' => 0
            ]
        ],
        'min_email' => 1,
        'max_email' => PHP_INT_MAX,
    ],
);

$config['term_unit'] = [
    'year' => 'Năm',
    'quarter' => 'Quý',
    'month' => 'Tháng',
];