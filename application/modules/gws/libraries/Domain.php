<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Domain
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
	}

	public function whois($domain = '')
	{
		$raw_data = $this->query($domain);
		$raw_data['Expired Date'] = strtotime($raw_data['Expired Date']);
		return $raw_data;
	}
	
	public function age($domain = '')
	{

	}

	private function query($domain = '')
	{
		$kq = file_get_contents("http://www.whois.net.vn/whois.php?act=getwhois&domain=".$domain);
		return $this->parse_data($kq);

	}

	private function parse_data($data)
	{
		if(!$data)
			return false;
		$data = strip_tags($data,'<br>');
		$data = trim($data);

		$result = array();
		$result['domain'] = $this->get_between($data, 'Domain :','<br>',1);
		$result['status'] = $this->get_between($data, 'Status :','<br>',1);
		$result['Issue Date'] = $this->get_between($data, 'Issue Date :','<br>',1);
		$result['Expired Date'] = $this->get_between($data, 'Expired Date :','<br>',1);
		$result['DNS'] = $this->get_between($data, 'DNS :','<br>',1);
		foreach($result as &$r)
		{
			$r = trim($r);
		}
		return $result;
	}

	function get_between($content,$start,$end, $position = 0){
		$start = str_replace(array('/','<','>','"'), array('\/','\<','\>','\"'), $start);
		$end = str_replace(array('/','<','>','"'), array('\/','\<','\>','\"'), $end);
		$regexp = "".$start."(.*?)".$end."";
		if(preg_match("/$regexp/is", $content, $matches)) {
			return isset($matches[$position])?$matches[$position]:'';
		}
		return '';
	}	
}

/* End of file Domain.php */
/* Location: ./application/modules/website/libraries/Domain.php */
