<?php
class Gws_Package extends Package
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * name
     *
     * @return void
     */
    public function name()
    {
        return 'GWS';
    }

    /**
     * title
     *
     * @return void
     */
    public function title()
    {
        return 'Gws';
    }

    /**
     * author
     *
     * @return void
     */
    public function author()
    {
        return 'Tan';
    }

    /**
     * version
     *
     * @return void
     */
    public function version()
    {
        return '0.1';
    }

    /**
     * description
     *
     * @return void
     */
    public function description()
    {
        return 'Google workspace service';
    }

    /**
     * init
     *
     * @return void
     */
    public function init()
    {
        $this->_load_menu();
    }

    /**
     * _load_menu
     *
     * @return void
     */
    public function _load_menu()
    {
        if (!is_module_active('gws')) return FALSE;

        $order = 1;
        $itemId = 'admin-gws';
        if (has_permission('gws.Index.access')) {
            $this->menu->add_item(array(
                'id' => $itemId,
                'name' => 'GWS',
                'parent' => null,
                'slug' => admin_url('gws'),
                'order' => $order++,
                'icon' => 'fa fa-cloud',
                'is_active' => is_module('gws')
            ), 'left-external-service');
        }
    }

    /**
     * _update_permissions
     *
     * @return void
     */
    private function _update_permissions()
    {
        $permissions = array();

        if (!$permissions) return false;

        foreach ($permissions as $name => $value) {
            $description = $value['description'];
            $actions = $value['actions'];
            $this->permission_m->add($name, $actions, $description);
        }
    }

    /**
     * init_permissions
     *
     * @return void
     */
    private function init_permissions()
    {
        $permissions = array();
        $permissions['Admin.Gws'] = array(
            'description' => 'Quản lý Gws',
            'actions' => array('view', 'add', 'edit', 'delete', 'update')
        );

        $permissions['Gws.Index'] = array(
            'description' => 'Trang chính',
            'actions' => array('manage', 'access')
        );

        $permissions['Gws.Overview'] = array(
            'description' => 'Trang tổng quan',
            'actions' => array('manage', 'access')
        );

        $permissions['Gws.Setting'] = array(
            'description' => 'Quản lý cấu hình',
            'actions' => array('manage', 'access', 'add', 'delete', 'update')
        );

        $permissions['Gws.Kpi'] = array(
            'description' => 'Bảng phân công KPI',
            'actions' => array('manage', 'access', 'add', 'delete', 'update')
        );

        $permissions['Gws.Start_service'] = array(
            'description' => 'Thực hiện dịch vụ',
            'actions' => array('manage')
        );

        $permissions['Gws.Stop_service'] = array(
            'description' => 'Kết thúc dịch vụ',
            'actions'       => array('manage')
        );

        $permissions['Contract.Gws'] = array(
            'description' => 'Trang chính',
            'actions' => array('manage', 'access')
        );

        return $permissions;
    }

    /**
     * install
     *
     * @return void
     */
    public function install()
    {
        $permissions = $this->init_permissions() ?: [];
        if (!$permissions) return false;

        foreach ($permissions as $name => $value) {
            $description = $value['description'];
            $actions = $value['actions'];
            $this->permission_m->add($name, $actions, $description);
        }
    }

    /**
     * uninstall
     *
     * @return void
     */
    public function uninstall()
    {
        $permissions = $this->init_permissions() ?: [];
        if (!$permissions) return false;

        foreach ($permissions as $name => $value) {
            $this->permission_m->delete_by_name($name);
        }
    }
}
