<!-- thong tin khach hang -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td height="15"></td>
        </tr>
        <tr>
            <td align="center">
                <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #f58220;">
                                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <!--Title-->
                                            <td height="40" align="center" bgcolor="#f58220" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin hỗ trợ</td>
                                            <!--End title-->
                                            <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape_adsplus.png" width="25" height="40" alt="img"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--Space-->
                                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                    <tbody>
                                        <tr>
                                            <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left: 24px;">&nbsp;</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--End Space-->
                                <!--detail-->
                                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--end detail-->
                            </td>
                        </tr>
                        <!--start Article-->
                        <tr>
                            <td bgcolor="#ffffff">
                                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <!--Content-->
                                        <?php if(!empty($staff)): ?>
                                        <tr>
                                            <td align="left" style="font-family: open sans, arial, sans-serif; font-size:16px; color:#444444; line-height:28px; font-weight: bold;">
                                                Nhân viên Kinh doanh
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px; padding-bottom: 16px">
                                                <!--Content-->
                                                <table align="left" border="0" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <img style="display:block; line-height:0px; font-size:0px; border:0px;" class="img1" src="<?= $staff['user_avatar']; ?>" width="100" height="100" alt="img">
                                                            </td>
                                                            <td width="16"></td>
                                                            <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                                                <span style="font-size: 16px"><?= $staff['display_name']; ?></span>
                                                                <br>
                                                                <span>Điện thoại: <?= $staff['user_phone'] ?: '(08) 6273.6363'; ?></span>
                                                                <br>
                                                                <span>Email: <?= $staff['user_email']; ?></span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($techs)) : ?>
                                            <tr>
                                                <td align="left" style="font-family: open sans, arial, sans-serif; font-size:16px; color:#444444; line-height:28px; font-weight: bold;">
                                                    Nhân viên Kỹ thuật
                                                </td>
                                            </tr>
                                            <?php foreach ($techs as $tech) : ?>
                                                <tr>
                                                    <td align="left" style="font-family: open sans, arial, sans-serif; font-size:16px; color:#444444; line-height:28px; font-weight: bold; padding-bottom: 20px">
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;" class="img1" src="<?= $tech['avatar']; ?>" width="100" height="100" alt="img">
                                                                    </td>
                                                                    <td width="16"></td>
                                                                    <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                                                        <span style="font-size: 16px"><?= $tech['name']; ?></span>
                                                                        <br>
                                                                        <span>Điện thoại: <?= $tech['phone']; ?></span>
                                                                        <br>
                                                                        <span>Email: <?= $tech['email']; ?></span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <!--End Content-->
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!--end Article-->
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="15"></td>
        </tr>
    </tbody>
</table>
<!-- thong tin khach hang -->