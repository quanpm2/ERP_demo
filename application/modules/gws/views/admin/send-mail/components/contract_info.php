<?php
$start_time  = get_term_meta_value($term->term_id, 'contract_begin');
$start_time  = $this->mdate->startOfDay($start_time);
$end_time    = get_term_meta_value($term->term_id, 'contract_end');
$end_time    = $this->mdate->startOfDay($end_time);
?>

<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td height="15"></td>
        </tr>
        <tr>
            <td align="center">
                <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #f58220;">
                                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <!--Title-->
                                            <td height="40" align="center" bgcolor="#f58220" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin hợp đồng</td>
                                            <!--End title-->
                                            <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape_adsplus.png" width="25" height="40" alt="img"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--Space-->
                                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                    <tbody>
                                        <tr>
                                            <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left: 24px;">&nbsp;</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--End Space-->
                                <!--detail-->
                                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--end detail-->
                            </td>
                        </tr>
                        <!--start Article-->
                        <tr>
                            <td bgcolor="#ffffff">
                                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <?php
                                        $service_date = my_date($start_time, 'd/m/Y') . ' - ' . my_date($end_time, 'd/m/Y');
                                        $contract_date = my_date(get_term_meta_value($term->term_id, 'contract_begin'), 'd/m/Y') . ' - ' . my_date(get_term_meta_value($term->term_id, 'contract_end'), 'd/m/Y');
                                        $contract_budget = (!get_term_meta_value($term->term_id, 'contract_budget')) ? 0 : (int) get_term_meta_value($term->term_id, 'contract_budget');
                                        $service_fee     = (!get_term_meta_value($term->term_id, 'service_fee')) ? 0 : (int) get_term_meta_value($term->term_id, 'service_fee');
                                        $num_of_licences = (int) get_term_meta_value($term->term_id, 'num_of_licences');
                                        $num_of_term = get_term_meta_value($term->term_id, 'num_of_term');
                                        $vat_rate = (float) get_term_meta_value($term->term_id, 'vat');
                                        $service_fee_rate = (float) get_term_meta_value($term->term_id, 'service_fee_rate');
                                        $discount_amount = (int) get_term_meta_value($term->term_id, 'discount_amount');
                                        $contract_value = (int) get_term_meta_value($term->term_id, 'contract_value');

                                        $email_accounts = get_term_meta($term->term_id, 'email_accounts');
                                        $email_accounts = array_filter(array_column($email_accounts, 'meta_value'));

                                        $service_package_detail = @json_decode(get_term_meta_value($term->term_id, 'service_package_detail'), TRUE);

                                        $service_total_price = 0;
                                        $_num_of_licences = $num_of_licences;
                                        foreach($service_package_detail['price_plan'] as $price_plan){
                                            if(1 > $_num_of_licences) break;

                                            $min_email = $price_plan['min_email'];
                                            $max_email = $price_plan['max_email'];
                                            $num_of_seat = $max_email - $min_email + 1;

                                            if($_num_of_licences > $num_of_seat){
                                                $service_total_price += $num_of_seat * $price_plan['price_per_email'];
                                                $_num_of_licences -= $num_of_seat;

                                                continue;
                                            }
                                            
                                            $service_total_price += $_num_of_licences * $price_plan['price_per_email'];
                                            $_num_of_licences = 0;
                                        };

                                        $term_unit = $service_package_detail['term_unit'] ?? 'month';
                                        $month = 1;
                                        'year' == $term_unit AND $month = 12;

                                        $service_price = $service_total_price * $month * $num_of_term;
                                        $vat = round($contract_value * div($vat_rate, 100));
                                        $service_fee = round($service_price * div($service_fee_rate, 100));

                                        $email_list = '--';
                                        if(!empty($email_accounts)) $email_list = implode('<br>', $email_accounts);

                                        $rows = array();
                                        $rows[] = array('Mã hợp đồng', get_term_meta_value($term->term_id, 'contract_code') ?: '--');
                                        $rows[] = array('Thời gian hợp đồng', $contract_date);
                                        $rows[] = array('Gói dịch vụ', $service_package_detail['label'] ?? '--');
                                        $rows[] = array('Đơn vị tính', $term_unit);
                                        $rows[] = array('Số kỳ đăng ký', $num_of_term ?: '--');
                                        $rows[] = array('Số lượng đăng ký', ($num_of_licences ?? '--') . ' user');
                                        $rows[] = array('Thành tiền', currency_numberformat($service_price, 'đ'));
                                        $rows[] = array("Phí dịch vụ {$service_fee_rate}%", currency_numberformat($service_fee, 'đ'));
                                        $rows[] = array("Giảm giá & Khuyến mãi", currency_numberformat($discount_amount, 'đ'));
                                        $rows[] = array("Giá trị hợp đồng trước thuế", currency_numberformat($contract_value, 'đ'));
                                        $rows[] = array("VAT {$vat_rate}%", currency_numberformat($vat, 'đ'));
                                        $rows[] = array("Giá trị hợp đồng sau thuế", currency_numberformat($contract_value + $vat, 'đ'));
                                        $rows[] = array("Danh sách email đăng ký", $email_list ?: '--');

                                        foreach ($rows as $i => $row) :
                                        ?>
                                            <?php $bg = (($i % 2 == 0) ? 'background: #f2f2ff;' : ''); ?>
                                            <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px; vertical-align: top"><?php echo $row[0]; ?>: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1]; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!--end Article-->
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="15"></td>
        </tr>
    </tbody>
</table>