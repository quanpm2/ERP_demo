<?php
$this->load->view('header');
$content_style = 'font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;';
?>
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="15"></td>
    </tr>
    <tr>
        <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #f58220;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                            
                                <td height="40" align="center" bgcolor="#f58220" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Kích hoạt dịch vụ</td>
                            
                                <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape_adsplus.png" width="25" height="40" alt="img" /></td>
                            </tr>
                        </table>
                    
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                            <tr>
                                <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                </td>
                            </tr>
                        </table>
                    
                    
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">
                                    Thông báo tiếp nhận quá trình On-boarding
                                </td>
                            </tr>
                        </table>
                    
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff">

                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td height="25"></td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                        <p>
                                            Dear quý khách , 
                                            <br/>
                                            Adsplus đã tiếp nhận và bắt đầu thiết lập cài đặt dịch vụ Google Workspace của quý khách trong vòng 2-24 giờ làm việc.
                                            <br/>
                                            Cảm ơn quý khách đã tin tưởng lựa chọn sử dụng dịch vụ Google Workspace.
                                        </p>
                                    </td>
                                </tr>
                            
                                <tr>
                                    <td align="left" height="30"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <?php if ( 1==2) : ?>
                <tr>
                    <td bgcolor="#ffffff">
                
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td height="25"></td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">

                                        <h4>
                                            Script 02 : trong trường hợp Adsplus cần khách hàng cung cấp thông tin truy cập
                                        </h4>
                                        <p>
                                            Dear quý khách , 
                                            
                                            <br/>
                                            Theo quy định và chính sách của Google nhằm đảm bảo minh bạch trong quyền sở hữu tài sản số . 
                                            Quý khách hàng vui lòng bổ sung thông tin để Adsplus hoàn thiện các bước còn lại .

                                            <br/>
                                            Adsplus đã tiếp nhận và báo cáo tiến độ hiện tại : 
                                            <br/>
                                        </p>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="15"></td>
                                </tr>

                                <tr>
                                    <td class="esd-structure es-p5t es-p5b es-p20r es-p20l esdev-adapt-off" align="left">
                                        <table class="esdev-mso-table" width="560" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="30" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-image es-m-txt-c" style="font-size: 0px;" align="right">
                                                                                        <a target="_blank">
                                                                                            <img src="https://e7.pngegg.com/pngimages/733/303/png-clipart-check-check-mark-computer-icons-x-mark-check-mark-icon-green-miscellaneous-angle-thumbnail.png" alt style="display: block;" width="25">
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="20"></td>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="510" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-l es-p5t es-p5b" align="left">
                                                                                        <p style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                                                                            Xác thực thông tin tên miền
                                                                                        <p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="15"></td>
                                </tr>

                                <tr>
                                    <td class="esd-structure es-p5t es-p5b es-p20r es-p20l esdev-adapt-off" align="left">
                                        <table class="esdev-mso-table" width="560" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="30" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-image es-m-txt-c" style="font-size: 0px;" align="right">
                                                                                        <a target="_blank">
                                                                                            <img src="https://e7.pngegg.com/pngimages/733/303/png-clipart-check-check-mark-computer-icons-x-mark-check-mark-icon-green-miscellaneous-angle-thumbnail.png" alt style="display: block;" width="25">
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="20"></td>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="510" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-l es-p5t es-p5b" align="left">
                                                                                        <p style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                                                                            Khởi tạo tài khoản quản lý Google Workspace
                                                                                        <p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="15"></td>
                                </tr>

                                <tr>
                                    <td class="esd-structure es-p5t es-p5b es-p20r es-p20l esdev-adapt-off" align="left">
                                        <table class="esdev-mso-table" width="560" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="30" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-image es-m-txt-c" style="font-size: 0px;" align="right">
                                                                                        <a target="_blank">
                                                                                            <img src="https://e7.pngegg.com/pngimages/733/303/png-clipart-check-check-mark-computer-icons-x-mark-check-mark-icon-green-miscellaneous-angle-thumbnail.png" alt style="display: block;" width="25">
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="20"></td>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="510" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-l es-p5t es-p5b" align="left">
                                                                                        <p style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                                                                            Khởi tạo thông tin sở hữu Google Workspace của quý khách hàng
                                                                                        <p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="15"></td>
                                </tr>

                                
                                <tr>
                                    <td class="esd-structure es-p5t es-p5b es-p20r es-p20l esdev-adapt-off" align="left">
                                        <table class="esdev-mso-table" width="560" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="25" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-image es-m-txt-c" style="font-size: 0px;" align="right">
                                                                                        <a target="_blank">
                                                                                            <img src="https://e7.pngegg.com/pngimages/968/375/png-clipart-question-mark-computer-icons-question-text-orange-thumbnail.png" alt style="display: block;" width="30">
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="20"></td>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="510" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-l es-p5t es-p5b" align="left">
                                                                                        <p style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                                                                            Xác minh quyền sở hữu tên miền
                                                                                        <p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="15"></td>
                                </tr>

                                <tr>
                                    <td class="esd-structure es-p5t es-p5b es-p20r es-p20l esdev-adapt-off" align="left">
                                        <table class="esdev-mso-table" width="560" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="25" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-image es-m-txt-c" style="font-size: 0px;" align="right">
                                                                                        <a target="_blank">
                                                                                            <img src="https://e7.pngegg.com/pngimages/968/375/png-clipart-question-mark-computer-icons-question-text-orange-thumbnail.png" alt style="display: block;" width="30">
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="20"></td>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="510" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-l es-p5t es-p5b" align="left">
                                                                                        <p style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                                                                            Chấp nhận điều khoản và chính sách của Google
                                                                                        <p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="15"></td>
                                </tr>
                                

                                <tr>
                                    <td class="esd-structure es-p5t es-p5b es-p20r es-p20l esdev-adapt-off" align="left">
                                        <table class="esdev-mso-table" width="560" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="25" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-image es-m-txt-c" style="font-size: 0px;" align="right">
                                                                                        <a target="_blank">
                                                                                            <img src="https://e7.pngegg.com/pngimages/968/375/png-clipart-question-mark-computer-icons-question-text-orange-thumbnail.png" alt style="display: block;" width="30">
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="20"></td>
                                                    <td class="esdev-mso-td" valign="top">
                                                        <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-m-p0r esd-container-frame" width="510" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-l es-p5t es-p5b" align="left">
                                                                                        <p style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                                                                            Thiết lập DNS Records đến E-mail server
                                                                                        <p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="15"></td>
                                </tr>

                                <tr>
                                    <td align="left" style="padding:0;Margin:0;padding-bottom:20px;padding-left:20px;padding-right:20px">
                                        <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr>
                                                <td align="left" style="padding:0;Margin:0;width:560px">
                                                    <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                        <tr>
                                                            <td align="center" style="padding:0;Margin:0">
                                                                <!--[if mso]>
                                                                <a href="https://forms.gle/cBYZGBNiUAPhMgn4A" target="_blank" hidden>
                                                                    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="https://forms.gle/cBYZGBNiUAPhMgn4A" style="height:44px; v-text-anchor:middle; width:217px" arcsize="11%" strokecolor="rgb(245, 130, 32)" strokeweight="2px" fillcolor="rgb(245, 130, 32)">
                                                                        <w:anchorlock></w:anchorlock>
                                                                            <center style='color:#ffffff; font-family:arial, "helvetica neue", helvetica, sans-serif; font-size:18px; font-weight:400; line-height:18px;  mso-text-raise:1px'>VIEW FULL LIST</center>
                                                                    </v:roundrect>
                                                                </a>
                                                                <![endif]-->
                                                                <!--[if !mso]>
                                                                <!-- -->
                                                                <span class="msohide es-button-border" style="border-style:solid;border-color:rgb(245, 130, 32);background:rgb(245, 130, 32);border-width:2px;display:inline-block;border-radius:5px;width:auto;mso-hide:all">
                                                                    <a href="https://forms.gle/cBYZGBNiUAPhMgn4A" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:20px;border-style:solid;border-color:rgb(245, 130, 32);border-width:10px 30px 10px 30px;display:inline-block;background:rgb(245, 130, 32);border-radius:5px;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-weight:normal;font-style:normal;line-height:24px;width:auto;text-align:center">
                                                                        BỔ SUNG HỒ SƠ
                                                                    </a>
                                                                </span>
                                                                <!--<![endif]-->
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <?php endif;?>

            </table>
        </td>
    </tr>
    <tr>
        <td height="15"></td>
    </tr>
</table>

<?php $this->load->view('components/support_info'); ?>
<?php $this->load->view('components/contract_info'); ?>
<?php $this->load->view('components/customer_info'); ?>
<?php $this->load->view('footer'); ?>