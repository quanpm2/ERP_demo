<?php $this->load->view('header'); ?>

<?php
$start_time  = get_term_meta_value($term->term_id, 'contract_begin');
$start_time  = $this->mdate->startOfDay($start_time);
$end_time    = get_term_meta_value($term->term_id, 'contract_end');
$end_time    = $this->mdate->startOfDay($end_time);

$num_of_term               = get_term_meta_value($term->term_id, 'num_of_term');
$service_package_detail    = @json_decode(get_term_meta_value($term->term_id, 'service_package_detail'), TRUE);
$term_unit                 = $service_package_detail['term_unit'] ?? 'month';
?>

<?php $content_style = 'font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;'; ?>
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td height="15"></td>
        </tr>
        <tr>
            <td align="center">
                <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid  #f58220">
                                <table width="600" class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #f58220;">
                                                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <!--Title-->
                                                            <td height="40" align="center" bgcolor="#f58220" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Khởi chạy dịch vụ</td>
                                                            <!--End title-->
                                                            <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape_adsplus.png" width="25" height="40" alt="img"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--Space-->
                                                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                                    <tbody>
                                                        <tr>
                                                            <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                                <p style="padding-left: 24px;">&nbsp;</p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--End Space-->
                                                <!--detail-->
                                                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>

                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--end detail-->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff">
                                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <!-- date -->
                                                <table style="border-radius:5px;" class="table1-3" bgcolor="#ecf0f1" width="183" border="0" align="left" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center">
                                                                <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td height="10"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:60px; color:#34495e;font-weight: bold;line-height: 48px;">
                                                                                <?= $num_of_term ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:30px; color:#f58220; font-weight: bold;">
                                                                                <?php
                                                                                $text = 'THÁNG';
                                                                                'year' == $term_unit AND $text = 'NĂM';
                                                                                
                                                                                echo $text;
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="10"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- End date -->
                                                <!--Space-->
                                                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                                    <tbody>
                                                        <tr>
                                                            <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                                <p style="padding-left:24px;">&nbsp;</p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--End Space-->
                                                <!--Content-->
                                                <table class="table3-1" width="342" border="0" align="right" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td align="left" style="font-family: open sans, arial, sans-serif; font-size:15px; color:#999999; line-height:28px;">
                                                                <b>Domain:</b> <?php echo $term->term_name; ?><br>
                                                                <b>Ngày kết thúc:</b> <?php echo my_date($end_time, 'd/m/Y'); ?><br>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--End Content-->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff">
                                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <!--Content-->
                                        <tr>
                                            <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999; line-height:28px;">
                                                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <?php if (!empty($staff)) : ?>
                                                            <tr>
                                                                <td>
                                                                    <!-- Email kinh doanh -->
                                                            <tr>
                                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">
                                                                    <span style="text-align: left">Kinh doanh </span> <?= $staff['display_name'] ?>:
                                                                </td>
                                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">
                                                                    <?= $staff['user_phone'] ?: '(08) 6273.6363'; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?= $staff['user_email']; ?></td>
                                                            </tr>
                                            </td>
                                        </tr>
                                    <?php endif; ?>

                                    <tr>
                                        <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Hotline Trung tâm Kinh doanh:</td>
                                        <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">(028) 7300. 4488</td>
                                    </tr>
                                    <tr>
                                        <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>
                                        <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">sales@webdoctor.vn</td>
                                    </tr>
                                    <tr>
                                        <td height="25"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!--End Content-->
                        <tr>
                            <td align="left" height="30"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
</td>
</tr>
<tr>
    <td height="15"></td>
</tr>
</tbody>
</table>

<!-- thong tin hop dong -->
<?php $this->load->view('components/contract_info'); ?>
<!-- Thong tin hop dong -->
<!-- thong tin khach hang -->
<?php $this->load->view('components/customer_info'); ?>
<!-- thong tin khach hang -->
<?php $this->load->view('footer'); ?>