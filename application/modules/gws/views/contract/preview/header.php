<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo get_term_meta_value($term->term_id,'contract_code');?>.pdf</title>
    <style>
        body{line-height:1.2em; font-size: 14px; padding: 0 20mm;}
        p {margin:8px 0}
        .title{background:#eee; font-weight:600; border-top:1px solid #000}
        ul{margin: 0}
    </style>
    <style type="text/css" media="print">
  @page {
      size: auto;   /* auto is the initial value */
      margin: 0;  /* this affects the margin in the printer settings */
  }
  </style>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <script type="text/javascript">
    window.onload = function () {

        var imgNum = Math.floor(document.body.scrollHeight/750) 
        var imgTags = '<img src="<?php echo base_url('template/admin/img/watermark-adplus-2.png');?>" width="100%" class="watermark" style="opacity:0.8">'
        var innerHTML = ""

        for(var i = 0;i < imgNum;i++){
            innerHTML +=imgTags
        }

        document.getElementById('img-holder').innerHTML = innerHTML;
    }
    </script>
</head>

<body style="font-family: 'Times New Roman', serif;">

    <div id="img-holder" style="position: absolute;top: 0;left:0"></div>

    <p align="center">
        CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br/>
        <span>Độc lập - Tự do - Hạnh phúc</span><br/>
        <span>--------------oOo--------------</span>
    </p>
    <p>&nbsp;</p>

    <p align="center">
        <span style="font-size:1.3em; line-height:1.3em"><?php echo ($printable_title?:'HỢP ĐỒNG'); ?></span><br>
        (<?php echo get_term_meta_value($term_id,'contract_code');?>)
    </p>


    <p align="center">&nbsp;</p>

    <p>- Căn cứ Bộ luật dân sự của nước Cộng hòa Xã hội Chủ nghĩa Việt Nam được Quốc hội khóa 11 thông qua ngày 14/6/2005.</p>
    <p>- Căn cứ Luật Thương mại số 36/2005/QH11 của nước Cộng hoà Xã hội Chủ nghĩa Việt Nam được Quốc hội 11 thông qua ngày 14/06/2005.</p>
    <p>- Căn cứ nghị định số 51/2001/NĐ-CP về quản lý, cung cấp và sử dụng dịch vụ Internet ngày 23 tháng 8 năm 2001 của Chính phủ.</p>
    <p>- Căn cứ Pháp lệnh Bưu chính viễn thông ngày 07/9/2002 của Chủ tịch nước CHXHCNVN.</p>

    <p>Hôm nay, ngày <?php echo my_date($verified_on, 'd');?> tháng <?php echo my_date($verified_on, 'm');?> năm <?php echo my_date($verified_on, 'Y');?> chúng tôi gồm:</p>

    <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td width="35%"><strong>BÊN THUÊ DỊCH VỤ (BÊN A)</strong></td>
            <td>: <strong><?php echo mb_strtoupper($customer->display_name);?></strong></td>
        </tr>
        <?php foreach ($data_customer as $label => $val) :?>
        <?php if (empty($val)) continue;?>
        <tr>
            <td width="130px" valign="top"><?php echo $label;?></td>
            <td>: <?php echo $val;?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td><strong>BÊN CUNG ỨNG DỊCH VỤ (BÊN B)</strong></td>
            <td>: <strong><?php echo $company_name; ?></strong></td>
        </tr>
        <?php foreach ($data_represent as $label => $val) :?>
        <?php if (empty($val)) continue;?>
        <tr>
            <td width="130px" valign="top"><?php echo $label;?></td>
            <td>: <?php echo $val;?></td>
        </tr>
        <?php endforeach; ?>

    </table>