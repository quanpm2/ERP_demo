<div class="content">
    <div class="row-containter">
        <p><b>Dựa trên:</b> Nhu cầu của Bên A và khả năng cung cấp của Bên B về dịch vụ. Hai bên thống nhất ký kết hợp đồng kinh tế với các điều khoản cụ thể như sau:</p>

        <!-- 1st policy -->
        <div class="row-containter">
            <p><b><u>Điều 1:</u> Nội dung hợp đồng</b></p>
            <p>- Đề xuất cung cấp <?=$num_of_licences?> Gmail doanh nghiệp - Google Workspace - <?=$service_package_name?></p>
            <?php if(!empty($email_accounts)): ?>
                <ul>
                <?php foreach($email_accounts as $email_account):?>
                    <li><?php echo $email_account; ?></li>
                <?php endforeach;?>
                </ul>
            <?php endif; ?>
        </div>
        <!-- ./1st policy -->

        <!-- 2nd policy -->
        <div class="row-containter">
            <p><b><u>Điều 2:</u> Giá trị hợp đồng và phương thức thanh toán</b></p>
            <p>Đơn vị tính giá: Việt Nam Đồng (VND).</p>
            <p>Thanh toán bằng tiền đồng Việt Nam.</p>
            <?php
                $this->table->clear();
                $this->table->set_template(['table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">']);
                $this->table->set_heading('STT', 'Gói dịch vụ', 'Thời gian', 'Số lượng (Email)', 'Đơn giá', 'Thành tiền');

                $step = 0;
                foreach($price_list as $item){
                    if(0 == $step){
                        $this->table->add_row(
                            ++$step,
                            ['data' => "Google Workspace - {$service_package_name}", 'rowspan' => count($price_list)], 
                            ['data' => '(từ ngày ' . my_date($contract_begin,'d/m/Y') . ' đến ' . my_date($contract_end,'d/m/Y') . ')', 'align' => 'center', 'vertical-align' => 'center', 'rowspan' => count($price_list) + 1],
                            ['data' => currency_numberformat($item['num_of_seat'], ''), 'align' => 'right'], 
                            ['data' => currency_numberformat($item['unit_price'], ''), 'align' => 'right'], 
                            ['data' => currency_numberformat($item['into_money'], ''), 'align' => 'right'], 
                        );
                        continue;
                    }

                    $this->table->add_row(
                        ++$step,
                        ['data' => currency_numberformat($item['num_of_seat'], ''), 'align' => 'right'], 
                        ['data' => currency_numberformat($item['unit_price'], ''), 'align' => 'right'], 
                        ['data' => currency_numberformat($item['into_money'], ''), 'align' => 'right'], 
                    );
                }

                $this->table->add_row(++$step, "Phí quản lý ({$service_fee_rate}%)", '', '', ['data' => currency_numberformat($service_fee, ''), 'align' => 'right']);

                if($discount_amount > 0){
                    $this->table->add_row(['data' => "Khuyến mãi & Giảm giá", 'colspan' => 5], ['data' => currency_numberformat($discount_amount, ''), 'align' => 'right']);
                }

                $this->table->add_row(['data' => 'Tổng cộng', 'colspan' => 5], ['data' => currency_numberformat($contract_value, ''), 'align' => 'right']);
                
                $vat_money = $contract_value * div($vat, 100);
                $this->table->add_row(['data' => "Thuế VAT ({$vat}%)", 'colspan' => 5], ['data' => currency_numberformat($vat_money, ''), 'align' => 'right']);

                $total = $contract_value + $vat_money;
                $this->table->add_row(['data' => '<b>Tổng thanh toán (đã bao gồm VAT)</b>', 'colspan' => 5], ['data' => '<b>' . currency_numberformat($total, '') . '</b>', 'align' => 'right']);
                echo $this->table->generate();           
			?>

            <p><em>Bằng chữ: <b><i><?php echo ucfirst(mb_strtolower(convert_number_to_words($total)));?> đồng.</i></b></em><br/>

            <p><b>Phương thức thanh toán</b></p>
            <p>- Ngay sau khi hợp đồng đươc ký BÊN A thanh toán cho BÊN B 100% chi phí khởi tạo Gmail doanh nghiệp</p>
            <p>- Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của bên B theo thông tin tài khoản do bên B cung cấp, cụ thể:</p>
            <?php if(!empty($bank_info)) :?>
                <ul style="list-style: none">
                <?php foreach ($bank_info as $label => $text) :?>
                    <?php if (is_array($text)) : ?>
                        <?php foreach ($text as $key => $value) :?>
                            <li>+ <?php echo $key;?>: <?php echo $value;?></li>
                        <?php endforeach;?>
                    <?php continue; endif;?>
                    <li>+ <?php echo $label;?>: <?php echo $text;?></li>
                <?php endforeach;?>
                </ul>
            <?php endif; ?>
            <p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
        </div>
        <!-- 2nd policy -->

        <!-- 3rd policy -->
        <div class="row-containter">
            <p><b><u>Điều 3:</u> Trách nhiệm và quyền hạn của mỗi bên</b></p>
            <p><u>3.1 Trách nhiệm và quyền hạn của Bên A:</u></p>
            <p>- Cung cấp nội dung thông tin đảm bảo chính xác, trung thực trong khuôn khổ luật pháp quy định.</p>
            <p>- Có trách nhiệm thanh toán các khoản chi phí theo như quy định tại Điều 2 trên.</p>
            <p>- Có quyền khiếu nại về chất lượng thông tin, chất lượng dịch vụ do Bên B cung cấp. Mọi khiếu nại phải được gửi cho Bên B dưới dạng văn bản trong vòng 05 ngày kể từ ngày phát sinh vấn đề và Bên B trả lời khiếu nại cho Bên A trong vòng 05 ngày kể từ ngày Bên B nhận được công văn của Bên A.</p>

            <p><u>3.2 Trách nhiệm và quyền hạn của Bên B:</u></p>
            <p>- Cung cấp các dịch vụ theo Điều 1 của Hợp đồng này.</p>
            <p>- Trước khi hết hạn sử dụng dịch vụ 15 ngày bên B sẽ thông báo cho khách hàng lần 1, và sẽ tiếp tục thông báo lần 2 trước khi hết hạn 03 ngày.</p>
            <p>- Bên B sẽ thông báo lần 3 trong kỳ gia hạn 05 ngày và sau thời gian gia hạn dịch vụ sẽ tạm ngừng nếu bên A vẫn chưa thanh toán cho bên B. Dịch vụ chỉ được khôi phục trong vòng 30 ngày kể từ ngày tạm ngừng cung cấp dịch vụ.</p>
        </div>
        <!-- ./3rd policy -->

        <!-- 4th policy -->
        <div class="row-containter">
            <p><b><u>Điều 4:</u> Bất khả kháng</b></p>
            <p>4.1. Nếu có bất kỳ sự kiện nào như thiên tai,dịch họa, lũ lụt,bão, hỏa hoạn , động đất hoặc các hiểm họa thiên tai khác hoặc việc can thiệp của nhà nước, hay bất kỳ sự kiện nào khác xảy ra ngoài tầm kiểm soát của bất kỳ bên nào và không thể lường trước được , thì Bên bị sự kiện bất khả kháng làm ảnh hưởng được tạm hoãn thực hiện nghĩa vụ,với điều kiện là Bên bị ảnh hưởng đó đã áp dụng mọi biện pháp cần thiết và có thể để ngăn ngừa ,hạn chế hoặc khắc phục hậu quả của sự kiện đó.</p>
            <p>4.2. Bên bị ảnh hưởng bởi sự kiện bất khả kháng đó có nghĩa vụ thông báo cho bên còn lại. Trong trường hợp sự kiện bất khả kháng xảy ra , các bên được miễn trách nhiệm bồi thường thiệt hại.</p>
            <p>4.3. Nếu sự kiện không chấm dứt trong vòng 30 (ba mươi) ngày làm việc hoặc một một khoản thời gian lâu hơn và vẫn tiếp tục ảnh hưởng đến việc thực hiện hợp đồng thì bên nào cũng có quyền đơn phương chấm dứt hợp đồng và thông báo cho bên còn lại bằng văn bản trong vòng 03 (ba) ngày làm việc kể từ ngày dự định chấm dứt.</p>
            <p>4.4. Khi sự kiện bất khả kháng chấm dứt , các bên sẽ tiếp tục thực hiện hợp đồng nếu việc tiếp tục thưc hiện hợp đồng được sự đồng ý của 2 bên và có thể thực hiện được.</p>
        </div>
        <!-- ./4th policy -->

        <!-- 5th policy -->
        <div class="row-containter">
            <p><b><u>Điều 5:</u> Điều khoản chung</b></p>
            <p>5.1. Hai Bên cam kết thực hiện đúng nghĩa vụ của mình theo các điều khoản đã ghi trong Hợp đồng.</p>
            <p>5.2. Trong quá trình thực hiện Hợp đồng, nếu có bất kỳ vấn đề nào phát sinh, hai Bên cùng trao đổi, giải quyết trên tinh thần hợp tác và giúp đỡ lẫn nhau.</p>
            <p>5.3. Nếu có tranh chấp xảy ra thì mọi vấn đề không thống nhất sẽ được giải quyết theo quy định của pháp luật.</p>
            <p>5.4. Hợp đồng này được lập thành 02 bản có giá trị pháp lý như nhau, mỗi Bên giữ 01 bản và có hiệu lực kể từ ngày ký.</p>
        </div>
        <!-- ./5th policy -->

