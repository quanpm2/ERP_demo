<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php 
    $term_id = $edit->term_id;
?>

<div class="col-md-12" id="review-partial">
    <h2 class="page-header"><small class="pull-right">Ngày tạo : <?php echo my_date(time(), 'd-m-Y');?></small></h2>

    <div class="row">
        <div class="col-xs-6">
            <p class="lead">Thông tin khách hàng</p>
            <div class="table-responsive">
                <?php
                    $representative_gender  = force_var(get_term_meta_value($edit->term_id,'representative_gender'),'Bà','Ông');
                    $representative_name    = get_term_meta_value($edit->term_id,'representative_name') ?: '';
                    $display_name           = "{$representative_gender} {$representative_name}";
                    $representative_email   = get_term_meta_value($edit->term_id,'representative_email');
                    $representative_address = get_term_meta_value($edit->term_id,'representative_address');
                    $representative_phone   = get_term_meta_value($edit->term_id,'representative_phone');

                    $contract_begin       = get_term_meta_value($edit->term_id,'contract_begin');
                    $contract_begin_date  = my_date($contract_begin,'d/m/Y');
                    $contract_end         = get_term_meta_value($edit->term_id,'contract_end');
                    $contract_end_date    = my_date($contract_end,'d/m/Y');
                    $contract_daterange   = "{$contract_begin_date} đến {$contract_end_date}";

                    echo $this->table->clear()
                    ->add_row('Người đại diện',$display_name?:'Chưa cập nhật')
                    ->add_row('Email',$representative_email?:'Chưa cập nhật')
                    ->add_row('Địa chỉ',$representative_address?:'Chưa cập nhật')
                    ->add_row('Số điện thoại',$representative_phone?:'Chưa cập nhật')
                    ->add_row('Chức vụ',$edit->extra['representative_position']??'Chưa cập nhật')
                    ->add_row('Mã Số thuế',$edit->extra['customer_tax']??'Chưa cập nhật')
                    ->add_row('Thời gian thực hiện',$contract_daterange?:'Chưa cập nhật')
                    ->generate();
                ?>
            </div>
        </div>
        <div class="col-xs-6">
            <p class="lead">Thông tin gói dịch vụ đã đăng ký</p>
            <?php
                $service_package = get_term_meta_value($edit->term_id, 'service_package');
                $service_package_detail = @json_decode(get_term_meta_value($edit->term_id, 'service_package_detail'), TRUE);
                $num_of_licences = (int)get_term_meta_value($edit->term_id, 'num_of_licences');
                $num_of_licences = (int)get_term_meta_value($edit->term_id, 'num_of_licences');
                $num_of_term = (int)get_term_meta_value($edit->term_id, 'num_of_term');
                $service_fee_rate = (int)get_term_meta_value($edit->term_id, 'service_fee_rate');
                $discount_amount = (int)get_term_meta_value($edit->term_id, 'discount_amount');
                $contract_value = (int)get_term_meta_value($edit->term_id, 'contract_value');
                $vat_rate = (int)get_term_meta_value($edit->term_id, 'vat');
                $fct_rate = (int)get_term_meta_value($edit->term_id, 'fct');

                $service_total_price = 0;
                $_num_of_licences = $num_of_licences;
                foreach($service_package_detail['price_plan'] as $price_plan){
                    if(1 > $_num_of_licences) break;

                    $min_email = $price_plan['min_email'];
                    $max_email = $price_plan['max_email'];
                    $num_of_seat = $max_email - $min_email + 1;

                    if($_num_of_licences > $num_of_seat){
                        $service_total_price += $num_of_seat * $price_plan['price_per_email'];
                        $_num_of_licences -= $num_of_seat;

                        continue;
                    }
                    
                    $service_total_price += $_num_of_licences * $price_plan['price_per_email'];
                    $_num_of_licences = 0;
                };

                $term_unit = $service_package_detail['term_unit'];
                
                $month = 1;
                'year' == $term_unit AND $month = 12;

                $service_price = $service_total_price * $month * $num_of_term;
                $vat = round($contract_value * div($vat_rate, 100));
                $service_fee = round($service_price * div($service_fee_rate, 100));

                $term_unit_label = 'Tháng';
                'year' == $term_unit AND $term_unit_label = 'Năm';

                $this->table->add_row(['data' => '<b>Gói dịch vụ</b>'], ['data' => $service_package_detail['label'], 'align' => 'right']);
                foreach($service_package_detail['price_plan'] as $price_plan){
                    $this->table->add_row(['data'=> $price_plan['label']], ['data' => currency_numberformat($price_plan['price_per_email'], ' đ') . '/user/' . $price_plan['unit'], 'align' => 'right']);
                }

                $this->table->add_row(['data' => '<b>Đơn vị tính</b>'], ['data' => $term_unit_label, 'align' => 'right']);
                $this->table->add_row(['data' => '<b>Số kỳ đăng ký</b>'], ['data' => $num_of_term, 'align' => 'right']);
                $this->table->add_row(['data' => '<b>Số lượng đăng ký</b>'], ['data' => $num_of_licences, 'align' => 'right']);
                $this->table->add_row(['data' => '<b>Thành tiền (1)</b>'], ['data' => currency_numberformat($service_price, 'đ'), 'align' => 'right']);
                $this->table->add_row(['data' => "<b>Phí dịch vụ {$service_fee_rate}% (2)</b>"], ['data' => currency_numberformat($service_fee, ' đ'), 'align' => 'right']);
                $this->table->add_row(['data' => "<b>Giảm giá & Khuyến mãi (3)</b>"], ['data' => currency_numberformat($discount_amount, ' đ'), 'align' => 'right']);
                $this->table->add_row(['data' => '<b>Giá trị hợp đồng trước thuế (1 + 2 - 3)</b>'], ['data' => currency_numberformat($contract_value, ' đ'), 'align' => 'right']);

                $this->table->add_row(['data' => "<b>VAT {$vat_rate}%</b>"], ['data' => currency_numberformat($vat, ' đ'), 'align' => 'right']);
                $this->table->add_row(['data' => '<b>Giá trị hợp đồng sau thuế</b>'], ['data' => currency_numberformat($contract_value + $vat, ' đ'), 'align' => 'right']);
            
                echo $this->table->generate();
            ?>
        </div>
    </div>

</div>
<div class="clearfix"></div>

<?php

$hidden_values = ['edit[term_status]'=>'waitingforapprove','edit[term_id]'=>$edit->term_id,'edit[term_type]'=>$edit->term_type];
echo $this->admin_form->form_open('',[],$hidden_values);
echo $this->admin_form->submit('','confirm_step_finish','confirm_step_finish','', array('style'=>'display:none;','id'=>'confirm_step_finish'));
echo $this->admin_form->form_close();