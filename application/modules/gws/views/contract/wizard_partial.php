<?php defined('BASEPATH') or exit('No direct script access allowed');

$this->template->stylesheet->add('plugins/tagsinput/tagsinput.css');
$this->template->stylesheet->add('plugins/iCheck/all.css');
$this->template->javascript->add('plugins/tagsinput/tagsinput.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.js');
$this->template->javascript->add('plugins/iCheck/icheck.min.js');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');
$this->template->javascript->add(base_url("dist/vGwsContractConfiguration.js"));
// icheck.js
?>
<div class="col-md-12" id="service_tab">
    <?php
    $term_id = $edit->term_id;

    echo $this->admin_form->form_open();
    echo form_hidden('edit[term_id]', $term_id);
    ?>

    <div id="vContractConfiguration">
        <v-contract-configuration term_id="<?php echo $edit->term_id; ?>"></v-contract-configuration>
    </div>

    <?php
    echo $this->admin_form->submit('', 'confirm_step_service', 'confirm_step_service', '', ['style' => 'display:none;', 'id' => 'confirm_step_service']);
    echo $this->admin_form->form_close();
    ?>
</div>
<script type="text/javascript">
    var app_root = new Vue({
        el: '#service_tab'
    });
</script>