<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gws_report_m extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('email');

        $models = array(
            'log_m',
            'staffs/admin_m',
            'gws/gws_m',
            'gws/gws_kpi_m'
        );

        $this->load->model($models);

        // 6 ngày sau ngày kết thúc hợp đồng
        defined('NUMDAYS_BEFORE_SUSPEND') or define('NUMDAYS_BEFORE_SUSPEND', 6);
        // 30 ngày trước ngày kết thúc hợp đồng
        defined('NUMDAYS_BEFORE_FINISH') or define('NUMDAYS_BEFORE_FINISH', 30);
        // 7 ngày nhắc một lần
        defined('NUMDAYS_NOTICEFINISH') or define('NUMDAYS_NOTICEFINISH', 7);
    }

    /**
     * Sets the mail to.
     *
     * @param      integer  $term_id  The term identifier
     * @param      string   $type_to  The type to [admin|customer|mailreport|specified]
     *
     * @return     self     ( description_of_the_return_value )
     */
    public function set_mail_to($term_id = 0, $type_to = 'admin')
    {
        $this->email->clear(TRUE);

        $recipients = array(
            'mail_to' => array(),
            'mail_cc' => array(),
            'mail_bcc' => array('thonh@webdoctor.vn', 'tanhn@adsplus.vn')
        );

        if (is_array($type_to) && !empty($type_to)) {
            $recipients = array_merge_recursive($type_to, $recipients);
            $type_to = $type_to['type_to'] ?? 'specified';
        }

        extract($recipients);

        $this->email->from('support@adsplus.vn', 'Adsplus.vn');

        // Nếu email được cấu hình phải gửi cho một nhóm người được truyền vào
        if ($type_to == 'specified') {
            $this->recipients = $mail_to;
            $this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
            return $this;
        }

        // Nếu email được cấu hình gửi cho nhóm quản lý
        if ($type_to == 'mailreport') {
            $mail_to = array('thonh@webdoctor.vn');
            $this->recipients = $mail_to;
            $this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
            return $this;
        }

        // Nếu KD nghỉ, người nhận là người quản lý
        $sale_id = get_term_meta_value($term_id, 'staff_business');
        $sale = $this->admin_m->set_get_admin()->set_get_active()->get($sale_id);

        // Lấy thông tin kỹ thuật phụ trách
        $tech_kpi = $this->gws_kpi_m
            ->select('user_id')
            ->group_by('user_id')
            ->get_many_by(['term_id' => $term_id]);

        // Nếu gửi mail cho 'admin'
        if ($type_to == 'admin') {
            if (!empty($tech_kpi)) {
                foreach ($tech_kpi as $i) {
                    $mail = $this->admin_m->get_field_by_id($i->user_id, 'user_email');
                    if (empty($mail)) continue;

                    $mail_to[] = $mail;
                }
            }

            if ($sale) {
                $mail_cc[] = $sale->user_email;
            }

            $this->recipients = $mail_to;
            $this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
            return $this;
        }

        // Nếu gửi mail cho khách hàng
        if ($representative_email = get_term_meta_value($term_id, 'representative_email')) {
            $mail_to[] = $representative_email;
        }

        // Email người nhận báo cáo được cấu hình theo dịch vụ
        if ($mail_report = get_term_meta_value($term_id, 'mail_report')) {
            $mails = explode(',', trim($mail_report));
            if (!empty($mails)) {
                foreach ($mails as $mail) {
                    $mail_to[] = $mail;
                }
            }
        }

        if (!empty($tech_kpi)) {
            foreach ($tech_kpi as $i) {
                $mail = $this->admin_m->get_field_by_id($i->user_id, 'user_email');
                if (empty($mail)) continue;

                $mail_cc[] = $mail;
            }
        }

        if ($sale) {
            $mail_cc[] = $sale->user_email;
        }

        $this->recipients = $mail_to;

        // test
        // $mail_to  = $mail_cc = $mail_bcc = array() ;
        // $mail_to  = [get_term_meta_value($term_id,'representative_email')] ;
        $this->email->from('support@adsplus.vn', 'Adsplus.vn')
            ->to($mail_to)
            ->cc($mail_cc)
            ->bcc($mail_bcc);

        return $this;
    }


    /**
     * Gửi E-mail thông báo hợp đồng đã hết hạn và thời hạn gia hạn hợp đồng là [NUMDAYS_BEFORE_SUSPEND] ngày
     *
     * @param      integer  $term_id  ID hợp đồng
     *
     * @return     bool   TRUE if success , otherwise return FALSE
     */
    // public function send_mail_before_suspend($term_id = 0)
    // {
    //     $term = $this->gws_m->set_term_type()->get($term_id);
    //     if (empty($term)) return FALSE;

    //     $time             = time();
    //     $contract_end    = get_term_meta_value($term_id, 'contract_end');

    //     // Nếu hợp đồng vẫn đang còn hiệu lực thì không gửi e-mail cảnh báo
    //     if ($time < $contract_end) return FALSE;

    //     //	Cập nhật hạn chót gửi thông báo "đã hết hạn dịch vụ" lưu trữ website
    //     //	Thời gian của hạn chót thông báo = thời gian kết thúc hợp đồng + số ngày thông báo.
    //     $gws_suspend_time = get_term_meta_value($term_id, 'gws_suspend_time');
    //     if (empty($gws_suspend_time)) {
    //         $contract_end = (int) get_term_meta_value($term_id, 'contract_end');
    //         $gws_suspend_time = $this->mdate->startOfDay(strtotime('+' . NUMDAYS_BEFORE_SUSPEND . ' days', $contract_end));
    //         update_term_meta($term_id, 'gws_suspend_time', $gws_suspend_time);
    //     }

    //     if ($gws_suspend_time < $time) return FALSE;

    //     // Tính số ngày còn lại (số ngày có thể gia hạn tiếp tục hợp đồng gws)
    //     $left_days         = number_format(diffInDates($time, $gws_suspend_time));

    //     $contract_begin = get_term_meta_value($term_id, 'contract_begin');
    //     $contract_date    = my_date($contract_begin, 'd/m/Y') . ' - ' . my_date($contract_end, 'd/m/Y');

    //     $data = array(
    //         'term'         => $term,
    //         'term_id'     => $term_id,
    //         'title'     => '[WEBDOCTOR] Cảnh báo đã hết hạn dịch vụ lưu trữ website',
    //         'time'         => $time,
    //         'left_days' => $left_days,

    //         'contract_begin' => $contract_begin,
    //         'contract_end'    => $contract_end,
    //         'contract_date'    => $contract_date,

    //         'gws_suspend_time'     => $gws_suspend_time
    //     );

    //     $data['email_source'] = $data['title'];
    //     // Truy xuất thông tin của NVKD phụ trách, 
    //     // Trong trường hợp NVKD không còn công tác , hệ thống sẽ lấy "trưởng bộ phận KD" để làm thông tin thay thế
    //     $sale_id         = (int) get_term_meta_value($term_id, 'staff_business');
    //     $sale             = $this->admin_m->set_get_admin()->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
    //     $data['sale']    = $sale;

    //     // Truy xuất thông tin của kỹ thuật phụ trách
    //     $technical_staff = array();
    //     if ($technical_kpi = $this->gws_kpi_m->select('user_id')->group_by('user_id')->get_by(['term_id' => $term_id])) {
    //         $technical_staff['name']     = $this->admin_m->get_field_by_id($technical_kpi->user_id, 'display_name');
    //         $technical_staff['email']     = $this->admin_m->get_field_by_id($technical_kpi->user_id, 'user_email');
    //         $technical_staff['phone']     = $this->admin_m->get_field_by_id($technical_kpi->user_id, 'user_phone');
    //     }

    //     $data['technical_staff'] = $technical_staff;
    //     $data['content']    = $this->load->view('report/send_mail_before_suspend', $data, TRUE);
    //     $content             = $this->load->view('email/yellow_tp_2', $data, TRUE);
    //     $this->email->message($content);

    //     $this->set_mail_to($term_id, 'customer');
    //     $this->email->subject($data['title']);
    //     $send_status = $this->email->send();

    //     $this->log_m->insert(array(
    //         'log_title'            => 'Email thông báo gia hạn dịch vụ',
    //         'log_status'        => (int) $send_status,
    //         'term_id'            => $term_id,
    //         'user_id'            => $this->admin_m->id ?? 0,
    //         'log_content'        => '',
    //         'log_type'            => 'send_mail_before_suspend',
    //         'log_time_create'     => date('Y-m-d H:i:s')
    //     ));

    //     return $send_status;
    // }


    /**
     * Gửi E-mail thông báo hợp đồng sắp kết thúc
     * 
     * Thời gian giữa 2 lần gửi mail là [NUMDAYS_NOTICEFINISH] ngày
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    // public function send_noticefinish_mail($term_id = 0)
    // {
    //     $term = $this->gws_m->set_term_type()->get($term_id);
    //     if (empty($term)) return FALSE;

    //     $time                 = time();
    //     $contract_end        = (int)get_term_meta_value($term_id, 'contract_end');
    //     $contract_end        = $this->mdate->endOfDay($contract_end);
    //     $start_time_allowed = $this->mdate->startOfDay(strtotime('-' . NUMDAYS_BEFORE_FINISH . ' days', $contract_end));

    //     // Nếu thời điểm gửi mail không nằm trong phạm vi [NUMDAYS_BEFORE_FINISH] đến thời gian kết thúc thì không gửi mail thông báo
    //     if ($time < $start_time_allowed || $contract_end < $time) return FALSE;

    //     $send_noticefinish_mail_next_time = (int) get_term_meta_value($term_id, 'send_noticefinish_mail_next_time');

    //     // Không hợp lệ nếu 2 lần gửi gần nhau có thời gian thấp hơn [NUMDAYS_NOTICEFINISH] ngày
    //     if ($time < $send_noticefinish_mail_next_time) return FALSE;

    //     update_term_meta($term_id, 'send_noticefinish_mail_next_time', strtotime('+' . NUMDAYS_NOTICEFINISH . ' days'));

    //     $data            = array();
    //     $contract_begin = get_term_meta_value($term_id, 'contract_begin');
    //     $remain_days    = number_format(diffInDates($time, $this->mdate->startOfDay($contract_end)));

    //     $data['contract_end']    = $contract_end;
    //     $data['contract_begin']    = $contract_begin;
    //     $data['remain_days']    = $remain_days;

    //     $data['term']            = $term;
    //     $data['term_id']        = $term_id;
    //     $data['time']            = $time;
    //     $data['title']            = '[WEBDOCTOR] Thông báo Hợp đồng "Hosting - dịch vụ lưu trữ" sắp hết hạn.';
    //     $data['email_source']     = '[WEBDOCTOR] Thông báo Hợp đồng "Hosting - dịch vụ lưu trữ" sắp hết hạn.';
    //     $data['contract_date']    = my_date($contract_begin, 'd/m/Y') . ' - ' . my_date($contract_end, 'd/m/Y');

    //     // Truy xuất thông tin của NVKD phụ trách, 
    //     // Trong trường hợp NVKD không còn công tác , hệ thống sẽ lấy "trưởng bộ phận KD" để làm thông tin thay thế
    //     $sale_id         = (int) get_term_meta_value($term_id, 'staff_business');
    //     $sale             = $this->admin_m->set_get_admin()->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
    //     $data['sale']    = $sale;

    //     // Truy xuất thông tin của kỹ thuật phụ trách
    //     $technical_staff = array();
    //     if ($technical_kpi = $this->gws_kpi_m->select('user_id')->group_by('user_id')->get_by(['term_id' => $term_id])) {
    //         $technical_staff['name']     = $this->admin_m->get_field_by_id($technical_kpi->user_id, 'display_name');
    //         $technical_staff['email']     = $this->admin_m->get_field_by_id($technical_kpi->user_id, 'user_email');
    //         $technical_staff['phone']     = $this->admin_m->get_field_by_id($technical_kpi->user_id, 'user_phone');
    //     }

    //     $data['technical_staff'] = $technical_staff;

    //     $data['content']    = $this->load->view('gws/report/send_noticefinish_mail', $data, TRUE);
    //     $content             = $this->load->view('email/yellow_tp_2', $data, TRUE);

    //     $this->set_mail_to($term_id, 'customer');
    //     $this->email->subject($data['title']);
    //     $this->email->message($content);
    //     $send_status = $this->email->send();

    //     $this->log_m->insert(array(
    //         'log_title'            => $data['title'],
    //         'log_status'        => (int) $send_status,
    //         'term_id'            => $term_id,
    //         'user_id'            => $this->admin_m->id ?? 0,
    //         'log_content'        => '',
    //         'log_type'            => 'send_noticefinish_mail',
    //         'log_time_create'     => date('Y-m-d H:i:s')
    //     ));

    //     return $send_status;
    // }


    // Thông báo khởi tạo hợp đồng
    public function send_mail_info_activated($term_id = 0)
    {
        $title  = 'Hợp đồng ' . get_term_meta_value($term_id, 'contract_code') . ' đã được kích hoạt.';

        $data['term_id']                          = $term_id;
        $data['title']                            = $title;
        $data['email_source']                            = $title;

        // Hiển thị thông tin kinh doanh ra ngoài view
        $data['sale']        = null;
        $sale_id             = get_term_meta_value($term_id, 'staff_business');
        $sale                 = $this->admin_m->set_get_admin()->set_get_active()->as_array()->get($sale_id);
        if(!empty($sale)){
            $sale['user_phone'] = get_user_meta_value($sale['user_id'], 'user_phone');
            $data['staff']        = $sale;
        }

        // Hiển thị thông tin kỹ thuật phụ trách
        $techs    = array();
        $tech_kpi = $this->gws_kpi_m
            ->select('user_id')
            ->group_by('user_id')
            ->get_many_by(['term_id' => $term_id]);
        if (!empty($tech_kpi)) {
            foreach ($tech_kpi as $i) {
                $techs[$i->user_id] = [];

                $name  = $this->admin_m->get_field_by_id($i->user_id, 'display_name');
                $techs[$i->user_id]['name'] = $name;

                $phone = get_user_meta_value($i->user_id, 'user_phone');
                $techs[$i->user_id]['phone'] = $phone;

                $mail  = $this->admin_m->get_field_by_id($i->user_id, 'user_email');
                $techs[$i->user_id]['email'] = $mail;

                $avatar  = $this->admin_m->get_field_by_id($i->user_id, 'user_avatar');
                $techs[$i->user_id]['avatar'] = $avatar;
            }
        }
        $data['techs'] = $techs;

        $this->set_mail_to($term_id, 'customer');
        $data['term']         = $this->term_m->get($term_id);
        $content = $data['content']    = $this->load->view('admin/send-mail/tpl_activation_email', $data, TRUE);

        $this->email->subject($title);
        $this->email->message($content);

        $send_status = $this->email->send();
        if (!$send_status) return FALSE;

        // Thực hiện ghi log	
        $log_id = $this->log_m->insert(array(
            'log_title'        => 'Gửi mail khi kích hoạt gws bằng email',
            'log_status'    => 1,
            'term_id'        => $term_id,
            'user_id'        => $this->admin_m->id,
            'log_content'    => 'Đã gửi mail thành công',
            'log_type'        => 'gws-report-email',
            'log_time_create' => date('Y-m-d H:i:s'),
        ));

        return TRUE;
    }

    // Thông báo kết thúc hợp đồng
    public function send_mail_info_finish($term_id = 0)
    {
        $term = $this->term_m->get($term_id);
        if (empty($term)) return FALSE;

        //gửi mail kết thúc hợp đồng
        $data = array();
        $data['time'] = time();
        $data['title'] = 'Kết thúc hợp đồng';

        $data['term'] = $term;
        $data['term_id'] = $term_id;

        $title = 'Thông báo kết thúc hợp đồng Webdoctor';
        $data['email_source'] = $title;

        // Hiển thị thông tin kinh doanh ra ngoài view
        $sale_id             = get_term_meta_value($term_id, 'staff_business');
        $sale                 = $this->admin_m->set_get_admin()->set_get_active()->as_array()->get($sale_id);
        if(!empty($sale)){
            $sale['user_phone'] = get_user_meta_value($sale['user_id'], 'user_phone');
            $data['staff']        = $sale;
        }

        // Hiển thị thông tin kỹ thuật phụ trách
        // $tech_kpi 		= $this->gws_kpi_m
        // 		        ->select('user_id')
        // 		        ->group_by('user_id')
        // 		        ->get_many_by(['term_id'=>$term_id]);
        $tech_kpi = [];

        $data['techs']    = array();

        if (!empty($tech_kpi)) {
            foreach ($tech_kpi as $i) {
                $mail  = $this->admin_m->get_field_by_id($i->user_id, 'user_email');
                $data['techs']['email'] = $mail;

                $phone = get_user_meta_value($i->user_id, 'user_phone');
                $data['techs']['phone'] = $phone;

                $name  = $this->admin_m->get_field_by_id($i->user_id, 'display_name');
                $data['techs']['name'] = $name;
            }
        }

        // customer-mailreport-admin
        $this->set_mail_to($term_id, 'customer');
        //->set_mail_to($term_id, 'mailreport')
        //->set_mail_to($term_id, 'admin') ;
        $this->email->subject($title);

        $content = $data['content']    = $this->load->view('admin/send-mail/end_contract', $data, TRUE);
        // $content 			= $this->load->view('email/yellow_tp_2',$data,TRUE);

        $this->email->message($content);

        $send_status         = $this->email->send();
        if (FALSE === $send_status) return FALSE;

        // Thực hiện ghi log	
        $log_id = $this->log_m->insert(array(
            'log_title'        => 'Gửi mail kết thúc hợp đồng',
            'log_status'    => 1,
            'term_id'        => $term_id,
            'user_id'        => $this->admin_m->id,
            'log_content'    => 'Đã gửi mail thành công',
            'log_type'        => 'gws-report-email',
            'log_time_create' => date('Y-m-d H:i:s'),
        ));

        return TRUE;
    }

    // Thông báo chuẩn bị hết hạn hợp đồng
    // public function send_mail_alert_expiring_soon($term_id = 0, $type = '', $day = '')
    // {
    //     $term = $this->term_m->get($term_id);
    //     if (empty($term)) return FALSE;

    //     $data              = array();
    //     $data['time']    = time();
    //     $data['title']   = 'Cảnh báo sắp hết hạn hợp đồng';
    //     $data['email_source']   = 'Cảnh báo sắp hết hạn hợp đồng';
    //     $data['term']    = $term;
    //     $data['term_id'] = $term_id;

    //     // Hiển thị thông tin kinh doanh ra ngoài view
    //     $sale_id             = get_term_meta_value($term_id, 'staff_business');
    //     $sale                 = $this->admin_m->set_get_admin()->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
    //     $data['staff']        = $sale;

    //     // Hiển thị thông tin kỹ thuật phụ trách
    //     $tech_kpi         = $this->gws_kpi_m
    //         ->select('user_id')
    //         ->group_by('user_id')
    //         ->get_many_by(['term_id' => $term_id]);

    //     $data['techs']    = array();

    //     if (!empty($tech_kpi)) {
    //         foreach ($tech_kpi as $i) {
    //             $mail  = $this->admin_m->get_field_by_id($i->user_id, 'user_email');
    //             $data['techs']['email'] = $mail;

    //             $phone = get_user_meta_value($i->user_id, 'user_phone');
    //             $data['techs']['phone'] = $phone;

    //             $name  = $this->admin_m->get_field_by_id($i->user_id, 'display_name');
    //             $data['techs']['name'] = $name;
    //         }
    //     }

    //     $this->set_mail_to($term_id, 'customer');
    //     //->set_mail_to($term_id, 'admin');
    //     $this->email->subject($data['title']);

    //     $data['content']    = $this->load->view('admin/send-mail/warning_expires', $data, TRUE);
    //     $content             = $this->load->view('email/yellow_tp_2', $data, TRUE);

    //     $this->email->message($content);

    //     $send_status   = $this->email->send();

    //     if (FALSE === $send_status) return FALSE;

    //     if (!empty($type)) $type =  '_' . $type; // before || after
    //     if (!empty($day) && is_integer($day))  $day  =  '_' . $day . 'day'; // day : int 		

    //     // Thực hiện ghi log
    //     $log_id = $this->log_m->insert(array(
    //         'log_title'            => 'Gửi mail cảnh báo sắp hết hạn hợp đồng',
    //         'log_status'        => 1,
    //         'term_id'            => $term_id,
    //         'user_id'            => $this->admin_m->id,
    //         'log_content'        => 'Đã gửi mail cảnh báo thành công',
    //         'log_type'            => 'warning_gws_deadline' . $type . $day,
    //         'log_time_create'     => date('Y-m-d H:i:s')
    //     ));

    //     if ($log_id) return TRUE;
    // }

    // public function send_mail_extend($term_id = 0)
    // {
    //     $term = $this->term_m->get($term_id);
    //     if (empty($term)) return FALSE;

    //     $data              = array();
    //     $time             = time();
    //     $time             = $this->mdate->startOfDay($time);
    //     $data['time']    = $time;

    //     $extension_day                           = strtotime('+30 day', get_term_meta_value($term_id, 'contract_end'));
    //     $extension_day                           = $this->mdate->startOfDay($extension_day);
    //     $number_of_extension_days               = ($extension_day - $time) / (24 * 60 * 60);
    //     if ($number_of_extension_days == 0) $number_of_extension_days += 1;
    //     $data['number_of_extension_days']       =  $number_of_extension_days;

    //     $data['title']   = 'Thông báo gia hạn gws';
    //     $data['email_source']   = 'Thông báo gia hạn gws';
    //     $data['term']    = $term;
    //     $data['term_id'] = $term_id;

    //     // Hiển thị thông tin kinh doanh ra ngoài view
    //     $sale_id             = get_term_meta_value($term_id, 'staff_business');
    //     $sale                 = $this->admin_m->set_get_admin()->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
    //     $data['staff']        = $sale;

    //     // Hiển thị thông tin kỹ thuật phụ trách
    //     $tech_kpi         = $this->gws_kpi_m
    //         ->select('user_id')
    //         ->group_by('user_id')
    //         ->get_many_by(['term_id' => $term_id]);

    //     $data['techs']    = array();

    //     if (!empty($tech_kpi)) {
    //         foreach ($tech_kpi as $i) {
    //             $mail  = $this->admin_m->get_field_by_id($i->user_id, 'user_email');
    //             $data['techs']['email'] = $mail;

    //             $phone = get_user_meta_value($i->user_id, 'user_phone');
    //             $data['techs']['phone'] = $phone;

    //             $name  = $this->admin_m->get_field_by_id($i->user_id, 'display_name');
    //             $data['techs']['name'] = $name;
    //         }
    //     }

    //     $this->set_mail_to($term_id, 'customer');
    //     //->set_mail_to($term_id, 'admin');
    //     $this->email->subject($data['title']);

    //     $data['content']    = $this->load->view('admin/send-mail/extend_contract', $data, TRUE);
    //     $content             = $this->load->view('email/yellow_tp_2', $data, TRUE);

    //     $this->email->message($content);

    //     $send_status   = $this->email->send();

    //     if (FALSE === $send_status) return FALSE;

    //     // Thực hiện ghi log
    //     $log_id = $this->log_m->insert(array(
    //         'log_title'            => 'Gửi mail gia hạn gws trong 7 ngày',
    //         'log_status'        => 1,
    //         'term_id'            => $term_id,
    //         'user_id'            => $this->admin_m->id,
    //         'log_content'        => 'Đã gửi mail thông báo gia hạn thành cônng',
    //         'log_type'            => 'warning_gws_extend',
    //         'log_time_create'     => date('Y-m-d H:i:s')
    //     ));

    //     if ($log_id) return TRUE;
    // }

    //send mail cho kinh doanh, kỹ thuật
    public function send_mail_info_to_admin($term_id)
    {
        $term = $this->term_m->get($term_id);
        if (empty($term)) return FALSE;

        $time = time();

        $contract_code         = get_term_meta_value($term_id, 'contract_code');
        $title                 = 'Hợp đồng GWS ' . $contract_code . ' đã được khởi tạo.';
        $data                 = array();
        $data['title']         = $title;

        $data['term']        = $term;
        $data['term_id']     = $term_id;
        $data['time']       = $time;


        // Hiển thị thông tin kinh doanh ra ngoài view
        $sale_id             = get_term_meta_value($term_id, 'staff_business');
        $sale                 = $this->admin_m->set_get_admin()->set_get_active()->as_array()->get($sale_id);
        if(!empty($sale)){
            $sale['user_phone'] = get_user_meta_value($sale['user_id'], 'user_phone');
            $data['staff']        = $sale;
        }

        $this->set_mail_to($term_id, ['mail_to' => 'thonh@webdoctor.vn', 'type_to' => 'admin']);
        $this->email->subject($title);
        $this->email->message($this->load->view('gws/admin/send-mail/send_mail_admin', $data, TRUE));

        $send_status                               = $this->email->send();

        if (FALSE === $send_status) return FALSE;

        // Thực hiện ghi log	
        $log_id = $this->log_m->insert(array(
            'log_title'        => 'Thông báo khởi tạo hợp đồng',
            'log_status'    => 1,
            'term_id'        => $term_id,
            'user_id'        => $this->admin_m->id,
            'log_content'    => 'Đã gửi mail thành công',
            'log_type'        => 'gws-report-email',
            'log_time_create' => date('Y-m-d H:i:s'),
        ));

        return TRUE;
    }
}
/* End of file Gws_report_m.php */
/* Location: ./application/modules/gws/models/Gws_report_m.php */