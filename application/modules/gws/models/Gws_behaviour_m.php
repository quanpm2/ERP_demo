<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'modules/contract/models/Behaviour_m.php');

class Gws_behaviour_m extends Behaviour_m
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('date');
    }

    /**
     * Preview data for printable contract version
     *
     * @return     String  HTML content
     */
    public function prepare_preview()
    {
        if (!$this->contract) return FALSE;

        parent::prepare_preview();

        $data = $this->data;

        $contract = $this->get_contract();
        $service_package_detail = get_term_meta_value($contract->term_id, 'service_package_detail');
        $service_package_detail = @json_decode($service_package_detail, TRUE) ?: [];

        $data['service_package_name'] = $service_package_detail['label'];
        $num_of_licences = $data['num_of_licences'] = (int) get_term_meta_value($contract->term_id, 'num_of_licences') ?: 0;
        $num_of_term = $data['num_of_term'] = (int) get_term_meta_value($contract->term_id, 'num_of_term') ?: 0;

        $term_unit = $service_package_detail['term_unit'];
        $month = 1;
        'year' == $term_unit AND $month = 12;

        $num_of_month = $month * $num_of_term;

        $price_list = [];
        $price_plan = $service_package_detail['price_plan'];
        foreach ($price_plan as $key => $plan) {
            if (1 > $num_of_licences) continue;

            $min_email = (int) $plan['min_email'];
            $max_email = (int) $plan['max_email'];
            $num_of_seat = $max_email - $min_email + 1;

            if ($num_of_licences > $num_of_seat) {
                $price = $num_of_seat * (int) $plan['price_per_email'] * $num_of_month;
                $price_list[$key] = ['num_of_seat' => $num_of_seat, 'unit_price' => (int) $plan['price_per_email'], 'into_money' => $price];

                $num_of_licences -= $num_of_seat;

                continue;
            }

            $price = $num_of_licences * (int) $plan['price_per_email'] * $num_of_month;
            $price_list[$key] = ['num_of_seat' => $num_of_licences, 'unit_price' => (int) $plan['price_per_email'], 'into_money' => $price];
            $num_of_licences = 0;
        }
        $data['price_list'] = $price_list;

        $data['total_into_money'] = array_sum(array_column($price_list, 'into_money'));

        $data['service_fee_rate'] = (int) get_term_meta_value($contract->term_id, 'service_fee_rate');
        $data['service_fee'] = (int) round($data['total_into_money'] * div($data['service_fee_rate'], 100));
        $data['vat'] = (int) get_term_meta_value($contract->term_id, 'vat');
        $data['fct'] = (int) get_term_meta_value($contract->term_id, 'fct');

        $data['discount_amount'] = (int) get_term_meta_value($contract->term_id, 'discount_amount');

        $data['contract_begin'] = get_term_meta_value($contract->term_id, 'contract_begin');
        $data['contract_end'] = get_term_meta_value($contract->term_id, 'contract_end');
        $data['domain'] = $contract->term_name;

        $email_accounts = get_term_meta($contract->term_id, 'email_accounts');
        $email_accounts = array_column($email_accounts, 'meta_value');
        $data['email_accounts'] = $email_accounts;

        $data['header'] = 'gws/contract/preview/header';
        $data['content'] = $this->load->view('gws/contract/preview/preview', $data, TRUE);

        return $data;
    }

    /**
     * Creates invoices.
     *
     * @throws     Exception  (description)
     *
     * @return     <type>     ( description_of_the_return_value )
     */
    public function create_invoices()
    {
        if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");
        $term_id 		= $this->contract->term_id;
        $contract_value = get_term_meta_value($term_id, 'contract_value') ?: 0;
        if(empty($contract_value)) throw new Exception("Giá trị hợp đồng ({$contract_value}) không hợp lệ.");


        $number_of_payments = (int) get_term_meta_value($term_id, 'number_of_payments');
        $number_of_payments = $number_of_payments ?: 1;

        $contract_begin = get_term_meta_value($term_id, 'contract_begin');
        $contract_end 	= get_term_meta_value($term_id, 'contract_end');
        $num_dates 		= diffInDates($contract_begin,$contract_end);

        $num_days4inv 			= ceil(div($num_dates,$number_of_payments));
        $amount_per_payments 	= div($contract_value,$number_of_payments);

        $start_date 	= $contract_begin;
        $invoice_items 	= array();

        for($i = 0 ; $i < $number_of_payments; $i++)
        {	
            if($num_days4inv == 0) break;

            $end_date 	= end_of_day(strtotime("+{$num_days4inv} day -1 second", $start_date));
            $_inv_data 	= array(
                'post_title' 	=> 'Thu tiền đợt '. ($i + 1),
                'post_content' 	=> '',
                'start_date' 	=> $start_date,
                'end_date' 		=> $end_date,
                'post_type' 	=> $this->invoice_m->post_type
            );


            $inv_id = $this->invoice_m->insert($_inv_data);
            if(empty($inv_id)) continue;

            $this->term_posts_m->set_post_terms($inv_id, $term_id, $this->contract->term_type);

            $quantity = $num_days4inv;
            $this->invoice_item_m->insert(array(
                    'invi_title' => 'Giá dịch vụ',
                    'inv_id' => $inv_id,
                    'invi_description' => '',
                    'invi_status' => 'publish',
                    'price' => $amount_per_payments,
                    'quantity' => 1,
                    'invi_rate' => 100,
                    'total' => $this->invoice_item_m->calc_total_price($amount_per_payments, 1, 100)
                ));

            $start_date = strtotime('+1 second', $end_date);

            $day_end = $num_dates - $num_days4inv;
            if($day_end < $num_days4inv) $num_days4inv = $day_end;
        }

        return TRUE;
    }
    
    /**
     * calc_payment_service_fee
     *
     * @return void
     */
    public function calc_payment_service_fee()
    {
        if( ! $this->contract) throw new \Exception('Property Contract undefined.');

        $stop_status = ['ending', 'liquidation'];
        if(!in_array($this->contract->term_status, $stop_status)) return 0;

        return parent::calc_payment_service_fee();
    }

    /**
     * Sets the technician identifier.
     */
    public function setTechnicianId()
    {
        parent::is_exist_contract(); // Determines if exist contract.

        $term_id = $this->contract->term_id;

        $this->load->model('gws/gws_kpi_m');

        $kpis = $this->gws_kpi_m->select('user_id')->distinct('user_id')->where('term_id', $this->contract->term_id)->get_all();
        if( ! $kpis)
        {
            update_term_meta($this->contract->term_id, 'technicianId', 0);
            return true;
        }

        $roleNameEnums 	= [ $this->option_m->get_value('fb_ads_role_id') => 'gat', $this->option_m->get_value('leader_fb_ads_role_id') => 'am', $this->option_m->get_value('manager_fb_ads_role_id') => 'manager' ];
        $staffs 		= array_map(function($x) use ($roleNameEnums){
            $_staff = $this->admin_m->get_field_by_id($x->user_id);
            $_staff['roleName'] = $roleNameEnums[$_staff['role_id']] ?? 'unknown';
            return $_staff;
        }, $kpis);
        
        if( 1 == count($staffs))
        {
            $_staff = reset($staffs);
            update_term_meta($this->contract->term_id, 'technicianId', $_staff['user_id']);
            return true;
        }
        $staffsGrouped = array_group_by($staffs, 'roleName');

        foreach (array_values($roleNameEnums) as $roleName)
        {
            if(empty($staffsGrouped[$roleName])) continue;

            $_staff = reset($staffsGrouped[$roleName]);
            update_term_meta($this->contract->term_id, 'technicianId', $_staff['user_id']);
            return true;
        }

        $_staff = reset($staffs);
        update_term_meta($this->contract->term_id, 'technicianId', $_staff['user_id']);
        return true;
    }
}
/* End of file Gws_behaviour_m.php */
/* Location: ./application/modules/hosting/models/Gws_behaviour_m.php */