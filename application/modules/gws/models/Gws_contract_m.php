<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gws_contract_m extends Base_contract_m {

    function __construct() 
    {
        parent::__construct();
        $this->load->model('log_m');
        $this->load->model('gws/gws_m');
        $this->load->model('gws/gws_report_m');
        $this->load->model('gws/gws_kpi_m');
    }

    public function start_service($term = null)
    {
        restrict('gws.start_service');	
        $this->load->model('gws/gws_report_m') ;
        if(empty($term)) return false;

        if(!$term || 'gws' != $term->term_type) return false;
        
        $term_id = $term->term_id;
        $this->gws_report_m->send_mail_info_to_admin($term_id);

        /***
         * Khởi tạo Meta thời gian dịch vụ để thuận tiện cho tính năng sắp xếp và truy vấn
         */
        $start_service_time = get_term_meta_value($term_id, 'start_service_time');
        empty($start_service_time) AND update_term_meta($term_id, 'start_service_time', 0);

        return true;
    }

    public function proc_service($term)
    {
        if(!$term || $term->term_type != 'gws') return FALSE;
        $term_id = $term->term_id;

        $staff_id = get_term_meta_value($term_id, 'staff_business');
        if(! $staff_id ) $this->messages->info('Chưa có nhân viên kinh doanh');
    
        $tech_kpis = $this->gws_kpi_m->select('user_id')
        ->where('term_id', $term_id)
        ->where('kpi_type', 'tech')
        ->as_array()
        ->get_many_by();

        if(empty($tech_kpis)) $this->messages->info('Chưa có nhân viên kỹ thuật');

        $is_send = $this->gws_report_m->send_mail_info_activated($term_id) ;
        if(FALSE === $is_send) return FALSE;

        // update_term_meta($term_id, 'start_service_time' , time());

        $this->log_m->insert(array(
            'log_type' =>'start_service_time',
            'user_id' => $this->admin_m->id,
            'term_id' => $term_id,
            'log_content' => date('Y/m/d H:i:s', time()). ' - Start Service Time'
        ));

        return TRUE;
    }

    public function stop_service($term)
    {
        if(!$term || $term->term_type != 'gws') return FALSE;
        $term_id = $term->term_id;
        
        $is_send = $this->gws_report_m->send_mail_info_finish($term_id);
        if( FALSE === $is_send) return FALSE ;

        update_term_meta($term_id,'end_service_time', time());
        $this->contract_m->update($term_id, array('term_status'=>'ending'));
        $this->log_m->insert(array(
            'log_type' =>'end_service_time',
            'user_id' => $this->admin_m->id,
            'term_id' => $term_id,
            'log_content' => date('Y/m/d H:i:s', time()) . ' - End Service Time'
        ));

        // Push queue sync service fee
        $this->load->config('amqps');
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        
        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];

        $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $channel     = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $payload = [
            'event' 	=> 'contract_payment.sync.service_fee',
            'contract_id' => $term_id
        ];
        $message = new \PhpAmqpLib\Message\AMQPMessage(
            json_encode($payload),
            array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $channel->basic_publish($message, '', $queue);	

        $channel->close();
        $connection->close();

        return TRUE;
    }
}

/* End of file Hosting_contract_m.php */
/* Location: ./application/modules/gws/models/Hosting_contract_m.php */