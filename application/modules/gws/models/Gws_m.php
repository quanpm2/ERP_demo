<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Contract_m.php');

class Gws_m extends Contract_m
{
    public $term_type = 'gws';

    function __construct()
    {
        parent::__construct();
    }
}
