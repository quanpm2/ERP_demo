<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calenders extends Admin_Controller {

	public $model = 'admin_m';

	function __construct(){
		parent::__construct();

		$type = $this->input->get('type');

		$this->type = ($type) ? $type : 'admin';
		
	}
	public function index(){	
		parent::render('','calender');
		//$this->load->view('calenders/calender');
	}
}