<?php 

$this->template->stylesheet->add('modules/calender/css/fullcalendar.min.css');
$this->template->stylesheet->add('modules/calender/css/fullcalendar.print.min.css','media="print"');
$this->template->stylesheet->add('modules/calender/css/AdminLTE.min.css');
$this->template->stylesheet->add('modules/calender/css/custom.css');

$this->template->javascript->add('plugins/input-mask/jquery.inputmask.js');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');
$this->template->javascript->add('modules/calender/js/bootstrap.min.js');
$this->template->javascript->add('modules/calender/js/jquery.slimscroll.min.js');
$this->template->javascript->add('modules/calender/js/fastclick.js');
$this->template->javascript->add('modules/calender/js/adminlte.min.js');
$this->template->javascript->add('modules/calender/js/demo1.js');
$this->template->javascript->add('modules/calender/js/moment.js');
$this->template->javascript->add('modules/calender/js/fullcalendar.min.js');
$this->template->javascript->add('modules/calender/js/custom.js');
$this->template->javascript->add('modules/calender/js/jquery-ui.min.js');
?>
<link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<section class="content" id="app-content">
      <div class="row">
        
        <div class="col-md-3"><view-cru-calender-component/></div>

        
        <div class="col-md-9"><view-calender-component/></div>

      </div>
    </section>


<?php
echo $this->template->trigger_javascript(admin_theme_url("modules/calender/js/calender.js"));
echo $this->template->trigger_javascript(admin_theme_url("modules/calender/js/app.js"));
?>