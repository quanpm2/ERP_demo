<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['protocol']    = 'smtp';
$config['smtp_host']    = 'smtp.gmail.com';
$config['smtp_port']    = 587;
$config['smtp_crypto']    = 'tls';
$config['smtp_timeout'] = 7;
$config['smtp_user'] = getenv('ADSPLUS_SMTP_USER');
$config['smtp_pass'] = getenv('ADSPLUS_SMTP_PWD');
$config['charset']    = 'utf-8';
$config['newline']    = "\r\n";
$config['mailtype'] = 'html';
$config['dsn'] = TRUE;
$config['bcc_batch_mode'] = TRUE;

$config['email-groups'] = array(
	
	'reporting' => array(
		['smtp_user' => getenv('REPORT1_ADSPLUS_SMTP_USER'), 'smtp_pass' => getenv('REPORT1_ADSPLUS_SMTP_PWD')],
		['smtp_user' => getenv('REPORT2_ADSPLUS_SMTP_USER'), 'smtp_pass' => getenv('REPORT2_ADSPLUS_SMTP_PWD')],
		['smtp_user' => getenv('REPORT3_ADSPLUS_SMTP_USER'), 'smtp_pass' => getenv('REPORT3_ADSPLUS_SMTP_PWD')]
	),

	'warning' => array(
		['smtp_user' => getenv('WARNING1_ADSPLUS_SMTP_USER'), 'smtp_pass' => getenv('WARNING1_ADSPLUS_SMTP_PWD')],
		['smtp_user' => getenv('WARNING2_ADSPLUS_SMTP_USER'), 'smtp_pass' => getenv('WARNING2_ADSPLUS_SMTP_PWD')],
		['smtp_user' => getenv('WARNING3_ADSPLUS_SMTP_USER'), 'smtp_pass' => getenv('WARNING3_ADSPLUS_SMTP_PWD')],
	),
);