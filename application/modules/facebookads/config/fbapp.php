<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['fbapps'] = array(

	'production' => array(
		'label' 		=> 'API Adsplus',
		'name'			=> 'production',
		'credential' 	=> array(
			'app_id' 		=> getenv('FB_APP_ID') ?: '871025543430030',
			'app_secret' 	=> getenv('FB_APP_SECRET') ?: '744974be79ce36a36bdf34b4d6cfd6c1',
			'access_token' 	=> getenv('FB_ACCESS_TOKEN') ?: 'EAAHCdzpvwtQBAOEJ8x8y1cxElzg2u4POfLWPZAbqIZCZC3GLxnf8dUR9G8FwkdFTuclHScpKcBFB432nM5Na7Aaj5lELVs1zLoS938b5qkg8Lb3mTt6FJyMLm68KNY3XnURJ5yWz2ZAVkc5cswjTv95tdYepJFcdzUDj502WTQZDZD',
			'graph_version' => getenv('FB_GRAPH_VERSION') ?: 'v14.0'

		)
	),

	'development' => array(
		'label' 		=> 'API Agency Adsplus',
		'name'			=> 'development',
		'credential' 	=> array(
			'app_id' 		=> getenv('FB_DEVELOPMENT_APP_ID') ?: null,
			'app_secret' 	=> getenv('FB_DEVELOPMENT_APP_SECRET') ?: null,
			'access_token' 	=> getenv('FB_DEVELOPMENT_ACCESS_TOKEN') ?: null,
			'graph_version' => getenv('FB_DEVELOPMENT_GRAPH_VERSION') ?: NULL
		)
	),

	'default' => getenv('FB_APP_ENV') ?: 'production'
);