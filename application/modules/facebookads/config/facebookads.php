<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$config['packages'] = array('service' => array(),'default' =>'');
$config['facebookads'] = array('status' => array('pending' => 'Ngưng hoạt động','publish' => 'Đang hoạt động'));

$config['adaccount_status'] = [
	'states' => [
		'UNSPECIFIED' 		=> 'Chưa cấu hình',
		'PENDING_APPROVAL'  => 'Đã cấu hình',
		'APPROVED' 			=> 'Đã kích hoạt',
		'REMOVED' 			=> 'Đã xóa cấu hình',
		'UNKNOWN' 			=> 'Chưa xác định',
	],
	'default' => 'UNSPECIFIED'
];

$config['fb_allowed_adaccount_ids'] = array(
	
	'act_1675714285818527', /*  AdsPlus2-vat1 */
	'act_1675714535818502', /*  AdsPlus2-vat2 */
	'act_1020892211300741', /*  AdsPlus2-no1(Đào Tạo Guru) */
	'act_1450622208327737', /*  AdsPlus2-no2(Webdoctor.vn) */

	'act_1386382775013756', /*  AdsPlus.vn - VND - (No 1) */
	'act_1377848335845601', /*  AdsPlus.vn - VND - (VAT 2) */
	'act_756294651142554', 	/*   AdsPlus.vn - VND - (VAT 1) */
	'act_973509289421088', 	/*  AdsPlus.vn - VND - (No 2) - Shopneo */
	'act_1061868633918486', /*  AdsPlus.vn - VND - (No 5) - trc 14-12-17 VAT */
	'act_1061892953916054', /*  AdsPlus.vn - VND - (No 3) */
	'act_1113984842040198', /*  Adsplus.vn (No 4 - Quytvn) */
	'act_651368818538936', /* Adsplus 3-No4 (Ng) */
	'act_237398680362878', /* Adsplus 3-No5 (TH) */
	'act_129588201249698', /* Adsplus 3-No3 (Q) */
	'act_1923330787685819', /* Adsplus 3-No2 (Q) */
	'act_2028044757205614', /* Adsplus 3-No1 (Q) */

	'act_272501276654376', /*  AdsPlus 4 – 2 */
	'act_191963354853545', /*  AdsPlus 4 – 3 */
	'act_1783993551684898', /*  AdsPlus 4 – 4 */
	'act_1885061994848865', /*  AdsPlus 4 – 5 */
	'act_784888984968036', /*  AdsPlus 4 - No1 */

	'act_2006440836043678', /* Adsplus 5 – No 1 */
	'act_2183217228617534', /* Adsplus 5 – No 2 */
	'act_323037061763018', /* Adsplus 5 – VAT 1 */

	'act_372432266849428', /* AdsPlus 6 - (No1) - HCM5 */
	'act_330880307718766', /* Adsplus 4 - No2 (Fixed) */

	'act_288028621902292', /* Adsplus 4 - ro17-Quý-0119 (no) */
	'act_2474781116143501', /* Adsplus 4 - ruchan-Quý-0119 (no) */
	'act_1998689803531540', /* Adsplus 6- VAT */
	'act_359691481292458', /* Adsplus 5-senkavietnam(Va) */
	'act_617280408705203', /* Adsplus 5 - TGBPreschool (VA) */
	'act_316816512309377', /* Adsplus 5 - nhahangbienrung.com(VA) VND */

	'act_277312789826705', /* Adsplus 5 - DaiHocNguyenTatThanh (Va) */

	'act_2338328172885357', /* Adsplus 4 - LeSa */
	'act_364035250908364', /* Adsplus 5 - azdigi.com (va) */
	'act_2569912259705431', /* Adsplus 5 - Adpex.vn (va) */

	'act_725103611218158', /* Adsplus 4 - Tunhuadailoanhuybich */
	'act_880188092338300', /* Adsplus 5 - Taithongmooncake */
	'act_456074694964615', /* Adsplus 5 - interhome */
	'act_679831985763195', /* Adsplus 5 - Gra*/
	'act_689444714851150', /* Adsplus 6 - CosyHome */
	'act_350631082316692', /* Adsplus 6 - Universal Language */

	'act_2390913157856702', /* Adsplus 4 - DragonLend (VA) */
	'act_315299842472421', /* Adsplus 4 - Mia trangsucbac (no) */
	'act_2369801296435591', /* Adsplus 5 - Victoria VUW (VA) */
	'act_428677357721603', /* Adsplus 6 - Shinypetvn (No) */
	'act_803640840030083', /* Adsplus 6 - BanBuonHangHot (No) */

	'act_362590074670140', /* Adsplus 4 - Amazonleaning (No) */
	'act_762431204187604', /* Adsplus 6 - Phenikkaa (VA) */
	'act_2377381555850248', /* Adsplus 6 - Dodacaocaphuyhieu (No) */
	'act_337931000474887', /* Adsplus 5 - HappyCook (VA) */


	'act_552302688572278', /* Adsplus 4 - vato.vn (No từ 040919) DatnenBRVT (va) */
	'act_1313781672120097', /* Adsplus 5 - Essilor (VA) */
	'act_742966562804083', /* Adsplus 5 - Molsionvn (VA) */
	'act_467453180476692', /* Adsplus 5 - Bolon (VA) */
	'act_743060779467945', /* Adsplus 5 - CaramelEnglish (VA) */


	'act_514914559079267', /* AdsPlus 7 - LANRUNGNGOCSU (no) */
	'act_2537516362967576', /* AdsPlus 7 - NGOCANHKIDS (no) */
	'act_2639433342760025', /* AdsPlus 7 - CHIEUNGUARENHATVIETNAM (no) */
	
	'act_2408008352800470', /* Adsplus 7 - Kocomei (VAT) */

	'act_2429646490648285', /* Adsplus 5 - Gymaster (VA) */

	'act_2894261757303567', /* Adsplus 7 - ANHCUOIDALAT2016 (No) */
	'act_975367402847173', /* Adsplus 5 - GIFTNETWORK.VN (va) */
	
	'act_479827742668508', /* Adsplus 6 - Nghiatv - TK NAMPERFUME */

	'act_157341622065407', /* Adsplus 7 - TRANG TRẠI CÂY GIỐNG (KH Payment)  */
	'act_2377381555850248', /* Adsplus 6 - dungcucongnghiep  */
	'act_1488861491264618', /* Adsplus 7 - TUAN123BATDONGSAN  */
	'act_2558411980882121', /* Adsplus 5 - THONGTINDUANCATTUONG.COM  */
	'act_876648506131230', /* Adsplus 5 - KHU ĐÔ THỊ NAM HOÀNG ĐỒNG  */
	'act_581110385817416', /* Adsplus 7 - Group Bất Động Sản   */
	'act_557702151620968', /* Adsplus 7 - BONGSHOP.TQXK  */
	'act_762431204187604', /* Adsplus 6 - Phenikkaa  */
	'act_556032678603632', /* Adsplus 7 - MAYKHOANPINGIATOT  */
	'act_289220582061055', /* Adsplus 6 - HPC-LANDMARK  */
	'act_2775550655858394', /* Adsplus 2 - THAMMYTHANHVUONG  */
	'act_497209720982015', /* Adsplus 5 - JANGIN.VN  */
	'act_463425821200859', /* Adsplus 5 - OBSERVATIONDECKHANOI  */
	'act_543233839795168', /* Adsplus 5 - Duhoctucap3taiDuc (va)  */
	'act_2501325146845838', /* Adsplus 5 - Misumi (Va)  */
	'act_350631082316692', /* Adsplus 6 - AodaiLinhPhuong  */
	'act_217265719409042', /* Adsplus 6 - BSMART.VN (No)  */
	'act_598260137570928', /* Adsplus 6 - BUYDERORDERHANGTRUNGQUOC (No)  */
	'act_428677357721603', /* Adsplus 6 - EVANZA.THOITRANG  */
	'act_1558668770966432', /* Adsplus 7 - LOOKSHOP.VN (No)  */
	'act_1057049371340478', /* Adsplus 7 - FAN.XEDAPDIEN (No)  */
	'act_300395680920564', /* Adsplus 7 - BATDONGSANTHONGMINH2020 (No)  */
	'act_1530452840452843', /* Adsplus 7 - MEELICHAN (No)   */
	'act_227200338419692', /* Adsplus 7 - ZANESS (No )  */
	'act_1998689803531540', /* Adsplus 6 - DUHOCNGHETAICHLBDUC (No)  */
	'act_1093660097661603', /* Adsplus 6 - GOODSSTOREGV (No)  */
	'act_741799323017758', /* Adsplus 6 - TUTISHOPMEVABE (No)  */
	'act_632318900879563', /* Adsplus 5 - VIGLACERAPAINT  */
	'act_520558445319982', /* Adsplus 6 - SKYDECOR  */
	'act_587243565213772', /* Adsplus 6 - GIAKESAT (No)  */
	'act_661730084587700', /* Adsplus 6 - NHAENNI (No)  */
	'act_1699490803527309', /* Adsplus 6 - LYA.NACLOTHING (No)  */
	'act_880188092338300', /* Adsplus 5 - Taithongmooncake (No)  */
	'act_231553137986150', /* Adsplus 5 - STUDINTERVN (Va)  */
);

$config['fb_allowed_adaccount_indsert_rows'] = array(
	[ 'id' => 'act_157341622065407', 'name' =>  'Adsplus 7 - TRANG TRẠI CÂY GIỐNG (KH Payment)'] , 
	[ 'id' => 'act_2377381555850248', 'name' =>  'Adsplus 6 - dungcucongnghiep'] , 
	[ 'id' => 'act_1488861491264618', 'name' =>  'Adsplus 7 - TUAN123BATDONGSAN'] , 
	[ 'id' => 'act_2558411980882121', 'name' =>  'Adsplus 5 - THONGTINDUANCATTUONG.COM'] , 
	[ 'id' => 'act_876648506131230', 'name' =>  'Adsplus 5 - KHU ĐÔ THỊ NAM HOÀNG ĐỒNG'] , 
	[ 'id' => 'act_581110385817416', 'name' =>  'Adsplus 7 - Group Bất Động Sản '] , 
	[ 'id' => 'act_557702151620968', 'name' =>  'Adsplus 7 - BONGSHOP.TQXK'] , 
	[ 'id' => 'act_762431204187604', 'name' =>  'Adsplus 6 - Phenikkaa'] , 
	[ 'id' => 'act_556032678603632', 'name' =>  'Adsplus 7 - MAYKHOANPINGIATOT'] , 
	[ 'id' => 'act_289220582061055', 'name' =>  'Adsplus 6 - HPC-LANDMARK'] , 
	[ 'id' => 'act_2775550655858394', 'name' =>  'Adsplus 2 - THAMMYTHANHVUONG'] , 
	[ 'id' => 'act_497209720982015', 'name' =>  'Adsplus 5 - JANGIN.VN'] , 
	[ 'id' => 'act_463425821200859', 'name' =>  'Adsplus 5 - OBSERVATIONDECKHANOI'] , 
	[ 'id' => 'act_543233839795168', 'name' =>  'Adsplus 5 - Duhoctucap3taiDuc (va)'] , 
	[ 'id' => 'act_2501325146845838', 'name' =>  'Adsplus 5 - Misumi (Va)'] , 
	[ 'id' => 'act_350631082316692', 'name' =>  'Adsplus 6 - AodaiLinhPhuong'] , 
	[ 'id' => 'act_217265719409042', 'name' =>  'Adsplus 6 - BSMART.VN (No)'] , 
	[ 'id' => 'act_598260137570928', 'name' =>  'Adsplus 6 - BUYDERORDERHANGTRUNGQUOC (No)'] , 
	[ 'id' => 'act_428677357721603', 'name' =>  'Adsplus 6 - EVANZA.THOITRANG'] , 
	[ 'id' => 'act_1558668770966432', 'name' =>  'Adsplus 7 - LOOKSHOP.VN (No)'] , 
	[ 'id' => 'act_1057049371340478', 'name' =>  'Adsplus 7 - FAN.XEDAPDIEN (No)'] , 
	[ 'id' => 'act_300395680920564', 'name' =>  'Adsplus 7 - BATDONGSANTHONGMINH2020 (No)'] , 
	[ 'id' => 'act_1530452840452843', 'name' =>  'Adsplus 7 - MEELICHAN (No) '] , 
	[ 'id' => 'act_227200338419692', 'name' =>  'Adsplus 7 - ZANESS (No )'] , 
	[ 'id' => 'act_1998689803531540', 'name' =>  'Adsplus 6 - DUHOCNGHETAICHLBDUC (No)'] , 
	[ 'id' => 'act_1093660097661603', 'name' =>  'Adsplus 6 - GOODSSTOREGV (No)'] , 
	[ 'id' => 'act_741799323017758', 'name' =>  'Adsplus 6 - TUTISHOPMEVABE (No)'] , 
	[ 'id' => 'act_632318900879563', 'name' =>  'Adsplus 5 - VIGLACERAPAINT'] , 
	[ 'id' => 'act_520558445319982', 'name' =>  'Adsplus 6 - SKYDECOR'] , 
	[ 'id' => 'act_587243565213772', 'name' =>  'Adsplus 6 - GIAKESAT (No)'] , 
	[ 'id' => 'act_661730084587700', 'name' =>  'Adsplus 6 - NHAENNI (No)'] , 
	[ 'id' => 'act_1699490803527309', 'name' =>  'Adsplus 6 - LYA.NACLOTHING (No)'] , 
	[ 'id' => 'act_880188092338300', 'name' =>  'Adsplus 5 - Taithongmooncake (No)'] , 
	[ 'id' => 'act_231553137986150', 'name' =>  'Adsplus 5 - STUDINTERVN (Va)'] , 
);