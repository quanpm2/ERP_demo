<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$columns = array(
	'id' => array(
		'name' => 'id',
		'label' => '#ID',
		'set_select' => FALSE,
		'title' => '#ID',
		'type' => 'string',
	)
);

$config['datasource'] = array(
	'columns' => $columns,
	'default_columns' => array(
		'website',
		'actual_result',
		'actual_budget',
		'payment_expected_end_time',
		'payment_real_progress',
		'payment_cost_per_day_left',
		'actual_progress_percent_net',
		'expected_end_time',
		'real_progress',
		'cost_per_day_left',
		'actual_progress_percent',
		'result_updated_on',
		'fb_staff_advertise',
		'staff_business',
		'status',
		'adaccounts'
	)
);
unset($columns);

// $insight_metrics = array(
// 	'account_id' => 1386382775013756
//     'account_name' => Adsplus.vn(no)
//     'call_to_action_clicks' => 0
//     'campaign_id' => 6071453649578
//     'campaign_name' => Banhduadailoan-TT
//     'canvas_avg_view_percent' => 0
//     'canvas_avg_view_time' => 0
//     'clicks' => 108
//     'cost_per_estimated_ad_recallers' => 527.6
//     'cost_per_inline_link_click' => 0
//     'cost_per_inline_post_engagement' => 722.739726
//     'cost_per_total_action' => 651.358025
//     'cost_per_unique_click' => 512.23301
//     'cost_per_unique_inline_link_click' => 0
//     'cpc' => 488.518519
//     'cpm' => 50391.595033
//     'cpp' => 50779.595765
//     'ctr' => 10.315186
//     'date_start' => 2017-08-08
//     'date_stop' => 2017-08-08
//     'estimated_ad_recall_rate' => 9.624639
//     'estimated_ad_recallers' => 100
//     'impressions' => 1047
//     'inline_link_click_ctr' => 0
//     'inline_link_clicks' => 0
//     'inline_post_engagement' => 73
//     'objective' => POST_ENGAGEMENT
//     'reach' => 1039
//     'social_clicks' => 28
//     'social_impressions' => 213
//     'social_reach' => 213
//     'social_spend' => 8012
//     'spend' => 52760
//     'total_action_value' => 0
//     'total_actions' => 81
//     'total_unique_actions' => 77
//     'actions.action_type.like' => 7
//     'actions.action_type.photo_view' => 66
//     'actions.action_type.post' => 1
//     'actions.action_type.post_reaction' => 7
//     'actions.action_type.page_engagement' => 81
//     'actions.action_type.post_engagement' => 74
//     'cost_per_action_type.action_type.like' => 7537.142857
//     'cost_per_action_type.action_type.photo_view' => 799.393939
//     'cost_per_action_type.action_type.post' => 52760
//     'cost_per_action_type.action_type.post_reaction' => 7537.142857
//     'cost_per_action_type.action_type.page_engagement' => 651.358025
//     'cost_per_action_type.action_type.post_engagement' => 712.972973
//     'cost_per_unique_action_type.action_type.commerce_event' => 26380
//     'cost_per_unique_action_type.action_type.like' => 7537.142857
//     'cost_per_unique_action_type.action_type.page_engagement' => 685.194805
//     'cost_per_unique_action_type.action_type.photo_view' => 799.393939
//     'cost_per_unique_action_type.action_type.post' => 52760
//     'cost_per_unique_action_type.action_type.post_engagement' => 712.972973
//     'cost_per_unique_action_type.action_type.post_reaction' => 7537.142857	
// );