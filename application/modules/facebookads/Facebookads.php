<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebookads_Package extends Package
{
	const ROOT_ROLE_ID = 1;
	
	function __construct()
	{
		parent::__construct();
		$this->website_id  = $this->uri->segment(4);
	}

	public function name()
	{
		return 'Facebook Ads';
	}

	public function init()
	{
		$this->_load_menu();
		// $this->_update_permissions();
	}

	/**
	 * Init menu LEFT | NAV ITEM FOR MODULE
	 * then check permission before render UI
	 */
	private function _load_menu()
	{
		$order = 0;
 
 		if( ! is_module_active('facebookads')) return FALSE;

 		$itemId = 'facebookads';

 		if(in_array($this->admin_m->role_id, [1, 5]))
		{
			$this->menu->add_item(array(
				'id' => 'facebookads-report',
				'name' => 'B/c Spend FacebookAds',
				'parent' => null,
				'slug' => '#',
				'order' => $order++,
				'icon' => 'fa fa-buysellads  fa-lg',
			), 'navbar');

			$reportItems = array(
				[
					'id' => uniqid('facebookads-report-'),
					'name' => 'Export B/c Spend tháng hiện tại',
					'url' => base_url('api-v2/facebookads/DataReport/monthly?'. http_build_query([ 'month' => date('m'), 'year' => date('Y'), 'download' => true ]))
				],
				[
					'id' => uniqid('facebookads-report-'),
					'name' => 'Export B/c Spend tháng ' . date('m', strtotime('-1 months')),
					'url' => base_url('api-v2/facebookads/DataReport/monthly?'. http_build_query([ 'month' => date('m', strtotime('-1 month')), 'year' => date('Y', strtotime('-1 month')), 'download' => true ]))
				],
				[
					'id' => uniqid('facebookads-report-'),
					'name' => 'Export B/c Spend tháng ' . date('m', strtotime('-2 months')),
					'url' => base_url('api-v2/facebookads/DataReport/monthly?'. http_build_query([ 'month' => date('m', strtotime('-2 month')), 'year' => date('Y', strtotime('-2 month')), 'download' => true ]))
				]
			);

			foreach($reportItems as $reportItem)
			{
				$this->menu->add_item(array(
					'id' => $reportItem['id'],
					'name' => $reportItem['name'],
					'parent' => 'facebookads-report',
					'slug' => $reportItem['url'],
					'order' => 101+$order++,
					'icon' => 'fa fa-buysellads  fa-lg',
				), 'navbar');
			}	
		}

		if(has_permission('facebookads.Index.access'))
		{
			$this->menu->add_item(array(
				'id'		=> $itemId,
				'name' 	    => 'Facebook Ads',
				'parent'    => null,
				'slug'      => admin_url('facebookads'),
				'order'     => $order++,
				'icon'		=> 'fa fa-lg fa-facebook-square',
				'is_active' => is_module('facebookads')
				), 'left-ads-service');	
		}

 		if(!is_module('facebookads')) return FALSE;
		
		if(has_permission('facebookads.Index.access'))
		{
			$this->menu->add_item(array(
			'id' => 'facebookads-service',
			'name' => 'Danh sách',
			'parent' => $itemId,
			'slug' => admin_url('facebookads'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-database'
			), 'left-ads-service');
		}

		if(has_permission('facebookads.staffs.manage'))
		{

			$this->menu->add_item(array(
			'id' => 'facebookads-staffs',
			'name' => 'Phân công/Phụ trách',
			'parent' => $itemId,
			'slug' => admin_url('facebookads/staffs'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-group'
			), 'left-ads-service');
		}

		$left_navs = array(
			'overview'=> array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-xs fa-tachometer',
				),
			'kpi' => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-xs fa-heartbeat',
				),
			'setting'  => array(
				'name' => 'Cấu hình',
				'icon' => 'fa fa-fw fa-xs fa-cogs',
				)
			) ;

		$term_id = $this->uri->segment(4);

		$this->website_id = $term_id;

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("facebookads.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "facebookads-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
				), 'left-ads-service');
		}
	}

	public function title()
	{
		return 'Facebook Ads';
	}

	public function author()
	{
		return 'tambt';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Quản lý hợp đồng Facebook';
	}
	
	/**
	 * Init default permissions for facebookads module
	 *
	 * @return     array  permissions default array
	 */
	private function init_permissions()
	{
		return array(

			'facebookads.index' => array(
				'description' => 'Quản lý Facebook Ads',
				'actions' => ['access','update','add','delete','manage','mdeparment','mgroup']),

			'facebookads.done' => array(
				'description' => 'Dịch vụ đã xong',
				'actions' => ['access','add','delete','update','manage','mdeparment','mgroup']),
		
			'facebookads.overview' => array(
				'description' => 'Tổng quan',
				'actions' => ['access','manage','mdeparment','mgroup']),

			'facebookads.kpi' => array(
				'description' => 'KPI',
				'actions' => ['access','add','delete','update','manage','mdeparment','mgroup']),
			
			'facebookads.start_service' => array(
				'description' => 'Kích hoạt Facebook Ads',
				'actions' => ['access','update','manage','mdeparment','mgroup']),
			
			'facebookads.stop_service' => array(
				'description' => 'Ngừng Facebook Ads',
				'actions' => ['access','update','manage','mdeparment','mgroup']),
			
			'facebookads.setting' => array(
				'description' => 'Cấu hình',
				'actions' => ['access','add','delete','update','manage']),

			'facebookads.metrics_core' => array(
				'description' => 'Quyền update các dữ liệu nâng cao',
				'actions' => array('update','manage')),

            'facebookads.unjoin_contract' => array(
                'description' => 'Quyền gỡ nối hợp đồng',
                'actions' => array('access', 'update', 'manage')
            ),

            'facebookads.rejoin_contract' => array(
                'description' => 'Quyền fix nối hợp đồng',
                'actions' => array('access', 'update', 'manage')
            ),

            'facebookads.fix' => array(
                'description' => 'Quyền fix dữ liệu (Chỉ dành cho admin)',
                'actions' => array('access', 'update', 'manage')
            ),
        );
            
	}

	private function _update_permissions()
	{
		$permissions = $this->init_permissions();
		if(empty($permissions)) return false;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	/**
	 * Install module
	 *
	 * @return     bool  status of command
	 */
	public function install()
	{
		$permissions = $this->init_permissions();
		if( ! $permissions) return FALSE;

		$rootPermissions = $this->role_permission_m->get_by_role_id(self::ROOT_ROLE_ID);
		$rootPermissions AND $rootPermissions = array_column($rootPermissions, 'role_id', 'permission_id');

		foreach($permissions as $name => $value)
		{
			$description   = $value['description'];
			$actions 	   = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);

			if( ! empty($rootPermissions[$permission_id])) continue;

			$this->role_permission_m->insert([
				'permission_id'	=> $permission_id,
				'role_id'		=> self::ROOT_ROLE_ID,
				'action'		=> serialize($actions)
			]);
		}

		return TRUE;
	}
	/**
	 * Uninstall module command
	 *
	 * @return     bool  status of command
	 */
	public function uninstall()
	{
		$permissions = $this->init_permissions();

		if(!$permissions) return FALSE;

		if($pers = $this->permission_m->like('name','facebookads')->get_many_by())
		{
			foreach($pers as $per)
			{
				$this->permission_m->delete_by_name($per->name);
			}
		}

		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}

		return TRUE;
	}
}
/* End of file facebookads.php */
/* Location: ./application/modules/facebookads/models/facebookads.php */