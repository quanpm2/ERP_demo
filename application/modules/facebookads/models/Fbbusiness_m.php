<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Term_m.php');

class FbBusiness_m extends Term_m 
{
    public $term_type = 'fbBusiness';

    /**
     * Sets the term type.
     *
     * @return     this
     */ 
    public function set_term_type()
    {
        return $this->where('term.term_type', $this->term_type);
    }
}
/* End of file FbBusiness_m.php */
/* Location: ./application/modules/contract/models/FbBusiness_m.php */