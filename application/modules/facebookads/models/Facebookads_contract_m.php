<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebookads_contract_m extends Base_contract_m {

	function __construct() 
	{
		parent::__construct();

		$models = ['facebookads/facebookads_kpi_m'];
		$this->load->model($models);
	}

	public function calc_contract_value($term_id = 0)
	{
		$term = $this->term_m->where('term_type','facebook-ads')->get($term_id);

		if(empty($term)) return FALSE;

		$total 			 		  	  = 0;

		$contract_budget			  = (int)get_term_meta_value($term->term_id,'contract_budget') ;
		$service_fee        		  = (int)get_term_meta_value($term->term_id,'service_fee');

		$facebookads_cost 	 		  = $contract_budget + $service_fee;
		$facebookads_discount_percent = (int)get_term_meta_value($term->term_id,'facebookads_discount_percent');

		if(!empty($facebookads_discount_percent) && $facebookads_discount_amound = ($facebookads_cost * div($facebookads_discount_percent,100)))
		{
			$facebookads_cost-=$facebookads_discount_amound;
		}

		$total+= $facebookads_cost;
		return $total;
	}

	public function has_permission($term_id = 0, $name = '', $action = '',$kpi_type = '',$user_id = 0,$role_id = null)
	{
		$this->load->model('facebookads_kpi_m');

		$permission = $name.'.'.$action;
		$permission = trim($permission, '.');
		
		if($this->permission_m->has_permission("{$name}.Manage",$role_id)
			&& $this->permission_m->has_permission($permission,$role_id)) 
			return TRUE;

		if(!$this->permission_m->has_permission($permission,$role_id)) return FALSE;

		$user_id = empty($user_id) ? $this->admin_m->id : $user_id;

		if($this->permission_m->has_permission('facebookads.sale.access',$role_id) 
			&& get_term_meta_value($term_id,'staff_business') == $user_id)
			return TRUE;

		$args = array(
			'term_id' => $term_id,
			'user_id' => $user_id);

		if(!empty($kpi_type))
		{
			$args['kpi_type'] = $kpi_type;
		}

		$kpis = $this->webgeneral_kpi_m->get_by($args);
		if(empty($kpis)) return FALSE;

		return TRUE;
	}

	public function start_service($term = null)
	{
		if(empty($term) ) return FALSE;

		if( ! $term || $term->term_type !='facebook-ads') return FALSE;
		$term_id = $term->term_id;

		$this->load->model('facebookads/fbads_report_m');
		$this->fbads_report_m->init($term->term_id);
		$this->fbads_report_m->send_started_email();

		
		/***
		 * Khởi tạo Meta thời gian dịch vụ để thuận tiện cho tính năng sắp xếp và truy vấn
		 */
		$start_service_time = get_term_meta_value($term_id, 'start_service_time');
		empty($start_service_time) AND update_term_meta($term_id, 'start_service_time', 0);
		
		$end_service_time = get_term_meta_value($term_id, 'end_service_time');
		empty($end_service_time) AND update_term_meta($term_id, 'end_service_time', 0);


		$this->load->config('facebookads/facebookads');
		update_term_meta($term_id, 'adaccount_status', $this->config->item('default', 'adaccount_status'));
		return TRUE;
	}

	
	/**
	 * Start proc service
	 *
	 * @param      Term_m    $term        The term
	 * @param      string|int  $start_time  The start time
	 *
	 * @return     bool    return TRUE If success , othersise return FALSE
	 */
	public function proc_service($term = FALSE, $start_time = '')
	{
		if(!$term || $term->term_type != 'facebook-ads') return FALSE;

		update_term_meta($term->term_id, 'start_service_time', time());
        update_term_meta($term->term_id, 'adaccount_status' , 'APPROVED');

        $this->load->model('facebookads/facebookads_report_m');
		$is_sent = $this->facebookads_report_m->send_activation_mail2customer($term->term_id);
		if( ! $is_sent) return FALSE;

		$this->log_m->insert(array(
			'log_title'		  => 'Kích hoạt thời gian chạy quảng cáo Facebook Ads',
			'log_status'	  => 1,
			'term_id'		  => $term->term_id,
			'log_type' 		  =>'start_service_time',
			'user_id' 		  => $this->admin_m->id,
			'log_content' 	  => 'Đã gửi mail kích hoạt thời gian chạy quảng cáo Facebook Ads',
			'log_time_create' => date('Y-m-d H:i:s'),
		));

		return TRUE;
	}


	/**
	 * Stops a service.
	 *
	 * @param      <type>  $term      The term
	 * @param      string  $end_time  The end time
	 *
	 * @return     bool    return TRUE If success , othersise return FALSE
	 */
	public function stop_service($term, $end_time = '')
	{
		$end_time = convert_time($end_time);
		$this->contract_m->update($term->term_id, ['term_status'=>'liquidation']);

		update_term_meta($term->term_id,'advertise_end_time',$end_time);
		update_term_meta($term->term_id,'end_service_time', time());
		$this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Thanh lý".');

		$this->log_m->insert(array(
			'log_type' =>'end_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => date('Y/m/d H:i:s')
			));

		$this->load->model('facebookads/facebookads_report_m');
		$result = $this->facebookads_report_m->send_finished_service($term->term_id);

		return $result;
	}
}

/* End of file facebookads_report_m.php */
/* Location: ./application/modules/facebookads/models/facebookads_report_m.php */