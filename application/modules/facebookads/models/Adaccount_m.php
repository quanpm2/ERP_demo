<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

use FacebookAds\Api;
use FacebookAds\Object\User;
use FacebookAds\Object\Fields\UserFields;

use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdsInsightsFields;
use FacebookAds\Object\Values\AdsInsightsDatePresetValues;

class Adaccount_m extends Term_m {
    
    const CACHE_ALL_KEY = "modules/facebookads/adaccounts-";

    public $term_type = 'adaccount';

    function __construct()
    {
        parent::__construct();
        $this->load->model('ads_segment_m');
        $this->load->model('facebookads/fbaccount_m');
        $this->load->model('facebookads/insight_segment_m');

		$this->load->config('facebookads/fbapp');
    }

    public function set_term_type()
	{
		return $this->where('term.term_type',$this->term_type);
	}

	/**
	 * Request Insight API Data
	 *
	 * @param      int              $adAccountId  The ad account identifier
	 * @param      <type>           $start_time   The start time
	 * @param      bool             $end_time     The end time
	 *
	 * @throws     Exception        (description)
	 *
	 * @return     AdAccount|array  ( description_of_the_return_value )
	 */
	protected function requestInsightAPI($adAccountId = 0, $start_time = FALSE, $end_time = FALSE)
	{
		$term = $this->set_term_type()->get($adAccountId);
		if( ! $term) return FALSE;

		

		$fbAppEnv		= $this->config->item('default', 'fbapps');
		$fbApp 			= $this->config->item($fbAppEnv, 'fbapps');
		$app_id 		= $fbApp['credential']['app_id'] ?? null;
		$app_secret 	= $fbApp['credential']['app_secret'] ?? null;

		$fbAccounts 	= array();
		$fbAccounts[] 	= array(
			'user_id' => null,
			'access_token' => ( (array) $this->config->item('production', 'fbapps'))['credential']['access_token'] ?? null
		);		
		
		$_fbAccounts = $this->term_users_m->get_the_users($adAccountId, $this->fbaccount_m->type);
		$_fbAccounts AND $fbAccounts = array_merge($fbAccounts, array_map(function($x){
			return [
				'user_id' => $x->user_id,
				'access_token' => get_user_meta_value($x->user_id, 'access_token')
			];
		}, $_fbAccounts));
		$result = array();

		foreach ($fbAccounts as $fbAccount)
		{
			$fbAccount = (array) $fbAccount;

			empty($fbAccount['access_token']) AND $fbAccount['access_token'] = get_user_meta_value($fbAccount['user_id'], 'access_token');
			Api::init($app_id, $app_secret, $fbAccount['access_token']);

			$params = array( 'time_increment' => 1, 'date_preset' => AdsInsightsDatePresetValues::LAST_7D );

			if( ! empty($start_time))
			{
				unset($params['date_preset']);

				$start_date = my_date($start_time,'Y-m-d');
				$end_date 	= $end_time ? my_date($end_time,'Y-m-d') : my_date(0,'Y-m-d');

				$params['time_range'] = (object) array(
					'since'	=> $start_date,
					'until'	=> $end_date
				);
			}

			$adAccount = new AdAccount($term->term_slug);	

			try
			{
				$insights 	= $adAccount->getInsights($this->get_fields(), $params);
				$content 	= $insights->getLastResponse()->getContent();
				while ( ! empty($content['paging']['next']))
				{
					try
					{
						$insights->fetchAfter();	
					} 
					catch (Exception $e)
					{
						log_message('error', $e->getMessage());
						throw new Exception($e->getMessage());
						break;
					}

					$content = $insights->getLastResponse()->getContent();
				}

				update_term_meta($term->term_id, 'fbaccount_status_with_user_id_' . ($fbAccount['user_id'] ?: 0), 'established');

				$result = $insights->getObjects();
			} 
			catch (\FacebookAds\Http\Exception\AuthorizationException $e)
			{
				update_term_meta($term->term_id, 'fbaccount_status_with_user_id_' . ($fbAccount['user_id'] ?: 0), 'unauthorized');
				log_message('error', $e->getMessage());
				continue;
			}
			catch (Exception $e)
			{
				log_message('error', $e->getMessage());
				continue;
			}	
		}

		$numOfconnectionEstablished = array_filter($fbAccounts, function($x) use ($term){
			$api_connected = get_term_meta_value($term->term_id, 'fbaccount_status_with_user_id_' . ($x['user_id'] ?: 0));
			return $api_connected != 'unauthorized';
		});

		if( ! $numOfconnectionEstablished) $this->set_term_type()->update($term->term_id, ['term_status' => 'unlinked']);

		return $result;
	}

	/**
	 * Gets the insight.
	 *
	 * @param      integer   $term_id     The term identifier
	 * @param      <type>    $start_time  The start time
	 * @param      <type>    $end_time    The end time
	 *
	 * @return     Campaign  The insight.
	 */
	public function get_insight($term_id = 0, $start_time = FALSE, $end_time = FALSE, $contract_id = 0)
	{
		$term = $this->set_term_type()->get($term_id);
		if( ! $term) return FALSE;

		$result 	= array();
		$insights 	= $this->requestInsightAPI($term_id, $start_time, $end_time);
		if(empty($insights)) return FALSE;

        $this->load->model('option_m');
        $exchange_rate_option = $this->option_m->get_value('exchange_rate', TRUE);
		foreach ($insights as $key => $insight)
		{
			$data = $insight->getData();
			if(empty($data)) continue;

			$data 			= array_filter($data);
			$_dataParsed 	= $this->parseDataInsight($data);
			$datetime		= start_of_day($data['date_start']);

			$_isegment = $this->term_posts_m->get_term_posts($term_id, $this->insight_segment_m->post_type, [
				'fields' => 'posts.post_id',
				'where' => [
					'start_date' 	=> $datetime,
					'end_date' 		=> $datetime,
					'post_name' 	=> 'day'
				]
			]);

            /* Create new if empty */
            if(empty($_isegment))
			{
                $_isegmentInsert 	= [
			    	'post_name' 	=> 'day',
			    	'post_type' 	=> 'insight_segment',
			    	'start_date' 	=> $datetime,
			    	'end_date' 		=> $datetime
			    ];

			    $_isegment_id = $this->insight_segment_m->insert($_isegmentInsert);

                $this->term_posts_m->set_term_posts($term_id, [$_isegment_id], $this->insight_segment_m->post_type);
            
                $_isegment = (object) $_isegmentInsert;
			    $_isegment->post_id = $_isegment_id;
            }

			is_array($_isegment) AND $_isegment = reset($_isegment);

            $account_currency = $data['account_currency'];
            if('VND' != $account_currency)
			{
                $exchange_rate = (int)get_post_meta_value($_isegment->post_id, 'exchange_rate');
                empty($exchange_rate) AND $exchange_rate = (double) get_term_meta_value($contract_id, strtolower("exchange_rate_{$data['account_currency']}_to_vnd"));
                empty($exchange_rate) AND $exchange_rate = (double) $exchange_rate_option[strtolower("{$data['account_currency']}_to_vnd")] ?? 0;
                
                $data['exchange_rate'] = $exchange_rate;
                $data['cost'] = (double) $data['spend'];
                $data['spend'] = (double) $data['spend'] * $exchange_rate;
            }
            else
			{
                $data['spend'] = $data['cost'] = (double) $data['spend'];
                $data['exchange_rate'] = 1;
            }

			array_walk($data, function($value, $key) use($_isegment){
				update_post_meta($_isegment->post_id, $key, $value);
			});

			$result[$datetime] = $data;
		}

		return $result;
	}

	/**
	 * Parse DAta Insight to Meta
	 *
	 * @param      <type>  $data   The data
	 */
	protected function parseDataInsight($data)
	{
		! empty($data['actions']) AND array_walk($data['actions'], function($x) use(&$item){
			$data["actions.action_type.{$x['action_type']}"] = $x['value'];
		});

		! empty($data['cost_per_action_type']) AND array_walk($data['cost_per_action_type'], function($x) use(&$item){
			$data["cost_per_action_type.action_type.{$x['action_type']}"] = $x['value'];
		});

		! empty($data['cost_per_unique_action_type']) AND array_walk($data['cost_per_unique_action_type'], function($x) use(&$item){
			$data["cost_per_unique_action_type.action_type.{$x['action_type']}"] = $x['value'];
		});

		! empty($data['cost_per_10_sec_video_view']) AND array_walk($data['cost_per_10_sec_video_view'], function($x) use(&$item){
			$data["cost_per_10_sec_video_view.action_type.{$x['action_type']}"] = $x['value'];
		});

		unset($data['actions']);
		unset($data['cost_per_action_type']);
		unset($data['cost_per_10_sec_video_view']);
		unset($data['cost_per_unique_action_type']);

		foreach ($data as $key => $value)
		{
			is_numeric($value) AND $data[$key] = (double) $data[$key];
			if( ! is_numeric($data[$key]) && empty($data[$key])) unset($data[$key]);
		}

		return $data;
	}


	/**
	 * Gets the AdCampaign's data insight
	 * If time input not defined , default is get LAST_7_DAYS
	 *
	 * @param      integer  $term_id     The term identifier
	 * @param      integer  $start_time  The start time
	 * @param      integer  $end_time    The end time
	 *
	 * @return     array    The data.
	 */
	public function get_data($term_id = 0,$start_time = FALSE, $end_time = FALSE)
	{
		$end_time		= $end_time ?: time();
		$end_time       = end_of_day($end_time);
		$start_time 	= $start_time ?: strtotime('-7 days',$end_time);
		$start_time 	= start_of_day($start_time);

		$iSegments = $this->term_posts_m->get_term_posts($term_id, $this->insight_segment_m->post_type, [
			'fields' => 'posts.post_id, posts.start_date',
			'where' => [
				'start_date >='	=> $start_time,
				'start_date <='	=> $end_time,
				'post_name' 	=> 'day'
			]
		]);

		if(empty($iSegments)) return [];

		$data = array_map(function($x){
			
			$metadata 		= get_post_meta_value($x->post_id);
			if(empty($metadata)) return [];

			$x = array_merge( (array) $x, array_column($metadata, 'meta_value', 'meta_key'));
			$x['post_id'] 	= (int) $x['post_id'];
			$x['time'] 		= (int) $x['start_date'];
			$x['start_date']= (int) $x['start_date'];
			$x['spend'] 	= (double) ($x['spend']??0);
			return $x;
		}, $iSegments);

		return array_values(array_filter($data));
	}


	/**
	 * Insert adaccount object to DB similar Insert command
	 *
	 * @param      <type>  $data   The data
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function add($data, $access_token = '')
	{
		if(empty($data)) return FALSE;

		$adaccount = $this->set_term_type()->get_by('term_slug',$data->id);
		if($adaccount) return FALSE;

		$insert_id = $this->insert(array(
			'term_slug' => $data->id,
			'term_name' => $data->name,
			'term_status' => $data->account_status,
			'term_type' => $this->term_type
		));

		$meta = array(
            'created_time' => $data->created_time,
            'currency' => $data->currency,
            'owner' => $data->owner,
            'access_token' => $access_token ?: (((array) $this->config->item('production', 'fbapps'))['credential']['access_token'] ?? null)
        );

        foreach ($meta as $meta_key => $meta_value)
        {
            update_term_meta($insert_id,$meta_key,$meta_value);
        }

        return $insert_id;
	}

	public function get_fields()
	{
		return array(
			'account_currency',
			'account_id',
			'account_name',
			'clicks',
			'cpc',
			'cpm',
			'cpp',
			'ctr',
			'date_start',
			'date_stop',
			'impressions',
			'objective',
			'reach',
			'spend',
		);
	}
}
/* End of file Adaccount_m.php */
/* Location: ./application/modules/facebookads/models/Adaccount_m.php */
