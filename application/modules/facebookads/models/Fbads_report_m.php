<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Adsplus_report_m.php');

class Fbads_report_m extends Adsplus_report_m {

    function __construct()
    {
        $this->recipients['bcc'][] = 'thonh@webdoctor.vn';
        $this->autoload['models'][] = 'facebookads/facebookads_m';
        $this->autoload['models'][] = 'facebookads/facebookads_kpi_m';

        parent::__construct();
    }
    
    /**
     * Sends a started mail to local staffs - department
     */
    public function send_started_email()
    {
        $this->config->load('googleads/googleads');
        $this->add_to('thonh@webdoctor.vn');
        // $this->add_bcc('thonh@webdoctor.vn');
        return parent::send_started_email();
    }

    /**
     * Sends an activation email.
     *
     * @param      string  $type_to  The type to
     */
    public function send_activation_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;

        $this->data['subject'] = "[ADSPLUS.VN] Thông báo kích hoạt dịch vụ Facebook của website {$this->term->term_name}";
        $this->data['content_tpl'] = 'facebookads/report/activation_email';
        return parent::send_activation_email($type_to);
    }

    /**
     * Sends a progress email.
     *
     * @param      string  $budget_type  The budget type commit : progress data
     *                                   with contract budget actual : progress
     *                                   data with actual payment budget
     * @param      string  $type_to  The type to
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function send_progress_email($budget_type = 'commit')
    {
        if( ! $this->term) return FALSE;

        $this->data['content_tpl'] = 'facebookads/report/progress_email';
        return parent::send_progress_email($budget_type);
    }
    
    
    public function init($term_id = 0)
    {
        parent::init($term_id);

        if( ! $this->term || $this->term->term_type != $this->facebookads_m->term_type) return FALSE;

        $this->init_people_belongs();

        $this->data['googleads_begin_time'] = get_term_meta_value($this->term->term_id, 'advertise_start_time');
        return $this;
    }


    /**
     * Loads workers.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_workers()
    {
        parent::load_workers();

        $kpis = $this->facebookads_kpi_m->select('user_id')->where('term_id',$this->term_id)->group_by('user_id')->get_many_by();
        if( ! $kpis) return $this;

        $_workers       = array_map(function($x){ return $this->admin_m->get_field_by_id($x->user_id); }, $kpis);
        $this->workers  = wp_parse_args(array_column($_workers, NULL, 'user_email'), $this->workers);

        return $this;
    }
}
/* End of file Fbads_report_m.php */
/* Location: ./application/modules/facebookads/models/Fbads_report_m.php */