<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

use FacebookAds\Api;

use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Fields\AdsInsightsFields;
use FacebookAds\Object\Values\AdsInsightsDatePresetValues;

class Adcampaign_m extends Term_m {

    public $term_type = 'adcampaign';

    function __construct()
    {
        parent::__construct();
        $models = array(
        	'facebookads/adaccount_m'
        );

        $this->load->model($models);
    }

    public function set_term_type()
	{
		return $this->where('term_type',$this->term_type);
	}


	/**
	 * Get all Ad campaigns via facebook ads api.
	 *
	 * @param      integer  $adaccount_term_id  The adaccount term identifier
	 * @param      bool		$refresh            The refresh
	 *
	 * @return     AdAccount   All via fbapi.
	 */
	public function get_all_via_fbapi($adaccount_term_id = 0,$refresh = FALSE)
	{
		$adaccount_term = $this->adaccount_m->set_term_type()->get($adaccount_term_id);
		if( ! $adaccount_term) return FALSE;
		
		// Get access token key for specified adaccount
		// $access_token = get_term_meta_value($adaccount_term_id,'access_token');
		// if(empty($access_token)) return FALSE;

		// Initialize a new Session and instantiate an API object
		Api::init(getenv('FB_APP_ID'),getenv('FB_APP_SECRET'),getenv('FB_ACCESS_TOKEN'));

		// Key obfuscated
		$cache_key = "modules/facebookads/facebookadsobject/{$adaccount_term_id}_all_adcampaigns";
		$adcampaings = $this->scache->get($cache_key);
		if(empty($adcampaings) || $refresh === TRUE)
		{
			try
			{	
				$adaccount 			= new AdAccount($adaccount_term->term_slug); // Construct AdAccount by account_id
				$campaign_fields 	= array_keys((new CampaignFields())->getFieldTypes()); // Get campaign fields default fbadssdk

				$campaigns 			= $adaccount->getCampaigns($campaign_fields);

				$content = $campaigns->getLastResponse()->getContent();
				while (!empty($content['paging']['next']))
				{
					try
					{
						$campaigns->fetchAfter();	
					} 
					catch (Exception $e)
					{
						break;	
					}

					$content = $campaigns->getLastResponse()->getContent();	
				}



				$adcampaings = $campaigns->getObjects();
				if(empty($adcampaings)) return FALSE;

				$this->scache->write($adcampaings,$cache_key);
			} 
			catch (Exception $e) { 
				log_message('error',$e->getMessage());
			}
		}

		return $adcampaings;	
	}



	/**
	 * Gets the insight.
	 *
	 * @param      integer   $term_id     The term identifier
	 * @param      <type>    $start_time  The start time
	 * @param      <type>    $end_time    The end time
	 *
	 * @return     Campaign  The insight.
	 */
	public function get_insight($term_id = 0,$start_time = FALSE, $end_time = FALSE)
	{
		$term = $this->set_term_type()->get($term_id);
		if( ! $term) return FALSE;
		Api::init(getenv('FB_APP_ID'),getenv('FB_APP_SECRET'),getenv('FB_ACCESS_TOKEN'));

		$params = array();
		$params['time_increment'] = 1;
		$params['date_preset'] = AdsInsightsDatePresetValues::LAST_7D;

		if(!empty($start_time))
		{
			unset($params['date_preset']);

			$start_date = my_date($start_time,'Y-m-d');
			$end_date = $end_time ? my_date($end_time,'Y-m-d') : my_date(0,'Y-m-d');
			
			$params['time_range'] = (object) array('since'=>$start_date,'until'=>$end_date);
		}
		

		$campaign = new Campaign($term->term_slug);

		try
		{
			$insights = $campaign->getInsights($this->get_fields(), $params);

			$content = $insights->getLastResponse()->getContent();
			while (!empty($content['paging']['next']))
			{
				try
				{
					$insights->fetchAfter();	
				} 
				catch (Exception $e)
				{
					break;	
				}

				$content = $insights->getLastResponse()->getContent();	
			}

			$insights = $insights->getObjects();
		} 
		catch (Exception $e)
		{
			log_message('error',$e->getMessage());
			return FALSE;
		}

		if(empty($insights))
		{
			return FALSE;
		}

		foreach ($insights as $key => $insight)
		{
			$data = $insight->getData();
			if(empty($data)) continue;

			$datetime = $this->mdate->startOfDay($data['date_start']);
			update_term_meta($term_id,"insight_{$datetime}",serialize($data));

			$insights[$datetime] = $data;
		}

		return $insights;
	}


	/**
	 * Gets the AdCampaign's data insight
	 * If time input not defined , default is get LAST_7_DAYS
	 *
	 * @param      integer  $term_id     The term identifier
	 * @param      integer  $start_time  The start time
	 * @param      integer  $end_time    The end time
	 *
	 * @return     array    The data.
	 */
	public function get_data($term_id = 0,$start_time = FALSE, $end_time = FALSE)
	{
		$end_time		= $end_time ?: time();
		$end_time		= $this->mdate->endOfDay($end_time);
		$start_time 	= $start_time ?: strtotime('-7 days',$end_time);
		$start_time 	= $this->mdate->startOfDay($start_time);
		$adcampaigns	= array();

		while ($start_time < $end_time)
		{
			$insight = get_term_meta_value($term_id,"insight_{$start_time}");
			$insight_date_start = $start_time;
			$start_time = strtotime('+1 days',$start_time);
			
			if(empty($insight)) continue;

			$insight = unserialize($insight);
			
			$result = 0;
			$cost_per_result = 0;
			$objective = strtolower($insight['objective']);

			$actions = $insight['actions'] ?? array();
			$actions = array_column($actions, NULL,'action_type');
			$cost_per_action_type = $insight['cost_per_action_type'] ?? array();
			$cost_per_action_type = array_column($cost_per_action_type, NULL,'action_type');

			switch ($insight['objective'])
			{
				case 'VIDEO_VIEWS':
					$result = $actions['video_view']['value'] ?? 0;
					$cost_per_result = $cost_per_action_type['video_view']['value'] ?? 0;
					break;

				case 'POST_ENGAGEMENT': // Get objective::POST_ENGAGEMENT exactly "RESULT" and "COST PER RESULT"
					$result = $actions['post_engagement']['value'] ?? 0;
					$cost_per_result = $cost_per_action_type['post_engagement']['value'] ?? 0;
					break;
				
				default:
					$result = $insight["inline_{$objective}"] ?? ($actions[$objective]['value'] ?? 0);
					$cost_per_result = $insight["cost_per_inline_{$objective}"] ?? ($cost_per_action_type[$objective]['value'] ?? 0);
					break;
			}

			$insight['result'] = $result;
			$insight['cost_per_result'] = $cost_per_result;

			// Xử lý số lượng result và số lượng cost_per{$objective} của mỗi loại campaign objective
			$adcampaigns[$insight_date_start] = $insight;
		}

		return $adcampaigns;
	}


	/**
	 * Insert adaccount object to DB similar Insert command
	 *
	 * @param      <type>  $data   The data
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function add($data,$adaccount_term_id = 0)
	{
		if(empty($data)) return FALSE;

		// Get access token key for specified adaccount
		$access_token = get_term_meta_value($adaccount_term_id,'access_token');
		if(empty($access_token)) return FALSE;

		$adcampaign = $this->set_term_type()->get_by(['term_parent'=>$adaccount_term_id,'term_slug'=>$data->id]);
		// If adcampaign is exists in storage
		if($adcampaign) 
		{
			$this->update($adcampaign->term_id,['term_name'=>$data->name,'term_status'=>$data->status]);
			return $adcampaign->term_id;
		}

		$insert_id = $this->insert(array(
				'term_slug' 	=> $data->id, // fb object Adcampaign ID
				'term_name' 	=> $data->name, // fb object adcampaign Name
				'term_status' 	=> $data->status, // fb object adcampaign status
				'term_type' 	=> $this->term_type,
				'term_parent' 	=> $adaccount_term_id
            ));
            
		$meta = array(
			'objective' 	=> $data->objective, // type post engagement | inclick | ...
			'spend_cap' 	=> $data->spend_cap,
			'start_time' 	=> $data->start_time, 
			'stop_time' 	=> $data->stop_time,
			'campaign_name' => $data->name,
			'campaign_id' 	=> $data->id,
			'created_time' 	=> $data->created_time,
			'updated_time' 	=> $data->updated_time
            );

        foreach ($meta as $meta_key => $meta_value)
        {
            update_term_meta($insert_id,$meta_key,$meta_value);
        }

        return $insert_id;
	}

	public function get_fields()
	{
		return array(
			'account_id',
			'account_name',
			'action_values',
			'actions',
			'ad_id',
			'ad_name',
			'adset_id',
			'adset_name',
			'campaign_id',
			'campaign_name',
			'canvas_avg_view_percent',
			'canvas_avg_view_time',
			'clicks',
			// 'cost_per_10_sec_video_view',
			'cost_per_action_type',
			'cost_per_estimated_ad_recallers',
			'cost_per_inline_link_click',
			'cost_per_inline_post_engagement',
			'cost_per_unique_action_type',
			'cost_per_unique_click',
			'cost_per_unique_inline_link_click',
			'cpc',
			'cpm',
			'cpp',
			'ctr',
			'date_start',
			'date_stop',
			'estimated_ad_recall_rate',
			'estimated_ad_recallers',
			'impressions',
			'inline_link_click_ctr',
			'inline_link_clicks',
			'inline_post_engagement',
			'objective',
			'reach',
			// 'relevance_score',
			'social_spend',
			'spend',
			);
	}
}
/* End of file AdCampaign_m.php */
/* Location: ./application/modules/facebookads/models/AdCampaign_m.php */