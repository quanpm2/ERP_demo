<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;

use FacebookAds\Object\Fields\AdsInsightsFields;
use FacebookAds\Object\Values\AdsInsightsDatePresetValues;

/**
 * A base model with a series of CRUD functions,
 * validation-in-model support, event callbacks and more.
 * 
 * @copyright Copyright (c) 2017, Thọ Nguyễn Hữu <thonh@webdoctor.vn>
 */
class Adset_m extends Post_m
{
	/**
     * This model's post_type default field value identifier.
     */
    public $post_type = 'adset';


    /* --------------------------------------------------------------
     * GENERIC METHODS
     * ------------------------------------------------------------ */


    /**
     * Initialise the model
     */
    function __construct()
    {
        parent::__construct();
        $models = array('facebookads/adaccount_m','facebookads/adcampaign_m','term_posts_m');
        $this->load->model($models);
    }

    /**
     * Sets the post type.
     *
     * @return     this
     */
    public function set_post_type()
	{
		return $this->where('post_type',$this->post_type);
	}


	/**
	 * Get all Adsets via facebook ads api.
	 *
	 * @param      integer    $adcampaign_term_id  The adcampaign term identifier
	 * @param      bool       $refresh             The refresh
	 *
	 * @return     Adsets 	All via fbapi.
	 */
	public function get_all_via_fbapi($adcampaign_term_id = 0,$refresh = FALSE)
	{
		$adcampaign_term = $this->adcampaign_m->set_term_type()->get($adcampaign_term_id);
		if( ! $adcampaign_term) return FALSE;

		$adaccount_term = $this->adaccount_m->set_term_type()->get($adcampaign_term->term_parent);
		if( ! $adaccount_term) return FALSE;
		
		// Get access token key for specified adaccount
		$access_token = get_term_meta_value($adaccount_term->term_id,'access_token');
		if(empty($access_token)) return FALSE;

		// Initialize a new Session and instantiate an API object
		Api::init(getenv('FB_APP_ID'),getenv('FB_APP_SECRET'),$access_token);

		// Key obfuscated
		$cache_key = "modules/facebookads/facebookadsobject/{$adcampaign_term_id}_all_adsets";
		$adsets = $this->scache->get($cache_key);
		if(empty($adsets) || $refresh === TRUE)
		{
			try
			{	
				$adcampaign 	= new Campaign($adcampaign_term->term_slug);
				$adsets 		= $adcampaign->getAdSets($this->get_fields());

				$content = $adsets->getLastResponse()->getContent();
				while (!empty($content['paging']['next']))
				{
					try { $adsets->fetchAfter(); } 
					catch (Exception $e) { break; }

					$content = $adsets->getLastResponse()->getContent();	
				}

				$adsets = $adsets->getObjects();

				if(empty($adsets)) return FALSE;

				$this->scache->write($adsets,$cache_key);
			} 
			catch (Exception $e) { log_message('error',$e->getMessage());}
		}

		return $adsets;	
	}


	/**
	 * Gets the insight.
	 *
	 * @param      integer   $post_id     The Adset identifier
	 * @param      <type>    $start_time  The start time
	 * @param      <type>    $end_time    The end time
	 *
	 * @return     Adset  The insight.
	 */
	public function get_insight($post_id = 0,$start_time = FALSE, $end_time = FALSE)
	{
		$post = $this->set_post_type()->get($post_id);
		if( ! $post) return FALSE;

		Api::init(getenv('FB_APP_ID'),getenv('FB_APP_SECRET'),getenv('FB_ACCESS_TOKEN'));

		$params = array();
		$params['time_increment'] = 1;
		$params['date_preset'] = AdsInsightsDatePresetValues::LAST_7D;

		if(!empty($start_time))
		{
			unset($params['date_preset']);

			$start_date = my_date($start_time,'Y-m-d');
			$end_date = $end_time ? my_date($end_time,'Y-m-d') : my_date(0,'Y-m-d');
			
			$params['time_range'] = (object) array('since'=>$start_date,'until'=>$end_date);
		}
		
		$adset = new AdSet($post->post_slug);
		try
		{
			$insights = $adset->getInsights($this->get_insight_fields(), $params);

			$content = $insights->getLastResponse()->getContent();
			while (!empty($content['paging']['next']))
			{
				try
				{
					$insights->fetchAfter();	
				} 
				catch (Exception $e)
				{
					break;	
				}

				$content = $insights->getLastResponse()->getContent();	
			}

			$insights = $insights->getObjects();
		} 
		catch (Exception $e)
		{
			log_message('error',$e->getMessage());
			return FALSE;
		}

		if(empty($insights))
		{
			return FALSE;
		}

		foreach ($insights as $key => $insight)
		{
			$data = $insight->getData();
			if(empty($data)) continue;

			$datetime = $this->mdate->startOfDay($data['date_start']);
			update_post_meta($post->post_id,"insight_{$datetime}",serialize($data));

			$insights[$datetime] = $data;
		}

		return $insights;
	}


	/**
	 * Gets the AdCampaign's data insight If time input not defined , default is
	 * get LAST_7_DAYS
	 *
	 * @param      integer  $post_id     The term identifier
	 * @param      integer  $start_time  The start time
	 * @param      integer  $end_time    The end time
	 *
	 * @return     array    The data.
	 */
	public function get_data($post_id = 0,$start_time = FALSE, $end_time = FALSE)
	{
		$end_time		= $end_time ?: time();
		$end_time		= $this->mdate->endOfDay($end_time);
		$start_time 	= $start_time ?: strtotime('-7 days',$end_time);
		$start_time 	= $this->mdate->startOfDay($start_time);

		$adsets	= array();

		while ($start_time < $end_time)
		{
			$insight = get_post_meta_value($post_id,"insight_{$start_time}");
			$insight_date_start = $start_time;
			$start_time = strtotime('+1 days',$start_time);
			
			if(empty($insight)) continue;

			$insight = unserialize($insight);
			
			$result = 0;
			$cost_per_result = 0;
			$objective = strtolower($insight['objective']);

			$actions = $insight['actions'] ?? array();
			$actions = array_column($actions, NULL,'action_type');
			$cost_per_action_type = $insight['cost_per_action_type'] ?? array();
			$cost_per_action_type = array_column($cost_per_action_type, NULL,'action_type');

			switch ($insight['objective'])
			{
				case 'VIDEO_VIEWS':
					$result = $actions['video_view']['value'] ?? 0;
					$cost_per_result = $cost_per_action_type['video_view']['value'] ?? 0;
					break;

				case 'POST_ENGAGEMENT': // Get objective::POST_ENGAGEMENT exactly "RESULT" and "COST PER RESULT"
					$result = $actions['post_engagement']['value'] ?? 0;
					$cost_per_result = $cost_per_action_type['post_engagement']['value'] ?? 0;
					break;
				
				default:
					$result = $insight["inline_{$objective}"] ?? ($actions[$objective]['value'] ?? 0);
					$cost_per_result = $insight["cost_per_inline_{$objective}"] ?? ($cost_per_action_type[$objective]['value'] ?? 0);
					break;
			}

			$insight['result'] = $result;
			$insight['cost_per_result'] = $cost_per_result;

			// Xử lý số lượng result và số lượng cost_per{$objective} của mỗi loại campaign objective
			$adsets[$insight_date_start] = $insight;
		}

		return $adsets;
	}


	/**
	 * Insert adset object to DB similar Insert command
	 *
	 * @param      <type>   $data                The data
	 * @param      integer  $adcampaign_term_id  The adcampaign term identifier
	 *
	 * @return     integer $adset_post_id
	 */
	public function add($data,$adcampaign_term_id = 0)
	{
		if(empty($data)) return FALSE;
		$adcampaign_term = $this->adcampaign_m->set_term_type()->get($adcampaign_term_id);
		if( ! $adcampaign_term) return FALSE;

		$adaccount_term = $this->adaccount_m->set_term_type()->get($adcampaign_term->term_parent);
		if( ! $adaccount_term) return FALSE;

		// Get access token key for specified adaccount
		$access_token = get_term_meta_value($adaccount_term->term_id,'access_token');
		if(empty($access_token)) return FALSE;

		$adset_data = array(
			'post_slug' 	=> $data->id, // fb object Adcampaign ID
			'post_name'		=> $data->name, /* fb object adcampaign Name */
			'post_status'	=> $data->status, /* fb object adcampaign status*/
			'post_type' 	=> $this->post_type,
			'start_date' 	=> $this->mdate->convert_time($data->start_time),
			'end_date' 		=> !empty($data->end_time) ? $this->mdate->convert_time($data->end_time) : '',
			'post_content' 	=> $data->optimization_goal,
			'created_on'	=> $this->mdate->convert_time($data->created_time),
			'updated_on' 	=> !empty($data->updated_time) ? $this->mdate->convert_time($data->updated_time) : '',
        );

		// If adcampaign is exists in storage
		$adset = $this->set_post_type()->get_by(['post_slug'=>$data->id]);
		if($adset)
		{
			if($adset_data['updated_on'] > $adset->updated_on) 
				$this->update($adset->post_id,$adset_data);
			return $adset->post_id;
		}

		$insert_id = $this->insert($adset_data);
		$this->term_posts_m->set_term_posts($adcampaign_term_id,[$insert_id]);

		return $insert_id;
	}


	/**
	 * Gets the object adset fields.
	 *
	 * @return     array  The adset fields.
	 */
	public function get_fields()
	{
		return array(
			'account_id',// => 'string',
			// 'adlabels',// => 'list<AdLabel>',
			// 'adset_schedule',// => 'list<DayPart>',
			'attribution_spec',// => 'list<Object>',
			// 'bid_amount',// => 'unsigned int',
			// 'bid_info',// => 'map<string, unsigned int>',
			'billing_event',// => 'BillingEvent',
			'budget_remaining',// => 'string',
			'campaign',// => 'Campaign',
			'campaign_id',// => 'string',
			// 'configured_status',// => 'ConfiguredStatus',
			'created_time',// => 'datetime',
			'creative_sequence',// => 'list<string>',
			'daily_budget',// => 'string',
			'effective_status',// => 'EffectiveStatus',
			'end_time',// => 'datetime',
			// 'frequency_cap',// => 'unsigned int',
			// 'frequency_cap_reset_period',// => 'unsigned int',
			// 'frequency_control_specs',// => 'list<Object>',
			'id',// => 'string',
			'is_autobid',// => 'bool',
			// 'is_average_price_pacing',// => 'bool',
			// 'lifetime_budget',// => 'string',
			// 'lifetime_frequency_cap',// => 'unsigned int',
			// 'lifetime_imps',// => 'int',
			'name',// => 'string',
			'optimization_goal',// => 'OptimizationGoal',
			// 'pacing_type',// => 'list<string>',
			// 'promoted_object',// => 'AdPromotedObject',
			// 'recommendations',// => 'list<AdRecommendation>',
			// 'recurring_budget_semantics',// => 'bool',
			// 'rf_prediction_id',// => 'string',
			// 'rtb_flag',// => 'bool',
			'start_time',// => 'datetime',
			'status',// => 'Status',
			// 'targeting',// => 'Targeting', Field này liệt kê độ tuổi (min|max) , nghề nghiệp , trình độ học vấn , công ty , vị trí làm việc,địa chị , nơi làm việc , publisher_platforms
			// 'time_based_ad_rotation_id_blocks',// => 'list<list<int>>',
			// 'time_based_ad_rotation_intervals',// => 'list<unsigned int>',
			'updated_time',// => 'datetime',
			// 'use_new_app_click',// => 'bool',
			// 'campaign_spec',// => 'Object',
			// 'daily_imps',// => 'unsigned int',
			// 'execution_options',// => 'list<ExecutionOptions>',
			// 'redownload',// => 'bool',
			);
	}


	/**
	 * Gets the insight fields.
	 *
	 * @return     array  The insight fields.
	 */
	public function get_insight_fields()
	{
		$fields = (new AdsInsightsFields())->getFieldTypes();

		unset($fields['age'],$fields['call_to_action_asset'],$fields['country'],$fields['device_platform'],$fields['dma'],$fields['frequency_value'],$fields['gender'],$fields['hourly_stats_aggregated_by_advertiser_time_zone'],$fields['hourly_stats_aggregated_by_audience_time_zone'],$fields['impression_device'],$fields['place_page_id'],$fields['placement'],$fields['product_id'],$fields['region'],$fields['impressions_dummy']);

		return array_keys($fields);
	}

}/* End of file Adset_m.php */
/* Location: ./application/modules/facebookads/models/Adset_m.php */