<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'models/Commission_m.php');

class Commission_facebookads_m extends Commission_m {

	protected $service_type = NULL;
    protected $commission_package = 'account_type';

    public function get_commission_package()
    {
        return $this->commission_package;
    }

    /**
     * Gets the commision rate.
     *
     * @param      integer  $amount  The amount
     *
     * @return     integer  The rate.
     */
    public function get_rate($amount = 0)
    {
        $package    = $this->commission_package;
        $commission = $this->config->item($package, 'commission');

        if(empty($commission)) return 0;

        $rate = 0;
        foreach ($commission as $rule)
        {   
            if($amount < $rule['min'] || $amount > $rule['max']) continue;

            $rate = $rule['rate'];
            break;
        }

        return $rate;
    }
    
    /**
     * Gets the actually collected.
     *
     * @param      integer         $term_id     The term identifier
     * @param      integer         $user_id     The user identifier
     * @param      integer|string  $start_time  The start time
     * @param      string          $end_time    The end time
     *
     * @return     integer         The actually collected.
     */
    public function get_actually_collected($term_id = 0, $user_id = 0, $start_time = '', $end_time = '')
    {
        $receipts = parent::get_receipts_paid($term_id,$user_id,$start_time,$end_time);
        $amount = array_sum(array_column($receipts, 'amount_not_vat'));

        $service_fee_payment_type = get_term_meta_value($term_id, 'service_fee_payment_type');

        if($service_fee_payment_type == 'devide')
        {
            $rate = $this->get_rate($amount);

            $service_fee_rule = $this->config->item('service_fee_rule');
            $rate = 0;
            foreach ($service_fee_rule as $level => $rule)
            {
                if($amount < $rule['min'] || $amount > $rule['max']) continue;

                if($rule['amount'] !== FALSE)
                {
                    $rate = $rule['amount'];
                    break;
                }

                if($rule['rate'] !== FALSE) 
                {
                    $rate = $rule['rate'];
                    break;
                }
            }

            $actually_collected = 0;

            if(is_int($rate)) $actually_collected = $rate;
            else if(is_float($rate)) $actually_collected = $amount * $rate;

            return $actually_collected;
        }

        // if($service_fee_payment_type == 'full')
        $contract_begin = get_term_meta_value($term_id, 'contract_begin');
        $contract_time  = $this->mdate->startOfDay($contract_begin);
        $tartget_time = $this->mdate->endOfDay(($start_time - 1));

        $past_amount_paid = 0;

        $past_receipts = parent::get_receipts_paid($term_id, $user_id, $contract_time, $tartget_time);
        if( ! empty($past_receipts))
        {
            $past_amount_paid = array_sum(array_column($past_receipts, 'amount_not_vat'));
        }

        $service_fee        = get_term_meta_value($term_id, 'service_fee');
        $service_fee_remain = $service_fee - $past_amount_paid;
        if($service_fee_remain < 0) return 0;

        $service_fee_remain = abs($service_fee_remain);

        if($service_fee_remain >= $amount) return $amount;

        if($service_fee_remain <= $amount) return $service_fee_remain;
    }
}
/* End of file Commisision_googleads_m.php */
/* Location: ./application/modules/googleads/models/Commisision_googleads_m.php */