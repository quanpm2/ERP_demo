<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td align="center">
      <p style="font-family:Roboto;color:#ffffff;font-size:30px; font-weight:300; margin:5px 0;  text-transform:uppercase;"><?php echo $title;?></p>
    </td>
  </tr>
  <tr>
    <td align="center">
      <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng: 
        <strong>
          <?php echo get_term_meta_value($term->term_id,'contract_code');?>
        </strong>
      </p>
      <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Thời gian thực hiện từ: <span style="font-weight:bold"><?php echo $contract_date;?></span> .</p> &nbsp;
    </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">Kính chào Quý khách</p>
      <p>Adsplus.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>
      <p>Adsplus.vn gửi email thông báo chiến dịch của quý khách DỰ KIẾN KẾT THÚC hợp đồng ADSPLUS cho <?php echo anchor(prep_url($term->term_name),$term->term_name);?> bao gồm các thông tin chi tiết sau</p>
    </td>
  </tr>

  <tr>
    <td align="center" style="border:20px solid #e6e6e6;"><table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <tbody>
        <tr>
          <td bgcolor="#fff">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
              <tbody>
                <tr>
                  <td bgcolor="#fff">
                    <?php
                    $template = array(
                      'table_open' => '<table border="0" cellspacing="0" cellpadding="5" width="100%" style="font-size:13px">',
                      'row_alt_start' => '<tr bgcolor="#f4f8fb">');

                    $this->table->set_template($template);
                    $this->table->add_row(array('data'=>'PHẦN CAM KẾT','style'=>'color:#fff;font-weight:bold','width'=>'80%','colspan'=>2,'bgcolor'=>'#f58220'));
                    $this->table->add_row('Thời gian bắt đầu :',my_date($advertise_start_time,'d/m/Y'));
                    $this->table->add_row(array('data'=>'Ngân sách cam kết','style'=>'color:#0072bc; font-weight:bold;'),currency_numberformat($contract_budget));

                    echo $this->table->generate();

                    $this->table->set_template($template);
                    $this->table->add_row(array('data'=>'PHẦN ĐANG THỰC HIỆN','style'=>'color:#fff;font-weight:bold','colspan'=>2,'bgcolor'=>'#f58220'));
                    $this->table->add_row(array('data'=>'Thời gian bắt đầu :','width'=>'60%'),my_date($advertise_start_time,'d/m/Y'));
                    $this->table->add_row('Thời gian dự kiến kết thúc :',my_date($expected_end_time,'d/m/Y'));
                    $this->table->add_row(array('data'=>'Ngân sách thực tế','style'=>'color:#0072bc; font-weight:bold;'),array('data'=>currency_numberformat($actual_result),'style'=>'color:#0072bc; font-weight:bold;'));

                    echo $this->table->generate();
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>


  <tr>
    <td align="center" style="border:20px solid #e6e6e6;">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
        <tbody>
          <tr>
            <td bgcolor="#fff">
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                <tbody>
                  <tr>
                    <td bgcolor="#fff">
                      <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Thông tin khách hàng</p>
                      <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                        <tbody>
                          <tr bgcolor="#f4f8fb">
                            <td width="25%" height="25">Tên khách hàng</td>
                            <td>: <?php echo 'Anh/Chị '.get_term_meta_value($term->term_id,'representative_name');?></td>
                          </tr>
                          <tr>
                            <td height="25">Email</td>
                            <td>: <?php echo mailto(get_term_meta_value($term->term_id,'representative_email'));?></td>
                          </tr>
                          <tr bgcolor="#f4f8fb">
                            <td  height="25">Điện thoại di động</td>
                            <td>: <?php echo get_term_meta_value($term->term_id,'representative_phone');?></td>
                          </tr>
                          <tr>
                            <td height="25">Địa chỉ</td>
                            <td>: <?php echo get_term_meta_value($term->term_id,'representative_address');?></td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>    
              <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">&nbsp;</p>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>

<tr>
  <td align="center" style="border:20px solid #e6e6e6;">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <tbody>
        <tr>
          <td bgcolor="#fff">
          <b><u>Quý khách lưu ý:</u></b> Đây là thông báo tiến độ chạy quảng cáo, có thể ngày dự kiến kết thúc sớm hơn hoặc chậm hơn so với cam kết do xu hướng tìm kiếm người dùng tăng hoặc giảm:
          <ul style="line-height: 1.2 em">
            <li style="margin-bottom: 10px"><b>Đối với trường hợp chạy chậm hơn so với tiến độ</b>, chúng tôi sẽ cố gắng kéo dài thời gian chạy so với cam kết để đạt được tiến độ cam kết..
            </li>
            <li style="margin-bottom: 10px"><b>Đối với trường hợp chạy nhanh hơn tiến độ</b>, chúng tôi có thể chạy nhanh hơn tiến độ cam kết  nhưng không vượt quá 30% thời gian cam kết để tránh bỏ lỡ cơ hội bán hàng khi xu hướng tìm kiếm tăng. Trong trường hợp Quý khách vẫn muốn chạy đúng tiến độ, vui lòng phản hồi để chúng tôi điều chỉnh để chạy đúng tiến độ như thời gian cam kết, điều này có thể làm giảm cơ hội bán hàng của Quý khách.
            </li>
          </ul>
          <b><u>Thanh Toán:</u></b>
          <ul style="line-height: 1.2 em">
            <li style="margin-bottom: 10px">
            <b>Đối với Quý khách hàng thanh toán theo từng đợt</b> xin vui lòng thanh toán trước ngày <b><?php echo my_date($expected_end_time,'d/m/Y');?></b>  để Quảng cáo không bị gián đoạn.
            </li>
            <li style="margin-bottom: 10px">
            <b>Đối với Quý khách hàng sắp hết hợp đồng</b> vui lòng liên hệ Nhân viên kinh doanh trực tiếp chăm sóc Quý khách để tiếp tục ký hợp đồng mới để Quảng cáo không bị gián đoạn.
            </li>
          </ul>

Cảm ơn Quý khách đã tin tưởng và đồng hành sử dụng dịch vụ của Adsplus.vn trong thời gian qua.
Xin chân thành cảm ơn !       
          </td>
        </tr>
      </tbody>
    </table>
  </td>
</tr>

</table>