<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td align="center">
      <p style="font-family:Roboto;color:#ffffff;font-size:30px; font-weight:300; margin:5px 0;  text-transform:uppercase;"><?php echo @$title;?></p>
    </td>
  </tr>
  <tr>
    <td align="center">
      <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng: 
        <strong><?php echo $contract_code;?></strong>
      </p>
      <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Thời gian thực hiện từ: <span style="font-weight:bold"><?php echo $contract_date;?></span> .</p> &nbsp;
    </td>
  </tr>
  
  <tr>
    <td align="center" style="border:20px solid #e6e6e6;"><table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <tbody>
        <tr>
          <td bgcolor="#fff">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
              <tbody>
                <tr>
                  <td bgcolor="#fff">
                  <?php
                  $template = array(
                    'table_open' => '<table border="0" cellspacing="0" cellpadding="5" width="100%" style="font-size:13px">',
                    'row_alt_start' => '<tr bgcolor="#f4f8fb">');

                  $this->table->set_template($template);
                  $this->table->add_row(array('data'=>'PHẦN CAM KẾT','style'=>'color:#fff;font-weight:bold','width'=>'80%','colspan'=>2,'bgcolor'=>'#f58220'));
                  $this->table->add_row('Thời gian bắt đầu :',my_date($start_service_time,'d/m/Y'));
                  $this->table->add_row(array('data'=>'Ngân sách cam kết','style'=>'color:#0072bc; font-weight:bold;'),array('data'=>currency_numberformat($contract_budget),'style'=>'color:#0072bc; font-weight:bold;'));

                  echo $this->table->generate();

                  $this->table->set_template($template);
                  $this->table->add_row(array('data'=>'PHẦN THỰC TẾ','style'=>'color:#fff;font-weight:bold','colspan'=>2,'bgcolor'=>'#f58220'));
                  $this->table->add_row(array('data'=>'Thời gian bắt đầu :','width'=>'60%'),my_date($start_service_time,'d/m/Y'));
                  $this->table->add_row(
                    array('data'=>'Ngân sách thực tế','style'=>'color:#0072bc; font-weight:bold;'),
                    array('data'=>currency_numberformat($actual_result),'style'=>'color:#0072bc; font-weight:bold;'));
                  
                  echo $this->table->generate();
                  ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>


  <tr>
    <td align="center" style="border:20px solid #e6e6e6;">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
        <tbody>
          <tr>
            <td bgcolor="#fff">
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                <tbody>
                  <tr>
                    <td bgcolor="#fff">
                      <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Thông tin khách hàng</p>
                      <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                        <tbody>
                          <tr bgcolor="#f4f8fb">
                            <td width="25%" height="25">Tên khách hàng</td>
                            <td>: <?php echo 'Anh/Chị '.get_term_meta_value($term->term_id,'representative_name');?></td>
                          </tr>
                          <tr>
                            <td height="25">Email</td>
                            <td>: <?php echo mailto(get_term_meta_value($term->term_id,'representative_email'));?></td>
                          </tr>
                          <tr bgcolor="#f4f8fb">
                            <td  height="25">Điện thoại di động</td>
                            <td>: <?php echo get_term_meta_value($term->term_id,'representative_phone');?></td>
                          </tr>
                          <tr>
                            <td height="25">Địa chỉ</td>
                            <td>: <?php echo get_term_meta_value($term->term_id,'representative_address');?></td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>    
              <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">&nbsp;</p>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
</table>