<table width="100%" border="0" cellspacing="0" cellpadding="0" >
    <tr>
        <td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
            <p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">Kính chào Quý khách</p>
            <p>Adsplus.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>
            <p>Adsplus.vn gửi email thông báo kích hoạt & thực hiện hợp đồng ADSPLUS cho <?php echo anchor(prep_url($term->term_name),$term->term_name);?></p>
        </td>
    </tr>

      <tr>
    <td align="center" style="border:20px solid #e6e6e6;"><table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <tbody>
        <tr>
          <td bgcolor="#fff">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
              <tbody>
                <tr>
                  <td bgcolor="#fff">
                    <?php
                    $template = array(
                      'table_open' => '<table border="0" cellspacing="0" cellpadding="5" width="100%" style="font-size:13px">',
                      'row_alt_start' => '<tr bgcolor="#f4f8fb">');

                    $this->table->set_template($template);
                    $this->table->add_row(array('data'=>'PHẦN CAM KẾT','style'=>'color:#fff;font-weight:bold','width'=>'80%','colspan'=>2,'bgcolor'=>'#f58220'));

                    $this->table->add_row('Thời gian bắt đầu :',my_date($googleads_begin_time,'d/m/Y'));
                    $this->table->add_row(['data'=>'Ngân sách cam kết','style'=>'color:#0072bc; font-weight:bold;'],['data'=>currency_numberformat($contract_budget),'style'=>'color:#0072bc; font-weight:bold;']);

                    echo $this->table->generate();

                    $this->table->set_template($template);
                    $this->table->add_row(array('data'=>'PHẦN THỰC TẾ','style'=>'color:#fff;font-weight:bold','colspan'=>2,'bgcolor'=>'#f58220'));

                    $this->table->add_row(array('data'=>'Thời gian bắt đầu :','width'=>'60%'),my_date($googleads_begin_time,'d/m/Y'));
                    $this->table->add_row(array('data'=>'Dự kiến kết thúc (theo ngân sách hợp đồng) :','width'=>'60%'),my_date($expected_end_time,'d/m/Y'));

                    $this->table->add_row(array('data'=>'Dự kiến kết thúc (theo ngân sách thực thu) :','width'=>'60%'),my_date($payment_expected_end_time,'d/m/Y'));

                    $this->table->add_row(
                        array('data'=>'Ngân sách đã thu : ','style'=>'color:#0072bc; font-weight:bold;'),
                        array('data'=>currency_numberformat($actual_budget), 'style'=>'color:#0072bc; font-weight:bold;'));

                    $this->table->add_row(
                        array('data'=>'Ngân sách đã sử dụng : ','style'=>'color:#0072bc; font-weight:bold;'),
                        array('data'=>currency_numberformat($actual_result), 'style'=>'color:#0072bc; font-weight:bold;'));
                    
                    $this->table->add_row('% hoàn thành (theo NS thực thu) : ', currency_numberformat($actual_progress_percent_net*100, ' %', 2));
                    $this->table->add_row('Tiến độ (theo NS hợp đồng) : ', currency_numberformat($payment_real_progress, ' %', 2));

                    $this->table->add_row('% hoàn thành (theo NS hợp đồng) : ', currency_numberformat($actual_progress_percent, ' %', 2));
                    $this->table->add_row('Tiến độ (theo NS thực thu) : ', currency_numberformat($real_progress, ' %', 2));

                    echo $this->table->generate();
                    
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>

    <tr>
        <td align="center" style="border:20px solid #e6e6e6;">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                <tbody>
                    <tr>
                        <td bgcolor="#fff">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                                <tbody>
                                    <tr>
                                        <td bgcolor="#fff">
                                            <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Thông tin khách hàng</p>
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                                                <tbody>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td width="25%" height="25">Tên khách hàng</td>
                                                        <td>: <?php echo 'Anh/Chị '.get_term_meta_value($term->term_id,'representative_name');?></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25">Email</td>
                                                        <td>: <?php echo mailto(get_term_meta_value($term->term_id,'representative_email'));?></td>
                                                    </tr>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td  height="25">Điện thoại di động</td>
                                                        <td>: <?php echo get_term_meta_value($term->term_id,'representative_phone');?></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25">Địa chỉ</td>
                                                        <td>: <?php echo get_term_meta_value($term->term_id,'representative_address');?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">
                                                Thông tin báo cáo công việc website 
                                                <?php echo anchor(prep_url($term->term_name),$term->term_name);?>
                                            </p>
                                            <?php
                                            $curators = get_term_meta_value($term->term_id, 'contract_curators');
                                            $curators = @unserialize($curators);
                                            $names = $emails = $phones =array();
                                            if(!empty($curators)){
                                                foreach ($curators as $item) {
                                                    if(!empty($item['name'])) $names[] = $item['name'];
                                                    if(!empty($item['phone'])) $phones[] = $item['phone'];
                                                    if(!empty($item['email'])) $emails[] = mailto($item['email']);
                                                }
                                            }
                                            ?>
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                                                <tbody>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td width="25%"  height="25">Nhận báo cáo</td>
                                                        <td>: <?php echo empty($names) ? '' : implode(', ', $names);?></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25">SMS </td>
                                                        <td>: <span style="font-weight:bold"><?php echo empty($phones) ? '' : implode(', ', $phones);?></span></td>
                                                    </tr>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td  height="25">Email</td>
                                                        <td>: <?php echo empty($emails) ? '' : implode(', ', $emails);?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>          
                            <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>