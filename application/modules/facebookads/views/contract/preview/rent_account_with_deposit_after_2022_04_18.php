<?php $this->load->view('facebookads/contract/preview/header_after_2021_jan');?>

<p><b>Xét theo</b>, nhu cầu của Bên A và khả năng cung cấp dịch vụ của Bên B. Hai bên thống nhất ký kết Hợp đồng Dịch vụ Quảng cáo với các điều khoản cụ thể như sau:</p>

<div contenteditable="true" id="9c0212ab-7a92-47de-a34d-bei9084c6c42">
    <strong>Điều 1: Nội dung công việc</strong><br />

    <p><b>1. Hình thức</b></p>
    <ol type="a" style="padding-left: 15px">

        <li>Các Bên đồng ý rằng, Bên B sẽ cung cấp các tài khoản quảng cáo Facebook cùng với toàn bộ các thông tin truy cập của tài khoản Facebook, cũng như các quyền của người sử dụng đối với các tài khoản đó ("<b>Tài Khoản Quảng Cáo</b>") cho Bên A để Bên A tiến hành hoạt động quảng cáo phù hợp với yêu cầu cụ thể của Bên A tại từng thời điểm cụ thể.</li>
        
        <li>Cho mục đích của hợp đồng này, Bên A thừa nhận và đồng ý rằng tài khoản quảng cáo là tài sản thuộc sở hữu hợp pháp của Bên B. Bên A có quyền sử dụng và vận hành tài khoản quảng cáo phù hợp với các điều kiện và điều khoản được quy định tại hợp đồng này.</li>
        
        <li>Bên A và nhân viên của Bên A có toàn quyền truy cập, quản lý và vận hành tài khoản quảng cáo trong thời gian thực hiện các hoạt động quảng cáo.</li>
        
        <li>Bên B sẽ hỗ trợ Bên A quản lý và sử dụng, khai thác tài khoản quảng cáo và hỗ trợ các vấn đề, bao gồm nhưng không giới hạn kỹ thuật, chuyên môn, bảo mật liên quan đến quy định và chính sách của Facebook liên quan tới tài khoản quảng cáo cho Bên A trong suốt thời hạn của hợp đồng này.</li>
        
        <li>Trong thời hạn hợp đồng, Bên A không sử dụng (các) tài khoản/email được Bên B cấp quyền điều chỉnh chiến dịch quảng cáo Facebook để thực hiện các hoạt động quảng cáo bên ngoài tài khoản quảng cáo được cung cấp bởi Bên B.</li>
        
        <li>Bên B sẽ tư vấn, hỗ trợ cho Bên A các vấn đề, chính sách của Facebook liên quan đến việc vận hành và sử dụng tài khoản quảng cáo. Trên cơ sở tư vấn của Bên B, Bên A sẽ thực hiện điều chỉnh việc vận hành và sử dụng tài khoản quảng cáo, trừ trường hợp các bên có thỏa thuận khác. </li>
        
        <li>Trong trường hợp Bên A không thể thực hiện hoạt động quảng cáo do vi phạm chính sách về quảng cáo của Facebook, Bên B có nghĩa vụ tư vấn, hỗ trợ về mặt kỹ thuật để đảm bảo hoạt động quảng cáo có thể thực hiện. Trong trường hợp Bên A không thực hiện hoặc tuân thủ các chỉ dẫn của Bên B dẫn tới thiệt hại phát sinh Bên A chịu trách nhiệm đối với các thiệt hại này, trừ trường hợp Các Bên đạt được thỏa thuận khác.</li>
        
        <li>Bên A sẽ thực hiện triển khai hoạt động quảng cáo tại địa chỉ Fanpage/Website của Bên A: <?php echo $websites_text;?> </li>
    </ol>
    <p><b>2. Nội dung quảng cáo</b></p>
    Do Bên A chịu trách nhiệm về nội dung quảng cáo.

    <p><b>3. Chi tiết quảng cáo</b></p>
    <ol type="a" style="padding-left: 15px">

        <li>Quảng cáo của Bên A sẽ xuất hiện tại các vị trí cho phép đặt quảng cáo của Facebook. Quảng cáo sẽ xuất hiện tại www.facebook.com, cho phép toàn bộ người dùng trên hệ thống có thể nhìn thấy quảng cáo theo đối tượng được nhắm tới .</li>

        <li>Thời gian Bên B bàn giao tài khoản quảng cáo cho Bên A sẽ được 2 bên xác nhận qua văn bản hoặc email. Kể từ ngày Bên A nhận tài khoản quảng cáo, Bên A sẽ chịu trách nhiệm hoàn toàn cho mọi chỉnh sửa, cài đặt do Bên A thực hiện trên tài khoản quảng cáo được Bên B cung cấp. Bên B có nghĩa vụ bàn giao toàn bộ thông tin, bao gồm nhưng không giới hạn tên truy cập, mật khẩu, các thông tin liên quan đến bảo mật phù hợp với quy định và chính sách của Facebook của tài khoản quảng cáo cho Bên A không chậm hơn một (01) ngày kể từ ngày nhận được yêu cầu của Bên A.</li>
    </ol>
</div>

<div contenteditable="true" id="570bacf8-eca2-4bd6-81ad-392aaec98001">
    <strong>Điều 2: Thời hạn hợp đồng</strong><br />
    <p>
    Hợp đồng này có hiệu lực <?php echo $contract_days;?> ngày, kể từ ngày <?php echo my_date($contract_begin, 'd');?> tháng <?php echo my_date($contract_begin, 'm');?> năm <?php echo my_date($contract_begin, 'Y');?> đến ngày <?php echo my_date($contract_end, 'd');?> tháng <?php echo my_date($contract_end, 'm');?> năm <?php echo my_date($contract_end, 'Y');?> hoặc đến khi hết ngân sách quảng cáo.    
    </p>
</div>

<div contenteditable="true" id="f188137c-937e-4563-a551-fe5b1dd6018f">
    <strong>Điều 3. Phương thức và phương tiện quảng cáo</strong><br />
    
    <p>1. <b>Phương thức: </b>Quảng cáo được thực hiện bằng hình thức:  Văn bản/ hình ảnh/ biểu tượng/ âm thanh.</p>
    <p>2. <b>Phương tiện: </b>Đăng trên fanpage của Facebook.</p>
    <p>Việc sử dụng phương tiện quảng cáo thương mại phải đảm bảo:</p>
    <ol type="a" style="padding-left: 15px">
        <li>Tuân thủ các quy định của pháp luật về báo chí, xuất bản, thông tin, chương trình hoạt động văn hoá, thể thao, hội chợ, triển lãm.</li>
        <li>Tuân thủ quy định về địa điểm quảng cáo, không gây ảnh hưởng xấu đến an ninh xã hội.</li>
        <li>Đúng mức độ, thời lượng, thời điểm quy định đối với từng loại phương tiện thông tin đại chúng.</li>
    </ol>
</div>

<?php

$total = $data_service['budget'];
$fct_amount = 0;

if($fct)
{
    $fct_amount = div($data_service['budget'], 0.95)*div($fct, 100);
    $total+=$fct_amount;
}

if ($data_service['service_fee'])
{
    $total+=$data_service['service_fee'];
}

$services_pricetag  = get_term_meta_value($term->term_id, 'services_pricetag');
$services_pricetag  = is_serialized($services_pricetag) ? unserialize($services_pricetag) : [];
$discount_amount    = 0;
$discount_rate      = 0;

if( ! empty($services_pricetag['discount']['is_discount']))
{
    $discount_amount = ($total * ((float) @$services_pricetag['discount']['percent'] / 100));
    if($discount_amount != 0)
    {
        $total-=$discount_amount;
        $discount_rate = (float) @$services_pricetag['discount']['percent'];
    }
}

$service_fee_with_discount = $data_service['service_fee'];
if( ! empty($data_service['discount_amount']))
{
    $service_fee_with_discount-= $data_service['discount_amount'];
    $total-= $data_service['discount_amount'];
}

$total_regular = $total;

$tax = 0;
if(!empty($vat))
{
    $tax    = $total*($vat/100);
    $total  = cal_vat($total,$vat);
}

$total_words = currency_number_to_words(numberformat($total,0,'.',''));

?>

<div contenteditable="true" id="f89857c4-3c81-4d2a-9f1d-fda82a588d7d">
    <strong>Điều 4. Phí dịch vụ, thời hạn và phương thức thanh toán</strong><br />

    <?php if('range' == get_term_meta_value($term->term_id, 'service_fee_payment_type')): ?>
    <p><b>1. Mức phí dịch vụ</b></p>

    <p>1.1. Giá trị thực hiện cụ thể căn cứ theo email yêu cầu và có xác nhận của hai bên, bao gồm:</p>
    <p>- Ngân sách quảng cáo (thực tế thanh toán cho Facebook).</p>
    <p>- Phí quản lý trên ngân sách quảng cáo sử dụng theo bảng sau:</p>

    <?php 
        $service_fee_plan = get_term_meta_value($term->term_id, 'service_fee_plan');
        $service_fee_plan = is_serialized($service_fee_plan) ? unserialize($service_fee_plan) : [];
        $service_fee_plan = array_values($service_fee_plan);
    ?>
    <table border="1" cellspacing="0" cellpadding="3" width="100%">
        <tr>
            <th align="center">Ngân sách (chưa bao gồm VAT) (VNĐ)</th>
            <th align="center">% phí quản lý (tính trên ngân sách thực chạy tháng)</th>
        </tr>
        <?php foreach ($service_fee_plan as $index => $plan): ?>
            <tr>
                <td align="center"> Từ  <?php echo currency_numberformat((int)$plan['from'], '') ?> <?php echo empty((int)$plan['to']) ? 'trở lên' : '- ' . currency_numberformat((int)$plan['to'], '') ?> </td>
                <td align="center"><?php echo currency_numberformat((int)$plan['service_fee_rate'], '%') ?></td>
            </tr>
        <!-- <tr>
            <td colspan="3" style="text-align:left"><?php echo $promotion['text'] ;?></td>
            <td style="text-align:right">-<?php echo currency_numberformat($promotion['amount'], '');?></td>
        </tr>  -->
        <?php endforeach;?>
    </table>


    <p>1.2. Thuế nhà thầu nước ngoài (FCT) được tính trên ngân sách quảng cáo thực chạy.</p>
    <p><i>Thuế nhà thầu nước ngoài = Doanh thu tính thuế Thu nhập doanh nghiệp (Ngân sách quảng cáo/95%)*Thuế suất thuế nhà thầu(5%))</i></p>

    <p>1.3. Thuế GTGT (VAT) được tính trên tổng số tiền gồm ngân sách thực chạy, phí quản lý và thuế nhà thầu nước ngoài.</p>

    <?php else: ?>

    <p><b>1. Mức phí dịch vụ</b></p>

    <ol type="a" style="padding-left: 15px">

        <li>
            <p>
                Thù lao của Hợp đồng này được Bên A thanh toán cho Bên B là <b><?php echo currency_numberformat($total, ' đồng');?></b> <i>(Bằng chữ: <?php echo $total_words;?>)</i> đã bao gồm <?php echo currency_numberformat($vat, ' %');?> thuế VAT. Thanh toán bằng tiền đồng Việt Nam. Chi tiết phí dịch vụ như sau:
            </p>

            <table border="1" cellspacing="0" cellpadding="3" width="100%">
                <tr>
                    <th>Dịch vụ</th>
                    <th>Ngân sách</th>
                    <th>Thời gian</th>
                    <th>Thành tiền(vnđ)</th>
                </tr>

                <tr>
                    <td><?php echo $data_service['service_name'];?></td>
                    <td style="text-align:center">
                        <?php echo currency_numberformat($data_service['budget'],'');?>
                            
                    </td>
                    <td style="text-align:center">
                        <?php echo $contract_days;?> ngày<br />
                        <?php printf('(từ %s đến %s)', my_date($contract_begin,'d/m/Y'), my_date($contract_end,'d/m/Y'));?>
                    </td>
                    <td style="text-align:right">
                        <?php echo currency_numberformat($data_service['budget'],'');?>    
                    </td>
                </tr>

                <?php if ($fct && !empty($fct_amount)): ?>
                <tr>
                    <td colspan="3" style="text-align:left">Thuế nhà thầu</td>
                    <td style="text-align:right"><?php echo currency_numberformat($fct_amount, '');?></td>
                </tr>
                <?php endif ?>

                <?php if($is_display_promotions_discount) :?>
                    <!-- NORNAL WAY -->
                    <?php if ($data_service['service_fee']): ?>
                    <tr>
                        <td colspan="3" style="text-align:left">Phí dịch vụ</td>
                        <td style="text-align:right"><?php echo currency_numberformat($data_service['service_fee'],'');?></td>
                    </tr>
                    <?php endif ?>
                    <?php if ( ! empty($services_pricetag['discount']['is_discount']) && $discount_amount != 0) : ?>
                    <tr>
                        <td  colspan="3" style="text-align:left">
                            <strong>Giảm giá <?php echo $discount_rate;?> %</strong>
                        </td>
                        <td style="text-align:right">
                            <?php echo currency_numberformat($discount_amount);?>    
                        </td>
                    </tr>
                    <?php endif; ?>

                    <?php if( ! empty($data_service['discount_amount'])) : ?>
                    <?php foreach ($data_service['promotions'] as $promotion): ?>
                    <tr>
                        <td colspan="3" style="text-align:left"><?php echo $promotion['text'] ;?></td>
                        <td style="text-align:right">-<?php echo currency_numberformat($promotion['amount'], '');?></td>
                    </tr> 
                    <?php endforeach;?>
                    <?php endif;?>

                <?php else : ?>
                    <!-- NORNAL WAY -->
                    <?php if ($service_fee_with_discount): ?>
                    <tr>
                        <td colspan="3" style="text-align:left">Phí dịch vụ</td>
                        <td style="text-align:right"><?php echo currency_numberformat($service_fee_with_discount,'');?></td>
                    </tr>
                    <?php endif ?>
                <?php endif; ?>

                <tr>
                    <td colspan="3" style="text-align:left"><strong>Tổng cộng</strong></td>
                    <td style="text-align:right"><?php echo currency_numberformat($total_regular,'');?></td>
                </tr>

                <?php if($vat) :?>
                <tr>
                    <td colspan="3" style="text-align:left">
                        VAT (<?php echo currency_numberformat($vat, '%') ;?>)
                    </td>
                    <td style="text-align:right"><?php echo currency_numberformat($tax,''); ?></td>
                </tr>
                <tr>
                    <td  colspan="3" style="text-align:left"><strong>Số tiền phải thanh toán</strong></td>
                    <td  style="text-align:right"><?php echo currency_numberformat($total,'');?></td>
                </tr>
                <?php endif;?>
            </table>

            <?php if (!empty($fct)): ?>    
            <p>
                <em>
                    <b><u>Ghi chú : </u></b>
                    Thuế nhà thầu nước ngoài = Doanh thu tính thuế Thu nhập doanh nghiệp (Ngân sách quảng cáo/95%)*Thuế suất thuế nhà thầu(5%))
                </em>
            </p>
            <?php endif ?>
        </li>
        <li>Bên A đặt cọc trước cho Bên B số tiền <?php echo currency_numberformat($deposit_amount, ' vnđ') ;?> (<?php echo currency_number_to_words(numberformat($deposit_amount,0,'.',''));?>) sau khi ký và trước khi thực hiện hợp đồng.</li>
        <li>Các chi phí liên quan đến thanh toán ngân sách quảng cáo do Bên A thực hiện trực tiếp với Facebook.</li>
        <li>Bên A có trách nhiệm thanh toán và đảm bảo việc thanh toán không bị gián đoạn, không làm ảnh hưởng đến chiến dịch quảng cáo.</li>
    </ol>

    <?php endif; ?>

    <p><b>2. Phương thức thanh toán</b></p>

    <p>Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin sau:</p>
    <?php if( ! empty($bank_info)) :?>
    <ul style="padding-left:0;list-style:none">
    <?php foreach ($bank_info as $label => $text) :?>
        <?php if (is_array($text)) : ?>
            <?php foreach ($text as $key => $value) :?>
                <li>- <?php echo $key;?>: <?php echo $value;?></li>
            <?php endforeach;?>
        <?php continue; endif;?>
        <li>- <?php echo $label;?>: <?php echo $text;?></li>
    <?php endforeach;?>
    </ul>
    <p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
    <?php endif; ?>

    <?php if ($vat > 0): ?>
    <p>Bên B xuất hóa đơn giá trị gia tăng cho Bên A theo thông tin sau:</p>
    <ul style="padding-left:0; list-style:none">
        <li>- Tên công ty: <?php echo $customer->display_name ?? '';?></li>
        <li>- Địa chỉ: <?php echo $data_customer['Địa chỉ'] ?? '';?></li>
        <li>- Mã số thuế: <?php echo $data_customer['Mã số thuế'] ?? '';?></li>
    </ul>

    <p>Hóa đơn điện tử được Bên B gửi cho Bên A theo  địa chỉ email: <?php echo $customer->representative['email'] ?? '';?></p>
    <?php endif ?>

    <p>
        <b>3. Thời hạn thanh toán: </b>
        <?php echo $payments;?>
    </p>

</div>

<div contenteditable="true" id="1163a317-a206-4727-8580-ce53de0b00cf">
    <strong>Điều 5. Quyền và nghĩa vụ của Bên A</strong>
    <br />

    <p><b>1. Quyền của Bên A</b></p>
    <ol type="a" style="padding-left: 15px">

        <li> Thiết lập, cài đặt, thay đổi các thông tin và cài đặt, cấu hình hoặc các thông số kỹ thuật khác của tài khoản quảng cáo mà không làm ảnh hưởng tới quyền sở hữu của Bên A đối với các tài khoản đó.</li>
        <li> Cài đặt, điều chỉnh các hoạt động quảng cáo trên tài khoản quảng cáo và chịu trách nhiệm hoàn toàn cho mọi chỉnh sửa, cài đặt do Bên A thực hiện trên tài khoản quảng cáo được Bên B cung cấp</li>
        <li> Yêu cầu Bên B hỗ trợ về mặt kỹ thuật, chuyên môn liên quan đến chất lượng thông tin, dịch vụ do Bên A cung cấp.</li>
        <li> Kiểm tra, giám sát việc thực hiện hợp đồng dịch vụ quảng cáo thương mại.</li>
    </ol>

    <p><b>2. Nghĩa vụ của Bên A</b></p>
    <ol type="a" style="padding-left: 15px">
        <li>Bên A có nghĩa vụ cung cấp thông tin trung thực, chính xác về hoạt động sản xuất, hàng hóa dịch vụ thương mại của đơn vị mình và chịu trách nhiệm về các thông tin do mình cung cấp cho Bên B.</li>
        <li>Chỉ thực hiện các hoạt động quảng cáo tại các địa chỉ website/fanpage như được nêu tại Điều 1 Hợp Đồng này hoặc tại một địa chỉ website/fanpage khác trong trường hợp được sự chấp thuận trước bằng văn bản của Bên B.</li>
        <li>Khi có sự tranh chấp của bên thứ ba về những nội dung thông tin kinh tế, nhãn hiệu hàng hóa, bản quyền… đối với Bên A thì Bên A phải tự mình giải quyết, trong trường hợp đó Bên B có quyền đơn phương đình chỉ hợp đồng và yêu cầu Bên A chịu trách nhiệm bồi thường chi phí cho Bên B (nếu có).</li>
        <li>Trả phí dịch vụ quảng cáo theo thỏa thuận nêu tại Điều 4 của hợp đồng.</li>
    </ol>
</div>

<div contenteditable="true" id="5151e149-6a80-4f9c-a078-c7790f7290e1">
    <strong>Điều 6. Quyền và nghĩa vụ của Bên B</strong>
    <br />

    <p><b>1. Quyền của Bên B</b></p>
    <ol type="a" style="padding-left: 15px">

        <li>Yêu cầu Bên A cung cấp thông tin quảng cáo trung thực, chính xác theo đúng thời hạn của hợp đồng.</li>
        <li>Yêu cầu Bên A phối hợp thực hiện chỉnh sửa các hoạt động quảng cáo hoặc các vấn đề liên quan khác theo đúng chính sách của Facebook trừ trường hợp việc điều chỉnh này ảnh hưởng tới mục tiêu kinh doanh hoặc kế hoạch kinh doanh của Bên A.</li>
        <li>Dừng hoạt động quảng cáo hoặc đóng tài khoản quảng cáo của Bên A trong trường hợp Bên B đã có văn bản yêu cầu Bên A ngừng các hoạt động, hành vi (i) không tuân thủ chính sách và quy định sử dụng của Facebook và pháp luật Việt Nam về việc sử dụng dịch vụ Internet, sử dụng website đã đăng ký, (ii) sử dụng tài khoản quảng cáo cho các mục đích gây rối trật tự xã hội, phá hoại an ninh quốc gia, làm tổn hại thuần phong mỹ tục, đạo đức hay kinh doanh bất hợp pháp, mà trong vòng hai (02) ngày kể từ ngày nhận được thông báo, Bên A không dừng các hoạt động trên. Trong trường hợp này, Bên B được loại trừ khỏi mọi nghĩa vụ pháp lý liên quan tới các hành vi của Bên A và  Bên B có quyền từ chối dịch vụ hoặc đơn phương chấm dứt hợp đồng và/hoặc phụ lục hợp đồng đối với lần sử dụng mà trong thời hạn đó Bên A thực hiện các hành vi vi phạm nêu trên.</li>
        <li>Nhận phí quảng cáo theo thỏa thuận trong hợp đồng.</li>
    </ol>

    <p><b>2. Nghĩa vụ của Bên B</b></p>
    <ol type="a" style="padding-left: 15px">
        <li>Thực hiện dịch vụ quảng cáo thương mại theo đúng thỏa thuận trong hợp đồng.</li>
        <li>Thực hiện sự lựa chọn của Bên A về người phát hành quảng cáo thương mại, hình thức, nội dung, phương tiện, phạm vi và thời gian quảng cáo thương mại.</li>
        <li>Thông báo cho Bên A về các thông báo, biện pháp, hành vi từ Facebook ảnh hưởng bất lợi đến hoạt động quảng cáo của Bên A và hỗ trợ Bên A nhằm hạn chế, khắc phục các ảnh hưởng trên.</li>
        <li>Không cho phép nhân viên của Bên B được tiếp cận, truy cập, sử dụng tài khoản quảng cáo mà chưa được sự chấp thuận của Bên A.</li>
        <li>Đảm bảo tài khoản quảng cáo được cung cấp cho Bên A luôn được duy trì và các thông tin tài khoản quảng cáo không được cấp cho một bên thứ ba bất kỳ dưới mọi hình thức nào trong suốt thời gian thực hiện các hoạt động quảng cáo và thời hạn của hợp đồng này, trừ trường hợp Các Bên đạt được thỏa thuận khác.</li>
    </ol>
</div>

<div contenteditable="true" id="97ffcab6-2a92-4f65-803a-bb55b2b90354">
    <strong>Điều 7. Điều khoản bảo mật thông tin</strong>
    <br />
    <p>1. Mỗi bên có nghĩa vụ bảo vệ thông tin mật của bên kia và cam kết chỉ sử dụng các thông tin đó cho mục đích thực hiện hợp đồng này.</p>    
    <p>2. Nghĩa vụ bảo mật thông tin không được xem là vi phạm đối với các trường hợp sau: </p>
    <ol type="a" style="padding-left: 15px">
        <li>Theo quy định pháp luật hoặc quyết định, bản án của cơ quan nhà nước có thẩm quyền.</li>
        <li>Cung cấp thông tin cho bên Tư vấn pháp luật hoặc tư vấn khác nhằm mục đích thực hiện hợp đồng này.</li>
        <li>Cung cấp thông tin cho nhân viên, nhà thầu, đối tác của mỗi bên nhằm mục đích thực hiện hợp đồng này.</li>
        <li>Trường hợp cung cấp thông tin trong các trường hợp ở trên, mỗi bên có nghĩa vụ áp dụng những biện pháp hợp lý nhằm đảm bảo các cá nhân, tổ chức tiếp nhận thông tin có nghĩa vụ bảo mật thông tin như các bên trong hợp đồng này.</li>
    </ol>
</div>

<div contenteditable="true" id="545f0ecd-3a08-491e-8762-5f1f7e006302">
    <strong>Điều 8. Tạm ngưng quảng cáo</strong>
    <br />
    <p><b>1. Tạm ngưng quảng cáo bởi Bên A</b></p>
    <ol type="a" style="padding-left: 15px">
        <li>
            Trong trường hợp Bên A đã thanh toán đủ 100% cho dịch vụ nhưng muốn tạm ngưng việc đăng quảng cáo trên website của Bên A, Bên A phải gửi yêu cầu bằng văn bản hoặc email cho Bên B đề nghị tạm ngưng một phần hoặc toàn bộ dịch vụ trước ít nhất ba (03) ngày. Trong trường hợp đó, số ngày hiện quảng cáo trên hệ thống quảng cáo của Bên A sẽ được tăng thêm đúng bằng số ngày tạm dừng quảng cáo nhưng thời gian dừng không quá 1 tháng (30 ngày).
        </li>
        <li>
            Nếu thời gian tạm ngưng vượt quá thời hạn của hợp đồng thì Bên A có quyền thay đổi giá trị dịch vụ theo chính sách giá của Bên B bằng việc gửi văn bản thông báo cho Bên B.
        </li>
    </ol>

    <p><b>2. Tạm ngưng quảng cáo bởi Bên B</b></p>
    <ol type="a" style="padding-left: 15px">
        <li>
            Bên B sẽ tạm ngưng cung cấp dịch vụ cho Bên A nếu:
            <br/>
            - Bên A vi phạm các điều khoản quy định trong hợp đồng quảng cáo, các quy định của pháp luật về quảng cáo và quy định quảng cáo của Bên B. Dịch vụ chỉ được Bên B mở lại sau khi Bên A chấm dứt hành vi vi phạm trong vòng ba (3) ngày kể từ ngày nhận được thông báo của Bên B, mọi chi phí khắc phục Bên A phải chịu trách nhiệm.
            <br/>
            - Bên A chậm thanh toán cho Bên B theo quy định tại Điều 4 của Hợp đồng này. Bên B sẽ tạm dừng chiến dịch quảng cáo tương ứng với số ngày chậm thanh toán của Bên A. Bên B có trách nhiệm đưa quảng cáo xuất hiện trở lại 01 (một) ngày sau khi nhận được đầy đủ phần thanh toán bị chậm của Bên A.
        </li>
        <li>
            Đối với những nội dung quảng cáo gây ra phản ứng không tốt đối với người dùng thì Bên B sẽ ngừng chạy quảng cáo ngay lập tức và thông báo cho Bên A để thay đổi và chỉnh sửa nội dung quảng cáo.
        </li>
    </ol>
</div>

<div contenteditable="true" id="84e08f68-41b4-4982-9862-742dbe838dbb">
    <strong>Điều 9. Chấm dứt hợp đồng</strong>
    <br />

    <ol style="padding-left: 15px">
        <li>Trong trường hợp Bên A có thay đổi chiến dịch quảng cáo và muốn chấm dứt hợp đồng trước thời hạn thì Bên A phải gửi thông báo trước bằng văn bản cho Bên B trước 07 (bảy) ngày khi chính thức dừng quảng cáo và phải được Bên B chấp nhận về thời điểm dừng hợp đồng.</li>
        <li>Bên B sẽ hoàn trả cho Bên A số tiền còn lại chưa sử dụng trong ngân sách quảng cáo trừ đi 50% giá trị gói chương trình khuyến mãi (nếu có) mà Bên A đã tạm ứng cho Bên B để thực hiện dịch vụ. Bên B sẽ không thực hiện hoàn trả cho Bên A giá trị phí dịch vụ quản lý tài khoản trong trường hợp Bên A chấm dứt hợp đồng trước hạn.</li>
        <li>
            Trong trường hợp có những sự kiện bất khả kháng không lường trước được, trách nhiệm và thời hạn thực hiện hợp đồng của cả hai bên sẽ được xem xét, đàm phán và quyết định lại. Các sự kiện bất khả kháng bao gồm nhưng không giới hạn các rủi ro sau:
            <ol type="a" style="padding-left: 0px">
                <li>Rủi ro do ngừng hoặc lỗi kĩ thuật từ dịch vụ hệ thống quảng cáo cung cấp</li>
                <li>Rủi ro về đường truyền internet, cơ sở hạ tầng mạng quốc gia</li>
                <li>Thiên tai, chiến tranh, khủng bố, hoả hoạn, dịch bệnh...</li>
            </ol>
        </li>
        <li>Chiến dịch quảng cáo sẽ hoàn tất khi Bên B gửi thông báo kết thúc dịch vụ qua email cho Bên A. Sau 2 ngày kể từ khi nhận được email từ Bên B, nếu Bên A không có phản hồi khác, hợp đồng được xem là kết thúc.</li>
    </ol>
</div>

<div contenteditable="true" id="57bc1b7b-7efe-4203-a07f-fe77c184ccd4">
    <strong>Điều 10. Tranh chấp và phân xử</strong>
    <br />
    <p>
        Hai bên cam kết thực hiện những điều khoản trong hợp đồng. Nếu có vướng mắc, mỗi bên thông báo cho nhau để cùng bàn bạc giải quyết trên tinh thần hợp tác, thiện chí, vì lợi ích cả hai bên. Trong trường hợp không thể giải quyết được bất đồng, tranh chấp sẽ được giải quyết tại Tòa án có thẩm quyền tại TP Hồ Chí Minh.
    </p>
</div>

<div contenteditable="true" id="2d78e475-42ea-4e6e-88bd-62a3a9c880bf">
    <strong>Điều 11. Hiệu lực Hợp đồng</strong>
    <br />
    <p>
        Hợp đồng có hiệu lực kể từ ngày hai bên ký và được xem như tự động thanh lý khi hai bên thực hiện đầy đủ tất cả các điều khoản nêu trong hợp đồng này.
        <br/>
        Mọi sửa đổi hoặc bổ sung phải được lập thành phụ lục bằng văn bản được hai bên ký, đóng dấu.
<br/>
        Hợp đồng này được làm thành 02 (hai) bản bằng tiếng Việt, có giá trị ngang nhau, Bên A giữ 01 bản, Bên B giữ 01 bản để thực hiện.
    </p>
</div>




<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
    <tr>
        <td width="44%" valign="top" style="text-align:center">
            <p>
                <strong>BÊN A</strong><br />
                <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                <strong><?php echo mb_strtoupper($customer->display_name);?>  </strong>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
            <td rowspan="2"></td>

            <td width="46%" valign="top" style="text-align:center">
                <p><strong>BÊN B</strong><br />
                    <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                    <strong>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</strong>
                </p>
                <p><strong>&nbsp;</strong></p>
                <p><strong>&nbsp;</strong></p>
                <p>&nbsp;</p>
            </td>
    </tr>
    <tr>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_customer['Đại diện'] ;?></strong><br/>
                <?php echo $data_customer['Chức vụ'] ;?>
            </p>
        </td>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_represent['Đại diện'] ?? ''; ?></strong><br />
                <?php echo $data_represent['Chức vụ']; ?>
            </p>
        </td>
    </tr>
</table>
