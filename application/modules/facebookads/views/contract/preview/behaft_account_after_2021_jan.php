<?php $this->load->view('googleads/contract/preview/header_after_2021_jan');?>

<p><b>Xét theo</b>, nhu cầu của Bên A và khả năng cung cấp dịch vụ của Bên B. Hai bên thống nhất ký kết Hợp đồng Dịch vụ Quảng cáo với các điều khoản cụ thể như sau:</p>

<div contenteditable="true" id="9c0212ab-7a92-47de-a34d-bei9084c6c42">
    <strong>Điều 1: Nội dung công việc</strong>
    <br />

    <p><b>1. Hình thức</b></p>
    Bên A thuê Bên B trực tiếp hoặc thông qua bên thứ ba cung cấp dịch vụ quảng cáo cho sản phẩm/ dịch vụ cho fanpage <?php echo $websites_text;?> bằng hình thức quản lý quảng cáo trực tuyến đăng trên fanpage của Facebook là: <a href="www.facebook.com">www.facebook.com</a>.

    <p><b>2. Nội dung quảng cáo</b></p>
    Do Bên A cung cấp nội dung quảng cáo và chịu trách nhiệm về nội dung quảng cáo.

    <p><b>3. Chi tiết quảng cáo</b></p>
    <ol type="a" style="padding-left: 15px">
        <li>Quảng cáo của Bên A sẽ xuất hiện tại các vị trí cho phép đặt quảng cáo của Facebook và theo Fanpage quảng cáo mà Bên A đã cung cấp. Trang web mà quảng cáo sẽ xuất hiện là social Facebook (www.facebook.com).</li>
        <li>Bên B sẽ tư vấn và phối hợp với Bên A xây dựng các chiến dịch quảng cáo, hình ảnh và thông điệp  để chiến dịch quảng cáo đạt hiệu quả cao nhất.</li>
        <li>Bên B sẽ đề xuất với Bên A những giải pháp để tăng cường hiệu quả của chiến dịch quảng cáo.</li>
        <li>Kể từ khi hợp đồng được ký kết, Bên A thanh toán tiền cho Bên B theo quy định của hợp đồng này và Bên A duyệt đề xuất triển khai của Bên B. Bên B có trách nhiệm gửi đề xuất đến Bên A trong vòng 03 ngày làm việc kể từ ngày hợp đồng được ký kết.</li>
        <li>Bên B sẽ theo dõi, giám sát hoạt động của chiến dịch quảng cáo hàng ngày và gửi báo cáo định kỳ đến Bên A hàng tuần.</li> 
        <li>Đối với các chiến dịch quảng cáo nhỏ trong thời gian còn hiệu lực của hợp đồng này sẽ được coi là hoàn tất khi Bên B gửi thông báo kết thúc dịch vụ qua email cho Bên A. Sau 2 ngày kể từ khi nhận được email từ Bên B, nếu Bên A không có phản hồi khác, chiến dịch quảng cáo được xem là kết thúc.</li>
        <li>
            Bên B chịu trách nhiệm gửi báo cáo về hoạt động quảng cáo cho Bên A.
            <?php if ( ! empty($contract_curators)):?>
                Toàn bộ báo cáo gửi cho Bên A theo thông tin sau:
                <ul style="padding-left: 0">
                <?php foreach ($contract_curators as $curator): ?>
                    <?php if (empty($curator['name']) || empty($curator['email'])): continue;?>
                        
                    <?php endif ?>
                    <li style="list-style-type: none; margin-bottom: 0"><b>Ông/bà: </b><?php echo $curator['name']??''; ?></li>
                    <li style="list-style-type: none; margin-bottom: 0"><b>Email: </b><?php echo $curator['email']??''; ?></li>
                <?php endforeach ?>
                </ul>
            <?php endif ?>
        </li>
    </ol>
</div>

<div contenteditable="true" id="570bacf8-eca2-4bd6-81ad-392aaec98001">
    <strong>Điều 2: Thời hạn hợp đồng</strong>
    <br />
    <p>
    Hợp đồng này có hiệu lực <?php echo $contract_days;?> ngày, kể từ ngày <?php echo my_date($contract_begin, 'd');?> tháng <?php echo my_date($contract_begin, 'm');?> năm <?php echo my_date($contract_begin, 'Y');?> đến ngày <?php echo my_date($contract_end, 'd');?> tháng <?php echo my_date($contract_end, 'm');?> năm <?php echo my_date($contract_end, 'Y');?> hoặc đến khi hết ngân sách quảng cáo.    
    </p>
</div>

<div contenteditable="true" id="f188137c-937e-4563-a551-fe5b1dd6018f">
    <strong>Điều 3. Phương thức và phương tiện quảng cáo</strong><br />
    
    <p>1. <b>Phương thức: </b>Quảng cáo được thực hiện bằng hình thức:  Văn bản/ hình ảnh/ biểu tượng/ âm thanh.</p>
    <p>2. <b>Phương tiện: </b>Đăng trên Fanpage của Facebook.</p>
    <p>Việc sử dụng phương tiện quảng cáo thương mại phải đảm bảo:</p>
    <ol type="a" style="padding-left: 15px">
        <li>Tuân thủ các quy định của pháp luật về báo chí, xuất bản, thông tin, chương trình hoạt động văn hoá, thể thao, hội chợ, triển lãm.</li>
        <li>Tuân thủ quy định về địa điểm quảng cáo, không gây ảnh hưởng xấu đến an ninh xã hội.</li>
        <li>Đúng mức độ, thời lượng, thời điểm quy định đối với từng loại phương tiện thông tin đại chúng.</li>
    </ol>
</div>

<?php

$total = 0;

if ($data_service['service_fee'])
{
    $total += $data_service['service_fee'];
}

$services_pricetag  = get_term_meta_value($term->term_id, 'services_pricetag');
$services_pricetag  = is_serialized($services_pricetag) ? unserialize($services_pricetag) : [];
$discount_amount    = 0;
$discount_rate      = 0;

$service_fee_with_discount = $data_service['service_fee'];
if(!empty($data_service['discount_amount']))
{
    $service_fee_with_discount-= $data_service['discount_amount'];
}

if( ! empty($services_pricetag['discount']['is_discount']))
{
    $discount_amount = ($total * ((float) @$services_pricetag['discount']['percent'] / 100));
    if($discount_amount != 0)
    {
        $total-=$discount_amount;
        $discount_rate = (float) @$services_pricetag['discount']['percent'];
    }
}

if( ! empty($data_service['discount_amount']))
{
    $total-= $data_service['discount_amount'];
}

$total_regular = $total;

$tax = 0;
if(!empty($vat))
{
    $tax    = $total*($vat/100);
    $total  = cal_vat($total,$vat);
}

$total_words = currency_number_to_words(numberformat($total,0,'.',''));

?>

<div contenteditable="true" id="f89857c4-3c81-4d2a-9f1d-fda82a588d7d">
    <strong>Điều 4. Phí dịch vụ, thời hạn và phương thức thanh toán</strong>
    <br />
    <p><b>1. Mức phí dịch vụ</b></p>

    <ol type="a" style="padding-left: 15px">
        <li>
            <p>
                Thù lao của Hợp đồng này được Bên A thanh toán cho Bên B là <b><?php echo currency_numberformat($total, ' đồng');?></b> <i>(Bằng chữ: <?php echo $total_words;?>)</i> đã bao gồm <?php echo currency_numberformat($vat, ' %');?> thuế VAT. Thanh toán bằng tiền đồng Việt Nam. Chi tiết phí dịch vụ như sau:
            </p>
            <table border="1" cellspacing="0" cellpadding="3" width="100%">
                <tr>
                    <th>Dịch vụ</th>
                    <th>Ngân sách</th>
                    <th>Thời gian</th>
                    <th>Thành tiền(vnđ)</th>
                </tr>

                <tr>
                    <td><?php echo $data_service['service_name'];?></td>
                    <td style="text-align:center">
                        <?php echo currency_numberformat($data_service['budget'],'');?>
                            
                    </td>
                    <td style="text-align:center">
                        <?php echo $contract_days;?> ngày<br />
                        <?php printf('(từ %s đến %s)', my_date($contract_begin,'d/m/Y'), my_date($contract_end,'d/m/Y'));?>
                    </td>
                    <td style="text-align:right">0</td>
                </tr>

                <?php if ($fct && !empty($fct_amount)): ?>
                <tr>
                    <td colspan="3" style="text-align:left">Thuế nhà thầu</td>
                    <td style="text-align:right"><?php echo currency_numberformat($fct_amount, '');?></td>
                </tr>
                <?php endif ?>

                <?php if($is_display_promotions_discount) :?>
                    <!-- NORNAL WAY -->
                    <?php if ($data_service['service_fee']): ?>
                    <tr>
                        <td colspan="3" style="text-align:left">Phí dịch vụ</td>
                        <td style="text-align:right"><?php echo currency_numberformat($data_service['service_fee'],'');?></td>
                    </tr>
                    <?php endif ?>
                    <?php if ( ! empty($services_pricetag['discount']['is_discount']) && $discount_amount != 0) : ?>
                    <tr>
                        <td  colspan="3" style="text-align:left">
                            <strong>Giảm giá <?php echo $discount_rate;?> %</strong>
                        </td>
                        <td style="text-align:right">
                            <?php echo currency_numberformat($discount_amount);?>    
                        </td>
                    </tr>
                    <?php endif; ?>

                    <?php if( ! empty($data_service['discount_amount'])) : ?>
                    <?php foreach ($data_service['promotions'] as $promotion): ?>
                    <tr>
                        <td colspan="3" style="text-align:left"><?php echo $promotion['text'] ;?></td>
                        <td style="text-align:right">-<?php echo currency_numberformat($promotion['amount'], '');?></td>
                    </tr> 
                    <?php endforeach;?>
                    <?php endif;?>

                <?php else : ?>
                    <!-- NORNAL WAY -->
                    <?php if ($service_fee_with_discount): ?>
                    <tr>
                        <td colspan="3" style="text-align:left">Phí dịch vụ</td>
                        <td style="text-align:right"><?php echo currency_numberformat($service_fee_with_discount,'');?></td>
                    </tr>
                    <?php endif ?>
                <?php endif; ?>

                <tr>
                    <td colspan="3" style="text-align:left"><strong>Tổng cộng</strong></td>
                    <td style="text-align:right"><?php echo currency_numberformat($total_regular,'');?></td>
                </tr>

                <?php if($vat) :?>
                <tr>
                    <td colspan="3" style="text-align:left">
                        VAT (<?php echo currency_numberformat($vat, '%') ;?>)
                    </td>
                    <td style="text-align:right"><?php echo currency_numberformat($tax,''); ?></td>
                </tr>
                <tr>
                    <td  colspan="3" style="text-align:left"><strong>Số tiền phải thanh toán</strong></td>
                    <td  style="text-align:right"><?php echo currency_numberformat($total,'');?></td>
                </tr>
                <?php endif;?>
            </table>
        </li>
        <?php if ($has_deposit) :?>
        <li>
            Bên A đặt cọc trước cho Bên B số tiền <?php echo currency_numberformat($deposit_amount, ' vnđ') ;?> (<?php echo currency_number_to_words(numberformat($deposit_amount,0,'.',''));?>) sau khi ký và trước khi thực hiện hợp đồng.
        </li>
        <?php endif; ?>
        <li>Các chi phí liên quan đến thanh toán ngân sách quảng cáo do Bên A thực hiện trực tiếp với Facebook.</li>
        <li>Bên A có trách nhiệm thanh toán và đảm bảo việc thanh toán không bị gián đoạn, không làm ảnh hưởng đến chiến dịch quảng cáo.</li>
    </ol>

    <p><b>2. Phương thức thanh toán</b></p>

    <p>Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin sau:</p>
    <?php if( ! empty($bank_info)) :?>
    <ul style="padding-left:0;list-style:none">
    <?php foreach ($bank_info as $label => $text) :?>
        <?php if (is_array($text)) : ?>
            <?php foreach ($text as $key => $value) :?>
                <li>- <?php echo $key;?>: <?php echo $value;?></li>
            <?php endforeach;?>
        <?php continue; endif;?>
        <li>- <?php echo $label;?>: <?php echo $text;?></li>
    <?php endforeach;?>
    </ul>
    <p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
    <?php endif; ?>

    <?php if ($vat > 0): ?>
    <p>Bên B xuất hóa đơn giá trị gia tăng cho Bên A theo thông tin sau:</p>
    <ul style="padding-left:0; list-style:none">
        <li>- Tên công ty: <?php echo $customer->display_name ?? '';?></li>
        <li>- Địa chỉ: <?php echo $data_customer['Địa chỉ'] ?? '';?></li>
        <li>- Mã số thuế: <?php echo $data_customer['Mã số thuế'] ?? '';?></li>
    </ul>

    <p>Hóa đơn điện tử được Bên B gửi cho Bên A theo  địa chỉ email: <?php echo $customer->representative['email'] ?? '';?></p>
    <?php endif ?>

    <p>
        <b>3. Thời hạn thanh toán: </b>
        <?php echo $payments;?>
    </p>
</div>

<div contenteditable="true" id="1163a317-a206-4727-8580-ce53de0b00cf">
    <strong>Điều 5. Quyền và nghĩa vụ của Bên A</strong>
    <br />

    <p><b>1. Quyền của Bên A</b></p>
    <ol type="a" style="padding-left: 15px">
        <li>Lựa chọn hình thức, nội dung, phương tiện, phạm vi và thời gian quảng cáo thương mại.</li>
        <li>Kiểm tra, giám sát việc thực hiện hợp đồng dịch vụ quảng cáo thương mại.</li>
    </ol>

    <p><b>2. Nghĩa vụ của Bên A</b></p>
    <ol type="a" style="padding-left: 15px">
        <li>Bên A có nghĩa vụ cung cấp thông tin trung thực, chính xác về hoạt động sản xuất, hàng hóa dịch vụ thương mại của đơn vị mình và chịu trách nhiệm về các thông tin do mình cung cấp cho Bên B.</li>
        <li>Khi có sự tranh chấp của bên thứ ba về những nội dung thông tin kinh tế, nhãn hiệu hàng hóa, bản quyền… đối với Bên A thì Bên A phải tự mình giải quyết, trong trường hợp đó Bên B có quyền đơn phương đình chỉ hợp đồng và yêu cầu Bên A chịu trách nhiệm bồi thường chi phí cho Bên B (nếu có).</li>
        <li>Trả phí dịch vụ quảng cáo theo thỏa thuận nêu tại Điều 4 của hợp đồng.</li>
        <li>Có nghĩa vụ khai báo các khoản thuế nhà thầu và các loại thuế có liên quan theo quy định pháp luật.</li>
    </ol>
</div>

<div contenteditable="true" id="5151e149-6a80-4f9c-a078-c7790f7290e1">
    <strong>Điều 6. Quyền và nghĩa vụ của Bên B</strong>
    <br />

    <p><b>1. Quyền của Bên B</b></p>
    <ol type="a" style="padding-left: 15px">
        <li>Yêu cầu Bên A cung cấp thông tin quảng cáo trung thực, chính xác theo đúng thời hạn của hợp đồng.</li>
        <li>Được nhập khẩu vật tư, nguyên liệu và các sản phẩm quảng cáo thương mại cần thiết cho hoạt động dịch vụ quảng cáo của mình theo quy định của pháp luật.</li>
        <li>Nhận phí quảng cáo theo thỏa thuận trong hợp đồng.</li>
    </ol>

    <p><b>2. Nghĩa vụ của Bên B</b></p>
    <ol type="a" style="padding-left: 15px">
        <li>Thực hiện dịch vụ quảng cáo thương mại theo đúng thỏa thuận trong hợp đồng.</li>
        <li>Thực hiện sự lựa chọn của Bên A về người phát hành quảng cáo thương mại, hình thức, nội dung, phương tiện, phạm vi và thời gian quảng cáo thương mại.</li>
        <li>Tổ chức quảng cáo trung thực, chính xác về hoạt động kinh doanh hàng hoá, dịch vụ thương mại theo thông tin mà Bên A đã cung cấp.</li>
    </ol>
</div>

<div contenteditable="true" id="97ffcab6-2a92-4f65-803a-bb55b2b90354">
    <strong>Điều 7. Điều khoản bảo mật thông tin</strong>
    <br />
    <p>1. Mỗi bên có nghĩa vụ bảo vệ thông tin mật của bên kia và cam kết chỉ sử dụng các thông tin đó cho mục đích thực hiện hợp đồng này.</p>    
    <p>2. Nghĩa vụ bảo mật thông tin không được xem là vi phạm đối với các trường hợp sau: </p>
    <ol type="a" style="padding-left: 15px">
        <li>Theo quy định pháp luật hoặc quyết định, bản án của cơ quan nhà nước có thẩm quyền.</li>
        <li>Cung cấp thông tin cho bên Tư vấn pháp luật hoặc tư vấn khác nhằm mục đích thực hiện hợp đồng này.</li>
        <li>Cung cấp thông tin cho nhân viên, nhà thầu, đối tác của mỗi bên nhằm mục đích thực hiện hợp đồng này.</li>
        <li>Trường hợp cung cấp thông tin trong các trường hợp ở trên, mỗi bên có nghĩa vụ áp dụng những biện pháp hợp lý nhằm đảm bảo các cá nhân, tổ chức tiếp nhận thông tin có nghĩa vụ bảo mật thông tin như các bên trong hợp đồng này.</li>
    </ol>
</div>

<div contenteditable="true" id="545f0ecd-3a08-491e-8762-5f1f7e006302">
    <strong>Điều 8. Tạm ngưng quảng cáo</strong>
    <br />
    <p><b>1. Tạm ngưng quảng cáo bởi Bên A</b></p>
    <ol type="a" style="padding-left: 15px">
        <li>
            Trong trường hợp Bên A đã thanh toán đủ 100% cho dịch vụ nhưng muốn tạm ngưng việc đăng quảng cáo trên website của Bên A, Bên A phải gửi yêu cầu bằng văn bản hoặc email cho Bên B đề nghị tạm ngưng một phần hoặc toàn bộ dịch vụ trước ít nhất ba (03) ngày. Trong trường hợp đó, số ngày hiện quảng cáo trên hệ thống quảng cáo của Bên A sẽ được tăng thêm đúng bằng số ngày tạm dừng quảng cáo nhưng thời gian dừng không quá 1 tháng (30 ngày).
        </li>
        <li>
            Nếu thời gian tạm ngưng vượt quá thời hạn của hợp đồng thì Bên A có quyền thay đổi giá trị dịch vụ theo chính sách giá của Bên B bằng việc gửi văn bản thông báo cho Bên B.
        </li>
    </ol>

    <p><b>2. Tạm ngưng quảng cáo bởi Bên B</b></p>
    <ol type="a" style="padding-left: 15px">
        <li>
            Bên B sẽ tạm ngưng cung cấp dịch vụ cho Bên A nếu:
            <br/>
            - Bên A vi phạm các điều khoản quy định trong hợp đồng quảng cáo, các quy định của pháp luật về quảng cáo và quy định quảng cáo của Bên B. Dịch vụ chỉ được Bên B mở lại sau khi Bên A chấm dứt hành vi vi phạm trong vòng ba (3) ngày kể từ ngày nhận được thông báo của Bên B, mọi chi phí khắc phục Bên A phải chịu trách nhiệm.
            <br/>
            - Bên A chậm thanh toán cho Bên B theo quy định tại Điều 4 của Hợp đồng này. Bên B sẽ tạm dừng chiến dịch quảng cáo tương ứng với số ngày chậm thanh toán của Bên A. Bên B có trách nhiệm đưa quảng cáo xuất hiện trở lại 01 (một) ngày sau khi nhận được đầy đủ phần thanh toán bị chậm của Bên A.
        </li>
        <li>
            Đối với những nội dung quảng cáo gây ra phản ứng không tốt đối với người dùng thì Bên B sẽ ngừng chạy quảng cáo ngay lập tức và thông báo cho Bên A để thay đổi và chỉnh sửa nội dung quảng cáo.
        </li>
    </ol>
</div>

<div contenteditable="true" id="84e08f68-41b4-4982-9862-742dbe838dbb">
    <strong>Điều 9. Chấm dứt hợp đồng</strong>
    <br />

    <ol style="padding-left: 15px">
        <li>Trong trường hợp Bên A có thay đổi chiến dịch quảng cáo và muốn chấm dứt hợp đồng trước thời hạn thì Bên A phải gửi thông báo trước bằng văn bản cho Bên B trước 07 (bảy) ngày khi chính thức dừng quảng cáo và phải được Bên B chấp nhận về thời điểm dừng hợp đồng.</li>
        <li>Bên B sẽ không thực hiện hoàn trả cho Bên A giá trị phí dịch vụ quản lý tài khoản trong trường hợp Bên A chấm dứt hợp đồng trước hạn.</li>
        <?php if ($has_deposit) :?>
        <li>
            Sau khi nghiệm thu đối soát, Bên B có trách nhiệm gửi biên bản nghiệm thu cho Bên A. Nếu Bên A nợ Bên Facebook, Bên A có trách nhiệm cắn trừ số tiền nợ vào số tiền Bên A đã đặt cọc cho Bên B. Nếu Bên A đã thanh toán đủ cho Facebook, Bên B có trách nhiệm hoàn trả đầy đủ khoản tiền cọc cho Bên A trong vòng 07 ngày làm việc kể từ khi Bên B nhận đủ chứng từ yêu cầu hoàn tiền cọc từ Bên A.
        </li>
        <?php endif; ?>
        <li>
            Trong trường hợp có những sự kiện bất khả kháng không lường trước được, trách nhiệm và thời hạn thực hiện hợp đồng của cả hai bên sẽ được xem xét, đàm phán và quyết định lại. Các sự kiện bất khả kháng bao gồm nhưng không giới hạn các rủi ro sau:
            <ol type="a" style="padding-left: 0px">
                <li>Rủi ro do ngừng hoặc lỗi kĩ thuật từ dịch vụ hệ thống quảng cáo cung cấp</li>
                <li>Rủi ro về đường truyền internet, cơ sở hạ tầng mạng quốc gia</li>
                <li>Thiên tai, chiến tranh, khủng bố, hoả hoạn, dịch bệnh...</li>
            </ol>
        </li>
        <li>
            Chiến dịch quảng cáo sẽ hoàn tất khi Bên B gửi thông báo kết thúc dịch vụ qua email cho Bên A. Sau 2 ngày kể từ khi nhận được email từ Bên B, nếu Bên A không có phản hồi khác, hợp đồng được xem là kết thúc.
        </li>
    </ol>
</div>

<div contenteditable="true" id="57bc1b7b-7efe-4203-a07f-fe77c184ccd4">
    <strong>Điều 10. Tranh chấp và phân xử</strong>
    <br />
    <p>
        Hai bên cam kết thực hiện những điều khoản trong hợp đồng. Nếu có vướng mắc, mỗi bên thông báo cho nhau để cùng bàn bạc giải quyết trên tinh thần hợp tác, thiện chí, vì lợi ích cả hai bên. Trong trường hợp không thể giải quyết được bất đồng, tranh chấp sẽ được giải quyết tại Tòa án có thẩm quyền tại TP Hồ Chí Minh.
    </p>
</div>

<div contenteditable="true" id="2d78e475-42ea-4e6e-88bd-62a3a9c880bf">
    <strong>Điều 11. Hiệu lực Hợp đồng</strong>
    <br />
    <p>
        Hợp đồng có hiệu lực kể từ ngày hai bên ký và được xem như tự động thanh lý khi hai bên thực hiện đầy đủ tất cả các điều khoản nêu trong hợp đồng này.
        <br/>
        Mọi sửa đổi hoặc bổ sung phải được lập thành phụ lục bằng văn bản được hai bên ký, đóng dấu.
<br/>
        Hợp đồng này được làm thành 02 (hai) bản bằng tiếng Việt, có giá trị ngang nhau, Bên A giữ 01 bản, Bên B giữ 01 bản để thực hiện.
    </p>
</div>



<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
    <tr>
        <td width="44%" valign="top" style="text-align:center">
            <p>
                <strong>BÊN A</strong><br />
                <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                <strong><?php echo mb_strtoupper($customer->display_name);?>  </strong>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
            <td rowspan="2"></td>

            <td width="46%" valign="top" style="text-align:center">
                <p><strong>BÊN B</strong><br />
                    <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                    <strong>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</strong>
                </p>
                <p><strong>&nbsp;</strong></p>
                <p><strong>&nbsp;</strong></p>
                <p>&nbsp;</p>
            </td>
    </tr>
    <tr>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_customer['Đại diện'] ;?></strong><br/>
                <?php echo $data_customer['Chức vụ'] ;?>
            </p>
        </td>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_represent['Đại diện'] ?? ''; ?></strong><br />
                <?php echo $data_represent['Chức vụ']; ?>
            </p>
        </td>
    </tr>
</table>

<?php if(!empty($contract_budget_customer_payment_type) && 'behalf' == $contract_budget_customer_payment_type) $this->load->view('googleads/contract/preview/behaft_appendix');?>

<script type="text/javascript">

    (function() {
        var beforePrint = function() {
            console.log('Functionality to run before printing.');
        };
        var afterPrint = function() {
            console.log('Functionality to run after printing');
        };

        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');
            mediaQueryList.addListener(function(mql) {
                console.log(mql);
                if (mql.matches) {
                    beforePrint();
                } else {
                    afterPrint();
                }
            });
        }

        window.onbeforeprint = beforePrint;
        window.onafterprint = afterPrint;
    }());
</script>
</body>
</html>