<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetAdAccount extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'facebookads/adaccount_m';

        parent::__construct();

        $this->load->config('contract/receipt');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {    
        $response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
        if( ! has_permission('facebookads.overview.access'))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $args = wp_parse_args( parent::get(), [
            'offset' => 0,
            'per_page' => 20,
            'cur_page' => 1,
            'is_filtering' => true,
            'is_ordering' => true
        ]);

        $this->filtering();

        $join_insight_conditon = '';
        $join_balance_spend_conditon = '';
        $dateRange = $args['where']['dateRange'] ?? FALSE;
        if ($dateRange) {
            $dates = explode(' - ', $dateRange);
            if('Invalid date' != $dates[0] && 'Invalid date' != $dates[1]) {
                $start_date = start_of_day(reset($dates));
                $end_date = end_of_day(end($dates));
                $join_insight_conditon .= " AND insights.start_date >= {$start_date} AND insights.end_date <= {$end_date}";
                $join_balance_spend_conditon .= " AND balance_spend.start_date >= {$start_date} AND balance_spend.end_date <= {$end_date}";
            }
        }

        $data['content'] = $this
        ->datatable_builder
        ->set_filter_position(FILTER_TOP_INNER)
        ->setOutputFormat('JSON')

        ->from('term')
        ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
        ->join('posts AS balance_spend', 'balance_spend.post_id = tp_contract_ads_segment.post_id AND balance_spend.post_type = "balance_spend"' . ($join_balance_spend_conditon ?: ''), 'LEFT')  
        ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'LEFT')  
        ->join('term_posts AS tp_segment_adaccount', 'tp_segment_adaccount.post_id = ads_segment.post_id', 'LEFT')  
        ->join('term AS adaccount', 'tp_segment_adaccount.term_id = adaccount.term_id AND adaccount.term_type = "adaccount"', 'LEFT')  
        ->join('termmeta AS adaccount_metadata', 'adaccount_metadata.term_id = adaccount.term_id AND meta_key = "source"', 'LEFT')  
        ->join('term_posts AS tp_adaccount_insights', 'tp_adaccount_insights.term_id = adaccount.term_id', 'LEFT')  
        ->join('posts AS insights', 'tp_adaccount_insights.post_id = insights.post_id AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) AND insights.start_date <= if(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) AND insights.post_type = "insight_segment" AND insights.post_name = "day"' . ($join_insight_conditon ?: ''), 'LEFT')
        ->join('postmeta AS insight_metadata', 'insight_metadata.post_id = insights.post_id AND insight_metadata.meta_key IN ("account_currency", "clicks", "cpc", "cpm", "cpp", "ctr", "impressions", "reach", "spend")', 'LEFT')
        ->where('term.term_type', 'facebook-ads') 
        ->where('( adaccount.term_id > 0 OR balance_spend.post_id > 0)')
        ->group_by('adaccount.term_id, ads_segment.post_id, balance_spend.post_id')
        ->order_by('adaccount.term_slug', 'ASC')

        ->select("term.term_id AS contractId")
        ->select("COALESCE(ads_segment.post_id, balance_spend.post_id) AS segmentId")
        ->select("COALESCE(ads_segment.post_type, balance_spend.post_type) AS segmentType")
        ->select("COALESCE(ads_segment.start_date, balance_spend.start_date) AS segmentStartDate")
        ->select("COALESCE(ads_segment.end_date, balance_spend.end_date) AS segmentEndDate")
        ->select("adaccount.term_id AS adAccountTermId")
        ->select("adaccount.term_slug AS adAccountId")
        ->select("adaccount.term_name AS adAccountName")
        ->select("adaccount.term_status AS adAccountStatus")
        ->select("COALESCE(max(if(adaccount_metadata.meta_key = 'source', adaccount_metadata.meta_value, null)), balance_spend.comment_status) as adAccountSource")
        ->select("MAX(IF(insight_metadata.meta_key = 'account_currency', insight_metadata.meta_value, NULL)) AS account_currency")
        ->select("SUM(IF(insight_metadata.meta_key = 'clicks', insight_metadata.meta_value, NULL)) AS clicks")
        ->select("SUM(IF(insight_metadata.meta_key = 'impressions', insight_metadata.meta_value, NULL)) AS impressions")
        ->select("SUM(IF(insight_metadata.meta_key = 'reach', insight_metadata.meta_value, NULL)) AS reach")
        ->select("COALESCE(SUM(IF(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, NULL)), balance_spend.post_content) AS spend")
        ->select("balance_spend.post_excerpt as note")
        ->select("ads_segment.post_author AS segment_technician_id")
        ->select("ads_segment.post_content AS segment_technician_rate")

        ->add_search('adAccountId',['placeholder'=>'ID Tài khoản'])
        ->add_search('adAccountName',['placeholder'=>'Tên tài khoản'])

        ->add_column('adAccountId', array( 'set_select' => FALSE, 'title' => 'AdAccount ID'))
        ->add_column('adAccountName', array( 'set_select' => FALSE, 'title' => 'AdAcount'))
        ->add_column('adAccountStatus', array( 'set_select' => FALSE, 'title' => 'Trạng thái'))
        ->add_column('adAccountSource', array( 'set_select' => FALSE, 'title' => 'Nguồn'))
        ->add_column('segmentStartDate', array( 'set_select' => FALSE, 'title' => 'Ng. bắt đầu'))
        ->add_column('segmentEndDate', array( 'set_select' => FALSE, 'title' => 'Ng. kết thúc'))
        ->add_column('segment_technician', array( 'set_select' => FALSE, 'title' => 'Kỹ thuật phụ trách'))
        ->add_column('segment_technician_rate', array( 'set_select' => FALSE, 'title' => 'Tỉ lệ phụ trách'))
        ->add_column('account_currency', array( 'set_select' => FALSE, 'title' => 'Đơn vị tiền'))
        ->add_column('spend', array( 'set_select' => FALSE, 'title' => 'Chi tiêu'))
        ->add_column('clicks', array( 'set_select' => FALSE, 'title' => 'Clicks'))
        ->add_column('impressions', array( 'set_select' => FALSE, 'title' => 'Hiển thị'))
        ->add_column('reach', array( 'set_select' => FALSE, 'title' => 'Reach'))

        ->add_column_callback('segmentType', function ($data, $row_name) {
            if('balance_spend' != $data['segmentType'])
            {
                return $data;
            }

            $data['adAccountId'] = 'Theo hợp đồng';
            $data['adAccountName'] = 'Cân bằng thủ công';

            if('auto' == $data['adAccountSource'])
            {
                $data['adAccountName'] = 'Chưa xác minh hợp đồng nối';

                if(empty($data['note']))
                {
                    return $data;
                }
                
                $data['adAccountName'] = $data['note'];
            }

            return $data;
        }, FALSE)

        ->add_column_callback('segment_technician_id', function ($data, $row_name) {
            if(empty($data['segment_technician_id']))
            {
                $data['segment_technician'] = null;
                return $data;
            }

            $technician_id = $data['segment_technician_id'];
            $staff_display_name = $this->admin_m->get_field_by_id($technician_id, 'display_name');
            $data['segment_technician'] = $staff_display_name ?: '--';
            return $data;
        }, FALSE);

        $pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);
        $subheadings_data = $this->datatable_builder->generate(['per_page' => PHP_INT_MAX, 'cur_page' => 1]);

        // Calc spend depends on segment_technician_rate
        $data_items = [$data, $subheadings_data];
        foreach($data_items as &$data_item)
        {
            $data_rows = $data_item['rows'];
            $_data_rows = array_group_by($data_rows, 'adAccountTermId');

            $_data_rows = array_map(function($rows) {
                $instance = reset($rows);
                if(empty($instance))
                {
                    return $rows;
                }
    
                if('ads_segment' != $instance['segmentType'])
                {
                    return $rows;
                }
    
                $num_of_overlap = count($rows);
                if($num_of_overlap < 2)
                {
                    return $rows;
                }
    
                foreach($rows as &$row)
                {
                    $segment_technician_rate = (int) $row['segment_technician_rate'];
                    $segment_technician_rate = div($segment_technician_rate, 100);
    
                    $spend = $row['spend'];
                    $spend = round($spend * $segment_technician_rate);
                    $row['spend'] = $spend;
    
                    $clicks = $row['clicks'];
                    $clicks = round($clicks * $segment_technician_rate);
                    $row['clicks'] = $clicks;
    
                    $impressions = $row['impressions'];
                    $impressions = round($impressions * $segment_technician_rate);
                    $row['impressions'] = $impressions;
    
                    $reach = $row['reach'];
                    $reach = round($reach * $segment_technician_rate);
                    $row['reach'] = $reach;
                }
    
                return $rows;
            },
            $_data_rows);
            $data_item['rows'] = array_merge(...array_values($_data_rows));
        }

        $data = reset($data_items);
        $subheadings_data = end($data_items);
        
        $subheadings_data = $subheadings_data['rows'];
        $subheadings = array_reduce($subheadings_data, function ($result, $item) {
            $result['spend'] += (double)$item['spend'];
            $result['clicks'] += (int)$item['clicks'];
            $result['impressions'] += (int)$item['impressions'];
            $result['reach'] += (int)$item['reach'];

            return $result;
        }, [
            'spend' => 0,
            'clicks' => 0,
            'impressions' => 0,
            'reach' => 0,
        ]);
        $subheadings = array_map(function ($item) {
            $item .= '';
            return $item;
        }, $subheadings);
        $data['subheadings'] = $subheadings;
        
        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering()
    {
        restrict('facebookads.overview.access');

        $args = parent::get(NULL, TRUE);
        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

        $contractId = $args['where']['contractId'] ?? FALSE;
		if($contractId)
		{
            $this->datatable_builder->where("term.term_id", (int) $contractId);
            unset($args['where']['contractId']);
		}

		$adAccountId = $args['where']['adAccountId'] ?? FALSE;
		if($adAccountId)
		{
            $this->datatable_builder->where("adaccount.term_slug like '%" . $this->db->escape_like_str(trim($adAccountId)) . "%'");
            unset($args['where']['adAccountId']);
		}

        $adAccountName = $args['where']['adAccountName'] ?? FALSE;
		if($adAccountName)
		{
            $this->datatable_builder->where("adaccount.term_name like '%" . $this->db->escape_like_str(trim($adAccountName)) . "%'");
            unset($args['where']['adAccountName']);
		}
    }
}
/* End of file DatasetReceipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetReceipts.php */