<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * DATASET KPI API FOR BACK-END
 */
class DatasetKpi extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'facebookads/facebookads_m';
        $this->autoload['models'][] = 'facebookads/facebookads_kpi_m';

        parent::__construct();
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response   = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        $permission = 'facebookads.kpi.access';

        if (!has_permission($permission)) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        $relate_users = $this->admin_m->get_all_by_permissions($permission);
        if ($relate_users === FALSE) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        $args = wp_parse_args(parent::get(), [
            'offset' => 0,
            'per_page' => 20,
            'cur_page' => 1,
            'is_filtering' => true,
            'is_ordering' => true
        ]);

        $this->load->library('datatable_builder');

        if (is_array($relate_users)) {
            $this->datatable_builder->join('term_users', 'term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
        }

        $this->filtering();

        $data['content'] = $this
            ->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')

            ->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])
            ->add_search('start_service_time', ['placeholder' => 'Ngày chạy DV', 'class' => 'form-control set-datepicker'])

            ->add_column('contract_code', array('title' => 'Mã hợp đồng', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('start_service_time', array('title' => 'Ngày chạy DV', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('staff_business', array('title' => 'Kinh doanh', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('tech_staff', array('title' => 'Phụ trách', 'set_select' => FALSE, 'set_order' => FALSE))

            ->select('term.term_id')
            ->from('term')
            ->where('term.term_type', $this->facebookads_m->term_type)
            ->where_in('term.term_status', array('pending', 'publish'))

            ->add_column_callback('term_id', function ($data, $row_name) {
                $term_id = $data['term_id'];

                $data['contract_code'] = get_term_meta_value($term_id, 'contract_code') ?: '--';

                $start_service_time = get_term_meta_value($term_id, 'start_service_time') ?: NULL;
                $data['start_service_time'] = $start_service_time ? my_date($start_service_time, 'd/m/Y') : '--';

                $staff_business_id = get_term_meta_value($term_id, 'staff_business');
                if (empty($staff_business_id)) $data['staff_business'] = [];
                else {
                    $staff_business = $this->admin_m->get_field_by_id($staff_business_id);

                    $staff_business = [
                        'user_id' => $staff_business['user_id'],
                        'display_name' => $staff_business['display_name'],
                        'user_email' => $staff_business['user_email'],
                        'user_avatar' => $staff_business['user_avatar'],
                    ];

                    $data['staff_business'] = $staff_business;
                }

                $kpis = $this->facebookads_kpi_m->select('user_id')->where('term_id', $term_id)->group_by('user_id')->as_array()->get_many_by();
                if (empty($kpis)) $data['tech_staff'] = '--';
                else {
                    $kpis = array_map(function ($item) {
                        $display_name = $this->admin_m->get_field_by_id($item['user_id'], 'display_name');

                        return $display_name;
                    }, $kpis);

                    $data['tech_staff'] = implode(', ', $kpis);
                }

                return $data;
            }, FALSE)

            ->order_by('term.term_id', 'DESC');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering()
    {
        restrict('facebookads.kpi.access');

        $args = parent::get(NULL, TRUE);
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        $is_unassigned = 'true' == $args['is_unassigned'];
        if($is_unassigned){
            $this->datatable_builder
				->join('googleads_kpi', 'googleads_kpi.term_id = term.term_id', 'LEFT')
                ->group_by('term.term_id')
                ->having('count(googleads_kpi.user_id) = 0');
        }

        // Contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if($filter_contract_code || $sort_contract_code)
        {
            $alias = uniqid('contract_code_');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

            if($filter_contract_code)
            {   
                $this->datatable_builder->like("{$alias}.meta_value",$filter_contract_code);
            }

            if($sort_contract_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_code);
            }

            unset($args['where']['contract_code'],$args['order_by']['contract_code']);
        }

        // start_service_time FILTERING & SORTING
        if( ! empty($args['where']['start_service_time']))
		{
			$explode = explode(' - ', $args['where']['start_service_time']);
			if( ! empty($explode))
			{
				$this->datatable_builder
				->join('termmeta AS m_stated_service', 'm_stated_service.term_id = term.term_id AND m_stated_service.meta_key = "start_service_time"', 'LEFT')
				->where('m_stated_service.meta_value >=', start_of_day(reset($explode)))
				->where('m_stated_service.meta_value <=', end_of_day(end($explode)));
			}
			unset($args['where']['start_service_time']);
		}

		if( ! empty($args['order_by']['start_service_time']))
		{
			$this->datatable_builder
            ->join('termmeta AS m_stated_service', 'm_stated_service.term_id = term.term_id AND m_stated_service.meta_key = "start_service_time"', 'LEFT')
            ->order_by('CAST(m_stated_service.meta_value AS UNSIGNED)', $args['order_by']['start_service_time']);

			unset($args['order_by']['start_service_time']);
		}
    }
}
/* End of file DatasetKpi.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/DatasetKpi.php */