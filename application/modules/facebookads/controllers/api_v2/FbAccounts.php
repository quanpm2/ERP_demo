<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FbAccounts extends MREST_Controller
{
	function __construct()
	{
        $this->autoload['models'][] 	= 'facebookads/facebookads_m';
        $this->autoload['helpers'][] 	= 'date';

		parent::__construct();
	}

	/**
	 * /PUT API-V2/FACEBOOKADS/CONTRACT/ADACCOUNT_INSIGHT/:ID
	 *
	 * @param      int    $term_id  The term identifier
	 *
	 * @return     Json  Result
	 */
	public function adaccount_insight_put(int $term_id = 0)
	{
		$result 	= [ 'success' => 'error', 'msg' => 'Xử lý không thành công', 'data' => ''];
		$contract 	= (new facebookads_m())->set_contract($term_id);

		try
		{
			$contract->update_insights();
			$behaviour_m = $contract->get_behaviour_m();
			$behaviour_m->get_the_progress();
			$behaviour_m->sync_all_amount();
		}
		catch (Exception $e)
		{
			$result['msg'] = $e->getMessage();
			return parent::response($result);
		}

		$result['success'] 	= 'success';
		$result['msg'] 		= 'Dữ liệu đã được đồng bộ thành công. Vui lòng (F5) để xem sự thay đổi';
		return parent::response($result);
	}

	public function exchange_longlive_token_put()
	{
		$this->config->load('facebookads/fbapp');
		$fbApp 	= $this->config->item('production', 'fbapps');

		$args 	= wp_parse_args(parent::put(), [
			'userId' 		=> null,
			'accessToken' 	=> null,
			'app_id' 		=> $fbApp['credential']['app_id'] ?? null,
			'app_secret' 	=> $fbApp['credential']['app_secret'] ?? null,
			'graph_version' => $fbApp['credential']['graph_version'] ?? null,
		]); 

		$this->load->library('form_validation');
		$this->form_validation->set_data($args);
		$this->form_validation->set_rules('userID', 'userID', 'required|numeric');
		$this->form_validation->set_rules('accessToken', 'accessToken', 'required|min[60]');
		$this->form_validation->set_rules('app_id', 'app_id', 'required');
		$this->form_validation->set_rules('app_secret', 'app_secret', 'required');
		$this->form_validation->set_rules('graph_version', 'graph_version', 'required');

		if(FALSE == $this->form_validation->run())
		{
			parent::response([
				'code' => 400,
				'error' => $this->form_validation->error_array()
			]);
		}

		$fb = new Facebook\Facebook([
			'app_id' => $fbApp['credential']['app_id'],
			'app_secret' => $fbApp['credential']['app_secret'],
			'default_graph_version' => $fbApp['credential']['graph_version'],
		]);

		$fbaccount = null;

		try
		{
			$response 	= $fb->get('/me?fields=id,email,name', $args['accessToken']);
			$user 		= $response->getGraphUser();

			$this->load->model('facebookads/fbaccount_m');

			$fbaccount = $this->fbaccount_m->select('user_id, user_login, display_name')->set_type()->get_by(['user_login' => $user->getId()]);

			/* Create new Fb Account if not exists */
			if( ! $fbaccount)
			{
				$insert_data = array(
					'user_login' => $user->getId(),
					'display_name' => $user->getName(),
					'user_email' => $user->getEmail() ?: 'fbaccount-unknown-'.$user->getId().'@adsplus.vn',
					'user_type' => 'fbaccount'
				);

				$fbaccount_id = $this->fbaccount_m->insert($insert_data);
				$insert_data['user_id'] = $fbaccount_id;
				$fbaccount = (object) $insert_data;

				update_user_meta($fbaccount->user_id, 'created_by', $this->admin_m->id);
			}

			update_user_meta($fbaccount->user_id, 'access_token', $args['accessToken']);
		}
		catch(Facebook\Exceptions\FacebookResponseException $e)
		{
			parent::response([ 'code' => 400, 'error' => 'Graph returned an error: ' . $e->getMessage() ]);
		}
		catch(Facebook\Exceptions\FacebookSDKException $e)
		{
			parent::response([ 'code' => 400, 'error' => 'Facebook SDK returned an error: ' . $e->getMessage() ]);
		}

		$accessToken 	= new \Facebook\Authentication\AccessToken($args['accessToken']);
		$oAuth2Client 	= $fb->getOAuth2Client();
		$tokenMetadata 	= $oAuth2Client->debugToken($accessToken);

		$tokenMetadata->validateAppId($args['app_id']);
		$tokenMetadata->validateExpiration();

		if ( ! $accessToken->isLongLived())
		{
			// Exchanges a short-lived access token for a long-lived one
			try
			{
				$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
				update_user_meta($fbaccount->user_id, 'access_token', $accessToken->getValue());
			}
			catch (Facebook\Exceptions\FacebookSDKException $e)
			{
				parent::response(['code' => 400, 'error' => 'Cập nhật thông tin tài khoản thành công, tuy nhiên không thể trao đổi lấy Token dài hạn.']);
			}
		}

		parent::response(['code' => 200, 'data' => $fbaccount]);
	}
}
/* End of file FbAccounts.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/FbAccounts.php */