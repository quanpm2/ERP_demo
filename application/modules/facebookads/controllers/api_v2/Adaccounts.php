<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adaccounts extends MREST_Controller
{
	function __construct()
	{
		$this->autoload['libraries'][] 	= 'form_validation';
        $this->autoload['models'][] 	= 'facebookads/facebookads_m';
        $this->autoload['models'][] 	= 'facebookads/adaccount_m';
        $this->autoload['helpers'][] 	= 'date';

		parent::__construct();
	}

	public function index_get()
	{
        $args           = wp_parse_args(parent::get(NULL, TRUE), []);
        $args_encypt    = md5(json_encode($args));

        $key_cache      = Adaccount_m::CACHE_ALL_KEY . $args_encypt;
        $adaccounts   = $this->scache->get($key_cache);
        if( ! $adaccounts)
        {
            $adaccounts = $this->adaccount_m
            ->set_term_type()
            ->select('term_id, term_name, term_slug, term_parent')
            ->order_by('term_id', 'DESC')
            ->limit(500);

            if(!empty($args['account_id'])){
                $adaccounts->where("(term_slug LIKE '%{$args['account_id']}%' OR term_name LIKE '%{$args['account_id']}%')");
            }

            $adaccounts = $adaccounts->get_all();

            if( ! $adaccounts)
            {
                return parent::responseHandler([], 'Empty account', 'success', 204);
            }

            $this->scache->write($adaccounts, $key_cache, 300);
        }
        
        $adaccounts = array_map(function($x){
            $x->term_id = (int) $x->term_id;
            $x->term_name = $x->term_name;
            $x->isExternal = get_term_meta_value($x->term_id,'isExternal');
            return $x;
        }, $adaccounts);

        parent::responseHandler($adaccounts, 'OK');
	}

	/**
	 * Create new Adaccount & Business Account
	 */
	public function index_post()
	{
		$this->load->library('form_validation');

		$post = parent::post(null, TRUE);

		$this->form_validation->set_data($post);
        $this->form_validation->set_rules('term_name', 'adaccount name', 'required');
        $this->form_validation->set_rules('term_slug', 'adaccount id', 'required|numeric');

        if(FALSE == $this->form_validation->run())
    	{
    		parent::response([
    			'code' => 400,
    			'error' => $this->form_validation->error_array()
    		]);
    	}

    	$this->load->model('facebookads/adaccount_m');

    	$isExists = $this->adaccount_m->select('term_id')->set_term_type()->where('term_slug', "act_{$post['term_slug']}")->count_by() > 0;

    	if($isExists)
    	{
    		parent::response([
				'code' => 400,
				'error' => "Tài khoản đã tồn tại. không thể thêm mới"
			]);
    	}

    	$insert_data = array(
            'term_slug' => "act_{$post['term_slug']}",
            'term_name' => $post['term_name'],
            'term_status' => 'active',
            'term_type' => $this->adaccount_m->term_type
        );

    	$insert_id 					= $this->adaccount_m->insert($insert_data);
    	$insert_data['term_id'] 	= (int) $insert_id;
    	$insert_data['isExternal'] 	= true;

		$meta = array(
            'created_time' => time(),
            'currency' => 'VND',
            'owner' => $this->admin_m->id,
            'isExternal' => 1,
			'source' => 'direct'
        );

        foreach ($meta as $meta_key => $meta_value)
        {
            update_term_meta($insert_id, $meta_key, $meta_value);
        }


		parent::response([
			'code' => 200,
			'message' => "Thêm mới thành công",
			'data' => $insert_data
		]);
	}
	
	/**
	 * { function_description }
	 *
	 * @param      int   $fbAccountId  The fb account identifier
	 */
	public function sync_by_account_id_put($fbAccountId = 0)
	{
		$fbAccountId = (int) $fbAccountId;
		if(empty($fbAccountId)) parent::response(['code' => 404, 'error' => 'Tài khoản Fb không tồn tại']);

		$this->load->model('facebookads/fbaccount_m');
		$fbAccount = $this->fbaccount_m->set_type()->get($fbAccountId);
		if( ! $fbAccount) parent::response(['code' => 404, 'error' => 'Tài khoản Fb không tồn tại [1]']);

		$this->load->config('external_service');
		$api = $this->config->item('adWarehouse', 'api');

		$response = null;

		try
		{
			$headers	= [ 'Authorization' => 'Bearer ' . $this->admin_m->generateKey(47) ];
			$response	= Requests::get("{$api}/facebookads/accounts/{$fbAccountId}/adaccounts/fetch", $headers, ['timeout' => 180,'connect_timeout' => 180]);

			if(200 != $response->status_code) throw new Exception($response->body);
			
			$response = json_decode($response->body, true);
		}
		catch(Exception $e)
		{
			log_message('info', $e->getMessage());
			parent::responseHandler([], $e->getMessage(), "error", 500);
		}

		parent::responseHandler($response['data'] ?? [], 'ok', 'success', $response['code'] ?? 200);
	}

	public function sync_by_account_id_put_backup($fbAccountId = 0)
	{
		$fbAccountId = (int) $fbAccountId;
		if(empty($fbAccountId)) parent::response(['code' => 404, 'error' => 'Tài khoản Fb không tồn tại']);

		$this->load->model('facebookads/fbaccount_m');
		$fbAccount = $this->fbaccount_m->set_type()->get($fbAccountId);
		if( ! $fbAccount) parent::response(['code' => 404, 'error' => 'Tài khoản Fb không tồn tại [1]']);

		$this->load->config('external_service');
		$api = $this->config->item('adWarehouse', 'api');

		try
		{
			$headers	= [ 'Authorization' => 'Bearer ' . $this->admin_m->generateKey(47) ];
			$response	= Requests::get("{$api}/facebookads/accounts/{$fbAccountId}/adaccounts/fetch", $headers, ['timeout' => 180,'connect_timeout' => 180]);

			if(200 != $response->status_code) throw new Exception($response->body);
			
			$response = json_decode($response->body);
			// if(200 != $response->code) continue;
			update_term_meta($contract->term_id, 'next_time_get_insight', strtotime('+2 hours'));
		}
		catch(Exception $e)
		{
			
			dd($e->getMessage());
			log_message('info', $e->getMessage());
		}

		dd();

		// public static function put($url, $headers = array(), $data = array(), $options = array()) {
		// 	return self::request($url, $headers, $data, self::PUT, $options);
		// }

		$access_token = get_user_meta_value($fbAccount->user_id, 'access_token');

		$this->config->load('facebookads/fbapp');
		$fbApp 	= $this->config->item('production', 'fbapps');
		$args 	= array(
			'app_id' 		=> $fbApp['credential']['app_id'] ?? null,
			'app_secret' 	=> $fbApp['credential']['app_secret'] ?? null,
			'graph_version' => 'v12.0',
			'access_token'  => $access_token
		);

		$this->load->library('form_validation');
		$this->form_validation->set_data($args);
		$this->form_validation->set_rules('access_token', 'access_token', 'required|min[60]');
		$this->form_validation->set_rules('app_id', 'app_id', 'required');
		$this->form_validation->set_rules('app_secret', 'app_secret', 'required');
		$this->form_validation->set_rules('graph_version', 'graph_version', 'required');

		if(FALSE == $this->form_validation->run())
		{
			parent::response([
				'code' => 400,
				'error' => $this->form_validation->error_array()
			]);
		}

		$fb = new Facebook\Facebook([
			'app_id' => $args['app_id'],
			'app_secret' => $args['app_secret'],
			'default_graph_version' => $args['graph_version'],
		]);

		$data = array('adAccounts' => []);

		$adAccounts = [];
		$cache_key 	= "modules/facebookads/{$fbAccount->user_id}-fbadaccounts";
		$adAccounts = $this->scache->get($cache_key);
		if( ! $adAccounts)
		{
			try
	        {
				$adAccounts 		= [];
				$fields 			= [ 'name', 'account_status', 'created_time', 'currency'];
	        	$query  			= http_build_query(['fields' => implode(',', $fields), 'limit' => 500]);
				$endpoint			= "/me/adaccounts?{$query}";
				
				$nextPageRequest 	= true;
				while( ! empty($nextPageRequest))
				{
					$response 	= $fb->get($endpoint, $args['access_token']);
					$graphEdge 	= $response->getGraphEdge();
					$graphEdge->map(function($node) use (&$adAccounts){
						$adAccounts[] = $node->asArray();
						return $node;
					});

					$nextPageRequest = $graphEdge->getNextPageRequest();
					$nextPageRequest AND $endpoint = $nextPageRequest->getEndpoint();
				}

				$adAccounts AND $this->scache->write($adAccounts, $cache_key, 5*60);
	        }
	        catch(Facebook\Exceptions\FacebookResponseException $e)
	        {
	        	parent::response([ 'code' => 400, 'error' => 'Graph returned an error: ' . $e->getMessage() ]);
	        }
	        catch(Facebook\Exceptions\FacebookSDKException $e)
	        {
	            parent::response([ 'code' => 400, 'error' => 'Facebook SDK returned an error: ' . $e->getMessage() ]);
	        }
		}

		if(empty($adAccounts)) parent::response(array( 'code' => 200, 'data' => ['total' => 0, 'adAccounts' => []]));

		$adAccountsInDB = $this->adaccount_m
		->select('term_id, term_type, term_slug, term_name, term_description')
		->set_term_type()
		->where_in('term_slug', array_unique(array_column($adAccounts, 'id')))
		->as_array()
		->get_all();

		$adAccountsInDB AND $adAccountsInDB = array_column($adAccountsInDB, null , 'term_slug');

		$this->load->model('facebookads/fbbusiness_m');
        $unSpecificBM = $this->fbbusiness_m
        ->set_term_type()
        ->select('term_id')
        ->where_in('term_slug', 0)
        ->get_by();

        if(empty($unSpecificBM))
        {
        	$_insertBusiness = array(
                'term_type' 		=> $this->fbbusiness_m->term_type,
                'term_slug' 		=> 0,
                'term_name' 		=> 'UNSPEFICED'
            );

    		$_insertBusinessId 				= $this->fbbusiness_m->insert($_insertBusiness);
    		$_insertBusiness['term_id'] 	= (int) $_insertBusinessId;
    		$unSpecificBM 					= (object) $_insertBusiness;
        }
        
		foreach ($adAccounts as $adAccount)
		{
			if(empty($adAccountsInDB[$adAccount['id']]))
			{
				$insert_id = $this->adaccount_m->insert([
					'term_type' 	=> $this->adaccount_m->term_type,
					'term_slug' 	=> $adAccount['id'],
					'term_name' 	=> $adAccount['name'],
					'term_parent' 	=> $unSpecificBM->term_id,
					'term_status' 	=> 'unauthorized'
				]);

				$this->term_users_m->set_relations_by_term($insert_id, [ $fbAccount->user_id ], $this->fbaccount_m->type);

    			$data['adAccounts'][] = $adAccount;
    			continue;
			}

			$_adAccountInDb = $adAccountsInDB[$adAccount['id']];

			if($_adAccountInDb['term_slug'] != $adAccount['id'] || $_adAccountInDb['term_name'] != $adAccount['name'])
			{
				$this->adaccount_m->update($_adAccountInDb['term_id'], array(
					'term_slug' => $adAccount['id'],
					'term_name' => $adAccount['name']
				));
			}

			$this->term_users_m->set_relations_by_term($_adAccountInDb['term_id'], [ $fbAccount->user_id ], $this->fbaccount_m->type);
		}
	    parent::response(array(
	    	'code' => 200,
	    	'data' => array(
	    		'total' => count($data['adAccounts']),
	    		'adAccounts' => $data['adAccounts']
	    	)
	    ));
	}
}
/* End of file Adaccount.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/Adaccount.php */