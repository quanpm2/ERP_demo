<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'facebookads/facebookads_m';
        $this->autoload['models'][] = 'facebookads/adaccount_m';
        $this->autoload['models'][] = 'ads_segment_m';
        parent::__construct($config);
    }

    public function accounts_get($contractId = null)
    {
        $contract   = (new facebookads_m())->set_contract($contractId);

        $insight        = [];
        $adsSegments    = $this->term_posts_m->get_term_posts($contractId, $this->ads_segment_m->post_type);
        if( ! empty($adsSegments))
        {
            foreach ($adsSegments as $adsSegment)
            {
                $adaccount = $this->term_posts_m->get_post_terms($adsSegment->post_id, $this->adaccount_m->term_type);
                if(empty($adaccount)) continue;

                $adaccount  = reset($adaccount);
                $_iSegments = $this->adaccount_m->get_data($adaccount->term_id, $adsSegment->start_date, $adsSegment->end_date);
                $insight    = array_merge($_iSegments, $insight);
            }
        }

        if($insight)
        {
            $insight = array_map(function($x){ 
                $x['result']    = (int) ($x['result'] ?? 0);
                $x['reach']     = (int) ($x['reach']  ?? 0);
                $x['spend']     = (int) ($x['spend']  ?? 0);
                return $x;
            }, $insight);
        }

        usort($insight, function($a, $b) { 
            if ($a['time'] == $b['time']) return 0;
            return ($a['time'] < $b['time']) ? -1 : 1;
        });

        parent::response([ 'status' => true, 'data' => $insight ]);
    }

    public function account_cost_get($contractId = null)
    {
        $contract   = (new facebookads_m())->set_contract($contractId);
        $accounts   = $contract->get_accounts(parent::get(NULL, TRUE));
        
        if(empty($accounts)) parent::response(['status' => true, 'data' => 0]);

        $ins    = reset($accounts);
        $cost   = array_sum(array_column($accounts, 'spend'));

        $exchange_rate = get_exchange_rate($ins['account_currency'], $contractId);
        $exchange_rate AND $cost *= $exchange_rate;

        parent::response([ 'status' => true, 'data' => $cost ]);
    }

    /**
     * GET All Contracts Realted by Segments
     *
     * @param      int   $id     The identifier
     */
    public function contracts_related_get(int $id)
    {
        if(FALSE == $this->facebookads_m->set_contract($id)) parent::response(['code' =>  parent::HTTP_BAD_REQUEST, 'error' => parent::HTTP_BAD_REQUEST ]);

        $contract = $this->facebookads_m->get_contract();

        $segments       = $this->facebookads_m->getSegments();
        $adaccountIds   = array_column($segments, 'adaccount_id');

        if(empty($adaccountIds))
        {
            parent::response(['code' => parent::HTTP_OK, 'total' => 0, 'data' => []]);    
        }

        $allSegments = $this->ads_segment_m->set_post_type()
        ->select('posts.post_id, start_date, end_date, term_posts.term_id as adaccount_id')
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->where_in('term_posts.term_id', array_unique($adaccountIds))
        ->group_by('posts.post_id')
        ->get_all();

        if(empty($allSegments)) parent::response(['code' => parent::HTTP_OK, 'total' => 0, 'data' => []]);


        $relatedContracts = $this->facebookads_m->set_term_type()
        ->select('term.term_id, term_name, term_type, term_parent, term_status, object_id, post_id as segment_id')
        ->join($this->term_posts_m->_table, "{$this->term_posts_m->_table}.term_id = term.term_id")
        ->where_in("{$this->term_posts_m->_table}.post_id", array_map('intval', array_unique(array_filter(array_column($allSegments, 'post_id')))))
        ->where('term.term_id <>', $id)
        ->get_all();

        if(empty($relatedContracts)) parent::response(['code' => parent::HTTP_OK, 'total' => 0, 'data' => []]);

        $relatedContracts AND $relatedContracts = array_group_by($relatedContracts, 'term_id');
        $allSegments AND $allSegments = array_column($allSegments, null, 'post_id');

        $contracts = array_map(function($items) use($allSegments){

            $_contract = reset($items);

            $this->load->model('staffs/admin_m');
            $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
            if(!empty($manipulation_locked['manipulation_locked_by']))
            {
                $manipulation_locked['manipulation_locked_by_name'] = $this->admin_m->get_field_by_id($manipulation_locked['manipulation_locked_by'], 'display_name');
            }

            if(!empty($manipulation_locked['manipulation_unlocked_by']))
            {
                $manipulation_locked['manipulation_unlocked_by_name'] = $this->admin_m->get_field_by_id($manipulation_locked['manipulation_unlocked_by'], 'display_name');
            }
            $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                    && TRUE == (bool)get_term_meta_value($_contract->term_id, 'is_manipulation_locked');
            $manipulation_locked['is_manipulation_locked'] = $is_manipulation_locked;

            return array(
                'term_id' => (int) $_contract->term_id,
                'term_name' => $_contract->term_name,
                'contract_code' => get_term_meta_value($_contract->term_id, 'contract_code') ?: $_contract->term_id,
                'term_type' => $_contract->term_type,
                'term_parent' => (int) $_contract->term_parent,
                'term_status' => $_contract->term_status,
                'advertise_start_time' => (int) get_term_meta_value($_contract->term_id, 'advertise_start_time'),
                'advertise_end_time' => (int) get_term_meta_value($_contract->term_id, 'advertise_end_time'),
                'segments' => array_map(function($item) use($allSegments){
                    return array_map('intval', (array) $allSegments[$item->segment_id]);
                }, $items),
                'manipulation_locked' => $manipulation_locked
            );
        }, $relatedContracts);

        parent::response([
            'code' => parent::HTTP_OK,
            'total' => count($contracts),
            'data' => array_values($contracts)
        ]);
    }

    public function segments_get(int $id)
    {
        $segment = $this->ads_segment_m
        ->select('post_id, start_date, end_date, post_status, post_type')
        ->set_post_type()
        ->get($id);

        if( ! $segment)
        {
            parent::response(['code' => parent::HTTP_BAD_REQUEST, 'error' => parent::HTTP_BAD_REQUEST]);
        }
        
        $adaccount = $this->term_posts_m->get_post_terms($segment->post_id, $this->adaccount_m->term_type);
        $adaccount AND $adaccount = reset($adaccount);

        $_iSegments = $this->adaccount_m->get_data($adaccount->term_id, $segment->start_date, $segment->end_date ?: time());

        parent::response([ 'code' => 200, 'data' => array(
            'post_id'       => (int) $segment->post_id,
            'start_date'    => (int) $segment->start_date,
            'end_date'      => (int) $segment->end_date,
            'adaccount_id'  => (int) $adaccount->term_id,
            'adaccount' => array(
                'id'    => (int) $adaccount->term_id,
                'account_name'  => get_term_meta_value($adaccount->term_id, 'account_name'),
                'customer_id'   => get_term_meta_value($adaccount->term_id, 'customer_id'),
                'currency_code' => get_term_meta_value($adaccount->term_id, 'currency_code'),
            ),
            'insight' => $_iSegments
        )]);
    }

    public function adaccount_get(int $id)
    {
        $segment = $this->ads_segment_m
        ->select('post_id, start_date, end_date, post_status, post_type')
        ->set_post_type()
        ->get($id);

        if( ! $segment)
        {
            parent::response(['code' => parent::HTTP_BAD_REQUEST, 'error' => parent::HTTP_BAD_REQUEST]);
        }

        $this->load->model('googleads/mcm_account_m');
        $this->load->model('googleads/base_adwords_m');
        
        $adaccount = $this->term_posts_m->get_post_terms($segment->post_id, $this->mcm_account_m->term_type);
        $adaccount AND $adaccount = reset($adaccount);

        $_iSegments = $this->adaccount_m->get_data($adaccount->term_id, $segment->start_date, $segment->end_date ?: time());

        parent::response([ 'code' => 200, 'data' => array(
            'post_id'       => (int) $segment->post_id,
            'start_date'    => (int) $segment->start_date,
            'end_date'      => (int) $segment->end_date,
            'adaccount_id'  => (int) $adaccount->term_id,
            'adaccount' => array(
                'id'    => (int) $adaccount->term_id,
                'account_name'  => get_term_meta_value($adaccount->term_id, 'account_name'),
                'customer_id'   => get_term_meta_value($adaccount->term_id, 'customer_id'),
                'currency_code' => get_term_meta_value($adaccount->term_id, 'currency_code'),
            ),
            'insight' => $_iSegments
        )]);
    }

    public function adaccount_cost_get($adaccount_id)
    {
        $args = wp_parse_args( array_filter(parent::get(NULL, TRUE)), [
            'end_time' => time(),
            'ignore_cache' => false,
        ]);


        $_iSegments = $this->adaccount_m->get_data($adaccount_id, $args['start_time'], $args['end_time']);
        if(empty($_iSegments)) parent::response(['code' => 200, 'data' => 0]);

        $sum_spend = array_sum(array_column($_iSegments, 'spend'));

        parent::response([
            'code' => 200,
            'data' => $sum_spend
        ]);
    }

    /**
     * Get & Sync Facebook Insight data of Contract
     */
    public function sync_put($contractId)
    {
        $args = wp_parse_args(parent::put(null, true), [
            'start_time' => start_of_day(strtotime("-7 days")),
            'end_time' => end_of_day()
        ]);

        if( ! $this->facebookads_m->set_contract($contractId))
        {
            parent::response(['code' => parent::HTTP_BAD_REQUEST]);
        }

        $adaccounts = get_term_meta($contractId, 'adaccounts', FALSE, TRUE);
        $adaccounts AND $adaccounts = array_map('intval', $adaccounts);
        $segments   = $this->term_posts_m->get_term_posts($contractId, $this->ads_segment_m->post_type);

        if(empty($adaccounts) || empty($segments))
        {
            parent::response([
                'code' => parent::HTTP_BAD_REQUEST,
                'error' => 'Invalid Adaccounts Conditions [0]'
            ]);
        }

        $next_time_get_insight = get_term_meta_value($contractId, 'next_time_get_insight');
        if($next_time_get_insight > time() && 1==2)
        {
            parent::response([
                'code' => parent::HTTP_LOCKED,
                'error' => 'TEMPORARY LOCKED'
            ]);
        }

        foreach ($segments as $segment)
        {
            $_segment_adaccount = $this->term_posts_m->get_post_terms( $segment->post_id, $this->adaccount_m->term_type);
            $_segment_adaccount AND $_segment_adaccount = reset($_segment_adaccount);
            
            $this->adaccount_m->get_insight($_segment_adaccount->term_id, $segment->start_date, $segment->end_date);
        }

        parent::response([
            'code' => 200,
            'message' => 'ok'
        ]);
    }


    /**
     * Get & Sync Facebook Insight data of Contract
     */
    public function sync_progress_n_alert_put($contractId)
    {
        $args = array_merge([
            'start_time' => start_of_day(strtotime("-7 days")),
            'end_time' => end_of_day()
        ], parent::put(null, TRUE));

        if( ! $this->facebookads_m->set_contract($contractId))
        {
            parent::response(['code' => parent::HTTP_BAD_REQUEST]);
        }

        $adaccounts = get_term_meta($contractId, 'adaccounts', FALSE, TRUE);
        $adaccounts AND $adaccounts = array_map('intval', $adaccounts);
        $segments   = $this->term_posts_m->get_term_posts($contractId, $this->ads_segment_m->post_type);

        if(empty($adaccounts) || empty($segments))
        {
            parent::response([
                'code' => parent::HTTP_BAD_REQUEST,
                'error' => 'Invalid Adaccounts Conditions [0]'
            ]);
        }

        $next_time_get_insight = get_term_meta_value($contractId, 'next_time_get_insight');
        if($next_time_get_insight > time() && 1==2)
        {
            parent::response([
                'code' => parent::HTTP_LOCKED,
                'error' => 'TEMPORARY LOCKED'
            ]);
        }

        foreach ($segments as $segment)
        {
            $_segment_adaccount = $this->term_posts_m->get_post_terms( $segment->post_id, $this->adaccount_m->term_type);
            $_segment_adaccount AND $_segment_adaccount = reset($_segment_adaccount);
            $this->adaccount_m->get_insight($_segment_adaccount->term_id, $args['start_time'], $args['end_time']);
        }

        parent::response([
            'code' => 200,
            'message' => 'ok'
        ]);
    }
}
/* End of file MCCReport.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/MCCReport.php */