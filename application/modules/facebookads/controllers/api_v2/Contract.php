<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use AdsService\AdsInsight\FacebookadsInsight;

class Contract extends MREST_Controller
{
	function __construct()
	{
        $this->autoload['models'][] 	= 'facebookads/facebookads_m';
        $this->autoload['models'][] 	= 'facebookads/adaccount_m';
        $this->autoload['models'][] 	= 'facebookads/facebookads_kpi_m';
        $this->autoload['helpers'][] 	= 'date';

		parent::__construct();
	}

	/**
	 * @param int $id Id hợp đồng FB
	 * 
	 * @return [type]
	 */
	public function overview_get(int $id)
	{
		$data = $this->data;

		if( false == $this->facebookads_m->set_contract($id)) return parent::responseHandler([], 'Hợp đồng không tồn tại hoặc đã bị xóa.', 'error', 404);
		if( ! $this->facebookads_m->can('facebookads.overview.access')) return parent::responseHandler([], 'Không có quyền truy cập.', 'error', 401);

		$contract = $this->facebookads_m->get_contract();

		$saleId = (int) get_term_meta_value($id, 'staff_business');
		$sale 	= $this->admin_m->get_field_by_id($saleId);


		$segments = $this->facebookads_m->getSegments();

		$kpis = $this->facebookads_kpi_m->order_by('kpi_type')->get_many_by([ 'term_id' => $id]);
		$kpis AND $kpis = array_map(function($kpi) {
			$kpi->display_name = $this->admin_m->get_field_by_id($kpi->user_id, 'display_name');
			return $kpi;
		}, $kpis);
		
		$customer = (array) $this->facebookads_m->getCustomer();
		$customer = elements([ 'user_id', 'cid', 'display_name' ], $customer);
		
		$data = array(
			'term_id'				=> $id,
			'term'					=> $contract,
			'kpis'					=> $kpis,
			'sale'					=> $sale,
			'customer'				=> $customer,
			'adaccount_status'		=> get_term_meta_value($id, 'adaccount_status'),
			'contract_code'			=> get_term_meta_value($id, 'contract_code'),
			'representative_name'	=> get_term_meta_value($id, 'representative_name'),
			'representative_email'		=> get_term_meta_value($id, 'representative_email'),
			'representative_address'	=> get_term_meta_value($id, 'representative_address'), 
		);

        $exchange_rates = [];
        $this->load->model('option_m');
        $exchange_rate_option = $this->option_m->get_value('exchange_rate', TRUE);
        foreach($exchange_rate_option as $currency => $exchange_rate){
            $meta_key = 'exchange_rate_' . strtolower($currency);

            $meta_value = (double) get_term_meta_value($id, $meta_key) ?? 0;
            if(!empty($meta_value)) $exchange_rates[$meta_key] = $meta_value;
        }
        $data['exchange_rates'] = $exchange_rates;

		$start_service_time 	= (int) get_term_meta_value($id,'start_service_time');
		$end_service_time 		= (int) get_term_meta_value($id,'end_service_time');
		$advertise_start_time	= (int) get_term_meta_value($id,'advertise_start_time');
		$advertise_end_time		= (int) get_term_meta_value($id,'advertise_end_time');
		$vat 					= div((double) get_term_meta_value($id,'vat'),100);
		$contract_budget 		= (double) get_term_meta_value($id,'contract_budget');
		$service_fee 			= (double) get_term_meta_value($id,'service_fee');
		$payment_amount 		= (double) get_term_meta_value($id,'payment_amount');
        $result_updated_on      = get_term_meta_value($id, 'result_updated_on');
		$time_next_api_check    = get_term_meta_value($id, 'time_next_api_check');

		if(!empty($vat)) $service_fee+= $service_fee*$vat;
		
		$actual_budget 				= (int) get_term_meta_value($id, 'actual_budget');
		$balanceBudgetReceived 		= (int) get_term_meta_value($id, 'balanceBudgetReceived');
        $balanceBudgetAddTo         = (int) get_term_meta_value($id, 'balanceBudgetAddTo');

        $balance_spend 				= (double) get_term_meta_value($id, 'balance_spend');

		$payment_percentage 		= 100 * (double) get_term_meta_value($id,'payment_percentage');

		$data['actual_budget']				= $actual_budget;
		$data['total_actual_budget']		= $actual_budget;
		$data['balanceBudgetReceived']		= $balanceBudgetReceived;
		$data['balanceBudgetAddTo']		    = $balanceBudgetAddTo;
		$data['balance_spend']		        = $balance_spend;
		$data['contract_budget']			= $contract_budget;
		$data['payment_amount']				= $payment_amount;
		$data['payment_percentage']			= $payment_percentage;
		$data['start_service_time']			= $start_service_time;
		$data['end_service_time']			= $end_service_time;
		$data['advertise_start_time']		= $advertise_start_time;
		$data['advertise_end_time']			= $advertise_end_time;
        $data['result_updated_on']          = $result_updated_on ? date('H:i:s d/m/Y', $result_updated_on) : '--';
        $data['time_next_api_check']        = $time_next_api_check ? date('H:i:s d/m/Y', $time_next_api_check) : '--';

		$amount_spend = 0;
		$start_service_time = start_of_day($start_service_time);
		$end_service_time 	= end_of_day(($end_service_time ?: time()));

		$insight 		= array();
		$adsSegments 	= $this->term_posts_m->get_term_posts($id, $this->ads_segment_m->post_type);
		if( ! empty($adsSegments))
		{
			foreach ($adsSegments as &$adsSegment)
			{
				$adaccount = $this->term_posts_m->get_post_terms($adsSegment->post_id, $this->adaccount_m->term_type);
				if(empty($adaccount)) continue;
				
				$adaccount 	= reset($adaccount);
				$_iSegments = $this->adaccount_m->get_data($adaccount->term_id, $adsSegment->start_date, end_of_day( (int) $adsSegment->end_date));
				$insight 	= array_merge($_iSegments, $insight);
				
				$adsSegment->adaccount 		= $adaccount;
				$adsSegment->adaccount_id 	= $adaccount->term_id;
				$adsSegment->insights 		= $_iSegments;
			}
		}

		if($insight)
		{
			$insight = array_group_by($insight, 'start_date');
			$insight = array_map(function($x){
				$_instance = reset($x);
				return [
					'result'		=> array_sum(array_column($x, 'result')),
					'reach'			=> array_sum(array_column($x, 'reach')),
					'spend'			=> array_sum(array_column($x, 'spend')),
                    'impressions'   => array_sum(array_column($x, 'impressions')),
					'date_start' 	=> $_instance['date_start']
				];
			}, $insight);
		}

		ksort($insight, SORT_NUMERIC);

		$data['chart_data'] = [
			'axis_categories' 	=> array_column($insight, 'date_start'),
			'result' 			=> array_column($insight, 'result'),
			'reach' 			=> array_column($insight, 'reach'),
			'spend' 			=> array_column($insight, 'spend'),
            'impressions'       => array_column($insight, 'impressions'),
			'cost_per_result' 	=> div(array_sum(array_column($insight, 'spend')), array_sum(array_column($insight, 'result')))
		];


		$data['reach']			= array_sum(array_column($insight, 'reach'));
		$data['result'] 		= array_sum(array_column($insight, 'result'));
		$data['impressions']	= array_sum(array_column($insight, 'impressions'));

		$actual_result                      = (double) get_term_meta_value($id, 'actual_result');
		$data['amount_spend']               = $actual_result + $data['balance_spend'];
		$data['amount_spend_percentage']	= div($data['amount_spend'], $actual_budget) * 100;
		

		$data['adAccountsInfomation'] = array();
		if( ! empty($adsSegments))
		{
			$rows = [];
			foreach ($adsSegments as $i => $segment)
			{
                if(empty($segment->insights)) continue;
                
				$spend = array_sum(array_column($segment->insights, 'spend'));

				$adaccount = $segment->adaccount;
				
				$row = [
					'id' => $adaccount->term_slug ?? null,
					'name' => $adaccount->term_name ?? null,
					'start' => my_date($segment->start_date, 'd/m/Y'),
					'end' => empty($segment->end_date) ? 'Hiện tại' : my_date($segment->end_date, 'd/m/Y'),
					'cost' => $spend
				];

				$data['adAccountsInfomation'][] = $row;
			}
		}

		parent::responseHandler($data, 'ok');
	}

	/**
	 * /PUT API-V2/FACEBOOKADS/CONTRACT/ADACCOUNT_INSIGHT/:ID
	 *
	 * @param      int    $term_id  The term identifier
	 *
	 * @return     Json  Result
	 */
	public function adaccount_insight_put_old(int $term_id = 0)
	{
		$contract 	= (new facebookads_m())->set_contract($term_id);
		if( ! $contract)
		{
            return parent::responseHandler([], 'Không tìm thấy dữ liệu của hợp đồng', 'success', 400);
		}

		try
		{
			$contract->update_insights();
			$behaviour_m 	= $contract->get_behaviour_m();
			$progress 		= $behaviour_m->get_the_progress();
			$behaviour_m->sync_all_amount();

			update_term_meta($term_id, 'time_next_api_check', strtotime('+30 minutes'));
		}
		catch (Exception $e)
		{
			$result = $e->getMessage();
			return parent::responseHandler([], $result, 'error', 400);
		}

        $description = 'Đã đồng bộ lúc ' . date('h:i:s d/m/Y') . '. Kỳ đồng bộ tiếp theo lúc ' . date('h:i:s d/m/Y', get_term_meta_value($term_id, 'time_next_api_check'));
        audit('sync_spend', 'term', $term_id, '', $description, '', $this->admin_m->id, 'erp');

		return parent::responseHandler([], 'Dữ liệu đã được đồng bộ thành công. Vui lòng (F5) để xem sự thay đổi', 'success', 200);
	}

    public function adaccount_insight_put(int $term_id = 0)
	{
		$contract 	= (new facebookads_m())->set_contract($term_id);
		if( ! $contract)
		{
            return parent::responseHandler([], 'Không tìm thấy dữ liệu của hợp đồng', 'success', 400);
		}

        $status = (new FacebookadsInsight())->syncInsight($term_id);
        if(!$status){
            return parent::responseHandler([], 'Tiến trình đồng bộ gặp sự cố. Vui lòng liên hệ quản trị viên để được hỗ trợ', 'failed', 400);
        }

		return parent::responseHandler([], 'Dữ liệu đã được đồng bộ thành công. Vui lòng chờ trong giây lát.', 'success', 200);
	}

	/**
	 * ĐỒNG BỘ TOÀN BỘ SỐ LIỆU THU CHI CỦA HỢP ĐỒNG FACEBOOKADS
	 * DỰA TRÊN IDS HỢP ĐỐNG ĐƯỢC YÊU CẦU
	 *
	 * @return     json
	 */
	public function sync_spend_put()
	{
		$ids 	= parent::put('ids', TRUE);
		if(empty($ids)) return parent::response([ 'code' => REST_Controller::HTTP_BAD_REQUEST, 'message' => 'HTTP_BAD_REQUEST']);

		is_array($ids) OR $ids = [$ids];
		$ids = array_unique(array_filter(array_map('intval', $ids)));
		
		$contracts = $this->facebookads_m->set_term_type()->where_in('term_id', $ids)->get_all();

		if(empty($contracts)) return parent::response([ 'code' => REST_Controller::HTTP_NO_CONTENT, 'message' => 'HTTP_NO_CONTENT']);

		$result = [ 'total' => count($contracts), 'completed' => 0, 'failed' => 0 ];

		foreach ($contracts as $contract)
		{
			$contract = (new facebookads_m())->set_contract($contract);
    		try
			{
				$behaviour_m = $contract->get_behaviour_m();
    			$behaviour_m->get_the_progress();
				$behaviour_m->sync_all_progress();
				$result['completed']++;
			}
			catch (Exception $e)
			{
				$result['failed']++;
				continue;
			}
		}

		return parent::response([ 'code' => REST_Controller::HTTP_OK, 'data' => $result]);
	}
}
/* End of file Adaccount.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/Adaccount.php */
