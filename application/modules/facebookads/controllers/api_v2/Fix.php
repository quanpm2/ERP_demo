<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Fix extends MREST_Controller
{
    function __construct()
    {
        $this->autoload['models'][]     = 'facebookads/facebookads_m';

        parent::__construct();
    }

    /**
     *
     * @param      int    $term_id  The term identifier
     *
     * @return     Json  Result
     */
    public function export_facebook_account_get()
    {
        $title = "hop_dong_fb_co_tai_khoan_unlink";
        $account_status = 'unlinked';

        $data = $this->facebookads_m
            ->select("term.term_id AS contract_id")
            ->select("MAX(IF(m_contract.meta_key = 'contract_code', m_contract.meta_value, NULL)) as contract_code")
            ->select("adaccount.term_id as adaccount_db_id, adaccount.term_slug as adaccount_id, adaccount.term_name as adaccount_name")
            ->select("audits.created_at AS unlink_date")
            
            ->join('term_posts', 'term_posts.term_id = term.term_id')
            ->join('posts AS ads_segment', "ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = 'ads_segment'")
            ->join('term_posts AS tp_adaccount', "tp_adaccount.post_id = ads_segment.post_id")
            ->join('term AS adaccount', "adaccount.term_id = tp_adaccount.term_id AND adaccount.term_type = 'adaccount'")
            ->join('termmeta AS m_contract', "m_contract.term_id = term.term_id AND m_contract.meta_key = 'contract_code'", 'LEFT')
            ->join('audits', "audits.auditable_id = term.term_id and audits.auditable_type = 'term' and audits.auditable_field = 'term_status' and audits.event = 'updated'", 'LEFT')

            ->where('term.term_type', 'facebook-ads')
            ->where('adaccount.term_status', $account_status)

            ->group_by('term.term_id')

            ->as_array()
            ->get_all();
        if(empty($data)){
            return parent::responseHandler([], 'Không có dữ liệu');
        }

        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));
        $sheet = $spreadsheet->getActiveSheet();

        $columns = $this->getColumns('export');
        $sheet->fromArray(array_column(array_values($columns), 'label'), NULL, 'A1');

        $rowIndex = 2;

        foreach ($data as $item) {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column) {
                switch ($column['type']) {
                    case 'number':
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'decimal':
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("0.00");
                        break;

                    case 'timestamp':
                        $item[$key] = Date::PHPToExcel($item[$key]);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default:
                        break;
                }

                @$sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item[$key]);
                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/tmp/';
        if (!is_dir($folder_upload)) {
            try {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}{$title}.xlsx";

        try {
            (new Xlsx($spreadsheet))->save($fileName);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        
        return parent::responseHandler([], 'File đang được tải về');
    }

    protected function getColumns()
    {
        $config = array(
            'contract_id' => [
                'type' => 'string',
                'label' => 'Id hợp đồng'
            ],
            'contract_code' => [
                'type' => 'string',
                'label' => 'Mã hợp đồng'
            ],
            'adaccount_db_id' => [
                'type' => 'string',
                'label' => 'Id tài khoản hệ thống'
            ],
            'adaccount_id' => [
                'type' => 'string',
                'label' => 'Id tài khoản facebook'
            ],
            'adaccount_name' => [
                'type' => 'string',
                'label' => 'Tên tài khoản'
            ],
            'unlink_date' => [
                'type' => 'string',
                'label' => 'Ngày ngắt kết nối'
            ],
        );

        return $config;
    }
}
/* End of file Fix.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/Fix.php */