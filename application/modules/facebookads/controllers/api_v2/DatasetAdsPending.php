<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class DatasetAdsPending extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'facebookads/facebookads_m';
        $this->autoload['models'][] = 'facebookads/adaccount_m';
        $this->autoload['models'][] = 'facebookads/facebookads_kpi_m';
        $this->autoload['models'][] = 'ads_segment_m';
        parent::__construct($config);

        $this->load->config('facebookads/facebookads');
        $this->load->config('facebookads/contract');
    }


    /**
     * Render dataset of FB's services
     */
    public function index_get()
    {
        $response   = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        $permission = 'facebookads.index.access';

        if (!has_permission($permission)) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        $relate_users       = $this->admin_m->get_all_by_permissions($permission);
        if ($relate_users === FALSE) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        $this->load->library('datatable_builder');

        if (is_array($relate_users)) {
            $this->datatable_builder->join('term_users', 'term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
        }

        $default = array(
            'offset'        => 0,
            'per_page'      => 10,
            'cur_page'      => 1,
            'is_filtering'  => TRUE,
            'is_ordering'   => TRUE,
            'filter_position'   => FILTER_TOP_OUTTER,
        );

        $args               = wp_parse_args($this->input->get(), $default);

        $this->search_filter();

        $columns = $this->getColumns();
        foreach($columns AS $key => $value) $this->datatable_builder->add_column($key, $value);

        $callbacks = $this->getCallbacks(array_keys($columns));
        foreach($callbacks AS $callback) $this->datatable_builder->add_column_callback($callback['field'], $callback['func'], $callback['row_data']);

        $typeOfServices = $this->config->item('items', 'typeOfServices');
        $data =  $this->datatable_builder
            ->setOutputFormat('JSON')

            ->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])
            ->add_search('customer_code', ['placeholder' => 'Mã khách hàng'])
            ->add_search('customer_name', ['placeholder' => 'Khách hàng'])
            ->add_search('type_of_service', ['content' => form_dropdown(['name' => 'where[type_of_service]', 'class' => 'form-control select2', 'placeholder' => 'Loại hình dịch vụ'], prepare_dropdown($typeOfServices, 'Loại hình dịch vụ'), $this->input->get('where[type_of_service]'))])
            ->add_search('contract_value', ['placeholder' => 'Giá trị hợp đồng'])
            ->add_search('start_service_time', ['placeholder' => 'Ngày bắt đầu dịch vụ', 'class' => 'form-control set-datepicker'])
            ->add_search('end_service_time', ['placeholder' => 'Ngày kết thúc dịch vụ', 'class' => 'form-control set-datepicker'])
            ->add_search('result_updated_on', ['placeholder' => 'Ngày cập nhật', 'class' => 'form-control set-datepicker'])
            ->add_search('staff_business', ['placeholder' => 'Nhân viên kinh doanh'])
            ->add_search('fb_staff_advertise', ['placeholder' => 'Nhân viên kỹ thuật'])

            ->from('term')

            ->select("term.term_id AS contractId")
            ->select("SUM(spend.meta_value) AS spend")

            ->join('term_posts AS tp_ads_segment', 'tp_ads_segment.term_id = term.term_id')
            ->join('posts AS ads_segment', 'ads_segment.post_id = tp_ads_segment.post_id and ads_segment.post_type = "ads_segment"', 'LEFT')
            ->join('term_posts AS tp_segment_mcm_account', 'tp_segment_mcm_account.post_id = ads_segment.post_id', 'LEFT')
            ->join('term AS mcm_account', "tp_segment_mcm_account.term_id = mcm_account.term_id and mcm_account.term_type = '{$this->adaccount_m->term_type}'", 'LEFT')
            ->join('term_posts AS tp_mcm_account_insights', 'tp_mcm_account_insights.term_id = mcm_account.term_id', 'LEFT')
            ->join('posts AS insights', 'tp_mcm_account_insights.post_id = insights.post_id AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) AND insights.start_date <= IF(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) AND insights.post_type = "insight_segment" AND insights.post_name = "day"', 'LEFT')
            ->join('postmeta AS spend', 'spend.post_id = insights.post_id AND spend.meta_key = "spend"', 'LEFT')
            ->join('termmeta AS result_updated_on', 'result_updated_on.term_id = term.term_id AND result_updated_on.meta_key = "result_updated_on"', 'LEFT')

            ->where('term.term_type', $this->facebookads_m->term_type)
            ->where_in('term.term_status', array_keys($this->config->item('status', 'facebookads')))
            ->where('result_updated_on.meta_value < UNIX_TIMESTAMP(TIMESTAMPADD(DAY, -1, CURRENT_TIMESTAMP))')
            ->group_by('term.term_id')
            ->generate(array('per_page' => $args['per_page'], 'cur_page' => $args['cur_page']));

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export($last_query);
            return TRUE;
        }

        parent::response($data);
    }

    protected function getColumns($type = 'default')
    {
        $enums = array(
            'contract_code' => [ 'type' => 'string', 'title' => 'Mã hợp đồng', 'set_order' => FALSE, 'set_select' => FALSE ],
            'contract_code_link' => [ 'type' => 'string', 'title' => 'Mã hợp đồng', 'set_order' => FALSE, 'set_select' => FALSE ],
            'contract_code_link_tag'  => [ 'type' => 'string', 'title' => 'Mã hợp đồng', 'set_order' => FALSE, 'set_select' => FALSE ],
            'customer_code' => [ 'type' => 'string', 'title' => 'Customer ID', 'set_order' => FALSE, 'set_select' => FALSE ],
            'customer_name' => [ 'type' => 'string', 'title' => 'Khách hàng', 'set_order' => FALSE, 'set_select' => FALSE ],
            'type_of_service' => [ 'type' => 'string', 'title' => 'Loại hình dịch vụ', 'set_order' => FALSE, 'set_select' => FALSE ],
            'cid' => [ 'type' => 'string', 'title' => 'Mã Khách Hàng', 'set_order' => FALSE, 'set_select' => FALSE ],
            'fb_staff_advertise' => [ 'type' => 'string', 'title' => 'NVKT', 'set_order' => FALSE, 'set_select' => FALSE ],
            'contract_daterange' => [ 'type' => 'string', 'title' => 'T/G HĐ', 'set_order' => FALSE, 'set_select' => FALSE ],
            'contract_value' => [ 'type' => 'number', 'title' => 'Giá trị hợp đồng', 'set_order' => FALSE, 'set_select' => FALSE ],
            'start_service_time' => [ 'type' => 'timestamp', 'title' => 'Ngày bắt đầu', 'set_order' => FALSE, 'set_select' => FALSE ],
            'end_service_time' => [ 'type' => 'timestamp', 'title' => 'Ngày kết thúc', 'set_order' => FALSE, 'set_select' => FALSE ],
            'staff_business' => [ 'type' => 'string', 'title' => 'NVKD', 'set_order' => FALSE, 'set_select' => FALSE ],
            'department' => [ 'type' => 'string', 'title' => 'Phòng', 'set_order' => FALSE, 'set_select' => FALSE ],
            'currency' => [ 'type' => 'string', 'title' => 'Currecy', 'set_order' => FALSE, 'set_select' => FALSE ],
            'actual_budget' => [ 'type' => 'number', 'title' => 'Ngân sách thực tế', 'set_order' => FALSE, 'set_select' => FALSE ],
            'spend' => [ 'type' => 'number', 'title' => 'Ngân sách đã chạy (VNĐ)', 'set_order' => FALSE, 'set_select' => FALSE ],
            'remain_budget' => [ 'type' => 'number', 'title' => 'Ngân sách còn lại', 'set_order' => FALSE, 'set_select' => FALSE ],
            'service_fee' => [ 'type' => 'number', 'title' => 'Phí dịch vụ', 'set_order' => FALSE, 'set_select' => FALSE ],
            'discount_amount' => [ 'type' => 'number', 'title' => 'Giảm giá', 'set_order' => FALSE, 'set_select' => FALSE ],
            'service_fee_rate' => [ 'type' => 'decimal', 'title' => '% Phí dịch vụ', 'set_order' => FALSE, 'set_select' => FALSE ],
            'service_fee_rate_actual' => [ 'type' => 'decimal', 'title' => '% Phí dịch vụ (thực tế)', 'set_order' => FALSE, 'set_select' => FALSE ],
            'result_updated_on' => [ 'type' => 'timestamp', 'title' => 'Ngày cập nhật', 'set_order' => FALSE, 'set_select' => FALSE ],
            'remain_pending_days' => [ 'type' => 'number', 'title' => 'Số ngày tạm ngưng', 'set_order' => FALSE, 'set_select' => FALSE ],
        );

        $config = array(
            'default' => array(
                'contract_code',
                'customer_code',
                'customer_name',
                'type_of_service',
                'contract_value',
                'start_service_time',
                'end_service_time',
                'result_updated_on',
                'remain_pending_days',
                'actual_budget',
                'spend',
                'remain_budget',
                'staff_business',
                'fb_staff_advertise',
            ),

            'export' => array(
                'contract_code',
                'customer_code',
                'customer_name',
                'type_of_service',
                'contract_value',
                'start_service_time',
                'end_service_time',
                'result_updated_on',
                'remain_pending_days',
                'actual_budget',
                'spend',
                'remain_budget',
                'staff_business',
                'fb_staff_advertise',
            )
        );

        return elements($config[$type], $enums);
    }

    protected function getCallbacks($args = [])
    {
        $callbacks = [];

        $callbacks['contract_code'] = [
            'field' => 'contract_code',    
            'func'  => function($data, $row_name){
                $contractId = $data['contractId'];

                $data['contract_code'] = get_term_meta_value($contractId, 'contract_code') ?: '--';

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['customer_code'] = [
            'field' => 'customer_code',    
            'func'  => function($data, $row_name){
                $contractId = $data['contractId'];

                $data['customer_code'] = '--';

                $customers = $this->term_users_m->get_the_users($contractId, ['customer_person','customer_company']);
                if(empty($customers)) return $data;

                $customer = end($customers);
                
                $data['customer_id'] = $customer->user_id;
                $data['customer_code'] = cid($customer->user_id, $customer->user_type);
                $data['customer_name'] = $customer->display_name;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $this->config->load('facebookads/contract');
        $typeOfServices = $this->config->item('items', 'typeOfServices');
        $callbacks['type_of_service'] = [
            'field' => 'type_of_service',    
            'func'  => function($data, $row_name) use ($typeOfServices){
                $contractId = $data['contractId'];

                $data['type_of_service'] = '--';
                $typeOfService = get_term_meta_value($contractId, 'typeOfService');
                if(empty($typeOfService)) return $data;
                
                $typeOfService = $typeOfServices[$typeOfService] ?? '--';
                $data['type_of_service_raw'] = get_term_meta_value($contractId, 'typeOfService') ?: '--';
                $data['type_of_service'] = $typeOfService;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['contract_value'] = [
            'field' => 'contract_value',    
            'func'  => function($data, $row_name){
                $contractId = $data['contractId'];

                $data['contract_value'] = (int)get_term_meta_value($contractId, 'contract_value');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['start_service_time'] = [
            'field' => 'start_service_time',    
            'func'  => function($data, $row_name){
                $contractId = $data['contractId'];

                $data['start_service_time'] = '--';

                $start_service_time = get_term_meta_value($contractId, 'start_service_time');
                if(empty($start_service_time)) return $data;

                $data['start_service_time_raw'] = $start_service_time;
                $data['start_service_time'] = my_date($start_service_time, 'd/m/Y');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['end_service_time'] = [
            'field' => 'end_service_time',    
            'func'  => function($data, $row_name){
                $contractId = $data['contractId'];

                $data['end_service_time'] = '--';

                $end_service_time = get_term_meta_value($contractId, 'end_service_time');
                if(empty($end_service_time)) return $data;

                $data['end_service_time_raw'] = $end_service_time;
                $data['end_service_time'] = my_date($end_service_time, 'd/m/Y');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['result_updated_on'] = [
            'field' => 'result_updated_on',    
            'func'  => function($data, $row_name){
                $contractId = $data['contractId'];

                $data['result_updated_on'] = '--';

                $result_updated_on = get_term_meta_value($contractId, 'result_updated_on');
                if(empty($result_updated_on)) return $data;

                $data['result_updated_on_raw'] = $result_updated_on;
                $data['result_updated_on'] = my_date($result_updated_on, 'd/m/Y');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['remain_pending_days'] = [
            'field' => 'remain_pending_days',    
            'func'  => function($data, $row_name){
                $contractId = $data['contractId'];

                $data['remain_pending_days'] = '--';

                $result_updated_on = get_term_meta_value($contractId, 'result_updated_on');
                if(empty($result_updated_on)) return $data;

                $data['remain_pending_days'] = round((time() - (int)$result_updated_on) / (60 * 60 * 24), 0);

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['actual_budget'] = [
            'field' => 'actual_budget',
            'func'  => function($data, $row_name) {
                $contractId = $data['contractId'];

                $data['actual_budget'] = (int) get_term_meta_value($contractId, 'actual_budget');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['spend'] = [
            'field' => 'spend',
            'func'  => function($data, $row_name) {
                $contractId = $data['contractId'];

                $data['spend_raw'] = (int) $data['spend'];

                $balance_spend = (int) get_term_meta_value($contractId, 'balance_spend');
                $data['spend'] = (int) $data['spend'] + $balance_spend;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['remain_budget'] = [
            'field' => 'remain_budget',    
            'func'  => function($data, $row_name){
                $contractId = $data['contractId'];
                
                $actual_budget = (int) get_term_meta_value($contractId, 'actual_budget');
                $balance_spend = (int) get_term_meta_value($contractId, 'balance_spend');
                $data['remain_budget'] = $actual_budget - (int) $data['spend_raw'] - $balance_spend;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['staff_business'] = [
            'field' => 'staff_business',    
            'func'  => function($data, $row_name){
                $contractId = $data['contractId'];

                $data['staff_business'] = [];
                
                $staff_business 		= get_term_meta_value($contractId, 'staff_business');
                if(empty($staff_business)) return $data;
                
                $staff_business = $this->admin_m->get_field_by_id($staff_business);
                
                $staff_business = [
                    'user_id' => $staff_business['user_id'],
                    'display_name' => $staff_business['display_name'],
                    'user_avatar' => $staff_business['user_avatar'],
                    'user_email' => $staff_business['user_email'],
                ];
            
                $data['staff_business'] = $staff_business;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $this->load->model('facebookads/facebookads_kpi_m');
        $callbacks['fb_staff_advertise'] = [
            'field' => 'fb_staff_advertise',    
            'func'  => function($data, $row_name){
                $contractId 		= $data['contractId'];

                $data['fb_staff_advertise'] = [];

			    $kpis = $this->facebookads_kpi_m->get_kpis($contractId, 'users', 'tech');
			    if(empty($kpis)) return $data;
                
			    $user_ids = array();
			    foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;

			    $data['fb_staff_advertise'] = array_reduce($user_ids, function($result, $user_id){
			    	$_user = $this->admin_m
                    ->select('user_id, user_email, display_name, user_avatar, user.role_id, role_name')
                    ->join('role', 'role.role_id = user.role_id')
                    ->as_array()
                    ->get($user_id);
                    if(empty($_user)) return $result;

                    $user = [
                        'user_id' => $_user['user_id'],
                        'user_email' => $_user['user_email'],
                        'display_name' => $_user['display_name'],
                        'user_avatar' => $_user['user_avatar'],
                        'role_name' => $_user['role_name'] ?? NULL,
                    ];
                    $result[] = $user;
			    	
                    return $result;
			    }, []);

                return $data;
            },
            'row_data' => FALSE,
        ];

        $selected_callbacks = [];
        if(empty($args)) $selected_callbacks = $callbacks;

        foreach($args AS $callback_name){
            if(!is_string($callback_name)) continue;
            if(!isset($callbacks[$callback_name])) continue;

            $selected_callbacks[$callback_name] = $callbacks[$callback_name];
        }

        return $selected_callbacks;
    }

    protected function search_filter($search_args = array())
    {
        $args = $this->input->get();

        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // Start update date FILTERING & SORTING
        $start_updated_date = $args['start_updated_date'] ?? FALSE;
        if ($start_updated_date) {
            $this->datatable_builder->where("result_updated_on.meta_value >=", start_of_day($start_updated_date));

            unset($args['start_updated_date']);
        }

        // end update date FILTERING & SORTING
        $end_updated_date = $args['end_updated_date'] ?? FALSE;
        if ($end_updated_date) {
            $this->datatable_builder->where("result_updated_on.meta_value <=", end_of_day($end_updated_date));

            unset($args['end_updated_date']);
        }

        // Contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $alias = uniqid('contract_code_');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_contract_code);
            }

            unset($args['where']['contract_code'], $args['order_by']['contract_code']);
        }

        // customer_code FILTERING & SORTING
        $filter_customer_code = $args['where']['customer_code'] ?? FALSE;
        $sort_customer_code   = $args['order_by']['customer_code'] ?? FALSE;
        if($filter_customer_code || $sort_customer_code)
        {
            $alias = uniqid('customer_code_');
            $this->datatable_builder
            ->join("term_users tu_customer","tu_customer.term_id = term.term_id")
            ->join("user customer","customer.user_id = tu_customer.user_id AND customer.user_type IN ('customer_person', 'customer_company')")
            ->join("usermeta {$alias}","{$alias}.user_id = customer.user_id and {$alias}.meta_key = 'cid'", 'LEFT');

            if($filter_customer_code)
            {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_customer_code);
                unset($args['where']['customer_code']);
            }

            if($sort_customer_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_customer_code);
                unset($args['order_by']['customer_code']);
            }
        }

        // customer_name FILTERING & SORTING
        $filter_customer_name = $args['where']['customer_name'] ?? FALSE;
        if ($filter_customer_name) {
            $alias = uniqid('customer_name_');

            $this->datatable_builder
                ->join("term_users AS tp_{$alias}", "tp_{$alias}.term_id = term.term_id", 'LEFT')
                ->join("user AS {$alias}", "tp_{$alias}.user_id = {$alias}.user_id", 'LEFT')
                ->where_in("{$alias}.user_type", ['customer_person', 'customer_company'])
                ->like("{$alias}.display_name", $filter_customer_name);

            unset($args['where']['customer_name']);
        }

        // type_of_service FILTERING & SORTING
        $filter_type_of_service = $args['where']['type_of_service'] ?? FALSE;
        $sort_type_of_service = $args['order_by']['type_of_service'] ?? FALSE;
        if ($filter_type_of_service || $sort_type_of_service) {
            $alias = uniqid('type_of_service_');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'typeOfService'", 'LEFT');

            if ($filter_type_of_service) {
                $this->datatable_builder->where("{$alias}.meta_value", $filter_type_of_service);
            }

            if ($sort_type_of_service) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_type_of_service);
            }

            unset($args['where']['type_of_service'], $args['order_by']['type_of_service']);
        }

        // Contract_value FILTERING & SORTING
        $filter_contract_value = $args['where']['contract_value'] ?? FALSE;
        $sort_contract_value = $args['order_by']['contract_value'] ?? FALSE;
        if ($filter_contract_value || $sort_contract_value) {
            $alias = uniqid('contract_value_');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_value'", 'LEFT');

            if ($filter_contract_value) {
                $this->datatable_builder->where("CAST({$alias}.meta_value AS UNSIGNED) >=", $filter_contract_value);
            }

            if ($sort_contract_value) {
                $this->datatable_builder->order_by("CAST({$alias}.meta_value AS UNSIGNED)", $sort_contract_value);
            }

            unset($args['where']['contract_value'], $args['order_by']['contract_value']);
        }

        // start_service_time FILTERING & SORTING
        $filter_start_service_time = $args['where']['start_service_time'] ?? FALSE;
        $sort_start_service_time = $args['order_by']['start_service_time'] ?? FALSE;
        if ($filter_start_service_time || $sort_start_service_time) {
            $alias = uniqid('start_service_time_');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'start_service_time'", 'LEFT');

            if ($filter_start_service_time) {
                $dates = explode(' - ', $filter_start_service_time);
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['start_service_time']);
            }

            if ($sort_start_service_time) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_start_service_time);
                unset($args['order_by']['start_service_time']);
            }
        }

        // end_service_time FILTERING & SORTING
        $filter_end_service_time = $args['where']['end_service_time'] ?? FALSE;
        $sort_end_service_time = $args['order_by']['end_service_time'] ?? FALSE;
        if ($filter_end_service_time || $sort_end_service_time) {
            $alias = uniqid('end_service_time_');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'end_service_time'", 'LEFT');

            if ($filter_end_service_time) {
                $dates = explode(' - ', $filter_end_service_time);
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['end_service_time']);
            }

            if ($sort_end_service_time) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_end_service_time);
                unset($args['order_by']['end_service_time']);
            }
        }

        // result_updated_on FILTERING & SORTING
        $filter_result_updated_on = $args['where']['result_updated_on'] ?? FALSE;
        if ($filter_result_updated_on) {
            if ($filter_result_updated_on) {
                $dates = explode(' - ', $filter_result_updated_on);
                $this->datatable_builder->where("result_updated_on.meta_value >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("result_updated_on.meta_value <=", $this->mdate->endOfDay(end($dates)));

                unset($args['where']['result_updated_on']);
            }
        }

        // Staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if ($filter_staff_business || $sort_staff_business) {
            $alias = uniqid('staff_business_');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT')
                ->join("user tblcustomer", "{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

            if ($filter_staff_business) {
                $this->datatable_builder->like("tblcustomer.display_name", $filter_staff_business);
                unset($args['where']['staff_business']);
            }

            if ($sort_staff_business) {
                $this->datatable_builder->order_by('tblcustomer.display_name', $sort_staff_business);
                unset($args['order_by']['staff_business']);
            }
        }

        // FIND AND SORT STAFF_ADVERTISE WITH Google_KPI
        $filter_fb_staff_advertise = $args['where']['fb_staff_advertise'] ?? FALSE;
        $sort_fb_staff_advertise = $args['order_by']['fb_fb_staff_advertise'] ?? FALSE;
        if ($filter_fb_staff_advertise || $sort_fb_staff_advertise) {
            $this->datatable_builder->join('facebookads_kpi', 'term.term_id = facebookads_kpi.term_id', 'LEFT OUTER');
            $this->datatable_builder->join('user', 'user.user_id = facebookads_kpi.user_id', 'LEFT OUTER');

            if ($filter_fb_staff_advertise) {
                $this->datatable_builder->like('user.user_email', strtolower($filter_fb_staff_advertise), 'both');
            }

            if ($sort_fb_staff_advertise) {
                $this->datatable_builder->order_by('user.user_email', $sort_fb_staff_advertise);
            }

            unset($args['where']['fb_staff_advertise'], $args['order_by']['fb_staff_advertise']);
        }
    }

    public function export($query = '')
    {
        if (empty($query)) return FALSE;

        // remove limit in query string
        $query = explode('LIMIT', $query);
        $query = reset($query);

        $terms = $this->contract_m->query($query)->result();

        if (!$terms) return FALSE;

        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("Báo cáo hợp đồng Google tạm ngưng"));
        $sheet = $spreadsheet->getActiveSheet();

        $columns = $this->getColumns('export');
        $sheet->fromArray(array_column(array_values($columns), 'title'), NULL, 'A1');

        $rowIndex = 2;

        $this->load->model('facebookads/facebookads_kpi_m');
        $typeOfServices = $this->config->item('items', 'typeOfServices');

        foreach ($terms as $item) {
            $i = 1;
            $colIndex = $i;

            $term_id = $item->contractId;

            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, get_term_meta_value($term_id, 'contract_code') ?: '--');
            $colIndex++;

            $customer_code = '--';
            $customer_name = '--';
            $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company']);
            if (!empty($customers)) {
                $customer = end($customers);
                
                $customer_code = cid($customer->user_id, $customer->user_type);
                $customer_name = $customer->display_name;
            }
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $customer_code);
            $colIndex++;
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $customer_name);
            $colIndex++;
            
            $type_of_services = '--';
            $typeOfService = get_term_meta_value($term_id, 'typeOfService');
            $typeOfService and $type_of_services = $typeOfServices[$typeOfService] ?? '--';
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $type_of_services);
            $colIndex++;

            $contract_value = (float) get_term_meta_value($term_id, 'contract_value') ?: 0;
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $contract_value)
                ->getStyleByColumnAndRow($colIndex, $rowIndex)
                ->getNumberFormat()
                ->setFormatCode("#,##0");
            $colIndex++;

            $start_service_time = '--';
            $start_service_time = get_term_meta_value($term_id, 'start_service_time');
            $start_service_time and $start_service_time = my_date($start_service_time, 'd/m/Y');
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $start_service_time)
                ->getStyleByColumnAndRow($colIndex, $rowIndex)
                ->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);;
            $colIndex++;

            $end_service_time = '--';
            $end_service_time = get_term_meta_value($term_id, 'end_service_time');
            $end_service_time and $end_service_time = my_date($end_service_time, 'd/m/Y');
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $end_service_time)
                ->getStyleByColumnAndRow($colIndex, $rowIndex)
                ->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);;
            $colIndex++;

            $result_updated_on = '--';
            $result_updated_on_raw = (int)get_term_meta_value($term_id, 'result_updated_on');
            $result_updated_on and $result_updated_on = my_date($result_updated_on_raw, 'd/m/Y');
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $result_updated_on)
                ->getStyleByColumnAndRow($colIndex, $rowIndex)
                ->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);;
            $colIndex++;

            $remain_pending_days = 0;
            $result_updated_on_raw > 0 AND $remain_pending_days = round((time() - (int)$result_updated_on_raw) / (60 * 60 * 24), 0);
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $remain_pending_days)
                ->getStyleByColumnAndRow($colIndex, $rowIndex)
                ->getNumberFormat()
                ->setFormatCode("#,##0");
            $colIndex++;

            $actual_budget = (float) get_term_meta_value($term_id, 'actual_budget') ?: 0;
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $actual_budget)
                ->getStyleByColumnAndRow($colIndex, $rowIndex)
                ->getNumberFormat()
                ->setFormatCode("#,##0");
            $colIndex++;

            $spend = (float) $item->spend;
            $balance_spend = (float) get_term_meta_value($term_id, 'balance_spend') ?: 0;
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $spend + $balance_spend)
                ->getStyleByColumnAndRow($colIndex, $rowIndex)
                ->getNumberFormat()
                ->setFormatCode("#,##0");
            $colIndex++;

            $remain_budget = $actual_budget - $spend - $balance_spend;
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $remain_budget)
                ->getStyleByColumnAndRow($colIndex, $rowIndex)
                ->getNumberFormat()
                ->setFormatCode("#,##0");
            $colIndex++;

            $staff_business         = '--';
            $staff_business_id         = get_term_meta_value($term_id, 'staff_business');
            if (!empty($staff_business_id)) {
                $staff_business = $this->admin_m->get_field_by_id($staff_business_id, 'display_name');
            }
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $staff_business);
            $colIndex++;

            $fb_staff_advertise = '--';
            $kpis = $this->facebookads_kpi_m->get_kpis($term_id, 'users', 'tech');
            if (!empty($kpis)) {
                $user_ids = array();
                foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;

                $fb_staff_advertise = implode(', ', array_values(array_map(function ($user_id) {
                    $_user = $this->admin_m->get_field_by_id($user_id);
                    return $_user['display_name'] ?: $_user['user_email'];
                }, $user_ids)));
            }
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $fb_staff_advertise);
            $colIndex++;

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report/';
        if (!is_dir($folder_upload)) {
            try {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-bao-cao-hop-dong-google-tam-ngung.xlsx";

        try {
            (new Xlsx($spreadsheet))->save($fileName);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetAdsPending.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/DatasetAdsPending.php */
