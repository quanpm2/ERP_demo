<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insight_Segments extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'facebookads/facebookads_m';
        $this->autoload['models'][] = 'facebookads/adaccount_m';
        $this->autoload['models'][] = 'ads_segment_m';
        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'log_m';

        parent::__construct($config);
    }

    public function index_get($id = 0)
    {
        $contract = $this->facebookads_m->set_term_type()->get($id);
        if(empty($contract)) parent::response(['status' => false, 'data' => null]);

        $contract->number_of_payments   = (int)(get_term_meta_value($id, 'number_of_payments') ?: 1);
        $contract->contract_budget_payment_type = get_term_meta_value($id, 'contract_budget_payment_type');
        $contract->contract_budget_customer_payment_type = get_term_meta_value($id, 'contract_budget_customer_payment_type');

        $contract->service_fee_payment_type = get_term_meta_value($id, 'service_fee_payment_type');        

        $contract->adaccount_source = get_term_meta_value($contract->term_id, 'adaccount_source') ?: 'internal';

        $contract->adaccounts = get_term_meta($id, 'adaccounts', FALSE, TRUE);
        $contract->adaccounts AND $contract->adaccounts = array_map('intval', $contract->adaccounts);

        $contract->advertise_start_time = (int) get_term_meta_value($id, 'advertise_start_time');
        $contract->advertise_end_time = (int) get_term_meta_value($id, 'advertise_end_time');
        $contract->contract_code = get_term_meta_value($id, 'contract_code');
        $contract->actual_budget = (int) get_term_meta_value($contract->term_id, 'actual_budget');

        $contract->service_fee          = (int) get_term_meta_value($id, 'service_fee');
        $contract->contract_budget      = (int) get_term_meta_value($id, 'contract_budget');

        /**
         * Danh sách chương trình khuyến mãi hợp đồng đang áp dụng
         *
         * @var        array
         */
        $_promotions = get_term_meta_value($id, 'promotions') ?: [];
        if($_promotions)
        {
            $_promotions = unserialize($_promotions);
            $_promotions = array_map(function($x){
                $x['value'] = (double) $x['value'];
                return $x;
            }, $_promotions);
        }
        $contract->promotions = $_promotions;

        $contract->is_service_proc    = (bool) is_service_proc($contract);
        $contract->is_service_end     = (bool) is_service_end($contract);

        $contract_m = (new contract_m())->set_contract($contract);
        $contract->actual_result = (double) $contract_m->get_behaviour_m()->get_actual_result();

        $contract_curators = get_term_meta_value($id, 'contract_curators');
        $contract_curators = is_serialized($contract_curators) ? unserialize($contract_curators) : [];
        $contract->curators = $contract_curators;

        $contract->is_display_promotions_discount =  (int) get_term_meta_value($id, 'is_display_promotions_discount');

        $this->load->model('facebookads/adaccount_m');
        $spent_segments = [];

        if( 'external' == $contract->adaccount_source && ! empty($contract->adaccounts))
        {
            // dd($contract->adaccounts);
            $spent_segments = array();
            foreach ($contract->adaccounts as $_adaccount_id)
            {
                $_insight = $this->adaccount_m->get_data($_adaccount_id, start_of_month($contract->advertise_start_time), $contract->advertise_end_time);
                if(empty($_insight)) continue;

                $_insight = array_map(function($x) use($_adaccount_id){
                    $x['adaccount_id'] = $_adaccount_id;
                    return $x;
                }, $_insight);

                $spent_segments = array_merge($_insight, $spent_segments);
            }
            
            $contract->spent_segments = $spent_segments;
        }


        $contract->segments = [];
        if($segments = $this->term_posts_m->get_term_posts($id, $this->ads_segment_m->post_type))
        {
            $segments = array_map(function($x){

                $adaccount = $this->term_posts_m->get_post_terms( $x->post_id, $this->adaccount_m->term_type);
                $adaccount AND $adaccount = reset($adaccount);

                return array(
                    'post_id' => (int)  $x->post_id,
                    'start_date' => (int) $x->start_date,
                    'end_date' => (int)  $x->end_date,
                    'adaccount_id' => (int)  $adaccount->term_id,
                    'contract_id' => (int)  $x->term_id
                );

            }, $segments);

            $segments = array_values($segments);

            $contract->segments = array_values($segments);
        }

        parent::response([ 'status' => true, 'data' => $contract ]);
    }
    
    /**
     * Get all activated by adaccount_id
     *
     * @param      int   $adaccount_id    The adaccount_id
     */
    public function activated_by_adaccount_id_get($adaccount_id = 0)
    {
        $args = parent::get();

        $m_args = array(
            'key' => 'adaccount_id', 'value' => (int) $adaccount_id, 'compare' => '=' 
        );

        $contracts = $this->facebookads_m->select('term.term_id, term_name, term_status, term.status')->m_find($m_args)->set_term_type()
        ->where_in('term_status', ['pending', 'publish'])
        ->get_all();

        $contracts AND $contracts = array_map(function($contract){

            $contract->adaccount_id = (int) get_term_meta_value($contract->term_id, 'adaccount_id');
            $contract->advertise_start_time = (int) get_term_meta_value($contract->term_id, 'advertise_start_time');
            $contract->advertise_end_time = (int) get_term_meta_value($contract->term_id, 'advertise_end_time');
            $contract->contract_code = get_term_meta_value($contract->term_id, 'contract_code');
            $contract->actual_budget = (int) get_term_meta_value($contract->term_id, 'actual_budget');
            
            $contract_m = (new contract_m())->set_contract($contract);
            $contract->actual_result = (double) $contract_m->get_behaviour_m()->get_actual_result();

            return $contract;

        }, $contracts);

        parent::response([ 'status' => true,
            'data' => [
                'isActive' => ! empty($contracts),
                'contracts' => $contracts
            ]
        ]);
    }

    /**
     * Determines if valid start date.
     *
     * @param      int   $term_id  The term identifier
     * @param      int   $adaccount_id      The adaccount_id
     */
    public function is_valid_start_date_get($term_id = 0, $adaccount_id = 0, $start_time = 0)
    {
        $contract   = new facebookads_m();
        $contract->set_contract($term_id);

        $contracts = $contract->get_conflict_by_start_date($adaccount_id, $start_time);

        parent::response([
            'status' => true,
            'data' => [
               'isValid' => empty($contracts),
               'contracts' => $contracts
            ] 
        ]);   
    }

    /**
     * { function_description }
     *
     * @param      int   $adaccount_id    The adaccount_id
     */
    public function related_by_adaccount_id_get($adaccount_id = 0)
    {
        $args = wp_parse_args(parent::get(null, true), array( 'adaccount_id' => $adaccount_id,  'term_id' => null ));

        ! empty($args['term_id']) AND $this->facebookads_m->where('term.term_id !=', (int) $args['term_id']);

        $contracts = $this->facebookads_m->get_all_by_adaccount_id($adaccount_id);

        parent::response([ 'status' => true, 'data' => $contracts ]);
    }

     /**
     * Determines if valid start date.
     *
     * @param      int   $term_id  The term identifier
     * @param      int   $adaccount_id      The adaccount_id
     */
    public function configurations_put($contractId = 0)
    {
        $this->load->model('ads_segment_m');
        $this->load->model('facebookads/adaccount_m');

        $response = array('status' => false, 'messages' => null);

        $this->load->library('form_validation');
        $this->form_validation->set_data(parent::put(null, TRUE));
        $this->form_validation->set_rules('adaccount_source', 'adaccount_source', 'in_list[internal,external]');
        $this->form_validation->set_rules('adaccounts[]', 'adaccounts[]', 'integer');
        $this->form_validation->set_rules('segments[]', 'segments', 'required');

        if( FALSE === $this->form_validation->run())
        {
            parent::response([
                'code' => 400,
                'error' => $this->form_validation->error_array()
            ]);            
        }

        $adaccounts = parent::put('adaccounts', TRUE) ?: [];
        $data       = parent::put(null, TRUE);
        if(isset($data['adaccounts']))
        {
            switch (empty($adaccounts))
            {
                case TRUE:
                    $this->contract_m->set_contract($contractId);
                    $this->contract_m->get_behaviour_m()->delete_segments();
                    $this->termmeta_m->delete_meta($contractId, 'adaccounts');
                    break;
                
                default:

                    $adaccounts = array_unique($adaccounts);
                    $this->termmeta_m->delete_meta($contractId, 'adaccounts');

                    array_walk($adaccounts, function($_adAccountId) use($contractId){
                        $this->termmeta_m->add_meta($contractId, 'adaccounts', $_adAccountId);
                    });

                    break;
            }
        }

        $adaccount_source = parent::put('adaccount_source', TRUE);
        update_term_meta($contractId, 'adaccount_source', $adaccount_source ?? 'internal');
        
        if('external' == $adaccount_source)
        {
            $external_adaccount_spent = 0;

            $spent_segments = parent::put('spent_segments', TRUE) ?: [];
            // if( ! empty($spent_segments))
            // {
            //     $spent_segments = array_filter($spent_segments, function($x){
            //         return ! empty($x['spend']);
            //     });

            //     $_spent_segments = array_values($spent_segments);
            //     $_spent_segments = array_map(function($x){
            //         $x['timestamp'] = start_of_day($x['timestamp']);
            //         return $x;
            //     }, $_spent_segments);

            //     $_spent_segments = array_group_by($_spent_segments, "adaccount_id");

            //     $advertise_start_time   = start_of_day($metadata['advertise_start_time']);
            //     $advertise_end_time     = start_of_day($metadata['advertise_end_time']);

            //     $_spent_items = array();

            //     foreach ($_spent_segments as $adaccount_id => $_items)
            //     {
            //         $adaccountInsights  = array_group_by($_items, 'timestamp');
            //         foreach ($adaccountInsights as $_time => $values)
            //         {
            //             $_spent_items[] = array(
            //                 'meta_key' => "insight_{$_time}",
            //                 'adaccount_id' => $adaccount_id,
            //                 'meta_value' => array(
            //                     'timestamp'     => $_time,
            //                     'date_start'    => my_date($_time, 'Y-m-d'),
            //                     'date_stop'     => my_date($_time, 'Y-m-d'),
            //                     'spend'         => array_sum(array_column($values, 'spend')),
            //                     'account_id'    => null,
            //                     'account_name'  => null,
            //                     'clicks'        => 0,
            //                     'impressions'   => 0,
            //                     'cpc'           => 0,
            //                     'ctr'           => 0,
            //                 )
            //             );
            //         }

            //         // $_insight = $this->adaccount_m->get_data($adaccount_id, $advertise_start_time, $advertise_end_time);
            //         // if( $_insight = array_keys($_insight))
            //         // {
            //         //  foreach ($_insight as $_key)
            //         //  {
            //         //      $_name = "insight_{$_key}";
            //         //      if(in_array($_name, $_meta_keys)) continue;

            //         //      update_term_meta($adaccount_id, $_name, null);
            //         //  }
            //         // }

            //         // foreach ($_metadata as $row)
            //         // {
            //         //  // dd($adaccount_id, $row['meta_key'], $row['meta_value']);
            //         //  update_term_meta($adaccount_id, $row['meta_key'], serialize($row['meta_value']));
            //         //  // _db();
            //         // }
            //     }

            //     // Create|Update trong phạm vi của các tài khoản được cấu hình trong hợp đồng
            //     foreach ($adaccounts as $adaccount_id)
            //     {
            //         /**
            //          * Case : 
            //          * 1. Dữ liệu chi tiêu không nằm trong tài khoản thì
            //          *      - 
            //          */
            //     }

            //     dd($_spent_items);

            //     unset($metadata['spent_segments']);
            // }

            // try
            // {   
            //     $behaviour_m    = $this->facebookads_m->get_behaviour_m();
            //     /* Get progress info */
            //     $behaviour_m->get_the_progress();
            //     /* Update all metadata in relations */ 
            //     $behaviour_m->sync_all_amount(); 
            // }
            // catch (Exception $e)
            // {
            //     $this->messages->error($e->getMessage());
            //     redirect(module_url("setting/{$contractId}"),'refresh');
            // }
        }

        $segments = parent::put('segments', TRUE);
        $segments AND $segments = array_filter($segments, function($x){ return !empty($x['adaccount_id']); });
        if( ! empty($segments))
        {
            $adsSegmentsIds = array();
            /* Load danh sách tất cả phân đoạn của hợp đồng */
            $adssegments = $this->term_posts_m->get_term_posts($contractId, $this->ads_segment_m->post_type); 
            $adssegments AND $adsSegmentsIds = array_filter(array_column($adssegments, 'post_id'));

            $removeSegmentsIds = array_filter($adsSegmentsIds, function($x) use ($segments){
                if(empty($segments)) return true;
                return ! in_array($x, array_column($segments, 'post_id'));
            });

            /* Nếu phân đoạn nào không được thấy trong giá trị này thì cần phải xóa nó ra khỏi data */
            if( ! empty($removeSegmentsIds))
            {
                $this->ads_segment_m->delete_many($removeSegmentsIds);
                $this->term_posts_m->where_in('post_id', $removeSegmentsIds)->delete_by();
            }

            foreach ($segments as &$segment)
            {
                $start_date = $segment['start_date'] ?: start_of_day($segment['start_date']);
                $end_date   = "";
                $segment['end_date'] AND $end_date = end_of_day($segment['end_date']);

                // Nếu $segment không tìm thấy trong DB thì tiến hành tạo mới.
                if(empty($segment['post_id']))
                {
                    $insert_id          = $this->ads_segment_m->insert([ 'start_date' => $start_date, 'end_date' => $end_date ]);
                    $segment['post_id'] = $insert_id;

                    $this->term_posts_m->set_post_terms($insert_id, $segment['adaccount_id'], $this->adaccount_m->term_type);
                    continue;
                }

                // Nếu $segment có tồn tại trong DB thì tiến hành cập nhật
                $this->ads_segment_m->update($segment['post_id'], [ 'start_date' => $start_date, 'end_date' => $end_date ]);                
                $this->term_posts_m->set_post_terms($segment['post_id'], $segment['adaccount_id'], $this->adaccount_m->term_type);
            }

            $this->term_posts_m->set_term_posts($contractId, array_unique(array_column($segments, 'post_id')), $this->ads_segment_m->post_type, FALSE);
        }

        $response = array('status' => true, 'messages' => 'Xử lý thành công.');

        parent::response($response);
    }

    /**
     * Excuse the set of stop services process
     *
     * 1. Update advertise_end_time metadata
     * 1. Update end_service_time metadata with now()
     * 2. Change term_status to "ending"
     * 3. Log trace
     * 4. Send Overall E-mail with Adwords Data to someone
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     JSON
     */
    public function stop_service_put($term_id = 0)
    {
        $response   = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
        $args       = wp_parse_args( parent::put(null, true), ['end_date' => my_date(time(), 'd-m-Y')]);

        $contract   = new facebookads_m();
        $contract->set_contract($term_id);


        // Restrict permission of current user
        if( ! $contract->can('facebookads.stop_service.update'))
        {
            $response['msg'] = 'Không có quyền thực hiện tác vụ này.';
            parent::response($response);
        }


        // Check if the service is running then stop excute and return
        if($contract->is_service_end($term_id))
        {
            $response['msg'] = 'Hợp đồng đã được đóng.';
            parent::response($response);
        }

        $end_time = end_of_day($args['end_date']);

        /* Load danh sách tất cả phân đoạn của hợp đồng */
        $adssegments = $this->term_posts_m->get_term_posts($term_id, $this->ads_segment_m->post_type); 
        if(empty($adssegments))
        {
            $response['msg'] = 'Có lỗi xảy ra vì phân đoạn không được tìm thấy.';
            parent::response($response);
        }

        foreach ($adssegments as $adssegment)
        {
            $_end_date = end_of_day($adssegment->end_date ?: $end_time);
            $this->ads_segment_m->set_post_type()->update($adssegment->post_id, [ 'end_date' => $_end_date ]);
        }

        update_term_meta($term_id, 'advertise_end_time', max(array_column($adssegments, 'end_date')));

        // Main process
        $this->load->model('facebookads/facebookads_contract_m');
        $result = $this->facebookads_contract_m->stop_service($contract->get_contract(), $end_time);
        if( ! $result)
        {
            $response['msg'] = 'Quá trình xử lý không thành công.';
            parent::response($response);
        }

        $response['msg']    = 'Hợp đồng đã được kết thúc.';
        $response['status'] = TRUE;

        parent::response($response);
    }

    /**
     * Excuse the set of start services process
     * 
     * 1. Update Money Exchange Rate from VIETCOMBANK API
     * 2. Update Advertise start time if is not set
     * 3. Log trace action 
     * 4. Send activation email to all responsibilty users
     * 5. Detect if first contract
     * 6. Send SMS notify to contract's customer
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     JSON
     */
    public function proc_service_put($term_id = 0)
    {
        $argsDefault    = [ 'begin_date' => my_date(time(), 'd-m-Y') ];
        $args           = wp_parse_args( parent::put(null, true), $argsDefault);
        $response       = [ 'status' => FALSE,'msg' => 'Quá trình xử lý không thành công.','data' => [] ];

        $contract       = new facebookads_m();
        if( ! $contract->set_contract($term_id))
        {
            $response['msg'] = 'Hợp đồng hết hiệu lực hoặc không tồn tại.';
            parent::response($response);    
        }

        // Restrict permission of current user
        if( ! $contract->can('facebookads.start_service.update'))
        {
            $response['msg'] = 'Không có quyền thực hiện tác vụ này.';
            parent::response($response);
        }

        // Check if the service is running then stop excute and return
        if($this->facebookads_m->is_service_proc($term_id))
        {
            $response['msg'] = 'Dịch vụ đã được thực hiện.';
            parent::response($response);
        }

        $this->load->model('facebookads/facebookads_contract_m');
        $result  = $this->facebookads_contract_m->proc_service($contract->get_contract(), start_of_day($args['begin_date']));
        if( ! $result)
        {
            $response['msg'] = 'Quá trình xử lý không thành công.';
            parent::response($response);
        }

        // Detect if contract is the first signature (tái ký | ký mới)
        $this->base_contract_m->detect_first_contract($term_id);

        // Send SMS to customer
        $this->load->model('contract/contract_report_m');
        $this->contract_report_m->send_sms_activation_2customer($term_id);

        $response['msg']    = 'Dịch vụ đã được kích hoạt thực hiện thành công.';
        $response['status'] = TRUE;

        parent::response($response);
    }
}
/* End of file MCCReport.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/MCCReport.php */