<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report extends MREST_Controller
{

    public function __construct()
    {
        $this->autoload['models'][] = 'customer/customer_m';
        $this->autoload['models'][] = 'facebookads/facebookads_m';
        $this->autoload['models'][] = 'facebookads/adaccount_m';
        $this->autoload['models'][] = 'ads_segment_m';
        $this->autoload['models'][] = 'facebooakds/insight_segment_m';
        
        parent::__construct();

        $this->load->config('contract/contract');
    }

    public function quarter_get()
    {
        $args = wp_parse_args(parent::get(null, TRUE), [
            'quarter' => ceil(div((int) my_date(time(), 'm'), 3)),
            'year' => my_date(time(), 'Y')
        ]);

        $this->load->library('form_validation');
        $this->form_validation->set_data($args);
        $this->form_validation->set_rules('quarter', 'Quarter', 'required|integer|greater_than_equal_to[1]|less_than_equal_to[4]');
        $this->form_validation->set_rules('year', 'Year', 'required|integer|greater_than_equal_to[2015]|less_than_equal_to['.my_date(strtotime("+1 year"), 'Y').']');
        if( FALSE == $this->form_validation->run()) parent::response([ 'code' => 400, 'error' => $this->form_validation->error_array() ]);

        $start_month    = ($args['quarter'] * 3) - 2;
        $end_month      = $args['quarter'] * 3;
        $start_time     = start_of_month(date("{$args['year']}/{$start_month}/d"));
        $end_time       = end_of_month(date("{$args['year']}/{$end_month}/d"));


        $iSegments = $this->insight_segment_m
        ->select('post_id, post_name, post_status, comment_status, post_type, start_date, end_date')
        ->set_post_type()
        ->where('post_name' , 'day')
        ->where('start_date >=', $start_time)
        ->where('start_date <=', $end_time)
        ->get_all();

        if(empty($iSegments)) 
        {
            parent::response(['code' => 400, 'error' => 'Hệ thống không tìm thấy bất kỳ dữ liệu nào trong phạm vi này !']);
        }

        $adAccounts = [];

        array_walk($iSegments, function(&$x) use(&$adAccounts){

            $adaccount = $this->term_posts_m->get_post_terms( $x->post_id, $this->adaccount_m->term_type);
            $adaccount AND $adaccount = reset($adaccount);
            $x->adaccount_id    = $adaccount->term_id;
            array_push($adAccounts, $adaccount); 
        });

        $iSegments = array_group_by($iSegments, 'adaccount_id');

        array_walk($adAccounts, function($x) use($iSegments){
            $x->isegments = $iSegments[$x->term_id] ?? [];
        });

        $adAccounts = array_column($adAccounts, NULL, 'term_id');

        $this->load->model('ads_segment_m');

        $segments = $this->ads_segment_m
        ->select('posts.post_id, start_date, end_date, term_posts.term_id')
        ->set_post_type()
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->where_in('term_posts.term_id', array_unique(array_map('intval', array_filter(array_keys($adAccounts)))))
        ->where('start_date <=', $end_time)
        ->where('if(end_date = 0 or end_date is null, UNIX_TIMESTAMP (), end_date) >=', $start_time)
        ->get_all();

        $payment_type_enums = [
            'normal' => 'Full VAT',
            'customer' => 'Khách hàng tự thanh toán',
            'behalf' => 'ADSPLUS thu & chi hộ'
        ];

        $data = array();

        $segmentContracts = $this
            ->facebookads_m
            ->select('term.term_id,term_name,term_type,term_status,term_parent,term_posts.post_id')
            ->set_term_type()
            ->join('term_posts', 'term_posts.term_id = term.term_id')
            ->where_in('term_posts.post_id', array_map('intval', array_column($segments, 'post_id')))
            ->get_all();

        $segmentContracts AND $segmentContracts = array_group_by($segmentContracts, 'post_id');

        foreach ($segments as $key => $segment)
        {
            if(empty($segmentContracts[$segment->post_id]))
            {
                unset($segments[$key]);
                continue;
            }

            $_contract = reset($segmentContracts[$segment->post_id]);
            $_adaccount = $adAccounts[$segment->term_id];

            $customer = $this->term_users_m->get_the_users($_contract->term_id, [ 'customer_person', 'customer_company' ]);
            $customer AND $customer = reset($customer);

            $is_renewal = ! ((bool) get_term_meta_value($_contract->term_id, 'is_first_contract'));

            $payment_type = 'normal';
            if('customer' == get_term_meta_value($_contract->term_id, 'contract_budget_payment_type'))
            {
                $payment_type = 'customer';
                'behalf' == get_term_meta_value($_contract->term_id, 'contract_budget_customer_payment_type') AND $payment_type = 'behalf';
            }

            $interval = (new DateTime())->setTimestamp($segment->start_date)->diff((new DateTime())->setTimestamp($segment->end_date?:end_of_day()));

            $contract_begin = get_term_meta_value($_contract->term_id, 'contract_begin');
            $contract_end = get_term_meta_value($_contract->term_id, 'contract_end');

            /* Contract Durations month */
            $month_durations = number_format($interval->m + ($interval->d/30), 1); 

            $_iSegments = array_filter($_adaccount->isegments, function($x) use($segment){
                return $x->start_date >= $segment->start_date && $x->start_date <= ($segment->end_date ?: end_of_day());
            });

            $_iSegments AND $_iSegments = array_map(function($x){
                $x->spend = (double) get_post_meta_value($x->post_id, 'spend');
                $x->account_currency = get_post_meta_value($x->post_id, 'account_currency');
                return $x;
            }, $_iSegments);

            $cost = (int) array_sum(array_column($_iSegments, 'spend'));
    
            $_instanceISegment = reset($_iSegments);
            $account_currency_code = $_instanceISegment->account_currency;
            $meta_exchange_rate_key = 'exchange_rate_'. strtolower($account_currency_code) .'_to_vnd';
            $exchange_rate = (int)get_post_meta_value($_instanceISegment->post_id, $meta_exchange_rate_key);
            $exchange_rate AND $account_currency_code == 'USD' AND $exchange_rate = (int)get_term_meta_value($_contract->term_id, 'dollar_exchange_rate');
            $exchange_rate AND $exchange_rate = (int)get_term_meta_value($_contract->term_id, $meta_exchange_rate_key);
            $exchange_rate AND $exchange_rate = get_exchange_rate($account_currency_code);
            $dollar_exchange_rate = $exchange_rate;

            $budget         = (new facebookads_m())->set_contract((object) $_contract)->get_behaviour_m()->calc_budget();
            $service_fee    = (int) get_term_meta_value($_contract->term_id, 'service_fee');
            $service_rate   = div($service_fee, $budget);

            $data[] = array(
                $_adaccount->term_slug,    
                $customer->display_name ?? '---',
                ($is_renewal ? 'renewal' : 'new'),
                $payment_type_enums[$payment_type] ?? '',
                $month_durations,
                my_date($contract_begin),
                my_date($segment->start_date),
                my_date($contract_end),
                my_date($segment->end_date),
                $cost,
                $account_currency_code,
                $dollar_exchange_rate,
                $cost*$service_rate,
                currency_numberformat($service_rate*100, '%', 2),
                $account_currency_code == 'VND' ? div($cost, $dollar_exchange_rate) : $cost,
                ($account_currency_code == 'VND' ? div($cost, $dollar_exchange_rate) : $cost)*$service_rate,
            );
        }

        $this->load->library('table');
        $this->table->set_heading(['AdAccount ID', 'Customer Name', 'Contract type', 'Payment Type', 'Total contract durations (months)', 'Contract Start date (contract)', 'Contract Start date (kick of campaign)', 'Contract End date (contract)', 'Contract End date (Off Campaign)', 'Media spend this Quarter','currency', 'Exchange', 'Management fee on Media Spend this quarter', 'Management fee %','Media spend this Quarter (USD)', 'Management fee on Media Spend this quarter (USD)']);
        echo $this->table->generate($data);
        die();
    }

    public function monthly_get()
    {
        $args = wp_parse_args(parent::get(null, TRUE), [
            'month' => my_date(time(), 'm'),
            'year' => my_date(time(), 'Y')
        ]);

        $this->load->library('form_validation');
        $this->form_validation->set_data($args);
        $this->form_validation->set_rules('month', 'Month', 'required|integer|greater_than_equal_to[1]|less_than_equal_to[12]');
        $this->form_validation->set_rules('year', 'Year', 'required|integer|greater_than_equal_to[2015]|less_than_equal_to['.my_date(strtotime("+1 year"), 'Y').']');
        if( FALSE == $this->form_validation->run()) parent::response([ 'code' => 400, 'error' => $this->form_validation->error_array() ]);

        $start_time = start_of_month(date("{$args['year']}/{$args['month']}/d"));
        $end_time   = end_of_month(date("{$args['year']}/{$args['month']}/d"));

        $cacheStacks    = [ 'functions', CI::$APP->router->fetch_module(), get_called_class(), __FUNCTION__, md5(json_encode($args)) ];
        
        $cache_key      = implode('/', array_merge($cacheStacks, ['iSegments']));
        $iSegments      = $this->scache->get($cache_key);
        if( ! $iSegments)
        {
            $iSegments = $this->insight_segment_m
            ->select('post_id, post_name, post_status, comment_status, post_type, start_date, end_date')
            ->set_post_type()
            ->where('post_name' , 'day')
            ->where('start_date >=', $start_time)
            ->where('start_date <=', $end_time)
            ->get_all();

            if(empty($iSegments)) 
            {
                parent::response(['code' => 400, 'error' => 'Hệ thống không tìm thấy bất kỳ dữ liệu nào trong phạm vi này !']);
            }

            $this->scache->write($iSegments, $cache_key, 5*300);
        }
        

        $adAccounts = [];

        array_walk($iSegments, function(&$x) use(&$adAccounts){

            $adaccount = $this->term_posts_m->get_post_terms( $x->post_id, $this->adaccount_m->term_type);
            $adaccount AND $adaccount = reset($adaccount);
            $x->adaccount_id    = $adaccount->term_id;
            array_push($adAccounts, $adaccount); 
        });

        $iSegments = array_group_by($iSegments, 'adaccount_id');

        array_walk($adAccounts, function($x) use($iSegments){
            $x->isegments = $iSegments[$x->term_id] ?? [];
        });

        $adAccounts = array_column($adAccounts, NULL, 'term_id');

        $this->load->model('ads_segment_m');

        $segments = $this->ads_segment_m
        ->select('posts.post_id, start_date, end_date, term_posts.term_id')
        ->set_post_type()
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->where_in('term_posts.term_id', array_unique(array_map('intval', array_filter(array_keys($adAccounts)))))
        ->where('start_date <=', $end_time)
        ->where('if(end_date = 0 or end_date is null, UNIX_TIMESTAMP (), end_date) >=', $start_time)
        ->get_all();


        $payment_type_enums = array(
            'normal' => 'Full VAT',
            'customer' => 'Khách hàng tự thanh toán',
            'behalf' => 'ADSPLUS thu & chi hộ'
        );

        $data = array();

        $segmentContracts = $this
            ->facebookads_m
            ->select('term.term_id,term_name,term_type,term_status,term_parent,term_posts.post_id')
            ->set_term_type()
            ->join('term_posts', 'term_posts.term_id = term.term_id')
            ->where_in('term_posts.post_id', array_map('intval', array_column($segments, 'post_id')))
            ->get_all();


        $segmentContracts AND $segmentContracts = array_group_by($segmentContracts, 'post_id');

        foreach ($segments as $key => &$segment)
        {
            if(empty($segmentContracts[$segment->post_id]))
            {
                unset($segments[$key]);
                continue;
            }

            $segment->start_date = start_of_day($segment->start_date);

            $_contract              = reset($segmentContracts[$segment->post_id]);
            $segment->contract_id   = $_contract->term_id;
            $segment->contract      = $_contract;

            $_adaccount             = $adAccounts[$segment->term_id];
            $segment->adaccount_id  = $_adaccount->term_id;
            $segment->adaccount     = $_adaccount;

            $customer = $this->term_users_m->get_the_users($_contract->term_id, [ 'customer_person', 'customer_company' ]);
            $customer AND $customer = reset($customer);

            $segment->customer_id   = $customer->user_id;
            $segment->customer      = $customer;

            $segment->is_renewal = ! ((bool) get_term_meta_value($_contract->term_id, 'is_first_contract'));

            $segment->payment_type = 'normal';
            if('customer' == get_term_meta_value($_contract->term_id, 'contract_budget_payment_type'))
            {
                $segment->payment_type = 'customer';
                'behalf' == get_term_meta_value($_contract->term_id, 'contract_budget_customer_payment_type') AND $segment->payment_type = 'behalf';
            }

            $interval = (new DateTime())->setTimestamp($segment->start_date)->diff((new DateTime())->setTimestamp($segment->end_date?:end_of_day()));

            $segment->contract_begin        = get_term_meta_value($_contract->term_id, 'contract_begin');
            $segment->contract_end          = get_term_meta_value($_contract->term_id, 'contract_end');
            $segment->contract_daterange    = implode(' - ', array_map(function($x){ return my_date($x, 'd/m/Y'); }, [$segment->contract_begin, $segment->contract_end]));

            /* Contract Durations month */
            $segment->month_durations = number_format($interval->m + ($interval->d/30), 1); 

            $segment->_iSegments = array_filter($_adaccount->isegments, function($x) use($segment){
                return $x->start_date >= $segment->start_date && $x->start_date <= ($segment->end_date ?: end_of_day());
            });

            $segment->_iSegments AND $segment->_iSegments = array_map(function($x){
                $x->spend = (double) get_post_meta_value($x->post_id, 'spend');
                $x->account_currency = get_post_meta_value($x->post_id, 'account_currency');
                return $x;
            }, $segment->_iSegments);

            $segment->cost = 0;
            $segment->account_currency_code = 'VND';
            $segment->exchange_rate = 1;

            if( ! empty($segment->_iSegments))
            {

                $segment->cost = (double) array_sum(array_column($segment->_iSegments, 'spend'));

                $_instance = reset($segment->_iSegments);
                $account_currency = $_instance->account_currency;
                
                if('VND' != $account_currency){
                    $exchange_rate = $_instance->exchange_rate;
                    empty($exchange_rate) AND ($account_currency == 'USD') AND $account_currency = get_term_meta_value($_contract->term_id, 'dollar_exchange_rate');
                    empty($exchange_rate) AND $account_currency = get_term_meta_value($_contract->term_id, strtolower("exchange_rate_{$account_currency}_to_vnd"));
                    empty($exchange_rate) AND $exchange_rate = get_exchange_rate($account_currency);

                    $segment->exchange_rate = $exchange_rate;
                }
            }

            $contract = (new facebookads_m())->set_contract((object) $_contract);
            $segment->budget         = $contract->get_behaviour_m()->calc_budget();
            $segment->service_fee    = (int) get_term_meta_value($_contract->term_id, 'service_fee');
            $segment->service_rate   = div($segment->service_fee, $segment->budget);

            $segment->discount_amount = $contract->get_behaviour_m()->calc_disacount_amount();
            $segment->service_fee_rate_actual = div($segment->service_fee - $segment->discount_amount, $segment->budget);
            
            $segment->actual_result  = (int) get_term_meta_value($_contract->term_id, 'actual_result');
            $segment->actual_budget  = (int) get_term_meta_value($_contract->term_id, 'actual_budget');
            $segment->remain_budget  = $segment->actual_budget - $segment->actual_result;
        }

        /* LOAD ALL STAFFS OF ADS CONTRACTS */
        $this->load->model('googleads/googleads_kpi_m');
        $kpis = $this
            ->googleads_kpi_m
            ->select('term_id, user_id')
            ->where_in('term_id', array_unique(array_column($segments, 'contract_id')))
            ->setTechnicalKpiTypes()
            ->group_by('term_id')
            ->as_array()
            ->get_all();

        $users = array();
        if($kpis)
        {
            $userIds = array_unique(array_column($kpis, 'user_id'));
            foreach ($userIds as $userId)
            {
                $users[$userId] = $this->admin_m->get_field_by_id($userId);
            } 
            $kpis AND $kpis = array_group_by($kpis, 'term_id');
        }
        /* LOAD ALL STAFFS OF ADS CONTRACTS - END */

        $data = array_map(function($x) use($payment_type_enums, $users, $kpis){

            /* PARSE TECHNICAL STAFFS */
            $techStaffs = "";
            empty($kpis[$x->contract->term_id]) 
            OR $techStaffs = implode(', ', array_filter(array_map(function($y) use ($users){
                if(empty($users[$y['user_id']])) return '';
                return $users[$y['user_id']]['display_name'] ?: $users[$y['user_id']]['user_email'];
            }, $kpis[$x->contract->term_id])));

            $saleId     = (int) get_term_meta_value($x->contract_id, 'staff_business');
            $sale       = $this->admin_m->get_field_by_id($saleId);
            $department = $this->term_users_m->get_user_terms($saleId, 'department');
            $department AND $department = reset($department);

            return [
                'contract_code' => get_term_meta_value($x->contract->term_id, 'contract_code'),
                'customer_code' => cid($x->customer->user_id, $x->customer->user_type),
                'customer' => $x->customer->display_name ?? '---',
                'Service' => 'ADSPLUS',
                'payment_type' => $payment_type_enums[$x->payment_type] ?? '',
                'adaccount_id' => $x->adaccount->term_slug,
                'tech' => $techStaffs, // Kỹ thuật
                'contract_daterange' => $x->contract_daterange, // T/G HĐ
                'adaccount_name' => $x->adaccount->term_name,
                'contract_value' => (int) get_term_meta_value($x->contract->term_id, 'contract_value'),
                'start_date'    => my_date($x->start_date, 'd/m/Y'), // Ng.Kích hoạt
                'end_date'      => my_date($x->end_date, 'd/m/Y'), // Ng.Kết thúc
                'term_status' => $this->config->item($x->contract->term_status, 'contract_status'), /*Trạng thái*/
                'remain_budget'     => currency_numberformat($x->remain_budget),
                'department'        => $department->term_name ?? '',
                'sale_name'         => $sale['display_name'],
                'sale_email'        => $sale['user_email'],
                'currency'          => $x->account_currency_code,
                'exchange'          => number_format($x->exchange_rate, 0, ',', ''),
                'cost'              => (int) number_format($x->cost, 2, ',', ''),
                'service_fee'       => (int) number_format($x->cost*$x->service_rate, 2, ',', ''),
                'discount_amount'   => (int) number_format($x->discount_amount, 2, ',', ''),
                'service_fee_rate'  => currency_numberformat($x->service_rate*100, '%'),
                'service_fee_rate_actual'   => currency_numberformat($x->service_fee_rate_actual*100, '%', 2),
            ];

        }, $segments);

        $title          = 'Export dữ liệu Hợp Đồng & Khách hàng đang thực hiện trong tháng ' . date('m', $end_time);
        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));
        
        $sheet = $spreadsheet->getActiveSheet();

        $columns = array(
            'contract_code'     => [ 'type' => 'string', 'label' => 'Mã hợp đồng' ],
            'customer_code'     => [ 'type' => 'string', 'label' => 'Mã Khách Hàng' ],
            'customer'          => [ 'type' => 'string', 'label' => 'Khách hàng' ],
            'Service'           => [ 'type' => 'string', 'label' => 'Dịch vụ' ],
            'payment_type'      => [ 'type' => 'string', 'label' => 'Loại' ],
            'adaccount_id'      => [ 'type' => 'string', 'label' => 'AdAccount ID' ],
            'adaccount_name'    => [ 'type' => 'string', 'label' => 'AdAccount Name' ],
            'tech'              => [ 'type' => 'string', 'label' => 'Kỹ thuật' ],
            'contract_daterange' => [ 'type' => 'string', 'label' => 'T/G HĐ' ],
            'contract_value'    => [ 'type' => 'number', 'label' => 'Giá tị hợp đồng' ],
            'start_date'  => [ 'type' => 'string', 'label' => 'Ngày bắt đầu' ],
            'end_date'    => [ 'type' => 'string', 'label' => 'Ngày kết thúc' ],
            'term_status'       => [ 'type' => 'string', 'label' => 'Trạng thái' ],
            'department'        => [ 'type' => 'string', 'label' => 'Phòng' ],
            'sale_name'         => [ 'type' => 'string', 'label' => 'Tên nhân viên' ],
            'sale_email'        => [ 'type' => 'string', 'label' => 'Email nhân viên' ],
            'currency'          => [ 'type' => 'string', 'label' => 'Currecy' ],
            'remain_budget'     => [ 'type' => 'number', 'label' => 'Ngân sách còn lại' ],
            'exchange'          => [ 'type' => 'number', 'label' => '1 USD = ? VND' ],
            'cost'              => [ 'type' => 'number', 'label' => 'Ngân sách chạy trong tháng' ],
            'service_fee'       => [ 'type' => 'number', 'label' => 'Phí quản lý' ],
            'discount_amount'  => [ 'type' => 'number', 'label' => 'Giảm giá' ],
            'service_fee_rate'  => [ 'type' => 'number', 'label' => '% Phí quản lý' ],
            'service_fee_rate_actual'  => [ 'type' => 'number', 'label' => '% Phí quản lý (thực tế)' ],
        );

        $sheet->fromArray(array_column(array_values($columns), 'label'), NULL, 'A1');

        $rowIndex = 2;

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column)
            {
                switch ($column['type'])
                {
                    case 'number': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }    

                $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item[$key]);
                $colIndex++;
            }

            $rowIndex++;
        }



        $folder_upload  = 'files/contract/report/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                dd($e->getMessage());
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        
        $fileName = implode('', [ $folder_upload, 'facebookads', create_slug($title), time(), '.xlsx' ]);

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }

    public function date_range_get($start = '', $end = '')
    {
        $start_date = DateTime::createFromFormat('Y-m-d', $start);
        $end_date   = DateTime::createFromFormat('Y-m-d', $end);

        if(empty($start_date) || empty($end_date))
        {
            parent::response([ 'code' => 400, 'error' =>'Thời gian truyền vào không hợp lệ. !' ]);
        }

        $start_time = start_of_day($start_date->getTimestamp());
        $end_time = end_of_day($end_date->getTimestamp());

        $iSegments = $this->insight_segment_m
        ->select('post_id, post_name, post_status, comment_status, post_type, start_date, end_date')
        ->set_post_type()
        ->where('post_name' , 'day')
        ->where('start_date >=', $start_time)
        ->where('start_date <=', $end_time)
        ->get_all();

        if(empty($iSegments)) 
        {
            parent::response(['code' => 400, 'error' => 'Hệ thống không tìm thấy bất kỳ dữ liệu nào trong phạm vi này !']);
        }

        $adAccounts = [];

        array_walk($iSegments, function(&$x) use(&$adAccounts){

            $adaccount = $this->term_posts_m->get_post_terms( $x->post_id, $this->adaccount_m->term_type);
            $adaccount AND $adaccount = reset($adaccount);
            $x->adaccount_id    = $adaccount->term_id;
            array_push($adAccounts, $adaccount); 
        });

        $iSegments = array_group_by($iSegments, 'adaccount_id');

        array_walk($adAccounts, function($x) use($iSegments){
            $x->isegments = $iSegments[$x->term_id] ?? [];
        });

        $adAccounts = array_column($adAccounts, NULL, 'term_id');

        $this->load->model('ads_segment_m');

        $segments = $this->ads_segment_m
        ->select('posts.post_id, start_date, end_date, term_posts.term_id')
        ->set_post_type()
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->where_in('term_posts.term_id', array_unique(array_map('intval', array_filter(array_keys($adAccounts)))))
        ->where('start_date <=', $end_time)
        ->where('if(end_date = 0 or end_date is null, UNIX_TIMESTAMP (), end_date) >=', $start_time)
        ->get_all();

        $payment_type_enums = array(
            'normal' => 'Full VAT',
            'customer' => 'Khách hàng tự thanh toán',
            'behalf' => 'ADSPLUS thu & chi hộ'
        );

        $data = array();

        $segmentContracts = $this
            ->facebookads_m
            ->select('term.term_id,term_name,term_type,term_status,term_parent,term_posts.post_id')
            ->set_term_type()
            ->join('term_posts', 'term_posts.term_id = term.term_id')
            ->where_in('term_posts.post_id', array_map('intval', array_column($segments, 'post_id')))
            ->get_all();

        $segmentContracts AND $segmentContracts = array_group_by($segmentContracts, 'post_id');

        foreach ($segments as $key => &$segment)
        {
            if(empty($segmentContracts[$segment->post_id]))
            {
                unset($segments[$key]);
                continue;
            }

            $_contract              = reset($segmentContracts[$segment->post_id]);
            $segment->contract_id   = $_contract->term_id;
            $segment->contract      = $_contract;

            $_adaccount             = $adAccounts[$segment->term_id];
            $segment->adaccount_id  = $_adaccount->term_id;
            $segment->adaccount     = $_adaccount;

            $customer = $this->term_users_m->get_the_users($_contract->term_id, [ 'customer_person', 'customer_company' ]);
            $customer AND $customer = reset($customer);

            $segment->customer_id   = $customer->user_id;
            $segment->customer      = $customer;

            $segment->is_renewal = ! ((bool) get_term_meta_value($_contract->term_id, 'is_first_contract'));

            $segment->payment_type = 'normal';
            if('customer' == get_term_meta_value($_contract->term_id, 'contract_budget_payment_type'))
            {
                $segment->payment_type = 'customer';
                'behalf' == get_term_meta_value($_contract->term_id, 'contract_budget_customer_payment_type') AND $segment->payment_type = 'behalf';
            }

            $interval = (new DateTime())->setTimestamp($segment->start_date)->diff((new DateTime())->setTimestamp($segment->end_date?:end_of_day()));

            $segment->contract_begin        = get_term_meta_value($_contract->term_id, 'contract_begin');
            $segment->contract_end          = get_term_meta_value($_contract->term_id, 'contract_end');
            $segment->contract_daterange    = implode(' - ', array_map(function($x){ return my_date($x, 'd/m/Y'); }, [$segment->contract_begin, $segment->contract_end]));

            /* Contract Durations month */
            $segment->month_durations = number_format($interval->m + ($interval->d/30), 1); 

            $segment->_iSegments = array_filter($_adaccount->isegments, function($x) use($segment){
                return $x->start_date >= $segment->start_date && $x->start_date <= ($segment->end_date ?: end_of_day());
            });

            $segment->_iSegments AND $segment->_iSegments = array_map(function($x){
                $x->spend = (double) get_post_meta_value($x->post_id, 'spend');
                $x->account_currency = get_post_meta_value($x->post_id, 'account_currency');
                return $x;
            }, $segment->_iSegments);

            $segment->cost = 0;
            $segment->account_currency_code = 'VND';
            $segment->exchange_rate = 1;

            if( ! empty($segment->_iSegments))
            {
                $segment->cost = (double) array_sum(array_column($segment->_iSegments, 'spend'));

                $_instance = reset($segment->_iSegments);
                $account_currency = $_instance->account_currency;
                
                if('VND' != $account_currency){
                    $exchange_rate = $_instance->exchange_rate;
                    empty($exchange_rate) AND ($account_currency == 'USD') AND $account_currency = get_term_meta_value($_contract->term_id, 'dollar_exchange_rate');
                    empty($exchange_rate) AND $account_currency = get_term_meta_value($_contract->term_id, strtolower("exchange_rate_{$account_currency}_to_vnd"));
                    empty($exchange_rate) AND $exchange_rate = get_exchange_rate($account_currency);

                    $segment->exchange_rate = $exchange_rate;
                }
            }

            $segment->budget         = (new facebookads_m())->set_contract((object) $_contract)->get_behaviour_m()->calc_budget();
            $segment->service_fee    = (int) get_term_meta_value($_contract->term_id, 'service_fee');
            $segment->service_rate   = div($segment->service_fee, $segment->budget);
            
            $segment->actual_result  = (int) get_term_meta_value($_contract->term_id, 'actual_result');
            $segment->actual_budget  = (int) get_term_meta_value($_contract->term_id, 'actual_budget');
            $segment->remain_budget  = $segment->actual_budget - $segment->actual_result;
        }

        /* LOAD ALL STAFFS OF ADS CONTRACTS */
        $this->load->model('googleads/googleads_kpi_m');
        $kpis = $this
            ->googleads_kpi_m
            ->select('term_id, user_id')
            ->where_in('term_id', array_unique(array_column($segments, 'contract_id')))
            ->setTechnicalKpiTypes()
            ->group_by('term_id')
            ->as_array()
            ->get_all();

        $users = array();
        if($kpis)
        {
            $userIds = array_unique(array_column($kpis, 'user_id'));
            foreach ($userIds as $userId)
            {
                $users[$userId] = $this->admin_m->get_field_by_id($userId);
            } 
            $kpis AND $kpis = array_group_by($kpis, 'term_id');
        }
        /* LOAD ALL STAFFS OF ADS CONTRACTS - END */

        $data = array_map(function($x) use($payment_type_enums, $users, $kpis){

            /* PARSE TECHNICAL STAFFS */
            $techStaffs = "";
            empty($kpis[$x->contract->term_id]) 
            OR $techStaffs = implode(', ', array_filter(array_map(function($y) use ($users){
                if(empty($users[$y['user_id']])) return '';
                return $users[$y['user_id']]['display_name'] ?: $users[$y['user_id']]['user_email'];
            }, $kpis[$x->contract->term_id])));

            $saleId     = (int) get_term_meta_value($x->contract_id, 'staff_business');
            $sale       = $this->admin_m->get_field_by_id($saleId);
            $department = $this->term_users_m->get_user_terms($saleId, 'department');
            $department AND $department = reset($department);

            return [
                'contract_code' => get_term_meta_value($x->contract->term_id, 'contract_code'),
                'customer' => $x->customer->display_name ?? '---',
                'Service' => 'ADSPLUS',
                'payment_type' => $payment_type_enums[$x->payment_type] ?? '',
                'adaccount_id' => $x->adaccount->term_slug,
                'tech' => $techStaffs, // Kỹ thuật
                'contract_daterange' => $x->contract_daterange, // T/G HĐ
                'adaccount_name' => $x->adaccount->term_name,
                'contract_value' => (int) get_term_meta_value($x->contract->term_id, 'contract_value'),
                'start_date'    => my_date($x->start_date, 'd/m/Y'), // Ng.Kích hoạt
                'end_date'      => my_date($x->end_date, 'd/m/Y'), // Ng.Kết thúc
                'term_status' => $this->config->item($x->contract->term_status, 'contract_status'), /*Trạng thái*/
                'remain_budget'     => currency_numberformat($x->remain_budget),
                'department'        => $department->term_name ?? '',
                'sale_name'         => $sale['display_name'],
                'sale_email'        => $sale['user_email'],
                'currency'          => $x->account_currency_code,
                'exchange'          => number_format($x->exchange_rate, 0, ',', ''),
                'cost'              => (int) number_format($x->cost, 2, ',', ''),
                'service_fee'       => (int) number_format($x->cost*$x->service_rate, 2, ',', ''),
                'service_fee_rate'  => currency_numberformat($x->service_rate*100, '%'),
            ];

        }, $segments);

        $title          = 'Export dữ liệu Hợp Đồng & Khách hàng đang thực hiện trong tháng ' . date('m', $end_time);
        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));
        
        $sheet = $spreadsheet->getActiveSheet();

        $columns = array(
            'contract_code'     => [ 'type' => 'string', 'label' => 'Mã hợp đồng' ],
            'customer'          => [ 'type' => 'string', 'label' => 'Khách hàng' ],
            'Service'           => [ 'type' => 'string', 'label' => 'Dịch vụ' ],
            'payment_type'      => [ 'type' => 'string', 'label' => 'Loại' ],
            'adaccount_id'      => [ 'type' => 'string', 'label' => 'AdAccount ID' ],
            'adaccount_name'    => [ 'type' => 'string', 'label' => 'AdAccount Name' ],
            'tech'              => [ 'type' => 'string', 'label' => 'Kỹ thuật' ],
            'contract_daterange' => [ 'type' => 'string', 'label' => 'T/G HĐ' ],
            'contract_value'    => [ 'type' => 'number', 'label' => 'Giá tị hợp đồng' ],
            'start_date'  => [ 'type' => 'string', 'label' => 'Ngày bắt đầu' ],
            'end_date'    => [ 'type' => 'string', 'label' => 'Ngày kết thúc' ],
            'term_status'       => [ 'type' => 'string', 'label' => 'Trạng thái' ],
            'department'        => [ 'type' => 'string', 'label' => 'Phòng' ],
            'sale_name'         => [ 'type' => 'string', 'label' => 'Tên nhân viên' ],
            'sale_email'        => [ 'type' => 'string', 'label' => 'Email nhân viên' ],
            'currency'          => [ 'type' => 'string', 'label' => 'Currecy' ],
            'remain_budget'     => [ 'type' => 'number', 'label' => 'Ngân sách còn lại' ],
            'exchange'          => [ 'type' => 'number', 'label' => '1 USD = ? VND' ],
            'cost'              => [ 'type' => 'number', 'label' => 'Ngân sách chạy trong tháng' ],
            'service_fee'       => [ 'type' => 'number', 'label' => 'Phí quản lý' ],
            'service_fee_rate'  => [ 'type' => 'number', 'label' => '% Phí quản lý' ],
        );

        $sheet->fromArray(array_column(array_values($columns), 'label'), NULL, 'A1');

        $rowIndex = 2;

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column)
            {
                switch ($column['type'])
                {
                    case 'number': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }    

                $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item[$key]);
                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file Report.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/Report.php */