<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends MREST_Controller
{
	/**
     * API dữ liệu các đợt thanh toán
     */
    public function items_get($key = '')
    {
    	$this->config->load('facebookads/facebookads', TRUE);
        $this->config->load('facebookads/contract', TRUE);

    	$result = array_merge($this->config->item('facebookads'), $this->config->item('contract'));

    	if(!empty($key)) $result = $this->config->item($key, 'facebookads');

    	parent::response([
    		'status' => TRUE, 
    		'data' => $result
    	]);
    }

    public function options_get($key = '')
    {
        parent::response([
            'status' => true,
            'data' => $this->option_m->get_value($key)
        ]);
    }
}
/* End of file Config.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/Config.php */