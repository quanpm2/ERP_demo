<?php defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetInsights extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';

        parent::__construct();

        $this->load->config('contract/receipt');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {    
        $response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
        if( ! has_permission('facebookads.overview.access'))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $args = wp_parse_args( parent::get(null, true), [
            'offset' => 0,
            'per_page' => 20,
            'cur_page' => 1,
            'is_filtering' => true,
            'is_ordering' => true
        ]);

        $this->filtering();

        $extra = [];
        $contractId = $args['where']['contractId'] ?? FALSE;
        if(!empty($contractId))
        {
            $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
            $manipulation_locked['is_manipulation_locked'] = (bool) $manipulation_locked['is_manipulation_locked']
                                    && TRUE == (bool)get_term_meta_value($contractId, 'is_manipulation_locked');
            $extra['manipulation_locked'] = $manipulation_locked;
        }

        $this->datatable_builder
        ->set_filter_position(FILTER_TOP_INNER)
        ->setOutputFormat('JSON')

        ->from('term')
        ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
        ->join('posts AS balance_spend', 'balance_spend.post_id = tp_contract_ads_segment.post_id AND balance_spend.post_type = "balance_spend"', 'LEFT')  
        ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'LEFT')  
        ->join('term_posts AS tp_segment_adaccount', 'tp_segment_adaccount.post_id = ads_segment.post_id', 'LEFT')  
        ->join('term AS adaccount', 'tp_segment_adaccount.term_id = adaccount.term_id AND adaccount.term_type = "adaccount"', 'LEFT')  
        ->join('termmeta AS adaccount_metadata', 'adaccount_metadata.term_id = adaccount.term_id AND meta_key = "source"', 'LEFT')  
        ->join('term_posts AS tp_adaccount_insights', 'tp_adaccount_insights.term_id = adaccount.term_id', 'LEFT')  
        ->join('posts AS insights', 'tp_adaccount_insights.post_id = insights.post_id AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) AND insights.start_date <= if(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) AND insights.post_type = "insight_segment" AND insights.post_name = "day"', 'LEFT')
        ->join('postmeta AS insight_metadata', 'insight_metadata.post_id = insights.post_id AND insight_metadata.meta_key IN ("account_currency", "clicks", "cpc", "cpm", "cpp", "ctr", "impressions", "reach", "spend")', 'LEFT')
        
        ->where('term.term_type', 'facebook-ads') 
        ->where('( insights.post_id > 0 or balance_spend.post_id > 0 )')
        ->group_by('balance_spend.post_id, insights.post_id')
        
        ->order_by('balance_spend.end_date', 'DESC')
        ->order_by('insights.start_date', 'DESC')

        ->select("term.term_id AS contractId")
        ->select("term.term_name AS contractUrl")
        ->select("term.term_type AS contractType")
        ->select("term.term_status AS contractStatus")
        ->select("COALESCE(ads_segment.post_id, balance_spend.post_id) AS segmentId")
        ->select("COALESCE(ads_segment.post_type, balance_spend.post_type) AS segmentType")
        ->select("COALESCE(ads_segment.post_status, balance_spend.post_status) AS segmentStatus")
        ->select("COALESCE(ads_segment.start_date, balance_spend.start_date) AS segmentStartDate")
        ->select("COALESCE(ads_segment.end_date, balance_spend.end_date) AS segmentEndDate")
        ->select("adaccount.term_id AS adAccountTermId")
        ->select("adaccount.term_slug AS adAccountId")
        ->select("adaccount.term_name AS adAccountName")
        ->select("adaccount.term_type AS adAccountType")
        ->select("adaccount.term_status AS adAccountStatus")
        ->select("MAX(IF(adaccount_metadata.meta_key = 'source', adaccount_metadata.meta_value, null)) AS adAccountSource")
        ->select("insights.post_id AS insightId")
        ->select("COALESCE(insights.post_name, balance_spend.post_name) AS insightSegmentType")
        ->select("insights.post_type AS insightType")
        ->select("insights.post_status AS insightStatus")
        ->select("insights.created_on AS insightCreatedOn")
        ->select("insights.updated_on AS insightUpdatedOn")
        ->select("COALESCE(insights.start_date, balance_spend.start_date) AS insightStartDate")
        ->select("COALESCE(MAX(IF(adaccount_metadata.meta_key = 'source', adaccount_metadata.meta_value, null)), balance_spend.comment_status) AS insightSource")
        ->select("MAX(IF(insight_metadata.meta_key = 'account_currency', insight_metadata.meta_value, null)) AS account_currency")
        ->select("MAX(IF(insight_metadata.meta_key = 'clicks', insight_metadata.meta_value, null)) AS clicks")
        ->select("MAX(IF(insight_metadata.meta_key = 'cpc', insight_metadata.meta_value, null)) AS cpc")
        ->select("MAX(IF(insight_metadata.meta_key = 'cpm', insight_metadata.meta_value, null)) AS cpm")
        ->select("MAX(IF(insight_metadata.meta_key = 'cpp', insight_metadata.meta_value, null)) AS cpp")
        ->select("MAX(IF(insight_metadata.meta_key = 'ctr', insight_metadata.meta_value, null)) AS ctr")
        ->select("MAX(IF(insight_metadata.meta_key = 'impressions', insight_metadata.meta_value, null)) AS impressions")
        ->select("MAX(IF(insight_metadata.meta_key = 'reach', insight_metadata.meta_value, null)) AS reach")
        ->select("COALESCE(MAX(IF(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, null)), balance_spend.post_content) AS spend")
        ->select("tp_contract_ads_segment.post_id")
        ->select("balance_spend.post_excerpt AS note")

        ->add_search('insightStartDate',['placeholder'=>'Ngày','class'=>'form-control set-datepicker'])
        ->add_search('adAccountId',['placeholder'=>'ID Tài khoản'])
        ->add_search('adAccountName',['placeholder'=>'Tên tài khoản'])
        ->add_search('segment_technician',['placeholder'=>'Kỹ thuật phụ trách'])

        ->add_column('insightStartDate', array( 'set_select' => FALSE, 'title' => 'Ngày'))
        ->add_column('adAccountId', array( 'set_select' => FALSE, 'title' => 'AdAccount ID'))
        ->add_column('adAccountName', array( 'set_select' => FALSE, 'title' => 'AdAcount'))
        ->add_column('adAccountStatus', array( 'set_select' => FALSE, 'title' => 'Trạng thái'))
        ->add_column('adAccountSource', array( 'set_select' => FALSE, 'title' => 'Nguồn'))
        ->add_column('segmentStatus', array( 'set_select' => FALSE, 'title' => 'TT Phân đoạn'))
        ->add_column('segmentStartDate', array( 'set_select' => FALSE, 'title' => 'Ng. bắt đầu'))
        ->add_column('segmentEndDate', array( 'set_select' => FALSE, 'title' => 'Ng. kết thúc'))
        ->add_column('insightSource', array( 'set_select' => FALSE, 'title' => 'Nguồn số liệu'))
        ->add_column('account_currency', array( 'set_select' => FALSE, 'title' => 'Đơn vị tiền'))
        ->add_column('spend', array( 'set_select' => FALSE, 'title' => 'Chi tiêu'))
        ->add_column('clicks', array( 'set_select' => FALSE, 'title' => 'Clicks'))
        ->add_column('impressions', array( 'set_select' => FALSE, 'title' => 'Hiển thị'))
        ->add_column('reach', array( 'set_select' => FALSE, 'title' => 'Reach'))
        ->add_column('ctr', array( 'set_select' => FALSE, 'title' => 'CTR'))
        ->add_column('cpc', array( 'set_select' => FALSE, 'title' => 'CPC'))
        ->add_column('cpm', array( 'set_select' => FALSE, 'title' => 'CPM'))
        ->add_column('cpp', array( 'set_select' => FALSE, 'title' => 'CPP'))
        ->add_column('insightCreatedOn', array('title' => 'Ngày tạo', 'set_select' => FALSE, 'set_order' => TRUE))
        ->add_column('insightUpdatedOn', array('title' => 'Ngày cập nhật', 'set_select' => FALSE, 'set_order' => TRUE))

        ->add_column_callback('segmentType', function ($data, $row_name) use ($extra){
            $data['extra'] = $extra;

            if('balance_spend' != $data['segmentType'])
            {
                return $data;
            }

            $data['adAccountId'] = 'Theo hợp đồng';
            $data['adAccountName'] = 'Cân bằng thủ công';

            if('auto' == $data['insightSource'])
            {
                $data['adAccountName'] = 'Chưa xác minh hợp đồng nối';

                if(empty($data['note']))
                {
                    return $data;
                }

                $data['adAccountName'] = $data['note'];
            }

            return $data;
        }, FALSE);

        $pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];
        $data = $this->datatable_builder->generate($pagination_config);

        $subheadings_data = $this->datatable_builder->generate(['per_page' => PHP_INT_MAX, 'cur_page' => 1]);
        $subheadings_data = $subheadings_data['rows'];
        $subheadings = array_reduce($subheadings_data, function ($result, $item) {
            $result['spend'] += (double)$item['spend'];
            $result['clicks'] += (int)$item['clicks'];
            $result['impressions'] += (int)$item['impressions'];
            $result['reach'] += (int)$item['reach'];

            return $result;
        }, [
            'spend' => 0,
            'clicks' => 0,
            'impressions' => 0,
            'reach' => 0,
        ]);
        $subheadings = array_map(function ($item) {
            $item .= '';
            return $item;
        }, $subheadings);
        $data['subheadings'] = $subheadings;
        
        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']))
        {
            $this->export_xlsx();
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering()
    {
        restrict('facebookads.overview.access');

        $args = parent::get(NULL, TRUE);
        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

        $contractId = $args['where']['contractId'] ?? FALSE;
		if($contractId)
		{
            $this->datatable_builder->where("term.term_id", (int) $contractId);
            unset($args['where']['contractId']);
		}

		$insightStartDate = $args['where']['insightStartDate'] ?? FALSE;
		if($insightStartDate)
		{
            $dates = explode(' - ', $insightStartDate);
            if('Invalid date' != $dates[0] && 'Invalid date' != $dates[1]) {
                $start_date = start_of_day(reset($dates));
                $end_date = end_of_day(end($dates));
                
                $this->datatable_builder->where("
                    ((insights.start_date >= {$start_date} AND insights.end_date <= {$end_date})
                    OR
                    (balance_spend.start_date >= {$start_date} AND balance_spend.start_date <= {$end_date}))
                ");
            }
            unset($args['where']['insightStartDate']);
		}

		$adAccountId = $args['where']['adAccountId'] ?? FALSE;
		if($adAccountId)
		{
            $dates = explode(' - ', $adAccountId);
            $this->datatable_builder->where("adaccount.term_slug like '%" . $this->db->escape_like_str(trim($adAccountId)) . "%'");
            unset($args['where']['adAccountId']);
		}

        $adAccountName = $args['where']['adAccountName'] ?? FALSE;
		if($adAccountName)
		{
            $dates = explode(' - ', $adAccountName);
            $this->datatable_builder->where("adaccount.term_name like '%" . $this->db->escape_like_str(trim($adAccountName)) . "%'");
            unset($args['where']['adAccountName']);
		}

        $segment_technician = $args['where']['segment_technician'] ?? FALSE;
        if($segment_technician)
        {
            $this->datatable_builder
            ->join('user', 'user.user_id = ads_segment.post_author AND user.user_type = "admin"')
            ->where("user.display_name like '%" . $this->db->escape_like_str(trim($segment_technician)) . "%'");
            unset($args['where']['segment_technician']);
            $args['cur_page'] = 1;
        }

        if(!empty($args['where'])) 
        {
            foreach ($args['where'] as $key => $value)
            {
                if(empty($value)) continue;
                if(!empty($key)) $this->datatable_builder->where($key, $value);
            }
        }
        if(!empty($args['order_by'])) 
        {
            foreach ($args['order_by'] as $key => $value)
            {
                $this->datatable_builder->order_by("[custom-select].{$key}", $value);
            }
        }
    }

    
    /**
     * Export the data to an Excel file (XLSX) and force download it.
     *
     * @throws Exception if there is an error creating or saving the Excel file.
     * @return bool true if the export is successful, false otherwise.
     */
    public function export_xlsx()
    {
        restrict('facebookads.overview.access');

        $data = $this->datatable_builder->generate(['per_page' => PHP_INT_MAX, 'cur_page' => 1]);
        $data_rows = $data['rows'] ?: [];
        if (!$data_rows) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $title          = my_date(time(), 'Ymd') . '_export_facebook_insight_spend';
        $spreadsheet    = IOFactory::load('./files/excel_tpl/insight/facebook_insight_export.xlsx');
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));

        $sheet = $spreadsheet->getActiveSheet();
        $rowIndex = 4;

        foreach ($data_rows as $data_item) {
            $col = 1;

            $insightStartDate = isset($data_item['insightStartDate']) ? my_date($data_item['insightStartDate'], 'd/m/Y') : '--';
            $segmentStartDate = isset($data_item['segmentStartDate']) ? my_date($data_item['segmentStartDate'], 'd/m/Y') : '--';
            $segmentEndDate = isset($data_item['segmentEndDate']) ? my_date($data_item['segmentEndDate'], 'd/m/Y') : '--';

            $adAccountId = $data_item['adAccountId'];
            if('balance_spend' == $data_item['segmentType']) $adAccountId = 'Theo hợp đồng';
            
            $adAccountName = $data_item['adAccountName'];
            if('balance_spend' == $data_item['segmentType']) $adAccountName = 'Cân bằng thủ công';

            $adAccountStatusEnums = [
                'unlinked' => 'Mất kết nối',
                'active' => 'Đang hoạt động',
                'unauthorized' => 'Đã xác thực (I)',
            ];
            $adAccountStatus = $data_item['adAccountStatus'];
            $adAccountStatus = $adAccountStatusEnums[$adAccountStatus] ?? $adAccountStatus;

            $adAccountSourceEnums = [
                'linked' => 'Ủy Quyền',
                'direct' => 'Tạo Trực Tiếp',
            ];
            $adAccountSource = $data_item['adAccountSource'];
            $adAccountSource = $adAccountSourceEnums[$adAccountSource] ?? $adAccountSource;

            $insightSourceEnums = [
                'direct' => 'Thủ công',
            ];
            $insightSource = $data_item['insightSource'];
            $insightSource = $insightSourceEnums[$insightSource] ?? '--';

            // Set Cell
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $insightStartDate);
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $adAccountId ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $adAccountName ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $adAccountStatus ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $adAccountSource ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item['segmentStatus'] ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $segmentStartDate);
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $segmentEndDate);
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $insightSource);
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item['account_currency'] ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item['spend'] ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item['clicks'] ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item['impressions'] ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item['reach'] ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item['ctr'] ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item['cpc'] ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item['cpm'] ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item['cpp'] ?? '--');

            $rowIndex++;
        }

        $numbers_of_data = count($data);
        // Set Cells style for Date
        $sheet->getStyle('A' . $rowIndex . ':A' . ($numbers_of_data + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
        $sheet->getStyle('G' . $rowIndex . ':H' . ($numbers_of_data + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);

        // Set Cells Style Currency
        $sheet->getStyle('K' . $rowIndex . ':R' . ($numbers_of_data + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        $folder_upload  = 'files/insight/facebook/';
        if (!is_dir($folder_upload)) {
            try {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}/{$title}.xlsx";
        try {
            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save($fileName);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetReceipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetReceipts.php */