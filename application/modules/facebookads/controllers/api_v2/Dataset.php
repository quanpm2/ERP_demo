<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataset extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'facebookads/facebookads_m';
        $this->autoload['models'][] = 'facebookads/adaccount_m';
        $this->autoload['models'][] = 'facebookads/facebookads_kpi_m';
        $this->autoload['models'][] = 'ads_segment_m';
        parent::__construct($config);

        $this->load->config('facebookads/facebookads');
        $this->load->config('facebookads/contract');
    }


    /**
     * Render dataset of FB's services
     */
    public function index_get()
    {
        $response   = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
        $permission = 'facebookads.index.access';

        if( ! has_permission($permission))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        $columns = $this->facebookads_m->get_field_config();
        $default = array(
            'offset'        => 0,
            'per_page'      => 10,
            'cur_page'      => 1,
            'is_filtering'  => TRUE,
            'is_ordering'   => TRUE,
            'filter_position'   => FILTER_TOP_OUTTER,
            'columns'       => implode(',', $columns['default_columns'])
        );

        $args               = wp_parse_args( $this->input->get(), $default);
        $args['columns']    = explode(',', $args['columns']);
        
        $this->load->library('datatable_builder');

        $relate_users       = $this->admin_m->get_all_by_permissions($permission);
        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        if($relate_users === FALSE)
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        if(is_array($relate_users))
        {
            $this->datatable_builder->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
        }

        /* Applies get query params for filter */
        $this->search_filter();
        $this->template->title->append('Tổng quan dịch vụ đang thực hiện ADSPLUS');
        if( ! empty($args['columns']))
        {
            foreach ($args['columns'] as $key)
            {
                if(empty($columns['columns'][$key])) continue;
                $this->datatable_builder->add_column($columns['columns'][$key]['name'], $columns['columns'][$key]);
            }    
        }
        $callbacks = $this->facebookads_m->get_field_callback();
        if($callbacks)
        {
            foreach ($callbacks as $callback)
            {
                $this->datatable_builder->add_column_callback($callback['field'], $callback['func'], $callback['row_data']);
            }    
        }

        $data =  $this->datatable_builder
        ->select('term.term_id,term.term_status,term.term_name,term.term_type')
        ->set_filter_position($args['filter_position'])
        ->add_search('term.term_id')
        ->add_search('contract_code')
        ->add_search('customer_code')
        ->add_search('status',array('content'=> form_dropdown(array('name'=>'where[term_status]','class'=>'form-control select2'),prepare_dropdown($this->config->item('status','facebookads'),'Trạng thái : All'),$this->input->get('where[term_status]'))))
        ->add_search('service_fee_payment_type',array('content'=> form_dropdown(array('name'=>'where[service_fee_payment_type]','class'=>'form-control select2'),prepare_dropdown($this->config->item('service_fee_payment_type'),'PTTV.PDV: All'),$this->input->get('where[service_fee_payment_type]'))))

        ->add_search('term_status',array('content'=> form_dropdown(array('name'=>'where[term_status]','class'=>'form-control select2'),prepare_dropdown($this->config->item('status','facebookads'),'Trạng thái : All'),$this->input->get('where[term_status]'))))
        ->add_search('fb_staff_advertise')
        ->add_search('staff_business')
        ->add_search('is_service_proc', array('content'=> form_dropdown(array('name'=>'where[is_service_proc]', 'class'=>'form-control select2'),prepare_dropdown(['active' => 'Đã kích hoạt', 'unactive' => 'Chưa kích hoạt'],'TT Kích hoạt : All'), $this->input->get('where[is_service_proc]'))))
        ->add_search('adaccount_status', array('content' => form_dropdown([ 'name'  =>'where[adaccount_status]', 'class' =>'form-control select2'],
            prepare_dropdown($this->config->item('states', 'adaccount_status'), 'Trạng thái : All'),
            $this->input->get('where[adaccount_status]'))))

        ->select('term.*')
        ->from('term')
        ->join('termmeta started_service', 'started_service.term_id = term.term_id', 'INNER')
        ->where('term.term_type', $this->facebookads_m->term_type)
        ->where_in('term.term_status', array_keys($this->config->item('status','facebookads')))
        ->where('started_service.meta_key ','started_service')
        ->group_by('term.term_id')
        ->generate(array('per_page'=>$args['per_page'],'cur_page'=>$args['cur_page']));

        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->export($last_query);
            return TRUE;
        }
        
        parent::response($data);
    }


    /**
     * Export Excel Dataset
     *
     * @param      string  $query  The query
     */
    protected function export($query = '')
    {
        if(empty($query)) return FALSE;

        // remove limit in query string
        $query = explode('LIMIT', $query);
        $query = reset($query);
        
        $terms = $this->contract_m->query($query)->result();

        if( ! $terms) return FALSE;

        $this->config->load('contract/fields');
        $default    = array('columns' => implode(',', $this->config->item('default_columns', 'datasource')));
        $args       = wp_parse_args( $this->input->get(), $default);
        $args['columns'] = explode(',', $args['columns']);

        $this->load->library('excel');
        $cacheMethod    = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings  = array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("WEBDOCTOR.VN")->setLastModifiedBy("WEBDOCTOR.VN")->setTitle(uniqid('Danh sách hợp đồng __'));
        $objPHPExcel->setActiveSheetIndex(0);

        $objWorksheet   = $objPHPExcel->getActiveSheet();
        $callbacks      = $this->facebookads_m->get_field_callback();
        $field_config   = $this->facebookads_m->get_field_config();

        $args['columns'] = array(
            'contract_code',
            'service_fee_payment_type',
            'service_fee_rate_actual',
            'actual_result',
            'actual_budget',
            'payment_expected_end_time',
            'payment_real_progress',
            'payment_cost_per_day_left',
            'actual_progress_percent_net',
            'expected_end_time',
            'real_progress',
            'cost_per_day_left',
            'actual_progress_percent',
            'result_updated_on',
            'fb_staff_advertise',
            'staff_business',
            'status',
            'adaccount_status',
        );

        $headings = array_map(function($x) use($field_config){
            return $field_config['columns'][$x]['title']??$x;
        }, $args['columns']);

        $objWorksheet->fromArray($headings, NULL, 'A1');
        $row_index = 2;
        foreach ($terms as $key => &$term)
        {
            $term = (array) $term;
            foreach ($args['columns'] as $column)
            {
                if(empty($field_config['columns'][$column])) continue;
                if(empty($callbacks[$column])) continue;

                $term = call_user_func_array($callbacks[$column]['func'], array($term, $column, []));
            }

            $i = 0;
            $row_number = $row_index + $key;

            foreach ($args['columns'] as $column)
            {
                if(empty($field_config['columns'][$column])) continue;

                $value = $term["{$column}_raw"] ?? ($term["{$column}"] ?? '');
                $col_number = $i;
                $i++;
                
                switch ($field_config['columns'][$column]['type'])
                {
                    case 'string' : 
                        $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
                        break;

                    case 'number': 
                        $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = PHPExcel_Shared_Date::PHPToExcel($value);
                        $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $value);
            }
        }

        $num_rows = count($terms);
        
        // We'll be outputting an excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $folder_upload = "files/contracts/";
        if(!is_dir($folder_upload))
        {
            try 
            {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $created_datetime   = my_date(time(),'Y-m-d-H-i-s');
        $file_name          = "{$folder_upload}danh-sach-hop-dong-facebook-ads-{$created_datetime}.xlsx";

        try { $objWriter->save($file_name); }
        catch (Exception $e) { trigger_error($e->getMessage()); return FALSE;  }

        $this->load->helper('download');
        force_download($file_name, NULL);
        return TRUE;
    }

    protected function search_filter($search_args = array())
    {   
        $args = $this->input->get();

        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

        // Contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if($filter_contract_code || $sort_contract_code)
        {
            $alias = uniqid('contract_code_');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

            if($filter_contract_code)
            {   
                $this->datatable_builder->like("{$alias}.meta_value",$filter_contract_code);
            }

            if($sort_contract_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_code);
            }

            unset($args['where']['contract_code'],$args['order_by']['contract_code']);
        }

        if( ! empty($args['where']['started_service']))
        {
            $explode = explode(' - ', $args['where']['started_service']);
            if( ! empty($explode))
            {
                $this->datatable_builder
                ->join('termmeta AS tm_stated_service', 'tm_stated_service.term_id = term.term_id', 'LEFT')
                ->where('tm_stated_service.meta_key','started_service')
                ->where('tm_stated_service.meta_value >=', start_of_day(reset($explode)))
                ->where('tm_stated_service.meta_value <=', end_of_day(end($explode)));
            }
            unset($args['where']['started_service']);
        }

        if( ! empty($args['order_by']['started_service']))
        {
            $this->datatable_builder->join('termmeta AS tm_stated_service', 'tm_stated_service.term_id = term.term_id', 'LEFT');
            $args['where']['tm_stated_service.meta_key'] = 'started_service';
            $args['order_by']['tm_stated_service.meta_value'] = $args['order_by']['started_service'];
            unset($args['order_by']['started_service']);
        }

        // customer_code FILTERING & SORTING
        $filter_customer_code = $args['where']['customer_code'] ?? FALSE;
        $sort_customer_code   = $args['order_by']['customer_code'] ?? FALSE;
        if($filter_customer_code || $sort_customer_code)
        {
            $alias = uniqid('customer_code_');
            $this->datatable_builder
            ->join("term_users tu_customer","tu_customer.term_id = term.term_id")
            ->join("user customer","customer.user_id = tu_customer.user_id AND customer.user_type IN ('customer_person', 'customer_company')")
            ->join("usermeta {$alias}","{$alias}.user_id = customer.user_id and {$alias}.meta_key = 'cid'", 'LEFT');

            if($filter_customer_code)
            {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_customer_code);
                unset($args['where']['customer_code']);
            }

            if($sort_customer_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_customer_code);
                unset($args['order_by']['customer_code']);
            }
        }

        // Staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if($filter_staff_business || $sort_staff_business)
        {
            $alias = uniqid('staff_business_');
            $this->datatable_builder
            ->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT')
            ->join("user tblcustomer","{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

            if($filter_staff_business)
            {   
                $this->datatable_builder->like("tblcustomer.display_name",$filter_staff_business);
                unset($args['where']['staff_business']);
            }

            if($sort_staff_business)
            {
                $this->datatable_builder->order_by('tblcustomer.display_name',$sort_staff_business);
                unset($args['order_by']['staff_business']);
            }
        }

        // is_service_proc FILTERING & SORTING
        $filter_is_service_proc = $args['where']['is_service_proc'] ?? FALSE;
        if($filter_is_service_proc)
        {
            $alias  = uniqid('start_service_time_');
            $type   = $filter_is_service_proc == 'active' ? '' : 'LEFT';
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'start_service_time'", $type);

            'active' == $filter_is_service_proc 
            ? $this->datatable_builder->where("({$alias}.meta_value is not NULL AND {$alias}.meta_value != '' AND {$alias}.meta_value > 0)")
            : $this->datatable_builder->where("({$alias}.meta_value is null or {$alias}.meta_value = '' OR {$alias}.meta_value = 0)");

            unset($args['where']['is_service_proc']);
        }

        // has_adaccount FILTERING & SORTING
        $filter_has_adaccount = $args['where']['has_adaccount'] ?? FALSE;
        if($filter_has_adaccount)
        {
            $alias  = uniqid('adaccount_id_');
            $type   = $filter_has_adaccount == 'active' ? '' : 'LEFT';
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'adaccount_id'", $type);

            'active' == $filter_has_adaccount 
            ? $this->datatable_builder->where("({$alias}.meta_value != '' OR {$alias}.meta_value > 0 )")
            // : $this->datatable_builder->where("{$alias}.meta_value in (NULL, '', 0)");
            : $this->datatable_builder->where("({$alias}.meta_value is null or {$alias}.meta_value = '' OR {$alias}.meta_value = 0)");
            
            unset($args['where']['has_adaccount']);
        }

        // adaccount_status FILTERING & SORTING
        $filter_adaccount_status    = $args['where']['adaccount_status'] ?? FALSE;
        $sort_adaccount_status      = $args['order_by']['adaccount_status'] ?? FALSE;
        if($filter_adaccount_status || $sort_adaccount_status)
        {
            $alias = uniqid('adaccount_status_');
            $this->datatable_builder
            ->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'adaccount_status'", 'LEFT');

            if($filter_adaccount_status)
            {   
                $this->datatable_builder->like("{$alias}.meta_value", $filter_adaccount_status);
                unset($args['where']['adaccount_status']);
            }

            if($sort_adaccount_status)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_adaccount_status);
                unset($args['order_by']['adaccount_status']);
            }
        }

        /* Sort for actual progress goal */
        if(!empty($args['order_by']['actual_progress_percent_net']))
        {
            $alias = uniqid('actual_progress_percent_net');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_progress_percent'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['actual_progress_percent_net'];
            unset($args['order_by']['actual_progress_percent_net']);
        }

        /* Sort for actual progress goal */
        if(!empty($args['order_by']['actual_progress_percent']))
        {
            $alias = uniqid('actual_progress_percent');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_progress_percent'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['actual_progress_percent'];
            unset($args['order_by']['actual_progress_percent']);
        }

        /* Sort for actual progress goal */
        if(!empty($args['order_by']['real_progress']))
        {
            $alias = uniqid('real_progress');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'real_progress'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['real_progress'];
            unset($args['order_by']['real_progress']);
        }

        /* Sort for actual progress goal */
        if(!empty($args['order_by']['payment_real_progress']))
        {
            $alias = uniqid('payment_real_progress');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_real_progress'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_real_progress'];
            unset($args['order_by']['payment_real_progress']);
        }

        /* Sort for actual progress goal */
        if(!empty($args['order_by']['result_updated_on']))
        {
            $alias = uniqid('result_updated_on');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'result_updated_on'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['result_updated_on'];
            unset($args['order_by']['result_updated_on']);
        }

        if(!empty($args['order_by']['actual_result']))
        {
            $alias = uniqid('actual_result');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_result'", 'LEFT OUTER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['actual_result'];
            unset($args['order_by']['actual_result']);
        }

        // FIND AND SORT service_fee_payment_type
		$filter_service_fee_payment_type = $args['where']['service_fee_payment_type'] ?? FALSE;
		$sort_service_fee_payment_type = $args['order_by']['service_fee_payment_type'] ?? FALSE;
		if($filter_service_fee_payment_type || $sort_service_fee_payment_type)
		{
            $alias = uniqid('service_fee_payment_type_');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'service_fee_payment_type'", 'LEFT');

			if($filter_service_fee_payment_type)
			{	
				$this->datatable_builder->where("{$alias}.meta_value", $filter_service_fee_payment_type);
				unset($args['where']['service_fee_payment_type']);
			}

			if($sort_service_fee_payment_type)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value", $sort_service_fee_payment_type);
				unset($args['order_by']['service_fee_payment_type']);
			}
		}

        // FIND AND SORT STAFF_ADVERTISE WITH FACEBOOK_KPI
        $filter_fb_staff_advertise = $args['where']['fb_staff_advertise'] ?? FALSE;
        $sort_fb_staff_advertise = $args['order_by']['fb_fb_staff_advertise'] ?? FALSE;
        if($filter_fb_staff_advertise || $sort_fb_staff_advertise)
        {
            $this->datatable_builder->join('googleads_kpi','term.term_id = googleads_kpi.term_id', 'LEFT OUTER');
            $this->datatable_builder->join('user','user.user_id = googleads_kpi.user_id', 'LEFT OUTER');

            if($filter_fb_staff_advertise)
            {   
                $this->datatable_builder->like('user.user_email',strtolower($filter_fb_staff_advertise),'both');
            }

            if($sort_fb_staff_advertise)
            {
                $this->datatable_builder->order_by('user.user_email',$sort_fb_staff_advertise);
            }

            unset($args['where']['fb_staff_advertise'],$args['order_by']['fb_staff_advertise']);
        }

        if( ! empty($args['order_by']['payment_percentage']))
        {
            $alias = uniqid('payment_percentage');
            $this->datatable_builder
            ->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_percentage'", 'INNER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_percentage'];
            unset($args['order_by']['payment_percentage']);
        }

        if( ! empty($args['order_by']['payment_cost_per_day_left']))
        {
            $alias = uniqid('payment_cost_per_day_left');
            $this->datatable_builder
            ->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_cost_per_day_left'", 'INNER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_cost_per_day_left'];
            unset($args['order_by']['payment_cost_per_day_left']);
        }

        if( ! empty($args['order_by']['payment_expected_end_time']))
        {
            $alias = uniqid('payment_expected_end_time');
            $this->datatable_builder
            ->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_expected_end_time'", 'INNER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_expected_end_time'];
            unset($args['order_by']['payment_expected_end_time']);
        }

        if( ! empty($args['order_by']['contract_budget']))
        {
            $alias = uniqid('contract_budget');
            $this->datatable_builder
            ->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_budget'", 'INNER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['contract_budget'];
            unset($args['order_by']['contract_budget']);
        }

        if( ! empty($args['order_by']['actual_budget']))
        {
            $alias = uniqid('actual_budget');
            $this->datatable_builder
            ->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_budget'", 'INNER');
            $args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['actual_budget'];
            unset($args['order_by']['actual_budget']);
        }

        if(!empty($args['order_by']['expected_end_time']))
        {
            $this->datatable_builder->join('termmeta AS expected_end_time_termmeta', 'expected_end_time_termmeta.term_id = term.term_id', 'INNER');
            $args['where']['expected_end_time_termmeta.meta_key'] = 'expected_end_time';

            $args['order_by']['(expected_end_time_termmeta.meta_value)*1'] = $args['order_by']['expected_end_time'];

            unset($args['order_by']['expected_end_time']);
        }

        if(!empty($args['order_by']['payment_expected_end_time']))
        {
            $this->datatable_builder->join('termmeta AS payment_expected_end_time_termmeta', 'payment_expected_end_time_termmeta.term_id = term.term_id', 'INNER');
            $args['where']['payment_expected_end_time_termmeta.meta_key'] = 'payment_expected_end_time';

            $args['order_by']['(payment_expected_end_time_termmeta.meta_value)*1'] = $args['order_by']['payment_expected_end_time'];

            unset($args['order_by']['payment_expected_end_time']);
        }

        $sort_status = $args['order_by']['status'] ?? FALSE;
        if($sort_status)
        {
            $this->datatable_builder->order_by('term.term_status', $sort_status);
            unset($args['order_by']['status']);
        }

        $sort_type = $args['order_by']['type'] ?? FALSE;
        if($sort_type)
        {
            $this->datatable_builder->order_by('term.term_type', $sort_type);
            unset($args['order_by']['type']);
        }

        $sort_website = $args['order_by']['website'] ?? FALSE;
        if($sort_website)
        {
            $this->datatable_builder->order_by('term.term_name', $sort_website);
            unset($args['order_by']['website']);
        }
        
        // APPLY DEFAULT FILTER BY MUTATE FIELD     
        $args = $this->datatable_builder->parse_relations_searches($args);
        if( ! empty($args['where']))
        {
            foreach ($args['where'] as $key => $value)
            {
                if(empty($value)) continue;

                if(empty($key))
                {
                    $this->datatable_builder->add_filter($value, '');
                    continue;
                }

                $this->datatable_builder->add_filter($key, $value);
            }
        }

        if(!empty($args['order_by'])) 
        {
            foreach ($args['order_by'] as $key => $value)
            {
                $this->datatable_builder->order_by($key, $value);
            }
        }
    }
}
/* End of file Dataset.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/Dataset.php */
