<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetHistory extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'audits_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'facebookads/facebookads_m';

        parent::__construct();

        $this->load->config('facebookads/facebookads');
        $this->load->config('audit');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get($term_id = 0)
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        // if (!has_permission('contract.receipt.access')) {
        //     $response['msg'] = 'Quyền truy cập không hợp lệ.';
        //     return parent::response($response);
        // }

        $args = wp_parse_args(parent::get(), [
            'offset' => 0,
            'per_page' => 20,
            'cur_page' => 1,
            'is_filtering' => true,
            'is_ordering' => true
        ]);

        $this->filtering();

        // Prepare data
        $auditable_fields_config = $this->audits_m->get_auditable_fields_config();
        $events_config = $this->audits_m->get_events_config();

        if (empty($term_id)) {
            $this->datatable_builder->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])
                ->add_column('contract_code', array('title' => 'Mã hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE));
        }

        $query = "
            SELECT 
                audits.auditable_id AS contract_id,
                MAX(IF(m_contract.meta_key = 'contract_code', m_contract.meta_value, NULL)) AS contract_code,
                audits.id AS audits_id,
                audits.event,
                audits.auditable_type,
                audits.auditable_field,
                audits.new_values,
                audits.old_values,
                audits.created_at,
                user.user_id,
                user.display_name,
                user.user_avatar
            FROM
                audits
                JOIN term ON term.term_id = audits.auditable_id AND term.term_type = 'facebook-ads'
                JOIN user ON user.user_id = audits.user_id
                LEFT JOIN termmeta m_contract ON m_contract.term_id = audits.auditable_id AND m_contract.meta_key = 'contract_code'
            WHERE
                1 = 1
                    AND audits.auditable_type IN ('term' , 'termmeta')";
        if (is_numeric($term_id) && $term_id > 0) {
            $query .= " and audits.auditable_id = {$term_id}";
        }
        $query .= "
            GROUP BY audits.id 
            UNION SELECT 
                contract.term_id AS contract_id,
                MAX(IF(m_contract.meta_key = 'contract_code', m_contract.meta_value, NULL)) AS contract_code,
                audits.id AS audits_id,
                audits.event,
                audits.auditable_type,
                audits.auditable_field,
                audits.new_values,
                audits.old_values,
                audits.created_at,
                user.user_id,
                user.display_name,
                user.user_avatar
            FROM
                audits
                    JOIN term_posts ON term_posts.post_id = audits.auditable_id
                    JOIN term adaccount ON adaccount.term_id = term_posts.term_id AND adaccount.term_type = 'adaccount'
                    JOIN term_posts tp_segment ON tp_segment.term_id = adaccount.term_id
                    JOIN posts segments ON segments.post_id = tp_segment.post_id AND segments.post_type = 'ads_segment'
                    JOIN term_posts tp_contract ON tp_contract.post_id = segments.post_id
                    JOIN term contract ON contract.term_id = tp_contract.term_id AND contract.term_type = 'facebook-ads'
                    JOIN user ON user.user_id = audits.user_id
                    LEFT JOIN termmeta m_contract ON m_contract.term_id = contract.term_id AND m_contract.meta_key = 'contract_code'
            WHERE
                1 = 1
                    AND audits.auditable_type IN ('post' , 'postmeta')";
        if (is_numeric($term_id) && $term_id > 0) {
            $query .= " and contract.term_id = {$term_id}";
        }
        $query .= "
            GROUP BY audits.id
            ORDER BY created_at DESC
        ";

        $data['content'] = $this
            ->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')

            ->add_search('display_name', ['placeholder' => 'Người thao tác'])
            ->add_search('event', ['content' => form_dropdown(['name' => 'where[event]', 'class' => 'form-control select2'], prepare_dropdown($events_config, 'Tác vụ'), parent::get('where[event]'))])
            ->add_search('auditable_field', ['content' => form_dropdown(['name' => 'where[auditable_field]', 'class' => 'form-control select2'], prepare_dropdown($auditable_fields_config, 'Trường dữ liệu'), parent::get('where[auditable_field]'))])
            ->add_search('new_values', ['placeholder' => 'Mô tả/Giá trị mới'])
            ->add_search('old_values', ['placeholder' => 'Giá trị cũ'])
            ->add_search('created_at', ['placeholder' => 'Ngày thao tác', 'class' => 'form-control set-datepicker'])

            ->add_column('display_name', array('title' => 'Người thao tác', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('event', array('title' => 'Tác vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('auditable_field', array('title' => 'Trường dữ liệu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('new_values', array('title' => 'Mô tả/Giá trị mới', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('old_values', array('title' => 'Giá trị cũ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('created_at', array('title' => 'Ngày thao tác', 'set_select' => FALSE, 'set_order' => TRUE))

            ->select('contract_id, contract_code, audits_id, event, auditable_type, auditable_field, new_values, old_values, created_at, user_id, display_name, user_avatar')
            ->from($query, TRUE)

            ->add_column_callback('audits_id', function ($data, $row_name) {
                $event_config = $this->config->item($data['event'], 'event');
                $event_config and $data['event'] = $event_config['label'];

                $field_config = $this->config->item($data['auditable_field'], $data['auditable_type']);
                if (!empty($field_config)) {
                    $data['auditable_field'] = $field_config['label'];
                    $data['new_values'] = detect_audit($data['new_values'], $field_config);
                    $data['old_values'] = detect_audit($data['old_values'], $field_config);
                }

                return $data;
            }, FALSE);

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            // $this->export_receipt($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering()
    {
        restrict('contract.revenue.access');

        $args = parent::get(NULL, TRUE);
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // Flter and Sort contract_code 
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        if ($filter_contract_code) {
            $this->datatable_builder->having("contract_code", $filter_contract_code);
        }

        $order_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($order_contract_code) {
            $this->datatable_builder->order_by("contract_code", $order_contract_code);
        }

        // Flter and Sort display_name 
        $filter_display_name = $args['where']['display_name'] ?? FALSE;
        if ($filter_display_name) {
            $this->datatable_builder->having("display_name", $filter_display_name);
        }

        $order_display_name = $args['order_by']['display_name'] ?? FALSE;
        if ($order_display_name) {
            $this->datatable_builder->order_by("display_name", $order_display_name);
        }

        // Flter and Sort event 
        $filter_event = $args['where']['event'] ?? FALSE;
        if ($filter_event) {
            $this->datatable_builder->having("event", $filter_event);
        }

        $order_event = $args['order_by']['event'] ?? FALSE;
        if ($order_event) {
            $this->datatable_builder->order_by("event", $order_event);
        }

        // Flter and Sort auditable_field 
        $filter_auditable_field = $args['where']['auditable_field'] ?? FALSE;
        if ($filter_auditable_field) {
            $this->datatable_builder->having("auditable_field", $filter_auditable_field);
        }

        $order_auditable_field = $args['order_by']['auditable_field'] ?? FALSE;
        if ($order_auditable_field) {
            $this->datatable_builder->order_by("auditable_field", $order_auditable_field);
        }

        // Flter and Sort new_values 
        $filter_new_values = $args['where']['new_values'] ?? FALSE;
        if ($filter_new_values) {
            $this->datatable_builder->having("new_values", $filter_new_values);
        }

        $order_new_values = $args['order_by']['new_values'] ?? FALSE;
        if ($order_new_values) {
            $this->datatable_builder->order_by("new_values", $order_new_values);
        }

        // Flter and Sort old_values 
        $filter_old_values = $args['where']['old_values'] ?? FALSE;
        if ($filter_old_values) {
            $this->datatable_builder->having("old_values", $filter_old_values);
        }

        $order_old_values = $args['order_by']['old_values'] ?? FALSE;
        if ($order_old_values) {
            $this->datatable_builder->order_by("old_values", $order_old_values);
        }

        // Flter and Sort created_at 
        $filter_created_at = $args['where']['created_at'] ?? FALSE;
        if ($filter_created_at) {
            $dates = explode(' - ', $filter_created_at);

            $this->datatable_builder->having("created_at >=", date('Y/m/d h:i:s', start_of_day(reset($dates))));
            $this->datatable_builder->having("created_at <=", date('Y/m/d h:i:s', end_of_day(end($dates))));
        }

        $order_created_at = $args['order_by']['created_at'] ?? FALSE;
        if ($order_created_at) {
            $this->datatable_builder->order_by("created_at", $order_created_at);
        }
    }
}
/* End of file DatasetReceipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetReceipts.php */