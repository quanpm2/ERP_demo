<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fix extends Admin_Controller {

	public function __construct(){
		parent::__construct();

		$models = array(
			'facebookads/facebookads_m',
			'facebookads/facebookads_contract_m',
			'facebookads/facebookads_report_m',
			'facebookads/facebookads_kpi_m',
			'facebookads/adcampaign_m',
			'facebookads/adset_m',
			'facebookads/insight_segment_m',
			'ads_segment_m',
		);

        if(!has_permission('facebookads.fix.access'))  redirect(module_url(),'refresh');

		$this->load->model($models);
	}

	public function switch_is_external($contractId = 0, $adaccountId = 0, $value = 0)
	{
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

		$this->facebookads_m->set_contract($contractId);
		$contract = $this->facebookads_m->get_contract();
		empty($contract) AND die ('No Contract found !');

		var_dump($contract);

		$adsSegments = $this->term_posts_m->get_term_posts($contract->term_id, $this->ads_segment_m->post_type);
        if(empty($adsSegments)) die('No Segments Found.');

		$adsSegments = array_map(function($x){

			$adaccount = $this->term_posts_m->get_post_terms($x->post_id, $this->adaccount_m->term_type);
			$adaccount  = reset($adaccount);
			$x->adaccount = $adaccount;
			return $x;

		}, $adsSegments);

		var_dump($adsSegments);

		update_term_meta($adaccountId, 'isExternal', $value);
	}

	public function batch_update_adaccount_status()
	{
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

		$contracts = $this->facebookads_m->set_term_type()->get_all();
		if(empty($contracts)) die('no-thing to-do !');

		$contracts = array_map(function($x){

			$x->start_service_time 	= (int) get_term_meta_value($x->term_id, 'start_service_time');
			$x->is_started 			= ! empty($x->start_service_time);

			$x->end_service_time 	= (int) get_term_meta_value($x->term_id, 'end_service_time');
			$x->is_stopped 			= ! empty($x->end_service_time);

			$x->is_service_proc 	= is_service_proc($x);
			$x->is_service_end 		= is_service_end($x);


			$x->segments 			= $this->term_posts_m->get_term_posts($x->term_id, $this->ads_segment_m->post_type);

			$x->adaccount_id = get_term_meta_value($x->term_id, 'adaccount_id');

			return $x;
		}, $contracts);

		$completedContracts = array_filter($contracts, function($x){
			return $x->is_service_end;
		});

		$enabledCompletedContracts	= [];
		$unknownCompletedContracts 	= [];

		foreach ($completedContracts as $item)
		{
			if( ! empty($item->segments) || !empty($item->adaccount_id))
			{
				array_push($enabledCompletedContracts, $item);
				continue;
			}

			array_push($unknownCompletedContracts, $item);
		}

		array_walk($enabledCompletedContracts, function($x){
			update_term_meta($x->term_id, 'adaccount_status', 'APPROVED');
		});

		array_walk($unknownCompletedContracts, function($x){
			update_term_meta($x->term_id, 'adaccount_status', 'UNKNOWN');
		});
		

		$incompletedContracts = array_filter($contracts, function($x){
			return ! $x->is_service_end;
		});

		foreach($incompletedContracts as $item)
		{
			if(in_array($item->term_status, ['unverified', 'waitingforapprove'])) continue;

			if( 'remove' == $item->term_status)
			{
				update_term_meta($item->term_id, 'adaccount_status', 'UNKNOWN');
				continue;
			}

			if(empty($item->segments)) 
			{
				update_term_meta($item->term_id, 'adaccount_status', 'UNSPECIFIED');
				continue;
			}

			if($item->is_service_proc)
			{
				update_term_meta($item->term_id, 'adaccount_status', 'APPROVED');
				continue;
			}

			update_term_meta($item->term_id, 'adaccount_status', 'PENDING_APPROVAL');
		}

		// dd($unknownCompletedContracts);

		// dd(count($contracts), count($completedContracts), count($enabledCompletedContracts));
		// dd($enabledContracts);
	}

	public function migrate_insight_fb()
	{
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

		// dd('safe locked');
		$metadata 		= $this->termmeta_m->like('meta_key', 'insight_', 'right')
		// ->where('term_id', 26674)
		->order_by('term_id')
		->get_all();
		// $adaccountIds 	= array_filter(array_unique(array_column($metadata, 'term_id')));
		foreach ($metadata as &$item)
		{
			$item->meta_value 	= unserialize($item->meta_value);

			! empty($item->meta_value['actions']) AND array_walk($item->meta_value['actions'], function($x) use(&$item){
				$item->meta_value["actions.action_type.{$x['action_type']}"] = $x['value'];
			});

			! empty($item->meta_value['cost_per_action_type']) AND array_walk($item->meta_value['cost_per_action_type'], function($x) use(&$item){
				$item->meta_value["cost_per_action_type.action_type.{$x['action_type']}"] = $x['value'];
			});

			! empty($item->meta_value['cost_per_unique_action_type']) AND array_walk($item->meta_value['cost_per_unique_action_type'], function($x) use(&$item){
				$item->meta_value["cost_per_unique_action_type.action_type.{$x['action_type']}"] = $x['value'];
			});

			! empty($item->meta_value['cost_per_10_sec_video_view']) AND array_walk($item->meta_value['cost_per_10_sec_video_view'], function($x) use(&$item){
				$item->meta_value["cost_per_10_sec_video_view.action_type.{$x['action_type']}"] = $x['value'];
			});

			if( ! empty($item->meta_value['action_values'])) dd($item);

			unset($item->meta_value['actions']);
			unset($item->meta_value['cost_per_action_type']);
			unset($item->meta_value['cost_per_10_sec_video_view']);
			unset($item->meta_value['cost_per_unique_action_type']);

			foreach ($item->meta_value as $key => $value)
			{
				is_numeric($value) AND $item->meta_value[$key] = (double) $item->meta_value[$key];
				if( ! is_numeric($item->meta_value[$key]) && empty($item->meta_value[$key])) unset($item->meta_value[$key]);
			}

			$time = str_replace('insight_', '', $item->meta_key);

			$_isegment = $this->term_posts_m->get_term_posts($item->term_id, $this->insight_segment_m->post_type, [
				'fields' => 'posts.post_id',
				'where' => [
					'start_date' 	=> $time,
					'end_date' 		=> $time,
					'post_name' 	=> 'day'
				]
			]);

			$_isegmentInsert = [
				// 'post_id',
				// post_author
				// post_title
				'post_name' => 'day',
				// post_excerpt
				// post_content
				// post_status
				// comment_status
				'post_type' => 'insight_segment',
				'start_date' => $time,
				'end_date' => $time
			];

			if(empty($_isegment))
			{
				$_isegment_id = $this->insight_segment_m->insert($_isegmentInsert);
				$this->term_posts_m->set_term_posts($item->term_id, [$_isegment_id], $this->insight_segment_m->post_type);

				$_isegment = (object) $_isegmentInsert;
				$_isegment->post_id = $_isegment_id;
			}

			is_array($_isegment) AND $_isegment = reset($_isegment);

			array_walk($item->meta_value, function($value, $key) use($_isegment, $item){
				var_dump($key, $value, $item->term_id);
				update_post_meta($_isegment->post_id, $key, $value);
			});	

			var_dump('term_id '.$item->term_id);
		}

		die('stop');
		

		// var_dump(is_numeric(''));
		// var_dump(is_numeric(''));
		// dd();
		$this->load->model('facebooakds/adaccount_m');

		$adAccounts = $this->adaccount_m->select('term_id, term_type')->set_term_type()
		->where([
			1 => 1,
			'term_id' => 4554
		])
		->get_all();
		foreach ($adAccounts as $adAccount)
		{
			$metadatas = $this->termmeta_m->like('meta_key', 'insight_', 'right')->get_all();
			if(empty($metadatas)) continue;

			foreach ($metadatas as $metadata)
			{
				$insight = unserialize($metadata->meta_value);

				!empty($insight['actions']) AND array_walk($insight['actions'], function($x) use(&$insight){
					$insight["actions.action_type.{$x['action_type']}"] = $x['value'];
				});

				!empty($insight['cost_per_action_type']) AND array_walk($insight['cost_per_action_type'], function($x) use(&$insight){
					$insight["cost_per_action_type.action_type.{$x['action_type']}"] = $x['value'];
				});

				!empty($insight['cost_per_unique_action_type']) AND array_walk($insight['cost_per_unique_action_type'], function($x) use(&$insight){
					$insight["cost_per_unique_action_type.action_type.{$x['action_type']}"] = $x['value'];
				});

				if( ! empty($insight['actions']))
				{
					
				}

				unset($insight['actions']);
				unset($insight['cost_per_action_type']);
				unset($insight['cost_per_unique_action_type']);

				array_walk($insight, function($value, $key){
					// udpate_pos
					dd($value, $key);
				});
			}
			dd($metadatas);
		}
	}

	public function migrate()
	{
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

		// $this->termmeta_m->where_in('meta_key', ['external_bmid', 'external_adaccount_id', 'external_adaccount_spent'])->delete_by();
		$adaccount_metadatas = $this->termmeta_m->where('meta_key', 'adaccount_id')->group_by('term_id')->get_all();
		if( ! empty($adaccount_metadatas))
		{
			foreach ($adaccount_metadatas as $meta)
			{
				$adaccounts = get_term_meta($meta->term_id, 'adaccounts', FALSE, TRUE) ?: [];
				$adaccounts[] = $meta->meta_value;
				$adaccounts = array_unique(array_filter($adaccounts));

				$this->termmeta_m->delete_meta($meta->term_id, 'adaccounts');

				if( ! empty($adaccounts))
				{
					foreach ($adaccounts as $adaccount_id)
					{
						$this->termmeta_m->add_meta($meta->term_id, 'adaccounts', $adaccount_id);
					}	
				}

				$adaccounts = get_term_meta($meta->term_id, 'adaccounts', FALSE, TRUE);
				if(empty($adaccounts)) continue;

				$segments = array_map(function($x) use($meta){
					return [
						'start_date' => (int) get_term_meta_value($meta->term_id, 'advertise_start_time'),
            			'end_date' => (int) get_term_meta_value($meta->term_id, 'advertise_end_time'),
            			'adaccount_id' => $x
					];
				}, $adaccounts);

				$adsSegmentsIds = array();
				$adssegments = $this->term_posts_m->get_term_posts($meta->term_id, $this->ads_segment_m->post_type); 
            	$adssegments AND $adsSegmentsIds = array_filter(array_column($adssegments, 'post_id'));

            	$removeSegmentsIds = array_filter($adsSegmentsIds, function($x) use ($segments){
	                if(empty($segments)) return true;
	                return ! in_array($x, array_column($segments, 'post_id'));
	            });

	            /* Nếu phân đoạn nào không được thấy trong giá trị này thì cần phải xóa nó ra khỏi data */
	            if( ! empty($removeSegmentsIds))
	            {
	                $this->ads_segment_m->delete_many($removeSegmentsIds);
	                $this->term_posts_m->where_in('post_id', $removeSegmentsIds)->delete_by();
	            }

	            foreach ($segments as &$segment)
	            {
	                $start_date = $segment['start_date'] ?: start_of_day($segment['start_date']);
	                $end_date   = "";
	                $segment['end_date'] AND $end_date = end_of_day($segment['end_date']);

	                // Nếu $segment không tìm thấy trong DB thì tiến hành tạo mới.
	                if(empty($segment['post_id']))
	                {
	                    $insert_id          = $this->ads_segment_m->insert([ 'start_date' => $start_date, 'end_date' => $end_date ]);
	                    $segment['post_id'] = $insert_id;

	                    $this->term_posts_m->set_post_terms($insert_id, $segment['adaccount_id'], $this->adaccount_m->term_type);
	                    continue;
	                }

	                // Nếu $segment có tồn tại trong DB thì tiến hành cập nhật
	                $this->ads_segment_m->update($segment['post_id'], [ 'start_date' => $start_date, 'end_date' => $end_date ]);                
	                $this->term_posts_m->set_post_terms($segment['post_id'], $segment['adaccount_id'], $this->adaccount_m->term_type);
	            }

	            $this->term_posts_m->set_term_posts($meta->term_id, array_unique(array_column($segments, 'post_id')), $this->ads_segment_m->post_type, FALSE);

			}
		}
		
		$this->load->model('facebookads/adaccount_m');
		$adaccounts = $this->adaccount_m->select('term_id, term_name, term_slug, term_parent')->set_term_type()->order_by('term_name')->as_array()->get_all();
		$adaccounts AND array_walk($adaccounts, function($x){

			$isExternal	= (int) get_term_meta_value($x['term_id'], 'isExternal');
			update_term_meta($x['term_id'], 'isExternal', $isExternal);
		});
	}

	public function migrate_down()
	{
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

		$adaccount_metadatas = $this->termmeta_m->where('meta_key', 'adaccount_id')->group_by('term_id')->get_all();

		foreach ($adaccount_metadatas as $meta)
		{
			$adaccounts = get_term_meta($meta->term_id, 'adaccounts', FALSE, TRUE) ?: [];
			$adaccounts[] = $meta->meta_value;
			$adaccounts = array_unique(array_filter($adaccounts));

			if(empty($adaccounts)) $this->termmeta_m->delete_meta($meta->term_id, 'adaccounts');

			$adssegments = $this->term_posts_m->get_term_posts($meta->term_id, $this->ads_segment_m->post_type); 
            if( ! empty($adssegments))
            {
            	$this->term_posts_m->delete_term_posts($meta->term_id, array_filter(array_column($adssegments, 'post_id')));
            }
		}
	}

    public function fix_join_contract(){
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $this->submit_fix_join_contract();

        $chains = $this->input->get('chains', '');
        if(empty($chains))
        {
            $data = [
                'contract_ids' => [],
                'contracts' => []
            ];
            return parent::render($data);
        }

        $contract_id_list = explode('_', $chains);

        $contracts = $this->facebookads_m->set_term_type()
        ->select("term.term_id AS contract_id")
        ->select("balance_spend.post_id AS balance_spend_id")
        ->select("SUM(IF(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, 0)) AS spend")
        ->select("balance_spend.post_content AS balance_spend_value")
        ->select("balance_spend.comment_status AS balance_spend_type")

        ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
        ->join('posts AS balance_spend', 'balance_spend.post_id = tp_contract_ads_segment.post_id  AND balance_spend.post_type = "balance_spend"', 'LEFT')
        ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'LEFT')  
        ->join('term_posts AS tp_segment_adaccount', 'tp_segment_adaccount.post_id = ads_segment.post_id', 'LEFT')  
        ->join('term AS adaccount', 'tp_segment_adaccount.term_id = adaccount.term_id AND adaccount.term_type = "adaccount"', 'LEFT')  
        ->join('termmeta AS adaccount_metadata', 'adaccount_metadata.term_id = adaccount.term_id AND meta_key = "source"', 'LEFT')  
        ->join('term_posts AS tp_adaccount_insights', 'tp_adaccount_insights.term_id = adaccount.term_id', 'LEFT')  
        ->join('posts AS insights', 'tp_adaccount_insights.post_id = insights.post_id AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) AND insights.start_date <= if(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) AND insights.post_type = "insight_segment" AND insights.post_name = "day"', 'LEFT')
        ->join('postmeta AS insight_metadata', 'insight_metadata.post_id = insights.post_id AND insight_metadata.meta_key = "spend"', 'LEFT')
        
        ->where('( adaccount.term_id > 0 OR balance_spend.post_id > 0)')
        ->group_by('term.term_id, ads_segment.post_id, adaccount.term_id, insights.post_id, balance_spend.post_id')

        ->where_in('term.term_id', $contract_id_list)
        ->as_array()
        ->get_all();

        $contracts_group_by_id = array_group_by($contracts, 'contract_id');
        $contracts_group_by_id = array_map(function($item){
            $instance = reset($item);

            $contract_id = $instance['contract_id'];
            $actual_budget = get_term_meta_value($contract_id, 'actual_budget');
            $advertise_start_time = get_term_meta_value($contract_id, 'advertise_start_time');
            $advertise_end_time = get_term_meta_value($contract_id, 'advertise_end_time');
            $balance_budget_add_to = get_term_meta_value($contract_id, 'balanceBudgetAddTo');
            $spend = $instance['spend'];

            $spend = array_sum(array_column($item, 'spend'));

            $_direct_balance_spend = array_filter($item, function($spend){ return 'direct' == $spend['balance_spend_type']; });
            $direct_balance_spend = array_sum(array_column($_direct_balance_spend, 'balance_spend_value'));

            $_auto_balance_spend = array_filter($item, function($spend){ return 'auto' == $spend['balance_spend_type']; });
            $auto_balance_spend = array_reduce($_auto_balance_spend, function($result, $item){
                $balance_spend_id = $item['balance_spend_id'];
                if(empty($balance_spend_id))
                {
                    return $result;
                }

                $join_direction = get_post_meta_value($item['balance_spend_id'], 'join_direction');
                if('from' != $join_direction)
                {
                    return $result;
                }

                $result += (int) $item['balance_spend_value'];

                return $result;
            }, 0);

            $actual_result = get_term_meta_value($contract_id, 'actual_result');

            $result = [
                'contract_id' => $contract_id,
                'contract_code' => get_term_meta_value($contract_id, 'contract_code'),
                'spend' => (float)$spend,
                'actual_result' => (float)$actual_result,
                'balance_spend_value' => (float)$direct_balance_spend,
                'auto_balance_spend' => (float)$auto_balance_spend,
                'actual_budget' => (float)$actual_budget,
                'balance_budget_add_to' => (float)$balance_budget_add_to,
                'advertise_start_time' => $advertise_start_time,
                'advertise_end_time' => $advertise_end_time,
            ];
            return $result;
        }, $contracts_group_by_id);

        $desc = $this->input->get('desc', '');
        $data = [
            'contract_ids' => $contract_id_list,
            'contracts' => $contracts_group_by_id,
            'desc' => $desc
        ];
        return parent::render($data);
    }

    protected function submit_fix_join_contract($args = [])
	{
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $join_start_at = 0;
        $contract_ids = [];
        $contracts = [];

        $post = $this->input->post(NULL, TRUE);
        if(!empty($post['submit_spend_join'])) {
            $join_start_at = $post['join_start_at'] ?? 0;
            $contract_ids = json_decode($post['contract_ids'], TRUE);
            $contracts = json_decode($post['contracts'], TRUE);
        }
        else if(!empty($args))
        {
            $contract_ids = $args['contract_ids'];
            $contracts = $args['contracts'];
            $args['is_redirect'] = FALSE;
        }
        else
        {
            return;
        }

        $this->load->model('balance_spend_m');
        $this->load->model('facebookads_behaviour_m');

        $_contract_ids = $contract_ids;
        
        // Pick 2 contract_id
        $origination_id = array_shift($_contract_ids);
        $destination_id = array_shift($_contract_ids);

        $is_end = FALSE;
        while(!$is_end)
        {
            log_message('debug', 'Start join contract:::::' . json_encode([
                'origination_id' => $origination_id, 
                'destination_id' => $destination_id
            ]));

            $origination_data = $contracts[$origination_id] ?? NULL;
            $destination_data = $contracts[$destination_id] ?? NULL;
            if(empty($origination_data) || empty($destination_data))
            {
                $is_end = TRUE;
                continue;
            }

            // Give (join to)
            $old_given_balance_spend_items = $this->balance_spend_m
            ->set_post_type()
            ->select('posts.post_id')
            ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$origination_id})")
            ->where('posts.comment_status', 'auto')
            ->where('posts.post_title', 'join_command')
            ->as_array()
            ->get_all();
            $old_given_balance_spend_items = array_filter($old_given_balance_spend_items, function($item){
                $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                return 'to' == $join_direction;
            });

            if( ! empty($old_given_balance_spend_items))
            {
                $this->term_posts_m->delete_term_posts($origination_id, array_column($old_given_balance_spend_items, 'post_id'));
                $this->balance_spend_m->delete_many(array_column($old_given_balance_spend_items, 'post_id'));
            }

            update_term_meta($origination_id, 'nextContractId', $destination_id);
            update_term_meta($origination_id, 'balanceBudgetAddTo', $contracts[$origination_id]['balanceBudgetAddTo']);
            $post_excerpt = 'Nối đến HĐ ' . get_term_meta_value($destination_id, 'contract_code');
            $balance_spend_join_to_id = $this->balance_spend_m->insert([
                'post_type' => $this->balance_spend_m->post_type,
                'comment_status' => 'auto',
                'post_title' => 'join_command',
                'post_status' => 'publish',
                'post_content' => $contracts[$origination_id]['balanceBudgetAddTo'],
                'post_excerpt' => $post_excerpt,
                'start_date' => time(),
                'end_date' => time(),
            ]);
            update_post_meta($balance_spend_join_to_id, 'join_direction', 'to');
            $this->term_posts_m->set_term_posts( $origination_id, [ $balance_spend_join_to_id ], $this->balance_spend_m->post_type);
            $this->log_m->insert([
                'log_type'        => 'joinContracts',
                'log_status'      => 1,
                'term_id'         => $origination_id,
                'log_content'     => json_encode(
                    [
                        'destination_id' => $destination_id, 
                        'given' => $contracts[$origination_id]['balanceBudgetAddTo']
                    ]
                ),
                'user_id'         => $this->admin_m->id
            ]);

            try
            {
                $contract = (new facebookads_m())->set_contract($origination_id);
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();
            }
            catch (Exception $e)
            {
                log_message('error', "[FacebookadsFix::fix_join_contract] Sync amount #{$origination_id} error. " . json_encode(['error' => $e->getMessage()]));
            }

            // Receive (join from)
            $old_received_balance_spend_items = $this->balance_spend_m
            ->set_post_type()
            ->select('posts.post_id')
            ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$destination_id})")
            ->where('posts.comment_status', 'auto')
            ->where('posts.post_title', 'join_command')
            ->as_array()
            ->get_all();
            $old_received_balance_spend_items = array_filter($old_received_balance_spend_items, function($item){
                $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                return 'from' == $join_direction;
            });

            if( ! empty($old_received_balance_spend_items))
            {
                $this->term_posts_m->delete_term_posts($destination_id, array_column($old_received_balance_spend_items, 'post_id'));
                $this->balance_spend_m->delete_many(array_column($old_received_balance_spend_items, 'post_id'));
            }

            update_term_meta($destination_id, 'previousContractId', $origination_id);
            update_term_meta($destination_id, 'balanceBudgetReceived', $contracts[$destination_id]['balanceBudgetReceived']);
            $post_excerpt = 'Nối từ HĐ ' . get_term_meta_value($origination_id, 'contract_code');
            $balance_spend_join_from_id = $this->balance_spend_m->insert([
                'post_type' => $this->balance_spend_m->post_type,
                'comment_status' => 'auto',
                'post_title' => 'join_command',
                'post_status' => 'publish',
                'post_content' => $contracts[$destination_id]['balanceBudgetReceived'],
                'post_excerpt' => $post_excerpt,
                'start_date' => time(),
                'end_date' => time(),
            ]);
            update_post_meta($balance_spend_join_from_id, 'join_direction', 'from');
            $this->term_posts_m->set_term_posts( $destination_id, [ $balance_spend_join_from_id ], $this->balance_spend_m->post_type);
            
            $this->log_m->insert([
                'log_type'        => 'joinContracts',
                'log_status'      => 1,
                'term_id'         => $destination_id,
                'log_content'     => json_encode(
                    [
                        'origination_id' => $origination_id, 
                        'received' => $contracts[$destination_id]['balanceBudgetReceived']
                    ]
                ),
                'user_id'         => $this->admin_m->id
            ]);

            try
            {
                $contract = (new facebookads_m())->set_contract($destination_id);
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();
            }
            catch (Exception $e)
            {
                log_message('error', "[FacebookadsFix::fix_join_contract] Sync amount #{$destination_id} error. " . json_encode(['error' => $e->getMessage()]));
            }

            if(empty($_contract_ids)) $is_end = TRUE;
            else 
            {
                $origination_id = $destination_id;
                $destination_id = array_shift($_contract_ids);
            }

            log_message('debug', 'Go to next contract:::::' . json_encode([
                'origination_id' => $origination_id, 
                'destination_id' => $destination_id,
                '_contract_ids' => $_contract_ids, 
            ]));
        }

        $this->log_m->insert([
            'log_type'        => 'joinContractChain',
            'log_status'      => 1,
            'log_title'       => implode('_', $contract_ids),
            'log_content'     => serialize(['contracts' => $contracts, 'join_start_at' => $join_start_at]),
            'user_id'         => $this->admin_m->id
        ]);

        $key_cache = 'facebookads/fix/contract_chains';
        $this->scache->delete($key_cache);

        if(!isset($args['is_redirect']) || $args['is_redirect'])
        {
            return redirect(current_url() . '?chains=' . implode('_', $contract_ids),'refresh');
        }

        return TRUE;
	}

    /**
     * Fix join all contract
     * 
     * Get all facebook ads contract which is having one of list status: publish, pending
     * Detact chain of contract depend on this contract id
     * From contract chain, detect has balance spend correct
     * IF balance spend is not correct, show it to screen
     */
    public function fix_join_all_contract(){
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $this->submit_fix_join_contract_remove();
        $this->submit_spend_join_all_contract();

        // Cache data
        $key_cache = 'facebookads/fix/contract_chains';
        $data = $this->scache->get($key_cache);
        if(!empty($data)) return parent::render($data);

        $allow_status = ['publish', 'pending', 'ending', 'liquidation'];
        $contracts = $this->facebookads_m->set_term_type()
            ->join('termmeta', 'termmeta.term_id = term.term_id AND termmeta.meta_key IN ("previousContractId", "nextContractId", "advertise_end_time")')
            
            ->select('term.term_id')
            ->select('MAX(IF(termmeta.meta_key = "previousContractId", termmeta.meta_value, NULL)) AS previousContractId')
            ->select('MAX(IF(termmeta.meta_key = "nextContractId", termmeta.meta_value, NULL)) AS nextContractId')
            ->select('MAX(IF(termmeta.meta_key = "advertise_end_time", termmeta.meta_value, NULL)) AS advertise_end_time')

            ->where_in('term.term_status', $allow_status)
            
            ->group_by('term.term_id')
            ->having('(previousContractId IS NOT NULL OR nextContractId IS NOT NULL)', NULL)

            ->as_array()
            ->get_all();
            
        $contract_ids = [];
        $term_id = array_column($contracts, 'term_id');
        $contract_ids = array_merge($contract_ids, $term_id);
        $previousContractId = array_column($contracts, 'previousContractId');
        $contract_ids = array_merge($contract_ids, $previousContractId);
        $nextContractId = array_column($contracts, 'nextContractId');
        $contract_ids = array_merge($contract_ids, $nextContractId);
        $contract_ids = array_filter($contract_ids);
        $contract_ids = array_unique($contract_ids);

        // Get chain
        $contracts_group_by_id = array_group_by($contracts, 'term_id');
        $contract_chains = [];
        foreach($contracts_group_by_id as $contract_id => $contract)
        {
            $chain = [$contract_id];

            $_contract = reset($contract);
            $previousContractId = $_contract['previousContractId'];
            while(!empty($previousContractId)){
                $chain = array_merge([$previousContractId], $chain);
                $_contract = empty($contracts_group_by_id[$previousContractId]) ? NULL : reset($contracts_group_by_id[$previousContractId]);
                $previousContractId = $_contract['previousContractId'] ?? NULL;

                if(in_array($previousContractId, $chain)) $previousContractId = NULL;
            }

            $_contract = reset($contract);
            $nextContractId = $_contract['nextContractId'];
            while(!empty($nextContractId)){
                $chain[] = $nextContractId;
                $_contract = empty($contracts_group_by_id[$nextContractId]) ? NULL : reset($contracts_group_by_id[$nextContractId]);
                $nextContractId = $_contract['nextContractId']  ?? NULL;

                if(in_array($nextContractId, $chain)) $nextContractId = NULL;
            }

            $contract_chains[] = $chain;
        }

        $this->load->model('balance_spend_m');
        $contract_chains = array_reduce($contract_chains, function($result, $item){
            $chain_str = array_column($result, 'str');
            $item_str = implode('_', $item);
            if(in_array($item_str, $chain_str)) return $result;

            $desc = [];
            $detail = array_reduce($item, function ($detail_result, $contract_id) use (&$desc){ 
                $spend = 0;
                $contract = $this->facebookads_m->set_contract($contract_id);
                if(!empty($contract)) {
                    $spend_data = $contract->get_insights(['summary' => TRUE]);
                    if(!empty($spend_data)) $spend = $spend_data['spend'] ?: 0;
                    else $spend = 0;
                }

                $actual_budget = get_term_meta_value($contract_id, 'actual_budget') ?? 0;
                $advertise_start_time = get_term_meta_value($contract_id, 'advertise_start_time') ?? 0;
                $advertise_end_time = get_term_meta_value($contract_id, 'advertise_end_time') ?? 0;
                $end_service_time = get_term_meta_value($contract_id, 'end_service_time') ?? 0;

                $balance_budget_add_to = 0;
                $balance_budget_received = 0;
                $balance_spend_items = $this->balance_spend_m->set_post_type()
                ->join('term_posts', "term_posts.post_id = posts.post_id AND term_posts.term_id = {$contract_id}")
                ->join('postmeta AS m_join_direction', "m_join_direction.post_id = posts.post_id")
                ->select('posts.post_id, posts.post_content, m_join_direction.meta_value')
                ->where('posts.comment_status', 'auto')
                ->where('posts.post_title', 'join_command')
                ->where('m_join_direction.meta_key', 'join_direction')
                ->as_array()
                ->get_all();
                if(!empty($balance_spend_items))
                {
                    $_balance_budget_add_to = array_filter($balance_spend_items, function($item){ return 'to' == $item['meta_value']; });
                    $balance_budget_add_to = array_sum(array_column($_balance_budget_add_to, 'post_content'));

                    $_balance_budget_received = array_filter($balance_spend_items, function($item){ return 'from' == $item['meta_value']; });
                    $balance_budget_received = array_sum(array_column($_balance_budget_received, 'post_content'));
                }

                $detail_result[$contract_id] = [
                    'spend' => $spend,
                    'budget' => $actual_budget,
                    'balance_budget_add_to' => $balance_budget_add_to,
                    'balance_budget_received' => $balance_budget_received,
                    'advertise_start_time' => $advertise_start_time,
                    'advertise_end_time' => $advertise_end_time,
                    'end_service_time' => $end_service_time
                ];

                return $detail_result;
            }, []);

            $result[] = [
                'chain' => $item,
                'str' => implode('_', $item),
                'is_conflict' => false,
                'conflict_at' => 0,
                'detail' => $detail,
                'desc' => $desc,
            ];
            return $result;
        }, []);

        // Get conflict chain
        $contract_chain_str = array_column($contract_chains, 'str');
        foreach($contract_ids as $contract_id)
        {
            $count = 0;
            $list_index = [];

            foreach($contract_chain_str as $index => $chain_str){
                $pos = strpos($chain_str, $contract_id);
                if(FALSE !== $pos) {
                    $list_index[] = $index;
                    $count++;
                }
            }

            if($count > 1)
            {
                foreach($list_index as $index)
                {
                    $contract_chains[$index]['is_conflict'] = TRUE;
                    $contract_chains[$index]['conflict_at'] = $contract_id;
                }
            }
        }
        $conflict_contract_chains = array_filter($contract_chains, function($item){ return  $item['is_conflict'];});
        $conflict_contract_chains_group_by_conflict_at = array_group_by($conflict_contract_chains, 'conflict_at');
        $contract_chains = array_filter($contract_chains, function($item){ return  !$item['is_conflict'];});

        foreach($conflict_contract_chains_group_by_conflict_at as &$conflict_contract_chains)
        {
            foreach($conflict_contract_chains as &$conflict_contract_chain)
            {
                $desc = '';
                $error_point = NULL;
                $directive = NULL;

                $chain = $conflict_contract_chain['chain'];
                $start_point = $chain[0];
                $end_point = $chain[count($chain) - 1];
                if($start_point == $end_point)
                {
                    $conflict_contract_chain['desc'][] = 'Chuỗi liên kết vòng tròn, xử lý tay';
                    $conflict_contract_chain['error_point'] = NULL;
                    $conflict_contract_chain['directive'] = NULL;
                    
                    continue;
                }

                $advertise_end_time = 0;
                foreach($chain as $index => $contract_id)
                {   
                    // Detect contract chain time
                    if($contract_id == reset($chain)) $advertise_end_time = $conflict_contract_chain['detail'][$contract_id]['advertise_end_time'] ?? 0;
                    else
                    {
                        $advertise_start_time = $conflict_contract_chain['detail'][$contract_id]['advertise_start_time'] ?? 0;
                        
                        0 == $advertise_end_time AND $advertise_end_time = time();
                        if($advertise_end_time >= $advertise_start_time){
                            $desc = 'Chuỗi nối không liên tục, xử lý tay';
                            $error_point = NULL;
                            $directive = NULL;

                            break;
                        }

                        $advertise_end_time = $conflict_contract_chain['detail'][$contract_id]['advertise_end_time'] ?? 0;
                    }

                    $previousContract = get_term_meta_value($contract_id, 'previousContractId');
                    $nextContract = get_term_meta_value($contract_id, 'nextContractId');
                    if(empty($previousContract) && empty($nextContract)){
                        $desc = $contract_id . ' không tìm thấy, xử lý tay';
                        $error_point = NULL;
                        $directive = NULL;
                        break;
                    }

                    $previousContractId = $previousContract['meta_value'] ?? NULL;
                    if(!empty($previousContractId)) {
                        $_nextContractId = get_term_meta_value($previousContractId, 'nextContractId');
                        
                        if(empty($_nextContractId))
                        {
                            $desc = 'Liên kết không hợp lệ';
                            $error_point = $contract_id;
                            $directive = 'left';
                            break;
                        }

                        if($contract_id != $_nextContractId)
                        {
                            $desc = $contract_id . ' không có liên kết';
                            $error_point = $contract_id;
                            $directive = 'left';
                            break;
                        }
                    };

                    $nextContractId = $nextContract['meta_value'] ?? NULL;
                    if(!empty($nextContractId)) {
                        $_previousContractId = get_term_meta_value($nextContractId, 'previousContractId');

                        if(empty($_previousContractId))
                        {
                            $desc = 'Liên kết không hợp lệ';
                            $error_point = $contract_id;
                            $directive = 'right';
                            break;
                        }
                        
                        if($contract_id != $_previousContractId)
                        {
                            $desc = $nextContractId . ' không có liên kết';
                            $error_point = $contract_id;
                            $directive = 'right';
                            break;
                        }
                    };
                }

                $conflict_contract_chain['desc'][] = $desc;
                $conflict_contract_chain['error_point'] = $error_point;
                $conflict_contract_chain['directive'] = $directive;
            }
        }

        // Get warning chain
        foreach($contract_chains as &$contract_chain)
        {
            $contract_chain['is_warning'] = FALSE;
            $contract_chain['warning_at'] = NULL;
            $contract_chain['log'] = '';

            $chain = $contract_chain['chain'];

            $this->load->model('balance_spend_m');

            $contracts = $contract_chain['detail'];

            $contract_ids = array_keys($contracts);
            $first_contract_id = reset($contract_ids);
            $last_contract_id = end($contract_ids);

            $spend_join = 0;
            $advertise_end_time = 0;
            $_contract_ids = $contract_ids;
            $is_end = FALSE;
            $max_diff_spend = 20;
            while(!$is_end){
                $contract_id = array_shift($_contract_ids);
                $contract = &$contracts[$contract_id];

                // Detect contract chain time
                if($contract_id == $first_contract_id) $advertise_end_time = $contract['advertise_end_time'] ?: 0;
                else
                {
                    $advertise_start_time = $contract['advertise_start_time'];

                    0 == $advertise_end_time AND $advertise_end_time = time();
                    if($advertise_end_time >= $advertise_start_time){
                        $contract_chain['is_warning'] = TRUE;
                        $contract_chain['warning_at'] = $contract_id;
                        $contract_chain['desc'][] = $contract_id . ' chuỗi nối không liên tục';
                    }

                    $advertise_end_time = $contract['advertise_end_time'];
                }

                // Detect stop contract
                $end_service_time = $contract['end_service_time'];
                if($contract_id != $last_contract_id && empty($end_service_time))
                {
                    $contract_chain['is_warning'] = TRUE;
                    $contract_chain['warning_at'] = $contract_id;
                    $contract_chain['desc'][] = $contract_id . ' chưa kết thúc khi nối';
                }

                // Detect Spend
                $budget = (int)$contract['budget'];
                $spend = (int)$contract['spend'];
                
                $balance_spend_received_value = $spend_join;
                $spend_join = $budget - ($spend + $balance_spend_received_value);
                
                // Detect balanceBudgetAddTo
                if($contract_id != $last_contract_id)
                {
                    $contract_spend_join = (int)$contract['balance_budget_add_to'];
                    $diff_spend_join_add_to = abs($contract_spend_join - $spend_join);
                    if($diff_spend_join_add_to > $max_diff_spend)
                    {
                        $contract_chain['is_warning'] = TRUE;
                        $contract_chain['warning_at'] = $contract_id;
                        $contract_chain['desc'][] = "{$contract_id} balanceBudgetAddTo không khớp (meta <b><u>{$contract['balance_budget_add_to']} != {$spend_join}</u></b> recalc)";
                    }
                }

                // Detect balanceBudgetReceived
                if($contract_id != $first_contract_id)
                {
                    $contract_spend_join = (int)$contract['balance_budget_received'];
                    $diff_spend_join_received = abs($contract_spend_join + $balance_spend_received_value);
                    if($diff_spend_join_received > $max_diff_spend){
                        $contract_chain['is_warning'] = TRUE;
                        $contract_chain['warning_at'] = $contract_id;
                        $contract_chain['desc'][] = "{$contract_id} balanceBudgetReceived không khớp (meta <b><u>{$contract['balance_budget_received']} != {$balance_spend_received_value}</u></b> recalc)";
                    }

                }

                if(empty($_contract_ids)) $is_end = TRUE;
            }
        }
        $warning_contract_chains = array_filter($contract_chains, function($item){ return  $item['is_warning'];});
        $contract_chains = array_filter($contract_chains, function($item){ return  !$item['is_warning'];});
        
        $data = [
            'contract_chains' => $contract_chains,
            'warning_contract_chains' => $warning_contract_chains,
            'conflict_contract_chains' => $conflict_contract_chains_group_by_conflict_at,
        ];

        // Cache data
        $key_cache = 'facebookads/fix/contract_chains';
        $data AND $this->scache->write($data, $key_cache, 60*60*24); // 1 days

        parent::render($data);
    }

    protected function submit_fix_join_contract_remove()
    {
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $post = $this->input->post(NULL, TRUE);
        if(empty($post['submit_remove_contract_join'])) return FALSE;

        $this->load->model('balance_spend_m');
        $this->load->model('facebookads_behaviour_m');

        $error_point = $post['error_point'];
        if(empty($error_point)) return FALSE;

        $directive = $post['directive'];
        switch($directive)
        {
            case 'right':
                update_term_meta($error_point, 'balanceBudgetAddTo', 0);
                update_term_meta($error_point, 'nextContractId', NULL);

                break;
            case 'left':
                update_term_meta($error_point, 'balanceBudgetReceive', 0);
                update_term_meta($error_point, 'previousContractId', NULL);

                $balance_spend_items = $this->balance_spend_m
                ->select('posts.post_id')
                ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$error_point})")
                ->set_post_type()
                ->where('comment_status', 'auto')
                ->where('post_title', 'join_command')
                ->get_all();
                if( ! empty($balance_spend_items))
                {
                    $this->term_posts_m->delete_term_posts($error_point, array_column($balance_spend_items, 'post_id'));
                    $this->balance_spend_m->delete_many(array_column($balance_spend_items, 'post_id'));
                }

                try
                {
                    $contract_m = (new facebookads_m())->set_contract($error_point);
                    $behaviour_m = $contract_m->get_behaviour_m();
                    $behaviour_m->get_the_progress();
                    $behaviour_m->sync_all_amount();
                }
                catch(Exception $e)
                {
                    dd($e->getMessage());
                }

                break;
            default:
                break;
        }

        $this->log_m->insert([
            'log_type'        => 'unjoinContracts',
            'log_status'      => 1,
            'term_id'         => $error_point,
            'log_content'     => serialize(['error_point' => $error_point, 'directive' => $directive]),
            'user_id'         => $this->admin_m->id
        ]);

        $key_cache = 'facebookads/fix/contract_chains';
        $this->scache->delete($key_cache);

        redirect(current_url(),'refresh');
    }

    protected function submit_spend_join_all_contract(){
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $post = $this->input->post(NULL, TRUE);
        if(empty($post['submit_spend_join_all_contract'])) return FALSE;

        foreach($post['spend_join_all_contract'] as $chain)
        {
            $contract_chain = explode('_', $chain);

            $contracts = $this->facebookads_m->set_term_type()
            ->select("term.term_id AS contract_id")
            ->select('MAX(IF(contract_metadata.meta_key = "actual_budget", contract_metadata.meta_value, NULL)) AS actual_budget')
            ->select('MAX(IF(contract_metadata.meta_key = "advertise_start_time", contract_metadata.meta_value, NULL)) AS advertise_start_time')
            ->select('MAX(IF(contract_metadata.meta_key = "advertise_end_time", contract_metadata.meta_value, NULL)) AS advertise_end_time')
            ->select("balance_spend.post_content AS balance_spend_value")
            ->select("balance_spend.comment_status AS balance_spend_type")

            ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
            ->join('posts AS balance_spend', 'balance_spend.post_id = tp_contract_ads_segment.post_id  AND balance_spend.post_type = "balance_spend"', 'LEFT')
            ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'LEFT')  
            ->join('term_posts AS tp_segment_adaccount', 'tp_segment_adaccount.post_id = ads_segment.post_id', 'LEFT')  
            ->join('term AS adaccount', 'tp_segment_adaccount.term_id = adaccount.term_id AND adaccount.term_type = "adaccount"', 'LEFT')  
            ->join('termmeta AS adaccount_metadata', 'adaccount_metadata.term_id = adaccount.term_id AND meta_key = "source"', 'LEFT')  
            ->join('term_posts AS tp_adaccount_insights', 'tp_adaccount_insights.term_id = adaccount.term_id', 'LEFT')  
            ->join('termmeta AS contract_metadata', 'contract_metadata.term_id = term.term_id AND contract_metadata.meta_key IN ("actual_budget", "advertise_start_time", "advertise_end_time")', 'LEFT')
            
            ->where('( adaccount.term_id > 0 OR balance_spend.post_id > 0)')
            ->group_by('term.term_id, ads_segment.post_id, adaccount.term_id, balance_spend.post_id')

            ->where_in('term.term_id', $contract_chain)
            ->as_array()
            ->get_all();

            $contracts_group_by_id = array_group_by($contracts, 'contract_id');
            $contracts_group_by_id = array_reduce($contracts_group_by_id, function($result, $item){
                $instance = reset($item);

                $contract_id = $instance['contract_id'];
                $actual_budget = $instance['actual_budget'];
                $advertise_start_time = $instance['advertise_start_time'];
                $advertise_end_time = $instance['advertise_end_time'];
                $balance_spend_value = $instance['balance_spend_value'];
                $balance_spend_type = $instance['balance_spend_type'];

                $spend = array_sum(array_column($item, 'spend'));
                $actual_result = (float) get_term_meta_value($contract_id, 'actual_result');

                $result[$contract_id] = [
                    'contract_id' => $contract_id,
                    'actual_budget' => $actual_budget,
                    'advertise_start_time' => $advertise_start_time,
                    'advertise_end_time' => $advertise_end_time,
                    'balance_spend_value' => $balance_spend_value,
                    'balance_spend_type' => $balance_spend_type,
                    'spend' => $spend,
                    'actual_result' => $actual_result,
                ];

                return $result;
            }, []);

            $_contract_chain = $contract_chain;
            $spend_join = 0;
            $is_end = FALSE;
            while(!$is_end){
                $contract_id = array_shift($_contract_chain);
                $contract = &$contracts_group_by_id[$contract_id] ?? [];
                if(empty($contract)) 
                {
                    if(empty($_contract_chain)) $is_end = TRUE;

                    continue;
                }

                // Set spend join value received by prev contract
                $contract['balanceBudgetReceived'] = $spend_join;

                $spend = (float) $contract['spend'] ?? 0;
                $actual_result = (float) $contract['actual_result'] ?? 0;
                $spend_to_calc = $spend > 0 ? $spend : $actual_result;

                $actual_budget = (float)$contract['actual_budget'];
                $balance_spend_value = (float)$contract['balance_spend_value'];
                $balanceBudgetReceived = -$spend_join;

                // Chi tiêu nối = Ngân sách - chi tiêu theo tài khoản - chi tiêu cân bằng thủ công - chi tiêu hợp đồng trước
                $spend_join = $actual_budget - $spend_to_calc - $balance_spend_value - $balanceBudgetReceived;

                // Set spend join value send to next contract
                $contract['balanceBudgetAddTo'] = $spend_join;

                if(empty($_contract_chain)) $is_end = true;
            }

            $this->submit_fix_join_contract(['contract_ids' => $contract_chain, 'contracts' => $contracts_group_by_id]);
        }

        return redirect(current_url(),'refresh');
    }

    public function fix_insight_contract($contract_id){
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $contract = $this->facebookads_m->set_contract($contract_id);
        if(empty($contract)) dd('Null');

        $start_date = $this->input->post('start_date', TRUE) ?: $this->input->get('start_date', TRUE) ?: date('Y-01-01');
        $start_time = $this->mdate->startOfDay(date($start_date));

        $insights = $contract->get_insights(['start_time' => $start_time, 'fields' => ['spend', 'clicks', 'impressions']]);
        
        $sum_spend = array_sum(array_column($insights, 'spend'));

        $insights_group_by_ad_account_id = array_group_by($insights, 'ad_account_id');

        $sum_spend_of_duplicated_insight = 0;
        $duplicated_insights = array_reduce($insights_group_by_ad_account_id, function($result, $item) use (&$sum_spend_of_duplicated_insight){ 
            $item_group_by_date_time = array_group_by($item, 'date_time');
            $item_group_by_date_time = array_filter($item_group_by_date_time, function($item) { return count($item) > 1; });

            foreach($item_group_by_date_time as $date_time => $_items){
                arsort($_items);
                $instance = reset($_items);
                if(!isset($result[$instance['ad_account_id']])) $result[$instance['ad_account_id']] = [];

                $sum_spend = array_sum(array_column($_items, 'spend'));
                $sum_spend_of_duplicated_insight += ($sum_spend - (float)$instance['spend']);
                
                $date = my_date($date_time, 'Y-m-d');
                $result[$instance['ad_account_id']][$date] = $_items;
            }

            return $result;
        }, []);

        $data = [
            'contract_id' => $contract_id,
            'sum_spend' => $sum_spend,
            'sum_spend_of_duplicated_insight' => $sum_spend_of_duplicated_insight,
            'duplicated_insights' => $duplicated_insights,
            'start_date' => $start_date
        ];

        $post = $this->input->post('submit_fix_insight_contract');
        if(!empty($post)) $this->submit_fix_insight_contract($contract_id, $duplicated_insights);

        return parent::render($data);
    }

    public function fake_insight_duplicated($insight_id, $num_of_duplicated = 0){
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $this->load->model('facebookads/insight_segment_m');

        $insight = $this->insight_segment_m
            ->set_post_type()
            ->where('post_id', $insight_id)
            ->as_array()
            ->get_by();
        if(empty($insight)) dd('NULL');

        $this->load->model('term_posts_m');
        $this->load->model('facebookads/adaccount_m');
        $ad_account_id = $this->term_posts_m->get_the_terms($insight_id, $this->adaccount_m->term_type) ?? [];
        if(empty($ad_account_id)) return FALSE;
        $ad_account_id = reset($ad_account_id);

        $insight_data = [
            'post_author' => $insight['post_author'],
            'post_name' => $insight['post_name'],
            'post_status' => $insight['post_status'],
            'comment_status' => $insight['comment_status'],
            'created_on' => $insight['created_on'],
            'updated_on' => $insight['updated_on'],
            'post_parent' => $insight['post_parent'],
            'post_type' => $insight['post_type'],
            'start_date' => $insight['start_date'],
            'end_date' => $insight['end_date'],
        ];

        $metadata = get_post_meta_value($insight_id);
        $metadata = key_value($metadata, 'meta_key', 'meta_value');

        for ($i = 0; $i < $num_of_duplicated; $i++) { 
            $_insight_id = $this->insight_segment_m->insert($insight_data);
            $this->term_posts_m->insert(array('post_id' => $_insight_id, 'term_id' => $ad_account_id ));

            foreach($metadata as $key => $value){
                update_post_meta($_insight_id, $key, $value);
            }
        }

        dd(1);
    }

    protected function submit_fix_insight_contract($contract_id, $data){
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        foreach($data as $insights_by_account){
            foreach($insights_by_account as $insights_by_date){
                arsort($insights_by_date);
                array_shift($insights_by_date);

                foreach($insights_by_date as $insight)
                {
                    $this->post_m->delete($insight['insight_id']);
                    $this->insight_segment_m->delete_relations($insight['insight_id']);
                }
            }
        }

        $contract = $this->facebookads_m->set_contract($contract_id);
        $behaviour = $contract->get_behaviour_m();
        $behaviour->get_the_progress(FALSE, FALSE);
        $behaviour->sync_all_amount();

        $log = $this->log_m->where('log_type', 'joinContractChain')
            ->like('log_title', $contract_id)
            ->order_by('log_id', 'DESC')
            ->select('log_id, log_title, log_content, log_time_create, log_status')
            ->as_array()
            ->get_by();
        if(!empty($log)) $this->log_m->update($log['log_id'], ['log_status' => 0]);

        update_term_meta($contract_id, 'balanceBudgetAddTo', 0);
        update_term_meta($contract_id, 'balanceBudgetReceived', 0);

        return redirect(current_url(),'refresh');
    }

    public function fix_all_insight_contract(){
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        restrict('facebookads.setting.manage');

        $start_date = $this->input->get('start_date', TRUE) ?: date('Y-01-01');
        $start_time = $this->mdate->startOfDay(date($start_date));
        $contracts = $this->facebookads_m->set_term_type()
            ->join('termmeta', 'termmeta.term_id = term.term_id AND termmeta.meta_key IN ("previousContractId", "nextContractId", "end_service_time", "advertise_start_time", "advertise_end_time")')
            
            ->select('term.term_id')
            ->select('MAX(IF(termmeta.meta_key = "advertise_start_time", termmeta.meta_value, NULL)) AS advertise_start_time')
            
            ->group_by('term.term_id')

            ->having("advertise_start_time >= {$start_time}", NULL)

            ->as_array()
            ->get_all();
        if(empty($contracts)) dd('Null');

        $contracts = array_map(function($contract) use ($start_time){
            $_contract = $this->facebookads_m->set_contract($contract['term_id']);
            $insights = $_contract->get_insights(['start_time' => $start_time]);
            
            $sum_spend = array_sum(array_column($insights, 'spend'));
    
            $insights_group_by_ad_account_id = array_group_by($insights, 'ad_account_id');
    
            $sum_spend_of_duplicated_insight = 0;
            $duplicated_insights = array_reduce($insights_group_by_ad_account_id, function($result, $item) use (&$sum_spend_of_duplicated_insight){ 
                $item_group_by_date_time = array_group_by($item, 'date_time');
                $item_group_by_date_time = array_filter($item_group_by_date_time, function($item) { return count($item) > 1; });
                foreach($item_group_by_date_time as $date_time => $_items){
                    arsort($_items);
                    $instance = reset($_items);
                    if(!isset($result[$instance['ad_account_id']])) $result[$instance['ad_account_id']] = [];
    
                    $sum_spend = array_sum(array_column($_items, 'spend'));
                    $sum_spend_of_duplicated_insight += ($sum_spend - (float)$instance['spend']);
                    
                    $date = my_date($date_time, 'Y-m-d');
                    $result[$instance['ad_account_id']][$date] = $sum_spend_of_duplicated_insight;
                }
    
                return $result;
            }, []);

            $contract_code = get_term_meta_value($contract['term_id'], 'contract_code');
            
            $data = [
                'term_id' => $contract['term_id'],
                'contract_code' => $contract_code,
                'sum_spend' => $sum_spend,
                'sum_spend_of_duplicated_insight' => $sum_spend_of_duplicated_insight,
                'duplicated_insights' => $duplicated_insights,
            ];

            return $data;
        }, $contracts);
        $contracts = array_filter($contracts, function($contract){return !empty($contract['duplicated_insights']);});

        return parent::render([
            'contracts' => $contracts,
            'start_date' => $start_date,
        ]);
    }
    
    /**
     * re_compute_metrics
     *
     * @return void
     */
    function re_compute_metrics(){
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $archor_time = $this->mdate->startOfDay(strtotime('2022-08-01'));

        $contracts = $this->facebookads_m->set_term_type()
            ->join('termmeta', 'termmeta.term_id = term.term_id AND termmeta.meta_key = "start_service_time"')
            ->where('termmeta.meta_value >=', $archor_time)
            ->select('term.term_id')
            ->as_array()
            ->get_all();

        $stat = [
            'total' => count($contracts),
            'processed' => 0,
            'failed' => 0,
        ];
        $contract_ids = array_column($contracts, 'term_id');
        foreach($contract_ids as $contract_id){
            $contract = (new facebookads_m())->set_contract($contract_id);
            if( ! $contract) {
                $stat['failed'] += 1;
                continue;
            }

            try
            {
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();

                $stat['processed'] += 1;
                continue;
            }
            catch (Exception $e)
            {
                $stat['failed'] += 1;
                continue;
            }
        }

        var_dump($stat);
    }
    
    /**
     * migrate_contract_join_excerpt
     *
     * @return void
     */
    public function migrate_contract_join_excerpt()
    {
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        if(! (has_permission('facebookads.metrics_core.update') 
           && has_permission('facebookads.metrics_core.manage')))
        {
            return FALSE;
        }

        $this->load->model('balance_spend_m');

        $join_contract_segments = $this->facebookads_m->set_term_type()
            ->join('term_posts AS tp_join_contract_segments', 'tp_join_contract_segments.term_id = term.term_id')
            ->join('posts AS join_contract_segments', '
                join_contract_segments.post_id = tp_join_contract_segments.post_id
                AND join_contract_segments.post_type = "balance_spend"
                AND join_contract_segments.comment_status = "auto"
                AND join_contract_segments.post_title = "join_command"
                AND (join_contract_segments.post_excerpt = "" OR join_contract_segments.post_excerpt IS NULL)
            ')
            ->as_array()

            ->select('term.*')
            ->select('join_contract_segments.*')
            
            ->get_all();

        $stat = [
            'total' => count($join_contract_segments),
            'processed' => 0,
            'ignored' => 0,
            'data' => [
                'processed' => [],
                'ignored' => [],
            ],
        ];
        
        $is_update = FALSE;
        foreach($join_contract_segments as $join_segment)
        {
            $contract_id = $join_segment['term_id'];
            
            $prev_contract_id = get_term_meta_value($contract_id, 'previousContractId');
            if(empty($prev_contract_id))
            {
                $stat['ignored'] += 1;
                $stat['data']['ignored'][] = [
                    'contract_id' => $contract_id,
                    'post_id' => $join_segment['post_id']
                ];

                continue;
            }

            $compare_contract_id = get_term_meta_value($prev_contract_id, 'nextContractId');
            if($compare_contract_id != $contract_id)
            {
                $stat['ignored'] += 1;
                $stat['data']['ignored'][] = [
                    'contract_id' => $contract_id,
                    'post_id' => $join_segment['post_id']
                ];

                continue;
            }

            $post_excerpt = 'Nối từ HĐ ' . get_term_meta_value($prev_contract_id, 'contract_code');
            $is_update AND $this->balance_spend_m->update($join_segment['post_id'], ['post_excerpt' => $post_excerpt]);

            $stat['processed'] += 1;
            $stat['data']['processed'][] = [
                'contract_id' => $contract_id,
                'post_id' => $join_segment['post_id']
            ];
        }

        dd($stat);
    }

    /**
     * Detects the difference between the advertising time and the segment time.
     *
     * @throws Exception if there is an error while loading the FacebookAds model.
     */
    public function detect_diff_advertise_time_and_semgent_time()
    {
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $this->load->model('facebookads_m');

        $args = $this->input->get();
        $args = wp_parse_args($args, [
            'start_time' => $this->mdate->startOfMonth(),
            'end_time' => $this->mdate->endOfMonth(),
        ]);

        $allow_status = [
            'publish',
            'pending',
            'liquidation',
            'ending',
        ];
        $contracts = $this->facebookads_m->set_term_type()
        ->join('term_posts AS tp_ads_semgent', 'tp_ads_semgent.term_id = term.term_id')
        ->join('posts AS ads_semgent', 'ads_semgent.post_id = tp_ads_semgent.post_id')
        ->join('termmeta AS m_advertise_start_time', 'm_advertise_start_time.term_id = term.term_id AND m_advertise_start_time.meta_key = "advertise_start_time"')
        ->join('termmeta AS m_advertise_end_time', 'm_advertise_end_time.term_id = term.term_id AND m_advertise_end_time.meta_key = "advertise_end_time"')
        
        ->where_in('term.term_status', $allow_status)
        ->where('ads_semgent.post_type', 'ads_segment')

        ->where('m_advertise_start_time.meta_value >=', $args['start_time'])
        ->where('m_advertise_end_time.meta_value <=', $args['end_time'])

        ->group_by('term.term_id')

        ->select('term.term_id AS contract_id')
        ->select('m_advertise_start_time.meta_value AS advertise_start_time')
        ->select('MIN(ads_semgent.start_date) AS ads_semgent_start')
        ->select('m_advertise_end_time.meta_value AS advertise_end_time')
        ->select('MAX(ads_semgent.end_date) AS ads_semgent_end')

        ->having('(advertise_start_time != ads_semgent_start OR advertise_end_time != ads_semgent_end)', FALSE, FALSE)
        
        ->as_array()
        ->get_all();

        $stat = [
            'total' => count($contracts),
            'success' => 0,
            'error' => 0,
            'data' => [
                'success' => [],
                'error' => [],
            ]
        ];

        $is_update = FALSE;
        foreach($contracts as $contract)
        {
            if( $is_update 
                && ($contract['advertise_start_time'] != $contract['ads_semgent_start']))
            {
                $ads_semgent_start = $this->mdate->startOfDay($contract['ads_semgent_start']);
                update_term_meta($contract['contract_id'], 'advertise_start_time', $ads_semgent_start);
            }

            if( $is_update 
                && ($contract['advertise_end_time'] != $contract['ads_semgent_end']))
            {
                $ads_semgent_end = $this->mdate->endOfDay($contract['ads_semgent_end']);
                update_term_meta($contract['contract_id'], 'advertise_end_time', $ads_semgent_end);
            }

            $stat['success'] += 1;
            $stat['data']['success'][] = $contract['contract_id'];
        }

        echo json_encode($stat);
    }

    /**
     * Migrates the join direction for Facebook ads segments.
     *
     * @throws Exception if an error occurs during migration.
     */
    public function migrate_join_direction()
    {
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $this->load->model('facebookads_m');

        $args = $this->input->get();
        $args = wp_parse_args($args, [
            'id_start' => 0,
            'id_end' => 1000,
            'is_update' => FALSE,
        ]);

        $allow_status = [
            'publish',
            'pending',
            'liquidation',
            'ending',
        ];
        $balance_spends = $this->facebookads_m->set_term_type()
        ->join('term_posts AS tp_balance_spend', "tp_balance_spend.term_id = term.term_id")
        ->join('posts AS balance_spend', "balance_spend.post_id = tp_balance_spend.post_id")

        ->where_in('term.term_status', $allow_status)
        ->where('balance_spend.post_type', 'balance_spend')
        ->where('balance_spend.comment_status', 'auto')
        ->where('balance_spend.post_title', 'join_command')

        ->where('balance_spend.post_id >=', (int) $args['id_start'])
        ->where('balance_spend.post_id <= ', (int) $args['id_end'])

        ->group_by('term.term_id, balance_spend.post_id')

        ->select('balance_spend.*, term.term_id')
        
        ->as_array()
        ->get_all();

        $stat = [
            'id_start' => $args['id_start'],
            'id_end' => $args['id_end'],
            'total' => count($balance_spends),
            'success' => 0,
            'ignored' => 0,
            'data' => [
                'success' => [],
                'ignored' => [],
            ]
        ];

        $is_update = $args['is_update'];
        foreach($balance_spends as $balance_spend)
        {
            $join_direction = get_post_meta_value($balance_spend['post_id'], 'join_direction');
            if( !$is_update || !empty($join_direction))
            {
                $stat['ignored'] += 1;
                $stat['data']['ignored'][] = $balance_spend['post_id'];

                continue;
            }

            // Store old values
            $meta = $this->postmeta_m->get_meta($balance_spend['post_id']);
            if(!empty($meta))
            {
                $meta = array_map(function($_meta){
                    $_meta = reset($_meta);
                    return $_meta['meta_value'];
                }, $meta);
            }

            // Update data
            update_post_meta($balance_spend['post_id'], 'join_direction', 'from');
            update_term_meta($balance_spend['term_id'], 'balanceBudgetReceived', $balance_spend['post_content']);

            $stat['success'] += 1;
            $stat['data']['success'][] = $balance_spend['post_id'];
        }

        echo json_encode($stat);
    }

    /**
     * Deletes duplicated balance spend records.
     *
     * @return void
     */
    public function delete_duplicated_balance_spend()
    {
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $this->load->model('facebookads_m');
        $this->load->model('balance_spend_m');

        $args = $this->input->get();
        $args = wp_parse_args($args, [
            'is_delete' => FALSE,
            'selected_contract_ids' => [],
        ]);

        if(!is_array($args['selected_contract_ids']))
        {
            $args['selected_contract_ids'] = [$args['selected_contract_ids']];
        }

        $allow_status = [
            'publish',
            'pending',
            'liquidation',
            'ending',
        ];
        $balance_spends = $this->facebookads_m->set_term_type()
        ->join('term_posts AS tp_balance_spend', "tp_balance_spend.term_id = term.term_id")
        ->join('posts AS balance_spend', "balance_spend.post_id = tp_balance_spend.post_id")

        ->where_in('term.term_status', $allow_status)
        ->where('balance_spend.post_type', 'balance_spend')
        ->where('balance_spend.comment_status', 'auto')
        ->where('balance_spend.post_title', 'join_command')

        ->group_by('term.term_id, balance_spend.post_id')

        ->select('balance_spend.post_id, balance_spend.post_content, term.term_id');

        if(!empty($args['selected_contract_ids']))
        {
            if(count($args['selected_contract_ids']) > 1)
            {
                $balance_spends->where_in('term.term_id', $args['selected_contract_ids']);
            }
            else
            {
                $_contract_id = reset($args['selected_contract_ids']);
                $balance_spends->where('term.term_id', (int) $_contract_id);
            }
        }
        
        $balance_spends = $balance_spends->as_array()->get_all();
        $balance_spends_group_by_contract_id = array_group_by($balance_spends, 'term_id');

        $stat = [
            'total' => count($balance_spends_group_by_contract_id),
            'is_delete' => (bool) $args['is_delete'],
            'success' => 0,
            'ignored' => 0,
            'data' => [
                'success' => [],
                'ignored' => [],
            ]
        ];

        $is_delete = (bool) $args['is_delete'];
        foreach($balance_spends_group_by_contract_id as $contract_id => $balance_spends)
        {
            $balance_spends = array_map(function($balance_spend){
                $balance_spend['join_direction'] = get_post_meta_value($balance_spend['post_id'], 'join_direction');
                
                return $balance_spend;
            }, $balance_spends);

            $is_recalc_contract = FALSE;
            $balance_spends_group_by_join_direction = array_group_by($balance_spends, 'join_direction');
            foreach($balance_spends_group_by_join_direction as $join_direction => $_balance_spends)
            {
                // Remove old segment which is not join_direction value
                if(empty($join_direction))
                {
                    $is_delete && $is_recalc_contract = TRUE;

                    $_balance_spend_post_ids = array_column($_balance_spends, 'post_id');
                    $is_delete AND $this->balance_spend_m->delete_many($_balance_spend_post_ids);

                    continue;
                }

                // Remove segment which num of join_direction grater than 1 AND segment_id is less than max of segment_ids
                $num_of_record = count($_balance_spends);
                if($num_of_record < 2)
                {
                    continue;
                }
                
                $is_delete && $is_recalc_contract = TRUE;

                $_balance_spend_post_ids = array_column($_balance_spends, 'post_id');
                $keep_id = max($_balance_spend_post_ids);
                
                $delete_ids = array_diff($_balance_spend_post_ids, [$keep_id]);
                $is_delete AND $this->balance_spend_m->delete_many($delete_ids);

                $_keep_balance_spend = array_filter($_balance_spends, function($_balance_spend) use ($keep_id){ return $keep_id == $_balance_spend['post_id']; });
                $_keep_balance_spend = reset($_keep_balance_spend);
                if(!empty($_keep_balance_spend))
                {
                    $contract_meta_key = ('from' == $join_direction) ? 'balanceBudgetReceived' : 'balanceBudgetAddTo';
                    $is_delete AND update_term_meta($contract_id, $contract_meta_key, $_keep_balance_spend['post_content']);
                }
            }

            if(!$is_recalc_contract)
            {
                $stat['ignored'] += 1;
                array_push($stat['data']['ignored'], $contract_id);
                continue;
            }

            try
            {
                $contract = (new facebookads_m())->set_contract($contract_id);
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();
                
                $stat['success'] += 1;
                array_push($stat['data']['success'], $contract_id);
            }
            catch(Exception $e)
            {
                log_message('error', $e->getMessage());

                $stat['ignored'] += 1;
                array_push($stat['data']['ignored'], $contract_id);
                continue;
            }

            // Dispatch sync chain
            $this->load->config('amqps');
            
            $amqps_host 	= $this->config->item('host', 'amqps');
            $amqps_port 	= $this->config->item('port', 'amqps');
            $amqps_user 	= $this->config->item('user', 'amqps');
            $amqps_password = $this->config->item('password', 'amqps');
            
            $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
            $queue = $amqps_queues['contract_events'];

            $attemp         = 0;
            $max_attemps    = 10;
            while($attemp < $max_attemps)
            {
                try
                {
                    $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
                    $channel 	= $connection->channel();
                    $channel->queue_declare($queue, false, true, false, false);
                
                    $payload = [
                        'event' => 'contract_chain.facebookads.recalc',
                        'contract_id' => $contract_id,
                    ];
                
                    $message = new \PhpAmqpLib\Message\AMQPMessage(
                        json_encode($payload),
                        array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                    );
                
                    $channel->basic_publish($message, '', $queue);	
                    $channel->close();
                    $connection->close();

                    break;
                }
                catch (\Exception $e)
                {
                    $attemp++;
                }
            }
        }

        echo json_encode($stat);
    }

    /**
     * Deletes duplicated balance spend records.
     *
     * @return void
     */
    public function delete_balance_spend()
    {
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $this->load->model('facebookads_m');
        $this->load->model('balance_spend_m');

        $args = $this->input->get();
        $args = wp_parse_args($args, [
            'is_delete' => FALSE,
            'selected_posts_ids' => [],
        ]);

        if(!is_array($args['selected_posts_ids']))
        {
            $args['selected_posts_ids'] = [$args['selected_posts_ids']];
        }

        $allow_status = [
            'publish',
            'pending',
            'liquidation',
            'ending',
        ];
        $balance_spends = $this->facebookads_m->set_term_type()
        ->join('term_posts AS tp_balance_spend', "tp_balance_spend.term_id = term.term_id")
        ->join('posts AS balance_spend', "balance_spend.post_id = tp_balance_spend.post_id")

        ->where_in('term.term_status', $allow_status)
        ->where('balance_spend.post_type', 'balance_spend')
        ->where('balance_spend.comment_status', 'auto')
        ->where('balance_spend.post_title', 'join_command')

        ->group_by('balance_spend.post_id, balance_spend.post_id')

        ->select('balance_spend.post_id, balance_spend.post_content, term.term_id');

        if(!empty($args['selected_posts_ids']))
        {
            if(count($args['selected_posts_ids']) > 1)
            {
                $balance_spends->where_in('balance_spend.post_id', $args['selected_posts_ids']);
            }
            else
            {
                $_balance_spend_id = reset($args['selected_posts_ids']);
                $balance_spends->where('balance_spend.post_id', (int) $_balance_spend_id);
            }
        }
        
        $balance_spends = $balance_spends->as_array()->get_all();
        $balance_spends_group_by_contract_id = array_group_by($balance_spends, 'term_id');

        $stat = [
            'total' => count($balance_spends_group_by_contract_id),
            'is_delete' => (bool) $args['is_delete'],
            'success' => 0,
            'ignored' => 0,
            'data' => [
                'success' => [],
                'ignored' => [],
            ]
        ];

        $is_delete = (bool) $args['is_delete'];
        foreach($balance_spends_group_by_contract_id as $contract_id => $balance_spends)
        {
            $balance_spends = array_map(function($balance_spend){
                $balance_spend['join_direction'] = get_post_meta_value($balance_spend['post_id'], 'join_direction');
                
                return $balance_spend;
            }, $balance_spends);

            $is_recalc_contract = FALSE;
            $balance_spends_group_by_join_direction = array_group_by($balance_spends, 'join_direction');
            foreach($balance_spends_group_by_join_direction as $join_direction => $_balance_spends)
            {
                $is_delete && $is_recalc_contract = TRUE;

                $_balance_spend_post_ids = array_column($_balance_spends, 'post_id');
                $is_delete AND $this->balance_spend_m->delete_many($_balance_spend_post_ids);

                $contract_meta_key = ('from' == $join_direction) ? 'balanceBudgetReceived' : 'balanceBudgetAddTo';
                $is_delete AND update_term_meta($contract_id, $contract_meta_key, 0);
            }

            if(!$is_recalc_contract)
            {
                $stat['ignored'] += 1;
                array_push($stat['data']['ignored'], $contract_id);
                continue;
            }

            $contract = (new facebookads_m())->set_contract($contract_id);
            try
            {
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();
                
                $stat['success'] += 1;
                array_push($stat['data']['success'], $contract_id);
            }
            catch(Exception $e)
            {
                log_message('error', $e->getMessage());

                $stat['ignored'] += 1;
                array_push($stat['data']['ignored'], $contract_id);
                continue;
            }

            // Dispatch sync chain
            $this->load->config('amqps');
            
            $amqps_host 	= $this->config->item('host', 'amqps');
            $amqps_port 	= $this->config->item('port', 'amqps');
            $amqps_user 	= $this->config->item('user', 'amqps');
            $amqps_password = $this->config->item('password', 'amqps');
            
            $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
            $queue = $amqps_queues['contract_events'];

            $attemp         = 0;
            $max_attemps    = 10;
            while($attemp < $max_attemps)
            {
                try
                {
                    $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
                    $channel 	= $connection->channel();
                    $channel->queue_declare($queue, false, true, false, false);
                
                    $payload = [
                        'event' => 'contract_chain.facebookads.recalc',
                        'contract_id' => $contract_id,
                    ];
                
                    $message = new \PhpAmqpLib\Message\AMQPMessage(
                        json_encode($payload),
                        array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                    );
                
                    $channel->basic_publish($message, '', $queue);	
                    $channel->close();
                    $connection->close();

                    break;
                }
                catch (\Exception $e)
                {
                    $attemp++;
                }
            }
        }

        echo json_encode($stat);
    }

    /**
     * Fixes the join duplicated.
     *
     * @return bool
     */
    public function join_tracking(){
        if(!has_permission('facebookads.fix.manage'))
        {
            return FALSE;
        }

        $allow_contract_status = ['publish', 'pending', 'ending', 'liquidation'];
        $this->load->model('balance_spend_m');
        $balance_spend_items = $this->facebookads_m
            ->set_term_type()
            ->join('term_posts AS tp_balance_spend', 'tp_balance_spend.term_id = term.term_id')
            ->join('posts AS balance_spend', 'balance_spend.post_id = tp_balance_spend.post_id')
            
            ->where_in('term.term_status', $allow_contract_status)
            ->where('balance_spend.post_type', 'balance_spend')
            ->where('balance_spend.comment_status', 'auto')
            ->where('balance_spend.post_title', 'join_command')
            
            ->group_by('
                term.term_id,
                balance_spend.post_id
            ')

            ->order_by('term.term_id', 'desc')

            ->select('
                term.term_id,
                balance_spend.*
            ')
            ->as_array()
            ->get_all();
        if(empty($balance_spend_items))
        {
            echo 'Không tìm thấy spend nối';
            
            return TRUE;
        }

        $balance_spend_items = array_map(function($item){
            $item['join_direction'] = get_post_meta_value($item['post_id'], 'join_direction');
            $item['post_content'] = numberformat($item['post_content']);

            return $item;
        }, $balance_spend_items);

        $balance_spend_items = array_group_by($balance_spend_items, 'term_id');
        $balance_spend_items = array_map(function($items){
            $items_group_by_join_direction = array_group_by($items, 'join_direction');

            // Detect join_direction is empty
            $items_empty = $items_group_by_join_direction[''] ?? [];
            if(!empty($items_empty))
            {
                foreach($items_empty as &$item)
                {
                    $item['error'] = 'Không có meta "join_direction"';
                }

                return $items;
            }
            
            // Detect join_direction "to" is more than 1
            $items_to = $items_group_by_join_direction['to'] ?? [];
            if(!empty($items_to))
            {
                if(count($items_to) > 1)
                {
                    foreach($items_to as &$item)
                    {
                        $item['error'] = 'Trùng join_direction "to"';
                    }

                    return $items;
                }

                // Detect join_direction "to" has invalid
                $to_contract = reset($items_to);
                $to_contract_id = $to_contract['term_id'];
                $next_contract_id = get_term_meta_value($to_contract_id, 'nextContractId');
                if(empty($next_contract_id))
                {
                    $items[0]['error'] = 'Không có nối';

                    return $items;
                }
            }

            // Detect join_direction "from" is more than 1
            $items_from = $items_group_by_join_direction['from'] ?? [];
            if(!empty($items_from))
            {
                if(count($items_from) > 1)
                {
                    foreach($items_from as &$item)
                    {
                        $item['error'] = 'Trùng join_direction "from"';
                    }

                    return $items;
                }

                // Detect join_direction "from" has invalid
                $from_contract = reset($items_from);
                $from_contract_id = $from_contract['term_id'];
                $prev_contract_id = get_term_meta_value($from_contract_id, 'previousContractId');
                if(empty($prev_contract_id))
                {
                    $items[0]['error'] = 'Không có nối';

                    return $items;
                }
            }
            
            return NULL;
        }, $balance_spend_items);
        $balance_spend_items = array_filter($balance_spend_items);

        echo '<p>Số lượng: ' . count($balance_spend_items) . '</p>';

        if(empty($balance_spend_items))
        {
            return TRUE;
        }

        echo '
            <p>
                <b>Cách kiểm tra:</b></br>
                Dữ liệu đúng khi:
                <ul>
                    <li>
                        Chỉ có 1 record "Loại" <b>from</b>
                    </li>
                    <li>
                        Chỉ có 1 record "Loại" <b>to</b>
                    </li>
                </ul>
                <b>Kịch bản Xử lý:</b></br>
                <u>Trùng join_direction</u>
                <ul>
                    <ol>
                        Kiểm tra lại dữ liệu bằng cách click vào "Đi tới Overview". Nếu xác định không có lỗi, bỏ qua và không cần xử lý
                    </ol>
                    <ol>
                        Xoá trùng bằng cách click vào "Xoá trùng", kiểm tra kết quả dự kiến đã đúng hay chưa.
                    </ol>
                </ul>

                <u>Không có join_direction</u>
                <ul>
                    <ol>
                        Kiểm tra lại dữ liệu bằng cách query post meta "join_direction" với post_id tương ứng. Nếu xác định không có lỗi, bỏ qua và không cần xử lý
                    </ol>
                    <ol>
                        Xoá record bằng cách click vào "Xoá record", update URL query params is_delete=1 để xoá<br>
                        <span style="color: red">* Dữ liệu bị xoá không thể hoàn tác</span>
                    </ol>
                    <ol>
                        Đi đến trang Setting và thực hiện fix nối
                    </ol>
                </ul>

                <u>Không có nối</u>
                <ul>
                    <ol>
                        Kiểm tra lại dữ liệu bằng cách query term meta "nextContractId"/"previousContractId" với term_id tương ứng. Nếu xác định không có lỗi, bỏ qua và không cần xử lý
                    </ol>
                    <ol>
                        Xoá record bằng cách click vào "Xoá record", update URL query params is_delete=1 để xoá<br>
                        <span style="color: red">* Dữ liệu bị xoá không thể hoàn tác</span>
                    </ol>
                </ul>
            </p>
        ';

        echo '
            <table style="width: 100vw">
                <thead>
                    <tr>
                        <th style="border: 1px solid">ID Hợp đồng</th>
                        <th style="border: 1px solid">ID join spend</th>
                        <th style="border: 1px solid">Mô tả</th>
                        <th style="border: 1px solid">Số nối</th>
                        <th style="border: 1px solid">Loại</th>
                        <th style="border: 1px solid">Lỗi</th>
                        <th style="border: 1px solid">Điều hướng</th>
                        <th style="border: 1px solid">Actions</th>
                    </tr>
                </thead>
                <tbody>
        ';
        
        $base_url = base_url();
        foreach($balance_spend_items as $items)
        {
            $instance = reset($items);

            echo "<tr><td colspan='8' style='border: 1px solid;'><b style='color: red'>{$instance['term_id']}</b></td></tr>";

            foreach($items as $item)
            {
                echo "
                    <tr>
                        <td style='border-left: 1px solid; border-bottom: 1px dotted; padding: 8px 4px'>{$item['term_id']}</td>
                        <td style='border-left: 1px solid; border-bottom: 1px dotted; padding: 8px 4px'>{$item['post_id']}</td>
                        <td style='border-left: 1px solid; border-bottom: 1px dotted; padding: 8px 4px'>{$item['post_excerpt']}</td>
                        <td style='border-left: 1px solid; border-bottom: 1px dotted; padding: 8px 4px'>{$item['post_content']}</td>
                        <td style='border-left: 1px solid; border-bottom: 1px dotted; padding: 8px 4px'>{$item['join_direction']}</td>
                        <td style='border-left: 1px solid; border-bottom: 1px dotted; padding: 8px 4px'>" . ($item['error'] ?? '--') ."</td>
                        <td style='border-left: 1px solid; border-bottom: 1px dotted; padding: 8px 4px'>
                            <a href='{$base_url}/admin/facebookads/overview/{$item['term_id']}' target='_blank'>Đi tới Overview</a>
                            <a href='{$base_url}/admin/facebookads/setting/{$item['term_id']}' target='_blank'>Đi tới Setting</a>
                        </td>
                        <td style='border-left: 1px solid; border-bottom: 1px dotted; padding: 8px 4px'>
                            <a href='{$base_url}/facebookads/fix/delete_duplicated_balance_spend?selected_contract_ids[]={$item['term_id']}&is_delete=1' target='_blank'>Xoá trùng</a>
                            <a href='{$base_url}/facebookads/fix/delete_balance_spend?selected_posts_ids[]={$item['post_id']}&is_delete=0' target='_blank'>Xoá record</a>
                        </td>
                    </tr>
                ";
            }
        }
        
        echo '
                </tbody>
            </table>
        ';
    }
}
/* End of file Fix.php */
/* Location: ./application/modules/facebookads/controllers/Fix.php */