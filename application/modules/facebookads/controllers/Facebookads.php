<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebookads extends Admin_Controller 
{
	private $term;
	private $term_type = 'facebook-ads';
	public  $model 	   = 'facebookads_contract_m';

	public function __construct(){

		parent::__construct();

		$models = [
			'history_m',
			'term_users_m',
			'staffs/sale_m',
			'customer/customer_m',
			'contract/contract_m',
			'contract/contract_wizard_m',
			'contract/invoice_m',
			'facebookads/facebookads_kpi_m',
			'facebookads/facebookads_contract_m',
			'facebookads/facebookads_m',
			'facebookads/facebookads_report_m',
			'facebookads/adaccount_m',
			'facebookads/adcampaign_m',
			'staffs/staffs_m',
			'ads_segment_m'
		];

		$this->load->model($models);
		

		$this->load->config('contract/contract');
		$this->load->config('staffs/group');
		$this->load->config('contract/invoice');
		$this->load->config('facebookads/facebookads');

		// kiểm tra facebookads có tồn tại và user có quyền truy cập ?
		$this->init_website();
	}


	/**
	 * Init|auth|prepare data before access admin
	 */
	private function init_website()
	{
		$term_id = $this->uri->segment(4);
		$method  = $this->uri->segment(3);
		$is_allowed_method = (!in_array($method, array('index','done')));
		if(!$is_allowed_method || empty($term_id)) return FALSE;


		$term = $this->facebookads_m->select('term_id,term_name,term_parent,term_slug,term_type,term_status')->set_term_type()->get($term_id);
		
		if(empty($term))
		{
			$this->messages->error('Hợp đồng không tồn tại hoặc không có quyền truy cập hợp đồng này !');
			redirect(module_url(),'refresh');	
		}

		if( ! $this->is_assigned($term_id) && 'kpi' != $method && 'overview2' != $method)
		{
			$this->messages->error('Truy cập #'.$term_id.' không hợp lệ !!!');
			redirect(module_url(), 'refresh');	
		}

		$contract_code = get_term_meta_value($term_id, 'contract_code');
		$this->template->title->set(strtoupper(' '.($contract_code ?: $term->term_name)));
		$this->website_id = $this->data['term_id'] = $term_id;
		$this->data['term'] = $this->term = $term;
	}


	/**
	 * Render "Publish" contract datatable
	 */
	public function index()
	{
		restrict('facebookads.index.access');

		$this->template->is_box_open->set(1);
		$this->template->title->set('Tổng quan dịch vụ Facebook ADS');
		$this->template->javascript->add(base_url("dist/vFacebookadsAdminIndexPage.js"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
        $this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		$this->template->publish();
	}

	/**
	 * The page mapping contract with staffs
	 */
	public function staffs()
	{
		restrict('facebookads.staffs');

        $this->template->is_box_open->set(1);
		$this->template->title->set('Bảng phân công phụ trách thực hiện');
		$this->template->javascript->add(base_url("dist/vFacebookConfigurationAssignedKpi.js"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
        $this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
		$this->template->publish();
	}
	
	/**
	 * Render "Publish" contract datatable
	 */
	public function overview($term_id)
	{
        if(FALSE === $this->facebookads_m->set_contract($term_id))
		{
			$this->messages->error('Hợp đồng không tồn tại');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === $this->facebookads_m->can('facebookads.Overview.access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === is_service_start($term_id))
		{
			$this->messages->error('Hợp đồng chưa được kích hoạt');
			redirect(admin_url(), 'refresh');
		}

		$this->template->is_box_open->set(1);
		$this->template->title->set( get_term_meta_value($term_id, 'contract_code') . ' | Dịch vụ Facebook');
		$this->template->javascript->add(base_url("dist/vFacebookadsAdminViewPage.js"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));

		$this->template->javascript->add('https://code.highcharts.com/highcharts.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/variable-pie.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/exporting.js');
		$this->template->javascript->add('https://www.gstatic.com/charts/loader.js');
		$this->template->footer->prepend('<script type="text/javascript">var contractId = '.$term_id.'</script>');
		$this->template->publish();
	}

	public function kpi($term_id = 0, $delete_id= 0)
	{
        if(FALSE === $this->facebookads_m->set_contract($term_id))
		{
			$this->messages->error('Hợp đồng không tồn tại');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === $this->facebookads_m->can('facebookads.kpi.Access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === is_service_start($term_id))
		{
			$this->messages->error('Hợp đồng chưa được kích hoạt');
			redirect(admin_url(), 'refresh');
		}

		$this->config->load('facebookads/group') ;			
		$this->template->title->prepend('KPI');

		$data = $this->data;
		$this->submit_kpi($term_id,$delete_id);

		$targets = $this->facebookads_kpi_m
		->as_array()
		->order_by('kpi_datetime')
		->get_many_by([ 'term_id' => $term_id ]);

		$data['targets'] = array();

		$data['targets']['tech'] = array();
		foreach($targets as $target)
		{
			$data['targets'][$target['kpi_type']][] = $target;
		}
		
		$roleIds 				= $this->option_m->get_value('group_fb_ads_role_ids', true);
		$data['technicians'] 	= $this->admin_m->select('user_id,display_name,user_email, role.role_name')->join('role', 'role.role_id = user.role_id')->where_in('user.role_id', array_map('intval', $roleIds))->set_get_active()->order_by('role_name')->get_all();
		// if( ! empty($technicians)) foreach ($technicians as $user) $data['users'][$user->user_id] = implode(' - ', [ $user->role_name, $user->display_name, $user->user_email ]);

		$this->data = $data;
		parent::render($this->data);
	}

	protected function submit_kpi($term_id = 0,$delete_id=0)
	{
		if($delete_id > 0)
		{
			$this->_delete_kpi($term_id,$delete_id);
			redirect(module_url('kpi/'.$term_id.'/'),'refresh');
		}

		$post = $this->input->post(NULL, TRUE);
		if(empty($post)) return FALSE;

		if(FALSE === $this->facebookads_m->has_permission($term_id, 'facebookads.Kpi.Add'))
		{
			$this->messages->error('Không có quyền thực hiện tác vụ này');
			redirect(current_url(), 'refresh');
		}

		$insert = array();
		$insert['kpi_datetime'] = $this->input->post('target_date', TRUE);
		$insert['user_id'] 		= $this->input->post('user_id', TRUE);
		$insert['kpi_value'] 	= (int)$this->input->post('target_post', TRUE);

		$kpi_type 				= $this->input->post('target_type', TRUE);

		if($insert['kpi_datetime'] < $this->mdate->startOfMonth())
		{
			$this->messages->error('Cập nhật không thành công do tháng cập nhật nhỏ hơn tháng hiện tại');
			redirect(current_url(),'refresh');
		}

		$this->facebookads_kpi_m->update_kpi_value($term_id, $kpi_type,$insert['kpi_value'], $insert['kpi_datetime'], $insert['user_id']);

		# Update relation of term's user in term_users table
		$this->term_users_m->set_relations_by_term($term_id,[$post['user_id']], 'admin');

		/* Update Technician to Contract */
        $this->facebookads_m->set_contract($term_id)->get_behaviour_m()->setTechnicianId();
		
		$this->messages->success('Cập nhật thành công');

		redirect(current_url(),'refresh');
	}

	/**
	 * Delete the KPI record in googleads_kpi_m
	 * @param  integer	$term_id	
	 * @param  integer	$delete_id		
	 * @return boolean
	 */
	private function _delete_kpi($term_id = 0, $delete_id = 0)
	{
		if(FALSE === $this->facebookads_m->has_permission($term_id, 'facebookads.Kpi.Delete'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			return FALSE;
		}

		$kpi = $this->facebookads_kpi_m->get($delete_id);

		if( ! $kpi)
		{
			$this->messages->error('Xóa không thành công, dữ liệu không tồn tại!');
			return FALSE;
		}

		// Xóa kpi thành công
		$this->facebookads_kpi_m->delete_cache($delete_id);
		$this->facebookads_kpi_m->delete($delete_id);

		$this->facebookads_m->set_contract($term_id)->get_behaviour_m()->setTechnicianId();

        /* Update Technician to Contract */

		$this->messages->success('Xóa thành công');

		// Nếu KPI đang dành cho bộ phận khác kỹ thuật thì kết thúc quá trình xử lý
		if($kpi->kpi_type != 'tech') return TRUE;

		$sale_role_ids 	= $this->option_m->get_value('group_sales_ids', TRUE) ?: [];
		$role_id 		= $this->admin_m->get_field_by_id($kpi->user_id, 'role_id');

		// NẾU KPI CÓ VAI TRÒ NVKD THÌ KHÔNG CẦN XỬ LÝ TIẾP
		if(empty($sale_role_ids) || in_array($role_id, $sale_role_ids)) return TRUE;

		// TRƯỜNG HỢP KPI KHÁC CỦA USER VẪN CÒN TỒN TẠI
		$kpis = $this->facebookads_kpi_m->get_many_by(['term_id'=>$term_id,'user_id'=>$kpi->user_id]);
		if( ! empty($kpis)) return TRUE;

		// NẾU USER KHÔNG CÒN BẤT KỲ LIÊN HỆ NÀO VỚI HỢP ĐỒNG => XÓA TERM_USERS
		$this->term_users_m->delete_term_users($term_id, [$kpi->user_id]);
		return TRUE;
	}


	public function setting($term_id = 0)
	{
		if(FALSE === $this->facebookads_m->set_contract($term_id))
		{
			$this->messages->error('Hợp đồng không tồn tại');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === $this->facebookads_m->can('facebookads.setting.access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === is_service_start($term_id))
		{
			$this->messages->error('Hợp đồng chưa được kích hoạt');
			redirect(admin_url(), 'refresh');
		}

		$this->submit($term_id);

		$this->template->title->prepend('Cấu hình');
		
		$data = $this->data;
		$data['term'] = $this->term;
		$data['term_id'] = $term_id;
		$this->template->content_box_class->set(time());
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
        $this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		parent::render($data);
	}

	protected function submit($term_id = 0)
	{
		$post = $this->input->post(null, true);
		if(empty($post)) return FALSE;

		if( ! $this->facebookads_m->set_contract((int) $term_id))
		{
			$this->messages->error('Dịch vụ không tồn tại hoặc không có quyền truy cập');
			redirect(module_url("overview/{$term_id}"), 'refresh');
		}

		$term = $this->facebookads_m->get_contract();
		
		$manager_fb_ads_role_id = $this->option_m->get_value('manager_fb_ads_role_id');

		if( in_array($term->term_status, [ 'ending', 'liquidation' ]) && ! in_array( $this->admin_m->role_id, [ $manager_fb_ads_role_id, 1]))
		{
			$this->messages->error('Hợp đồng đã kết thúc và quyền truy cập không phù hợp để thực hiện tác vụ này');
			redirect(current_url(), 'refresh');
		}

		// Update Service infomation
		if($this->input->post('submit') !== NULL)
		{
			if( ! $this->facebookads_m->can('facebookads.setting.update')) restrict('facebookads.setting.update', current_url());

			if(empty($post['edit']['meta'])) 
			{
				$this->messages->error('Dữ liệu input không hợp lệ');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$metadata = $post['edit']['meta'];

			/* Update curators box */
			if(!empty($metadata['curator_name']))
			{
				$i = 0;
				$curators = array();
				$count = count($metadata['curator_name']);

				for ($i; $i < $count; $i++)
				{
					$curators[] = array(
						'name' => $metadata['curator_name'][$i],
						'phone' => $metadata['curator_phone'][$i],
						'email' => $metadata['curator_email'][$i]
						);
				}

				$contract_curators = serialize($curators);

				$this->history_m->table = $this->term_m->_table;
				$this->history_m->object_id = $term_id;
				$this->history_m
				->set_original_data(['contract_curators'=>get_term_meta_value($term_id,'contract_curators')])
				->get_dirty(['contract_curators'=>$contract_curators])
				->save();

				update_term_meta($term_id,'contract_curators',$contract_curators);
				$this->messages->success('Cập nhật thành công !');

				unset($metadata['curator_name'],$metadata['curator_phone'],$metadata['curator_email']);
			}

			empty($metadata['advertise_start_time']) 	OR $metadata['advertise_start_time'] 	= start_of_day($metadata['advertise_start_time']);
			empty($metadata['advertise_end_time']) 		OR $metadata['advertise_end_time'] 		= start_of_day($metadata['advertise_end_time']);
			
			if($metadata)
			{
				foreach ($metadata as $meta_key => $meta_value)
				{
					update_term_meta($term_id,$meta_key,$meta_value);
				}

				$this->messages->success('Cập nhật thành công');	
			}
			
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
	}

	protected function is_assigned($term_id = 0,$kpi_type = '')
	{
		// check user has manager role permission
		$class  = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		$result = $this->facebookads_m->has_permission($term_id);
		return $result;
	}
}
/* End of file facebookads.php */
/* Location: ./application/modules/facebookads/controllers/facebookads.php */