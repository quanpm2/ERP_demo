<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adcampaign extends API_Controller {

	function __construct()
	{
		parent::__construct();

		$models = array(
			'facebookads/facebookads_m',
			'facebookads/adcampaign_m'
		);

		$this->load->model($models);
	}

	public function list($term_id = 0)
	{
		$defaults = array(
			'orderby'=>'start_date',
			'order'=>'ASC',
			'fields'=>'all',
			'where'=>'',
			'start_time' => $this->mdate->startOfDay(strtotime('2017/08/21')),
			'end_time' => $this->mdate->endOfDay(),
		);
		$args 			= wp_parse_args($this->get(),$defaults);
		$args_encrypt	= md5(json_encode($args));


		/**
		 * Truy xuất dữ liệu hợp đồng input và xác thực thông tin
		 *
		 * @var        facebookads_m
		 */
		$term = $this->facebookads_m->set_term_type()->get($term_id);
		if( ! $term)
		{
			$response['msg'] = 'Hợp đồng không tồn tại hoặc không hợp lệ !';
			return parent::render($response,500);
		}

		/**
		 * Truy xuất thông tin các campaign được kết nối với hợp đồng
		 *
		 * @var        array
		 */
		$adcampaign_ids = get_term_meta_value($term_id,'adcampaign_ids');
        $adcampaign_ids = $adcampaign_ids ? unserialize($adcampaign_ids) : array();
        if( ! $adcampaign_ids)
		{
			$response['msg'] = 'Cấu hình chiến dịch tạm thời không khả dụng !';
			return parent::render($response,500);
		}

		/**
		 * Dữ liệu tất cả các campaign quảng cáo đang thực hiện
		 *
		 * @var        array
		 */
		$adcampaign_insight = array();
		$start_time = $args['start_time'];
		$end_time 	= $args['end_time'];
        foreach ($adcampaign_ids as $adcampaign_id)
        {
        	$adcamp_insight = $this->adcampaign_m->get_data($adcampaign_id,$start_time,$end_time);

        	if(empty($adcamp_insight)) continue;
        	
        	$adcampaign_insight = array_merge($adcampaign_insight,$adcamp_insight);
        }

        if(empty($adcampaign_insight))
		{
			$response['msg'] = 'Không tìm thấy dữ liệu !';
			return parent::render($response,500);
		}

        $adcampaign_insight = array_group_by($adcampaign_insight,'date_start');

        $datatable = array();
        foreach ($adcampaign_insight as $date => $campaigns)
        {
        	$date = my_date($this->mdate->startOfDay($date),'d/m/Y');
        	if(empty($campaigns)) continue;


        	foreach ($campaigns as $campaign)
        	{
				/* Ngày | Tên chiến dịch | Tổng hiển thị | Tiếp Cận | Tổng kết quả | Giá mỗi kết quả | Clicks | Chi phí CLicks | Chi phí | Đối tượng */
        		$datatable[] = array(
        			'date' => $date,
        			'campaign_id' 		=> $campaign['campaign_id'],
        			'campaign_name' 	=> $campaign['campaign_name'],
        			'objective' 		=> $campaign['objective'],
        			'reach' 			=> $campaign['reach'],
        			'impressions' 		=> $campaign['impressions'],
        			'result' 			=> $campaign['result'],
        			'cost_per_result' 	=> $campaign['cost_per_result'],
        			'clicks' 			=> $campaign['clicks'],
        			'cpc' 				=> $campaign['cpc'],
        			'spend' 			=> $campaign['spend']
        		);
        	}
        }

        $response['data'] = $datatable;
        $this->default_response['auth'] = 1;
		return parent::render($response);
	}
}
/* End of file Adcampaign.php */
/* Location: ./application/modules/Adcampaign/controllers/Adcampaign.php */