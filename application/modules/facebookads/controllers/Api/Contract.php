<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends API_Controller {

	function __construct()
	{
		parent::__construct();

		$models = array(
			'api_user_m',
            'facebookads/api_facebookads_m',
			'facebookads/facebookads_m',
			'message/message_m'
			);

		$this->load->model($models);
		$this->load->config('contract/contract');
	}


	public function list()
	{
        if( ! $this->authenticate) return parent::render();

		$response = array('status' => 1, 'data' => array());

		$terms = $this->api_facebookads_m->get_contract_list_by($this->token);
		if( ! $terms)
		{
			return parent::render($response);
		}

		$response['data'] = $terms;

		$this->default_response['auth'] = 1;
		return parent::render($response);
	}

    public function info()
    {
        if(!$this->authenticate) return parent::render();

        $response = array('status' => 0, 'data' => array());

        $id = (int) $this->post('id');
        $token = $this->get('token');

        $response['data'] = $this->api_facebookads_m->get_contract_info($id, $token);
        $response['status'] = 1;

        $this->default_response['auth'] = 1;
        return parent::render($response);
    }
}
/* End of file Contract.php */
/* Location: ./application/modules/facebookads/controllers/Api/Contract.php */