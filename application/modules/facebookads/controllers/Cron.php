<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Cron extends Public_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$models = array(
			'log_m',
			'facebookads/facebookads_m',
			'facebookads/facebookads_report_m',
			'facebookads/adcampaign_m',
			'facebookads/fbads_report_m'
		);

		$this->load->model($models);
	}

	public function index() 
	{
        if( ! parent::check_token())
        {
            log_message('error',"#fbamod/cron/index has been illegal access to");
            return FALSE;
        }

		if(ENVIRONMENT != 'production') return FALSE;

		$hour = date('G');

		$token = parent::build_token();

		if(in_array($hour, array(5)))
        {
			// try
			// {
			// 	Requests::get(base_url("admin/facebookads/cron/request_fbadsapi?token={$token}"),[],['timeout' => 1,'connect_timeout' => 10]);
			// }
	  //       catch(Exception $e) { /*do nothing*/ }
		}
		else if(in_array($hour, [9]))
		{
			// Call curl for SMS DAILY REPORT + SEND WEEKLY REPORT and no need to wait
	        // try{ Requests::get(base_url("admin/facebookads/cron/scheduled_reporting?token={$token}"),[],['timeout' => 1,'connect_timeout' => 10]); }
	        // catch(Exception $e) { /*do nothing*/ }
		}

		else if($hour >= 7)
		{
			$this->proc_alert_services();
		}
	}


	/**
	 * Send daily sms and weekly report email
	 */
	public function scheduled_reporting()
	{
		if( ! parent::check_token())
        {
            log_message('error','#fbamod/cron/index has been illegal access to');
            return FALSE;
        }

		$this->send_weekly_report_email();
		$this->send_daily_sms();
		$this->facebookads_report_m->send_statistical_cronmail2admin();
	}


	/**
	 * Sends daily sms.
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function send_daily_sms()
	{
		$terms = $this->facebookads_m->get_all_active();

		if(empty($terms)) return FALSE;

		$sentIds = array();

		$logs = $this->log_m->select('term_id')->get_many_by(['log_type' => 'facebookads_report_sms','log_status > 0','log_time_create >= curdate()','log_time_create < curdate() + interval 1 day']) AND $sentIds = array_column($logs, 'term_id');

		foreach ($terms as $term)
		{
			if(in_array($term->term_id, $sentIds)) continue;

			/* Redownloaded all campaigms insight data */

			try
			{
				$insight = (new facebookads_m())->set_contract($term)->get_behaviour_m()->get_the_progress(strtotime('-3 days'),time());
		   		if(empty($insight)) continue;
				$this->facebookads_report_m->send_sms($term->term_id, 'yesterday', TRUE);
			}
			catch (Exception $e) {
				continue;
			}
		}

		return TRUE;
	}


	/**
	 * Sends a weekly report email.
	 */
	private function send_weekly_report_email()
	{
        $terms = $this->facebookads_m->get_all_active();
		if(empty($terms)) return FALSE;

		$log_term_ids = array();
		if($logs = $this->log_m->select('term_id')->get_many_by(['log_type' => 'facebookads_report_email','log_status > 0','log_time_create >= curdate()','log_time_create < curdate() + interval 1 day'
			])) 
		{	
			$log_term_ids = array_column($logs, 'term_id');
		}

		$now = time();

		foreach ($terms as $term)
		{
			if(in_array($term->term_id, $log_term_ids)) continue;

			$duration_day = div(($now - $term->begin_time),86400);
			if($duration_day < 7) return FALSE;

			$weeks_count = round(div($duration_day,7));
			$report_time = $term->begin_time + ($weeks_count*604800);

			$report_date = my_date($report_time);
			if($report_date != my_date()) return FALSE;

			$start_time = strtotime('-7 days',$report_time);
			$end_time = strtotime('-1 days',$report_time);

			/* Redownloaded all campaigms insight data */
			$is_success = (new facebookads_m())->set_contract($term)->get_behaviour_m()->get_the_progress($start_time,$end_time);
	   		if(empty($is_success)) continue;

	   		/* Send week report */
	   		$is_send = $this->facebookads_report_m->send_report_email($term->term_id,$start_time,$end_time);
		}
	}
	
	/**
	 * CRON actions that get new and Sending mail alert report
	 *
	 * @param      array   $terms  The terms
	 *
	 * @return     bool  Result of function
	 */
	public function proc_alert_services_bak()
	{	
		if( ! parent::check_token())
		{
			log_message('error','#fbamod/cron/index has been illegal access to');
  			return FALSE;
		}

        $terms = $this->facebookads_m->get_all_active();
		if(empty($terms)) return FALSE;

		$end_time 	= $this->mdate->endOfDay();
		$start_time = $this->mdate->startOfDay(strtotime('-10 days',$end_time));
		$now 		= time();

    	foreach ($terms as $term) 
    	{
    		$now = time();
    		$time_next_api_check = (int) get_term_meta_value($term->term_id,'time_next_api_check');
    		if(!empty($time_next_api_check) && $time_next_api_check > $now) continue;
    		
    		/* Init the instance of contract behaviour_m */
    		$contract = (new facebookads_m())->set_contract($term);
    		$contract->update_insights();
    		
    		$behaviour_m 	= $contract->get_behaviour_m();
    		$progress 		= 0;

			try
			{	
				$behaviour_m->get_the_progress(); /* Get progress info */
				$behaviour_m->sync_all_amount(); /* Update all metadata in relations */
			}
			catch (Exception $e)
			{
				var_dump($e->getMessage());
			}

    		$actual_progress_percent_net = ((float) get_term_meta_value($term->term_id, 'actual_progress_percent_net'))*100;
			if($actual_progress_percent_net >= 80) /* Xử lý theo Ngân sách hợp đồng */
			{	
				$this->send_progress_email($term, 'actual', TRUE); //gửi mail thông báo đến admin và các bên liên quan
				$time_next_api_check = strtotime('+1 hour',$now);
			}
			else $time_next_api_check = strtotime('+1 hour',$now);

    		if(empty($progress)) continue;


			if($progress >= 100)
			{
	    		# gửi kèm mail [demo] báo cáo kết thúc gửi cho khách hàng đến admin và các bên liên quan để xem trước
	    		$this->facebookads_report_m->send_finished_service($term->term_id,'admin');
    			update_term_meta($term->term_id, 'time_next_api_check', strtotime('+1 hour',$now));
    			continue;
			}

			if($progress >= 96)
			{
    			update_term_meta($term->term_id, 'time_next_api_check', strtotime('+1 hour',$now));
    			continue;
			}

			if($progress >= 80)
			{
				empty($notice_finish_mail_has_sent) AND update_term_meta($term->term_id, 'notice_finish_mail_has_sent',1);
    			update_term_meta($term->term_id, 'time_next_api_check', strtotime('+2 hour',$now));
    			continue;
			}

	  		update_term_meta($term->term_id, 'time_next_api_check', strtotime('+6 hour',$now));
    	}
	}

	/**
	 * CRON actions that get new and Sending mail alert report
	 *
	 * @param      array   $terms  The terms
	 *
	 * @return     bool  Result of function
	 */
	public function proc_alert_services()
	{
		$terms = $this->facebookads_m
		->set_term_type()
        ->select("term.term_id")
        ->select("max(if(termmeta.meta_key = 'adaccount_status', termmeta.meta_value, null)) as adaccount_status")
        ->select("max(if(termmeta.meta_key = 'start_service_time', termmeta.meta_value, null)) as start_service_time")
        ->select("max(if(termmeta.meta_key = 'advertise_start_time', termmeta.meta_value, null)) as advertise_start_time")
        ->select("max(if(termmeta.meta_key = 'advertise_end_time', termmeta.meta_value, null)) as advertise_end_time")
        ->select("max(if(termmeta.meta_key = 'time_next_api_check', termmeta.meta_value, null)) as time_next_api_check")
        ->join('termmeta', 'termmeta.term_id = term.term_id', 'left')
        ->where('term_type', 'facebook-ads')
        ->where('term_status', 'publish')
        ->group_by('term.term_id')
        ->having('adaccount_status', 'APPROVED')
        ->having('start_service_time >', 0)
        ->having('advertise_start_time >', 0)
        ->having('time_next_api_check <', 'UNIX_TIMESTAMP()', false)
        ->get_all();
		
		if(empty($terms)) return FALSE;

		$now 		= time();
		$connection = new AMQPStreamConnection('172.104.32.147', 5672, 'guest', 'gameover123');
		$channel 	= $connection->channel();
		$queue 		= 'erp-facebookads-insight';

		$channel->queue_declare($queue, false, false, false, false);

		foreach ($terms as $term)
		{
			$payload 	= [ 'contractId' => $term->term_id ];
			$message	= new AMQPMessage(json_encode($payload));
			$channel->basic_publish($message, '', $queue);	
		}

		$channel->close();
		$connection->close();
	}

	/**
	 * CRON actions that get new ADWORD DATA and Sending mail alert report
	 *
	 * @param      array   $terms  The terms
	 *
	 * @return     bool  Result of function
	 */
	public function proc_alert_services_2($terms = array())
	{
		$terms = empty($terms) ? $this->facebookads_m->get_all_active() : $terms;
    	if(!$terms) return FALSE;

		$this->load->config('amqps');
    	$amqps_host 	= $this->config->item('host', 'amqps');
		$amqps_port 	= $this->config->item('port', 'amqps');
		$amqps_user 	= $this->config->item('user', 'amqps');
		$amqps_password = $this->config->item('password', 'amqps');
		$queue 			= $this->config->item('facebookads', 'amqps_queues');
		
		$connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
		$channel 	= $connection->channel();
		$channel->queue_declare($queue, false, true, false, false);

		foreach ($terms as $term)
		{
			$time_next_api_check = get_term_meta_value($term->term_id, 'time_next_api_check');
    		if(!empty($time_next_api_check) && $time_next_api_check > time()) continue;
    		$payload = [
				'event'			=> 'fbapi.sync',
				'contractId' 	=> $term->term_id,
				'created' 		=> my_date()
			];

			$message = new AMQPMessage(
				json_encode($payload),
				array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
			);
			$channel->basic_publish($message, '', $queue);		
		}

		$channel->close();
		$connection->close();
	}

	/**
	 * Sends a progress email.
	 *
	 * @param      <type>  $term         The term
	 * @param      string  $budget_type  The budget type
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	protected function send_progress_email($term = NULL, $budget_type = 'commit', $force_send = FALSE)
	{
		if( ! $term) return FALSE;

		$cache_key = "modules/facebookads/old-progress/term_{$term->term_id}"; // Cache progress theo hợp đồng
		if($budget_type == 'actual') $cache_key = "modules/facebookads/old-actual-progress/term_{$term->term_id}"; // Cache progress theo thực thu

		$past_progress 	= (float) $this->scache->get($cache_key);
		$progress 		= $past_progress;

		switch ($budget_type)
		{
			case 'actual': $progress = (float) get_term_meta_value($term->term_id, 'actual_progress_percent_net'); break;
			default: $progress = ((float) get_term_meta_value($term->term_id, 'actual_progress_percent'))*100; break;
		}

		/* Nếu tiến độ không thay đổi so với lần load dữ liệu trước */
		if( ! $force_send && $progress == $past_progress) return TRUE;

		$fbads_report_m = new fbads_report_m();
		$fbads_report_m = $fbads_report_m->init($term);
		$fbads_report_m->send_progress_email($budget_type);

		$this->scache->write($progress, $cache_key);
		return TRUE;
	}


	/**
	 * Request FBADSAPI INSIGHT DATA
	 *
	 * @return     bool  TRUE if successfully , otherwise return FALSE
	 */
	public function request_fbadsapi()
	{
		$this->load->model('facebookads/adaccount_m');

		if( ! parent::check_token())
  		{
  			log_message('error','#fbamod/cron/request_fbadsapi has been illegal access to');
  			return FALSE;
  		}

        $contracts = $this->facebookads_m->get_all_active();
		if(empty($contracts)) return FALSE;
		
		$headers 	= [ 'Authorization' => 'Bearer ' . $this->admin_m->generateKey(47) ];
		$data 		= [ 'start_time' => start_of_day(strtotime('-7 days')), 'end_time' => end_of_day() ];

		foreach ($contracts as $contract)
		{
			$next_time_get_insight = get_term_meta_value($contract->term_id,'next_time_get_insight');
			if(!empty($next_time_get_insight) && $next_time_get_insight < time())
			{
				log_message('info',"request_fbadsapi/#term_id locked until next turn get insight");
				continue;
			}

			$adaccounts = get_term_meta($contract->term_id, 'adaccounts', FALSE, TRUE);
        	if(empty($adaccounts)) continue;

			try
			{
				$response = Requests::put(base_url("api-v2/facebookads/data/sync/{$contract->term_id}"), $headers, $data);

				if(200 != $response->status_code) throw new Exception($response->body);
				
				$response = json_decode($response->body);
				if(200 != $response->code) continue;
				update_term_meta($contract->term_id, 'next_time_get_insight', strtotime('+2 hours'));
			}
			catch(Exception $e)
			{
				log_message('info', $e->getMessage());
			}
		}

		return TRUE;
	}

	/**
	 * Writes a log cron.
	 */
	private function _write_log_cron()
	{
        $this->load->helper('file');
        write_file(FCPATH.'log/log_cron.txt', $this->input->ip_address() .' [' . date("D M j G:i:s Y") . ']'  . "\n" ,'a');
    }
}
/* End of file Crond.php */
/* Location: ./application/modules/facebookads/controllers/Crond.php */