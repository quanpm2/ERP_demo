<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends REST_Controller
{
	function __construct()
	{
		parent::__construct('facebookads/rest');
		$this->load->model('usermeta_m');
		$this->load->model('permission_m');
		$this->load->config('facebookads/fields');
	}

	public function config_get()
	{
		$default 	= array('name' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if( ! has_permission('facebookads.index.access')) parent::response(['msg'=>'Quyền hạn không hợp lệ.'], parent::HTTP_UNAUTHORIZED);
		
		$response = array(
			'msg' => 'Dữ liệu tải thành công',
			'data' => array(
				'columns' => $this->config->item('columns', 'datasource'),
				'default_columns' => $this->config->item('default_columns', 'datasource')
			)
		);

		parent::response($response);
	}
}
/* End of file Contract.php */
/* Location: ./application/modules/wservice/controllers/api/Contract.php */