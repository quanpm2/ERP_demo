<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Website_m extends Term_m {

    public $term_type = 'website';
    public $validate = array(
        array('field' => 'edit[term_name]','label' => 'Website','rules' => 'required|valid_url')
    );


    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Sets the term type.
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function set_term_type()
    {
        $this->where('term_type',$this->term_type);
        return $this;
    }

    /**
     * Domain check
     *
     * @param      string  $url    The url
     *
     * @return     string  ( description_of_the_return_value )
     */
    public function domain_check($url = '', $ignoreId = null)
    {
        $url = preg_replace('/\s+/', '',trim(mb_strtolower($url)));

        $url = prep_url($url);
        
        if( ! $url) return FALSE;

        if( ! checkdomain($url)) return FALSE;

        $domain = parse_url(prep_url($url),PHP_URL_HOST);

        if(strpos($domain, 'www.') === 0)
        {
            $domain = str_replace('www.', '', strtolower($domain));
        }

        $domain_list = [
            'tiktok.com',
            'facebook.com',
            'lazada.vn',
            'youtube.com',
            'tiki.vn',
            'sendo.vn',
            'shopee.vn',
            '1web.vn',
            'instagram.com',
            'zalo.me',
            'linkedin.com'
        ];
        if( in_array($domain, $domain_list))
        {
            $path   = parse_url($url,PHP_URL_PATH);
            $domain.= !empty($path) ? rtrim($path,'/') : '';
        }
        else if(strpos($url, 'github.io'))
        {
            $path   = parse_url($url,PHP_URL_PATH);
            $domain.= !empty($path) ? rtrim($path,'/') : '';
        }
        else if( $domain == 'play.google.com')
        {
            $path   = parse_url($url,PHP_URL_PATH);
            $domain.= !empty($path) ? rtrim($path,'/') : '';

            $query_string = parse_url($url, PHP_URL_QUERY);
            if(empty($query_string)) return false;

            $query_params = [];
            parse_str($query_string, $query_params);

            if(empty($query_params['id'])) return false;

            $domain.= '?id=' . $query_params['id'];
        }

        $ignoreId AND $this->where('term.term_id !=', $ignoreId);
        $exists = $this->select('term_id')->set_term_type()->get_by(['term_name'=>$domain]);
        if($exists) return FALSE;
        
        return $domain;
    }
}
/* End of file Customer_m.php */
/* Location: ./application/modules/customer/models/Customer_m.php */