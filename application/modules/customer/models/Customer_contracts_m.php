<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Contract_m.php');

class Customer_contracts_m extends Base_model {

	/**
	 * Constructs a new instance.
	 */
	public function __construct()
    {
        parent::__construct();

        $this->load->model('contract/contract_m');
        $this->load->model('customer/customer_m');
    }

	/**
	 * Gets the first contracts.
	 *
	 * @param      array  $args   The arguments
	 *
	 * @return     bool   The first contracts.
	 */
	public function get_first_contracts($args = [])
	{
		$this->load->model('contract/contract_m');


        $taxonomies = $this->config->item('taxonomy');
        $taxonomies AND array_keys($taxonomies);

        $defaultArgs = [
            'user_id' => null,
            'taxonomies' => $taxonomies
        ];

        $mappingTypes   = [ 'google-ads' => 'googleads', 'facebook-ads' => 'facebookads' ];

        $customers = $this
        ->select('user.user_id, user_type, display_name, user_email, user_status')
        ->set_user_type()
        ->get_all();

        if(empty($customers)) return false;

        $this->load->model('contract/contract_m');

        $contracts = $this->contract_m
        ->select('term.term_id, term_type, term_status, term_users.user_id')
        ->join('term_users', 'term_users.term_id = term.term_id')
        ->where_in('term_type', array_keys($mappingTypes))
        ->where_in('term_users.user_id', array_column($customers, 'user_id'))
        ->get_all();

        $contractsGroupByUserId = array_group_by($contracts, 'user_id');

        $data = array();

        foreach ($customers as $key => $customer)
        {
            if(empty($contractsGroupByUserId[$customer->user_id])) continue;

            $_contracts = array_map(function($x){
                $x->start_service_time = (int) get_term_meta_value($x->term_id, 'start_service_time');
                return $x;
            }, $contractsGroupByUserId[$customer->user_id]);

            $_contracts = array_filter($_contracts, function($x){
                if( ! in_array($x->term_status, ['pending', 'publish', 'ending', 'liquidation'])) return false;
                return ! empty($x->start_service_time);
            });

            if(empty($_contracts)) continue;


            $_contracts = array_group_by($contractsGroupByUserId[$customer->user_id], 'term_type');

            foreach ($_contracts as $_type => $items)
            {
                usort($items, function($a, $b){
                    if(empty($a->start_service_time)) return 0;
                    return $a->start_service_time < $b->start_service_time ? -1 : 1;
                });

                $customer->{$mappingTypes[$_type]} = reset($items);
            }

            $data[] = $customer;
        }

        $this->scache->write($data, $cacheKey, 900);
	}
}
/* End of file Customer_contracts_m.php */
/* Location: ./application/modules/customer/models/Customer_contracts_m.php */