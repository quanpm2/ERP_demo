<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
require_once(APPPATH. 'models/User_m.php');

class Customer_m extends User_m {

    public $customer_types  = array('customer_person','customer_company');
    public $before_create   = array('create_timestamp');

    public $validate = array(
        array(
            'field' => 'edit[user_type]',
            'label' => 'Loại khách hàng',
            'rules' => 'required|in_list[customer_person,customer_company]'
        ),
        array(
            'field' => 'edit[user_email]',
            'label' => 'email',
            'rules' => 'valid_email|is_unique[user.user_email]'
        ),
        array(
            'field' => 'meta[customer_phone]',
            'label' => 'Số điện thoại',
            'rules' => 'required|is_natural|min_length[5]|max_length[20]'
        ),

        array(
            'field' => 'meta[customer_address]',
            'label' => 'Địa chỉ',
            'rules' => 'required'
        ),

        array(
            'field' => 'meta[customer_email]',
            'label' => 'E-mail',
            'rules' => 'required|valid_email'
        ),

        array(
            'field' => 'meta[customer_tax]',
            'label' => 'Mã số thuế',
            'rules' => ''
        )
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('usermeta_m');
    }
    
    /**
     * set_customer
     *
     * @param  array|object|int $customer
     * @return self
     */
    public function set_customer($customer)
    {
        if(is_object($customer))
        {
            $customer = (array)$customer;
        }

        if(is_array($customer))
        {
            foreach($customer as $key => $value)
            {
                $this->$key = $value;
            }

            return $this;
        }

        $this->id = $this->user_id = $customer;

        return $this;
    }

    /**
     * Check unique customer tax metadata
     *
     * @param      String  $tax    The tax
     *
     * @return     boolean  TRUE if tax is unique , othewise return FALSE
     */
    public function tax_uniquecheck($value, $user_type = 'customer_company', $user_id = 0)
    {
        return ! $this->usermeta_m->select('umeta_id')
        ->join('user', "user.user_id = usermeta.user_id AND user.user_id != {$user_id}")
        ->limit(1)
        ->count_by([ 'user.user_type' => $user_type, 'meta_key' => 'customer_tax', 'meta_value' => trim($value) ]);
    }

    /**
     * Check unique customer email metadata by user type
     *
     * @param      <type>  $value      The value
     * @param      string  $user_type  The user type
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function email_uniquecheck($value, $user_type = 'customer_company', $user_id = 0)
    {
        $query = $this->usermeta_m->select('umeta_id');
        if($user_id) $query->join('user', "user.user_id = usermeta.user_id AND user.user_id != {$user_id}");
        else $query->join('user','user.user_id = usermeta.user_id');
        
        $query = $query->limit(1)
        ->count_by([ 'user.user_type' => $user_type, 'meta_key' => 'customer_email', 'meta_value' => trim($value) ]);
        return  !$query;
    }


    /**
     * Check unique customer address metadata by user type
     *
     * @param      <type>  $value      The value
     * @param      string  $user_type  The user type
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function address_uniquecheck($value,$user_type = 'customer_company')
    {
        return  ! $this->usermeta_m->select('umeta_id')
        ->join('user','user.user_id = usermeta.user_id')
        ->limit(1)
        ->count_by([ 'user.user_type' => $user_type, 'meta_key' => 'customer_address', 'meta_value' => trim($value) ]);
    }


    /**
     * Check unique customer phone metadata by user type
     *
     * @param      <type>  $value      The value
     * @param      string  $user_type  The user type
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function phone_uniquecheck($value, $user_type = 'customer_company', $user_id = 0)
    {
        $value = preg_replace('/\s+/', '', $value);

        return  ! $this->usermeta_m->select('umeta_id')
        ->join('user', "user.user_id = usermeta.user_id AND user.user_id != {$user_id}")
        ->limit(1)
        ->count_by([ 'user.user_type' => $user_type, 'meta_key' => 'customer_phone', 'meta_value' => trim($value) ]);
    }


    public function create_timestamp($row)
    {
        $row['user_time_create']    = time();
        $row['user_status']         = 1;
        return $row;
    }

    public function set_get_customer($user_type = '')
    {
        if(empty($user_type)) return $this->where_in('user.user_type', $this->customer_types);
        return $this->where('user.user_type', $user_type);
    }

    public function set_user_type()
    {
        return $this->where_in('user.user_type',$this->customer_types);
    }

    public function get_customers($term_id = 0, $taxonomy = '', $option = array())
    {
        $customer_type = array('customer_person','customer_company','system');
        return $this->term_users_m->get_term_users($term_id, $customer_type, $option);
    }
    
    /**
     * Gets the first contracts.
     *
     * @param      array   $args   The arguments
     *
     * @return     <type>  The first contracts.
     */
    public function get_first_contracts($args = [])
    {
        $this->load->model('customer/customer_contract_m');
        return $this->customer_contract_m->get_first_contracts($args);
    }


    /**
     * Insert a new row into the table. $data should be an associative array
     * of data to be inserted. Returns newly created ID.
     */
    public function insert($data, $skip_validation = FALSE)
    {
        $result = parent::insert($data, $skip_validation);
        if(FALSE == $result) return $result;

        $audits = array();
        array_walk($data, function($value, $key) use (&$audits, $result){
            $audits[] = [ 'type' => 'user', 'id' => $result, 'field' => $key, 'new' => $value ];
        });

        audit($audits);
        return $result;
    }

    /**
     * Updated a record based on the primary value.
     */
    public function update($primary_value, $data, $skip_validation = FALSE)
    {
        $previous   = $this->as_array()->get($primary_value);
        $result     = parent::update($primary_value, $data, $skip_validation);

        if(empty($result)) return $result;

        $changed_values = array_filter($data, function($v, $k) use($previous){
            return $previous[$k] != $v;
        }, ARRAY_FILTER_USE_BOTH);

        if(empty($changed_values)) return $result;

        $records = array();
        array_walk($changed_values, function($value, $key) use (&$records, $primary_value){
            $records[] = ['event' => 'updated', 'type' => $this->_table, 'id' => $primary_value, 'field' => $key, 'new' => $value ];
        });

        audit($records);

        return $result;
    }
    
    /**
     * lock
     * Lock update customer
     *
     * @return bool
     */
    public function lock()
    {
        $is_lock = (bool)get_user_meta_value($this->user_id, 'lock');
        if($is_lock)
        {
            return TRUE;
        }
        
        update_user_meta($this->user_id, 'lock', TRUE);
    }
}
/* End of file Customer_m.php */
/* Location: ./application/modules/customer/models/Customer_m.php */