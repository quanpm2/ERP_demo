<?php
Class Customer_Package extends Package{

	public function name()
	{
		return 'Customer';
	}

	public function init(){
		$this->_load_menu();
		// $this->_update_permissions();
	}

	protected function _load_menu()
	{
		if(has_permission('Admin.Customer') && is_module_active('customer')){
			
			$this->menu->add_item(array(
				'id' => 'customer',
				'name' => 'Khách hàng',	
				'parent' => null,
				'slug' => admin_url('customer'),
				'icon' => 'fa fa-user-secret',
				'order' => 1,
				), 'left');
		}
	}

	public function title()
	{
		return 'Khách hàng';
	}

	public function author()
	{
		return 'Thonh';
	}

	public function version()
	{
		return '0.1';
	}

	public function description()
	{
		return 'Customer';
	}
	
	private function init_permissions()
	{
		$permissions = array();
		$permissions['Admin.Customer'] = array(
			'description' => 'Quản lý khách hàng',
			'actions' => array('view','add','edit','delete','update', 'manage', 'mdeparment', 'mgroup'));

        $permissions['customer.overwrite'] = array(
            'description' => 'Ghi đè thông tin khách hàng',
            'actions' => array('update', 'manage', 'mdeparment', 'mgroup'));

		return $permissions;
	}

    private function _update_permissions()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
	
}