<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends Admin_Controller {

	public $model = 'customer_m';

	public function __construct()
	{
		parent::__construct();
		restrict('Admin.Customer');

		$models = ['customer_m','term_users_m'];
		$this->load->model($models);
		$this->load->config('customer');

		$type = $this->input->get('type');
		$this->data['user_type'] = ($type && in_array($type,$this->customer_m->customer_types)) ? $type :'customer_person';
	}

	public function index()
	{
		restrict('Admin.Customer.View');
		$this->template->title->set('Danh sách khách hàng');
		$this->template->javascript->add(base_url("dist/vCustomersAdminIndexPage.js"));
        $this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->publish();
	}

	public function edit($id = 0)
	{		
		restrict('Admin.Customer.edit,Admin.Customer.add');

		$id = absint($id);

		if($this->customer_m->set_user_type()->where('user.user_id', $id)->count_by() <= 0)
		{
			$this->messages->error('Thông tin hiện tại không thể truy cập !');
			redirect(module_url(), 'refresh');
		}

        $edit_relate_users       = $this->admin_m->get_all_by_permissions('admin.customer.edit');
        $add_relate_users       = $this->admin_m->get_all_by_permissions('admin.customer.add');
        if($edit_relate_users === FALSE && $add_relate_users === FALSE )
        {
            $this->messages->error('Quyền truy cập không hợp lệ.');
			redirect(module_url(), 'refresh');
        }
        
        if(is_array($edit_relate_users) || is_array($add_relate_users)){
            $relate_users = [];
            if(is_array($edit_relate_users)) $relate_users = array_merge($relate_users, $edit_relate_users);
            if(is_array($add_relate_users)) $relate_users = array_merge($relate_users, $add_relate_users);
            $relate_users = array_unique($relate_users);
    
            $is_access = $this->customer_m
            ->set_get_customer()
            ->join('usermeta AS m_customer', 'm_customer.user_id = user.user_id AND m_customer.meta_key IN ("created_by", "assigned_to")')
            ->join('user AS sale', 'sale.user_id = m_customer.meta_value AND sale.user_type = "admin"')
            ->where('user.user_id', $id)
            ->where_in('sale.user_id', $relate_users)
            ->where_in('m_customer.meta_value', $relate_users)
            ->count_by() > 0;
            if(!$is_access){
                $this->messages->error('Quyền truy cập không hợp lệ.');
                redirect(module_url(), 'refresh');
            }
        }

        $customer = $this->customer_m->set_user_type()->where('user.user_id', $id)->get_by();

        $cid = get_user_meta_value($id, 'cid');
        if(empty($cid)){
            
            $cid = cid($id, $customer->user_type);
            update_user_meta($id, 'cid', $cid);
        }

		$this->template->footer->prepend('<script type="text/javascript">var userId = '.$id.'</script>');
		$this->template->title->set("#{$cid} | {$customer->display_name}");
		$this->template->javascript->add(base_url("dist/vCustomersAdminUpdatePage.js"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->publish();
	}
}
/* End of file Customer.php */
/* Location: ./application/modules/customer/controllers/Customer.php */