<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends Admin_Controller {

	public $model = 'customer_m';

	protected $validate = array();

	public function __construct()
	{
		parent::__construct();

		$models = ['customer/customer_m','term_users_m'];

		$this->load->model($models);
		$this->load->config('customer');
	}

	public function create()
	{
		$response = array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		if( ! has_permission('admin.customer.add'))
		{
			$response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
			return parent::renderJson($response,500);
		}

		$edit = $this->input->post('edit');
		if(empty($edit)) 
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ';
			return parent::renderJson($response,500);
		}

		$form_data = $this->customer_m->validate($this->input->post());
		if( ! $form_data)
		{
			$errors = $this->form_validation->error_array();
			$response['msg'] = implode('<br/>', $errors);
			$response['data']['errors'] = $errors;

			return parent::renderJson($response,500);
		}

		$user_type 			= $form_data['edit']['user_type'];
		$is_tax_valid 		= $this->customer_m->tax_uniquecheck($form_data['meta']['customer_tax'] ?? '', $user_type);
		$is_email_valid 	= $this->customer_m->email_uniquecheck($form_data['meta']['customer_email'] ?? '', $user_type);
		$is_address_valid 	= $this->customer_m->address_uniquecheck($form_data['meta']['customer_address'] ?? '', $user_type);
		$is_phone_valid 	= $this->customer_m->phone_uniquecheck($form_data['meta']['customer_phone'] ?? '', $user_type);

		if(!$is_tax_valid || !$is_email_valid || !$is_phone_valid)
		{
			$response['msg'] = 'Thông tin khách hàng đã tồn tại, liên hệ quản trị viên để xử lý !';
			$response['data']['errors'] = 'Thông tin khách hàng đã tồn tại, liên hệ quản trị viên để xử lý !';

			return parent::renderJson($response,500);
		}

		$form_data['edit']['user_email']	= $form_data['meta']['customer_email'];
		$form_data['edit']['user_status']	= 1;

		$form_data['meta'] 					= $form_data['meta'] ?? array();
		$form_data['meta']['created_by'] 	= $this->admin_m->id;
		$form_data['meta']['assigned_to'] 	= $this->admin_m->id;

		$this->customer_m->skip_validation();
		$insert_id = $this->customer_m->insert($form_data['edit']);

		if(!empty($form_data['meta']))
		{
			foreach ($form_data['meta'] as $key => $value)
			{	
				update_user_meta($insert_id, $key, $value);
				audit('updated', 'usermeta', $insert_id, $key, $value);
			}
		}

		$response['data']['insert_id']		= $insert_id;
		$response['data']['display_name']	= $form_data['edit']['display_name'] ?: '';
		$response['msg'] 		= 'Thêm mới dữ liệu thành công !';
		$response['success'] 	= TRUE;

		return parent::renderJson($response);
	}
}
/* End of file Customer.php */
/* Location: ./application/modules/customer/controllers/Customer.php */