<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends Admin_Controller {

	public $model = 'customer_m';

	protected $validate = array();

	public function __construct()
	{
		parent::__construct();

		$models = ['customer/customer_m','term_users_m','customer/website_m'];
		$this->load->model($models);

		$this->load->library('form_validation');
		$this->load->config('customer');
	}

	/**
	 * Create new website
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function create()
	{
		$response = array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		if( ! has_permission('admin.customer.add'))
		{
			$response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
			return parent::renderJson($response,500);
		}

		$edit = $this->input->post('edit');
		if(empty($edit)) 
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ';
			return parent::renderJson($response,500);
		}

		$form_data = $this->website_m->validate($this->input->post());
		if( ! $form_data)
		{
			$errors = $this->form_validation->error_array();
			$response['msg'] = implode('<br/>', $errors);
			$response['data']['errors'] = $errors;

			return parent::renderJson($response,500);
		}

		$valid_website = $this->website_m->domain_check($form_data['edit']['term_name']);
		if( ! $valid_website)
		{
			$response['msg'] = 'Website đã tồn tại hoặc không khả dụng !';
			return parent::renderJson($response,500);
		}

		$customer_id = $form_data['customer_id'];
		if(empty($customer_id))
		{
			$response['msg'] = 'Dữ liệu không hợp lệ [CID404] !';
			return parent::renderJson($response,500);
		}


		$form_data['edit']['term_name']		= $valid_website;
		$form_data['edit']['term_type']		= $this->website_m->term_type;
		$form_data['edit']['term_status']	= 'publish';
		
		$this->website_m->skip_validation();
		$insert_id = $this->website_m->insert($form_data['edit']);

		// Mapp website với khách hàng
		$this->term_users_m->set_term_users( $insert_id, array($customer_id));


		$form_data['meta'] 						= $form_data['meta'] ?? array();
		$form_data['meta']['create_time']		= time();
		$form_data['meta']['create_user_id'] 	= $this->admin_m->id;

		if(!empty($form_data['meta']))
		{
			foreach ($form_data['meta'] as $key => $value)
			{
				update_user_meta($insert_id,$key,$value);
			}
		}

		$response['data']['insert_id']	= $insert_id;
		$response['data']['website']	= $valid_website;
		$response['msg'] 				= 'Thêm mới dữ liệu thành công !';
		$response['success'] 			= TRUE;

		return parent::renderJson($response);
	}

	public function domain_check()
	{
		$website = $this->input->post('domain');
		if(empty($website) || !$this->form_validation->valid_url($website))
		{
			die('false');
		}

		$valid_website = $this->website_m->domain_check($website);
		if( ! $valid_website)
		{
			die('false');
		}

		die("true");
	}
}
/* End of file Website.php */
/* Location: ./application/modules/Customer/controllers/Website.php */