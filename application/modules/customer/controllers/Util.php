<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Util extends Admin_Controller 
{
	function __construct()
	{
		parent::__construct();
		if($this->admin_m->id != 47) redirect(admin_url(),'refresh');
		$this->load->model(['customer/customer_m']);	
	}

	function dumpAllCustomers()
	{
		$this->load->model('term_users_m');
		$users = $this->customer_m
			->join('term_users','term_users.user_id = user.user_id','LEFT')
			->join('term','term_users.term_id = term.term_id','LEFT')
			->set_get_customer()
			->where('term_type !=','website')
			->not_like('term_status', 'draft', 'after')
			->order_by('display_name')
			->group_by('user.user_id,term.term_type')
			->get_many_by();

		$userGroups = array_group_by($users, 'user_id');
		if(empty($userGroups)) prd('empty data');
		$i=1;
		foreach ($userGroups as $user_id => $group) 
		{
			$baseUser = reset($group);
			
			$display_name = mb_ucwords($baseUser->display_name.get_user_meta_value($baseUser->user_id,'customer_name'));
			$phone = get_user_meta_value($baseUser->user_id,'customer_phone');
			$email = get_user_meta_value($baseUser->user_id,'customer_email');
			$services = implode(',', array_map(function($x){
				$result = '';
				switch ($x->term_type) {
					case 'google-ads':
						$result = 'Adsplus';
						break;

					case 'webgeneral':
						$result = 'Web tổng hợp';
						break;

					case 'webdesign':
						$result = 'Thiết kế web';
						break;

					case 'seo-top':
						$result = 'SEO-TOP';
						break;

					case 'webdoctor':
						$result = 'Chăm sóc web';
						break;

					case 'seo-traffic':
						$result = 'SEO traffic';
						break;
					
					default:
						$result = $x->term_type;
						break;
				}
				return $result;
				
			}, $group)) ;

			$this->table->add_row($i++,$display_name,$email,$services,$phone);
		}

		prd($this->table->generate());
	}

	function stopCustomers()
	{
		$this->load->model('contract_m');

		$terms = $this->contract_m->set_term_type()->where_in('term_status', ['pending','publish','ending','liquidation','remove'])->get_all();
		$terms = array_column($terms, NULL, 'term_id');

		$this->load->model('term_users_m');

		$terms_users 	= $this->term_users_m->where_in('term_id', array_column($terms, 'term_id'))->get_all();
		$term_user 		= array_column($terms_users, 'term_id','user_id');

		$user_terms 	= array_group_by($terms_users,'user_id');

		$result = array();
		foreach ($user_terms as $user_id => $_terms)
		{
			if(empty($_terms)) continue;

			$_terms = array_column($_terms, NULL,'term_id');
			foreach ($_terms as $key => $_term)
			{
				$contract_begin = (int) get_term_meta_value($_term->term_id, 'contract_begin');
				if(empty($terms[$_term->term_id]) || empty($contract_begin) || $contract_begin < $this->mdate->startOfYear('2017/01/01'))
				{
					unset($_terms[$key],$terms[$_term->term_id]);
					continue;
				}

				$_term->contract_end = (int)get_term_meta_value($_term->term_id, 'contract_end');
			}

			usort($_terms, function($a,$b){
				return ($a->contract_end < $b->contract_end ? 1 : -1); 
			});

			$last_term = reset($_terms);

			if(empty($last_term->contract_end)) continue;

			if($last_term->contract_end > $this->mdate->startOfDay(strtotime('-2 months'))) continue;

			$result[] = [
				'Khách hàng' => $this->admin_m->get_field_by_id($user_id, 'display_name'),
				'số điện thoại' => get_user_meta_value($user_id, 'customer_phone'),
				'địa chỉ' => get_user_meta_value($user_id, 'customer_address'),
				'email' => get_user_meta_value($user_id, 'customer_email'),
				'hợp đồng gần nhất' => get_term_meta_value($last_term->term_id, 'contract_code'),
				'Ngày kết thúc HĐ' => my_date($last_term->contract_end,'Y/m/d'),
				'trạng thái' => $this->config->item($terms[$last_term->term_id]->term_status, 'contract_status'),
				'kinh doanh phụ trách' => $this->admin_m->get_field_by_id(get_term_meta_value($last_term->term_id, 'staff_business'),'display_name'),
			];
		}

		$this->load->library('table');

		$heading = ['Khách hàng','số điện thoại','địa chỉ','email','hợp đồng gần nhất','Trạng thái','kinh doanh phụ trách'];

		$this->table->set_heading($heading);

		$datatable = $this->table->generate($result);
		echo $datatable;
	}
}