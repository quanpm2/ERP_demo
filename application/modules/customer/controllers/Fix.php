<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Fix extends Admin_Controller
{	
	public function __construct()
	{
		parent::__construct();

		if($this->admin_m->role_id != 1) die('401');

		$model = array('customer/customer_m', 'user_m', 'term_users_m');
		$this->load->model($model) ;
	}

	public function update_customer_type($userId, $user_type, $overwrite_cid = true)
	{
		$this->customer_m->update($userId, [ 'user_type' =>  $user_type], true);
		if( ! $overwrite_cid) die('done');
		
		$cid = cid($userId, $user_type);
		update_user_meta($userId, 'cid', $cid);
		die($cid);
	}

	public function clone($customerId)
	{
		$customer = $this->customer_m->set_user_type()->as_array()->get((int) $customerId);
		if( ! $customer) die(400);

		$newCustomer = $customer;
		unset($newCustomer['user_id']);
		unset($newCustomer['user_time_create']);

		$had_cloned = $this->customer_m->count_by($newCustomer) > 1;
		if($had_cloned) die(' cloned completed !');
	
		// Create new customer with main properties
		$this->customer_m->skip_validation();
		$_customer_id_inserted = $this->customer_m->insert($newCustomer);
		$metadata = get_user_meta_value($customer['user_id']);
		if(empty($metadata)) die('cloned completed [1]');

		$_cid = $metadata['cid']['meta_value'];
		if(empty($_cid))
		{
			$_cid = cid($_customer_id_inserted, $customer['user_type']);
		}
		else if(FALSE === strpos($_cid, '.')) $_cid.='.1';
		else
		{
			$_prefix = mb_substr($_cid, 0, strpos($_cid, '.'));
			$_suffix = (int) mb_substr($_cid, strpos($_cid, '.') + 1, strlen($_cid));
			$_cid = $_prefix . '.' . ($_suffix+1);
		}

		$metadata['cid']['meta_value'] = $_cid;

		foreach($metadata as $item)
		{
			update_user_meta($_customer_id_inserted, $item['meta_key'], $item['meta_value']);
		}

		$this->load->model('customer/website_m');
		$websiteIds = $this->term_users_m->get_user_terms($customer['user_id'], $this->website_m->term_type);
		$websiteIds AND $websiteIds = array_keys($websiteIds);
		$websiteIds AND $this->term_users_m->set_user_terms( $_customer_id_inserted, $websiteIds);

		dd($_customer_id_inserted);
	}

	public function migrate_customer($originalId, $targetId)
	{
		$args = [
			'originalId' => $originalId,
			'targetId' => $targetId,
		];

		$customerOriginal = $this->customer_m->get($args['originalId']);
		$customerTarget = $this->customer_m->get($args['targetId']);

		$termsOriginalRelated = $this->term_users_m
		->join('term', 'term.term_id = term_users.term_id')
		->where('user_id', $customerOriginal->user_id)->get_all();

		$termsTargetRelated = $this->term_users_m
		->join('term', 'term.term_id = term_users.term_id')
		->where('user_id', $customerTarget->user_id)->get_all();

		var_dump($termsOriginalRelated, $termsTargetRelated);
		foreach($termsOriginalRelated as $_contract)
		{
			$this->term_users_m->set_relations_by_term($_contract->term_id, [ $customerTarget->user_id ], $customerTarget->user_type, [], FALSE);
		}
	}

	public function index()
	{
		// DỮ LIỆU TEST EXPORT
		$last_query = $this->scache->get("modules/customer/index-last-query-{$this->admin_m->id}");
		if( ! $last_query)
		{
			$this->messages->info('Không tìm thấy lệnh đã truy vấn trước đó , vui lòng thử lại sau');
			redirect(current_url(),'refresh');	
		}

		$users = $this->user_m->query($last_query)->result();
		if( ! $users)
		{
			$this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
			redirect(current_url(),'refresh');
		}

		// EXPORT
		$this->export_list_customer_csv($users) ;

		$this->scache->delete("odules/customer/index-last-query-{$this->admin_m->id}");
	}

	public function export_list_customer_csv($customers = array())
	{
		$this->load->model('customer/usermeta_m');
		$this->load->library('excel');


		$customers = $this->customer_m->select('*')->where('user_type', 'customer_person')->or_where('user_type', 'customer_company')->get_many_by();
		if(empty($customers)) return FALSE ;

		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	    $cacheSettings = array( 'memoryCacheSize' => '512MB');
	    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	    $objPHPExcel = new PHPExcel();
	    $objPHPExcel->getProperties()->setCreator("WEBDOCTOR.VN")->setLastModifiedBy("WEBDOCTOR.VN")->setTitle(uniqid('Thông tin khách hàng'));
	    $objPHPExcel->setActiveSheetIndex(0);

	    $objWorksheet = $objPHPExcel->getActiveSheet();

	    $headings = array('Tên', 
	    				  'ID', 
	    				  'Trang web' ,
	    				  'Email Address',
	    				  'Non Primary E-mails', 
	    				  'Điện thoại VP',
	    				  'Điện thoại',
	    				  'Fax', 
	    				  'Tên đường',
	    				  'Thành phố',
	    				  'Tiểu bang',
	    				  'Mã bưu chính',
	    				  'Quốc gia',
	    				  'Địa chỉ giao hàng',
	    				  'Thành phố',
	    				  'Tiểu bang',
	    				  'Mã bưu chính',
	    				  'Quốc gia',
	    				  'Chi tiết',
	    				  'Kiểu',
	    				  'Loại hình',
	    				  'Doanh thu',
	    				  'Nhân viên',
	    				  'SIC Code',
	    				  'Ticker Symbol',
	    				  'ID Khách hàng cấp trên',
	    				  'Quyền sở hữu',
	    				  'ID Chiến dịch',
	    				  'Rating',
	    				  'Tên người dùng được chỉ định',
	    				  'Giao cho',
	    				  'Ngày tạo',
	    				  'Ngày sửa',
	    				  'Modified',
	    				  'Created By',
	    				  'Deleted',
						  'Địa chỉ',
						  'Geocode Status',
						  'Latitude',
						  'Longitude',
	    				  );
	    $objWorksheet->fromArray($headings, NULL, 'A1');

	    $row_index = 2;

		foreach($customers as $key => $customer)
		{
			$id      = $customer->user_id;	
			$name 	 = $customer->display_name ;
			$type    = $this->config->item($customer->user_type,'customer_type') ;

			$email 	 = $this->usermeta_m->get_meta_value($id,'customer_email') ;	
			$address = $this->usermeta_m->get_meta_value($id,'customer_address') ;
			$created = date('d/m/Y', $customer->user_time_create);
			$websites = $this->term_users_m->where(array('term_users.user_id' => $id ,'term.term_type'=>'website'))
										   ->join('term','term.term_id = term_users.term_id')
										   ->get_many_by();

			if(!empty($websites)) 
			{
				$webname = '';
				foreach($websites as $web) 
				{
					$webname .= ", $web->term_name";
				}
				$webname = substr($webname, 2);
			}	

			$row_number = $row_index + $key;

			// Tên
		    $objWorksheet->setCellValueByColumnAndRow(0, $row_number, $name);

		    // ID
		    $objWorksheet->setCellValueByColumnAndRow(1, $row_number, $id);

		    // Trang web
		    $objWorksheet->setCellValueByColumnAndRow(2, $row_number, $webname);

		    // Email Address
		    $objWorksheet->setCellValueByColumnAndRow(3, $row_number, $email);

		    // 'Non Primary E-mails'
		    $objWorksheet->setCellValueByColumnAndRow(4, $row_number, '');

		    // Điện thoại VP
		    $objWorksheet->setCellValueExplicit("F$row_number", '="' . get_user_meta_value($id,'customer_phone') . '"', PHPExcel_Cell_DataType::TYPE_STRING);

		    // Điện thoại
		    $objWorksheet->setCellValueByColumnAndRow(6, $row_number, '');

		    // Fax
		    $objWorksheet->setCellValueByColumnAndRow(7, $row_number, '');

		    // Tên đường
		    $objWorksheet->setCellValueByColumnAndRow(8, $row_number, '');

		    // Thành phố
		    $objWorksheet->setCellValueByColumnAndRow(9, $row_number, '');

		    // Tiểu bang
		    $objWorksheet->setCellValueByColumnAndRow(10, $row_number, '');

		    // Mã bưu chính
		    $objWorksheet->setCellValueByColumnAndRow(11, $row_number, '');

		    // Quốc gia
		    $objWorksheet->setCellValueByColumnAndRow(12, $row_number, '');

		    // Địa chỉ giao hàng
		    $objWorksheet->setCellValueByColumnAndRow(13, $row_number, '');

	    	// Thành phố
	    	$objWorksheet->setCellValueByColumnAndRow(14, $row_number, '');
	    	
	    	// Tiểu bang
	    	$objWorksheet->setCellValueByColumnAndRow(15, $row_number, '');

	    	// Mã bưu chính 
	    	$objWorksheet->setCellValueByColumnAndRow(16, $row_number, '');

			// Quốc gia
			$objWorksheet->setCellValueByColumnAndRow(17, $row_number, '');

	    	// Chi tiết'
	    	$objWorksheet->setCellValueByColumnAndRow(18, $row_number, '');

	    	// Kiểu
	    	$objWorksheet->setCellValueByColumnAndRow(19, $row_number, $type);

	    	// Loại hình
	    	$objWorksheet->setCellValueByColumnAndRow(20, $row_number, '');

	    	// Doanh thu
	    	$objWorksheet->setCellValueByColumnAndRow(21, $row_number, '');

	    	// Nhân viên
	    	$objWorksheet->setCellValueByColumnAndRow(22, $row_number, '');

	    	// SIC Code
	    	$objWorksheet->setCellValueByColumnAndRow(23, $row_number, '');

	    	// Ticker Symbol
	    	$objWorksheet->setCellValueByColumnAndRow(24, $row_number, '');

	    	// ID Khách hàng cấp trên
	    	$objWorksheet->setCellValueByColumnAndRow(25, $row_number, '');

	    	// Quyền sở hữu
	    	$objWorksheet->setCellValueByColumnAndRow(26, $row_number, '');
	    	
	    	// ID Chiến dịch
	    	$objWorksheet->setCellValueByColumnAndRow(27, $row_number, '');
	    	
	    	// Rating
	    	$objWorksheet->setCellValueByColumnAndRow(28, $row_number, '');

	    	// Tên người dùng được chỉ định'
	    	$objWorksheet->setCellValueByColumnAndRow(29, $row_number, '');

	    	// Giao cho
	    	$objWorksheet->setCellValueByColumnAndRow(30, $row_number, '');

	    	// Ngày tạo
	    	$objWorksheet->setCellValueByColumnAndRow(31, $row_number, $created);

	    	// Ngày sửa
	    	$objWorksheet->setCellValueByColumnAndRow(32, $row_number, '');

	    	// Modified
	    	$objWorksheet->setCellValueByColumnAndRow(33, $row_number, '');

	    	// Created By
	    	$objWorksheet->setCellValueByColumnAndRow(34, $row_number, '');

	    	// Deleted
	    	$objWorksheet->setCellValueByColumnAndRow(35, $row_number, '');

	    	// Địa chỉ
	    	$objWorksheet->setCellValueByColumnAndRow(36, $row_number, $address);

	    	// Geocode Status
	    	$objWorksheet->setCellValueByColumnAndRow(37, $row_number, '');
			
			// Latitude
			$objWorksheet->setCellValueByColumnAndRow(38, $row_number, 0);

			// Longitude
			$objWorksheet->setCellValueByColumnAndRow(39, $row_number, 0);		  
	
		}

		$num_rows = count($customers);
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
	    $objWriter->setUseBOM(TRUE);
	    
	    $folder_export  =  dirname(APPPATH) . '/export/';
	    if(!is_dir($folder_export))
	    {
	        try 
	        {
	            $oldmask = umask(0);
	            mkdir($folder_export, 0777, TRUE);
	            umask($oldmask);
	        }
	        catch (Exception $e)
	        {
	            trigger_error($e->getMessage());
	            return FALSE;
	        }
	    }

	    $file_name = $folder_export . 'customers-list-' . my_date(time(), 'd-m-Y'). '.csv';
	    try 
	    {
	        $objWriter->save($file_name);
	    }
	    catch (Exception $e) 
	    {
	        trigger_error($e->getMessage());
	        return FALSE; 
	    }

	    $this->load->helper('download');
	    force_download($file_name,NULL);
	}

    public function migrate_customer_admin(){
        $customers = $this->customer_m
        ->set_get_customer()
        ->join('usermeta', 'usermeta.user_id = user.user_id AND meta_key IN ("created_by", "assigned_to")', 'LEFT')
        ->select('user.user_id')
        ->select('MAX(IF(usermeta.meta_key = "created_by", usermeta.meta_value, NULL)) AS created_by')
        ->select('MAX(IF(usermeta.meta_key = "assigned_to", usermeta.meta_value, NULL)) AS assigned_to')
        ->group_by('user.user_id')
        ->having("(created_by = '' OR created_by IS NULL OR assigned_to = '' OR assigned_to IS NULL)", NULL)
        ->as_array()
        ->get_all();

        if(empty($customers)){
            echo 'Không tìm thấy khách hàng cần xử lý';
            return;
        }

        $this->load->config('contract/contract');

        $sumarry = [
            'total' => count($customers),
            'ignored' => 0,
            'success' => 0,
        ];
        foreach($customers as $customer){
            $customer_id = $customer['user_id'];
            $created_by = $customer['created_by'] ?: '';
            update_user_meta($customer_id, 'created_by', $created_by);
            
            $assigned_to = $customer['assigned_to'] ?: '';
            if(!empty($assigned_to)){
                $sumarry['ignored']++;
                continue;
            }

            $taxonomies = array_keys($this->config->item('taxonomy'));
            $terms = $this->term_users_m->get_user_terms($customer_id, $taxonomies);
            $term_ids = $terms ? array_keys($terms) : array();
            rsort($term_ids);
            
            $sale_id = '';
            foreach($term_ids as $term_id){
                if(!empty($sale_id)) break;

                $sale_id = get_term_meta_value($term_id, 'staff_business');
            }
            if(empty($sale_id)){
                $sumarry['ignored']++;
                continue;
            }

            update_user_meta($customer_id, 'assigned_to', $sale_id);
            $sumarry['success']++;
        }

        echo nl2br('Tổng cộng: ' . $sumarry['total'] . "\n");
        echo nl2br('Đã xử lý: ' . $sumarry['success'] . "\n");
        echo nl2br('Bỏ qua: ' . $sumarry['ignored'] . "\n");
        return;
    }
    
    /**
     * migrate_lock_customer
     *
     * @return void
     */
    public function migrate_lock_customer()
    {
        $this->load->model('customer/customer_m');
        $this->load->config('contract/contract');
        
        $contract_type = $this->config->item('taxonomy');
        $contract_type = array_keys($contract_type);

        $lock_customers = $this->customer_m->set_user_type()
        ->join('term_users', 'term_users.user_id = user.user_id')
        ->join('term', 'term.term_id = term_users.term_id')
        ->where_in('term.term_type', $contract_type)
        ->where_not_in('term.term_status', ['draft', 'unverified'])
        ->group_by('user.user_id')
        ->select('user.user_id')
        ->as_array()
        ->get_all();

        $stat = [
            'total' => count($lock_customers),
            'processed' => 0,
            'ignored' => 0,
            'data' => [
                'processed' => [],
                'ignored' => [],
            ]
        ];

        foreach($lock_customers as $lock_customer)
        {
            $is_lock = (bool)get_user_meta_value($lock_customer['user_id'], 'lock');
            if($is_lock)
            {
                $stat['ignored'] += 1;
                $stat['data']['ignored'][] = $lock_customer['user_id'];

                continue;
            }

            update_user_meta($lock_customer['user_id'], 'lock', TRUE);
            $stat['processed'] += 1;
            $stat['data']['processed'][] = $lock_customer['user_id'];
        }
        
        echo nl2br('Tổng cộng: ' . $stat['total'] . "\n");
        echo nl2br('Đã xử lý: ' . $stat['processed'] . "\n");
        echo nl2br('Đã bỏ qua: ' . $stat['ignored'] . "\n");
        echo nl2br('Danh sách khách hàng đã xử lý: ' . implode(', ', $stat['data']['processed']) . "\n");
        echo nl2br('Danh sách khách hàng bỏ qua: ' . implode(', ', $stat['data']['ignored']) . "\n");
        return;
    }
}