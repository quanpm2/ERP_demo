<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends MREST_Controller
{
	/**
	 * CONSTRUCTION
	 *
	 * @param      string  $config  The configuration
	 */
	function __construct($config = 'rest')
	{
		array_push($this->autoload['models'], 'customer/customer_m', 'term_users_m', 'customer/website_m', 'option_m');
		parent::__construct($config);
	}

    /**
     * Get config
     */
    public function index_get()
    {
        $configs = [];

        $configs['is_active_tin_check'] = (bool)$this->option_m->get_value('is_active_tin_check');
        $configs['has_force_update'] = has_permission('Admin.Customer.Update') && has_permission('Admin.Customer.Manage');

        parent::responseHandler($configs, 'ok');
    }
}
/* End of file Config.php */
/* Location: ./application/modules/customer/controllers/api_v2/Config.php */