<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataset extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'customer/customer_m';
        parent::__construct($config);

        $this->load->config('customer/customer');
    }


    /**
     * Render dataset of FB's services
     */
    public function index_get()
    {
        $this->load->config('contract/contract');

        $response   = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

        $permission = 'Admin.Customer.View';

        if( ! has_permission($permission))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        $default = array(
            'offset'        => 0,
            'per_page'      => 10,
            'cur_page'      => 1,
            'is_filtering'  => TRUE,
            'is_ordering'   => TRUE,
            'filter_position'   => FILTER_TOP_OUTTER
        );

        $args               = wp_parse_args( $this->input->get(), $default);

        $this->load->library('datatable_builder');

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        $relate_users       = $this->admin_m->get_all_by_permissions($permission);
        if($relate_users === FALSE)
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        $customer_terms = $this->customer_m
            ->select('user.user_id,term_users.term_id,term.term_name,term_type,term_status')
            ->join('term_users','term_users.user_id = user.user_id')
            ->join('term','term_users.term_id = term.term_id')
            ->set_get_customer()
            ->where('term_type !=','website')
            ->not_like('term_status', 'draft', 'after')
            ->order_by('display_name')
            ->group_by('user.user_id,term.term_type')
            ->get_many_by();

        $customer_terms = array_group_by($customer_terms, 'user_id');

        /* Applies get query params for filter */
        $this->search_filter();
        $this->template->title->append('Khách hàng');

        $data = $this->datatable_builder
        ->select('user.user_id')
        ->set_filter_position($args['filter_position'])
        ->add_search('customer_code')
        ->add_search('display_name')
        ->add_search('user_time_create',['class'=>'form-control input_daterange'])
        ->add_search('user_type',array('content'=> form_dropdown(array('name'=>'where[user_type]','class'=>'form-control select2'),prepare_dropdown($this->config->item('customer_type')),$this->input->get('where[user_type]'))))

        ->add_search('customer_phone')
        ->add_search('customer_email')
        ->add_search('name_created_by')
        ->add_search('name_assigned_to')
        ->add_search('term_type',array('content'=> form_dropdown(array('name'=>'where[term_type]','class'=>'form-control select2'),prepare_dropdown($this->config->item('services'),'Tất cả dịch vụ'),$this->input->get('where[term_type]'))))
        ->add_search('customer_tax',['placeholder'=>'Mã số thuế'])
        ->add_search('action',array('content'=> $this->admin_form->submit('search','Tìm kiếm').
                $this->admin_form->submit(['name'=>'Export','class'=>'btn btn-success'],'Export .xls')))
        ->add_column('display_name',array('set_select'=>FALSE, 'title' =>'Tên'))  
        ->add_column('customer_code', array('set_select'=>FALSE, 'title' =>'ID Khách hàng'))
        ->add_column('user_time_create',array('set_select'=>FALSE, 'title' =>'Ngày tạo'))
        ->add_column('user_type',array('set_select'=>FALSE, 'title' => 'Loại'))
        ->add_column('customer_phone', array('set_select'=>FALSE, 'title' =>'Điện thoại'))
        ->add_column('customer_email', array('set_select'=>FALSE, 'title' =>'E-mail'))
        ->add_column('term_type', array('set_select'=>FALSE, 'title' =>'Hợp đồng'))
        ->add_column('term_name', array('set_select'=>FALSE, 'title' =>'Website'))
        ->add_column('customer_tax', array('set_select'=>FALSE, 'title' =>'MST'))
        ->add_column('name_created_by', array('set_select'=>FALSE, 'title' =>'Người tạo'))
        ->add_column('name_assigned_to', array('set_select' => FALSE, 'title' =>'Người đang phụ trách'))
        ->add_column('active', array('set_select'=>FALSE, 'title' =>'Hiệu lực','set_order'=>FALSE))
        ->add_column('action', array('set_select'=>FALSE, 'title'=>'Actions','set_order'=>FALSE))

        ->add_column_callback('customer_code',function($data,$row_name) use ($customer_terms) {
            $user_id = $data['user_id'];
            $data['customer_code'] = cid($data['user_id'], $data['user_type']);

            $data['user_time_create'] = date("d/m/Y", $data['user_time_create']);

            $data['display_name'] = mb_convert_case($data['display_name'],  MB_CASE_UPPER, "UTF-8");
            $data['display_name'] = anchor(admin_url("customer/edit/{$user_id}"), '<b><i class="fa fa-edit"></i> &nbsp&nbsp'.mb_convert_case($data['display_name'].'</b>',  MB_CASE_UPPER, "UTF-8"), 'class="btn-xs"');

            $data['user_type'] = $this->config->item($data['user_type'],'customer_type');
            $data['customer_phone'] = get_user_meta_value($user_id,'customer_phone');
            $data['customer_email'] = get_user_meta_value($user_id,'customer_email');
            $data['customer_tax'] = get_user_meta_value($user_id,'customer_tax');

            $user_created_by = '--';
            $created_by = get_user_meta_value($user_id, 'created_by');
            if(!empty($created_by)) $user_created_by = $this->admin_m->get_field_by_id($created_by, 'display_name');
            $data['name_created_by'] = $user_created_by;

            $user_assigned_to = '--';
            $assigned_to = get_user_meta_value($user_id, 'assigned_to');
            if(!empty($assigned_to)) $user_assigned_to = $this->admin_m->get_field_by_id($assigned_to, 'display_name');
            $data['name_assigned_to'] = $user_assigned_to;
            
            // Dịch vụ khách hàng đang sử dụng
            $data['active'] = '';
            if(!empty($customer_terms[$user_id]))
            {
                $terms = $customer_terms[$user_id];
                $services = array();
                foreach ($terms as $term)
                {   
                    $label = 'label-default';
                    $text = $this->config->item($term->term_type,'services');
                    if(in_array($term->term_status, ['publish','pending']))
                    {
                        $data['active'] = 'Y';
                    }

                    $services[] = "<span class='label {$label}'>{$text}</span>&nbsp";
                }

                $data['term_type'] = implode(', ', $services);

                $data['term_name'] = implode(', <br/> ', array_unique(array_column($terms, 'term_name')));

            }

            return $data;

        },FALSE)

        ->from('user')
        ->select('
            user.user_id, 
            user.display_name, 
            user.user_type, 
            user.user_time_create
        ')
        ->where_in('user.user_type',  $this->customer_m->customer_types);

        if(is_array($relate_users))
        {
            $data->join('usermeta AS m_customer', '
                m_customer.user_id = user.user_id 
                AND m_customer.meta_key IN ("created_by", "assigned_to")
            ')
            ->where_in('m_customer.meta_value',  $relate_users);
        }

        $data = $data->group_by('user.user_id')
        ->generate(array('per_page'=>$args['per_page'],'cur_page'=>$args['cur_page']));

        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->export($last_query);
            return TRUE;
        }
        
        parent::response($data);
    }


    /**
     * Export Excel Dataset
     *
     * @param      string  $query  The query
     */
    protected function export($query = '')
    {
        if(empty($query)) return FALSE;

        // remove limit in query string
        $query = explode('LIMIT', $query);
        $query = reset($query);


        $users = $this->customer_m->query($query)->result();

        if( ! $users) return FALSE;

        $this->load->library('excel');
        $cacheMethod    = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings  = array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
        ->getProperties()
        ->setCreator("WEBDOCTOR.VN")
        ->setLastModifiedBy("WEBDOCTOR.VN")
        ->setTitle(uniqid('Danh sách Khách hàng __'));

        $objPHPExcel->setActiveSheetIndex(0);

        $objWorksheet   = $objPHPExcel->getActiveSheet();
        $headings = array(
            '#',
            'Mã số',
            'Khách hàng',
            'Ngày tạo',
            'Loại',
            'Điện thoại',
            'Địa chỉ',
            'E-mail',
            'Mã số thuế',
            'Hiệu lực',
            'Website'
        );
        $objWorksheet->fromArray($headings, NULL, 'A1');

        /**
         * LOAD CUSTOMER - CONTRACTS RELATIONS
         */
        $customer_terms = $this->customer_m
        ->select('user.user_id,term_users.term_id,term_type,term_status,term.term_name')
        ->join('term_users','term_users.user_id = user.user_id')
        ->join('term','term_users.term_id = term.term_id')
        ->set_get_customer()
        ->where_in('user.user_id', array_unique(array_column($users, 'user_id')))
        ->where('term_type !=','website')
        ->not_like('term_status', 'draft', 'after')
        ->order_by('display_name')
        ->group_by('user.user_id,term.term_type')
        ->get_many_by();

        $customer_terms = array_group_by($customer_terms, 'user_id');


        $row_index = 2;
        foreach ($users as $key => $user)
        {   
            $row_number = $row_index + $key;

            // Set Cell
            $objWorksheet->setCellValueByColumnAndRow(0, $row_number, ($key+1));
            $objWorksheet->setCellValueByColumnAndRow(1, $row_number, cid($user->user_id, $user->user_type));
            $objWorksheet->setCellValueByColumnAndRow(2, $row_number, $user->display_name);
            $objWorksheet->setCellValueByColumnAndRow(3, $row_number, PHPExcel_Shared_Date::PHPToExcel($user->user_time_create));
            $objWorksheet->setCellValueByColumnAndRow(4, $row_number, $this->config->item($user->user_type,'customer_type'));
            $objWorksheet->setCellValueExplicit("F{$row_number}", get_user_meta_value($user->user_id,'customer_phone'),PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorksheet->setCellValueByColumnAndRow(6, $row_number, get_user_meta_value($user->user_id,'customer_address'));
            $objWorksheet->setCellValueByColumnAndRow(7, $row_number, get_user_meta_value($user->user_id,'customer_email'));
            
            $objWorksheet->setCellValueExplicit("I{$row_number}", get_user_meta_value($user->user_id,'customer_tax'),PHPExcel_Cell_DataType::TYPE_STRING);


            $website = '';

            $active = 'N';
            if(!empty($customer_terms[$user->user_id]))
            {
                $terms = $customer_terms[$user->user_id];
                $services = array();
                foreach ($terms as $term)
                {
                    if( ! in_array($term->term_status, ['publish','pending']))  continue;

                    $active = 'Y';
                    break;
                }

                $website = implode(', ', array_unique(array_column($terms, 'term_name')));
            }

            $objWorksheet->setCellValueByColumnAndRow(9, $row_number, $active);
            $objWorksheet->setCellValueByColumnAndRow(10, $row_number, $website);

        }

        $num_rows = count($users);
        
        // Set Cells style for Date
        $objPHPExcel->getActiveSheet()
            ->getStyle('D'.$row_index.':D'.($num_rows+$row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
        
        // We'll be outputting an excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $folder_upload = "files/customers/";
        if(!is_dir($folder_upload))
        {
            try 
            {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $created_at = my_date(time(),'Y-m-d-H-i-s');
        $file_name = "{$folder_upload}customers-list-{$created_at}.xlsx";

        try {
            $objWriter->save($file_name);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($file_name, NULL);

        return TRUE;
    }

    /**
     * Search Filter Operation
     *
     * @param      array  $search_args  The search arguments
     */
    protected function search_filter($search_args = array())
    {   
        $args = $this->input->get();

        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

        // customer_code FILTERING & SORTING
        $filter_customer_code = $args['where']['customer_code'] ?? FALSE;
        $sort_customer_code   = $args['order_by']['customer_code'] ?? FALSE;

        if($filter_customer_code || $sort_customer_code)
        {
            $alias = uniqid('customer_code_');
            $this->datatable_builder->join("usermeta {$alias}","{$alias}.user_id = user.user_id and {$alias}.meta_key = 'cid'", 'LEFT');

            if($filter_customer_code)
            {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_customer_code);
            }

            if($sort_customer_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_customer_code);
            }
        }

        /* Dịch vụ đang sử dụng */
        $filter_term_type = $args['where']['term_type'] ?? FALSE;
        $sort_term_type = $args['order_by']['term_type'] ?? FALSE;
        if($filter_term_type || $sort_term_type)
        {
            $this->config->load('contract/contract');
            $this->datatable_builder
            ->join('term_users','term_users.user_id = user.user_id')
            ->join('term','term_users.term_id = term.term_id')
            ->group_by('user.user_id');

            if($filter_term_type)
            {
                $term_types = array_keys($this->config->item('services'));
                $this->datatable_builder->where('term.term_type', $filter_term_type);
            }

            if($sort_term_type)
            {
                $this->datatable_builder->order_by("term.term_type",$sort_term_type);
            }
        }

        // display_name FILTERING & SORTING
        $filter_display_name = $args['where']['display_name'] ?? FALSE;
        if($filter_display_name)
        {
            $this->datatable_builder->like('user.display_name',$filter_display_name);
        }
        
        $sort_display_name = $args['order_by']['display_name'] ?? FALSE;
        if($sort_display_name)
        {
            $this->datatable_builder->order_by("user.display_name",$sort_display_name);
        }

        // user_type FILTERING & SORTING
        $filter_user_type = $args['where']['user_type'] ?? FALSE;
        if($filter_user_type)
        {
            $this->datatable_builder->where('user.user_type',$filter_user_type);
        }
        
        $sort_user_type = $args['order_by']['user_type'] ?? FALSE;
        if($sort_user_type)
        {
            $this->datatable_builder->order_by("user.user_type",$sort_user_type);
        }

        // customer_email FILTERING & SORTING
        $filter_customer_email = $args['where']['customer_email'] ?? FALSE;
        $sort_customer_email = $args['order_by']['customer_email'] ?? FALSE;
        if($filter_customer_email || $sort_customer_email)
        {
            $alias = uniqid('customer_email_');
            $this->datatable_builder->join("usermeta {$alias}","{$alias}.user_id = user.user_id and {$alias}.meta_key = 'customer_email'", 'LEFT');

            if($filter_customer_email)
            {
                $this->datatable_builder->like("{$alias}.meta_value",$filter_customer_email);
            }

            if($sort_customer_email)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_customer_email);
            }
        }


        // customer_phone FILTERING & SORTING
        $filter_customer_phone = $args['where']['customer_phone'] ?? FALSE;
        $sort_customer_phone = $args['order_by']['customer_phone'] ?? FALSE;
        if($filter_customer_phone || $sort_customer_phone)
        {
            $alias = uniqid('customer_phone_');
            $this->datatable_builder->join("usermeta {$alias}","{$alias}.user_id = user.user_id and {$alias}.meta_key = 'customer_phone'", 'LEFT');
            if($filter_customer_phone)
            {
                $this->datatable_builder->like("{$alias}.meta_value",$filter_customer_phone);
            }

            if($sort_customer_phone)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_customer_phone);
            }
        }


        // customer_tax FILTERING & SORTING
        $filter_customer_tax = $args['where']['customer_tax'] ?? FALSE;
        $sort_customer_tax = $args['order_by']['customer_tax'] ?? FALSE;
        if($filter_customer_tax || $sort_customer_tax)
        {
            $alias = uniqid('customer_tax_');
            $this->datatable_builder->join("usermeta {$alias}","{$alias}.user_id = user.user_id and {$alias}.meta_key = 'customer_tax'", 'LEFT');

            if($filter_customer_tax)
            {
                $this->datatable_builder->like("{$alias}.meta_value",$filter_customer_tax);
            }

            if($sort_customer_tax)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_customer_tax);
            }
        }


        // user_time_create FILTERING & SORTING
        $filter_user_time_create = $args['where']['user_time_create'] ?? FALSE;
        $sort_user_time_create = $args['order_by']['user_time_create'] ?? FALSE;
        if($filter_user_time_create || $sort_user_time_create)
        {
            if($filter_user_time_create)
            {   
                $dates = explode(' - ', $args['where']['user_time_create']);
                $this->datatable_builder->where('user.user_time_create >=', $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where('user.user_time_create <=', $this->mdate->endOfDay(end($dates)));
            }

            if($sort_user_time_create)
            {
                $this->datatable_builder->order_by('user.user_time_create',$sort_user_time_create);
            }
        }

        // name_created_by FILTERING & SORTING
        $filter_name_created_by = $args['where']['name_created_by'] ?? FALSE;
        $sort_name_created_by = $args['order_by']['name_created_by'] ?? FALSE;
        if($filter_name_created_by || $sort_name_created_by)
        {
            $alias = uniqid('name_created_by_');
            $alias_sale = uniqid('alias_sale_');
            $this->datatable_builder
            ->join("usermeta {$alias}","{$alias}.user_id = user.user_id and {$alias}.meta_key = 'created_by'", 'LEFT')
                 ->join("user {$alias_sale}","{$alias_sale}.user_id = {$alias}.meta_value and {$alias_sale}.user_type = 'admin'", 'LEFT');

            if($filter_name_created_by)
            {   
                $this->datatable_builder->like("{$alias_sale}.display_name", $filter_name_created_by);
            }

            if($sort_name_created_by)
            {
                $this->datatable_builder->order_by("{$alias_sale}.display_name",$sort_name_created_by);
            }
        }

        $filter_name_assigned_to = $args['where']['name_assigned_to'] ?? FALSE;
        $sort_name_assigned_to = $args['order_by']['name_assigned_to'] ?? FALSE;
        if($filter_name_assigned_to || $sort_name_assigned_to)
        {
            $alias = uniqid('name_assigned_to_');
            $alias_sale = uniqid('alias_sale_');
            $this->datatable_builder
            ->join("usermeta {$alias}","{$alias}.user_id = user.user_id and {$alias}.meta_key = 'assigned_to'", 'LEFT')
            ->join("user {$alias_sale}","{$alias_sale}.user_id = {$alias}.meta_value and {$alias_sale}.user_type = 'admin'", 'LEFT');

            if($filter_name_assigned_to)
            {   
                $this->datatable_builder->like("{$alias_sale}.display_name", $filter_name_assigned_to);
            }

            if($sort_name_assigned_to)
            {
                $this->datatable_builder->order_by("{$alias_sale}.display_name",$sort_name_assigned_to);
            }
        }
    }
}
/* End of file Dataset.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/Dataset.php */