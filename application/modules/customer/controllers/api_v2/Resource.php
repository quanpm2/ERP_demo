<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Crawler\TaxCode\TaxCode;

class Resource extends MREST_Controller
{
	/**
	 * CONSTRUCTION
	 *
	 * @param      string  $config  The configuration
	 */
	function __construct($config = 'rest')
	{
		array_push($this->autoload['models'], 'customer/customer_m', 'term_users_m', 'customer/website_m', 'option_m');
		parent::__construct($config);

        $this->load->config('customer');
        $this->load->library('form_validation');
        $this->load->helper('text');
	}

    /**
     * Creates a Customer.
     */
    public function website_post()
    {
        $inputs = parent::post(null, TRUE);
        empty($inputs['term_name']) OR $inputs['term_name'] = convert_accented_characters($inputs['term_name']);

        $this->form_validation->set_data($inputs);

        $rules = array(
            [
                'field' => 'term_name',
                'label' => 'Webpage',
                'rules' => 'required|valid_url'
            ],
            [
                'field' => 'customerId',
                'label' => 'customerId',
                'rules' => 'required|integer'
            ]
        );

        $this->form_validation->set_rules($rules);

        if( FALSE == $this->form_validation->run())
        {
            parent::response([ 'code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array() ]);
        }

        $response = array( 'code' => parent::HTTP_BAD_REQUEST, 'msg' => 'Quá trình xử lý thất bại', 'data' => []);

        if( ! has_permission('admin.customer.add'))
        {
            $response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::renderJson($response, 500);
        }

        $relate_users = $this->admin_m->get_all_by_permissions('admin.customer.add');
        $customer = $this->customer_m->select('user.*')->set_user_type()->get($inputs['customerId']);
        if(empty($customer))
        {
            $response['msg'] = 'Thông tin khách hàng không hợp lệ.';
            parent::response($response);
        }
        
        if(is_bool($relate_users) && FALSE == $relate_users)
        {
            $response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::response($response);
        }

        if(is_array($relate_users) 
            && ! $this->usermeta_m
            ->where('user_id', $customer->user_id)
            ->where_in('usermeta.meta_key', ['assigned_to','created_by'])
            ->where_in('usermeta.meta_value', $relate_users)
            ->count_by()) {

            $response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::response($response);
        }

        $inputs['term_name'] = $valid_website = $this->website_m->domain_check($inputs['term_name']);
        if( ! $valid_website)
        {
            $response['msg'] = 'Website đã tồn tại hoặc không khả dụng !';
            parent::response($response);
        }


        $websites = $this->term_users_m->get_user_terms($customer->user_id, $this->website_m->term_type) ?: [];
        $websites AND $websites = array_map(function($x){ return $x->term_name; }, $websites);
        sort($websites);

        $oldWebsites = implode(',', $websites);

        $websites[] = $inputs['term_name'];
        sort($websites);

        $newWebsites = implode(',', $websites);

        $insert_id = $this->website_m->skip_validation()->insert([
            'term_type'     => $this->website_m->term_type,
            'term_name'     => $inputs['term_name'],
            'term_status'   => 'publish'
        ]);

        $metadata = array(
            'create_time'       => time(),
            'create_user_id'    => $this->admin_m->id
        );

        array_walk($metadata, function($value, $key) use($insert_id){
            update_user_meta($insert_id, $key, $value);
        });

        
        $this->term_users_m->set_relations_by_term($insert_id, array($customer->user_id), $customer->user_type);

        audit('updated', 'usermeta', $customer->user_id, 'websites', $newWebsites, $oldWebsites);

        parent::response(['code' => parent::HTTP_OK, 'data' => $insert_id]);
    }

    /**
     * Update the webpage
     */
    public function website_put($id)
    {
        $inputs = parent::put(null, TRUE);
        $this->form_validation->set_data($inputs);

        $rules = array(
            [
                'field' => 'term_name',
                'label' => 'Webpage',
                'rules' => 'required|valid_url'
            ],
            [
                'field' => 'customerId',
                'label' => 'customerId',
                'rules' => 'required|integer'
            ]
        );

        $this->form_validation->set_rules($rules);

        if( FALSE == $this->form_validation->run())
        {
            parent::response([ 'code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array() ]);
        }

        $response = array( 'code' => parent::HTTP_BAD_REQUEST, 'msg' => 'Quá trình xử lý thất bại', 'data' => []);

        if( ! has_permission('admin.customer.update'))
        {
            $response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::renderJson($response, 500);
        }

        $relate_users = $this->admin_m->get_all_by_permissions('admin.customer.update');
        $customer = $this->customer_m->select('user.*')->set_user_type()->get($inputs['customerId']);
        if(empty($customer))
        {
            $response['msg'] = 'Thông tin khách hàng không hợp lệ.';
            parent::response($response);
        }
        
        if(is_bool($relate_users) && FALSE == $relate_users)
        {
            $response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::response($response);
        }

        if(is_array($relate_users) 
            && ! $this->usermeta_m
            ->where('user_id', $customer->user_id)
            ->where_in('usermeta.meta_key', ['assigned_to','created_by'])
            ->where_in('usermeta.meta_value', $relate_users)
            ->count_by()) {

            $response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::response($response);
        }        

        $websites = $this->term_users_m->get_user_terms($inputs['customerId'], $this->website_m->term_type) ?: [];
        if(empty($websites) || ! in_array($id, array_column($websites, 'term_id')))
        {
            $response['msg'] = 'Quyền truy cập webpage này bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::response($response);   
        }
    
        $inputs['term_name'] = $valid_website = $this->website_m->domain_check($inputs['term_name'], $id);
        if( ! $valid_website)
        {
            $response['msg'] = 'Website đã tồn tại hoặc không khả dụng !';
            parent::response($response);
        }

        $is_lock = (bool)get_user_meta_value($inputs['customerId'], 'lock');
        if($is_lock)
        {
            $is_overwrite_permission = has_permission('customer.overwrite.update')
                                       && has_permission('customer.overwrite.manage');
            if(!$is_overwrite_permission)
            {
                $this->load->model('contract/contract_m');
                $website_has_contract = $this->contract_m->set_term_type()
                ->where('term_parent', $id)
                ->select('term_parent')
                ->as_array()
                ->get_all();
                if(!empty($website_has_contract))
                {
                    $response['msg'] = "Website {$inputs['term_name']} không được phép cập nhật do khách hàng đã bị khoá. Vui lòng liên hệ bộ phận Sale-Admin hoặc Kế toán để được hỗ trợ!";
                    parent::response($response);   
                }
            }
        }

        $websites AND $websites = array_map(function($x){ return $x->term_name; }, $websites);
        sort($websites);
        $oldWebsites = implode(',', $websites);

        $this->website_m->skip_validation()->update($id, [ 'term_name' => $inputs['term_name'] ]);

        $this->scache->delete_group("user/{$inputs['customerId']}_terms_");

        $new_websites = $this->term_users_m->get_user_terms($inputs['customerId'], $this->website_m->term_type) ?: [];
        $new_websites AND $new_websites = array_map(function($x){ return $x->term_name; }, $new_websites);
        sort($new_websites);
        $newWebsites = implode(',', $new_websites);

        $oldWebsites != $newWebsites AND audit('updated', 'usermeta', $customer->user_id, 'websites', $newWebsites, $oldWebsites);

        // Update term name
        $this->load->model('contract/contract_m');
        $this->load->config('contract/contract');
        $ref_contracts = $this->term_users_m->get_user_terms(
            $inputs['customerId'], 
            array_keys($this->config->item('taxonomy')), 
            [
                'where'=> [
                    'term.term_parent' => $id
                ]
            ]
        );

        if(empty($ref_contracts))
        {
            return parent::response(['code' => parent::HTTP_OK]);
        }

        foreach($ref_contracts as $contract)
        {
            if($contract->term_name == $inputs['term_name']) continue;

            $this->contract_m->update($contract->term_id, ['term_name' => $inputs['term_name']]);
        }

        parent::response(['code' => parent::HTTP_OK]);
    }

    /**
     * Delete the webpage
     */

    public function website_delete(int $customerId = 0,int $id = 0)
    {
        $this->form_validation->set_data([ 'customerId' => $customerId, 'websiteId' => $id ]);
        
        $rules = array(
            [ 'field' => 'customerId', 'label' => 'customerId', 'rules' => 'required|integer|greater_than[0]' ],
            [ 'field' => 'websiteId', 'label' => 'websiteId', 'rules' => 'required|integer|greater_than[0]' ]
        );
        $this->form_validation->set_rules($rules);

        if( FALSE == $this->form_validation->run())
        {
            parent::response([ 'code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array() ]);
        }

        $response = array( 'code' => parent::HTTP_BAD_REQUEST, 'msg' => 'Quá trình xử lý thất bại', 'data' => []);

        $relate_users   = $this->admin_m->get_all_by_permissions('admin.customer.delete');
        $customer       = $this->customer_m->select('user.*')->set_user_type()->get($customerId);
        if(empty($customer))
        {
            $response['msg'] = 'Thông tin khách hàng không hợp lệ.';
            parent::response($response);
        }
        
        if(is_bool($relate_users) && FALSE == $relate_users)
        {
            $response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::response($response);
        }

        if(is_array($relate_users) 
            && ! $this->usermeta_m
            ->where('user_id', $customer->user_id)
            ->where_in('usermeta.meta_key', ['assigned_to','created_by'])
            ->where_in('usermeta.meta_value', $relate_users)
            ->count_by()) {

            $response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::response($response);
        }        

        $websites = $this->term_users_m->get_user_terms($customerId, $this->website_m->term_type) ?: [];
        if(empty($websites) || ! in_array($id, array_column($websites, 'term_id')))
        {
            $response['msg'] = 'Quyền truy cập webpage này bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::response($response);   
        }

        $this->load->model('contract/contract_m');
        if($this->contract_m->set_term_type()->where('term_parent', $id)->count_by() > 0)
        {
            $response['msg'] = 'Webpage này được liên kết đến một hợp đồng nào đấy, Không thể xóa !';
            parent::response($response);
        }

        $websites AND $websites = array_map(function($x){ return $x->term_name; }, $websites);
        sort($websites);
        $oldWebsites = implode(',', $websites);


        $this->website_m->skip_validation()->set_term_type()->delete($id);
        $this->term_users_m->delete_term_users($id, $customer->user_id);

        $websites = $this->term_users_m->get_user_terms($customerId, $this->website_m->term_type) ?: [];
        $websites AND $websites = array_map(function($x){ return $x->term_name; }, $websites);
        sort($websites);
        $newWebsites = implode(',', $websites);

        $oldWebsites != $newWebsites AND audit('deleted', 'usermeta', $customer->user_id, 'websites', $newWebsites, $oldWebsites);

        parent::response(['code' => parent::HTTP_OK]);
    }

    /**
     * List tax code
     */
    public function tax_code_get(){
        $is_active_tin_check = (bool) $this->option_m->get_value('is_active_tin_check');
        if(!$is_active_tin_check){
            $response = array( 'code' => parent::HTTP_BAD_REQUEST, 'msg' => 'Tính năng hông khả dụng', 'data' => []);
            parent::response($response);
        }

        $response = array( 'code' => parent::HTTP_BAD_REQUEST, 'msg' => 'Quá trình xử lý thất bại', 'data' => []);

        $default_filters = [];

        $filters = parent::get(null, TRUE);
        $filters = wp_parse_args($filters, $default_filters);

        $rules = [];
        if(isset($filters['filters'])){
            $operator = ['=', '>', '<', '>=', '<=', '!=', 'between', 'like', 'not like', 'in', 'not in'];
            $operator_implode = implode(',', $operator);

            $list_filter = $filters['filters'];
            foreach($list_filter as $filter_name => $value){
                $rules[] = [
                    'field' => 'filters[' . $filter_name . '][operator]',
                    'label' => 'filters[' . $filter_name . '][operator]',
                    'rules' => "required|in_list[{$operator_implode}]"
                ];

                $rules[] = [
                    'field' => 'filters[' . $filter_name . '][value]',
                    'label' => 'filters[' . $filter_name . '][value]',
                    'rules' => 'required'
                ];
            }
        }

        if(!empty($rules)){
            $this->form_validation->set_data($filters);
            $this->form_validation->set_rules($rules);
    
            if( FALSE == $this->form_validation->run())
            {
                parent::response([ 'code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array() ]);
            }
        }
        
        $company = (new TaxCode())->list($filters);
        if(empty($company)){
            $response['msg'] = 'Không tìm thấy công ty với mã số thuế tương ứng';
            parent::response($response);
        }

        parent::response(['code' => parent::HTTP_OK, 'data' => $company]);
    }

    /**
     * Find company by tax code
     */
    public function find_company_by_tax_code_get($tax_code){
        $is_active_tin_check = (bool) $this->option_m->get_value('is_active_tin_check');
        if(!$is_active_tin_check){
            $response = array( 'code' => parent::HTTP_BAD_REQUEST, 'msg' => 'Tính năng hông khả dụng', 'data' => []);
            parent::response($response);
        }

        $response = array( 'code' => parent::HTTP_BAD_REQUEST, 'msg' => 'Quá trình xử lý thất bại', 'data' => []);
        
        $company = (new TaxCode())->find($tax_code);
        if(empty($company)){
            $response['msg'] = 'Không tìm thấy công ty với mã số thuế tương ứng';
            parent::response($response);
        }

        parent::response(['code' => parent::HTTP_OK, 'data' => $company]);
    }
}
/* End of file Index.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Index.php */