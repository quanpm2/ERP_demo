<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AdvancedFiltering extends MREST_Controller
{
	/**
	 * CONSTRUCTION
	 *
	 * @param      string  $config  The configuration
	 */
	function __construct($config = 'rest')
	{
		array_push($this->autoload['models'], 'customer/customer_m', 'term_users_m', 'customer/website_m');
		parent::__construct($config);

        $this->load->config('customer');
        $this->load->library('form_validation');
	}

    public function search_get()
    {
        $response = [ 'code' => 204, 'data' => [], 'total' => 0 ];

        $args = wp_parse_args( parent::get(NULL, TRUE), array(
            'user_type'         => 'customer_company',
            'lp'                => null,
            'display_name'      => null,
            'customer_email'    => null,
            'customer_phone'    => null,
            'customer_tax'      => null,
            'customer_name'     => null, 
        ));

        $conditions = [
            'where'     => [],
            'where_in'  => [],
            'like'      => []
        ];

        if( ! empty($args['lp']))
        {
            $websites = $this->website_m->select('term_id')->set_term_type()->like('term_name', trim($args['lp']))->get_all();
            if(empty($websites)) parent::response($response);

            $_customers = $this
                ->term_users_m
                ->select('term_users.user_id')
                ->join('user', 'user.user_id = term_users.user_id and user.user_type = ' .str_pad($args['user_type'], strlen($args['user_type']) + 2, "\"", STR_PAD_BOTH))
                ->where_in('term_id', array_map('intval', array_column($websites, 'term_id')))
                ->get_all();    

            $_customers AND array_push($conditions['where_in'], [ 'key' => 'user.user_id', 'value' => array_map('intval', array_column($_customers, 'user_id'))]);
        }

        empty($args['user_type'])       OR array_push($conditions['where'], ['key' => 'user_type', 'value' => $args['user_type']]);
        empty($args['display_name'])    OR array_push($conditions['like'], ['key' => 'display_name', 'value' => trim($args['display_name'])]);

        if( ! empty($args['customer_email']))
        {
            $metadata = $this->usermeta_m->select('user.user_id')->where('meta_key', 'customer_email')
            ->join('user', 'user.user_id = usermeta.user_id and user_type in ('.implode(',', array_map(function($x){ return str_pad($x, strlen($x) + 2, "\"", STR_PAD_BOTH); }, $this->customer_m->customer_types)).')')
            ->like('meta_value', trim($args['customer_email']))->get_all();
            empty($metadata) AND parent::response($response);
            array_push($conditions['where_in'], [ 'key' => 'user.user_id', 'value' => array_map('intval', array_column($metadata, 'user_id'))]);
        }

        if( ! empty($args['customer_phone']))
        {
            $metadata = $this->usermeta_m->select('user.user_id')->where('meta_key', 'customer_phone')
            ->join('user', 'user.user_id = usermeta.user_id and user_type in ('.implode(',', array_map(function($x){ return str_pad($x, strlen($x) + 2, "\"", STR_PAD_BOTH); }, $this->customer_m->customer_types)).')')
            ->like('meta_value', trim($args['customer_phone']))->get_all();
            empty($metadata) AND parent::response($response);
            array_push($conditions['where_in'], [ 'key' => 'user.user_id', 'value' => array_map('intval', array_column($metadata, 'user_id'))]);
        }

        if( ! empty($args['customer_tax']))
        {
            $metadata = $this->usermeta_m->select('user.user_id')->where('meta_key', 'customer_tax')
            ->join('user', 'user.user_id = usermeta.user_id and user_type in ('.implode(',', array_map(function($x){ return str_pad($x, strlen($x) + 2, "\"", STR_PAD_BOTH); }, $this->customer_m->customer_types)).')')
            ->like('meta_value', trim($args['customer_tax']))->get_all();
            empty($metadata) AND parent::response($response);
            array_push($conditions['where_in'], [ 'key' => 'user.user_id', 'value' => array_map('intval', array_column($metadata, 'user_id'))]);
        }

        if( ! empty($customer_name))
        {
            $metadata = $this->usermeta_m->select('user.user_id')->where('meta_key', 'customer_name')
            ->join('user', 'user.user_id = usermeta.user_id and user_type in ('.implode(',', array_map(function($x){ return str_pad($x, strlen($x) + 2, "\"", STR_PAD_BOTH); }, $this->customer_m->customer_types)).')')
            ->like('meta_value', trim($customer_name))->get_all();
            empty($metadata) AND parent::response($response);
            array_push($conditions['where_in'], [ 'key' => 'user.user_id', 'value' => array_map('intval', array_column($metadata, 'user_id'))]);
        }

        if( ! has_permission('admin.contract.manage'))
        {
            $this->load->config('contract/contract');

            $relate_users       = $this->admin_m->get_all_by_permissions('Admin.Customer.View');
            $_customerIds = $this->customer_m
            ->set_get_customer()
            ->select('user.user_id')
            ->join('usermeta AS m_customer', '
                m_customer.user_id = user.user_id 
                AND m_customer.meta_key IN ("created_by", "assigned_to")
            ')
            ->where_in('m_customer.meta_value',  $relate_users)
            ->group_by('user.user_id')
            ->as_array()
            ->get_all([ 'user_type' => 'admin' ]);

            empty($_customerIds) AND parent::response([ 'code' => 204, 'data' => [] ]);

            array_push($conditions['where_in'], [ 'key' => 'user.user_id', 'value' => array_column($_customerIds, 'user_id')]);
        }

        foreach ($conditions as $condition => $row)
        {
            if(empty($row)) continue;
            array_walk($row, function($query) use($condition){
                $this->customer_m->{$condition}($query['key'], $query['value']);
            });
        }

        $customers = $this->customer_m->select('user.user_id,user.user_type,user.user_email,user.display_name')->set_get_customer()->get_all();
        $customers AND $customers = array_values(array_map(function($x){
            $x->user_id = (int) $x->user_id;
            $x->display_name = $x->display_name ?: get_user_meta_value($x->user_id, 'customer_name');
            $x->customer_phone = get_user_meta_value($x->user_id, 'customer_phone');
            $x->created_source = get_user_meta_value($x->user_id, 'created_source');
            return $x;
        }, $customers));

        $response = [ 'code' => 200, 'total' => count($customers), 'data' => $customers ];

        parent::response($response);
    }

    /**
     * API dữ liệu các đợt thanh toán
     */
    public function owned_get()
    {
        if( ! has_permission('Admin.Customer.View')) parent::response("401 UNAUTHORIZED", self::HTTP_UNAUTHORIZED);

        $query = null;
        empty(parent::get('q')) OR $query = implode(' ', explode(' ', parent::get('q')));

        $customers = array();

        switch (TRUE) {

        	case has_permission('admin.contract.manage'):
        		empty($query) OR $this->customer_m->like('display_name', $query);
        		$customers = $this->customer_m
        			->select('user.user_id,user.user_type,user.user_email,user.display_name')
        			->set_get_customer()
        			->order_by('display_name')
        			->limit(1000)
        			->get_all();
        		break;

        	default:
        		$this->load->config('contract/contract');
				$taxonomies = $this->config->item('taxonomy');
				$key_taxonomies = array_keys($taxonomies);
				$key_taxonomies[] = '';

				# Load tất cả các khách hàng mà user có liên quan đến hiện tại
				if( $owners_terms = $this->term_users_m->get_the_terms($this->admin_m->id,$key_taxonomies))
				{
					empty($query) OR $this->customer_m->like('display_name', $query);

					if($owners_terms_customers = $this->customer_m
					->set_user_type()
					->distinct('user.user_id')
					->select('user.user_id,user.user_type,user.user_email,user.display_name')
					->join('term_users','term_users.user_id = user.user_id')
					->join('termmeta tm_sale','tm_sale.term_id = term_users.term_id AND tm_sale.meta_key = "staff_business"')
					->group_start()
					->where_in('term_users.term_id',$owners_terms)
					->or_where('tm_sale.meta_value',$this->admin_m->id)
					->group_end()	
					->limit(1000)			
					->get_all(['user_type'=>'admin'])) foreach ($owners_terms_customers as $i) $customers[$i->user_id] = $i;
				}

				empty($query) OR $this->customer_m->like('display_name', $query);

				if( $ownered_customers = $this->customer_m
					->select('user.user_id,user.display_name,user.user_email,user.user_type,user.user_status')
					->set_user_type()
					->join('usermeta',"usermeta.user_id = user.user_id")
					->where_in('usermeta.meta_key', ['assigned_to','created_by'])
					->where('usermeta.meta_value',$this->admin_m->id)
					->group_by('user.user_id')
					->limit(1000)
					->get_all()) foreach ($ownered_customers as $i) $customers[$i->user_id] = $i;

        		break;
        }

        $customers AND $customers = array_values(array_map(function($x){
        	$x->user_id = (int) $x->user_id;
        	$x->display_name = $x->display_name ?: get_user_meta_value($x->user_id, 'customer_name');
        	$x->customer_phone = get_user_meta_value($x->user_id, 'customer_phone');
        	$x->created_source = get_user_meta_value($x->user_id, 'created_source');
        	return $x;
        }, $customers));

    	parent::response(['status'=>TRUE, 'data' => $customers]);
    }

    /**
     * API dữ liệu các đợt thanh toán
     */
    public function view_get($id)
    {
        if( ! has_permission('Admin.Customer.View')) parent::response("401 UNAUTHORIZED", self::HTTP_UNAUTHORIZED);

        $customer = $this->customer_m->set_user_type()->get($id);
        if(empty($customer)) parent::response(['status' => TRUE, 'data' => null]);

    	$customer->user_id 			= (int) $customer->user_id;
    	$customer->display_name 	= $customer->display_name ?: get_user_meta_value($customer->user_id, 'customer_name');
    	$customer->customer_phone	= get_user_meta_value($customer->user_id, 'customer_phone');
    	$customer->created_source	= get_user_meta_value($customer->user_id, 'created_source');

    	parent::response(['status'=>TRUE, 'data' => $customer]);
    }

    /**
     * Determines whether the specified value is unique tin get.
     *
     * @param      string  $value  The value
     *
     * @return     bool    True if the specified value is unique tin get, False otherwise.
     */
    public function is_unique_tin_get($value = "")
    {
        if( ! preg_match('/^\d{10}(-\d{3})?$/', $value, $matches)) return parent::response([ 'code' => parent::HTTP_OK, 'data' => false ]);

        parent::response([
            'code' => parent::HTTP_OK,
            'data' => ! $this->usermeta_m->where('meta_key', 'customer_tax')->like('meta_value', trim($value))->count_by() > 0
        ]);
    }

    /**
     * Determines if unique email get.
     */
    public function is_unique_email_get()
    {
        parent::response([
            'code' => parent::HTTP_OK,
            'data' => $this->customer_m->email_uniquecheck(parent::get('value', TRUE), parent::get('type', TRUE))
        ]);
    }

    /**
     * Determines if unique phone get.
     */
    public function is_unique_phone_get()
    {
        parent::response([
            'code' => parent::HTTP_OK,
            'data' => $this->customer_m->phone_uniquecheck(parent::get('value', TRUE), parent::get('type', TRUE))
        ]);
    }
}
/* End of file Index.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Index.php */