<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'vendor/autoload.php';

class Fix extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'customer/customer_m';

        parent::__construct();
    }

    /**
     * Endpoint: GET //api-v2/customer/fix/migrate_contract_customer_assign_to
     * Description:
     *     Update sale meta_key = assign_to by
     *     staff_bussiness_id meta in contracts of customer
     */
    public function migrate_contract_customer_assign_to_get()
    {
        $is_review_mode = true;

        if(!( has_permission('Admin.Customer.manage') 
              && has_permission('Admin.Customer.update')))
        {
            return parent::responseHandler([], 'Permission deny', 'error', 403);
        }

        $contracts = $this->contract_m->set_term_type()
            ->join('term_users AS tu_customer', 'tu_customer.term_id = term.term_id')
            ->join('user AS customer', '
                customer.user_id = tu_customer.user_id 
                AND customer.user_type IN ("customer_company", "customer_person")
            ')
            ->join('termmeta AS m_contract', '
                m_contract.term_id = term.term_id
                AND m_contract.meta_key = "staff_business"
            ')
            ->select('term.term_id')
            ->select('customer.user_id')
            ->select('m_contract.meta_value AS staff_business_id')
            ->where('m_contract.meta_value IS NOT NULL')
            ->group_by('term.term_id')
            ->order_by('term.term_id', 'DESC')
            ->as_array()
            ->get_all();

        log_message('debug', 'Num Of Contracts is ' . count($contracts));

        $contracts = array_filter($contracts, function($contract){
            return !empty($contract['staff_business_id']);
        });

        log_message('debug', 'Num Of Contracts belongs to somebody is ' . count($contracts));

        $stat = [
            'total' => count($contracts),
            'processed' => 0,
            'ignored' => 0,
            'need_check' => 0,
            'data' => [
                'processed' => [],
                'ignored' => [],
                'need_check' => [],
            ]
        ];

        log_message('debug', 'Num Of Contract\' customers is ' . $stat['total']);

        $stat = [
            'total' => count($contracts),
            'processed' => 0,
            'ignored' => 0,
            'need_check' => 0,
            'data' => [
                'processed' => [],
                'ignored' => [],
                'need_check' => [],
            ]
        ];

        log_message('debug', 'Num Of Contract\' customers is ' . $stat['total']);

        while (!empty($contracts)) {
            $prepend_contracts = array_splice($contracts, 0, 1000);

            foreach ($prepend_contracts as $contract) {
                $customer_id = $contract['user_id'];
                $assigned_to = get_user_meta($customer_id, 'assigned_to', FALSE, TRUE);
                $created_by = get_user_meta_value($customer_id, 'created_by');
                $staff_business_id = $contract['staff_business_id'];

                $contract['assigned_to'] = $assigned_to;
                $contract['created_by'] = $created_by;

                $staff_unique = array_unique(array_merge($assigned_to, [$created_by]));
                if(in_array($staff_business_id, $staff_unique))
                {
                    $stat['ignored'] += 1;
                    $stat['data']['ignored'][] = $contract;
                    log_message('debug', "Ignore staff_business {$staff_business_id} assigned to customer {$customer_id}. " . json_encode($contract));

                    continue;
                }

                if(empty($assigned_to))
                {
                    $is_review_mode OR update_user_meta($customer_id, 'assigned_to', $staff_business_id);
                    log_message('debug', "Update staff_business {$staff_business_id} assigned to customer {$customer_id}. " . json_encode($contract));

                    $stat['processed'] += 1;
                    $stat['data']['processed'][] = $contract;

                    continue;
                }

                $is_review_mode OR $this->usermeta_m->add_meta($customer_id, 'assigned_to', $staff_business_id);
                log_message('debug', "Append staff_business {$staff_business_id} assigned to customer {$customer_id}. " . json_encode($contract));

                $stat['need_check'] += 1;
                $stat['data']['need_check'][] = $contract;
            }
        }

        return parent::responseHandler($stat, 'Migrated');
    }
}
/* End of file Index.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Index.php */