<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Crawler\TaxCode\TaxCode;
use VerifyEmail\VerifyEmail;
use VerifyEmail\Enums\Status;

class Index extends MREST_Controller
{
	/**
	 * CONSTRUCTION
	 *
	 * @param      string  $config  The configuration
	 */
	function __construct($config = 'rest')
	{
		array_push($this->autoload['models'], 'customer/customer_m', 'term_users_m');
		parent::__construct($config);

        $this->load->config('customer');
        $this->load->config('contract/contract');
        $this->load->library('form_validation');
	}

    /**
     * Creates a Customer.
     */
    public function update_put($id)
    {
        $response = array( 'code' => parent::HTTP_BAD_REQUEST, 'msg' => 'Quá trình xử lý thất bại', 'data' => []);

        $relate_users = $this->admin_m->get_all_by_permissions('admin.customer.update');

        $customer = $this->customer_m->select('user.*')->set_user_type()->get($id);
        if(empty($customer))
        {
            $response['msg'] = 'Thông tin khách hàng không hợp lệ.';
            parent::response($response);   
        }
        
        if(is_bool($relate_users) && FALSE === $relate_users)
        {
            $response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::response($response);
        }

        $is_lock = (bool)get_user_meta_value($id, 'lock');
        if($is_lock)
        {
            $is_overwrite_permission = has_permission('customer.overwrite.update')
                                       && has_permission('customer.overwrite.manage');
            
            if(!$is_overwrite_permission)
            {
                $response['msg'] = 'Thao tác cập nhật đã bị khoá. Vui lòng liên hệ bộ phận Sale-Admin hoặc Kế toán để được hỗ trợ.';
                parent::response($response);   
            }
        }

        $relate_users       = $this->admin_m->get_all_by_permissions('admin.customer.update');
        if($relate_users === FALSE)
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);   
        }

        if(is_array($relate_users)){
            $taxonomies = array_keys($this->config->item('taxonomy'));
            $taxonomies = array_map(function($item){ return '"'. $item . '"';}, $taxonomies);
            $contract_type_str = implode(',', $taxonomies);
        
            $is_access = $this->customer_m
            ->set_get_customer()
            ->join('term_users AS tu_contract', 'tu_contract.user_id = user.user_id', 'LEFT')
            ->join('term AS contract', "contract.term_id = tu_contract.term_id AND contract.term_type IN ({$contract_type_str})", 'LEFT')
            ->join('term_users AS tu_sale', 'tu_sale.term_id = contract.term_id', 'LEFT')
            ->join('user AS sale', 'sale.user_id = tu_sale.user_id AND sale.user_type = "admin"', 'LEFT')
            ->join('usermeta AS m_customer', 'm_customer.user_id = user.user_id AND m_customer.meta_key IN ("created_by", "assigned_to")', 'LEFT')
            ->select('MAX(IF(m_customer.meta_key = "created_by", m_customer.meta_value, NULL)) AS created_by')
            ->select('MAX(IF(m_customer.meta_key = "assigned_to", m_customer.meta_value, NULL)) AS assigned_to')
            ->where('user.user_id', $id)
            ->where_in('sale.user_id', $relate_users)
            ->count_by() > 0;
            if(!$is_access){
                $response['msg'] = 'Quyền truy cập không hợp lệ.';
                parent::response($response);  
            }
        }

        $inputs = parent::put(null, TRUE);
        $this->form_validation->set_data($inputs);

        $rules = array(
            [
                'field' => 'user_type',
                'label' => 'Loại Khách hàng',
                'rules' => 'required|in_list[customer_person,customer_company]'
            ],
            [
                'field' => 'display_name',
                'label' => 'Tên hiển thị',
                'rules' => 'required'
            ],
            [
                'field' => 'customer_email',
                'label' => 'Email',
                'rules' => 'required'
            ],
            [
                'field' => 'customer_phone',
                'label' => 'Số điện thoại',
                'rules' => [
                    'required',
                    ['is_valid_phone', function($value){ 
                        $is_valid = phone_validator($value);
                        if(!$is_valid){
                            $this->form_validation->set_message('is_valid_phone', 'Số điện thoại không hợp lệ');
                        }

                        return $is_valid;
                    }]
                ]
            ],
            [
                'field' => 'customer_address',
                'label' => 'Địa chỉ',
                'rules' => 'required'
            ]
        );

        if('customer_company' == ($inputs['user_type'] ?? null))
        {
            $rules = array_merge($rules, [
                [
                    'field' => 'customer_name',
                    'label' => 'Họ tên người đại diện',
                    'rules' => 'required'
                ],
                [
                    'field' => 'customer_tax',
                    'label' => 'Mã số thuế',
                    'rules' => [
                        'required',
                        ['is_valid_tin', function($value){ 
                            $is_valid = tin_validator($value);
                            if(!$is_valid){
                                $this->form_validation->set_message('is_valid_tin', 'Mã số thuế không hợp lệ');
                            }
    
                            return $is_valid;
                        }]
                    ]
                ]
            ]);
        }

        $this->form_validation->set_rules($rules);

        if( FALSE == $this->form_validation->run())
        {
            parent::response([
                'code'  => parent::HTTP_BAD_REQUEST,
                'msg'   => $this->form_validation->error_array()
            ]);
        }

        // Detect user and force update option
        $is_force_update = has_permission('Admin.Customer.manage') && (bool)$inputs['is_force_update'];
        if( ! $is_force_update )
        {
            $has_manage_customer = has_permission('Admin.Customer.mdeparment') || has_permission('Admin.Customer.mgroup');
    
            /**
             * Check Phone Number Unique
             */
            $isPhoneUnique = $this->customer_m->phone_uniquecheck(trim($inputs['customer_phone']), trim($inputs['user_type']), $id);
            if( ! $isPhoneUnique) 
            {
                parent::response([
                    'code'  => parent::HTTP_BAD_REQUEST, 
                    'msg'   => 'Số điện thoại đã tồn tại.' 
                ]);
            }

            /**
             * Check tax code in database tax
             */
            if( 'customer_company' == $inputs['user_type'] 
                && FALSE == $this->customer_m->tax_uniquecheck(trim($inputs['customer_tax']), trim($inputs['user_type']), $id)
            )
            {
                parent::response([
                    'code'  => parent::HTTP_BAD_REQUEST,
                    'msg'   => 'Mã số thuế của doanh nghiệp đã tồn tại.'
                ]);
            }

            if( 'customer_company' == $inputs['user_type']
                && (bool) $this->option_m->get_value('is_active_tin_check')
            )
            {
                $customer_tax = (new TaxCode())->find(trim($inputs['customer_tax']));

                if( empty($customer_tax) 
                    && !$has_manage_customer
                )
                {
                    parent::response([
                        'code'  => parent::HTTP_BAD_REQUEST,
                        'msg'   => 'Mã số thuế của doanh nghiệp chưa được xác nhận bởi cơ quan thuế'
                    ]);
                }
            }
    
            /**
             * Check Email Unique
             */
            $isEmailUnique = $this->customer_m->email_uniquecheck(trim($inputs['customer_email']), trim($inputs['user_type']), $id);
            if( ! $isEmailUnique) 
            {
                parent::response([ 
                    'code'  => parent::HTTP_BAD_REQUEST, 
                    'msg' => 'Email khách hàng đã tồn tại.' 
                ]);
            }

            $isValid = (new VerifyEmail())->setEmail(trim($inputs['customer_email']))->verify();
            if( Status::SUCCESS != $isValid
                && !$has_manage_customer
            )
            {
                parent::response([ 
                    'code'  => parent::HTTP_BAD_REQUEST,
                    'msg'   => Status::name($isValid) 
                ]);
            }
        }

        $originalProperties = array(
            'user_type'         => $customer->user_type,
            'display_name'      => $customer->display_name,
            'user_email'        => $customer->user_email,
            'customer_name'     => get_user_meta_value($customer->user_id, 'customer_name'),
            'customer_gender'   => get_user_meta_value($customer->user_id, 'customer_gender'),
            'customer_email'    => get_user_meta_value($customer->user_id, 'customer_email'),
            'customer_address'  => get_user_meta_value($customer->user_id, 'customer_address'),
            'customer_phone'    => get_user_meta_value($customer->user_id, 'customer_phone'),
            'customer_tax'      => get_user_meta_value($customer->user_id, 'customer_tax')
        );

        $updateProperties = array_filter($inputs, function($value, $key) use($originalProperties){

            if( ! in_array( $key, array_keys($originalProperties))) return false;
            return $value != $originalProperties[$key];

        }, ARRAY_FILTER_USE_BOTH);

        if(empty($updateProperties)) parent::response(['code' => parent::HTTP_OK, 'message' => 'OK']);

        $userProps = array_filter($updateProperties, function($value, $key) use($customer){
            return in_array( $key, array_keys( (array) $customer));
        }, ARRAY_FILTER_USE_BOTH);
        
        if( ! empty($userProps))
        {
            $this->customer_m->skip_validation()->update($customer->user_id, $userProps);
            array_walk($userProps, function($value, $key) use($originalProperties, $customer){
                audit('updated', 'user', $customer->user_id, $key, $value, $originalProperties[$key]);
            });
        }

        $metaProps = array_filter($updateProperties, function($value, $key) use($customer){
            return ! in_array( $key, array_keys( (array) $customer));
        }, ARRAY_FILTER_USE_BOTH);

        if( ! empty($metaProps))
        {
            array_walk($metaProps, function($value, $key) use($customer, $originalProperties){
                update_user_meta($customer->user_id, $key, $value);
                audit('updated', 'usermeta', $customer->user_id, $key, $value, $originalProperties[$key]);
            });
        }

        parent::response(['code' => parent::HTTP_OK]);
    }

    /**
     * Creates a Customer.
     */
    public function create_post()
    {
        $response = array( 'code' => parent::HTTP_BAD_REQUEST, 'msg' => 'Quá trình xử lý thất bại', 'data' => []);

        if( ! has_permission('admin.customer.add'))
        {
            $response['msg'] = 'Quyền truy cập bị hạn chế. Vui lòng Liên hệ quản trị để được cấp quyền !';
            parent::response($response);
        }

        $inputs = parent::post(null, TRUE);
        $this->form_validation->set_data($inputs);

        $rules = array(
            [
                'field' => 'user_type',
                'label' => 'Loại Khách hàng',
                'rules' => 'required|in_list[customer_person,customer_company]'
            ],
            [
                'field' => 'display_name',
                'label' => 'Tên hiển thị',
                'rules' => 'required'
            ],
            [
                'field' => 'customer_email',
                'label' => 'Email',
                'rules' => 'required'
            ],
            [
                'field' => 'customer_phone',
                'label' => 'Số điện thoại',
                'rules' => [
                    'required',
                    ['is_valid_phone', function($value){ 
                        $is_valid = phone_validator($value);
                        if(!$is_valid){
                            $this->form_validation->set_message('is_valid_phone', 'Số điện thoại không hợp lệ');
                        }

                        return $is_valid;
                    }]
                ]
            ],
            [
                'field' => 'customer_address',
                'label' => 'Địa chỉ',
                'rules' => 'required'
            ]
        );

        if('customer_company' == ($inputs['user_type'] ?? null))
        {
            $rules = array_merge($rules, [
                [
                    'field' => 'customer_name',
                    'label' => 'Họ tên người đại diện',
                    'rules' => 'required'
                ],
                [
                    'field' => 'customer_tax',
                    'label' => 'Mã số thuế',
                    'rules' => [
                        'required',
                        ['is_valid_tin', function($value){ 
                            $is_valid = tin_validator(trim($value));
                            if(!$is_valid){
                                $this->form_validation->set_message('is_valid_tin', 'Mã số thuế không hợp lệ');
                            }
    
                            return $is_valid;
                        }]
                    ]
                ]
            ]);
        }

        $this->form_validation->set_rules($rules);

        if( FALSE == $this->form_validation->run())
        {
            parent::response([
                'code'  => parent::HTTP_BAD_REQUEST,
                'msg'   => $this->form_validation->error_array()
            ]);
        }

        // Detect user and force update option
        $is_force_update = has_permission('Admin.Customer.manage') && (bool)$inputs['is_force_update'];
        if( ! $is_force_update )
        {
            $has_manage_customer = has_permission('Admin.Customer.mdeparment') || has_permission('Admin.Customer.mgroup');
    
            /**
             * Check Phone Number Unique
             */
            $isPhoneUnique = $this->customer_m->phone_uniquecheck(trim($inputs['customer_phone']), trim($inputs['user_type']));
            if( ! $isPhoneUnique) 
            {
                parent::response([
                    'code'  => parent::HTTP_BAD_REQUEST, 
                    'msg'   => 'Số điện thoại đã tồn tại.' 
                ]);
            }

            /**
             * Check tax code in database tax
             */
            if( 'customer_company' == $inputs['user_type'] 
                && FALSE == $this->customer_m->tax_uniquecheck(trim($inputs['customer_tax']), trim($inputs['user_type']))
            )
            {
                parent::response([
                    'code'  => parent::HTTP_BAD_REQUEST,
                    'msg'   => 'Mã số thuế của doanh nghiệp đã tồn tại.'
                ]);
            }

            if( 'customer_company' == $inputs['user_type']
                && (bool) $this->option_m->get_value('is_active_tin_check')
            )
            {
                $customer_tax = (new TaxCode())->find(trim($inputs['customer_tax']));

                if( empty($customer_tax) 
                    && !$has_manage_customer
                )
                {
                    parent::response([
                        'code'  => parent::HTTP_BAD_REQUEST,
                        'msg'   => 'Mã số thuế của doanh nghiệp chưa được xác nhận bởi cơ quan thuế'
                    ]);
                }
            }
    
            /**
             * Check Email Unique
             */
            $isEmailUnique = $this->customer_m->email_uniquecheck(trim($inputs['customer_email']), trim($inputs['user_type']));
            if( ! $isEmailUnique) 
            {
                parent::response([ 
                    'code'  => parent::HTTP_BAD_REQUEST, 
                    'msg' => 'Email khách hàng đã tồn tại.' 
                ]);
            }

            $isValid = (new VerifyEmail())->setEmail(trim($inputs['customer_email']))->verify();
            if( Status::SUCCESS != $isValid
                && !$has_manage_customer
            )
            {
                parent::response([ 
                    'code'  => parent::HTTP_BAD_REQUEST,
                    'msg'   => Status::name($isValid) 
                ]);
            }
        }

        $insert_id = $this->customer_m->skip_validation()->insert([
            'user_type'     => $inputs['user_type'],
            'display_name'  => $inputs['display_name'],
            'user_email'    => $inputs['customer_email']
        ]);

        $metadata = array(
            'created_by'        => $this->admin_m->id,
            'assigned_to'       => $this->admin_m->id,
            'customer_name'     => (string) ( $inputs['customer_name'] ?? null ),
            'customer_gender'   => (string) $inputs['customer_gender'],
            'customer_email'    => (string) $inputs['customer_email'],
            'customer_address'  => (string) $inputs['customer_address'],
            'customer_phone'    => (string) $inputs['customer_phone']
        );

        'customer_company' == $inputs['user_type'] AND $metadata['customer_tax'] = (string) $inputs['customer_tax'];

        array_walk($metadata, function($value, $key) use($insert_id){
            update_user_meta($insert_id, $key, $value);
            audit('created', 'usermeta', $insert_id, $key, $value);
        });

        $response = array(
            'code' => parent::HTTP_OK,
            'data' => $insert_id
        );

        parent::response($response);
    }

    /**
     * API dữ liệu các đợt thanh toán
     */
    public function owned_get()
    {
        if( ! has_permission('Admin.Customer.View')) parent::response("401 UNAUTHORIZED", self::HTTP_UNAUTHORIZED);

        $query = null;
        empty(parent::get('q')) OR $query = implode(' ', explode(' ', parent::get('q')));

        $customers = array();

        switch (TRUE) {

        	case has_permission('admin.contract.manage'):
        		empty($query) OR $this->customer_m->like('display_name', $query);
        		$customers = $this->customer_m
        			->select('user.user_id,user.user_type,user.user_email,user.display_name')
        			->set_get_customer()
        			->order_by('display_name')
        			->limit(1000)
        			->get_all();
        		break;

        	default:
        		$this->load->config('contract/contract');
				$taxonomies = $this->config->item('taxonomy');
				$key_taxonomies = array_keys($taxonomies);
				$key_taxonomies[] = '';

				# Load tất cả các khách hàng mà user có liên quan đến hiện tại
				if( $owners_terms = $this->term_users_m->get_the_terms($this->admin_m->id, $key_taxonomies))
				{
					empty($query) OR $this->customer_m->like('display_name', $query);

					if($owners_terms_customers = $this->customer_m
					->set_user_type()
					->distinct('user.user_id')
					->select('user.user_id,user.user_type,user.user_email,user.display_name')
					->join('term_users','term_users.user_id = user.user_id')
					->join('termmeta tm_sale','tm_sale.term_id = term_users.term_id AND tm_sale.meta_key = "staff_business"')
					->group_start()
					->where_in('term_users.term_id',$owners_terms)
					->or_where('tm_sale.meta_value',$this->admin_m->id)
					->group_end()	
					->limit(1000)			
					->get_all(['user_type'=>'admin'])) foreach ($owners_terms_customers as $i) $customers[$i->user_id] = $i;
				}

				empty($query) OR $this->customer_m->like('display_name', $query);

				if( $ownered_customers = $this->customer_m
					->select('user.user_id,user.display_name,user.user_email,user.user_type,user.user_status')
					->set_user_type()
					->join('usermeta',"usermeta.user_id = user.user_id")
					->where_in('usermeta.meta_key', ['assigned_to','created_by'])
					->where('usermeta.meta_value',$this->admin_m->id)
					->group_by('user.user_id')
					->limit(1000)
					->get_all()) foreach ($ownered_customers as $i) $customers[$i->user_id] = $i;

        		break;
        }

        $customers AND $customers = array_values(array_map(function($x){
        	$x->user_id = (int) $x->user_id;
        	$x->display_name = $x->display_name ?: get_user_meta_value($x->user_id, 'customer_name');
        	$x->customer_phone = get_user_meta_value($x->user_id, 'customer_phone');
        	$x->created_source = get_user_meta_value($x->user_id, 'created_source');
        	return $x;
        }, $customers));

    	parent::response([
            'status'    => TRUE,
            'total'     => count($customers),
            'data'      => $customers
        ]);
    }

    /**
     * API dữ liệu các đợt thanh toán
     */
    public function view_get($id)
    {
        if( ! has_permission('Admin.Customer.View')) parent::response("401 UNAUTHORIZED", self::HTTP_UNAUTHORIZED);

        $customer = $this->customer_m->set_user_type()->get($id);
        if(empty($customer)) parent::response(['status' => TRUE, 'data' => null]);

    	$customer->user_id 			= (int) $customer->user_id;
    	$customer->display_name 	= $customer->display_name ?: get_user_meta_value($customer->user_id, 'customer_name');

    	$customer->created_source	= get_user_meta_value($customer->user_id, 'created_source');

        $customer->customer_name =  get_user_meta_value($customer->user_id, 'customer_name');
        $customer->customer_gender =  get_user_meta_value($customer->user_id, 'customer_gender');
        $customer->customer_email =  get_user_meta_value($customer->user_id, 'customer_email');
        $customer->customer_address =  get_user_meta_value($customer->user_id, 'customer_address');
        $customer->customer_phone =  get_user_meta_value($customer->user_id, 'customer_phone');
        $customer->customer_tax =  get_user_meta_value($customer->user_id, 'customer_tax');

        $this->load->model('customer/website_m');
        $websites = $this->term_users_m->get_user_terms($id, $this->website_m->term_type) ?: [];
        
        $this->load->model('contract/contract_m');
        $website_has_contract = $this->contract_m->set_term_type()
        ->where_in('term_parent', array_keys($websites))
        ->select('term_parent')
        ->as_array()
        ->get_all();
        $disabled_update_website_ids = array_column($website_has_contract, 'term_parent');

        $websites AND $websites = array_map(function($x) use ($disabled_update_website_ids){
            $is_disabled_update = in_array($x->term_id, $disabled_update_website_ids);

            return [
                'term_id'            => (int) $x->term_id,
                'term_status'        => $x->term_status,
                'term_name'          => $x->term_name,
                'is_disabled_update' => $is_disabled_update,
            ];
        }, array_values($websites)); 

        $customer->websites = $websites;

        $customer->is_lock = (bool)get_user_meta_value($id, 'lock');
        $customer->is_overwrite_permission = has_permission('customer.overwrite.update')
                                             && has_permission('customer.overwrite.manage');

    	parent::response([ 'code' => 200, 'status'=>TRUE, 'data' => $customer]);
    }

    /**
     * Determines whether the specified value is unique tin get.
     *
     * @param      string  $value  The value
     *
     * @return     bool    True if the specified value is unique tin get, False otherwise.
     */
    public function is_unique_tin_get($value = "")
    {
        $isValidTin = tin_validator($value);
        if(!$isValidTin) return parent::response([ 'code' => parent::HTTP_OK, 'data' => false ]);

        $ignoreId = (int) parent::get('ignoreId', TRUE);
        $ignoreId AND $this->usermeta_m->where('usermeta.user_id !=', $ignoreId);
        
        parent::response([
            'code' => parent::HTTP_OK,
            'data' => ! $this->usermeta_m->where('meta_key', 'customer_tax')->where('meta_value', trim($value))->count_by() > 0
        ]);
    }

    /**
     * Determines if verify email get.
     */
    public function is_verify_email_get()
    {
        $value = parent::get('value', TRUE);

        $isValid = (new VerifyEmail())->setEmail(trim($value))->verify();
        parent::responseHandler(Status::SUCCESS == $isValid, Status::name($isValid));
    }

    /**
     * Determines if unique email get.
     */
    public function is_unique_email_get()
    {

        parent::response([
            'code' => parent::HTTP_OK,
            'data' => $this->customer_m->email_uniquecheck(trim(parent::get('value', TRUE)), parent::get('type', TRUE), (int) parent::get('ignoreId', TRUE))
        ]);
    }

    /**
     * Determines if unique phone get.
     */
    public function is_unique_phone_get()
    {

        parent::response([
            'code' => parent::HTTP_OK,
            'data' => $this->customer_m->phone_uniquecheck(trim(parent::get('value', TRUE)), parent::get('type', TRUE), (int) parent::get('ignoreId', TRUE))
        ]);
    }

    /**
     * Determines if unique webpage.
     */
    public function is_unique_webpage_get()
    {
        $this->load->library('form_validation');
        $webpage = parent::get('value', true);
        $webpageId = (int) parent::get('id', true);

        if(empty($webpage) || ! $this->form_validation->valid_url(filter_var($webpage, FILTER_SANITIZE_URL))) parent::response([ 'code' => parent::HTTP_OK, 'data' => false ]);

        $this->load->model('customer/website_m');
        $valid_website = $this->website_m->domain_check($webpage, $webpageId);
        if( ! $valid_website) parent::response([ 'code' => parent::HTTP_OK, 'data' => false ]);

        parent::response([ 'code' => parent::HTTP_OK, 'data' => true ]);
    }
}
/* End of file Index.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Index.php */