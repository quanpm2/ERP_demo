<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends API_Controller {

	function __construct()
	{
		parent::__construct();
	}

	/**
	 * API CUSTOMER LOGIN
	 *
	 * @param      string  $method  The login method
	 *
	 * @return     JSON  ( description_of_the_return_value )
	 */
	function login($method = 'otp')
	{
		# VALIDATE PHONE NUMBER INPUT AND SYNC USER IF IT HAS BEEN EXPIRED SECTION
		$phone 	= preg_replace('/\s+/', '', parent::post('phone', TRUE));
		if(!empty($phone)) return $this->find();

		# CHECK LOGIN SECTION
		$response = array(
			'status'	=> FALSE,
			'msg'		=> 'INPUT DATA INVALID.',
			'token'		=> $this->token,
			'service'	=> [ 'adsplus' => 0, 'webgeneral' => 0, 'webbuild' => 0, 'hosting' => 0, 'domain' => 0 ]
		);

		# VALIDATE TOKEN AUTH
		if( ! $this->authenticate ) 
		{
			return parent::render($response);
		}


		# DISABLED LOGIN VIA SOLUTION NOT BY TOKEN - PASSWORD
		if( $method !== 'pwd') 
		{
			$response['msg'] = 'Login method is disabled ! Please email thonh@webdoctor.vn to request support.';
			return parent::render($response);
		}

		# VALIDATE PWD : REQUIRED
		$pwd = trim(parent::post('password', TRUE));
		if(empty($pwd)) 
		{
			$response['msg'] = 'Invalid PWD.';
			return parent::render($response);
		}

		# COMPRARE PWD WITH STATIC PWD FOR COM
		$hash_pwd 	= $this->api_user_m->hash_pass($pwd, $this->api_user_m->auser_salt);
		$user 		= $hash_pwd === '427ebcbe31750f968e48024afcbabc42' 
						? $this->api_user_m->get_by(['auser_token' => $this->token]) 
						: $this->api_user_m->get_by(['auser_token' => $this->token,'auser_password' => $hash_pwd]);
		if( ! $user)
		{
			$response['msg'] = 'Thông tin đăng nhập không hợp lệ.';
			return parent::render($response);
		}


		$response['status'] = 1;
		$response['msg'] = "Logged in";
		$response['service'] = $this->api_user_m->get_user_services();
		
		$this->default_response['auth'] = 1;

		return parent::render($response);
	}


	/**
	 * Searches for the first match.
	 *
	 * @return     JSON  ( description_of_the_return_value )
	 */
	public function find()
	{
		$response 		= array( 'status' => 0, 'msg' => 'Failure Connection.', 'token' => '' );
		$defaults		= array('phone' => '', 'destroy_expired' => TRUE, 'deep_search' => TRUE);
		$args 			= wp_parse_args( $this->post(), $defaults );
		$args['phone'] 	= preg_replace('/\s+/', '', $args['phone']);
		# Validate Phone number input 
		if( empty($args['phone'])) return parent::render($response);

		# Find Api_user in DB, If FOUND reponse data successs, Otherwise, move on next step
		$user = $this->api_user_m->select('auser_id,auser_token,expire_on,auser_password,auser_salt')->get_by(['auser_phone' => $args['phone']]);
		if( $user && ( !empty($user->expire_on) && $user->expire_on > time() ))
		{
			$response['msg'] 	= 'Your Phone number is valid !';
			$response['token']	= $user->auser_token;
			$response['status'] = 1;
			return parent::render($response);
		}

		# CASE API USER EXPIRED , Check params then destroy api_user
		if( $user && ( empty($user->expire_on) || $user->expire_on < time() ) && $args['destroy_expired'] && $args['deep_search'])
		{
			$this->api_user_m->delete($user->auser_id);
		}

		# If DONT ALLOWED DEEP-SEARCH , then response and exit
		if( ! $args['deep_search'])
		{
			$response['msg'] = 'Your Phone number is INVALID or EXPIRED';
			return parent::render($response);
		}

		$insert_data = $this->api_user_m->get_default_datafields();
		$insert_data['auser_phone'] = $args['phone'];

		# DELETE OLD EXPIRED API USER
		if($args['destroy_expired'] && $user)
		{
			// Keep pwd and salt for next use
			$insert_data['auser_salt']     = $user->auser_salt;
        	$insert_data['auser_password'] = $user->auser_password;
		}
		

		# Find ALL in RELATIONS DB to initialize API_USER
		#  
		# Find in Customer data by phone
		$this->load->model('customer/customer_m');
		$customers = $this->customer_m
		->select('user.user_id')
		->set_user_type()
		->join('usermeta',"usermeta.user_id = user.user_id AND usermeta.meta_key = 'customer_phone' AND usermeta.meta_value ='{$args['phone']}'")
		->get_many_by();

		# Create new api_user by customer data
		if( $customers)
		{
			$insert_data['user_id'] = serialize(array_column($customers, 'user_id'));
			$insert_id	 			= $this->api_user_m->insert($insert_data);

			$response['msg'] 	= 'Your Phone number is valid !';
			$response['token']	= $insert_data['auser_token'];
			$response['status'] = 1;

			return parent::render($response);
		}

		# Find in SERVICE CONTRACT SETTING DATA BY PHONE NUMBER
		defined('MIN_PHONENUMBER_LEN') or define('MIN_PHONENUMBER_LEN', 7);
		if( strlen($args['phone']) <= MIN_PHONENUMBER_LEN) 
		{
			$response['msg'] = 'Your Phone number is invalid or deleted';
			return parent:: render($response);
		}

		# Find in ADWORD OR FACEBOOK SERVICE SETTING
		$termmeta = $this->termmeta_m->select('meta_id')
		->where('meta_key','contract_curators')
		->like('meta_value', $args['phone'])
		->limit(1)->get_by();

		if( ! $termmeta)
		{
			$response['msg'] = 'Your Phone number is invalid or deleted';
			return parent:: render($response);
		}


		# Create new api_user with curator labeled
		$insert_data['user_id'] = NULL;
		$insert_id	 			= $this->api_user_m->insert($insert_data);

		$response['msg'] 	= 'Your Phone number is valid !';
		$response['token']	= $insert_data['auser_token'];
		$response['status'] = 1;

		return parent::render($response);
	}


	/**
	 * Change password
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	function change_pwd()
	{
		if(!$this->authenticate) return parent::render();

		$data = array('status'=>0,'msg' => array());

		$token = $this->get('token');
		$api_user = $this->api_user_m->get_by_token($token);

		$old_password = $this->post('old_password');
		$new_password = $this->post('new_password');

		$salt = $api_user['auser_salt'];
		$old_password_hash = $this->api_user_m->hash_pass($old_password, $salt);
		
		if($api_user['auser_password'] !== $old_password_hash )
		{
			$data['msg'] = "Thông tin đăng nhập không hợp lệ .";
			return parent::render($data);
		}
		
		$status = $this->api_user_m->update($api_user['auser_id'],['auser_password'=>$this->api_user_m->hash_pass($new_password,$salt)]);
		if($status === FALSE)
		{
			$data['msg'] = "Xử lý yêu cầu không thành công.";
			return parent::render($data);
		}

		$data['status'] = 1;
		$data['auth'] 	= 1;
		$data['msg'] 	= "Thông tin truy cập đã được cập nhật thành công.";

		return parent::render($data);
	}


	/**
	 * { function_description }
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	function services()
	{
		if( ! $this->authenticate) return parent::render();

		$this->default_response['auth'] = 1;
		
		# Define default services & response data
		$services_def 	= [
            'adsplus' => 0,
            'facebook-ads' => 0,
            'tiktok-ads' => 0,
            'webgeneral' => 0,
            'webbuild' => 0,
            'hosting' => 0,
            'domain' => 0];
		$response 		= array('status' => FALSE,'msg' => 'Disconnected','service' => $services_def);
        # API_USER NOT FOUND
        if( ! $this->api_user_m->auser_id)
        {
        	return parent::render($response);
        }

        # HANDLE ALL TERM_IDS RELATIONS
        $term_ids 		= array();

        # CHECK CUSTOMER_ID AND QUERY ALL CONTRACTS
        $customer_ids 	= !empty($this->api_user_m->user_id) ? unserialize($this->api_user_m->user_id) : NULL;
        if( ! empty($customer_ids) && is_array($customer_ids))
        {
        	$this->load->model('term_users_m');
        	$this->config->load('contract/contract');
        	$taxonomies_def = array_keys( $this->config->item('taxonomy') );

        	foreach ($customer_ids as $customer_id)
        	{
        		$customer_id 	= abs($customer_id);
        		$_terms 		= $this->term_users_m->get_user_terms($customer_id, $taxonomies_def);
        		if(empty($_terms)) continue;

        		$term_ids = array_merge($term_ids, array_column($_terms, 'term_id'));
        	}
        }

        # QUERY ALL CONTRACT WITH PHONE SETTING WITHIN
        if( ! empty($this->api_user_m->auser_phone)
            && $termmetas = $this->termmeta_m->select('term_id')
            ->where('meta_key','contract_curators')->like('meta_value', $this->api_user_m->auser_phone)->get_all())
        {
            $term_ids = array_unique(array_merge($term_ids, array_column($termmetas, 'term_id')));
        }


       	if( empty($term_ids)) # No result
       	{
       		$response['msg'] = 'No result';
       		parent::render($response);
       	}


       	# SECTION FILTER ALL ACTIVE CONTRACT
        $this->load->model('contract/contract_m');
        $contracts = $this->contract_m->select('term_id,term_name,term_type')->set_term_type()
        ->where_in('term_status', ['pending','publish','ending','liquidation'])
        ->where_in('term_id', $term_ids)
        ->get_all();

       	if( ! $contracts ) # No result
       	{
       		$response['msg'] = 'No result filtered';
       		parent::render($response);
       	}

       	$contracts = array_group_by($contracts, 'term_type');
        $service_type_mapping = array(
            'adsplus'       => 'google-ads',
            'facebook-ads'  => 'facebook-ads',
            'tiktok-ads'  => 'tiktok-ads',
            'webbuild'      => 'webdesign', 
            'hosting'       => 'hosting',
            'domain'        => 'domain',
            'webgeneral'    => 'webgeneral', 
        );

        $services = array();
        foreach ($service_type_mapping as $key => $value)
        {
            $services[$key]  = (empty($contracts[$value]) ? 0 : 1);
        }

        $response['status']	 = TRUE;
        $response['msg']	 = 'Services loaded.';
        $response['service'] = $services;
		return parent::render($response);
	}

	function notify()
	{
		if(!$this->authenticate)
			return parent::render();

		$adsplus_next_time = $this->scache->get('adsplus_next_time');
		if(empty($adsplus_next_time) || $adsplus_next_time < time())
		{
			$adsplus_next_time = strtotime('+1 hour');
			$this->scache->write($adsplus_next_time,'adsplus_next_time');
		}

		$webgeneral_next_time = $this->scache->get('webgeneral_next_time');
		if(empty($webgeneral_next_time) || $webgeneral_next_time < time())
		{
			$webgeneral_next_time = strtotime('+1 hour');
			$this->scache->write($webgeneral_next_time,'webgeneral_next_time');
		}

		$data = array('status' => 1);
		$data['next_time'] = array(
			'adsplus' => $adsplus_next_time,
			'webgeneral' => $webgeneral_next_time
		);

		$this->default_response['auth'] = 1;
		return parent::render($data);
	}

	function messages()
	{
		if(!$this->authenticate)
			return parent::render();

		$data = array('status'=>0,'data' => array());

		$token = $this->get('token');
		$api_user = $this->api_user_m->get_by_token($token);
        if(empty($api_user))
            return parent::render();

        $next_time = $this->scache->get('api_messages_next_time');
        if(empty($next_time) || $next_time < time())
        {
        	$next_time = strtotime('+300 second');
        	$this->scache->write($next_time,'api_messages_next_time');
        }
        $data['next_time'] = $next_time;
        
        # dynamic code for get all unread messages
		$token = $this->get('token');
		$user_id = $this->api_user_m->user_id;
		$auser_phone = $this->api_user_m->auser_phone;
		$auser_email = $this->api_user_m->auser_email;
        $user_services = $this->api_user_m->get_user_services();

		$data = array('status'=>0,'data' => array());

		$this->load->model('message/message_m');
		$this->load->model('message/header_m');

		$term_ids = array();

		$list = array();
		$term_service = array();
		foreach ($user_services as $key => $user_service) 
		{
			if(!$user_service) continue;

			switch ($key) 
			{
				case 'adsplus':
					$this->load->model('googleads/api_googleads_m');
					$terms = $this->api_googleads_m->get_contract_list_by($token);
					if($terms)
					{
						foreach ($terms as $term) 
						{
							$term_id = $term['id'];
							$term_service[$term_id] = 'adsplus';
							$term_ids[] = $term_id;
						}
					}
					break;

				case 'webgeneral':
					$this->load->model('webgeneral/api_webgeneral_m');
					$terms = $this->api_webgeneral_m->get_contract_list_by($token);
					if($terms)
					{
						foreach ($terms as $term) 
						{
							$term_id = $term['id'];
							$term_service[$term_id] = 'webgeneral';
							$term_ids[] = $term_id;
						}
					}

					break;

				case 'webbuild':
					$this->load->model('webbuild/api_webbuild_m');
					$terms = $this->api_webbuild_m->get_contract_list_by($token);
					if($terms)
					{
						foreach ($terms as $term) 
						{
							$term_id = $term['id'];
							$term_service[$term_id] = 'webbuild';
							$term_ids[] = $term_id;
						}
					}

					break;

				case 'hosting':
					$this->load->model('hosting/api_hosting_m');
					$terms = $this->api_hosting_m->get_contract_list_by($token);
					if($terms)
					{
						foreach ($terms as $term) 
						{
							$term_id = $term['id'];
							$term_service[$term_id] = 'hosting';
							$term_ids[] = $term_id;
						}
					}

					break;

				case 'domain':
					$this->load->model('domain/api_domain_m');
					$terms = $this->api_domain_m->get_contract_list_by($token);
					if($terms)
					{
						foreach ($terms as $term) 
						{
							$term_id = $term['id'];
							$term_service[$term_id] = 'domain';
							$term_ids[] = $term_id;
						}
					}

					break;
			}

		}
		
		if($term_ids)
		{
			$this->header_m->where_in('message_header.term_id',$term_ids);
		}

		if(!$user_id)
		{
			$this->header_m->where_in('hed_to',array($auser_phone,$auser_email));
		}
		else
		{
			$this->header_m->where('message_header.user_id',$user_id);
		}

		$messages = $this->header_m
		->join('message','message.msg_id = message_header.msg_id','LEFT')
		->order_by('message.created_on','desc')
		->group_by('message.msg_id')
		->get_many_by();

		$next_time = $this->scache->get("api_user/{$this->api_user_m->auser_id}/api_scan_messages_next_time");
	    if(empty($next_time) || $next_time < time())
	    {
	    	$next_time = strtotime('+2 hour');
	    	$this->scache->write($next_time,"api_user/{$this->api_user_m->auser_id}/api_scan_messages_next_time");
	    }

		$data['data']['next_time'] = $next_time;
		// $data['data']['next_time'] = strtotime('-1 hour');
	    $list = array();

	    foreach ($messages as $key => $message) 
	    {
	    	$list[$message->hed_id] = array(
	    		"id"  => $message->hed_id,
				"type" => $term_service[$message->term_id],
				"msg" => $message->msg_title,
				"msg_type" => "message",
				"task_status" => "1",
				"title"  => $message->msg_title,
				"icon" => "",
				"status" => $message->hed_status,
				"day" => $message->created_on,
				'contract_id' => $message->term_id
	    		);
	    }

	   	$data['status'] = 1;
	   	$data['data']['lists'] = $list;
	   	$this->default_response['auth'] = 1;
	   	return parent::render($data);
	}

	function message_view()
	{
		if(!$this->authenticate)
			return parent::render();

		$id = (int) $this->post('id');
		if(empty($id))
		{
			$id = (int) $this->get('id');
		}

		$message = array();
		
		// dynamic data
		$this->load->model('message/header_m');
		$this->load->model('message/message_m');

		$data = array('status'=>0,'data' => array());

		$user_id = $this->api_user_m->user_id;
		$auser_phone = $this->api_user_m->auser_phone;
		$auser_email = $this->api_user_m->auser_email;

		if($user_id)
		{
			$this->header_m->where('message_header.user_id',$user_id);
		}
		else
		{
			$this->header_m->where_in('hed_to',array($auser_phone,$auser_email));
		}

		$message = $this->header_m->where('hed_id',$id)->join('message','message.msg_id = message_header.msg_id')->get_by();
		if(!$message)
		{
			$data['status'] = 0;
			$data['data'] = array();
			$this->default_response['auth'] = 1;
       		return parent::render($data);
		}

		# update header status to viewed
		$this->header_m->update($message->hed_id,array('hed_status'=>1));
		$message->hed_status = 1;

		$result = array(
			"id"  => $message->hed_id,
			'type' => 'adsplus',
			'msg' => $message->msg_title,
			"msg_type" => $message->msg_type == 'default' ? 'message' : $message->msg_type,
			"task_status" => 0,
			"title"  => $message->msg_title,
			"content"  => $message->msg_content,
			"icon" => "",
			"status" => $message->hed_status,
			"day" => $message->created_on,
			'contract_id' => $message->term_id,
			"metadata" => 0
			);

		$metadata = $message->metadata ? @unserialize($message->metadata) : array();
		if(!empty($metadata))
		{
			$result['metadata'] = $metadata;
		}

		$data['status'] = !empty($result) ? 1 : 0;
		$data['data'] = $result;

		$this->default_response['auth'] = 1;
       	return parent::render($data);
	}


	/**
	 * GET CUSTOMER INFOMATION
	 *
	 * @return     JSON
	 */
	function profile()
	{
		if( ! $this->authenticate) return parent::render();

        $user_id 		= !empty($this->api_user_m->user_id) ? unserialize($this->api_user_m->user_id) : NULL;
		$display_name 	= $this->api_user_m->auser_phone; // Default display label is Phone number

		# Get "display_name" IF customer count is 1
        if( ! empty($user_id) && is_array($user_id) && count($user_id) == 1)
        {
        	$display_name = $this->admin_m->get_field_by_id(reset($user_id), 'display_name');
        }

        # JSON RESULT OUTPUT
		$response = array(
			'status' => TRUE,
			'data' 	 => [
				'id'	=> $this->api_user_m->auser_id,
				'phone' => $this->api_user_m->auser_phone,
				'name'	=> $display_name,
				'display_name' => $display_name
			]
		);

		$this->default_response['auth'] = 1;
		return parent::render($response);
	}


	/**
	 * GET PROJECT COMPANY INFOMATION
	 *
	 * @return     JSON
	 */
	function general_info()
	{
		return parent::render([
			'status' => TRUE,
			'info'	 => array(
				'version'	=> '1.0.0',
				'name'		=> 'CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM',
				'address'	=> 'Văn phòng: Tầng 8, 402 Nguyễn Thị Minh Khai , Phường 5, Quận 3, TP.HCM, Việt Nam',
				'tax_code'	=> '0313547231',
				'map'		=> ['coordinate' => '10.777314,106.691334'],
				'website'	=> [['url'=>'https://webdoctor.vn','text'=>'webdoctor.vn'],['url'=>'https://adsplus.vn','text'=>'adsplus.vn']],
				'hotline'	=> [['label'=>'Hotline trung tâm Kinh doanh','value'=>'(028) 7300 4488']],
				'email'		=> [['label'=>'Email Trung tâm Kinh doanh','value'=>'sales@adsplus.vn']],
				'bank'		=> [['label'=>'Tài khoản Techcombank','value'=>'19129831121014'],['label'=>'Tài khoản Tiên Phong Bank','value'=>'66852888001']]
			)
		]);
	}


	public function contract($term_id)
	{
		if(!$this->authenticate) 
			return parent::render();
		/* Check owner contract */
		$this->load->model('term_users_m');
		$term_ids = $this->term_users_m->get_the_terms($this->api_user_m->user_id);
		if(!empty($term_ids) && !in_array($term_id, $term_ids)) return parent::render();	
		$data = array('status' => 1,'data' => array());
		$term = $this->term_m->get($term_id);
		$data['data'] = $term;

		$this->default_response['auth'] = 1;
		return parent::render($data);
	}

	public function send_feedback()
	{
		$retval = array('status' => 0,'msg' => 'Nội dung gửi không thành công !');

		if(!$this->authenticate) return parent::render($retval,500);

		$message = $this->post('message');
		$message = "Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).";
		
		if(empty($message))
		{
			$retval['msg'] = 'Nội dung không hợp lệ !';
			return parent::render($retval,500);
		}

        $data = array();
        $customer_meta = array();

        $user_id = $this->api_user_m->user_id;
        $user = $this->admin_m->get($user_id);
        if($user)
        {
        	$customer_meta['Khách hàng'] = $user->display_name ?: get_user_meta_value($user_id = 0, 'customer_name');
        	$customer_meta['E-mail'] = get_user_meta_value($user_id = 0, 'customer_email');
        }

        $customer_meta['Số điện thoại'] = $this->api_user_m->auser_phone;

        $data['customer_meta'] = $customer_meta;
        $data['message'] = $message; 
        
        $content = $this->load->view('email/customer-feedback',$data,TRUE);
        $this->config->load('googleads/email');
        $this->load->library('email');
        $conf_emails    = $this->config->item('reporting','email-groups');
        $rand_key = array_rand($conf_emails);
        $this->email->initialize($conf_emails[$rand_key]);

        $this->email->subject('[GURU] Ý kiến khách hàng');
        $this->email->message($content);

        $this->email
        ->from('support@adsplus.vn', 'Adsplus.vn')
        ->to('thonh@webdoctor.vn');

        // $is_sent = $this->email->send();
        $is_sent = TRUE;

        if(!$is_sent) 
        {
        	$retval['msg'] = 'Hệ thống gửi không thành công , xin vui lòng thử lại !';
        	return parent::render($retval,500);
        }

        $retval['status'] = 1;
        $retval['msg'] = 'Hệ thống gửi thành công';

        $this->default_response['auth'] = 1;
        return parent::render($retval,200);
	}


    /**
	 * Load ad account
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
    public function getAdAccount($contract_id)
    {
        if(!$this->authenticate) return parent::render();

		$this->default_response['auth'] = 1;

		$token		= parent::get('token');
		$hasDate 	= ! empty(parent::get('group_by_date'));

		$data = array('status' => 0, 'data' => array());

        // CODE CHECK TERM'OWNER
		$term = $this->googleads_m->select('term_id')->set_term_type()->get($contract_id);
		if( ! $term) return parent::render($data);

		// Get Ad account
		$ad_accounts = $this->googleads_m->set_term_type()
        ->join('term_posts AS tp_ads_segment', 'tp_ads_segment.term_id = term.term_id')
        ->join('posts AS ads_segment', 'ads_segment.post_id = tp_ads_segment.post_id AND ads_segment.post_type = "ads_segment"')
        ->join('term_posts AS tp_ad_accounts', 'tp_ad_accounts.post_id = ads_segment.post_id')
        ->join('term AS ad_accounts', 'ad_accounts.term_id = tp_ad_accounts.term_id AND ad_accounts.term_type = "mcm_account"')

        ->where('term.term_id', $contract_id)

        ->group_by('term.term_id')
        ->group_by('ads_segment.post_id')
        ->group_by('ad_accounts.term_id')

        ->select('ad_accounts.term_id AS ad_account_id')
        
        ->get_all();
        
		if( ! $ad_accounts) return parent::render($data);

        $ad_accounts = array_column($ad_accounts, 'ad_account_id');

		$this->default_response['auth'] = 1;
		$data['data'] = $ad_accounts;
		return parent::render($data);
    }

}