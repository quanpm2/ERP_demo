<div class="row">
	<div class="col-md-12">
		<?php 
		echo 
		$this->admin_form->formGroup_begin(0,'Tên khách hàng'),
		'<div class="col-xs-2" style="padding-left: 0;"> ',
		form_dropdown(array('name'=>'edit[meta][customer_gender]','class'=>'form-control'),array(1=>'Ông',0=>'Bà'),
			$this->usermeta_m->get_meta_value(@$edit->user_id,'customer_gender')),
		'</div>',
		'<div class="col-xs-10">',
			form_input(array('name'=>'edit[display_name]','class'=>'form-control'), @$edit->display_name),
		'</div>',
		$this->admin_form->formGroup_end(),
		$this->admin_form->input('Địa chỉ email','edit[meta][customer_email]',
			$this->usermeta_m->get_meta_value(@$edit->user_id,'customer_email')),
		$this->admin_form->input('Địa chỉ liên hệ','edit[meta][customer_address]',
			$this->usermeta_m->get_meta_value(@$edit->user_id,'customer_address')),
		$this->admin_form->input('Điện thoại di động','edit[meta][customer_phone]',
			$this->usermeta_m->get_meta_value(@$edit->user_id,'customer_phone')),
		$this->admin_form->hidden('','edit[user_type]',  $user_type);	
		?>
	</div>
</div>
