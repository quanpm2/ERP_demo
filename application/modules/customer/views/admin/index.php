<?php
if(has_permission('Admin.Customer.add')) : 
    $this->template->javascript->add(base_url('dist/vCustomerCrud.js'));
?>
<div class="col-md-12" id="app-container-crud-container">
    <v-customer-crud-container/>
</div>    
<?php endif; ?>

<?php
if(!empty($content)){
    
    $this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');

    $this->template->javascript->add('plugins/daterangepicker/moment.min.js');

    $this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

    echo $content['table'] . $content['pagination'];
}
?>
<script type="text/javascript">

$(function(){
    $('[data-toggle=confirmation]').confirmation();
    $(".input_daterange").daterangepicker({
        format: 'DD-MM-YYYY',
    });
});
</script>