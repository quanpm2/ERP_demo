<div class="row">
	<div class="col-md-12">
		<?php 
		echo 
		$this->admin_form->input('Tên tổ chức', 'edit[display_name]', @$edit->display_name),
		$this->admin_form->input('Địa chỉ','edit[meta][customer_address]', $this->usermeta_m->get_meta_value(@$edit->user_id,'customer_address')),
		$this->admin_form->formGroup_begin(0,'Người liên hệ'),
		'<div class="col-xs-2" style="padding-left: 0;">',
			form_dropdown(array('name'=>'edit[meta][customer_gender]','class'=>'form-control'),array(1=>'Ông',0=>'Bà'),
				$this->usermeta_m->get_meta_value(@$edit->user_id,'customer_gender')),
		'</div>',
		'<div class="col-xs-10">',
			form_input(array('name'=>'edit[meta][customer_name]','class'=>'form-control'), 
				$this->usermeta_m->get_meta_value(@$edit->user_id,'customer_name')),
		'</div>',
		$this->admin_form->formGroup_end(),
		$this->admin_form->input('Email người liên hệ','edit[meta][customer_email]',
			$this->usermeta_m->get_meta_value(@$edit->user_id,'customer_email')),
		$this->admin_form->input('Điện thoại người liên hệ','edit[meta][customer_phone]',
			$this->usermeta_m->get_meta_value(@$edit->user_id,'customer_phone')),
		$this->admin_form->input('Mã số thuế','edit[meta][customer_tax]',
			$this->usermeta_m->get_meta_value(@$edit->user_id,'customer_tax')),
		$this->admin_form->hidden('','edit[user_type]',  $user_type);
		?>
	</div>
</div>
