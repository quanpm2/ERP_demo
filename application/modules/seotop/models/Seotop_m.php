<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seotop_m extends Term_m {

	public $term_type = 'seo-top';
	public $meta;

	function __construct(){

		parent::__construct();
		$this->meta = $this->termmeta_m;
	}

	public function set_term_type()
	{
		return $this->where('term_type',$this->term_type);
	}

	function has_permission($term_id = 0, $name = '', $action = '')
	{		

		$permission = $name.'.'.$action;
		$permission = trim($permission, '.');

		//Webgeneral.XYZ.Manage toàn quyền
		if(has_permission($name.'.Manage') && has_permission($permission))
			return true;

		if(!has_permission($permission))
			return false;


		// if(in_array($this->admin_m->role_id, array(1,3)))
			// return true;

		// $kpis = $this->webgeneral_kpi_m->get_kpis($term_id, 'users', 'content', time(), $this->admin_m->id);
		// return (bool)$kpis;
	}
}

/* End of file Seotop_m.php */
/* Location: ./application/modules/googleads/models/Seotop_m.php */