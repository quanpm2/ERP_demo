<?php
class Seotop_Package extends Package
{
	function __construct()
	{
		parent::__construct();
		$this->website_id  = $this->uri->segment(4);
	}

	public function name()
	{
		return 'SEOTOP';
	}

	public function init()
	{
		$this->_load_menu();
	}

	private function _load_menu()
	{
		if(!has_permission('Admin.Seotop')) return FALSE;

		$order = 0;
		$this->menu->add_item(array(
			'id' => 'seotop',
			'name' => 'SEO top',
			'parent' => null,
			'slug' => admin_url('seotop'),
			'order' =>$order++,
			'is_active' => is_module('seotop')
			),'navbar');

		if(!is_module('seotop')) return FALSE;
		
		$term_id = $this->uri->segment(4);

		if(empty($term_id)) return FALSE;

		$this->menu->add_item(array(
			'id' => 'seotop-kpi',
			'name' => 'KPI',
			'parent' => NULL,
			'slug' => module_url("kpi/{$term_id}"),
			'order' => $order++,
			'icon' => '	fa fa-fw fa-heartbeat'
			), 'left');
	}

	public function title()
	{
		return 'SEO top';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'SEO top';
	}
	
	private function init_permissions()
	{
		$permissions = array(

			'Seotop.Index' => array(
				'description' => 'Danh sách website đang chạy dịch vụ SEOTOP',
				'actions' => array('manage','access')
				),

			'Seotop.Kpi' => array(
				'description' => 'Trang quản lý Domain',
				'actions' => array('manage','access')
				),

			'Admin.Seotop' => array(
				'description' => '',
				'actions' => array('manage')
				),

			'Module.Seotop' => array(
				'description' => '',
				'actions' => array('manage')
				)
			);

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			// $this->permission_m->delete_by_name($name);
		}
	}
}