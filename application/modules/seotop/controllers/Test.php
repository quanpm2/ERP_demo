<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends Admin_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('unit_test');
		$this->load->model('seotraffic_m');
		$this->load->model('seotraffic_kpi_m');
		$this->load->model('seotraffic_mail_m');
	}

	public function index()
	{
		$this->test_date();
		echo $this->unit->report();
	}
	function copy()
	{
		$this->load->model('seotraffic_mail_m');
		$this->seotraffic_mail_m->copy(1477);
		
	}
	function test_date()
	{
		$date = '2015-12-01';
		$test_name = 'get_date_range';
		$time = strtotime($date);

		$test = $this->seotraffic_m->get_date_range(2, $time);
		$expected_result = '2015-12-08';
		$this->unit->run($test[0], $expected_result, $test_name);


		$expected_result = '2015-12-14';
		$this->unit->run($test[1], $expected_result, $test_name);


		$date = '2015-12-01';
		$test_name = 'get_next_week 2';
		$time = strtotime($date);
		$test = $this->seotraffic_mail_m->get_next_week($time);
		$expected_result = 2;
		$this->unit->run($test, $expected_result, $test_name);
		

		$date = '2015-12-08';
		$test_name = 'get_next_week 3';
		$time = strtotime($date);
		$test = $this->seotraffic_mail_m->get_next_week($time);
		$expected_result = 3;
		$this->unit->run($test, $expected_result, $test_name);
		
		$date = '2015-12-15';
		$test_name = 'get_next_week 4';
		$time = strtotime($date);
		$test = $this->seotraffic_mail_m->get_next_week($time);
		$expected_result = 4;
		$this->unit->run($test, $expected_result, $test_name);

		
		$date = '2015-12-22';
		$test_name = 'get_next_week 1';
		$time = strtotime($date);
		$test = $this->seotraffic_mail_m->get_next_week($time);
		$expected_result = 1;
		$this->unit->run($test, $expected_result, $test_name);



	}
}

/* End of file test.php */
/* Location: ./application/controllers/test.php */