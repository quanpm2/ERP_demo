<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fix extends Admin_Controller {

	public function index()
	{
		
	}

	function fix_encrypt_code()
	{
		$this->load->model('seotraffic_m');
		$terms = $this->term_m->get_many_by('term_type', 'seo-traffic');
		foreach($terms as $term)
		{
			$code = $this->termmeta_m->get_meta_value($term->term_id, 'report_key');
			$code = rawurldecode($code);
			$decrypted_termID = $this->seotraffic_m->encrypt($code,$term, 'decode');
			if($decrypted_termID != $term->term_id)
			{
				$new_code = $this->seotraffic_m->encrypt('',$term);
				$this->termmeta_m->update_meta($term->term_id, 'report_key',$new_code);
				echo $new_code . '<br><br>';
			}
		}
	}
	function sendmail_error()
	{
		$this->load->model('seotraffic_m');
		$this->load->model('seotraffic_mail_m');
		$terms = $this->term_m->get_many_by('term_type', 'seo-traffic');
		$time = strtotime('2016/02/13');
		foreach($terms as $term)
		{
			
		// echo $time ;
			$posts = $this->post_m->get_posts(array(
				'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,post_status',
				'tax_query'=> array(
					'taxonomy' => $this->seotraffic_m->term_type,
					'terms' => $term->term_id,
					),
				// 'where' => array(
				// 	// 'end_date <=' => $time
				// 	),
				'post_status' => 'publish',
			// 'meta_key' => 'webdoctor_mail_report',
				'post_type' => $this->seotraffic_mail_m->mail_type
				));

			if(!$posts)
				continue;
			foreach($posts as $post)
			{
				$this->post_m->update($post->post_id, array(
					'post_status' => 'sended'
					));
			}
			$post->start_date = strtotime('2016/02/01');
			$post->end_date = strtotime('2016/02/07');
			$post->end_date = $this->mdate->endOfDay($post->end_date);
			echo $this->seotraffic_mail_m->copy($post).'<br>';
			// prd($posts);
		}
	}
	function fix_start()
	{
		$posts = $this->post_m->where(array(
			'post_status'=>'publish',
			'post_type'=>'webdoctor-email'
			))->get_all();
		if($posts)
		{
			foreach($posts as $post)
			{
				echo date('d/m/Y',$post->start_date). ' - '.date('d/m/Y',$post->end_date).' ';
				echo $post->post_type.' ';
				echo $post->post_id.' ';
				echo '<br>';
				$start = strtotime('2016-01-11');
				$end = $this->mdate->endOfDay('2016-01-17');
				$this->post_m->update($post->post_id, array(
					'post_title' => '[BÁO CÁO {website_name_up}] HIỆN TRẠNG DOANH NGHIỆP TỪ NGÀY {start_date} ĐẾN NGÀY {end_date}',
					'start_date' =>$start,
					'post_excerpt' => '',
					'end_date' =>$end
					));
			}
		}
	}
}

/* End of file fix.php */
/* Location: ./application/controllers/fix.php */