<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seotop extends Admin_Controller {

	protected $model = 'seotop_m';

	public $term_type = 'seo-top';

	function __construct()
	{
		parent::__construct();
		$_models = array(
			'seotop/seotop_m',
			'backlink/base_kpi_m'
			);
		$this->load->model($_models);
	}

	public function index()
	{
		restrict('Seotop.Index');
		
		$this->search_filter();

		$this->admin_ui
			->add_column('term.term_name','Website')
			->add_search('term_name')

			->add_column('action', array('set_select'=>FALSE, 'title'=>'Actions', 'set_order'=>FALSE))
			->add_column_callback('action', function($data, $row_name){

				$html = '';
				$html.= anchor(module_url('kpi/'.$data['term_id']),'<i class="fa fa-fw fa-heartbeat"></i>','data-toggle="tooltip" title="Đặt KPI cho dịch vụ"');
				$data['action'] = $html;
				return $data;	
				}, FALSE)
			->select('term.term_id')
			->where('term_status','publish')
			->where('term.term_type', $this->term_type)
			->from('term')
			->group_by('term.term_id');

		$data['content'] = $this->admin_ui->generate(array('per_page' => 100,'uri_segment' => 4,'base_url'=> module_url('index/')));

		parent::render($data);
	}
	
	public function kpi($term_id = 0, $delete_id= 0)
	{
		if(!has_permission('Seotop.Kpi'))
		{
			$this->template->title->set('Lỗi không tồn tại.');
			return parent::render404('Bạn không có quyền hoặc Website không tồn tại');
		}
		
		$this->template->title->prepend('KPI');
		$this->load->config('webgeneral/group');
		$this->submit_kpi($term_id,$delete_id);

		$data['time'] = $this->mdate->startOfMonth(time());
		$time_start = get_term_meta_value($term_id, 'contract_begin');
		$time_end = get_term_meta_value($term_id, 'contract_end');

		$data['term_id'] = $term_id;
		$data['time_start'] = $time_start;
		$data['time_end'] = $time_end;
		$data['time_start'] = $this->mdate->startOfDay($data['time_start']);
		$data['time_end'] = $this->mdate->endOfDay($data['time_end']);

		$targets = $this
		->base_kpi_m
		->where('term_id',$term_id)
		->as_array()
		->order_by('kpi_datetime')
		->get_many_by();

		$data['targets'] = array();
		$data['targets']['content'] = array();
		$data['targets']['seotraffic'] = array();
		$data['targets']['backlink'] = array();
		$data['targets']['keyword'] = array();
		foreach($targets as $target)
		{
			$data['targets'][$target['kpi_type']][] = $target;
		}

		$data['date_contract'] = array();
		for($i=$data['time_start'];$i< $data['time_end']; $i++)
		{
			$data['date_contract'][$time_start]  = date('Y-m', $time_start);
			$date = $data['date_contract'][$time_start];

			$time_start = strtotime('+1 month', strtotime($date));
			if($time_start > $time_end)
				break;
		}

		$data['users'] = $this->admin_m->select('user_id,display_name')->where_in('role_id', $this->config->item('group_memberId'))->set_get_active()->order_by('display_name')->get_many_by();
		$data['users'] = key_value($data['users'],'user_id','display_name');

		$this->data = $data;
		parent::render($this->data);
	}

	public function submit_kpi($term_id = 0,$delete_id = 0)
	{
		if($delete_id > 0)
		{
			$this->_delete_kpi($term_id,$delete_id);
			redirect(module_url('kpi/'.$term_id.'/'),'refresh');
		}
		
		$post = $this->input->post();
		if(empty($post)) return FALSE;

		if($this->input->post('submit_kpi_backlink') !== NULL)
		{
			$kpi_datetime = $this->input->post('target_date');
			$user_id = $this->input->post('user_id');
			$kpi_value = (int)$this->input->post('target_post');
			$kpi_type = $this->input->post('target_type');

			Modules::run('backlink/update_kpi',$term_id,$kpi_datetime,$user_id,$kpi_value,$kpi_type);
		}

		redirect(current_url(),'refresh');
	}

	private function _delete_kpi($term_id = 0,$delete_id=0)
	{
		$_tmp_model = 'webgeneral_kpi_m';
		$is_backlink_kpi = $this->input->get('kpi_type') == 'backlink';
		if($is_backlink_kpi){
			$_tmp_model = 'backlink_kpi_m';
			$this->load->model('backlink/backlink_kpi_m');
		}

		$check = $this->{$_tmp_model}->get($delete_id);
		$is_deleted = false;
		if($check)
		{
			if(strtotime($check->kpi_datetime) >= $this->mdate->startOfMonth())
			{
				$this->{$_tmp_model}->delete_cache($delete_id);
				$this->{$_tmp_model}->delete($delete_id);
				$is_deleted = true;
				$this->messages->success('Xóa thành công');
			}
		}
		($is_deleted) OR $this->messages->error('Xóa không thành công');
	}
}
/* End of file Seotop.php */
/* Location: ./application/modules/seotop/controllers/Seotop.php */