<?php
class Phonebook_Package extends Package
{
	private $website_id;
	function __construct()
	{
		parent::__construct();
	}

	public function name(){
		
		return 'Phonebook';
	}

	public function init(){	
		// $this->_update_permissions();
		if(has_permission('Phonebook.Index') && is_module_active('phonebook'))
		{
			$this->website_id  = $this->uri->segment(4);
			$module_url = admin_url().'phonebook/';

			$this->menu->add_item(array(
				'id' => 'phonebook',
				'name' => 'Danh bạ khách hàng',
				'parent' => null,
				'slug' => $module_url,
				'order' => 5,
				'is_active' => is_module('phonebook'),
				),'navbar');

			if(is_module('phonebook'))
			{
				$this->menu->add_item(array(
					'id' => 'phonebook-index',
					'name' => 'Danh sách mới',
					'parent' => null,
					'slug' => $module_url.'',
					'order' => 1,
					'icon' => ''
					), 'left');
				$this->menu->add_item(array(
					'id' => 'phonebook-sticky',
					'name' => 'Danh sách đã lưu',
					'parent' => null,
					'slug' => $module_url.'sticky/',
					'order' => 2,
					'icon' => ''
					), 'left');

				$this->menu->add_item(array(
					'id' => 'phonebook-viewed',
					'name' => 'Danh sách đã xem',
					'parent' => null,
					'slug' => $module_url.'viewed/',
					'order' => 3,
					'icon' => ''
					), 'left');

				$this->menu->add_item(array(
					'id' => 'phonebook-statistic',
					'name' => 'Thống kê cuộc gọi',
					'parent' => null,
					'slug' => $module_url.'statistic/',
					'order' => 4,
					'icon' => ''
					), 'left');
			}
			
		}
	}

	private function _update_permissions()
	{
		$permissions = array();

		if(!permission_exists('Phonebook.statistic'))
			$permissions['Phonebook.statistic'] = array(
				'description' => 'Thống kê cuộc gọi',
				'actions' => array('manage'));

		if(!$permissions) return false;
		foreach($permissions as $name => $value){
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function title()
	{
		return 'Danh bạ khách hàng';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Danh bạ điện thoại khách hàng';
	}
	private function init_permissions()
	{
		$permissions = array();

		$permissions['Admin.Phonebook'] = array(
			'description' => '',
			'actions' => array('manage'));

		$permissions['Module.Phonebook'] = array(
			'description' => '',
			'actions' => array('manage'));

		$permissions['Phonebook.Index'] = array(
			'description' => 'Quản lý danh bạ khách hàng',
			'actions' => array('access'));

		$permissions['Phonebook.Sticky'] = array(
			'description' => 'Danh sách KH đã lưu',
			'actions' => array('access'));


		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);

			$this->role_permission_m
			->insert(array(
					'role_id' => 1, //admin role
					'permission_id' => $permission_id,
					'action' => serialize($actions)
					));
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}