<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Phonebook extends Admin_Controller {

	public $model = 'phonebook_m';

	function __construct()
	{
		parent::__construct();
		// ini_set('memory_limit','2048M');
		restrict('Phonebook.Index');
		$this->load->model($this->model);
		$this->load->model('phonebook_log_m');
	}
	
	public function index()
	{
		// $this->messages->error('Hệ thống tạm thời ngưng hoạt động để cập nhật lại dữ liệu.');
		// redirect(admin_url(),'refresh');

		if($this->input->post())
		{
			$this->submit();
		}
		$this->cron();

		$this->admin_ui->select('pbook_id');
		$i = 1;
		$this->admin_ui->add_column('stt', array('set_select'=>FALSE, 'title'=>'STT', 'set_order'=>FALSE))
		->add_column_callback('stt', function($data, $row_name){
			global $i;
			$data['stt'] = ++$i;
			return $data;	
		}, FALSE);

		$this->admin_ui->add_column('pbook_name','Tên');

		$this->admin_ui->add_column('assign_time','Cấp ngày');

		$this->admin_ui->add_column('action', array('set_select'=>FALSE, 'title'=>'Actions', 'set_order'=>FALSE))
		->add_column_callback('action', function($data, $row_name){
			$data['action'] = anchor(module_url('overview/'),'<button type="button" class="btn btn-block btn-danger btn-sm btn-ajax" data-toggle="modal" data-target="#myModal" data-id="'.$data['pbook_id'].'">Xem</button>');
			return $data;	
		}, FALSE);

		$this->admin_ui->from('phonebook');
		$this->admin_ui->where('pbook_status', 'publish');
		$this->admin_ui->where('user_id', $this->admin_m->id);
		$this->admin_ui->where('assign_time <=', date('Y-m-d'));
		$this->admin_ui->limit(500);

		$content = $this->admin_ui->generate();
		// $data['content'] = $content;

		$data['content'] = $content['table'];

		$data['rows'] = array(
			'name' =>'Tên công ty',
			'customer' =>'Tên chủ công ty',
			'addr' =>'Địa chỉ',
			'cate' =>'Ngành nghề',
			'mail' =>'Mail',
			'date_est' =>'Ngày thành lập',
			'phone' =>'Điện thoại'
			);
		parent::render($data, 'index');
		// echo $content['pagination'];
	}
	function viewed()
	{
		if($this->input->post())
		{
			$this->submit();
		}
		// $this->cron();

		$this->admin_ui->select('pbook_id');
		$i = 1;
		$this->admin_ui->add_column('stt', array('set_select'=>FALSE, 'title'=>'STT', 'set_order'=>FALSE))
		->add_column_callback('stt', function($data, $row_name){
			global $i;
			$data['stt'] = ++$i;
			return $data;	
		}, FALSE);

		$this->admin_ui->add_column('pbook_name','Tên');

		$this->admin_ui->add_column('assign_time','Cấp ngày');

		$this->admin_ui->add_column('action', array('set_select'=>FALSE, 'title'=>'Actions', 'set_order'=>FALSE))
		->add_column_callback('action', function($data, $row_name){
			$data['action'] = anchor(module_url('overview/'),'<button type="button" class="btn btn-block btn-danger btn-sm btn-ajax" data-toggle="modal" data-target="#myModal" data-id="'.$data['pbook_id'].'">Xem</button>');
			return $data;	
		}, FALSE);

		$this->admin_ui->from('phonebook');
		$this->admin_ui->where('pbook_status', 'viewed');
		$this->admin_ui->where('user_id', $this->admin_m->id);
		$this->admin_ui->where('assign_time <=', date('Y-m-d'));
		// $this->admin_ui->limit(10000);

		$content = $this->admin_ui->generate();
		$data['content'] = $content;

		$data['content'] = $content['table'];
		$data['content'].= $content['pagination'];

		$data['rows'] = array(
			'name' =>'Tên công ty',
			'customer' =>'Tên chủ công ty',
			'addr' =>'Địa chỉ',
			'cate' =>'Ngành nghề',
			'mail' =>'Mail',
			'date_est' =>'Ngày thành lập',
			'phone' =>'Điện thoại'
			);
		parent::render($data, 'index');
	}
	function listsale()
	{
		$data = array();
		$exts = array();

		for($i=2; $i<30; $i++)
		{
			$ext = str_pad($i, 2, '0', STR_PAD_LEFT);
			$exts[] = '6'.$ext;
			
		}
		for($i=1; $i<30; $i++)
		{
			$ext = str_pad($i, 2, '0', STR_PAD_LEFT);
			$exts[] = '8'.$ext;
			
		}

		
		parent::render($data, 'blank');
	}
	function callback($id = 0, $phone_numer = '')
	{
		$log_insert = array(
			'pbook_id' =>$id,
			'user_id' =>$this->admin_m->id,
			'log_title' => 'callback',
			'log_content' =>  '',
			'log_create_time' => date('Y-m-d H:i:s'));

		if($id == 'call')
		{
			if($phone_numer == '')
			{
				die('phone_numer is null');
				return ;
			}
			$log_insert['pbook_id'] = 0;
			$phone = $phone_numer;
		}
		else
		{
			$result = $this->phonebook_m->get($id);
			if(!$this->check_prv($result))
			{
				die('No pri');
				return ;
			}
			$phone = $result->pbook_phone;
		}
		
		// $phone = '0974681379';
		$ext = $this->usermeta_m->get_meta_value($this->admin_m->id,'phone-ext');
		if(empty($ext))
		{
			die('No ext');
			return ;
		}


		@file_get_contents('http://203.162.56.196/t.php?ext='.$ext.'&to='.$phone);
		$log_insert['log_content'] = ''.$phone.' - '.$ext;
		$this->phonebook_log_m->insert($log_insert);
		// die('http://203.162.56.196/t.php?ext='.$ext.'&to='.$phone);
	}


	function sticky()
	{
		if($this->input->post())
		{
			$this->submit();
		}
		// $this->cron();

		$this->admin_ui->select('pbook_id,pbook_district');
		$i = 1;
		$this->admin_ui->add_column('stt', array('set_select'=>FALSE, 'title'=>'STT', 'set_order'=>FALSE))
		->add_column_callback('stt', function($data, $row_name){
			global $i;
			$data['stt'] = ++$i;
			return $data;	
		}, FALSE);

		$this->admin_ui->add_column('pbook_name','Tên');

		$this->admin_ui->add_column('pbook_customer_name','Tên chủ công ty');
		$this->admin_ui->add_column('pbook_addr','Địa chỉ')
		->add_column_callback('pbook_addr', function($data, $row_name){
			$data['pbook_addr'] = $data['pbook_addr'].', '.$data['pbook_district'];
			return $data;	
		}, FALSE);
		$this->admin_ui->add_column('pbook_cate','Ngành nghề chính');
		$this->admin_ui->add_column('pbook_career','Ngành nghề phụ');
		$this->admin_ui->add_column('pbook_email','Email');
		$this->admin_ui->add_column('pbook_phone','Số điện thoại');

		$this->admin_ui->add_column('action', array('set_select'=>FALSE, 'title'=>'Actions', 'set_order'=>FALSE))
		->add_column_callback('action', function($data, $row_name){
			$data['action'] = anchor(module_url('overview/'),'<button type="button" class="btn btn-block btn-danger btn-sm btn-ajax" data-toggle="modal" data-target="#myModal" data-id="'.$data['pbook_id'].'">Xem</button>');
			return $data;	
		}, FALSE);

		$this->admin_ui->from('phonebook');
		$this->admin_ui->where('pbook_status', 'sticked');
		$this->admin_ui->where('user_id', $this->admin_m->id);
		$this->admin_ui->where('assign_time <=', date('Y-m-d'));
		$this->admin_ui->limit(10000);

		$content = $this->admin_ui->generate();
		$data['content'] = $content['table'];
		$data['rows'] = array(
			'name' =>'Tên công ty',
			'customer' =>'Tên chủ công ty',
			'addr' =>'Địa chỉ',
			'cate' =>'Ngành nghề',
			'mail' =>'Mail',
			'date_est' =>'Ngày thành lập',
			'phone' =>'Điện thoại'
			);
		parent::render($data, 'index');
	}

	public function cron()
	{
		$check = $this->phonebook_m->cache('phonebook/is-'.$this->admin_m->id.'-'.date('Y-m-d'))->get_by(array(
			'user_id' => $this->admin_m->id,
			'assign_time' => my_date()
			));

		if($check)
			return;
		if(!$total = $this->scache->get('phonebook/total-'.$this->admin_m->id.'-'.date('Y-m-d')))
		{
			$total = $this->phonebook_m->count_by(array(
				'user_id' => $this->admin_m->id,
				'pbook_status' => 'publish'
				));
			$this->scache->write($total, 'phonebook/total-'.$this->admin_m->id.'-'.date('Y-m-d'));
		}

		if($total > 300)
		{
			$this->messages->error('Dữ liệu của bạn hiện tại quá nhiều so với quy định nên không thể cấp phát thêm.');
			return;
		}

		// $this->messages->error('Hệ thống tạm thời ngưng cấp phát để cập nhật lại dữ liệu.');
		// return ;
		$this->phonebook_m->order_by('pbook_date_est','desc')->limit(50)->where(array(
			'user_id'=> 0,
			'is_mobile'=> 1
			))->update_all(array(
			'user_id'=> $this->admin_m->id,
			'pbook_status'=> 'publish',
			'assign_time'=> my_date(),
			));
			$affecs = $this->db->affected_rows();
			$this->messages->success('Đã thêm '.$affecs.' dữ liệu mới cho bạn.');
		}

		function renew()
		{
			$this->phonebook_m->update_all(array(
				'user_id'=> 0,
				'pbook_status'=> 'publish',
				'assign_time'=> null,
				));
			$affecs = $this->db->affected_rows();
			$this->messages->success('Cập nhật '.$affecs.' dòng.');
			$data = array();
			$data['content'] = 'Cập nhật '.$affecs.' dòng.';
			parent::render($data, 'blank');
		}
		private function submit()
		{
			$type =$this->input->post('submit-type');
			$id =$this->input->post('id');
			$update = array();
			if($type == 'delete')
			{
				$this->messages->success('Xóa thành công');
				$update['pbook_status'] = 'deleted';
			}
			else
			{
				$this->messages->success('Lưu thành công');
				$update['pbook_status'] = 'sticked';
			}

			$check = $this->phonebook_m->get($id);
			if($check)
			{
				$log_insert = array(
					'pbook_id' =>$id,
					'user_id' =>$this->admin_m->id,
					'log_title' => '',
					'log_content' => '',
					'log_create_time' => date('Y-m-d H:i:s'));

				if($check->user_id != $this->admin_m->id)
				{
					$log_insert['log_title'] = 'detect hacked';
					$log_insert['log_content'] = $check->user_id.' - '.$this->admin_m->id;
				}
				else
				{
					$this->phonebook_m->where('user_id', $this->admin_m->id)->update($id,$update);
					$log_insert['log_title'] = 'update status';
					$log_insert['log_content'] = $check->pbook_status.' - '.$update['pbook_status'];
					$this->scache->delete('phonebook/total-'.$this->admin_m->id.'-'.date('Y-m-d'));
				}
				$this->phonebook_log_m->insert($log_insert);
				redirect(current_url(),'refresh');
			}
		}
		function check_prv($phone = 0)
		{
			if(empty($phone))
				return false;
			return ($phone->user_id == $this->admin_m->id);

		}
		function get($id = 0, $type = '')
		{
			$r = $this->phonebook_m->get($id);
			if(!$r)
				return false;
			$log_insert = array(
				'pbook_id' =>$id,
				'user_id' =>$this->admin_m->id,
				'log_title' => 'Viewed',
				'log_content' => 'update status from '.$r->pbook_status.' to viewed',
				'log_create_time' => date('Y-m-d H:i:s'));

			if(!$this->check_prv($r))
			{
				$log_insert['log_title'] = 'detect hacked';
				$this->phonebook_log_m->insert($log_insert);
				return parent::renderJson(array());
			}

			$result = array();
			$result['id'] = $r->pbook_id;
			$result['name'] = $r->pbook_name;
			$result['customer'] = $r->pbook_customer_name;
			$result['addr'] = $r->pbook_addr.', '.$r->pbook_district;
			$result['cate'] = $r->pbook_cate;
			$result['mail'] = $r->pbook_email;
			$result['phone'] = $r->pbook_phone;
			$result['phone_action'] = $r->pbook_phone;
			$result['date_est'] = $r->pbook_date_est;

			if($type != 'view')
			{
				$this->phonebook_log_m->insert($log_insert);
				$this->phonebook_m->update($r->pbook_id, array('pbook_status'=>'viewed', 'viewed_time'=> date('Y-m-d H:i:s')));
				$this->scache->delete('phonebook/total-'.$this->admin_m->id.'-'.date('Y-m-d'));
			}
			parent::renderJson($result);
		}
		function phone_static()
		{
			$config = array();

			$config['hostname'] = '203.162.56.196';
			$config['username'] = 'cdr';
			$config['password'] = 'J8JKjDkoOr';
			$config['database'] = 'asteriskcdrdb';
			$config['dbdriver'] = 'mysqli';
			$db = $this->load->database( $config,true);

			$results = $db->limit(10)->get('cdr')->result();
        // var_dump($db);
			prd($results);

		}
		function check()
		{
			$this->benchmark->mark('code_start');
			$this->benchmark->mark('get_start');
			$results = $this->phonebook_m->get_many_by();
			$this->benchmark->mark('get_end');
			foreach($results as $i=>$result)
			{
				echo "\r".$i."";
			}
			echo "\r\n";
			$this->benchmark->mark('code_end');
			echo $this->benchmark->elapsed_time('get_start', 'get_end')."\n";
			echo $this->benchmark->elapsed_time('code_start', 'code_end')."\n";
		}
		function check2()
		{
			$this->benchmark->mark('code_start');
			$this->benchmark->mark('get_start');
			$results = $this->db->get('phonebook')->result();
			$this->benchmark->mark('get_end');
			$total = count($results);
			$this->benchmark->mark('for_start');
			for($i=0; $i< $total; $i++)
			{
				echo "\r".$i."";
			}
		// foreach($results as $i=>$result)
		// {
		// 	echo "\r".$i."";
		// }
			$this->benchmark->mark('for_end');
			echo "\r\n";
			$this->benchmark->mark('code_end');
			echo $this->benchmark->elapsed_time('get_start', 'get_end')."\n";
			echo $this->benchmark->elapsed_time('code_start', 'code_end')."\n";
			echo $this->benchmark->elapsed_time('for_start', 'for_end')."\n";
		}
		function import_update()
		{
			$file = './resource/file.xlsx';
		// $file = './resource/danhbadn_DT.xlsx';
			$this->load->library('excel');
			ini_set('memory_limit','2048M');
			$inputFileType = PHPExcel_IOFactory::identify($file);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType, false, 'UTF-8');
			$objPHPExcel = $objReader->load($file);
			$sheetData = $objPHPExcel->getActiveSheet()->toArray(true,true,true,true);

			$arr_desc_id = array();
			unset($sheetData[1]);
			$data_cache = array();
			$insert = array(); 

			foreach ($sheetData as $i=>$data) {
				if(!$data['B'] || !$data['A'])
					continue;
				$t = explode('-', $data['E']);
				if(empty($t[2]))
					continue;
				if($t[2] > 80)
					$t[2] = '19'.$t[2];
				else
					$t[2] = '20'.$t[2];
				$d = @$t[2].'/'.@$t[1].'/'.@$t[0];
				$insert['pbook_date_est'] = $d;

				$this->phonebook_m->where(array('pbook_name' =>$data['B'], 'pbook_mst'=>$data['A']))->update_by($insert);
				echo "\r".$i."";
			}
		}
		function import()
		{
			$file = './resource/file.xlsx';
		// $file = './resource/danhbadn_DT.xlsx';
			$this->load->library('excel');
			ini_set('memory_limit','2048M');
			$inputFileType = PHPExcel_IOFactory::identify($file);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType, false, 'UTF-8');
			$objPHPExcel = $objReader->load($file);
			$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

			$arr_desc_id = array();
			unset($sheetData[1]);
			$data_cache = array();
			$insert = array(); 
			foreach ($sheetData as $i=>$data) {
				$insert['pbook_mst'] = $data['A'];
				$insert['pbook_name'] = $data['B'];
				$insert['pbook_addr'] = $data['C'];
				$insert['pbook_district'] = $data['D'];
				$insert['pbook_date_est'] = $data['E'];
				$insert['pbook_cate'] = $data['F'];
				$insert['pbook_email'] = $data['H'];
				$insert['pbook_phone'] = $data['I'];
				$insert['pbook_career'] = $data['J'];
				$insert['pbook_money'] = $data['K'];
				$insert['pbook_customer_name'] = $data['G'];
				$this->phonebook_m->insert($insert);
			// $data_cache[$insert['pbook_mst']] = $insert;
				echo "\r".$i."";
			}
		}

		function statistic($month = '')
		{
			restrict('Phonebook.statistic');
			if(!$month)
			{
				$month = date('m');
			}
			$this->load->model('report_phonebook_m');
			$data = array();

			$time = time();
			$first_month = date('Y-m-01 00:00:00',$time);
			$end_month = date('Y-m-d 00:00:00',$time);

			// $first_month = '2016-09-01 00:00:00';
			// $end_month = '2016-10-01 00:00:00';

			if($this->input->get('s'))
			{
				$filter_date = $this->input->get('datefilter');
				$filter_date = urldecode($filter_date);
				$filter_date = explode(' ', $filter_date);
				$first_month = (isset($filter_date[0]) ? $filter_date[0].' 00:00:00' : '');
				$end_month = (isset($filter_date[1]) ? $filter_date[1].' 00:00:00' : '');
			}

			$key_cache = 'modules/phonebook/statistic-'.md5($first_month.$end_month);

			if(!$calleds = $this->scache->get($key_cache))
			{
				$end_month = $this->mdate->endOfDay($end_month);
				$end_month = date('Y-m-d H:i:s',$end_month);
				$results = $this->report_phonebook_m->get_called_result($first_month, $end_month);
				$calleds = $this->report_phonebook_m->build_tree_called($results);
				$this->scache->write($calleds,$key_cache, 3600);
			}

			$num = 3;
			
			$users = $this->report_phonebook_m->get_users();
			$i = 0;
			$data['charts'] = array();
			$datasets = array();

			foreach($users['users'] as $user_id =>$user)
			{
				$ext = ($users['usersExtArray'][$user_id])??0;
				$r = $this->generateChartData($calleds, $ext, $user);
				$data['charts'][$user_id]  = json_encode($r);
			}

			// $data['charts']  = json_encode($results);
			$data['users']  = $users;
			$dateFilter = $this->input->get('datefilter');
			$data['dateFilter'] = ($dateFilter ? urldecode($dateFilter) : '');
				// prd($data['charts']);
			parent::render($data);
		}

		function rgbcode($id){
			return '#'.substr(md5($id), 0, 6);
		}

		function generateChartData($results, $ext, $user)
		{

			$data = array();
			$data['labels'] = array();
			$datasets = array();
			$kpi = 50;
			$datasets['fill'] = true;
			$datasets['label'] = 'Backlink';
			$datasets['fillColor'] = 'rgba(220,220,220,0)';
			$datasets['strokeColor'] = '#00f';
			$datasets['pointColor'] = $datasets['strokeColor'];
			$datasets['pointStrokeColor'] = '#fff';
			$datasets['pointHighlightStroke'] = 'rgba(220,220,220,1)';
			$datasets['data'] = array();

			$total = $datasets;
			$answered = $datasets;
			$answered_kpi = $datasets;
			$no_answer = $datasets;
			$KPI = $datasets;

			$total['label'] = '00c0ef';
			$total['strokeColor'] = '#00c0ef';
			$total['pointColor'] = $total['strokeColor'];

			$answered['label'] = '00c0ef';
			$answered['strokeColor'] = '#00a65a';
			$answered['pointColor'] = $answered['strokeColor'];

			$answered_kpi['label'] = '00f';
			$answered_kpi['strokeColor'] = '#00f';
			$answered_kpi['pointColor'] = $answered_kpi['strokeColor'];

			$no_answer['label'] = 'f39c12';
			$no_answer['strokeColor'] = '#f39c12';
			$no_answer['pointColor'] = $no_answer['strokeColor'];


			$KPI['label'] = 'f00';
			$KPI['strokeColor'] = '#f00';
			$KPI['pointColor'] = $KPI['strokeColor'];

			$mKPI = ($user->role_id == 13) ? 30 : 45;
			$i = 0;
			foreach($results as $date => $result)
			{
				$data['labels'][] = date('d/m', strtotime($date));
				if(isset($result[$ext]))
				{
					$failed = (isset($result[$ext]['status']['FAILED']['total']) ? $result[$ext]['status']['FAILED']['total'] : 0);
					$total['data'][] = $result[$ext]['total'] - $failed;
					$answered['data'][] = ($result[$ext]['status']['ANSWERED']['total'])??0;
					$answered_kpi['data'][] = ($result[$ext]['callKPI'])??0;
					$no_answer['data'][] = ($result[$ext]['status']['BUSY']['total'])??0;
				}
				else
				{
					$total['data'][] = 0;
					$answered['data'][] = 0;
					$answered_kpi['data'][] = 0;
					$no_answer['data'][] = 0;
				}
				$i++;
				$KPI['data'][] = $mKPI;
			}

			// $data['datasets'][] = $total;
			$data['datasets'][] = $answered;
			$data['datasets'][] = $answered_kpi;
			// $data['datasets'][] = $no_answer;
			$data['datasets'][] = $KPI;

			return $data;

		}
	}