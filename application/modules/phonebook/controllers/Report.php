<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Public_Controller {

	public $model = 'phonebook_m';
	private $start_date = '';
	private $end_date = '';

	function __construct()
	{
		parent::__construct();
		// restrict('Phonebook.Index');
		$this->load->model($this->model);
		$this->load->model('phonebook_log_m');
		$this->load->model('report_phonebook_m');

		$this->load->config('mbusiness/mbusiness');
	}

	function index()
	{

	}

	function month($month = false)
	{
		if(!$month)
			$month = date('m');

		$date_from = date('Y-'.$month.'-01');
		$date_end = date('Y-'.$month.'-t');
		$results = $this->phonebook_m->where(array(
			'assign_time >=' => $date_from,
			'assign_time <=' => $date_end,
			'user_id >' => '0',
			))->order_by('assign_time')->get_many_by();

		$data = array();
		$users = array();
		foreach($results as $result)
		{
			$u_id = $result->user_id;

			if(!isset($users[$u_id]))
			{
				$users[$u_id] = array(
					'total'=>0,
					);
			}

			$users[$u_id]['total']++;
			$users[$u_id]['status'][$result->pbook_status] = (!isset($users[$u_id]['status'][$result->pbook_status])) ? 1 : $users[$u_id]['status'][$result->pbook_status] + 1;
		}

		$status_labels = array(
			'total' =>'Tổng',
			'publish' =>'Cấp mới',
			'viewed' =>'Đã xem',
			'sticked' =>'Đánh dấu',
			'deleted' =>'Xóa'
			);
		array_unshift($status_labels, 'Tên');
		array_unshift($status_labels, '#');

		$i = 1;
		foreach($users as $user_id => $user)
		{
			$status = $user['status'];
			$display_name = $this->admin_m->get_field_by_id($user_id, 'display_name');
			$this->table->add_row($i++,$display_name, $user['total'],$status['publish'], $status['viewed'], $status['sticked'], $status['deleted']);
		}
		$this->table->set_heading(array_values($status_labels));
		$this->template->title->set('Thống kê danh bạ khách hàng cấp cho nhân viên');
		$this->template->description->set('Từ '.date('d/m/Y',strtotime($date_from)).' đến '.date('d/m/Y',strtotime($date_end)));
		echo $this->table->generate();
	}


	/*
	$is_send: 
	0: viewed
	1: báo cáo của ngày hôm qua
	2: báo cáo ngày hôm nay đến thời điểm hiện tại 
	*/
	function called($is_send = 0)
	{
		$month = '';
		if(!$month)
			$month = date('m');

		$time = time();
		$day = date('d',$time) -1;

		// if($day == 0){
		// 	$time = strtotime('yesterday',$time);
		// 	$day = date('d',$time);
		// 	$month = date('m',$time);
		// }

		if($is_send == 1)
		{
			$time = strtotime('yesterday',$time);
		}
		else
		{
			$time = time();
		}

		$week = date('w');

		//nếu báo cáo hôm qua thì không gửi chủ nhật và thứ 2
		if($is_send == 1  && in_array($week, array(0, 1)))
		{
			echo $week;
			return false;
		}

		//nếu báo cáo ngày hôm nay thì ko gửi vào thứ 7 và chủ nhật
		if($is_send == 2  && in_array($week, array(6, 0)))
		{
			echo $week;
			return false;
		}

		$day = date('d',$time);
		$month = date('m',$time);

		$time = strtotime(date('Y-m-d', $time));
		$day = str_pad($day, 2, "0", STR_PAD_LEFT);

		// $day = '01';
		$date_from = date('Y-m-d 00:00:00',$time);
		$date_end = date('Y-m-d 00:00:00', strtotime('+1 day',$time));
		// prd($date_from,$date_end);
		$first_month = date('Y-m-01 00:00:00',$time);
		$end_month = date('Y-m-t 00:00:00',$time);

		// $date_from = '2016-06-01 00:00:00';
		// $date_end = '2016-06-31 00:00:00';

		if($is_send == 1)
		{
			$this->table->set_caption('<h2>XẾP HẠNG THÁNH CHĂM CHỈ</h2><br>Thống kê cuộc gọi ngày hôm qua '.date('d/m/Y',strtotime($date_from)));
			$title = 'Thống kê cuộc gọi của kinh doanh ngày hôm qua '.date('d/m/Y', $time);
		}
		else 
		{
			$this->table->set_caption('<h2>XẾP HẠNG THÁNH CHĂM CHỈ</h2><br>Thống kê cuộc gọi giữa ngày '.date('d/m/Y',strtotime($date_from)));
			$title = 'Thống kê cuộc gọi của kinh doanh ngày '.date('d/m/Y H:i:s', time());
		}

		$content = '';
		


		$roleLeader = 13;
		$roleMember = 9;
		$num_day_event = 0; //số ngày nghỉ lễ trong tháng

		if(!$calleds = $this->scache->get('phonebook-cache-new-14'))
		{
			$end_month = $this->mdate->endOfDay($end_month);
			$end_month = date('Y-m-d H:i:s',$end_month);

			$results = $this->report_phonebook_m->get_called_result($first_month, $end_month);
			$calleds = $this->report_phonebook_m->build_tree_called($results);
			$this->scache->write($calleds, 'phonebook-cache-new-13', 600);
		}

		$time_from = strtotime($date_from);
		$time_end = strtotime($date_end);

		$date = date('Y-m-d', $time_from);

		$results = $calleds[$date];

		$users = $this->report_phonebook_m->get_users();
		$check_point = 1;
		$i = 1;
		$role_name = 'NV';

		//KPI Trưởng nhóm:
		//KPI Nhân viên
		$KPILeader = $this->option_m->get_value('leader_kpi_per_day') ?: $this->config->item('leader_kpi_per_day');
		$KPIMember = $this->option_m->get_value('member_kpi_per_day') ?: $this->config->item('member_kpi_per_day');

		// prd($users);
		// $users['user_working_day'][158] = strtotime('-10 day');
		// $users['user_working_day'][157] = strtotime('-10 day');
		// $users['user_working_day'][159] = strtotime('-10 day');

		$total = array();
		$total['total'] = 0;
		$total['BUSY'] = 0;
		$total['ANSWERED'] = 0;
		$total['NO ANSWER'] = 0;
		$total['callKPI'] = 0;
		$total['percent_day'] = 0;
		$total['total_month'] = 0;
		$total['percent_month'] = 0;
		$count = 1;

		// unset($results[601]);//anh Hải 
		// prd($results);
		//sort by KPI
		// uasort($results, function ($a, $b) {
		// 	if(!isset($a['status']['ANSWERED']['total']))
		// 		$a['status']['ANSWERED']['total'] = 0;
		// 	if(!isset($b['status']['ANSWERED']['total']))
		// 		$b['status']['ANSWERED']['total'] = 0;
		// 	return $b['status']['ANSWERED']['total'] > $a['status']['ANSWERED']['total'];
		// });

		$total_result = count($results);
		$bad_result = $total_result - 3;
		$rows = array();
		$total_months = array();

		foreach($results as $ext=>$result)
		{
			$user_id = isset($users['usersExtIDArray'][$ext]) ? $users['usersExtIDArray'][$ext] : 0;
			$first_month = date('Y-m-01 00:00:00',$time);
			if(!$user_id)
			{
				$user_id = 0;
				// die('cmmd'.$ext);
				// continue;
			}
			$first_ext = substr($ext, 0,1);
			// var_dump($first_ext);
			if(!in_array($first_ext, array('6','8')))
				continue;

			$userTimeStart = (isset($users['user_working_day'][$user_id]) ? $users['user_working_day'][$user_id] : 0);

			$kpi_user_month = 0;
			$kpi_user_day = 0;
			

			if($userTimeStart > 0 && isset($users['users'][$user_id]))
			{

				$userTimeStart = strtotime('+15 day', $userTimeStart);

				if($userTimeStart > $this->mdate->startOfMonth($time))
					$first_month = date('Y-m-d 00:00:00',$userTimeStart);

				$dayWorking = 0;
				if($userTimeStart < time())
				{
					if(date('m',$userTimeStart) != date('m'))
					{
						$userTimeStart = $this->mdate->startOfMonth($time);
					}

					$dayWorking = $this->report_phonebook_m->calc_business_days($userTimeStart, 
						$this->mdate->endOfMonth($time));
					$dayWorking = ($dayWorking - $num_day_event);

					if($dayWorking < 0)
					{
						$dayWorking = 0;
					}
				}

				if($users['users'][$user_id]->role_id == $roleMember)
				{
					$kpi_user_month = $KPIMember * $dayWorking;
					$kpi_user_day = $KPIMember;
					$role_name = 'NV';
				}
				else if($users['users'][$user_id]->role_id == $roleLeader)
				{
					$kpi_user_month = $KPILeader * $dayWorking;
					$kpi_user_day = $KPILeader;
					$role_name = 'TN';
				}
				else
				{
					//???
					
					$role_name = '??';
				}
			}

			$result_user = $this->report_phonebook_m->calc_total_from_ext($calleds, $first_month,$end_month);

			$total_month = (isset($result_user[$ext]['status']['ANSWERED']['total']) ? $result_user[$ext]['status']['ANSWERED']['total']: 0);

			$percent_day = 0;
			$percent_month = 0;
			$name = '(#'.$ext.')'.(isset($users['user_name'][$user_id])? $users['user_name'][$user_id] : '');

			$status = $result['status'];
			$call_total = (isset($status['ANSWERED']['total']) ? $status['ANSWERED']['total'] : 0);
			
			($kpi_user_day > 0) AND $percent_day = (($call_total/$kpi_user_day)*100);
			($kpi_user_month > 0) AND $percent_month = (($total_month/$kpi_user_month)*100);




			$result_total = isset($result['total']) ? $result['total'] : 0;
			$failed = isset($status['FAILED']['total']) ? $status['FAILED']['total'] : 0;
			$result_total = $result_total - $failed;

			$result_busy = isset($status['BUSY']['total']) ? $status['BUSY']['total'] : 0;
			$result_no_answer = isset($status['NO ANSWER']['total']) ? $status['NO ANSWER']['total'] : 0;
			$result_call_kpi = isset($result['callKPI']) ? $result['callKPI'] : 0;

			$total['total']+= $result_total;
			$total['BUSY'] += $result_busy;
			$total['NO ANSWER'] += $result_no_answer;
			$total['ANSWERED'] += $call_total;
			$total['callKPI'] += $result_call_kpi;
			$total['percent_day'] += $percent_day;
			$total['total_month'] += $total_month;
			$total['percent_month'] += $percent_month;

			if(!isset($total_months[$total_month]))
				$total_months[$total_month] = array();
			array_push($total_months[$total_month], $user_id);

			if(isset($rows[$user_id]))
				$user_id = rand(9999, 99999);

			$rows[$user_id] = array($i, 
				$name,
				$role_name,
				$call_total.'/'.numberformat($kpi_user_day),
				$result_call_kpi,
				numberformat($percent_day,2).'%',
				numberformat($total_month).'/'.numberformat($kpi_user_month),
				numberformat($percent_month,2).'%',
				$result_busy,
				$result_no_answer,
				$result_total
				);

			// $this->table->add_row($i, 
			// 	$name,
			// 	$role_name,
			// 	$call_total.'/'.numberformat($kpi_user_day),
			// 	$result_call_kpi,
			// 	numberformat($percent_day,2).'%',
			// 	numberformat($total_month).'/'.numberformat($kpi_user_month),
			// 	numberformat($percent_month,2).'%',
			// 	$result_busy,
			// 	$result_no_answer,
			// 	$result_total
			// 	);
			
			unset($users['users'][$user_id]);
			$i++;
		}


		uasort($rows, function ($a, $b) {

			if(!isset($a[5]))
				$a[5] = 0;
			if(!isset($b[5]))
				$b[5] = 0;

			$a[5] = (int)$a[5];
			$b[5] = (int)$b[5];

			return $b[5] > $a[5];
		});

		//sort value highest to lowest
		ksort($total_months);

		$best1 = array_pop($total_months);
		$best2 = array_pop($total_months);

		// unset($rows[365]);
		foreach($rows as $user_id => $row)
		{

				//tên 3 người đầu tiên đc bold
			if(in_array($count, array(1,2,3)))
			{
				$row[1] = '<b>'.$row[1].'</b>';
				$row[2] = '<b>'.$row[2].'</b>';
			}
			if($count > $bad_result)
			{
				$row[1] = '<b><span style="color: #f00">'.$row[1].'</span></b>';
				$row[2] = '<b><span style="color: #f00">'.$row[2].'</span></b>';
			}

			if($user_id > 0)
			{
				//Lũy kế cuộc gọi tháng cao nhất được thưởng
				if(in_array($user_id, $best1))
				{
					$row[1] = $row[1].'<img src="https://webdoctor.vn/images/gifts.png">';
				}

				//Lũy kế cuộc gọi tháng cao nhì được thưởng
				if(in_array($user_id, $best2))
				{
					$row[1] = $row[1].'<img src="https://webdoctor.vn/images/gift.png">';
				}
			}
			
			//STT
			$row[0] = $count;
			$this->table->add_row($row);
			$count++;
		}
		if($users['users'])
		{
			foreach($users['users'] as $u)
			{
				$this->table->add_row($i,$u->display_name,'??','??','??','??','??','??','??','??','??');
				$i++;
			}
		}

		
		$percent_day = (isset($total['percent_day']) ? $total['percent_day'] : 0);
		$percent_day = $percent_day / $count;

		$percent_month = (isset($total['percent_month']) ? $total['percent_month'] : 0);
		$percent_month = $percent_month / $count;


		$this->table->add_row(
			array('data' => '<b>Tổng</b>',  'colspan' => 3),
			'<b>'.$total['ANSWERED'].'</b>',
			'<b>'.(isset($total['callKPI']) ? $total['callKPI'] : 0).'</b>',
			'<b>'.myNumber_format($percent_day,2).'%'.'</b>',
			'<b>'.numberformat($total['total_month']).'</b>',
			'<b>'.numberformat($percent_month,2).'%</b>',
			'<b>'.(isset($total['BUSY']) ? $total['BUSY'] : 0).'</b>',
			'<b>'.(isset($total['NO ANSWER']) ? $total['NO ANSWER'] : 0).'</b>',
			'<b>'.$total['total'].'</b>'
			);

		$this->table->set_heading(array(
			'#',
			'Tên',
			'Chức vụ',
			'Gọi kết nối được/KPI ngày',
			'Gọi kết nối >15s',
			'Tỷ lệ % đạt KPI ngày',
			'Lũy kế tháng cuộc gọi',
			'Tỷ lệ % đạt KPIs tháng',
			'Máy bận',
			'Gọi không kết nối được',
			'Tổng',
			));

		


		
		$this->config->load('table_mail');
		$this->table->set_template($this->config->item('mail_template'));
		
		$content = $this->table->generate();

		$work_time = $this->report_phonebook_m->calc_business_days($this->mdate->startOfMonth($time), 
			$this->mdate->endOfMonth($time));
		$content .='<br> <b>Số ngày làm việc trung bình của tháng '.$month.' là: '.$work_time.' ngày</b>';
		$content .='<br> <img src="https://webdoctor.vn/images/gifts.png"> : dành cho người có tổng cuộc gọi trong tháng cao nhất.';
		$content .='<br> <img src="https://webdoctor.vn/images/gift.png"> : dành cho người có tổng cuộc gọi trong tháng cao nhì.';

		if($is_send)
		{
			$status = $this->send_mail($title, $content);
			var_dump($status);
			echo $content;
			return '';
		}
		echo $content;

	}

	function check_connect()
	{
		$status = $this->report_phonebook_m->_connect_db_callcenter();
		var_dump($status);
	}

	function update_date()
	{

		$erpDB = $this->report_phonebook_m->_connect_db_erp();

		$users = $erpDB->from('user')->where('user_status', 1)->where_in('role_id', array(13, 9))->get()->result();
		$day_works = array(
			'141' => '09/11/2015', //phuongtdn@adsplus.vn
			'280' => '27/07/2016', //anld@adsplus.vn
			'320' => '19/07/2016', //uyenvh@adsplus.vn
			'321' => '18/07/2016', //hiennm@adsplus.vn
			'322' => '19/07/2016', //linhnt@adsplus.vn
			'329' => '19/07/2016', //anhptl@adsplus.vn
			'332' => '01/08/2016', //phuongnn@adsplus.vn
			'157' => '01/03/2016', //thaottk@adsplus.vn
			'159' => '14/03/2016', //thuyvtt@adsplus.vn
			'160' => '16/03/2016', //linhhtm@adsplus.vn
			'217' => '17/05/2016', //trangntt@adsplus.vn

			);

		foreach($users as $user)
		{
			if(isset($day_works[$user->user_id]))
			{
				$check = $erpDB->where(array(
					'meta_key'=>'user_working_day',
					'user_id'=>$user->user_id
					))->from('usermeta')->get()->row();

				if($check)
				{
					$date = explode('/', $day_works[$user->user_id]);

					$data['meta_key'] = 'user_working_day';
					$data['meta_value'] = strtotime($date[2].'/'.$date[1].'/'.$date[0]);
					$data['user_id'] = $user->user_id;
					$erpDB->where('umeta_id',$check->umeta_id)->update('usermeta', $data);
					// var_dump($user);
					echo 'insert '.$user->user_id.'<br>';
				}
				else
				{
					// var_dump($check);
				}
			}
			//user_working_day
		}
		// prd($users);
	}

	function send_mail($title = '', $content = '')
	{
		$this->load->library('email');
		$this->email->from('report@webdoctor.vn','Report');

		// $this->email->to('hieupt@webdoctor.vn');
		$this->email->set_debug(false);
		
		$this->email->to('kd@adsplus.vn');
		$this->email->cc('hcns@adsplus.vn,kdhn@adsplus.vn');

		$this->email->subject($title);
		$this->email->message($content);
		return $this->email->send();
	}

	/*
	$is_send: 
	0: viewed
	1: báo cáo của ngày hôm qua
	2: báo cáo ngày hôm nay đến thời điểm hiện tại 
	*/
	function viewcalled($is_send = 0)
	{
		// error_reporting(-1);
		// ini_set('display_errors', 1);

		$month = '';
		if(!$month)
			$month = date('m');

		$time = time();
		$day = date('d',$time) -1;

		// if($day == 0){
		// 	$time = strtotime('yesterday',$time);
		// 	$day = date('d',$time);
		// 	$month = date('m',$time);
		// }

		if($is_send == 1)
		{
			$time = strtotime('yesterday',$time);
		}
		else
		{
			$time = time();
		}

		$week = date('w');

		//nếu báo cáo hôm qua thì không gửi chủ nhật và thứ 2
		if($is_send == 1  && in_array($week, array(0, 1)))
		{
			echo $week;
			return false;
		}

		//nếu báo cáo ngày hôm nay thì ko gửi vào thứ 7 và chủ nhật
		if($is_send == 2  && in_array($week, array(6, 0)))
		{
			echo $week;
			return false;
		}

		$day = date('d',$time);
		$month = date('m',$time);

		$time = strtotime(date('Y-m-d', $time));
		$day = str_pad($day, 2, "0", STR_PAD_LEFT);

		// $day = '01';
		$date_from = date('Y-m-d 00:00:00',$time);
		$date_end = date('Y-m-d 00:00:00', strtotime('+1 day',$time));
		// prd($date_from,$date_end);
		$first_month = date('Y-m-01 00:00:00',$time);
		$end_month = date('Y-m-t 00:00:00',$time);

		$this->load->model('ordermeeting/ordermeeting_m');
		$orders = $this->ordermeeting_m
		->where('status','verified')
		->where('start_time >=',$this->mdate->startOfMonth($time))
		->where('start_time <=',$this->mdate->endOfMonth($time))
		->order_by('start_time','asc')
		->get_many_by();

		$orderGroups = array();
		foreach ($orders as &$order)
		{
			$order->start_time = $this->mdate->startOfDay($order->start_time);
		}

		$orderGroups = array_group_by($orders,'user_id');
		$orderSupportGroups = array_group_by($orders,'support_id');
		if(!empty($orderSupportGroups))
		{
			unset($orderSupportGroups[0]);
			foreach ($orderSupportGroups as $support_id => $orderSupportGroup) 
			{
				if(!isset($orderGroups[$support_id]))
				{
					$orderGroups[$support_id] = $orderSupportGroup;
					continue;
				}

				$orderSupportGroup = array_column($orderSupportGroup, NULL,'id');
				$orderGroups[$support_id] = array_column($orderGroups[$support_id], NULL,'id');
				$orderGroups[$support_id] = array_merge($orderSupportGroup,$orderGroups[$support_id]);
			}
		}

		// $date_from = '2016-06-01 00:00:00';
		// $date_end = '2016-06-31 00:00:00';

		if($is_send == 1)
		{
			$this->table->set_caption('<h2>XẾP HẠNG THÁNH CHĂM CHỈ</h2><br>Thống kê cuộc gọi ngày hôm qua '.date('d/m/Y',strtotime($date_from)));
			$title = 'Thống kê cuộc gọi của kinh doanh ngày hôm qua '.date('d/m/Y', $time);
		}
		else 
		{
			$this->table->set_caption('<h2>XẾP HẠNG THÁNH CHĂM CHỈ</h2><br>Thống kê cuộc gọi giữa ngày '.date('d/m/Y',strtotime($date_from)));
			$title = 'Thống kê cuộc gọi của kinh doanh ngày '.date('d/m/Y H:i:s', time());
		}

		$content = '';
		$roleLeader = 13;
		$roleMember = 9;
		//$num_day_event = 3; //số ngày nghỉ lễ trong tháng 2/2017 : 1 ngày lễ + 2 ngày nghỉ trừ hao đầu tháng
		$num_day_event = 0; //số ngày nghỉ lễ trong tháng 3/2017

		if(!$calleds = $this->scache->get('phonebook-cache-new-14'))
		{
			$end_month = $this->mdate->endOfDay($end_month);
			$end_month = date('Y-m-d H:i:s',$end_month);

			$results = $this->report_phonebook_m->get_called_result($first_month, $end_month);
			$calleds = $this->report_phonebook_m->build_tree_called($results);
			$this->scache->write($calleds, 'phonebook-cache-new-13', 600);
		}

		$time_from = strtotime($date_from);
		$time_end = strtotime($date_end);

		$date = date('Y-m-d', $time_from);
		$results = $calleds[$date];

		$users = $this->report_phonebook_m->get_users();
		$check_point = 1;
		$i = 1;
		$role_name = 'NV';

		//KPI Trưởng nhóm:
		//KPI Nhân viên
		$KPILeader = $this->option_m->get_value('leader_kpi_per_day') ?: $this->config->item('leader_kpi_per_day');
		$KPIMember = $this->option_m->get_value('member_kpi_per_day') ?: $this->config->item('member_kpi_per_day');

		// prd($users);
		// $users['user_working_day'][158] = strtotime('-10 day');
		// $users['user_working_day'][157] = strtotime('-10 day');
		// $users['user_working_day'][159] = strtotime('-10 day');

		$total = array();
		$total['total'] = 0;
		$total['meet_total'] = 0;
		$total['BUSY'] = 0;
		$total['ANSWERED'] = 0;
		$total['NO ANSWER'] = 0;
		$total['callKPI'] = 0;
		$total['percent_day'] = 0;
		$total['total_month'] = 0;
		$total['meet_total_month'] = 0;
		$total['percent_month'] = 0;
		$count = 1;

		// unset($results[601]);//anh Hải 
		// prd($results);
		//sort by KPI
		// uasort($results, function ($a, $b) {
		// 	if(!isset($a['status']['ANSWERED']['total']))
		// 		$a['status']['ANSWERED']['total'] = 0;
		// 	if(!isset($b['status']['ANSWERED']['total']))
		// 		$b['status']['ANSWERED']['total'] = 0;
		// 	return $b['status']['ANSWERED']['total'] > $a['status']['ANSWERED']['total'];
		// });

		$total_result = count($results);
		$bad_result = $total_result - 3;
		$rows = array();
		$total_months = array();
		
		foreach($results as $ext=>$result)
		{
			$user_id = isset($users['usersExtIDArray'][$ext]) ? $users['usersExtIDArray'][$ext] : 0;
			$first_month = date('Y-m-01 00:00:00',$time);
			if(!$user_id)
			{
				$user_id = 0;
				// die('cmmd'.$ext);
				// continue;
			}

			$first_ext = substr($ext, 0,1);
			if(!in_array($first_ext, array('6','8'))) continue;

			$userTimeStart = (isset($users['user_working_day'][$user_id]) ? $users['user_working_day'][$user_id] : 0);

			$kpi_user_month = 0;
			$kpi_user_day = 0;

			if($userTimeStart > 0 && isset($users['users'][$user_id]))
			{

				$userTimeStart = strtotime('+7 day', $userTimeStart);

				if($userTimeStart > $this->mdate->startOfMonth($time))
					$first_month = date('Y-m-d 00:00:00',$userTimeStart);

				$dayWorking = 0;
				if($userTimeStart < time())
				{
					if(date('m-Y',$userTimeStart) != date('m-Y'))
					{
						$userTimeStart = $this->mdate->startOfMonth($time);
					}

					$dayWorking = $this->report_phonebook_m->calc_business_days($userTimeStart,$this->mdate->endOfMonth($time));
					$dayWorking = ($dayWorking - $num_day_event);

					if($dayWorking < 0)
					{
						$dayWorking = 0;
					}
				}

				if($users['users'][$user_id]->role_id == $roleMember)
				{
					$kpi_user_month = $KPIMember * $dayWorking;
					$kpi_user_day = $KPIMember;
					$role_name = 'NV';
				}
				else if($users['users'][$user_id]->role_id == $roleLeader)
				{
					$kpi_user_month = $KPILeader * $dayWorking;
					$kpi_user_day = $KPILeader;
					$role_name = 'TN';
				}
				else
				{
					//???
					
					$role_name = '??';
				}
			}

			$result_user = $this->report_phonebook_m->calc_total_from_ext($calleds, $first_month,$end_month);

			$total_month = (isset($result_user[$ext]['status']['ANSWERED']['total']) ? $result_user[$ext]['status']['ANSWERED']['total']: 0);

			$percent_day = 0;
			$percent_month = 0;
			$name = '(#'.$ext.')'.(isset($users['user_name'][$user_id])? $users['user_name'][$user_id] : '');

			$status = $result['status'];
			$call_total = (isset($status['ANSWERED']['total']) ? $status['ANSWERED']['total'] : 0);


			/* add column ordermeeting*/
			$meet_total = 0;
			$dateGroup = array_group_by($orders,'start_time');
			if(!empty($dateGroup[$time_from]))
			{
				foreach ($dateGroup[$time_from] as $orderItem) 
				{
					if($user_id == $orderItem->user_id)
					{
						$meet_total++;
						continue;
					}

					if(!empty($orderItem->support_id) && $user_id == $orderItem->support_id)
					{
						$meet_total++;
						continue;
					}
				}
			}

			/*add column ordermeeting all*/
			$meet_total_month = 0;
			if(!empty($orderGroups[$user_id]))
			{
				$meet_total_month = count($orderGroups[$user_id]);
			}
			
			($kpi_user_day > 0) AND $percent_day = ((($call_total + ($meet_total*10))/$kpi_user_day)*100);
			($kpi_user_month > 0) AND $percent_month = ((($total_month + ($meet_total_month*10))/$kpi_user_month)*100);


			$result_total = isset($result['total']) ? $result['total'] : 0;
			$failed = isset($status['FAILED']['total']) ? $status['FAILED']['total'] : 0;
			$result_total = $result_total - $failed;

			$result_busy = isset($status['BUSY']['total']) ? $status['BUSY']['total'] : 0;
			$result_no_answer = isset($status['NO ANSWER']['total']) ? $status['NO ANSWER']['total'] : 0;
			$result_call_kpi = isset($result['callKPI']) ? $result['callKPI'] : 0;

			$total['total']+= $result_total;
			$total['BUSY'] += $result_busy;
			$total['NO ANSWER'] += $result_no_answer;
			$total['ANSWERED'] += $call_total;
			$total['meet_total'] += $meet_total;
			$total['callKPI'] += $result_call_kpi;
			$total['percent_day'] += $percent_day;
			$total['total_month'] += $total_month;
			$total['meet_total_month'] += $meet_total_month;
			$total['percent_month'] += $percent_month;

			if(!isset($total_months[$total_month]))
				$total_months[$total_month] = array();
			array_push($total_months[$total_month], $user_id);

			if(isset($rows[$user_id]))
				$user_id = rand(9999, 99999);

			$rows[$user_id] = array($i, 
				$name,
				$role_name,
				$call_total,
				$meet_total,
				($call_total + ($meet_total*10)).'/'.numberformat($kpi_user_day),
				numberformat($total_month + ($meet_total_month*10)).'/'.numberformat($kpi_user_month),
				numberformat($percent_month,2).'%',
				);
			
			unset($users['users'][$user_id]);
			$i++;
		}

		/* FILTER USER HAS ORDER METTING AND HAS NOT EXT OR PHONE CALL BY TIME*/
		if($users['users'])
		{
			foreach($users['users'] as $u)
			{
				$userTimeStart = (isset($users['user_working_day'][$u->user_id]) ? $users['user_working_day'][$u->user_id] : 0);

				$kpi_user_month = 0;
				$kpi_user_day = 0;

				if($userTimeStart > 0 && isset($users['users'][$u->user_id]))
				{
					$userTimeStart = strtotime('+7 day', $userTimeStart);

					if($userTimeStart > $this->mdate->startOfMonth($time))
						$first_month = date('Y-m-d 00:00:00',$userTimeStart);

					$dayWorking = 0;
					if($userTimeStart < time())
					{
						if(date('m',$userTimeStart) != date('m'))
						{
							$userTimeStart = $this->mdate->startOfMonth($time);
						}

						$dayWorking = $this->report_phonebook_m->calc_business_days($userTimeStart, 
							$this->mdate->endOfMonth($time));
						$dayWorking = ($dayWorking - $num_day_event);

						if($dayWorking < 0)
						{
							$dayWorking = 0;
						}
					}

					if($users['users'][$u->user_id]->role_id == $roleMember)
					{
						$kpi_user_month = $KPIMember * $dayWorking;
						$kpi_user_day = $KPIMember;
						$role_name = 'NV';
					}
					else if($users['users'][$u->user_id]->role_id == $roleLeader)
					{
						$kpi_user_month = $KPILeader * $dayWorking;
						$kpi_user_day = $KPILeader;
						$role_name = 'TN';
					}
					else
					{
						$role_name = '??';
					}
				}
				
				$ext = $users['usersExtArray'][$u->user_id];				
				$first_ext = substr($ext, 0,1);
				if(!in_array($first_ext, array('6','8'))) continue;

				$result_user = $this->report_phonebook_m->calc_total_from_ext($calleds, my_date($userTimeStart,'Y-m-d H:i:s'),$end_month);

				$total_month = $result_user[$ext]['status']['ANSWERED']['total'] ?? 0;

				$meet_total_month = 0;
				$meet_total = 0;
				if(!empty($orderGroups[$u->user_id]))
				{
					$meet_total_month = count($orderGroups[$u->user_id]);	
					$UserOrdersGroupByDay = array_group_by($orderGroups[$u->user_id],'start_time');
					$UserOrdersGroupToday = $UserOrdersGroupByDay[$time_from] ?? array();
					$meet_total = count($UserOrdersGroupToday);

				}

				$percent_month = 0;
				($kpi_user_month > 0) AND $percent_month = div(($total_month + $meet_total_month*10),$kpi_user_month)*100;

				$result_call_kpi = $result_user[$ext]['callKPI'] ?? 0;

				$total['meet_total'] += $meet_total;
				$total['meet_total_month'] += $meet_total_month;
				$total['callKPI'] += $result_call_kpi;
				$total['total_month'] += $total_month;
				$total['percent_month'] += $percent_month;

				$name = (isset($users['user_name'][$u->user_id])? $users['user_name'][$u->user_id] : '');

				$rows[$u->user_id] = array($i,
					$name,
					$role_name,
					'??',
					$meet_total,
					($meet_total*10).'/'.numberformat($kpi_user_day),
					($total_month + ($meet_total_month*10)).'/'.numberformat($kpi_user_month),
					numberformat($percent_month,2).'%',
					);

				unset($users['users'][$u->user_id]);
				$i++;
			}
		}


		uasort($rows, function ($a, $b) {

			if(!isset($a[7]))
				$a[7] = 0;
			if(!isset($b[7]))
				$b[7] = 0;

			$a[7] = (int)$a[7];
			$b[7] = (int)$b[7];

			return $b[7] > $a[7];
		});

		//sort value highest to lowest
		ksort($total_months);

		$best1 = array_pop($total_months);
		$best2 = array_pop($total_months);

		// unset($rows[365]);
		foreach($rows as $user_id => $row)
		{

			//tên 3 người đầu tiên đc bold
			if(in_array($count, array(1,2,3)))
			{
				$row[1] = '<b>'.$row[1].'</b>';
				$row[2] = '<b>'.$row[2].'</b>';
			}
			if($count > $bad_result)
			{
				$row[1] = '<b><span style="color: #f00">'.$row[1].'</span></b>';
				$row[2] = '<b><span style="color: #f00">'.$row[2].'</span></b>';
			}

			// if($user_id > 0)
			// {
			// 	//Lũy kế cuộc gọi tháng cao nhất được thưởng
			// 	if(in_array($user_id, $best1))
			// 	{
			// 		$row[1] = $row[1].'<img src="https://webdoctor.vn/images/gifts.png">';
			// 	}

			// 	//Lũy kế cuộc gọi tháng cao nhì được thưởng
			// 	if(in_array($user_id, $best2))
			// 	{
			// 		$row[1] = $row[1].'<img src="https://webdoctor.vn/images/gift.png">';
			// 	}
			// }
			
			//STT
			$row[0] = $count;
			$this->table->add_row($row);
			$count++;
		}

		if($users['users'])
		{
			foreach($users['users'] as $u)
			{
				$this->table->add_row($i,$u->display_name,'??','??','??','??','??','??');
				$i++;
			}
		}

		
		$percent_day = (isset($total['percent_day']) ? $total['percent_day'] : 0);
		$percent_day = $percent_day / $count;

		$percent_month = (isset($total['percent_month']) ? $total['percent_month'] : 0);
		$percent_month = $percent_month / $count;

		$totalKPIToday = $total['meet_total']*10 + $total['ANSWERED'];
		
		$totalKPIMonth = $total['total_month'] + $total['meet_total_month']*10;

		$this->table->add_row(
			array('data' => '<b>Tổng</b>',  'colspan' => 3),
			'<b>'.$total['ANSWERED'].'</b>',
			'<b>'.$total['meet_total'].'</b>',
			'<b>'.$totalKPIToday.'</b>',
			'<b>'.$totalKPIMonth.'</b>',
			'<b>'.numberformat($percent_month,2).'%</b>'
			);

		$this->table->set_heading(array(
			'#',
			'Tên',
			'Chức vụ',
			'Cuộc gọi',
			'Cuộc gặp',
			'Quy đổi cuộc gọi theo ngày',
			'Quy đổi cuộc gọi theo tháng',
			'Tỷ lệ % đạt KPIs tháng',
			));
		
		$this->config->load('table_mail');
		$this->table->set_template($this->config->item('mail_template'));
		
		$content = $this->table->generate();

		$work_time = $this->report_phonebook_m->calc_business_days($this->mdate->startOfMonth($time), 
			$this->mdate->endOfMonth($time));
		
		if(date('Y') == '2017' && date('m') == '01') # Nếu là tháng giêng 2017 , bỏ 6 ngày nghỉ lễ trong tháng
		{
			$work_time-= 7;
		}

		$content .='<br> <b>Số ngày làm việc trung bình của tháng '.$month.' là: '.$work_time.' ngày</b>';
		// $content .='<br> <img src="https://webdoctor.vn/images/gifts.png"> : dành cho người có tổng cuộc gọi trong tháng cao nhất.';
		// $content .='<br> <img src="https://webdoctor.vn/images/gift.png"> : dành cho người có tổng cuộc gọi trong tháng cao nhì.';

		if($is_send)
		{
			$status = $this->send_mail($title, $content);
			var_dump($status);
			echo $content;
			return '';
		}
		echo $content;
	}	

	public function cron_call_kpi()
	{
		$end_time = $this->mdate->endOfDay();
		$start_time = $this->mdate->startOfDay(strtotime('-3 days',$end_time));

		while ($start_time < $end_time)
		{
			$this->recalc_call_kpi($start_time);
			$start_time = strtotime('+1 day',$start_time);
		}
	}

	public function recalc_call_kpi($kpi_datetime = 0)
	{
		$kpi_datetime = $kpi_datetime ?: $this->mdate->startOfDay();
		$this->load->model('mbusiness/call_kpi_m');
		$this->load->model('phonebook/report_phonebook_m');
		$users = $this->report_phonebook_m->get_users();
		$exts = $users['usersExtIDArray'] ?? array();

		$roleLeader = 13;
		$roleMember = 9;
		$KPILeader = $this->option_m->get_value('leader_kpi_per_day') ?: $this->config->item('leader_kpi_per_day');
		$KPIMember = $this->option_m->get_value('member_kpi_per_day') ?: $this->config->item('member_kpi_per_day');

		if(empty($exts)) return FALSE;

		foreach ($exts as $ext => $user_id)
		{
			if(empty($ext)) continue;

			$role_id = $this->admin_m->get_field_by_id($user_id,'role_id');
			if( ! in_array($role_id, [$roleLeader,$roleMember])) continue;

			$user_status = $this->admin_m->get_field_by_id($user_id,'user_status');
			if($user_status != '1') continue;

			$kpi_value = $KPIMember;
			if($role_id == $roleLeader) $kpi_value = $KPILeader;

			$this->call_kpi_m->update_kpi_value($ext, 'phonecall', $kpi_value, $kpi_datetime, $user_id);
		}
	}
}
/* End of file Report.php */
/* Location: ./application/models/phonebook/Report.php */