<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fix extends Admin_Controller {

	public $model = 'phonebook_m';

	function __construct()
	{
		parent::__construct();
		ini_set('memory_limit','2048M');
		$this->load->model($this->model);
		$this->load->model('phonebook_log_m');
	}

	function index()
	{
		$this->FunctionName1();
	}

	public function calc_sale_kpi()
	{
		$post = $this->input->post();
		if(!empty($post['submit']))
		{
			$this->load->helper('security');
			
			$ext = (int) xss_clean($post['ext']);
			$end_date = xss_clean($post['end_date']);
			$months = (int) xss_clean($post['months']);
			$email = trim(xss_clean($post['email']));

			try 
			{
				$this->load->model('report_phonebook_m');
				$end_time = $this->mdate->endOfDay($end_date);
				$start_time = $this->mdate->startOfDay(strtotime("-{$months} months",$end_time));

				$results = $this->report_phonebook_m->get_called_result(date('Y-m-d 00:00:00',$start_time), date('Y-m-d 00:00:00',$end_time),$ext);
		  		$calleds = $this->report_phonebook_m->build_tree_called($results);
		  		$result_ext = $this->report_phonebook_m->calc_total_from_ext($calleds,date('Y-m-d 00:00:00',$start_time), date('Y-m-d 00:00:00',$end_time));

		  		if(!empty($result_ext))
		  		{
		  			echo 't/g bắt đầu : '.my_date($start_time);
		  			echo '<br/>t/g kết thúc : '.my_date($end_time);
		  			echo '<br/>Tổng cuộc gọi có trả lời : '. @$result_ext[$ext]['status']['ANSWERED']['total'];
		  			echo '<br/>Tổng cuộc gọi đạt KPI : '. @$result_ext[$ext]['callKPI'];
		  			echo '<hr/>';
		  		}

				$this->load->model('admin_m');
		  		$user = $this->admin_m->set_get_admin()->where('user_email',$email)->get_by();
		  		if(!empty($user))
		  		{
		  			$this->load->model('ordermeeting/ordermeeting_m');
		  			$orders = $this->ordermeeting_m
		  				->where_in('status',['sent','verified','deleted'])
		  				->group_start()
			  				->where('ordermeeting.user_id',$user->user_id)
			  				->or_where('ordermeeting.support_id',$user->user_id)
		  				->group_end()
		  				->get_many_by();


		  			if(empty($orders))
		  			{
		  				echo '<br/>Không tìm thấy order hẹn làm việc với khách hàng.';
		  				echo '<hr/>';
		  			}
		  			else
		  			{
			  			$order_group = array_group_by($orders,'status');

			  			$order_verified_count = count($order_group['verified']??NULL);
			  			$order_sent_count = count($order_group['sent']??NULL);
			  			echo '<br/>Tổng cuộc hẹn gặp :'. ($order_verified_count + $order_sent_count);
			  			echo '<br/>Tổng cuộc hẹn được xác nhận :'.$order_verified_count;
			  			echo '<br/>Tổng cuộc hẹn đang chờ được xác nhận :'.$order_sent_count;
			  			echo '<hr/>';
		  			}

		  		}
			} 
			catch (Exception $e)
			{
				echo 'Dữ liệu đầu vào không hợp lệ';
			}
		}

  		echo $this->admin_form->form_open();
  		echo $this->admin_form->input('email NVKD','email',$email ?? '');
  		echo $this->admin_form->input('Ext','ext',$ext ?? '');
  		echo $this->admin_form->input('Số tháng thử việc','months',$months??'','Phải là số .');
  		echo $this->admin_form->input('Thời gian kết thúc','end_date',$end_date ?? '','Phải là Y/m/d , vd: 2017/06/27');


  		echo $this->admin_form->submit('submit','lấy thông tin');
  		echo $this->admin_form->form_close();
	}

	public function output(){	
		$start = 0;
		$limit = 5000;

		$key_cache = '500-rows-phones';
		$data = $this->scache->get($key_cache);
		if(empty($data)){

			$phones = $this
			->phonebook_m
			->order_by('pbook_date_est','desc')
			->limit(5000, 0)
			->get_many_by();

			$this->scache->write($phones,$key_cache, 10*24*3600);
			$data = $phones;
			die();
		}

		if(empty($data)) return FALSE;

		$this->load->library('table');
		foreach ($data as $p) {

			$phone = $this->phone_replace($p->pbook_phone);

			$i=0;
			$phone_formated = '';
			$size = strlen($phone);
			for ($i; $i < $size; $i++) 
				$phone_formated.=(int)$phone[$i];

			$i=0;
			$result = '';
			for ($i; $i < $size; $i++){ 
				if($phone_formated[$i] == $phone[$i])
					$result.=$phone_formated[$i];
			}

			$leng = strlen($result);

			$is_mobile = substr($result, 0,2) != '08' ? 'is_mobile' : 'table phone';

			$is_invalid = $leng>12;

			if($is_invalid){
				$delimiters = array('-','/');
				$phone = str_replace($delimiters, $delimiters[0], $phone);
				$_phone = explode($delimiters[0], $phone);
				$_phone = reset($_phone);
				$result = $this->convert_pattern($_phone);	
				$leng = strlen($result);
				$is_mobile = substr($result, 0,2) != '08' ? 'is_mobile' : 'table phone';
				$is_invalid = $leng>12;
			}

			$this->table->add_row($p->pbook_phone,$phone_formated,$result,$is_mobile,$leng.' số',$is_invalid?'invalid':'');
		}
		echo $this->table->generate();
	}
//19006712
//1900545465
//0932239682
	public function FunctionName1($value='')
	{
		$phones = $this
		->phonebook_m
		->order_by('pbook_date_est','desc')
		->get_many_by();
$update = array();
		foreach ($phones as $i=>$phone) {
			$d = explode('/', $phone->pbook_date_est);

			$update['pbook_date_est'] = $phone->pbook_date_est;

			if(!empty($d[0]) && $d[0] > 2016)
			{
				$update['pbook_date_est'] = 0;
			}

			$update['is_mobile'] = (int)$this->is_mobile($phone->pbook_phone);
			$this->phonebook_m->update($phone->pbook_id, $update);

			echo "\r                                              ";
			echo "\r".$phone->pbook_id.' - '.$i.'         '."";

		}
		echo "END;";
	}

	public function is_mobile($phone = '')
	{
		if(empty($phone)) return FALSE;

		$phone = $this->phone_replace($phone);
		$phone = $this->convert_pattern($phone);

		$first_number = substr($phone, 0,1);
		if($first_number=='0'){
			if(in_array(substr($phone, 1,1),array(1,9))) return TRUE;
			return FALSE;
		}
		else{
			if(in_array($first_number,array(1,9))) return TRUE;
			return FALSE;
		}

		return FALSE;
	}

	private function phone_replace($phone_string = '')
	{
		$phone = preg_replace('/\s+/', '', $phone_string);
		$phone = str_replace('+84(0)', '0', $phone);
		$phone = str_replace('+084(0)', '0', $phone);

		$phone = str_replace('+84', '0', $phone);
		$phone = str_replace('(84)', '0', $phone);
		$phone = str_replace('(848)', '08', $phone);
		$phone = str_replace('(84-8)', '08', $phone);
		$phone = str_replace('(84-08)', '08', $phone);

		if(in_array(substr($phone, 0,4), array('84-8','(08)')))
		{
			$phone = substr_replace($phone,'08',0,4);
		}
		if(substr($phone, 0,3) == '848'){
			$phone = substr_replace($phone,'08',0,3);
		}

		return $phone;
	}

	public function update_new_number(){
		$offset = 0;
		$limit = 5000;
		$key_cache = $offset.'-'.$limit.'-rows-phones';
		$data = $this->scache->get($key_cache);
		if(empty($data)){

			$phones = $this->phonebook_m
			->select('pbook_id,pbook_phone')
			->order_by('pbook_date_est','desc')
			->limit($limit, $offset)
			->get_many_by();

			$this->scache->write($phones,$key_cache, 10*24*3600);
			$data = $phones;
		}

		if(empty($data)) return FALSE;

		foreach($data as $phone){
			$number_formatted = $this->convert_pattern($phone->pbook_phone);
			$this->phonebook_m->update($phone->pbook_id,array('pbook_phone_convert'=>$number_formatted));
		}
	}

	public function test()
	{
		$offset = 50000;
		$limit = 100000;
		$key_cache = $offset.'-'.$limit.'-rows-phones';
		$data = $this->scache->get($key_cache);
		if(empty($data)){

			$phones = $this
			->phonebook_m
			->order_by('pbook_date_est','desc')
			->limit($limit, $offset)
			->get_many_by();

			$this->scache->write($phones,$key_cache, 10*24*3600);
			$data = $phones;
		}

		if(empty($data)) return FALSE;

		$this->load->library('table');
		$max_col = 7;
		$i = 0;
		$tr = array();
		foreach ($data as $key => $p){
			if($i==$max_col){
				$i=0;
				$this->table->add_row($tr);
				$tr = array();
				$tr[] = $p->pbook_phone.'<br/>'.convert_pattern($p->pbook_phone);
			}
			$tr[] = $p->pbook_phone.'<br/>'.convert_pattern($p->pbook_phone);
			$i++;
		}
		echo $this->table->generate();
	}

	private function convert_pattern($phone_string = '')
	{
		$phone = $this->phone_replace($phone_string);

		$i=0;
		$phone_formated = '';
		$size = strlen($phone);
		for ($i; $i < $size; $i++) 
			$phone_formated.=(int)$phone[$i];
		$j=0;
		$result = '';
		$size = strlen($phone) ;
		for ($j; $j < $size; $j++){
			if($phone_formated[$j] == $phone[$j])
				$result.=$phone_formated[$j];
		}
		if(strlen($result) > 11){
			$delimiters = array('-','/');
			$result = str_replace($delimiters, reset($delimiters), $phone_string);
			$strings = explode(reset($delimiters), $result);
			$count = count($strings);
			if($count > 1){
				$arg = reset($strings);
				$count = count($strings);
				if($count>2){
					array_pop($strings);
					$arg = implode('-', $strings);
				}

				$result = $this->convert_pattern($arg);
			}
		}

		if(in_array(strlen($result), array(7,8)))
			$result = '08'.$result;

		return $result;
	}

	public function unit_test()
	{
		$this->load->library('unit_test');
		$mobiles = array(
			'01243 564 - 444 / 23 /56' 		=> '01243564444',
			'01243 / 23 /56/11/12/32' 		=> '01243235611',
			'01243 564 444' 		=> '01243564444',
			'(08) 62570000' 		=> '0862570000',
			'0906753882' 			=> '0906753882',
			'8556732' 				=> '088556732',
			'8294345 - 8225423' 	=> '088294345',
			'7307576' 				=> '087307576',
			'0839303931/0839302' 	=> '0839303931',
			'(84-8) 3841 3883' 		=> '0838413883',
			'0918488612 -MS HNG' 	=> '0918488612',
			'08 8835 3354 - 0120' 	=> '0888353354',
			'(8)38 392 010' 		=> '0838392010',
			'+84 8 6264 4666' 		=> '0862644666',
			'0912858986 - 0941819' 	=> '0912858986',
			'(08) 6686 1115' 		=> '0866861115',
			'0126 3333 389- 0163' 	=> '01263333389'
		);

		foreach($mobiles as $key => $number)
		{
			$result = $this->convert_pattern($key);
			$this->unit->run($result, $number, $key.' <br/> '.$result.' <br/> '.$number);
		}

		echo $this->unit->report();
	}
}