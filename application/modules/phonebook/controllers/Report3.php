<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report3 extends Admin_Controller {

	public $model = 'phonebook_m';
	private $start_date = '';
	private $end_date = '';

	function __construct()
	{
		parent::__construct();
		// restrict('Phonebook.Index');
		$this->load->model($this->model);
		$this->load->model('phonebook_log_m');
	}

	function index()
	{

	}

	function month($month = false)
	{
		if(!$month)
			$month = date('m');

		$date_from = date('Y-'.$month.'-01');
		$date_end = date('Y-'.$month.'-t');
		$results = $this->phonebook_m->where(array(
			'assign_time >=' => $date_from,
			'assign_time <=' => $date_end,
			'user_id >' => '0',
			))->order_by('assign_time')->get_many_by();

		$data = array();
		$users = array();
		foreach($results as $result)
		{
			$u_id = $result->user_id;

			if(!isset($users[$u_id]))
			{
				$users[$u_id] = array(
					'total'=>0,
					);
			}

			$users[$u_id]['total']++;
			$users[$u_id]['status'][$result->pbook_status] = (!isset($users[$u_id]['status'][$result->pbook_status])) ? 1 : $users[$u_id]['status'][$result->pbook_status] + 1;
		}

		$status_labels = array(
			'total' =>'Tổng',
			'publish' =>'Cấp mới',
			'viewed' =>'Đã xem',
			'sticked' =>'Đánh dấu',
			'deleted' =>'Xóa'
			);
		array_unshift($status_labels, 'Tên');
		array_unshift($status_labels, '#');

		$i = 1;
		foreach($users as $user_id => $user)
		{
			$status = $user['status'];
			$display_name = $this->admin_m->get_field_by_id($user_id, 'display_name');
			$this->table->add_row($i++,$display_name, $user['total'],$status['publish'], $status['viewed'], $status['sticked'], $status['deleted']);
		}
		$this->table->set_heading(array_values($status_labels));
		$this->template->title->set('Thống kê danh bạ khách hàng cấp cho nhân viên');
		$this->template->description->set('Từ '.date('d/m/Y',strtotime($date_from)).' đến '.date('d/m/Y',strtotime($date_end)));
		parent::render(array('content'=>$this->table->generate()), 'blank');
	}

	function called($is_send = 0)
	{
		$month = '';
		if(!$month)
			$month = date('m');
		if(date('w') == 1)
			die('Hôm nay thứ 2 cmnr');
		$time = time();
		$day = date('d',$time) - 1;
		if($day == 0){
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}
		$time = strtotime(date('Y-m-'.$day, $time));
		$day = str_pad($day, 2, "0", STR_PAD_LEFT);

		// $day = '01';
		$date_from = date('Y-'.$month.'-'.$day.' 00:00:00');
		$date_end = date('Y-'.$month.'-'.($day + 1).' 00:00:00');

		// $date_from = '2016-06-01 00:00:00';
		// $date_end = '2016-06-31 00:00:00';
		$title = 'Thống kê cuộc gọi của kinh doanh ngày '.date($day.'/'.$month.'/Y');

 // 203.162.56.196
 // mysql -u cdr -h 203.162.56.196 -p -P 3306
		$config = array('dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => true,
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci');
		$config['hostname'] = '203.162.56.196';
		$config['username'] = 'cdr';
		$config['password'] = 'J8JKjDkoOr';
		$config['database'] = 'asteriskcdrdb';
		$config['dbdriver'] = 'mysqli';

		$db = $this->load->database( $config,true);

		$configERP = array();
		$configERP['hostname'] = '103.56.157.157';
		$configERP['username'] = 'erp.user';
		$configERP['password'] = 'PZ7K8IFmtZ';
		$configERP['database'] = 'erp_webdoctor_vn';
		$configERP['dbdriver'] = 'mysqli';

		$erpDB = $this->load->database( $configERP,true);
		$results = $db->where_in('dcontext',array('from-internal', 'from-did-direct'))->where(array(
			'calldate >=' => $date_from,
			'calldate <=' => $date_end,
			))->order_by('calldate')->get('cdr')->result();
// prd($results);

		$data = array();
		$users = array();
		$exts = array('601','602','603','604','605','606','607','608','609','610','611','612', '613', '614','615', '616', '617', '618', '619', '620','621', '622', '623', '624', '625');
		$exts = array();

		for($i=2; $i<30; $i++)
		{
			$ext = str_pad($i, 2, '0', STR_PAD_LEFT);
			$exts[] = '6'.$ext;
			
		}
		for($i=1; $i<30; $i++)
		{
			$ext = str_pad($i, 2, '0', STR_PAD_LEFT);
			$exts[] = '8'.$ext;
			
		}

		$total = array();
		foreach($results as $result)
		{

			if(!in_array($result->src, $exts))
				continue;

			$u_id = $result->src;

			if(!isset($users[$u_id]))
			{
				$users[$u_id] = array(
					'ext'=>$u_id,
					'total'=>0,
					'called'=>0,
					'status'=> array(),
					);
			}

			$users[$u_id]['total']++;

			!isset($total[$result->disposition]) AND $total[$result->disposition] = 0;
			!isset($total['time']) AND $total['time'] = 0;
			$total[$result->disposition]++;
			$total['time']+=$result->billsec;

			if(!isset($users[$u_id]['status'][$result->disposition]))
				$users[$u_id]['status'][$result->disposition] = array('total'=>0, 'billsec'=> 0, 'duration'=>0);

			$users[$u_id]['status'][$result->disposition]['total']++;
			$users[$u_id]['status'][$result->disposition]['billsec']+= $result->billsec;
			$users[$u_id]['status'][$result->disposition]['duration']+= $result->duration;

			if($result->disposition =='ANSWERED')
			{
				if($result->billsec > 30)
				{
					$users[$u_id]['called']++;
					!isset($total['called']) AND $total['called'] = 0;
						$total['called']++;
				}
			}


		}

		$status_labels = array(
			'' =>'#',
			'name' =>'Tên',
			'total' =>'Tổng',
			'BUSY' =>'Máy bận',
			'NO ANSWER' =>'Gọi không thành công',
			'ANSWERED' =>'Gọi thành công',
			'OK' =>'Gọi > 30s',
			'time_total' =>'Tổng thời gian (s)',
			'percent' =>'Tỉ lệ gọi thành công',
			);

		$i = 1;
		usort($users, function ($a, $b) {
			return $b['total'] > $a['total'] ;
		});

		foreach($users as $user)
		{
			$phone_ext = $user['ext'];
			$status = $user['status'];
			// $user_info = $this->usermeta_m->join('user', 'user.user_id = usermeta.user_id')->where('user_status', 1)->get_by(array('meta_key'=>'phone-ext', 'meta_value'=> $phone_ext));
			$user_info = $erpDB->from('usermeta')->join('user', 'user.user_id = usermeta.user_id')->where('user_status', 1)->where(array('meta_key'=>'phone-ext', 'meta_value'=> $phone_ext))->where_in('role_id', array(9,13))->get()->row();
// prd($user_info);
			if(!empty($user_info->user_id))
			{
				$user_id = $user_info->user_id;

				$display_name = '(#'.$phone_ext.') ';
				$display_name.= $user_info->display_name;
			}
			else
			{
				$display_name = '#'.$phone_ext;
			}
			$answer_total = isset($status['ANSWERED']['total']) ? $status['ANSWERED']['total'] : 0;
			$percent = ($user['total'] >0 ) ? $answer_total / $user['total'] : 0;

			$percent = $percent*100;
			$this->table->add_row(
				$i++,
				$display_name,
				$user['total'],
				isset($status['BUSY']['total']) ? $status['BUSY']['total'] : 0,
				isset($status['NO ANSWER']['total']) ? $status['NO ANSWER']['total'] : 0,
				$answer_total,
				@$user['called'],
				isset($status['ANSWERED']['billsec']) ? myNumber_format($status['ANSWERED']['billsec']).' ('.dateDifference(0,$status['ANSWERED']['billsec'], '','').')' : 0,
				numberformat($percent,2).'%'
				);
		}

		$total['total'] = count($results);
		$answer_total = isset($total['ANSWERED']) ? $total['ANSWERED'] : 0;
		$total['percent'] = ($total['total'] >0) ? $answer_total / $total['total'] : 0;
		$total['percent'] = $total['percent'] * 100;
		//tổng
		$this->table->add_row(
				array('data' => '<b>Tổng</b>',  'colspan' => 2),
				'<b>'.$total['total'].'</b>',
				'<b>'.(isset($total['BUSY']) ? $total['BUSY'] : 0).'</b>',
				'<b>'.(isset($total['NO ANSWER']) ? $total['NO ANSWER'] : 0).'</b>',
				'<b>'.$answer_total.'</b>',
				'<b>'.(isset($total['called']) ? $total['called'] : 0).'</b>',
				'<b>'.(isset($total['time']) ? myNumber_format($total['time']).' ('.dateDifference(0,$total['time'], '','').')' : 0).'</b>',
				'<b>'.numberformat($total['percent'],2).'%'.'</b>'
				);

		$this->table->set_heading(array_values($status_labels));
		$this->table->set_caption('<h2>XẾP HẠNG THÁNH CHĂM CHỈ</h2><br>Thống kê cuộc gọi ngày '.date('d/m/Y',strtotime($date_from)));
		$this->template->title->set('Thống kê cuộc gọi của kinh doanh');
		$this->template->description->set('Thống kê ngày '.date('d/m/Y',strtotime($date_from)));

		$is_send = (int)$is_send;
		if($is_send == 1)
		{
			$this->config->load('table_mail');
			$this->table->set_template($this->config->item('mail_template'));
		}
		$content = $this->table->generate();
		if($is_send == 1)
		{
			$status = $this->send_mail($title, $content);
			var_dump($status);
			echo $content;
			prd();
			return '';
		}
		parent::render(array('content'=>$content), 'blank');
	}

	function send_mail($title = '', $content = '')
	{
		$this->load->library('email');
		$this->email->set_debug(false);
		$this->email->from('report@webdoctor.vn','Report');
		$this->email->to('thonh@webdoctor.vn');
		$this->email->cc('sales@adsplus.vn');
		$this->email->subject($title);
		$this->email->message($content);
		return $this->email->send();
	}
}