<span class="label label-danger">KPI</span> 
<span class="label label-success">Gọi kết nối được</span> 
<span class="label" style="background-color: #00f;">Gọi kết nối >15s</span> 

<?php
$this->template->javascript->add('plugins/chartjs/Chart.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker.css');
?>

		<div class="box-body">
						 <form method="get"  class="&#x20;form-horizontal" url="">
 <div class="row">
 	<div class="form-group col-sm-12">
 	<div class="pull-right col-sm-1">
 			 <input  id="s" name="s" placeholder="123123" value="Lọc" type="submit" class="form-control btn btn-info btn-sm"> </form> 		</div>
 		<div class="pull-right col-sm-2">
 			<div class="input-group2">
 				<button type="button" name="daterange" class="btn btn-default pull-right" id="daterange-btn">
 					<span>
 						<i class="fa fa-calendar"></i> <span><?php echo (($dateFilter != '') ? $dateFilter : 'Chọn ngày cần lọc'); ?></span>
 					</span>
 					<i class="fa fa-caret-down"></i>
 				</button>
 			</div>
 			<input type="hidden" name="datefilter" value="<?= $dateFilter; ?>" />
 		</div>

 	</div>
 </div>
 </div>



<script>

	function writeChart(data, divid)
	{
		$(function(){

			var ctx3 = document.getElementById(divid).getContext("2d");
			ctx3.canvas.height = 200;
			window.myLine = new Chart(ctx3).Line(data, {responsive: true});
		});
	}

</script>
<?php
// prd($users);
foreach($charts as $user_id=>$chart)
	{ ?>


 <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">#<?php echo $users['usersExtArray'][$user_id]; ?> - <?php echo $users['user_name'][$user_id]; ?>  (<?php echo my_date($users['user_working_day'][$user_id]); ?>) - <?php echo ($users['users'][$user_id]->role_id == 13 ? 'TN' : 'NV'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <canvas id="chart_<?php echo $user_id;?>" height="200" width="600"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

<?php	echo '';

}


?>
<script async="async" type="text/javascript">
	<?php
	echo 'var responseNew;';
	foreach($charts as $i=>$chart)
	{
		echo 'responseNew = JSON.parse(\''.$chart.'\' );'."\n
		console.log(responseNew);
		";
			// echo '<canvas id="chart_'.$i.'" height="200" width="900"></canvas>';
		echo 'writeChart(responseNew, "chart_'.$i.'");';
	}

	?>
</script> 


 <script type="text/javascript">
 	$('#daterange-btn').daterangepicker(
 	{
 		ranges: {
 			'Today': [moment(), moment()],
 			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
 			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
 			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
 			'This Month': [moment().startOf('month'), moment().endOf('month')],
 			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
 		},
 		startDate: moment().subtract(29, 'days'),
 		endDate: moment()
 	},
 	function (start, end) {
 		$('#daterange-btn span span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
 		$("input[name=datefilter]").val(start.format('YYYY-MM-DD') + '+' + end.format('YYYY-MM-DD'));
 	}
 	);
</script>