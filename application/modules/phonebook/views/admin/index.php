<div class="row">
<?php

$exts = array();
$exts[] = array('value'=>'', 'text'=> 'Chưa chọn');
for($i=1; $i<30; $i++)
{
  $ext = str_pad($i, 2, '0', STR_PAD_LEFT);
  $exts[] = array('value'=>'6'.$ext, 'text'=> '6'.$ext);
  
}
for($i=1; $i<30; $i++)
{
  $ext = str_pad($i, 2, '0', STR_PAD_LEFT);
  $exts[] = array('value'=>'8'.$ext, 'text'=> '8'.$ext);
  
}

$k = json_encode($exts);
$k = str_replace(array('"value"','"text"','"'), array("value","text", "'"), $k);

echo $this->admin_form->set_col(4,3)->input('Số Ext điện thoại bàn của bạn:','',$this->usermeta_m->get_meta_value($this->admin_m->id,'phone-ext'),'',array('data-original-title' =>  'Ext máy bàn của bạn','data-pk'=>@$this->admin_m->id,'data-type-data'=>'meta_data','data-name'=>'phone-ext','data-type'  =>  'select','data-source'  =>  $k,'is_x_editable'=>'true','data-ajax_call_url'=>'staffs/ajax_edit/' . @$this->admin_m->id));
echo '<br>';
echo $this->admin_form->set_col(6,2)->input('Gọi nhanh','call_number','','Nhập số điện thoại của khách hàng và nhấn gọi', array('id'=>'fast-call2','addon_end'=>'<button type="button" class="btn btn-xs btn-success" id="fast-call"><i class="fa fa-phone"></i> Gọi cho khách</button>'));
$this->admin_form->set_col();
?>
</div>
 

<div class="row">
<div class="col-md-12">
  <b>Chú thích về nút <i>Gọi cho khách</i></b>
   <ol>
  <li>Nhập số điện thoại và nhấn vào nút <b class="label bg-green"><i class="fa fa-phone"></i> Gọi cho khách</b> </li>
  <li><b>Điện thoại bàn</b> của bạn sẽ <b>đổ chuông</b> tương ứng với số đã gọi</li>
  <li>Khi nhấc lên, hệ thống sẽ tiến hành gọi cho khách hàng bằng số Ext của bạn</li>
</ol>
</div>
</div>
 
<?php echo $content;?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Thông tin <span id="span-id"></span></h4>
      </div>
      <div class="modal-body">
        <?php foreach($rows as $key =>$title): ?>
          <div class="row">
            <div class="col-md-3"><b class="pull-right"><?php echo $title;?>:</b></div>
            <div class="col-md-9"  id="<?php echo $key;?>"></div>
          </div>
        <?php endforeach;?>
        <?php  echo $this->admin_form->form_open('',array('id'=>'info_customer'));
        echo $this->admin_form->hidden('','id',0,'', array('id'=>'id')); 
        ?>
        <div class="row">
         <div class="col-md-3"> <button type="button" class="btn btn-block btn-success btn-sm" value="" id="callback"><i class="fa fa-phone"></i> Gọi cho khách </button></div>
         <div class="col-md-2"> <button type="submit" class="btn btn-block btn-info btn-sm" name="submit-type" value="save"><i class="fa fa-save"></i> Lưu lại </button></div>
         <div class="col-md-2"> <button type="submit" class="btn btn-block btn-danger btn-sm" name="submit-type" value="delete"><i class="fa fa-trash"></i> Xóa</button></div>
       </div>
     </div>
     <div class="modal-footer">
     </div>
   </div>
 </div>
</div>

<iframe src="" id="aaaaaa" width="1" height="1"></iframe>
<script type="text/javascript">

  $('.btn-ajax').click(function(){
    var id = $(this).data('id');
    var jqxhr = $.get( "<?php echo module_url();?>get/"+id, function(data) {

      $.each(data, function(arrayID,group) {
        // console.log(group);
        $('#id').val(data.id);
        $('#span-id').text('#'+data.id);
        $('#name').text(data.name);
        $('#customer').text(data.customer);
        $('#addr').text(data.addr);
        $('#cate').text(data.cate);
        $('#mail').text(data.mail);
        $('#phone').text(data.phone);
        $('#date_est').text(data.date_est);
      });

      $('#myModal').modal({
        show: 'true'
      }); 
    })
    .done(function() {
      // alert( "second success" );
    })
    .fail(function() {
      // alert( "error" );
    })
    .always(function() {
      // alert( "finished" );
    });

    return false;
  });

  $('#callback').click(function(){
    var id = $('#info_customer #id').val();

    $.get( "<?php echo module_url();?>callback/"+id, function(data) {

      // $('#aaaaaa').attr('src', data);
      return false;
    });

    return false;
  });

  $('#fast-call').click(function(){
    var phone = $('input[name="call_number"]').val();
    $.get( "<?php echo module_url();?>callback/call/"+phone, function(data) {
      // $('#aaaaaa').attr('src', data);
      return false;
    });
    return false;
  });

</script>