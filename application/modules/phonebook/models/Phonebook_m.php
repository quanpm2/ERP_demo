<?php

if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Phonebook_m extends Base_Model {

	public $_table = 'phonebook';
	public $primary_key = "pbook_id";
	
	function __construct() 
	{
		parent::__construct();
	}
}