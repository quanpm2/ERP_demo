<?php

if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Report_phonebook_m extends Base_Model {

	public $_table = 'phonebook';
	public $primary_key = "pbook_id";
	private $_db_voiip = null;
	private $_db_erp = null;
	
	function __construct() 
	{
		parent::__construct();
	}

	function calc_business_days($time_start = 0, $time_end = 0)
	{
		$this->load->library('mdate');
		if($time_start ==0)
		{
			$time_start = time();
		}
		$time_start = $this->mdate->startOfDay($time_start);

		if($time_end == 0)
		{
			//end month
			$time_end = $this->mdate->startOfMonth(strtotime('next month')) - 1;
		}
		$busDays = 0;
		while(true)
		{
			if($time_start > $time_end)
				break;
			// 0 (for Sunday) through 6 (for Saturday)
			//not in weekends 
			if(!in_array(date('w',$time_start), array(6,0)))
			{
				$busDays++;
			}
			$time_start = strtotime('+1 day', $time_start);
		}
		return $busDays;
	}

	function get_called_result($date_from,  $date_end, $ext = false)
	{
		$db = $this->_connect_db_callcenter();
		$where =  array();
		$where['calldate >='] = $date_from;
		$where['calldate <='] = $date_end;
		if($ext !== false)
			$where['src'] = $ext;
		$results = $db->where_in('dcontext',array('from-internal', 'from-did-direct','02873088088','02873004488','02473091616','2873088088','2873004488','2473091616'))->where($where)->order_by('calldate')->get('cdr')->result();
		return $results;
	}
	
	function build_tree_called($results)
	{
		if(!$results)
			return [];
		$calleds = array();
		foreach($results as $result)
		{
			$calldate = date('Y-m-d', strtotime($result->calldate));
			$ext = $result->src;

			if(strlen($ext) > 3)
				continue;

			if(!isset($calleds[$calldate][$ext]['status'][$result->disposition]))
			{
				$calleds[$calldate][$ext]['status'][$result->disposition] = array('total'=>0, 'billsec'=> 0, 'duration'=>0);
			}

			if(!isset($calleds[$calldate][$ext]['callKPI']))
			{
				$calleds[$calldate][$ext]['callKPI'] = 0;
			}
			if(!isset($calleds[$calldate][$ext]['total']))
				$calleds[$calldate][$ext]['total'] = 0;
			$calleds[$calldate][$ext]['total']++;
			$calleds[$calldate][$ext]['status'][$result->disposition]['total']++;
			$calleds[$calldate][$ext]['status'][$result->disposition]['billsec']+= $result->billsec;
			$calleds[$calldate][$ext]['status'][$result->disposition]['duration']+= $result->duration;

			if($result->disposition =='ANSWERED')
			{
				if($result->billsec > 0)
				{
					$calleds[$calldate][$ext]['callKPI']++;
				}
			}

		}
		return $calleds;
	}

	function calc_total_from_ext($results, $date_from, $date_end)
	{

		$data = array();
		$point = 1;
		$time_from = strtotime($date_from);
		$time_end = strtotime($date_end);

		while(true)
		{

			$point++;
			if($point > 1000)
				break;

			if($time_from > $time_end)
				break;

			$date = date('Y-m-d', $time_from);
			$time_from = strtotime('+1 day',$time_from);

			if(!isset($results[$date]))
				continue;
			$result = $results[$date];

			foreach($result as $ext=>$r)
			{
				if(!isset( $data[$ext]))
					$data[$ext] = array();

				if(!isset( $data[$ext]['callKPI']))
					$data[$ext]['callKPI'] = 0;

				if(!isset( $data[$ext]['total']))
					$data[$ext]['total'] = 0;

				if(isset($r['status']))
				{
					if(!isset($data[$ext]['status']))
						$data[$ext]['status'] = array();

					foreach($r['status'] as $key =>$times)
					{
						foreach($times as $d =>$t)
						{
							// prd($times );
							if(!isset($data[$ext]['status'][$key][$d]))
							{
								$data[$ext]['status'][$key][$d] = 0;
							}
							$data[$ext]['status'][$key][$d] += $t;
						}
					}
				}
				
				$data[$ext]['total'] += $r['total'];
				$data[$ext]['callKPI'] += $r['callKPI'];
			}
		}
		return $data;
	}

	function get_users()
	{

		$erpDB = $this->_connect_db_erp();

		$roleLeader = 13;
		$roleMember = 9;

		$tmpUsers = $erpDB->from('user')->where('user_status', 1)->where_in('role_id', array($roleLeader, $roleMember))->get()->result();
		$user_name = key_value($tmpUsers, 'user_id', 'display_name');
		$users = [];
		foreach($tmpUsers as $user)
		{
			$users[$user->user_id] = $user;
		}
		$usersExt = $erpDB->from('usermeta')->where('meta_key', 'phone-ext')->get()->result();
		$usersExtArray = array();
		$usersExtIDArray = array();
		foreach($usersExt as $user)
		{
			$usersExtArray[$user->user_id] = $user->meta_value;
			$usersExtIDArray[$user->meta_value] = $user->user_id;
		}

		$userTimeKPI = array();
		$usersKPI = $erpDB->from('usermeta')->where('meta_key', 'user_working_day')->get()->result();
		foreach($usersKPI as $user)
		{
			$userTimeKPI[$user->user_id] = $user->meta_value;
		}

		return array(
			'roleLeader' =>$roleLeader,
			'roleMember' =>$roleMember,
			'users' =>$users,
			'user_name' =>$user_name,
			'usersExtArray' =>$usersExtArray,
			'usersExtIDArray' =>$usersExtIDArray,
			'user_working_day' =>$userTimeKPI,
			);
	}
	function _connect_db_callcenter()
	{
		if($this->_db_voiip !== null)
			return $this->_db_voiip;

		$config = array('dbprefix' => '',
			'dsn' => 'mysql://cdr:J8JKjDkoOr@203.162.56.196/asteriskcdrdb',
			'pconnect' => FALSE,
			'db_debug' => true,
			'cache_on' => FALSE,
			'cachedir' => '',
			'port' => 3306,
			'char_set' => 'utf8',
			'dbcollat' => 'utf8_general_ci');
		$config['hostname'] = '203.162.56.196';
		$config['username'] = 'cdr';
		$config['password'] = 'J8JKjDkoOr';
		$config['database'] = 'asteriskcdrdb';
		$config['dbdriver'] = 'mysqli';

		$this->_db_voiip = $this->load->database( $config,true);
		return $this->_db_voiip;
	}

	function _connect_db_erp()
	{
		return $this->db;
		if($this->_db_erp !== null)
			return $this->_db_erp;
		$configERP = array();
		$configERP['hostname'] = '103.56.157.157';
		$configERP['username'] = 'erp.user';
		$configERP['password'] = 'PZ7K8IFmtZ';
		$configERP['database'] = 'erp_webdoctor_vn';
		$configERP['dbdriver'] = 'mysqli';
		$configERP['pconnect'] = TRUE;

		$this->_db_erp = $this->load->database( $configERP,true);
		return $this->_db_erp;
	}
}