<?php

if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Phonebook_log_m extends Base_Model {

	public $_table = 'phonebook_log';
	public $primary_key = "plog_id";
	
	function __construct() 
	{
		parent::__construct();
	}
}