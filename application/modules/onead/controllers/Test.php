<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;
use GuzzleHttp\Client as Guzzle;

class Test extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function send_init_service_email($id)
	{
		$this->load->model('onead/onead_report_m');
		$this->load->model('onead/onead_m');

		$contract = $this->onead_m->set_term_type()->get($id);

		$this->onead_report_m->init($contract);
		$this->onead_report_m->send_init_service_email();
		dd($this->onead_report_m);
	}

	public function send_contract_verified_email($id)
	{
		$this->load->model('onead/onead_m');
		$term = $this->onead_m->get($id);
		
		$this->load->model('contract/common_report_m');
		$this->common_report_m->init($term);
		$this->common_report_m->send_contract_verified_email();
	}
}
/* End of file Test.php */
/* Location: ./application/modules/googleads/controllers/Test.php */