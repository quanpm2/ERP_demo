<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class OneadDataset extends MREST_Controller
{
    protected $permission = 'onead.index.access';

    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'onead_m';

        parent::__construct();

        $this->load->config('onead/onead');
        $this->load->config('contract/contract');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response   = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        $relate_users       = $this->admin_m->get_all_by_permissions($this->permission);

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        if($relate_users === FALSE)
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        if(is_array($relate_users))
        {
            $this->datatable_builder->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
        }

        // Prepare search
        $contract_status_config = $this->config->item('contract_status');

        $service_packages_config = $this->config->item('service', 'packages');
        $service_packages_config = key_value($service_packages_config, 'name', 'label');

        // Build datatable
        $data = $this->data;
        $this->search_filter_onead();

        $data['content'] = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')
            ->select('term.term_id, term.term_status')

            ->add_search('contract_code', ['placeholder' => 'Hợp đồng'])
            ->add_search('service_package', ['content' => form_dropdown(['name' => 'where[service_package]', 'class' => 'form-control select2'], prepare_dropdown($service_packages_config, 'Gói dịch vụ'), parent::get('where[service_package]'))])
            ->add_search('contract_begin', ['placeholder' => 'T/G bắt đầu', 'class' => 'form-control set-datepicker'])
            ->add_search('contract_end', ['placeholder' => 'T/G kết thúc', 'class' => 'form-control set-datepicker'])
            ->add_search('quantity', ['placeholder' => 'Số lượng cài đặt'])
            ->add_search('term_status', ['content' => form_dropdown(['name' => 'where[term_status]', 'class' => 'form-control select2'], prepare_dropdown($contract_status_config, 'Trạng thái'), parent::get('where[term_status]'))])
            ->add_search('contract_value', ['placeholder' => 'GTHĐ (-VAT)'])
            ->add_search('payment_percentage', ['placeholder' => 'T/đ thanh toán'])
            ->add_search('staff_business', ['placeholder' => 'Nhân viên kinh doanh'])

            ->add_column('contract_code', array('title' => 'Hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_package', array('title' => 'Gói dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_begin', array('title' => 'T/G bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_end', array('title' => 'T/G kết thúc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('quantity', array('title' => 'Số lượng cài đặt', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('term_status', array('title' => 'Trạng thái', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_value', array('title' => 'GTHĐ (-VAT)', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('payment_percentage', array('title' => 'T/đ thanh toán', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('staff_business', array('title' => 'Nhân viên kinh doanh', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('action', array('title' => 'Hành động', 'set_select' => FALSE, 'set_order' => TRUE))

            ->add_column_callback('term_id', function ($data, $row_name) use ($service_packages_config, $contract_status_config) {
                $term_id = (int)$data['term_id'];

                $data['term_id'] = $term_id;

                $data['term_status_raw'] = $data['term_status'];
                $data['term_status'] = $contract_status_config[$data['term_status_raw']] ?? '--';

                // Get contract_code
                $contract_code = get_term_meta_value($term_id, 'contract_code');
                $data['contract_code'] = $contract_code ?? '--';

                // Get service_package
                $service_package = get_term_meta_value($term_id, 'service_package');
                $data['service_package'] = $service_packages_config[$service_package] ?? '--';

                // Get contract_begin
                $contract_begin = get_term_meta_value($data['term_id'], 'contract_begin');
                $data['contract_begin'] = $contract_begin ? my_date($contract_begin, 'd/m/Y') : '--';

                // Get contract_end
                $contract_end = get_term_meta_value($data['term_id'], 'contract_end');
                $data['contract_end'] = $contract_end ? my_date($contract_end, 'd/m/Y') : '--';

                // Get quantity
                $quantity = get_term_meta_value($data['term_id'], 'quantity');
                $data['quantity'] = '' . $quantity ? (int)$quantity : '--';

                // Get contract_value
                $contract_value = get_term_meta_value($data['term_id'], 'contract_value');
                $data['contract_value'] = '' . $contract_value ? currency_numberformat($contract_value, 'đ') : '--';

                // Get payment_percentage
                $invs_amount = (float)get_term_meta_value($term_id, 'invs_amount');
                $payment_amount = (float)get_term_meta_value($term_id, 'payment_amount');
                $payment_percentage = 100 * (float) get_term_meta_value($term_id, 'payment_percentage');
                $data['payment_progress'] = [
                    'payment_amount' => $payment_amount,
                    'invs_amount' => $invs_amount,
                    'payment_percentage' => $payment_percentage,
                ];

                // Get staff_business
                $sale_id = get_term_meta_value($data['term_id'], 'staff_business');
                $staff_business = $this->admin_m->get_field_by_id($sale_id, 'display_name');
                $data['staff_business'] = $staff_business ?: '--';

                // Add on
                $data['is_service_proc'] = is_service_proc($term_id);
                $data['is_service_end'] = is_service_end($term_id);

                return $data;
            }, FALSE)
            ->where('term_status', 'publish')
            ->where('term_type', $this->onead_m->term_type)
            ->from('term');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);
        // dd($this->datatable_builder->last_query());

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export_onead($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function search_filter_onead($args = array())
    {
        restrict($this->permission);

        $args = parent::get();
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $alias = uniqid('contract_code_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_contract_code);
            }
        }

        // term_status FILTERING & SORTING
        $filter_term_status = $args['where']['term_status'] ?? FALSE;
        if (!empty($filter_term_status)) {
            $this->datatable_builder->like('term.term_status', $filter_term_status);
        }

        $sort_term_status = $args['order_by']['term_status'] ?? FALSE;
        if (!empty($sort_term_status)) {
            $this->datatable_builder->order_by('term.term_status', $sort_term_status);
        }

        // service_package FILTERING & SORTING
        $filter_service_package = $args['where']['service_package'] ?? FALSE;
        $sort_service_package = $args['order_by']['service_package'] ?? FALSE;
        if ($filter_service_package || $sort_service_package) {
            $alias = uniqid('service_package_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'service_package'", 'LEFT');

            if ($filter_service_package) {
                $this->datatable_builder->where("{$alias}.meta_value", $filter_service_package);
            }

            if ($sort_service_package) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_service_package);
            }
        }

        // contract_begin FILTERING & SORTING
        $filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
        $sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
        if ($filter_contract_begin || $sort_contract_begin) {
            $dates = explode(' - ', $args['where']['contract_begin']);

            $alias = uniqid('contract_begin_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

            if ($filter_contract_begin) {
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_begin) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_contract_begin);
            }
        }

        // contract_end FILTERING & SORTING
        $filter_contract_end = $args['where']['contract_end'] ?? FALSE;
        $sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
        if ($filter_contract_end || $sort_contract_end) {
            $dates = explode(' - ', $args['where']['contract_end']);

            $alias = uniqid('contract_end_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'contract_end'", 'LEFT');

            if ($filter_contract_end) {
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_end) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_contract_end);
            }
        }

        // quantity FILTERING & SORTING
        $filter_quantity = $args['where']['quantity'] ?? FALSE;
        $sort_quantity = $args['order_by']['quantity'] ?? FALSE;
        if ($filter_quantity || $sort_quantity) {
            $alias = uniqid('quantity_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'quantity'", 'LEFT');

            if ($filter_quantity) {
                $this->datatable_builder->where("{$alias}.meta_value >=", (int)$filter_quantity);
            }

            if ($sort_quantity) {
                $this->datatable_builder->order_by("cast({$alias}.meta_value as unsigned)", $sort_quantity);
            }
        }

        // contract_value FILTERING & SORTING
        $filter_contract_value = $args['where']['contract_value'] ?? FALSE;
        $sort_contract_value = $args['order_by']['contract_value'] ?? FALSE;
        if ($filter_contract_value || $sort_contract_value) {
            $alias = uniqid('contract_value_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'contract_value'", 'LEFT');

            if ($filter_contract_value) {
                $this->datatable_builder->where("{$alias}.meta_value >=", (int)$filter_contract_value);
            }

            if ($sort_contract_value) {
                $this->datatable_builder->order_by("cast({$alias}.meta_value as unsigned)", $sort_contract_value);
            }
        }

        // payment_percentage FILTERING & SORTING
        $filter_payment_percentage = $args['where']['payment_percentage'] ?? FALSE;
        $sort_payment_percentage = $args['order_by']['payment_percentage'] ?? FALSE;
        if ($filter_payment_percentage || $sort_payment_percentage) {
            $alias = uniqid('payment_percentage_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'payment_percentage'", 'LEFT');

            if ($filter_payment_percentage) {
                $this->datatable_builder->where("{$alias}.meta_value >=", (int)$filter_payment_percentage);
            }

            if ($sort_payment_percentage) {
                $this->datatable_builder->order_by("cast({$alias}.meta_value as unsigned)", $sort_payment_percentage);
            }
        }

        // staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if ($filter_staff_business || $sort_staff_business) {
            $alias_staff_business = uniqid('staff_business_');
            $alias_user = uniqid('staff_business_');

            $this->datatable_builder->join("termmeta {$alias_staff_business}", "term.term_id = {$alias_staff_business}.term_id and {$alias_staff_business}.meta_key = 'staff_business'", 'LEFT')
                ->join("user {$alias_user}", "{$alias_staff_business}.meta_value = {$alias_user}.user_id and {$alias_user}.user_type = 'admin'", 'LEFT');

            if ($filter_staff_business) {
                $this->datatable_builder->like("{$alias_user}.display_name", $filter_staff_business);
            }

            if ($sort_staff_business) {
                $this->datatable_builder->order_by("{$alias_user}.display_name", $sort_staff_business);
            }
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function export_onead($query = '')
    {
        restrict($this->permission);

        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $oneads = $this->onead_m->query($query)->result();
        if (!$oneads) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $this->load->library('excel');
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array('memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
            ->getProperties()
            ->setCreator("WEBDOCTOR.VN")
            ->setLastModifiedBy("WEBDOCTOR.VN")
            ->setTitle(uniqid('Danh sách dịch vụ 1Ad __'));

        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/onead/onead-service-list-export.xlsx');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $row_index = 3;

        foreach ($oneads as $key => &$onead) {
            // Prepare data
            $term_id = (int)$onead->term_id;

            $contract_code = get_term_meta_value($term_id, 'contract_code') ?? '--';

            $service_packages_config = $this->config->item('service', 'packages');
            $service_packages_config = key_value($service_packages_config, 'name', 'label');
            $service_package = get_term_meta_value($term_id, 'service_package') ?? '--';
            $service_package = $service_packages_config[$service_package] ?? '--';

            $contract_begin = get_term_meta_value($onead->term_id, 'contract_begin');
            $contract_begin = $contract_begin ? my_date($contract_begin, 'd/m/Y') : '--';

            $contract_end = get_term_meta_value($onead->term_id, 'contract_end');
            $contract_end = $contract_end ? my_date($contract_end, 'd/m/Y') : '--';

            $quantity = get_term_meta_value($onead->term_id, 'quantity');
            $quantity = '' . $quantity ? (int)$quantity : '--';

            $contract_value = get_term_meta_value($onead->term_id, 'contract_value');
            $contract_value = $contract_value ?? '--';

            $payment_percentage = 100 * (float) get_term_meta_value($term_id, 'payment_percentage');
            $payment_percentage = $payment_percentage;

            // Get staff_business
            $sale_id = get_term_meta_value($onead->term_id, 'staff_business');
            $staff_business = $this->admin_m->get_field_by_id($sale_id, 'display_name');
            $staff_business = $staff_business ?: '--';

            $row_number = $row_index + $key;

            $i = 0;

            // Set Cell
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $key + 1);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_code);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $service_package);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_begin);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_end);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $quantity);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_value);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $payment_percentage);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $staff_business);
        }

        $numbers_of_onead = count($oneads);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $row_index . ':A' . ($numbers_of_onead + $row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

        $objPHPExcel->getActiveSheet()
            ->getStyle('D' . $row_index . ':D' . ($numbers_of_onead + $row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        $objPHPExcel->getActiveSheet()
            ->getStyle('E' . $row_index . ':E' . ($numbers_of_onead + $row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        $objPHPExcel->getActiveSheet()
            ->getStyle('G' . $row_index . ':G' . ($numbers_of_onead + $row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

        $objPHPExcel->getActiveSheet()
            ->getStyle('H' . $row_index . ':H' . ($numbers_of_onead + $row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $file_name = "tmp/export-onead-service.xlsx";
        $objWriter->save($file_name);

        try {
            $objWriter->save($file_name);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        if (file_exists($file_name)) {
            $this->load->helper('download');
            force_download($file_name, NULL);
        }

        return FALSE;
    }
}
/* End of file OneadDataset.php */
/* Location: ./application/modules/onead/controllers/api_v2/OneadDataset.php */