<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class Resource extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'form_validation';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'contract/contract_report_m';
        $this->autoload['models'][] = 'log_m';
        $this->autoload['models'][] = 'onead_m';
        $this->autoload['models'][] = 'onead_contract_m';
        $this->autoload['models'][] = 'option_m';
        $this->autoload['models'][] = 'onead_kpi_m';
        $this->autoload['models'][] = 'admin_m';

        parent::__construct();

        $this->load->config('onead/onead');
        $this->load->config('contract/contract');
    }

    public function index_get($contractId = 0)
    {
        if (!has_permission('onead.setting.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), ['contractId' => $contractId]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->onead_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::response(['code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array()]);
        }

        if ((int)$contractId > 0) {
            $this->get_by($contractId);
        } else {
            $this->get_many();
        }
    }

    protected function get_by($contractId)
    {
        $data = [];

        $data['term_id'] = $contractId;

        $contract_code = get_term_meta_value($contractId, 'contract_code');
        $data['contract_code'] = $contract_code ?: '';

        $contract_begin = get_term_meta_value($contractId, 'contract_begin');
        $data['contract_begin'] = isset($contract_begin) ? my_date($contract_begin, 'd/m/Y') : '';

        $contract_end = get_term_meta_value($contractId, 'contract_end');
        $data['contract_end'] = isset($contract_end) ? my_date($contract_end, 'd/m/Y') : '';

        $contract_region = get_term_meta_value($contractId, 'contract_region');
        $data['contract_region'] = $contract_region ?: '';

        $contract_value = get_term_meta_value($contractId, 'contract_value');
        $data['contract_value'] = (int) $contract_value ?: 0;

        $start_server_time = get_term_meta_value($contractId, 'start_server_time');
        $data['start_server_time'] = isset($start_server_time) ? my_date($start_server_time, 'd/m/Y') : '';

        $service_packages_config = $this->config->item('service', 'packages');
        $service_packages_config = key_value($service_packages_config, 'name', 'label');
        $service_package = get_term_meta_value($contractId, 'service_package');
        $data['service_package'] = $service_packages_config[$service_package] ?? '';

        $representative_gender = get_term_meta_value($contractId, 'representative_gender');
        $data['representative_gender'] = $representative_gender ?: '';

        $representative_name = get_term_meta_value($contractId, 'representative_name');
        $data['representative_name'] = $representative_name ?: '';

        $representative_email = get_term_meta_value($contractId, 'representative_email');
        $data['representative_email'] = $representative_email ?: '';

        $representative_phone = get_term_meta_value($contractId, 'representative_phone');
        $data['representative_phone'] = $representative_phone ?: '';

        $sale_id = get_term_meta_value($contractId, 'staff_business');
        $data['staff_business_id'] = $sale_id ?: '';
        $data['staff_business_name'] = $this->admin_m->get_field_by_id($sale_id, 'display_name') ?: '';
        $data['staff_business_email'] = $this->admin_m->get_field_by_id($sale_id, 'user_email') ?: '';
        $data['staff_business_avatar'] = $this->admin_m->get_field_by_id($sale_id, 'user_avatar') ?: '';

        $data['technical_staffs'] = [];
        $kpis = $this->onead_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(['term_id' => $contractId]);
        if (empty($kpis)) {
            $data['technical_staffs'] = [];
        } else {
            foreach ($kpis as $kpi) {
                $tech_name = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');
                $tech_email = $this->admin_m->get_field_by_id($kpi['user_id'], 'user_email');
                $tech_avatar = $this->admin_m->get_field_by_id($kpi['user_id'], 'user_avatar');
                $staff = [
                    'id' => $kpi['user_id'],
                    'name' => $tech_name,
                    'email' => $tech_email,
                    'avatar' => $tech_avatar,
                ];
                $data['technical_staffs'][] = $staff;
            }
        }

        return parent::responseHandler($data, 'Lấy thông tin thành công.');
    }

    protected function get_many()
    {
    }

    /**
     * @return json
     */
    public function setting_get($contractId)
    {
        if (!has_permission('onead.setting.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), ['contractId' => $contractId]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->onead_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::response(['code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array()]);
        }

        // Process
        $kpis = $this->onead_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(['term_id' => $inputs['contractId']]);

        $data = [];
        foreach ($kpis as $kpi) {
            $item = [];

            $item['kpi_id'] = $kpi['kpi_id'];
            $item['user_id'] = $kpi['user_id'];
            $item['display_name'] = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');
            $item['user_email'] = $this->admin_m->get_field_by_id($kpi['user_id'], 'user_email');

            $data[] = $item;
        }

        return parent::responseHandler($data, 'Lấy dữ liệu thành công.');
    }

    /**
     * @return json
     */
    public function staffs_get()
    {
        if (!has_permission('onead.setting.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $role_ids = $this->option_m->get_value('group_1ad_ids', TRUE);
        $staffs = $this->admin_m->select('user_id, display_name, user_email')
            ->where_in('role_id', $role_ids)
            ->set_get_active()
            ->order_by('display_name')
            ->as_array()
            ->get_all();
        if (empty($staffs)) {
            return parent::responseHandler(null, 'Không có dữ liệu kỹ thuật viên.', 'success', 201);
        }

        return parent::responseHandler($staffs, 'Lấy dữ liệu thành công.');
    }

    /**
     * @return json
     */
    public function kpi_post($contractId)
    {
        if (!has_permission('onead.kpi.add')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::post(), ['contractId' => $contractId]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->onead_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'staffId',
                'label' => 'staffId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', function ($value) {
                        return $this->admin_m->existed_check($value, 'group_1ad_ids');
                    })
                ]
            ]
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::response(['code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array()]);
        }

        // Process
        // ??? Hop dong trang thai nao duoc add tech staff
        $kpi_datetime = time();
        $this->onead_kpi_m->update_kpi_value($inputs['contractId'], 'account_type', 1, $kpi_datetime, $inputs['staffId']);

        $this->term_users_m->set_relations_by_term($inputs['contractId'], [$inputs['staffId']], 'admin');

        $this->onead_kpi_m->delete_cache(0);

        return parent::responseHandler([], 'Thêm kỹ thuật thành công.');
    }

    /**
     * @return json
     */
    public function kpi_delete($contractId, $kpiId)
    {
        if (!has_permission('onead.kpi.delete')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::delete(), [
            'contractId' => $contractId,
            'kpiId' => $kpiId
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->onead_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'kpiId',
                'label' => 'kpiId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->onead_kpi_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $this->onead_kpi_m->delete_cache($kpiId);
        $this->onead_kpi_m->delete($kpiId);

        return parent::responseHandler([], 'Xoá kỹ thuật thành công.');
    }

    /**
     * @return json
     */
    public function start_contract_post($contractId)
    {
        if (!has_permission('onead.start_service.update')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::post(), [
            'contractId' => $contractId,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->onead_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $term = $this->onead_m->set_term_type()->where('term_id', $inputs['contractId'])->get_by();

        $is_proc_service = $this->onead_contract_m->proc_service($term);
        if (!$is_proc_service) {
            return parent::responseHandler([], 'Có lỗi trong quá trình khởi chạy.', 'error', 400);
        }

        /* Phân tích hợp đồng ký mới | tái ký */
        $this->base_contract_m->detect_first_contract($inputs['contractId']);


        /* Gửi SMS thông báo kích hoạt hợp đồng đến khách hàng */
        $this->contract_report_m->send_sms_activation_2customer($inputs['contractId']);

        return parent::responseHandler([], 'Dịch vụ khởi chạy thành công.');
    }

    /**
     * @return json
     */
    public function stop_contract_post($contractId)
    {
        if (!has_permission('onead.stop_service.update')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::post(), [
            'contractId' => $contractId,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->onead_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $term = $this->onead_m->set_term_type()->where('term_id', $inputs['contractId'])->get_by();

        $is_stop_service = $this->onead_contract_m->stop_service($term);
        if (!$is_stop_service) {
            return parent::responseHandler([], 'Có lỗi trong quá trình kết thúc hợp đồng.', 'error', 400);
        }

        return parent::responseHandler([], 'Hợp đồng đã chuyển sang trạng thái "Đã kết thúc".');
    }
}
/* End of file Resource.php */
/* Location: ./application/modules/onead/controllers/api_v2/Resource.php */