<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends REST_Controller
{
	function __construct()
	{
		parent::__construct('onead/rest');
		$this->load->model('usermeta_m');
		$this->load->model('term_m');
		$this->load->model('termmeta_m');
		$this->load->model('permission_m');
		$this->load->model('staffs/admin_m');
		$this->load->model('contract/contract_m');
		$this->load->model('onead/onead_m');
		$this->load->model('term_users_m');
	}

	public function index_get()
	{
		$default 	= array('id' => '', 'meta' => '', 'field' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if( empty($args['id']) || empty($args['meta'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);
		
		$data 			= array('msg' => 'Xử lý không thành công', 'data' => []);

		if(FALSE === $this->contract_m->has_permission($args['id'], 'admin.contract.view'))
		{
			$data['msg'] = 'Không có quyền truy xuất hoặc hợp đồng không khả dụng !';
			parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
		}

		$data = array('id' => $args['id']);
		$args['meta'] = is_string($args['meta']) ? explode(',', $args['meta']) : $args['meta'];
		foreach ($args['meta'] as $meta)
		{
			$_value = get_term_meta_value($args['id'], $meta);
			$data[$meta] = is_serialized($_value) ? unserialize($_value) : $_value;
		}		

		$response['data'] = $data;
		parent::response($response);
	}

	public function config_get()
	{
		$default 	= array('name' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if( ! has_permission('admin.contract.view')) parent::response(['msg'=>'Quyền hạn không hợp lệ.'], parent::HTTP_UNAUTHORIZED);

		$this->config->load('onead/onead');

		$response = array(
			'msg' => 'Dữ liệu tải thành công',
			'data' => array(
				'services' => $this->config->item('service', 'packages'),
				'default' => $this->config->item('default', 'packages')
			)
		);

		parent::response($response);
	}
}
/* End of file Contract.php */
/* Location: ./application/modules/onead/controllers/api/Contract.php */