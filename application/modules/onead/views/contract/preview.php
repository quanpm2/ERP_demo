<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>  
<div class="content">
	<div class="row-containter">
		
		<p>Dựa trên: Nhu cầu của Bên A và khả năng cung cấp của Bên B về dịch vụ. Hai bên thống nhất ký kết hợp đồng kinh tế với các điều khoản cụ thể như sau:</p>

		<p><b><u>Điều 1:</u> Điều khoản chung</b></p>
		<p>Bên B nhận cung cấp cho Bên A dịch vụ setup chiến dịch quảng cáo theo các nội dung sau:</p>

		<?php
		if( ! empty($service_package_items))
		{
			$this->table->clear();
			$this->table->set_template(['table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">']);
			$this->table->set_heading('Hạng mục', 'Nội dung', 'Thời gian');
			
			foreach ($service_package_items as $row)
			{
                if(is_array($row['content'])){
                    $list_item = '<ul>';
                    foreach($row['content'] as $item) $list_item .= '<li>' . $item . '</li>';
                    $list_item .= '<ul>';

                    $row['content'] = $list_item;
                }

				$this->table->add_row($row['item'], $row['content'], $row['duration']);
			}

			echo $this->table->generate();
		}
		?>

		<br/>
		<p>Bên B sẽ bàn giao tài khoản quảng cáo qua email cho Bên A. Sau 2 ngày kể từ khi nhận được email từ Bên B, nếu Bên A không có phản hồi khác, xem như Bên A đã đồng ý với kết quả Bên B triển khai.</p>
	</div>
	<div class="row-containter">
		<p><b><u>Điều 2:</u> Giá trị hợp đồng:</b></p>
		<p>Đơn vị tính giá: Việt Nam Đồng (VND).</p>
		<p>Thanh toán bằng tiền đồng Việt Nam.</p>
	</div>
	<div class="row-containter">
		<?php

		$this->table->clear();
		$this->table->set_template(['table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">']);
		$this->table->set_heading('Dịch vụ', 'Đơn giá', 'Số lượng', 'Thành tiền(vnđ)');

		$total = $quantity*$price;
		$this->table->add_row($service_package_config['label'],[ 'data' => currency_numberformat($price, ''), 'align' => 'right' ],[ 'data' => $quantity, 'align' => 'right' ],[ 'data' => currency_numberformat($total), 'align' => 'right' ]);

		if( ! empty($discount_amount))
		{
			$this->table->add_row([ 'data' => '<b>Chương trình Giảm giá</b>', 'colspan' => 3 ], [ 'data'=> '-'.currency_numberformat($discount_amount), 'align' => 'right']);
			$total-= (int) $discount_amount;
		}
		
		if( ! empty($additionServices))
		{
            foreach ($additionServices as $additionService)
			{
                $additionServicePrice = (int) $additionService['value'];
                if(0 == $additionServicePrice) continue;

                $this->table->add_row($additionService['name'], [ 'data'=> currency_numberformat($additionServicePrice, ''), 'align' => 'right'], [ 'data'=> 1, 'align' => 'right'], [ 'data'=> currency_numberformat($additionServicePrice), 'align' => 'right']);
                $total += $additionServicePrice;
			}
		}

		if(!empty($vat))
		{
			$tax_amount 		= $total*div($vat,100);
			$tax_amount_text 	= currency_numberformat($tax_amount);
			$this->table->add_row(['data'=>"VAT {$vat}%",'colspan'=>3],['data'=>"<strong>{$vat}&#37; ({$tax_amount_text}) </strong>",'align'=>'right']);
			$total = cal_vat($total, $vat);
		}
        $total = round($total);

		$this->table->add_row(array('data'=>'Số tiền phải thanh toán', 'colspan'=>3),array('data'=>currency_numberformat($total),'align'=>'right'));

		echo $this->table->generate();

		?>
		<p><em>Bằng chữ: <b><i><?php echo ucfirst(mb_strtolower(convert_number_to_words($total)));?> đồng.</i></b></em><br/>
		</div>
		<div class="row-containter">
			<br/>
			<p><b><u>Điều 3:</u> Phương thức thanh toán</b></p>

			<p>Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin tài khoản do Bên B cung cấp, cụ thể:</p>

			<?php if( ! empty($bank_info)) :?>
		    <ul style="padding-left:0;list-style:none">
		    <?php foreach ($bank_info as $label => $text) :?>
		        <?php if (is_array($text)) : ?>
		            <?php foreach ($text as $key => $value) :?>
		                <li>- <?php echo $key;?>: <?php echo $value;?></li>
		            <?php endforeach;?>
		        <?php continue; endif;?>
		        <li>- <?php echo $label;?>: <?php echo $text;?></li>
		    <?php endforeach;?>
		    </ul>
		    <p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
		    <?php endif; ?>

			<?php

			$args = array(
				'select' => 'posts.post_id, post_title,post_status,start_date,post_content,end_date',
				'tax_query' => array( 'terms'=>$term->term_id),
				'numberposts' =>0,
				'orderby' => 'posts.end_date',
				'order' => 'ASC',
				'post_type' => 'contract-invoices'
			);
			$lists = $this->invoice_m->get_posts($args);

			$is_one_payments = empty($lists) || (!empty($lists) && count($lists) == 1);

			if($is_one_payments)
			{
				echo '<p>- Ngay sau khi hợp đồng đươc ký BÊN A thanh toán cho BÊN B 100% phí dịch vụ</p>';
			}
			else
			{
				$number_of_payments = count($lists);
				$i=1;
				echo '<p>Bên A thanh toán cho Bên B làm '.$number_of_payments.' đợt:</p>';
				echo '<ul style="list-style-type: square;">';
				foreach ($lists as $inv) { 
					$price_total = $this->invoice_item_m->get_total($inv->post_id, 'total');
					$price_total = cal_vat($price_total, $vat);
					$price_total = numberformat($price_total,0,'.','');

					printf('<li> Đợt %s: thanh toán %s (%s), chậm nhất là ngày %s.</li>',
						$i,
						currency_numberformat($price_total,'đ'),
						currency_number_to_words($price_total), 
						my_date($inv->end_date,'d/m/Y'));
					$i++;
				}
				echo '</ul>';
			}
			?>

			<br/>
			<p><b><u>Điều 4:</u> Thời gian thực hiện</b></p>
			<p>- Bên B cam kết bàn giao tài khoản cho Bên A trong vòng <?= $this->mdate->endOfDay(strtotime('2022-09-27')) < $contract_begin ? '72h' : '24h'; ?> kể từ khi Bên A nhận được email xác nhận Bên B đã nhận đủ khoản thanh toán từ Bên A.</p>
			<p>- Sau khi bàn giao nếu Bên A vi phạm các chính sách về quảng cáo dẫn đến việc tài khoản quảng cáo bị ngưng thì Bên B hoàn toàn không chịu trách nhiệm.</p>
			<br/>

			<p><b><u>Điều 5:</u> Quyền lợi</b></p>
			
			<p>- Bên A được Bên B hướng dẫn cụ thể cách sử dụng thông qua các buổi workshop và qua tài liệu online.</p>
			<p>- Bên B sẽ không thực hiện hoàn trả cho Bên A giá trị dịch vụ trong trường hợp Bên A chấm dứt hợp đồng trước hạn.</p>	

			<br/>
			<p><b><u>Điều 6:</u> Sự kiện bất khả kháng</b></p>
			<p>Trong trường hợp có những sự kiện bất khả kháng không lường trước được, trách nhiệm và thời hạn thực hiện hợp đồng của cả hai bên sẽ được xem xét, đàm phán và quyết định lại. Các sự kiện bất khả kháng bao gồm nhưng không giới hạn các rủi ro sau:</p>
			<p>- Rủi ro do ngừng hoặc lỗi kĩ thuật từ dịch vụ hệ thống quảng cáo cung cấp</p>
			<p>- Rủi ro về đường truyền internet, cơ sở hạ tầng mạng quốc gia</p>
			<p>- Thiên tai, chiến tranh, khủng bố, hoả hoạn, dịch bệnh...</p>
			<p>- Bên A không cung cấp kịp thời thông tin tài khoản đăng nhập và các thông tin liên quan cho Bên B qua Email</p>
			

			<br/>
			<p><b><u>Điều 7:</u> Tranh chấp và phân xử</b></p>
			<p>Hai bên cam kết thực hiện những điều khoản trong hợp đồng. Nếu có vướng mắc, mỗi bên thông báo cho nhau để cùng bàn bạc giải quyết trên tinh thần hợp tác, thiện chí, vì lợi ích cả hai bên. Trong trường hợp không thể giải quyết được bất đồng, tranh chấp sẽ được giải quyết tại Tòa án có thẩm quyền tại TP Hồ Chí Minh.</p>

			<br/>
			<p><b><u>Điều 8:</u> Điều khoản khác</b></p>
			<p>- Hợp đồng có hiệu lực kể từ ngày ký và được xem như tự động thanh lý khi Hai bên thực hiện đầy đủ tất cả các điều khoản nêu trong hợp đồng này.</p>
			<p>- Mọi sửa đổi hoặc bổ sung cho Hợp đồng này sẽ chỉ có hiệu lực sau khi các đại diện của cả hai bên kí kết bằng văn bản tạo thành bộ phận hợp nhất của hợp đồng.</p>
			<p>- Hợp đồng này được làm thành 02 bản có giá trị ngang nhau, Bên A giữ 01 bản, Bên B giữ 01 bản. </p>
		</div>
	</div>