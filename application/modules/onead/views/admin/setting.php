<?php

$this->template->stylesheet->add('plugins/tagsinput/tagsinput.css');
$this->template->javascript->add('plugins/tagsinput/tagsinput.js');

$has_start_permission   = has_permission('Onead.start_service');
$has_stop_permission    = has_permission('Onead.stop_service');

$is_service_proc        = is_service_proc($term);
$is_service_end         = is_service_end($term);

if(has_permission('Onead.start_service') && !is_service_proc($term))
{
    $this->admin_form->set_col(12,6);
    echo $this->admin_form->form_open('', 'id="start_process_submit"');

    $btn_submit_proc = array('id'=>'start_process', 'name'=>'start_process', 'type'=>'submit', 'class'=>'btn btn-danger', 'value'=>'start_process','data-toggle'=>'confirmation', 'class' => 'btn btn-default');

    echo form_button($btn_submit_proc, '<i class="fa fa-fw fa-play"></i>Kích hoạt');

    echo $this->admin_form->form_close();
}
else if(has_permission('Onead.stop_service') && is_service_proc($term) && !is_service_proc($term)  || 1==1)
{
    $this->admin_form->set_col(12,6);
    echo $this->admin_form->form_open();

    $btn_submit_proc = array(
        'id'=>'start_process',
        'name'=>'end_process',
        'type'=>'submit',
        'value'=>'end_process',
        'data-toggle'=>'confirmation',
        'class' => 'btn btn-danger'
    );

    echo form_button($btn_submit_proc, '<i class="fa fa-fw fa-stop"></i>Kết thúc dịch vụ');
    echo $this->admin_form->form_close();
}

echo '<br/><hr/>';
$this->admin_form->set_col(6,6);

$rows_id = array();
$data = array();
$start_month = $this->mdate->startOfMonth();

$this->table->set_heading(array('STT','Kỹ thuật thực hiện','Hành động'));
$this->table->set_caption('Kỹ thuật thực hiện');

if( ! empty($targets['account_type']))
{
    $i = 0;
    foreach ($targets['account_type'] as $kpi) 
    {
        $btn_delete = anchor(module_url("kpi/{$term_id}/{$kpi['kpi_id']}/delete"), 'Xóa', 'class="btn btn-danger btn-flat btn-xs"');
        $username   = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');

        $this->table->add_row(++$i, $username, $btn_delete);
    }
}

$_users = key_value(array_map(function($x){
    $x['display_name'] = implode(' - ', array_filter([ $x['display_name'], $x['user_email'] ]));
    return $x;
}, $users), 'user_id', 'display_name');

echo $this->table->generate();
echo $this->admin_form->form_open(module_url("kpi/{$term_id}"));
echo $this->admin_form->dropdown('Nhân viên thực hiện', 'user_id', $_users, '');
echo $this->admin_form->hidden('', 'target_date',time());
echo $this->admin_form->hidden('', 'target_type', 'account_type');
echo $this->admin_form->hidden('', 'target_post', '1');
echo $this->admin_form->submit('submit_kpi_tech', 'Lưu lại');
echo $this->admin_form->form_close();
?>

<script type="text/javascript">
    $(function(){
        $('[data-toggle=confirmation]').confirmation();
        $('#btn-ajax-reload').click(function(){
            $.ajax({url: '<?php echo module_url();?>ajax/setting/ga-reload',dataType: 'json', success: function(result){
                $('form > .form-group  select').eq(1).select2({
                    allowClear: true,
                    data: result
                });

                $('form > .form-group .select2-container').eq(1).css('width','100%');
            }});
        });


        $('#btn-ajax-checkservice').click(function(){
            $.ajax({url: '<?php echo module_url();?>ajax/setting/ga-reload',dataType: 'json', success: function(result){

                alert('12312');
// $('form > .form-group .select2-container').eq(1).css('width','100%');
}});
        });


// To make Pace works on Ajax calls
$(document).ajaxStart(function() { Pace.restart(); });
$('#start_process').click(function(){
    $(this).hide();
    $('#start_process_submit').prepend('<p class="help-block label label-info">Hệ thống đang gửi mail, vui lòng đợi trong giây lát.</p>');
    return true;
});

});
</script>