<table width="100%" border="0" cellspacing="0" cellpadding="0" >
    <tr>
        <td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
            <!-- <p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">Kính chào Quý khách</p> -->
            <p style="font-family: open sans, arial, sans-serif; font-size:15px"><b>Kính chào Quý khách!</b> 1AD.VN cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>
            <p style="font-family: open sans, arial, sans-serif; font-size:15px">Chúng tôi xin thông báo tới quý khách Hợp đồng <?php echo get_term_meta_value($term->term_id,'contract_code'); ?> đã được hoàn thành và bàn giao vào <?php echo my_date(get_term_meta_value($term->term_id,'end_service_time'), 'd/m/Y') ?></p>
            <p style="font-family: open sans, arial, sans-serif; font-size:15px">Các chiến dịch được cài đặt theo đúng tiêu chuẩn cấu trúc tài khoản Google Ads tốt nhất hiện nay.</p>

            <p style="font-family: open sans, arial, sans-serif; font-size:15px">Hướng dẫn: </p>
            <ul>
                <li style="font-family: open sans, arial, sans-serif; font-size:15px">Quy trình nạp tiền vào tài khoản (<a href="https://docs.google.com/document/d/1dykoyttO0ju9Qs-2FFdN5MHmkz9OTgUyTuT2drRhOXo/edit?usp=sharing" target="__blank">tại đây>></a>)</li>
                <li style="font-family: open sans, arial, sans-serif; font-size:15px">Video demo một số thao tác cơ bản với tài khoản Ads (<a href="https://drive.google.com/drive/folders/1WHTbcpgsNz0taUY2FMH4G4yqJYxAce5R?usp=sharing" target="__blank">tại đây>></a>)</li>
                <li style="font-family: open sans, arial, sans-serif; font-size:15px">Video demo hướng dẫn một số thao tác tối ưu quảng cáo (<a href="https://www.youtube.com/watch?v=ZGSId0TuVU0" target="__blank">tại đây>></a>)</li>
            </ul>

            <p style="font-family: open sans, arial, sans-serif; font-size:15px">Trong quá trình <?php echo $days_support; ?> ngày (giờ hành chính trừ thứ 7, Chủ Nhật) kể từ thời điểm hoàn tất bàn giao này, quý khách xem xét cần hỗ trợ gì thêm thì liên hệ em. Chúc quý khách một ngày làm việc hiệu quả!</p>

            <p style="font-family: open sans, arial, sans-serif; font-size:15px">Mọi thắc mắc vui lòng liên hệ với chúng tôi theo thông tin dưới đây:</p>
            <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <?php if(!empty($sale)) { ?>
                     <tr>
                        <td>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">
                                    Kinh doanh <?php echo $sale['display_name'];?>: 
                                </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">
                                    <?php
                                    $phone = get_user_meta_value($sale['user_id'],'user_phone');
                                    if( ! $phone) $phone = '(028) 7300. 4488';
                                    echo $phone; ?>
                                </td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $sale['user_email'];?></td>
                              </tr>
                        </td>
                     </tr>
                  <?php } ?>
                  <tr>
                     <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Hotline Trung tâm Kinh doanh:</td>
                     <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">(028) 7300. 4488</td>
                  </tr>
                  <tr>
                     <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>
                     <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">sales@webdoctor.vn</td>
                  </tr>
                  <tr>
                     <td height="25"></td>
                  </tr>
               </tbody>
            </table>
        </td>
    </tr>

    <tr>
        <td align="center" style="border:20px solid #e6e6e6;"><table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
            <tbody>
                 <!--start Article-->
                  <tr>
                      <td bgcolor="#ffffff">
                          <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                              <tbody>
                                  <tr>
                                      <td height="25"></td>
                                  </tr>
                                  <?php
                                  $contract_date = my_date(get_term_meta_value($term->term_id,'contract_begin'),'d/m/Y').' - '.my_date(get_term_meta_value($term->term_id,'contract_end'),'d/m/Y');
                                  ?>
                                  <?php 
                                  $rows = array(
                                      [ 'Mã hợp đồng', get_term_meta_value($term->term_id,'contract_code') ],
                                      [ 'Thời gian hợp đồng', $contract_date ],
                                      [ 'Gói dịch vụ', $service_package_label ],
                                      [ 'Đơn giá', currency_numberformat($price) ],
                                      [ 'Số lượng', $quantity ]
                                  );

                                  !empty($discount_amount) AND $rows[] = [ 'Giảm theo khuyến mãi',  currency_numberformat($discount_amount) ];
                                  !empty($vat) AND $rows[] = [ 'VAT',  currency_numberformat($vat, '%') ];

                                  $rows[] = [ 'Giá trị hợp đồng',  currency_numberformat($contract_value)];
                                  $rows[] = [ 'Thành tiền',  currency_numberformat( cal_vat($contract_value, $vat))];

                                  if( ! empty($service_package_items))
                                  {
                                      foreach ($service_package_items as $item)
                                      {
                                          $rows[] = [ $item['duration'], $item['content'] ];
                                      }
                                  }
                                  ?>

                                  <?php foreach ($rows as $i => $row): ?>
                                  <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>    
                                  <tr>
                                      <td width="250" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                      <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                      </td>
                                  </tr>
                                  <?php endforeach ?>
                                  <tr>
                                      <td height="25"></td>
                                  </tr>
                              </tbody>
                          </table>
                      </td>
                  </tr>
                  <!--end Article-->
                </tbody>
            </table>
        </td>
    </tr>

    <tr>
        <td align="center" style="border:20px solid #e6e6e6;">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                <tbody>
                    <tr>
                        <td bgcolor="#fff">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                                <tbody>
                                    <tr>
                                        <td bgcolor="#fff">
                                            <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Thông tin khách hàng</p>
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                                                <tbody>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td width="25%" height="25">Tên khách hàng</td>
                                                        <td>: <?php echo 'Anh/Chị '.get_term_meta_value($term->term_id,'representative_name');?></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25">Email</td>
                                                        <td>: <?php echo mailto(get_term_meta_value($term->term_id,'representative_email'));?></td>
                                                    </tr>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td  height="25">Điện thoại di động</td>
                                                        <td>: <?php echo get_term_meta_value($term->term_id,'representative_phone');?></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25">Địa chỉ</td>
                                                        <td>: <?php echo get_term_meta_value($term->term_id,'representative_address');?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>    
                            <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>