<?php
$contract_end = (int)get_term_meta_value($term->term_id, 'contract_end');
$date_diff = $contract_end - time();
$day_remain = round($date_diff / (60 * 60 * 24));
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
            <p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">Kính chào Quý khách</p>
            <p>1AD.VN cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>
            <p>1AD.VN gửi email thông báo dịch vụ của quý khách còn <b><?php echo $day_remain; ?> ngày</b> nữa sẽ kết thúc. Quý khách cần gia hạn để có thể tiếp tục sử dụng dịch vụ của chúng tôi. </p>
        </td>
    </tr>

    <tr>
        <td align="center" style="border:20px solid #e6e6e6;">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                <tbody>
                    <!--start Article-->
                    <tr>
                        <td bgcolor="#ffffff">
                            <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td height="25"></td>
                                    </tr>
                                    <?php
                                    $contract_date = my_date(get_term_meta_value($term->term_id, 'contract_begin'), 'd/m/Y') . ' - ' . my_date(get_term_meta_value($term->term_id, 'contract_end'), 'd/m/Y');
                                    ?>
                                    <?php
                                    $rows = array(
                                        ['Mã hợp đồng', get_term_meta_value($term->term_id, 'contract_code')],
                                        ['Thời gian hợp đồng', $contract_date],
                                        ['Gói dịch vụ', $service_package_label],
                                        ['Đơn giá', currency_numberformat($price)],
                                        ['Số lượng', $quantity]
                                    );

                                    !empty($discount_amount) and $rows[] = ['Giảm theo khuyến mãi',  currency_numberformat($discount_amount)];
                                    !empty($vat) and $rows[] = ['VAT',  currency_numberformat($vat, '%')];

                                    $rows[] = ['Giá trị hợp đồng',  currency_numberformat($contract_value)];
                                    $rows[] = ['Thành tiền',  currency_numberformat(cal_vat($contract_value, $vat))];

                                    if (!empty($service_package_items)) {
                                        foreach ($service_package_items as $item) {
                                            $rows[] = [$item['duration'], $item['content']];
                                        }
                                    }
                                    ?>

                                    <?php foreach ($rows as $i => $row) : ?>
                                        <?php $bg = (($i % 2 == 0) ? 'background: #f2f2ff;' : ''); ?>
                                        <tr>
                                            <td width="250" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[0]; ?>: </td>
                                            <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1]; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                    <tr>
                                        <td height="25"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <!--end Article-->
                </tbody>
            </table>
        </td>
    </tr>

    <tr>
        <td align="center" style="border:20px solid #e6e6e6;">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                <tbody>
                    <tr>
                        <td bgcolor="#fff">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                                <tbody>
                                    <tr>
                                        <td bgcolor="#fff">
                                            <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Thông tin khách hàng</p>
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                                                <tbody>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td width="25%" height="25">Tên khách hàng</td>
                                                        <td>: <?php echo 'Anh/Chị ' . get_term_meta_value($term->term_id, 'representative_name'); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25">Email</td>
                                                        <td>: <?php echo mailto(get_term_meta_value($term->term_id, 'representative_email')); ?></td>
                                                    </tr>
                                                    <tr bgcolor="#f4f8fb">
                                                        <td height="25">Điện thoại di động</td>
                                                        <td>: <?php echo get_term_meta_value($term->term_id, 'representative_phone'); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25">Địa chỉ</td>
                                                        <td>: <?php echo get_term_meta_value($term->term_id, 'representative_address'); ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>