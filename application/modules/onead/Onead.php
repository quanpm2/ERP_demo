<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onead_Package extends Package
{
	public function name()
	{
		return '1AD';
	}

	public function init()
	{
		$this->_load_menu();
	}

	/**
	 * Init menu LEFT | NAV ITEM FOR MODULE
	 * then check permission before render UI
	 */
	private function _load_menu()
	{
		$order = 1;
		$itemId = 'admin-onead';
 
 		if(!is_module_active('onead')) return FALSE;	
 		
		if(has_permission('Onead.Index.access'))
		{
			$this->menu->add_item(array(
				'id'        => $itemId,
				'name' 	    => '1 AD',
				'parent'    => null,
				'slug'      => admin_url('onead'),
				'order'     => $order++,
				'icon' 		=> 'fa fa-lg fa-buysellads',
				'is_active' => is_module('onead')
			),'left-ads-service');	
		}

		if( ! is_module('onead')) return FALSE;

		if(has_permission('Onead.Index.access'))
		{
			$this->menu->add_item(array(
				'id' => 'onead-service',
				'name' => 'Dịch vụ',
				'parent' => $itemId,
				'slug' => admin_url('onead'),
				'order' => $order++,
				'icon' => 'fa fa-fw fa-xs fa-circle-o'
			), 'left-ads-service');
		}
		
		$left_navs = array(
			'overview'=> array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-xs fa-tachometer',
			),
			'setting'  => array(
				'name' => 'Cấu hình',
				'icon' => 'fa fa-fw fa-xs fa-cogs',
			)
		);

		$term_id = $this->uri->segment(4);

		$this->website_id = $term_id;

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("Onead.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "onead-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
			), 'left-ads-service');
		}
	}

	private function _update_permissions()
	{
		$permissions = array();
		// if(!permission_exists('Onead.renew'))
		// 	$permissions['Onead.renew'] = array(
		// 		'description' => 'Bảng phân công phụ trách thực hiện',
		// 		'actions' => array('manage','update'));

		if(!$permissions) return TRUE;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function title()
	{
		return '1AD';
	}

	public function author()
	{
		return 'thonh';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return '1AD';
	}
	
	/**
	 * Init default permissions for 1AD module
	 *
	 * @return     array  permissions default array
	 */
	private function init_permissions()
	{
		return array(

			'Onead.index' => array(
				'description' => 'Quản lý HĐ 1AD.VN',
				'actions' => ['manage','access','update','add','delete']),

			'Onead.done' => array(
				'description' => 'Dịch vụ đã xong',
				'actions' => ['manage','access','add','delete','update']),
		
			'Onead.overview' => array(
				'description' => 'Tổng quan',
				'actions' => ['manage','access','add','delete','update']),

			'Onead.kpi' => array(
				'description' => 'KPI',
				'actions' => ['manage','access','add','delete','update']),

			'Onead.start_service' => array(
				'description' => 'Kích hoạt dịch vụ',
				'actions' => ['manage','access','update']),
			
			'Onead.stop_service' => array(
				'description' => 'kết thúc dịch vụ',
				'actions' => ['manage','access','update']),
			
			'Onead.setting' => array(
				'description' => 'Cấu hình',
				'actions' => ['manage','access','add','delete','update']
			),
		);
	}

	/**
	 * Install module
	 *
	 * @return     bool  status of command
	 */
	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);
			$this->role_permission_m->insert(['role_id'=>1,'permission_id'=>$permission_id,'action'=>serialize($actions)]);
		}

		return TRUE;
	}

	/**
	 * Uninstall module command
	 *
	 * @return     bool  status of command
	 */
	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		if($pers = $this->permission_m->like('name','Onead')->get_many_by())
		{
			foreach($pers as $per)
			{
				$this->permission_m->delete_by_name($per->name);
			}
		}

		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}

		return TRUE;
	}
}
/* End of file Onead.php */
/* Location: ./application/modules/onead/models/Onead.php */