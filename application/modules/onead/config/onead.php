<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['packages'] = [
    'service' => [
        '1ad_googelads_7_days_support' => [
            'label' => 'Gói Set up quảng cáo Google hỗ trợ 7 ngày',
            'name' => '1ad_googelads_7_days_support',
            'short_name' => 'Google',
            'price' => 2400000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'days_support' => 7,
            'items' => [
                [
                    'item' => 'Setup 5 chiến dịch quảng cáo',
                    'content' => 'Lên tối đa 5 chiến dịch quảng cáo đạt chuẩn Google',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 50 từ khoá cạnh tranh',
                    'content' => 'Lên tối đa 50 từ khoá cạnh tranh theo sản phẩm dịch vụ của Bên A',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch Google Remarketing',
                    'content' => 'Lên 1 chiến dịch Remarketing để tận dụng tối đa traffic vào web',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao trực tiếp/online (30 phút) và giải đáp thắc mắc về sử dụng tài khoản trong 7 ngày làm việc tiếp theo',
                    'duration' => 'Trong 168h'
                ],
                [
                    'item' => 'Cài đặt thành công theo dõi chuyển đổi',
                    'content' => 'Gắn tracking để đo lường độ hiệu quả của các chiến dịch, các nhóm quảng cáo',
                    'duration' => 'Trong 24h'
                ]
            ]
        ],

        '1ad_googelads_14_days_support' => [
            'label' => 'Gói Set up quảng cáo Google hỗ trợ 14 ngày',
            'name' => '1ad_googelads_14_days_support',
            'short_name' => 'Google',
            'price' => 3000000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'days_support' => 14,
            'items' => [
                [
                    'item' => 'Setup 5 chiến dịch quảng cáo',
                    'content' => 'Lên tối đa 5 chiến dịch quảng cáo đạt chuẩn Google',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 50 từ khoá cạnh tranh',
                    'content' => 'Lên tối đa 50 từ khoá cạnh tranh theo sản phẩm dịch vụ của Bên A',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch Google Remarketing',
                    'content' => 'Lên 1 chiến dịch Remarketing để tận dụng tối đa traffic vào web',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao trực tiếp/online (30 phút) và giải đáp thắc mắc về sử dụng tài khoản trong 14 ngày làm việc tiếp theo',
                    'duration' => 'Trong 336h'
                ],
                [
                    'item' => 'Cài đặt thành công theo dõi chuyển đổi',
                    'content' => 'Gắn tracking để đo lường độ hiệu quả của các chiến dịch, các nhóm quảng cáo',
                    'duration' => 'Trong 24h'
                ]
            ]
        ],

        '1ad_googelads_30_days_support' => [
            'label' => 'Gói Set up quảng cáo Google hỗ trợ 30 ngày',
            'name' => '1ad_googelads_30_days_support',
            'short_name' => 'Google',
            'price' => 3400000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'days_support' => 30,
            'items' => [
                [
                    'item' => 'Setup 5 chiến dịch quảng cáo',
                    'content' => 'Lên tối đa 5 chiến dịch quảng cáo đạt chuẩn Google',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 50 từ khoá cạnh tranh',
                    'content' => 'Lên tối đa 50 từ khoá cạnh tranh theo sản phẩm dịch vụ của Bên A',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch Google Remarketing',
                    'content' => 'Lên 1 chiến dịch Remarketing để tận dụng tối đa traffic vào web',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao trực tiếp/online (30 phút) và giải đáp thắc mắc về sử dụng tài khoản trong 30 ngày làm việc tiếp theo',
                    'duration' => 'Trong 720h'
                ],
                [
                    'item' => 'Cài đặt thành công theo dõi chuyển đổi',
                    'content' => 'Gắn tracking để đo lường độ hiệu quả của các chiến dịch, các nhóm quảng cáo',
                    'duration' => 'Trong 24h'
                ]
            ]
        ],

        '1ad_facebookads_7_days_support' => [
            'label' => 'Gói Set up quảng cáo Facebook hỗ trợ 7 ngày',
            'name' => '1ad_facebookads_7_days_support',
            'short_name' => 'Facebook',
            'price'   => 2400000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'days_support' => 7,
            'items' => [
                [
                    'item' => 'Setup 3 chiến dịch quảng cáo',
                    'content' => 'Lên 3 chiến dịch quảng cáo trên tài khoản Business Facebook',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 9 nhóm quảng cáo',
                    'content' => 'Lên tối đa 9 nhóm quảng cáo với các nhóm đối tượng khác nhau',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch Facebook Remarketing',
                    'content' => 'Cài đặt Pixel để remarketing trên tệp khách hàng từ Website',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => '1 bài post chuẩn Facebook Ads',
                    'content' => 'Lên 1 bài post với nội dung Quảng cáo sản phẩm và dịch vụ của khách hàng',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao trực tiếp/online (30 phút) và giải đáp thắc mắc về sử dụng tài khoản trong 7 ngày làm việc tiếp theo',
                    'duration' => 'Trong 168h'
                ],
            ],
        ],

        '1ad_facebookads_14_days_support' => [
            'label' => 'Gói Set up quảng cáo Facebook hỗ trợ 14 ngày',
            'name' => '1ad_facebookads_14_days_support',
            'short_name' => 'Facebook',
            'price'   => 3000000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'days_support' => 14,
            'items' => [
                [
                    'item' => 'Setup 3 chiến dịch quảng cáo',
                    'content' => 'Lên 3 chiến dịch quảng cáo trên tài khoản Business Facebook',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 9 nhóm quảng cáo',
                    'content' => 'Lên tối đa 9 nhóm quảng cáo với các nhóm đối tượng khác nhau',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch Facebook Remarketing',
                    'content' => 'Cài đặt Pixel để remarketing trên tệp khách hàng từ Website',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => '1 bài post chuẩn Facebook Ads',
                    'content' => 'Lên 1 bài post với nội dung Quảng cáo sản phẩm và dịch vụ của khách hàng',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao trực tiếp/online (30 phút) và giải đáp thắc mắc về sử dụng tài khoản trong 14 ngày làm việc tiếp theo',
                    'duration' => 'Trong 366h'
                ],
            ],
        ],

        '1ad_facebookads_30_days_support' => [
            'label' => 'Gói Set up quảng cáo Facebook hỗ trợ 30 ngày',
            'name' => '1ad_facebookads_30_days_support',
            'short_name' => 'Facebook',
            'price'   => 3400000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'days_support' => 30,
            'items' => [
                [
                    'item' => 'Setup 3 chiến dịch quảng cáo',
                    'content' => 'Lên 3 chiến dịch quảng cáo trên tài khoản Business Facebook',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 9 nhóm quảng cáo',
                    'content' => 'Lên tối đa 9 nhóm quảng cáo với các nhóm đối tượng khác nhau',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch Facebook Remarketing',
                    'content' => 'Cài đặt Pixel để remarketing trên tệp khách hàng từ Website',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => '1 bài post chuẩn Facebook Ads',
                    'content' => 'Lên 1 bài post với nội dung Quảng cáo sản phẩm và dịch vụ của khách hàng',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao trực tiếp/online (30 phút) và giải đáp thắc mắc về sử dụng tài khoản trong 30 ngày làm việc tiếp theo',
                    'duration' => 'Trong 720h'
                ],
            ],
        ],

        '1ad_facebookads_basic' => [
            'label' => 'Gói Set up quảng cáo Facebook 1tr9',
            'name' => '1ad_facebookads_basic',
            'short_name' => 'Facebook',
            'price'   => 1900000,
            'discount_amount_default' => 0,
            'status' => 'unactive',
            'items' => [
                [
                    'item' => 'Setup 3 chiến dịch quảng cáo',
                    'content' => 'Lên 3 chiến dịch quảng cáo trên tài khoản Business Facebook',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 9 nhóm quảng cáo',
                    'content' => 'Lên tối đa 3 nhóm quảng cáo với các nhóm đối tượng khác nhau',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch Facebook Remarketing',
                    'content' => 'Cài đặt Pixel để remarketing trên tệp khách hàng từ Website',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => '1 bài post chuẩn Facebook Ads',
                    'content' => 'Lên 1 bài post với nội dung Quảng cáo sản phẩm và dịch vụ của khách hàng',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao và tặng 1 khoá học online miễn phí (trong 1 tuần đầu tiên)',
                    'duration' => 'Trong 24h'
                ],
            ],
        ],

        '1ad_facebookads_start' => [
            'label' => 'Gói Set up quảng cáo Facebook 2tr4',
            'name' => '1ad_facebookads_start',
            'short_name' => 'Facebook',
            'price'   => 2400000,
            'discount_amount_default' => 0,
            'status' => 'unactive',
            'items' => [
                [
                    'item' => 'Setup 3 chiến dịch quảng cáo',
                    'content' => 'Lên 3 chiến dịch quảng cáo trên tài khoản Business Facebook',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 9 nhóm quảng cáo',
                    'content' => 'Lên tối đa 9 nhóm quảng cáo với các nhóm đối tượng khác nhau',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch Facebook Remarketing',
                    'content' => 'Cài đặt Pixel để remarketing trên tệp khách hàng từ Website',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => '1 bài post chuẩn Facebook Ads',
                    'content' => 'Lên 1 bài post với nội dung Quảng cáo sản phẩm và dịch vụ của khách hàng',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao và tặng 1 khoá học online miễn phí (trong 1 tuần đầu tiên)',
                    'duration' => 'Trong 24h'
                ],
            ],
        ],

        '1ad_googelads_basic' => [
            'label' => 'Gói Set up quảng cáo Google 900',
            'name' => '1ad_googelads_basic',
            'short_name' => 'Google',
            'price' => 900000,
            'discount_amount_default' => 0,
            'status' => 'unactive',
            'items' => [
                [
                    'item' => 'Setup 5 chiến dịch quảng cáo',
                    'content' => 'Lên tối đa 5 chiến dịch quảng cáo đạt chuẩn Google',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 35 từ khoá cạnh tranh',
                    'content' => 'Lên tối đa 35 từ khoá cạnh tranh theo sản phẩm dịch vụ của Bên A',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao và tặng 1 khoá học online miễn phí (trong 1 tuần đầu tiên)',
                    'duration' => 'Trong 24h'
                ],
            ]
        ],

        '1ad_googelads_plus' => [
            'label' => 'Gói Set up quảng cáo Google 1tr4',
            'name' => '1ad_googelads_plus',
            'short_name' => 'Google',
            'price' => 1400000,
            'discount_amount_default' => 0,
            'status' => 'unactive',
            'items' => [
                [
                    'item' => 'Setup 5 chiến dịch quảng cáo',
                    'content' => 'Lên tối đa 5 chiến dịch quảng cáo đạt chuẩn Google',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 35 từ khoá cạnh tranh',
                    'content' => 'Lên tối đa 35 từ khoá cạnh tranh theo sản phẩm dịch vụ của Bên A',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch Google Remarketing',
                    'content' => 'Lên 1 chiến dịch Remarketing để tận dụng tối đa traffic vào web',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao và tặng 1 khoá học online miễn phí (trong 1 tuần đầu tiên',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Cài đặt thành công theo dõi chuyển đổi',
                    'content' => 'Gắn tracking để đo lường độ hiệu quả của các chiến dịch, các nhóm quảng cáo',
                    'duration' => 'Trong 24h'
                ]
            ]
        ],

        '1ad_googelads' => [
            'label' => 'Gói Set up quảng cáo Google',
            'name' => '1ad_googelads',
            'short_name' => 'Google',
            'price' => 3000000,
            'discount_amount_default' => 1100000,
            'status' => 'unactive',
            'items' => [
                [
                    'item' => 'Setup 10 chiến dịch quảng cáo',
                    'content' => 'Lên tối đa 10 chiến dịch quảng cáo đạt chuẩn Google',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 50 từ khoá cạnh tranh',
                    'content' => 'Lên tối đa 50 từ khoá cạnh tranh theo sản phẩm dịch vụ của Bên A ',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch Google Remarketing',
                    'content' => 'Lên 1 chiến dịch Remarketing để tận dụng tối đa traffic vào web',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao và tặng 1 khoá học online miễn phí',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Cài đặt thành công theo dõi chuyển đổi',
                    'content' => 'Gắn tracking để đo lường độ hiệu quả của các chiến dịch, các nhóm quảng cáo',
                    'duration' => 'Trong 24h'
                ],
            ]
        ],

        '1ad_facebookads' => [
            'label' => 'Gói Set up quảng cáo Facebook',
            'name' => '1ad_facebookads',
            'short_name' => 'Facebook',
            'price'   => 4000000,
            'discount_amount_default' => 1100000,
            'status' => 'unactive',
            'items' => [
                [
                    'item' => 'Setup 3 chiến dịch quảng cáo',
                    'content' => 'Lên 3 chiến dịch quảng cáo trên tài khoảng Business Facebook',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 10 nhóm quảng cáo',
                    'content' => 'Lên tối đa 10 nhóm quảng cáo với các nhóm đối tượng khác nhau',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch Facebook Remarketing',
                    'content' => 'Cài đặt Pixel để remarketing trên tệp khách hàng từ Website',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => '1 bài post chuẩn Facebook Ads',
                    'content' => 'Lên 1 bài post với nội dung Quảng cáo sản phẩm và dịch vụ của khách hàng',
                    'duration' => 'Trong 24h'
                ],
                [
                    'item' => 'Cài đặt thành công theo dõi chuyển đổi',
                    'content' => 'Gắn tracking để đo lường độ hiệu quả của các chiến dịch, các nhóm quảng cáo',
                    'duration' => 'Trong 24h'
                ],
            ]
        ],

        // Tiktok
        '1ad_tiktokads_basic' => [
            'label' => 'Gói Set up quảng cáo Tiktok cơ bản',
            'name' => '1ad_tiktokads_basic',
            'short_name' => 'Tiktok',
            'price'   => 2400000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'days_support' => 7,
            'items' => [
                [
                    'item' => 'Setup Tiktok shop cơ bản',
                    'content' => [
                        'Xác minh danh tính',
                        'Duyệt chính sách sản phẩm',
                        'Tối đa 50 sản phẩm',
                        'Liên kết shop với kênh tiktok',
                    ],
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng sẽ được gửi sang mail bao gồm tài liệu hướng dẫn bằng file hoặc video và giải đáp thắc mắc về sử dụng tài khoản trong 7 ngày làm việc tiếp theo',
                    'duration' => 'Trong 168h'
                ],
            ],
        ],

        '1ad_tiktokads_advance' => [
            'label' => 'Gói Set up quảng cáo Tiktok nâng cao',
            'name' => '1ad_tiktokads_advance',
            'short_name' => 'Tiktok',
            'price'   => 4000000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'days_support' => 7,
            'items' => [
                [
                    'item' => 'Setup Tiktok shop nâng cao',
                    'content' => [
                        'Xác minh danh tính',
                        'Duyệt chính sách sản phẩm, hỗ trợ tối ưu danh sách sản phẩm, thiết kế ảnh Thumb',
                        'Tối đa 50 sản phẩm',
                        'Liên kết shop với kênh tiktok',
                    ],
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Hỗ trợ tài khoản quảng cáo',
                    'content' => [
                        '1000 follower',
                        '5000 view',
                        'Gắn video lên Tiktok shop',
                    ],
                    'duration' => 'Trong 168h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng sẽ được gửi sang mail bao gồm tài liệu hướng dẫn bằng file hoặc video và giải đáp thắc mắc về sử dụng tài khoản trong 7 ngày làm việc tiếp theo',
                    'duration' => 'Trong 168h'
                ],
            ]
        ],

        '1ad_tiktokads_7_days' => [
            'label' => 'Gói Set up quảng cáo TikTok ads 7 ngày',
            'name' => '1ad_tiktokads_7_days',
            'short_name' => 'TikTok',
            'price'   => 2400000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'days_support' => 7,
            'items' => [
                [
                    'item' => 'Setup 2 chiến dịch quảng cáo',
                    'content' => 'Lên tối đa 02 chiến dịch quảng cáo mục tiêu chuẩn TikTok',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Setup 6 nhóm quảng cáo',
                    'content' => 'Lên tối đa 06 nhóm quảng cáo khác nhau',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch tiếp thị lại',
                    'content' => 'Lên 1 chiến dịch tiếp thị lại khi đạt yêu cầu',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Mở tính năng Livestream',
                    'content' => 'Hỗ trợ mở tính năng livestream trên TikTok khi có TikTok Shop',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao và hỗ trợ 1:1 miễn phí (trong 07 ngày đầu tiên)',
                    'duration' => 'Trong 168h'
                ],
            ]
        ],

        '1ad_tiktokads_14_days' => [
            'label' => 'Gói Set up quảng cáo TikTok ads 14 ngày',
            'name' => '1ad_tiktokads_14_days',
            'short_name' => 'TikTok',
            'price'   => 3000000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'days_support' => 14,
            'items' => [
                [
                    'item' => 'Setup 2 chiến dịch quảng cáo',
                    'content' => 'Lên tối đa 02 chiến dịch quảng cáo mục tiêu chuẩn TikTok',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Setup 6 nhóm quảng cáo',
                    'content' => 'Lên tối đa 06 nhóm quảng cáo khác nhau',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch tiếp thị lại',
                    'content' => 'Lên 1 chiến dịch tiếp thị lại khi đạt yêu cầu',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Mở tính năng Livestream',
                    'content' => 'Hỗ trợ mở tính năng livestream trên TikTok khi có TikTok Shop',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao và hỗ trợ 1:1 miễn phí (trong 14 ngày đầu tiên)',
                    'duration' => 'Trong 336h'
                ],
            ]
        ],

        '1ad_tiktokads_30_days' => [
            'label' => 'Gói Set up quảng cáo TikTok ads 30 ngày',
            'name' => '1ad_tiktokads_30_days',
            'short_name' => 'TikTok',
            'price'   => 3400000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'days_support' => 30,
            'items' => [
                [
                    'item' => 'Setup 2 chiến dịch quảng cáo',
                    'content' => 'Lên tối đa 02 chiến dịch quảng cáo mục tiêu chuẩn TikTok',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Setup 6 nhóm quảng cáo',
                    'content' => 'Lên tối đa 06 nhóm quảng cáo khác nhau',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Setup 1 chiến dịch tiếp thị lại',
                    'content' => 'Lên 1 chiến dịch tiếp thị lại khi đạt yêu cầu',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Mở tính năng Livestream',
                    'content' => 'Hỗ trợ mở tính năng livestream trên TikTok khi có TikTok Shop',
                    'duration' => 'Trong 72h'
                ],
                [
                    'item' => 'Hướng dẫn sử dụng',
                    'content' => 'Hướng dẫn sử dụng khi bàn giao và hỗ trợ 1:1 miễn phí (trong 30 ngày đầu tiên)',
                    'duration' => 'Trong 720h'
                ],
            ]
        ],
    ],
    'default' => '1ad_googelads'
];
