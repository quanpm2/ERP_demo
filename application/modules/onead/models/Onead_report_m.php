<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Base_report_m.php');

class Onead_report_m extends Base_report_m {

    function __construct()
    {
        $this->recipients['cc'][]   = 'baont@adsplus.vn';
        parent::__construct();

        $this->load->model('googleads/googleads_kpi_m');

        // 30 ngày trước ngày kết thúc hợp đồng
        defined('NUMDAYS_BEFORE_FINISH') OR define('NUMDAYS_BEFORE_FINISH',30);
		// 7 ngày nhắc một lần
		defined('NUMDAYS_NOTICEFINISH') OR define('NUMDAYS_NOTICEFINISH',7);
    }

    //send mail cho kinh doanh, kỹ thuật
    public function send_init_service_email($type_to = 'admin')
	{
		if( ! $this->term) return FALSE;

		$this->autogen_msg          = FALSE;
        
        $this->set_mail_to($type_to); /* Add người nhận mặc định theo hợp đồng vào email */
        $this->set_send_from('reporting'); /* Cấu hình email gửi tự động random */

        $contract_code = get_term_meta_value($this->term_id, 'contract_code');

        $default = array(
            'term'      			=> $this->term,
            'term_id'   			=> $this->term_id,

            'title'     => "Hợp đồng {$this->data['contract_code']} đã được khởi tạo.",
            'subject'   => "Hợp đồng {$this->data['contract_code']} đã được khởi tạo.",
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'time'		=> time(),
            'content_tpl' => 'onead/report/init_service_email',
        );

        $this->data = wp_parse_args($this->data, $default);

        if( empty($this->data['content_tpl'])) return FALSE;

        $this->data['content'] = null;

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        $result = parent::send_email();
		if( FALSE === $result ) return FALSE ;

		// Thực hiện ghi log	
		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Thông báo khởi tạo hợp đồng',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id,
			'log_content'	=> 'Đã gửi mail thành công',
			'log_type'		=> 'onead-report-email',
			'log_time_create' => date('Y-m-d H:i:s'),
		));

		return TRUE ;
	}

	/**
     * Sends an activation email.
     *
     * @param      string  $type_to  The type to
     */
    public function send_activation_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;


        $this->autogen_msg          = FALSE;
        $this->recipients['cc'][]   = 'ketoan@adsplus.vn';

        $this->set_mail_to($type_to); /* Add người nhận mặc định theo hợp đồng vào email */
        $this->set_send_from('reporting'); /* Cấu hình email gửi tự động random */

        $default = array(
            'term'      => $this->term,
            'term_id'   => $this->term_id,
            'title'     => 'Thông tin kích hoạt dịch vụ',
            'subject'   => '[1AD.VN] Thông báo kích hoạt dịch vụ 1AD cho HĐ'.$this->data['contract_code'],
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'sale'      => $this->sale,
            'content_tpl' => 'onead/report/activation_email',
        );

        $this->data = wp_parse_args($this->data, $default);
        if( empty($this->data['content_tpl'])) return FALSE;

        $this->data['content'] = $this->render_content($this->data['content_tpl']);

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        $result = $this->send_email();
        $result AND $this->log_m->insert(array(
            'log_title'     => 'Gửi mail khi kích hoạt 1AD bằng email',
            'log_status'    => 1,
            'term_id'       => $this->term_id,
            'user_id'       => $this->admin_m->id,
            'log_content'   => 'Đã gửi mail thành công',
            'log_type'      => 'onead-report-email',
            'log_time_create' => date('Y-m-d H:i:s'),
        ));

        return $result;
    }


    /**
     * Sends a finished email.
     *
     * @param      string  $type_to  The type to
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function send_finished_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;

        $this->autogen_msg          = FALSE;
        $this->recipients['cc'][]   = 'ketoan@adsplus.vn';

        $this->set_mail_to($type_to); /* Add người nhận mặc định theo hợp đồng vào email */
        $this->set_send_from('reporting'); /* Cấu hình email gửi tự động random */

        $this->load->config('onead/onead');
        $service_package_config = $this->config->item('service', 'packages');
        $service_package 		= get_term_meta_value($this->term_id, 'service_package');
        $days_support = $service_package_config[$service_package]['days_support'] ?? 0;

        $default = array(
            'term'      => $this->term,
            'term_id'   => $this->term_id,
            'contract_date' => my_date($this->data['contract_begin'], 'd/m/Y').' - '.my_date($this->data['contract_end'], 'd/m/Y'),
            'title'     => 'Thông tin kết thúc dịch vụ',
            'subject'   => "[1AD.VN] Thông báo kết thúc dịch vụ HĐ {$this->data['contract_code']}",
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'sale'   => $this->sale,
            'days_support'   => $days_support,
            'content_tpl' => 'onead/report/finished_email',
        );

        $this->data = wp_parse_args($this->data, $default);
        if( empty($this->data['content_tpl'])) return FALSE;

        $this->data['content'] = $this->render_content($this->data['content_tpl']);

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        $result = $this->send_email();
        $result AND $this->log_m->insert(array(
            'log_title'     => 'Gửi mail kết thúc hợp đồng',
            'log_status'    => 1,
            'term_id'       => $this->term_id,
            'user_id'       => $this->admin_m->id,
            'log_content'   => 'Đã gửi mail thành công',
            'log_type'      => 'onead-report-email',
            'log_time_create' => date('Y-m-d H:i:s'),
        ));

        return $result;
    }

    /**
     * Sends a renew notice email.
     *
     * @param      string  $type_to  The type to
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function send_renew_notice_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;

        $time 				= time();
		$contract_end		= (int)get_term_meta_value($this->term_id,'contract_end');
		$contract_end		= $this->mdate->endOfDay($contract_end);
		$start_time_allowed = $this->mdate->startOfDay(strtotime('-'.NUMDAYS_BEFORE_FINISH.' days',$contract_end));

		// Nếu thời điểm gửi mail không nằm trong phạm vi [NUMDAYS_BEFORE_FINISH] đến thời gian kết thúc thì không gửi mail thông báo
		if($time < $start_time_allowed || $contract_end < $time) return FALSE;

		$send_renew_notice_email_next_time = (int) get_term_meta_value($this->term_id,'send_renew_notice_email_next_time');
		
		// Không hợp lệ nếu 2 lần gửi gần nhau có thời gian thấp hơn [NUMDAYS_NOTICEFINISH] ngày
		if($time < $send_renew_notice_email_next_time) return FALSE;

		update_term_meta($this->term_id,'send_renew_notice_email_next_time',strtotime('+'.NUMDAYS_NOTICEFINISH.' days'));

        // Main process
        $this->autogen_msg          = FALSE;
        $this->recipients['cc'][]   = 'ketoan@adsplus.vn';

        $this->set_mail_to($type_to); /* Add người nhận mặc định theo hợp đồng vào email */
        $this->set_send_from('reporting'); /* Cấu hình email gửi tự động random */

        $default = array(
            'term'      => $this->term,
            'term_id'   => $this->term_id,
            'contract_date' => my_date($this->data['contract_begin'], 'd/m/Y').' - '.my_date($this->data['contract_end'], 'd/m/Y'),
            'title'     => 'Thông tin gia hạn dịch vụ',
            'subject'   => "[1AD.VN] Thông báo gia hạn dịch vụ HĐ {$this->data['contract_code']}",
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'content_tpl' => 'onead/report/renew_notice_email',
        );

        $this->data = wp_parse_args($this->data, $default);
        if( empty($this->data['content_tpl'])) return FALSE;

        $this->data['content'] = $this->render_content($this->data['content_tpl']);

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        $result = $this->send_email();
        $result AND $this->log_m->insert(array(
            'log_title'     => 'Gửi mail thông báo gia hạn dịch vụ',
            'log_status'    => 1,
            'term_id'       => $this->term_id,
            'user_id'       => $this->admin_m->id,
            'log_content'   => 'Đã gửi mail thành công',
            'log_type'      => 'onead-notice-renew-email',
            'log_time_create' => date('Y-m-d H:i:s'),
        ));

        return $result;
    }


	/**
	 * Init Service Generate data
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     self     ( description_of_the_return_value )
	 */
	public function init($term_id = 0)
    {
        parent::init($term_id);

        if( ! $this->term_id) return FALSE;

        $this->load->config('onead/onead');

        $service_package_config = $this->config->item('service', 'packages');
        $service_package 		= get_term_meta_value($this->term_id, 'service_package');
        $service_package_label 	= $service_package_config[$service_package]['label'] ?? 'null';

        $data = array(

            'contract_value' 		=> get_term_meta_value($this->term_id, 'contract_value'),
            'discount_amount'		=> get_term_meta_value($this->term_id, 'discount_amount'),
            'quantity'				=> get_term_meta_value($this->term_id, 'quantity'),
            'price' 				=> get_term_meta_value($this->term_id, 'price'),
            'vat' 					=> get_term_meta_value($this->term_id, 'vat'),

            'service_package' 		=> $service_package,
            'service_package_label' => $service_package_label,
            'service_package_items' => @unserialize(get_term_meta_value($this->term_id, 'service_package_items')),

            'contract_code' => get_term_meta_value($this->term_id, 'contract_code'),
            'contract_begin' => get_term_meta_value($this->term_id, 'contract_begin'),
            'contract_end' => get_term_meta_value($this->term_id, 'contract_end'),

            'start_service_time' => get_term_meta_value($this->term_id, 'start_service_time') ?: 0,
        );

        $this->data = wp_parse_args($data, $this->data);
        return $this;
    }

    /**
     * Loads workers.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_workers()
    {
        parent::load_workers();

        $kpis = $this->googleads_kpi_m->select('user_id')->where('term_id',$this->term_id)->group_by('user_id')->get_many_by();
        if( ! $kpis) return $this;

        $_workers       = array_map(function($x){ return $this->admin_m->get_field_by_id($x->user_id); }, $kpis);
        $this->workers  = wp_parse_args(array_column($_workers, NULL, 'user_email'), $this->workers);

        return $this;
    }

    /**
     * Render Send content;
     *
     * @param      <type>  $content_view  The content view
     * @param      string  $layout        The layout
     *
     * @return     Mixed  Result
     */
    public function render_content($content_view, $layout = 'report/template/email/1ad')
    {
        if( ! $content_view || ! $layout) return FALSE;

        $data = $this->data;
        if( ! is_array($data)) $data = array($data);

        $data['content'] = $this->load->view($content_view, $data, TRUE);

        $content = $this->load->view($layout, $data, TRUE);
        if( ! $content) return FALSE;
        return $content;
    }
}
/* End of file Onead_report_m.php */
/* Location: ./application/modules/onead/models/Onead_report_m.php */