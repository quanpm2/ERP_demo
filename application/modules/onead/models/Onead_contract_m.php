<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Base_report_m.php');

class Onead_contract_m extends Base_contract_m {

	function __construct() 
	{
		parent::__construct();
		$this->load->model('onead/onead_m');
	}
	
	public function has_permission($term_id = 0, $name = '', $action = '',$kpi_type = '',$user_id = 0,$role_id = null)
	{
		$this->load->model('onead/onead_kpi_m');

		$permission = $name.'.'.$action;
		$permission = trim($permission, '.');
		
		if($this->permission_m->has_permission("{$name}.Manage",$role_id)
			&& $this->permission_m->has_permission($permission,$role_id)) 
			return TRUE;

		if(!$this->permission_m->has_permission($permission,$role_id)) return FALSE;

		$user_id = empty($user_id) ? $this->admin_m->id : $user_id;

		if($this->permission_m->has_permission('Onead.sale.access',$role_id) 
			&& get_term_meta_value($term_id,'staff_business') == $user_id)
			return TRUE;

		$args = array(
			'term_id' => $term_id,
			'user_id' => $user_id);

		if(!empty($kpi_type))
		{
			$args['kpi_type'] = $kpi_type;
		}

		$kpis = $this->webgeneral_kpi_m->get_by($args);
		if(empty($kpis)) return FALSE;

		return TRUE;
	}

	/**
	 * Starts a service.
	 *
	 * @param      <type>   $term   The term
	 *
	 * @return     boolean  ( description_of_the_return_value )
	 */
	public function start_service($term = null)
	{
		restrict('Onead.start_service');

		if( empty($term) || $term->term_type != 'onead') return false;

		$this->load->model('onead/onead_report_m') ;
		$this->onead_report_m->init($term);
		$this->onead_report_m->send_init_service_email();

		/***
		 * Khởi tạo Meta thời gian dịch vụ để thuận tiện cho tính năng sắp xếp và truy vấn
		 */
		$start_service_time = get_term_meta_value($term->term_id, 'start_service_time');
		empty($start_service_time) AND update_term_meta($term->term_id, 'start_service_time', 0);
		return TRUE;
	}


	/**
	 * Kỹ thuật phụ trách tiến hành kích hoạt thực hiện dịch vụ
	 *
	 * @param      <type>  $term   The term
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function proc_service($term = FALSE)
	{
		if(!$term || $term->term_type != 'onead') return FALSE;
		$term_id = $term->term_id;

		$this->load->model('onead/onead_report_m') ;
		$this->onead_report_m->init($term);
		$is_send = $this->onead_report_m->send_activation_email();

		if(FALSE === $is_send) return FALSE ;

		update_term_meta($term_id, 'start_service_time' , time());

		$this->log_m->insert(array(
			'log_type' =>'start_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => date('Y/m/d H:i:s', time()). ' - Start Service Time'
		 ));

		return TRUE;
	}

	/**
	 * Stops a service.
	 *
	 * @param      <type>  $term   The term
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function stop_service($term)
	{
		if(!$term || $term->term_type != 'onead') return FALSE;
		$term_id = $term->term_id;

		$this->load->model('onead/onead_report_m') ;
		$this->onead_report_m->init($term);
		$is_send = $this->onead_report_m->send_finished_email();
		if( FALSE === $is_send) return FALSE ;

		update_term_meta($term_id,'end_service_time', time());
		$this->contract_m->update($term_id, array('term_status'=>'ending'));
		$this->log_m->insert(array(
			'log_type' =>'end_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term_id,
			'log_content' => date('Y/m/d H:i:s', time()) . ' - End Service Time'
			));

        // Push queue sync service fee
        $this->load->config('amqps');
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        
        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];

        $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $channel     = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $payload = [
            'event' 	=> 'contract_payment.sync.service_fee',
            'contract_id' => $term_id
        ];
        $message = new \PhpAmqpLib\Message\AMQPMessage(
            json_encode($payload),
            array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $channel->basic_publish($message, '', $queue);	

        $channel->close();
        $connection->close();

		return TRUE;
	}

    /**
	 * Gửi email nhắc khách hàng gia hạn dịch vụ
	 *
	 * @param      <type>  $term   The term
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function notice_renew_service($term = FALSE)
	{
        if(!$term || $term->term_type != 'onead') return FALSE;
		$term_id = $term->term_id;

		$this->load->model('onead/onead_report_m') ;
		$this->onead_report_m->init($term);
		$is_send = $this->onead_report_m->send_renew_notice_email();

		if(FALSE === $is_send) return FALSE ;

		return TRUE;
	}
}

/* End of file Onead_contract_m.php */
/* Location: ./application/modules/onead/models/Onead_contract_m.php */