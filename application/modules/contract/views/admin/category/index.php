<div class="row">
  <div class="col-sm-12">
    <div class="form-group"><a href="<?php echo module_url('category/form');?>" class="btn btn-primary"><i class="fa fa-fw fa-plus "></i> Thêm ngành hàng</a></div>
    <?php echo $content; ?>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
      $('.delete-category').off('click').on('click', function(e) {

        var term_id = $(this).data('term-id');

        $(this).closest('tr').addClass('term-' + term_id);

        var url_delete = '<?php echo module_url('ajax/category/delete_term') ;?>' ;  

        $.ajax({
          url       : url_delete,
          type      : 'post',
          dataType  : 'json' ,
          data      : { term_id : term_id }
        }).done(function(response) {
            if(response.status == 0) return false;
            location.reload();
        }) ;   

      }) ;
  }) ;
</script>