  <!--<div class="box box-warning">-->
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo $this->admin_form->form_open('contract/category/form/' . $term_id , array('method' => 'POST', 'id' => 'form-category')) ; ?>
          <?php

            $option_status      = array('publish' => 'Có', 'ending' => 'Không');

            $term_name          = $this->input->post('term_name');
            $term_description   = $this->input->post('term_description');
            $term_parent        = $this->input->post('term_parent');
            $term_status        = $this->input->post('term_status');

            if( !empty($term) ) {
                $term_name        = $term->term_name;
                $term_parent      = $term->term_parent;
                $term_description = $term->term_description;
                $term_status      = $term->term_status;
            }

            // TÊN NGÀNH NGHỀ 
            echo $this->admin_form->input('Tên ngành nghề', 'term_name', $term_name) ;

            // MÔ TẢ
            echo $this->admin_form->textarea('Mô tả', 'term_description', $term_description, '', array('id' => 'cat_description')); 
          ?>

          <?php 
            // CẤP BẬC
            echo $this->admin_form->dropdown('Cấp bậc', 'term_parent', prepare_dropdown($categories,[0=>'--Trống--']),$term_parent);
          ?>

          <?php 
            // HIỂN THỊ
            echo $this->admin_form->dropdown('Hiển thị', 'term_status', $option_status, array($term_status)) ;
          ?>

          <!-- BUTTON -->
          <div class="form-group">
              <a href="<?php echo module_url('category/index') ; ?>" class="btn btn-primary text-center" ><i class="fa fa-arrow-left" aria-hidden="true"></i> Trở về</a>
              <button type="submit" class="btn btn-primary text-center" name="action" value="Save"/><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu lại</button>
          </div>
        <?php echo $this->admin_form->form_close() ;?>   
    
    </div>