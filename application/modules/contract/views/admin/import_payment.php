<?php
$this->template->javascript->add('plugins/validate/jquery.validate.js');
$this->template->stylesheet->add('plugins/bootstrap-fileinput/css/fileinput.css');
$this->template->javascript->add('plugins/bootstrap-fileinput/js/fileinput.min.js');

echo $this->admin_form->form_open('',['enctype'=>'multipart/form-data'],['btnImportFile'=>'1']);
echo $this->admin_form->upload('Tệp dữ liệu (*.xlsx)','inputFile','',anchor(base_url('files/excel_tpl/tool/danh-sach-thanh-toan.xlsx'), 'Tải về mẫu template file import'));
echo $this->admin_form->form_close();

if(!empty($content))
{
	$this->admin_form->set_col(12,6);
	echo $this->admin_form->box_open('Dữ liệu');
	echo ($content ?? '');
	echo $this->admin_form->form_open();

	echo $this->admin_form->submit(['name'=>'update_data','class'=>'btn btn-primary'] ,'LƯU LẠI (- dữ liệu không tìm thấy)');
	echo $this->admin_form->submit(['name'=>'export_mismatch','class'=>'btn btn-default'] ,'EXPORT THANH TOÁN KHÔNG TÌM THẤY');

	echo $this->admin_form->form_close();
}
?>

<script type="text/javascript">
$(function(){

	/* Upload common excel */
	$("input[type=file][name=inputFile]").fileinput({
	    showPreview: false,
	    showRemove: false,
	    allowedFileExtensions: ['xls', 'xlsx'],
	});
});
</script>