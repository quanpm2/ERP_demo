<?php
$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

$this->template->stylesheet->add('plugins/bootstrap-fileinput/css/fileinput.css');
$this->template->javascript->add('plugins/bootstrap-fileinput/js/fileinput.min.js');

$name 			= get_term_meta_value($edit->term_id, 'representative_name');
$lock_editable 	= get_term_meta_value($edit->term_id,'lock_editable');
if(empty($name) && !empty($customer))
{
	$name = $customer->user_type == 'customer_person' ? $customer->display_name : get_user_meta_value($customer->user_id,'customer_name');
}

$this->admin_form->set_col(12,6);
?>

<div class="col-md-12" id="step-detail-partial">
	<?php echo $this->admin_form->form_open('',[],['edit[term_id]'=>$edit->term_id,'edit[term_type]'=>$edit->term_type]); ?>
	<div class="row">
		<div class="container-fuild">
			<div class="col-xs-12 col-md-6">
				<fieldset class="scheduler-border">
					<legend class="scheduler-border">Thông tin đại diện khách hàng</legend>
					<div class="row">
						<?php
                        echo form_hidden('tmp_customer_type', $customer->user_type ?? '');
                        echo form_hidden('tmp_customer_id', $customer->user_id ?? 0);

						/* Thông tin người đại diện trên hợp đồng */
						echo $this->admin_form->formGroup_begin(0,'Người đại diện');
						echo '<div class="col-xs-2" style="padding-left: 0;">';

						$gender = get_term_meta_value($edit->term_id,'representative_gender') ?: get_user_meta_value(($customer->user_id??0), 'customer_gender');
						$attrs = ['name'=>'edit[meta][representative_gender]','class'=>'form-control'];
						if( ! $is_manager) 
						{
							echo form_hidden('edit[meta][representative_gender]', $gender);
							$attrs['disabled'] = 'disabled';
						}
						echo form_dropdown($attrs,array(1=>'Ông',0=>'Bà'),$gender);

						echo '</div>';
						echo '<div class="col-xs-10">';

						$attrs = ['name'=>'edit[meta][representative_name]','class'=>'form-control'];
						if( ! $is_manager) 
						{
							echo form_hidden('edit[meta][representative_name]', $name);
							$attrs['disabled'] = 'disabled';
						}
						echo form_input($attrs, $name);

						echo '</div>';
						echo $this->admin_form->formGroup_end();

						/* Thông tin Email người người đại diện trên hợp đồng*/
						$email = get_term_meta_value($edit->term_id, 'representative_email') ?: get_user_meta_value($customer->user_id??0,'customer_email');
						if($is_manager) 
						{
							echo $this->admin_form->input('Email','edit[meta][representative_email]', $email);
						}
						else
						{
							echo form_hidden('edit[meta][representative_email]', $email);
							echo $this->admin_form->input('Email','representative_email', $email,'',['disabled'=>'disabled']);
						} 


						/* Thông tin địa chỉ liên hệ đại diện trên hợp đồng */
						$address = get_term_meta_value($edit->term_id, 'representative_address') ?: get_user_meta_value($customer->user_id??0,'customer_address');
						if($is_manager) echo $this->admin_form->input('Địa chỉ','edit[meta][representative_address]', $address);
						else
						{
							echo form_hidden('edit[meta][representative_address]', $address);
							echo $this->admin_form->input('Địa chỉ','representative_address', $address,'',['disabled'=>'disabled']);
						}
						/* Thông tin vùng miền trên hợp đồng */
						$attrs = array('name'=>'edit[meta][zone]','class'=>'form-control');
						$zone = get_user_meta_value($this->admin_m->id,'user_zone');
						$this->load->config('staffs/group');
						$zones = $this->config->item('zone');	
						$z=[];
						foreach ($zones as $key => $value) {
							$z[$key]=$value['text'];
						}
						echo $this->admin_form->dropdown('Vùng miền','edit[meta][representative_zone]',$z,$zone,'',$attrs);
						/* Thông tin Email người người đại diện trên hợp đồng */
						$phone = get_term_meta_value($edit->term_id, 'representative_phone') ?: get_user_meta_value($customer->user_id??0,'customer_phone');
						if($is_manager) echo $this->admin_form->input('Điện thoại','edit[meta][representative_phone]', $phone);
						else 
						{
							echo form_hidden('edit[meta][representative_phone]', $phone);
							echo $this->admin_form->input('Điện thoại','representative_phone', $phone,'',['disabled'=>'disabled']);
						}


						/* Thông tin chức vụ người đại diện trên hợp đồng */
						$representative_position = get_term_meta_value($edit->term_id,'representative_position') ?: get_user_meta_value($customer->user_id ?? 0,'customer_position');
						echo $this->admin_form->input('Chức vụ','edit[meta][representative_position]', $representative_position,'', ['placeholder'=>'chức vụ ']);

						echo '<div id="customer_profile">'. $this->template->customer_profile .'</div>';

						/* Thông tin mã số thuế doanh nghiệp */
						$customer_tax = $edit->extra['customer_tax'] ?? get_user_meta_value($customer->user_id??0,'customer_tax');
						echo $this->admin_form->input('Mã Số thuế', 'edit[extra][customer_tax]', $customer_tax, '', ['placeholder'=>'mã số thuế ']);
						?>
					</div>
				</fieldset>			
			</div>

			<div class="col-xs-12 col-md-6">
				<fieldset class="scheduler-border">
					<legend class="scheduler-border">Thông tin hợp đồng</legend>
					<div class="row">
						<?php
						if($is_manager)
						{
							echo $this->admin_form->input(
								'Mã hợp đồng',
								'edit[meta][contract_code]',
								get_term_meta_value($edit->term_id, 'contract_code'),
								'Nếu để trống hệ thống sẽ tự động tạo mã hợp đồng , vd: HD249/0316/example.com với 249 là ID hợp đồng và 0316 là tháng_năm',
								['placeholder' => 'mã hợp đồng ']
							);
						}

						$contract_begin = get_term_meta_value($edit->term_id, 'contract_begin');
						$contract_begin OR $contract_begin 	= start_of_day();

						$contract_end 	= get_term_meta_value($edit->term_id, 'contract_end');
						$contract_end 	OR $contract_end 	= start_of_day(strtotime("+1 months"));

						$contract_begin_date 	= my_date($contract_begin, 'd-m-Y');
						$contract_end_date 		= my_date($contract_end, 'd-m-Y');
						$contract_daterange 	= "{$contract_begin_date} - {$contract_end_date}";

						echo $this->admin_form->input('Thời gian thực hiện', 'edit[meta][contract_daterange]', $contract_daterange,'', array('class'=>'form-control','id'=>'input_daterange'));

						echo $this->admin_form->formGroup_begin(0,'Thông tin ngân hàng');

						echo '<div class="col-xs-4" style="padding-left: 0;">';

						echo form_input(array('name'=>'edit[extra][bank_id]','class'=>'form-control','placeholder'=>'Số tài khoản'),$edit->extra['bank_id']??'');

						echo '</div>';

						echo '<div class="col-xs-4">';

						echo form_input(
							array('name'	=> 'edit[extra][bank_owner]',
								'class'	=> 'form-control',
								'placeholder'	=> 'chủ ngân hàng'),
							$edit->extra['bank_owner'] ?? '');

						echo '</div>';

						echo '<div class="col-xs-4">';

						echo form_input(
							array('name'	=> 'edit[extra][bank_name]',
								'class'	=> 'form-control',
								'placeholder'	=> 'Ngân hàng'),
							$edit->extra['bank_name'] ?? '');

						echo '</div>';

						echo $this->admin_form->formGroup_end();

						# check role of admin is in sale groups & build input form for its
						if($is_manager)
						{
                            $assigned_to_ids = get_user_meta($customer->user_id, 'assigned_to', FALSE, TRUE);
                            $assigned_to_ids = array_filter(array_unique($assigned_to_ids));

                            $assigned_display_names = [];
                            if(!empty($assigned_to_ids)){
                                foreach($assigned_to_ids as $assigned_to)
                                {
                                    $assigned_display_names[] = trim($this->admin_m->get_field_by_id($assigned_to, 'display_name'));
                                }
                            }
                            $assigned_display_names = array_filter(array_unique($assigned_display_names));

                            $staff_business_id = get_term_meta_value($edit->term_id, 'staff_business');

                            $helper = '';
                            if(!empty($assigned_to_ids))
                            {
                                $helper = '<p class="help-block">Khách hàng đang được Kinh doanh <b>' . implode(', ', $assigned_display_names) . '</b> phụ trách</p>';
                            }
                            
							echo $this->admin_form->dropdown(
                                'Nhân viên kinh doanh',
                                'edit[meta][staff_business]',
                                prepare_dropdown($staffs, 'Chọn nhân viên kinh doanh phụ trách'), 
                                $staff_business_id,
                                $helper,
                                [
                                    'id' => 'staff_business_selector',
                                    'data-staff-business-assigned-id' => htmlentities(json_encode($assigned_to_ids)),
                                ]
                            );

                            echo $this->admin_form->formGroup_begin('chk_is_overwrite');
                            echo form_checkbox('is_overwrite_assigned_to', '', FALSE, 'id="chk_is_overwrite_assigned_to" name="is_overwrite_assigned_to"');
                            echo '<label for="chk_is_overwrite_assigned_to" class="control-label">Xác nhận khách hàng chỉ thuộc kinh doanh đang chọn.</label>';
                            echo $this->admin_form->formGroup_end();

                            echo form_hidden('customer_id', $customer->user_id);
						}
						else
						{
							echo form_hidden('edit[meta][staff_business]', $this->admin_m->id);
						}

						echo $this->admin_form->input('Ghi chú', 'edit[meta][contract_note]', get_term_meta_value($edit->term_id,'contract_note'), '',['placeholder'=>'nguồn khách hàng']);


						if( 'pushdy' == $edit->term_type )
						{
							echo $this->admin_form->hidden(null, 'edit[meta][vat]', 0);
						}
						else
						{
                            echo $this->admin_form->dropdown('% thuế VAT','edit[meta][vat]', prepare_dropdown([0 => '0%', 8 => '8%', 10 => '10%'], 'Chọn % thuế VAT'), 8);
						}

						echo form_hidden('edit[term_status]', $edit->term_status);

						if($edit->term_type == 'webgeneral'){
							$this->table->clear()
							->set_caption('Dịch vụ kèm theo');

							$services_pricetag = get_term_meta_value($edit->term_id,'services_pricetag');
							$services_pricetag = @unserialize($services_pricetag);

							$this->table
							->add_row(array('Thiết kế web',
									form_checkbox('edit[meta][services_pricetag][webdesign][is_design]','1', (bool) @$services_pricetag['webdesign']['is_design']),
									$lock_editable ? force_var(@$services_pricetag['webdesign']['price'],'miễn phí')  : form_input(array('name'=>'edit[meta][services_pricetag][webdesign][price]','placeholder'=>'Đơn giá'), @$services_pricetag['webdesign']['price']),
									$lock_editable ? force_var(@$services_pricetag['webdesign']['theme_id'],'chưa chọn') : form_input(array('name'=>'edit[meta][services_pricetag][webdesign][theme_id]','placeholder'=>'Mã THEME'),@$services_pricetag['webdesign']['theme_id'])))

							->add_row(array('Hosting',
									form_checkbox('edit[meta][services_pricetag][hosting][has_hosting]','1', (bool) @$services_pricetag['hosting']['has_hosting']),
									$lock_editable ? force_var(@$services_pricetag['hosting']['price'],'miễn phí') : form_input(array('name'=>'edit[meta][services_pricetag][hosting][price]','placeholder'=>'Đơn giá'),@$services_pricetag['hosting']['price'])))

							->add_row(array('Domain',
									form_checkbox('services_pricetag[domain][is_free]','1', (bool) @$services_pricetag['domain']['is_free']),
									$lock_editable ? force_var(@$services_pricetag['domain']['identity'],'Tên miền') : form_input(array('name'=>'services_pricetag[domain][identity]','placeholder'=>'Tên miền'),@$services_pricetag['domain']['identity'])))

							->add_row(array('Giảm giá',
									form_checkbox('edit[meta][services_pricetag][discount][is_discount]','1',(bool) @$services_pricetag['discount']['is_discount']),
									$lock_editable ? force_var(@$services_pricetag['discount']['percent'],'0%') : form_input(array('name'=>'edit[meta][services_pricetag][discount][percent]','placeholder'=>'0.0%'),@$services_pricetag['discount']['percent'])))

							->add_row(array(
									'Số lần thanh toán','',
									form_input(array(
										'name'=>'edit[meta][services_pricetag][number_of_payments]',
										'type'=>'number',
										'placeholder'=>'Số lần thanh toán'),
										@$services_pricetag['number_of_payments'])
								));

							echo
							$this->table->generate();
						}

						echo $this->admin_form->submit(
							'',
							'confirm_step_detail',
							'confirm_step_detail',
							'',
							array('style'=>'display:none','id'=>'confirm_step_detail'));
						?>
					</div>
				</fieldset>			
			</div>
		</div>
	</div>
	<?php echo $this->admin_form->form_close();?>
</div>
<script type="text/javascript">
$(function(){

	$("#contract-inputfile").fileinput({
	    uploadUrl: module_url + 'ajax_dipatcher/act_upload_file',
	    uploadAsync: true,
	    maxFileCount: 5,
	    showPreview: false,
    	uploadExtraData: {
	        term_id: "<?php echo $edit->term_id;?>",
	        term_name: "<?php echo $edit->term_name;?>",
	    },
	    allowedFileExtensions: ['docx', 'doc', 'xls', 'xlsx'],
	})
	.on('fileuploaded', function(event, data, previewId, index) {

		var response = data.response

	    if(response.response != 'undefined' 
	    	&& response.response.success == true){

			$.each(response.response.ret_obj,function(i, e){
				$("#grid_files").append('<a href="<?php echo base_url();?>' + e.file_path + '">' + e.file_name + '</a><br/>');
			});
	    }
	});
	$("#input_daterange").daterangepicker({
		format: 'DD-MM-YYYY',
	});
	$('.select2').select2();
	$('.select2').css('width','100%');

    // Handle staff_business_selector
    let chk_is_overwrite_assigned_to = $("#chk_is_overwrite_assigned_to").parent().parent();
    $(chk_is_overwrite_assigned_to).hide()

    let staff_business_selector_data = $("#staff_business_selector").data();
    if(undefined != staff_business_selector_data)
    { 
        let staff_business_assigned_ids = staff_business_selector_data.staffBusinessAssignedId || [];
        if(0 == staff_business_assigned_ids.length)
        {
            let help_block = $("#staff_business_selector").parent().parent().find('.help-block');
            $(help_block).remove();
        }
        else
        {
            $("#staff_business_selector").change((event) => {
                let selected_id = event.target.value;
                
                let index_of_selected = staff_business_assigned_ids.findIndex((assigned_id) => assigned_id == selected_id);
                if(0 > index_of_selected)
                {
                    $(chk_is_overwrite_assigned_to).show()
                }
                else 
                {
                    $(chk_is_overwrite_assigned_to).hide()
                }
            })
        }
    }
});
</script>