<?php
$this->template->javascript->add('plugins/wizard/jquery.bootstrap.wizard.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');
?>
<div id="rootwizard">	
<?php
if(!empty($wizard_tabs)){

	$html = '<ul>';
	foreach ($wizard_tabs as $key => $tab) {
		$html .= '<li><a href="#' . $tab['id'] . '" data-toggle="tab"><span class="label">' . ++$key . '</span><span class="' . $tab['id'] . '">' . $tab['title'] . '</span></a></li>';
	}

	$html .= '</ul>';
	$html .= '<div class="tab-content">';
	foreach ($wizard_tabs as $key => $tab) {

		$html .= '<div class="tab-pane" id="' . $tab['id'] . '">' . $tab['content'] . '</div>';
	}

	$html .= 
		'<ul class="pager wizard">
			<li class="previous"><a href="#">Quay lại</a></li>
		  	<li class="next"><a href="#">Tiếp tục</a></li>
		  	<li class="finish" id="finishstep"><a href="javascript:;">Hoàn tất</a></li>
		</ul>
	</div>';

	echo $html;
}
?>	
	<div id="bar" class="progress">
	  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
	</div>
</div>


<script type="text/javascript">

<?php
$role_id = $this->admin_m->role_id;
$is_sale_roles = in_array($role_id, $this->config->item('salesexecutive','groups'));
$referral_url = $is_sale_roles ? admin_url('order_contact/approval_contract') : module_url("edit/$edit->term_id");
?>
var module_url = "<?php echo module_url('create_wizard/');?>";
var wizard_step = '<?php echo get_term_meta_value($edit->term_id,'wizard_step');?>';
var contract_type = '<?php echo $edit->term_type;?>';

function reload_detail_panel(data){

  $("#step-detail-partial").replaceWith(data);

  $("#contract_convert_name").val($('#chosen_website option:selected').text());
}

function reload_review_panel(data){
	$("#review-partial").replaceWith(data);	
	$("#contract_convert_name").val($('#chosen_website option:selected').text());
}

function reload_service_partial(data){
	$("#service_tab").replaceWith(data);
}

function refresh_customer_websites(datasource){

  if(datasource != 'undefined'){

    $("#chosen_website").empty();

    var dtasource = [];

    $.each(datasource,function(i,e){
      dtasource.push({id:i,text:e});
    });

    $("#chosen_website").select2({
        data:dtasource,
    });
  }
}

function toggleBootstrapWizard(){

	var selected_option = $("#servicedrp").val();
	switch(selected_option){
		case 'google-ads' : 
			$('#rootwizard').bootstrapWizard('display', 4);
			break;
		case 'facebookads' : 
			$('#rootwizard').bootstrapWizard('display', 4);
			break;		

		case 'tiktok-ads' : 
			$('#rootwizard').bootstrapWizard('display', 4);
			break;		

		case 'webdoctor' : 
			$('#rootwizard').bootstrapWizard('hide', 4);
			break;
		case 'webdesign' : 
			$('#rootwizard').bootstrapWizard('display', 4);
			break;
		case 'seo-traffic' :
			$('#rootwizard').bootstrapWizard('display', 4);
			break;
		case 'seo-top' :
			$('#rootwizard').bootstrapWizard('hide', 4);
			break;

		case 'webgeneral' :
			$('#rootwizard').bootstrapWizard('display', 4);
		case 'courseads' :
			$('#rootwizard').bootstrapWizard('hide', 2);
			break;

		case '0' : 
			$('#rootwizard').bootstrapWizard('hide', 4);
			break;
	}
}

function onNextSubmitHandler(ajax_action,ajax_form,forcereload)
{
	if(ajax_action != '')
	{
		$.post( ajax_action, ajax_form.serializeArray(), function( data ) {
			
				if(forcereload != '')
				{
					window.location.href = forcereload;
				}

				if($.isEmptyObject(data.response)) return;

				if(data.response.hasOwnProperty("jscallback")){

					$.each(data.response.jscallback,function(i,e){

						var fn = window[e.function_to_call];

						fn(e.data);
					});
				}
				

		}, "json");
	}
}

$.validator.addMethod('isValidPhone', function(value, element){
    if(this.optional(element)) return true;

    value = value.trim();

    // Validate VN phone number
    if(/^((03|05|07|08|09|01[2|6|8|9]|02[0|1|2|3|4|5|6|7|8|9]{1,2})+([0-9]{8})|(1900+([0-9]{4}|[0-9]{6})))$/g.test(value)) return true;

    // Validate Australia phone number
    if(/^(((\d *?){11})|([0-9a-zA-Z]{2}-[0-9a-zA-Z]{7}))$/g.test(value)) return true;

    // Validate US phone number
    if(/^\+61+[0-9]{9}$/g.test(value)) return true;

    // Validate South Korea phone number
    if(/^((001|\+82)+[0-9]{10})$/g.test(value)) return true;

    // Validate Hong Kong phone number
    if(/^((001|\+852)+[0-9]{8})$/g.test(value)) return true;

    // Validate Singapore phone number
    if(/^((001|\+65)+[0-9]{8})$/g.test(value)) return true;

    // Validate Taiwan phone number
    if(/^((0(2|3|4|5|6|7|8|9)|\+8869)+[0-9]{8})$/) return true;

    return false;
}, 'Cấu trúc số điện thoại không hợp lệ');

$.validator.addMethod('isValidTin', function(value, element){
    if(this.optional(element)) return true;

    value = value.trim();

    // Validation Syntax with Vietnam TIN
    if(/^[0-9a-zA-Z]{10}(-\d{3})?$/g.test(value)) return true;

    // Validation Syntax with Australia TIN
    if(/^(((\d *?){11})|([0-9a-zA-Z]{2}-[0-9a-zA-Z]{7}))$/g.test(value)) return true;
    
    // Validation Syntax with Japan TIN
    if(/^[0-9]{12,13}$/g.test(value)) return true;

    // Validation Syntax with Hongkong TIN
    if(/^(([A-Z]\d{6}[0-9A])|([0-9]{8}-[0-9A-Z]{3}-[0-9A-Z]{2}-[0-9A-Z]{2}-[0-9A-Z]{1}))$/g.test(value)) return true;

    // Validation Syntax with Singapore TIN
    if(/^(([0-9]{8,9}[a-zA-Z])|((F000|F   )[0-9]{5}[a-zA-Z])|((A|4)[0-9]{7}[a-zA-Z])|((20|19)[0-9]{2}(LP|LL|FC|PF|RF|MQ|MM|NB|CC|CS|MB|FM|GS|DP|CP|NR|CM|CD|MD|HS|VH|CH|MH|CL|XL|CX|HC|RP|TU|TC|FB|FN|PA|PB|SS|MC|SM|GA|GB)[0-9]{4}[a-zA-Z]))$/g.test(value)) return true;

    // Validation Syntax with Taiwan TIN
    if(/^[0-9]{8}$/g.test(value)) return true;

    return false;
}, 'Cấu trúc mã số thuế không hợp lệ');

$.validator.addMethod('isUniquePhone', function(value, element){
    let url = "<?php echo base_url('api-v2/customer/index/is_unique_phone') ?>";
    
    let type = $("input[name='tmp_customer_type']").val();
    let ignoreId = $("input[name='tmp_customer_id']").val();

    window.statusIsUniquePhone = true;
    $.get( url, {'value': value, 'type': type, 'ignoreId': ignoreId}, function( response ) {
        window.statusIsUniquePhone = response.data
	}, "json")
    .fail(function(response){})

    return this.optional(element) || window.statusIsUniquePhone;
}, 'Số điện thoại đã tồn tại');

$.validator.addMethod('isUniqueTin', function(value, element){
    let url = "<?php echo base_url('api-v2/customer/index/is_unique_tin') ?>";
    
    let type = $("input[name='tmp_customer_type']").val();
    let ignoreId = $("input[name='tmp_customer_id']").val();

    window.statusIsUniqueTin = true;
    $.get( url, {'value': value, 'type': type, 'ignoreId': ignoreId}, function( response ) {
        window.statusIsUniqueTin = response.data
	}, "json")
    .fail(function(response){})

    return this.optional(element) || window.statusIsUniqueTin;
}, 'Mã số thuế đã tồn tại');

$.validator.addMethod('isUniqueEmail', function(value, element){
    let url = "<?php echo base_url('api-v2/customer/index/is_unique_email') ?>";
    
    let type = $("input[name='tmp_customer_type']").val();
    let ignoreId = $("input[name='tmp_customer_id']").val();

    window.statusIsUniqueEmail = true;
    $.get( url, {'value': value, 'type': type, 'ignoreId': ignoreId}, function( response ) {
        window.statusIsUniqueEmail = response.data
	}, "json")
    .fail(function(response){})

    return this.optional(element) || window.statusIsUniqueEmail;
}, 'Email đã tồn tại');

$(function()
{
	$("#rootwizard ul li a[href='#tab-service'] span.tab-service").text($('#servicedrp option:selected').text());

	var detailfrm = $("#confirm_step_detail").closest('form');

	detailfrm.validate({  
		rules: {
		  'edit[meta][representative_name]': {
		    required: true,
		  },
		  'edit[meta][representative_address]': {
		    required: true,
		  },
		  'edit[meta][representative_email]': {
		    required: true,
		    email: true,
            isUniqueEmail: true,
		  },
		  'edit[meta][representative_phone]': {
		    required: true,
		    minlength: 3,
		    digits: true,
            isValidPhone: true,
            isUniquePhone: true,
		  },
		  'edit[meta][contract_daterange]' : {
		  	required:true,
		  },
		  'edit[meta][contract_note]' : {
		  	required:true,
		  },
		  'edit[extra][customer_tax]' : {
		  	required:function(){
		  		let _vat = $("input[name='edit[meta][vat]']").val();
		  		if(_vat > 0) return true;
		  		return false;
		  	},
            isValidTin: true,
            isUniqueTin: true,
		  },
		  'edit[meta][vat]' : {
		  	max : function(){
		  		let _term_type = $("input[name='edit[term_type]']").val();
		  		if(_term_type == 'webdesign') return 0;
		  		return 12;
		  	},
		  },
          'is_overwrite_assigned_to': {
              required:function(){
                let staff_business_selector_data = $("#staff_business_selector").data();

                if(undefined != staff_business_selector_data)
                {
                    let staff_business_assigned_ids = staff_business_selector_data.staffBusinessAssignedId || [];
                    if(0 == staff_business_assigned_ids.length)
                    {
                        return false;
                    }

                    let selected_id = $("#staff_business_selector").val();
                    let index_of_selected = staff_business_assigned_ids.findIndex((assigned_id) => assigned_id == selected_id);
                    if(0 > index_of_selected)
                    {
                        return true;
                    }
                }

                return false;
              },
          }
		},
		messages:{
		  'edit[meta][representative_name]': {
		    required: 'Trường này không được để trống',
		  },
		  'edit[meta][representative_address]': {
		    required: 'Trường này không được để trống',
		  },
		  'edit[meta][representative_email]': {
		    required: 'Trường này không được để trống',
		    email: 'Kiểu dữ liệu không hợp lệ',
		  },
		  'edit[meta][representative_phone]': {
		    required: 'Trường này không được để trống',
		    digits: 'Kiểu dữ liệu không hợp lệ , số điện thoại phải là kiểu số',
		  },
		  'edit[meta][contract_daterange]' : {
		  	required:'Trường này không được để trống',
		  },
		  'edit[meta][contract_note]' : {
		  	required:'Trường này không được để trống',
		  },
		  'edit[extra][customer_tax]' : {
		  	required:"Trường này không được để trống",
		  },
		  'edit[meta][vat]' : {
		  	max:'Giá trị thuế VAT không hợp lệ',
		  },
          'is_overwrite_assigned_to': {
            required:"Trường này không được để trống",
          }
		}
	});

	$("#contract_convert_name").val($('#chosen_website option:selected').text());

	$('#rootwizard').bootstrapWizard({'tabClass': 'bwizard-steps',
		onNext : function(tab, navigation, index) {
		var ajax_action = ajax_form = '';
		var forcereload = '';
		switch(index) {

		    case 1:
		    	var objchosen = $("#submit_chosen_type" ).closest('form').find("select[name='edit[term_type]']");

		    	if(objchosen.val() == 0){

    				$.notify('Vui lòng chọn dịch vụ !','error');
		    		return false;
		    	}

		        ajax_action = module_url + 'ajax_dipatcher/act_step1';
				ajax_form = $("#submit_chosen_type" ).closest('form');
				forcereload = "<?php echo module_url("create_wizard/index/{$edit->term_id}");?>";
				onNextSubmitHandler(ajax_action,ajax_form,forcereload);
				return false;
		        break;

		    case 2:

		    	if( ! _.gt($("#chosen_customer").val(), 0))
		    	{
		    		$.notify('Vui lòng chọn khách hàng !','error');
		    		return false;
		    	}
		    	
		        ajax_action = module_url + 'ajax_dipatcher/act_step2';
				ajax_form = $("#confirm_step_customer" ).closest('form');
				forcereload = "<?php echo module_url("create_wizard/index/{$edit->term_id}");?>";
				onNextSubmitHandler(ajax_action,ajax_form,forcereload);
				return false;
		        break;

		    case 3: /* STEP : Cấu hình thông tin website, dòng ngành của hợp đồng */

		    	/* Nếu hợp đồng thuộc gọi loại không có website tương tụ : Courseads => pass qua bước này */
			    // var objchosen = $("#confirm_step_website" ).closest('form').find("select[name='edit[term_parent]']");
			    var objchosen 	= $("#chosen_website");
		    	if(contract_type !== 'courseads' && _.isEmpty(objchosen.val()))
		    	{
    				$.notify('Hợp đồng cần có đối tượng áp dụng ! Xin vui lòng chọn Website !','error');
		    		return false;
		    	}

                if(contract_type == 'gws'){
                    let datasource = $("#chosen_website").select2('data');
                    if(datasource.length > 1){
                        $.notify(`Dịch vụ này chỉ nhận 01 Website/URL!`, 'error');
    
                        return false;
                    }
                    
                    for (const selected_item of datasource) {
                        let domain = _.get(selected_item, 'text');
    
                        let reg_pattern = /^[a-zA-Z][a-zA-Z0-9\-]{2,}\.(com|com\.vn|net|vn|org|edu|edu\.vn|gov|gov\.vn|info|asia|xzy)$/;
                        if(!reg_pattern.test(domain)){
                            $.notify(`Website ${domain} không khả dụng cho dịch vụ này!`, 'error');
    
                            return false;
                        }
                    }
                }

		    	forcereload = "<?php echo module_url("create_wizard/index/{$edit->term_id}");?>";
		        ajax_action = module_url + 'ajax_dipatcher/act_step3';
				ajax_form 	= $("#confirm_step_website" ).closest('form');

				onNextSubmitHandler(ajax_action,ajax_form,forcereload);
				return false;
		        break;

		    case 4:
		        ajax_action = module_url + 'ajax_dipatcher/act_step4';
				ajax_form = $("#confirm_step_detail").closest('form');
				ajax_form.validate({  
					rules: {
					  'edit[meta][representative_name]': {
					    required: true,
					  },
					  'edit[meta][representative_address]': {
					    required: true,
					  },
					  'edit[meta][representative_email]': {
					    required: true,
					    email: true,
					  },
					  'edit[meta][representative_phone]': {
					    required: true,
					    minlength: 3,
					    digits: true,
					  },
					  'edit[meta][contract_daterange]' : {
					  	required:true,
					  },
					  'edit[meta][contract_note]' : {
					  	required:true,
					  }
					},
					messages:{
					  'edit[meta][representative_name]': {
					    required: 'Trường này không được để trống',
					  },
					  'edit[meta][representative_address]': {
					    required: 'Trường này không được để trống',
					  },
					  'edit[meta][representative_email]': {
					    required: 'Trường này không được để trống',
					    email: 'Kiểu dữ liệu không hợp lệ',
					  },
					  'edit[meta][representative_phone]': {
					    required: 'Trường này không được để trống',
					    digits: 'Kiểu dữ liệu không hợp lệ , số điện thoại phải là kiểu số',
					  },
					  'edit[meta][contract_daterange]' : {
					  	required:'Trường này không được để trống',
					  },
					  'edit[meta][contract_note]' : {
					  	required:'Trường này không được để trống',
					  }
					}
				});
				var valid = ajax_form.valid();
				if(!valid) return false;

				forcereload = "<?php echo module_url("create_wizard/index/{$edit->term_id}");?>";
				onNextSubmitHandler(ajax_action,ajax_form,forcereload);
				return false;

		        break;

		  	case 5:
		        ajax_action = module_url + 'ajax_dipatcher/act_service';
				ajax_form = $("#confirm_step_service" ).closest('form');

                if('gws' == contract_type){
                    contract_configuration.$children[0].$validator.validate()
                    .then((result) => {
                        if( ! result) return result;

                        forcereload = "<?php echo module_url("create_wizard/index/{$edit->term_id}");?>";
                        onNextSubmitHandler(ajax_action,ajax_form,forcereload);
                        return false;
                    })
                    return false;
                }

                let rules = [];
                let messages = {};

                /* SECTION : VALIDATE RULE FOR WEBBUILD CONTRACT */
                rules['edit[meta][webdesign_price]'] = { required : true, min : 3000000, digits : true };
                messages['edit[meta][webdesign_price]'] = { 
                    required : 'Trường này không được để trống.', 
                    min : 'Giá trị hợp đồng không đạt mức tối thiểu.', 
                    digits : 'Giá trị nhập không hợp lệ.'
                };

                rules['edit[meta][discount_percent]'] = { min : 0, max : 100, digits : true };
                messages['edit[meta][discount_percent]'] = { 
                    min : 'Giá trị % giảm giá không đạt mức tối thiểu.', 
                    max : 'Giá trị % giảm giá không được vượt quá 100 %.', 
                    digits : 'Giá trị nhập không hợp lệ.'
                };
                /* END SECTION : VALIDATE RULE FOR WEBBUILD CONTRACT */


                /* SECTION : VALIDATE RULE FOR ADSPLUS CONTRACT */
                rules['edit[meta][contract_cpc]'] = {
                    required : {
                        depends : function(element) {

                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'cpc_type') return false;

                            var same_field_another_type = $("input[name='edit[meta][gdn_contract_cpc]']").val();

                            if ( typeof same_field_another_type == 'undefined' 
                            || same_field_another_type  == '' 
                            || same_field_another_type  <= 0) return true;

                            return false;
                        }
                    },
                    min : {
                        param : Number.MAX_SAFE_INTEGER, // a larged Integer to trigger validate min
                        depends : function(element)
                        {
                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'cpc_type') return false;

                            var contract_cpc = $("input[name='edit[meta][contract_cpc]']").val();
                            var contract_clicks = $("input[name='edit[meta][contract_clicks]']").val();

                            var gdn_contract_cpc = $("input[name='edit[meta][gdn_contract_cpc]']").val();
                            var gdn_contract_clicks = $("input[name='edit[meta][gdn_contract_clicks]']").val();

                            var total = (contract_cpc*contract_clicks) + (gdn_contract_cpc*gdn_contract_clicks);

                            return total < 3500000;
                        }
                    },
                    digits: true,
                };
                messages['edit[meta][contract_cpc]'] = {
                    required: 'Trường này không được để trống',
                    digits: 'Kiểu dữ liệu không hợp lệ',
                    min : 'Tổng giá trị hợp đồng không đạt mức tối thiểu'
                };

                rules['edit[meta][contract_clicks]'] = {
                    required : {
                        depends : function(element)
                        {
                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'cpc_type') return false;

                            var same_field_another_type = $("input[name='edit[meta][gdn_contract_clicks]']").val();

                            if ( typeof same_field_another_type == 'undefined' 
                            || same_field_another_type  == '' 
                            || same_field_another_type  <= 0) return true;

                            return false;
                        }
                    },
                    min : {
                        param : Number.MAX_SAFE_INTEGER, // a larged Integer to trigger validate min
                        depends : function(element)
                        {
                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'cpc_type') return false;

                            var contract_cpc = $("input[name='edit[meta][contract_cpc]']").val();
                            var contract_clicks = $("input[name='edit[meta][contract_clicks]']").val();

                            var gdn_contract_cpc = $("input[name='edit[meta][gdn_contract_cpc]']").val();
                            var gdn_contract_clicks = $("input[name='edit[meta][gdn_contract_clicks]']").val();

                            var total = (contract_cpc*contract_clicks) + (gdn_contract_cpc*gdn_contract_clicks);

                            return total < 3500000;
                        }
                    },
                    digits: true,
                };
                messages['edit[meta][contract_clicks]'] = {
                    required: 'Trường này không được để trống',
                    digits: 'Kiểu dữ liệu không hợp lệ',
                    min : 'Tổng giá trị hợp đồng không đạt mức tối thiểu'
                };

                rules['edit[meta][gdn_contract_cpc]'] = {
                    required : {
                        depends : function(element){

                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'cpc_type') return false;

                            var same_field_another_type = $("input[name='edit[meta][contract_cpc]']").val();

                            if ( typeof same_field_another_type == 'undefined' 
                            || same_field_another_type  == '' 
                            || same_field_another_type  <= 0) return true;

                            return false;
                        }
                    },
                    min : {
                        param : Number.MAX_SAFE_INTEGER, // a larged Integer to trigger validate min
                        depends : function(element)
                        {
                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'cpc_type') return false;

                            var contract_cpc = $("input[name='edit[meta][contract_cpc]']").val();
                            var contract_clicks = $("input[name='edit[meta][contract_clicks]']").val();

                            var gdn_contract_cpc = $("input[name='edit[meta][gdn_contract_cpc]']").val();
                            var gdn_contract_clicks = $("input[name='edit[meta][gdn_contract_clicks]']").val();

                            var total = (contract_cpc*contract_clicks) + (gdn_contract_cpc*gdn_contract_clicks);

                            return total < 3500000;
                        }
                    },
                    digits: true,
                };
                messages['edit[meta][gdn_contract_cpc]'] = {
                    required: 'Trường này không được để trống',
                    digits: 'Kiểu dữ liệu không hợp lệ',
                    min : 'Tổng giá trị hợp đồng không đạt mức tối thiểu'
                };

                rules['edit[meta][gdn_contract_clicks]'] = {
                    required : {
                        depends : function(element)
                        {
                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'cpc_type') return false;

                            var same_field_another_type = $("input[name='edit[meta][contract_clicks]']").val();

                            if ( typeof same_field_another_type == 'undefined' 
                            || same_field_another_type  == '' 
                            || same_field_another_type  <= 0) return true;

                            return false;
                        }
                    },
                    min : {
                        param : Number.MAX_SAFE_INTEGER, // a larged Integer to trigger validate min
                        depends : function(element)
                        {
                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'cpc_type') return false;

                            var contract_cpc = $("input[name='edit[meta][contract_cpc]']").val();
                            var contract_clicks = $("input[name='edit[meta][contract_clicks]']").val();

                            var gdn_contract_cpc = $("input[name='edit[meta][gdn_contract_cpc]']").val();
                            var gdn_contract_clicks = $("input[name='edit[meta][gdn_contract_clicks]']").val();

                            var total = (contract_cpc*contract_clicks) + (gdn_contract_cpc*gdn_contract_clicks);

                            return total < 3500000;
                        }
                    },
                    digits: true,
                };
                rules['edit[meta][gdn_contract_clicks]'] = {
                    required: 'Trường này không được để trống',
                    digits: 'Kiểu dữ liệu không hợp lệ',
                    // min : '[GDN] Click cam kết không hợp lệ - Tổng giá trị hợp đồng không đạt mức tối thiểu'
                };

                rules['edit[meta][number_of_payments]'] = {
                    required : {
                        depends : function(element)
                        {
                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'account_type') return false;

                            return true;
                        }
                    },
                    digits: true,
                    min : 1,
                    max : 50,
                };
                messages['edit[meta][number_of_payments]'] = { 
                    required : 'Số đợt thanh toán không được để trống',
                    min : 'Số lần thanh toán ít nhất phải bằng 1',
                    max : 'Số lần thanh toán vượt quá ngưỡng tối đa cho phép' 
                };

                rules['edit[meta][exchange_rate_usd_to_vnd]'] = {
                    required : {
                        depends : function(element)
                        {
                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'account_type') return false;

                            return true;
                        }
                    },
                    digits: true,
                    min : 22000,
                };
                messages['edit[meta][exchange_rate_aud_to_vnd]'] = {
                    required: 'Trường này không được để trống',
                    digits: 'Kiểu dữ liệu không hợp lệ',
                    min : 'Tỷ giá không đạt mức tối thiểu'
                };

                rules['edit[meta][contract_budget]'] = {
                    required : {
                        depends : function(element)
                        {
                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'account_type') return false;

                            return true;
                        }
                    },
                    digits: true,
                    min :
                    {
                        param : 5000000,
                        depends : function(element)
                        {
                            var contract_budget = $("input[name='edit[meta][contract_budget]']").val();

                            var service_fee = $("input[name='edit[meta][service_fee]']").val();
                            var total       = contract_budget + service_fee;

                        	let term_type = $("select[name='edit[term_type]']").val();
                        	switch(term_type)
                        	{
                        		case 'facebook-ads' : 
                        			return total < 1;
                        			break;
                        		case 'google-ads' : 
                        			if(contract_budget < 3500000) return true;
		                            return total < 5000000;
                        			break;
                                case 'zalo-ads' : 
                        			if(contract_budget < 3500000) return true;
		                            return total < 5000000;
                        			break;
                        		default : 
                        			return false;
                        			break;
                        	}

                        	return false;
                        }
                    }
                };
                messages['edit[meta][contract_budget]'] = {
                    required: 'Trường này không được để trống',
                    min : 'Tổng giá trị hợp đồng (ngân sách + phí dịch vụ) phải lớn hơn 5.000.000đ'
                };

                rules['edit[meta][service_fee]'] = {
                    required : {
                        depends : function(element)
                        {
                            var service_type = $("input[name='edit[meta][service_type]']").val();
                            if(service_type != 'account_type') return false;

                            return true;
                        }
                    },
                    min :
                    {
                        param : 5000000,
                        depends : function(element)
                        {
                        	var service_fee = $("input[name='edit[meta][service_fee]']").val();
                            var contract_budget = $("input[name='edit[meta][contract_budget]']").val();
                            var total           = contract_budget + service_fee;

                        	let term_type = $("select[name='edit[term_type]']").val();
                        	switch(term_type)
                        	{
                        		case 'facebook-ads' : 
                        			return total < 1;
                        			break;
                        		case 'google-ads' : 
                        			if(service_fee  < 1000000) return true;
		                            return total < 5000000;
                        			break;
                        		default : 
                        			return false;
                        			break;
                        	}

                        	return false;
                        }
                    },
                    digits: true,
                };
                messages['edit[meta][service_fee]'] = {
                    required: 'Trường này không được để trống',
                    min : 'Tổng giá trị hợp đồng (ngân sách + phí dịch vụ) phải lớn hơn 5.000.000đ'
                };

                rules['edit[meta][googleads-begin_time]'] = { required: true };
                messages['edit[meta][googleads-begin_time]'] = { required: 'Trường này không được để trống' };

                rules['edit[meta][advertise_start_time]'] = { required: true };
                messages['edit[meta][advertise_start_time]'] = { required: 'Trường này không được để trống' };

                var lengthOfServiceFeePlan = parseInt($("input[name='edit[meta][service_fee_plan][length]']").val(), 10);
                for (let index = 0; index < lengthOfServiceFeePlan; index++) {
                    rules[`edit[meta][service_fee_plan][from][${index}]`] = {
                        required : {
                            depends : function(element) {

                                var contract_budget_payment_type = $("input[name='edit[meta][contract_budget_payment_type]']").val();
                                if('normal' != contract_budget_payment_type) return false;

                                return true;
                            }
                        },
                        
                        min : {
                            param: 5000000, // a larged Integer to trigger validate min
                            depends : function(element)
                            {
                                if(index > 0 && lengthOfServiceFeePlan > 1){
                                    var service_fee_plan_prev_to = parseInt($(`input[name='edit[meta][service_fee_plan][to][${index - 1}]']`).val(), 10);
                                    var service_fee_plan_curr_to = parseInt($(`input[name='edit[meta][service_fee_plan][to][${index}]']`).val(), 10);
                                    
                                    return service_fee_plan_prev_to > service_fee_plan_curr_to;
                                }

                                return true;
                            }
                        },
                        digits: true,
                    };
                    messages[`edit[meta][service_fee_plan][from][${index}]`] = {
                        required: 'Trường này không được để trống',
                        digits: 'Kiểu dữ liệu không hợp lệ',
                        min : index == 0 ? 'Mức ngân sách thấp nhất là 5.000.000 VNĐ' : 'Mức ngân sách phải lớn hơn mức ngân sách trước đó'
                    };

                    rules[`edit[meta][service_fee_plan][to][${index}]`] = {
                        required : {
                            depends : function(element) {

                                var contract_budget_payment_type = $("input[name='edit[meta][contract_budget_payment_type]']").val();
                                if('normal' != contract_budget_payment_type) return false;

                                return true;
                            }
                        },
                        
                        min : {
                            param: Number.MAX_SAFE_INTEGER, // a larged Integer to trigger validate min
                            depends : function(element)
                            {
                                var service_fee_plan_to = parseInt($(`input[name='edit[meta][service_fee_plan][to][${index}]']`).val(), 10);
                                if(service_fee_plan_to == 0){
                                    return false;
                                }

                                var service_fee_plan_from = parseInt($(`input[name='edit[meta][service_fee_plan][from][${index}]']`).val(), 10);

                                return (service_fee_plan_from > service_fee_plan_to);
                            }
                        },
                        digits: true,
                    };
                    messages[`edit[meta][service_fee_plan][to][${index}]`] = {
                        required: 'Trường này không được để trống',
                        digits: 'Kiểu dữ liệu không hợp lệ',
                        min : 'Mức ngân sách phải lớn hơn mức ngân sách trước đó'
                    };

                    rules[`edit[meta][service_fee_plan][service_fee_rate][${index}]`] = {
                        required : {
                            depends : function(element) {

                                var contract_budget_payment_type = $("input[name='edit[meta][contract_budget_payment_type]']").val();
                                if('normal' != contract_budget_payment_type) return false;

                                return true;
                            }
                        },
                        max : {
                            param: 100, // a larged Integer to trigger validate min
                        },
                        min : {
                            param: Number.MAX_SAFE_INTEGER, // a larged Integer service_fee_rate trigger validate min50
                            depends : function(element)
                            {
                                return element > 0;
                            }
                        },
                        digits: true,
                    };
                    messages[`edit[meta][service_fee_plan][service_fee_rate][${index}]`] = {
                        required: 'Trường này không được để trống',
                        digits: 'Kiểu dữ liệu không hợp lệ',
                        max : '% Phí dịch vụ tối đa là 100',
                        min : '% Phí dịch vụ không hợp lệ',
                    };

                }

                rules['edit[meta][hubspot_link]'] = {
                    required : true,
                };
                messages['edit[meta][hubspot_link]'] = {
                    required: 'Trường này không được để trống',
                };

                /* END SECTION : VALIDATE RULE FOR ADSPLUS CONTRACT */

                ajax_form.validate({ rules : rules, messages : messages});

                var valid = ajax_form.valid();

				if(!valid) return false;

				forcereload = "<?php echo module_url("create_wizard/index/{$edit->term_id}");?>";
				onNextSubmitHandler(ajax_action,ajax_form,forcereload);
				return false;

		        break;
		    case 6:
		        ajax_action = module_url + 'ajax_dipatcher/act_finish';
				ajax_form = $("#confirm_step_finish" ).closest('form');
				forcereload = "<?php echo module_url("create_wizard/index/{$edit->term_id}");?>";
				onNextSubmitHandler(ajax_action,ajax_form,forcereload);
				return false;
		        break;
		}
	}, 
	onPrevious : function(tab, navigation, index) {

		let _term_type = $("#submit_chosen_type").closest('form').find("select[name='edit[term_type]']").val();

		if(_term_type == 'courseads') 
		{
			$('#rootwizard').bootstrapWizard('show', index); 
		}
	},
	onTabShow: function(tab, navigation, index) {
		var $total = navigation.find('li').length;
		var $current = index + 1;
		var $percent = ($current/$total) * 100;
		$('#rootwizard .progress-bar').css({width:$percent+'%'});
	},
	onTabClick: function(tab, navigation, index) {
		return false;
	}});

	$('#finishstep').click(function() {

		ajax_form = $("#confirm_step_finish").closest('form');
		$.notify('Hệ thống đang xử lý, vui lòng đợi.','info');
		$.post( "<?php echo module_url("ajax/create_wizard/complete/{$edit->term_id}");?>", ajax_form.serializeArray(), function( response ) {
			
			$('#finishstep').hide();
			window.location = response.data.redirect_url;

		}, "json").fail(function(response){
			$.notify('Lỗi xảy ra: '+response.responseJSON.msg,'error');
		});
	});

	toggleBootstrapWizard();
	
	if(wizard_step != '')
	{
		var maxTabs = $('#rootwizard').bootstrapWizard('navigationLength');
		wizard_step = parseInt(wizard_step);

		switch(contract_type)
		{
			case 'courseads' : 
				if(wizard_step == 2) wizard_step++;
				break;

			case 'webdoctor' : 
				if(wizard_step == 4) wizard_step+=1;
				break;
		}

		$('#rootwizard').bootstrapWizard('show',wizard_step);
	}

	window.prettyPrint && prettyPrint();	
});
</script>