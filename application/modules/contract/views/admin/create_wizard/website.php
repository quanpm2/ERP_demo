<div class="col-md-12">
<?php 

$edit_lock = in_array($edit->term_status, array('publish',1)) ? TRUE : FALSE;

echo $this->admin_form->form_open();

// Chọn ngành nghề thực hiện hợp đồng

$c_options = array();
if(!empty($hcategories))
{
	foreach ($hcategories as $item)
	{
		if(!isset($c_options[$item->term_name]))
		{
			$c_options[$item->term_name] = array();
		}

		if(empty($item->child)) continue;

		foreach ($item->child as $key => $value)
		{
			$c_options[$item->term_name][$value->term_id] = $value->term_name;
		}
	}
}

echo $this->admin_form->fieldset_open('Dòng ngành thực hiện hợp đồng');
echo $this->admin_form->dropdown('Dòng ngành', 'term_categories[]', ($c_options ?? array()), ($term_categories ?? array()), 'Nhập dòng ngành nghề sẽ thực hiện hợp đồng này để hệ thống tối ưu hiệu quả thực hiện dịch vụ !',['multiple'=>'multiple']);
echo $this->admin_form->fieldset_close();


// Chọn website thực hiện hợp đồng

$websiteIds = get_term_meta($edit->term_id, 'websites', FALSE, TRUE) ?: [];
$websiteIds AND $websiteIds = array_map('intval', $websiteIds);
$websiteIds = array_unique(array_merge($websiteIds, [ $edit->term_parent ]));

echo $this->admin_form->fieldset_open('Website/URL');
echo $this->admin_form->fieldset_close();
echo $this->admin_form->dropdown('Website/URL', 'edit[term_parent][]', @$websites, $websiteIds, 'Website/URL thực hiện hợp đồng thuộc sở hữu của khách hàng', ['id'=>'chosen_website', 'multiple' => true]);

echo form_hidden('edit[term_id]', $edit->term_id);
echo $this->admin_form->submit('','confirm_step_website','confirm_step_website','',array('style'=>'display:none;','id'=>'confirm_step_website'));

echo $this->admin_form->form_close();
?>

<!-- FORM CREATE NEW WEBSITE -->
<?php if (!$edit_lock || empty($edit->term_parent)):?>
	<?php $website_create_api_url = admin_url('customer/ajax/website/create'); ?>
	<div class="clearfix"></div>
	<?php echo $this->admin_form->formGroup_begin(0,'<code>Hoặc tạo mới</code>'); ?>
	<?php echo $this->admin_form->form_open($website_create_api_url,['id'=>'createwebfrm'],['customer_id'=>$customer->user_id??0]); ?>
	<div class="row">
		<div class="col-xs-6 col-sm-4 col-md-3">
			<?php echo form_input(array('name'=>'edit[term_name]','class'=>'form-control','placeholder'=>'Website/URL')); ?>
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3">
			<?php echo form_input(array('name'=>'edit[term_description]','class'=>'form-control','placeholder'=>'Mô tả')); ?>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-3">	
			<?php echo form_button(['id'=>'add_website','class'=>'btn btn-primary setting_options'],'<i class="fa fa-fw fa-plus"></i> Thêm mới website'); ?>
		</div>
		
	</div>
	<?php echo $this->admin_form->form_close(); ?>
	<?php echo $this->admin_form->formGroup_end(); ?>
<?php endif ?>

</div>

<script type="text/javascript">

$.validator.addMethod("domain", function(value, element) {
  return this.optional(element) || !/^(http(s)?\/\/:)?(www\.)?[a-zA-Z\-]{3,}(\.[a-z]{2,4})?$/.test(value);
}, "Tên miền không hợp lệ hoặc đã được sử dụng");

function bindSubmitWebsiteForm(form)
{
	$.post( form.attr('action'),form.serializeArray(),

		function( response ) {

			var datasource = $("#chosen_website").select2('data');
			datasource.push({id : response.data.insert_id, text:response.data.website});
			
			$("#chosen_website").select2({ data : datasource });
			$("#chosen_website").val(response.data.insert_id).trigger("change");

			$('#createwebfrm div.alert-danger').remove();
			var html = "<div class='alert alert-success alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='error-handler'>Thêm mới thành công</div></div>";
    		$('#createwebfrm').prepend(html);

	},"json").fail(function(response) {
		
			$('#createwebfrm div.alert-danger').remove();
			var html = "<div class='alert alert-danger alert-dismissible customer-alert-msg'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><div class='error-handler'>"+response.responseJSON.msg+"</div></div>";
    		$('#createwebfrm').prepend(html);
		});
}

$(document).ready(function() {	

	$('#chosen_website').css('width','100%');
	$('#chosen_website').select2({ data:[]});
	$("#chosen_website").on("change", function(e){
		$("#contract_convert_name").val($('#chosen_website option:selected').text());
	});

	$("#createwebfrm").validate({
		rules: {
			"edit[term_name]":{
				required: true,
				// remote: {
				// 	url: "<?php echo admin_url('customer/ajax/website/domain_check');?>",
				// 	type: "post",
				// 	data: { domain: function() { return $("#createwebfrm input[name='edit[term_name]").val(); } }
				// }
			},
		},
		messages: {
            "edit[term_name]": {
            	required : "Tên miền không được để trống.",
            	remote : "Tên miền không hợp lệ hoặc đã tồn tại.",
            }
        }
    });

	
	$('#add_website').click(function(){

        var form = $(this).closest('form');
        var valid = form.valid();
        if(!valid) return false;

        bindSubmitWebsiteForm(form);
    });
});
</script>