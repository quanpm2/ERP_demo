<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
$this->template->javascript->add(base_url('dist/vContractUpdate.js?v=202105270940'));

echo $this->admin_form->form_open('',['id' => 'contract-wizard-customers-setting-stage'],['edit[term_id]' => $edit->term_id, 'user_updated' => 0]);
?>
<div class="col-md-12" id="app-containercontainer">
    <v-customer-contract-wizard-tab v-bind:customerId="<?php echo $customer->user_id ?? 0;?>"/>
</div>
<?php
echo $this->admin_form->submit('','confirm_step_customer','confirm_step_customer','',array('style'=>'display:none;','id'=>'confirm_step_customer'));
echo $this->admin_form->form_close();