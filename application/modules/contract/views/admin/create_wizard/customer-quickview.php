
<?php
echo $this->admin_form->formGroup_begin(0,' ');
echo $this->admin_form->set_col(6)->box_open('Thông tin khách hàng');

$this->table->clear()->add_row('Loại hình',($customer->user_type == 'customer_person' ? 'Cá nhân' : 'Tổ chức'));

// ->add_row('Người đại diện',force_var(get_term_meta_value($customer->term_id,'representative_gender'),'Bà','Ông').' '.get_term_meta_value($customer->term_id,'representative_name'))
// ->add_row('Email',force_var(get_term_meta_value($customer->term_id,'representative_email'),'Chưa cập nhật'))
// ->add_row('Địa chỉ',force_var(get_term_meta_value($customer->term_id,'representative_address'),'Chưa cập nhật'))
// ->add_row('Số điện thoại',force_var(get_term_meta_value($customer->term_id,'representative_phone'),'Chưa cập nhật'))
// ->add_row('Chức vụ',force_var($customer->extra['representative_position'],'Chưa cập nhật'))
// ->add_row('Mã Số thuế',force_var($customer->extra['customer_tax'],'Chưa cập nhật'))
// ->add_row('Thời gian thực hiện',force_var(get_term_meta_value($customer->term_id,'contract_daterange'),'Chưa cập nhật'))

echo $this->table->generate();
echo $this->admin_form->box_close();
echo $this->admin_form->formGroup_end();
?>