<div class="col-md-12">
<?php 
$edit_lock = in_array($edit->term_status, array('publish',1)) ? TRUE : FALSE;

echo $this->admin_form->form_open();

if(!$edit_lock) {

	echo $this->admin_form->dropdown('Chọn website', 'edit[term_parent]', @$websites, $edit->term_parent, '', array('id'=>'chosen_website'));
}
else{

	$attrs = array('id'=>'chosen_website','disabled'=>'disabled');
	$attrs = array('id'=>'chosen_website');

	if(empty($edit->term_parent)) unset($attrs['disabled']);

	echo $this->admin_form->dropdown('Chọn website', 'edit[term_parent]', @$websites, $edit->term_parent, '', $attrs);
}

echo form_hidden('edit[term_id]', $edit->term_id);

echo $this->admin_form->submit('','confirm_step_website','confirm_step_website','',array('style'=>'display:none;','id'=>'confirm_step_website'));

echo $this->admin_form->form_close();

if(!$edit_lock || empty($edit->term_parent)){
	
	echo $this->admin_form->formGroup_begin(0,'Hoặc');
	echo $this->admin_form->form_open('',array('id'=>'createwebfrm'));
?>
	<div class="row">
		<div class="col-xs-3">
		<?php 
		echo form_input(array('name'=>'edit[term_name]','class'=>'form-control','placeholder'=>'website domain'));
		?>
		</div>
		<div class="col-xs-3">
		<?php
		echo form_input(array('name'=>'edit[term_description]','class'=>'form-control','placeholder'=>'Mô tả'));
		
		echo $this->admin_form->hidden('','user_id','','', array('id'=>'userid_term'));
		?>
		</div>
		<?php
		echo $this->admin_form->submit( array('id'=>'add_website','class'=>'btn btn-info setting_options'),'Thêm mới website');
		?>
	</div>
<?php
	echo $this->admin_form->form_close();
	echo $this->admin_form->formGroup_end();
}

?>
</div>

<script type="text/javascript">

$.validator.addMethod("domain", function(value, element) {
  return this.optional(element) || !/^(http(s)?\/\/:)?(www\.)?[a-zA-Z\-]{3,}(\.[a-z]{2,4})?$/.test(value);
}, "Tên miền không hợp lệ");

$(document).ready(function() {	

	$('#chosen_website').select2({ data:[{id:0,text:"Chọn website"}] });

	$("#chosen_website").on("change", function(e) {
		
		$("#contract_convert_name").val($('#chosen_website option:selected').text());
	});

	$('#chosen_website').css('width','100%');

	$("#createwebfrm").validate({
		rules: {
			"edit[term_name]":{
				required:true,
				// domain:true,
				// remote: {
				// 	url: module_url + "check_domain",
				// 	type: "post",
				// 	data: {
				// 		domain: function() {
				// 			return $( "input[name='edit[term_name]']" ).val();
				// 		}
				// 	}
				// }
			},
		},
		messages: {
            "edit[term_name]": {
            	required : "Tên miền không được để trống",	
            	// domain : "Tên miền không hợp lệ hoặc đã tồn tại",
            	// remote : "Tên miền không hợp lệ hoặc đã tồn tại",
            }
        },
        submitHandler: function(form) {

        	$("#userid_term").val($('#chosen_customer option:selected').val());

        	$.post( module_url + 'ajax_dipatcher/act_create_website'

    			, $(form).serializeArray()

    			, function( data ) {

    				console.log(data);

    				data.response.success = data.response.success ? 'info' : 'error';

    				$.notify(data.response.msg,'info');

    				if (typeof data.response.ret_obj != 'undefined') {

    					_datasource = [];

						_datasource.push({id:data.response.ret_obj.id,text:data.response.ret_obj.text});

						$("#chosen_website").select2({
						    data:_datasource,
						});  

						$("#chosen_website").val(data.response.ret_obj.id).trigger("change");
					}
				}
				, "json");

        	return false;
        },
	});
});
</script>