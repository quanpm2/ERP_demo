<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');
$this->template->javascript->add('https://cdn.jsdelivr.net/vuejs-paginator/2.0.0/vuejs-paginator.min.js');
?>
<div id="setting-page-root" class="col-md-12"> <setting-page-component></setting-page-component> </div>

<script type="text/javascript">
var jsobjectdata = <?php echo $jsdataobject;?>;

Vue.component('setting-page-component', {
    template: `
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class=""> <a href="#tab_1" data-toggle="tab"><i class="fa fw fa-usd"></i> BẢO LÃNH HĐ</a> </li>
			<li class="active"> <a href="#tab_2" data-toggle="tab"><i class="fa fw fa-print"></i> HỢP ĐỒNG</a> </li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane" id="tab_1">
				<sales-datatable-component></sales-datatable-component>
			</div>
			<div class="tab-pane active" id="tab_2">
				<contract-represent-config-component></contract-represent-config-component>
			</div>
		</div>
	</div>`
});
</script>

<!-- /.col -->
<?php
$v = 201811230431;
echo $this->template->trigger_javascript(admin_theme_url("modules/component/form.js?v={$v}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/contract/js/contract-represent-config-row.js?v={$v}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/contract/js/contract-represent-config.js?v={$v}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/contract/js/setting-page-app.js?v={$v}"));