<div class="col-md-12" id="review-partial">
   <h2 class="page-header">
      <i class="fa fa-globe"></i> Mã số hợp đồng : <?php echo $this->config->item($edit->term_type, 'contract_prefix') . $edit->term_id;?> 
      <small class="pull-right">Ngày tạo : <?php echo date('d-m-Y',time());?></small>
   </h2>
   <div class="row">
      <div class="col-xs-12">
         <p class="lead">Thông tin khách hàng</p>
         <div class="table-responsive">
            <?php
               echo
               $this->table
                 ->clear()
                 ->add_row(array('Người đại diện',force_var(get_term_meta_value($edit->term_id, 'representative_gender'),'Bà','Ông').' '.
                     get_term_meta_value($edit->term_id, 'representative_name')))
                 ->add_row(array('Email',force_var(get_term_meta_value($edit->term_id,'representative_email'),'Chưa cập nhật')))
                 ->add_row(array('Địa chỉ',force_var(get_term_meta_value($edit->term_id,'representative_address'),'Chưa cập nhật')))
                 ->add_row(array('Số điện thoại',force_var(get_term_meta_value($edit->term_id,'representative_phone'),'Chưa cập nhật')))
                 ->add_row(array('Chức vụ',force_var($edit->extra['representative_position'],'Chưa cập nhật')))
                 ->add_row(array('Mã Số thuế',force_var($edit->extra['customer_tax'],'Chưa cập nhật')))
                 ->add_row(array('Thời gian thực hiện',force_var(get_term_meta_value($edit->term_id,'contract_daterange'),'Chưa cập nhật')))
                 ->generate();
               ?>
         </div>
      </div>
      <!-- /.col -->    
   </div>
</div>
<?php
   echo $this->admin_form->form_open();
   echo $this->admin_form->hidden('','edit[term_status]', 'waitingforapprove');
   echo $this->admin_form->hidden('','edit[term_name]','', '', array('id'=>'contract_convert_name'));
   echo form_hidden('edit[term_id]',$edit->term_id);
   echo $this->admin_form->submit('','confirm_step_finish','confirm_step_finish','', array('style'=>'display:none;','id'=>'confirm_step_finish'));
   echo $this->admin_form->form_close();
   ?>
<div style="clear: both;"></div>   
<script type="text/javascript">
   $(function(){
     $("#contract_convert_name").val($('#chosen_website option:selected').text());
   });
</script>