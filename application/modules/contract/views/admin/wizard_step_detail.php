<?php
$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

$this->template->stylesheet->add('plugins/bootstrap-fileinput/css/fileinput.css');
$this->template->javascript->add('plugins/bootstrap-fileinput/js/fileinput.min.js');
?>

<div class="col-md-12" id="step-detail-partial">
<?php

$name = $this->termmeta_m->get_meta_value($edit->term_id, 'representative_name');
$lock_editable = get_term_meta_value($edit->term_id,'lock_editable');

if(empty($name) && !empty($customer)){

	$name = $customer->user_type == 'customer_person' 
	? $customer->display_name 
	: $this->usermeta_m->get_meta_value($customer->user_id,'customer_name');
}

echo $this->admin_form->form_open();

echo $this->admin_form->input(
	'Mã hợp đồng',
	'edit[meta][contract_code]',
	$this->termmeta_m->get_meta_value($edit->term_id, 'contract_code'),
	'Nếu để trống hệ thống sẽ tự động tạo mã hợp đồng , vd: HD249/0316/example.com với 249 là ID hợp đồng và 0316 là tháng_năm'
);

echo form_hidden('edit[term_id]',$edit->term_id);
echo form_hidden('edit[term_type]',$edit->term_type);

echo $this->admin_form->formGroup_begin(0,'Người đại diện');

echo '<div class="col-xs-2" style="padding-left: 0;">';

$gender = $this->termmeta_m->get_meta_value($edit->term_id, 'representative_gender');

$gender = empty($gender) ? $this->usermeta_m->get_meta_value(@$customer->user_id, 'customer_gender') : $gender;

echo form_dropdown(
	array('name'	=>'edit[meta][representative_gender]',
		'class'	=>'form-control'),
	array(1	=>'Ông',
		0	=>'Bà'), 
	$gender);

echo '</div>';

echo '<div class="col-xs-10">';

echo form_input(array('name'=>'edit[meta][representative_name]','class'=>'form-control'), $name);

echo '</div>';

echo $this->admin_form->formGroup_end();

$email = $this->termmeta_m->get_meta_value($edit->term_id, 'representative_email');

$email = empty($email) ? $this->usermeta_m->get_meta_value(@$customer->user_id, 'customer_email') : $email;

echo $this->admin_form->input('Email','edit[meta][representative_email]', $email);

$address = $this->termmeta_m->get_meta_value($edit->term_id, 'representative_address');
$address = empty($address) ? $this->usermeta_m->get_meta_value(@$customer->user_id, 'customer_address') : $address;

echo $this->admin_form->input('Địa chỉ','edit[meta][representative_address]', $address);

$phone = $this->termmeta_m->get_meta_value($edit->term_id, 'representative_phone');
$phone = empty($phone) ? $this->usermeta_m->get_meta_value(@$customer->user_id, 'customer_phone') : $phone;

echo $this->admin_form->input('Điện thoại','edit[meta][representative_phone]', $phone);

$position = empty($edit->extra['representative_position']) 
? $this->usermeta_m->get_meta_value(@$customer->user_id, 'customer_position')
: $edit->extra['representative_position'];

echo $this->admin_form->input('Chức vụ','edit[extra][representative_position]', $position);

echo '<div id="customer_profile">'. $this->template->customer_profile .'</div>';

echo $this->admin_form->input('Mã Số thuế','edit[extra][customer_tax]', $edit->extra['customer_tax']);

$contract_daterange = array();

if($_contract_begin = $this->termmeta_m->get_meta_value($edit->term_id, 'contract_begin')){

	$contract_daterange['begin'] = date('d-m-Y', $_contract_begin);
}
if($contract_end = $this->termmeta_m->get_meta_value($edit->term_id, 'contract_end')){

	$contract_daterange['end'] = date('d-m-Y', $contract_end);
}

$contract_daterange = (!empty($contract_daterange['begin']) && !empty($contract_daterange['end']))
? $contract_daterange['begin'] . ' - ' . $contract_daterange['end']	
: '';

echo $this->admin_form->input('Thời gian thực hiện'
	, 'edit[meta][contract_daterange]'
	, $contract_daterange
	, ''
	, array('class'=>'form-control','id'=>'input_daterange'));

echo $this->admin_form->formGroup_begin(0,'Thông tin ngân hàng');

echo '<div class="col-xs-4" style="padding-left: 0;">';

echo form_input(
	array('name'	=> 'edit[extra][bank_id]',
		'class'	=> 'form-control',
		'placeholder'	=> 'Số tài khoản'),
	$edit->extra['bank_id']);

echo '</div>';

echo '<div class="col-xs-4">';

echo form_input(
	array('name'	=> 'edit[extra][bank_owner]',
		'class'	=> 'form-control',
		'placeholder'	=> 'Chủ ngân hàng'),
	$edit->extra['bank_owner']);

echo '</div>';

echo '<div class="col-xs-4">';

echo form_input(
	array('name'	=> 'edit[extra][bank_name]',
		'class'	=> 'form-control',
		'placeholder'	=> 'Ngân hàng'),
	$edit->extra['bank_name']);

echo '</div>';

echo $this->admin_form->formGroup_end();

# check role of admin is in sale groups & build input form for its
$sale_groupids = $this->config->item('salesexecutive','groups');
$is_sale_role = (in_array($this->admin_m->role_id, $sale_groupids));
if($is_sale_role){
	echo form_hidden('edit[meta][staff_business]', $this->admin_m->id);
}
else{
	echo $this->admin_form->dropdown('Nhân viên kinh doanh','edit[meta][staff_business]',prepare_dropdown($staffs, 'Chọn nhân viên kinh doanh phụ trách'),$this->termmeta_m->get_meta_value($edit->term_id, 'staff_business'));
}

if(empty($is_system)){

	$grid_files = '<div id="grid_files">';
	$attachment_files = @unserialize($this->termmeta_m->get_meta_value($edit->term_id, '_attachment_files'));
	if(!empty($attachment_files) && is_array($attachment_files))
		foreach ($attachment_files as $file)
			$grid_files .= anchor(base_url().$file['file_path'],$file['file_name'],'id="attactment_file" title="Tải File ' . $file['file_name'] . '" target="_blank"') . '<br/>';
	$grid_files.= '</div>';

	echo $this->admin_form->upload('File hợp đồng','fileinput',$this->termmeta_m->get_meta_value($edit->term_id, '_tmp_file_upload'),$grid_files,array('id'=>'contract-inputfile','multiple'=>'multiple'));
}

echo $this->admin_form->textarea(
	'Ghi chú hợp đồng',
	'edit[meta][contract_note]',
	$this->termmeta_m->get_meta_value($edit->term_id, 'contract_note'));

echo $this->admin_form->input_numberic(
	'% VAT',
	'edit[meta][vat]',
	$this->termmeta_m->get_meta_value($edit->term_id, 'vat'),
	'',
	array('min'	=>	0));

echo $this->admin_form->input_numberic(
	'Giá trị hợp đồng',
	'edit[meta][contract_value]',
	$this->termmeta_m->get_meta_value($edit->term_id, 'contract_value'),
	'',
	array('min'	=>	0));

echo form_hidden('edit[term_status]', $edit->term_status);

if($edit->term_type == 'webgeneral'){
	$this->table->clear()
	->set_caption('Dịch vụ kèm theo');

	$services_pricetag = get_term_meta_value($edit->term_id,'services_pricetag');
	$services_pricetag = @unserialize($services_pricetag);

	$this->table
	->add_row(array('Thiết kế web',
			form_checkbox('edit[meta][services_pricetag][webdesign][is_design]','1', (bool) @$services_pricetag['webdesign']['is_design']),
			$lock_editable ? force_var(@$services_pricetag['webdesign']['price'],'miễn phí')  : form_input(array('name'=>'edit[meta][services_pricetag][webdesign][price]','placeholder'=>'Đơn giá'), @$services_pricetag['webdesign']['price']),
			$lock_editable ? force_var(@$services_pricetag['webdesign']['theme_id'],'chưa chọn') : form_input(array('name'=>'edit[meta][services_pricetag][webdesign][theme_id]','placeholder'=>'Mã THEME'),@$services_pricetag['webdesign']['theme_id'])))

	->add_row(array('Hosting',
			form_checkbox('edit[meta][services_pricetag][hosting][has_hosting]','1', (bool) @$services_pricetag['hosting']['has_hosting']),
			$lock_editable ? force_var(@$services_pricetag['hosting']['price'],'miễn phí') : form_input(array('name'=>'edit[meta][services_pricetag][hosting][price]','placeholder'=>'Đơn giá'),@$services_pricetag['hosting']['price'])))

	->add_row(array('Domain',
			form_checkbox('services_pricetag[domain][is_free]','1', (bool) @$services_pricetag['domain']['is_free']),
			$lock_editable ? force_var(@$services_pricetag['domain']['identity'],'Tên miền') : form_input(array('name'=>'services_pricetag[domain][identity]','placeholder'=>'Tên miền'),@$services_pricetag['domain']['identity'])))

	->add_row(array('Giảm giá',
			form_checkbox('edit[meta][services_pricetag][discount][is_discount]','1',(bool) @$services_pricetag['discount']['is_discount']),
			$lock_editable ? force_var(@$services_pricetag['discount']['percent'],'0%') : form_input(array('name'=>'edit[meta][services_pricetag][discount][percent]','placeholder'=>'0.0%'),@$services_pricetag['discount']['percent'])))

	->add_row(array(
			'Số lần thanh toán','',
			form_input(array(
				'name'=>'edit[meta][services_pricetag][number_of_payments]',
				'type'=>'number',
				'placeholder'=>'Số lần thanh toán'),
				@$services_pricetag['number_of_payments'])
		));

	echo
	$this->table->generate();
}

echo $this->admin_form->submit(
	'',
	'confirm_step_detail',
	'confirm_step_detail',
	'',
	array('style'=>'display:none','id'=>'confirm_step_detail'));

echo $this->admin_form->form_close();
?>
</div>
<script type="text/javascript">
$(function(){

	$("#contract-inputfile").fileinput({
	    uploadUrl: module_url + 'ajax_dipatcher/act_upload_file',
	    uploadAsync: true,
	    maxFileCount: 5,
	    showPreview: false,
    	uploadExtraData: {
	        term_id: "<?php echo $edit->term_id;?>",
	        term_name: "<?php echo $edit->term_name;?>",
	    },
	    allowedFileExtensions: ['docx', 'doc', 'xls', 'xlsx'],
	})
	.on('fileuploaded', function(event, data, previewId, index) {

		var response = data.response

	    if(response.response != 'undefined' 
	    	&& response.response.success == true){

			$.each(response.response.ret_obj,function(i, e){
				$("#grid_files").append('<a href="<?php echo base_url();?>' + e.file_path + '">' + e.file_name + '</a><br/>');
			});
	    }
	});
	$("#input_daterange").daterangepicker({
		format: 'DD-MM-YYYY',
	});
	$('.select2').select2();
	$('.select2').css('width','100%');
});
</script>
