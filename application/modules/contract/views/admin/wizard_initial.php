<div class="col-md-12">
<?php 

$edit_lock = in_array($edit->term_status, array('publish',1)) ? TRUE : FALSE;

echo $this->admin_form->form_open();	

$attrs = array('id'=>'servicedrp');

if($edit_lock) $attrs['disabled'] = 'disabled';

echo $this->admin_form->dropdown(	
	'Loại hợp đồng',
	'edit[term_type]',
	prepare_dropdown($this->config->item('group-services'),'Chọn loại hợp đồng'),
	$edit->term_type,
	'Chọn loại hợp đồng cần thực hiện.',
	$attrs);

echo form_hidden('edit[term_id]',$edit->term_id);

echo $this->admin_form->submit('','submit_chosen_type','submit_chosen_type','',array('style'=>'display:none;','id'=>'submit_chosen_type'));

echo $this->admin_form->form_close();

?>
</div>

<script type="text/javascript">

$(function(){

	$("#servicedrp").on("change", function(e) {

		var service_name = $('#servicedrp option:selected').text();
		service_name = service_name == 'None' ? 'Dịch vụ' : service_name;
		
		$("#rootwizard ul li a[href='#tab-service'] span.tab-service").text(service_name);

		toggleBootstrapWizard();
	});
	$('#servicedrp').css('width','100%');
});
</script>