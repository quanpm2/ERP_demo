<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/datatables/dataTables.bootstrap.css');
$this->template->stylesheet->add('plugins/datatables/extensions/FixedHeader/css/dataTables.fixedHeader.css');
$this->template->stylesheet->add('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css');
$this->template->stylesheet->add('plugins/datatables/extensions/Scroller/css/dataTables.scroller.css');
$this->template->javascript->add('plugins/datatables/jquery.dataTables.min.js');
$this->template->javascript->add('plugins/datatables/dataTables.bootstrap.min.js');
$this->template->javascript->add('plugins/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js');
$this->template->javascript->add('plugins/datatables/extensions/KeyTable/js/dataTables.keyTable.min.js');
$this->template->javascript->add('plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js');
$this->template->javascript->add('plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js');
?>

<info-box-component v-bind:datasource="totalSummary" color="aqua" heading="Tổng dự thu (đã cấp số và đang chạy)"></info-box-component>
<info-box-component v-bind:datasource="waitingforapproveSummary" color="yellow" heading="Dự thu hợp đồng mới (chờ duyệt)"></info-box-component>
<info-box-component v-bind:datasource="bailmentSummary" color="green" heading="Dự thu HĐ đang chạy (đang bảo lãnh)"></info-box-component>
<info-box-component v-bind:datasource="bailmentExpriedSummary" color="red" heading="Dự thu HĐ đang chạy (quá hạn bảo lãnh)"></info-box-component>

<revenue-statistic-container-component v-on:update_summary_boxes="updateSummaryBoxes"></revenue-statistic-container-component>

<script type="text/javascript">
var baseURL = "<?php echo admin_url('');?>";
var jsobjectdata = <?php echo $jsobjectdata;?>;
</script>

<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/revenue-component.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/usergroups-revenue-component.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/users-revenue-component.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/terms-datatable-component.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/app.js'));