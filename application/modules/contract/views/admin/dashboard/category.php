<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="col-md-12">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-fw fa-puzzle-piece"></i> Trend ngành nghề HĐ</a></li>
            <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-fw fa-line-chart"></i> Số liệu tổng quang ngành nghề</a></li>
            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">

        <!-- OPEN TAB CONTENT 1 -->
        <div class="tab-pane active" id="tab_1">
            <b>Chú thích :</b>

            <p>
                <b>Biểu đồ 1 (bên trái) :</b> Chi tiết về số lượng hợp đồng , Tổng giá trị hợp đồng và số tiền đã thu của mỗi ngành nghề trong năm<br/>
                <b>Biểu đồ 2 (bên phải) :</b> Đại biểu cho tỷ lệ % số lượng hợp đồng trên mỗi ngành nghề trong tổng hợp đồng đã thực hiện trong năm 
            </p>
            <?php echo $this->template->tabpane2->view('parts/charts/categories_contract_detail');?>
        </div>
        <!-- /.tab-pane -->


        <!-- OPEN TAB CONTENT 2 -->
        <div class="tab-pane" id="tab_2">
            <b>Chú thích :</b>

            <p>
                <b>Biểu đồ 1 (bên trên) :</b> Xu hướng tổng giá trị hợp đồng mỗi ngành nghề trên mỗi tháng trong năm<br/>
                <b>Biểu đồ 2 (bên dưới) :</b> Xu hướng Số lượng hợp đồng mỗi ngành nghề trên mỗi tháng trong năm 
            </p>
            <?php echo $this->template->tabpane1->view('parts/charts/categories_contract_trends');?>
        </div>
        <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
</div>
<!-- /.col -->