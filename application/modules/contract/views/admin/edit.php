<style>.content .box-body .col-md-6 > table > tbody > tr > td:first-child {width: 40%;}</style>
<div id="app_root">
<?php 
$version = 202104221802;
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->javascript->add('plugins/wizard/jquery.bootstrap.wizard.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');

$this->template->javascript->add(admin_theme_url("modules/component/form.js?v={$version}"));
$this->template->javascript->add(admin_theme_url("modules/contract/js/select-staff-business.js?v={$version}"));

$this->template->javascript->add('plugins/validate/jquery.validate.js');
$this->template->stylesheet->add('plugins/bootstrap-fileinput/css/fileinput.css');
$this->template->javascript->add('plugins/bootstrap-fileinput/js/fileinput.min.js');

$this->template->javascript->add(admin_theme_url("modules/contract/js/list-invoices-component.js?v={$version}"));
$this->template->javascript->add(admin_theme_url("modules/contract/js/edit-contract-component.js?v={$version}"));

/* SECTION : Common actions for contract */
echo $this->template->common_actions->view('parts/common_actions');

/* SECTION : Contact setting*/
$lock_editable 	= get_term_meta_value($edit->term_id,'lock_editable');
$edit_view 		= (empty($lock_editable) && $has_edit_permission) ? 'unlock_edit' : 'lock_edit';

echo $this->template->edit->view('unlock_edit');
?>
<?php
/* SECTION : SETTING DETAIL ADDTION SERVICE */
$addition_tpl = 'default';

switch ($edit->term_type)
{
	case 'webdesign': 	$addition_tpl = 'webdesign'; break;
	case 'oneweb': 		$addition_tpl = 'oneweb'; break;
	case 'pushdy': 		$addition_tpl = 'pushdy'; break;
	case 'onead': 		$addition_tpl = 'onead'; break;
	case 'hosting': 	$addition_tpl = 'hosting'; break;
	case 'domain': 		$addition_tpl = 'domain'; break;
	case 'banner':		$addition_tpl = 'banner'; break;
	case 'courseads':	$addition_tpl = 'courseads'; break;					
	case 'facebook-ads': 	$addition_tpl = 'facebook-ads'; break;
	case 'tiktok-ads': 	$addition_tpl = 'tiktok-ads'; break;
	case 'zalo-ads': 	$addition_tpl = 'zalo-ads'; break;
	case 'google-ads': 	$addition_tpl = 'google-ads'; break;
	case 'weboptimize': 	$addition_tpl = 'weboptimize'; break;
	case 'gws': 	$addition_tpl = 'gws'; break;
}

echo $this->template->addition_service->view("parts/addition_service/{$addition_tpl}");

/* SECTION : CRUD INVOICES */
echo $this->template->invoices->view('invoices/list_inv');
?>
</div>
<script type="text/javascript"> var app_root = new Vue({ 
	mounted : function(){
		
		var original = $.fn.editableutils.setCursorPosition;
		$.fn.editableutils.setCursorPosition = function() {
			try { original.apply(this, Array.prototype.slice.call(arguments)); } catch (e) { /* noop */ }
		};

		$( ".myeditable" ).each(function( index ) {
			$(this).editable({
				url : (function(base_url, el) {

					if(el.data("ajax_call_url") != undefined){

						base_url += el.data("ajax_call_url");
					}
					else base_url += "contract/ajax_dipatcher/ajax_edit/<?php echo $edit->term_id;?>";
					
				    return base_url; 

				})(admin_url, $(this)),
				params: function(params) {
					params.type = $(this).data("type-data");
					return params;
				},
				success: function(data, newValue) {
					if($.isEmptyObject(data.response)) return;
					if($.isEmptyObject(data.response.response)) return;
					if(data.response.response.hasOwnProperty("jscallback")){
						$.each(data.response.response.jscallback,function(i,e){
							var fn = window[e.function_to_call];
							fn(e.data);
						});
					}
				},
				defaultValue: "",
			});	  
		});

	},
	el: '#app_root',
}); </script>