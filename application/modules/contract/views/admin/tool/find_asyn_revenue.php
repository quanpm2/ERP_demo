<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

echo $this->template->trigger_javascript('https://www.gstatic.com/charts/loader.js');
?>
<div class="row">

    <div class="col-xs-12 col-md-8">
        <div id="contract-overral-diff-chart"></div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div id="overral-diff-chart"></div>
    </div>
    
    <?php

    echo $this->admin_form->set_col(12,0)->box_open('Danh sách tất cả hợp đồng không đồng bộ');
    echo $content ;
    echo $this->admin_form->box_close();

    ?>

</div>

<script type="text/javascript">

function drawContractOveralDiffChart()
{
    var data = google.visualization.arrayToDataTable(<?php echo $contract_chart_data?:'[]';?>);

    var options = {
            title: 'Dự thu đồng bộ và không đồng bộ',
            chartArea: {width: '50%'},
            annotations: {
            alwaysOutside: true,
            textStyle: {
                    fontSize: 12,
                    auraColor: 'none',
                    color: '#555'
                },
            boxStyle: {
                    stroke: '#ccc',
                    strokeWidth: 1,
                    gradient: {
                    color1: '#f3e5f5',
                    color2: '#f3e5f5',
                    x1: '0%', y1: '0%',
                    x2: '100%', y2: '100%'
                }
            }
            },
            hAxis: {
                    title: 'Số lượng hợp đồng',
                    minValue: 0,
                },
            vAxis: {
                    title: 'Dịch vụ'
                }
        };
        
    var chart = new google.visualization.BarChart(document.getElementById('contract-overral-diff-chart'));
    chart.draw(data, options);
}

function drawOveralDiffChart()
{
    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');
    data.addRows(<?php echo $overall_chart_data ?: '[]';?>);

    // Set chart options
    var options = {'title':'Phân bổ dữ liệu giữa dự thu và giá trị hợp đồng'};

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.PieChart(document.getElementById('overral-diff-chart'));
    chart.draw(data, options);
}

$(function(){

    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawContractOveralDiffChart);

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawOveralDiffChart);


})
</script>