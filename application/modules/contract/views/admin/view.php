<?php 

if(empty($content)) return;

if(is_array($content)) 
{
	echo implode('', $content);
	return;
}

if(is_string($content))
{
	echo $content;
}
?>
