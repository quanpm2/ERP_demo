<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-md-12">
    <div id="categories-contracts-values" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    <div id="categories-contracts-quantity" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

<script type="text/javascript">

// Draw chart trend contract's value categories per month in year
function drawCategoriesValuesByMonth()
{
    $.ajax({url:'<?php echo module_url('ajax/dataset/categoriesValues');?>',type:'post',dataType:'json'})
    .done(function(response) {
        
        Highcharts.chart('categories-contracts-values', {
            chart: {
                type: 'spline'
            },
            title: {
                text: response.data.chart.title
            },
            xAxis: { 
                categories: response.data.chart.categories ,
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Giá trị hợp đồng'
                }
            },
            tooltip: {
                shared: true
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series : response.data.chart.series
        });
    });
}
drawCategoriesValuesByMonth();

// Draw chart trend contract's quantity categories per month in year
function drawCategoriesQuantityByMonth()
{
    $.ajax({url:'<?php echo module_url('ajax/dataset/categoriesQuantity');?>',type:'post',dataType:'json'})
    .done(function(response) {
        
        Highcharts.chart('categories-contracts-quantity', {
            chart: {
                type: 'spline'
            },
            title: {
                text: response.data.chart.title
            },
            xAxis: { 
                categories: response.data.chart.categories ,
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Số lượng hợp đồng thực hiện'
                }
            },
            tooltip: {
                shared: true
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series : response.data.chart.series
        });
    });
}
drawCategoriesQuantityByMonth();
</script>