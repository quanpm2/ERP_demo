<?php defined('BASEPATH') OR exit('No direct script access allowed');

echo '<div class="col-md-12 form-group row">';
echo form_open();
echo anchor(module_url(),'<span class="fa fa-reply"></span> Trở về danh sách HĐ </a>','class="btn btn-success" style="margin-right:5px"');

if($has_verification_permission)
{
	echo form_button(['name'=>'verify_contract','type'=>'submit','class'=>'btn btn-warning','data-toggle'=>'confirmation'],'<i class="fa fa-fw fa-check-circle"></i>Cấp số HĐ');
}

if($has_start_contract_permission)
{
	echo form_button(['name'=>'start_service','type'=>'submit','class'=>'btn btn-warning','data-toggle'=>'confirmation'],'<i class="fa fa-fw fa-play"></i>Chạy dịch vụ');
}

if($has_stop_contract_permission)
{
	echo form_button(array('name'=>'stop_contract','type'=>'submit','class'=>'btn btn-danger','data-toggle'=>'confirmation'),
		'<span class="fa fa-fw fa-stop"></span>Kết thúc HĐ');
}

if($has_renew_contract_permission)
{
	$attrs = array(
		'id'	=>	'show_btn_renew',
		'name'	=>	'show_btn_renew',
		'type'	=>	'button',
		'class'	=>	'btn btn bg-orange',
		'value'			=>	'show_btn_renew',
		'data-toggle'	=>	'modal',
		'data-target'	=>	'#show_modal_renew',
		'data-toggle'	=>	'confirmation'
	);
	echo form_button($attrs, '<i class="fa fa-files-o" aria-hidden="true"></i> Gia hạn');
}

if($has_lock_permission)
{
	$is_editable = get_term_meta_value($edit->term_id,'lock_editable');
	$icon_lock_html = !empty($is_editable)  ? '<i class="fa fa-fw fa-unlock"></i>Mở khóa' : '<i class="fa fa-fw fa-lock"></i>Khóa hợp đồng';
	echo form_button(['name'=>'lock_editable','type'=>'submit','class'=>'btn btn-default'],$icon_lock_html);
}

if($has_destroy_permission)
{
	echo form_button(array('name'=>'remove_contract','type'=>'submit','class'=>'btn bg-orange','data-toggle'=>'confirmation'),'<span class="fa fa-fw fa-recycle"></span>Hủy bỏ HĐ');
}

if( $has_manipulation_lock_permission
    && (bool) $manipulation_locked['is_manipulation_locked'])
{
	$is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                              && FALSE == (bool)get_term_meta_value($edit->term_id, 'is_manipulation_locked');
    if($is_manipulation_locked)
    {
        $icon_manipulation_lock_html = '<i class="fa fa-eye"></i> Mở khóa thao tác';
        $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
        $manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
        echo form_button(['name' => 'manipulation_locked', 'type' => 'submit', 'class' => 'btn btn-info', 'data-toggle' => 'confirmation'], $icon_manipulation_lock_html);
    }
    else 
    {
        $icon_manipulation_lock_html = '<i class="fa fa-eye-slash"></i> Khóa thao tác</button>';
        echo form_button(['name' => 'manipulation_locked', 'type' => 'submit', 'class' => 'btn btn-info', 'data-toggle' => 'confirmation'], $icon_manipulation_lock_html);
    }
}


echo anchor(module_url("preview/{$edit->term_id}"), '<div class="col-md-3 col-sm-4"><i class="fa fa-fw fa-print"></i> In Hợp đồng</div>', 'class="btn btn-success pull-right" style="margin-right:10px;" target="_blank"');
 
echo form_close();
echo '</div>';
?>

<script type="text/javascript">
	$(function(){
		$('[data-toggle=confirmation]').confirmation();
		$(".input-mask").inputmask();
		$('.set-datepicker').datepicker({
			format: 'dd-mm-yyyy',
			todayHighlight: true,
			autoclose: true,
		});
		$("#updatefrm").submit(function(){
			var form = $(this);
			$.each(form.find($("input[type='text'],textarea")), function( index, value ) {
				if($(value).prop("defaultValue") == $(value).val())
				$(value).remove();
			});
			$.each(form.find($("select")), function( index, value) {
				if(value.options[value.selectedIndex].defaultSelected)	
					$(value).remove();
			});
			$.each(form.find($("input[type='checkbox'],input[type='radio']")), function( index, value ) {
				if($(value).prop("defaultChecked") == $(value).prop("checked"))	
					$(value).remove();
			});
		});
	});
</script>