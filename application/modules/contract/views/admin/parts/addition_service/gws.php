<?php
$this->template->javascript->add(base_url("dist/vGwsContractConfiguration.js"));
?>
<div style="clear:both;"></div>

<div class="row">
    <div class="container-fluid">
        <div class="col-md-12">
            <div id="vContractConfiguration">
                <v-contract-configuration is_edit="<?= true ?>" <?= $is_lock_editable ? 'is_lock_editable="true"' : ''?> term_id="<?= $edit->term_id; ?>"></v-contract-configuration>
            </div>
        </div>
    </div>
</div>