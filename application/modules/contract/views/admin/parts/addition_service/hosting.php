<div style="clear:both;"></div>
<div class="col-md-12">
    <?php

    echo $this->admin_form->form_open('', ['id' => 'form-hosting']);

    $buttons = '';
    $contract_end = get_term_meta_value($edit->term_id,'contract_end');
    $is_editable_locked = (bool) get_term_meta_value($edit->term_id,'lock_editable');
    if( ! $is_editable_locked)
    {
        $buttons.= form_button(['name'=>'addition_service_submit','type'=>'submit','class'=>'btn btn-primary pull-right'],'<i class="fa fa-fw fa-save"></i>Lưu thay đổi');
    }

    $this->table->set_caption('Chi tiết dịch vụ'.$buttons);

    $this->config->load('hosting/hosting');
    $packages = $this->config->item('service', 'packages');
    $headings = [''];
    $spackage = get_term_meta_value($edit->term_id, 'hosting_service_package');

    $packages = array_reduce($packages, function($result, $item) use ($spackage, $edit){
        if('active' != $item['status']) return $result;

        if($spackage == $item['name'])
        {
            $_package = get_term_meta_value($edit->term_id, 'service_package');
            if(empty($_package) || !is_serialized($_package)) return FALSE;
    
            $_package = unserialize($_package);
            $result[$_package['name']] = $_package;
            
            return $result;
        }

        $result[$item['name']] = $item;
        return $result;
    }, []);

    foreach ($packages as $key => $value) 
    {
        $headings[] = array(
            
            'data' => '<label>'.form_radio('hosting_service_package', $value['name'],  $spackage == $value['name'], ['class'=>'minimal']).br(2).$value['label'].($value['status']!='active'?'<br><small><i>(Đã hủy bỏ)</i></small>':'').'</label>',

            'class' => 'text-center',
            'style' => 'vertical-align: top;'
            );
    }
    
    $this->table->set_heading($headings);    
    $package_config = $this->config->item('package_config');
    unset($package_config['month']);
    foreach ($package_config as $key => $field)
    {
        $text = is_array($field) ? $field['text'] : $field;
        $_row = array($text);
        foreach ($packages as $p)
        {
            if('hosting_custom' == $p['name'])
            {
                if(array_key_exists($key, $p) && $_config = $this->config->item($key, 'package_config'))
                {
                    $_input = '';
                    $value = $p[$key];
                    switch ($_config['type'])
                    {
                        case 'number':
                            $_input = form_input(['name'=>"meta[service_package][{$p['name']}][{$key}]",'type'=>'number'], $value, ['class'=>'form-control','placeholder'=>'Tùy chỉnh']);
                            break;

                        case 'bool':
                            $_input = form_dropdown("meta[service_package][{$p['name']}][{$key}]",array(FALSE => 'Không',TRUE => 'Có'), $value, ['class'=>'form-control','placeholder'=>'Tùy chỉnh']);
                            break;

                        case 'enum':
                            $_input = form_dropdown("meta[service_package][{$p['name']}][{$key}]", ($_config['list']??[]), $value, ['class'=>'form-control', 'style'=>'text-align:center','placeholder'=>'Số năm đăng ký']);
                            break;

                        default:
                            $_input = form_input("meta[service_package][{$p['name']}][{$key}]", $value, ['class'=>'form-control','placeholder'=>'Tùy chỉnh']);	
                            break;
                    }

                    $_row[] = array('data' => $_input, 'class' => 'text-center');
                    continue;
                }

                if('price_year' == $key) $p[$key] = $p['price']*12;

                if( ! isset($p[$key])) $p[$key] = '--';

                if( ! empty($field['divide_by'])) $p[$key]/=$field['divide_by'];


                if(is_bool($p[$key])) $p[$key] = $p[$key] ? '<i class="fa fa-fw fa-check text-green"></i>' : '--';
                else if(is_numeric($p[$key])) $p[$key] = currency_numberformat($p[$key], ($field['unit']??''));

                $_row[] = array('data' => $p[$key], 'class' => 'text-center');
                
                continue;
            }

            if('price_year' == $key) $p[$key] = $p['price']*12;

            if( ! empty($field['divide_by'])) $p[$key]/=$field['divide_by'];
            
            if(is_bool($p[$key])) $p[$key] = $p[$key] ? '<i class="fa fa-fw fa-check text-green"></i>' : '--';
            else if(is_numeric($p[$key])) $p[$key] = currency_numberformat($p[$key], ($field['unit']??''));

            $_row[] = array('data' => $p[$key], 'class' => 'text-center');
        }

        $this->table->add_row($_row);
    }

    echo $this->table->generate();
    ?>

    <div class="modal fade" id="show_modal_renew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Xác nhận gia hạn hợp đồng hosting</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <?php
                        $attrs_hosting_contract_end = array(
                            'addon_begin'=>'<i class="fa fa-calendar"></i>',
                            'class'=>'input-mask set-datepicker',
                            'data-inputmask'=>"'alias': 'dd-mm-yyyy'",
                            'data-mask'=>""
                            );
                        $end_time = my_date(get_term_meta_value($edit->term_id, 'contract_end'), 'd/m/Y') ?: my_date(0, 'd/m/Y');
                        echo $this->admin_form->input('Thời gian kết thúc hosting','hosting_contract_end',$end_time,'',$attrs_hosting_contract_end);
                        ?>  
                    </div>
                    <div class="row">
                        <?php 	
                        $attrs_hrosting_months = array(
                            'addon_begin'=>'<i class="fa fa-calendar"></i>',
                            'type' => 'number'
                            );
                        $hosting_months = get_term_meta_value($edit->term_id, 'hosting_months') ?: 12;
                        echo $this->admin_form->input('Số tháng gia hạn','hosting_months',$hosting_months,'',$attrs_hosting_months); 
                        ?>
                    </div>  
                    <p>Sau khi dịch vụ được kết thúc , hệ thống sẽ tự động gửi mail kết thúc kèm file chứa dữ liệu từ khi thực hiện đến thời gian kết thúc dịch cho khách hàng và các bên liên quan đến dịch vụ này.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
                    <button type="submit" name="btn_renew" class="btn btn-primary">Xác nhận</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("button[name|='btn_renew']").on('click', function() {
            $(this).hide();
            $('#show_modal_renew .modal-footer').append('<button type="button" class="btn btn-info">Đang xử lý...</button>');
        });
    </script>

    <?php
    echo $this->admin_form->form_close();
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass   : 'iradio_minimal-blue'
        });

        $('.set-datepicker').datepicker({
            format: 'yyyy/mm/dd',
            todayHighlight: true,
            autoclose: true,	
        });

        let validator = $("#form-hosting").validate({
            rules: {
                'meta[service_package][hosting_custom][price]': {
                    required: true,
                },
                'meta[service_package][hosting_custom][disk]': {
                    required: true,
                },
                'meta[service_package][hosting_custom][bandwidth]': {
                    required: true,
                },
            },
            messages: {
                'meta[service_package][hosting_custom][price]': {
                    required: 'Giá không được để trống',
                    digits  : 'Kiểu dữ liệu không hợp lệ , chi phí tháng phải là kiểu số',
                },
                'meta[service_package][hosting_custom][disk]': {
                    required: 'Dung lượng không được để trống',
                    digits: 'Kiểu dữ liệu không hợp lệ , dung lượng phải là kiểu số',
                },
                'meta[service_package][hosting_custom][bandwidth]': {
                    required: 'Băng thông không được để trống',
                    digits: 'Kiểu dữ liệu không hợp lệ , băng thông phải là kiểu số',
                },
            },
        });
    });

    $(function(){
        $("#form-hosting").submit(function(e){
            e.preventDefault();
            
            const HOSTING_SERVICE_PACKAGE = $(this).find('input[name="hosting_service_package"][type="radio"]:checked').val();
            if('hosting_custom' == HOSTING_SERVICE_PACKAGE){
                let is_valid = $("#form-hosting").valid();
                if(!is_valid) return;

                let __package = <?= json_encode($this->config->item('service', 'packages')); ?>;
                __package = _.get(__package, 'hosting_custom');

                let package_config = <?= json_encode($this->config->item('package_config')); ?>;
                _.forEach(package_config, (config, key) => {
                    let type = _.get(config, 'type');
                    let value = null;
                    
                    if(['bool', 'enum'].includes(type)) value = $(`select[name="meta[service_package][hosting_custom][${key}]"]`).val();
                    else  value = $(`input[name="meta[service_package][hosting_custom][${key}]"]`).val();

                    _.set(__package, key, value);
                })

                let month = <?= (int) get_term_meta_value($edit->term_id, 'hosting_months') ?: 12 ?>;
                _.set(__package, 'month', month);

                let promise = new Promise((resolve, reject) => {
                    let service_package_data = {
                        pk: <?php echo $edit->term_id;?>,
                        type: 'meta',
                        name: 'service_package',
                        value: __package
                    }
                    $.post(admin_url + 'contract/api/contract/update', service_package_data)
                    .done(function(response) {
                        let messages = response.msg;
                        for(let i in messages)
                        {
                            $.notify(messages[i], 'success');
                        }

                        resolve(true);
                    })
                    .fail(function(jqXHR, textStatus, errorThrown) {
                        $.notify(JSON.parse(jqXHR.responseText));
                        reject();
                    });
                });

                promise
                .then((result) => {
                    let params = {
                        pk : <?php echo $edit->term_id;?>,
                        type : 'meta',
                        name : 'hosting_service_package',
                        value : HOSTING_SERVICE_PACKAGE,
                    };
                    $.post(admin_url + 'contract/api/contract/update', params)
                    .done(function(response) {
                        let messages = response.msg;
                        for(let i in messages)
                        {
                            $.notify(messages[i], 'success');
                        }
                        setTimeout( () => { location.reload(true); }, 2000);
                    })
                    .fail(function(jqXHR, textStatus, errorThrown) {
                        $.notify(JSON.parse(jqXHR.responseText));
                    });
                })
                .catch((err) => {})

                return;
            }

            var params = {
                pk : <?php echo $edit->term_id;?>,
                type : 'meta',
                name : 'hosting_service_package',
                value : HOSTING_SERVICE_PACKAGE,
            };
            $.post(admin_url + 'contract/api/contract/update', params)
            .done(function(response) {
                let messages = response.msg;
                for(let i in messages)
                {
                    $.notify(messages[i], 'success');
                }
                setTimeout( () => { location.reload(true); }, 2000);
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                $.notify(JSON.parse(jqXHR.responseText));
            });
        });
    })
</script>
<!-- Modal canh bao gia han -->