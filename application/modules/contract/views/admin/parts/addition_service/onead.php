<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$this->template->javascript->add(admin_theme_url('modules/onead/onead-service-price-component.js'));
?>
<div style="clear:both;"></div>
<div class="col-md-12" id="package_detail">
	<onead-service-price-component :id="<?php echo $edit->term_id;?>"></onead-service-price-component>
</div>