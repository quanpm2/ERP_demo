<?php 
/*
 *---------------------------------------------------------------------------------------
 * THÔNG TIN DOMAIN
 *---------------------------------------------------------------------------------------
 *
 * 1. Load file config : domain/config/domain.php
 * 2. Hiển thị thông tin các gói domain
 */
  $this->config->load('domain/domain') ;

  // THÔNG TIN CÁC GÓI DOMAIN
  $services = $this->config->item('service', 'packages') ;

  // CHỌN SỐ NĂM ĐĂNG KÝ DOMAIN
  $number_year_registers = $this->config->item('number_year_register') ;

?>
<div style="clear:both;"></div>
<div class="col-md-12">
    <?php

    $contract_end = get_term_meta_value($edit->term_id,'contract_end');
    $is_editable_locked = (bool) get_term_meta_value($edit->term_id,'lock_editable');

    /*
     *---------------------------------------------------------------------------------------
     * BUTTON LƯU THAY ĐỔI
     *---------------------------------------------------------------------------------------
     *
     * 1. Khi hợp đồng được mở : hiện
     * 2. Khi hợp đồng được khóad : ẩn
     */
        $buttons = '';
        if( ! $is_editable_locked)
        {
          $buttons .= form_button(['name'=>'addition_service_submit','type'=>'submit','class'=>'btn btn-primary pull-right'],'<i class="fa fa-fw fa-save"></i>Lưu thay đổi');
        }

    /*
     *---------------------------------------------------------------------------------------
     * FORM GIA HẠN HỢP ĐỒNG DOMAIN
     *---------------------------------------------------------------------------------------
     *
     * 1. CẬP NHẬT THỜI GIAN KẾT THÚC HĐ
     * 2. CHỌN SỐ NĂM GIA HẠN
     */

      echo $this->admin_form->form_open('', ['id' => 'form-domain']);

    ?>

    <!-- MODAL : GIA HẠN HỢP ĐỒNG DOMAIN -->
    <div class="modal fade" id="show_modal_renew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Xác nhận gia hạn hợp đồng domain</h4>
          </div>
          <div class="modal-body">
            <div class="row">
                <?php
                  $attrs_domain_contract_end = array(
                    'addon_begin'=>'<i class="fa fa-calendar"></i>',
                    'class'=>'input-mask set-datepicker',
                    'data-inputmask'=>"'alias': 'dd-mm-yyyy'",
                    'data-mask'=>""
                    );
                  $end_time = $this->mdate->date('d/m/Y', get_term_meta_value($edit->term_id, 'contract_end')) ?: $this->mdate->date('d/m/Y', 0);
                  echo $this->admin_form->input('Thời gian kết thúc domain','domain_contract_end',$end_time,'',$attrs_domain_contract_end);
                ?>  
            </div>
            <div class="row">
                <?php
                  $attrs_domain_number_year_register = array(
                    'addon_begin'=>'<i class="fa fa-calendar"></i>',
                    'class'=>'input-mask set-datepicker',
                    'data-inputmask'=>"'alias': 'dd-mm-yyyy'",
                    'data-mask'=> ""
                    );
                  echo $this->admin_form->dropdown('Số năm gia hạn','domain_number_year_register',$number_year_registers, 1);
                ?>  
            </div> 
            <p>Sau khi dịch vụ được kết thúc , hệ thống sẽ tự động gửi mail kết thúc kèm file chứa dữ liệu từ khi thực hiện đến thời gian kết thúc dịch cho khách hàng và các bên liên quan đến dịch vụ này.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
            <button type="submit" name="btn_renew" class="btn btn-primary">Xác nhận</button>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $("button[name|='btn_renew']").on('click', function() {
                $(this).hide();
                $('#show_modal_renew .modal-footer').append('<button type="button" class="btn btn-info">Đang xử lý...</button>');
            });

            $('.set-datepicker').datepicker({
                format: 'yyyy/mm/dd',
                todayHighlight: true,
                autoclose: true,  
            });
        });
    </script>

    <?php echo $this->admin_form->form_close(); ?>
    </div>
