<div style="clear:both;"></div>
<div class="col-md-12">
<?php

$buttons = '';
$contract_end = get_term_meta_value($edit->term_id,'contract_end');
$is_editable_locked = (bool) get_term_meta_value($edit->term_id,'lock_editable');
if( ! $is_editable_locked)
{
	$buttons.= form_button(['name'=>'addition_service_submit','type'=>'submit','class'=>'btn btn-primary pull-right'],'<i class="fa fa-fw fa-save"></i>Lưu thay đổi');
}

echo $this->admin_form->form_open('', ['id' => 'form-hosting']);

$this->table->set_caption('Chi tiết dịch vụ'.$buttons);



$this->table->set_heading('Dịch vụ', 'Gói đang sử dụng' , 'Ngày bắt đầu', 'Ngày kết thúc' , 'Giá trị');

$hosting_price 	   = (get_term_meta_value($edit->term_id, 'hosting_price') == '') ? 0 : get_term_meta_value($edit->term_id, 'hosting_price') ;
$hosting_months    = (get_term_meta_value($edit->term_id, 'hosting_months') == '') ? 12 : get_term_meta_value($edit->term_id, 'hosting_months') ;
//echo 'hosting_months : '  . var_dump($hosting_months) ;
$hosting_cost 	   = currency_numberformat($hosting_price * $hosting_months, 'đ');
$contract_begin    = get_term_meta_value($edit->term_id, 'contract_begin');
$contract_end      = get_term_meta_value($edit->term_id, 'contract_end');

$hosting_input 	   = "{$hosting_price}đ X {$hosting_months} tháng = {$hosting_cost} đ";
if( ! $is_editable_locked)
{
	$price_input    = form_input(['type'=>'number', 'name'=>'meta[hosting_price]','placeholder'=>'Đơn giá', 'id' => 'hosting_price_input'], $hosting_price ?? '');
	$months_input   = form_input(['type'=>'number', 'name'=>'meta[hosting_months]','placeholder'=>'Số tháng'],$hosting_months);
	$hosting_input  = "{$price_input}đ X {$months_input} tháng = {$hosting_cost}";
}

$hosting_dpicker 	= my_date($contract_begin);

//$this->table->add_row('Hosting',$hosting_cbx,$hosting_dpicker,$hosting_input);
$this->table->add_row('Hosting', 'Hosting ' . get_term_meta_value($edit->term_id, 'hosting_disk') . 'MB' , $hosting_dpicker, my_date($contract_end), $hosting_input);

$hosting_discount_percent 	  				  = get_term_meta_value($edit->term_id, 'hosting_discount_percent') ?? 0;
$hosting_discount_input 	  				  = $hosting_discount_percent;
if( ! $is_editable_locked)
{
	$hosting_discount_input   				  = form_input(['type'=>'number','step'=>'0.01', 'name'=>'meta[hosting_discount_percent]','placeholder'=>'0.0'], $hosting_discount_percent);
}
$this->table->add_row('Giảm giá hosting','', '', '', $hosting_discount_input.' %');

echo $this->table->generate();

echo $this->admin_form->form_close();
?>
</div>
<script type="text/javascript">
$(function(){
	$('.set-datepicker').datepicker({
		format: 'yyyy/mm/dd',
		todayHighlight: true,
		autoclose: true,	
	});
})
</script>