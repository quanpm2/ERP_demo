<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$this->template->javascript->add(base_url("dist/vCourseadsConfigurationBox.js"));
?>
<div style="clear:both;"></div>
<div class="col-md-12">
<?php
$buttons 			= '';
$is_editable_locked = (bool) get_term_meta_value($edit->term_id,'lock_editable');
if( ! $is_editable_locked)
{
	$_attrs = [
		'name'=>'addition_service_submit',
		'type'=>'submit',
		'class'=>'btn btn-primary pull-right'
	];

	$buttons = form_button($_attrs, '<i class="fa fa-fw fa-save"></i>Lưu thay đổi');
}

echo $this->admin_form->form_open();

$this->table->set_caption('Thông tin dịch vụ'.$buttons);
$this->table->add_row(['data'=>'','colspan'=>4]);
echo $this->table->generate();

echo form_hidden('edit[term_id]', $edit->term_id);
?>
<div id="app-process-course-setting-container">
	<v-courseads-setting term_id="<?php echo $edit->term_id;?>"></v-courseads-setting>
</div>
<?php echo $this->admin_form->form_close(); ?>
</div>