<?php
$this->template->javascript->add(base_url("dist/vFacebookadsConfigurationContractEditPageBox.js?v=202105251728"));
$this->template->javascript->add(base_url("dist/vFacebookadsConfigurationContractCuratorsEditPageBox.js?v=202105251728"));
$this->template->javascript->add(base_url("dist/vFacebookadsConfigurationContractServiceFeePlanBox.js?v=202202241648"));
?>
<div style="clear:both;"></div>

<?php if('range' == get_term_meta_value($edit->term_id, 'service_fee_payment_type')): ?>
<div class="row">
    <div class="container-fluid" >
        <div class="col-md-12">
            <div class="box box-theme">
                <div class="box-header with-border">
                    <h3 class="box-title">Mức % phí dịch vụ</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                	<div id="vFacebookadsConfigurationContractServiceFeePlanBox">
					    <v-facebookads-configuration-contract-service-fee-plan-box :term_id="<?php echo $edit->term_id;?>" 
                            <?php echo $is_lock_editable ? '' : ':can-submit="1"'; ?>
                            >
					    </v-facebookads-configuration-contract-service-fee-plan-box>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif;?>

<div class="row">
    <div class="container-fluid" >
        <div class="col-md-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Chương trình khuyến mãi / giảm giá</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div id="vFacebookadsConfigurationContractPromotionsBox">
                        <v-facebookads-configuration-contract-promotions-box
                            :term_id="<?php echo $edit->term_id;?>" 
                            <?php echo $is_lock_editable ? '' : ':can-submit="1"'; ?>
                            >
                        </v-facebookads-configuration-contract-promotions-box>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="container-fluid" >
        <div class="col-md-12">
                <div class="box box-info box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin người nhận báo cáo</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div id="vFacebookadsConfigurationContractCuratorsBox">
                        <v-facebookads-configuration-contract-curators-box :term_id="<?php echo $edit->term_id;?>" 
                            <?php echo $is_lock_editable ? '' : ':can-submit="1"'; ?>
                            >
                        </v-facebookads-configuration-contract-curators-box>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>