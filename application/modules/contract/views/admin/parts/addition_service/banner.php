<div style="clear:both;"></div>
<div class="col-md-12">
<?php

$buttons = '';
$contract_end = get_term_meta_value($edit->term_id,'contract_end');
$is_editable_locked = (bool) get_term_meta_value($edit->term_id,'lock_editable');
if( ! $is_editable_locked)
{
	$buttons.= form_button(['name'=>'addition_service_submit','type'=>'submit','class'=>'btn btn-primary pull-right'],'<i class="fa fa-fw fa-save"></i>Lưu thay đổi');
}

echo $this->admin_form->form_open('', ['id' => 'service-config-form']);

$this->table->set_caption('Chi tiết dịch vụ'.$buttons);

function total_price($price='', $quanties='',$sale='') {
		if($price == null) return false;
		if($quanties == null) return false;

		if($sale!='' && $quanties>= $sale['min']){
			$total = ($price * $quanties)-((($price * $quanties)*$sale['value'])/100);
			$total = $total/1000;
			$total = round($total);
			return $total*1000;
		}
		return ($price * $quanties);
};	


?>
<div class="col-md-12" id="service_tab">
<?php 

$this->config->load('banner/banner');
$banners 	= $this->config->item('packages_banner');
$str 		=  get_term_meta_value($edit->term_id, 'banners_selected');
$services  	= unserialize($str);

echo $this->admin_form->form_open(), form_hidden('edit[term_id]', $edit->term_id), $this->admin_form->submit('', 'confirm_step_service','confirm_step_service','',array('style'=>'display:none;','id'=>'confirm_step_service')) ;

if($banners)
{
	echo '<div id="load-package-info">' ;
	$this->table->set_heading('<b>TÊN GÓI</b>','<b>ĐƠN GIÁ (VNĐ)</b>','<b>SỐ LƯỢNG</b>','<b>GIẢM GIÁ (%)</b>', '<b>TỔNG TIỀN:</b> <span class="sum-price">0 VNĐ</span>');

	foreach ($banners as $key => $packages_banner)
	{
		$banner_type = $key == 'nomal_banner' ? 'BANNER TĨNH' : ('moving_banner' ? 'BANNER ĐỘNG' : 'BANNER NỘI BỘ CÔNG TY');
		$this->table->add_row(array('data' => "<b>{$banner_type}</b>", 'colspan' => 5 , 'class' => 'bg-info'));

		foreach ($packages_banner as $type => $value) 
		{
			$number 	= 1;
			$price 		= $value['price_banner'];
			$selected 	= FALSE;

			$label_sale_off = $label_sum_total = 0;

			if(!empty($services[$value['number']]) && !empty($services[$value['number']]))
			{
				$number 	= $services[$value['number']];
				$price 		= $services[$value['name']];
				$selected 	= TRUE;
			}

			if($number >= $value['sale_off']['min']) $label_sale_off = $value['sale_off']['value'];
			if($selected) $label_sum_total = total_price($price , $number, $value['sale_off']);

			$items = explode('_', $value['number']);
			$index = array_shift($items);
			$item_key = implode('_', $items);
			$item_label_key 	= "label_{$item_key}";
			$item_sale_of_key 	= "saleoff_{$item_key}";

			$_data = array();
			$_data[] = nbs(10)
			.$this->admin_form->checkbox("meta[banners_selected][{$key}][{$type}][checked]", $value['label'], $selected, ['id'=>$value['name']]).form_label($value['label'], $value['name'])
			.form_hidden("meta[banners_selected][{$key}][{$type}][{$item_label_key}]", $value['label'])
			.form_hidden("meta[banners_selected][{$key}][{$type}][{$item_sale_of_key}]", serialize($value['sale_off']));
			
			$_attrs = ['type'=>'number','name'=>"meta[banners_selected][{$key}][{$type}][{$value['name']}]",'class'=>'form-control','id'=>$value['name'],'placeholder'=>'Đơn giá'];
			if( ! $selected) $_attrs['readonly'] = 'true';
			$_data[] = form_input($_attrs, $price);

			$_attrs = ['type'=>'number', 'name'=>"meta[banners_selected][{$key}][{$type}][{$value['number']}]",'class'=>'form-control','id'=>$value['number'],'value'=>$number,'placeholder' => 'Nhập số lượng'];
			if( ! $selected) $_attrs['disabled'] = 'true';
			$_data[] = form_input($_attrs, $number);

			$_data[] = array('data'=>$label_sale_off.'%','class'=>'sale-off-'.$value['name']);
			$_data[] = array('data' => number_format($label_sum_total).' VNĐ' , 'class' => 'td-' . $value['name']);

			$this->table->add_row($_data);
 	 	}
	}

	echo $this->table->generate();
	echo '</div>';
}
?>

</div>
<?php echo $this->admin_form->form_close(); ?>
</div>
<script type="text/javascript">

	function calc_price(price, quanties,sale='') {
		if(price == null) return false;
		if(quanties == null) return false;

		if(sale!='' && quanties>= parseInt(sale['min'])){
			var total = (price * quanties)-(((price * quanties)*parseInt(sale['value']))/100);
			total = total/1000;
			total = Math.round(total);
			return total*1000;
		}
		return (price * quanties);
	}

	function calc_sum_price(array) {
		if(array.length == 0) return false;
		var total_price = 0;

		$.each(array, function(i, val) {
			total_price = total_price + val;
		}); 
		
		return total_price;
	}


	function formatNumber(nStr, decSeperate = '.', groupSeperate = ',', type = ' VNĐ') {
        nStr += '';
        x = nStr.split(decSeperate);
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
        }
        return (x1 + x2) + type;
    }

	var sum_total 		= {};

 	$(document).ready(function() {
 		var clock_contract = '<?php echo $is_editable_locked;?>';
 		if(clock_contract==1){
 			$(":checkbox").attr("disabled",true);
 		}else{
 			$(":checkbox").attr("disabled",false);
 		}


 		$('input[type="checkbox"]').iCheck({
		    checkboxClass: 'icheckbox_flat-blue',
		    radioClass: 'iradio_flat-blue'
  		});

 		var banners = <?php echo json_encode($banners) ;?>;

 		if(banners.length == 0) return false;
 		$('#load-package-info table thead tr span.sum-price').text(formatNumber(0)) ; 
		$.each(banners, function(key, banner) {
			$.each(banner, function(i, val) {
				$('#' + val.name).on('ifChanged', function() {

					// XÁC ĐỊNH THẺ CHA CHỨA
					var tag_parent  = $(this).closest('tr');

					// REMOVE CLASS KHI CHƯA CHỌN GÓI
					tag_parent.removeClass(val.name) ;
					
					// CÓ CHECK VÀO BUTTON RADIO HAY CHƯA: TRUE/FALSE
					var has_checked = $(this).prop( "checked");

					// REMOVE HTML KHI CHƯA CHỌN GÓI
					$('.td-' + val.name).text(formatNumber(0));

					delete sum_total[val.name];

					// TÍNH TỔNG TIỀN CỦA HỢP ĐỒNG
					$('#load-package-info table thead tr span.sum-price').text(formatNumber(0)) ;
					$('#load-package-info table thead tr span.sum-price').text(formatNumber(calc_sum_price(sum_total))) ;

					// KHI CHỌN GÓI => TÍNH LẠI TIỀN
			        if(has_checked == true ) {
			        	tag_parent.addClass(val.name) ;
						$(':input[name|="meta[banners_selected]['+key+']['+i+'][' + val.name + ']"]').attr('readonly',false);
			        	var price  		= $(':input[name|="meta[banners_selected]['+key+']['+i+'][' + val.name + ']"]').val();
			        	var quanties 	= $(':input[name|="meta[banners_selected]['+key+']['+i+'][' + val.number + ']"]').val();

			        	if(quanties>=3)
			        		$('.sale-off-'+ val.name).empty().text(val['sale_off']['value']+'%');
		        		else 
		        			$('.sale-off-'+ val.name).empty().text('0%');

			        	var total  		= calc_price(price, quanties,val['sale_off']) ;

			        	// GÁN MỖI GÓI VỚI TỔNG TIỀN LÀ BAO NHIÊU
			        	sum_total[val.name] = total;
			        	
			        	// TÍNH TỔNG TIỀN CỦA HỢP ĐỒNG
						$('#load-package-info table thead tr span.sum-price').text(formatNumber(0)) ;
						$('#load-package-info table thead tr span.sum-price').text(formatNumber(calc_sum_price(sum_total))) ;
			        	
			        	$('.td-' + val.name).text(formatNumber(total));

			        	// KHI THAY ĐỔI SỐ LƯỢNG => TÍNH TIỀN
			        	$('#' + val.number).on('change keyup', function() {
			        		if($(':input[name|="meta[banners_selected]['+key+']['+i+'][' + val.number + ']"]').val().length <= 0 || $(':input[name|="meta[banners_selected]['+key+']['+i+'][' + val.number + ']"]').val() == 0) 
			        		{
			        			$(':input[name|="meta[banners_selected]['+key+']['+i+'][' + val.number + ']"]').val(1)
			        		}
			        		var price  		= $(':input[name|="meta[banners_selected]['+key+']['+i+'][' + val.name + ']"]').val();
			        		var quanties 	= $(':input[name|="meta[banners_selected]['+key+']['+i+'][' + val.number + ']"]').val();

			        		if(quanties>=3)
			        			$('.sale-off-'+ val.name).empty().text(val['sale_off']['value']+'%');
			        		else 
			        			$('.sale-off-'+ val.name).empty().text('0%');

			        		var total  		= calc_price(price, quanties,val['sale_off']);

			        		$('.td-' + val.name).empty();
							$('.td-' + val.name).text(formatNumber(total));	

							// GÁN MỖI GÓI VỚI TỔNG TIỀN LÀ BAO NHIÊU
			        		sum_total[val.name] = total;
			        		
			        		// TÍNH TỔNG TIỀN CỦA HỢP ĐỒNG
							$('#load-package-info table thead tr span.sum-price').text(formatNumber(0)) ;
							$('#load-package-info table thead tr span.sum-price').text(formatNumber(calc_sum_price(sum_total))) ;
				        	});
			        }else{
			        	$(':input[name|="meta[banners_selected]['+key+']['+i+'][' + val.name + ']"]').attr('readonly',true);
			        	$('.sale-off-'+ val.name).text('0%');
			        }
					document.getElementById(val.number).disabled = !this.checked;
				});	
			});
		});	
		 	});
	$(function(){
		$('.set-datepicker').datepicker({
			format: 'yyyy/mm/dd',
			todayHighlight: true,
			autoclose: true,	
		});
	});

	$(document).ready(function() {
		var load_package_info = $("#load-package-info").closest('form');
		load_package_info.validate({  
			rules: {
			  'meta[banners_selected][description_banner]': {
			    required: true,
			  },
			  'meta[banners_selected][number_banner]': {
			    required: true,
			  },
			  'meta[banners_selected][price_banner]': {
			    required: true,
			  },			  			  		  
			},
			messages: {
			  'meta[banners_selected][description_banner]': {
			    required	: 'Mô tả thiết kế banner không được để trống',
			  },
			  'meta[banners_selected][number_banner]': {
			    required	: 'Số lượng Banner không được để trống',
			    digits		: 'Kiểu dữ liệu không hợp lệ, Số lượng Banner phải là kiểu số',
			  }	,
			  'meta[banners_selected][price_banner]': {
			    required	: 'Đơn giá thiết kế banner không để trống',
			    digits		: 'Kiểu dữ liệu không hợp lệ, Đơn giá thiết kế banner phải là kiểu số',
			  }			  		
			}
		});

		$("#service-config-form").submit(function(e){
			e.preventDefault();

			$.post(admin_url + 'banner/api/contract/service/id/' + <?php echo $edit->term_id;?> + '/meta/' + 'banners_selected', $(this).serializeArray())
            .done(function(response) {
                
                let messages = response.msg;
                for(let i in messages)
                {
                    $.notify(messages[i], 'success');
                }
                setTimeout( () => { location.reload(true); }, 2000);
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                $.notify(JSON.parse(jqXHR.responseText));
            });
		});
	}) ;
 </script>