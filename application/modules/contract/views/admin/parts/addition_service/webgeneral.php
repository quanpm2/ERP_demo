<?php
echo 
'<div style="clear:both;"></div>',
'<div class="col-md-12">',
	$this->admin_form->form_open();

if($edit->term_type == 'webgeneral'){
	$this->load->model('webgeneral/webgeneral_contract_m');
	$this->table->set_caption('Dịch vụ đăng ký');

	$contract_begin = get_term_meta_value($edit->term_id, 'contract_begin');
	$contract_end = get_term_meta_value($edit->term_id, 'contract_end');
	$months = diffInMonths($contract_begin,$contract_end);

	$this->load->config('webgeneral/webgeneral'); 
	$services_pricetag = get_term_meta_value($edit->term_id,'services_pricetag');
	$services_pricetag = @unserialize($services_pricetag);
	$service_price = get_term_meta_value($edit->term_id,'service_package_price');
	if(empty($service_price))
		$service_price = $this->webgeneral_config_m->get_price($edit->term_id);

	echo 
	$this->table
		->add_row(['Dịch vụ','Đơn giá (vnđ/tháng)','Thời gian','Thành tiền'])
		->add_row(['Chăm sóc website', currency_numberformat($service_price), $months, currency_numberformat($service_price * $months)])
		->generate();

	$this->table->clear()->set_caption('Dịch vụ kèm theo');
	$this->table
	->add_row(array('Thiết kế web',
			form_checkbox('services_pricetag[webdesign][is_design]','1', (bool) @$services_pricetag['webdesign']['is_design']),
			$is_editable ? force_var(@$services_pricetag['webdesign']['price'],'miễn phí')  : form_input(array('name'=>'services_pricetag[webdesign][price]','placeholder'=>'Đơn giá'), @$services_pricetag['webdesign']['price']),
			$is_editable ? force_var(@$services_pricetag['webdesign']['theme_id'],'chưa chọn') : form_input(array('name'=>'services_pricetag[webdesign][theme_id]','placeholder'=>'Mã THEME'),@$services_pricetag['webdesign']['theme_id'])))

	->add_row(array('Hosting',
			form_checkbox('services_pricetag[hosting][has_hosting]','1', (bool) @$services_pricetag['hosting']['has_hosting']),
			$is_editable ? force_var(@$services_pricetag['hosting']['price'],'miễn phí') : form_input(array('name'=>'services_pricetag[hosting][price]','placeholder'=>'Đơn giá'),@$services_pricetag['hosting']['price'])))

	->add_row(array('Domain',
			form_checkbox('services_pricetag[domain][is_free]','1', (bool) @$services_pricetag['domain']['is_free']),
			$is_editable ? force_var(@$services_pricetag['domain']['identity'],'Tên miền') : form_input(array('name'=>'services_pricetag[domain][identity]','placeholder'=>'Tên miền'),@$services_pricetag['domain']['identity'])))
	->add_row(array('Tặng theo tháng', '',
			 form_input(array('name'=>'services_pricetag[discount_month]','placeholder'=>'0'),@$services_pricetag['discount_month']).' tháng'))
	->add_row(array('Giảm giá',
			form_checkbox('services_pricetag[discount][is_discount]','1',(bool) @$services_pricetag['discount']['is_discount']),
			$is_editable ? force_var(@$services_pricetag['discount']['percent'],'0%') : form_input(array('name'=>'services_pricetag[discount][percent]','placeholder'=>'0.0%'),@$services_pricetag['discount']['percent']).'%'))

	->add_row(array(
			'Số lần thanh toán','',
			$is_editable 
			? (empty($services_pricetag['number_of_payments']) ? $months : $services_pricetag['number_of_payments']) 
			: form_input(array(
				'name'=>'services_pricetag[number_of_payments]',
				'type'=>'number',	
				'max'=>$months,
				'placeholder'=>'Số lần thanh toán'),
				empty($services_pricetag['number_of_payments']) ? $months : $services_pricetag['number_of_payments'])
		));

	$recalc_contract_value = $this->webgeneral_contract_m->calc_contract_value($edit);
	
	$contract_value = get_term_meta_value($edit->term_id, 'contract_value');
	if($recalc_contract_value != $contract_value){
		update_term_meta($edit->term_id,'contract_value',$contract_value);
		$contract_value = $recalc_contract_value;
	}
	
	if(!$is_editable)
		$btn_action_list .= form_button(['name'=>'service_list_submit','type'=>'submit','class'=>'btn btn-primary'],'<i class="fa fa-fw fa-save"></i>Lưu thay đổi');
	
	echo
	$this->table
	->add_row(['Tổng cộng','','', currency_numberformat($contract_value)])
	->generate();
}
else if($edit->term_type == 'google-ads') 
{

	$services_pricetag = get_term_meta_value($edit->term_id,'services_pricetag');
	$services_pricetag = @unserialize($services_pricetag);

	$this->table->set_caption('Dịch vụ kèm theo');
	$this->table
	->add_row(array('Giảm giá',
			form_checkbox('services_pricetag[discount][is_discount]','1',(bool) @$services_pricetag['discount']['is_discount']),
			$is_editable ? force_var(@$services_pricetag['discount']['percent'],'0%') : form_input(array('name'=>'services_pricetag[discount][percent]','placeholder'=>'0.0%'),@$services_pricetag['discount']['percent']).'%'));
	
	if(!$is_editable)
		$btn_action_list .= form_button(['name'=>'service_list_submit','type'=>'submit','class'=>'btn btn-primary'],'<i class="fa fa-fw fa-save"></i>Lưu thay đổi');
	
	echo $this->table->generate();
}

$this->table->clear()->set_caption(' ');
$this->table->add_row(array($btn_action_list));

echo
	$this->table->generate(),
	$this->admin_form->form_close(),
'</div>';