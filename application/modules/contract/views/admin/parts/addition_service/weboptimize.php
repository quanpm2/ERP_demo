<div style="clear:both;"></div>
<div class="col-md-12">
<?php

$buttons = '';
$contract_end = get_term_meta_value($edit->term_id,'contract_end');
$is_editable_locked = (bool) get_term_meta_value($edit->term_id,'lock_editable');
if( ! $is_editable_locked)
{
	$buttons.= form_button(['name'=>'addition_service_submit','type'=>'submit','class'=>'btn btn-primary pull-right'],'<i class="fa fa-fw fa-save"></i>Lưu thay đổi');
}

echo $this->admin_form->form_open('', ['id' => 'form-hosting']);

$this->table->set_caption('Chi tiết dịch vụ'.$buttons);

$this->table->set_heading('Dịch vụ', 'Số tiền');

$contract_price_weboptimize   	= ( ! get_term_meta_value($edit->term_id, 'contract_price_weboptimize') ) ? 0 : get_term_meta_value($edit->term_id, 'contract_price_weboptimize') ;
$start_service_time = ( ! get_term_meta_value($edit->term_id, 'start_service_time') ) ? '' : my_date(get_term_meta_value($edit->term_id, 'start_service_time'), 'd/m/Y') ;
$end_service_time 	= ( ! get_term_meta_value($edit->term_id, 'end_service_time') ) ? '' : my_date(get_term_meta_value($edit->term_id, 'end_service_time'), 'd/m/Y') ;

$weboptimize_discount_percent 	   = get_term_meta_value($edit->term_id, 'weboptimize_discount_percent') ?? 0;

if( ! $is_editable_locked)
{
	$contract_price_weboptimize_input 		   = form_input(['type'=>'number', 'name'=>'meta[contract_price_weboptimize]','placeholder'=>'Nhập đơn giá', 'id' => 'contract_price_weboptimize_input'], $contract_price_weboptimize);
	$weboptimize_discount_input   			  = form_input(['type'=>'number','step'=>'0.01', 'name'=>'meta[weboptimize_discount_percent]','placeholder'=>'0.0'], $weboptimize_discount_percent);
}
else {
	$contract_price_weboptimize_input 		   = form_input(['type'=>'number', 'name'=>'meta[contract_price_weboptimize_input]','placeholder'=>'Nhập đơn giá', 'id' => 'contract_price_weboptimize_input'], $contract_price_weboptimize, array('readonly' => 'readonly'));	
	$weboptimize_discount_input   			  = form_input(['type'=>'number','step'=>'0.01', 'name'=>'meta[weboptimize_discount_percent]','placeholder'=>'0.0'], $weboptimize_discount_percent, array('readonly' => 'readonly'));
}

$this->table->add_row('Tối ưu website', $contract_price_weboptimize_input);

$this->table->add_row('Giảm giá (nếu có)', $weboptimize_discount_input.' %');

echo $this->table->generate();

echo $this->admin_form->form_close();
?>
</div>
<script type="text/javascript">
$(function(){
	$('.set-datepicker').datepicker({
		format: 'yyyy/mm/dd',
		todayHighlight: true,
		autoclose: true,	
	});
}) ;



</script>