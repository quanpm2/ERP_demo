<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$this->template->javascript->add(base_url("dist/vPushdyConfigurationContractBox.js"));
$is_editable_locked = (bool) get_term_meta_value($edit->term_id, 'lock_editable');
?>
<div style="clear:both;"></div>
<div class="col-md-12" id="package_detail">
	<div id="vPushdyConfigurationContractBox">
	    <v-pushdy-configuration-contract-box
	    	:id="<?php echo $edit->term_id;?>"
	    	mode="<?php echo $is_editable_locked ? 'view' : 'update';?>"
	    	submit_type="api">
	     </v-pushdy-configuration-contract-box>
	</div>
</div>