<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$this->template->javascript->add(admin_theme_url('modules/webbuild/js/webbuild-service-price-component.js'));
?>
<div style="clear:both;"></div>
<div class="col-md-12" id="package_detail">
	<webbuild-service-price-component :id="<?php echo $edit->term_id;?>"></webbuild-service-price-component>
</div>