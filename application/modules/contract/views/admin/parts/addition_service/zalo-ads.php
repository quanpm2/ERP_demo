<?php
$this->template->javascript->add(base_url("dist/vZaloadsConfigurationContractEditPageBox.js?v=202201171035"));
$this->template->javascript->add(base_url("dist/vZaloadsConfigurationContractCuratorsEditPageBox.js?v=202201171035"));
?>
<div style="clear:both;"></div>

<div class="row">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="box box-theme">
                <div class="box-header with-border">
                    <h3 class="box-title">Chương trình khuyến mãi / giảm giá</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div id="vZaloadsConfigurationContractPromotionsBox">
                        <v-zaloads-configuration-contract-promotions-box :term_id="<?php echo $edit->term_id; ?>" <?php echo $is_lock_editable ? '' : ':can-submit="1"'; ?>>
                        </v-zaloads-configuration-contract-promotions-box>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="box box-theme">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin người nhận báo cáo</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div id="vZaloadsConfigurationContractCuratorsBox">
                        <v-zaloads-configuration-contract-curators-box :term_id="<?php echo $edit->term_id; ?>" <?php echo $is_lock_editable ? '' : ':can-submit="1"'; ?>>
                        </v-zaloads-configuration-contract-curators-box>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>