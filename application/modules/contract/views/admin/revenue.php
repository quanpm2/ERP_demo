<?php defined('BASEPATH') OR exit('No direct script access allowed');

if(empty($content))
{
	echo $this->admin_form->box_alert('Không có số liệu thống kê','warning','Cảnh báo');
	return;
}

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');
$this->template->stylesheet->add('plugins/bootstrap-fileinput/css/fileinput.css');
$this->template->javascript->add('plugins/bootstrap-fileinput/js/fileinput.min.js');
$this->template->stylesheet->add('plugins/bootstrap-slider/slider.css');
$this->template->javascript->add('plugins/bootstrap-slider/bootstrap-slider.js');

$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');
?>

<style scoped>
.progress-group .progress-number { font-size: 0.8em; }
section#app-container div.row div.box div.box-body {overflow-x: scroll;}
section#app-container div.row div.box div.box-body table tr td .text-muted { font-size: 0.8em; font-style: italic; }
section#app-container div.row div.box div.box-body table tr th:first-child { width: 150px !important;  }
</style>

<?php 
echo $content['table'] ?? '';
echo $content['pagination'] ?? '';
?>

<!-- MODAL FOR AJAX CALLBACK -->
<div class="modal fade" id="receipt-modal" tabindex="-1" role="dialog" aria-labelledby="receipt-modal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body col-md-12">
				LOADING...
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- END MODAL FOR AJAX CALLBACK -->


<!-- MODAL FOR LIST RECEIPT AJAX CALLBACK -->
<div class="modal fade" id="crud-receipt-modal" tabindex="-1" role="dialog" aria-labelledby="crud-receipt-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="crud-receipt-modal">Danh sách đã thanh toán</h4>
      </div>
      <div class="modal-body">
      	LOADING...
      </div>
    </div>
  </div>
</div>
<!-- END MODAL FOR LIST FOR AJAX CALLBACK -->



<script type="text/javascript">

function ajax_load_receipt_list(ajax_url)
{
	$.ajax({
		url: ajax_url,
		type : 'POST',
		dataType: 'JSON', 
		success: function(response){

			if( ! response.success)	
			{
				$.notify(response.msg, "error");
				return false;
			}
			
			$("#receipt-modal div.modal-content").html($(response.data));

			$("#receipt-modal div.modal-content").find("script").each(function(i) {
                eval($(this).text());
            });	

			$("#receipt-modal").modal('show');
		}
	});
}

$(function()
{
	$('.caution_date').editable({
		format: 'dd-mm-yyyy',    
		viewformat: 'dd-mm-yyyy',    
		datepicker: { weekStart: 1 },
		params: function(params) {

				params.edit = {'end_date':params.value,'post_type':$(this).data('ptype'),'post_title':$(this).data('ptitle'),'post_status':$(this).data('pstatus')};
				return params;
			},
			success: function(data, newValue) {
				if($.isEmptyObject(data.response)) return;
				if($.isEmptyObject(data.response.response)) return;
				if(data.response.response.hasOwnProperty("jscallback")){
					$.each(data.response.response.jscallback,function(i,e){
						var fn = window[e.function_to_call];
						fn(e.data);
					});
				}
			},
		});

	$(".select2").select2({'placeholder':$(this).attr('placeholder')});
	$(".set-datepicker").daterangepicker({format: 'DD-MM-YYYY'});

	$( ".myeditable" ).each(function( index ) {
		$(this).editable({
			url : (function(base_url, el) {

				if(el.data("ajax_call_url") != undefined){

					base_url += el.data("ajax_call_url");
				}
				else base_url += "contract/ajax_dipatcher/ajax_edit/3960";
				
			    return base_url; 

			})(admin_url, $(this)),
			params: function(params) {
				params.type = $(this).data("type-data");
				return params;
			},
			success: function(data, newValue) {
				if($.isEmptyObject(data.response)) return;
				if($.isEmptyObject(data.response.response)) return;
				if(data.response.response.hasOwnProperty("jscallback")){
					$.each(data.response.response.jscallback,function(i,e){
						var fn = window[e.function_to_call];
						fn(e.data);
					});
				}
			},
			defaultValue: "",
		});	  
	});

	$("a.ajax-receipt-list")
	.off('click')
	.on('click',function(e){
		e.preventDefault();
		ajax_load_receipt_list($(this).attr('href'));		
	});
});

$(':input[name|="export_list_revenue_type_excel"]').parent().removeClass('col-xs-2').addClass('col-xs-3') ;
</script>

<script type="text/javascript">
var baseURL = "<?php echo admin_url('');?>";
</script>
<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/revenue-page-app.js'));