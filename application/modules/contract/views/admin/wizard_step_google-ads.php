<div class="col-md-12" id="service_tab">
<?php
	
echo $this->admin_form->form_open();

echo form_hidden('edit[term_id]',$edit->term_id);

echo $this->admin_form->input('CPC cam kết','edit[meta][contract_cpc]',get_term_meta_value($edit->term_id,'contract_cpc'));

echo $this->admin_form->input('Tổng click cam kết','edit[meta][contract_clicks]',get_term_meta_value($edit->term_id,'contract_clicks'));

$_tmp_var = '';

if($begin_time = get_term_meta_value($edit->term_id,'googleads-begin_time')){

	$_tmp_var = date('d-m-Y', $begin_time);
}

echo $this->admin_form->input('Ngày bắt đầu','edit[meta][googleads-begin_time]', $_tmp_var,'Thời gian chạy adword chính thức',
array('class'=>'form-control set-datepicker'));

$_tmp_var = '';

if($end_time = get_term_meta_value($edit->term_id,'googleads-end_time')){

	$_tmp_var = date('d-m-Y', $end_time);
}

echo $this->admin_form->input('Ngày Kết thúc','edit[meta][googleads-end_time]', $_tmp_var,'Thời gian kết thúc adword chính thức',
array('class'=>'form-control set-datepicker'));

echo $this->admin_form->submit(array('name'=>'confirm_step_googleads','id'=>'confirm_step_googleads'), 'confirm_step_googleads','style="display: none;"');

echo $this->admin_form->form_close();
?>
</div>
<script type="text/javascript">
$(function(){
  $(".input-mask").inputmask();
  $('.set-datepicker').datepicker({
    format: 'dd-mm-yyyy',
    todayHighlight: true,
    autoclose: true,
    // startDate: '-0d'
  });
})
</script>

<?php 
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.date.extensions.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.extensions.js');
?>