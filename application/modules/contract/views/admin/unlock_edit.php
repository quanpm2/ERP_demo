<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* SECTION : Contact setting*/
$lock_editable 	= (bool) get_term_meta_value($edit->term_id,'lock_editable');
$readonly 		= $lock_editable || !$has_edit_permission;
$this->table->set_template(['table_open'=>'<table border="1" cellpadding="2" cellspacing="1" class="table table-responsive table-bordered table-hover col-6">']);
$disabled_attr  = $lock_editable ? 'true' : 'false';

$is_system = (!empty($edit->customer) && $edit->customer->user_type == 'system') ? TRUE : FALSE;
$customer_actions = '';

if(has_permission('Admin.Customer.update,Admin.Customer.add') && !empty($edit->customer) && !$is_system)
{
	$customer_actions    .= anchor(admin_url("customer/edit/{$edit->customer->user_id}"),'<i class="fa fa-eye"></i> Xem hồ sơ khách hàng',['target' => '_blank', 'class' => 'text-light-blue']);

    $has_permission = has_permission('customer.overwrite.update')
                      && has_permission('customer.overwrite.manage');
    if(!$lock_editable && $has_permission)
    {
        $customer_actions    .= '<br>' . anchor(admin_url("contract/sync_customer/{$edit->term_id}"),'<i class="fa fa-refresh"></i> Nạp mới thông tin khách hàng', ['data-toggle' => 'confirmation', 'class' => 'text-green']);
    }
}

if($is_system) 
{
	$customer_actions	.= '<a href="#" data-toggle="modal" data-target="#change-customer-owner-modal">Chưa có khách hàng ! Cập nhật ?</a>';
}

$this->table->set_caption('Thông tin hợp đồng');
// Mã hơp đồng
$this->table->add_row('Mã hợp đồng','<xeditable '._attributes_to_string(['name'=>'contract_code','value'=>get_term_meta_value($edit->term_id, 'contract_code'),'title'=>'Mã hợp đồng','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta','remote_url'=>admin_url('contract/api/contract/update'),':disabled'=> $lock_editable ? 'true' : ($readonly ? 'true' : ($is_manage ? 'false' : 'true'))]).'></xeditable>');
$this->table->add_row('Khách hàng',($edit->customer->display_name??'') . '<br/>'.$customer_actions);

$_attrs = array(
	'name' 	=> 'representative_gender',
	'value' => (int) get_term_meta_value($edit->term_id, 'representative_gender'),
	'title' => 'Giới tính',
	'type' 	=> 'select',
	'pk' 	=> $edit->term_id,
	'metadata' 		=> 'meta',
	':disabled'  	=> $disabled_attr,
	'datasource'	=> htmlspecialchars(json_encode([['value'=>1,'text'=>'Ông'],['value'=>0,'text'=>'Bà']])),
	'remote_url' 	=> admin_url('contract/api/contract/update')
	);
$_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';

$_attrs = array(
	'name' 	=> 'representative_name',
	'value' => get_term_meta_value($edit->term_id, 'representative_name'),
	'title' => 'Người đại diện',
	'type' 	=> 'text',
	'pk' 	=> $edit->term_id,
	'metadata' 		=> 'meta',
	':disabled'  	=> $disabled_attr,
	'remote_url' 	=> admin_url('contract/api/contract/update')
	);
$_cell_value.= str_repeat('&nbsp', 3).'<xeditable '._attributes_to_string($_attrs).'></xeditable>';
$this->table->add_row('Người đại diện', "<span id='incline-data'>{$_cell_value}</span>");


$this->table->add_row('Theo giấy ủy quyền số','<xeditable '._attributes_to_string(['name'=>'representative_loa','value'=>get_term_meta_value($edit->term_id, 'representative_loa'),'title'=>'Theo giấy ủy quyền số','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');


$this->table->add_row('Email','<xeditable '._attributes_to_string(['name'=>'representative_email','value'=>get_term_meta_value($edit->term_id, 'representative_email'),'title'=>'Email','type'=>'email','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

$this->table->add_row('Địa chỉ','<xeditable '._attributes_to_string(['name'=>'representative_address','value'=>get_term_meta_value($edit->term_id, 'representative_address'),'title'=>'Địa chỉ','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

$this->table->add_row('Vùng miền','<xeditable '._attributes_to_string(['name'=>'representative_zone','value'=>get_term_meta_value($edit->term_id, 'representative_zone'),'title'=>'Vùng miền','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array_values($this->config->item('zone')))))]).'></xeditable>');

$this->table->add_row('Số điện thoại','<xeditable '._attributes_to_string(['name'=>'representative_phone','value'=>get_term_meta_value($edit->term_id, 'representative_phone'),'title'=>'Số điện thoại','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

$this->table->add_row('Chức vụ','<xeditable '._attributes_to_string(['name'=>'representative_position','value'=>get_term_meta_value($edit->term_id, 'representative_position'),'title'=>'Chức vụ','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');


/* BEGIN : Thông tin người đại diện xác nhận hợp đồng thiết kế website */
if($edit->term_type == 'webdesign')
{
	$_attrs = array(
		'name' 	=> 'qc_confirm_gender',
		'value' => (int) get_term_meta_value($edit->term_id, 'qc_confirm_gender'),
		'title' => 'Giới tính',
		'type' 	=> 'select',
		'pk' 	=> $edit->term_id,
		'metadata' 		=> 'meta',
		':disabled'		=> $disabled_attr,
		'datasource'	=> htmlspecialchars(json_encode([['value'=>1,'text'=>'Ông'],['value'=>0,'text'=>'Bà']])),
		'remote_url' 	=> admin_url('contract/api/contract/update')
		);
	$_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';

	$_attrs = array(
		'name' 	=> 'qc_confirm_name',
		'value' => get_term_meta_value($edit->term_id, 'qc_confirm_name'),
		'title' => 'Người đại diện',
		'type' 	=> 'text',
		'pk' 	=> $edit->term_id,
		'metadata' 		=> 'meta',
		':disabled'		=> $disabled_attr,
		'remote_url' 	=> admin_url('contract/api/contract/update')
		);
	$_cell_value.= str_repeat('&nbsp', 3).'<xeditable '._attributes_to_string($_attrs).'></xeditable>';

	$this->table->add_row('Người Đại diện duyệt sản phẩm', "<span id='incline-data'>{$_cell_value}</span>");
	$this->table->add_row('Email người duyệt','<xeditable '._attributes_to_string(['name'=>'qc_confirm_email','value'=>get_term_meta_value($edit->term_id, 'qc_confirm_email'),'title'=>'Email','type'=>'email','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

	$this->table->add_row('Số ĐT người duyệt','<xeditable '._attributes_to_string(['name'=>'qc_confirm_phone','value'=>get_term_meta_value($edit->term_id, 'qc_confirm_phone'),'title'=>'Địa chỉ','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');
}
/* END : Thông tin người đại diện xác nhận hợp đồng thiết kế website */


$this->table->add_row('Mã Số thuế','<xeditable '._attributes_to_string(['name'=>'customer_tax','value'=>($edit->extra['customer_tax'] ?? ''),'title'=>'Mã Số thuế','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

$this->table->add_row('Giá trị hợp đồng', currency_numberformat(get_term_meta_value($edit->term_id, 'contract_value')));

if($edit->term_type != 'webgeneral')
{
	$this->table->add_row('Số đợt thanh toán','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'number_of_payments','value'=>(get_term_meta_value($edit->term_id, 'number_of_payments')?:1),'title'=>'Số đợt thanh toán','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');
}

$this->table->add_row('VAT (%)','<xeditable  :reload_if_success="true" '._attributes_to_string(['name'=>'vat','value'=>get_term_meta_value($edit->term_id, 'vat'),'title'=>'VAT (%)','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'0 %','name'=>0], ['text'=>'10 %','name'=>10], ['text'=>'8 %','name'=>8]))))]).'></xeditable>');

$this->table->add_row('Hình thức','<xeditable '._attributes_to_string(['name'=>'trading_type','value'=>get_term_meta_value($edit->term_id, 'trading_type'),'title'=>'Hình thức hợp đồng','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array(['value'=>'normal','text'=>'Thông dụng'], ['value'=>'agency','text'=>'Agency'])))]).'></xeditable>');

$this->table->add_row('Ngày bắt đầu','<xeditable '._attributes_to_string(['name'=>'contract_begin','value'=>my_date(get_term_meta_value($edit->term_id,'contract_begin'), 'd-m-Y'),'title'=>'Ngày bắt đầu','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

$this->table->add_row('Ngày kết thúc','<xeditable '._attributes_to_string(['name'=>'contract_end','value'=>my_date(get_term_meta_value($edit->term_id,'contract_end'), 'd-m-Y'),'title'=>'Ngày kết thúc','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

$attachment_files = get_term_meta_value($edit->term_id, '_attachment_files');
$attachment_files = is_serialized($attachment_files) ? unserialize($attachment_files) : [];

$grid_files = '<div id="grid_files">';
if( ! empty($attachment_files))
{
	$grid_files.= implode(br(), array_map(function($x){ return anchor(base_url($x['file_path']), $x['file_name'], "id='attactment_file' target='_blank' title='Tải File {$x['file_name']}'");}, $attachment_files));
}
$grid_files.= '</div>';

$this->table
->add_row(array('Tệp đính kèm', 
	$this->admin_form->form_open().
	$this->admin_form->upload('','fileinput',get_term_meta_value($edit->term_id, '_tmp_file_upload'),$grid_files,
		array('id'=>'contract-inputfile','multiple'=>'multiple','none_label'=>TRUE)).
	$this->admin_form->form_close()));

echo '<div class="col-md-6">'.$this->table->generate().'</div>';

// --------------------------------------------------------------------
// THÔNG TIN NGÂN HÀNG
// --------------------------------------------------------------------
$this->table->set_caption('Thông tin ngân hàng');

$this->table->add_row('Số TK','<xeditable '._attributes_to_string(['name'=>'bank_id','value'=>($edit->extra['bank_id']??''),'title'=>'Số TK','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

$this->table->add_row('Chủ TK','<xeditable '._attributes_to_string(['name'=>'bank_owner','value'=>($edit->extra['bank_owner']??''),'title'=>'Chủ TK','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

$this->table->add_row('Thuộc ngân hàng','<xeditable '._attributes_to_string(['name'=>'bank_name','value'=>($edit->extra['bank_name']??''),'title'=>'Thuộc ngân hàng','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

$this->table->add_row('Hình thức thanh toán','<xeditable '._attributes_to_string(['name'=>'payment_method','value'=>get_term_meta_value($edit->term_id, 'payment_method'),'title'=>'Hình thức thanh toán','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array_values($this->config->item('payment_methods')))))]).'></xeditable>');

$this->table->add_row('Tài khoản đến','<xeditable '._attributes_to_string(['name'=>'payment_bank_account','value'=>get_term_meta_value($edit->term_id, 'payment_bank_account'),'title'=>'Tài khoản đến','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array(['value'=>'company','text'=>'Tài khoản công ty'], ['value'=>'person','text'=>'Tài khoản cá nhân'])))]).'></xeditable>');

echo '<div class="col-md-6">'.$this->table->generate().'</div>';

// --------------------------------------------------------------------
// THÔNG TIN QUẢN TRỊ
// --------------------------------------------------------------------
$this->table->set_caption('Thông tin quản trị');

// Thông tin nhân viên kinh doanh phụ trách

// $this->table->add_row('Nhân viên kinh doanh','<xeditable '._attributes_to_string([
// 	'name'			=> 'staff_business',
// 	'value'			=> htmlentities(get_term_meta_value($edit->term_id, 'staff_business')),
// 	'title'			=> 'Nhân viên kinh doanh',
// 	'type'			=> 'select2',
// 	'pk'			=> $edit->term_id,
// 	'metadata'		=> 'meta',
// 	':disabled'		=> $disabled_attr,
// 	'remote_url'	=> admin_url('contract/api/contract/update'),
// 	'datasource'	=> htmlspecialchars(json_encode(array_map(function($k,$v){
// 		return array(
// 			'id' => $k,
// 			'text' 	=> $v
// 		);
// 	},array_keys($staffs) , $staffs)))
// ]).'></xeditable>');

# check role of admin is in sale groups & build input form for its

$assigned_to_ids = get_user_meta($edit->customer->user_id, 'assigned_to', FALSE, TRUE);
$assigned_to = array_filter(array_unique($assigned_to_ids));
if(!empty($assigned_to))
{
    $assigned_to = array_map(function($assigned_to_id){
        $item = [
            'id' => $assigned_to_id,
            'display_name' =>trim($this->admin_m->get_field_by_id($assigned_to_id, 'display_name')),
        ];
        return $item;
    }, $assigned_to);
}
$this->table->add_row('Nhân viên kinh doanh','<select-staff-business '._attributes_to_string([
    'name'              => 'staff_business',
    'term_id'           => $edit->term_id,
	'staff_business_id' => htmlentities(get_term_meta_value($edit->term_id, 'staff_business')),
    'remote_url'        => admin_url('contract/api/contract/update'),
	':disabled'         => $disabled_attr,
	':datasource'       => htmlspecialchars(json_encode(array_map(function($k,$v){
		return [
			'id' => $k,
			'text' 	=> $v
        ];
	}, array_keys($staffs) , $staffs))),
    ':extra_data'        => htmlspecialchars(json_encode($assigned_to)),
]).'></xeditable>');

$this->table->add_row('Ghi chú hợp đồng','<xeditable '._attributes_to_string(['name'=>'contract_note','value'=>htmlentities(get_term_meta_value($edit->term_id, 'contract_note')),'title'=>'Ghi chú hợp đồng','type'=>'textarea','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');


$this->table->add_row('Đã nhận bản cứng','<xeditable '._attributes_to_string(['name'=>'HardCoppyStatus','value'=>get_term_meta_value($edit->term_id, 'HardCoppyStatus')?:'UNKNOWN','title'=>'Đã nhận bản cứng','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array(['value'=>'UNKNOWN','text'=>'Chưa xác định'], ['value'=>'RECEIVED','text'=>'Đã nhận'])))]).'></xeditable>');


if(in_array($edit->term_type, [ 'google-ads', 'facebook-ads' ]) && in_array($edit->term_status, [ 'ending', 'liquidation' ]) && in_array($this->admin_m->role_id, [ 1, 5 ]))
{
	$stateOptions = array(
		[ 'value' => 'publish', 'text' => $this->config->item('publish', 'contract_status') ],
		[ 'value' => $edit->term_status, 'text' => $this->config->item($edit->term_status, 'contract_status') ]
	);

	$this->table->add_row(
		'Trạng thái hợp đồng',
		'<xeditable :reload_if_success="true" '._attributes_to_string([
			'name' => 'term_status',
			'value' => $edit->term_status,
			'title' => 'Trạng thái hợp đồng',
			'type' => 'select',
			'pk' => $edit->term_id,
			'metadata' => 'field',
			':disabled' => $disabled_attr,
			'remote_url' => admin_url('contract/api/contract/update'),
			'datasource' => htmlspecialchars(json_encode($stateOptions))
	]).'></xeditable>');
}
else
{
	$this->table->add_row('Trạng thái hợp đồng',$this->config->item($edit->term_status, 'contract_status'));
}

echo '<div class="col-md-6">'.$this->table->generate().'</div>';
// --------------------------------------------------------------------
// CHI TIẾT DỊCH VỤ
// --------------------------------------------------------------------
$this->table->set_caption('Chi tiết dịch vụ');
$this->table->add_row(['Dịch vụ hợp đồng', $this->config->item($edit->term_type,'services')]);

// HIỂN THỊ TÊN WEBSITE
$_website_lbl = ($edit->term_type == 'domain') ? 'Tên miền' : 'Website thực hiện' ;

$this->table->add_row($_website_lbl,'<xeditable '._attributes_to_string(['name'=>'term_parent','value'=>$edit->term_parent,'title'=>$_website_lbl,'type'=>'select','pk'=>$edit->term_id,'metadata'=>'field',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($websites) , $websites)))]).'></xeditable>');

/**
* CHỌN NGÀNH NGHỀ THỰC HIỆN CHO HỢP ĐỒNG
*/
$categories_values = implode(',', array_map(function($x) use ($categories) { return $categories[$x]; }, $term_categories));
$this->table->add_row('Ngành nghề thực hiện',
$this->admin_form->input('','',$categories_values,'', array(
	'data-original-title'	=> 'Ngành nghề thực hiện',
	'data-pk' => $edit->term_id,
	'data-type-data' => 'field',	
	'data-ajax_call_url' => "contract/ajax/contract/set_categories/{$edit->term_id}",
	'data-name'	=> 'term_categories',
	'data-type'	=> 'checklist',
	'data-value' => htmlspecialchars(json_encode($term_categories)),
	'data-source' => str_replace('"', '\'', json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},
		array_keys($categories) , $categories))), 
	':disabled' =>	$disabled_attr,
	'is_x_editable'	=> 'true', 
	'none_label'=>TRUE,
	'override_jscript'=>TRUE))
);


switch ($edit->term_type) // SERVICE DETAIL OF CONTRACT
{
	case 'hosting': 
		$this->config->load('hosting/hosting');

		$service_package 	= get_term_meta_value($edit->term_id, 'service_package');
		$service_package 	= is_serialized($service_package) ? unserialize($service_package) : [];
		$month_enum 		= $this->config->item('month', 'package_config')['list'];

		$this->table->add_row('Số tiền giảm giá','<xeditable '._attributes_to_string(['name'=>'discount_amount','value'=>get_term_meta_value($edit->term_id, 'discount_amount'),'title'=>'Số tiền giảm giá','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>base_url('api-v2/hosting/contract')]).'></xeditable>');

		$this->table->add_row('Số năm đăng ký','<xeditable '._attributes_to_string(['name'=>'hosting_months','value'=>get_term_meta_value($edit->term_id, 'hosting_months'),'title'=>'Số năm đăng ký','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta','remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($month_enum) , $month_enum)))]).'></xeditable>');

		if(has_permission('Hosting.security.manage'))
		{
			$this->table->add_row('Cpanel Username','<xeditable '._attributes_to_string(['name'=>'hosting_username','value'=>get_term_meta_value($edit->term_id, 'hosting_username'),'title'=>'Cpanel Username','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta','remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

			$this->table->add_row('Cpanel Password','<xeditable '._attributes_to_string(['name'=>'hosting_password','value'=>get_term_meta_value($edit->term_id, 'hosting_password'),'title'=>'Cpanel Password','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta','remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

			$this->table->add_row('FTP Hostname','<xeditable '._attributes_to_string(['name'=>'hosting_ftp_hostname','value'=>get_term_meta_value($edit->term_id, 'hosting_ftp_hostname'),'title'=>'FTP Hostname','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta','remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

			$this->table->add_row('FTP Port','<xeditable '._attributes_to_string(['name'=>'hosting_ftp_port','value'=>get_term_meta_value($edit->term_id, 'hosting_ftp_port'),'title'=>'FTP Port','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta','remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

			$this->table->add_row('FTP Username','<xeditable '._attributes_to_string(['name'=>'hosting_ftp_username','value'=>get_term_meta_value($edit->term_id, 'hosting_ftp_username'),'title'=>'FTP Username','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta','remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

			$this->table->add_row('FTP Password','<xeditable '._attributes_to_string(['name'=>'hosting_ftp_password','value'=>get_term_meta_value($edit->term_id, 'hosting_ftp_password'),'title'=>'FTP Password','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta','remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');
		}

		break;

	case 'google-ads':

		$this->table->add_row('Ẩn/Hiện CTKM trong bản In','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'is_display_promotions_discount','value'=> (int) get_term_meta_value($edit->term_id, 'is_display_promotions_discount'),'title'=>'Ẩn/Hiện CTKM trong bản In','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'Hiện', 'name'=> 1], ['text'=>'Ẩn', 'name'=> 0]))))]).'></xeditable>');

		$this->load->config('googleads/googleads');
		$service_types 		= $this->config->item('report_type','googleads');
		$keys_service_type 	= array_keys($service_types);
		$service_type 		= force_var(get_term_meta_value($edit->term_id,'service_type'), reset($keys_service_type));
		$is_cpc_type		= ($service_type == 'cpc_type');

		if($is_cpc_type) # if the service is 'cpc_type'type
		{
			$this->table
			->add_row('Hình thức quản lý','Quản lý theo clicks & CPC')

			->add_row('[SEARCH] CPC cam kết','<xeditable '._attributes_to_string(['name'=>'contract_cpc','value'=>get_term_meta_value($edit->term_id, 'contract_cpc'),'title'=>'Cost per click (CPC) cam kết','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>')

			->add_row('[SEARCH] Click cam kết','<xeditable '._attributes_to_string(['name'=>'contract_clicks','value'=>get_term_meta_value($edit->term_id, 'contract_clicks'),'title'=>'Tổng click cam kết','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>')

			->add_row('[GDN] CPC cam kết','<xeditable '._attributes_to_string(['name'=>'gdn_contract_cpc','value'=>get_term_meta_value($edit->term_id, 'gdn_contract_cpc'),'title'=>'[GDN] CPC cam kết','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>')

			->add_row('[GDN] Click cam kết','<xeditable '._attributes_to_string(['name'=>'gdn_contract_clicks','value'=>get_term_meta_value($edit->term_id, 'gdn_contract_clicks'),'title'=>'[GDN] Click cam kết','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');
		}
		# else the service type is 'account_type' type
		else
		{ 
			$isAccountForRent = (int) get_term_meta_value($edit->term_id, 'isAccountForRent');

			$this->table->add_row('Hình thức quản lý', $isAccountForRent ? 'Cho thuê tài khoản' : 'Quản lý theo tài khoản');

			$this->table->add_row('Loại hình dịch vụ','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'isAccountForRent','value'=> (int) get_term_meta_value($edit->term_id, 'isAccountForRent'),'title'=>'Loại hình dịch vụ','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'Mặc định', 'name'=> 0], ['text'=>'Cho thuê tài khoản', 'name'=> 1]))))]).'></xeditable>');

			$this->config->load('googleads/contract');

			$contract_budget_payment_types = $this->config->item('enums', 'contract_budget_payment_types');
			$_contract_budget_payment_type = get_term_meta_value($edit->term_id, 'contract_budget_payment_type') ?: $this->config->item('default', 'contract_budget_payment_types');

			$this->table->add_row('Phương thức thanh toán NSQC','<xeditable :reload_if_success="true"'._attributes_to_string(['name'=>'contract_budget_payment_type','value'=>$_contract_budget_payment_type,'title'=>'Phương thức thanh toán ngân sách quảng cáo','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($contract_budget_payment_types) , $contract_budget_payment_types)))]).'></xeditable>');

			if('normal' != $_contract_budget_payment_type)
			{
				$contract_budget_customer_payment_type 	= get_term_meta_value($edit->term_id, 'contract_budget_customer_payment_type') ?: $this->config->item('default', 'contract_budget_customer_payment_types');
				$contract_budget_customer_payment_types = $this->config->item('enums', 'contract_budget_customer_payment_types');

				$this->table->add_row('Tùy chọn thu & chi','<xeditable :reload_if_success="true"'._attributes_to_string(['name'=>'contract_budget_customer_payment_type','value'=>$contract_budget_customer_payment_type,'title'=>'Tùy chọn thu & chi','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($contract_budget_customer_payment_types) , $contract_budget_customer_payment_types)))]).'></xeditable>');
			}

			$service_fee_payment_type_def = $this->config->item('service_fee_payment_type');

			$this->table->add_row('Phương thức thanh toán phí dịch vụ','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'service_fee_payment_type','value'=>get_term_meta_value($edit->term_id, 'service_fee_payment_type'),'title'=>'Phương thức thanh toán phí dịch vụ','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($service_fee_payment_type_def) , $service_fee_payment_type_def)))]).'></xeditable>');

			$this->table
			->add_row('Số tiền khách cọc','<xeditable '._attributes_to_string([
				'name' 			=> 'deposit_amount',
				'value' 		=> get_term_meta_value($edit->term_id, 'deposit_amount'),
				'title' 		=> 'Số tiền khách cọc',
				'type' 			=> 'number',
				'pk' 			=> $edit->term_id,
				'metadata' 		=> 'meta',
				':disabled' 	=> $disabled_attr,
				'remote_url' 	=> admin_url('contract/api/contract/update')
			]).'></xeditable>');

			$this->table
			->add_row('Ngân sách chạy quảng cáo','<xeditable '._attributes_to_string([
				'name' 			=> 'contract_budget',
				'value' 		=> get_term_meta_value($edit->term_id, 'contract_budget'),
				'title' 		=> 'Ngân sách chạy quảng cáo',
				'type' 			=> 'number',
				'pk' 			=> $edit->term_id,
				'metadata' 		=> 'meta',
				':disabled' 	=> $disabled_attr,
				'remote_url' 	=> admin_url('contract/api/contract/update'),
				':reload_if_success' => "true"
			]).'></xeditable>')

			->add_row('Thuế Google thu (%)','<xeditable '._attributes_to_string([
				'name' 			=> 'service_provider_tax_rate',
				'value' 		=> ((double) get_term_meta_value($edit->term_id, 'service_provider_tax_rate')*100),
				'title' 		=> 'Phí dịch vụ',
				'type' 			=> 'text',
				'pk' 			=> $edit->term_id,
				'metadata' 		=> 'meta',
				':disabled'		=> 'true',
				'remote_url' 	=> admin_url('#'),
				':reload_if_success' => "true"
			]).'></xeditable>')

			->add_row('Thuế Google thu','<xeditable '._attributes_to_string([
				'name' 			=> 'service_provider_tax',
				'value' 		=> get_term_meta_value($edit->term_id, 'service_provider_tax'),
				'title' 		=> 'Phí dịch vụ',
				'type' 			=> 'number',
				'pk' 			=> $edit->term_id,
				'metadata' 		=> 'meta',
				':disabled'		=> 'true',
				'remote_url' 	=> admin_url('#'),
				':reload_if_success' => "true"
			]).'></xeditable>')

			->add_row('Phí áp dụng (%)','<xeditable '._attributes_to_string([
				'name' 			=> 'original_service_fee_rate',
				'value' 		=> ((double) get_term_meta_value($edit->term_id, 'original_service_fee_rate')*100),
				'title' 		=> 'Phí dịch vụ',
				'type' 			=> 'text',
				'pk' 			=> $edit->term_id,
				'metadata' 		=> 'meta',
				':disabled' 	=> $disabled_attr,
				'remote_url' 	=> admin_url('contract/api/contract/update'),
				':reload_if_success' => "true"
			]).'></xeditable>');

            if('range' == get_term_meta_value($edit->term_id, 'service_fee_payment_type')){
                $this->table->add_row('Phí thực tế (%)','<xeditable '._attributes_to_string([
                    'name' 			=> 'service_fee_rate_actual',
                    'value' 		=> ((double) get_term_meta_value($edit->term_id, 'service_fee_rate_actual')*100),
                    'title' 		=> 'Phí thực tế (%)',
                    'type' 			=> 'text',
                    'pk' 			=> $edit->term_id,
                    'metadata' 		=> 'meta',
                    ':disabled' 	=> true,
                    'remote_url' 	=> admin_url('contract/api/contract/update'),
                    ':reload_if_success' => "true"
                ]).'></xeditable>');
            }

			$this->table->add_row('Phí dịch vụ','<xeditable '._attributes_to_string([
				'name' 			=> 'service_fee',
				'value' 		=> get_term_meta_value($edit->term_id, 'service_fee'),
				'title' 		=> 'Phí dịch vụ',
				'type' 			=> 'number',
				'pk' 			=> $edit->term_id,
				'metadata' 		=> 'meta',
				':disabled'		=> 'true',
				'remote_url' 	=> admin_url('contract/api/contract/update'),
				':reload_if_success' => "true"
			]).'></xeditable>');

            $this->load->model('option_m');
            $exchange_rates = $this->option_m->get_value('exchange_rate', TRUE);
            foreach($exchange_rates as $currency => $exchange_rate){
                $meta_key = 'exchange_rate_' . strtolower($currency);
            
                $meta_value = (double) get_term_meta_value($edit->term_id, $meta_key) ?? 0;
                if(!empty($meta_value)) {
                    $regex = '/^([a-z]{3})_to_[a-z]{3}$/';
                    preg_match($regex, $currency, $currency);
                    $currency = strtoupper($currency[1]);

                    $_attrs = [
                        'name' 	=> $meta_key,
                        'value' => (float) $meta_value,
                        'title' => "Tỷ giá 1 {$currency}",
                        'type' 	=> 'number',
                        'pk' 	=> $edit->term_id,
                        'metadata' 		=> 'meta',
                        ':disabled'		=> $disabled_attr,
                        'remote_url' 	=> admin_url('contract/api/contract/update'),
                        ':reload_if_success' => "true"
                    ];
                    
                    $_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
                    $this->table->add_row($_attrs['title'], $_cell_value);
                }
            }

            $usd_to_vnd = (double) get_term_meta_value($edit->term_id, 'exchange_rate_usd_to_vnd') ?? 0;
            if(empty($usd_to_vnd)){
                $_attrs = [
                    'name' 	=> 'exchange_rate_usd_to_vnd',
                    'value' => (float) $usd_to_vnd,
                    'title' => "Tỷ giá 1 USD",
                    'type' 	=> 'number',
                    'pk' 	=> $edit->term_id,
                    'metadata' 		=> 'meta',
                    ':disabled'		=> $disabled_attr,
                    'remote_url' 	=> admin_url('contract/api/contract/update'),
                    ':reload_if_success' => "true"
                ];
                
                $_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
                $this->table->add_row($_attrs['title'], $_cell_value);
            }

            $aud_to_vnd = (double) get_term_meta_value($edit->term_id, 'exchange_rate_aud_to_vnd') ?? 0;
            if(empty($aud_to_vnd)){
                $_attrs = [
                    'name' 	=> 'exchange_rate_aud_to_vnd',
                    'value' => (float) $aud_to_vnd,
                    'title' => "Tỷ giá 1 AUD",
                    'type' 	=> 'number',
                    'pk' 	=> $edit->term_id,
                    'metadata' 		=> 'meta',
                    ':disabled'		=> $disabled_attr,
                    'remote_url' 	=> admin_url('contract/api/contract/update'),
                    ':reload_if_success' => "true"
                ];
                
                $_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
                $this->table->add_row($_attrs['title'], $_cell_value);
            }
		}

		$this->table->add_row('Phí nhà thầu khách chịu (%)','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'fct','value'=>get_term_meta_value($edit->term_id, 'fct'),'title'=>'Phí nhà thầu khách chịu (%)','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'5 %','name'=>5], ['text'=>'0 %','name'=>0]))))]).'></xeditable>');

		$this->table->add_row('Cty chịu VAT','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'isVatCompanyPay','value'=> (int) get_term_meta_value($edit->term_id, 'isVatCompanyPay'),'title'=>'Cty chịu VAT','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'Có', 'name'=> 1], ['text'=>'Không', 'name'=> 0]))))]).'></xeditable>');

		$default_network_types = array('search'=>'search','display'=>'display','facebook'=>'facebook');
		if($is_cpc_type && isset($default_network_types['facebook'])) unset($default_network_types['facebook']);

		$network_types 			= get_term_meta_value($edit->term_id,'network_type');

		$this->table->add_row('Kênh quảng cáo','<xeditable '._attributes_to_string(['name'=>'network_type','value'=>get_term_meta_value($edit->term_id, 'network_type'),'title'=>'Kênh quảng cáo','type'=>'checklist','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($default_network_types) , $default_network_types)))]).'></xeditable>');

		$this->load->model('googleads/googleads_config_m');
		$this->load->config('googleads/googleads');
		$bonus_packages = array_map(function($x){ return $x['label'];}, $this->config->item('service','packages'));

		$this->table->add_row('Gói dịch vụ tặng kèm','<xeditable '._attributes_to_string(['name'=>'bonus_package','value'=>get_term_meta_value($edit->term_id, 'bonus_package'),'title'=>'Gói dịch vụ tặng kèm','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>'true','remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($bonus_packages) , $bonus_packages)))]).'></xeditable>');

		$this->load->model('googleads/mcm_account_m');
		$mcm_client_id = get_term_meta_value($edit->term_id,'mcm_client_id');
		$mcm_account = $this->mcm_account_m->get($mcm_client_id);

		if( ! empty($mcm_account))
		{
			$this->table->add_row('Adword client customer ID','<xeditable '._attributes_to_string(['name'=>'account_name','value'=>get_term_meta_value($mcm_account->term_id, 'account_name'),'title'=>'Adword client customer ID','type'=>'text','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>'true']).'></xeditable>');
		}

		$this->table->add_row('Ngày chạy Adword','<xeditable '._attributes_to_string(['name'=>'googleads-begin_time','value'=>my_date(get_term_meta_value($edit->term_id,'googleads-begin_time'), 'd-m-Y'),'title'=>'Ngày chạy Adword','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

		$this->table->add_row('Ngày kết thúc Adword','<xeditable '._attributes_to_string(['name'=>'googleads-end_time','value'=>my_date(get_term_meta_value($edit->term_id,'googleads-end_time'), 'd-m-Y'),'title'=>'Ngày kết thúc Adword','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');
		break;

	case 'facebook-ads':

		$this->table->add_row('Ẩn/Hiện CTKM trong bản In','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'is_display_promotions_discount','value'=> (int) get_term_meta_value($edit->term_id, 'is_display_promotions_discount'),'title'=>'Ẩn/Hiện CTKM trong bản In','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'Hiện', 'name'=> 1], ['text'=>'Ẩn', 'name'=> 0]))))]).'></xeditable>');

		$this->config->load('facebookads/contract');

		$isAccountForRent = (int) get_term_meta_value($edit->term_id, 'isAccountForRent');

		$this->table->add_row('Hình thức quản lý', $isAccountForRent ? 'Cho thuê tài khoản' : 'Quản lý theo tài khoản');

		$this->table->add_row('Loại hình dịch vụ','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'isAccountForRent','value'=> (int) get_term_meta_value($edit->term_id, 'isAccountForRent'),'title'=>'Loại hình dịch vụ','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'Mặc định', 'name'=> 0], ['text'=>'Cho thuê tài khoản', 'name'=> 1]))))]).'></xeditable>');

		$this->config->load('facebookads/contract');

		$contract_budget_payment_types = $this->config->item('enums', 'contract_budget_payment_types');
		$this->table->add_row('Phương thức thanh toán NSQC','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'contract_budget_payment_type','value'=>get_term_meta_value($edit->term_id, 'contract_budget_payment_type'),'title'=>'Phương thức thanh toán ngân sách quảng cáo','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($contract_budget_payment_types) , $contract_budget_payment_types)))]).'></xeditable>');

		$contract_budget_customer_payment_types = $this->config->item('enums', 'contract_budget_customer_payment_types');
		$this->table->add_row('Tùy chọn thu & chi','<xeditable '._attributes_to_string(['name'=>'contract_budget_customer_payment_type','value'=>get_term_meta_value($edit->term_id, 'contract_budget_customer_payment_type'),'title'=>'Tùy chọn thu & chi','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($contract_budget_customer_payment_types) , $contract_budget_customer_payment_types)))]).'></xeditable>');

		$service_fee_payment_type_def = $this->config->item('service_fee_payment_type');
		$this->table->add_row('Phương thức thanh toán phí dịch vụ','<xeditable '._attributes_to_string(['name'=>'service_fee_payment_type','value'=>get_term_meta_value($edit->term_id, 'service_fee_payment_type'),'title'=>'Phương thức thanh toán phí dịch vụ','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($service_fee_payment_type_def) , $service_fee_payment_type_def)))]).'></xeditable>');

		$this->table
		->add_row('Số tiền khách cọc','<xeditable '._attributes_to_string([
			'name' 			=> 'deposit_amount',
			'value' 		=> get_term_meta_value($edit->term_id, 'deposit_amount'),
			'title' 		=> 'Số tiền khách cọc',
			'type' 			=> 'number',
			'pk' 			=> $edit->term_id,
			'metadata' 		=> 'meta',
			':disabled' 	=> $disabled_attr,
			'remote_url' 	=> admin_url('contract/api/contract/update')
		]).'></xeditable>');

		$_attrs = array(
			'name' 	=> 'contract_budget',
			'value' => (int) get_term_meta_value($edit->term_id, 'contract_budget'),
			'title' => 'Ngân sách chạy quảng cáo',
			'type' 	=> 'number',
			'pk' 	=> $edit->term_id,
			'metadata' 		=> 'meta',
			':disabled'		=> $disabled_attr,
			'remote_url' 	=> admin_url('contract/api/contract/update'),
			':reload_if_success' => "true"
			);

		$_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
		$this->table->add_row($_attrs['title'], $_cell_value);

		$this->table->add_row('Phí áp dụng (%)','<xeditable '._attributes_to_string([
				'name' 			=> 'original_service_fee_rate',
				'value' 		=> ((double) get_term_meta_value($edit->term_id, 'original_service_fee_rate')*100),
				'title' 		=> 'Phí dịch vụ',
				'type' 			=> 'text',
				'pk' 			=> $edit->term_id,
				'metadata' 		=> 'meta',
				':disabled' 	=> $disabled_attr,
				'remote_url' 	=> admin_url('contract/api/contract/update'),
				':reload_if_success' => "true"
			]).'></xeditable>');

        if('range' == get_term_meta_value($edit->term_id, 'service_fee_payment_type')){
            $this->table->add_row('Phí thực tế (%)','<xeditable '._attributes_to_string([
                'name' 			=> 'service_fee_rate_actual',
                'value' 		=> ((double) get_term_meta_value($edit->term_id, 'service_fee_rate_actual')*100),
                'title' 		=> 'Phí thực tế (%)',
                'type' 			=> 'text',
                'pk' 			=> $edit->term_id,
                'metadata' 		=> 'meta',
                ':disabled' 	=> true,
                'remote_url' 	=> admin_url('contract/api/contract/update'),
                ':reload_if_success' => "true"
            ]).'></xeditable>');
        }

		$_attrs = array(
			'name' 			=> 'service_fee',
			'value' 		=> (int) get_term_meta_value($edit->term_id, 'service_fee'),
			'title' 		=> 'Phí dịch vụ',
			'type' 			=> 'number',
			'pk' 			=> $edit->term_id,
			'metadata' 		=> 'meta',
			':disabled'		=> 'true',
			'remote_url' 	=> admin_url('contract/api/contract/update'),
			':reload_if_success' => "true"
		);

		$_cell_value = '<xeditable'._attributes_to_string($_attrs).'></xeditable>';
		$this->table->add_row($_attrs['title'], $_cell_value);

        $this->load->model('option_m');
        $exchange_rates = $this->option_m->get_value('exchange_rate', TRUE);
        foreach($exchange_rates as $currency => $exchange_rate){
            $meta_key = 'exchange_rate_' . strtolower($currency);
        
            $meta_value = (double) get_term_meta_value($edit->term_id, $meta_key) ?? 0;
            if(!empty($meta_value)) {
                $regex = '/^([a-z]{3})_to_[a-z]{3}$/';
                preg_match($regex, $currency, $currency);
                $currency = strtoupper($currency[1]);

                $_attrs = [
                    'name' 	=> $meta_key,
                    'value' => (float) $meta_value,
                    'title' => "Tỷ giá 1 {$currency}",
                    'type' 	=> 'number',
                    'pk' 	=> $edit->term_id,
                    'metadata' 		=> 'meta',
                    ':disabled'		=> $disabled_attr,
                    'remote_url' 	=> admin_url('contract/api/contract/update'),
                    ':reload_if_success' => "true"
                ];
                
                $_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
                $this->table->add_row($_attrs['title'], $_cell_value);
            }
        }

        $usd_to_vnd = (double) get_term_meta_value($edit->term_id, 'exchange_rate_usd_to_vnd') ?? 0;
        if(empty($usd_to_vnd)){
            $_attrs = [
                'name' 	=> 'exchange_rate_usd_to_vnd',
                'value' => (float) $usd_to_vnd,
                'title' => "Tỷ giá 1 USD",
                'type' 	=> 'number',
                'pk' 	=> $edit->term_id,
                'metadata' 		=> 'meta',
                ':disabled'		=> $disabled_attr,
                'remote_url' 	=> admin_url('contract/api/contract/update'),
                ':reload_if_success' => "true"
            ];
            
            $_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
            $this->table->add_row($_attrs['title'], $_cell_value);
        }

        $aud_to_vnd = (double) get_term_meta_value($edit->term_id, 'exchange_rate_aud_to_vnd') ?? 0;
        if(empty($aud_to_vnd)){
            $_attrs = [
                'name' 	=> 'exchange_rate_aud_to_vnd',
                'value' => (float) $aud_to_vnd,
                'title' => "Tỷ giá 1 AUD",
                'type' 	=> 'number',
                'pk' 	=> $edit->term_id,
                'metadata' 		=> 'meta',
                ':disabled'		=> $disabled_attr,
                'remote_url' 	=> admin_url('contract/api/contract/update'),
                ':reload_if_success' => "true"
            ];
            
            $_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
            $this->table->add_row($_attrs['title'], $_cell_value);
        }

		$this->table->add_row('Phí nhà thầu khách chịu (%)','<xeditable '._attributes_to_string(['name'=>'fct','value'=>get_term_meta_value($edit->term_id, 'fct'),'title'=>'Phí nhà thầu khách chịu (%)','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'5 %','name'=>5], ['text'=>'0 %','name'=>0]))))]).'></xeditable>');

		$_attrs = array(
			'name' 	=> 'link_fanpage_facebook',
			'value' => get_term_meta_value($edit->term_id, 'link_fanpage_facebook'),
			'title' => 'Link fanpage facebook',
			'type' 	=> 'text',
			'pk' 	=> $edit->term_id,
			'metadata' 		=> 'meta',
			':disabled'		=> $disabled_attr,
			'remote_url' 	=> admin_url('contract/api/contract/update')
			);

		$_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
		$this->table->add_row($_attrs['title'], $_cell_value);

		$this->table->add_row('Ngày chạy QC','<xeditable '._attributes_to_string(['name'=>'advertise_start_time','value'=>my_date(get_term_meta_value($edit->term_id,'advertise_start_time'), 'd-m-Y'),'title'=>'Ngày chạy QC','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

		$this->table->add_row('Ngày kết thúc QC','<xeditable '._attributes_to_string(['name'=>'advertise_end_time','value'=>my_date(get_term_meta_value($edit->term_id,'advertise_end_time'), 'd-m-Y'),'title'=>'Ngày kết thúc QC','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

		break ;

	case 'tiktok-ads' :

		$this->table->add_row('Ẩn/Hiện CTKM trong bản In','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'is_display_promotions_discount','value'=> (int) get_term_meta_value($edit->term_id, 'is_display_promotions_discount'),'title'=>'Ẩn/Hiện CTKM trong bản In','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'Hiện', 'name'=> 1], ['text'=>'Ẩn', 'name'=> 0]))))]).'></xeditable>');

		$this->config->load('facebookads/contract');

		$isAccountForRent = (int) get_term_meta_value($edit->term_id, 'isAccountForRent');

		$this->table->add_row('Hình thức quản lý', $isAccountForRent ? 'Cho thuê tài khoản' : 'Quản lý theo tài khoản');

		$this->table->add_row('Loại hình dịch vụ','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'isAccountForRent','value'=> (int) get_term_meta_value($edit->term_id, 'isAccountForRent'),'title'=>'Loại hình dịch vụ','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'Mặc định', 'name'=> 0], ['text'=>'Cho thuê tài khoản', 'name'=> 1]))))]).'></xeditable>');

		$this->config->load('googleads/contract');

		$contract_budget_payment_types = $this->config->item('enums', 'contract_budget_payment_types');
		$this->table->add_row('Phương thức thanh toán NSQC','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'contract_budget_payment_type','value'=>get_term_meta_value($edit->term_id, 'contract_budget_payment_type'),'title'=>'Phương thức thanh toán ngân sách quảng cáo','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($contract_budget_payment_types) , $contract_budget_payment_types)))]).'></xeditable>');

		$contract_budget_customer_payment_types = $this->config->item('enums', 'contract_budget_customer_payment_types');
		$this->table->add_row('Tùy chọn thu & chi','<xeditable '._attributes_to_string(['name'=>'contract_budget_customer_payment_type','value'=>get_term_meta_value($edit->term_id, 'contract_budget_customer_payment_type'),'title'=>'Tùy chọn thu & chi','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($contract_budget_customer_payment_types) , $contract_budget_customer_payment_types)))]).'></xeditable>');

		$service_fee_payment_type_def = $this->config->item('service_fee_payment_type');
		$this->table->add_row('Phương thức thanh toán phí dịch vụ','<xeditable '._attributes_to_string(['name'=>'service_fee_payment_type','value'=>get_term_meta_value($edit->term_id, 'service_fee_payment_type'),'title'=>'Phương thức thanh toán phí dịch vụ','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($service_fee_payment_type_def) , $service_fee_payment_type_def)))]).'></xeditable>');

		$this->table
		->add_row('Số tiền khách cọc','<xeditable '._attributes_to_string([
			'name' 			=> 'deposit_amount',
			'value' 		=> get_term_meta_value($edit->term_id, 'deposit_amount'),
			'title' 		=> 'Số tiền khách cọc',
			'type' 			=> 'number',
			'pk' 			=> $edit->term_id,
			'metadata' 		=> 'meta',
			':disabled' 	=> $disabled_attr,
			'remote_url' 	=> admin_url('contract/api/contract/update')
		]).'></xeditable>');

		$_attrs = array(
			'name' 	=> 'contract_budget',
			'value' => (int) get_term_meta_value($edit->term_id, 'contract_budget'),
			'title' => 'Ngân sách chạy quảng cáo',
			'type' 	=> 'number',
			'pk' 	=> $edit->term_id,
			'metadata' 		=> 'meta',
			':disabled'		=> $disabled_attr,
			'remote_url' 	=> admin_url('contract/api/contract/update')
			);

		$_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
		$this->table->add_row($_attrs['title'], $_cell_value);


		$_attrs = array(
			'name' 	=> 'service_fee',
			'value' => (int) get_term_meta_value($edit->term_id, 'service_fee'),
			'title' => 'Phí dịch vụ',
			'type' 	=> 'number',
			'pk' 	=> $edit->term_id,
			'metadata' 		=> 'meta',
			':disabled'		=> $disabled_attr,
			'remote_url' 	=> admin_url('contract/api/contract/update')
			);

		$_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
		$this->table->add_row($_attrs['title'], $_cell_value);

        $this->load->model('option_m');
        $exchange_rates = $this->option_m->get_value('exchange_rate', TRUE);
        foreach($exchange_rates as $currency => $exchange_rate){
            $meta_key = 'exchange_rate_' . strtolower($currency);
        
            $meta_value = (double) get_term_meta_value($edit->term_id, $meta_key) ?? 0;
            if(!empty($meta_value)) {
                $regex = '/^([a-z]{3})_to_[a-z]{3}$/';
                preg_match($regex, $currency, $currency);
                $currency = strtoupper($currency[1]);

                $_attrs = [
                    'name' 	=> $meta_key,
                    'value' => (float) $meta_value,
                    'title' => "Tỷ giá 1 {$currency}",
                    'type' 	=> 'number',
                    'pk' 	=> $edit->term_id,
                    'metadata' 		=> 'meta',
                    ':disabled'		=> $disabled_attr,
                    'remote_url' 	=> admin_url('contract/api/contract/update'),
                    ':reload_if_success' => "true"
                ];
                
                $_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
                $this->table->add_row($_attrs['title'], $_cell_value);
            }
        }

        $usd_to_vnd = (double) get_term_meta_value($edit->term_id, 'exchange_rate_usd_to_vnd') ?? 0;
        if(empty($usd_to_vnd)){
            $_attrs = [
                'name' 	=> 'exchange_rate_usd_to_vnd',
                'value' => (float) $usd_to_vnd,
                'title' => "Tỷ giá 1 USD",
                'type' 	=> 'number',
                'pk' 	=> $edit->term_id,
                'metadata' 		=> 'meta',
                ':disabled'		=> $disabled_attr,
                'remote_url' 	=> admin_url('contract/api/contract/update'),
                ':reload_if_success' => "true"
            ];
            
            $_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
            $this->table->add_row($_attrs['title'], $_cell_value);
        }
        
        $aud_to_vnd = (double) get_term_meta_value($edit->term_id, 'exchange_rate_aud_to_vnd') ?? 0;
        if(empty($aud_to_vnd)){
            $_attrs = [
                'name' 	=> 'exchange_rate_aud_to_vnd',
                'value' => (float) $aud_to_vnd,
                'title' => "Tỷ giá 1 AUD",
                'type' 	=> 'number',
                'pk' 	=> $edit->term_id,
                'metadata' 		=> 'meta',
                ':disabled'		=> $disabled_attr,
                'remote_url' 	=> admin_url('contract/api/contract/update'),
                ':reload_if_success' => "true"
            ];
            
            $_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
            $this->table->add_row($_attrs['title'], $_cell_value);
        }

		$this->table->add_row('Phí nhà thầu khách chịu (%)','<xeditable '._attributes_to_string(['name'=>'fct','value'=>get_term_meta_value($edit->term_id, 'fct'),'title'=>'Phí nhà thầu khách chịu (%)','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'5 %','name'=>5], ['text'=>'0 %','name'=>0]))))]).'></xeditable>');

		$_attrs = array(
			'name' 	=> 'link_fanpage_facebook',
			'value' => get_term_meta_value($edit->term_id, 'link_fanpage_facebook'),
			'title' => 'Link fanpage facebook',
			'type' 	=> 'text',
			'pk' 	=> $edit->term_id,
			'metadata' 		=> 'meta',
			':disabled'		=> $disabled_attr,
			'remote_url' 	=> admin_url('contract/api/contract/update')
			);

		$_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
		$this->table->add_row($_attrs['title'], $_cell_value);

		$this->table->add_row('Ngày chạy QC','<xeditable '._attributes_to_string(['name'=>'advertise_start_time','value'=>my_date(get_term_meta_value($edit->term_id,'advertise_start_time'), 'd-m-Y'),'title'=>'Ngày chạy QC','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

		$this->table->add_row('Ngày kết thúc QC','<xeditable '._attributes_to_string(['name'=>'advertise_end_time','value'=>my_date(get_term_meta_value($edit->term_id,'advertise_end_time'), 'd-m-Y'),'title'=>'Ngày kết thúc QC','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

		break ;

    case 'zalo-ads' :

            $this->table->add_row('Ẩn/Hiện CTKM trong bản In','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'is_display_promotions_discount','value'=> (int) get_term_meta_value($edit->term_id, 'is_display_promotions_discount'),'title'=>'Ẩn/Hiện CTKM trong bản In','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'Hiện', 'name'=> 1], ['text'=>'Ẩn', 'name'=> 0]))))]).'></xeditable>');
    
            $this->config->load('facebookads/contract');
    
            $isAccountForRent = (int) get_term_meta_value($edit->term_id, 'isAccountForRent');
    
            $this->table->add_row('Hình thức quản lý', $isAccountForRent ? 'Cho thuê tài khoản' : 'Quản lý theo tài khoản');
    
            $this->table->add_row('Loại hình dịch vụ','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'isAccountForRent','value'=> (int) get_term_meta_value($edit->term_id, 'isAccountForRent'),'title'=>'Loại hình dịch vụ','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($x){ return ['value'=>$x['name'],'text'=>$x['text']]; }, array(['text'=>'Mặc định', 'name'=> 0], ['text'=>'Cho thuê tài khoản', 'name'=> 1]))))]).'></xeditable>');
    
            $this->config->load('googleads/contract');
    
            $contract_budget_payment_types = $this->config->item('enums', 'contract_budget_payment_types');
            $this->table->add_row('Phương thức thanh toán NSQC','<xeditable :reload_if_success="true" '._attributes_to_string(['name'=>'contract_budget_payment_type','value'=>get_term_meta_value($edit->term_id, 'contract_budget_payment_type'),'title'=>'Phương thức thanh toán ngân sách quảng cáo','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($contract_budget_payment_types) , $contract_budget_payment_types)))]).'></xeditable>');
    
            $contract_budget_customer_payment_types = $this->config->item('enums', 'contract_budget_customer_payment_types');
            $this->table->add_row('Tùy chọn thu & chi','<xeditable '._attributes_to_string(['name'=>'contract_budget_customer_payment_type','value'=>get_term_meta_value($edit->term_id, 'contract_budget_customer_payment_type'),'title'=>'Tùy chọn thu & chi','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($contract_budget_customer_payment_types) , $contract_budget_customer_payment_types)))]).'></xeditable>');
    
            $service_fee_payment_type_def = $this->config->item('service_fee_payment_type');
            $this->table->add_row('Phương thức thanh toán phí dịch vụ','<xeditable '._attributes_to_string(['name'=>'service_fee_payment_type','value'=>get_term_meta_value($edit->term_id, 'service_fee_payment_type'),'title'=>'Phương thức thanh toán phí dịch vụ','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($service_fee_payment_type_def) , $service_fee_payment_type_def)))]).'></xeditable>');
    
            $this->table
            ->add_row('Số tiền khách cọc','<xeditable '._attributes_to_string([
                'name' 			=> 'deposit_amount',
                'value' 		=> get_term_meta_value($edit->term_id, 'deposit_amount'),
                'title' 		=> 'Số tiền khách cọc',
                'type' 			=> 'number',
                'pk' 			=> $edit->term_id,
                'metadata' 		=> 'meta',
                ':disabled' 	=> $disabled_attr,
                'remote_url' 	=> admin_url('contract/api/contract/update')
            ]).'></xeditable>');
    
            $_attrs = array(
                'name' 	=> 'contract_budget',
                'value' => (int) get_term_meta_value($edit->term_id, 'contract_budget'),
                'title' => 'Ngân sách chạy quảng cáo',
                'type' 	=> 'number',
                'pk' 	=> $edit->term_id,
                'metadata' 		=> 'meta',
                ':disabled'		=> $disabled_attr,
                'remote_url' 	=> admin_url('contract/api/contract/update')
                );
    
            $_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
            $this->table->add_row($_attrs['title'], $_cell_value);
    
    
            $_attrs = array(
                'name' 	=> 'service_fee',
                'value' => (int) get_term_meta_value($edit->term_id, 'service_fee'),
                'title' => 'Phí dịch vụ',
                'type' 	=> 'number',
                'pk' 	=> $edit->term_id,
                'metadata' 		=> 'meta',
                ':disabled'		=> $disabled_attr,
                'remote_url' 	=> admin_url('contract/api/contract/update')
                );
    
            $_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
            $this->table->add_row($_attrs['title'], $_cell_value);
    
            $this->table->add_row('Ngày chạy QC','<xeditable '._attributes_to_string(['name'=>'advertise_start_time','value'=>my_date(get_term_meta_value($edit->term_id,'advertise_start_time'), 'd-m-Y'),'title'=>'Ngày chạy QC','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');
    
            $this->table->add_row('Ngày kết thúc QC','<xeditable '._attributes_to_string(['name'=>'advertise_end_time','value'=>my_date(get_term_meta_value($edit->term_id,'advertise_end_time'), 'd-m-Y'),'title'=>'Ngày kết thúc QC','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');
    
            break ;

	case 'weboptimize' :

		$_attrs = array(
			'name' 	=> "contract_price_weboptimize",
			'value' => (int) (get_term_meta_value($edit->term_id, 'contract_price_weboptimize') ?: 0),
			'title' => 'Phí tối ưu Website',
			'type' 	=> 'number',
			'pk' 	=> $edit->term_id,
			'metadata' 		=> 'meta',
			':disabled'		=> $disabled_attr,
			'remote_url' 	=> admin_url('contract/api/contract/update')
			);

		$_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
		$this->table->add_row($_attrs['title'], $_cell_value);

		$_attrs = array(
			'name' 	=> "score_commitment_mobile_weboptimize",
			'value' => (int) (get_term_meta_value($edit->term_id, 'score_commitment_mobile_weboptimize') ?: 0),
			'title' => 'Điểm cam kết tối ưu trên mobile',
			'type' 	=> 'number',
			'pk' 	=> $edit->term_id,
			'metadata' 		=> 'meta',
			':disabled'		=> $disabled_attr,
			'remote_url' 	=> admin_url('contract/api/contract/update')
			);

		$_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
		$this->table->add_row($_attrs['title'], $_cell_value);

		$_attrs = array(
			'name' 	=> 'score_commitment_desktop_weboptimize',
			'value' => (int) (get_term_meta_value($edit->term_id, 'score_commitment_desktop_weboptimize') ?: 0),
			'title' => 'Điểm cam kết tối ưu trên desktop',
			'type' 	=> 'number',
			'pk' 	=> $edit->term_id,
			'metadata' 		=> 'meta',  
			':disabled'		=> $disabled_attr,
			'remote_url' 	=> admin_url('contract/api/contract/update')
			);

		$_cell_value = '<xeditable '._attributes_to_string($_attrs).'></xeditable>';
		$this->table->add_row($_attrs['title'], $_cell_value);
		break ;

	case 'webgeneral':
		$this->load->model('webgeneral/webgeneral_config_m');
		$package_name = $this->webgeneral_config_m->get_package_name($edit->term_id);
		$this->table->add_row(array('Gói sử dụng: ',$package_name));
		break;

	case 'onead':
		$this->config->load('onead/onead');

		$service_packages = array_map(function($x){return $x['label'];}, $this->config->item('service','packages')); // TÊN CÁC GÓI DOMAIN : AJAX
		$this->table->add_row('Gói dịch vụ','<xeditable '._attributes_to_string(['name'=>'service_package','value'=>get_term_meta_value($edit->term_id, 'service_package'),'title'=>'Gói dịch vụ','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($service_packages) , $service_packages)))]).'></xeditable>');

		$this->table->add_row('Đơn giá','<xeditable '._attributes_to_string(['name'=>'price','value'=>get_term_meta_value($edit->term_id, 'price'),'title'=>'Đơn giá','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

		$this->table->add_row('Số lượng','<xeditable '._attributes_to_string(['name'=>'quantity','value'=>get_term_meta_value($edit->term_id, 'quantity'),'title'=>'Số lượng TK','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

		$this->table->add_row('Số tiền giảm giá','<xeditable '._attributes_to_string(['name'=>'discount_amount','value'=>get_term_meta_value($edit->term_id, 'discount_amount'),'title'=>'Số tiền giảm giá','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

		break;

	case 'domain':

		$this->config->load('domain/domain');


		$domain_package_default = array_map(function($x){return $x['label'];}, $this->config->item('service','packages')); // TÊN CÁC GÓI DOMAIN : AJAX

		$this->table->add_row('Gói domain','<xeditable '._attributes_to_string(['name'=>'domain_service_package','value'=>get_term_meta_value($edit->term_id, 'domain_service_package'),'title'=>'Gói domain','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($domain_package_default) , $domain_package_default)))]).'></xeditable>');

		$this->table->add_row('Số năm đăng ký','<xeditable '._attributes_to_string(['name'=>'domain_number_year_register','value'=>get_term_meta_value($edit->term_id, 'domain_number_year_register'),'title'=>'Số năm đăng ký','type'=>'select','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update'),'datasource'=>htmlspecialchars(json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},array_keys($this->config->item('number_year_register')) , $this->config->item('number_year_register'))))]).'></xeditable>');

		$this->table->add_row('Phí khởi tạo','<xeditable '._attributes_to_string(['name'=>'domain_fee_initialization','value'=>get_term_meta_value($edit->term_id, 'domain_fee_initialization'),'title'=>'Phí khởi tạo','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

		$this->table->add_row('Phí duy trì hàng năm','<xeditable '._attributes_to_string(['name'=>'domain_fee_maintain','value'=>get_term_meta_value($edit->term_id, 'domain_fee_maintain'),'title'=>'Phí duy trì hàng năm','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');



		$services = $this->config->item('service', 'packages') ; // THÔNG TIN CÁC GÓI DOMAIN
		$domain_service_package = get_term_meta_value($edit->term_id,'domain_service_package'); // GÓI DOMAIN ĐƯỢC CHỌN
		$has_domain_renew  		= get_term_meta_value($edit->term_id, 'has_domain_renew') ; // KIỂM TRA ĐÃ GIA HẠN HAY CHƯA

		$domain_fee_initialization  = get_term_meta_value($edit->term_id, 'domain_fee_initialization') ?: $services[$domain_service_package]['fee_initialization']; // PHÍ KHỞI TẠO
		$domain_fee_maintain = get_term_meta_value($edit->term_id, 'domain_fee_maintain') ?: $services[$domain_service_package]['fee_maintain']; // PHÍ DUY TRÌ

		$domain_fee_initialization_maintain = get_term_meta_value($edit->term_id, 'domain_fee_initialization_maintain') ?: $domain_fee_maintain + $domain_fee_initialization; // PHÍ KHỞI TẠO VÀ DUY TRÌ HÀNG NĂM	

		// SỐ NĂM ĐĂNG KÝ
		$domain_number_year_register = get_term_meta_value($edit->term_id, 'domain_number_year_register') ?: 1 ;
		$domain_number_year_register_default = array_map(function($x){ return $x ;}, $this->config->item('number_year_register'));

		$domain_fee_maintain_years = $domain_fee_maintain * ($domain_number_year_register - 1) ;
		if($has_domain_renew == 1) $domain_fee_maintain_years =  $domain_fee_maintain * $domain_number_year_register;

		if( ! $has_domain_renew)
		{
			$this->table->add_row('Phí khởi tạo và duy trì năm đầu tiên','<xeditable '._attributes_to_string(['name'=>'domain_fee_initialization_maintain','value'=>get_term_meta_value($edit->term_id, 'domain_fee_initialization_maintain'),'title'=>'Phí khởi tạo và duy trì năm đầu tiên','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');
		}

		$this->table->add_row('Phí duy trì năm tiếp theo','<xeditable '._attributes_to_string(['name'=>'domain_fee_maintain_years','value'=>$domain_fee_maintain_years,'title'=>'Phí duy trì năm tiếp theo','type'=>'number','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

		break;
	
	case 'banner':
		$this->table->add_row('Mô tả Banner','<xeditable '._attributes_to_string(['name'=>'description_banner','value'=>get_term_meta_value($edit->term_id, 'description_banner'),'title'=>'Mô tả Banner','type'=>'textarea','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');		
		break;

	case 'courseads': 
		$_attrs = array(
			'title' => 'Link Hubspot',
			'name' 	=> "hubspot_link",
			'value' => get_term_meta_value($edit->term_id, 'hubspot_link'),
            'type' 	=> 'text',
			'pk' 	=> $edit->term_id,
			'metadata' 		=> 'meta',
			':disabled'		=> $disabled_attr,
			'remote_url' 	=> admin_url('contract/api/contract/update')
			);

		$_cell_value = '<xeditable '. _attributes_to_string($_attrs).'></xeditable>';
		$this->table->add_row($_attrs['title'], $_cell_value);
		break;

	default: break;
}
// HIỂN THỊ THỜI GIAN CỦA CÁC LOẠI HỢP ĐỒNG
if(in_array($edit->term_type, array('webgeneral','google-ads', 'facebook-ads')))
{
	$_disabled_attr = $disabled_attr == 'true' ? 'true' : (in_array($this->admin_m->role_id, array(1,5)) ? 'true' : 'false');

	if (in_array($edit->term_type, array('webgeneral','google-ads')))
	{
		$this->table->add_row('Ngày chạy dịch vụ','<xeditable '._attributes_to_string(['name'=>'started_service','value'=>my_date(get_term_meta_value($edit->term_id,'started_service'), 'd-m-Y'),'title'=>'Ngày chạy dịch vụ','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$_disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');
	}

	$this->table->add_row('Ngày thực hiện dịch vụ','<xeditable '._attributes_to_string(['name'=>'start_service_time','value'=>my_date(get_term_meta_value($edit->term_id,'start_service_time'), 'd-m-Y'),'title'=>'Ngày thực hiện dịch vụ','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$_disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');

	if($edit->term_type == 'webgeneral' || $edit->term_type == 'facebook-ads')
	{
		$this->table->add_row('Ngày kết thúc dự kiến','<xeditable '._attributes_to_string(['name'=>'end_service_time','value'=>my_date(get_term_meta_value($edit->term_id,'end_service_time'), 'd-m-Y'),'title'=>'Ngày kết thúc dự kiến','type'=>'combodate','pk'=>$edit->term_id,'metadata'=>'meta',':disabled'=>$_disabled_attr,'remote_url'=>admin_url('contract/api/contract/update')]).'></xeditable>');
	}
}
?>
<div class="col-md-6">
	<?php echo $this->table->generate(); ?>
</div>


<script type="text/javascript">

	function reload_page(data){ location.href = admin_url + "contract/edit/<?php echo $edit->term_id; ?>"; }

	$(function(){

		var incline_childs = $("#incline-data").find('.input-group');

		$.each(incline_childs , function(i,e){

			var el_dom = $(e).children();

			$(e).replaceWith(el_dom);
		});

		$("#contract-inputfile").fileinput({
			uploadUrl: admin_url + 'contract/ajax_dipatcher/act_upload_real_file',
			uploadAsync: true,
			maxFileCount: 5,
			showPreview: false,
			showRemove: false,
			uploadExtraData: {
				term_id: "<?php echo $edit->term_id;?>",
				term_name: "<?php echo $edit->term_name;?>",
			},
			allowedFileExtensions: ['docx', 'doc', 'xls', 'xlsx'],
		})
		.on('fileuploaded', function(event, data, previewId, index) {

			var response = data.response

			if(response.response != 'undefined' 
				&& response.response.success == true){

				$.each(response.response.ret_obj,function(i, e){
					$("#grid_files").append('<a href="<?php echo base_url();?>' + e.file_path + '">' + e.file_name + '</a><br/>');
				});
		}
	});
	})

	var module_url = "<?php echo module_url();?>";
	$(function(){ $('[data-toggle=confirmation]').confirmation(); });

</script>

<?php if ($is_system && !$readonly): ?>	
	<!-- change customer owner modal -->
	<div class="modal" id="change-customer-owner-modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Thông tin khách hàng sử dụng dịch vụ</h4>
				</div>
				<?php
				echo $this->admin_form->form_open();
				?>
				<div class="modal-body">
					<div class="col-md-12">
						<?php 
						$edit_lock = FALSE;

						echo
						form_hidden('edit[term_id]',$edit->term_id),
						form_hidden('user_updated',0);

						$customers = prepare_dropdown($customers,'Chọn');

						$cus_belongs = '' ;		  
						if(!$edit_lock)
						{	
							echo $this->admin_form->dropdown('Chọn khách hàng', 'edit[user_id]', $customers, $cus_belongs,'',array('id'=>'chosen_customer'));
						}
						else{ 
							$attrs = array('id'=>'chosen_customer','disabled'=>'disabled');
							if(empty($cus_belongs)) unset($attrs['disabled']);
							echo $this->admin_form->dropdown('Chọn khách hàng', 'edit[user_id]', $customers, $cus_belongs,'', $attrs);
						}

						if(!$is_system && (!$edit_lock || empty($cus_belongs))){

							echo 
							$this->admin_form->formGroup_begin(0,'Hoặc'),
							'<div class="btn-group">
							<button type="button" class="btn btn-default">Tạo mới khách hàng</button>
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#create_person_customer" data-toggle="modal">Cá nhân</a></li>
								<li><a href="#create_company_customer" data-toggle="modal">Tổ chức</a></li>
							</ul>
						</div>',
						$this->admin_form->formGroup_end();          
					}

					echo $this->admin_form->submit('','confirm_step_customer','confirm_step_customer','', array('style'=>'display:none;','id'=>'confirm_step_customer'));
					?>

					<div id="customer_profile"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				<?php 
				echo form_submit(array('type'=>'submit','class'=>'btn btn-primary','name'=>'update_customer'),'Cập nhật khách hàng');
				?>
			</div>
			<?php 
			echo $this->admin_form->form_close();
			?>
		</div>
	</div>
</div>
<div class="modal" id="create_person_customer" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Tạo mới khách hàng Cá nhân</h4>
				</div>
				<?php
				echo $this->admin_form->form_open();
				?>
				<div class="modal-body">
					<?php 
					echo $this->load->view('customer/admin/edit_customer_person',array('user_type'=>'customer_person'));
					?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
					<?php 
					echo form_submit(array('type'=>'button','class'=>'btn btn-primary create_customer','name'=>'create_customer', 'data-dismiss'=>'modal'),'Tạo mới tài khoản');
					?>
				</div>
				<?php 
				echo $this->admin_form->form_close();
				?>
			</div>
		</div>
	</div>
	<div class="modal" id="create_company_customer" data-backdrop="static">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span></button>
						<h4 class="modal-title">Tạo mới khách hàng Doanh nghiệp</h4>
					</div>
					<?php
					echo $this->admin_form->form_open();
					?>
					<div class="modal-body">
						<?php echo $this->load->view('customer/admin/edit_customer_company',array('user_type'=>'customer_company'));?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
						<?php 
						echo form_submit(array('type'=>'button','class'=>'btn btn-primary create_customer','name'=>'create_customer','data-dismiss'=>"modal"),'Tạo mới tài khoản');
						?>
					</div>
					<?php 
					echo $this->admin_form->form_close();
					?>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- end change customer owner modal -->
		<script type="text/javascript">

			function load_customer_profile(cus_id){
				$.notify('Đang xử lý ...','info');
				$("#customer_profile").empty();
				if(cus_id > 0)
					$.post( module_url + 'render_customer_info/' + cus_id, {}, function( resp ) { $("#customer_profile").append(resp.content);}, "json");
			}

			$(function(){

				$("#chosen_customer").on("change", function(e) {
					var form = $(this).closest('form');
					form.find("input[name='user_updated']").val(1);
					load_customer_profile($(this).val());
				});

				$("#create_person_customer")
				.find('form')
				.validate({  
					rules: {
						'edit[meta][customer_email]': {
							email: true,
						},
						'edit[meta][customer_phone]': {
							minlength: 3,
							digits: true,
						}
					},
					messages:{
						'edit[meta][customer_email]': {
							email: 'Kiểu dữ liệu không hợp lệ',
						},
						'edit[meta][customer_phone]': {
							digits: 'Kiểu dữ liệu không hợp lệ , số điện thoại phải là kiểu số',
						}
					}
				});

				$("#create_company_customer")
				.find('form')
				.validate({  
					rules: {
						'edit[meta][customer_email]': {
							email: true,
						},
						'edit[meta][customer_phone]': {
							minlength: 3,
							digits: true,
						}
					},
					messages:{
						'edit[meta][customer_email]': {
							email: 'Kiểu dữ liệu không hợp lệ',
						},
						'edit[meta][customer_phone]': {
							digits: 'Kiểu dữ liệu không hợp lệ , số điện thoại phải là kiểu số',
						},
					}
				});

				$(".create_customer").click(function(){

					var parent_form = $(this).closest('.modal-content').find('form');
					var $valid = parent_form.valid();
					if(!$valid) return false;

					$.post(
						module_url + 'ajax_dipatcher/act_create_customer', 
						parent_form.serializeArray(),
						function(data)
						{
							if(data.response != 'undefined')
							{
								var datasource = $("#chosen_customer").select2('data');
								datasource.push({id : data.response.id, text:data.response.text});
								$("#chosen_customer").select2({data : datasource,});
								$("#chosen_customer").val(data.response.id).trigger("change");
							}
						},
						"json");
				});

			});
		</script>
	<?php endif ?>

<script type="text/javascript">
$(function(){
    $('[data-toggle=confirmation]').confirmation();
});
</script>