<?php 
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.date.extensions.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.extensions.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.numeric.extensions.js');

$auto_identity = uniqid('receipt-edit-view');
echo $this->admin_form->form_open(module_url("ajax/receipt/edit/{$edit->post_id}/{$term_id}"),['id'=>$auto_identity],['post_id'=>$edit->post_id]);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="crud-receipt-modal">Danh sách đã thanh toán</h4>
</div>

<div class="modal-body row">
<?php

$status = $this->config->item('status','receipt');
$transfer_type = $this->config->item('transfer_type','receipt');
$transfer_account = $this->config->item('transfer_account','receipt');

echo $this->admin_form->input('Ghi chú','edit[post_title]',$edit->post_title);

echo $this->admin_form->input_numberic('Số tiền','meta[amount]',(int)get_post_meta_value($edit->post_id,'amount'),'',['class'=>'inputmark']);


echo $this->admin_form->dropdown('Hình thức','meta[transfer_type]',$transfer_type,get_post_meta_value($edit->post_id,'transfer_type'),'',['id'=>'transfer_type']);
echo $this->admin_form->dropdown('Tài khoản','meta[transfer_account]',$transfer_account,get_post_meta_value($edit->post_id,'transfer_account'),'',['id'=>'transfer_account']);

echo $this->admin_form->dropdown('Loại','edit[post_type]', $post_type_enums,$edit->post_type,'',['id'=>'rec_type']);

echo $this->admin_form->dropdown('Trạng thái','edit[post_status]',($status[$edit->post_type]??[]),$edit->post_status,'',['id'=>'rec_status']);

echo $this->admin_form->input('Ngày thanh toán/ Hạn bảo lãnh','edit[end_date]', my_date($edit->end_date,'Y/m/d'),'', array('class'=>'set-datepicker'));

echo '<div class="clearfix"></div>';
echo $this->admin_form->formGroup_begin('is_order_printed','Đã xuất hóa đơn');
echo form_checkbox('meta[is_order_printed]',1,!empty(get_post_meta_value($edit->post_id,'is_order_printed')),'class="minimal"');
echo $this->admin_form->formGroup_end();

?>
</div>

<div class="modal-footer">	
<?php 
echo form_button(['class'=>'btn btn-primary','name'=>'submit','id'=>"submit-{$auto_identity}"],'Lưu thay đổi');
echo form_button('close','Close',['class'=>'btn btn-default','data-dismiss'=>'modal']);
?>
</div>

<?php echo $this->admin_form->form_close();?>
<script type="text/javascript">

var caution_status,payment_status;
<?php
if(!empty($status['receipt_caution']))
{
	$stack_tmp = array();
	foreach ($status['receipt_caution'] as $key => $value)
	{
		$stack_tmp[] = '{id:"'.$key.'",text:"'.$value.'"}';
	}
	$stack_tmp = implode(',', $stack_tmp);

	echo "caution_status = [{$stack_tmp}]";
}
?>

<?php
if(!empty($status['receipt_payment']))
{
	$stack_tmp = array();
	foreach ($status['receipt_payment'] as $key => $value)
	{
		$stack_tmp[] = '{id:"'.$key.'",text:"'.$value.'"}';
	}
	$stack_tmp = implode(',', $stack_tmp);

	echo "payment_status = [{$stack_tmp}]";
}
?>

$(function(){

	$('input[type="checkbox"].minimal, input[type="radio"].minimal')
	.iCheck('destroy')
	.iCheck({checkboxClass: 'icheckbox_minimal-blue',radioClass: 'iradio_minimal-blue'});

  	$("#transfer_type, #transfer_account, #rec_status").select2();
	$('.set-datepicker').datepicker({format: 'yyyy-mm-dd',todayHighlight: true,autoclose: true});

	$("#transfer_type").on('change',function(){

		if($(this).val() == 'cash')
		{
			$("#transfer_account").prop('disabled',true);
			$("#transfer_account").val("none").trigger("change");
			return;
		}

		$("#transfer_account").prop('disabled',false);
	});

	$('#rec_type').select2().on('change',function(e){

		switch($(this).val())
		{
			case 'receipt_caution' : 
				$("#rec_status").select2('destroy');
				$("#rec_status").html('');
				$("#rec_status").select2({data: caution_status});
				break;

			default:
				$("#rec_status").select2('destroy');
				$("#rec_status").html('');
				$("#rec_status").select2({data: payment_status});
				break;
		}
		$("form#<?php echo $auto_identity ?> .select2-container").css('width','100%');
	});

	$("form#<?php echo $auto_identity ?> .select2-container").css('width','100%');

	$("button#submit-<?php echo $auto_identity;?>").off('click').on('click',function(e){

		var edit_form = $("#<?php echo $auto_identity;?>");

		$.post( edit_form.attr('action'), edit_form.serializeArray(), function( response ) {

			if( ! response.success)
			{
				$.notify(response.msg, "error");
				return false;
			}
			
			$("#crud-receipt-modal").modal('hide');
			$.notify(response.msg, "info");

			if(response.data.refresh_url != '')
			{
				ajax_load_receipt_list(response.data.refresh_url);
			}

		}, "json");
	});
})
</script>