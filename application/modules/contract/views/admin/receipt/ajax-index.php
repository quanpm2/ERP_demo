<?php
$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
$random_id = uniqid('update-receipt-');
?>

<style>
.progress-group .progress-number { font-size: 0.8em; }
section.content div.row div.box div.box-body {overflow-x: scroll;}
section.content div.row div.box div.box-body table tr td .text-muted { font-size: 0.8em; font-style: italic; }
</style>
<input type="hidden" name="">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php echo $contract_code;?></h4>
  <div id="msg"></div>
</div>
<div class="modal-body col-md-12">
  <div class="col-md-2 control-group form-group">
    <div class="btn-group">
    <?php if (has_permission('contract.receipt.add')): ?>  
    <button href="<?php echo module_url("ajax/receipt/edit/0/{$term_id}");?>" class="btn btn-primary" id="<?php echo $random_id; ?>">Thêm mới</button>
    <?php endif ?>
    </div>
  </div>
  <?php echo ($content['table'] ?? '').($content['pagination'] ?? '');?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

<script type="text/javascript">
  $(document).ready(function() {
      $('.btn-send-mail').off('click').on('click', function(e) {
        e.preventDefault();
        var post_id  = $(this).data('post-id') ;
        var url      = '<?php echo module_url("ajax/report/send_email_successful_payment"); ?>';
        
        $(this).addClass('disabled').find('i').addClass('fa-spin');
        $('#msg').empty() ;

        $.ajax({
            url         : url,
            type        : 'post' ,
            dataType    : 'json' ,
            data        : { post_id : post_id },
        }).done(function(response) {
            $('.receipt-' + post_id).css('display' , 'none') ;
            $(this).css('display' , 'none');
            var status = response.status;

            if(status == 1) {
                var msg = '<div class="callout callout-success"><p>' + response.msg + '</p></div>' ;
            }
            else {
                var msg = '<div class="callout callout-warning"><p>' + response.msg + '</p></div>' ;
            }

            $('#msg').append(msg); 
        });
        
        return false;
      });
  });  

</script>


<script type="text/javascript">
$(function(){

  $(".receipt-edit")
    .off('click')
    .on('click',function(e){
      e.preventDefault();

      $.ajax({
        url: $(this).attr('href'),
        type : 'POST',
        dataType: 'JSON',
        success: function(response){

          if( ! response.success)
          {
            $.notify(response.msg, "error");
            return false;
          }
          
          $("#crud-receipt-modal .modal-content").html($(response.data));
          $("#crud-receipt-modal .modal-content").find('script').each(function($i){
            eval($(this).text());
          });
          $("#crud-receipt-modal").modal('show');
        }
      });

    });

  $("#<?php echo $random_id;?>")
  .off('click')
  .on('click',function(e){
    e.preventDefault();

    $.ajax({
      url: $(this).attr('href'),
      type : 'POST',
      dataType: 'JSON',
      success: function(response){

        if( ! response.success)
        {
          $.notify(response.msg, "error");
          return false;
        }
        
        $("#crud-receipt-modal .modal-content").html($(response.data));

        $("#crud-receipt-modal .modal-content").find('script').each(function($i){
          eval($(this).text());
        });

        $("#crud-receipt-modal").modal('show');
      }
    });
  });    
     
});
</script>