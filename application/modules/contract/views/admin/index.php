<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$this->template->stylesheet->add('modules/contract/css/style.css');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
?>

<div class="row" id="contract-databuilder-container">
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-list-ul"></i> Tất cả hợp đồng</a></li>
                <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-pause"></i> Đang chờ duyệt</a></li>
                <li class="pull-right">
                    <?php 
                        if(has_permission('contract.manipulation_lock.update') 
                        && has_permission('contract.manipulation_lock.manage')):
                    ?> 
                        <div id="contract-manipulation-lock-box" style="display: inline-block">
                            <v-contract-manipulation-lock-box></v-contract-manipulation-lock-box>
                        </div>
                    <?php endif ?>
                    <?php if (has_permission('admin.contract.add')): ?> 
                        <create-contract-component></create-contract-component>
                    <?php endif ?>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <?php echo $this->admin_form->datatable(['remote_url' => admin_url('contract/ajax/dataset'), 'is_download' => TRUE, 'controller' =>'contract']);?>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <?php echo $this->admin_form->datatable(['remote_url' => admin_url('contract/ajax/dataset?&where%5Bterm_status%5D=waitingforapprove'),'is_download' => TRUE, 'controller' =>'contract']);?>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<script type="text/javascript">
    $('[data-toggle=confirmation]').confirmation();

    Vue.component('create-contract-component', { 
        data : function(){
            return {
                admin_url : admin_url
            };
        },
        methods : {
            create_contract : function(){
                window.location.href = admin_url + 'contract/create_wizard';
            },
        },
        template: `
        <button v-on:click="create_contract" class="btn btn-primary btn-flat"><i class="fa fa-fw fa-plus "></i> Hợp đồng</button>
        ` 
    });

</script>

<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/component/ui.js'));  
echo $this->template->trigger_javascript(admin_theme_url('modules/contract/js/app.js'));
echo $this->template->javascript->add(base_url('dist/vContractManipulationLockBox.js?v=20230410232520'));
/* End of file index.php */
/* Location: ./application/modules/googleads/views/admin/index.php */