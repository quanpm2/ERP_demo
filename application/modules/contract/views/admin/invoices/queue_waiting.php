<?php

$this->table->set_caption('Danh sách hóa đơn cần thu trong tháng '.my_date(0,'m'));

if(!empty($invoices)){

	$this->table->add_row(array('#ID','Tiêu đề','Ngày bắt đầu','Ngày kết thúc','Số tiền (vnđ)','Trạng thái','dịch vụ','website','Thời hạn'));

	foreach ($invoices as $invoice) {

		$price_total = $this->invoice_item_m->get_total($invoice->post_id, 'total');

		if($price_total == 0) continue;

		$this->table->add_row(array(
			$invoice->post_id,
			anchor(module_url('invoices/view/'.$invoice->post_id), ' '.$invoice->post_title),
			my_date($invoice->start_date,'d/m/Y'),
			my_date($invoice->end_date,'d/m/Y'),
			currency_numberformat($price_total,''),
			$invoice_status[$invoice->post_status],
			$this->config->item($invoice->term_type,'services'),
			$invoice->term_name,
			dateDifference(time(),$invoice->end_date,'<span class="label label-warning">Đã trễ ','<span class="label label-info">Còn lại ').'</span>'
		));
	}
}
echo $this->table->generate();