<style type="text/css">
  button
  {
    margin-left : 10px;
  }
</style>
<?php
$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

$title = 'Hóa đơn đã thu trong tháng '.date('m');
if(!empty($datefilter))
{
  $date_range = str_replace(' ', ' đến ', $datefilter);
  $title = 'Hóa đơn đã thu từ ' . $date_range;
}

$caption = form_open();
$caption.= $title;
$caption.= form_button(array('name'=>'revenue_excel_export','type'=>'submit'),'<i class="glyphicon glyphicon-export"></i> Export', 'class="btn btn-primary pull-right"');
$caption.= form_button(array('name'=>'filter_submit','type'=>'submit'),'Lọc', 'class="btn btn-info pull-right"');
$caption.= form_hidden('datefilter',$datefilter??'');
$caption.= form_button('daterange','<i class="fa fa-calendar"></i><span>'.($datefilter??'Chọn thời gian').'</span> <i class="fa fa-caret-down"></i>',array('class'=>'btn btn-default pull-right', 'id'=>'daterange-btn'));
$caption.= form_close();

$this->table->set_caption($caption);

$heading = array('Ngày thu','Tên công ty','Mã HĐ','Số tiền','NVKD','VAT','Doanh thu','Dịch vụ');
$this->table->set_heading($heading);
echo $this->table->generate($inv_data);

if(!empty($content))
{
  $this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
  $this->template->javascript->add('plugins/daterangepicker/moment.min.js');
  $this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');  
  echo $content;
}

?>

<script type="text/javascript">

$(function(){

  $('#daterange-btn').daterangepicker({
    ranges: {
      'Last 7 Days': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
      'Last 30 Days': [moment().subtract(30, 'days'), moment().subtract(1, 'days')],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate: moment()
    },
    function (start, end) {
      $('#daterange-btn span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
      $("input[name=datefilter]").val(start.format('YYYY-MM-DD') + ' ' + end.format('YYYY-MM-DD'));
      console.log(moment());
    }
  );
 })
</script>

<style type="text/css">

  .tooltip-inner
  {
    max-width: none;
    white-space: nowrap;
  }
  
  .td
  {
    min-width: 500px; 
  }

</style>