<?php

echo $this->table->set_caption('Lịch sử thanh toán
	<span class=" pull-right"><a href="#" class="btn btn-md btn-primary" data-toggle="mainmodal"><i class="fa fa fa-plus visible-xs"></i><span class="hidden-xs">Thêm</span></a></span>
	');

$this->table->add_row(array(1,'23/01/2015', 'Chuyển khoản','102245',20000000));

$this->table->add_row(array('data' => '<b>Tổng thanh toán: </b>', 'class' => 'text-right', 'colspan' =>4), '10000000');
$this->table->add_row(array('data' => '<b>Còn lại: </b>', 'class' => 'text-right', 'colspan' =>4), $price_total);
echo $this->table->generate();

return '';
?>


<?php

echo $this->table->set_caption('Thông tin <button type="button" id="add_task" name="add_new" class="btn btn-info  btn-primary pull-right btn-add-new" data-toggle="modal" data-target="#myModal" onclick="javascript:set_default_form();"><i class="glyphicon glyphicon-plus"></i> Thêm mới</button>');
$this->table->set_heading(array('#','Tên','Chú thích', 'Số lượng', 'Đơn giá', 'Tỉ lệ thu', 'Thành tiền','#'));

$price_total = 0;
if($items)
{
	foreach($items as $i=>$item)
	{
		$this->table->add_row(array(
			++$i,
			$item->invi_title,
			$item->invi_description,
			$item->quantity,
			$this->invoice_m->price_format($item->price,2),
			$item->invi_rate.'%',
			$this->invoice_m->price_format($item->total),
			anchor('', 'Sửa', 'class="ajax_edit" data-type="item-edit" data-itemid="'.$item->invi_id.'"').' | '.
			anchor('', 'Xóa', 'class="ajax_delete" data-type="item-delete" data-itemid="'.$item->invi_id.'"')
			));
		$price_total+= $item->total;
	}
	$price_total = $this->invoice_m->price_format($price_total);
	$this->table->add_row(array('data' => '<b>Thuế: </b>', 'class' => 'text-right', 'colspan' =>7), '5%');
	$this->table->add_row(array('data' => '<b>Thành tiền: </b>', 'class' => 'text-right', 'colspan' =>7), $price_total);
}
else
{
	$this->table->add_row(array('data' => 'Chưa có nội dung thu', 'class' => 'text-center', 'colspan' =>8), '');
}

echo $this->table->generate();


?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Thêm mới</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<?php  echo $this->admin_form->form_open();
					echo $this->admin_form->hidden('','item_id',0,'', array('id'=>'item_id')); 
					echo $this->admin_form->input('Tiêu đề','title', '', '', array('id'=>'title'));
					echo $this->admin_form->input('Ghi chú','description', '', '', array('id'=>'description'));

					echo $this->admin_form->dropdown('Trạng thái', 'status', $this->config->item('status','invoice'), 'post-write','', array('id'=>'status'));
					echo $this->admin_form->input('Đơn giá','price', '', '', array('id'=>'price'));
					echo $this->admin_form->input('Số lượng','quantity', '', '', array('id'=>'quantity'));
					echo $this->admin_form->input('Tỉ lệ thu','rate', '', '', array('id'=>'rate', 'addon_begin'=>'%'));
					?>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" name="add_item" class="btn btn-primary">Save changes</button>
				<?php echo $this->admin_form->form_close();?>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	function set_default_form()
	{
		$('input#item_id').val(0);
		$('input#title').val('');
		$('input#price').val('');
		$('input#quantity').val(1);
		$('input#description').val('');
		$('input#rate').val('100');
		$('input#status').val(0).change();
	}
	$('.ajax_edit').click(function(ev){
		var id = $(this).data('itemid');
		var type = $(this).data('type');
		var jqxhr = $.getJSON( "<?php echo module_url();?>invoices/ajax/"+type+"/"+id, function(data) {
			$('input#item_id').val(data.id);
			$('input#title').val(data.title);
			$('input#price').val(data.price);
			$('input#quantity').val(data.quantity);
			$('input#description').val(data.description);
			$('input#rate').val(data.rate);
			$('select#status').val(data.status).change();
		})
		.done(function() {
			$('#myModal').modal({
				show: 'true'
			}); 
		})
		.fail(function() {
			$.notify("Có lỗi xảy ra, không kết nối được dữ liệu", "error");
		});
		ev.preventDefault();
		return false;
	});
	$('.ajax_delete').click(function(ev){
		var id = $(this).data('itemid');
		var me = $(this);
		$(me).parents('tr').css('background-color','#FFEB3B');
		var jqxhr = $.get( "<?php echo module_url();?>invoices/ajax/item-delete/"+id, function(data) {
			if(data =='OK')
			{
				$(me).parents('tr').fadeOut();
			}
			else
			{
				$(me).parents('tr').css('background-color','#FFF');
				$.notify(data, "error");
			}
		});
		ev.preventDefault();
		return false;
	});

</script>
