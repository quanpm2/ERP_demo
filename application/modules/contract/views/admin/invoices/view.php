<div class="col-md-10   form-group row">
	<a href="<?php echo module_url('edit/'.$contract_id);?>" class="btn btn-success"> <span class="fa fa-reply"></span> Trở về hợp đồng </a>
	<div class="btn-group">
		<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="glyphicon glyphicon-export"></span> Xuất <span class="caret"></span>
			<span class="sr-only">Xuất</span>
		</button>
		<ul class="dropdown-menu" role="menu">
			<li><a href="#"><span class="fa fa-file-excel-o"></span> Excel</a></li>
			<li><a href="#"><span class="fa fa-file-pdf-o"></span> PDF</a></li>
		</ul>
	</div>
	<button type="button" class="btn btn-primary"><span class="fa fa-envelope"></span> Gửi mail </button>
	
</div>


<?php

// $this->table->set_heading(array('',''));
$contract  = $this->term_m->get($contract_id);

$invoice->start_date = !empty($invoice->start_date) ? my_date($invoice->start_date, 'd-m-Y') : 'Chưa nhập';

$invoice->end_date = !empty($invoice->end_date) ? my_date($invoice->end_date, 'd-m-Y') : 'Chưa nhập';

$this->load->model('term_users_m');

$customer = $this->term_users_m->get_term_users($contract_id, array('customer_person','customer_company'));

$customer = !empty($customer) ? end($customer) : $customer ;

$this->table->set_caption('Thông tin hóa đơn');

$this->table->add_row(array('<span>Tiêu đề</span> '. anchor('#', (empty($invoice->post_title) ? 'Chưa nhập' : $invoice->post_title), 
	"data-original-title='Tiêu đề' 
	data-pk='{$id}' 
	data-type-data='field'
	data-value='{$invoice->post_title}'
	data-name='post_title',
	class='myeditable editable'"),'<span>Khách hàng</span> ' . @$customer->display_name));

$gender = $this->termmeta_m->get_meta_value($contract_id, 'representative_gender');

$gender = $gender == 1 ? 'Ông ' : 'Bà ';

$this->table->add_row(array(
	'<span>Trạng thái</span>'.anchor('#', $this->invoice_m->status_label($invoice->post_status), 
		"data-original-title='Trạng thái' 
		data-pk='{$id}' 
		data-type-data='field'
		data-name='post_status'
		data-type='select' 
		data-value='{$invoice->post_status}'
		data-source=\"" . str_replace('"', '\'', json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},
			array_keys($this->config->item('status', 'invoice')) , $this->config->item('status', 'invoice')))) . "\"
		class='myeditable editable'"),
	'<span>Người đại diện</span>' . $gender . $this->termmeta_m->get_meta_value($contract_id, 'representative_name')
	));

$this->table->add_row(array(
	'<span>Ngày bắt đầu</span>'.anchor('#', $invoice->start_date, 
		"data-original-title='Ngày bắt đầu của phiếu thu' 
		data-pk='{$id}'
		data-type-data='field'
		data-type='combodate' 
		data-value='{$invoice->start_date}'
		data-format='DD-MM-YYYY'
		data-viewformat='DD-MM-YYYY',
		data-template='D MM YYYY',
		data-name='start_date',
		class='myeditable editable'")
	,'<span>Địa chỉ</span>' . $this->termmeta_m->get_meta_value($contract_id, 'representative_address')
));

$email = $this->termmeta_m->get_meta_value($contract_id, 'representative_email');

$email = empty($email) ? 'Chưa nhập' : safe_mailto($email);

$this->table->add_row(array(
	'<span>Ngày kết thúc</span>'.anchor('#', $invoice->end_date, 
		"data-original-title='Ngày kết thúc của phiếu thu' 
		data-pk='{$id}' 
		data-type-data='field'
		data-type='combodate' 
		data-value='{$invoice->end_date}'
		data-format='DD-MM-YYYY'
		data-viewformat='DD-MM-YYYY',
		data-template='D MM YYYY',
		data-name='end_date',
		class='myeditable editable'")
	,'<span>Email</span>' . $email
	));

$service = $this->config->item($contract->term_type,'services');

$service = $service ? ucfirst($service) : 'Chưa nhập';

$this->table->add_row(array('<span>Dịch vụ</span> ' . $service,
	'<span>Điện thoại</span>' . $this->termmeta_m->get_meta_value($contract_id, 'representative_phone')));

$extra_data = @unserialize($this->termmeta_m->get_meta_value($contract_id, 'extra'));

$website = $this->term_m->get($contract->term_parent);

$website = !empty($website) ? anchor(prep_url($website->term_name)) : 'Chưa nhập';

$this->table->add_row(array('<span>Website</span> ' . $website,
	'<span>Mã số thuế</span>' . @$extra_data['customer_tax']));



$staff_business = $this->postmeta_m->get_meta_value($id, 'staff_business');
$staff_business = ($staff_business) ? $this->admin_m->get_field_by_id($staff_business, 'display_name') : 'Chưa có';

$commission = $this->postmeta_m->get_meta_value($id, 'invoice_commission');
if(!$commission)
{
	$commission = $this->config->item('commission', 'invoice');
	$this->postmeta_m->update_meta($id, 'invoice_commission', $commission);
}

$this->table->add_row(array(
	'<span>Nhân viên KD</span> '.anchor('#', $staff_business, 
		"data-original-title='Hoa hồng' 
		data-pk='{$id}' 
		data-type-data='meta_data'
		data-type='select'
		data-value='{$staff_business}'
		data-name='staff_business'
		data-source=\"" . str_replace('"', '\'', json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},
			array_keys($staffs) , $staffs))) . "\"
		class='myeditable editable'"),

	'<span>Hoa hồng</span>'.anchor('#', $commission, 
		"data-original-title='Hoa hồng' 
		data-pk='{$id}' 
		data-type-data='meta_data'
		data-type='number'
		data-value='{$commission}'
		data-name='invoice_commission',
		class='myeditable editable'")  . ' %'
	));

echo $this->table->generate();

echo $this->template->content_items->view('invoices/items')->content();

?>
<style type="text/css">
	.content table tr > td >span {
		width: 100px;
		display: block;
		float: left;
		font-weight: bold;
	}
	.content table tr > td >span::after {
		content: ':';
	}
</style>

<script type="text/javascript">

	function reload_page(data){

		location.href = '<?php echo module_url("invoices/view/{$id}");?>';
	}

	$(function(){
		$(".myeditable").editable({
			url : '<?php echo module_url("invoices/ajax_dipatcher/ajax_edit/{$id}");?>',
			params: function(params) {
				params.type = $(this).data("type-data");
				return params;
			},
			success: function(data, newValue) {
				if($.isEmptyObject(data.response)) return;
				if($.isEmptyObject(data.response.response)) return;
				if(data.response.response.hasOwnProperty("jscallback")){
					$.each(data.response.response.jscallback,function(i,e){
						var fn = window[e.function_to_call];
						fn(e.data);
					});
				}
			},
			defaultValue: "",
		});
	});
</script>