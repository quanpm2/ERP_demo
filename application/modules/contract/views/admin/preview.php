<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WEBDOCTOR-<?php echo date('mY');?>-<?php echo $term->term_id;?>-<?php echo $term->term_name;?>.pdf</title>
<style>
body{line-height:1.2em; font-size: 14px; padding: 0 15mm;}
p {margin:10px 0}
.title{background:#eee; font-weight:600; border-top:1px solid #000}
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body style="font-family: 'Times New Roman', serif;">
<p align="center">
    CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br/>
    <span>Độc lập - Tự do - Hạnh phúc</span><br/>
    <span>--------------oOo--------------</span>
</p>
<p>&nbsp;</p>
<p align="center"><span style="font-size:1.3em; line-height:1.3em">HỢP ĐỒNG CUNG CẤP GIẢI PHÁP PHẦN MỀM ỨNG DỤNG <br />
  <strong>CHĂM SÓC WEBSITE</strong></span><br />
  (<?php echo get_term_meta_value($term->term_id,'contract_code');?>)</p>
<p align="center">&nbsp;</p>
<p>Hôm nay, <?php echo my_date(time(),'\n\g\à\y d \t\h\á\n\g m \n\ă\m Y');?> chúng tôi gồm:</p>
<?php
$this->table->clear();
$this->table->set_template(array('table_open' => '<table width="100%" border="0" cellspacing="0" cellpadding="3">'));
$this->table
  ->add_row(array('data'=>'Bên thuê dịch vụ','style'=>'font-weight:bold','width'=>'25%'),
    array('data'=>mb_strtoupper($term->customer->display_name),'style'=>'font-weight:bold'))
  ->add_row('Địa chỉ',get_term_meta_value($term->term_id, 'representative_address'));

$phone = get_term_meta_value($term->term_id,'representative_phone');
if(!empty($phone)) 
  $this->table->add_row('Điện thoại',$phone);

if(!empty($term->extra['customer_tax'])) 
  $this->table->add_row('Mã số thuế',$term->extra['customer_tax']);

$gender = get_term_meta_value($term->term_id,'representative_gender');
$gender = $gender == 1 ? 'Ông' : 'Bà';
$fullname = $gender.' '.get_term_meta_value($term->term_id,'representative_name');
$this->table->add_row('Đại diện',$fullname);

if(!empty($term->extra['representative_position']))
  $this->table->add_row('Chức vụ',$term->extra['representative_position']);

$this->table
  ->add_row(array('data'=>'<em>(Sau đây gọi là Bên A)</em>','colspan'=>2))
  ->add_row('<strong>Bên cung ứng dịch vụ</strong>','<strong>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</strong>')
  ->add_row('Địa chỉ',':Tầng 8, 402 Nguyễn Thị Minh Khai , Phường 5, Quận 3, TP.HCM, Việt Nam')
  ->add_row('Điện thoại',': (08) 66852 888')
  ->add_row('Mã số thuế',': 0313547231')
  ->add_row('Đại diện',': Ông Triệu Minh Hải')
  ->add_row('Chức vụ',': Giám đốc kinh doanh')
  ->add_row(array('data'=>'<em>(Sau đây gọi là Bên B)</em>','colspan'=>2));

echo $this->table->generate();
?>
<p><strong>Dựa trên: </strong><br />Nhu cầu của Bên A và khả năng cung cấp của Bên B về dịch vụ. Hai bên đồng ý ký kết hợp đồng này với các điều khoản sau:</p>
<p> <strong>ĐIỀU 1: DỊCH VỤ ĐĂNG KÝ</strong></p>
<?php
$this->table->clear();
$this->table->set_template(array('table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">'));
$this->table->set_heading(array(
    array('data'=>'Dịch vụ','style'=>'font-weight:bold','align'=>'center','width'=>'25%'),
    array('data'=>'Đơn giá','style'=>'font-weight:bold','align'=>'center'),
    array('data'=>'Thời gian','style'=>'font-weight:bold','align'=>'center','width'=>'35%'),
    array('data'=>'Thành tiền (vnđ)','style'=>'font-weight:bold','align'=>'center','width'=>'25%')));

$contract_begin = get_term_meta_value($term->term_id,'contract_begin');
$contract_end = get_term_meta_value($term->term_id,'contract_end');
$months = diffInMonths($contract_begin, $contract_end);

$this->load->model('webgeneral/webgeneral_config_m');
$package_name = strtolower($this->webgeneral_config_m->get_package_name($term->term_id));
$service_price = $this->webgeneral_config_m->get_price($term->term_id);
$total_service_price = $service_price * $months;
$total = $total_service_price;

$services_pricetag = get_term_meta_value($term->term_id, 'services_pricetag');
$services_pricetag = @unserialize($services_pricetag);

$vat = get_term_meta_value($term->term_id,'vat');
$first_month = $months - (int)@$services_pricetag['discount_month'];
$tmp_contract_end = strtotime('+ '.$first_month.' month',$contract_begin);
$tmp_total = $first_month*$service_price;

$has_display_gift_service = FALSE;

$this->table->add_row('Chăm sóc website (*)<br/><em>'.$term->term_name.'</em>',
  currency_numberformat($service_price).' <br/><em>'.$package_name.'</em>',
  $first_month.' tháng<br/>(từ '.my_date($contract_begin,'d/m/Y').' - '.my_date($tmp_contract_end,'d/m/Y').')',
  array('data' => currency_numberformat($tmp_total),'align' => 'right'));

if(!empty($services_pricetag['discount_month'])){
  $discount_month_price = $services_pricetag['discount_month'] * $service_price* (-1);
  $total += $discount_month_price;
  $time_col = $services_pricetag['discount_month'].' tháng<br/>(từ '.my_date(strtotime('+1 day',$tmp_contract_end),'d/m/Y').
  ' - '.my_date($contract_end,'d/m/Y').')';
  $this->table->add_row(array('data'=>'Khuyến mãi chăm sóc Website','colspan'=>2),$time_col,array('data' => 'Miễn phí','align' => 'right'));
  $total_service_price = $total_service_price + $discount_month_price;
}

$is_free_webdesign = FALSE;
if(!empty($services_pricetag['webdesign']['is_design'])){
  $is_free_webdesign = empty($services_pricetag['webdesign']['price']); 
  if(!$is_free_webdesign){
    $total += @$services_pricetag['webdesign']['price'];
    $this->table->add_row(array('data'=>'Thiết kế website','colspan'=>3),
      array('data'=>currency_numberformat($services_pricetag['webdesign']['price']),'align'=>'right'));
  }
  else if($is_free_webdesign && !$has_display_gift_service) $has_display_gift_service = TRUE;
}

$is_free_hosting = FALSE;
if(!empty($services_pricetag['hosting']['has_hosting'])){
  $is_free_hosting = empty($services_pricetag['hosting']['price']);
  if(!$is_free_hosting){
    $total += @$services_pricetag['hosting']['price'];
    $this->table->add_row(array('data'=>'Hosting','colspan'=>3),
      array('data'=>currency_numberformat($services_pricetag['hosting']['price']),'align'=>'right'));
  }
  else if($is_free_hosting && !$has_display_gift_service) $has_display_gift_service = TRUE;
}

$is_free_domain = !empty($services_pricetag['domain']['is_free']);
if($is_free_domain && !$has_display_gift_service) 
  $has_display_gift_service = TRUE;

$discount_amound = 0;
if(!empty($services_pricetag['discount']['is_discount'])){
  $discount_amound = ($total_service_price * ((float) @$services_pricetag['discount']['percent'] / 100));
  if($discount_amound != 0){
    $total-=$discount_amound;
    $discount_rate = (float) @$services_pricetag['discount']['percent'];
    $this->table->add_row(array('data'=>'Giảm giá '.$discount_rate.'&#37','colspan'=>3),
      array('data'=>currency_numberformat(-1*$discount_amound),'align'=>'right'));
  }
}

$this->table->add_row(array('data'=>'Tổng cộng','colspan'=>3),array('data'=>currency_numberformat($total),'align'=>'right'));

if(!empty($vat)){
  $tax = $total*($vat/100);
  $total = cal_vat($total,$vat);
  $this->table
    ->add_row(array('data'=>'VAT','colspan'=>3),
      array('data'=>'<strong>'.$vat.'&#37; ('.currency_numberformat($tax).')</strong>','align'=>'right'))
    ->add_row(array('data'=>'Thành tiền','colspan'=>3),
      array('data'=>currency_numberformat($total),'align'=>'right'));
}
echo $this->table->generate();
?>
<p><em>Bằng chữ: <?php echo ucfirst(strtolower(convert_number_to_words($total)));?> đồng.</em><br/>
<br/>
<em>(*) Chi tiết các hạng mục dịch vụ, khách hàng xem ở Phụ lục đính kèm</em></p>
<p>Website đăng ký dịch vụ: <strong><?php echo prep_url($term->term_name);?></strong></p>
<?php

$gift_package = $this->webgeneral_config_m->get_gift($term->term_id);
if($has_display_gift_service && $gift_package !== FALSE){
  $this->table->clear();
  $heading_row = array('data' => 'Dịch vụ tặng kèm - khuyến mãi', 'colspan' => 2,'style'=>'font-weight:bold');
  $this->table->add_row($heading_row);
  $total = 0;
  foreach ($gift_package as $key => $item){
    if(empty(${"is_free_{$key}"})) continue;
    $is_exists_domain = $key == 'domain' && !empty($services_pricetag['domain']['identity']);
    if($is_exists_domain) $item['label'].= ' '.$services_pricetag['domain']['identity'];
    $total+= $item['value'];

    $this->table->add_row($item['label'],array('data' => currency_numberformat($item['value']),'align' => 'right'));
  }

  if($total > 0) $this->table->add_row('Tổng giá trị tặng kèm từ',array('data' => currency_numberformat($total),'align' => 'right'));

  $this->table->set_template(array('table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">'));
  echo $this->table->generate();
  echo '
  <p>(*) Chương trình khuyến mãi áp dụng ngay sau khi khách hàng ký kết hợp đồng</p>
  <p>Sau 12 tháng thực hiện dịch vụ, Webdoctor.vn sẽ bàn giao miễn phí website, domain cho khách hàng</p>
  <p>Trường hợp không sử dụng dịch vụ đủ 12 tháng, nếu quý khách muốn được sở hữu website sẽ được khấu trừ theo thời gian sử dụng</p>';
}

?>
<p> <strong>ĐIỀU 2: PHƯƠNG THỨC THANH TOÁN</strong><br />
 
  <?php
  $services_pricetag['number_of_payments'] = 
    empty($services_pricetag['number_of_payments']) ? $months : $services_pricetag['number_of_payments'];
if($total > 0) {
  echo ' 2.1  Bên A thanh toán cho Bên B thành '.$services_pricetag['number_of_payments'].' đợt như sau:<br />';
  if($months == $services_pricetag['number_of_payments']){
    $has_sale = !empty($services_pricetag['discount']['is_discount']) && !empty($services_pricetag['discount']['percent']);
    $rate = $has_sale ? $services_pricetag['discount']['percent'] : 0 ;
    $rate = 100 - $rate;
    $price_total = cal_vat($service_price, $vat);
    $price_total = $this->invoice_item_m->calc_total_price($price_total, 1, $rate);

    if($months == 1)
    {
        echo 'Bên A  thanh toán cho Bên B '.currency_numberformat($price_total).' sau khi ký và trước khi thực hiện hợp đồng, 
    chậm nhất trước ngày '.my_date($contract_begin,'d/m/Y').'.<br />';
    }else{
     echo '<u>Đợt 1</u>: Bên A  thanh toán cho Bên B '.currency_numberformat($price_total).' sau khi ký và trước khi thực hiện hợp đồng, 
    chậm nhất trước ngày '.my_date($contract_begin,'d/m/Y').'.<br />';
       echo '<u>Các đợt tiếp theo</u>:  Bên A thanh toán cho Bên B '.currency_numberformat($price_total).' định kỳ, 
    chậm nhất trước ngày '.my_date($contract_begin,'d').' hàng tháng.';
    }
   
  }
  else{

    $start_date = $contract_begin;
    $end_date = strtotime("+1 months", $start_date);

    $items = $this->invoice_m->get_posts(array(
      'select' => 'posts.post_id, post_title,post_status,start_date,post_content,end_date',
      'tax_query' => array( 'terms'=>$term_id),
      'numberposts' =>0,
      'orderby' => 'posts.end_date',
      'order' => 'asc',
      'post_type' => 'contract-invoices'
    ));

    $i = 1;
    if(count($items) == 1){
      $price_total = $this->invoice_item_m->get_total($items[0]->post_id, 'total');
      $price_total = cal_vat($price_total, $vat);
      $inv_price = $this->invoice_m->price_format($price_total);
      $inv_deadline = my_date($items[0]->start_date,'d/m/Y');
      echo 'Bên A thanh toán cho Bên B '.$inv_price.', chậm nhất trước ngày '.$inv_deadline.'.<br />';
    }
    else foreach($items as $i =>$inv){
      $price_total = $this->invoice_item_m->get_total($inv->post_id, 'total');
      $price_total = cal_vat($price_total, $vat);
      $inv_price = $this->invoice_m->price_format($price_total);
      $inv_deadline = my_date($inv->start_date,'d/m/Y');
      echo '<u>Đợt '.(++$i).'</u>: Bên A thanh toán cho Bên B '.$inv_price.', chậm nhất trước ngày '.$inv_deadline.'.<br />';
    }
  }

?>
</p>
<p>2.2  Thanh toán bằng hình thức chuyển khoản<br />

<?php
  if(!empty($bank_info))
  {
    $this->table->clear();
    $template = array('table_open' => '<table width="80%" border="0" cellspacing="0" cellpadding="3" align="center">');
    $this->table->set_template($template);

    foreach ($bank_info as $label => $text) 
    {
      if(is_array($text))
      {
        foreach ($text as $key => $value)
        {
          $this->table->add_row(array('data'=>'+ '.$key,'width'=>'30%'),': '.$value);
        }
        echo $this->table->generate().br();
        $this->table->set_template($template);
        continue;
      }

      $this->table->add_row(array('data'=>'+ '.$label,'width'=>'30%'),': '.$text);
    }

    if(!empty($this->table->rows))
    {
      echo $this->table->generate();
    }
  }
} //end if $total > 0
else
{
  echo ' 2.1  Bên B miễn phí phí thanh toán cho Bên A<br />';
}
?>
<p> <strong>ĐIỀU 3: TRÁCH NHIỆM CỦA BÊN A</strong><br />
  3.1 Cung cấp đầy đủ các  thông tin để Bên B tiến hành khởi tạo dịch vụ cho Bên A </p>
<p> 3.2 Chịu trách nhiệm trước  pháp luật về các sản phẩm, dịch vụ, thông tin,…mà Bên B cung cấp thông qua  website được nêu ra trong Điều 1 của hợp đồng này. </p>
<p> 3.3 Thanh toán đầy đủ các khoản chi phí cho Bên B như đã nêu tại Điều 2 của hợp đồng này. </p>
<p> 3.4 Bên A có quyền yêu cầu  Bên B hỗ trợ điều chỉnh lại bài viết nếu bài viết chưa đạt yêu cầu dựa trên bài  đã viết và dựa trên thông tin Bên A cung cấp. </p>
<p> 3.5.  Bên A có trách nhiệm duyệt và chấp thuận về nội dung bài viết, thông qua email  hoặc văn bản, để Bên B đăng tải lên website.<strong></strong></p>
<p><strong>ĐIỀU 4: TRÁCH NHIỆM CỦA BÊN B</strong><br />
  4.1. Hoàn thành việc khởi  tạo dịch vụ theo Điều 1 về chi tiết dịch vụ kể từ khi nhận được đầy đủ thông  tin và tiền thanh toán của Bên A. </p>
<p> 4.2. Điều chỉnh dịch vụ  cho Bên A, trong phạm vi thông số dịch vụ, nếu được Bên A yêu cầu điều chỉnh hoặc  được Bên A chấp nhận đề nghị điều chỉnh của Bên B. Các &ldquo;yêu cầu điều chỉnh&rdquo; và  &ldquo;đề nghị điều chỉnh&rdquo; sẽ được gởi và xác nhận bằng văn bản hoặc email của 2 bên. </p>
<p> 4.3.  Bên B không chịu trách nhiệm pháp lý và bồi thường cho Bên A và bên thứ ba đối  với các thiệt hại trực tiếp, gián tiếp, vô ý, đặc biệt, vô hình, các thiệt hại  về lợi nhuận, doanh thu, uy tín phát sinh từ việc sử dụng sản phẩm, dịch vụ của  Bên A.</p>
<p> <strong>ĐIỀU 5: BẢO MẬT VÀ CÔNG BỐ THÔNG TIN</strong><br />
  5.1 Mỗi  bên sẽ bảo mật và sẽ không tiết lộ cho các bên thứ ba trong suốt thời hạn hiệu  lực của hợp đồng này hoặc sau đó, toàn bộ hay một phần nội dung của hợp đồng  này và bất kỳ thông tin nào do một trong hai bên trao đổi bằng văn bản, lời nói  hay bằng bất kỳ hình thức nào ngoại trừ các thông tin được công bố theo yêu cầu  của các cơ quan quản lý chuyên ngành, cơ quan chủ quản và các cơ quan pháp luật.</p>
<p> 5.2  Hai Bên đảm bảo không tiết lộ các thông tin liên quan đến các tài khoản được dùng để Bên B thực hiện dịch vụ cho Bên A, các tài khoản được liệt  kê bao gồm nhưng không giới hạn sau đây: tài khoản quản trị website, tài khoản  quản trị hosting, mã nguồn website.</p>
<p> <strong>ĐIỀU 6: THANH LÝ HỢP ĐỒNG </strong><br />
  Hai bên tiến hành thanh lý  hợp đồng trong các trường hợp sau: <br />
  6.1 Một trong hai bên vi  phạm các điều khoản trong hợp đồng này. </p>

<p> 6.2 Kể từ ngày hết hạn dịch  vụ thì hợp đồng mặc nhiên được thanh lý. </p>
<p> 6.3 Hợp đồng kinh tế bị  đình chỉ, hủy bỏ. </p>
<p> 6.4 Pháp nhân giải thể.</p>
<p>- Trường hợp Bên A muốn dừng hoặc thanh lý hợp đồng trước hạn thì Bên A phải gửi thông báo trước bằng văn bản cho Bên B và phải được Bên B chấp nhận về thời điểm dừng hợp đồng. Bên B sẽ không hoàn trả phí dịch vụ mà Bên A đã thanh toán cho Bên B khi chấm dứt hợp đồng trước hạn.</p>
<p> <strong>ĐIỀU 7: ĐIỀU KHOẢN THI HÀNH</strong> <br />
  7.1&nbsp;&nbsp;&nbsp;&nbsp; Hai bên  cam kết thực hiện đúng các điều khoản của hợp đồng, bên nào vi phạm sẽ phải chịu  trách nhiệm theo quy định của pháp luật.</p>
<p> 7.2&nbsp;&nbsp;&nbsp;&nbsp; Trong quá  trình thực hiện, nếu có vướng mắc gì thì 2 bên chủ động thương lượng giải quyết  trên tinh thần hợp tác, tôn trọng lẫn nhau. Nếu hai bên không tự giải quyết được  sẽ thống nhất chuyển vụ việc tới Toà án kinh tế có thẩm quyền để giải quyết.</p>
<p> 7.3&nbsp;&nbsp;&nbsp;&nbsp; Hợp đồng này có hiệu lực kể từ ngày ký cho đến hết thời hạn đăng ký tại Điều 1. Khi hợp đồng hết hiệu lực, nếu hai bên tiếp tục gia hạn hợp đồng, hai bên sẽ ký kết phụ lục gia hạn hợp đồng theo bảng báo giá của thời điểm ký phụ lục. </p>
<p> 7.4&nbsp;&nbsp;&nbsp;&nbsp; Hợp đồng này được lập thành 02 bản có giá trị như nhau: Bên A giữ 01 bản, Bên B giữ 01 bản. </p>
<p style="text-align:right"> 
<em>Tp HCM, <?php echo my_date(time(),'\n\g\à\y d \t\h\á\n\g m \n\ă\m Y');?></em>
</p>
<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
  <tr>
    <td width="40%" valign="top" style="text-align:center"><p><strong>BÊN A</strong><br />
      <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
      <strong><?php echo mb_strtoupper($term->customer->display_name);?></strong></p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p></td>
  	<td rowspan="2"></td>
  
    <td width="50%" valign="top" style="text-align:center"><p><strong>BÊN B</strong><br />
      <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
      <strong>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</strong></p>
      <p><strong>&nbsp;</strong></p>
      <p><strong>&nbsp;</strong></p>
      <p>&nbsp;</p></td>
  </tr>
  
  <tr>
    <td valign="top" style="text-align:center">
      <p><strong><?php echo $gender . ' ' . get_term_meta_value($term->term_id,'representative_name');?></strong><br />
    <?php echo @$term->extra['representative_position'];?></p></td>
    <td valign="top" style="text-align:center">
      <p><strong>Triệu Minh Hải</strong><br />
    Giám đốc kinh doanh</p></td>
  </tr>
</table>
<p>&nbsp; </p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html> 