<?php
$this->template->javascript->add('plugins/wizard/jquery.bootstrap.wizard.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');
?>
<div id="rootwizard">	
<?php
if(!empty($wizard_tabs)){

	$html = '<ul>';

	foreach ($wizard_tabs as $key => $tab) {
		
		$html .= '<li><a href="#' . $tab['id'] . '" data-toggle="tab"><span class="label">' . ++$key . '</span><span class="' . $tab['id'] . '">' . $tab['title'] . '</span></a></li>';
	}

	$html .= '</ul>';

	$html .= '<div class="tab-content">';

	foreach ($wizard_tabs as $key => $tab) {

		$html .= '<div class="tab-pane" id="' . $tab['id'] . '">' . $tab['content'] . '</div>';
	}

	$html .= 
		'<ul class="pager wizard">
			<li class="previous"><a href="#">Quay lại</a></li>
		  	<li class="next"><a href="#">Tiếp tục</a></li>
		  	<li class="finish" id="finishstep"><a href="javascript:;">Hoàn tất</a></li>
		</ul>
	</div>';

	echo $html;
}
?>	
	<div id="bar" class="progress">
	  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
	</div>
</div>


<script type="text/javascript">

<?php
$role_id = $this->admin_m->role_id;
$is_sale_roles = in_array($role_id, $this->config->item('salesexecutive','groups'));
$referral_url = $is_sale_roles ? admin_url('order_contact/approval_contract') : module_url("edit/$edit->term_id");
?>
var module_url = "<?php echo module_url();?>";
var wizard_step = '<?php echo get_term_meta_value($edit->term_id,'wizard_step');?>';
var contract_type = '<?php echo $edit->term_type;?>';

function load_customer_profile(cus_id){

  $.notify('Đang xử lý ...','info');

  $("#customer_profile").empty();

  if(cus_id > 0) {

    $.post( module_url + 'render_customer_info/' + cus_id, {}, function( resp ) { $("#customer_profile").append(resp.content);}, "json");
  }
}
function reload_detail_panel(data){

  $("#step-detail-partial").replaceWith(data);

  $("#contract_convert_name").val($('#chosen_website option:selected').text());
}
function reload_review_panel(data){
	$("#review-partial").replaceWith(data);	
	$("#contract_convert_name").val($('#chosen_website option:selected').text());
}
function reload_service_partial(data){
	$("#service_tab").replaceWith(data);
}
function refresh_customer_websites(datasource){

  if(datasource != 'undefined'){

    $("#chosen_website").empty();

    var dtasource = [];

    $.each(datasource,function(i,e){
      dtasource.push({id:i,text:e});
    });

    $("#chosen_website").select2({
        data:dtasource,
    });
  }
}

function toggleBootstrapWizard(){

	var selected_option = $("#servicedrp").val();	

	switch(selected_option){
		case 'google-ads' : 
			$('#rootwizard').bootstrapWizard('display', 4);
			break;
		case 'facebookads' : 
			$('#rootwizard').bootstrapWizard('display', 4);
			break;		
		case 'webdoctor' : 
			$('#rootwizard').bootstrapWizard('hide', 4);
			break;
		case 'webdesign' : 
			$('#rootwizard').bootstrapWizard('display', 4);
			break;
		case 'seo-traffic' :
			$('#rootwizard').bootstrapWizard('display', 4);
			break;
		case 'seo-top' :
			$('#rootwizard').bootstrapWizard('hide', 4);
			break;
		case 'webgeneral' :
			$('#rootwizard').bootstrapWizard('display', 4);
			break;
		case '0' : 
			$('#rootwizard').bootstrapWizard('hide', 4);
			break;
	}
}

$.validator.addMethod('isPhoneNumber', function(value, element){
    if(this.optional(element)) return true;

    value = value.trim();

    // Validate VN phone number
    if(/^((03|05|07|08|09|01[2|6|8|9]|02[0|1|2|3|4|5|6|7|8|9]{1,2})+([0-9]{8})|(1900+([0-9]{4}|[0-9]{6})))$/g.test(value)) return true;

    // Validate Australia phone number
    if(/^(((\d *?){11})|([0-9a-zA-Z]{2}-[0-9a-zA-Z]{7}))$/g.test(value)) return true;

    // Validate US phone number
    if(/^\+61+[0-9]{9}$/g.test(value)) return true;

    // Validate South Korea phone number
    if(/^((001|\+82)+[0-9]{10})$/g.test(value)) return true;

    // Validate Hong Kong phone number
    if(/^((001|\+852)+[0-9]{8})$/g.test(value)) return true;

    // Validate Singapore phone number
    if(/^((001|\+65)+[0-9]{8})$/g.test(value)) return true;

    // Validate Taiwan phone number
    if(/^((0(2|3|4|5|6|7|8|9)|\+8869)+[0-9]{8})$/) return true;

    return false;
}, 'Cấu trúc số điện thoại không hợp lệ');

$.validator.addMethod('isTin', function(value, element){
    if(this.optional(element)) return true;

    value = value.trim();

    // Validation Syntax with Vietnam TIN
    if(/^[0-9a-zA-Z]{10}(-\d{3})?$/g.test(value)) return true;

    // Validation Syntax with Australia TIN
    if(/^(((\d *?){11})|([0-9a-zA-Z]{2}-[0-9a-zA-Z]{7}))$/g.test(value)) return true;
    
    // Validation Syntax with Japan TIN
    if(/^[0-9]{12,13}$/g.test(value)) return true;

    // Validation Syntax with Hongkong TIN
    if(/^(([A-Z]\d{6}[0-9A])|([0-9]{8}-[0-9A-Z]{3}-[0-9A-Z]{2}-[0-9A-Z]{2}-[0-9A-Z]{1}))$/g.test(value)) return true;

    // Validation Syntax with Singapore TIN
    if(/^(([0-9]{8,9}[a-zA-Z])|((F000|F   )[0-9]{5}[a-zA-Z])|((A|4)[0-9]{7}[a-zA-Z])|((20|19)[0-9]{2}(LP|LL|FC|PF|RF|MQ|MM|NB|CC|CS|MB|FM|GS|DP|CP|NR|CM|CD|MD|HS|VH|CH|MH|CL|XL|CX|HC|RP|TU|TC|FB|FN|PA|PB|SS|MC|SM|GA|GB)[0-9]{4}[a-zA-Z]))$/g.test(value)) return true;

    // Validation Syntax with Taiwan TIN
    if(/^[0-9]{8}$/g.test(value)) return true;

    return false;
}, 'Cấu trúc mã số thuế không hợp lệ');

$(function()
{
	$("#rootwizard ul li a[href='#tab-service'] span.tab-service").text($('#servicedrp option:selected').text());

	var detailfrm = $("#confirm_step_detail").closest('form');
	detailfrm.validate({  
		rules: {
		  'edit[meta][representative_name]': {
		    required: true,
		  },
		  'edit[meta][representative_address]': {
		    required: true,
		  },
		  'edit[meta][representative_email]': {
		    required: true,
		    email: true,
		  },
		  'edit[meta][representative_phone]': {
		    required: true,
		    minlength: 3,
		    digits: true,
		  },
		  'edit[meta][contract_daterange]' : {
		  	required:true,
		  }
		},
		messages:{
		  'edit[meta][representative_name]': {
		    required: 'Trường này không được để trống',
		  },
		  'edit[meta][representative_address]': {
		    required: 'Trường này không được để trống',
		  },
		  'edit[meta][representative_email]': {
		    required: 'Trường này không được để trống',
		    email: 'Kiểu dữ liệu không hợp lệ',
		  },
		  'edit[meta][representative_phone]': {
		    required: 'Trường này không được để trống',
		    digits: 'Kiểu dữ liệu không hợp lệ , số điện thoại phải là kiểu số',
		  },
		  'edit[meta][contract_daterange]' : {
		  	required:'Trường này không được để trống',
		  }
		}
	});

	$("#contract_convert_name").val($('#chosen_website option:selected').text());

	$("#chosen_customer").on("change", function(e) {
		var form = $(this).closest('form');
		form.find("input[name='user_updated']").val(1);
		load_customer_profile($(this).val());
	});

	$("#create_person_customer").find('form').validate({  
	rules: {
	  'edit[meta][customer_email]': {
	    email: true,
	  },
	  'edit[meta][customer_phone]': {
	    minlength: 3,
	    digits: true,
	  }
	},
	messages:{
	  'edit[meta][customer_email]': {
	    email: 'Kiểu dữ liệu không hợp lệ',
	  },
	  'edit[meta][customer_phone]': {
	    digits: 'Kiểu dữ liệu không hợp lệ , số điện thoại phải là kiểu số',
	  }
	}
	});

  	$("#create_company_customer").find('form').validate({  
	rules: {
	  'edit[meta][customer_email]': {
	    email: true,
	  },
	  'edit[meta][customer_phone]': {
	    minlength: 3,
	    digits: true,
	  }
	},
	messages:{
	  'edit[meta][customer_email]': {
	    email: 'Kiểu dữ liệu không hợp lệ',
	  },
	  'edit[meta][customer_phone]': {
	    digits: 'Kiểu dữ liệu không hợp lệ , số điện thoại phải là kiểu số',
	  },
	}
	});

	$(".create_customer").click(function(){

	var parent_form = $(this).closest('.modal-content').find('form');

	var $valid = parent_form.valid();

	if(!$valid) return false;

	$.post( module_url + 'ajax_dipatcher/act_create_customer', 

	  parent_form.serializeArray(),

	  function( data ) {

	    console.log(data);

	    if(data.response != 'undefined'){

	      var datasource = $("#chosen_customer").select2('data');

	      datasource.push({id : data.response.id, text:data.response.text});

	      $("#chosen_customer").select2({
	        data : datasource,
	      });
	      
	      $("#chosen_customer").val(data.response.id).trigger("change");
	    }
	  },
	  "json");
	});

	load_customer_profile($('#chosen_customer option:selected').val());

	$('#rootwizard').bootstrapWizard({'tabClass': 'bwizard-steps',
		onNext: function(tab, navigation, index) {
		var ajax_action = ajax_form = '';
		switch(index) {
		    case 1:
		    	var objchosen = $("#submit_chosen_type" ).closest('form').find("select[name='edit[term_type]']");

		    	if(objchosen.val() == 0){

    				$.notify('Vui lòng chọn dịch vụ !','error');
		    		return false;
		    	}

		        ajax_action = module_url + 'ajax_dipatcher/act_step1';
				ajax_form = $("#submit_chosen_type" ).closest('form');

		        break;
		    case 2:
		    	var objchosen = $("#confirm_step_customer" ).closest('form').find("select[name='edit[user_id]']");

		    	if(objchosen.val() == 0){

    				$.notify('Vui lòng chọn khách hàng !','error');
		    		return false;
		    	}

		        ajax_action = module_url + 'ajax_dipatcher/act_step2';
				ajax_form = $("#confirm_step_customer" ).closest('form');
		        break;
		    case 3:
		    	var objchosen = $("#confirm_step_website" ).closest('form').find("select[name='edit[term_parent]']");

		    	if(objchosen.val() == 0){

    				$.notify('Vui lòng chọn Website !','error');
		    		return false;
		    	}
		        ajax_action = module_url + 'ajax_dipatcher/act_step3';
				ajax_form = $("#confirm_step_website" ).closest('form');
		        break;
		    case 4:
		        ajax_action = module_url + 'ajax_dipatcher/act_step4';
				ajax_form = $("#confirm_step_detail").closest('form');
				ajax_form.validate({  
					rules: {
					  'edit[meta][representative_name]': {
					    required: true,
					  },
					  'edit[meta][representative_address]': {
					    required: true,
					  },
					  'edit[meta][representative_email]': {
					    required: true,
					    email: true,
					  },
					  'edit[meta][representative_phone]': {
					    required: true,
					    minlength: 3,
					    digits: true,
					  },
					  'edit[meta][contract_daterange]' : {
					  	required:true,
					  }
					},
					messages:{
					  'edit[meta][representative_name]': {
					    required: 'Trường này không được để trống',
					  },
					  'edit[meta][representative_address]': {
					    required: 'Trường này không được để trống',
					  },
					  'edit[meta][representative_email]': {
					    required: 'Trường này không được để trống',
					    email: 'Kiểu dữ liệu không hợp lệ',
					  },
					  'edit[meta][representative_phone]': {
					    required: 'Trường này không được để trống',
					    digits: 'Kiểu dữ liệu không hợp lệ , số điện thoại phải là kiểu số',
					  },
					  'edit[meta][contract_daterange]' : {
					  	required:'Trường này không được để trống',
					  }
					}
				});
				var $valid = ajax_form.valid();
				if(!$valid) return false;
		        break;
		  	case 5:
		        ajax_action = module_url + 'ajax_dipatcher/act_service';
				ajax_form = $("#confirm_step_service" ).closest('form');

				ajax_form.validate({  
					rules: {
					  'edit[meta][contract_cpc]': {
					  	required : {
					  		depends : function(element){
					  			return $("input[name='edit[meta][service_type]']").val() == 'cpc_type';
					  		}
					  	}
					    required: true,
					    digits: true,
					  },
					  'edit[meta][contract_clicks]': {
					    // required: true,
					    digits: true,
					  },
					  'edit[meta][gdn_contract_cpc]': {
					    // required: true,
					    digits: true,
					  },
					  'edit[meta][gdn_contract_clicks]': {
					    // required: true,
					    digits: true,
					  },
					  'edit[meta][exchange_rate_usd_to_vnd]': {
					    // required: true,
					    digits: true,
					  },
					  // 'edit[meta][googleads-adwordclient]': {
					  //   required: true,
					  // },
					  'edit[meta][googleads-begin_time]': {
					    required: true,
					  },
					},
					messages:{
					  'edit[meta][contract_cpc]': {
					    required: 'Trường này không được để trống',
					    digits: 'Kiểu dữ liệu không hợp lệ',
					  },
					  'edit[meta][contract_clicks]': {
					    // required: 'Trường này không được để trống',
					    digits: 'Kiểu dữ liệu không hợp lệ',
					  },
					  'edit[meta][gdn_contract_cpc]': {
					    // required: 'Trường này không được để trống',
					    digits: 'Kiểu dữ liệu không hợp lệ',
					  },
					  'edit[meta][gdn_contract_clicks]': {
					    // required: 'Trường này không được để trống',
					    digits: 'Kiểu dữ liệu không hợp lệ',
					  },
					  'edit[meta][exchange_rate_usd_to_vnd]': {
					    // required: 'Trường này không được để trống',
					    digits: 'Kiểu dữ liệu không hợp lệ',
					  },
					  'edit[meta][googleads-begin_time]': {
					    required: 'Trường này không được để trống',
					  },
					}
				});

				var $valid = ajax_form.valid();

				if(!$valid) return false;

		        break;
		    case 6:
		        ajax_action = module_url + 'ajax_dipatcher/act_finish';
				ajax_form = $("#confirm_step_finish" ).closest('form');
		        break;
		}

		if(ajax_action != ''){

			$.post( ajax_action, ajax_form.serializeArray(), function( data ) {

					// if($.isEmptyObject(data.response)) return;

					if($.isEmptyObject(data.response)) return;

					if(data.response.hasOwnProperty("jscallback")){

						$.each(data.response.jscallback,function(i,e){

							var fn = window[e.function_to_call];

							fn(e.data);
						});
					}
			}, "json");
		}

	}, onTabShow: function(tab, navigation, index) {
		var $total = navigation.find('li').length;
		var $current = index + 1;
		var $percent = ($current/$total) * 100;
		$('#rootwizard .progress-bar').css({width:$percent+'%'});
	},onTabClick: function(tab, navigation, index) {
		return false;
	}});

	$('#finishstep').click(function() {

		ajax_action = module_url + 'ajax_dipatcher/act_finish';

		ajax_form = $("#confirm_step_finish").closest('form');
		$.notify('Hệ thống đang xử lý, vui lòng đợi.','info');
		$.post( ajax_action, ajax_form.serializeArray(), function( data ) {
			$('#finishstep').hide();
			if(data.success == true)
			{
				window.location = '<?php echo $referral_url;?>';
			}
			else
			{
				$.notify('Lỗi xảy ra: '+data.msg,'error');
			}

		}, "json");
	});

	toggleBootstrapWizard();

	if(wizard_step != '') {
		var maxTabs = $('#rootwizard').bootstrapWizard('navigationLength');
		wizard_step = parseInt(wizard_step);
		if(wizard_step == 4 && contract_type == 'webdoctor')
			wizard_step += 1;
		$('#rootwizard').bootstrapWizard('show',wizard_step);
	}

	window.prettyPrint && prettyPrint();	
});
</script>