<!DOCTYPE html PUBLIC "-//W3C//Ddiv XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Ddiv/xhtml1-transitional.ddiv">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Untitled Document</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="<?php echo theme_url();?>/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo theme_url();?>css/style.css"/>
  <script type="text/javascript" src="<?php echo theme_url();?>js/Chart.js"></script>
</head>
<body>
  <div class="container">
    <div class="head">
      <div class="col-sm-3 col-xs-3"><img src="<?php echo theme_url();?>images/webdoctor_logo.png" /></div>
      <div class="col-sm-pull-3" style="float:right">sản phẩm của <img src="<?php echo theme_url();?>images/SC_logo.png"  /></div>
    </div>
    <div class="clear"></div>
    <div class="bg-success">
      <h2 class="text-center">BÁO CÁO CHỈ SỐ GOOGLE ANALYTICS <?php echo $site_name;?></h2>
      <h5 class="text-center">(Từ ngày <?php echo date('d/m/Y',$start_date);?> đến ngày <?php echo date('d/m/Y',$end_date);?>)</h5>

      <div class="clear10"></div>
    </div>
    <?php $this->load->view('report/kpi_view');?>
    <div class="clear10"></div>
    <?php $this->load->view('report/report_detail');?>

    <div class="col-sm-12" >
      <h4 class="text-center">BIỂU ĐỒ LƯU LƯỢNG TRUY CẬP - LƯỢT TÌM KIẾM</h4>
      <div class="row">
        <div class="col-sm-9">
          <canvas id="canvas" height="150" width="600"></canvas>
        </div>
        <div class="col-xs-2 col-sm-2" style="margin:50px 0 0 40px">
          <div class="totalvisit">Tổng visit</div>
          <div class="organic">Organic search</div>
        </div>
        <script async="async" type="text/javascript">

         <?php 
         $labels = array();
         foreach($ga['headers'] as $header) {
          $labels[] = $this->mdate->week_name(strtotime($header));
        }
        $labels = implode('","', $labels);
        ?>
        var lineChartData = {
         labels : ["<?php echo $labels;?>"],
         datasets : [
         {
           label: "Tổng visit",
           fillColor : "rgba(220,220,220,0)",
           strokeColor : "rgba(220,220,220,1)",
           pointColor : "rgba(220,220,220,1)",
           pointStrokeColor : "#fff",
           pointHighlightFill : "#fff",
           pointHighlightStroke : "rgba(220,220,220,1)",
           <?php 
           $charData = array();
           $charData['ga:sessions'] = array();
           $charData['Organic Search'] = array();
           // prd($ga['total']);
           foreach($ga['headers'] as $header) {
            $charData['ga:sessions'][] = (isset($ga['results'][$header]['ga:sessions']) ? $ga['results'][$header]['ga:sessions'] : 0);

            $charData['Organic Search'][] = (isset($ga['results'][$header]['Organic Search']) ? $ga['results'][$header]['Organic Search'] : 0);
          }
          $charData['ga:sessions'] = implode(',', $charData['ga:sessions']);
          $charData['Organic Search'] = implode(',', $charData['Organic Search']);
          ?>
          data : [<?php echo $charData['ga:sessions'];?>]
        },
        {
         label: "Organic search",
         fillColor : "rgba(247,70,74,0)",
         strokeColor : "rgba(247,70,74,1)",
         pointColor : "rgba(247,70,74,1)",
         pointStrokeColor : "#fff",
         pointHighlightFill : "#fff",
         pointHighlightStroke : "rgba(247,70,74,1)",
         data : [<?php echo $charData['Organic Search'];?>]
       }
       ]

     }




   </script> 
 </div>
</div>
<div class="clear10"></div>

<?php if($ga_weeks): ?>
  <h4 class="text-center">CHỈ SỐ THÁNG <?php echo date('m',$end_date);?></h4>
  <div class="clear10"></div>
  <div class="col-sm-8 mresult col-xs-12">

    <div class="clear10"></div>
    <h4 class="text-center">CHỈ SỐ THEO TUẦN</h4>
    <div class="row" >
      <div class="col-sm-4 col-xs-4">
        <div class="line bg-warning">&nbsp;</div>
        <div class="line">Tổng visit</div>
        <div class="line text-organic">Organic Search</div>
        <div class="line text-direct">Direct</div>
        <div class="line text-referral">Referral</div>
        <div class="line text-social">Social</div>
        <div class="line">Page/ Visit</div>
        <div class="line">Bounce Rate</div>
        <div class="line">Time on site</div>
      </div>

      <?php 
      $ga_week_total = $ga_weeks['total'];
      unset($ga_weeks['total']);
      foreach($ga_weeks as $ga_week_key => $ga_week_value) {
        ?>
        <div class="col-sm-2 col-xs-2">
         <div class="line bg-warning text-center"><strong>Tuần <?php echo $ga_week_key;?></strong></div>
         <div class="line text-center"><?php echo $ga_week_value['ga:sessions'];?></div>
         <div class="line text-center"><strong><?php echo $ga_week_value['Organic Search'];?></strong></div>
         <div class="line text-center"><?php echo $ga_week_value['Direct'];?></div>
         <div class="line text-center"><?php echo $ga_week_value['Referral'];?></div>
         <div class="line text-center"><?php echo $ga_week_value['Social'];?></div>
         <div class="line text-center"><?php echo round($ga_week_value['ga:pageviewsPerSession'],2);?></div>
         <div class="line text-center"><?php echo $ga_week_value['ga:bounceRate'];?></div>
         <div class="line text-center"><?php echo $ga_week_value['ga:avgSessionDuration'];?></div>
       </div>
       <?php } ?>
       <div class="clear10"></div>
     </div>
     <div class="clear10"></div>
   </div>
   <div class="col-sm-1 col-xs-1"></div>
   <div class="col-sm-3 dotnut col-xs-12">
    <div id="canvas-holder-1" class="col-sm-8 col-centered">
      <canvas id="chart-pie" width="200" height="200"/>
    </div>
    <div class="col-sm-12 ">
      <div class="organic">Search Engine</div>
      <div class="direct">Direct</div>
      <div class="referral" >Referral</div>
      <div class="social" >Social</div>
    </div>
    <script>

      var pieData = [
      {
       value: <?php echo $ga_week_total['Organic Search'];?>,
       color:"#F7464A",
       highlight: "#FF5A5E",
       label: "Search Engine"
     },
     {
       value: <?php echo $ga_week_total['Direct'];?>,
       color: "#46BFBD",
       highlight: "#5AD3D1",
       label: "Direct"
     },
     {
       value: <?php echo $ga_week_total['Referral'];?>,
       color: "#FDB45C",
       highlight: "#FFC870",
       label: "Referral"
     },
     {
       value: <?php echo $ga_week_total['Social'];?>,
       color: "#949FB1",
       highlight: "#A8B3C5",
       label: "Social"
     }

     ];




   </script>
   <script>
     window.onload = function(){
      var ctx = document.getElementById("chart-area").getContext("2d");
      window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});

      var ctx2 = document.getElementById("canvas").getContext("2d");
      window.myLine = new Chart(ctx2).Line(lineChartData, {responsive: true});

      var ctx3 = document.getElementById("chart-pie").getContext("2d");
      window.myPie = new Chart(ctx3).Pie(pieData, {responsive : true});

    }
  </script>
  <div class="clear"></div>
</div>
<div class="clear10"></div>

<?php endif;?>


<?php if($post->post_content !='') : ?>
  <div class="col-sm-12 success" >
    <h4 class="text-center">ĐÁNH GIÁ TÌNH TRẠNG CỦA NHÂN VIÊN QUẢN LÝ</h4>
    <div class="clear"></div>
    <div class="head row">
      <div class="ordinal">Stt</div>
      <div class="issue">Tình trạng</div>
      <div class="clear"></div>
    </div>
    <?php 
    $rows = explode("\n", $post->post_content); 
    if($rows){
      foreach($rows as $i => $row)
      { 
        if($row == '')
          continue;
        ?>
        <div class="row">
          <div class="ordinal"><?php echo ++$i;?></div>
          <div class="issue"><?php echo $row;?>
          </div>
        </div>
        <?php }
      }
      ?>
    </div>
  <?php endif;?>

  <div class="clear10"></div>
  <div class="modal-footer"> Mọi chi tiết vui lòng liên hệ: Ms.Hồng <a href="tel:0935366615">0935 366 615</a> | <a href="mailto:hongvt@sccom.vn">hongvt@sccom.vn</a> - Mr.Hải <a href="tel:0935366615">0935 366 615</a> | <a href="mailto:haitm@sccom.vn">haitm@sccom.vn</a> </div>
</div>
</body>
</html>
