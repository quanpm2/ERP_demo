<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Adsplus.vn - Thông báo thay đổi nhân sự kinh doanh</title>
</head>
<body>
<div style="margin:0px auto">
	<p>Kính gửi quý khách!</p>
	<p>Trước hết, Công ty CP Quảng Cáo Cổng Việt Nam (Adsplus.vn) xin cám ơn các quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi trong thời gian qua. </p>
	<p>Để thuận lợi trong việc hỗ trợ và tránh nhầm lẫn thông tin, chúng tôi xin thông báo kể từ ngày 13/6/2017, bạn Đặng Thị Mỹ Nga đã chính thức ngừng công tác tại Adsplus, mọi thông tin liên quan quý khách vui lòng liên hệ với anh Triệu Minh Hải, trưởng phòng kinh doanh của Adsplus theo thông tin sau để được xử lý và hỗ trợ:</p>
	<p>•	Điện thoại: <b><a href="tel:0972899723"> 0972 899 723 (Zalo/Viber)</a></b></p>
	<p>•	Email: <a href="mailto:haitm@adsplus.vn"><b>haitm@adsplus.vn</b></a></p>
	<p>•	Skype: <b><a href="skype:live:trieuhai24h?call">trieuhai24h</a></b></p>

	<p>Một lần nữa cám ơn quý khách đã tin tưởng và hợp tác với Adsplus.</p>
</div>	
</body> 
</html>