<div class="col-sm-12 result">
    <div class="" >
      <h4 class="text-center">SỐ LiỆU CHI TiẾT</h4>
      <div class="row head">
        <div class="col-sm-2 col-xs-2">Ngày</div>
        <?php foreach($ga['headers'] as $header) { ?>
        <div class="col-sm-1 col-xs-1 text-center"><?php echo date('d/m', strtotime($header));?></div>
        <?php } ?>
        <div class="col-sm-3 col-xs-3">Tổng</div>
      </div>
      <div class="row bg-warning">
        <div class="col-sm-2 col-xs-2">Thứ</div>
        <?php foreach($ga['headers'] as $header) { ?>
        <div class="col-sm-1 col-xs-1 text-center"><?php echo $this->mdate->week_name(strtotime($header), array('CN','T2','T3','T4','T5','T6','T7'));?></div>
        <?php } ?>
        <div class="col-sm-3 col-xs-3">&nbsp;</div>
      </div>
      <div class="result">
      <?php 
     
      foreach($ga['total'] as $key=> $total) { ?>
        <div class="row">
          <div class="col-sm-2 col-xs-2"><?php echo @$ga_label[$key];?></div>
          <?php foreach($ga['headers'] as $header) {
            $val = isset($ga['results'][$header][$key]) ? $ga['results'][$header][$key] : 0;
            ?>
          <div class="col-sm-1 col-xs-1 text-center"><?php echo $val;?></div>
          <?php } ?> 
          <div class="col-sm-1 col-xs-1 text-center"><?php echo $total;?></div>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="clear10"></div>
  </div>