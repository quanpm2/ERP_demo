<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>

<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr> <td height="15"></td> </tr>
        <tr>
            <td align="center">
                <table style="background-repeat: no-repeat;margin-bottom:20px;" class="m_4810850691267720898table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#1d89e4">
                    <tbody>
                        <tr>
                            <td align="center" style="border-top:3px solid #f58220;">
                                <table class="m_4810850691267720898table-full" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="center">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding-bottom: 0px;padding-top:0;padding-bottom:0px;font-family:open sans,arial,sans-serif;font-size:27px;color:#ffffff;line-height:32px;width:50%;">
                                                <img src="https://event.adsplus.vn/wp-content/uploads/2017/12/email3.png" alt="img" class="CToWUds">

                                                <p style="padding-left: 31px;margin: 0;" align="center"><b><span style="color:#ffcb05;"> 
                                                <?php echo $gender ?> <?php echo $display_name ?> (nhóm <?php echo $group_name ?>)</span> <br> vừa ký được hợp đồng <?php echo $contract_type; ?></b></p>


                                                <img src="<?php echo $user_avatar; ?>" alt="img" class="CToWUds" style="max-width: 220px;border-radius: 50%;padding-top:15px;padding-bottom:15px;">

                                            </td>

                                        </tr>
                                        <tr bgcolor="#ffffff" style="padding-top:10px;padding-bottom:10px;">
                                            <td bgcolor="#ffffff" style="padding-top:10px;padding-bottom:10px;">
                                                <strong style="font-family: open sans,arial,sans-serif;font-size:18px;margin-left:15px;padding-left:15px;padding-top:10px;padding-bottom:10px;" bgcolor="#ffffff;">Giá trị hợp đồng:<span style="color:#f58220;padding-top:10px;padding-bottom:10px;"> <?php echo $contract_value; ?> </span></strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <tr>

                                </tr>

                    </tbody>
                </table>
                </td>
                </tr>

                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left" bgcolor="#ecf0f1">
                    <tbody>
                        <tr>
                            <td height="0" style="font-size:0;line-height:0px;border-collapse:collapse">
                                <p style="padding-left:24px">&nbsp;</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>

    </tbody>
</table>