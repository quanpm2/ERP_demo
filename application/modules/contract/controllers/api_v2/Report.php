<?php defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'contract/category_m';
        $this->autoload['models'][] = 'contract/term_categories_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'contract/contract_report_m';
        $this->autoload['models'][] = 'log_m';

        parent::__construct();

        $this->load->config('contract/receipt');
        $this->load->config('contract/contract');
    }

    /**
     * API dữ liệu các đợt thanh toán
     */
    public function adsplus_get()
    {
        $this->load->model([ 'googleads/googleads_m', 'googleads/googleads_kpi_m' ]);
        $this->load->config('contract/receipt');

        $this->googleads_m
        ->select('term.*, term_posts.*')
        ->join('term_posts', 'term_posts.term_id = term.term_id')
        ->join('posts', 'posts.post_id = term_posts.post_id AND posts.post_status = "paid"')
        ->where_in('posts.post_type', $this->config->item('originalPaymentTypes', 'receipt'))
        ->group_by('term.term_id')
        ->set_term_type();

        parent::get('startAt', TRUE) AND $this->googleads_m->where('posts.end_date >=', start_of_day(parent::get('startAt', TRUE)));
        parent::get('endAt', TRUE) AND $this->googleads_m->where('posts.end_date <=', end_of_day(parent::get('endAt', TRUE)));

        $terms = $this->googleads_m->get_all();

        foreach ($terms as &$term)
        {
            $contract = (new Googleads_m())->set_contract($term);

            // Tính giá trị đã thanh toán
            $payment_amount = (int) get_term_meta_value($term->term_id, 'payment_amount');

            /* Giá trị hợp đồng */
            $contract_value = (int) $contract->calc_contract_value();

            /* Calculates the Actual budget Money from Payment money. */
            $budget     = $contract->get_behaviour_m()->calc_actual_budget();

            $vat_amount = $contract->get_behaviour_m()->cacl_vat_amount();
            $fct        = $contract->get_behaviour_m()->get_fct_tax();
            $fct_amount = $contract->get_behaviour_m()->calc_fct($fct, $budget);

            $payment_service_fee    = $contract->get_behaviour_m()->calc_payment_service_fee();

            $term->customer_id  = NULL;
            $mcm_client_id      = get_term_meta_value($term->term_id, 'mcm_client_id');
            empty($mcm_client_id) OR $term->customer_id = get_term_meta_value($mcm_client_id, 'customer_id');

            $term->contract_code    = get_term_meta_value($term->term_id, 'contract_code');
            $term->contract_begin   = (int) get_term_meta_value($term->term_id, 'contract_begin');
            $term->contract_end     = (int) get_term_meta_value($term->term_id, 'contract_end');
            $term->contract_budget  = $contract->get_behaviour_m()->calc_budget();
            $term->service_fee      = (int) get_term_meta_value($term->term_id, 'service_fee');

            $term->contract_budget_payment_type = get_term_meta_value($term->term_id, 'contract_budget_payment_type');
            $term->payment_amount = (int) $payment_amount;
            $term->budget = (int) $budget;
            $term->vat_amount = (int) $vat_amount;
            $term->fct_amount = (int) $fct_amount;
            $term->payment_service_fee = (int) $payment_service_fee;

            $term->staff_advertise  = null;
            $user_ids               = array();
            $service_type           = get_term_meta_value($term->term_id, 'service_type');

            if( $kpis = $this->googleads_kpi_m->get_kpis($term->term_id, 'users',$service_type))
            {
                foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;
            }
            
            empty($user_ids) OR $term->staff_advertise = implode(', ', array_map(function($user_id){ return $this->admin_m->get_field_by_id($user_id,'user_email'); }, $user_ids));

            $term->staff_business  = get_term_meta_value($term->term_id, 'staff_business');
            $term->staff_business AND $term->staff_business = $this->admin_m->get_field_by_id($term->staff_business, 'user_email');

            $term->customer = null;
            $customers      = $this->term_users_m->get_the_users($term->term_id, ['customer_person', 'customer_company']);
            $customers AND $term->customer = implode(', ', array_column($customers, 'display_name'));
            
            // CALLBACK : SALE USER
            $term = (array) $term;
        }

        parent::response(['status'=>TRUE, 'data' => $terms]);
    }

    /**
     * { function_description }
     */
    public function revenue_get()
    {
        parent::response($this->calculate());
    }

    /**
     * { function_description }
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function export_get()
    {
        $data = $this->calculate();

        // $title = array(
        // 'Mã hợp đồng',
        // 'Ngày bắt đầu',
        // 'Ngày kết thúc',
        // 'Ngày kích hoạt',
        // 'Loại HĐ',
        // 'Ngân sách',
        // 'Phí dịch vụ',
        // 'Thuế nhà thầu',
        // 'VAT',
        // 'Số tiền thanh toán',
        // 'Số kỳ thanh toán',
        // 'Số tiền 1 kỳ thanh toán',
        // 'Ngân sách',
        // 'Phí dịch vụ',
        // 'Thuế nhà thầu',
        // 'VAT',
        // 'Ngân sách',
        // 'Phí dịch vụ',
        // 'Thuế nhà thầu',
        // 'VAT',
        // 'Dự thu',
        // 'Spent');
        // $this->load->library('table');
        // $this->table->set_heading($title);
        // echo $this->table->generate($data);
        // prd($data);
        // die();


        $title          = 'Export dữ liệu Hợp Đồng & Khách hàng đang thực hiện trong tháng ' . my_date(time(), 'Y-m-d');
        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));
        
        $sheet = $spreadsheet->getActiveSheet();

        $rowIndex = 1;

        $spreadsheet->getActiveSheet()->mergeCells("A{$rowIndex}:G{$rowIndex}");
        $spreadsheet->getActiveSheet()->mergeCells("H{$rowIndex}:M{$rowIndex}");
        $spreadsheet->getActiveSheet()->mergeCells("N{$rowIndex}:Q{$rowIndex}");
        $spreadsheet->getActiveSheet()->mergeCells("R{$rowIndex}:U{$rowIndex}");

        $sheet->setCellValue("A{$rowIndex}", 'Thông tin hợp đồng');
        $sheet->setCellValue("H{$rowIndex}", 'Giá trị hợp đồng');
        $sheet->setCellValue("N{$rowIndex}", 'Số tiền đã thanh toán');
        $sheet->setCellValue("R{$rowIndex}", 'Số tiền còn lại');

        $rowIndex++;

        $columns = array(
            'customer'                  => [ 'type' => 'string', 'label' => 'Khách hàng' ],
            'contract_code'             => [ 'type' => 'string', 'label' => 'Mã hợp đồng' ],
            'contract_status'           => [ 'type' => 'string', 'label' => 'Trạng thái' ],
            'contract_begin'            => [ 'type' => 'timestamp', 'label' => 'Ngày bắt đầu' ],
            'contract_end'              => [ 'type' => 'timestamp', 'label' => 'Ngày kết thúc' ],
            'start_service_time'        => [ 'type' => 'timestamp', 'label' => 'Ngày kích hoạt' ],
            'has_vat'                   => [ 'type' => 'string', 'label' => 'Loại HĐ' ],
            'contract_budget'           => [ 'type' => 'number', 'label' => 'Ngân sách' ],
            'service_fee'               => [ 'type' => 'number', 'label' => 'Phí dịch vụ' ],
            'fct'                       => [ 'type' => 'string', 'label' => 'Thuế nhà thầu' ],
            'vat'                       => [ 'type' => 'string', 'label' => 'VAT' ],
            'number_of_payments'        => [ 'type' => 'number', 'label' => 'Số kỳ thanh toán' ],
            'amount_per_payment'        => [ 'type' => 'number', 'label' => 'Số tiền 1 kỳ thanh toán' ],
            'paymented_budget'          => [ 'type' => 'number', 'label' => 'Ngân sách' ],
            'paymented_service_fee'     => [ 'type' => 'number', 'label' => 'Phí dịch vụ' ],
            'paymented_fct'             => [ 'type' => 'number', 'label' => 'Thuế nhà thầu' ],
            'paymented_vat'             => [ 'type' => 'number', 'label' => 'VAT' ],
            'must_payment_budget'       => [ 'type' => 'number', 'label' => 'Ngân sách' ],
            'must_payment_service_fee'  => [ 'type' => 'number', 'label' => 'Phí dịch vụ' ],
            'must_payment_fct'          => [ 'type' => 'number', 'label' => 'Thuế nhà thầu' ],
            'must_payment_vat'          => [ 'type' => 'number', 'label' => 'VAT' ],
            'invs_amount'               => [ 'type' => 'number', 'label' => 'Dự thu' ],
            'spent'                     => [ 'type' => 'number', 'label' => 'Spent' ],
        );        

        $sheet->fromArray(array_column(array_values($columns), 'label'), NULL, "A{$rowIndex}");

        $rowIndex++;

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column)
            {
                $value = $item[$key];
                switch ($column['type'])
                {
                    case 'number': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        if(empty($value)) break;
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $value);
                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }

    /**
     * { function_description }
     *
     * @return     array  ( description_of_the_return_value )
     */
    protected function calculate()
    {
        $cache_key  = 'api-v2-contract-report-' . my_date(start_of_day(), 'Y-m-d');
        $data       = $this->scache->get($cache_key);
        if(! empty($data) && 1==2) return $data;

        $states = [ 'waitingforapprove','publish', 'pending' ];

        $this->load->model('googleads/googleads_m');

        $contract_cache_key = 'thonh09-googleads-amount-';
        $contracts = $this->scache->get($contract_cache_key);
        if( ! $contracts || 1==1)
        {
            $contracts = $this->googleads_m->set_term_type()->where_in('term.term_status', $states)->order_by('term_id', 'desc')->as_array()
            // ->where('term_id', 25012)
            ->get_all();
            $this->scache->write($contracts, $contract_cache_key);
        }

        $data = array();
        foreach ($contracts as $contract)
        {
            $term_id = $contract['term_id'];
            $_contract = new contract_m();
            $_contract->set_contract( (object) $contract);
            $_behaviour_m = $_contract->get_behaviour_m();

            $customer = $this->term_users_m->get_the_users($term_id, [ 'customer_person', 'customer_company' ]);
            $customer AND $customer = reset($customer);

            $contract_code      = (string) get_term_meta_value($contract['term_id'], 'contract_code');
            $contract_value     = (double) get_term_meta_value($contract['term_id'], 'contract_value');
            $contract_begin     = (string) get_term_meta_value($contract['term_id'], 'contract_begin')  AND $contract_begin = my_date($contract_begin, 'd/m/Y');
            $contract_end       = (string) get_term_meta_value($contract['term_id'], 'contract_end')    AND $contract_end = my_date($contract_end, 'd/m/Y');
            $start_service_time = (double) get_term_meta_value($contract['term_id'], 'start_service_time');
            $vat                = (double) get_term_meta_value($contract['term_id'], 'vat');
            $vat_amount         = $contract_value*div($vat, 100);


            $contract_budget    = (double) get_term_meta_value($contract['term_id'], 'contract_budget');
            
            $service_fee        = round((int) get_term_meta_value($contract['term_id'], 'service_fee'));
            $fct                = (double) get_term_meta_value($contract['term_id'], 'fct');
            $fct_amount = $_behaviour_m->calc_fct(div($fct, 100), $contract_budget);

            $number_of_payments = (int) get_term_meta_value($contract['term_id'], 'number_of_payments');
            $payment_amount     = (double) get_term_meta_value($contract['term_id'], 'payment_amount');

            try
            {
                $paymented_actual_budget    = max([(double) $_behaviour_m->calc_actual_budget(), 0]);
                'customer' == get_term_meta_value( $term_id, 'contract_budget_payment_type') AND $paymented_actual_budget = (int) get_term_meta_value($term_id, 'actual_budget');
            }
            catch (Exception $e) { $paymented_actual_budget = 0; }

            try
            {
                $paymented_service_fee  = max([(double) $_behaviour_m->calc_payment_service_fee(), 0]);
                $paymented_service_fee  = round($paymented_service_fee, -1);
            }
            catch (Exception $e) { $paymented_service_fee = 0; }

            try
            {
                $paymented_fct = max([(double) $_behaviour_m->get_fct_tax(), 0]);
            }
            catch (Exception $e) { $paymented_fct = 0; }

            try
            {
                $paymented_fct_amount = max([(double) $_behaviour_m->calc_fct($fct, $paymented_actual_budget), 0]);
            }
            catch (Exception $e) { $paymented_fct_amount = 0; }

            try
            {
                $paymented_vat_amount = max([(double) $_behaviour_m->cacl_vat_amount(), 0]);
            }
            catch (Exception $e) { $paymented_vat_amount = 0; }

            $invs_amount            = (int) get_term_meta_value($contract['term_id'],'invs_amount');
            $actual_result          = (double) get_term_meta_value($contract['term_id'], 'actual_result');
            $account_currency_code  = get_term_meta_value($contract['term_id'], 'account_currency_code');

            $exchange_rate = get_exchange_rate($account_currency_code, $contract['term_id']);
            ('VND' != $account_currency_code) AND $actual_result *= $exchange_rate;

            $contract_type = 'ADSPLUS thanh toán theo hợp dồng';

            if('customer' == get_term_meta_value($term_id, 'contract_budget_payment_type'))
            {
                $contract_type = 'Khách tự thanh toán';
                'behalf' == get_term_meta_value($term_id, 'contract_budget_customer_payment_type') 
                AND $contract_type = 'ADSPLUS thu & chi hộ';
            }

            $_item = [
                'customer'          => $customer->display_name ?: null,
                'contract_code'     => $contract_code,
                'contract_status'   => $this->config->item($contract['term_status'], 'contract_status'),
                'contract_begin'    => $contract_begin,
                'contract_end'      => $contract_end,
                'start_service_time'=> $start_service_time,
                'has_vat'           => $contract_type,
                'contract_budget'   => $contract_budget,
                'service_fee'       => $service_fee,
                'fct'               => $fct . '%',
                'vat'               => $vat . '%',
                'payment_amount'    => $payment_amount,
                'number_of_payments' => $number_of_payments,
                'amount_per_payment' => div($contract_value, $number_of_payments),
                'paymented_budget'        => $paymented_actual_budget,
                'paymented_service_fee'   => $paymented_service_fee,
                'paymented_fct'           => $paymented_fct_amount,
                'paymented_vat'           => $paymented_vat_amount,
                'must_payment_budget'        => $contract_budget - $paymented_actual_budget,
                'must_payment_service_fee'   => (int) $service_fee - (int) $paymented_service_fee,
                'must_payment_fct'           => $fct_amount - $paymented_fct_amount,
                'must_payment_vat'           => $vat_amount - $paymented_vat_amount,
                'invs_amount'       => $invs_amount,
                'spent'             => $actual_result
            ];

            $data[] = $_item;
        }

        $this->scache->write($data, $cache_key, 3600);
        return $data;
    }

    /**
	 * Sends a mail notification payment.
	 *
	 * @return     JSON  The result
	 */
	public function send_payment_completed_post($id)
	{
        if( ! $this->receipt_m->set_post_type()->where([ 'post_status' => 'paid', 'post_id' => $id])->count_by())
        {
            parent::responseHandler(null, 'Phiếu thanh toán không tồn tại hoặc không hợp lệ.', 'error', 400);
        }

        if( ! has_permission('contract.notification_payment.manage'))
		{
            parent::responseHandler(null, 'Tài khoản của bạn không đủ quyền hạn để thực hiện tác vụ này .', 'error', 401);
		}

		$has_send_mail_payment = (bool) get_post_meta_value($id, 'has_send_mail_payment');
		if($has_send_mail_payment) 
		{
            parent::responseHandler(null, 'Email đã được gửi thành công.');
		}		

		$this->load->model('contract/contract_report_m');
		$status = $this->contract_report_m->send_email_successful_payment($id);
		if( ! $status)
		{
            parent::responseHandler(null, 'Quá trình xử lý bị gián đoạn , xin vui lòng thử gửi lại sau 5 phút!', 'error', 500);
		}

		/* Send SMS thông báo khách hàng */
		$this->contract_report_m->send_sms_successful_payment($id);

		update_post_meta($id, 'has_send_mail_payment', 1);

        parent::responseHandler(null, "E-Mail đã được gửi thành công.");
	}
}
/* End of file Index.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Index.php */