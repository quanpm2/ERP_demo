<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetStopContractReport extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'report_m';
        $this->autoload['models'][] = 'term_m';
        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'base_contract_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'staffs/department_m';
        $this->autoload['models'][] = 'staffs/user_group_m';

        parent::__construct();

        $this->load->config('contract');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('reports.spend.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults           = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args               = wp_parse_args(parent::get(), $defaults);
        $pagination_config  = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        // Filtering 
        $this->filtering($args);

        $data = $this->datatable_builder
            ->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])
            ->add_search('display_name', ['placeholder' => 'Tên khách hàng'])
            ->add_search('start_service_time', ['placeholder' => 'Ngày bắt đầu dịch vụ', 'class' => 'form-control set-datepicker'])
            ->add_search('end_service_time', ['placeholder' => 'Ngày kết thúc dịch vụ', 'class' => 'form-control set-datepicker'])
            ->add_search('actual_result', ['placeholder' => 'Chi tiêu'])

            ->add_column('contract_code', array('title' => 'Mã hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('cid', array('title' => 'Mã khách hàng', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('display_name', array('title' => 'Tên khách hàng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('start_service_time', array('title' => 'Ngày bắt đầu dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('end_service_time', array('title' => 'Ngày kết thúc dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('actual_result', array('title' => 'Chi tiêu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('staff_techs', array('title' => 'Kỹ thuật', 'set_select' => FALSE, 'set_order' => FALSE))

            ->add_column_callback('term_id', function ($data, $row_name) {
                $term_id = $data['term_id'];

                // Get cid
                $data['user_id'] = '';
                $data['display_name'] = '';
                $data['cid'] = '';
                $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company', 'system']);
                if (!empty($customers)) {
                    $customer = end($customers);
                    $data['user_id'] = $customer->user_id;
                    $data['display_name'] = $customer->display_name;
                    $data['cid'] = get_user_meta_value($customer->user_id, 'cid') ?: cid($customer->user_id, $customer->user_type);
                }

                // Get contract_code
                $contract_code = get_term_meta_value($term_id, 'contract_code');
                $data['contract_code'] = $contract_code ?: '';

                // Get start_service_time
                $start_service_time = get_term_meta_value($term_id, 'start_service_time');
                $data['start_service_time'] = $start_service_time ? my_date($start_service_time, 'd/m/Y') : '';

                // Get contract_end
                $is_service_end = is_service_end($term_id);
                $end_service_time = get_term_meta_value($term_id, 'end_service_time');
                $data['end_service_time'] = $is_service_end ? ($end_service_time ? my_date($end_service_time, 'd/m/Y') : '') : '';

                // Get actual_result
                $actual_result = get_term_meta_value($term_id, 'actual_result') ?: 0;
                $data['actual_result'] = '' . $actual_result;

                // Get staff_techs
                $data['staff_techs'] = '';

                $model_kpi = '';
                switch ($data['term_type']) {
                    case 'courseads':
                        $this->load->model('courseads/courseads_kpi_m');
                        $model_kpi = 'courseads_kpi_m';
                        break;
                    case 'banner':
                        $this->load->model('banner/banner_kpi_m');
                        $model_kpi = 'banner_kpi_m';
                        break;
                    case 'facebook-ads':
                        $this->load->model('facebookads/facebookads_kpi_m');
                        $model_kpi = 'facebookads_kpi_m';
                        break;
                    case 'google-ads':
                        $this->load->model('googleads/googleads_kpi_m');
                        $model_kpi = 'googleads_kpi_m';
                        break;
                    case 'hosting':
                        $this->load->model('hosting/hosting_kpi_m');
                        $model_kpi = 'hosting_kpi_m';
                        break;
                    case 'onead':
                        $this->load->model('onead/onead_kpi_m');
                        $model_kpi = 'onead_kpi_m';
                        break;
                    case 'oneweb' || 'webgeneral':
                        $this->load->model('webgeneral/webgeneral_kpi_m');
                        $model_kpi = 'webgeneral_kpi_m';
                        break;
                    case 'seo-traffic':
                        $this->load->model('seotraffic/seotraffic_kpi_m');
                        $model_kpi = 'seotraffic_kpi_m';
                        break;
                    case 'webdoctor':
                        $this->load->model('webdoctor/webdoctor_kpi_m');
                        $model_kpi = 'webdoctor_kpi_m';
                        break;
                    case 'weboptimize':
                        $this->load->model('weboptimize/weboptimize_kpi_m');
                        $model_kpi = 'weboptimize_kpi_m';
                        break;
                    default:
                        break;
                }

                $staff_techs = [];
                $kpis = $this->$model_kpi->get_kpi_value($term_id, 'users');
                foreach ($kpis as $user_id => $value) {
                    $staff_techs[] = $this->admin_m->get_field_by_id($user_id, 'display_name');
                }
                $data['staff_techs'] = implode(', ', $staff_techs);

                return $data;
            }, FALSE)

            ->from('term')
            ->select('term.term_id, term.term_type')
            ->where("term.term_status IN ('ending', 'liquidation')")
            ->group_by('term.term_id')
            ->order_by('term.term_id', 'DESC')
            ->generate($pagination_config, 'JSON');

        // remove limit in query string
        $last_query = $this->datatable_builder->last_query();
        $pos = strpos($last_query, 'LIMIT');
        if (FALSE !== $pos) $last_query = mb_substr($last_query, 0, $pos);
        $subheadings_data = $this->db->query($last_query)->result_array();
        $subheadings = array_reduce($subheadings_data, function ($result, $item) {
            $actual_result = (int)get_term_meta_value($item['term_id'], 'actual_result');
            $result['actual_result'] += $actual_result;

            return $result;
        }, [
            'actual_result' => 0,
        ]);
        $subheadings = array_map(function ($item) {
            $item .= '';
            return $item;
        }, $subheadings);
        $data['subheadings'] = $subheadings;

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export($last_query);
            return TRUE;
        }

        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering($args = array())
    {
        restrict('reports.spend.access');

        $args = parent::get();
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // start_time FILTERING & SORTING
        $filter_start_time = $args['start_time'] ?? FALSE;
        if ($filter_start_time) {
            $alias_start_service_time = uniqid('m_start_service_time_');
            $this->datatable_builder->join("termmeta $alias_start_service_time", "$alias_start_service_time.term_id = term.term_id AND $alias_start_service_time.meta_key = 'start_service_time'", 'LEFT');
            $this->datatable_builder->where("$alias_start_service_time.meta_value >=", $filter_start_time);
        }

        // end_time FILTERING & SORTING
        $filter_end_time = $args['end_time'] ?? FALSE;
        if ($filter_end_time) {
            $alias_end_service_time = uniqid('m_end_service_time_');
            $this->datatable_builder->join("termmeta $alias_end_service_time", "$alias_end_service_time.term_id = term.term_id AND $alias_end_service_time.meta_key = 'end_service_time'", 'LEFT');
            $this->datatable_builder->where("$alias_end_service_time.meta_value <=", $filter_end_time);
        }

        // display_name FILTERING & SORTING
        $filter_display_name = $args['where']['display_name'] ?? FALSE;
        if ($filter_display_name) {
            $this->datatable_builder->like('display_name', $filter_display_name);
        }

        $sort_display_name = $args['order_by']['display_name'] ?? FALSE;
        if ($sort_display_name) {
            $this->datatable_builder->order_by('display_name', $sort_display_name);
        }

        // contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $aliasMetaContractCode = uniqid('m_contract_code_');

            $this->datatable_builder->join("termmeta $aliasMetaContractCode", "$aliasMetaContractCode.term_id = term.term_id AND $aliasMetaContractCode.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("$aliasMetaContractCode.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder->order_by("$aliasMetaContractCode.meta_value", $sort_contract_code);
            }
        }

        // start_service_time FILTERING & SORTING
        $filter_start_service_time = $args['where']['start_service_time'] ?? FALSE;
        $sort_start_service_time = $args['order_by']['start_service_time'] ?? FALSE;
        if ($filter_start_service_time || $sort_start_service_time) {
            $alias_start_service_time = uniqid('m_start_service_time_');

            $this->datatable_builder->join("termmeta $alias_start_service_time", "$alias_start_service_time.term_id = term.term_id AND $alias_start_service_time.meta_key = 'start_service_time'", 'LEFT');

            if ($filter_start_service_time) {
                $dates = explode(' - ', $filter_start_service_time);

                $this->datatable_builder
                    ->where("$alias_start_service_time.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("$alias_start_service_time.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_start_service_time) {
                $this->datatable_builder->order_by("CAST($alias_start_service_time.meta_value AS UNSIGNED)", $sort_start_service_time);
            }
        }

        // end_service_time FILTERING & SORTING
        $filter_end_service_time = $args['where']['end_service_time'] ?? FALSE;
        $sort_end_service_time = $args['order_by']['end_service_time'] ?? FALSE;
        if ($filter_end_service_time || $sort_end_service_time) {
            $alias_end_service_time = uniqid('m_end_service_time_');

            $this->datatable_builder->join("termmeta $alias_end_service_time", "$alias_end_service_time.term_id = term.term_id AND $alias_end_service_time.meta_key = 'end_service_time'", 'LEFT');

            if ($filter_end_service_time) {
                $dates = explode(' - ', $filter_end_service_time);

                $this->datatable_builder
                    ->where("$alias_end_service_time.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("$alias_end_service_time.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_end_service_time) {
                $this->datatable_builder->order_by("$alias_end_service_time.meta_value", $sort_end_service_time);
            }
        }

        // actual_result FILTERING & SORTING
        $filter_actual_result = $args['where']['actual_result'] ?? FALSE;
        $sort_actual_result = $args['order_by']['actual_result'] ?? FALSE;
        if ($filter_actual_result || $sort_actual_result) {
            $alias_actual_result = uniqid('m_actual_result_');

            $this->datatable_builder->join("termmeta $alias_actual_result", "$alias_actual_result.term_id = term.term_id AND $alias_actual_result.meta_key = 'actual_result'", 'LEFT');

            if ($filter_actual_result) {
                $this->datatable_builder
                    ->where("CAST($alias_actual_result.meta_value AS UNSIGNED) >=", $filter_actual_result);
            }

            if ($sort_actual_result) {
                $this->datatable_builder->order_by("CAST($alias_actual_result.meta_value AS UNSIGNED)", $sort_actual_result);
            }
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function export($query = '')
    {
        restrict('reports.spend.access');

        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $contracts = $this->contract_m->query($query)->result();
        if (!$contracts) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $this->load->library('excel');
        $cacheMethod    = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings  = array('memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("ADSPLUS.VN")->setLastModifiedBy("ADSPLUS.VN")->setTitle(uniqid('Báo cáo hợp đồng đã đã kết thúc __'));
        $objPHPExcel->setActiveSheetIndex(0);

        $objWorksheet   = $objPHPExcel->getActiveSheet();

        $headings = array(
            'Mã hợp đồng',
            'Mã khách hàng',
            'Tên khách hàng',
            'Ngày bắt đầu',
            'Ngày kết thúc',
            'Chi tiêu',
            'Kỹ thuật',
        );
        $objWorksheet->fromArray($headings, NULL, 'A1');

        $row_index = 2;
        foreach ($contracts as $key => &$contract) {
            $contract = (array) $contract;
            $term_id = $contract['term_id'];

            $row_number = $row_index + $key;
            $col_number = 0;

            $contract_code = get_term_meta_value($term_id, 'contract_code');
            $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $contract_code);
            $col_number++;

            // Get cid
            $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company', 'system']);
            $customer = end($customers);
            if (!empty($customer)) {
                $cid = get_user_meta_value($customer->user_id, 'cid') ?: cid($customer->user_id, $customer->user_type);
                $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $cid);
                $col_number++;

                $display_name = $customer->display_name;
                $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $display_name);
                $col_number++;
            } else {
                $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, '--');
                $col_number++;

                $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, '--');
                $col_number++;
            }


            // Get start_service_time
            $start_service_time = get_term_meta_value($term_id, 'start_service_time');
            $start_service_time = $start_service_time ? my_date($start_service_time, 'd/m/Y') : '--';
            $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $start_service_time);
            $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
            $col_number++;

            // Get end_service_time
            $end_service_time = get_term_meta_value($term_id, 'end_service_time');
            $end_service_time = $end_service_time ? my_date($end_service_time, 'd/m/Y') : '--';
            $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $end_service_time);
            $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
            $col_number++;

            // Get actual_result
            $actual_result = get_term_meta_value($term_id, 'actual_result');
            $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $actual_result ?: 0);
            $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
            $col_number++;

            // Get staff_techs
            $model_kpi = '';
            switch ($contract['term_type']) {
                case 'courseads':
                    $this->load->model('courseads/courseads_kpi_m');
                    $model_kpi = 'courseads_kpi_m';
                    break;
                case 'banner':
                    $this->load->model('banner/banner_kpi_m');
                    $model_kpi = 'banner_kpi_m';
                    break;
                case 'facebook-ads':
                    $this->load->model('facebookads/facebookads_kpi_m');
                    $model_kpi = 'facebookads_kpi_m';
                    break;
                case 'google-ads':
                    $this->load->model('googleads/googleads_kpi_m');
                    $model_kpi = 'googleads_kpi_m';
                    break;
                case 'hosting':
                    $this->load->model('hosting/hosting_kpi_m');
                    $model_kpi = 'hosting_kpi_m';
                    break;
                case 'onead':
                    $this->load->model('onead/onead_kpi_m');
                    $model_kpi = 'onead_kpi_m';
                    break;
                case 'oneweb' || 'webgeneral':
                    $this->load->model('webgeneral/webgeneral_kpi_m');
                    $model_kpi = 'webgeneral_kpi_m';
                    break;
                case 'seo-traffic':
                    $this->load->model('seotraffic/seotraffic_kpi_m');
                    $model_kpi = 'seotraffic_kpi_m';
                    break;
                case 'webdoctor':
                    $this->load->model('webdoctor/webdoctor_kpi_m');
                    $model_kpi = 'webdoctor_kpi_m';
                    break;
                case 'weboptimize':
                    $this->load->model('weboptimize/weboptimize_kpi_m');
                    $model_kpi = 'weboptimize_kpi_m';
                    break;
                default:
                    break;
            }

            $staff_techs = [];
            $kpis = $this->$model_kpi->get_kpi_value($term_id, 'users');
            foreach ($kpis as $user_id => $value) {
                $staff_techs[] = $this->admin_m->get_field_by_id($user_id, 'display_name');
            }
            $staff_techs = implode(', ', $staff_techs);
            $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $staff_techs ?: '--');
        }

        // We'll be outputting an excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $folder_upload = "files/contracts/";
        if (!is_dir($folder_upload)) {
            try {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $created_datetime   = my_date(time(), 'Y-m-d-H-i-s');
        $file_name          = "{$folder_upload}danh-sach-hop-dong-da-ket-thuc-{$created_datetime}.xlsx";

        try {
            $objWriter->save($file_name);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($file_name, NULL);
        return TRUE;
    }
}
/* End of file DatasetStopContractReport.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetStopContractReport.php */