<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetReceiptCautionDueSoonReport extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_m';
        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'staffs/department_m';
        $this->autoload['models'][] = 'staffs/user_group_m';

        parent::__construct();
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('reports.receipt_caution.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        $data = $this->data;
        $this->filtering();

        // Prepare data
        $relate_users = $this->admin_m->get_all_by_permissions('reports.receipt_caution.access');
        if (!$relate_users) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        if (is_array($relate_users)) {
            $contract = $this->contract_m->select('term.term_id')
                ->join("termmeta m_contract", "m_contract.term_id = term.term_id AND m_contract.meta_key = 'staff_business'", 'LEFT')
                ->where_in('m_contract.meta_value', $relate_users)
                ->as_array()
                ->get_all();
            $contract_ids = array_column($contract, 'term_id');

            $this->datatable_builder->where_in('total_report.term_id', $contract_ids);
        }

        $departmentData = $this->department_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $departmentData = $departmentData ? key_value($departmentData, 'term_id', 'term_name') : [];

        $userGroupData = $this->user_group_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $userGroupData = $userGroupData ? key_value($userGroupData, 'term_id', 'term_name') : [];

        $time = strtotime('+ 5days', time());

        // Data builder
        $data['content'] = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')

            ->add_search('cid', ['placeholder' => 'CID'])
            ->add_search('customer', ['placeholder' => 'Tên khách hàng'])
            ->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])
            ->add_search('staff_business', ['placeholder' => 'NVKD'])
            ->add_search('contract_value', ['placeholder' => 'Giá Trị trước VAT'])
            ->add_search('payment_amount', ['placeholder' => 'Đã thu'])
            ->add_search('payment_amount_remaining', ['placeholder' => 'Còn phải thu'])
            ->add_search('caution_amount', ['placeholder' => 'Số tiền bảo lãnh'])
            ->add_search('caution_end_time', ['placeholder' => 'Hạn bảo lãnh', 'class' => 'form-control set-datepicker'])
            ->add_search('note', ['placeholder' => 'Ghi chú'])
            ->add_search('department', ['content' => form_dropdown(['name' => 'where[department]', 'class' => 'form-control select2'], prepare_dropdown($departmentData, 'Phòng ban'), parent::get('where[department]'))])
            ->add_search('user_group', ['content' => form_dropdown(['name' => 'where[user_group]', 'class' => 'form-control select2'], prepare_dropdown($userGroupData, 'Nhóm'), parent::get('where[user_group]'))])

            ->add_column('cid', ['title' => 'CID', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('customer', ['title' => 'Khách hàng', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('contract_code', ['title' => 'Mã hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('contract_value', ['title' => 'Giá Trị trước VAT', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('payment_amount', ['title' => 'Đã thu', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('payment_amount_remaining', ['title' => 'Còn phải thu', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('caution_amount', ['title' => 'Số tiền bảo lãnh', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('caution_end_time', ['title' => 'Hạn bảo lãnh', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('note', ['title' => 'Ghi chú', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('staff_business', ['title' => 'NVKD', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('department', array('title' => 'Phòng ban', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('user_group', array('title' => 'Nhóm', 'set_select' => FALSE, 'set_order' => FALSE))

            ->add_column_callback('term_id', function ($data, $row_name) {
                $term_id = $data['term_id'];
                $post_id = $data['post_id'];

                $data['contract_code'] = get_term_meta_value($term_id, 'contract_code') ?? '--';

                // Get customer
                $data['user_id'] = '';
                $data['customer'] = '';
                $data['cid'] = '';
                $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company', 'system']);
                if (!empty($customers)) {
                    $customer = end($customers);
                    $data['user_id'] = $customer->user_id;
                    $data['customer'] = $customer->display_name;
                    $data['cid'] = get_user_meta_value($customer->user_id, 'cid') ?: cid($customer->user_id, $customer->user_type);
                }

                // Get contract_value
                $contract_value = (int)get_term_meta_value($term_id, 'contract_value') ?? 0;
                $data['contract_value'] = $contract_value;

                // Get payment_amount
                $payment_amount = (int)get_term_meta_value($term_id, 'payment_amount') ?? 0;
                $data['payment_amount'] = $payment_amount;

                // Get payment_amount_remaining
                $invs_amount = (int) get_term_meta_value($term_id, 'invs_amount');
                $payment_amount = (int) get_term_meta_value($term_id, 'payment_amount');
                $payment_amount_remaining = $invs_amount - $payment_amount;

                $payment_amount_remaining = max(0, $payment_amount_remaining);
                $data['payment_amount_remaining'] = $payment_amount_remaining;

                // Số caution_amount
                $caution_amount = (int)get_post_meta_value($post_id, 'amount') ?? 0;
                $data['caution_amount'] = $caution_amount;

                // Get caution_end_time
                $data['caution_end_time'] = my_date((int)$data['end_date'], 'd/m/Y');

                // Get note
                $data['note'] = $data['post_title'] ?: '--';

                // Get staff_business
                $data['staff_business'] = '';
                $staff_business = get_term_meta_value($term_id, 'staff_business');
                if (!empty($staff_business)) {
                    $data['staff_business'] = [];
                    $data['staff_business']['user_id'] = $staff_business;
                    $data['staff_business']['display_name'] = $this->admin_m->get_field_by_id($staff_business, 'display_name');
                    $data['staff_business']['user_avatar'] = $this->admin_m->get_field_by_id($staff_business, 'user_avatar');
                }

                // Get departments
                $data['department'] = '';
                $departments = $this->term_users_m->get_user_terms($staff_business, $this->department_m->term_type);
                $departments and $data['department'] = implode(', ', array_column($departments, 'term_name'));

                // Get user_groups
                $data['user_group'] = '';
                $user_groups = $this->term_users_m->get_user_terms($staff_business, $this->user_group_m->term_type);
                $user_groups and $data['user_group'] = implode(', ', array_column($user_groups, 'term_name'));

                return $data;
            }, FALSE)

            ->select('term.term_id, term.term_status, term.term_type, posts.post_id, posts.end_date, posts.post_title')
            ->from('term')
            ->join('term_posts', 'term_posts.term_id = term.term_id', 'LEFT')
            ->join('posts', 'posts.post_id = term_posts.post_id', 'LEFT')
            ->where('posts.post_type', 'receipt_caution')
            ->where('posts.post_status', 'publish')
            ->where('posts.end_date <=', $time)
            ->order_by('posts.end_date', 'DESC');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        $last_query = $this->datatable_builder->last_query();
        $last_query = preg_replace('/((L|l)(I|i)(M|m)(I|i)(T|t) \d+)/', '', $last_query);
        $subheadings_data = $this->db->query($last_query)->result_array();
        $subheadings = array_reduce($subheadings_data, function ($result, $item) {
            $term_id = $item['term_id'];
            $post_id = $item['post_id'];

            // Get contract_value
            $contract_value = (int)get_term_meta_value($term_id, 'contract_value') ?? 0;
            $result['contract_value'] += $contract_value;

            // Get payment_amount
            $payment_amount = (int)get_term_meta_value($term_id, 'payment_amount') ?? 0;
            $result['payment_amount'] += $payment_amount;

            // Get payment_amount_remaining
            $invs_amount = (int) get_term_meta_value($term_id, 'invs_amount');
            $payment_amount = (int) get_term_meta_value($term_id, 'payment_amount');
            $payment_amount_remaining = $invs_amount - $payment_amount;

            $payment_amount_remaining = max(0, $payment_amount_remaining);
            $result['payment_amount_remaining'] += $payment_amount_remaining;

            // Số caution_amount
            $caution_amount = (int)get_post_meta_value($post_id, 'amount') ?? 0;
            $result['caution_amount'] += $caution_amount;

            return $result;
        }, [
            'contract_value' => 0,
            'payment_amount' => 0,
            'payment_amount_remaining' => 0,
            'caution_amount' => 0,
        ]);
        $subheadings = array_map(function ($item) {
            $item .= '';
            return $item;
        }, $subheadings);
        $data['subheadings'] = $subheadings;

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->exportXlsx($last_query);
            return TRUE;
        }

        $this->template->title->append('Kho tài khoản');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering($args = array())
    {
        restrict('reports.receipt_caution.access');

        $args = parent::get();
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // cid FILTERING & SORTING
        $filter_cid = $args['where']['cid'] ?? FALSE;
        $sort_cid = $args['order_by']['cid'] ?? FALSE;
        if ($filter_cid || $sort_cid) {
            $alias_term_users = uniqid('tu_term_users_');
            $alias_user = uniqid('user_');
            $alias_usermeta = uniqid('m_user_');

            $this->datatable_builder->join("term_users {$alias_term_users}", "{$alias_term_users}.term_id = term.term_id", 'LEFT')
                ->join("user {$alias_user}", "{$alias_term_users}.user_id = {$alias_user}.user_id AND {$alias_user}.user_type IN ('customer_company','customer_person')", 'LEFT')
                ->join("usermeta {$alias_usermeta}", "{$alias_usermeta}.user_id = {$alias_user}.user_id AND {$alias_usermeta}.meta_key = 'cid'", 'LEFT');

            if ($filter_cid) {
                $this->datatable_builder->like("{$alias_usermeta}.meta_value", $filter_cid);
            }

            if ($sort_cid) {
                $this->datatable_builder->order_by("{$alias_usermeta}.meta_value", $sort_cid)
                    ->group_by("term.term_id, posts.post_id, posts.end_date, posts.post_title");
            }
        }

        // customer FILTERING & SORTING
        $filter_customer = $args['where']['customer'] ?? FALSE;
        $sort_customer = $args['order_by']['customer'] ?? FALSE;
        if ($filter_customer || $sort_customer) {
            $alias_term_users = uniqid('term_users_');
            $alias_user = uniqid('user_');

            $this->datatable_builder->join("term_users {$alias_term_users}", "{$alias_term_users}.term_id = term.term_id", 'LEFT')
                ->join("user {$alias_user}", "{$alias_term_users}.user_id = {$alias_user}.user_id AND {$alias_user}.user_type IN ('customer_company','customer_person')", 'LEFT');

            if ($filter_customer) {
                $this->datatable_builder->like("{$alias_user}.display_name", $filter_customer);
            }

            if ($sort_customer) {
                $this->datatable_builder->order_by("{$alias_user}.display_name", $sort_customer)
                    ->group_by("term.term_id, posts.post_id, posts.end_date, posts.post_title");
            }
        }

        // contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $alias_contract_code = uniqid('contract_code_');

            $this->datatable_builder->join("termmeta {$alias_contract_code}", "{$alias_contract_code}.term_id = term.term_id AND {$alias_contract_code}.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("{$alias_contract_code}.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder->order_by("{$alias_contract_code}.meta_value", $sort_contract_code);
            }
        }

        // contract_value FILTERING & SORTING
        $filter_contract_value = $args['where']['contract_value'] ?? FALSE;
        $sort_contract_value = $args['order_by']['contract_value'] ?? FALSE;
        if ($filter_contract_value || $sort_contract_value) {
            $alias_contract_value = uniqid('contract_value_');

            $this->datatable_builder->join("termmeta {$alias_contract_value}", "{$alias_contract_value}.term_id = term.term_id AND {$alias_contract_value}.meta_key = 'contract_value'", 'LEFT');

            if ($filter_contract_value) {
                $this->datatable_builder->where("CAST({$alias_contract_value}.meta_value AS SIGNED) >=", $filter_contract_value);
            }

            if ($sort_contract_value) {
                $this->datatable_builder->order_by("CAST({$alias_contract_value}.meta_value AS SIGNED)", $sort_contract_value);
            }
        }

        // payment_amount FILTERING & SORTING
        $filter_payment_amount = $args['where']['payment_amount'] ?? FALSE;
        $sort_payment_amount = $args['order_by']['payment_amount'] ?? FALSE;
        if ($filter_payment_amount || $sort_payment_amount) {
            $alias_payment_amount = uniqid('payment_amount_');

            $this->datatable_builder->join("termmeta {$alias_payment_amount}", "{$alias_payment_amount}.term_id = term.term_id AND {$alias_payment_amount}.meta_key = 'payment_amount'", 'LEFT');

            if ($filter_payment_amount) {
                $this->datatable_builder->where("CAST{$alias_payment_amount}.meta_value AS SIGNED) >=", $filter_payment_amount);
            }

            if ($sort_payment_amount) {
                $this->datatable_builder->order_by("CAST({$alias_payment_amount}.meta_value AS SIGNED)", $sort_payment_amount);
            }
        }

        // payment_amount_remaining FILTERING & SORTING
        $filter_payment_amount_remaining = $args['where']['payment_amount_remaining'] ?? FALSE;
        $sort_payment_amount_remaining = $args['order_by']['payment_amount_remaining'] ?? FALSE;
        if ($filter_payment_amount_remaining || $sort_payment_amount_remaining) {
            $alias_invs_amount = uniqid('invs_amount_');
            $alias_payment_amount = uniqid('payment_amount_');

            $this->datatable_builder
                ->select("(CAST($alias_invs_amount.meta_value AS UNSIGNED) - CAST($alias_payment_amount.meta_value AS UNSIGNED)) AS payment_amount_remaining")
                ->join("termmeta {$alias_invs_amount}", "{$alias_invs_amount}.term_id = term.term_id AND {$alias_invs_amount}.meta_key = 'invs_amount'", 'LEFT')
                ->join("termmeta {$alias_payment_amount}", "{$alias_payment_amount}.term_id = term.term_id AND {$alias_payment_amount}.meta_key = 'payment_amount'", 'LEFT');

            if ($filter_payment_amount_remaining) {
                $this->datatable_builder->having('CAST(payment_amount_remaining AS SIGNED) >=', $filter_payment_amount_remaining);
            }

            if ($sort_payment_amount_remaining) {
                $this->datatable_builder->order_by("CAST([custom-select].payment_amount_remaining AS SIGNED)", $sort_payment_amount_remaining);
            }
        }

        // caution_amount FILTERING & SORTING
        $filter_caution_amount = $args['where']['caution_amount'] ?? FALSE;
        $sort_caution_amount = $args['order_by']['caution_amount'] ?? FALSE;
        if ($filter_caution_amount || $sort_caution_amount) {
            $alias_caution_amount = uniqid('caution_amount_');

            $this->datatable_builder
                ->join('term_posts', 'term_posts.term_id = term.term_id', 'LEFT')
                ->join('posts', 'posts.post_id = term_posts.post_id', 'LEFT')
                ->join("postmeta {$alias_caution_amount}", "{$alias_caution_amount}.post_id = posts.post_id AND {$alias_caution_amount}.meta_key = 'amount'", 'LEFT');

            if ($filter_caution_amount) {
                $this->datatable_builder->where("CAST({$alias_caution_amount}.meta_value AS SIGNED) >=", $filter_caution_amount);
            }

            if ($sort_caution_amount) {
                $this->datatable_builder->order_by("CAST({$alias_caution_amount}.meta_value AS SIGNED)", $sort_caution_amount);
            }
        }

        // caution_end_time FILTERING & SORTING
        $filter_caution_end_time = $args['where']['caution_end_time'] ?? FALSE;
        $sort_caution_end_time = $args['order_by']['caution_end_time'] ?? FALSE;
        if ($filter_caution_end_time || $sort_caution_end_time) {
            $this->datatable_builder
                ->join('term_posts', 'term_posts.term_id = term.term_id', 'LEFT')
                ->join('posts', 'posts.post_id = term_posts.post_id', 'LEFT');

            if ($filter_caution_end_time) {
                $dates = explode(' - ', $filter_caution_end_time);

                $this->datatable_builder->where("posts.end_date >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("posts.end_date <=", $this->mdate->startOfDay(end($dates)));;
            }

            if ($sort_caution_end_time) {
                $this->datatable_builder->order_by("posts.end_date", $sort_caution_end_time);
            }
        }

        // note FILTERING & SORTING
        $filter_note = $args['where']['note'] ?? FALSE;
        $sort_note = $args['order_by']['note'] ?? FALSE;
        if ($filter_note || $sort_note) {
            $this->datatable_builder
                ->join('term_posts', 'term_posts.term_id = term.term_id', 'LEFT')
                ->join('posts', 'posts.post_id = term_posts.post_id', 'LEFT');

            if ($filter_note) {
                $this->datatable_builder->like("posts.post_title", $filter_note);
            }

            if ($sort_note) {
                $this->datatable_builder->order_by("posts.post_title", $sort_note);
            }
        }

        // staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if ($filter_staff_business || $sort_staff_business) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('staff_business_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = term.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("user $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT');

            if ($filter_staff_business) {
                $this->datatable_builder->like("$aliasStaffBusiness.display_name", $filter_staff_business);
            }

            if ($sort_staff_business) {
                $this->datatable_builder->order_by("$aliasStaffBusiness.display_name", $sort_staff_business);
            }
        }

        // department FILTERING & SORTING
        $filter_department = $args['where']['department'] ?? FALSE;
        $sort_department = $args['order_by']['department'] ?? FALSE;
        if ($filter_department || $sort_department) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('tu_department_');
            $aliasDepartment = uniqid('department_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = term.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("term_users $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT')
                ->join("term $aliasDepartment", "$aliasDepartment.term_id = $aliasStaffBusiness.term_id AND $aliasDepartment.term_type = '{$this->department_m->term_type}'", 'LEFT');

            if ($filter_department) {
                $this->datatable_builder->where("$aliasDepartment.term_id", $filter_department);
            }

            if ($sort_department) {
                $this->datatable_builder->order_by("$aliasDepartment.term_name", $sort_department);
            }
        }

        // user_group FILTERING & SORTING
        $filter_user_group = $args['where']['user_group'] ?? FALSE;
        $sort_user_group = $args['order_by']['user_group'] ?? FALSE;
        if ($filter_user_group || $sort_user_group) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('tu_user_group_');
            $aliaUserGroup = uniqid('user_group_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = term.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("term_users $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT')
                ->join("term $aliaUserGroup", "$aliaUserGroup.term_id = $aliasStaffBusiness.term_id AND $aliaUserGroup.term_type = '{$this->user_group_m->term_type}'", 'LEFT');

            if ($filter_user_group) {
                $this->datatable_builder->where("$aliaUserGroup.term_id", $filter_user_group);
            }

            if ($sort_user_group) {
                $this->datatable_builder->order_by("$aliaUserGroup.term_name", $sort_user_group);
            }
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function exportXlsx($query = '')
    {
        restrict('reports.over_budget.access');

        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $contracts = $this->contract_m->query($query)->result();
        if (!$contracts) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $title          = my_date(time(), 'Ymd') . '_export_bao_cao_canh_bao_hop_dong_co_bao_lanh';
        $spreadsheet    = IOFactory::load('./files/excel_tpl/contract/receipt-caution-due-soon-report.xlsx');
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));

        $sheet = $spreadsheet->getActiveSheet();
        $rowIndex = 4;

        foreach ($contracts as $key => &$contract) {
            $col = 1;

            // Prepare data
            $term_id = $contract->term_id;
            $post_id = $contract->post_id;

            $contract_code = get_term_meta_value($term_id, 'contract_code');

            // Get customer
            $customer_name = '';
            $cid = '';
            $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company', 'system']);
            if (!empty($customers)) {
                $customer = end($customers);
                $customer_name = $customer->display_name;
                $cid = get_user_meta_value($customer->user_id, 'cid');
                empty($cid) and $cid = cid($customer->user_id, $customer->user_type);
            }

            // Get staff_business
            $sale_id = get_term_meta_value($term_id, 'staff_business');
            $staff_business = $this->admin_m->get_field_by_id($sale_id, 'display_name') ?: '--';

            // Get departments
            $department = '';
            $departments = $this->term_users_m->get_user_terms($sale_id, $this->department_m->term_type);
            $departments and $department = implode(', ', array_column($departments, 'term_name'));

            // Get user_groups
            $user_group = '';
            $user_groups = $this->term_users_m->get_user_terms($sale_id, $this->user_group_m->term_type);
            $user_groups and $user_group = implode(', ', array_column($user_groups, 'term_name'));

            // Get contract_value
            $contract_value = (int)get_term_meta_value($term_id, 'contract_value');

            // Get payment_amount
            $payment_amount = (int)get_term_meta_value($term_id, 'payment_amount');

            // Get payment_amount_remaining
            $invs_amount = (int) get_term_meta_value($term_id, 'invs_amount');
            $payment_amount = (int) get_term_meta_value($term_id, 'payment_amount');
            $payment_amount_remaining = $invs_amount - $payment_amount;

            $payment_amount_remaining = max(0, $payment_amount_remaining);

            // Số caution_amount
            $caution_amount = (int)get_post_meta_value($post_id, 'amount');

            // Get caution_end_time
            $caution_end_time = my_date((int)$contract->end_date, 'd/m/Y');

            // Get note
            $note = $contract->post_title;

            // Set Cell
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $cid ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $customer_name ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_code ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_value ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $payment_amount ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $payment_amount_remaining ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $caution_amount ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $caution_end_time ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $note ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $staff_business ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $department ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $user_group ?: '--');

            $rowIndex++;
        }

        $numbers_of_contract = count($contracts);

        // Set Cells Number
        $sheet->getStyle('E' . $rowIndex . ':H' . ($numbers_of_contract + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        // Set Cells Date
        $sheet->getStyle('I' . $rowIndex . ':I' . ($numbers_of_contract + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);

        $folder_upload  = 'files/contract/report/';
        if (!is_dir($folder_upload)) {
            try {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}/{$title}.xlsx";
        try {
            $writer = IOFactory::createWriter($spreadsheet, 'Xls');
            $writer->save($fileName);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetReceipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetReceipts.php */