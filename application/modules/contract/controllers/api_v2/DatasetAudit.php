<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetAudit extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'audits_m';
        $this->autoload['models'][] = 'contract/contract_m';

        parent::__construct();

        $this->load->config('contract/receipt');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        $this->load->config('contract');
        $this->load->config('contract/receipt');
        $this->load->config('customer/customer');

        $this->load->model('staffs/department_m');
        $this->load->model('staffs/user_group_m');
        $this->load->model('staffs/term_users_m');
        $this->load->model('staffs/sale_m');

        $data = $this->data;
        $this->search_filter_audit();

        $dropdown_auditable_type = [
            'posts' => 'posts',
            'postmeta' => 'postmeta',
            'term' => 'term',
            'termmeta' => 'termmeta',
        ];

        $data['content'] = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')

            ->add_search('created_at', ['placeholder' => 'Thời gian', 'class' => 'form-control set-datepicker'])
            ->add_search('event', ['content' => form_dropdown(['name' => 'where[event]', 'class' => 'form-control select2'], prepare_dropdown(['updated' => 'Cập nhật', 'created' => 'Tạo mới'], 'Sự kiện'), parent::get('where[event]'))])
            ->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])
            ->add_search('user', ['placeholder' => 'Người thao tác'])
            ->add_search('auditable_type', ['content' => form_dropdown(['name' => 'where[auditable_type]', 'class' => 'form-control select2'], prepare_dropdown($dropdown_auditable_type, 'Bảng'), parent::get('where[auditable_type]'))])
            ->add_search('auditable_field', ['placeholder' => 'Trường dữ liệu'])
            ->add_search('new_values', ['placeholder' => 'Giá trị mới'])
            ->add_search('old_values', ['placeholder' => 'Giá trị cũ'])
            ->add_search('ip', ['placeholder' => 'Địa chỉ IP'])
            ->add_search('ua', ['placeholder' => 'User agent'])

            ->add_column('created_at', ['title' => 'Thời gian', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('event', ['title' => 'Sự kiện', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('contract_code', ['title' => 'Mã hợp đồng', 'set_order' => TRUE, 'set_select' => FALSE])
            ->add_column('user', ['title' => 'Người thao tác', 'set_order' => TRUE, 'set_select' => FALSE])
            ->add_column('auditable_type', ['title' => 'Bảng', 'set_order' => TRUE, 'set_select' => FALSE])
            ->add_column('auditable_field', ['title' => 'Trường dữ liệu', 'set_order' => TRUE, 'set_select' => FALSE])
            ->add_column('new_values', ['title' => 'Giá trị mới', 'set_order' => TRUE, 'set_select' => FALSE])
            ->add_column('old_values', ['title' => 'Giá trị cũ', 'set_order' => TRUE, 'set_select' => FALSE])
            ->add_column('ip', ['title' => 'Địa chỉ IP', 'set_order' => TRUE, 'set_select' => FALSE])
            ->add_column('ua', ['title' => 'User agent', 'set_order' => TRUE, 'set_select' => FALSE])

            ->add_column_callback('id', function ($data, $row_name) {
                $user_id = (int)$data['user_id'];
                $data['user'] = $this->admin_m->get_field_by_id($user_id, 'display_name');

                $data['event_raw'] = $data['event'];
                switch ($data['event_raw']) {
                    case 'updated':
                        $data['event'] = 'Cập nhật';
                        break;
                    case 'created':
                        $data['event'] = 'Tạo mới';
                        break;
                    default:
                        $data['event'] = '--';
                        break;
                }

                $auditable_id = $data['auditable_id'];
                $contract_code = get_term_meta_value($auditable_id, 'contract_code');
                $data['contract_code'] = $contract_code ?: '--';

                $data['old_values_raw'] = '' . $data['old_values'];
                $data['old_values'] = $data['old_values_raw'] ?: '--';

                return $data;
            }, FALSE)

            ->select('id, created_at, audits.user_id, event, auditable_id, auditable_type, auditable_field, new_values, old_values, ip, ua')
            ->from('audits')
            ->where_in('auditable_type', ['term', 'termmeta'])
            ->order_by('created_at', 'DESC');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export_audit($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function search_filter_audit($args = array())
    {
        restrict('contract.revenue.access');

        $args = parent::get();
        //if(empty($args) && parent::get('search')) $args = parent::get();  
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        //event FILTERING & SORTING
        $filter_event = $args['where']['event'] ?? FALSE;
        if ($filter_event) {
            $this->datatable_builder->where('event', $filter_event);
        }

        $order_event = $args['order_by']['event'] ?? FALSE;
        if ($order_event) {
            $this->datatable_builder->order_by('event', $order_event);
        }

        //auditable_type FILTERING & SORTING
        $filter_auditable_type = $args['where']['auditable_type'] ?? FALSE;
        if ($filter_auditable_type) {
            $this->datatable_builder->where('auditable_type', $filter_auditable_type);
        }

        $order_auditable_type = $args['order_by']['auditable_type'] ?? FALSE;
        if ($order_auditable_type) {
            $this->datatable_builder->order_by('auditable_type', $order_auditable_type);
        }

        //auditable_field FILTERING & SORTING
        $filter_auditable_field = $args['where']['auditable_field'] ?? FALSE;
        if ($filter_auditable_field) {
            $this->datatable_builder->like('auditable_field', $filter_auditable_field);
        }

        $order_auditable_field = $args['order_by']['auditable_field'] ?? FALSE;
        if ($order_auditable_field) {
            $this->datatable_builder->order_by('auditable_field', $order_auditable_field);
        }

        //new_values FILTERING & SORTING
        $filter_new_values = $args['where']['new_values'] ?? FALSE;
        if ($filter_new_values) {
            $this->datatable_builder->like('new_values', $filter_new_values);
        }

        $order_new_values = $args['order_by']['new_values'] ?? FALSE;
        if ($order_new_values) {
            $this->datatable_builder->order_by('new_values', $order_new_values);
        }

        //old_values FILTERING & SORTING
        $filter_old_values = $args['where']['old_values'] ?? FALSE;
        if ($filter_old_values) {
            $this->datatable_builder->like('old_values', $filter_old_values);
        }

        $order_old_values = $args['order_by']['old_values'] ?? FALSE;
        if ($order_old_values) {
            $this->datatable_builder->order_by('old_values', $order_old_values);
        }

        //ip FILTERING & SORTING
        $filter_ip = $args['where']['ip'] ?? FALSE;
        if ($filter_ip) {
            $this->datatable_builder->like('ip', $filter_ip);
        }

        $order_ip = $args['order_by']['ip'] ?? FALSE;
        if ($order_ip) {
            $this->datatable_builder->order_by('ip', $order_ip);
        }

        //ua FILTERING & SORTING
        $filter_ua = $args['where']['ua'] ?? FALSE;
        if ($filter_ua) {
            $this->datatable_builder->like('ua', $filter_ua);
        }

        $order_ua = $args['order_by']['ua'] ?? FALSE;
        if ($order_ua) {
            $this->datatable_builder->order_by('ua', $order_ua);
        }

        //contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $order_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $order_contract_code) {
            $alias = uniqid('contract_code_');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = audits.auditable_id AND {$alias}.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_contract_code);
            }

            if ($order_contract_code) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $order_contract_code);
            }
        }

        //user FILTERING & SORTING
        $filter_user = $args['where']['user'] ?? FALSE;
        $order_user = $args['order_by']['user'] ?? FALSE;
        if ($filter_user || $order_user) {
            $alias = uniqid('user_');
            $this->datatable_builder->join("user {$alias}", "{$alias}.user_id = audits.user_id", 'LEFT');

            if ($filter_user) {
                $this->datatable_builder->like("{$alias}.display_name", $filter_user);
            }

            if ($order_user) {
                $this->datatable_builder->order_by("{$alias}.display_name", $order_user);
            }
        }

        //created_at FILTERING & SORTING
        $filter_created_at = $args['where']['created_at'] ?? FALSE;
        if ($filter_created_at) {
            $dates = explode(' - ', $filter_created_at);
            $this->datatable_builder->where('created_at >=', date('Y-m-d 00:00:00', strtotime($dates[0])))
                ->where('created_at <=', date('Y-m-d 23:59:59', strtotime($dates[1])));
        }

        $order_created_at = $args['order_by']['created_at'] ?? FALSE;
        if ($order_created_at) {
            $this->datatable_builder->order_by('created_at', $order_created_at);
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function export_audit($query = '')
    {
        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $audits = $this->audits_m->query($query)->result();
        if (!$audits) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $this->load->library('excel');
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array('memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
            ->getProperties()
            ->setCreator("WEBDOCTOR.VN")
            ->setLastModifiedBy("WEBDOCTOR.VN")
            ->setTitle(uniqid('Danh sách thanh toán __'));

        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/audit/data_log_export.xlsx');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $row_index = 3;

        foreach ($audits as $key => $audit) {
            //  Prepare data
            $event = $audit->event;
            switch ($event) {
                case 'updated':
                    $event = 'Cập nhật';
                    break;
                case 'created':
                    $event = 'Tạo mới';
                    break;
                default:
                    $event = '--';
                    break;
            }

            $contract_id = $audit->auditable_id;
            $contract_code = get_term_meta_value($contract_id, 'contract_code') ?: '--';

            $user_id = $audit->user_id;
            $user = $this->admin_m->get_field_by_id($user_id, 'display_name') ?: '--';

            $row_number = $row_index + $key;

            $i = 0;

            // Set Cell
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $audit->created_at ?: '--');
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $event);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_code);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $user);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $audit->auditable_type ?: '--');
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $audit->auditable_field ?: '--');
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $audit->new_values ?: '--');
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $audit->old_values ?: '--');
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $audit->ip ?: '--');
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $audit->ua ?: '--');
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $file_name = "tmp/export-data-log.xlsx";
        $objWriter->save($file_name);

        try {
            $objWriter->save($file_name);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        if (file_exists($file_name)) {
            $this->load->helper('download');
            force_download($file_name, NULL);
        }

        return FALSE;
    }
}
/* End of file DatasetAudit.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetAudit.php */