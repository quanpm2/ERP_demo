<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * API FOR BACK-END
 */
class DatasetAdStorageCompleted extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'googleads/mcm_account_m';
        $this->autoload['models'][] = 'googleads/googleads_kpi_m';
        $this->autoload['models'][] = 'facebookads/facebookads_m';
        $this->autoload['models'][] = 'facebookads/adaccount_m';
        $this->autoload['models'][] = 'facebookads/facebookads_kpi_m';
        $this->autoload['models'][] = 'ads_segment_m';
        
        parent::__construct();
        
        $this->load->config('contract/receipt');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data'=>[]);
        if( ! has_permission('admin.contract.view'))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args( parent::get(), $defaults);

        $data = $this->data;
        $this->filtering();

        $data['content'] = $this->datatable_builder
        ->set_filter_position(FILTER_TOP_INNER)
        ->setOutputFormat('JSON')
        ->select('posts.post_id, start_date, end_date, post_type, post_status')

        ->add_search('adaccount_id')
        ->add_search('adaccount_name')
        ->add_search('adaccount_type')

        ->add_search('has_right_join', array(
            'content' => form_dropdown([
                'name'=>'where[has_right_join]',
                'class'=>'form-control'
            ], 
            prepare_dropdown(['yes' => 'Có', 'no' => 'Không'], 'Tất cả'),
            parent::get('where[has_right_join]'))
        ))

        ->add_search('right_join_contract_code')
        
        ->add_search('adaccount_type', array(
            'content' => form_dropdown([
                'name'=>'where[adaccount_type]',
                'class'=>'form-control select2'
            ], 
            prepare_dropdown(['mcm_account' => 'Gooogle', 'adaccount' => 'Facebook'], 'Tất cả'),
            parent::get('where[adaccount_type]'))
        ))

        ->add_search('contract_code')
        ->add_search('customer_code')
        ->add_search('start_date',['placeholder'=>'Ng. Bắt đầu','class'=>'form-control set-datepicker'])
        ->add_search('end_date',['placeholder'=>'Ng. Kết thúc','class'=>'form-control set-datepicker'])

        ->add_column('adaccount_id', array('title' => 'ID Tài khoản / CID', 'set_select' => FALSE, 'set_order' => FALSE))
        ->add_column('adaccount_name', array('title' => 'Tên tài khoản', 'set_select' => FALSE, 'set_order' => FALSE))
        ->add_column('adaccount_type', array('title' => 'Loại', 'set_select' => FALSE, 'set_order' => FALSE))
        ->add_column('contract_code', array('title' => 'Mã hợp đồng', 'set_order' =>  FALSE, 'set_select' => FALSE))
        
        ->add_column('has_right_join', array('title' => 'Tình trạng nối', 'set_order' =>  FALSE, 'set_select' => FALSE))
        ->add_column('right_join_contract_code', array('title' => 'MHĐ nối đến', 'set_order' =>  FALSE, 'set_select' => FALSE))

        ->add_column('customer_code', array('title' => 'Mã KH', 'set_order' =>  FALSE, 'set_select' => FALSE))
        ->add_column('customer_name', array('title' => 'Tên khách hàng', 'set_order' =>  FALSE, 'set_select' => FALSE))
        ->add_column('start_date', array('title' => 'Ng. Bắt đầu', 'set_order' => TRUE, 'set_select' => FALSE))
        ->add_column('end_date', array('title' => 'Ng. Kết thúc', 'set_order' => TRUE, 'set_select' => FALSE))
        ->add_column('spend', array('title' => 'Chi tiêu', 'set_order' => FALSE, 'set_select' => FALSE))
        ->add_column('staffs', array('title' => 'Kỹ thuật', 'set_order' => FALSE, 'set_select' => FALSE))
        ->add_column('sale', array('title' => 'Kinh doanh', 'set_order' => FALSE, 'set_select' => FALSE))

        ->add_column_callback('post_id',function($data, $row_name) {
            
            $post_id = $data['post_id'];
            $data['post_id'] = (int) $data['post_id'];
            
            $data['start_date'] = (int) $data['start_date'];
            $data['end_date'] = (int) $data['end_date'];
            
            $contracts  = $this->term_posts_m->get_post_terms($post_id, [ $this->googleads_m->term_type, $this->facebookads_m->term_type ]);
            $contracts  = array_filter($contracts);
            $contracts  = reset($contracts);
            $contract   = reset($contracts);
            $contract->contract_code = get_term_meta_value($contract->term_id, 'contract_code');
            $data['contract'] = elements([ 'term_id','term_type','term_name','term_status', 'contract_code' ], (array) $contract);

            $saleId = (int) get_term_meta_value($contract->term_id, 'staff_business');
            $sale   = $this->admin_m->get_field_by_id($saleId);
            $sale AND $sale = elements([ 'user_id', 'role_id', 'display_name', 'user_email', 'user_avatar' ], $sale);
            
            $data['sale'] = $sale;

            $adAccountType = 'google-ads' == $contract->term_type ? $this->mcm_account_m->term_type : $this->adaccount_m->term_type;
            $adAccount = $this->term_posts_m->get_post_terms($post_id, $adAccountType);
            $adAccount AND $adAccount = reset($adAccount);

            $adAccount->account_name = $this->mcm_account_m->term_type == $adAccountType ? get_term_meta_value($adAccount->term_id, 'account_name') : $adAccount->term_name;
            $adAccount->customer_id = $this->mcm_account_m->term_type == $adAccountType ? get_term_meta_value($adAccount->term_id, 'customer_id') : $adAccount->term_slug;
            $adAccount->currency_code = get_term_meta_value($adAccount->term_id, 'currency_code') ?: 'VND';

            $data['adAccount'] = elements([ 'term_id', 'term_type', 'term_status', 'account_name','customer_id','currency_code' ], (array) $adAccount);
            $data['adaccount_id'] = $adAccount->term_id;
            $data['adaccount_cid'] = $adAccount->customer_id;
            $data['adaccount_name'] = $adAccount->account_name;
            $data['adaccount_type'] = $this->mcm_account_m->term_type == $adAccountType ? 'googleads' : 'facebookads';
            $data['contract_code'] = $contract->contract_code;
            $data['contract_type'] = $contract->term_type;

            $nextContractId = (int) get_term_meta_value($contract->term_id, 'nextContractId');
            $data['has_right_join'] = (bool) $nextContractId;
            $data['right_join_contract_id'] = $nextContractId;
            $data['right_join_contract_code'] = get_term_meta_value($nextContractId, 'contract_code');

            // Get customer display name
            if($customers = $this->term_users_m->get_the_users($contract->term_id, ['customer_person','customer_company','system']))
            {
                $customer       = end($customers);
                $data['customer_code'] = cid($customer->user_id, $customer->user_type);
                $data['customer_name'] = $customer->display_name;
            }

            $spend = 0;

            switch ($adAccountType) {

                case $this->mcm_account_m->term_type :
                    
                    $iSegments  = $this->mcm_account_m->get_data($adAccount->term_id, $data['start_date'], $data['end_date']);
                    $iSegments AND $spend = array_sum(array_map('doubleval', array_column($iSegments, 'spend')));
                    
                    break;

                case $this->adaccount_m->term_type :

                    $iSegments  = $this->adaccount_m->get_data($adAccount->term_id, $data['start_date'], $data['end_date']);
                    $iSegments AND $spend = array_sum(array_map('doubleval', array_column($iSegments, 'spend')));
                    break;

                default: 
                    break;
            }

            $data['spend'] = (double) $spend;


            $kpis = array();
            if($adAccountType == $this->adaccount_m->term_type) 
            {
                $kpis = $this->facebookads_kpi_m->get_kpis($contract->term_id, 'users', 'tech');
            }
            else if($adAccountType == $this->mcm_account_m->term_type)
            {
                $kpis = $this->googleads_kpi_m->get_kpis($contract->term_id, 'users', 'account_type');
            }

            $user_ids = array();
            foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;

            $data['staffs'] = implode(', ', array_values(array_map(function($user_id){
                $_user = $this->admin_m->get_field_by_id($user_id);
                return $_user['display_name'] ?: $_user['user_email'];
            }, $user_ids)));

            return $data;

        },FALSE)

        ->from('posts')
        ->where('posts.end_date != 0')
        ->where('posts.post_type', $this->ads_segment_m->post_type)
        ->group_by('posts.post_id');

        $pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->exportXlsx($last_query);
            return TRUE;
        }

        $this->template->title->append('Kho tài khoản');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering($args = array())
    {
        restrict('admin.contract.view');

        $args = parent::get(null, true);

        //if(empty($args) && parent::get('search')) $args = parent::get();  
        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

        // Email kinh doanh FILTERING & SORTING
        $filter_adaccount_id    = $args['where']['adaccount_id'] ?? FALSE;
        if($filter_adaccount_id)
        {
            // Filter for mcm account
            $mcmAccountIds = $this->mcm_account_m
            ->set_term_type()
            ->distinct()
            ->select('term.term_id')
            ->join('termmeta', 'termmeta.term_id = term.term_id and meta_key = "customer_id"')
            ->like('meta_value', $filter_adaccount_id)
            ->get_all();

            $adAccountIds = $this->adaccount_m
            ->set_term_type()
            ->distinct()
            ->select('term.term_id')
            ->like('term_slug', $filter_adaccount_id)
            ->get_all();

            if(empty($mcmAccountIds) && empty($adAccountIds))
            {
                $this->datatable_builder->where('1==2');
            }
            else
            {
                $_ids = array_filter(array_map('intval', array_merge(array_column((array) $mcmAccountIds, 'term_id'), array_column((array) $adAccountIds, 'term_id'))));
                $this->datatable_builder
                    ->join('term_posts as tp_adaccount', 'tp_adaccount.post_id = posts.post_id')
                    ->join('term as adAccount', 'adAccount.term_id = tp_adaccount.term_id and adAccount.term_type in ("mcm_account", "adaccount")')
                    ->where_in('adAccount.term_id', $_ids);
            }
        }

        $filter_adaccount_name    = $args['where']['adaccount_name'] ?? FALSE;
        if($filter_adaccount_name)
        {
            // Filter for mcm account
            $mcmAccountIds = $this->mcm_account_m
            ->set_term_type()
            ->distinct()
            ->select('term.term_id')
            ->join('termmeta', 'termmeta.term_id = term.term_id and meta_key = "account_name"')
            ->like('meta_value', $filter_adaccount_name)
            ->get_all();

            $adAccountIds = $this->adaccount_m
            ->set_term_type()
            ->distinct()
            ->select('term.term_id')
            ->like('term_name', $filter_adaccount_name)
            ->get_all();

            if(empty($mcmAccountIds) && empty($adAccountIds))
            {
                $this->datatable_builder->where('1==2');
            }
            else
            {
                $_ids = array_filter(array_map('intval', array_merge(array_column((array) $mcmAccountIds, 'term_id'), array_column((array) $adAccountIds, 'term_id'))));
                $this->datatable_builder
                    ->join('term_posts as tp_adaccount', 'tp_adaccount.post_id = posts.post_id')
                    ->join('term as adAccount', 'adAccount.term_id = tp_adaccount.term_id and adAccount.term_type in ("mcm_account", "adaccount")')
                    ->where_in('adAccount.term_id', $_ids);
            }
        }
        
        $filter_adaccount_type    = $args['where']['adaccount_type'] ?? FALSE;
        if($filter_adaccount_type)
        {
            if( ! in_array($filter_adaccount_type, [ 'mcm_account', 'adaccount' ]))
            {
                $this->datatable_builder->where(1, 2);
            }
            else
            {
                $this->datatable_builder
                    ->join('term_posts as tp_adaccount', 'tp_adaccount.post_id = posts.post_id')
                    ->join('term as adAccount', 'adAccount.term_id = tp_adaccount.term_id and adAccount.term_type in ("mcm_account", "adaccount")')
                    ->where_in('adAccount.term_type', $filter_adaccount_type);
            }
        }

        $filter_has_right_join = $args['where']['has_right_join'] ?? FALSE;
        if($filter_has_right_join)
        {
            if( ! in_array($filter_has_right_join, [ 'yes', 'no' ]))
            {
                $this->datatable_builder->where(1, 2);
            }
            else
            {
                $this->datatable_builder
                    ->join('term_posts as tp_contract', 'tp_contract.post_id = posts.post_id')
                    ->join('term as contract', 'contract.term_id = tp_contract.term_id and contract.term_type in ("google-ads", "facebook-ads")')
                    ->join('termmeta as tm_has_right_join', 'tm_has_right_join.term_id = contract.term_id AND tm_has_right_join.meta_key = "nextContractId"', 'left');

                switch ($filter_has_right_join) {
                    case 'yes':
                        $this->datatable_builder->where('(tm_has_right_join.meta_value*1) >', 0);
                        break;
                    case 'no':
                        $this->datatable_builder->where('tm_has_right_join.meta_value is null');
                }
            }
        }

        $filter_right_join_contract_code = $args['where']['right_join_contract_code'] ?? FALSE;
        $filter_has_right_join = $args['where']['has_right_join'] ?? FALSE;
        if($filter_right_join_contract_code && 'no' != $filter_has_right_join)
        {
            $contractIds = $this
            ->termmeta_m
            ->select('term_id')
            ->where('meta_key', 'contract_code')
            ->like('meta_value', $filter_right_join_contract_code)
            ->get_all();

            if(empty($contractIds))
            {
                $this->datatable_builder->where(1, 2);
            }
            else
            {
                $this->datatable_builder
                    ->join('term_posts as tp_contract', 'tp_contract.post_id = posts.post_id')
                    ->join('term as contract', 'contract.term_id = tp_contract.term_id and contract.term_type in ("google-ads", "facebook-ads")')
                    ->join('termmeta as tm_has_right_join', 'tm_has_right_join.term_id = contract.term_id AND tm_has_right_join.meta_key = "nextContractId"', 'left')
                    ->where_in('tm_has_right_join.meta_value', array_column($contractIds, 'term_id'));
            }
        }

        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        if($filter_contract_code)
        {
            $contractIds = $this->termmeta_m->select('term_id')->where('meta_key', 'contract_code')->like('meta_value', $filter_contract_code)->get_all();
            if(empty($contractIds))
            {
                $this->datatable_builder->where(1, 2);
            }
            else
            {
                $this->datatable_builder
                    ->join('term_posts as tp_contract', 'tp_contract.post_id = posts.post_id')
                    ->join('term as contract', 'contract.term_id = tp_contract.term_id and contract.term_type in ("google-ads", "facebook-ads")')
                    ->where_in('contract.term_id', array_map('intval', array_column($contractIds, 'term_id')));
            }
        }

        // customer_code FILTERING & SORTING
        $filter_customer_code = $args['where']['customer_code'] ?? FALSE;
        $sort_customer_code   = $args['order_by']['customer_code'] ?? FALSE;
        if($filter_customer_code || $sort_customer_code)
        {
            $alias = uniqid('customer_code_');
            $this->datatable_builder
            ->join('term_posts as tp_contract', 'tp_contract.post_id = posts.post_id')
            ->join('term as contract', 'contract.term_id = tp_contract.term_id and contract.term_type in ("google-ads", "facebook-ads")')
            ->join('term_users AS tu_customer', 'tu_customer.term_id = contract.term_id')
            ->join('user AS customer', 'customer.user_id = tu_customer.user_id AND customer.user_type IN ("customer_person", "customer_company")')
            ->join("usermeta {$alias}","{$alias}.user_id = customer.user_id and {$alias}.meta_key = 'cid'", 'LEFT');

            if($filter_customer_code)
            {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_customer_code);
                unset($args['where']['customer_code']);
            }

            if($sort_customer_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_customer_code);
                unset($args['order_by']['customer_code']);
            }
        }

        $filter_start_date = $args['where']['start_date'] ?? FALSE;
        $sort_start_date = $args['order_by']['start_date'] ?? FALSE;
        if($filter_start_date || $sort_start_date)
        {
            if($filter_start_date)
            {
                $dates = explode(' - ', $args['where']['start_date']);
                $this->datatable_builder->where("posts.start_date >=", start_of_day(reset($dates)))->where("posts.start_date <=", end_of_day(end($dates)));   
            }

            if($sort_start_date)
            {
                $this->datatable_builder->order_by("posts.start_date", $sort_start_date);
                unset($args['order_by']['start_date']);
            }
        }

        $filter_end_date = $args['where']['end_date'] ?? FALSE;
        $sort_end_date = $args['order_by']['end_date'] ?? FALSE;
        if($filter_end_date || $sort_end_date)
        {
            if($filter_end_date)
            {
                $dates = explode(' - ', $args['where']['end_date']);
                $this->datatable_builder->where("posts.end_date >=", start_of_day(reset($dates)))->where("posts.end_date <=", end_of_day(end($dates)));   
            }

            if($sort_end_date)
            {
                $this->datatable_builder->order_by("posts.end_date", $sort_end_date);
                unset($args['order_by']['end_date']);
            }
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function exportXlsx($query = '')
    {
        if(empty($query)) return FALSE;

        $title = 'AdAccount-Completed';

        $pos = strpos($query, 'LIMIT');
        if(FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $posts = $this->ads_segment_m->query($query)->result();
        if( ! $posts)
        {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(),'refresh');
        }

        $columns = array(
            'adaccount_id' => array( 'type' => 'string', 'label' => 'ID Tài khoản / CID'),
            'adaccount_name' => array( 'type' => 'string', 'label' => 'Tên tài khoản'),
            'adaccount_type' => array( 'type' => 'string', 'label' => 'Loại'),
            'contract_code' => array( 'type' => 'string', 'label' => 'Mã hợp đồng'),
            'has_right_join' => array( 'type' => 'string', 'label' => 'TT Nối'),
            'right_join_contract_code' => array( 'type' => 'string', 'label' => 'HĐ Nối đến'),
            'customer_code' => array( 'type' => 'string', 'label' => 'Mã KH'),
            'customer_name' => array( 'type' => 'string', 'label' => 'Tên khách hàng'),
            'start_date' => array( 'type' => 'timestamp', 'label' => 'Ng. Bắt đầu'),
            'end_date' => array( 'type' => 'timestamp', 'label' => 'Ng. Kết thúc'),
            'spend' => array( 'type' => 'number', 'label' => 'Chi tiêu'),
            'staffs' => array( 'type' => 'string', 'label' => 'Kỹ thuật'),
            'sale' => array( 'type' => 'string', 'label' => 'Kinh doanh'),
        );

        $spreadsheet = new Spreadsheet();
        $spreadsheet
        ->getProperties()
        ->setCreator('ADSPLUS.VN')
        ->setLastModifiedBy('ADSPLUS.VN')
        ->setTitle(time());

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->fromArray(array_column(array_values($columns), 'label'), NULL, 'A1');

        $rowIndex = 2;
        $data = [];

        $segments = $this->ads_segment_m
        ->set_post_type()
        ->select([
            'posts.post_id as segmentId',
            'posts.start_date as segmentStartDate',
            'posts.end_date as segmentEndDate',

            'posts.post_status',
            'posts.post_type',

            'posts.post_type as segmentType',
            'adAccountTbl.term_type as adAccountType',
            'adAccountTbl.term_id as adAccountId',
            'adAccountTbl.term_name as adAccountName',
            'adAccountTbl.term_slug as adAccountSlug',
            'adAccountTbl.term_status as adAccountStatus',
            'sum(postmeta.meta_value) as spend'
        ])
        ->join('term_posts tp_adAccountTbl', 'tp_adAccountTbl.post_id = posts.post_id')
        ->join('term adAccountTbl', 'adAccountTbl.term_id = tp_adAccountTbl.term_id')
        ->where_in('adAccountTbl.term_type', [ $this->mcm_account_m->term_type, $this->adaccount_m->term_type ])

        ->join('term_posts tp_iSegmentTbl', 'tp_iSegmentTbl.term_id = adAccountTbl.term_id', 'left')
        
        ->join('posts isegments', '
            isegments.post_id = tp_iSegmentTbl.post_id 
            AND isegments.post_type = "insight_segment"
            AND isegments.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(posts.start_date, "%Y-%m-%d 00:00:00")) 
            AND isegments.start_date <= if(posts.end_date = 0 or posts.end_date is null, UNIX_TIMESTAMP (), posts.end_date)
            AND isegments.post_name = "day"
        ', 'left')

        ->join('postmeta', '
            postmeta.post_id = isegments.post_id
            AND postmeta.meta_key = "spend"
        ', 'left')
            
        ->where_in('posts.post_id', array_map('intval', array_column($posts, 'post_id')))
        ->group_by('posts.post_id, adAccountTbl.term_id')
        ->get_all();

        $data = array_map(function($post){

            $contract = $this->term_posts_m->get_post_terms($post->segmentId, [ $this->googleads_m->term_type, $this->facebookads_m->term_type ]);
            $contract AND $contract = array_values(array_filter($contract));
            $contract AND $contract = array_merge(...$contract);
            $contract AND $contract = reset($contract);

            $contract->contract_code = get_term_meta_value($contract->term_id, 'contract_code');

            $nextContractId = (int) get_term_meta_value($contract->term_id, 'nextContractId');
            $has_right_join = (bool) $nextContractId;
            $right_join_contract_id = $nextContractId;
            $right_join_contract_code = get_term_meta_value($nextContractId, 'contract_code');

            $saleId = (int) get_term_meta_value($contract->term_id, 'staff_business');
            $sale   = $this->admin_m->get_field_by_id($saleId);

            $this->mcm_account_m->term_type == $post->adAccountType AND $post->adAccountName = get_term_meta_value($post->adAccountId, 'account_name');

            $post->adAccountCustomerId = $this->mcm_account_m->term_type == $post->adAccountType ? get_term_meta_value($post->adAccountId, 'customer_id') : $post->adAccountSlug;
            $post->adAccountCurrencyCode = get_term_meta_value($post->adAccountId, 'currency_code') ?: 'VND';

            $customer = null;
            if($customers = $this->term_users_m->get_the_users($contract->term_id, ['customer_person', 'customer_company', 'system']))
            {
                $customer = end($customers);
                $customer->customer_code = cid($customer->user_id, $customer->user_type);
            }

            if($this->mcm_account_m->term_type == $post->adAccountType)
            {
                $insights = $this->mcm_account_m->get_data($post->adAccountId, $post->segmentStartDate, $post->segmentEndDate);
                $insights AND $post->spend = array_sum(array_map('doubleval', array_column($insights, 'spend')));   
            }

            $kpis = array();
            if($post->adAccountType == $this->adaccount_m->term_type) $kpis = $this->facebookads_kpi_m->get_kpis($contract->term_id, 'users', 'tech');
            else if($post->adAccountType == $this->mcm_account_m->term_type) $kpis = $this->googleads_kpi_m->get_kpis($contract->term_id, 'users', 'account_type');

            $user_ids = array();
            foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;

            $staffs = implode(', ', array_values(array_map(function($user_id){
                $_user = $this->admin_m->get_field_by_id($user_id);
                return $_user['display_name'] ?: $_user['user_email'];
            }, $user_ids)));


            return [
                'post_id'                   => (int) $post->segmentId,
                'post_status'               => $post->post_status,
                'post_type'                 => $post->post_type,
                'start_date'                => (int) $post->segmentStartDate,
                'end_date'                  => (int) $post->segmentEndDate,
                'adaccount_id'              => (int) $post->adAccountId,
                'adaccount_name'            => $post->adAccountName,
                'adaccount_type'            => $this->mcm_account_m->term_type == $post->adAccountType ? 'googleads' : 'facebookads',
                'adaccount_cid'             => $post->adAccountCustomerId,
                'contract_code'             => $contract->contract_code,
                'has_right_join'            => $has_right_join ? 'Yes' : 'No',
                'right_join_contract_code'  => $right_join_contract_code,
                'customer_code'             => $customer->customer_code,
                'customer_name'             => $customer->display_name,
                'spend'                     => (double) $post->spend,
                'staffs'                    => $staffs,
                'sale'                      => $sale['display_name'] ?? null,
            ];

        }, $segments);

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column)
            {
                $value = $item[$key];
                switch ($column['type'])
                {
                    case 'number': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }    

                @$sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $value);
                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetReceipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetReceipts.php */