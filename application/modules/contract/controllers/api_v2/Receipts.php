<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class Receipts extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';
        $this->autoload['libraries'][] = 'form_validation';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'contract/category_m';
        $this->autoload['models'][] = 'contract/term_categories_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'contract/common_report_m';
        $this->autoload['models'][] = 'log_m';

        parent::__construct();

        $this->load->config('contract/receipt');
        $this->load->config('contract/contract');
    }

    public function compute_meta_get($id)
    {
        $receipt = $this->receipt_m->set_post_type()->as_array()->get($id);
        if(empty($receipt)) parent::response( [ 'code' => parent::HTTP_NOT_FOUND]);

        $types      = $this->config->item('ADSPLUS.VN', 'group-services');
        $contract   = $this->contract_m
        ->select('term.term_id, term.term_name, term.term_type, term.term_status')
        ->join('term_posts', 'term_posts.term_id = term.term_id and term_posts.post_id = ' . (int) $id)
        ->get_by();

        if(empty($contract)) parent::response( [ 'code' => parent::HTTP_NOT_FOUND . '[1]']);

        $_receipt = (new receipt_m())->forceFill($receipt);
        $_receipt->contract = $contract;

        $_instance = $_receipt->get_instance();

        var_dump('contract_id :'. $_receipt->contract->term_id);
            
        $vat_amount     = $_instance->calc_vat_amount();
        var_dump('vat_amount : ' . currency_numberformat($vat_amount, ' đ'));
        update_post_meta($_receipt->post_id, 'vat_amount', $vat_amount);

        $actual_budget  = $_instance->calc_actual_budget();
        var_dump('actual_budget : ' . currency_numberformat($actual_budget, ' đ'));
        update_post_meta($_receipt->post_id, 'actual_budget', $actual_budget);

        $fct_amount     = $_instance->calc_fct();
        var_dump('fct_amount : ' . currency_numberformat($fct_amount, ' đ'));
        update_post_meta($_receipt->post_id, 'fct_amount', $fct_amount);

        $deal_reduction_fee = (int) $_instance->calc_deal_reduction_fee();
        var_dump('deal_reduction_fee : ' . currency_numberformat($deal_reduction_fee, ' đ'));
        update_post_meta($_receipt->post_id, 'deal_reduction_fee', $deal_reduction_fee);
        
        $service_fee    = $_instance->calc_service_fee();
        var_dump('service_fee : ' . currency_numberformat($service_fee, ' đ'));
        update_post_meta($_receipt->post_id, 'service_fee', ($deal_reduction_fee > 0 ? max([ $service_fee - $deal_reduction_fee, 0]) : $service_fee));

        parent::response(['code' => parent::HTTP_OK]);
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function dataset_get()
    {
        $response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
        if( ! has_permission('contract.receipt.access'))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args( parent::get(), $defaults);

        $this->load->config('contract');
        $this->load->config('contract/receipt');
        $this->load->config('customer/customer');

        $this->load->model('staffs/department_m');
        $this->load->model('staffs/user_group_m');
        $this->load->model('staffs/term_users_m');
        $this->load->model('staffs/sale_m');

        $sales = $this->sale_m->set_user_type()->set_role()->get_all();
        $sales = $sales ? key_value($sales,'user_id','user_email') : [];

        $data = $this->data;
        $this->search_filter_receipt();
        
        $sum_amount_not_vat = 0;
        $receipt_status = $this->config->item('status','receipt');

        /* Lấy tất cả phòng ban và convert về dạng array với key = id và value = tên phòng ban */
        $departments = $this->department_m->select('term_id,term_name')->set_term_type()->as_array()->get_all();
        $departments = $departments ? key_value($departments, 'term_id', 'term_name') : [];
        /* 
         * Lấy tất cả nhóm và convert về dạng array với key = id và value = tên phòng ban
         * Đồng thời group từng nhóm lại theo đúng tên phòng ban chứa nhóm đấy
         */
        $user_groups = $this->user_group_m->select('term_id,term_name,term_parent')->set_term_type()->as_array()->get_all();
        if( ! empty($user_groups))
        {
            $_user_groups = array();
            foreach (array_group_by($user_groups, 'term_parent') as $department_id => $groups)
            {
                if(empty($departments[$department_id])) continue;

                $_user_groups[$departments[$department_id]] = key_value($groups, 'term_id', 'term_name');
            }

            $user_groups = $_user_groups;
        }

        $receipt_enums = [ 'receipt_payment_all' => 'Thanh toán (+ thu chi hộ)'] + $this->config->item('type', 'receipt');

        $data['content'] = $this->datatable_builder
        ->set_filter_position(FILTER_TOP_INNER)
        ->setOutputFormat('JSON')
        ->select('posts.post_id,posts.post_author,posts.post_title')    

        // ->add_column('all_checkbox',array('title'=> '<input type="checkbox" name="all-checkbox" value="" id="all-checkbox" class="minimal"/>','set_order'=> FALSE, 'set_select' => FALSE))
        ->add_search('end_date',['placeholder'=>'Ngày thanh toán','class'=>'form-control set-datepicker'])
        ->add_search('contract_code',['placeholder'=>'Số hợp đồng'])
        ->add_search('customer',['placeholder'=>'Khách hàng'])
        ->add_search('sale_email',['placeholder'=>'Email kinh doanh'])

        ->add_search('vat',array('content'=> form_dropdown(['name'=>'where[vat]','class'=>'form-control select2'],prepare_dropdown([10=>'10'],'VAT'),parent::get('where[vat]'))))
        ->add_search('term_type',array('content'=> form_dropdown(['name'=>'where[term_type]','class'=>'form-control select2'],prepare_dropdown($this->config->item('taxonomy'),'Loại dịch vụ'),parent::get('where[term_type]'))))
        ->add_search('post_type',array('content'=> form_dropdown(['name'=>'where[post_type]','class'=>'form-control select2'],prepare_dropdown($receipt_enums,'Loại thanh toán'),parent::get('where[post_type]'))))
        ->add_search('post_status',array('content'=> form_dropdown(['name'=>'where[post_status]','class'=>'form-control select2'],prepare_dropdown($receipt_status['all'],'Tình trạng'),parent::get('where[post_status]'))))
        ->add_search('transfer_type',array('content'=> form_dropdown(['name'=>'where[transfer_type]','class'=>'form-control select2'],prepare_dropdown($this->config->item('transfer_type','receipt'),'Hình thức chuyển'),parent::get('where[transfer_type]'))))
        ->add_search('departments',array('content'=> form_dropdown(['name'=>'where[departments]','class'=>'form-control select2'],prepare_dropdown($departments,'Phòng ban'),parent::get('where[departments]'))))
        ->add_search('user_groups',array('content'=> form_dropdown(['name'=>'where[user_groups]','class'=>'form-control select2'],prepare_dropdown($user_groups,'Nhóm'),parent::get('where[user_groups]'))))
        ->add_search('action',['content'=>form_button(['name'=>'notification_of_payment','class'=>'btn btn-info', 'id'=>'send-mail-payment'],'<i class="fa fa-fw fa-send"></i> Email')])

    
        ->add_column('contract_code', array('title'=>'Hợp đồng','set_select'=>FALSE,'set_order'=>FALSE))
        ->add_column('posts.end_date', array('title'=> 'Ngày thanh toán','set_order'=> TRUE))
        ->add_column('amount', array('title'=> 'Số tiền(vnđ)','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('vat', array('title'=> 'VAT','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('amount_not_vat', array('title'=> 'Doanh thu(vnđ)','set_order'=> FALSE,'set_select'=>FALSE))

        ->add_column('vat_amount', array('title'=> 'Thuế','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('service_fee', array('title'=> 'Phí dịch vụ','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('fct_amount', array('title'=> 'Thuế nhà thầu','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('actual_budget', array('title'=> 'Ngân sách thực','set_order'=> TRUE,'set_select'=>FALSE))

        ->add_column('term_type', array('title'=> 'Dịch vụ','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('posts.post_type', array('title'=> 'Loại','set_order'=> TRUE))
        ->add_column('posts.post_status', array('title'=> 'Trạng thái','set_order'=> TRUE))
        ->add_column('is_order_printed', array('title'=> 'In HĐ','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('transfer_type', array('title'=> 'Hình thức','set_order'=> TRUE,'set_select'=>FALSE))

        ->add_column_callback('post_id',function($data,$row_name) use ($sales) {

            $post_id = $data['post_id'];
            $data['post_id'] = (int) $data['post_id'];
            $data['end_date'] = (int) $data['end_date'];

            $data['has_send_mail_payment'] = (bool) get_post_meta_value($post_id, 'has_send_mail_payment');

            $data['customer'] = $data['sale'] = $data['author'] = null;

            $taxonomies = $this->config->item('taxonomy');
            $taxonomies = array_keys($taxonomies);
            $term_id    = $this->term_posts_m->get_the_terms($post_id, $taxonomies);
            if( ! empty($term_id)) $term_id = reset($term_id);

            $data['term_id'] = (int) $term_id;

            $data['contract_code'] = get_term_meta_value($term_id,'contract_code');

            $sale_id = (int) get_term_meta_value($term_id,'staff_business');
            $data['sale'] = elements(['user_id', 'display_name', 'user_email'], $this->admin_m->get_field_by_id($sale_id));
            
            $data['sale']['departments'] = null;
            $departments = $this->term_users_m->get_user_terms($sale_id, $this->department_m->term_type);
            empty($departments) OR $data['sale']['departments'] = array_column($departments, 'term_name');

            $data['sale']['user_groups'] = null;
            $user_groups = $this->term_users_m->get_user_terms($sale_id, $this->user_group_m->term_type);
            empty($user_groups) OR $data['sale']['user_groups'] = array_column($user_groups, 'term_name');

            // Get customer display name
            if($customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company','system']))
            {
                $customer       = end($customers);
                $data['customer'] = elements(['user_id', 'display_name', 'user_email'], (array) $customer);
                $data['customer']['cid'] = cid($customer->user_id, $customer->user_type);
            }

            // Hình thức thanh toán
            $this->load->config('contract/receipt');
            $def_transfer_type = $this->config->item('transfer_type','receipt');
            $transfer_type = get_post_meta_value($post_id,'transfer_type');

            $data['transfer_type'] = $def_transfer_type[$transfer_type] ?? reset($def_transfer_type);

            // tài khoản
            'account' == $transfer_type AND $data['transfer_account'] = get_post_meta_value($post_id,'transfer_account');

            $data['amount']         = (double) get_post_meta_value($post_id,'amount');
            $data['vat_amount']     = (int) get_post_meta_value($post_id,'vat_amount');
            $data['service_fee']    = (double) get_post_meta_value($post_id,'service_fee');
            $data['fct_amount']     = (double) get_post_meta_value($post_id,'fct_amount');
            $data['actual_budget'] = (double) get_post_meta_value($post_id,'actual_budget');

            switch ($data['post_type']) {
                
                case 'receipt_payment_on_behaft':
                    $data['vat'] = 0;
                    break;
                
                default:
                    $data['vat'] = (int) get_term_meta_value($term_id,'vat');
                    $data['amount_not_vat'] = (double) div($data['amount'], (1+div($data['vat'], 100)));
                    break;
            }


            $term = $this->term_m->get($term_id);
            $data['term_type'] = $this->config->item($term->term_type,'taxonomy');

            // Ngày thanh toán
            $payment_time = get_post_meta_value($post_id,'payment_time');
            $data['payment_time'] = my_date($payment_time,'d/m/Y');
            $data['post_author'] = $this->admin_m->get_field_by_id($data['post_author'],'user_email');
            $def_post_status = $this->config->item('status','receipt');
            $data['post_status'] = $def_post_status[$data['post_type']][$data['post_status']];

            $post_type = $data['post_type'];
            $def_post_type = $this->config->item('type','receipt');
            $data['post_type'] = $def_post_type[$post_type];

            $data['is_order_printed'] = (bool) get_post_meta_value($post_id,'is_order_printed');


            if($post_type == 'receipt_caution')
            {
                $data['end_date'].= br().'<span class="text-yellow">'.$def_post_type[$post_type].'</span>';
            }
            return $data;
        },FALSE)

        ->from('posts')
        ->where('posts.post_status !=','draft')
        ->where_in('posts.post_type',['receipt_payment','receipt_caution', 'receipt_payment_on_behaft'])
        //->generate(['per_page' => 100]);
        ->group_by('posts.post_id');

        $pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

        $data['sum_amount'] = array(
            'amount'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'amount'),array('amount'=>0)),
            // 'vat_amount'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'vat_amount'),array('vat_amount'=>0)),
            // 'service_fee'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'service_fee'),array('service_fee'=>0)),
            // 'actual_budget'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'actual_budget'),array('actual_budget'=>0)),
            // 'fct_amount'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'fct_amount'),array('fct_amount'=>0)),
        );

        $data = $this->datatable_builder->generate($pagination_config);
        // dd($data['headings'], $data['rows']);
        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->export_receipt($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }
    
    public function index_get()
    {
        $response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
        if( ! has_permission('contract.receipt.access'))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args( parent::get(), $defaults);

        $this->load->config('contract');
        $this->load->config('contract/receipt');
        $this->load->config('customer/customer');

        $this->load->model('staffs/department_m');
        $this->load->model('staffs/user_group_m');
        $this->load->model('staffs/term_users_m');
        $this->load->model('staffs/sale_m');

        $sales = $this->sale_m->set_user_type()->set_role()->get_all();
        $sales = $sales ? key_value($sales,'user_id','user_email') : [];

        $data = $this->data;
        $this->search_filter_receipt();
        
        $sum_amount_not_vat = 0;
        $receipt_status = $this->config->item('status','receipt');

        /* Lấy tất cả phòng ban và convert về dạng array với key = id và value = tên phòng ban */
        $departments = $this->department_m->select('term_id,term_name')->set_term_type()->as_array()->get_all();
        $departments = $departments ? key_value($departments, 'term_id', 'term_name') : [];
        /* 
         * Lấy tất cả nhóm và convert về dạng array với key = id và value = tên phòng ban
         * Đồng thời group từng nhóm lại theo đúng tên phòng ban chứa nhóm đấy
         */
        $user_groups = $this->user_group_m->select('term_id,term_name,term_parent')->set_term_type()->as_array()->get_all();
        if( ! empty($user_groups))
        {
            $_user_groups = array();
            foreach (array_group_by($user_groups, 'term_parent') as $department_id => $groups)
            {
                if(empty($departments[$department_id])) continue;

                $_user_groups[$departments[$department_id]] = key_value($groups, 'term_id', 'term_name');
            }

            $user_groups = $_user_groups;
        }

        $receipt_enums = [ 'receipt_payment_all' => 'Thanh toán (+ thu chi hộ)'] + $this->config->item('type', 'receipt');

        $data['content'] = $this->datatable_builder
        ->set_filter_position(FILTER_TOP_OUTTER)
        ->select('posts.post_id,posts.post_author,posts.post_title')    

        ->add_column('all_checkbox',array('title'=> '<input type="checkbox" name="all-checkbox" value="" id="all-checkbox" class="minimal"/>','set_order'=> FALSE, 'set_select' => FALSE))
        ->add_search('end_date',['placeholder'=>'Ngày thanh toán','class'=>'form-control set-datepicker'])
        ->add_search('contract_code',['placeholder'=>'Số hợp đồng'])
        ->add_search('customer',['placeholder'=>'Khách hàng'])
        ->add_search('sale_email',['placeholder'=>'Email kinh doanh'])

        ->add_search('vat',array('content'=> form_dropdown(['name'=>'where[vat]','class'=>'form-control select2'],prepare_dropdown([10=>'10'],'VAT'),parent::get('where[vat]'))))
        ->add_search('term_type',array('content'=> form_dropdown(['name'=>'where[term_type]','class'=>'form-control select2'],prepare_dropdown($this->config->item('taxonomy'),'Loại dịch vụ'),parent::get('where[term_type]'))))
        ->add_search('post_type',array('content'=> form_dropdown(['name'=>'where[post_type]','class'=>'form-control select2'],prepare_dropdown($receipt_enums,'Loại thanh toán'),parent::get('where[post_type]'))))
        ->add_search('post_status',array('content'=> form_dropdown(['name'=>'where[post_status]','class'=>'form-control select2'],prepare_dropdown($receipt_status['all'],'Tình trạng'),parent::get('where[post_status]'))))
        ->add_search('transfer_type',array('content'=> form_dropdown(['name'=>'where[transfer_type]','class'=>'form-control select2'],prepare_dropdown($this->config->item('transfer_type','receipt'),'Hình thức chuyển'),parent::get('where[transfer_type]'))))
        ->add_search('departments',array('content'=> form_dropdown(['name'=>'where[departments]','class'=>'form-control select2'],prepare_dropdown($departments,'Phòng ban'),parent::get('where[departments]'))))
        ->add_search('user_groups',array('content'=> form_dropdown(['name'=>'where[user_groups]','class'=>'form-control select2'],prepare_dropdown($user_groups,'Nhóm'),parent::get('where[user_groups]'))))
        ->add_search('action',['content'=>form_button(['name'=>'notification_of_payment','class'=>'btn btn-info', 'id'=>'send-mail-payment'],'<i class="fa fa-fw fa-send"></i> Email')])

        ->add_column('posts.end_date', array('title'=> 'Ngày thanh toán','set_order'=> TRUE))
        ->add_column('contract_code', array('title'=>'Hợp đồng','set_select'=>FALSE,'set_order'=>FALSE))
        ->add_column('amount', array('title'=> 'Số tiền(vnđ)','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('vat', array('title'=> 'VAT','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('amount_not_vat', array('title'=> 'Doanh thu(vnđ)','set_order'=> FALSE,'set_select'=>FALSE))

        ->add_column('vat_amount', array('title'=> 'Thuế','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('service_fee', array('title'=> 'Phí dịch vụ','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('fct_amount', array('title'=> 'Thuế nhà thầu','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('actual_budget', array('title'=> 'Ngân sách thực','set_order'=> TRUE,'set_select'=>FALSE))

        ->add_column('term_type', array('title'=> 'Dịch vụ','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('posts.post_type', array('title'=> 'Loại','set_order'=> TRUE))
        ->add_column('posts.post_status', array('title'=> 'Trạng thái','set_order'=> TRUE))
        ->add_column('is_order_printed', array('title'=> 'In HĐ','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('transfer_type', array('title'=> 'Hình thức','set_order'=> TRUE,'set_select'=>FALSE))

        ->add_column_callback('post_id',function($data,$row_name) use ($sales) {

            $post_id = $data['post_id'];
            
            // Check send mail
            $data['all_checkbox'] = '<input type="checkbox" name="post_ids[]" value="'.$post_id.'" class="minimal"/>' ;
            $has_send_mail_payment = get_post_meta_value($post_id, 'has_send_mail_payment');
            if($has_send_mail_payment == 1)
            {
                $data['all_checkbox'] = '<input type="checkbox" name="nocheck-post_ids[]" value="" class="minimal" checked="checked" disabled="disabled"/>' ;   
            }
        
            // Ngày thanh toán | Hạn chót bảo lãnh
            $data['end_date'] = my_date($data['end_date'],'d/m/Y');

            $taxonomies = $this->config->item('taxonomy');
            $taxonomies = array_keys($taxonomies);
            $term_id    = $this->term_posts_m->get_the_terms($post_id, $taxonomies);
            if( ! empty($term_id)) $term_id = reset($term_id);

            $data['contract_code'] = '<b>'.character_limiter(get_term_meta_value($term_id,'contract_code'), 30) .'</b><span class="clearfix"></span>';
            $sale_id = (int) get_term_meta_value($term_id,'staff_business');

            $user_group = '--';
            $department = '--';
            if( ! empty($sale_id))
            {
                $user_groups = $this->term_users_m->get_user_terms($sale_id, $this->user_group_m->term_type);
                if( ! empty($user_groups)) $user_group = implode(', ', array_column($user_groups, 'term_name'));

                $departments = $this->term_users_m->get_user_terms($sale_id, $this->department_m->term_type);
                if( ! empty($departments)) $department = implode(', ', array_column($departments, 'term_name'));
            }

            // $sale_email = $sales[$sale_id] ?? '--';
            $info = !empty($sales[$sale_id])? $sales[$sale_id] .' / ' . $user_group .' / '. $department : '--';
            $data['contract_code'].= "<span class='text-muted col-xs-12' data-toggle='tooltip' title='NVKD / Nhóm / Phòng ban'><i class='fa fa-fw fa-user'></i>{$info}</span>";

            // Get customer display name
            if($customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company','system']))
            {
                $customer       = end($customers);
                $display_name   = mb_ucwords($this->admin_m->get_field_by_id($customer->user_id,'display_name'));
                $cid            = cid($customer->user_id, $customer->user_type);

                $data['contract_code'].= br()."<span class='text-muted col-xs-12' data-toggle='tooltip' title='Khách hàng'><i class='fa fa-fw fa-user-secret'></i>{$cid} - {$display_name}</span>";
            }

            // Hình thức thanh toán
            $this->load->config('contract/receipt');
            $def_transfer_type = $this->config->item('transfer_type','receipt');
            $transfer_type = get_post_meta_value($post_id,'transfer_type');

            $data['transfer_type'] = $def_transfer_type[$transfer_type] ?? reset($def_transfer_type);

            // tài khoản
            if($transfer_type == 'account')
            {
                $transfer_account = get_post_meta_value($post_id,'transfer_account');
                $data['transfer_type'].= nbs().'<span>'.$transfer_account.'</span>';
            }

            // Số tiền thanh toán
            $amount = get_post_meta_value($post_id,'amount');
            $data['amount'] = currency_numberformat($amount,'');

            $vat_amount = get_post_meta_value($post_id,'vat_amount');
            $data['vat_amount'] = is_numeric($vat_amount) ? currency_numberformat((double) $vat_amount, '') : '-';

            $service_fee = get_post_meta_value($post_id,'service_fee');
            $data['service_fee'] = is_numeric($service_fee) ? currency_numberformat((double) $service_fee, '') : '-';

            $fct_amount = get_post_meta_value($post_id,'fct_amount');
            $data['fct_amount'] = is_numeric($fct_amount) ? currency_numberformat((double) $fct_amount, '') : '-';

            $actual_budget = get_post_meta_value($post_id,'actual_budget');
            $data['actual_budget'] = is_numeric($actual_budget) ? currency_numberformat((double) $actual_budget, '') : '-';

            switch ($data['post_type']) {
                
                case 'receipt_payment_on_behaft':

                    $data['vat'] = currency_numberformat(0, '%');
                    $data['amount_not_vat'] = currency_numberformat($amount, '');
                    break;
                
                default:
                    $vat = (int) get_term_meta_value($term_id,'vat');
                    $data['vat'] = currency_numberformat($vat,'%');

                    $amount_not_vat = div($amount, (1+div($vat, 100)));
                    $data['amount_not_vat'] = currency_numberformat($amount_not_vat,'');
                    break;
            }


            $term = $this->term_m->get($term_id);
            $data['term_type'] = $this->config->item($term->term_type,'taxonomy');

            // Ngày thanh toán
            $payment_time = get_post_meta_value($post_id,'payment_time');
            $data['payment_time'] = my_date($payment_time,'d/m/Y');

            $data['post_author'] = $this->admin_m->get_field_by_id($data['post_author'],'user_email');

            $def_post_status = $this->config->item('status','receipt');
            $data['post_status'] = $def_post_status[$data['post_type']][$data['post_status']];

            $post_type = $data['post_type'];
            $def_post_type = $this->config->item('type','receipt');
            $data['post_type'] = $def_post_type[$post_type];

            $is_order_printed = (bool) get_post_meta_value($post_id,'is_order_printed');
            $data['is_order_printed'] = $is_order_printed ? '<i class="fa fw fa-check-square-o"></i>' : '<i class="fa fw fa-square-o"></i>';

            if($post_type == 'receipt_caution')
            {
                $data['end_date'].= br().'<span class="text-yellow">'.$def_post_type[$post_type].'</span>';
            }

            $data['post_id'] = $post_id;
            // var_dump($data);
            return $data;



        },FALSE)

        ->from('posts')
        ->where('posts.post_status !=','draft')
        ->where_in('posts.post_type',['receipt_payment','receipt_caution', 'receipt_payment_on_behaft'])
        //->generate(['per_page' => 100]);
        ->group_by('posts.post_id');

        $pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

        $data['sum_amount'] = array(
            'amount'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'amount'),array('amount'=>0)),
            // 'vat_amount'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'vat_amount'),array('vat_amount'=>0)),
            // 'service_fee'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'service_fee'),array('service_fee'=>0)),
            // 'actual_budget'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'actual_budget'),array('actual_budget'=>0)),
            // 'fct_amount'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'fct_amount'),array('fct_amount'=>0)),
        );

        $data = $this->datatable_builder->generate($pagination_config);
        // dd($data['headings'], $data['rows']);
        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->export_receipt($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function search_filter_receipt($args = array())
    {
        restrict('contract.revenue.access');

        $args = parent::get();    
        //if(empty($args) && parent::get('search')) $args = parent::get();  
        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);
        
        //departments FILTERING & SORTING
        $filter_departments = $args['where']['departments'] ?? FALSE;
        $filter_user_groups = $args['where']['user_groups'] ?? FALSE;

        if(!empty($filter_departments) || !empty($filter_user_groups)){
            
            $user_ids = array();
            if( ! empty($filter_departments) && $_users = $this->term_users_m->get_the_users($filter_departments, 'admin'))
            {
                $user_ids = array_column($_users, 'user_id');
            }

            if( ! empty($filter_user_groups))
            {
                $_args  = $user_ids ?: array('where_in'=>['term_users.user_id'=>$user_ids]);
                $_users = $this->term_users_m->get_term_users($filter_user_groups, 'admin', $_args);
                $user_ids = $_users ? array_column($_users, 'user_id') : [];
            }

            if(empty($user_ids)) $this->datatable_builder->where('post.post_id', uniqid());

            $alias_post_contract    = uniqid('term_posts_');
            $alias_contract_users   = uniqid('term_users_');
            $alias_meta = uniqid('staff_business_');

            $this->datatable_builder
            ->join("term_posts {$alias_post_contract}","posts.post_id = {$alias_post_contract}.post_id")
            ->join("termmeta {$alias_meta}","{$alias_meta}.term_id = {$alias_post_contract}.term_id and {$alias_meta}.meta_key = 'staff_business'")
            ->where_in("{$alias_meta}.meta_value", $user_ids);
        }   

        // contract_daterange FILTERING & SORTING
        $filter_contract_daterange = $args['where']['contract_daterange'] ?? FALSE;
        $sort_contract_daterange = $args['order_by']['contract_daterange'] ?? FALSE;
        if($filter_contract_daterange || $sort_contract_daterange)
        {
            $alias = uniqid('contract_daterange_');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

            if($sort_contract_daterange)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_daterange);
                unset($args['order_by']['contract_daterange']);
            }
        }

        // post_title FILTERING & SORTING
        empty($args['where']['post_title']) OR $this->datatable_builder->like('posts.post_title', trim($args['where']['post_title']));

        // Contract_begin FILTERING & SORTING
        if( ! empty($args['where']['end_date']))
        {
            $dates = explode(' - ', $args['where']['end_date']);
            $this->datatable_builder
                ->where("posts.end_date >=", $this->mdate->startOfDay(reset($dates)))
                ->where("posts.end_date <=", $this->mdate->endOfDay(end($dates)));
        }
        
        if( ! empty($args['order_by']['end_date']))
        {
            $this->datatable_builder->order_by("posts.end_date",$args['order_by']['end_date']);
        }

        // Contract_code FILTERING & SORTING
        if(!empty($args['where']['contract_code']))
        {
            $termmetas = $this->termmeta_m->select('term_id')
                ->like('meta_value',$args['where']['contract_code'])
                ->get_many_by(['meta_key'=>'contract_code']);

            $this->datatable_builder
                ->join('term_posts','term_posts.post_id = posts.post_id')
                ->where_in('term_posts.term_id', ($termmetas ? array_column($termmetas, 'term_id') : [0]));
        }
        // Customer display_name FILTERING & SORTING
        if(!empty($args['where']['customer']))
        {

            $matches = [];
            preg_match('/^(PE|CO)(\d*)$/i', trim($args['where']['customer']), $matches);
            if( ! empty($matches))
            {    
                $matches = end($matches);
                $this->datatable_builder
                ->join('term_posts','term_posts.post_id = posts.post_id')
                ->join('term_users','term_users.term_id = term_posts.term_id')
                ->where_in('term_users.user_id', (int) $matches);
            }
            else
            {
                $this->load->model('customer/customer_m');
                $customers = $this->customer_m
                    ->select('user.user_id')
                    ->where_in('user.user_type',$this->customer_m->customer_types)
                    ->like('user.display_name', trim($args['where']['customer']))
                    ->get_many_by();

                $this->datatable_builder
                    ->join('term_posts','term_posts.post_id = posts.post_id')
                    ->join('term_users','term_users.term_id = term_posts.term_id')
                    ->where_in('term_users.user_id', ($customers ? array_column($customers, 'user_id') : [0]));
            }
        }

        // Email kinh doanh FILTERING & SORTING
        $filter_sale_email = $args['where']['sale_email'] ?? FALSE;
        $sort_sale_email = $args['order_by']['sale_email'] ?? FALSE;
        if($filter_sale_email || $sort_sale_email)
        {
            $alias = uniqid('staff_business_');
            $this->datatable_builder
                ->join('term_posts','term_posts.post_id = posts.post_id')
                ->join("termmeta {$alias}","{$alias}.term_id = term_posts.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT');

            if($filter_sale_email)
            {   
                $sales = $this->admin_m
                ->select('user.user_id')
                ->set_get_admin()
                ->like('user.user_email',trim($args['where']['sale_email']))
                ->get_many_by();

                $this->datatable_builder->where_in("{$alias}.meta_value",($sales ? array_column($sales, 'user_id') : ['-1']));
            }

            if($sort_sale_email)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_sale_email);
            }
        }

        // Email kinh doanh FILTERING & SORTING
        $filter_vat = $args['where']['vat'] ?? FALSE;
        $sort_vat = $args['order_by']['vat'] ?? FALSE;
        if($filter_vat || $sort_vat)
        {
            $alias = uniqid('vat_');
            $this->datatable_builder
                ->join('term_posts','term_posts.post_id = posts.post_id')
                ->join("termmeta {$alias}","{$alias}.term_id = term_posts.term_id and {$alias}.meta_key = 'vat'",'LEFT');

            if($filter_vat)
            {   
                $this->datatable_builder->where("{$alias}.meta_value",$filter_vat);
            }

            if($sort_vat)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_vat);
            }
        }

        // Tình trạng In hóa đơn FILTERING & SORTING
        $filter_post_type = $args['where']['post_type'] ?? FALSE;
        if($filter_post_type)
        {
            $_condition = 'where';
            if('receipt_payment_all' == $args['where']['post_type'])
            {
                $_condition         = 'where_in';
                $filter_post_type   = ['receipt_payment', 'receipt_payment_on_behaft'];
            }

            $this->datatable_builder->{$_condition}('posts.post_type', $filter_post_type);
        }

        empty($args['order_by']['post_type']) OR $this->datatable_builder->order_by('posts.post_type', $args['order_by']['post_type']);

        // Trạng thái thanh toán FILTERING & SORTING
        if( ! empty($args['where']['post_status']))
        {
            $this->datatable_builder->where('posts.post_status',$args['where']['post_status']);
        }

        if( ! empty($args['where']['post_status']))
        {
            $this->datatable_builder->order_by('posts.post_status',$args['where']['post_status']);
        }

        // Tình trạng In hóa đơn FILTERING & SORTING
        $filter_is_order_printed = $args['where']['is_order_printed'] ?? FALSE;
        $sort_is_order_printed = $args['order_by']['is_order_printed'] ?? FALSE;
        if($filter_is_order_printed || $sort_is_order_printed)
        {
            $alias = uniqid('is_order_printed_');
            $this->datatable_builder
                ->join('term_posts','term_posts.post_id = posts.post_id')
                ->join("termmeta {$alias}","{$alias}.term_id = term_posts.term_id and {$alias}.meta_key = 'is_order_printed'",'LEFT');

            if($filter_is_order_printed)
            {   
                $this->datatable_builder->where("{$alias}.meta_value",$filter_is_order_printed);
            }

            if($sort_is_order_printed)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_is_order_printed);
            }
        }


        // Hình thức chuyển tiền FILTERING & SORTING
        $filter_transfer_type = $args['where']['transfer_type'] ?? FALSE;
        $sort_transfer_type = $args['order_by']['transfer_type'] ?? FALSE;
        if($filter_transfer_type || $sort_transfer_type)
        {
            $alias = uniqid('transfer_type_');
            $this->datatable_builder
            ->join("postmeta {$alias}","{$alias}.post_id = posts.post_id and {$alias}.meta_key = 'transfer_type'",'LEFT');

            if($filter_transfer_type)
            {   
                $this->datatable_builder->where("{$alias}.meta_value",$filter_transfer_type);
            }

            if($sort_transfer_type)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_transfer_type);
            }
        }

        // Loại hình dịch vụ FILTERING & SORTING
        $filter_term_type = $args['where']['term_type'] ?? FALSE;
        $sort_term_type = $args['order_by']['term_type'] ?? FALSE;
        if($filter_term_type || $sort_term_type)
        {
            $this->datatable_builder
            ->join('term_posts','term_posts.post_id = posts.post_id')
            ->join('term','term.term_id = term_posts.term_id','LEFT');

            if($filter_term_type)
            {   
                $this->datatable_builder->where('term.term_type',$filter_term_type);
            }

            if($sort_term_type)
            {
                $this->datatable_builder->order_by('term.term_type',$sort_term_type);
            }
        }

        if( ! empty($args['order_by']['amount']))
        {
            $alias = uniqid('amount_');
            $this->datatable_builder->join("postmeta {$alias}","{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'amount'")->order_by("({$alias}.meta_value * 1)",$args['order_by']['amount']);
        }

        if( ! empty($args['order_by']['vat_amount']))
        {
            $alias = uniqid('vat_amount_');
            $this->datatable_builder->join("postmeta {$alias}","{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'vat_amount'")->order_by("({$alias}.meta_value * 1)",$args['order_by']['vat_amount']);
        }

        if( ! empty($args['order_by']['service_fee']))
        {
            $alias = uniqid('service_fee_');
            $this->datatable_builder->join("postmeta {$alias}","{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'service_fee'")->order_by("({$alias}.meta_value * 1)",$args['order_by']['service_fee']);
        }

        if( ! empty($args['order_by']['actual_budget']))
        {
            $alias = uniqid('actual_budget_');
            $this->datatable_builder->join("postmeta {$alias}","{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'actual_budget'")->order_by("({$alias}.meta_value * 1)",$args['order_by']['actual_budget']);
        }

        if( ! empty($args['order_by']['fct_amount']))
        {
            $alias = uniqid('fct_amount_');
            $this->datatable_builder->join("postmeta {$alias}","{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'fct_amount'")->order_by("({$alias}.meta_value * 1)",$args['order_by']['fct_amount']);
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function export_receipt($query = '')
    {
        if(empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if(FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $posts = $this->receipt_m->query($query)->result();
        if( ! $posts)
        {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(),'refresh');
        }

        $this->load->library('excel');
        $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
        ->getProperties()
        ->setCreator("WEBDOCTOR.VN")
        ->setLastModifiedBy("WEBDOCTOR.VN")
        ->setTitle(uniqid('Danh sách thanh toán __'));

        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/receipt/receipt-list-export.xlsx');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $receipt_type_default       = $this->config->item('type','receipt');
        $transfer_type_default      = $this->config->item('transfer_type','receipt');
        $transfer_account_default   = $this->config->item('transfer_account','receipt');

        $row_index = 3;

        $posts = array_map(function($x){
            $x->term_id = null;
            $term_ids   = $this->term_posts_m->select('term_id')->get_the_terms($x->post_id)
            AND $x->term_id = reset($term_ids);
            return $x;
        }, $posts);

        $contracts = $this->contract_m->set_term_type()->where_in('term_id', array_unique(array_column($posts, 'term_id')))->get_many_by();
        ! empty($contracts) AND $contracts = array_column($contracts, NULL, 'term_id');

        $typeContracts      = array_group_by($contracts, 'term_type');
        $webdoctorContracts = [];

        ! empty($typeContracts['webdesign']) AND $webdoctorContracts = array_merge($typeContracts['webdesign']);
        ! empty($typeContracts['oneweb']) AND $webdoctorContracts = array_merge($typeContracts['oneweb']);

        $this->load->model('webgeneral/webgeneral_kpi_m');
        $kpis = array();

        if( ! empty($webdoctorContracts)
            && $targets = $this->webgeneral_kpi_m->select('kpi_id, term_id, user_id, kpi_type')->order_by('kpi_datetime')->where_in('term_id', array_column($webdoctorContracts, 'term_id'))->as_array()->get_all())
        {
            $targets = array_group_by($targets, 'term_id');

            foreach ($targets as $_term_id => $kpi)
            {
                $typeKpi = array_group_by($kpi, 'kpi_type');
                foreach ($typeKpi as $type => $_kpis)
                {
                    $contracts[$_term_id]->{$type} = implode(', ', array_map(function($_kpi){
                        return $this->admin_m->get_field_by_id($_kpi['user_id'], 'display_name') ?: $this->admin_m->get_field_by_id($_kpi['user_id'], 'user_email');
                    }, $_kpis));
                }
            }
        }

        foreach ($posts as $key => &$post)
        {
            $term = $contracts[$post->term_id];

            
            // Get customer display name
            $customer           = '';
            $cid                = null;
            $customer_tax       = null;
            $customer_address   = null;
            if($term_users      = $this->term_users_m->get_term_users($post->term_id, ['customer_person','customer_company','system']))
            {
                $customer           = end($term_users);
                $cid                = cid($customer->user_id, $customer->user_type);
                $customer_tax       = get_user_meta_value($customer->user_id, 'customer_tax');
                $customer_address   = get_user_meta_value($customer->user_id, 'customer_address');

                $customer           = mb_ucwords($this->admin_m->get_field_by_id($customer->user_id,'display_name'));
            }

            // Get contract code
            $contract_code = get_term_meta_value($post->term_id,'contract_code');

            /* Get New|Renewal Contract */
            $is_first_contract = (bool) get_term_meta_value($term->term_id, 'is_first_contract');

            // Nhân viên kinh doanh phụ trách
            $sale_id    = (int) get_term_meta_value($post->term_id,'staff_business');
            $sale       = $this->admin_m->get_field_by_id($sale_id,'display_name') ?: ($this->admin_m->get_field_by_id($sale_id,'user_email') ?: '--');

            // Số tiền đã thu
            $post->amount = (double) get_post_meta_value($post->post_id,'amount');

            switch ($post->post_type) {
                
                case 'receipt_payment_on_behaft':
                    $vat = 0; /* VAT của hợp đồng */
                    $post->amount_not_vat = $post->amount; /* Doanh thu (-VAT) */
                    break;
                
                default:
                    $vat = (int) get_term_meta_value($post->term_id,'vat'); /* VAT của hợp đồng */
                    $post->amount_not_vat = div($post->amount, (1+div($vat, 100))); /* Doanh thu (-VAT) */
                    break;
            }


            // Loại hình thanh toán
            $transfer_type = get_post_meta_value($post->post_id,'transfer_type');
            $transfer_account = get_post_meta_value($post->post_id,'transfer_account');
            $transfer_by = $transfer_type_default[$transfer_type].' '.($transfer_account_default[$transfer_account] ?? $transfer_account);

            $row_number = $row_index + $key;

            $date = $this->mdate->endOfDay($post->end_date);

            $i = 0;

            // Set Cell
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, PHPExcel_Shared_Date::PHPToExcel($date));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $customer);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $cid);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $customer_tax);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $customer_address);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_code);

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $post->amount);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $sale);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, div($vat,100));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $post->amount_not_vat);

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (double) get_post_meta_value($post->post_id, 'vat_amount'));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (double) get_post_meta_value($post->post_id, 'service_fee'));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (double) get_post_meta_value($post->post_id, 'fct_amount'));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (double) get_post_meta_value($post->post_id, 'actual_budget'));

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $this->config->item($term->term_type,'taxonomy'));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $transfer_by);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $receipt_type_default[$post->post_type]);

            if(in_array($term->term_type, ['webdesign', 'oneweb']))
            {
                $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $term->tech ?? '');
                $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $term->design ?? '');
            }
            else $i+=2;

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $is_first_contract ? 'Ký mới' : 'Tái Ký' );
        }

        $numbers_of_post = count($posts);

        // Set Cells style for Date
        $objPHPExcel->getActiveSheet()
            ->getStyle('A'.$row_index.':A'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY); // Set Cells style for Date
        $objPHPExcel->getActiveSheet()
            ->getStyle('A'.$row_index.':A'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        // Set Cells Style For Amount
        $objPHPExcel->getActiveSheet()
            ->getStyle('G'.$row_index.':G'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        // Set Cells Style For VAT
        $objPHPExcel->getActiveSheet()
            ->getStyle('I'.$row_index.':I'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);

        // Set Cells Style For Amount without VAT
        $objPHPExcel->getActiveSheet()
            ->getStyle('J'.$row_index.':J'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        $objPHPExcel->getActiveSheet()
            ->getStyle('K'.$row_index.':K'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        $objPHPExcel->getActiveSheet()
            ->getStyle('L'.$row_index.':L'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        $objPHPExcel->getActiveSheet()
            ->getStyle('M'.$row_index.':M'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        $objPHPExcel->getActiveSheet()
            ->getStyle('N'.$row_index.':N'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        // Calc sum amount rows and sum amout not vat rows
        $total_amount = array_sum(array_column($posts, 'amount'));
        $total_amount_not_vat = array_sum(array_column($posts, 'amount_not_vat'));

        // Set cell label summary
        $objWorksheet->setCellValueByColumnAndRow(1, ($row_index + $numbers_of_post + 2), 'Tổng cộng');

        // Set cell sumary amount and amount not vat on TOP
        $objWorksheet->setCellValueByColumnAndRow(6, 1, $total_amount);
        $objWorksheet->setCellValueByColumnAndRow(9, 1, $total_amount_not_vat);

        // Set cell sumary amount and amount not vat on BOTTOM
        $objWorksheet->setCellValueByColumnAndRow(6, ($row_index + $numbers_of_post + 2), $total_amount);
        $objWorksheet->setCellValueByColumnAndRow(9, ($row_index + $numbers_of_post + 2), $total_amount_not_vat);
        
        // Set Cells Style For Amount without VAT
        $objPHPExcel->getActiveSheet()
            ->getStyle('G'.($numbers_of_post+$row_index+2))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        // Set Cells Style For Amount without VAT
        $objPHPExcel->getActiveSheet()
            ->getStyle('J'.($numbers_of_post+$row_index+2))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $file_name = "tmp/export-du-thu.xlsx";
        $objWriter->save($file_name);

        try { $objWriter->save($file_name); }
        catch (Exception $e) { trigger_error($e->getMessage()); return FALSE;  }

        if(file_exists($file_name))
        {   
            $this->load->helper('download');
            force_download($file_name,NULL);
        }

        return FALSE;
    }


    /**
     * Insert new Payment
     * @return json
     */
    public function index_post()
    {

        if( ! has_permission('contract.receipt.add'))
		{
            parent::responseHandler(null, 'Không có quyền tạo biên lai thanh toán', 'error', 401);
		}        

        $args = wp_parse_args(parent::post(null, true), [ 'post_author' => $this->admin_m->id ]);

        if(empty($args['contractId']) && empty($args['contract_code']))
        {
            parent::responseHandler(null, "Hợp đồng là dữ liệu bắt buộc", 'error', 400);
        }

        $this->load->model('staffs/sale_m');
        $rules = array(
            'row_id' => [ 'field' => 'row_id', 'label' => 'ID Phiếu', 'rules' => [ 'required', array('row_id_check', array($this->receipt_m, 'row_id_check')) ]],
            'contractId' => [ 'field' => 'contractId', 'label' => 'ID Hợp đồng', 'rules' => ['required', 'numeric', array('contract_id_check', array($this->receipt_m, 'contract_id_check')) ]],
            'post_type' => [ 'field' => 'post_type', 'label' => 'Loại thanh toán', 'rules' => 'required|in_list['. implode(',', array_keys($this->config->item('type', 'receipt'))).']'],
            'is_order_printed' => [ 'field' => 'is_order_printed', 'label' => 'Xuất hóa đơn', 'rules' => [['is_boolean', function($value){ return is_bool($value);} ]]],
            'post_status' => [ 'field' => 'post_status', 'label' => 'Trạng thái', 'rules' => 'required|in_list['. implode(',', [ 'draft', 'publish', 'paid' ]).']'],
            'transfer_type' => [ 'field' => 'transfer_type', 'label' => 'Hình thức', 'rules' => 'required|in_list['. implode(',', array_keys($this->config->item('transfer_type', 'receipt'))).']'],
            'end_date' => [ 'field' => 'end_date', 'label' => 'Ngày thanh toán', 'rules' => 'required|numeric'],
            'input_method' => [ 'field' => 'input_method', 'label' => 'Hình thức nhập', 'rules' => 'required|in_list[default,manual,none]'],
            'amount' => [ 'field' => 'amount', 'label' => 'Số tiền thu', 'rules' => 'required|numeric'],
            'is_range_contract' => [ 'field' => 'is_range_contract', 'label' => 'Hợp đồng thanh toán theo mức ngân sách?', 'rules' => [['is_boolean', function($value){return is_bool($value);} ]]],
            'vat' => [ 'field' => 'vat', 'label' => '% Thuế VAT', 'rules' => 'required|numeric'],
            'fct' => [ 'field' => 'fct', 'label' => '% Thuế nhà thầu', 'rules' => 'required|numeric'],
            'service_provider_tax_rate' => [ 'field' => 'service_provider_tax_rate', 'label' => '% Thuế Google Thu', 'rules' => 'required|numeric'],
            'staff_business_id' => [ 'field' => 'staff_business_id', 'label' => 'Nhân viên kinh doanh', 'rules' => ['required', ['staff_business_check', [$this->sale_m, 'staff_business_check']]]],
        );

        if('promotion' == $args['post_type'])
        {
            $rules['actual_budget'] = [ 
                'field' => 'actual_budget', 
                'label' => 'Số tiền thu', 
                'rules' => 'required|numeric'
            ];
        }
        else {
            // Rule of 'Tài khoản nhận'
            ( ! empty($args['transfer_type']) && 'account' == $args['transfer_type']) AND $transfer_account_rules[] = 'required';
            $transfer_account_rules = ['in_list['. implode(',', array_keys($this->config->item('transfer_account', 'receipt'))).']'];
            $rules['transfer_account'] = [
                'field' => 'transfer_account',
                'label' => 'Tài khoản nhận',
                'rules' => implode('|', $transfer_account_rules)
            ];

            // Rule of 'Range contract'
            if((bool)$args['is_range_contract']){
                $rules['service_fee_rate'] = [
                    'field' => 'service_fee_rate',
                    'label' => '% Phí dịch vụ',
                    'rules' => 'required|numeric|greater_than_equal_to[0]'
                ];
            }

            // Rule of 'Hình thức nhập'
            if('manual' == $args['input_method']){
                $rules['formula_input'] = [
                    'field' => 'formula_input', 
                    'label' => 'Hình thức về phí', 
                    'rules' => 'required|in_list[' . implode(',', array_keys($this->config->item('formula_input', 'receipt'))) . ']',
                ];
                
                if(empty($args['formula_input']) && 'service_fee' == $args['formula_input']){
                    $_rule = [
                        'field' => 'service_fee', 
                        'label' => 'Phí dịch vụ', 
                        'rules' => 'required|numeric',
                    ];

                    ((int) $args['amount'] > 0) ? ($_rule['rules'] . "|less_than_equal_to[{$args['amount']}]") : ($_rule['rules'] . "|greater_than_equal_to[{$args['amount']}]");
                }
            }

        }

        $this->form_validation->set_data($args);
        $this->form_validation->set_rules($rules);

        if( FALSE === $this->form_validation->run())
        {
            if(empty($args['row_id'])) parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);

            $existedId = $this->postmeta_m->select('post_id')->where([ 'meta_key' => 'row_id', 'meta_value' => trim($args['row_id']) ])->get_by();
            if(empty($existedId)) parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);

            $existedId AND $existedId = $existedId->post_id;
            parent::responseHandler([ 'foundId' => (int) $existedId ], $this->form_validation->error_array(), 'error', 403);
        }

        // Main process
        $contract = null;
        if( ! empty($args['contractId']))
        {
            $contract = $this->contract_m->set_term_type()->get((int) $args['contractId']);
        }

        if( ! empty($args['contract_code']))
        {
            $_meta = $this->termmeta_m->select('term_id')->get_by([ 'meta_key' => 'contract_code', 'meta_value' => trim($args['contract_code']) ]);
            $contract = $this->contract_m->set_term_type()->get($_meta->term_id);
        }

        if( empty($contract))
        {
            parent::responseHandler(null, [ 'Không tìm thấy hợp đồng.' ], 'error', 400);
        }

        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        if($manipulation_locked['is_manipulation_locked'])
        {
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
            if($manipulation_locked_at > end_of_day($args['end_date']))
            {
                $manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                parent::responseHandler(null, "Hợp đồng đã khoá thao tác lúc {$manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá.", 'error', 403);
            }
        }

        $adsServiceGroup = $this->config->item('ADSPLUS.VN', 'group-services');
        $adsServiceGroup AND $adsServiceGroup = array_keys($adsServiceGroup);
        $isAdsService = in_array($contract->term_type, $adsServiceGroup);
        if(((int) $args['amount'] < 0) && !$isAdsService){
            parent::responseHandler(null, [ 'Tổng số tiền nhận phải lớn hơn 0' ], 'error', 400);
        }

        $isManualInput = (!empty($args['input_method']) && 'manual' == $args['input_method']);

        if($isAdsService && $isManualInput && 'normal' != get_term_meta_value($contract->term_id, 'contract_budget_payment_type'))
        {
            parent::responseHandler(null, 'Loại hợp đồng không phải công ty thanh toán không hỗ trợ nhập thủ công .', 'error', 400);
        }

        // if(0 == $args['service_fee_rate']){
        //     $args['service_fee_rate'] = get_term_meta_value($contract->term_id, 'contract_budget_payment_type');
        // }

        if($args['is_promotion']){
            $args['amount'] = 0;
            $args['transfer_type'] = 'none';
            $args['transfer_account'] = 'none';
            $args['service_fee'] = 0;
            $args['service_fee_rate'] = 0;
            $args['is_order_printed'] = 0;
            $args['input_method'] = 'none';
            $args['formula_input'] = 'none';
            $args['vat'] = 0;
            $args['fct'] = 0;
        }

        $insertData = elements([ 'post_author', 'post_type', 'post_status', 'end_date', 'post_title' ], $args);
        $insertedId = $this->receipt_m->insert($insertData);
 
        $meta_key = [ 
            'row_id',
            'is_order_printed',
            'transfer_type',
            'transfer_account',
            'input_method',
            'amount',
            'vat',
            'fct',
            'service_fee',
            'service_fee_rate',
            'service_provider_tax_rate',
            'formula_input',
            'actual_budget',
            'is_promotion',
            'staff_business_id',
        ];
        $insertMeta = elements($meta_key, $args);
        foreach($insertMeta as $key => $value)
        {
            update_post_meta($insertedId, $key, $value);
        }

        $this->term_posts_m->set_term_posts($contract->term_id, [ $insertedId ]);
        $this->receipt_m->update($insertedId, [ 
            'updated_on' => time(), 
            'post_type' => $args['post_type'],
            'post_status' => $args['post_status']
        ]);

        if(in_array($args['post_status'], ['paid', 'publish']))
        {
            try {
                /* Add Deal Hubspot Job for another service queue work */
                $this->log_m->insert(array(
                    'log_type' =>'callDealHubspotApi',
                    'user_id' => $this->admin_m->id,
                    'log_status' => 0,
                    'log_content' => serialize([
                        'action' => 'DealPaymented',
                        'term_id' => $contract->term_id,
                        'post_id' => $insertedId
                    ])
                ));

                if($args['amount'] > 0)
                {
                    /* POST SLACK */
                    $this->common_report_m->init($contract->term_id);
                    $this->common_report_m->setReceipt($insertedId);
                    $this->common_report_m->postSlackNewCustomerPayment();

                    /* Post Money24h App */	
                    $this->common_report_m->init($contract->term_id);
                    $this->common_report_m->setReceipt($insertedId);
                    $this->common_report_m->postMoney24HNewCustomerPayment();
                }
            } 
            catch (Exception $e)
            {
                # need for log
            }
        }

        // Push queue sync service fee. Check contract end
        $stop_status = ['ending', 'liquidation'];
        if(in_array($contract->term_status, $stop_status)){
            $this->load->config('amqps');
            $amqps_host     = $this->config->item('host', 'amqps');
            $amqps_port     = $this->config->item('port', 'amqps');
            $amqps_user     = $this->config->item('user', 'amqps');
            $amqps_password = $this->config->item('password', 'amqps');
            
            $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
            $queue = $amqps_queues['contract_events'];
    
            $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
            $channel     = $connection->channel();
            $channel->queue_declare($queue, false, true, false, false);
    
            $payload = [
                'event' 	=> 'contract_payment.sync.service_fee',
                'contract_id' => $contract->term_id
            ];
            $message = new \PhpAmqpLib\Message\AMQPMessage(
                json_encode($payload),
                array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
            );
            $channel->basic_publish($message, '', $queue);	
    
            $channel->close();
            $connection->close();
        }

        parent::responseHandler($insertedId, "#{$insertedId} đã được thêm mới thành công");
    }

    /**
     * Update the Payment
     * @return json
     */
    public function index_put($id)
    {
        if( ! has_permission('contract.receipt.update'))
		{
            parent::responseHandler(null, 'Không có quyền tạo biên lai thanh toán', 'error', 401);
		}

        $args = wp_parse_args(parent::put(null, true), [
            'post_id' => $id,
            'updated_on' => time(),
        ]);

        if(empty($args['contractId']) && empty($args['contract_code']))
        {
            parent::responseHandler(null, "Hợp đồng là dữ liệu bắt buộc", 'error', 400);
        }

        $rules = array(
            'row_id' => [ 'field' => 'row_id', 'label' => 'ID Phiếu', 'rules' => [ array('row_id_exist_check', array($this->receipt_m, 'row_id_exist_check')) ]],
            'contractId' => [ 'field' => 'contractId', 'label' => 'ID Hợp đồng', 'rules' => ['required', 'numeric', array('contract_id_check', array($this->receipt_m, 'contract_id_check')) ]],
            
            'post_type' => [ 'field' => 'post_type', 'label' => 'Loại thanh toán', 'rules' => 'required|in_list['. implode(',', array_keys($this->config->item('type', 'receipt'))).']'],
            'is_order_printed' => [ 'field' => 'is_order_printed', 'label' => 'Xuất hóa đơn', 'rules' => [['is_boolean', function($value){ return is_bool($value);} ]]],
            'post_status' => [ 'field' => 'post_status', 'label' => 'Trạng thái', 'rules' => 'required|in_list['. implode(',', [ 'draft', 'publish', 'paid' ]).']'],
            'transfer_type' => [ 'field' => 'transfer_type', 'label' => 'Hình thức', 'rules' => 'required|in_list['. implode(',', array_keys($this->config->item('transfer_type', 'receipt'))).']'],
            'end_date' => [ 'field' => 'end_date', 'label' => 'Ngày thanh toán', 'rules' => 'required|numeric'],
            'input_method' => [ 'field' => 'input_method', 'label' => 'Hình thức nhập', 'rules' => 'required|in_list[default,manual,none]'],
            'amount' => [ 'field' => 'amount', 'label' => 'Số tiền thu', 'rules' => 'required|numeric'],
            'is_range_contract' => [ 'field' => 'is_range_contract', 'label' => 'Hợp đồng thanh toán theo mức ngân sách?', 'rules' => [['is_boolean', function($value){return is_bool($value);} ]]],
            'vat' => [ 'field' => 'vat', 'label' => '% Thuế VAT', 'rules' => 'required|numeric'],
            'fct' => [ 'field' => 'fct', 'label' => '% Thuế nhà thầu', 'rules' => 'required|numeric'],
            'service_provider_tax_rate' => [ 'field' => 'service_provider_tax_rate', 'label' => '% Thuế Google Thu', 'rules' => 'required|numeric'],
            'staff_business_id' => [ 'field' => 'staff_business_id', 'label' => 'Kinh doanh phụ trách', 'rules' => 'required|numeric'],
        );

        if('promotion' == $args['post_type'])
        {
            $rules['actual_budget'] = [ 
                'field' => 'actual_budget', 
                'label' => 'Số tiền thu', 
                'rules' => 'required|numeric'
            ];
        }
        else {
            // Rule of 'Tài khoản nhận'
            ( ! empty($args['transfer_type']) && 'account' == $args['transfer_type']) AND $transfer_account_rules[] = 'required';
            $transfer_account_rules = ['in_list['. implode(',', array_keys($this->config->item('transfer_account', 'receipt'))).']'];
            $rules['transfer_account'] = [
                'field' => 'transfer_account',
                'label' => 'Tài khoản nhận',
                'rules' => implode('|', $transfer_account_rules)
            ];

            // Rule of 'Range contract'
            if((bool)$args['is_range_contract']){
                $rules['service_fee_rate'] = [
                    'field' => 'service_fee_rate',
                    'label' => '% Phí dịch vụ',
                    'rules' => 'required|numeric|greater_than_equal_to[0]'
                ];
            }

            // Rule of 'Hình thức nhập'
            if('manual' == $args['input_method']){
                $rules['formula_input'] = [
                    'field' => 'formula_input', 
                    'label' => 'Hình thức về phí', 
                    'rules' => 'required|in_list[' . implode(',', array_keys($this->config->item('formula_input', 'receipt'))) . ']',
                ];
                
                if(empty($args['formula_input']) && 'service_fee' == $args['formula_input']){
                    $_rule = [
                        'field' => 'service_fee', 
                        'label' => 'Phí dịch vụ', 
                        'rules' => 'required|numeric',
                    ];

                    ((int) $args['amount'] > 0) ? ($_rule['rules'] . "|less_than_equal_to[{$args['amount']}]") : ($_rule['rules'] . "|greater_than_equal_to[{$args['amount']}]");
                }
            }
        }

        $this->form_validation->set_data($args);
        $this->form_validation->set_rules($rules);
        
        if( FALSE === $this->form_validation->run())
        {
            parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        // Main process
        if(empty(trim($args['row_id'])))
        {
            $args['row_id'] = uuidv4();
        }

        $contract = null;
        if( ! empty($args['contractId']))
        {
            $contract = $this->contract_m->set_term_type()->get((int) $args['contractId']);
        }

        if( ! empty($args['contract_code']))
        {
            $_meta = $this->termmeta_m->select('term_id')->get_by([ 'meta_key' => 'contract_code', 'meta_value' => trim($args['contract_code']) ]);
            $contract = $this->contract_m->set_term_type()->get($_meta->term_id);
        }

        if( empty($contract))
        {
            parent::responseHandler(null, [ 'Không tìm thấy hợp đồng.' ], 'error', 400);
        }

        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        if($manipulation_locked['is_manipulation_locked'])
        {
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
            if($manipulation_locked_at > end_of_day($args['end_date']))
            {
                $manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                parent::responseHandler(null, "Hợp đồng đã khoá thao tác lúc {$manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá.", 'error', 403);
            }
            
            $receipt = $this->receipt_m->select('post_id, end_date')->where('post_id', $id)->as_array()->get_by();
            $receipt_date = $receipt['end_date'] ?: 0;
            
            if($manipulation_locked_at > end_of_day($receipt_date))
            {
                $manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                parent::responseHandler(null, "Hợp đồng đã khoá thao tác lúc {$manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá.", 'error', 403);
            }
        }

        $adsServiceGroup = $this->config->item('ADSPLUS.VN', 'group-services');
        $adsServiceGroup AND $adsServiceGroup = array_keys($adsServiceGroup);
        $isAdsService = in_array($contract->term_type, $adsServiceGroup);
        if(((int) $args['amount'] < 0) && !$isAdsService){
            parent::responseHandler(null, [ 'Tổng số tiền nhận phải lớn hơn 0' ], 'error', 400);
        }

        $isManualInput = (! empty($args['input_method']) && 'manual' == $args['input_method']);
        if($isAdsService && $isManualInput && 'normal' != get_term_meta_value($contract->term_id, 'contract_budget_payment_type'))
        {
            parent::responseHandler(null, [ 'Loại hợp đồng không phải công ty thanh toán không hỗ trợ nhập thủ công .' ], 'error', 400);
        }

        $this->load->model('staffs/department_m');
        $staff_business_id = get_post_meta_value($id, 'staff_business_id');
        $departments = $this->term_users_m->get_user_terms($staff_business_id, $this->department_m->term_type);
        if(!empty($departments))
        {
            $department_id = array_keys($departments);
            $department_id = reset($department_id);
            $department_staffs = $this->department_m->get_staffs($department_id, [1, -1]);

            $user_ids = array_column($department_staffs, 'user_id');
            if(!in_array($args['staff_business_id'], $user_ids))
            {
                parent::responseHandler(null, [ 'Kinh doanh phụ trách không thuộc phòng ban.' ], 'error', 400);
            }
        }

        if($args['is_promotion']){
            $args['amount'] = 0;
            $args['transfer_type'] = 'none';
            $args['transfer_account'] = 'none';
            $args['service_fee'] = 0;
            $args['is_order_printed'] = 0;
            $args['input_method'] = 'none';
            $args['formula_input'] = 'none';
            $args['vat'] = 0;
            $args['fct'] = 0;
        }

        $metaChanges = elements([
            'amount',
            'transfer_type',
            'transfer_account',
            'row_id',
            'service_fee',
            'service_fee_rate',
            'is_order_printed',
            'input_method',
            'formula_input',
            'is_promotion',
            'service_provider_tax_rate',
            'actual_budget',
            'vat',
            'fct',
            'staff_business_id'
        ], $args);

        foreach($metaChanges as $key => $value)
        {
            update_post_meta($id, $key, $value);
        }


        $dataChanges = elements([ 'post_type', 'post_status', 'end_date', 'post_title', 'updated_on' ], $args);
        $this->receipt_m->update($id, $dataChanges);

        if(in_array($args['post_status'], ['paid', 'publish']))
        {
            try
            {
                /* Add Deal Hubspot Job for another service queue work */
                $this->log_m->insert(array(
                    'log_type' =>'callDealHubspotApi',
                    'user_id' => $this->admin_m->id,
                    'log_status' => 0,
                    'log_content' => serialize([
                        'action' => 'DealPaymented',
                        'term_id' => $contract->term_id,
                        'post_id' => $id
                    ])
                ));

                if($args['amount'] > 0)
                {
                    /* POST SLACK */
                    $this->common_report_m->init($contract->term_id);
                    $this->common_report_m->setReceipt($id);
                    $this->common_report_m->postSlackNewCustomerPayment();

                    /* Post Money24h App */	
                    $this->common_report_m->init($contract->term_id);
                    $this->common_report_m->setReceipt($id);
                    $this->common_report_m->postMoney24HNewCustomerPayment();
                }
            } 
            catch (Exception $e)
            {
                # need for log
                parent::responseHandler(null, [$e->getMessage()], 'error', 500);
            }
        }

        // Push queue sync service fee. Check contract end
        $stop_status = ['ending', 'liquidation'];
        if(in_array($contract->term_status, $stop_status)){
            $this->load->config('amqps');
            $amqps_host     = $this->config->item('host', 'amqps');
            $amqps_port     = $this->config->item('port', 'amqps');
            $amqps_user     = $this->config->item('user', 'amqps');
            $amqps_password = $this->config->item('password', 'amqps');
            
            $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
            $queue = $amqps_queues['contract_events'];
    
            $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
            $channel     = $connection->channel();
            $channel->queue_declare($queue, false, true, false, false);
    
            $payload = [
                'event' 	=> 'contract_payment.sync.service_fee',
                'contract_id' => $contract->term_id
            ];
            $message = new \PhpAmqpLib\Message\AMQPMessage(
                json_encode($payload),
                array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
            );
            $channel->basic_publish($message, '', $queue);	
    
            $channel->close();
            $connection->close();
        }
        
        parent::responseHandler($id, "#{$id} đã được cập nhật thành công");
    }

    /**
     * View a Payment Receipt
     *
     * @param      int   $id     The identifier
     */
    public function view_get($id = 0)
    {
        if( ! $this->receipt_m->existed_n_owned_check($id))
        {
            parent::responseHandler(null, 'Thanh toán không tồn tại hoặc không có quyền truy cập', 'error', 400);
        }

        $receipt = $this->receipt_m->set_post_type()->get($id);

        $contract = $this->term_posts_m->get_post_terms($id, $this->contract_m->getTypes());
        if($contract)
        {
            $contract = array_filter($contract);
            $contract = reset($contract);
            $contract = reset($contract);

            $contract->contract_code = get_term_meta_value($contract->term_id, 'contract_code');
            $contract->service_fee_rate_actual = (double) get_term_meta_value($contract->term_id, 'service_fee_rate_actual');
            $contract->service_fee = (double) get_term_meta_value($contract->term_id, 'service_fee');
            $contract->contract_budget_payment_type = get_term_meta_value($contract->term_id, 'contract_budget_payment_type') ?: 'normal';
            $contract->service_fee_payment_type = get_term_meta_value($contract->term_id, 'service_fee_payment_type');
            $contract->vat = (double) div((int) get_post_meta_value($id, 'vat'), 100) ?: (double) div((int) get_term_meta_value($contract->term_id, 'vat'), 100);
            $contract->fct = (double) div((int) get_post_meta_value($id, 'fct'), 100) ?: (double) div((int) get_term_meta_value($contract->term_id, 'fct'), 100);
            $contract->service_provider_tax_rate = (float) get_term_meta_value($contract->term_id, 'service_provider_tax_rate');

            $actual_result = (double) get_term_meta_value($contract->term_id, 'actual_result') ?: 0;
            $balance_spend = (double) get_term_meta_value($contract->term_id, 'balance_spend') ?: 0;
            $contract->actual_result = $actual_result + $balance_spend;

            $receipt->contract = elements([ 
                'term_id',
                'term_name',
                'term_type',
                'term_status',
                'contract_code',
                'service_fee_rate_actual',
                'vat',
                'contract_budget_payment_type',
                'actual_result',
                'service_fee_rate_actual',
                'service_fee',
                'service_fee_payment_type',
                'service_provider_tax_rate'
            ], (array) $contract);
            $receipt->contract['term_id'] = (int) $receipt->contract['term_id'];
            $receipt->contract['service_fee_rate_actual'] = (double) $receipt->contract['service_fee_rate_actual'];
            $receipt->contract['vat'] = (double) $receipt->contract['vat'];
        }

        $receipt->is_order_printed = (bool) get_post_meta_value($id, 'is_order_printed');
        $receipt->is_promotion = (bool) get_post_meta_value($id, 'is_promotion');
        $receipt->input_method = get_post_meta_value($id, 'input_method') ?: 'default';
        $receipt->formula_input = get_post_meta_value($id, 'formula_input');
        $receipt->row_id = get_post_meta_value($id, 'row_id');
        $receipt->transfer_type = get_post_meta_value($id, 'transfer_type');
        $receipt->transfer_account = get_post_meta_value($id, 'transfer_account');
        $receipt->service_fee =  (double) get_post_meta_value($id, 'service_fee');
        $receipt->service_fee_rate =  (double) get_post_meta_value($id, 'service_fee_rate');
        $receipt->post_id = (int) $receipt->post_id;
        $receipt->end_date = (int) $receipt->end_date;

        $receipt->service_provider_tax_rate = (float) get_post_meta_value($id, 'service_provider_tax_rate');
        
        if($receipt->is_promotion){
            $receipt->amount = (double) get_post_meta_value($id, 'actual_budget');
        }
        else{
            $receipt->amount = (double) get_post_meta_value($id, 'amount');
        }

        $receipt->staff_business = NULL;
        $receipt->department_staffs = [];
        $this->load->model('staffs/department_m');
        $staff_business_id = get_post_meta_value($id, 'staff_business_id') ?: 0;
        if(!empty($staff_business_id))
        {
            $sale_data = elements(['user_id', 'display_name', 'user_email'], $this->admin_m->get_field_by_id($staff_business_id));

            $sale_data['departments'] = null;
            $departments = $this->term_users_m->get_user_terms($staff_business_id, $this->department_m->term_type);
            if(!empty($departments))
            {
                $sale_data['departments'] = array_column($departments, 'term_name');
                
                $department_id = array_keys($departments);
                $department_id = reset($department_id);
                $receipt->department_staffs = $this->department_m->get_staffs($department_id, [-1, 1]);
            }

            $receipt->staff_business = $sale_data;
        }

        $data = elements([
            'post_id',
            'post_title',
            'post_type',
            'post_status',
            'end_date',
            'is_order_printed',
            'is_promotion',
            'input_method',
            'formula_input',
            'row_id',
            'transfer_type',
            'transfer_account',
            'amount',
            'service_fee',
            'service_fee_rate',
            'service_provider_tax_rate',
            'contract',
            'staff_business',
            'department_staffs'
        ], (array) $receipt);

        parent::responseHandler($data, 'OK');
    }
}
/* End of file Receipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/Receipts.php */