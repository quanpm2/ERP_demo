<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetTotal extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'report_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'staffs/department_m';
        $this->autoload['models'][] = 'staffs/user_group_m';

        parent::__construct();

        $this->load->config('contract/receipt');
        $this->load->config('contract/contract');
        $this->load->config('googleads/contract');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('reports.total.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults           = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args               = wp_parse_args(parent::get(), $defaults);
        $pagination_config  = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');

        // Filtering 
        $this->filtering($args);

        // Data builder
        $relate_users = $this->admin_m->get_all_by_permissions('reports.receipt_caution.access');
        if (!$relate_users) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        if (is_array($relate_users)) {
            $contract = $this->contract_m->select('term.term_id')
                ->join("termmeta m_contract", "m_contract.term_id = term.term_id AND m_contract.meta_key = 'staff_business'", 'LEFT')
                ->where_in('m_contract.meta_value', $relate_users)
                ->as_array()
                ->get_all();
            $contract_ids = array_column($contract, 'term_id');

            $this->datatable_builder->where_in('total_report.term_id', $contract_ids);
        }

        $contract_budget_payment_types = $this->config->item('enums', 'contract_budget_payment_types');
        $service_fee_payment_types = $this->config->item('service_fee_payment_type');

        $departmentData = $this->department_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $departmentData = $departmentData ? key_value($departmentData, 'term_id', 'term_name') : [];

        $userGroupData = $this->user_group_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $userGroupData = $userGroupData ? key_value($userGroupData, 'term_id', 'term_name') : [];

        $data = $this->datatable_builder
            ->add_search('customer', ['placeholder' => 'Khách hàng'])
            ->add_search('contract_code', ['placeholder' => 'Số hợp đồng'])
            ->add_search('contract_budget_payment_type', ['content' => form_dropdown(['name' => 'where[contract_budget_payment_type]', 'class' => 'form-control select2'], prepare_dropdown($contract_budget_payment_types, 'Loại hợp đồng'), parent::get('where[contract_budget_payment_type]'))])
            ->add_search('service_fee_payment_type', ['content' => form_dropdown(['name' => 'where[service_fee_payment_type]', 'class' => 'form-control select2'], prepare_dropdown($service_fee_payment_types, 'Phương thức thanh toán phí DV'), parent::get('where[service_fee_payment_type]'))])
            ->add_search('service_fee_rate_actual', ['placeholder' => '% phí dịch vụ'])
            ->add_search('contract_begin', ['placeholder' => 'Ngày bắt đầu', 'class' => 'form-control set-datepicker'])
            ->add_search('contract_end', ['placeholder' => 'Ngày kết thúc', 'class' => 'form-control set-datepicker'])
            ->add_search('advertise_start_time', ['placeholder' => 'Ngày chạy quảng cáo', 'class' => 'form-control set-datepicker'])
            ->add_search('budget_payment', ['placeholder' => 'Ngân sách'])
            ->add_search('service_fee_payment', ['placeholder' => 'Phí dịch vụ'])
            ->add_search('fct_amount_payment', ['placeholder' => 'Thuế nhà thầu'])
            ->add_search('vat_amount_payment', ['placeholder' => 'GTGT'])
            ->add_search('amount_payment', ['placeholder' => 'Tổng tiền đã thu'])
            ->add_search('spend', ['placeholder' => 'Ngân sách'])
            ->add_search('balance_spend', ['placeholder' => 'Cân bằng thủ công'])
            ->add_search('service_fee_actual', ['placeholder' => 'Phí dịch vụ'])
            ->add_search('budget_remain', ['placeholder' => 'Ngân sách'])
            ->add_search('service_fee_remain', ['placeholder' => 'Phí dịch vụ'])
            ->add_search('total_remain', ['placeholder' => 'Tổng cộng'])
            ->add_search('staff_business', ['placeholder' => 'NVKD'])
            ->add_search('department', ['content' => form_dropdown(['name' => 'where[department]', 'class' => 'form-control select2'], prepare_dropdown($departmentData, 'Phòng ban'), parent::get('where[department]'))])
            ->add_search('user_group', ['content' => form_dropdown(['name' => 'where[user_group]', 'class' => 'form-control select2'], prepare_dropdown($userGroupData, 'Nhóm'), parent::get('where[user_group]'))])

            ->add_column('cid', array('title' => 'CID', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('customer', array('title' => 'Khách hàng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_code', array('title' => 'Số hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_budget_payment_type', array('title' => 'Loại hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_fee_payment_type', array('title' => 'Phương thức thanh toán phí DV', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_fee_rate_actual', array('title' => '% phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_begin', array('title' => 'Ngày bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_end', array('title' => 'Ngày kết thúc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('advertise_start_time', array('title' => 'Ngày chạy quảng cáo', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('budget_payment', array('title' => 'Ngân sách', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_fee_payment', array('title' => 'Phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('fct_amount_payment', array('title' => 'Thuế nhà thầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('vat_amount_payment', array('title' => 'GTGT', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('amount_payment', array('title' => 'Tổng tiền đã thu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('spend', array('title' => 'Ngân sách', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('balance_spend', array('title' => '+/- Cân bằng thủ công', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_fee_actual', array('title' => 'Phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('budget_remain', array('title' => 'Ngân sách', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_fee_remain', array('title' => 'Phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('total_remain', array('title' => 'Tổng cộng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('staff_business', array('title' => 'NVKD', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('department', array('title' => 'Phòng ban', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('user_group', array('title' => 'Nhóm', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('staff_techs', array('title' => 'NVKT', 'set_select' => FALSE, 'set_order' => FALSE))

            ->add_column_callback('term_id', function ($data, $row_name) use ($contract_budget_payment_types, $service_fee_payment_types) {
                $term_id = $data['term_id'];

                // Get cid
                $cid = $data['cid'];
                empty($cid) and $cid = cid($data['user_id'], $data['user_type']);
                $data['cid'] = $cid;

                // Get contract_budget_payment_type
                $contract_budget_payment_type = get_term_meta_value($term_id, 'contract_budget_payment_type');
                $data['contract_budget_payment_type'] = $contract_budget_payment_types[$contract_budget_payment_type] ?? '';

                // Get service_fee_payment_type
                $service_fee_payment_type = get_term_meta_value($term_id, 'service_fee_payment_type');
                $data['service_fee_payment_type'] = $service_fee_payment_types[$service_fee_payment_type] ?? '';

                // Get contract_code
                $contract_code = get_term_meta_value($term_id, 'contract_code');
                $data['contract_code'] = $contract_code ?: '';

                // Get contract_begin
                $contract_begin = get_term_meta_value($term_id, 'contract_begin');
                $data['contract_begin'] = $contract_begin ? my_date($contract_begin, 'd/m/Y') : '';

                // Get contract_end
                $contract_end = get_term_meta_value($term_id, 'contract_end');
                $data['contract_end'] = $contract_end ? my_date($contract_end, 'd/m/Y') : '';

                // Get advertise_start_time
                $advertise_start_time = get_term_meta_value($term_id, 'advertise_start_time');
                $data['advertise_start_time'] = $advertise_start_time ? my_date($advertise_start_time, 'd/m/Y') : '';

                // Get staff_business
                $data['staff_business'] = '';
                $staff_business = get_term_meta_value($term_id, 'staff_business');
                if (!empty($staff_business)) {
                    $data['staff_business'] = [];
                    $data['staff_business']['user_id'] = $staff_business;
                    $data['staff_business']['display_name'] = $this->admin_m->get_field_by_id($staff_business, 'display_name');
                    $data['staff_business']['user_avatar'] = $this->admin_m->get_field_by_id($staff_business, 'user_avatar');
                }

                // Get departments
                $data['department'] = '';
                $departments = $this->term_users_m->get_user_terms($staff_business, $this->department_m->term_type);
                $departments and $data['department'] = implode(', ', array_column($departments, 'term_name'));

                // Get user_groups
                $data['user_group'] = '';
                $user_groups = $this->term_users_m->get_user_terms($staff_business, $this->user_group_m->term_type);
                $user_groups and $data['user_group'] = implode(', ', array_column($user_groups, 'term_name'));

                // Get staff_techs
                $data['staff_techs'] = '';

                $model_kpi = '';
                switch ($data['term_type']) {
                    case 'courseads':
                        $this->load->model('courseads/courseads_kpi_m');
                        $model_kpi = 'courseads_kpi_m';
                        break;
                    case 'banner':
                        $this->load->model('banner/banner_kpi_m');
                        $model_kpi = 'banner_kpi_m';
                        break;
                    case 'facebook-ads':
                        $this->load->model('facebookads/facebookads_kpi_m');
                        $model_kpi = 'facebookads_kpi_m';
                        break;
                    case 'google-ads':
                        $this->load->model('googleads/googleads_kpi_m');
                        $model_kpi = 'googleads_kpi_m';
                        break;
                    case 'hosting':
                        $this->load->model('hosting/hosting_kpi_m');
                        $model_kpi = 'hosting_kpi_m';
                        break;
                    case 'onead':
                        $this->load->model('onead/onead_kpi_m');
                        $model_kpi = 'onead_kpi_m';
                        break;
                    case 'oneweb' || 'webgeneral':
                        $this->load->model('webgeneral/webgeneral_kpi_m');
                        $model_kpi = 'webgeneral_kpi_m';
                        break;
                    case 'seo-traffic':
                        $this->load->model('seotraffic/seotraffic_kpi_m');
                        $model_kpi = 'seotraffic_kpi_m';
                        break;
                    case 'webdoctor':
                        $this->load->model('webdoctor/webdoctor_kpi_m');
                        $model_kpi = 'webdoctor_kpi_m';
                        break;
                    case 'weboptimize':
                        $this->load->model('weboptimize/weboptimize_kpi_m');
                        $model_kpi = 'weboptimize_kpi_m';
                        break;
                    default:
                        break;
                }

                $kpis = [];
                if ($data['term_type'] == 'courseads') {

                    $kpis = $this->$model_kpi
                        ->order_by('kpi_type')
                        ->where('object_id', $term_id)
                        ->as_array()
                        ->get_many_by();
                } else {
                    $kpis = $this->$model_kpi->get_many_by(['term_id' => $term_id, 'kpi_type' => 'tech']);
                }

                if (!empty($kpis)) {
                    $staff_techs = array_map(function ($x) {
                        return $this->admin_m->get_field_by_id($x->user_id, 'display_name');
                    }, $kpis);

                    $data['staff_techs'] = implode(', ', $staff_techs);
                }

                return $data;
            }, FALSE)

            ->from('total_report')
            ->select('total_report.id, total_report.term_id, total_report.term_type, total_report.user_id, total_report.display_name, total_report.user_type, total_report.post_id, total_report.post_type, total_report.start_date, total_report.end_date, total_report.service_fee_rate_actual')
            ->select('sum(budget_payment) AS budget_payment')
            ->select('sum(service_fee_payment) AS service_fee_payment')
            ->select('sum(amount_payment) AS amount_payment')
            ->select('sum(fct_amount_payment) AS fct_amount_payment')
            ->select('sum(vat_amount_payment) AS vat_amount_payment')
            ->select('(sum(spend)) AS spend')
            ->select('((sum(spend) + sum(balance_spend)) * service_fee_rate_actual) AS service_fee_actual')
            ->select('(sum(budget_payment) - (sum(spend) + sum(balance_spend))) AS budget_remain')
            ->select('(sum(service_fee_payment) - ((sum(spend) + sum(balance_spend)) * service_fee_rate_actual)) AS service_fee_remain')
            ->select('((sum(budget_payment) - (sum(spend) + sum(balance_spend))) + (sum(service_fee_payment) - ((sum(spend) + sum(balance_spend)) * service_fee_rate_actual))) AS total_remain')
            ->select('sum(balance_spend) AS balance_spend')

            ->group_by('total_report.term_id, total_report.user_id')
            ->order_by('total_report.term_id', 'DESC')
            ->generate($pagination_config, 'JSON');

        $last_query = $this->datatable_builder->last_query();
        $pos = strpos($last_query, 'LIMIT');
        if (FALSE !== $pos) $last_query = mb_substr($last_query, 0, $pos);
        $subheadings_data = $this->db->query($last_query)->result_array();
        $subheadings = array_reduce($subheadings_data, function ($result, $item) {
            $result['budget_payment'] += (int)$item['budget_payment'];
            $result['service_fee_payment'] += (int)$item['service_fee_payment'];
            $result['amount_payment'] += (int)$item['amount_payment'];
            $result['fct_amount_payment'] += (int)$item['fct_amount_payment'];
            $result['vat_amount_payment'] += (int)$item['vat_amount_payment'];
            $result['spend'] += (int)$item['spend'];
            $result['service_fee_actual'] += (int)$item['service_fee_actual'];
            $result['budget_remain'] += (int)$item['budget_remain'];
            $result['service_fee_remain'] += (int)$item['service_fee_remain'];
            $result['total_remain'] += (int)$item['total_remain'];
            $result['balance_spend'] += (int)$item['balance_spend'];

            return $result;
        }, [
            'budget_payment' => 0,
            'service_fee_payment' => 0,
            'amount_payment' => 0,
            'fct_amount_payment' => 0,
            'vat_amount_payment' => 0,
            'spend' => 0,
            'service_fee_actual' => 0,
            'budget_remain' => 0,
            'service_fee_remain' => 0,
            'total_remain' => 0,
            'balance_spend' => 0,
        ]);
        $subheadings = array_map(function ($item) {
            $item .= '';
            return $item;
        }, $subheadings);
        $data['subheadings'] = $subheadings;

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export_total_report($last_query);
            return TRUE;
        }

        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering($args = array())
    {
        restrict('reports.total.access');

        $args = parent::get();
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // start_time FILTERING & SORTING
        $filter_start_time = $args['start_time'] ?? FALSE;
        if ($filter_start_time) {
            $this->datatable_builder->where("total_report.end_date >=", $filter_start_time);
        }

        // end_time FILTERING & SORTING
        $filter_end_time = $args['end_time'] ?? FALSE;
        if ($filter_end_time) {
            $this->datatable_builder->where("total_report.end_date <=", $filter_end_time);
        }

        $sort_cid = $args['order_by']['cid'] ?? FALSE;
        if ($sort_cid) {
            $this->datatable_builder->order_by('user_id', $sort_cid);
        }

        // staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if ($filter_staff_business || $sort_staff_business) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('staff_business_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = total_report.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("user $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT');

            if ($filter_staff_business) {
                $this->datatable_builder->like("$aliasStaffBusiness.display_name", $filter_staff_business);
            }

            if ($sort_staff_business) {
                $this->datatable_builder->order_by("$aliasStaffBusiness.display_name", $sort_staff_business);
            }
        }

        // department FILTERING & SORTING
        $filter_department = $args['where']['department'] ?? FALSE;
        $sort_department = $args['order_by']['department'] ?? FALSE;
        if ($filter_department || $sort_department) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('tu_department_');
            $aliasDepartment = uniqid('department_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = total_report.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("term_users $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT')
                ->join("term $aliasDepartment", "$aliasDepartment.term_id = $aliasStaffBusiness.term_id AND $aliasDepartment.term_type = '{$this->department_m->term_type}'", 'LEFT');

            if ($filter_department) {
                $this->datatable_builder->where("$aliasDepartment.term_id", $filter_department);
            }

            if ($sort_department) {
                $this->datatable_builder->order_by("$aliasDepartment.term_name", $sort_department);
            }
        }

        // user_group FILTERING & SORTING
        $filter_user_group = $args['where']['user_group'] ?? FALSE;
        $sort_user_group = $args['order_by']['user_group'] ?? FALSE;
        if ($filter_user_group || $sort_user_group) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('tu_user_group_');
            $aliaUserGroup = uniqid('user_group_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = total_report.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("term_users $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT')
                ->join("term $aliaUserGroup", "$aliaUserGroup.term_id = $aliasStaffBusiness.term_id AND $aliaUserGroup.term_type = '{$this->user_group_m->term_type}'", 'LEFT');

            if ($filter_user_group) {
                $this->datatable_builder->where("$aliaUserGroup.term_id", $filter_user_group);
            }

            if ($sort_user_group) {
                $this->datatable_builder->order_by("$aliaUserGroup.term_name", $sort_user_group);
            }
        }

        // customer FILTERING & SORTING
        $filter_customer = $args['where']['customer'] ?? FALSE;
        if ($filter_customer) {
            $this->datatable_builder->like('display_name', $filter_customer);
        }

        $sort_customer = $args['order_by']['customer'] ?? FALSE;
        if ($sort_customer) {
            $this->datatable_builder->order_by('display_name', $sort_customer);
        }

        // contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $aliasMetaContractCode = uniqid('m_contract_code_');

            $this->datatable_builder->join("termmeta $aliasMetaContractCode", "$aliasMetaContractCode.term_id = total_report.term_id AND $aliasMetaContractCode.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("$aliasMetaContractCode.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder->order_by("$aliasMetaContractCode.meta_value", $sort_contract_code);
            }
        }

        // contract_budget_payment_type FILTERING & SORTING
        $filter_contract_budget_payment_type = $args['where']['contract_budget_payment_type'] ?? FALSE;
        $sort_contract_budget_payment_type = $args['order_by']['contract_budget_payment_type'] ?? FALSE;
        if ($filter_contract_budget_payment_type || $sort_contract_budget_payment_type) {
            $aliasMetaContractBudgetPaymentType = uniqid('m_contract_budget_payment_type_');

            $this->datatable_builder->join("termmeta $aliasMetaContractBudgetPaymentType", "$aliasMetaContractBudgetPaymentType.term_id = total_report.term_id AND $aliasMetaContractBudgetPaymentType.meta_key = 'contract_budget_payment_type'", 'LEFT');

            if ($filter_contract_budget_payment_type) {
                $this->datatable_builder->where("$aliasMetaContractBudgetPaymentType.meta_value", $filter_contract_budget_payment_type);
            }

            if ($sort_contract_budget_payment_type) {
                $this->datatable_builder->order_by("$aliasMetaContractBudgetPaymentType.meta_value", $sort_contract_budget_payment_type);
            }
        }

        // service_fee_payment_type FILTERING & SORTING
        $filter_service_fee_payment_type = $args['where']['service_fee_payment_type'] ?? FALSE;
        $sort_service_fee_payment_type = $args['order_by']['service_fee_payment_type'] ?? FALSE;
        if ($filter_service_fee_payment_type || $sort_service_fee_payment_type) {
            $aliasMetaContractBudgetPaymentType = uniqid('m_service_fee_payment_type_');

            $this->datatable_builder->join("termmeta $aliasMetaContractBudgetPaymentType", "$aliasMetaContractBudgetPaymentType.term_id = total_report.term_id AND $aliasMetaContractBudgetPaymentType.meta_key = 'service_fee_payment_type'", 'LEFT');

            if ($filter_service_fee_payment_type) {
                $this->datatable_builder->where("$aliasMetaContractBudgetPaymentType.meta_value", $filter_service_fee_payment_type);
            }

            if ($sort_service_fee_payment_type) {
                $this->datatable_builder->order_by("$aliasMetaContractBudgetPaymentType.meta_value", $sort_service_fee_payment_type);
            }
        }

        

        // service_fee_rate_actual FILTERING & SORTING
        $filter_service_fee_rate_actual = $args['where']['service_fee_rate_actual'] ?? FALSE;
        if ($filter_service_fee_rate_actual) {
            $filter_service_fee_rate_actual = div($filter_service_fee_rate_actual, 100);

            $this->datatable_builder->where('service_fee_rate_actual >=', $filter_service_fee_rate_actual);
        }

        $sort_service_fee_rate_actual = $args['order_by']['service_fee_rate_actual'] ?? FALSE;
        if ($sort_service_fee_rate_actual) {
            $this->datatable_builder->order_by('service_fee_rate_actual', $sort_service_fee_rate_actual);
        }

        // contract_begin FILTERING & SORTING
        $filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
        $sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
        if ($filter_contract_begin || $sort_contract_begin) {
            $dates = explode(' - ', $filter_contract_begin);

            $aliasContractBegin = uniqid('m_contract_begin_');

            $this->datatable_builder->join("termmeta $aliasContractBegin", "$aliasContractBegin.term_id = total_report.term_id AND $aliasContractBegin.meta_key = 'contract_begin'", 'LEFT');

            if ($filter_contract_begin) {
                $this->datatable_builder
                    ->where("$aliasContractBegin.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("$aliasContractBegin.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_begin) {
                $this->datatable_builder->order_by("$aliasContractBegin.meta_value", $sort_contract_begin);
            }
        }

        // contract_end FILTERING & SORTING
        $filter_contract_end = $args['where']['contract_end'] ?? FALSE;
        $sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
        if ($filter_contract_end || $sort_contract_end) {
            $dates = explode(' - ', $filter_contract_end);

            $aliasContractEnd = uniqid('m_contract_end_');

            $this->datatable_builder->join("termmeta $aliasContractEnd", "$aliasContractEnd.term_id = total_report.term_id AND $aliasContractEnd.meta_key = 'contract_end'", 'LEFT');

            if ($filter_contract_end) {
                $this->datatable_builder
                    ->where("$aliasContractEnd.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("$aliasContractEnd.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_end) {
                $this->datatable_builder->order_by("$aliasContractEnd.meta_value", $sort_contract_end);
            }
        }

        // advertise_start_time FILTERING & SORTING
        $filter_advertise_start_time = $args['where']['advertise_start_time'] ?? FALSE;
        $sort_advertise_start_time = $args['order_by']['advertise_start_time'] ?? FALSE;
        if ($filter_advertise_start_time || $sort_advertise_start_time) {
            $dates = explode(' - ', $filter_advertise_start_time);

            $aliasAdvertiseStartTime = uniqid('m_advertise_start_time_');

            $this->datatable_builder->join("termmeta $aliasAdvertiseStartTime", "$aliasAdvertiseStartTime.term_id = total_report.term_id AND $aliasAdvertiseStartTime.meta_key = 'advertise_start_time'", 'LEFT');

            if ($filter_advertise_start_time) {
                $this->datatable_builder
                    ->where("$aliasAdvertiseStartTime.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("$aliasAdvertiseStartTime.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_advertise_start_time) {
                $this->datatable_builder->order_by("$aliasAdvertiseStartTime.meta_value", $sort_advertise_start_time);
            }
        }

        // budget_payment FILTERING & SORTING
        $filter_budget_payment = $args['where']['budget_payment'] ?? FALSE;
        if ($filter_budget_payment) {
            $this->datatable_builder->having('budget_payment >=', $filter_budget_payment);
        }

        $sort_budget_payment = $args['order_by']['budget_payment'] ?? FALSE;
        if ($sort_budget_payment) {
            $this->datatable_builder->order_by('budget_payment', $sort_budget_payment);
        }

        // service_fee_payment FILTERING & SORTING
        $filter_service_fee_payment = $args['where']['service_fee_payment'] ?? FALSE;
        if ($filter_service_fee_payment) {
            $this->datatable_builder->having('service_fee_payment >=', $filter_service_fee_payment);
        }

        $sort_service_fee_payment = $args['order_by']['service_fee_payment'] ?? FALSE;
        if ($sort_service_fee_payment) {
            $this->datatable_builder->order_by('service_fee_payment', $sort_service_fee_payment);
        }

        // fct_amount_payment FILTERING & SORTING
        $filter_fct_amount_payment = $args['where']['fct_amount_payment'] ?? FALSE;
        if ($filter_fct_amount_payment) {
            $this->datatable_builder->having('fct_amount_payment AS SIGNED', $filter_fct_amount_payment);
        }

        $sort_fct_amount_payment = $args['order_by']['fct_amount_payment'] ?? FALSE;
        if ($sort_fct_amount_payment) {
            $this->datatable_builder->order_by('fct_amount_payment', $sort_fct_amount_payment);
        }

        // vat_amount_payment FILTERING & SORTING
        $filter_vat_amount_payment = $args['where']['vat_amount_payment'] ?? FALSE;
        if ($filter_vat_amount_payment) {
            $this->datatable_builder->having('vat_amount_payment >=', $filter_vat_amount_payment);
        }

        $sort_vat_amount_payment = $args['order_by']['vat_amount_payment'] ?? FALSE;
        if ($sort_vat_amount_payment) {
            $this->datatable_builder->order_by('vat_amount_payment', $sort_vat_amount_payment);
        }

        // amount_payment FILTERING & SORTING
        $filter_amount_payment = $args['where']['amount_payment'] ?? FALSE;
        if ($filter_amount_payment) {
            $this->datatable_builder->having('amount_payment >=', $filter_amount_payment);
        }

        $sort_amount_payment = $args['order_by']['amount_payment'] ?? FALSE;
        if ($sort_amount_payment) {
            $this->datatable_builder->order_by('amount_payment', $sort_amount_payment);
        }

        // spend FILTERING & SORTING
        $filter_spend = $args['where']['spend'] ?? FALSE;
        if ($filter_spend) {
            $this->datatable_builder->having('spend >=', $filter_spend);
        }

        $sort_spend = $args['order_by']['spend'] ?? FALSE;
        if ($sort_spend) {
            $this->datatable_builder->order_by('spend', $sort_spend);
        }

        // balance_spend FILTERING & SORTING
        $filter_balance_spend = $args['where']['balance_spend'] ?? FALSE;
        if ($filter_balance_spend) {
            $this->datatable_builder->having('balance_spend >=', $filter_balance_spend);
        }

        $sort_balance_spend = $args['order_by']['balance_spend'] ?? FALSE;
        if ($sort_balance_spend) {
            $this->datatable_builder->order_by('balance_spend', $sort_balance_spend);
        }

        // service_fee_actual FILTERING & SORTING
        $filter_service_fee_actual = $args['where']['service_fee_actual'] ?? FALSE;
        if ($filter_service_fee_actual) {
            $this->datatable_builder->having('service_fee_actual >=', $filter_service_fee_actual);
        }

        $sort_service_fee_actual = $args['order_by']['service_fee_actual'] ?? FALSE;
        if ($sort_service_fee_actual) {
            $this->datatable_builder->order_by('service_fee_actual', $sort_service_fee_actual);
        }

        // budget_payment_order_printed FILTERING & SORTING
        $filter_budget_payment_order_printed = $args['where']['budget_payment_order_printed'] ?? FALSE;
        if ($filter_budget_payment_order_printed) {
            $this->datatable_builder->having('budget_payment_order_printed >=', $filter_budget_payment_order_printed);
        }

        $sort_budget_payment_order_printed = $args['order_by']['budget_payment_order_printed'] ?? FALSE;
        if ($sort_budget_payment_order_printed) {
            $this->datatable_builder->order_by('budget_payment_order_printed', $sort_budget_payment_order_printed);
        }

        // service_fee_payment_order_printed FILTERING & SORTING
        $filter_service_fee_payment_order_printed = $args['where']['service_fee_payment_order_printed'] ?? FALSE;
        if ($filter_service_fee_payment_order_printed) {
            $this->datatable_builder->having('service_fee_payment_order_printed >=', $filter_service_fee_payment_order_printed);
        }

        $sort_service_fee_payment_order_printed = $args['order_by']['service_fee_payment_order_printed'] ?? FALSE;
        if ($sort_service_fee_payment_order_printed) {
            $this->datatable_builder->order_by('service_fee_payment_order_printed', $sort_service_fee_payment_order_printed);
        }

        // amount_payment_order_printed FILTERING & SORTING
        $filter_amount_payment_order_printed = $args['where']['amount_payment_order_printed'] ?? FALSE;
        if ($filter_amount_payment_order_printed) {
            $this->datatable_builder->having('amount_payment_order_printed >=', $filter_amount_payment_order_printed);
        }

        $sort_amount_payment_order_printed = $args['order_by']['amount_payment_order_printed'] ?? FALSE;
        if ($sort_amount_payment_order_printed) {
            $this->datatable_builder->order_by('amount_payment_order_printed', $sort_amount_payment_order_printed);
        }

        // budget_remain FILTERING & SORTING
        $filter_budget_remain = $args['where']['budget_remain'] ?? FALSE;
        if ($filter_budget_remain) {
            $this->datatable_builder->having('budget_remain >=', $filter_budget_remain);
        }

        $sort_budget_remain = $args['order_by']['budget_remain'] ?? FALSE;
        if ($sort_budget_remain) {
            $this->datatable_builder->order_by('budget_remain', $sort_budget_remain);
        }

        // service_fee_remain FILTERING & SORTING
        $filter_service_fee_remain = $args['where']['service_fee_remain'] ?? FALSE;
        if ($filter_service_fee_remain) {
            $this->datatable_builder->having('service_fee_remain >=', $filter_service_fee_remain);
        }

        $sort_service_fee_remain = $args['order_by']['service_fee_remain'] ?? FALSE;
        if ($sort_service_fee_remain) {
            $this->datatable_builder->order_by('service_fee_remain', $sort_service_fee_remain);
        }

        // total_remain FILTERING & SORTING
        $filter_total_remain = $args['where']['total_remain'] ?? FALSE;
        if ($filter_total_remain) {
            $this->datatable_builder->having('total_remain >=', $filter_total_remain);
        }

        $sort_total_remain = $args['order_by']['total_remain'] ?? FALSE;
        if ($sort_total_remain) {
            $this->datatable_builder->order_by('total_remain', $sort_total_remain);
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function export_total_report($query = '')
    {
        restrict('reports.total.access');

        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $contracts = $this->contract_m->query($query)->result();
        if (!$contracts) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $title          = my_date(time(), 'Ymd') . '_export_bao_cao_tong_hop';
        $spreadsheet    = IOFactory::load('./files/excel_tpl/contract/contract-synthesis-report.xlsx');
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));

        $sheet = $spreadsheet->getActiveSheet();
        $rowIndex = 5;

        $contract_budget_payment_types = $this->config->item('enums', 'contract_budget_payment_types');
        $service_fee_payment_types = $this->config->item('service_fee_payment_type');

        foreach ($contracts as $contract) {
            $col = 1;

            // Prepare data
            $term_id = $contract->term_id;
            $term_type = $contract->term_type;

            // Get cid
            $cid = cid($contract->user_id, $contract->user_type);

            // Get contract_budget_payment_type
            $contract_budget_payment_type = get_term_meta_value($term_id, 'contract_budget_payment_type');
            $contract_budget_payment_type = $contract_budget_payment_types[$contract_budget_payment_type] ?? '';

            // Get contract_code
            $contract_code = get_term_meta_value($term_id, 'contract_code') ?: '';

            // Get contract_begin
            $contract_begin = get_term_meta_value($term_id, 'contract_begin');
            $contract_begin = $contract_begin ? my_date($contract_begin, 'd/m/Y') : '';

            // Get contract_end
            $contract_end = get_term_meta_value($term_id, 'contract_end');
            $contract_end = $contract_end ? my_date($contract_end, 'd/m/Y') : '';

            // Get advertise_start_time
            $advertise_start_time = get_term_meta_value($term_id, 'advertise_start_time');
            $advertise_start_time = $advertise_start_time ? my_date($advertise_start_time, 'd/m/Y') : '';

            // Get contract_budget_payment_type
            $contract_budget_payment_type = get_term_meta_value($term_id, 'contract_budget_payment_type');
            $contract_budget_payment_type = $contract_budget_payment_types[$contract_budget_payment_type] ?? '';

            // Get contract_budget_payment_type
            $service_fee_payment_type = get_term_meta_value($term_id, 'service_fee_payment_type');
            $service_fee_payment_type = $service_fee_payment_types[$service_fee_payment_type] ?? '';

            // Get staff_techs
            $staff_techs = '';

            $model_kpi = '';
            switch ($term_type) {
                case 'courseads':
                    $this->load->model('courseads/courseads_kpi_m');
                    $model_kpi = 'courseads_kpi_m';
                    break;
                case 'banner':
                    $this->load->model('banner/banner_kpi_m');
                    $model_kpi = 'banner_kpi_m';
                    break;
                case 'facebook-ads':
                    $this->load->model('facebookads/facebookads_kpi_m');
                    $model_kpi = 'facebookads_kpi_m';
                    break;
                case 'google-ads':
                    $this->load->model('googleads/googleads_kpi_m');
                    $model_kpi = 'googleads_kpi_m';
                    break;
                case 'hosting':
                    $this->load->model('hosting/hosting_kpi_m');
                    $model_kpi = 'hosting_kpi_m';
                    break;
                case 'onead':
                    $this->load->model('onead/onead_kpi_m');
                    $model_kpi = 'onead_kpi_m';
                    break;
                case 'oneweb' || 'webgeneral':
                    $this->load->model('webgeneral/webgeneral_kpi_m');
                    $model_kpi = 'webgeneral_kpi_m';
                    break;
                case 'seo-traffic':
                    $this->load->model('seotraffic/seotraffic_kpi_m');
                    $model_kpi = 'seotraffic_kpi_m';
                    break;
                case 'webdoctor':
                    $this->load->model('webdoctor/webdoctor_kpi_m');
                    $model_kpi = 'webdoctor_kpi_m';
                    break;
                case 'weboptimize':
                    $this->load->model('weboptimize/weboptimize_kpi_m');
                    $model_kpi = 'weboptimize_kpi_m';
                    break;
                default:
                    break;
            }

            $kpis = [];
            if ($term_type == 'courseads') {

                $kpis = $this->$model_kpi
                    ->order_by('kpi_type')
                    ->where('object_id', $term_id)
                    ->as_array()
                    ->get_many_by();
            } else {
                $kpis = $this->$model_kpi->get_many_by(['term_id' => $term_id, 'kpi_type' => 'tech']);
            }

            if (!empty($kpis)) {
                $staff_techs = array_map(function ($x) {
                    return $this->admin_m->get_field_by_id($x->user_id, 'display_name');
                }, $kpis);

                $staff_techs = implode(', ', $staff_techs);
            }

            // Get staff_business
            $staff_business = '';
            $staff_business_id = get_term_meta_value($term_id, 'staff_business');
            $staff_business_id and $staff_business = $this->admin_m->get_field_by_id($staff_business_id, 'display_name');

            // Get departments
            $department = '';
            $staff_business_id = get_term_meta_value($term_id, 'staff_business');
            $departments = $this->term_users_m->get_user_terms($staff_business_id, $this->department_m->term_type);
            $departments and $department = implode(', ', array_column($departments, 'term_name'));

            // Get user_groups
            $user_group = '';
            $staff_business_id = get_term_meta_value($term_id, 'staff_business');
            $user_groups = $this->term_users_m->get_user_terms($staff_business_id, $this->user_group_m->term_type);
            $user_groups and $user_group = implode(', ', array_column($user_groups, 'term_name'));

            // Set Cell
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $cid ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->display_name ?? '--');

            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_code ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_budget_payment_type ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $service_fee_payment_type ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->service_fee_rate_actual ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_begin ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_end ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $advertise_start_time ?: '--');

            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->budget_payment ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->service_fee_payment ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->fct_amount_payment ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->vat_amount_payment ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->amount_payment ?? '--');

            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->spend ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->balance_spend ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->service_fee_actual ?? '--');

            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->budget_remain ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->service_fee_remain ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract->total_remain ?? '--');

            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $staff_business ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $department ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $user_group ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $staff_techs ?: '--');

            $rowIndex++;
        }

        $numbers_of_contract = count($contracts);
        // Set Cells style for Date
        $sheet->getStyle('F' . $rowIndex . ':H' . ($numbers_of_contract + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);

        // Set Cells Style Percentage
        $sheet->getStyle('E' . $rowIndex . ':E' . ($numbers_of_contract + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);

        // Set Cells Style Currency
        $sheet->getStyle('I' . $rowIndex . ':R' . ($numbers_of_contract + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        $folder_upload  = 'files/contract/report/';
        if (!is_dir($folder_upload)) {
            try {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}/{$title}.xlsx";
        try {
            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save($fileName);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetReceipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetReceipts.php */