<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Enums\DatatableAdWarehouseSummarySearchFields;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class DatasetAdWarehouseSummary extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'fact_warehouse_summary_m';
        
        parent::__construct();
        
        $this->config->load('contract/fact_warehouse_summary_fields');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data'=>[]);
        if( ! has_permission('admin.adwarehouse.access'))
        {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', "error", 401);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args( parent::get(), $defaults);
        

        $args['columns'] = $this->config->item('default_columns', 'datasource');
        $columns = $this->config->item('datasource');

        if( ! empty($args['columns']))
        {
            foreach ($args['columns'] as $key)
            {
                $this->datatable_builder->add_column($columns['columns'][$key]['name'], $columns['columns'][$key]);
            }    
        }

        $data = $this->data;
        $this->filtering();

        $data['content'] = $this->datatable_builder
        ->set_filter_position(FILTER_TOP_INNER)
        ->setOutputFormat('JSON')

        ->add_search('fact_warehouse_summary_id')
        ->add_search('ad_warehouse_id')
        ->add_search('ad_warehouse_name')
        ->add_search('ad_warehouse_status')
        ->add_search('ad_warehouse_quantity')
        ->add_search('ad_warehouse_created_at')
        ->add_search('ad_warehouse_updated_at')
        ->add_search('ad_warehouse_bm_id')
        ->add_search('ad_warehouse_original_bm_term_id')
        ->add_search('ad_business_manager_id')
        ->add_search('ad_business_manager_name')
        ->add_search('ad_business_manager_created_by')
        ->add_search('ad_business_manager_created_time')
        ->add_search('ad_business_manager_extended_updated_time')
        ->add_search('ad_business_manager_is_hidden')
        ->add_search('ad_business_manager_verification_status')
        ->add_search('ad_business_manager_payment_account_id')
        ->add_search('ad_business_manager_timezone_id')
        ->add_search('ad_business_manager_created_at')
        ->add_search('ad_business_manager_updated_at')
        ->add_search('ad_business_manager_original_term_id')
        ->add_search('ad_business_manager_business_id')
        ->add_search('fact_warehouse_summary_time_id')
        ->add_search('fact_warehouse_summary_ad_warehouse_id')
        ->add_search('fact_warehouse_summary_ad_bm_id')
        ->add_search('fact_warehouse_summary_import')
        ->add_search('fact_warehouse_summary_export_use')
        ->add_search('fact_warehouse_summary_export_suspend')
        ->add_search('fact_warehouse_summary_ad_accounts_count')
        ->add_search('fact_warehouse_summary_ad_accounts_status_active_count')
        ->add_search('fact_warehouse_summary_ad_accounts_status_disabled_count')
        ->add_search('fact_warehouse_summary_ad_accounts_status_unsettled_count')
        ->add_search('fact_warehouse_summary_ad_accounts_status_pending_risk_review_count')
        ->add_search('fact_warehouse_summary_ad_accounts_status_pending_settlement_count')
        ->add_search('fact_warehouse_summary_ad_accounts_status_in_grace_period_count')
        ->add_search('fact_warehouse_summary_ad_accounts_status_pending_closure_count')
        ->add_search('fact_warehouse_summary_ad_accounts_status_closed_count')
        ->add_search('fact_warehouse_summary_ad_accounts_status_any_active_count')
        ->add_search('fact_warehouse_summary_ad_accounts_status_any_closed_count')
        ->add_search('fact_warehouse_summary_ad_accounts_status_unspecified_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_none_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_ads_integrity_policy_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_ads_ip_review_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_risk_payment_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_gray_account_shut_down_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_ads_afc_review_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_business_integrity_rar_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_permanent_close_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_unused_reseller_account_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_unused_account_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_umbrella_ad_account_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_business_manager_integrity_policy_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_misrepresented_ad_account_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_aoab_deshare_legal_entity_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_ctx_thread_review_count')
        ->add_search('fact_warehouse_summary_ad_accounts_disable_compromised_ad_account_count')
        ->add_search('fact_warehouse_summary_ad_accounts_stock_status_inuse_count')
        ->add_search('fact_warehouse_summary_ad_accounts_stock_status_instock_count')
        ->add_search('fact_warehouse_summary_ad_accounts_stock_status_pending_io_review_count')
        ->add_search('fact_warehouse_summary_ad_accounts_stock_status_closed_count')
        ->add_search('fact_warehouse_summary_ad_accounts_stock_status_unspecified_count')
        ->add_search('fact_warehouse_summary_ad_accounts_type_of_ownership_internal')
        ->add_search('fact_warehouse_summary_ad_accounts_type_of_ownership_external')
        ->add_search('fact_warehouse_summary_ad_accounts_type_of_ownership_unspecified')
        ->add_search('fact_warehouse_summary_ad_accounts_creation_type_purchase_count')
        ->add_search('fact_warehouse_summary_ad_accounts_creation_type_create_count')
        ->add_search('fact_warehouse_summary_ad_accounts_creation_type_default_count')
        ->add_search('fact_warehouse_summary_ad_accounts_creation_type_unknown_count')
        ->add_search('fact_warehouse_summary_ad_accounts_creation_type_unspecified_count')
        ->add_search('fact_warehouse_summary_ad_accounts_creation_type_partner_count')
        ->add_search('fact_warehouse_summary_ad_accounts_amount_spent')
        ->add_search('fact_warehouse_summary_ad_accounts_spend_cap_avg')
        ->add_search('fact_warehouse_summary_ad_accounts_min_daily_budget_avg')
        ->add_search('fact_warehouse_summary_ad_accounts_min_balance_total')


        ->select('fact_warehouse_summary.id as fact_warehouse_summary_id')

        ->select('ad_warehouse.id as ad_warehouse_id')
        ->select('ad_warehouse.name as ad_warehouse_name')
        ->select('ad_warehouse.status as ad_warehouse_status')
        ->select('ad_warehouse.quantity as ad_warehouse_quantity')
        ->select('ad_warehouse.created_at as ad_warehouse_created_at')
        ->select('ad_warehouse.updated_at as ad_warehouse_updated_at')
        ->select('ad_warehouse.bm_id as ad_warehouse_bm_id')
        ->select('ad_warehouse.original_bm_term_id as ad_warehouse_original_bm_term_id')

        ->select('ad_business_manager.id as ad_business_manager_id')
        ->select('ad_business_manager.name as ad_business_manager_name')
        ->select('ad_business_manager.created_by as ad_business_manager_created_by')
        ->select('ad_business_manager.created_time as ad_business_manager_created_time')
        ->select('ad_business_manager.extended_updated_time as ad_business_manager_extended_updated_time')
        ->select('ad_business_manager.is_hidden as ad_business_manager_is_hidden')
        ->select('ad_business_manager.verification_status as ad_business_manager_verification_status')
        ->select('ad_business_manager.payment_account_id as ad_business_manager_payment_account_id')
        ->select('ad_business_manager.timezone_id as ad_business_manager_timezone_id')
        ->select('ad_business_manager.created_at as ad_business_manager_created_at')
        ->select('ad_business_manager.updated_at as ad_business_manager_updated_at')
        ->select('ad_business_manager.original_term_id as ad_business_manager_original_term_id')
        ->select('ad_business_manager.business_id as ad_business_manager_business_id')

        ->select('fact_warehouse_summary.time_id as fact_warehouse_summary_time_id')
        ->select('fact_warehouse_summary.ad_warehouse_id as fact_warehouse_summary_ad_warehouse_id')
        ->select('fact_warehouse_summary.ad_bm_id as fact_warehouse_summary_ad_bm_id')
        ->select('fact_warehouse_summary.import as fact_warehouse_summary_import')
        ->select('fact_warehouse_summary.export_use as fact_warehouse_summary_export_use')
        ->select('fact_warehouse_summary.export_suspend as fact_warehouse_summary_export_suspend')
        ->select('fact_warehouse_summary.ad_accounts_count as fact_warehouse_summary_ad_accounts_count')
        ->select('fact_warehouse_summary.ad_accounts_status_active_count as fact_warehouse_summary_ad_accounts_status_active_count')
        ->select('fact_warehouse_summary.ad_accounts_status_disabled_count as fact_warehouse_summary_ad_accounts_status_disabled_count')
        ->select('fact_warehouse_summary.ad_accounts_status_unsettled_count as fact_warehouse_summary_ad_accounts_status_unsettled_count')
        ->select('fact_warehouse_summary.ad_accounts_status_pending_risk_review_count as fact_warehouse_summary_ad_accounts_status_pending_risk_review_count')
        ->select('fact_warehouse_summary.ad_accounts_status_pending_settlement_count as fact_warehouse_summary_ad_accounts_status_pending_settlement_count')
        ->select('fact_warehouse_summary.ad_accounts_status_in_grace_period_count as fact_warehouse_summary_ad_accounts_status_in_grace_period_count')
        ->select('fact_warehouse_summary.ad_accounts_status_pending_closure_count as fact_warehouse_summary_ad_accounts_status_pending_closure_count')
        ->select('fact_warehouse_summary.ad_accounts_status_closed_count as fact_warehouse_summary_ad_accounts_status_closed_count')
        ->select('fact_warehouse_summary.ad_accounts_status_any_active_count as fact_warehouse_summary_ad_accounts_status_any_active_count')
        ->select('fact_warehouse_summary.ad_accounts_status_any_closed_count as fact_warehouse_summary_ad_accounts_status_any_closed_count')
        ->select('fact_warehouse_summary.ad_accounts_status_unspecified_count as fact_warehouse_summary_ad_accounts_status_unspecified_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_none_count as fact_warehouse_summary_ad_accounts_disable_none_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_ads_integrity_policy_count as fact_warehouse_summary_ad_accounts_disable_ads_integrity_policy_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_ads_ip_review_count as fact_warehouse_summary_ad_accounts_disable_ads_ip_review_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_risk_payment_count as fact_warehouse_summary_ad_accounts_disable_risk_payment_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_gray_account_shut_down_count as fact_warehouse_summary_ad_accounts_disable_gray_account_shut_down_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_ads_afc_review_count as fact_warehouse_summary_ad_accounts_disable_ads_afc_review_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_business_integrity_rar_count as fact_warehouse_summary_ad_accounts_disable_business_integrity_rar_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_permanent_close_count as fact_warehouse_summary_ad_accounts_disable_permanent_close_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_unused_reseller_account_count as fact_warehouse_summary_ad_accounts_disable_unused_reseller_account_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_unused_account_count as fact_warehouse_summary_ad_accounts_disable_unused_account_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_umbrella_ad_account_count as fact_warehouse_summary_ad_accounts_disable_umbrella_ad_account_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_business_manager_integrity_policy_count as fact_warehouse_summary_ad_accounts_disable_business_manager_integrity_policy_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_misrepresented_ad_account_count as fact_warehouse_summary_ad_accounts_disable_misrepresented_ad_account_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_aoab_deshare_legal_entity_count as fact_warehouse_summary_ad_accounts_disable_aoab_deshare_legal_entity_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_ctx_thread_review_count as fact_warehouse_summary_ad_accounts_disable_ctx_thread_review_count')
        ->select('fact_warehouse_summary.ad_accounts_disable_compromised_ad_account_count as fact_warehouse_summary_ad_accounts_disable_compromised_ad_account_count')
        ->select('fact_warehouse_summary.ad_accounts_stock_status_inuse_count as fact_warehouse_summary_ad_accounts_stock_status_inuse_count')
        ->select('fact_warehouse_summary.ad_accounts_stock_status_instock_count as fact_warehouse_summary_ad_accounts_stock_status_instock_count')
        ->select('fact_warehouse_summary.ad_accounts_stock_status_pending_io_review_count as fact_warehouse_summary_ad_accounts_stock_status_pending_io_review_count')
        ->select('fact_warehouse_summary.ad_accounts_stock_status_closed_count as fact_warehouse_summary_ad_accounts_stock_status_closed_count')
        ->select('fact_warehouse_summary.ad_accounts_stock_status_unspecified_count as fact_warehouse_summary_ad_accounts_stock_status_unspecified_count')
        ->select('fact_warehouse_summary.ad_accounts_type_of_ownership_internal as fact_warehouse_summary_ad_accounts_type_of_ownership_internal')
        ->select('fact_warehouse_summary.ad_accounts_type_of_ownership_external as fact_warehouse_summary_ad_accounts_type_of_ownership_external')
        ->select('fact_warehouse_summary.ad_accounts_type_of_ownership_unspecified as fact_warehouse_summary_ad_accounts_type_of_ownership_unspecified')
        ->select('fact_warehouse_summary.ad_accounts_creation_type_purchase_count as fact_warehouse_summary_ad_accounts_creation_type_purchase_count')
        ->select('fact_warehouse_summary.ad_accounts_creation_type_create_count as fact_warehouse_summary_ad_accounts_creation_type_create_count')
        ->select('fact_warehouse_summary.ad_accounts_creation_type_default_count as fact_warehouse_summary_ad_accounts_creation_type_default_count')
        ->select('fact_warehouse_summary.ad_accounts_creation_type_unknown_count as fact_warehouse_summary_ad_accounts_creation_type_unknown_count')
        ->select('fact_warehouse_summary.ad_accounts_creation_type_unspecified_count as fact_warehouse_summary_ad_accounts_creation_type_unspecified_count')
        ->select('fact_warehouse_summary.ad_accounts_creation_type_partner_count as fact_warehouse_summary_ad_accounts_creation_type_partner_count')
        ->select('fact_warehouse_summary.ad_accounts_amount_spent as fact_warehouse_summary_ad_accounts_amount_spent')
        ->select('fact_warehouse_summary.ad_accounts_spend_cap_avg as fact_warehouse_summary_ad_accounts_spend_cap_avg')
        ->select('fact_warehouse_summary.ad_accounts_min_daily_budget_avg as fact_warehouse_summary_ad_accounts_min_daily_budget_avg')
        ->select('fact_warehouse_summary.ad_accounts_min_balance_total as fact_warehouse_summary_ad_accounts_min_balance_total')

        ->from('fact_warehouse_summary')
        ->join('ad_warehouse', 'ad_warehouse.id = fact_warehouse_summary.ad_warehouse_id')
        ->join('ad_business_manager', 'ad_business_manager.id = fact_warehouse_summary.ad_bm_id')
        ->join('dim_time', 'dim_time.id = fact_warehouse_summary.time_id')
        ->order_by('dim_time.date', 'desc')
        ->group_by('fact_warehouse_summary.ad_bm_id');

        $pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);
        $data['config'] = $columns['columns'];

        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->exportXlsx($last_query);
            return TRUE;
        }
        
        return parent::response($data);
    }

    public function config_get()
    {
		parent::response(array('msg' => 'Dữ liệu tải thành công','data' => $this->config->item('datasource')));        
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering($args = array())
    {
        restrict('admin.adwarehouse.access');

        $args = parent::get(null, true);

        //if(empty($args) && parent::get('search')) $args = parent::get();  
        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

        if( ! empty($args['where']))
        {
            foreach($args['where'] as $key => $value)
            {
                $key = DatatableAdWarehouseSummarySearchFields::name($key);
                if(empty($value)) continue;

                if(strpos($value, '%') !== FALSE)
                {
                    $_queryMethod ='like';

                    $this->datatable_builder->{$_queryMethod}( $key, preg_replace("/[^A-Za-z0-9 ]/", '', $value));
                    continue;
                }

                $_queryMethod = 'where';

                $operator = trim(preg_replace('/[^\D]/', '', $value));
                if(in_array($operator, [ '>', '>=', '=', '<', '<=', '=!' ]))
                {
                    $this->datatable_builder->{$_queryMethod}("{$key} {$operator}", (double) preg_replace("/[^0-9.]/", "", $value));
                    continue;
                }

                $this->datatable_builder->{$_queryMethod}( $key, $value);
            }
        }

        if(!empty($args['order_by'])) 
        {
            foreach ($args['order_by'] as $key => $value)
            {
                $this->datatable_builder->order_by(DatatableAdWarehouseSummarySearchFields::name($key), $value);
            }
        }
    }

    /**
     * Xuất file excel
     */
    public function exportXlsx($query = '')
    {
        if(empty($query)) return FALSE;

        $pos = strpos($query, 'LIMIT');
        if(FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $data = $this->fact_warehouse_summary_m->query($query)->result();
        if( ! $data)
        {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(),'refresh');
        }

        $args = parent::get(null, true);
        $columns = $this->config->item('columns', 'datasource');
        $selected_columns = explode(',', $args['columns']);
        $selected_columns = array_reduce($selected_columns, function($result, $item) use ($columns){
            if(empty($columns[$item])) return $result;

            $result[$item] = $columns[$item];

            return $result;
        }, []);

        $spreadsheet = new Spreadsheet();
        $spreadsheet
        ->getProperties()
        ->setCreator('ADSPLUS.VN')
        ->setLastModifiedBy('ADSPLUS.VN')
        ->setTitle(time());

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray(array_column(array_values($selected_columns), 'label'), NULL, 'A1');

        $rowIndex = 2;

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($selected_columns as $key => $column)
            {
                $value = $item->$key;
                switch ($column['type'])
                {
                    case 'number': 
                        $value = (int) $value;
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        $sheet->setCellValueExplicitByColumnAndRow($colIndex, $rowIndex, $value, DataType::TYPE_NUMERIC);
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $value);

                        break;

                    default:
                        $sheet->setCellValueExplicitByColumnAndRow($colIndex, $rowIndex, $value, DataType::TYPE_STRING);
                        break;
                }

                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/ad-warehouse-summary';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $title = time() . '-ad-warehouse-summary';
        $fileName = "{$folder_upload}/{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetAdWarehouseIE.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetAdWarehouseIE.php */