<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetWarningOverBudgetReport extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'admin_m';
        $this->autoload['models'][] = 'facebookads/adaccount_m';
        // $this->autoload['models'][] = 'googleads/mcm_accounts_m';
        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'term_posts_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'facebookads/facebookads_m';
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'tiktokads/tiktokads_m';
        $this->autoload['models'][] = 'staffs/department_m';
        $this->autoload['models'][] = 'staffs/user_group_m';

        parent::__construct();

        $this->load->config('contract/contract');
        $this->load->config('googleads/contract');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('reports.over_budget.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        // Prepare data
        $relate_users = $this->admin_m->get_all_by_permissions('reports.receipt_caution.access');
        if (!$relate_users) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        if (is_array($relate_users)) {
            $contract = $this->contract_m->select('term.term_id')
                ->join("termmeta m_contract", "m_contract.term_id = term.term_id AND m_contract.meta_key = 'staff_business'", 'LEFT')
                ->where_in('m_contract.meta_value', $relate_users)
                ->as_array()
                ->get_all();
            $contract_ids = array_column($contract, 'term_id');

            $this->datatable_builder->where_in('total_report.term_id', $contract_ids);
        }

        $contract_status_config = $this->config->item('contract_status');
        unset($contract_status_config['draft']);
        unset($contract_status_config['unverified']);
        $contract_status = array_keys($contract_status_config);

        $contract_services_config = $this->config->item('services');

        $departmentData = $this->department_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $departmentData = $departmentData ? key_value($departmentData, 'term_id', 'term_name') : [];

        $userGroupData = $this->user_group_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $userGroupData = $userGroupData ? key_value($userGroupData, 'term_id', 'term_name') : [];

        $now = time();

        $day_warning = 5;

        $data = $this->data;
        $this->search_filter_contract();

        $data['content'] = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')

            ->add_search('cid', ['placeholder' => 'CID'])
            ->add_search('customer', ['placeholder' => 'Khách hàng'])
            ->add_search('contract_code', ['placeholder' => 'Số hợp đồng'])
            ->add_search('contract_begin', ['placeholder' => 'Ngày bắt đầu', 'class' => 'form-control set-datepicker'])
            ->add_search('contract_end', ['placeholder' => 'Ngày kết thúc', 'class' => 'form-control set-datepicker'])
            ->add_search('start_service_time', ['placeholder' => 'Ngày kích hoạt', 'class' => 'form-control set-datepicker'])
            ->add_search('term_type', ['content' => form_dropdown(['name' => 'where[term_type]', 'class' => 'form-control select2'], prepare_dropdown($contract_services_config, 'Dịch vụ'), parent::get('where[term_type]'))])
            ->add_search('term_status', ['content' => form_dropdown(['name' => 'where[term_status]', 'class' => 'form-control select2'], prepare_dropdown($contract_status_config, 'Trạng thái'), parent::get('where[term_status]'))])
            ->add_search('advertise_start_time', ['placeholder' => 'Ngày chạy quảng cáo', 'class' => 'form-control set-datepicker'])
            ->add_search('contract_budget', ['placeholder' => 'Ngân sách'])
            ->add_search('actual_result', ['placeholder' => 'Đã chạy'])
            ->add_search('remain', ['placeholder' => 'Còn lại'])
            ->add_search('remain_day', ['placeholder' => 'Ngày còn lại'])
            ->add_search('staff_business', ['placeholder' => 'NVKD'])
            ->add_search('department', ['content' => form_dropdown(['name' => 'where[department]', 'class' => 'form-control select2'], prepare_dropdown($departmentData, 'Phòng ban'), parent::get('where[department]'))])
            ->add_search('user_group', ['content' => form_dropdown(['name' => 'where[user_group]', 'class' => 'form-control select2'], prepare_dropdown($userGroupData, 'Nhóm'), parent::get('where[user_group]'))])

            ->add_column('cid', array('title' => 'CID', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('customer', array('title' => 'Khách hàng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_code', array('title' => 'Mã hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_begin', array('title' => 'Ngày bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_end', array('title' => 'Ngày kết thúc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('start_service_time', array('title' => 'Ngày kích hoạt', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('term_type', array('title' => 'Dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('term_status', array('title' => 'Trạng thái', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('account_name', array('title' => 'Tài khoản', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('advertise_start_time', array('title' => 'Ngày chạy quảng cáo', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_budget', array('title' => 'Ngân sách', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('actual_result', array('title' => 'Đã chạy', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('remain', array('title' => 'Số tiền còn lại', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('remain_day', array('title' => 'Ngày còn lại', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('staff_business', array('title' => 'NVKD', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('department', array('title' => 'Phòng ban', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('user_group', array('title' => 'Nhóm', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('staff_techs', array('title' => 'NVKT', 'set_select' => FALSE, 'set_order' => FALSE))

            ->add_column_callback('term_id', function ($data, $row_name) use ($contract_services_config, $contract_status_config) {
                $term_id = $data['term_id'];

                // Get customer
                $data['user_id'] = '';
                $data['customer'] = '';
                $data['cid'] = '';
                $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company', 'system']);
                if (!empty($customers)) {
                    $customer = end($customers);
                    $data['user_id'] = $customer->user_id;
                    $data['customer'] = $customer->display_name;
                    $data['cid'] = get_user_meta_value($customer->user_id, 'cid') ?: cid($customer->user_id, $customer->user_type);
                }

                // Get contract_code
                $contract_code = get_term_meta_value($term_id, 'contract_code');
                $data['contract_code'] = $contract_code ?: '';

                // Get contract_begin
                $contract_begin = get_term_meta_value($term_id, 'contract_begin');
                $data['contract_begin'] = $contract_begin ? date('d/m/Y', $contract_begin) : '';

                // Get contract_end
                $contract_end = get_term_meta_value($term_id, 'contract_end');
                $data['contract_end'] = $contract_end ? date('d/m/Y', $contract_end) : '';

                // Get start_service_time
                $start_service_time = get_term_meta_value($term_id, 'start_service_time');
                $data['start_service_time'] = $start_service_time ? date('d/m/Y', $start_service_time) : '';

                // Get term_type
                $data['term_type_raw'] = $data['term_type'];
                $data['term_type'] = $contract_services_config[$data['term_type']] ?? '';

                // Get term_status
                $data['term_status_raw'] = $data['term_status'];
                $data['term_status'] = $contract_status_config[$data['term_status']] ?? '';

                // Get account_name
                $data['account_name'] = [];
                $adaccounts = get_term_meta($term_id, 'adaccounts', FALSE, TRUE);
                if (!empty($adaccounts)) {
                    $account_name = [];
                    foreach ($adaccounts as $adaccount) {
                        switch ($data['term_type_raw']) {
                            case 'facebook-ads':
                                $name = $this->adaccount_m->select('term_name')->where('term_id', $adaccount)->get_by();
                                $name && $account_name[] = $name->term_name;
                                break;
                            case 'google-ads':
                                $name = get_term_meta_value($adaccount, 'account_name');
                                $name && $account_name[] = $name;
                                break;
                            default:
                                break;
                        }
                    }

                    $data['account_name'] = $account_name;
                }

                // Get advertise_start_time
                $advertise_start_time = get_term_meta_value($term_id, 'advertise_start_time');
                $data['advertise_start_time'] = $advertise_start_time ? date('d/m/Y', $advertise_start_time) : '';

                // Get contract_budget
                $contract_budget = (int)get_term_meta_value($term_id, 'contract_budget');
                $data['contract_budget'] = $contract_budget ?: 0;

                // Get actual_result
                $actual_result = (int)get_term_meta_value($term_id, 'actual_result');
                $data['actual_result'] = $actual_result ?: 0;

                // Get remain
                $data['remain'] = $contract_budget - $actual_result;

                // Get remain_day
                $remain_day = div($contract_budget, $actual_result);
                $remain_day < 1 ? $data['remain_day'] = 0 : $data['remain_day'] = round($remain_day, 1);

                // Get staff_business
                $data['staff_business'] = '';
                $staff_business = get_term_meta_value($term_id, 'staff_business');
                if (!empty($staff_business)) {
                    $data['staff_business'] = [];
                    $data['staff_business']['user_id'] = $staff_business;
                    $data['staff_business']['display_name'] = $this->admin_m->get_field_by_id($staff_business, 'display_name');
                    $data['staff_business']['user_avatar'] = $this->admin_m->get_field_by_id($staff_business, 'user_avatar');
                }

                // Get departments
                $data['department'] = '';
                $departments = $this->term_users_m->get_user_terms($staff_business, $this->department_m->term_type);
                $departments and $data['department'] = implode(', ', array_column($departments, 'term_name'));

                // Get user_groups
                $data['user_group'] = '';
                $user_groups = $this->term_users_m->get_user_terms($staff_business, $this->user_group_m->term_type);
                $user_groups and $data['user_group'] = implode(', ', array_column($user_groups, 'term_name'));

                // Get staff_techs
                $data['staff_techs'] = '--';

                $model_kpi = '';
                switch ($data['term_type']) {
                    case 'courseads':
                        $this->load->model('courseads/courseads_kpi_m');
                        $model_kpi = 'courseads_kpi_m';
                        break;
                    case 'banner':
                        $this->load->model('banner/banner_kpi_m');
                        $model_kpi = 'banner_kpi_m';
                        break;
                    case 'facebook-ads':
                        $this->load->model('facebookads/facebookads_kpi_m');
                        $model_kpi = 'facebookads_kpi_m';
                        break;
                    case 'google-ads':
                        $this->load->model('googleads/googleads_kpi_m');
                        $model_kpi = 'googleads_kpi_m';
                        break;
                    case 'hosting':
                        $this->load->model('hosting/hosting_kpi_m');
                        $model_kpi = 'hosting_kpi_m';
                        break;
                    case 'onead':
                        $this->load->model('onead/onead_kpi_m');
                        $model_kpi = 'onead_kpi_m';
                        break;
                    case 'oneweb' || 'webgeneral':
                        $this->load->model('webgeneral/webgeneral_kpi_m');
                        $model_kpi = 'webgeneral_kpi_m';
                        break;
                    case 'seo-traffic':
                        $this->load->model('seotraffic/seotraffic_kpi_m');
                        $model_kpi = 'seotraffic_kpi_m';
                        break;
                    case 'webdoctor':
                        $this->load->model('webdoctor/webdoctor_kpi_m');
                        $model_kpi = 'webdoctor_kpi_m';
                        break;
                    case 'weboptimize':
                        $this->load->model('weboptimize/weboptimize_kpi_m');
                        $model_kpi = 'weboptimize_kpi_m';
                        break;
                    default:
                        break;
                }

                $kpis = [];
                if ($data['term_type'] == 'courseads') {

                    $kpis = $this->$model_kpi
                        ->order_by('kpi_type')
                        ->where('object_id', $term_id)
                        ->as_array()
                        ->get_many_by();
                } else {
                    $kpis = $this->$model_kpi->get_many_by(['term_id' => $term_id, 'kpi_type' => 'tech']);
                }

                if (!empty($kpis)) {
                    $staff_techs = array_map(function ($x) {
                        return $this->admin_m->get_field_by_id($x->user_id, 'display_name');
                    }, $kpis);

                    $data['staff_techs'] = implode(', ', $staff_techs);
                }

                return $data;
            }, FALSE)

            ->select('term.term_id, term.term_status, term.term_type')

            ->from('term')
            ->join('termmeta advertise_start_time', "advertise_start_time.term_id = term.term_id AND advertise_start_time.meta_key = 'advertise_start_time'", 'LEFT')
            ->join('termmeta contract_budget', "contract_budget.term_id = term.term_id AND contract_budget.meta_key = 'contract_budget'", 'LEFT')
            ->join('termmeta actual_result', "actual_result.term_id = term.term_id AND actual_result.meta_key = 'actual_result'", 'LEFT')
            ->select("TIMESTAMPDIFF(DAY, FROM_UNIXTIME(advertise_start_time.meta_value), FROM_UNIXTIME({$now})) as diff_day")
            ->select('contract_budget.meta_value AS contract_budget')
            ->select('actual_result.meta_value AS actual_result')
            ->select("CASE WHEN CAST(actual_result.meta_value AS UNSIGNED) = 0 THEN '' ELSE (IFNULL(CAST(contract_budget.meta_value AS UNSIGNED), 0) / IFNULL(CAST(actual_result.meta_value AS UNSIGNED), 0)) END AS remain_day")
            ->having("contract_budget < (actual_result + (actual_result / diff_day) * $day_warning)")

            ->where_in('term.term_type', [$this->facebookads_m->term_type, $this->googleads_m->term_type, $this->tiktokads_m->term_type])
            ->where_in('term.term_status', 'publish')

            ->order_by('[custom-select].remain_day');

        if (is_array($relate_users)) {
            $this->datatable_builder->where_in('term.term_id', $contract_ids);
        }

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        $last_query = $this->datatable_builder->last_query();
        $last_query = preg_replace('/((L|l)(I|i)(M|m)(I|i)(T|t) \d+)/', '', $last_query);
        $subheadings_data = $this->db->query($last_query)->result_array();
        $subheadings = array_reduce($subheadings_data, function ($result, $item) {
            $result['contract_budget'] += (int)$item['contract_budget'];
            $result['actual_result'] += (int)$item['actual_result'];
            $result['remain'] = $result['contract_budget'] - $result['actual_result'];

            return $result;
        }, [
            'contract_budget' => 0,
            'actual_result' => 0,
            'remain' => 0,
        ]);
        $subheadings = array_map(function ($item) {
            $item .= '';
            return $item;
        }, $subheadings);
        $data['subheadings'] = $subheadings;

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->exportXlsx($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function search_filter_contract($args = array())
    {
        restrict('reports.over_budget.access');

        $args = parent::get();
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // cid FILTERING & SORTING
        $filter_cid = $args['where']['cid'] ?? FALSE;
        $sort_cid = $args['order_by']['cid'] ?? FALSE;
        if ($filter_cid || $sort_cid) {
            $alias_term_users = uniqid('tu_term_users_');
            $alias_user = uniqid('user_');
            $alias_usermeta = uniqid('m_user_');

            $this->datatable_builder->join("term_users {$alias_term_users}", "{$alias_term_users}.term_id = term.term_id", 'LEFT')
                ->join("user {$alias_user}", "{$alias_term_users}.user_id = {$alias_user}.user_id AND {$alias_user}.user_type IN ('customer_company','customer_person')", 'LEFT')
                ->join("usermeta {$alias_usermeta}", "{$alias_usermeta}.user_id = {$alias_user}.user_id AND {$alias_usermeta}.meta_key = 'cid'", 'LEFT');

            if ($filter_cid) {
                $this->datatable_builder->like("{$alias_usermeta}.meta_value", $filter_cid);
            }

            if ($sort_cid) {
                $this->datatable_builder->order_by("{$alias_usermeta}.meta_value", $sort_cid);
            }
        }

        // customer FILTERING & SORTING
        $filter_customer = $args['where']['customer'] ?? FALSE;
        $sort_customer = $args['order_by']['customer'] ?? FALSE;
        if ($filter_customer || $sort_customer) {
            $term_user_alias = uniqid('term_user_');
            $customer_alias = uniqid('customer_');

            $this->datatable_builder->join("term_users $term_user_alias", "$term_user_alias.term_id = term.term_id", 'LEFT')
                ->join("user $customer_alias", "$customer_alias.user_id = $term_user_alias.user_id AND $customer_alias.user_type in ('customer_person', 'customer_company', 'system')", 'LEFT');

            if ($filter_customer) {
                $this->datatable_builder->like("$customer_alias.display_name", $filter_customer);
            }

            if ($sort_customer) {
                $this->datatable_builder->order_by("$customer_alias.display_name", $sort_customer);
            }
        }

        // contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $alias = uniqid('contract_code_');

            $this->datatable_builder->join("termmeta $alias", "$alias.term_id = term.term_id and $alias.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("$alias.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder->order_by("$alias.meta_value", $sort_contract_code);
            }
        }

        // contract_begin FILTERING & SORTING
        $filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
        $sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
        if ($filter_contract_begin || $sort_contract_begin) {
            $alias = uniqid('contract_begin_');

            $this->datatable_builder->join("termmeta $alias", "$alias.term_id = term.term_id and $alias.meta_key = 'contract_begin'", 'LEFT');

            if ($filter_contract_begin) {
                $dates = explode(' - ', $filter_contract_begin);

                $this->datatable_builder->where("CAST($alias.meta_value AS UNSIGNED) >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("CAST($alias.meta_value AS UNSIGNED) <=", $this->mdate->startOfDay(end($dates)));
            }

            if ($sort_contract_begin) {
                $this->datatable_builder->order_by("CAST($alias.meta_value AS UNSIGNED)", $sort_contract_begin);
            }
        }

        // contract_end FILTERING & SORTING
        $filter_contract_end = $args['where']['contract_end'] ?? FALSE;
        $sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
        if ($filter_contract_end || $sort_contract_end) {
            $alias = uniqid('contract_end_');

            $this->datatable_builder->join("termmeta $alias", "$alias.term_id = term.term_id and $alias.meta_key = 'contract_end'", 'LEFT');

            if ($filter_contract_end) {
                $dates = explode(' - ', $filter_contract_end);

                $this->datatable_builder->where("CAST($alias.meta_value AS UNSIGNED) >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("CAST($alias.meta_value AS UNSIGNED) <=", $this->mdate->startOfDay(end($dates)));
            }

            if ($sort_contract_end) {
                $this->datatable_builder->order_by("CAST($alias.meta_value AS UNSIGNED)", $sort_contract_end);
            }
        }

        // start_service_time FILTERING & SORTING
        $filter_start_service_time = $args['where']['start_service_time'] ?? FALSE;
        $sort_start_service_time = $args['order_by']['start_service_time'] ?? FALSE;
        if ($filter_start_service_time || $sort_start_service_time) {
            $alias = uniqid('start_service_time_');

            $this->datatable_builder->join("termmeta $alias", "$alias.term_id = term.term_id and $alias.meta_key = 'start_service_time'", 'LEFT');

            if ($filter_start_service_time) {
                $dates = explode(' - ', $filter_start_service_time);

                $this->datatable_builder->where("CAST($alias.meta_value AS UNSIGNED) >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("CAST($alias.meta_value AS UNSIGNED) <=", $this->mdate->startOfDay(end($dates)));
            }

            if ($sort_start_service_time) {
                $this->datatable_builder->order_by("CAST($alias.meta_value AS UNSIGNED)", $sort_start_service_time);
            }
        }

        // term_type FILTERING & SORTING
        $filter_term_type = $args['where']['term_type'] ?? FALSE;
        if ($filter_term_type) {
            $this->datatable_builder->where('term_type', $filter_term_type);
        }

        $sort_term_type = $args['order_by']['term_type'] ?? FALSE;
        if ($sort_term_type) {
            $this->datatable_builder->order_by('term_type', $sort_term_type);
        }

        // term_status FILTERING & SORTING
        $filter_term_status = $args['where']['term_status'] ?? FALSE;
        if ($filter_term_status) {
            $this->datatable_builder->where('term_status', $filter_term_status);
        }

        $sort_term_status = $args['order_by']['term_status'] ?? FALSE;
        if ($sort_term_status) {
            $this->datatable_builder->order_by('term_status', $sort_term_status);
        }

        // advertise_start_time FILTERING & SORTING
        $filter_advertise_start_time = $args['where']['advertise_start_time'] ?? FALSE;
        $sort_advertise_start_time = $args['order_by']['advertise_start_time'] ?? FALSE;
        if ($filter_advertise_start_time || $sort_advertise_start_time) {
            $alias = uniqid('advertise_start_time_');

            $this->datatable_builder->join("termmeta $alias", "$alias.term_id = term.term_id and $alias.meta_key = 'advertise_start_time'", 'LEFT');

            if ($filter_advertise_start_time) {
                $dates = explode(' - ', $filter_advertise_start_time);

                $this->datatable_builder->where("CAST($alias.meta_value AS UNSIGNED) >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("CAST($alias.meta_value AS UNSIGNED) <=", $this->mdate->startOfDay(end($dates)));
            }

            if ($sort_advertise_start_time) {
                $this->datatable_builder->order_by("CAST($alias.meta_value AS UNSIGNED)", $sort_advertise_start_time);
            }
        }

        // contract_budget FILTERING & SORTING
        $filter_contract_budget = $args['where']['contract_budget'] ?? FALSE;
        $sort_contract_budget = $args['order_by']['contract_budget'] ?? FALSE;
        if ($filter_contract_budget || $sort_contract_budget) {
            $alias = uniqid('contract_budget_');

            $this->datatable_builder->join("termmeta $alias", "$alias.term_id = term.term_id and $alias.meta_key = 'contract_budget'", 'LEFT');

            if ($filter_contract_budget) {
                $this->datatable_builder->where("CAST($alias.meta_value AS UNSIGNED) >=", $filter_contract_budget);
            }

            if ($sort_contract_budget) {
                $this->datatable_builder->order_by("CAST($alias.meta_value AS UNSIGNED)", $sort_contract_budget);
            }
        }

        // actual_result FILTERING & SORTING
        $filter_actual_result = $args['where']['actual_result'] ?? FALSE;
        $sort_actual_result = $args['order_by']['actual_result'] ?? FALSE;
        if ($filter_actual_result || $sort_actual_result) {
            $alias = uniqid('actual_result_');

            $this->datatable_builder->join("termmeta $alias", "$alias.term_id = term.term_id and $alias.meta_key = 'actual_result'", 'LEFT');

            if ($filter_actual_result) {
                $this->datatable_builder->where("CAST($alias.meta_value AS UNSIGNED) >=", $filter_actual_result);
            }

            if ($sort_actual_result) {
                $this->datatable_builder->order_by("CAST($alias.meta_value AS UNSIGNED)", $sort_actual_result);
            }
        }

        // remain FILTERING & SORTING
        $filter_remain = $args['where']['remain'] ?? FALSE;
        $sort_remain = $args['order_by']['remain'] ?? FALSE;
        if ($filter_remain || $sort_remain) {
            $alias_contract_budget = uniqid('contract_budget_');
            $alias_actual_result = uniqid('actual_result_');

            $alias_remain = uniqid('remain_');

            $this->datatable_builder->select("(IFNULL(CAST($alias_contract_budget.meta_value AS UNSIGNED), 0) - IFNULL(CAST($alias_actual_result.meta_value AS UNSIGNED), 0)) AS $alias_remain")
                ->join("termmeta $alias_contract_budget", "$alias_contract_budget.term_id = term.term_id and $alias_contract_budget.meta_key = 'contract_budget'", 'LEFT')
                ->join("termmeta $alias_actual_result", "$alias_actual_result.term_id = term.term_id and $alias_actual_result.meta_key = 'actual_result'", 'LEFT');

            if ($filter_remain) {
                $this->datatable_builder->having("$alias_remain >=", $filter_remain);
            }

            if ($sort_remain) {
                $this->datatable_builder->order_by("[custom-select].$alias_remain", $sort_remain);
            }
        }

        // remain_day FILTERING & SORTING
        $filter_remain_day = $args['where']['remain_day'] ?? FALSE;
        $sort_remain_day = $args['order_by']['remain_day'] ?? FALSE;
        if ($filter_remain_day || $sort_remain_day) {
            $alias_contract_budget = uniqid('contract_budget_');
            $alias_actual_result = uniqid('actual_result_');

            $alias_remain_day = uniqid('remain_day_');

            $this->datatable_builder
                ->join("termmeta $alias_contract_budget", "$alias_contract_budget.term_id = term.term_id and $alias_contract_budget.meta_key = 'contract_budget'", 'LEFT')
                ->join("termmeta $alias_actual_result", "$alias_actual_result.term_id = term.term_id and $alias_actual_result.meta_key = 'actual_result'", 'LEFT')
                ->select("CASE WHEN CAST($alias_actual_result.meta_value AS UNSIGNED) = 0 THEN '' ELSE (IFNULL(CAST($alias_contract_budget.meta_value AS UNSIGNED), 0) / IFNULL(CAST($alias_actual_result.meta_value AS UNSIGNED), 0)) END AS $alias_remain_day");

            if ($filter_remain_day) {
                $this->datatable_builder->having("$alias_remain_day >=", $filter_remain_day);
            }

            if ($sort_remain_day) {
                $this->datatable_builder->order_by("[custom-select].$alias_remain_day", $sort_remain_day);
            }
        }

        // staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if ($filter_staff_business || $sort_staff_business) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('staff_business_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = term.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("user $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT');

            if ($filter_staff_business) {
                $this->datatable_builder->like("$aliasStaffBusiness.display_name", $filter_staff_business);
            }

            if ($sort_staff_business) {
                $this->datatable_builder->order_by("$aliasStaffBusiness.display_name", $sort_staff_business);
            }
        }

        // department FILTERING & SORTING
        $filter_department = $args['where']['department'] ?? FALSE;
        $sort_department = $args['order_by']['department'] ?? FALSE;
        if ($filter_department || $sort_department) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('tu_department_');
            $aliasDepartment = uniqid('department_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = term.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("term_users $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT')
                ->join("term $aliasDepartment", "$aliasDepartment.term_id = $aliasStaffBusiness.term_id AND $aliasDepartment.term_type = '{$this->department_m->term_type}'", 'LEFT');

            if ($filter_department) {
                $this->datatable_builder->where("$aliasDepartment.term_id", $filter_department);
            }

            if ($sort_department) {
                $this->datatable_builder->order_by("$aliasDepartment.term_name", $sort_department);
            }
        }

        // user_group FILTERING & SORTING
        $filter_user_group = $args['where']['user_group'] ?? FALSE;
        $sort_user_group = $args['order_by']['user_group'] ?? FALSE;
        if ($filter_user_group || $sort_user_group) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('tu_user_group_');
            $aliaUserGroup = uniqid('user_group_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = term.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("term_users $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT')
                ->join("term $aliaUserGroup", "$aliaUserGroup.term_id = $aliasStaffBusiness.term_id AND $aliaUserGroup.term_type = '{$this->user_group_m->term_type}'", 'LEFT');

            if ($filter_user_group) {
                $this->datatable_builder->where("$aliaUserGroup.term_id", $filter_user_group);
            }

            if ($sort_user_group) {
                $this->datatable_builder->order_by("$aliaUserGroup.term_name", $sort_user_group);
            }
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function exportXlsx($query = '')
    {
        restrict('reports.over_budget.access');

        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $contracts = $this->contract_m->query($query)->result();
        if (!$contracts) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $title          = my_date(time(), 'Ymd') . '_export_bao_cao_canh_bao_hop_dong_chay_lo';
        $spreadsheet    = IOFactory::load('./files/excel_tpl/contract/warning-over-budget-report.xlsx');
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));

        $sheet = $spreadsheet->getActiveSheet();
        $rowIndex = 4;

        foreach ($contracts as $key => &$contract) {
            $col = 1;

            // Prepare data
            $term_id = $contract->term_id;

            // Get customer
            $customer_name = '';
            $cid = '';
            $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company', 'system']);
            if (!empty($customers)) {
                $customer = end($customers);
                $customer_name = $customer->display_name;
                $cid = get_user_meta_value($customer->user_id, 'cid');
                empty($cid) and $cid = cid($customer->user_id, $customer->user_type);
            }

            // Get contract_code
            $contract_code = get_term_meta_value($term_id, 'contract_code');
            $contract_code = $contract_code ?: '--';

            // Get contract_begin
            $contract_begin = get_term_meta_value($term_id, 'contract_begin');
            $contract_begin = $contract_begin ? date('d/m/Y', $contract_begin) : '--';

            // Get contract_end
            $contract_end = get_term_meta_value($term_id, 'contract_end');
            $contract_end = $contract_end ? date('d/m/Y', $contract_end) : '--';

            // Get start_service_time
            $start_service_time = get_term_meta_value($term_id, 'start_service_time');
            $start_service_time = $start_service_time ? date('d/m/Y', $start_service_time) : '--';

            // Get term_type
            $term_type = $contract_services_config[$contract->term_type] ?? '--';

            // Get term_status
            $term_status = $contract_status_config[$contract->term_status] ?? '--';

            // Get account_name
            $account_name = [];
            $adaccounts = get_term_meta($term_id, 'adaccounts', FALSE, TRUE);
            if (!empty($adaccounts)) {
                $list_name = [];
                foreach ($adaccounts as $adaccount) {
                    switch ($contract->term_type) {
                        case 'facebook-ads':
                            $name = $this->adaccount_m->select('term_name')->where('term_id', $adaccount)->get_by();
                            $name && $list_name[] = $name->term_name;
                            break;
                        case 'google-ads':
                            $name = get_term_meta_value($adaccount, 'account_name');
                            $name && $list_name[] = $name;
                            break;
                        default:
                            break;
                    }
                }

                $account_name = $list_name;
            }
            empty($account_name) ? $account_name = '--' : $account_name = implode(', ', $account_name);

            // Get advertise_start_time
            $advertise_start_time = get_term_meta_value($term_id, 'advertise_start_time');
            $advertise_start_time = $advertise_start_time ? date('d/m/Y', $advertise_start_time) : '--';

            // Get contract_budget
            $contract_budget = (int)get_term_meta_value($term_id, 'contract_budget');
            $contract_budget = $contract_budget ?: 0;

            // Get actual_result
            $actual_result = (int)get_term_meta_value($term_id, 'actual_result');
            $actual_result = $actual_result ?: 0;

            // Get remain
            $remain = $contract_budget - $actual_result;

            // Get staff_business
            $sale_id = get_term_meta_value($term_id, 'staff_business');
            $staff_business = $this->admin_m->get_field_by_id($sale_id, 'display_name');

            // Get departments
            $department = '';
            $departments = $this->term_users_m->get_user_terms($sale_id, $this->department_m->term_type);
            $departments and $department = implode(', ', array_column($departments, 'term_name'));

            // Get user_groups
            $user_group = '';
            $user_groups = $this->term_users_m->get_user_terms($sale_id, $this->user_group_m->term_type);
            $user_groups and $user_group = implode(', ', array_column($user_groups, 'term_name'));

            // Get remain_day
            $remain_day = div($contract_budget, $actual_result);
            $remain_day < 1 ? $remain_day = '0 ngày' : $remain_day = round($remain_day, 1) . ' ngày';

            // Set Cell
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $cid ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $customer_name ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_code ?: '--');

            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_begin ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_end ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $start_service_time ?: '--');

            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $term_type ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $term_status ?: '--');

            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $account_name ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $advertise_start_time ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_budget ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $actual_result ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $remain ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $remain_day ?: '--');

            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $staff_business ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $department ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $user_group ?: '--');

            $rowIndex++;
        }

        $numbers_of_contract = count($contracts);

        // Set Cells style for Date
        $sheet->getStyle('D' . $rowIndex . ':J' . ($numbers_of_contract + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);

        // Set Cells Number
        $sheet->getStyle('K' . $rowIndex . ':M' . ($numbers_of_contract + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        $folder_upload  = 'files/contract/report/';
        if (!is_dir($folder_upload)) {
            try {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}/{$title}.xlsx";
        try {
            $writer = IOFactory::createWriter($spreadsheet, 'Xls');
            $writer->save($fileName);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetWarningOverBudgetReport.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetWarningOverBudgetReport.php */