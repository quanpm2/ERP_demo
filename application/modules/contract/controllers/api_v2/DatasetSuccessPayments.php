<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetSuccessPayments extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';

        parent::__construct();

        $this->load->config('contract/receipt');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('contract.receipt.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        $this->load->config('contract');
        $this->load->config('contract/receipt');
        $this->load->config('customer/customer');

        $this->load->model('staffs/department_m');
        $this->load->model('staffs/user_group_m');
        $this->load->model('staffs/term_users_m');
        $this->load->model('staffs/sale_m');

        $sales = $this->sale_m->set_user_type()->set_role()->get_all();
        $sales = $sales ? key_value($sales, 'user_id', 'user_email') : [];

        $departments = $this->department_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $departments = $departments ? key_value($departments, 'term_id', 'term_name') : [];

        $userGroups = $this->user_group_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $userGroups = $userGroups ? key_value($userGroups, 'term_id', 'term_name') : [];

        $data = $this->data;
        $this->search_filter_contract();

        $data['content'] = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')
            ->select('term.term_id, term.term_status, term.term_type, __meta_payment_amount.meta_value AS payment_amount, __meta_contract_code.meta_value AS contract_code')

            ->add_search('contract_code', ['placeholder' => 'Số hợp đồng'])
            ->add_search('sale', ['placeholder' => 'NVKD'])
            ->add_search('user_groups', ['content' => form_dropdown(['name' => 'where[user_groups]', 'class' => 'form-control select2'], prepare_dropdown($userGroups, 'Nhóm'), parent::get('where[user_groups]'))])
            ->add_search('departments', ['content' => form_dropdown(['name' => 'where[departments]', 'class' => 'form-control select2'], prepare_dropdown($departments, 'Phòng ban'), parent::get('where[departments]'))])
            // ->add_search('customer_id', ['placeholder' => 'Mã khách hàng'])
            ->add_search('customer_name', ['placeholder' => 'Tên khách hàng'])
            ->add_search('contract_begin', ['placeholder' => 'Ngày bắt đầu', 'class' => 'form-control set-datepicker'])
            ->add_search('contract_end', ['placeholder' => 'Ngày kết thúc', 'class' => 'form-control set-datepicker'])
            ->add_search('payment_amount', ['placeholder' => 'Số tiền'])
            ->add_search('contract_type', ['content' => form_dropdown(['name' => 'where[term_type]', 'class' => 'form-control select2'], prepare_dropdown($this->config->item('taxonomy'), 'Loại dịch vụ'), parent::get('where[term_type]'))])

            ->add_column('contract_code', array('title' => 'Số hợp đồng', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('contract_type', array('title' => 'Dịch vụ', 'set_order' => FALSE, 'set_select' => FALSE))
            ->add_column('customer_id', array('title' => 'Mã Khách hàng', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('customer_name', array('title' => 'Tên Khách hàng', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('sale', array('title' => 'NVKD', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('user_groups', array('title' => 'Nhóm', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('departments', array('title' => 'Phòng ban', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('contract_begin', array('title' => 'Ngày bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_end', array('title' => 'Ngày kết thúc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('payment_amount', array('title' => 'Số tiền (vnđ)', 'set_order' => TRUE, 'set_select' => FALSE))
            ->add_column('vat', array('title' => 'VAT', 'set_order' => TRUE, 'set_select' => FALSE))
            ->add_column('contract_status', array('title' => 'Trạng thái', 'set_order' => FALSE, 'set_select' => FALSE))

            ->add_column_callback('term_id', function ($data, $row_name) {
                $termId = $data['term_id'];

                $data['term_id'] = (int) $data['term_id'];
                $data['contract_code'] = $data['contract_code'];

                $term_type = $data['term_type'];
                $term_type && $data['contract_type'] = $this->config->item($term_type, 'taxonomy');

                $term_status = $data['term_status'];
                $term_status && $data['contract_status'] = $this->config->item($term_status, 'contract_status');

                // Get sale data
                $sale_id = (int) get_term_meta_value($termId, 'staff_business');
                if (!empty($sale_id)) {
                    $data['sale'] = elements(['user_id', 'display_name', 'user_email'], $this->admin_m->get_field_by_id($sale_id));
                    $data['sale']['departments'] = null;
                    $departments = $this->term_users_m->get_user_terms($sale_id, $this->department_m->term_type);
                    empty($departments) or $data['sale']['departments'] = array_column($departments, 'term_name');

                    $data['sale']['user_groups'] = null;
                    $user_groups = $this->term_users_m->get_user_terms($sale_id, $this->user_group_m->term_type);
                    empty($user_groups) or $data['sale']['user_groups'] = array_column($user_groups, 'term_name');
                }

                // Get customer display name
                if ($customers = $this->term_users_m->get_the_users($termId, ['customer_person', 'customer_company', 'system'])) {
                    $customer       = end($customers);
                    $data['customer'] = elements(['user_id', 'display_name', 'user_email'], (array) $customer);
                    $data['customer']['cid'] = cid($customer->user_id, $customer->user_type);
                }

                // Get contract begin
                $contract_begin = !empty($data['contract_begin']) ? $data['contract_begin'] : get_term_meta_value($termId, 'contract_begin');
                $data['contract_begin_raw'] = $contract_begin;

                if (is_numeric($contract_begin) || empty($contract_begin)) {
                    $data['contract_begin'] = $contract_begin ? my_date($contract_begin, 'd/m/Y') : '--';
                }

                // Get contract end
                $contract_end = !empty($data['contract_end']) ? $data['contract_end'] : get_term_meta_value($termId, 'contract_end');
                $data['contract_end_raw'] = $contract_end;

                if (is_numeric($contract_end) || empty($contract_end)) {
                    $data['contract_end'] = $contract_end ? my_date($contract_end, 'd/m/Y') : '--';
                }

                // Get contract value
                $payment_amount = $data['payment_amount'];
                $data['payment_amount_raw'] = $payment_amount;
                $data['payment_amount'] = currency_numberformat($payment_amount, 'đ');

                // Get VAT
                $vat         = get_term_meta_value($termId, 'vat');
                $data['vat'] = $vat;

                return $data;
            }, FALSE)

            ->from('term')
            ->join('termmeta __meta_payment_amount', '__meta_payment_amount.term_id = term.term_id', 'LEFT')
            ->join('termmeta __meta_contract_code', '__meta_contract_code.term_id = term.term_id', 'LEFT')
            ->where('term.term_status', 'waitingforapprove')
            ->where('__meta_payment_amount.meta_key', 'payment_amount')
            ->where('__meta_payment_amount.meta_value >', '0')
            ->where('__meta_contract_code.meta_key', 'contract_code')
            ->where('__meta_contract_code.meta_value != ', '')
            ->order_by('term.term_id', 'desc');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        // OUTPUT : DOWNLOAD XLSX
        // if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
        // $this->export_receipt($last_query);
        // return TRUE;
        // }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function search_filter_contract($args = array())
    {
        restrict('contract.revenue.access');

        $args = parent::get();
        //if(empty($args) && parent::get('search')) $args = parent::get();  
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // contract_code FILTERING & SORTING
        if (!empty($args['where']['contract_code'])) {
            $termmetas = $this->termmeta_m->select('term_id')
                ->like('meta_value', $args['where']['contract_code'])
                ->get_many_by(['meta_key' => 'contract_code']);

            $this->datatable_builder
                ->where_in('term.term_id', ($termmetas ? array_column($termmetas, 'term_id') : [0]));
        }

        // contract_begin FILTERING & SORTING
        $filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
        $sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
        if ($filter_contract_begin || $sort_contract_begin) {
            $alias = uniqid('contract_begin_');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

            if ($filter_contract_begin) {
                $dates = explode(' - ', $filter_contract_begin);
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_begin) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_contract_begin);
            }
        }

        // contract_end FILTERING & SORTING
        $filter_contract_end = $args['where']['contract_end'] ?? FALSE;
        $sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
        if ($filter_contract_end || $sort_contract_end) {
            $alias = uniqid('contract_end_');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_end'", 'LEFT');

            if ($filter_contract_end) {
                $dates = explode(' - ', $filter_contract_end);
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_end) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_contract_end);
            }
        }

        // payment_amount FILTERING & SORTING
        $filter_payment_amount = $args['where']['payment_amount'] ?? FALSE;
        $sort_payment_amount = $args['order_by']['payment_amount'] ?? FALSE;
        if ($filter_payment_amount || $sort_payment_amount) {
            $filter_payment_amount = (int)$filter_payment_amount;
            $alias = uniqid('payment_amount_');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_amount'", 'LEFT');

            if ($filter_payment_amount) {
                $this->datatable_builder->where("{$alias}.meta_value >=", $filter_payment_amount);
            }

            if ($sort_payment_amount) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_payment_amount);
            }
        }

        // contract_type FILTERING & SORTING
        $filter_contract_type = $args['where']['contract_type'] ?? FALSE;
        if ($filter_contract_type) {
            $this->datatable_builder->where("term.term_type", $filter_contract_type);
        }

        // sale FILTERING & SORTING
        $filter_sale = $args['where']['sale'] ?? FALSE;
        if ($filter_sale) {
            $sale_alias = uniqid('sale_');
            $staff_alias = uniqid('staff_');

            $this->datatable_builder
                ->join("termmeta {$sale_alias}", "{$sale_alias}.term_id = term.term_id and {$sale_alias}.meta_key = 'staff_business'", 'LEFT')
                ->join("user {$staff_alias}", "{$staff_alias}.user_id = {$sale_alias}.meta_value and {$staff_alias}.user_type = 'admin'", 'LEFT')
                ->like("{$staff_alias}.display_name", $filter_sale);
        }

        // user_groups FILTERING & SORTING
        $filter_user_groups = $args['where']['user_groups'] ?? FALSE;
        if ($filter_user_groups) {
            $sale_alias = uniqid('sale_');
            $staff_alias = uniqid('staff_');
            $term_users_alias = uniqid('term_users_');
            $user_groups_alias = uniqid('user_groups_');

            $this->datatable_builder
                ->join("termmeta {$sale_alias}", "{$sale_alias}.term_id = term.term_id and {$sale_alias}.meta_key = 'staff_business'", 'LEFT')
                ->join("user {$staff_alias}", "{$staff_alias}.user_id = {$sale_alias}.meta_value and {$staff_alias}.user_type = 'admin'", 'LEFT')
                ->join("term_users {$term_users_alias}", "{$term_users_alias}.user_id = {$staff_alias}.user_id", 'LEFT')
                ->join("term {$user_groups_alias}", "{$user_groups_alias}.term_id = {$term_users_alias}.term_id and {$user_groups_alias}.term_type = 'user_group'", 'LEFT')
                ->like("{$user_groups_alias}.term_name", $filter_user_groups);
        }

        // departments FILTERING & SORTING
        $filter_departments = $args['where']['departments'] ?? FALSE;
        if ($filter_departments) {
            $sale_alias = uniqid('sale_');
            $staff_alias = uniqid('staff_');
            $term_users_alias = uniqid('term_users_');
            $departments_alias = uniqid('departments_');

            $this->datatable_builder
                ->join("termmeta {$sale_alias}", "{$sale_alias}.term_id = term.term_id and {$sale_alias}.meta_key = 'staff_business'", 'LEFT')
                ->join("user {$staff_alias}", "{$staff_alias}.user_id = {$sale_alias}.meta_value and {$staff_alias}.user_type = 'admin'", 'LEFT')
                ->join("term_users {$term_users_alias}", "{$term_users_alias}.user_id = {$staff_alias}.user_id", 'LEFT')
                ->join("term {$departments_alias}", "{$departments_alias}.term_id = {$term_users_alias}.term_id and {$departments_alias}.term_type = 'department'", 'LEFT')
                ->like("{$departments_alias}.term_name", $filter_departments);
        }

        // customer_name FILTERING & SORTING
        $filter_customer = $args['where']['customer_name'] ?? FALSE;
        if ($filter_customer) {
            $alias = uniqid('customer_name_');
            $this->datatable_builder->join("term_users {$alias}", "{$alias}.term_id = term.term_id", 'LEFT')
                ->join("user", "{$alias}.user_id = user.user_id and user.user_type IN ('customer_company','customer_person')", 'LEFT')
                ->like("user.display_name", $filter_customer);
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    // public function export_receipt($query = '')
    // {
    //     if (empty($query)) return FALSE;

    //     // remove limit in query string
    //     $pos = strpos($query, 'LIMIT');
    //     if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

    //     $posts = $this->receipt_m->query($query)->result();
    //     if (!$posts) {
    //         $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
    //         redirect(current_url(), 'refresh');
    //     }

    //     $this->load->library('excel');
    //     $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
    //     $cacheSettings = array('memoryCacheSize' => '512MB');
    //     PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

    //     $objPHPExcel = new PHPExcel();
    //     $objPHPExcel
    //         ->getProperties()
    //         ->setCreator("WEBDOCTOR.VN")
    //         ->setLastModifiedBy("WEBDOCTOR.VN")
    //         ->setTitle(uniqid('Danh sách thanh toán __'));

    //     $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/receipt/receipt-list-export.xlsx');
    //     $objPHPExcel->setActiveSheetIndex(0);
    //     $objWorksheet = $objPHPExcel->getActiveSheet();

    //     $receipt_type_default       = $this->config->item('type', 'receipt');
    //     $transfer_type_default      = $this->config->item('transfer_type', 'receipt');
    //     $transfer_account_default   = $this->config->item('transfer_account', 'receipt');

    //     $row_index = 3;

    //     $posts = array_map(function ($x) {
    //         $x->term_id = null;
    //         $term_ids   = $this->term_posts_m->select('term_id')->get_the_terms($x->post_id)
    //             and $x->term_id = reset($term_ids);
    //         return $x;
    //     }, $posts);

    //     $contracts = $this->contract_m->set_term_type()->where_in('term_id', array_unique(array_column($posts, 'term_id')))->get_many_by();
    //     !empty($contracts) and $contracts = array_column($contracts, NULL, 'term_id');

    //     $typeContracts      = array_group_by($contracts, 'term_type');
    //     $webdoctorContracts = [];

    //     !empty($typeContracts['webdesign']) and $webdoctorContracts = array_merge($typeContracts['webdesign']);
    //     !empty($typeContracts['oneweb']) and $webdoctorContracts = array_merge($typeContracts['oneweb']);

    //     $this->load->model('webgeneral/webgeneral_kpi_m');
    //     $kpis = array();

    //     if (
    //         !empty($webdoctorContracts)
    //         && $targets = $this->webgeneral_kpi_m->select('kpi_id, term_id, user_id, kpi_type')->order_by('kpi_datetime')->where_in('term_id', array_column($webdoctorContracts, 'term_id'))->as_array()->get_all()
    //     ) {
    //         $targets = array_group_by($targets, 'term_id');

    //         foreach ($targets as $_term_id => $kpi) {
    //             $typeKpi = array_group_by($kpi, 'kpi_type');
    //             foreach ($typeKpi as $type => $_kpis) {
    //                 $contracts[$_term_id]->{$type} = implode(', ', array_map(function ($_kpi) {
    //                     return $this->admin_m->get_field_by_id($_kpi['user_id'], 'display_name') ?: $this->admin_m->get_field_by_id($_kpi['user_id'], 'user_email');
    //                 }, $_kpis));
    //             }
    //         }
    //     }

    //     foreach ($posts as $key => &$post) {
    //         $term = $contracts[$post->term_id];


    //         // Get customer display name
    //         $customer           = '';
    //         $cid                = null;
    //         $customer_tax       = null;
    //         $customer_address   = null;
    //         if ($term_users      = $this->term_users_m->get_term_users($post->term_id, ['customer_person', 'customer_company', 'system'])) {
    //             $customer           = end($term_users);
    //             $cid                = cid($customer->user_id, $customer->user_type);
    //             $customer_tax       = get_user_meta_value($customer->user_id, 'customer_tax');
    //             $customer_address   = get_user_meta_value($customer->user_id, 'customer_address');

    //             $customer           = mb_ucwords($this->admin_m->get_field_by_id($customer->user_id, 'display_name'));
    //         }

    //         // Get contract code
    //         $contract_code = get_term_meta_value($post->term_id, 'contract_code');

    //         /* Get New|Renewal Contract */
    //         $is_first_contract = (bool) get_term_meta_value($term->term_id, 'is_first_contract');

    //         // Nhân viên kinh doanh phụ trách
    //         $sale_id    = (int) get_term_meta_value($post->term_id, 'staff_business');
    //         $sale       = $this->admin_m->get_field_by_id($sale_id, 'display_name') ?: ($this->admin_m->get_field_by_id($sale_id, 'user_email') ?: '--');

    //         // Số tiền đã thu
    //         $post->amount = (float) get_post_meta_value($post->post_id, 'amount');

    //         switch ($post->post_type) {

    //             case 'receipt_payment_on_behaft':
    //                 $vat = 0; /* VAT của hợp đồng */
    //                 $post->amount_not_vat = $post->amount; /* Doanh thu (-VAT) */
    //                 break;

    //             default:
    //                 $vat = (int) get_term_meta_value($post->term_id, 'vat'); /* VAT của hợp đồng */
    //                 $post->amount_not_vat = div($post->amount, (1 + div($vat, 100))); /* Doanh thu (-VAT) */
    //                 break;
    //         }


    //         // Loại hình thanh toán
    //         $transfer_type = get_post_meta_value($post->post_id, 'transfer_type');
    //         $transfer_account = get_post_meta_value($post->post_id, 'transfer_account');
    //         $transfer_by = $transfer_type_default[$transfer_type] . ' ' . ($transfer_account_default[$transfer_account] ?? $transfer_account);

    //         $row_number = $row_index + $key;

    //         $date = $this->mdate->endOfDay($post->end_date);

    //         $i = 0;

    //         // Set Cell
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, PHPExcel_Shared_Date::PHPToExcel($date));
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $customer);
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $cid);
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $customer_tax);
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $customer_address);
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_code);

    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $post->amount);
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $sale);
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, div($vat, 100));
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $post->amount_not_vat);

    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (float) get_post_meta_value($post->post_id, 'vat_amount'));
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (float) get_post_meta_value($post->post_id, 'service_fee'));
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (float) get_post_meta_value($post->post_id, 'fct_amount'));
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (float) get_post_meta_value($post->post_id, 'actual_budget'));

    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $this->config->item($term->term_type, 'taxonomy'));
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $transfer_by);
    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $receipt_type_default[$post->post_type]);

    //         if (in_array($term->term_type, ['webdesign', 'oneweb'])) {
    //             $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $term->tech ?? '');
    //             $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $term->design ?? '');
    //         } else $i += 2;

    //         $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $is_first_contract ? 'Ký mới' : 'Tái Ký');
    //     }

    //     $numbers_of_post = count($posts);

    //     // Set Cells style for Date
    //     $objPHPExcel->getActiveSheet()
    //         ->getStyle('A' . $row_index . ':A' . ($numbers_of_post + $row_index))
    //         ->getNumberFormat()
    //         ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY); // Set Cells style for Date
    //     $objPHPExcel->getActiveSheet()
    //         ->getStyle('A' . $row_index . ':A' . ($numbers_of_post + $row_index))
    //         ->getNumberFormat()
    //         ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

    //     // Set Cells Style For Amount
    //     $objPHPExcel->getActiveSheet()
    //         ->getStyle('G' . $row_index . ':G' . ($numbers_of_post + $row_index))
    //         ->getNumberFormat()
    //         ->setFormatCode("#,##0");

    //     // Set Cells Style For VAT
    //     $objPHPExcel->getActiveSheet()
    //         ->getStyle('I' . $row_index . ':I' . ($numbers_of_post + $row_index))
    //         ->getNumberFormat()
    //         ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);

    //     // Set Cells Style For Amount without VAT
    //     $objPHPExcel->getActiveSheet()
    //         ->getStyle('J' . $row_index . ':J' . ($numbers_of_post + $row_index))
    //         ->getNumberFormat()
    //         ->setFormatCode("#,##0");

    //     $objPHPExcel->getActiveSheet()
    //         ->getStyle('K' . $row_index . ':K' . ($numbers_of_post + $row_index))
    //         ->getNumberFormat()
    //         ->setFormatCode("#,##0");

    //     $objPHPExcel->getActiveSheet()
    //         ->getStyle('L' . $row_index . ':L' . ($numbers_of_post + $row_index))
    //         ->getNumberFormat()
    //         ->setFormatCode("#,##0");

    //     $objPHPExcel->getActiveSheet()
    //         ->getStyle('M' . $row_index . ':M' . ($numbers_of_post + $row_index))
    //         ->getNumberFormat()
    //         ->setFormatCode("#,##0");

    //     $objPHPExcel->getActiveSheet()
    //         ->getStyle('N' . $row_index . ':N' . ($numbers_of_post + $row_index))
    //         ->getNumberFormat()
    //         ->setFormatCode("#,##0");

    //     // Calc sum amount rows and sum amout not vat rows
    //     $total_amount = array_sum(array_column($posts, 'amount'));
    //     $total_amount_not_vat = array_sum(array_column($posts, 'amount_not_vat'));

    //     // Set cell label summary
    //     $objWorksheet->setCellValueByColumnAndRow(1, ($row_index + $numbers_of_post + 2), 'Tổng cộng');

    //     // Set cell sumary amount and amount not vat on TOP
    //     $objWorksheet->setCellValueByColumnAndRow(6, 1, $total_amount);
    //     $objWorksheet->setCellValueByColumnAndRow(9, 1, $total_amount_not_vat);

    //     // Set cell sumary amount and amount not vat on BOTTOM
    //     $objWorksheet->setCellValueByColumnAndRow(6, ($row_index + $numbers_of_post + 2), $total_amount);
    //     $objWorksheet->setCellValueByColumnAndRow(9, ($row_index + $numbers_of_post + 2), $total_amount_not_vat);

    //     // Set Cells Style For Amount without VAT
    //     $objPHPExcel->getActiveSheet()
    //         ->getStyle('G' . ($numbers_of_post + $row_index + 2))
    //         ->getNumberFormat()
    //         ->setFormatCode("#,##0");

    //     // Set Cells Style For Amount without VAT
    //     $objPHPExcel->getActiveSheet()
    //         ->getStyle('J' . ($numbers_of_post + $row_index + 2))
    //         ->getNumberFormat()
    //         ->setFormatCode("#,##0");

    //     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

    //     $file_name = "tmp/export-du-thu.xlsx";
    //     $objWriter->save($file_name);

    //     try {
    //         $objWriter->save($file_name);
    //     } catch (Exception $e) {
    //         trigger_error($e->getMessage());
    //         return FALSE;
    //     }

    //     if (file_exists($file_name)) {
    //         $this->load->helper('download');
    //         force_download($file_name, NULL);
    //     }

    //     return FALSE;
    // }
}
/* End of file DatasetReceipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetReceipts.php */