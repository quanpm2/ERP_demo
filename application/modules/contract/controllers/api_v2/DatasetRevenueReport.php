<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetRevenueReport extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_m';
        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'base_contract_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'staffs/department_m';
        $this->autoload['models'][] = 'staffs/user_group_m';

        parent::__construct();

        $this->load->config('contract');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('reports.revenue.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        $data = $this->data;
        $this->filtering();

        // Prepare data
        $relate_users = $this->admin_m->get_all_by_permissions('reports.revenue.access');
        if (!$relate_users) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $contract_ids = [];
        if (is_array($relate_users)) {
            $contract = $this->contract_m->select('term.term_id')
                ->join("termmeta m_contract", "m_contract.term_id = term.term_id AND m_contract.meta_key = 'staff_business'", 'LEFT')
                ->where_in('m_contract.meta_value', $relate_users)
                ->as_array()
                ->get_all();
            $contract_ids = array_column($contract, 'term_id');
        }

        $contract_status_config = $this->config->item('contract_status');
        unset($contract_status_config['draft']);
        unset($contract_status_config['unverified']);

        $contract_services_config = $this->config->item('services');

        $departmentData = $this->department_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $departmentData = $departmentData ? key_value($departmentData, 'term_id', 'term_name') : [];

        $userGroupData = $this->user_group_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $userGroupData = $userGroupData ? key_value($userGroupData, 'term_id', 'term_name') : [];

        // Data builder
        $data['content'] = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')
            ->select('term.term_id, term.term_status, term.term_type')

            ->add_search('cid', ['placeholder' => 'CID'])
            ->add_search('customer', ['placeholder' => 'Tên khách hàng'])
            ->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])
            ->add_search('contract_service', ['content' => form_dropdown(['name' => 'where[contract_service]', 'class' => 'form-control select2'], prepare_dropdown($contract_services_config), $this->input->get('where[contract_service]'))])
            ->add_search('term_status', ['content' => form_dropdown(['name' => 'where[term_status]', 'class' => 'form-control select2'], prepare_dropdown($contract_status_config), $this->input->get('where[term_status]'))])
            ->add_search('contract_begin', ['placeholder' => 'Ngày bắt đầu', 'class' => 'form-control set-datepicker'])
            ->add_search('contract_end', ['placeholder' => 'Ngày kết thúc', 'class' => 'form-control set-datepicker'])
            ->add_search('start_service_time', ['placeholder' => 'Thời gian kích hoạt', 'class' => 'form-control set-datepicker'])
            ->add_search('contract_value', ['placeholder' => 'GTHĐ'])
            ->add_search('service_fee', ['placeholder' => 'Phí dịch vụ'])
            ->add_search('payment_amount', ['placeholder' => 'Đã thu'])
            ->add_search('payment_amount_remaining', ['placeholder' => 'Còn lại'])
            ->add_search('staff_business', ['placeholder' => 'NVKD'])
            ->add_search('department', ['content' => form_dropdown(['name' => 'where[department]', 'class' => 'form-control select2'], prepare_dropdown($departmentData, 'Phòng ban'), parent::get('where[department]'))])
            ->add_search('user_group', ['content' => form_dropdown(['name' => 'where[user_group]', 'class' => 'form-control select2'], prepare_dropdown($userGroupData, 'Nhóm'), parent::get('where[user_group]'))])

            ->add_column('cid', ['title' => 'CID', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('customer', ['title' => 'Khách hàng', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('contract_code', ['title' => 'Mã HĐ', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('contract_service', ['title' => 'Dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('term_status', ['title' => 'Trạng thái', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('contract_begin', ['title' => 'Ngày bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('contract_end', ['title' => 'Ngày kết thúc', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('start_service_time', ['title' => 'Thời gian kích hoạt', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('contract_value', ['title' => 'GTHĐ', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('service_fee', ['title' => 'Phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('payment_amount', ['title' => 'Đã thu', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('payment_amount_remaining', ['title' => 'Còn lại', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('staff_business', ['title' => 'NVKD', 'set_select' => FALSE, 'set_order' => TRUE])
            ->add_column('department', array('title' => 'Phòng ban', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('user_group', array('title' => 'Nhóm', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('staff_techs', ['title' => 'NVKT', 'set_select' => FALSE, 'set_order' => FALSE])

            ->add_column_callback('term_id', function ($data, $row_name) use ($contract_services_config, $contract_status_config) {
                $term_id = $data['term_id'];

                // Get contract_code
                $data['contract_code'] = get_term_meta_value($term_id, 'contract_code') ?: '--';

                // Get customer
                $data['customer'] = '';
                $data['cid'] = '';
                $data['user_id'] = '';
                $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company', 'system']);
                if (!empty($customers)) {
                    $customer = end($customers);
                    $data['user_id'] = $customer->user_id;
                    $data['customer'] = $customer->display_name;
                    $data['cid'] = get_user_meta_value($customer->user_id, 'cid') ?: cid($customer->user_id, $customer->user_type);
                }

                // Get contract_service
                $data['contract_service'] = $contract_services_config[$data['term_type']] ?? '--';

                // Get term_status
                $data['term_status'] = $contract_status_config[$data['term_status']] ?? '--';

                // Get contract_begin
                $contract_begin = get_term_meta_value($term_id, 'contract_begin');
                $data['contract_begin'] = $contract_begin ? my_date((int)$contract_begin, 'd/m/Y') : '--';

                // Get contract_end
                $contract_end = get_term_meta_value($term_id, 'contract_end');
                $data['contract_end'] = $contract_end ? my_date((int)$contract_end, 'd/m/Y') : '--';

                // Get start_service_time
                $start_service_time = get_term_meta_value($term_id, 'start_service_time');
                $data['start_service_time'] = $start_service_time ? my_date((int)$start_service_time, 'd/m/Y') : '--';

                // Get contract_value
                $contract_value = (int)get_term_meta_value($term_id, 'contract_value') ?? 0;
                $data['contract_value'] = $contract_value;

                // Get service_fee
                $service_fee = (int)get_term_meta_value($term_id, 'service_fee') ?? 0;
                $data['service_fee'] = $service_fee;
                // dd($term_id, $service_fee, get_term_meta_value($term_id, 'service_fee'));

                // Get payment_amount
                $payment_amount = (int)get_term_meta_value($term_id, 'payment_amount') ?? 0;
                $data['payment_amount'] = $payment_amount;

                // Get payment_amount_remaining
                $invs_amount = (int) get_term_meta_value($term_id, 'invs_amount');
                $payment_amount = (int) get_term_meta_value($term_id, 'payment_amount');
                $payment_amount_remaining = $invs_amount - $payment_amount;

                $payment_amount_remaining = max(0, $payment_amount_remaining);
                $data['payment_amount_remaining'] = $payment_amount_remaining;

                // Get staff_techs
                $data['staff_techs'] = '--';

                $model_kpi = '';
                switch ($data['term_type']) {
                    case 'courseads':
                        $this->load->model('courseads/courseads_kpi_m');
                        $model_kpi = 'courseads_kpi_m';
                        break;
                    case 'banner':
                        $this->load->model('banner/banner_kpi_m');
                        $model_kpi = 'banner_kpi_m';
                        break;
                    case 'facebook-ads':
                        $this->load->model('facebookads/facebookads_kpi_m');
                        $model_kpi = 'facebookads_kpi_m';
                        break;
                    case 'google-ads':
                        $this->load->model('googleads/googleads_kpi_m');
                        $model_kpi = 'googleads_kpi_m';
                        break;
                    case 'hosting':
                        $this->load->model('hosting/hosting_kpi_m');
                        $model_kpi = 'hosting_kpi_m';
                        break;
                    case 'onead':
                        $this->load->model('onead/onead_kpi_m');
                        $model_kpi = 'onead_kpi_m';
                        break;
                    case 'oneweb' || 'webgeneral':
                        $this->load->model('webgeneral/webgeneral_kpi_m');
                        $model_kpi = 'webgeneral_kpi_m';
                        break;
                    case 'seo-traffic':
                        $this->load->model('seotraffic/seotraffic_kpi_m');
                        $model_kpi = 'seotraffic_kpi_m';
                        break;
                    case 'webdoctor':
                        $this->load->model('webdoctor/webdoctor_kpi_m');
                        $model_kpi = 'webdoctor_kpi_m';
                        break;
                    case 'weboptimize':
                        $this->load->model('weboptimize/weboptimize_kpi_m');
                        $model_kpi = 'weboptimize_kpi_m';
                        break;
                    default:
                        break;
                }

                $kpis = [];
                if ($data['term_type'] == 'courseads') {

                    $kpis = $this->$model_kpi
                        ->order_by('kpi_type')
                        ->where('object_id', $term_id)
                        ->get_many_by();
                } else {
                    $kpis = $this->$model_kpi->get_many_by(['term_id' => $term_id, 'kpi_type' => 'tech']);
                }

                if (!empty($kpis)) {
                    $staff_techs = array_map(function ($x) {
                        return $this->admin_m->get_field_by_id($x->user_id, 'display_name');
                    }, $kpis);

                    $data['staff_techs'] = implode(', ', $staff_techs);
                }

                // Get staff_business
                $data['staff_business'] = '';
                $staff_business = get_term_meta_value($term_id, 'staff_business');
                if (!empty($staff_business)) {
                    $data['staff_business'] = [];
                    $data['staff_business']['user_id'] = $staff_business;
                    $data['staff_business']['display_name'] = $this->admin_m->get_field_by_id($staff_business, 'display_name');
                    $data['staff_business']['user_avatar'] = $this->admin_m->get_field_by_id($staff_business, 'user_avatar');
                }

                // Get departments
                $data['department'] = '';
                $departments = $this->term_users_m->get_user_terms($staff_business, $this->department_m->term_type);
                $departments and $data['department'] = implode(', ', array_column($departments, 'term_name'));

                // Get user_groups
                $data['user_group'] = '';
                $user_groups = $this->term_users_m->get_user_terms($staff_business, $this->user_group_m->term_type);
                $user_groups and $data['user_group'] = implode(', ', array_column($user_groups, 'term_name'));

                return $data;
            }, FALSE)

            ->from('term')
            ->where_in('term.term_type', array_keys($contract_services_config))
            ->where_in('term.term_status', ['publish', 'ending']);

        if (is_array($relate_users)) {
            $this->datatable_builder->where_in('term.term_id', $contract_ids);
        }

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        $last_query = $this->datatable_builder->last_query();
        $last_query = preg_replace('/((L|l)(I|i)(M|m)(I|i)(T|t) \d+)/', '', $last_query);
        $subheadings_data = $this->db->query($last_query)->result_array();
        $subheadings = array_reduce($subheadings_data, function ($result, $item) {
            $term_id = $item['term_id'];

            // Get contract_value
            $contract_value = (int)get_term_meta_value($term_id, 'contract_value') ?? 0;
            $result['contract_value'] += $contract_value;

            // Get service_fee
            $service_fee = (int)get_term_meta_value($term_id, 'service_fee') ?? 0;
            $result['service_fee'] += $service_fee;

            // Get payment_amount
            $payment_amount = (int)get_term_meta_value($term_id, 'payment_amount') ?? 0;
            $result['payment_amount'] += $payment_amount;

            // Get payment_amount_remaining
            $invs_amount = (int) get_term_meta_value($term_id, 'invs_amount');
            $payment_amount = (int) get_term_meta_value($term_id, 'payment_amount');
            $payment_amount_remaining = $invs_amount - $payment_amount;

            $payment_amount_remaining = max(0, $payment_amount_remaining);
            $result['payment_amount_remaining'] += $payment_amount_remaining;

            return $result;
        }, [
            'contract_value' => 0,
            'service_fee' => 0,
            'payment_amount' => 0,
            'payment_amount_remaining' => 0,
        ]);
        $subheadings = array_map(function ($item) {
            $item .= '';
            return $item;
        }, $subheadings);
        $data['subheadings'] = $subheadings;

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->exportXlsx($last_query);
            return TRUE;
        }

        $this->template->title->append('Kho tài khoản');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering($args = array())
    {
        restrict('reports.revenue.access');

        $args = parent::get();
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // cid FILTERING & SORTING
        $filter_cid = $args['where']['cid'] ?? FALSE;
        $sort_cid = $args['order_by']['cid'] ?? FALSE;
        if ($filter_cid || $sort_cid) {
            $alias_term_users = uniqid('tu_term_users_');
            $alias_user = uniqid('user_');
            $alias_usermeta = uniqid('m_user_');

            $this->datatable_builder->join("term_users {$alias_term_users}", "{$alias_term_users}.term_id = term.term_id", 'LEFT')
                ->join("user {$alias_user}", "{$alias_term_users}.user_id = {$alias_user}.user_id AND {$alias_user}.user_type IN ('customer_company','customer_person')", 'LEFT')
                ->join("usermeta {$alias_usermeta}", "{$alias_usermeta}.user_id = {$alias_user}.user_id AND {$alias_usermeta}.meta_key = 'cid'", 'LEFT');

            if ($filter_cid) {
                $this->datatable_builder->like("{$alias_usermeta}.meta_value", $filter_cid);
            }

            if ($sort_cid) {
                $this->datatable_builder->order_by("{$alias_usermeta}.meta_value", $sort_cid);
            }
        }

        // customer FILTERING & SORTING
        $filter_customer = $args['where']['customer'] ?? FALSE;
        $sort_customer = $args['order_by']['customer'] ?? FALSE;
        if ($filter_customer || $sort_customer) {
            $alias_term_users = uniqid('term_users_');
            $alias_user = uniqid('user_');

            $this->datatable_builder->join("term_users {$alias_term_users}", "{$alias_term_users}.term_id = term.term_id", 'LEFT')
                ->join("user {$alias_user}", "{$alias_term_users}.user_id = {$alias_user}.user_id AND {$alias_user}.user_type IN ('customer_company','customer_person')", 'LEFT');

            if ($filter_customer) {
                $this->datatable_builder->like("{$alias_user}.display_name", $filter_customer);
            }

            if ($sort_customer) {
                $this->datatable_builder->order_by("{$alias_user}.display_name", $sort_customer);
            }
        }

        // contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $alias_contract_code = uniqid('contract_code_');

            $this->datatable_builder->join("termmeta {$alias_contract_code}", "{$alias_contract_code}.term_id = term.term_id AND {$alias_contract_code}.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("{$alias_contract_code}.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder->order_by("{$alias_contract_code}.meta_value", $sort_contract_code);
            }
        }

        // contract_service FILTERING & SORTING
        $filter_contract_service = $args['where']['contract_service'] ?? FALSE;
        if ($filter_contract_service) {
            $this->datatable_builder->where('term.term_type', $filter_contract_service);
        }

        $sort_contract_service = $args['order_by']['contract_service'] ?? FALSE;
        if ($sort_contract_service) {
            $this->datatable_builder->order_by('term.term_type', $sort_contract_service);
        }

        // term_status FILTERING & SORTING
        $filter_term_status = $args['where']['term_status'] ?? FALSE;
        if ($filter_term_status) {
            $this->datatable_builder->where('term.term_status', $filter_term_status);
        }

        $sort_term_status = $args['order_by']['term_status'] ?? FALSE;
        if ($sort_term_status) {
            $this->datatable_builder->order_by('term.term_status', $sort_term_status);
        }

        // contract_begin FILTERING & SORTING
        $filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
        $sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
        if ($filter_contract_begin || $sort_contract_begin) {
            $alias_contract_begin = uniqid('contract_begin_');

            $this->datatable_builder->join("termmeta {$alias_contract_begin}", "{$alias_contract_begin}.term_id = term.term_id AND {$alias_contract_begin}.meta_key = 'contract_begin'", 'LEFT');

            if ($filter_contract_begin) {
                $dates = explode(' - ', $filter_contract_begin);

                $this->datatable_builder
                    ->where("{$alias_contract_begin}.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("{$alias_contract_begin}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_begin) {
                $this->datatable_builder->order_by("{$alias_contract_begin}.meta_value", $sort_contract_begin);
            }
        }

        // contract_end FILTERING & SORTING
        $filter_contract_end = $args['where']['contract_end'] ?? FALSE;
        $sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
        if ($filter_contract_end || $sort_contract_end) {
            $alias_contract_end = uniqid('contract_end_');

            $this->datatable_builder->join("termmeta {$alias_contract_end}", "{$alias_contract_end}.term_id = term.term_id AND {$alias_contract_end}.meta_key = 'contract_end'", 'LEFT');

            if ($filter_contract_end) {
                $dates = explode(' - ', $filter_contract_end);

                $this->datatable_builder
                    ->where("{$alias_contract_end}.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("{$alias_contract_end}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_end) {
                $this->datatable_builder->order_by("{$alias_contract_end}.meta_value", $sort_contract_end);
            }
        }

        // start_service_time FILTERING & SORTING
        $filter_start_service_time = $args['where']['start_service_time'] ?? FALSE;
        $sort_start_service_time = $args['order_by']['start_service_time'] ?? FALSE;
        if ($filter_start_service_time || $sort_start_service_time) {
            $alias_start_service_time = uniqid('start_service_time_');

            $this->datatable_builder->join("termmeta {$alias_start_service_time}", "{$alias_start_service_time}.term_id = term.term_id AND {$alias_start_service_time}.meta_key = 'start_service_time'", 'LEFT');

            if ($filter_start_service_time) {
                $dates = explode(' - ', $filter_start_service_time);

                $this->datatable_builder
                    ->where("{$alias_start_service_time}.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("{$alias_start_service_time}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_start_service_time) {
                $this->datatable_builder->order_by("{$alias_start_service_time}.meta_value", $sort_start_service_time);
            }
        }

        // contract_value FILTERING & SORTING
        $filter_contract_value = $args['where']['contract_value'] ?? FALSE;
        $sort_contract_value = $args['order_by']['contract_value'] ?? FALSE;
        if ($filter_contract_value || $sort_contract_value) {
            $alias_contract_value = uniqid('contract_value_');

            $this->datatable_builder->join("termmeta {$alias_contract_value}", "{$alias_contract_value}.term_id = term.term_id AND {$alias_contract_value}.meta_key = 'contract_value'", 'LEFT');

            if ($filter_contract_value) {
                $this->datatable_builder->where("CAST({$alias_contract_value}.meta_value AS SIGNED) >=", $filter_contract_value);
            }

            if ($sort_contract_value) {
                $this->datatable_builder->order_by("CAST({$alias_contract_value}.meta_value AS SIGNED)", $sort_contract_value);
            }
        }

        // service_fee FILTERING & SORTING
        $filter_service_fee = $args['where']['service_fee'] ?? FALSE;
        $sort_service_fee = $args['order_by']['service_fee'] ?? FALSE;
        if ($filter_service_fee || $sort_service_fee) {
            $alias_service_fee = uniqid('service_fee_');

            $this->datatable_builder->join("termmeta {$alias_service_fee}", "{$alias_service_fee}.term_id = term.term_id AND {$alias_service_fee}.meta_key = 'service_fee'", 'LEFT');

            if ($filter_service_fee) {
                $this->datatable_builder->where("CAST({$alias_service_fee}.meta_value AS SIGNED) >=", $filter_service_fee);
            }

            if ($sort_service_fee) {
                $this->datatable_builder->order_by("CAST({$alias_service_fee}.meta_value AS SIGNED)", $sort_service_fee);
            }
        }

        // payment_amount FILTERING & SORTING
        $filter_payment_amount = $args['where']['payment_amount'] ?? FALSE;
        $sort_payment_amount = $args['order_by']['payment_amount'] ?? FALSE;
        if ($filter_payment_amount || $sort_payment_amount) {
            $alias_payment_amount = uniqid('payment_amount_');

            $this->datatable_builder->join("termmeta {$alias_payment_amount}", "{$alias_payment_amount}.term_id = term.term_id AND {$alias_payment_amount}.meta_key = 'payment_amount'", 'LEFT');

            if ($filter_payment_amount) {
                $this->datatable_builder->where("CAST({$alias_payment_amount}.meta_value AS SIGNED) >=", $filter_payment_amount);
            }

            if ($sort_payment_amount) {
                $this->datatable_builder->order_by("CAST({$alias_payment_amount}.meta_value AS SIGNED)", $sort_payment_amount);
            }
        }

        // payment_amount_remaining FILTERING & SORTING
        $filter_payment_amount_remaining = $args['where']['payment_amount_remaining'] ?? FALSE;
        $sort_payment_amount_remaining = $args['order_by']['payment_amount_remaining'] ?? FALSE;
        if ($filter_payment_amount_remaining || $sort_payment_amount_remaining) {
            $alias_invs_amount = uniqid('invs_amount_');
            $alias_payment_amount = uniqid('payment_amount_');

            $this->datatable_builder
                ->join("termmeta {$alias_invs_amount}", "{$alias_invs_amount}.term_id = term.term_id AND {$alias_invs_amount}.meta_key = 'invs_amount'", 'LEFT')
                ->join("termmeta {$alias_payment_amount}", "{$alias_payment_amount}.term_id = term.term_id AND {$alias_payment_amount}.meta_key = 'payment_amount'", 'LEFT')
                ->select("IF(($alias_invs_amount.meta_value - $alias_payment_amount.meta_value < 0), 0, $alias_invs_amount.meta_value - $alias_payment_amount.meta_value) AS payment_amount_remaining");

            if ($filter_payment_amount_remaining) {
                $this->datatable_builder->having('CAST(payment_amount_remaining AS SIGNED) >=', $filter_payment_amount_remaining);
            }

            if ($sort_payment_amount_remaining) {
                $this->datatable_builder->order_by('[custom-select].CAST(payment_amount_remaining AS SIGNED)', $sort_payment_amount_remaining);
            }
        }

        // staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if ($filter_staff_business || $sort_staff_business) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('staff_business_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = total_report.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("user $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT');

            if ($filter_staff_business) {
                $this->datatable_builder->like("$aliasStaffBusiness.display_name", $filter_staff_business);
            }

            if ($sort_staff_business) {
                $this->datatable_builder->order_by("$aliasStaffBusiness.display_name", $sort_staff_business);
            }
        }

        // department FILTERING & SORTING
        $filter_department = $args['where']['department'] ?? FALSE;
        $sort_department = $args['order_by']['department'] ?? FALSE;
        if ($filter_department || $sort_department) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('tu_department_');
            $aliasDepartment = uniqid('department_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = total_report.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("term_users $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT')
                ->join("term $aliasDepartment", "$aliasDepartment.term_id = $aliasStaffBusiness.term_id AND $aliasDepartment.term_type = '{$this->department_m->term_type}'", 'LEFT');

            if ($filter_department) {
                $this->datatable_builder->where("$aliasDepartment.term_id", $filter_department);
            }

            if ($sort_department) {
                $this->datatable_builder->order_by("$aliasDepartment.term_name", $sort_department);
            }
        }

        // user_group FILTERING & SORTING
        $filter_user_group = $args['where']['user_group'] ?? FALSE;
        $sort_user_group = $args['order_by']['user_group'] ?? FALSE;
        if ($filter_user_group || $sort_user_group) {
            $aliasMetaStaffBusiness = uniqid('m_staff_business_');
            $aliasStaffBusiness = uniqid('tu_user_group_');
            $aliaUserGroup = uniqid('user_group_');

            $this->datatable_builder->join("termmeta $aliasMetaStaffBusiness", "$aliasMetaStaffBusiness.term_id = total_report.term_id AND $aliasMetaStaffBusiness.meta_key = 'staff_business'", 'LEFT')
                ->join("term_users $aliasStaffBusiness", "$aliasStaffBusiness.user_id = $aliasMetaStaffBusiness.meta_value", 'LEFT')
                ->join("term $aliaUserGroup", "$aliaUserGroup.term_id = $aliasStaffBusiness.term_id AND $aliaUserGroup.term_type = '{$this->user_group_m->term_type}'", 'LEFT');

            if ($filter_user_group) {
                $this->datatable_builder->where("$aliaUserGroup.term_id", $filter_user_group);
            }

            if ($sort_user_group) {
                $this->datatable_builder->order_by("$aliaUserGroup.term_name", $sort_user_group);
            }
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function exportXlsx($query = '')
    {
        restrict('reports.revenue.access');

        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $contracts = $this->term_m->query($query)->result();
        if (!$contracts) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $title          = my_date(time(), 'Ymd'). '_export_bao_cao_doanh_thu';
        $spreadsheet    = IOFactory::load('./files/excel_tpl/contract/revenua-report.xlsx');
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));

        $sheet = $spreadsheet->getActiveSheet();
        $rowIndex = 4;

        $contract_status_config = $this->config->item('contract_status');
        unset($contract_status_config['draft']);
        unset($contract_status_config['unverified']);

        $contract_services_config = $this->config->item('services');

        foreach ($contracts as $key => &$contract) {
            $col = 1;

            $term_id = $contract->term_id;
            $term_type = $contract->term_type;
            $term_status = $contract->term_status;

            // Get contract_code
            $contract_code = get_term_meta_value($term_id, 'contract_code') ?: '--';

            // Get customer
            $customer_name = '';
            $cid = '';
            $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company', 'system']);
            if (!empty($customers)) {
                $customer = end($customers);
                $customer_name = $customer->display_name;
                $cid = get_user_meta_value($customer->user_id, 'cid') ?? '';
                empty($cid) and $cid = cid($customer->user_id, $customer->user_type);
            }

            // Get contract_service
            $contract_service = $contract_services_config[$term_type] ?? '--';

            // Get term_status
            $term_status = $contract_status_config[$term_status] ?? '--';

            // Get contract_begin
            $contract_begin = get_term_meta_value($term_id, 'contract_begin');
            $contract_begin = $contract_begin ? my_date((int)$contract_begin, 'd/m/Y') : '--';

            // Get contract_end
            $contract_end = get_term_meta_value($term_id, 'contract_end');
            $contract_end = $contract_end ? my_date((int)$contract_end, 'd/m/Y') : '--';

            // Get start_service_time
            $start_service_time = get_term_meta_value($term_id, 'start_service_time');
            $start_service_time = $start_service_time ? my_date((int)$start_service_time, 'd/m/Y') : '--';

            // Get contract_value
            $contract_value = (int)get_term_meta_value($term_id, 'contract_value') ?? 0;

            // Get service_fee
            $service_fee = (int)get_term_meta_value($term_id, 'service_fee') ?? 0;

            // Get payment_amount
            $payment_amount = (int)get_term_meta_value($term_id, 'payment_amount') ?? 0;

            // Get payment_amount_remaining
            $invs_amount = (int) get_term_meta_value($term_id, 'invs_amount');
            $payment_amount = (int) get_term_meta_value($term_id, 'payment_amount');
            $payment_amount_remaining = $invs_amount - $payment_amount;
            $payment_amount_remaining = max(0, $payment_amount_remaining);

            // Get latest_receipt_date
            $latest_receipt_date = '--';

            $receipts = $this->receipt_m
                ->select('posts.post_id,posts.post_author,posts.post_type,posts.end_date,posts.post_status')
                ->join('term_posts', 'term_posts.post_id = posts.post_id', 'LEFT')
                ->where('term_posts.term_id', $term_id)
                ->where('posts.post_status', 'paid')
                ->where_in('posts.post_type', ['receipt_payment', 'receipt_caution'])
                ->order_by('posts.end_date', 'desc')
                ->get_many_by();
            $receipts = array_group_by($receipts, 'post_type');

            if (!empty($receipts['receipt_payment'])) {
                $receipts['receipt_payment'] = array_group_by($receipts['receipt_payment'], 'post_status');
                if (!empty($receipts['receipt_payment']['paid'])) {
                    $receipt_payment_paid = $receipts['receipt_payment']['paid'];
                    $receipt_payment_newest = reset($receipt_payment_paid);

                    $latest_receipt_date = my_date($receipt_payment_newest->end_date, 'd/m/Y');
                }
            }

            // Get staff_business
            $sale_id = get_term_meta_value($term_id, 'staff_business');
            $staff_business = $this->admin_m->get_field_by_id($sale_id, 'display_name') ?: '--';

            // Get departments
            $department = '';
            $departments = $this->term_users_m->get_user_terms($sale_id, $this->department_m->term_type);
            $departments and $department = implode(', ', array_column($departments, 'term_name'));

            // Get user_groups
            $user_group = '';
            $user_groups = $this->term_users_m->get_user_terms($sale_id, $this->user_group_m->term_type);
            $user_groups and $user_group = implode(', ', array_column($user_groups, 'term_name'));

            // Get staff_techs
            $staff_techs = '--';

            $model_kpi = '';
            switch ($term_type) {
                case 'courseads':
                    $this->load->model('courseads/courseads_kpi_m');
                    $model_kpi = 'courseads_kpi_m';
                    break;
                case 'banner':
                    $this->load->model('banner/banner_kpi_m');
                    $model_kpi = 'banner_kpi_m';
                    break;
                case 'facebook-ads':
                    $this->load->model('facebookads/facebookads_kpi_m');
                    $model_kpi = 'facebookads_kpi_m';
                    break;
                case 'google-ads':
                    $this->load->model('googleads/googleads_kpi_m');
                    $model_kpi = 'googleads_kpi_m';
                    break;
                case 'hosting':
                    $this->load->model('hosting/hosting_kpi_m');
                    $model_kpi = 'hosting_kpi_m';
                    break;
                case 'onead':
                    $this->load->model('onead/onead_kpi_m');
                    $model_kpi = 'onead_kpi_m';
                    break;
                case 'oneweb' || 'webgeneral':
                    $this->load->model('webgeneral/webgeneral_kpi_m');
                    $model_kpi = 'webgeneral_kpi_m';
                    break;
                case 'seo-traffic':
                    $this->load->model('seotraffic/seotraffic_kpi_m');
                    $model_kpi = 'seotraffic_kpi_m';
                    break;
                case 'webdoctor':
                    $this->load->model('webdoctor/webdoctor_kpi_m');
                    $model_kpi = 'webdoctor_kpi_m';
                    break;
                case 'weboptimize':
                    $this->load->model('weboptimize/weboptimize_kpi_m');
                    $model_kpi = 'weboptimize_kpi_m';
                    break;
                default:
                    break;
            }

            $kpis = [];
            if ($term_type == 'courseads') {

                $kpis = $this->$model_kpi
                    ->order_by('kpi_type')
                    ->where('object_id', $term_id)
                    ->as_array()
                    ->get_many_by();
            } else {
                $kpis = $this->$model_kpi->get_many_by(['term_id' => $term_id, 'kpi_type' => 'tech']);
            }

            if (!empty($kpis)) {
                $staff_techs = array_map(function ($x) {
                    return $this->admin_m->get_field_by_id($x->user_id, 'display_name');
                }, $kpis);

                $staff_techs = implode(', ', $staff_techs);
            }

            // Set Cell
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $cid ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $customer_name ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_code ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_service ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $term_status ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_begin ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_end ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $start_service_time ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $contract_value ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $service_fee ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $payment_amount ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $payment_amount_remaining ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $latest_receipt_date ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $staff_business ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $department ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $user_group ?: '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $staff_techs ?: '--');

            $rowIndex++;
        }

        $numbers_of_spend = count($contracts);
        // Set Cells Number
        $sheet->getStyle('G' . $rowIndex . ':K' . ($numbers_of_spend + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        // Set Cells Date
        $sheet->getStyle('D' . $rowIndex . ':F' . ($numbers_of_spend + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);

        $folder_upload  = 'files/contract/report/';
        if (!is_dir($folder_upload)) {
            try {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}/{$title}.xlsx";
        try {
            $writer = IOFactory::createWriter($spreadsheet, 'Xls');
            $writer->save($fileName);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetRevenueReport.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetRevenueReport.php */