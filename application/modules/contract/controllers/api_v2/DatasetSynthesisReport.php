<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetSynthesisReport extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'term_posts_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';

        parent::__construct();

        $this->load->config('contract/contract');
        $this->load->config('googleads/contract');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('admin.report.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        if (isset($args['is_monthly']) && $args['is_monthly']) {
            $this->load->library('form_validation');
            $this->form_validation->set_data($args);
            $this->form_validation->set_rules('month', 'Month', 'required|integer|greater_than_equal_to[1]|less_than_equal_to[12]');
            $this->form_validation->set_rules('year', 'Year', 'required|integer|greater_than_equal_to[2015]|less_than_equal_to[' . my_date(strtotime("+1 year"), 'Y') . ']');
            if (FALSE == $this->form_validation->run()) parent::response(['code' => 400, 'error' => $this->form_validation->error_array()]);
        }

        $data = $this->data;
        $this->filtering($args);

        $data['content'] = $this->get_content($args);

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export_synthesis_report($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    protected function get_content($args = [])
    {
        // Prepare data
        $contract_status = array_keys($this->config->item('contract_status'));
        $contract_status = array_filter($contract_status, function ($key) {
            return !in_array($key, ['draft', 'unverified', '']);
        });

        $contract_budget_payment_types = $this->config->item('enums', 'contract_budget_payment_types');
        $service_fee_payment_type = $this->config->item('enums', 'service_fee_payment_type');

        $content = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')
            ->select('term.term_id, term.term_status, term.term_type')

            ->add_search('cid', ['placeholder' => 'CID'])
            ->add_search('customer', ['placeholder' => 'Khách hàng'])
            ->add_search('contract_code', ['placeholder' => 'Số hợp đồng'])
            ->add_search('contract_budget_payment_type', ['content' => form_dropdown(['name' => 'where[contract_budget_payment_type]', 'class' => 'form-control select2'], prepare_dropdown($contract_budget_payment_types, 'Loại hợp đồng'), parent::get('where[contract_budget_payment_type]'))])
            ->add_search('service_fee_payment_type', ['content' => form_dropdown(['name' => 'where[service_fee_payment_type]', 'class' => 'form-control select2'], prepare_dropdown($service_fee_payment_type, 'Phương thức thanh toán phí DV'), parent::get('where[service_fee_payment_type]'))])
            ->add_search('service_fee_rate_actual', ['placeholder' => '% phí dịch vụ'])
            ->add_search('start_date', ['placeholder' => 'Ngày bắt đầu', 'class' => 'form-control set-datepicker'])
            ->add_search('end_date', ['placeholder' => 'Ngày kết thúc', 'class' => 'form-control set-datepicker'])
            ->add_search('receipt_payment_actual_budget', ['placeholder' => 'Ngân sách'])
            ->add_search('receipt_payment_service_fee', ['placeholder' => 'Phí dịch vụ'])
            ->add_search('receipt_payment_fct_amount', ['placeholder' => 'Thuế nhà thầu'])
            ->add_search('receipt_payment_vat_amount', ['placeholder' => 'GTGT'])
            ->add_search('receipt_payment_amount', ['placeholder' => 'Tổng tiền đã thu'])
            ->add_search('actual_result', ['placeholder' => 'Ngân sách'])
            ->add_search('service_fee_spent', ['placeholder' => 'Phí dịch vụ'])
            ->add_search('invoice_actual_budget', ['placeholder' => 'Ngân sách'])
            ->add_search('invoice_service_fee', ['placeholder' => 'Phí dịch vụ'])
            ->add_search('invoice_total', ['placeholder' => 'Tổng cộng'])
            ->add_search('compare_budget', ['placeholder' => 'Ngân sách'])
            ->add_search('compare_service_fee', ['placeholder' => 'Phí dịch vụ'])
            ->add_search('compare_total', ['placeholder' => 'Tổng cộng'])

            // Group customer
            ->add_column('cid', array('title' => 'CID', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('customer', array('title' => 'Khách hàng', 'set_select' => FALSE, 'set_order' => TRUE))

            // Group contract
            ->add_column('contract_code', array('title' => 'Số hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_budget_payment_type', array('title' => 'Loại hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_fee_rate_actual', array('title' => '% phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))

            ->add_column('start_date', array('title' => 'Ngày bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('end_date', array('title' => 'Ngày kết thúc', 'set_select' => FALSE, 'set_order' => TRUE))

            // Group receipt payment
            ->add_column('receipt_payment_actual_budget', array('title' => 'Ngân sách', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('receipt_payment_service_fee', array('title' => 'Phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('receipt_payment_fct_amount', array('title' => 'Thuế nhà thầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('receipt_payment_vat_amount', array('title' => 'GTGT', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('receipt_payment_amount', array('title' => 'Tổng tiền đã thu', 'set_select' => FALSE, 'set_order' => TRUE))

            // Group spend
            ->add_column('actual_result', array('title' => 'Ngân sách', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('service_fee_spent', array('title' => 'Phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('check_invoice', array('title' => 'Check hoá đơn', 'set_select' => FALSE, 'set_order' => FALSE))

            // Group invoice
            ->add_column('invoice_actual_budget', array('title' => 'Ngân sách', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('invoice_service_fee', array('title' => 'Phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('invoice_total', array('title' => 'Tổng cộng', 'set_select' => FALSE, 'set_order' => TRUE))

            ->add_column('compare_invoice', array('title' => 'Đối chiếu XHĐ', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('process', array('title' => 'Xử lý', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('confỉm', array('title' => 'Xác nhận', 'set_select' => FALSE, 'set_order' => FALSE))

            ->add_column('compare_budget', array('title' => 'Ngân sách', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('compare_service_fee', array('title' => 'Phí dịch vụ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('compare_total', array('title' => 'Tổng cộng', 'set_select' => FALSE, 'set_order' => TRUE))

            ->add_column('compare_budget_actual', array('title' => 'Ngân sách', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('compare_service_fee_actual', array('title' => 'Phí dịch vụ', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('compare_summary_actual', array('title' => 'Tổng cộng', 'set_select' => FALSE, 'set_order' => FALSE))

            ->add_column_callback('contract_id', function ($data, $row_name) use ($contract_budget_payment_types, $service_fee_payment_type) {
                $term_id = $data['contract_id'];

                // Get customer
                $data['customer'] = '--';
                $data['cid'] = '--';
                $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company', 'system']);
                if (!empty($customers)) {
                    $customer = end($customers);
                    $data['customer'] = $customer->display_name;
                    $data['cid'] = get_user_meta_value($customer->user_id, 'cid') ?? '--';
                }

                // Get contract_code
                $contract_code = get_term_meta_value($term_id, 'contract_code');
                $data['contract_code'] = $contract_code ?: '--';

                // Get start_date
                $contract_begin = get_term_meta_value($term_id, 'contract_begin');
                $data['start_date'] = $contract_begin ? date('d/m/Y', $contract_begin) : '--';

                // Get end_date
                $contract_end = get_term_meta_value($term_id, 'contract_end');
                $data['end_date'] = $contract_end ? date('d/m/Y', $contract_end) : '--';

                // Get contract_budget_payment_type
                $contract_budget_payment_type = get_term_meta_value($term_id, 'contract_budget_payment_type');
                $data['contract_budget_payment_type'] = $contract_budget_payment_types[$contract_budget_payment_type] ?? '--';

                // Get service_fee_payment_type
                $service_fee_payment_type = get_term_meta_value($term_id, 'service_fee_payment_type');
                $data['service_fee_payment_type'] = $service_fee_payment_type[$service_fee_payment_type] ?? '--';

                // Get service_fee_rate_actual
                $service_fee_rate_actual = (float)get_term_meta_value($term_id, 'service_fee_rate_actual');
                $data['service_fee_rate_actual'] = $service_fee_rate_actual ?? 0;

                // Get postmeta
                $receipt_payment_actual_budget = 0;
                $receipt_payment_service_fee = 0;
                $receipt_payment_fct_amount = 0;
                $receipt_payment_vat_amount = 0;
                $receipt_payment_amount = 0;

                $invoice_actual_budget = 0;
                $invoice_service_fee = 0;

                $posts = $this->term_posts_m->get_term_posts($term_id, 'receipt_payment');
                if (!empty($posts)) {
                    foreach ($posts as $post) {
                        $post_id = $post->post_id;

                        $is_order_printed = get_post_meta_value($post_id, 'is_order_printed');

                        $actual_budget = (int)get_post_meta_value($post_id, 'actual_budget');
                        $receipt_payment_actual_budget += $actual_budget;
                        $is_order_printed && $invoice_actual_budget += $actual_budget;

                        $service_fee = (int)get_post_meta_value($post_id, 'service_fee');
                        $receipt_payment_service_fee += $service_fee;
                        $is_order_printed && $invoice_service_fee += $service_fee;

                        $fct_amount = (int)get_post_meta_value($post_id, 'fct_amount');
                        $receipt_payment_fct_amount += $fct_amount;

                        $vat_amount = (int)get_post_meta_value($post_id, 'vat_amount');
                        $receipt_payment_vat_amount += $vat_amount;

                        $amount = (int)get_post_meta_value($post_id, 'amount');
                        $receipt_payment_amount += $amount;
                    }
                }

                $data['receipt_payment_actual_budget'] = $receipt_payment_actual_budget;
                $data['receipt_payment_service_fee'] = $receipt_payment_service_fee;
                $data['receipt_payment_fct_amount'] = $receipt_payment_fct_amount;
                $data['receipt_payment_vat_amount'] = $receipt_payment_vat_amount;
                $data['receipt_payment_amount'] = $receipt_payment_amount;

                $data['invoice_actual_budget'] = $invoice_actual_budget;
                $data['invoice_service_fee'] = $invoice_service_fee;
                $data['invoice_total'] = $invoice_actual_budget + $invoice_service_fee;

                // Get spent
                $spend = (int)$data['spend'];
                $data['actual_result'] = $spend;
                $service_fee_spent = $spend ? $spend * $service_fee_rate_actual : 0;
                $data['service_fee_spent'] = $service_fee_spent;

                // Get Compare
                $compare_budget = $receipt_payment_actual_budget - $spend;
                $data['compare_budget'] = $compare_budget;
                $compare_service_fee = $receipt_payment_service_fee - $service_fee_spent;
                $data['compare_service_fee'] = $compare_service_fee;

                $data['compare_total'] = $compare_budget + $compare_service_fee;

                return $data;
            }, FALSE)

            ->from('term')
            ->select('term.term_id AS contract_id')
            ->select('SUM(insight_segment_spend.meta_value) AS spend')
            ->join('term_posts', 'term_posts.term_id = term.term_id')
            ->join("posts ads_segment", "ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = 'ads_segment'")
            ->join('term_posts tp_adaccounts', 'tp_adaccounts.post_id = ads_segment.post_id')
            ->join("term adaccount", "adaccount.term_id = tp_adaccounts.term_id AND (adaccount.term_type = 'adaccount' OR adaccount.term_type = 'mcm_account')")
            ->join('term_posts tp_insight_segment', 'tp_insight_segment.term_id = adaccount.term_id')
            ->join("posts insight_segment", "insight_segment.post_id = tp_insight_segment.post_id AND insight_segment.post_type = 'insight_segment'")
            ->join("postmeta insight_segment_spend", "insight_segment_spend.post_id = insight_segment.post_id AND insight_segment_spend.meta_key = 'spend'")
            ->where_in('term.term_type', $this->contract_m->getTypes())
            ->where_in('term.term_status', $contract_status)
            ->where('term.term_status', 'publish')
            ->group_by('contract_id');

        if (isset($args['is_monthly']) && $args['is_monthly']) {
            $month = $args['month'] ?? my_date(time(), 'm');
            $year = $args['year'] ?? my_date(time(), 'y');

            $start_time = strtotime(date("$year-$month-1 00:00:00"));
            $end_time = strtotime(date("$year-$month-t 23:59:59"));

            $content->where('insight_segment.post_name', 'day')
                ->where('insight_segment.start_date >=', $start_time)
                ->where('insight_segment.end_date <=', $end_time);
        }

        if (isset($args['is_range']) && $args['is_range']) {
            $start = $args['start'] ?? time();
            $end = $args['end'] ?? time();

            $start_time = start_of_day($start);
            $end_time = end_of_day($end);

            $content->where('insight_segment.post_name', 'day')
                ->where('insight_segment.start_date >=', $start_time)
                ->where('insight_segment.end_date <=', $end_time);
        }

        return $content;
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering($args = array())
    {
        restrict('contract.revenue.access');

        $args = parent::get();
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // cid FILTERING & SORTING
        $filter_cid = $args['where']['cid'] ?? FALSE;
        $sort_cid = $args['order_by']['cid'] ?? FALSE;
        if ($filter_cid || $sort_cid) {
            $alias_term_customer = uniqid('tu_customer_');
            $alias_customer = uniqid('customer_');
            $alias_cid = uniqid('cid_');

            $this->datatable_builder->join("term_users {$alias_term_customer}", "{$alias_term_customer}.term_id = term.term_id", 'LEFT')
                ->join("user {$alias_customer}", "{$alias_term_customer}.user_id = {$alias_customer}.user_id AND {$alias_customer}.user_type IN ('customer_company','customer_person')", 'LEFT')
                ->join("usermeta {$alias_cid}", "{$alias_cid}.user_id = {$alias_customer}.user_id AND {$alias_cid}.meta_key = 'cid'", 'LEFT');

            if ($filter_cid) {
                $this->datatable_builder->like("{$alias_cid}.meta_value", $filter_cid);
            }

            if ($sort_cid) {
                $this->datatable_builder
                    ->select("{$alias_cid}.meta_value as cid")
                    ->group_by('cid')
                    ->having('cid IS NOT NULL')
                    ->order_by('[custom-select].cid', $sort_cid);

                unset($args['order_by']['cid']);
            }
        }

        // customer FILTERING & SORTING
        $filter_customer = $args['where']['customer'] ?? FALSE;
        $sort_customer = $args['order_by']['customer'] ?? FALSE;
        if ($filter_customer || $sort_customer) {
            $alias_term_customer = uniqid('tu_customer_');
            $alias_customer = uniqid('customer_');

            $this->datatable_builder->join("term_users {$alias_term_customer}", "{$alias_term_customer}.term_id = term.term_id", 'LEFT')
                ->join("user {$alias_customer}", "{$alias_term_customer}.user_id = {$alias_customer}.user_id AND {$alias_customer}.user_type IN ('customer_company','customer_person')", 'LEFT');

            if ($filter_customer) {
                $this->datatable_builder->like("{$alias_customer}.display_name", $filter_customer);
            }

            if ($sort_customer) {
                $this->datatable_builder
                    ->select("{$alias_customer}.display_name as customer_name")
                    ->group_by('customer_name')
                    ->having('customer_name IS NOT NULL')
                    ->order_by('[custom-select].customer_name', $sort_customer);
            }
        }

        // contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $alias_contract_code = uniqid('contract_code_');

            $this->datatable_builder->join("termmeta {$alias_contract_code}", "{$alias_contract_code}.term_id = term.term_id AND {$alias_contract_code}.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("{$alias_contract_code}.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder
                    ->select("{$alias_contract_code}.meta_value AS contract_code")
                    ->group_by('contract_code')
                    ->order_by('[custom-select].contract_code', $sort_contract_code);
            }
        }

        // contract_budget_payment_type FILTERING & SORTING
        $filter_contract_budget_payment_type = $args['where']['contract_budget_payment_type'] ?? FALSE;
        $sort_contract_budget_payment_type = $args['order_by']['contract_budget_payment_type'] ?? FALSE;
        if ($filter_contract_budget_payment_type || $sort_contract_budget_payment_type) {
            $alias = uniqid('contract_budget_payment_type_');

            $this->datatable_builder->join("termmeta $alias", "$alias.term_id = term.term_id and $alias.meta_key = 'contract_budget_payment_type'", 'LEFT');

            if ($filter_contract_budget_payment_type) {
                $this->datatable_builder->like("$alias.meta_value", $filter_contract_budget_payment_type);
            }

            if ($sort_contract_budget_payment_type) {
                $this->datatable_builder
                    ->select("{$alias}.meta_value AS contract_budget_payment_type")
                    ->group_by('contract_budget_payment_type')
                    ->order_by("$alias.meta_value", $sort_contract_budget_payment_type);
            }
        }

        // service_fee_rate_actual FILTERING & SORTING
        $filter_service_fee_rate_actual = $args['where']['service_fee_rate_actual'] ?? FALSE;
        $sort_service_fee_rate_actual = $args['order_by']['service_fee_rate_actual'] ?? FALSE;
        if ($filter_service_fee_rate_actual || $sort_service_fee_rate_actual) {
            $alias = uniqid('service_fee_rate_actual_');

            $this->datatable_builder->join("termmeta $alias", "$alias.term_id = term.term_id and $alias.meta_key = 'service_fee_rate_actual'", 'LEFT');

            if ($filter_service_fee_rate_actual) {
                $this->datatable_builder->where("CAST($alias.meta_value AS DECIMAL(10,2)) >=", div((float)$filter_service_fee_rate_actual, 100));
            }

            if ($sort_service_fee_rate_actual) {
                $this->datatable_builder
                    ->select("{$alias}.meta_value AS service_fee_rate_actual")
                    ->group_by('service_fee_rate_actual')
                    ->order_by("$alias.meta_value", $sort_service_fee_rate_actual);
            }
        }

        // start_date FILTERING & SORTING
        $filter_start_date = $args['where']['start_date'] ?? FALSE;
        $sort_start_date = $args['order_by']['start_date'] ?? FALSE;
        if ($filter_start_date || $sort_start_date) {
            $alias = uniqid('start_date_');

            $this->datatable_builder->join("termmeta $alias", "$alias.term_id = term.term_id and $alias.meta_key = 'contract_begin'", 'LEFT');

            if ($filter_start_date) {
                $dates = explode(' - ', $filter_start_date);

                $this->datatable_builder->where("CAST($alias.meta_value AS UNSIGNED) >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("CAST($alias.meta_value AS UNSIGNED) <=", $this->mdate->startOfDay(reset($dates)));
            }

            if ($sort_start_date) {
                $this->datatable_builder
                    ->select("{$alias}.meta_value AS start_date")
                    ->group_by('start_date')
                    ->order_by("CAST($alias.meta_value AS UNSIGNED)", $sort_start_date);
            }
        }

        // end_date FILTERING & SORTING
        $filter_end_date = $args['where']['end_date'] ?? FALSE;
        $sort_end_date = $args['order_by']['end_date'] ?? FALSE;
        if ($filter_end_date || $sort_end_date) {
            $alias = uniqid('end_date_');

            $this->datatable_builder->join("termmeta $alias", "$alias.term_id = term.term_id and $alias.meta_key = 'contract_end'", 'LEFT');

            if ($filter_end_date) {
                $dates = explode(' - ', $filter_end_date);

                $this->datatable_builder->where("CAST($alias.meta_value AS UNSIGNED) >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("CAST($alias.meta_value AS UNSIGNED) <=", $this->mdate->startOfDay(reset($dates)));
            }

            if ($sort_end_date) {
                $this->datatable_builder
                    ->select("{$alias}.meta_value AS end_date")
                    ->group_by('end_date')
                    ->order_by("CAST($alias.meta_value AS UNSIGNED)", $sort_end_date);
            }
        }

        // contract_begin FILTERING & SORTING
        $filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
        $sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
        if ($filter_contract_begin || $sort_contract_begin) {
            $alias = uniqid('contract_begin_');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

            if ($filter_contract_begin) {
                $dates = explode(' - ', $filter_contract_begin);
                $this->datatable_builder->where("CAST($alias.meta_value AS UNSIGNED) >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("CAST($alias.meta_value AS UNSIGNED) <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_begin) {
                $this->datatable_builder
                    ->select("{$alias}.meta_value AS contract_begin")
                    ->group_by('contract_begin')
                    ->order_by("CAST($alias.meta_value AS UNSIGNED)", $sort_contract_begin);
            }
        }

        // contract_end FILTERING & SORTING
        $filter_contract_end = $args['where']['contract_end'] ?? FALSE;
        $sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
        if ($filter_contract_end || $sort_contract_end) {
            $alias = uniqid('contract_end_');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_end'", 'LEFT');

            if ($filter_contract_end) {
                $dates = explode(' - ', $filter_contract_end);
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
                $this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_end) {
                $this->datatable_builder
                    ->select("{$alias}.meta_value AS contract_end")
                    ->group_by('contract_end')
                    ->order_by("{$alias}.meta_value", $sort_contract_end);
            }
        }

        // receipt_payment_actual_budget FILTERING & SORTING
        $filter_receipt_payment_actual_budget = $args['where']['receipt_payment_actual_budget'] ?? FALSE;
        $sort_receipt_payment_actual_budget = $args['order_by']['receipt_payment_actual_budget'] ?? FALSE;
        if ($filter_receipt_payment_actual_budget || $sort_receipt_payment_actual_budget) {
            $alias_tp_receipt_payment_actual_budget = uniqid('tp_receipt_payment_actual_budget_');
            $alias_receipt_payment_actual_budget = uniqid('receipt_payment_actual_budget_');
            $alias_actual_budget = uniqid('actual_budget');

            $this->datatable_builder
                ->join("term_posts $alias_tp_receipt_payment_actual_budget", "$alias_tp_receipt_payment_actual_budget.term_id = term.term_id")
                ->join("posts $alias_receipt_payment_actual_budget", "$alias_receipt_payment_actual_budget.post_id = $alias_tp_receipt_payment_actual_budget.post_id AND $alias_receipt_payment_actual_budget.post_type ='receipt_payment'")
                ->join("postmeta $alias_actual_budget", "$alias_actual_budget.post_id = $alias_receipt_payment_actual_budget.post_id AND $alias_actual_budget.meta_key = 'actual_budget'")
                ->select("(SUM($alias_actual_budget.meta_value) * COUNT(DISTINCT $alias_receipt_payment_actual_budget.post_id) / COUNT(term.term_id)) AS receipt_payment_actual_budget");

            if ($filter_receipt_payment_actual_budget) {
                $this->datatable_builder
                    ->having('CAST(receipt_payment_actual_budget AS SIGNED) >=', $filter_receipt_payment_actual_budget);
            }

            if ($sort_receipt_payment_actual_budget) {
                $this->datatable_builder
                    ->order_by('CAST([custom-select].receipt_payment_actual_budget AS SIGNED)', $sort_receipt_payment_actual_budget);
            }
        }

        // receipt_payment_service_fee FILTERING & SORTING
        $filter_receipt_payment_service_fee = $args['where']['receipt_payment_service_fee'] ?? FALSE;
        $sort_receipt_payment_service_fee = $args['order_by']['receipt_payment_service_fee'] ?? FALSE;
        if ($filter_receipt_payment_service_fee || $sort_receipt_payment_service_fee) {
            $alias_tp_receipt_payment_service_fee = uniqid('tp_receipt_payment_service_fee_');
            $alias_receipt_payment_service_fee = uniqid('receipt_payment_service_fee_');
            $alias_service_fee = uniqid('service_fee');

            $this->datatable_builder
                ->join("term_posts $alias_tp_receipt_payment_service_fee", "$alias_tp_receipt_payment_service_fee.term_id = term.term_id")
                ->join("posts $alias_receipt_payment_service_fee", "$alias_receipt_payment_service_fee.post_id = $alias_tp_receipt_payment_service_fee.post_id AND $alias_receipt_payment_service_fee.post_type ='receipt_payment'")
                ->join("postmeta $alias_service_fee", "$alias_service_fee.post_id = $alias_receipt_payment_service_fee.post_id AND $alias_service_fee.meta_key = 'service_fee'")
                ->select("(SUM($alias_service_fee.meta_value) * COUNT(DISTINCT $alias_receipt_payment_service_fee.post_id) / COUNT(term.term_id)) AS receipt_payment_service_fee");

            if ($filter_receipt_payment_service_fee) {
                $this->datatable_builder
                    ->having('CAST(receipt_payment_service_fee AS SIGNED) >=', $filter_receipt_payment_service_fee);
            }

            if ($sort_receipt_payment_service_fee) {
                $this->datatable_builder
                    ->order_by('CAST([custom-select].receipt_payment_service_fee AS SIGNED)', $sort_receipt_payment_service_fee);
            }
        }

        // receipt_payment_fct_amount FILTERING & SORTING
        $filter_receipt_payment_fct_amount = $args['where']['receipt_payment_fct_amount'] ?? FALSE;
        $sort_receipt_payment_fct_amount = $args['order_by']['receipt_payment_fct_amount'] ?? FALSE;
        if ($filter_receipt_payment_fct_amount || $sort_receipt_payment_fct_amount) {
            $alias_tp_receipt_payment_fct_amount = uniqid('tp_receipt_payment_fct_amount_');
            $alias_receipt_payment_fct_amount = uniqid('receipt_payment_fct_amount_');
            $alias_fct_amount = uniqid('fct_amount');

            $this->datatable_builder
                ->join("term_posts $alias_tp_receipt_payment_fct_amount", "$alias_tp_receipt_payment_fct_amount.term_id = term.term_id")
                ->join("posts $alias_receipt_payment_fct_amount", "$alias_receipt_payment_fct_amount.post_id = $alias_tp_receipt_payment_fct_amount.post_id AND $alias_receipt_payment_fct_amount.post_type ='receipt_payment'")
                ->join("postmeta $alias_fct_amount", "$alias_fct_amount.post_id = $alias_receipt_payment_fct_amount.post_id AND $alias_fct_amount.meta_key = 'fct_amount'");

            if ($filter_receipt_payment_fct_amount) {
                $this->datatable_builder
                    ->select("(SUM($alias_fct_amount.meta_value) * COUNT(DISTINCT $alias_receipt_payment_fct_amount.post_id) / COUNT(term.term_id)) AS receipt_payment_fct_amount")
                    ->having('CAST(receipt_payment_fct_amount AS SIGNED) >=', $filter_receipt_payment_fct_amount);
            }

            if ($sort_receipt_payment_fct_amount) {
                $this->datatable_builder
                    ->select("(SUM($alias_fct_amount.meta_value) * COUNT(DISTINCT $alias_receipt_payment_fct_amount.post_id) / COUNT(term.term_id)) AS receipt_payment_fct_amount")
                    ->order_by('CAST([custom-select].receipt_payment_fct_amount AS SIGNED)', $sort_receipt_payment_fct_amount);
            }
        }

        // receipt_payment_vat_amount FILTERING & SORTING
        $filter_receipt_payment_vat_amount = $args['where']['receipt_payment_vat_amount'] ?? FALSE;
        $sort_receipt_payment_vat_amount = $args['order_by']['receipt_payment_vat_amount'] ?? FALSE;
        if ($filter_receipt_payment_vat_amount || $sort_receipt_payment_vat_amount) {
            $alias_tp_receipt_payment_vat_amount = uniqid('tp_receipt_payment_vat_amount_');
            $alias_receipt_payment_vat_amount = uniqid('receipt_payment_vat_amount_');
            $alias_vat_amount = uniqid('vat_amount');

            $this->datatable_builder
                ->join("term_posts $alias_tp_receipt_payment_vat_amount", "$alias_tp_receipt_payment_vat_amount.term_id = term.term_id")
                ->join("posts $alias_receipt_payment_vat_amount", "$alias_receipt_payment_vat_amount.post_id = $alias_tp_receipt_payment_vat_amount.post_id AND $alias_receipt_payment_vat_amount.post_type ='receipt_payment'")
                ->join("postmeta $alias_vat_amount", "$alias_vat_amount.post_id = $alias_receipt_payment_vat_amount.post_id AND $alias_vat_amount.meta_key = 'vat_amount'");

            if ($filter_receipt_payment_vat_amount) {
                $this->datatable_builder
                    ->select("(SUM($alias_vat_amount.meta_value) * COUNT(DISTINCT $alias_receipt_payment_vat_amount.post_id) / COUNT(term.term_id)) AS receipt_payment_vat_amount")
                    ->having('CAST(receipt_payment_vat_amount AS SIGNED) >=', $filter_receipt_payment_vat_amount);
            }

            if ($sort_receipt_payment_vat_amount) {
                $this->datatable_builder
                    ->select("(SUM($alias_vat_amount.meta_value) * COUNT(DISTINCT $alias_receipt_payment_vat_amount.post_id) / COUNT(term.term_id)) AS receipt_payment_vat_amount")
                    ->order_by('CAST([custom-select].receipt_payment_vat_amount AS SIGNED)', $sort_receipt_payment_vat_amount);
            }
        }

        // receipt_payment_amount FILTERING & SORTING
        $filter_receipt_payment_amount = $args['where']['receipt_payment_amount'] ?? FALSE;
        $sort_receipt_payment_amount = $args['order_by']['receipt_payment_amount'] ?? FALSE;
        if ($filter_receipt_payment_amount || $sort_receipt_payment_amount) {
            $alias_tp_receipt_payment_amount = uniqid('tp_receipt_payment_amount_');
            $alias_receipt_payment_amount = uniqid('receipt_payment_amount_');
            $alias_amount = uniqid('amount');

            $this->datatable_builder
                ->join("term_posts $alias_tp_receipt_payment_amount", "$alias_tp_receipt_payment_amount.term_id = term.term_id")
                ->join("posts $alias_receipt_payment_amount", "$alias_receipt_payment_amount.post_id = $alias_tp_receipt_payment_amount.post_id AND $alias_receipt_payment_amount.post_type ='receipt_payment'")
                ->join("postmeta $alias_amount", "$alias_amount.post_id = $alias_receipt_payment_amount.post_id AND $alias_amount.meta_key = 'amount'")
                ->select("(SUM($alias_amount.meta_value) * COUNT(DISTINCT $alias_receipt_payment_amount.post_id) / COUNT(term.term_id)) AS receipt_payment_amount");

            if ($filter_receipt_payment_amount) {
                $this->datatable_builder
                    ->having('CAST(receipt_payment_amount AS SIGNED) >=', $filter_receipt_payment_amount);
            }

            if ($sort_receipt_payment_amount) {
                $this->datatable_builder
                    ->order_by('CAST([custom-select].receipt_payment_amount AS SIGNED)', $sort_receipt_payment_amount);
            }
        }

        // actual_result FILTERING & SORTING
        $filter_actual_result = $args['where']['actual_result'] ?? FALSE;
        if ($filter_actual_result) {
            $this->datatable_builder->where("CAST(spend AS SIGNED) >=", $filter_actual_result);
        }

        $sort_actual_result = $args['order_by']['actual_result'] ?? FALSE;
        if ($sort_actual_result) {
            $this->datatable_builder->order_by("[custom-select].CAST(spend AS SIGNED)", $sort_actual_result);
        }

        // actual_result FILTERING & SORTING
        $filter_actual_result = $args['where']['actual_result'] ?? FALSE;
        if ($filter_actual_result) {
            $this->datatable_builder->where("CAST(spend AS SIGNED) >=", $filter_actual_result);
        }

        $sort_actual_result = $args['order_by']['actual_result'] ?? FALSE;
        if ($sort_actual_result) {
            $this->datatable_builder->order_by("[custom-select].CAST(spend AS SIGNED)", $sort_actual_result);
        }

        // service_fee_spent FILTERING & SORTING
        $filter_service_fee_spent = $args['where']['service_fee_spent'] ?? FALSE;
        $sort_service_fee_spent = $args['order_by']['service_fee_spent'] ?? FALSE;
        if ($filter_service_fee_spent || $sort_service_fee_spent) {
            $alias_service_fee_rate_actual = uniqid('service_fee_rate_actual_');

            $this->datatable_builder
                ->join("termmeta $alias_service_fee_rate_actual", "$alias_service_fee_rate_actual.term_id = term.term_id AND $alias_service_fee_rate_actual.meta_key = 'service_fee_rate_actual'")
                ->select("(SUM(CAST(insight_segment_spend.meta_value AS SIGNED)) * (SUM($alias_service_fee_rate_actual.meta_value) * COUNT(DISTINCT $alias_service_fee_rate_actual.meta_id) / COUNT(term.term_id))) AS service_fee_spent");

            if ($filter_service_fee_spent) {
                $this->datatable_builder
                    ->having('service_fee_spent >=', $filter_service_fee_spent);
            }

            if ($sort_service_fee_spent) {
                $this->datatable_builder
                    ->order_by('[custom-select].service_fee_spent', $sort_service_fee_spent);
            }
        }

        // invoice_actual_budget FILTERING & SORTING
        $filter_invoice_actual_budget = $args['where']['invoice_actual_budget'] ?? FALSE;
        $sort_invoice_actual_budget = $args['order_by']['invoice_actual_budget'] ?? FALSE;
        if ($filter_invoice_actual_budget || $sort_invoice_actual_budget) {
            $alias_tp_invoice_actual_budget = uniqid('tp_invoice_actual_budget_');
            $alias_p_invoice_actual_budget = uniqid('invoice_actual_budget_');
            $alias_is_order_printed = uniqid('is_order_printed_');
            $alias_invoice_actual_budget = uniqid('invoice_actual_budget_');

            $this->datatable_builder
                ->join("term_posts $alias_tp_invoice_actual_budget", "$alias_tp_invoice_actual_budget.term_id = term.term_id")
                ->join("posts $alias_p_invoice_actual_budget", "$alias_p_invoice_actual_budget.post_id = $alias_tp_invoice_actual_budget.post_id AND $alias_p_invoice_actual_budget.post_type ='receipt_payment'")
                ->join("postmeta $alias_is_order_printed", "$alias_is_order_printed.post_id = $alias_p_invoice_actual_budget.post_id AND $alias_is_order_printed.meta_key = 'is_order_printed'", 'LEFT')
                ->join("postmeta $alias_invoice_actual_budget", "$alias_invoice_actual_budget.post_id = $alias_p_invoice_actual_budget.post_id AND $alias_invoice_actual_budget.meta_key = 'invoice_actual_budget'", 'LEFT')
                ->select("(CASE WHEN $alias_is_order_printed.meta_value != 1 THEN 0 ELSE 1 END) AS is_order_printed")
                ->select("(SUM($alias_invoice_actual_budget.meta_value) * COUNT(DISTINCT $alias_invoice_actual_budget.post_id) / COUNT(term.term_id)) AS invoice_actual_budget")
                ->group_by('is_order_printed')
                ->having('is_order_printed', '1');

            if ($filter_invoice_actual_budget) {
                $this->datatable_builder
                    ->having('CAST(invoice_actual_budget AS SIGNED) >=', $filter_invoice_actual_budget);
            }

            if ($sort_invoice_actual_budget) {
                $this->datatable_builder
                    ->order_by('CAST([custom-select].invoice_actual_budget AS SIGNED)', $sort_invoice_actual_budget);

                unset($args['order_by']['invoice_actual_budget']);
            }
        }

        // invoice_service_fee FILTERING & SORTING
        $filter_invoice_service_fee = $args['where']['invoice_service_fee'] ?? FALSE;
        $sort_invoice_service_fee = $args['order_by']['invoice_service_fee'] ?? FALSE;
        if ($filter_invoice_service_fee || $sort_invoice_service_fee) {
            $alias_tp_invoice_service_fee = uniqid('tp_invoice_service_fee_');
            $alias_p_invoice_service_fee = uniqid('invoice_service_fee_');
            $alias_is_order_printed = uniqid('is_order_printed_');
            $alias_invoice_service_fee = uniqid('invoice_service_fee_');

            $this->datatable_builder
                ->join("term_posts $alias_tp_invoice_service_fee", "$alias_tp_invoice_service_fee.term_id = term.term_id")
                ->join("posts $alias_p_invoice_service_fee", "$alias_p_invoice_service_fee.post_id = $alias_tp_invoice_service_fee.post_id AND $alias_p_invoice_service_fee.post_type ='receipt_payment'")
                ->join("postmeta $alias_is_order_printed", "$alias_is_order_printed.post_id = $alias_p_invoice_service_fee.post_id AND $alias_is_order_printed.meta_key = 'is_order_printed'", 'LEFT')
                ->join("postmeta $alias_invoice_service_fee", "$alias_invoice_service_fee.post_id = $alias_p_invoice_service_fee.post_id AND $alias_invoice_service_fee.meta_key = 'invoice_service_fee'", 'LEFT')
                ->select("(CASE WHEN $alias_is_order_printed.meta_value != 1 THEN 0 ELSE 1 END) AS is_order_printed")
                ->select("(SUM($alias_invoice_service_fee.meta_value) * COUNT(DISTINCT $alias_invoice_service_fee.post_id) / COUNT(term.term_id)) AS invoice_service_fee")
                ->group_by('is_order_printed')
                ->having('is_order_printed', '1');

            if ($filter_invoice_service_fee) {
                $this->datatable_builder
                    ->having('CAST(invoice_service_fee AS SIGNED) >=', $filter_invoice_service_fee);
            }

            if ($sort_invoice_service_fee) {
                $this->datatable_builder
                    ->order_by('CAST([custom-select].invoice_service_fee AS SIGNED)', $sort_invoice_service_fee);

                unset($args['order_by']['invoice_service_fee']);
            }
        }

        // invoice_total FILTERING & SORTING
        $filter_invoice_total = $args['where']['invoice_total'] ?? FALSE;
        $sort_invoice_total = $args['order_by']['invoice_total'] ?? FALSE;
        if ($filter_invoice_total || $sort_invoice_total) {
            $alias_tp_invoice_total = uniqid('tp_invoice_total_');
            $alias_p_invoice_total = uniqid('invoice_total_');
            $alias_is_order_printed = uniqid('is_order_printed_');
            $alias_invoice_actual_budget = uniqid('invoice_actual_budget_');
            $alias_invoice_service_fee = uniqid('invoice_service_fee_');

            $this->datatable_builder
                ->join("term_posts $alias_tp_invoice_total", "$alias_tp_invoice_total.term_id = term.term_id")
                ->join("posts $alias_p_invoice_total", "$alias_p_invoice_total.post_id = $alias_tp_invoice_total.post_id AND $alias_p_invoice_total.post_type ='receipt_payment'")
                ->join("postmeta $alias_is_order_printed", "$alias_is_order_printed.post_id = $alias_p_invoice_total.post_id AND $alias_is_order_printed.meta_key = 'is_order_printed'", 'LEFT')
                ->join("postmeta $alias_invoice_actual_budget", "$alias_invoice_actual_budget.post_id = $alias_p_invoice_total.post_id AND $alias_invoice_actual_budget.meta_key = 'invoice_actual_budget'", 'LEFT')
                ->join("postmeta $alias_invoice_service_fee", "$alias_invoice_service_fee.post_id = $alias_p_invoice_total.post_id AND $alias_invoice_service_fee.meta_key = 'invoice_service_fee'", 'LEFT')
                ->select("(CASE WHEN $alias_is_order_printed.meta_value != 1 THEN 0 ELSE 1 END) AS is_order_printed")
                ->select("((SUM($alias_invoice_actual_budget.meta_value) * COUNT(DISTINCT $alias_invoice_actual_budget.post_id) / COUNT(term.term_id)) + (SUM($alias_invoice_service_fee.meta_value) * COUNT(DISTINCT $alias_invoice_service_fee.post_id) / COUNT(term.term_id))) AS invoice_total")
                ->group_by('is_order_printed')
                ->having('is_order_printed', '1');

            if ($filter_invoice_total) {
                $this->datatable_builder
                    ->having('CAST(invoice_total AS SIGNED) >=', $filter_invoice_total);
            }

            if ($sort_invoice_total) {
                $this->datatable_builder
                    ->order_by('CAST([custom-select].invoice_total AS SIGNED)', $sort_invoice_total);

                unset($args['order_by']['invoice_total']);
            }
        }


        // compare_budget FILTERING & SORTING
        $filter_compare_budget = $args['where']['compare_budget'] ?? FALSE;
        $sort_compare_budget = $args['order_by']['compare_budget'] ?? FALSE;
        if ($filter_compare_budget || $sort_compare_budget) {
            $alias_tp_receipt_payment_actual_budget = uniqid('tp_receipt_payment_actual_budget_');
            $alias_receipt_payment_actual_budget = uniqid('receipt_payment_actual_budget_');
            $alias_actual_budget = uniqid('actual_budget');

            $this->datatable_builder
                ->join("term_posts $alias_tp_receipt_payment_actual_budget", "$alias_tp_receipt_payment_actual_budget.term_id = term.term_id")
                ->join("posts $alias_receipt_payment_actual_budget", "$alias_receipt_payment_actual_budget.post_id = $alias_tp_receipt_payment_actual_budget.post_id AND $alias_receipt_payment_actual_budget.post_type ='receipt_payment'")
                ->join("postmeta $alias_actual_budget", "$alias_actual_budget.post_id = $alias_receipt_payment_actual_budget.post_id AND $alias_actual_budget.meta_key = 'actual_budget'")
                ->select("((SUM($alias_actual_budget.meta_value) * COUNT(DISTINCT $alias_receipt_payment_actual_budget.post_id) / COUNT(term.term_id)) - CAST(SUM(insight_segment_spend.meta_value) AS SIGNED)) AS compare_budget");

            if ($filter_compare_budget) {
                $this->datatable_builder
                    ->having('CAST(compare_budget AS SIGNED) >=', $filter_compare_budget);
            }

            if ($sort_compare_budget) {
                $this->datatable_builder
                    ->order_by('CAST([custom-select].compare_budget AS SIGNED)', $sort_compare_budget);
            }
        }

        // compare_service_fee FILTERING & SORTING
        $filter_compare_service_fee = $args['where']['compare_service_fee'] ?? FALSE;
        $sort_compare_service_fee = $args['order_by']['compare_service_fee'] ?? FALSE;
        if ($filter_compare_service_fee || $sort_compare_service_fee) {
            $alias_tp_receipt_payment_service_fee = uniqid('tp_receipt_payment_service_fee_');
            $alias_receipt_payment_service_fee = uniqid('receipt_payment_service_fee_');
            $alias_service_fee = uniqid('service_fee');
            $alias_service_fee_rate_actual = uniqid('service_fee_rate_actual_');

            $this->datatable_builder
                ->join("termmeta $alias_service_fee_rate_actual", "$alias_service_fee_rate_actual.term_id = term.term_id AND $alias_service_fee_rate_actual.meta_key = 'service_fee_rate_actual'")
                ->join("term_posts $alias_tp_receipt_payment_service_fee", "$alias_tp_receipt_payment_service_fee.term_id = term.term_id")
                ->join("posts $alias_receipt_payment_service_fee", "$alias_receipt_payment_service_fee.post_id = $alias_tp_receipt_payment_service_fee.post_id AND $alias_receipt_payment_service_fee.post_type ='receipt_payment'")
                ->join("postmeta $alias_service_fee", "$alias_service_fee.post_id = $alias_receipt_payment_service_fee.post_id AND $alias_service_fee.meta_key = 'service_fee'")
                ->select("(((SUM($alias_actual_budget.meta_value) * COUNT(DISTINCT $alias_receipt_payment_actual_budget.post_id) / COUNT(term.term_id)) - CAST(SUM(insight_segment_spend.meta_value) AS SIGNED)) + ((SUM($alias_service_fee.meta_value) * COUNT(DISTINCT $alias_receipt_payment_service_fee.post_id) / COUNT(term.term_id)) - ((SUM(CAST(insight_segment_spend.meta_value AS SIGNED)) * (SUM($alias_service_fee_rate_actual.meta_value) * COUNT(DISTINCT $alias_service_fee_rate_actual.meta_id)) / COUNT(term.term_id))))) AS compare_service_fee");

            if ($filter_compare_service_fee) {
                $this->datatable_builder
                    ->having('CAST(compare_service_fee AS SIGNED) >=', $filter_compare_service_fee);
            }

            if ($sort_compare_service_fee) {
                $this->datatable_builder
                    ->order_by('CAST([custom-select].compare_service_fee AS SIGNED)', $sort_compare_service_fee);
            }
        }

        // compare_total FILTERING & SORTING
        $filter_compare_total = $args['where']['compare_total'] ?? FALSE;
        $sort_compare_total = $args['order_by']['compare_total'] ?? FALSE;
        if ($filter_compare_total || $sort_compare_total) {
            $alias_tp_receipt_payment_actual_budget = uniqid('tp_receipt_payment_actual_budget_');
            $alias_receipt_payment_actual_budget = uniqid('receipt_payment_actual_budget_');
            $alias_actual_budget = uniqid('actual_budget');
            $alias_tp_receipt_payment_service_fee = uniqid('tp_receipt_payment_service_fee_');
            $alias_receipt_payment_service_fee = uniqid('receipt_payment_service_fee_');
            $alias_service_fee = uniqid('service_fee');
            $alias_service_fee_rate_actual = uniqid('service_fee_rate_actual_');

            $this->datatable_builder
                ->join("term_posts $alias_tp_receipt_payment_actual_budget", "$alias_tp_receipt_payment_actual_budget.term_id = term.term_id")
                ->join("posts $alias_receipt_payment_actual_budget", "$alias_receipt_payment_actual_budget.post_id = $alias_tp_receipt_payment_actual_budget.post_id AND $alias_receipt_payment_actual_budget.post_type ='receipt_payment'")
                ->join("postmeta $alias_actual_budget", "$alias_actual_budget.post_id = $alias_receipt_payment_actual_budget.post_id AND $alias_actual_budget.meta_key = 'actual_budget'")
                ->join("termmeta $alias_service_fee_rate_actual", "$alias_service_fee_rate_actual.term_id = term.term_id AND $alias_service_fee_rate_actual.meta_key = 'service_fee_rate_actual'")
                ->join("term_posts $alias_tp_receipt_payment_service_fee", "$alias_tp_receipt_payment_service_fee.term_id = term.term_id")
                ->join("posts $alias_receipt_payment_service_fee", "$alias_receipt_payment_service_fee.post_id = $alias_tp_receipt_payment_service_fee.post_id AND $alias_receipt_payment_service_fee.post_type ='receipt_payment'")
                ->join("postmeta $alias_service_fee", "$alias_service_fee.post_id = $alias_receipt_payment_service_fee.post_id AND $alias_service_fee.meta_key = 'service_fee'")
                ->select("(((SUM($alias_actual_budget.meta_value) * COUNT(DISTINCT $alias_receipt_payment_actual_budget.post_id) / COUNT(term.term_id)) - CAST(SUM(insight_segment_spend.meta_value) AS SIGNED)) + (((SUM($alias_actual_budget.meta_value) * COUNT(DISTINCT $alias_receipt_payment_actual_budget.post_id) / COUNT(term.term_id)) - CAST(SUM(insight_segment_spend.meta_value) AS SIGNED)) + ((SUM($alias_service_fee.meta_value) * COUNT(DISTINCT $alias_receipt_payment_service_fee.post_id) / COUNT(term.term_id)) - ((SUM(CAST(insight_segment_spend.meta_value AS SIGNED)) * (SUM($alias_service_fee_rate_actual.meta_value) * COUNT(DISTINCT $alias_service_fee_rate_actual.meta_id)) / COUNT(term.term_id)))))) AS compare_total");

            // $data['compare_total'] = $compare_budget + $compare_service_fee;

            if ($filter_compare_total) {
                $this->datatable_builder
                    ->having('CAST(compare_total AS SIGNED) >=', $filter_compare_total);
            }

            if ($sort_compare_total) {
                $this->datatable_builder
                    ->order_by('CAST([custom-select].compare_total AS SIGNED)', $sort_compare_total);
            }
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function export_synthesis_report($query = '')
    {
        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $contracts = $this->contract_m->query($query)->result();
        if (!$contracts) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $this->load->library('excel');
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array('memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
            ->getProperties()
            ->setCreator("WEBDOCTOR.VN")
            ->setLastModifiedBy("WEBDOCTOR.VN")
            ->setTitle(uniqid('Báo cáo tổng hợp __'));

        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/contract/contract-synthesis-report.xlsx');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $contract_budget_payment_types = $this->config->item('enums', 'contract_budget_payment_types');

        $row_index = 3;

        foreach ($contracts as $key => &$contract) {
            $term_id = $contract->term_id;

            // Prepare data
            // Get customer
            $customer_name = '--';
            $cid = '--';
            $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company', 'system']);
            if (!empty($customers)) {
                $customer = end($customers);
                $customer_name = $customer->display_name;
                $cid = get_user_meta_value($customer->user_id, 'cid') ?? '--';
            }

            // Get contract_code
            $contract_code = get_term_meta_value($term_id, 'contract_code');
            $contract_code = $contract_code ?: '--';

            // Get contract_budget_payment_type
            $contract_budget_payment_type = get_term_meta_value($term_id, 'contract_budget_payment_type');
            $contract_budget_payment_type = $contract_budget_payment_types[$contract_budget_payment_type] ?? '--';

            // Get service_fee_rate_actual
            $service_fee_rate_actual = (float)get_term_meta_value($term_id, 'service_fee_rate_actual');
            $service_fee_rate_actual = $service_fee_rate_actual ?? 0;

            // Get postmeta
            $receipt_payment_actual_budget = 0;
            $receipt_payment_service_fee = 0;
            $receipt_payment_fct_amount = 0;
            $receipt_payment_vat_amount = 0;
            $receipt_payment_amount = 0;

            $invoice_actual_budget = 0;
            $invoice_service_fee = 0;

            $posts = $this->term_posts_m->get_term_posts($term_id, 'receipt_payment');
            if (!empty($posts)) {
                foreach ($posts as $post) {
                    $post_id = $post->post_id;

                    $is_order_printed = get_post_meta_value($post_id, 'is_order_printed');

                    $actual_budget = (int)get_post_meta_value($post_id, 'actual_budget');
                    $receipt_payment_actual_budget += $actual_budget;
                    $is_order_printed && $invoice_actual_budget += $actual_budget;

                    $service_fee = (int)get_post_meta_value($post_id, 'service_fee');
                    $receipt_payment_service_fee += $service_fee;
                    $is_order_printed && $invoice_service_fee += $service_fee;

                    $fct_amount = (int)get_post_meta_value($post_id, 'fct_amount');
                    $receipt_payment_fct_amount += $fct_amount;

                    $vat_amount = (int)get_post_meta_value($post_id, 'vat_amount');
                    $receipt_payment_vat_amount += $vat_amount;

                    $amount = (int)get_post_meta_value($post_id, 'amount');
                    $receipt_payment_amount += $amount;
                }
            }

            $receipt_payment_actual_budget = $receipt_payment_actual_budget;
            $receipt_payment_service_fee = $receipt_payment_service_fee;
            $receipt_payment_fct_amount = $receipt_payment_fct_amount;
            $receipt_payment_vat_amount = $receipt_payment_vat_amount;
            $receipt_payment_amount = $receipt_payment_amount;

            $invoice_actual_budget = $invoice_actual_budget;
            $invoice_service_fee = $invoice_service_fee;
            $invoice_total = $invoice_actual_budget + $invoice_service_fee;

            // Get spent
            $spend = (int)$contract->spend;
            $actual_result = $spend ?: 0;
            $service_fee_spent = $actual_result ? $actual_result * $service_fee_rate_actual : 0;
            $service_fee_spent = $service_fee_spent;

            // Get Compare
            $compare_budget = $actual_result - $receipt_payment_actual_budget;
            $compare_budget = $compare_budget;
            $compare_service_fee = $service_fee_spent - $receipt_payment_service_fee;
            $compare_service_fee = $compare_service_fee;

            $compare_total = $compare_budget + $compare_service_fee;

            $row_number = $row_index + $key;

            $i = 0;

            // Set Cell
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $key + 1);

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (($cid != '--') ? "{$cid} - {$customer_name}" : $customer_name));

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_code);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_budget_payment_type);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $service_fee_rate_actual);

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $receipt_payment_actual_budget);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $receipt_payment_service_fee);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $receipt_payment_fct_amount);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $receipt_payment_vat_amount);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $receipt_payment_amount);

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $actual_result);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $service_fee_spent);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, 0);

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $invoice_actual_budget);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $invoice_service_fee);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $invoice_total);

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, 0);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, 0);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, 0);

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $compare_budget);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $compare_service_fee);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $compare_total);

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, 0);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, 0);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, 0);
        }

        $numbers_of_contract = count($contracts);

        // // Set Cells style for Date
        // $objPHPExcel->getActiveSheet()
        //     ->getStyle('A' . $row_index . ':A' . ($numbers_of_contract + $row_index))
        //     ->getNumberFormat()
        //     ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY); // Set Cells style for Date
        // $objPHPExcel->getActiveSheet()
        //     ->getStyle('A' . $row_index . ':A' . ($numbers_of_contract + $row_index))
        //     ->getNumberFormat()
        //     ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        // Set Cells Style Percentage
        $objPHPExcel->getActiveSheet()
            ->getStyle('E' . $row_index . ':E' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);

        // Set Cells Style Currency
        $objPHPExcel->getActiveSheet()
            ->getStyle('F' . $row_index . ':F' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('G' . $row_index . ':G' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('H' . $row_index . ':H' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('I' . $row_index . ':I' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('J' . $row_index . ':J' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('K' . $row_index . ':K' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('L' . $row_index . ':L' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('M' . $row_index . ':M' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('N' . $row_index . ':N' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('O' . $row_index . ':O' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('P' . $row_index . ':P' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('Q' . $row_index . ':Q' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('R' . $row_index . ':R' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('S' . $row_index . ':S' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('T' . $row_index . ':T' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('U' . $row_index . ':U' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('V' . $row_index . ':V' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('W' . $row_index . ':W' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('X' . $row_index . ':X' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");
        $objPHPExcel->getActiveSheet()
            ->getStyle('Y' . $row_index . ':Y' . ($numbers_of_contract + $row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $file_name = "tmp/export-bao-cao-tong-hop-hop-dong.xlsx";
        $objWriter->save($file_name);

        try {
            $objWriter->save($file_name);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        if (file_exists($file_name)) {
            $this->load->helper('download');
            force_download($file_name, NULL);
        }

        return FALSE;
    }
}
/* End of file DatasetSynthesisReport.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetSynthesisReport.php */