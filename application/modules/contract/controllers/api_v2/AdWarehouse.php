<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'vendor/autoload.php';

use AdsService\AdWarehouse\AdWarehouseBusinessManager;

class AdWarehouse extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'ad_business_manager_m';
        $this->autoload['models'][] = 'ad_account_m';
        $this->autoload['models'][] = 'business_manager_m';
        $this->autoload['models'][] = 'ad_business_managers_ad_accounts_m';
        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'contract/category_m';
        $this->autoload['models'][] = 'contract/term_categories_m';
        $this->autoload['models'][] = 'facebookads/fbaccount_m';

        parent::__construct($config);
    }
    
    /**
     * business_manager_by_id_get
     *
     * @param  int|string $business_manager_id
     * @return void
     */
    public function business_manager_by_id_get($business_manager_id)
    {
        if(!has_permission('admin.adwarehouse.access')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', parent::HTTP_FORBIDDEN);

        $business_manager = $this->ad_business_manager_m->as_array()->get_by(['id' => $business_manager_id]);
        if(empty($business_manager)) return parent::responseHandler([], 'Dữ liệu không tồn tại.', 'error', parent::HTTP_NO_CONTENT);
        
        $business_manager['system_users'] = [];
        $original_term_id = $business_manager['original_term_id'];
        if(!empty($original_term_id)){
            $system_users = $this->term_users_m->as_array()->get_term_users($original_term_id, $this->fbaccount_m->type);
            $business_manager['system_users'] = $system_users;
        }

        return parent::responseHandler($business_manager, 'Lấy dữ liệu thành công.');
    }
    
    /**
     * system_users_get
     *
     * @return void
     */
    public function system_users_get(){
        if(!has_permission('admin.adwarehouse.access')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', parent::HTTP_FORBIDDEN);

        $system_users = $this->fbaccount_m->set_type()->limit(100)->order_by('user_id', 'desc')->as_array()->get_all();

        return parent::responseHandler($system_users, 'Lấy dữ liệu thành công.');
    }
    
    /**
     * update_system_users_put
     *
     * @param  int|string $business_manager_id
     * @return void
     */
    public function update_business_manager_system_users_put($business_manager_id){
        if(!has_permission('admin.adwarehouse.manage')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', parent::HTTP_FORBIDDEN);

        $args = wp_parse_args(parent::put(null, true), ['business_manager_id' => $business_manager_id]);
        $this->load->library('form_validation');
        $this->form_validation->set_data($args);

        $this->form_validation->set_rules('business_manager_id', 'business_manager_id', [
            'integer', 
            ['is_business_manager_exist', function($value){
                $is_exist = $this->ad_business_manager_m->as_array()->count_by(['id' => $value]);
                if (!$is_exist) {
                    $this->form_validation->set_message('is_business_manager_exist', 'Business manager không có trên hệ thống');
                    return FALSE;
                }

                return TRUE;
            }],
            ['is_original_term_id_exist', function($value){
                $business_manager = $this->ad_business_manager_m->as_array()->get_by(['id' => $value]);
                if(empty($business_manager['original_term_id'])){
                    $this->form_validation->set_message('is_original_term_id_exist', 'Business manager sai dữ liệu');
                    return FALSE;
                }

                return TRUE;
            }],
        ]);
        
        $this->form_validation->set_rules('system_users[]', 'system_users', 'array');
        $system_users = $args['system_users'] ?? [];
        array_walk($system_users, function($system_user, $index){
            $this->form_validation->set_rules( "system_users[{$index}][user_id]", null, [ 'integer', [ 'is_system_user_exist', function ($value) {
                $is_exist = $this->fbaccount_m->set_type()->count_by('user_id', $value) > 0;
                if (!$is_exist) {
                    $this->form_validation->set_message('is_system_user_exist', 'Tài khoản không có trên hệ thống');
                    return FALSE;
                }
            
                return TRUE;
            }]]);
        });

        if( FALSE === $this->form_validation->run())
        {
            return parent::responseHandler($this->form_validation->error_array(), 'Dữ liệu không hợp lệ', 'error', parent::HTTP_BAD_REQUEST);
        }

        $business_manager = $this->ad_business_manager_m->as_array()->get_by(['id' => $business_manager_id]);
        $original_term_id = $business_manager['original_term_id'];

        $system_user_ids = array_column($system_users, 'user_id');
        if(empty($system_user_ids)){
            $is_success = $this->term_users_m->set_term_users($original_term_id, [], $this->fbaccount_m->type, ['command' => 'remove']);
        }
        else {
            $is_success = $this->term_users_m->set_term_users($original_term_id, $system_user_ids, $this->fbaccount_m->type, ['command' => 'replace']);
        }

        if(!$is_success){
            return parent::responseHandler([], 'Có lỗi trong quá trình cập nhật', 'error', parent::HTTP_BAD_REQUEST);
        }

        return parent::responseHandler($business_manager, 'Cập nhật thành công');
    }
    
    /**
     * industries_get
     *
     * @return void
     */
    public function industries_get()
    {
        if(!has_permission('admin.adwarehouse.access')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', parent::HTTP_FORBIDDEN);

        $industries = $this->category_m->set_term_type()->as_array()->get_all();
        if(empty($industries)) return parent::responseHandler([], 'Dữ liệu không tồn tại.', 'error', parent::HTTP_NO_CONTENT);

        return parent::responseHandler($industries, 'Lấy dữ liệu thành công.');
    }

    /**
     * industries_post
     *
     * @return void
     */
    public function industries_post()
    {
        if(!has_permission('admin.adwarehouse.manage')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', parent::HTTP_FORBIDDEN);

        $args = wp_parse_args(parent::post(null, true), []);
        $this->load->library('form_validation');
        $this->form_validation->set_data($args);
        $this->form_validation->set_rules('name', 'name', ['required']);
        if( FALSE === $this->form_validation->run())
        {
            return parent::responseHandler($this->form_validation->error_array(), 'Dữ liệu không hợp lệ', 'error', parent::HTTP_BAD_REQUEST);
        }

        $industry_id = $this->category_m->add(['term_name' => $args['name']]);
        if(empty($industry_id)){
            return parent::responseHandler($args, 'Tạo mới industry thất bại', 'error', parent::HTTP_BAD_REQUEST);
        }

        $industry = $this->category_m->set_term_type()->get_by(['term_id' => $industry_id]);
        return parent::responseHandler($industry, 'Lấy dữ liệu thành công.');
    }

    /**
     * business_manager_get
     *
     * @return void
     */
    public function business_manager_get()
    {
        if(!has_permission('admin.adwarehouse.access')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', parent::HTTP_FORBIDDEN);

        $business_manager = $this->ad_business_manager_m->limit(100)->as_array()->get_all();
        if(empty($business_manager)) return parent::responseHandler([], 'Dữ liệu không tồn tại.', 'error', parent::HTTP_NO_CONTENT);

        return parent::responseHandler($business_manager, 'Lấy dữ liệu thành công.');
    }

    /**
     * ad_account_get
     *
     * @param  int|string $ad_account_id
     * @return void
     */
    public function ad_account_get($ad_account_id)
    {
        if(!has_permission('admin.adwarehouse.access')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', parent::HTTP_FORBIDDEN);

        $ad_account = $this->ad_account_m->as_array()->get_by(['id' => $ad_account_id]);
        if(empty($ad_account)) return parent::responseHandler([], 'Dữ liệu không tồn tại.', 'error', parent::HTTP_NO_CONTENT);
        
        $ad_account['ad_business_manager'] = $this->ad_account_m->get_ad_business_manager($ad_account_id);
        $ad_account['industries'] = $this->ad_account_m->get_industries($ad_account_id);

        return parent::responseHandler($ad_account, 'Lấy dữ liệu thành công.');
    }
    
    /**
     * update_ad_account_put
     *
     * @param  int|string $ad_account_id
     * @return void
     */
    public function update_ad_account_put($ad_account_id){
        if(!has_permission('admin.adwarehouse.manage')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', parent::HTTP_FORBIDDEN);

        $args = wp_parse_args(parent::put(null, true), ['ad_account_id' => $ad_account_id]);
        $this->load->library('form_validation');
        $this->form_validation->set_data($args);

        $this->form_validation->set_rules('ad_account_id', 'ad_account_id', [
            'integer', 
            ['is_ad_account_exist', function($value){
                $is_exist = $this->ad_account_m->as_array()->count_by(['id' => $value]);
                if (!$is_exist) {
                    $this->form_validation->set_message('is_ad_account_exist', 'Ad account không có trên hệ thống');
                    return FALSE;
                }

                return TRUE;
            }],
            ['is_original_term_id_exist', function($value){
                $ad_account = $this->ad_account_m->as_array()->get_by(['id' => $value]);
                if(empty($ad_account['original_term_id'])){
                    $this->form_validation->set_message('is_original_term_id_exist', 'Ad account sai dữ liệu');
                    return FALSE;
                }

                return TRUE;
            }],
        ]);
        
        // $this->form_validation->set_rules('ad_business_manager[id]', 'ad_business_manager', [
        //     'required',
        //     'integer',
        //     ['is_business_manager_exist', function($value){
        //         $is_exist = $this->ad_business_manager_m->as_array()->count_by(['id' => $value]);
        //         if (!$is_exist) {
        //             $this->form_validation->set_message('is_business_manager_exist', 'Business manager không có trên hệ thống');
        //             return FALSE;
        //         }

        //         return TRUE;
        //     }],
        // ]);

        $this->form_validation->set_rules('industries[]', 'industries', 'array');
        $industries = $args['industries'] ?? [];
        array_walk($industries, function($industry, $index){
            $this->form_validation->set_rules( "industries[{$index}][term_id]", null, [ 'integer', [ 'is_industry_exist', function ($value) {
                $is_exist = $this->category_m->set_term_type()->count_by('term_id', $value) > 0;
                if (!$is_exist) {
                    $this->form_validation->set_message('is_industry_exist', 'Industry không có trên hệ thống');
                    return FALSE;
                }
            
                return TRUE;
            }]]);
        });

        if( FALSE === $this->form_validation->run())
        {
            return parent::responseHandler($this->form_validation->error_array(), 'Dữ liệu không hợp lệ', 'error', parent::HTTP_BAD_REQUEST);
        }

        $ad_account = $this->ad_account_m->as_array()->get_by(['id' => $ad_account_id]);
        
        $original_term_id = $ad_account['original_term_id'];
        $industry_ids = array_column($args['industries'], 'term_id');
        if(empty($industry_ids)){
            $this->term_categories_m->clear_relations($original_term_id);
        }
        else{
            $this->term_categories_m->set_relations($original_term_id, $industry_ids, [], TRUE);
        }

        $resposne = $ad_account;
        $resposne['industries'] = $args['industries'];
        $resposne['ad_business_manager'] = $args['ad_business_manager'];

        return parent::responseHandler($resposne, 'Cập nhật thành công');
    }

    /**
     * get_bm_by_system_user_id_get
     *
     * @param  int|string $ad_account_id
     * @return void
     */
    public function get_bm_by_system_user_id_get(){
        if(!has_permission('admin.adwarehouse.manage')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', parent::HTTP_FORBIDDEN);

        $args = wp_parse_args(parent::get(null, true), ['system_user_id' => null]);
        
        $this->load->library('form_validation');
        $this->form_validation->set_data($args);
        
        $this->form_validation->set_rules("system_user_id", "system_user_id", [ 'required', [ 'is_system_user_exist', function ($value) {
            $is_exist = $this->fbaccount_m->set_type()->count_by('user_id', $value) > 0;
            if (!$is_exist) {
                $this->form_validation->set_message('is_system_user_exist', 'System user không có trên hệ thống');
                return FALSE;
            }
        
            return TRUE;
        }]]);

        if( FALSE === $this->form_validation->run())
        {
            return parent::responseHandler($this->form_validation->error_array(), 'Dữ liệu không hợp lệ', 'error', parent::HTTP_BAD_REQUEST);
        }

        $system_user_id = $args['system_user_id'] ?? null;
        $response = (new AdWarehouseBusinessManager())->fetch_bm($system_user_id);

        if(FALSE === $response)
        {
            return parent::responseHandler([], 'Có lỗi xảy ra trong quá trình lấy dữ liệu.', 'error', parent::HTTP_FAILED_DEPENDENCY);
        }

        if(200 != $response['code']){
            return parent::responseHandler([], $response['message'], 'error', parent::HTTP_EXPECTATION_FAILED);
        }

        $business_managers = $response['data']['businesManagers'] ?? [];
        return parent::responseHandler($business_managers, 'Xử lý thành công');
    }
    
    /**
     * save_bm_post
     *
     * @return void
     */
    public function save_bm_post(){
        if(!has_permission('admin.adwarehouse.manage')) return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', parent::HTTP_FORBIDDEN);

        $args = wp_parse_args(parent::post(null, true), []);
        
        $system_user_id = $args['system_user_id'];
        $business_manager = (new AdWarehouseBusinessManager())->store_bm($system_user_id, $args);
        if(FALSE === $business_manager)
        {
            return parent::responseHandler([], "Có lỗi xảy ra trong quá trình ghi nhận BM [{$args['business_id']}].", 'error', parent::HTTP_FAILED_DEPENDENCY);
        }

        return parent::response($business_manager, parent::HTTP_OK);
    }
}
/* End of file AdWarehouse.php */
/* Location: ./application/modules/contract/controllers/api_v2/AdWarehouse.php */