<?php defined('BASEPATH') or exit('No direct script access allowed');

class Resource extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['models'][] = 'log_m';
        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'balance_spend_m';
        
        parent::__construct();
    }

    public function is_running($id)
    {
        $contract_m = new contract_m();
        $contract_m->set_contract($id);

        parent::response([
            'status' => TRUE,
            'data' => (bool) $contract_m->is_running()
        ]);
    }

    public function has_ended($id)
    {
        $contract_m = new contract_m();
        $contract_m->set_contract($id);

        parent::response([
            'status' => TRUE,
            'data' => (bool) $contract_m->has_ended()
        ]);
    }

    public function start_service_put($term_id)
    {
        if( ! has_permission('Contract.Start_service.Manage'))
        {
            return parent::responseHandler(null, 'Quyền hạn bị hạn chế ! Vui lòng liên hệ quản trị viên để nâng quyền .', 'error', 401);
        }

        if (!$this->contract_m->set_contract($term_id)) {
            return parent::responseHandler(null, 'Không tìm thấy hợp đồng.', 'error', 400);
        };

        $contract = $this->contract_m->set_term_type()->get_contract();
        if ($contract->term_status != 'waitingforapprove') {
            return parent::responseHandler(null, 'Hợp đồng chưa được cấp số hoặc đang thực hiện.', 'error', 400);
        }

        $payment_amount = (int)get_term_meta_value($term_id, 'payment_amount');
        if ($payment_amount <= 0) {
            return parent::responseHandler(null, 'Hợp đồng chưa được thanh toán.', 'error', 400);
        }

        $this->contract_m->update($term_id, array('term_status' => 'publish'));
        update_term_meta($term_id, 'started_service', time());

        /* Log started_service action*/
        $this->log_m->insert(array(
            'log_type' => 'started_service',
            'user_id' => $this->admin_m->id,
            'term_id' => $term_id,
            'log_content' => date('Y/m/d H:i:s')
        ));

        $model = $this->contract_m->get_contract_model($contract);
        if (!$model) return parent::responseHandler(null, 'Hợp đồng khởi chạy thành công.');

        // Xử lý các tác vụ ngầm sau khi "Chạy dịch vụ" tùy từng dịch vụ như gửi mail kết nối nội bộ ,...
        if (is_string($model)) {
            $this->load->model($model, 'tmp_contract_m');
            if (!method_exists($this->tmp_contract_m, 'start_service')) return parent::responseHandler(null, 'Hợp đồng khởi chạy thành công.');
            $this->tmp_contract_m->start_service($contract);
        } else {
            $model->start_service();
        }

        $this->load->model('contract/common_report_m');
        try /* Email truyền thông chúc mừng nhân viên kinh doanh đã ký được hợp đồng */ {
            $this->common_report_m->init($contract)->send_new_contract_congratulation_email();
        } catch (Exception $e) {
            return parent::responseHandler(null, $e->getMessage(), 'error', 400);
        }

        /* truyền thông chúc mừng nhân viên kinh doanh đã ký được hợp đồng */
        try {
            $this->contract_m->set_contract($term_id);
            $this->contract_m->mutateIsRenewal();

            $this->common_report_m->init($contract)->postSlackNewContractMessage();
        } catch (Exception $e) {
            return parent::responseHandler(null, $e->getMessage(), 'error', 400);
        }

        return parent::responseHandler(null, 'Hợp đồng khởi chạy thành công.');
    }

    public function stop_service_put($term_id)
    {
        if( ! has_permission('Contract.Liquidation.Manage'))
        {
            return parent::responseHandler(null, 'Quyền hạn bị hạn chế ! Vui lòng liên hệ quản trị viên để nâng quyền .', 'error', 401);
        }

        if (!$this->contract_m->set_contract($term_id)) {
            return parent::responseHandler(null, 'Không tìm thấy hợp đồng.', 'error', 400);
        };

        $contract = $this->contract_m->set_term_type()->get_contract();
        if (in_array($contract->term_status, ['ending', 'liquidation'])) {
            return parent::responseHandler(null, 'Hợp đồng đã thanh lý hoặc đã kết thúc.', 'error', 400);
        }

        $model = $this->contract_m->get_contract_model($contract);
        if(!empty($model)){
			$this->load->model($model,'tmp_contract_m');
			if(method_exists($this->tmp_contract_m, 'stop_service')){
				$this->tmp_contract_m->stop_service($contract);

                return parent::responseHandler(null, 'Hợp đồng thanh lý thành công.');
            }
		}

		$this->contract_m->update($term_id, array('term_status' => 'liquidation'));
		$time = time();
		update_term_meta($term_id,'end_contract_time',$time);
		$this->log_m->insert(array('log_type' =>'end_contract_time','user_id' => $this->admin_m->id,'term_id' => $term_id,'log_content' => my_date($time,'Y/m/d H:i:s')));

        return parent::responseHandler(null, 'Hợp đồng thanh lý thành công.');
    }

    // stop_service

    /**
     * Create new Insight Record
     *
     * @param      int   $id     The identifier
     */
    public function balance_spend_post()
    {
        $user_id_allow_change_balance_spend = $this->config->item('user_id_allow_change_balance_spend');
        if(!in_array($this->admin_m->id, $user_id_allow_change_balance_spend)){
            parent::responseHandler(null, 'Không có quyền tạo cân bằng thủ công', 'error', 401);
        }

        $args = wp_parse_args(parent::post(null, true), [ 'post_author' => $this->admin_m->id ]);
        $rules = array(
            'contractId' => [
                'field' => 'contractId',
                'label' => 'Mã hợp đồng',
                'rules' => [
                    'required',
                    'numeric',
                    array('contract_id_check', array($this->balance_spend_m, 'contract_id_check'))
                ]
            ],
            'time' => [
                'field' => 'time',
                'label' => 'time',
                'rules' => 'required|numeric'
            ],
            'spend' => [
                'field' => 'spend',
                'label' => 'spend',
                'rules' => 'required|numeric'
            ],
        );

        $this->load->library('form_validation');
        $this->form_validation->set_data($args);
        $this->form_validation->set_rules($rules);

        if( FALSE === $this->form_validation->run())
        {
            parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                  && FALSE == (bool)get_term_meta_value($args['contractId'], 'is_manipulation_locked');
        if($is_manipulation_locked)
        {
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
            $time = end_of_day($args['time']);
            if($time < $manipulation_locked_at)
            {
                parent::response([
                    'code' => 400,
                    'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                ]);
            }
        }

        ! empty($args['time']) AND $args['time'] = start_of_day($args['time']);

        $insert_data = [
            'post_name'     => 'day',
            'post_type'     => $this->balance_spend_m->post_type,
            'start_date'    => start_of_day($args['time']),
            'end_date'      => start_of_day($args['time']),
            'post_content'  => $args['spend'],
            'post_author'   => $this->admin_m->id,
            'post_excerpt'  => $args['note'] ?? '',
            'comment_status' => 'direct'
        ];

        $insertedId = $this->balance_spend_m->insert($insert_data);
        if(empty($insertedId))
        {
            parent::responseHandler(null, 'Quá trình xử lý không thành công, Vui lòng thử lại !', 'error', parent::HTTP_NOT_IMPLEMENTED);
        }

        $this->term_posts_m->set_term_posts($args['contractId'], [$insertedId], $this->balance_spend_m->post_type);

        try
        {
            $this->contract_m->set_contract($args['contractId']);
            $this->contract_m->sync_all_amount();
        }
        catch (Exception $e)
        {
            parent::responseHandler(null, 'Quá trình xử lý không thành công - 500, Vui lòng thử lại !', 'error', 500);
        }

        parent::responseHandler($insertedId, "#{$insertedId} đã được thêm mới thành công");
    }

    /**
     * Create new Insight Record
     *
     * @param      int   $id     The identifier
     */
    public function balance_spend_put($id)
    {
        $user_id_allow_change_balance_spend = $this->config->item('user_id_allow_change_balance_spend');
        if(!in_array($this->admin_m->id, $user_id_allow_change_balance_spend)){
            parent::responseHandler(null, 'Không có quyền cập nhật cân bằng thủ công', 'error', 401);
        }

        $args = wp_parse_args(parent::put(null, true), [
            'post_id' => $id,
            'post_author' => $this->admin_m->id
        ]);

        $rules = array(
            'post_id' => [
                'field' => 'post_id',
                'label' => 'ID phiếu',
                'rules' => [
                    'required',
                    'numeric',
                    array('id_check', array($this->balance_spend_m, 'id_check')),
                    array('editable_mode_check', array($this->balance_spend_m, 'editable_mode_check'))
                ]
            ],
            'contractId' => [
                'field' => 'contractId',
                'label' => 'Mã hợp đồng',
                'rules' => [
                    'required',
                    'numeric',
                    array('contract_id_check', array($this->balance_spend_m, 'contract_id_check'))
                ]
            ],
            'time' => [
                'field' => 'time',
                'label' => 'time',
                'rules' => 'required|numeric'
            ],
            'spend' => [
                'field' => 'spend',
                'label' => 'spend',
                'rules' => 'required|numeric'
            ]
        );

        $this->load->library('form_validation');
        $this->form_validation->set_data($args);
        $this->form_validation->set_rules($rules);

        if( FALSE === $this->form_validation->run())
        {
            parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                  && FALSE == (bool)get_term_meta_value($args['contractId'], 'is_manipulation_locked');
        if($is_manipulation_locked)
        {
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
            
            $balance_spend = $this->balance_spend_m->set_post_type()->as_array()->get_by(['post_id' => $id]);
            $end_date = end_of_day($balance_spend['end_date']);
            $time = end_of_day($args['time']);
            if( $end_date < $manipulation_locked_at
                && ($balance_spend['post_content'] != $args['spend']
                    || $time != $end_date
                )
            )
            {
                parent::response([
                    'code' => 400,
                    'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                ]);
            }
        }

        ! empty($args['time']) AND $args['time'] = start_of_day($args['time']);

        $data_changes = [
            'start_date'    => start_of_day($args['time']),
            'end_date'      => start_of_day($args['time']),
            'post_content'  => $args['spend'],
            'post_excerpt'  => $args['note'] ?? '',
            'post_author'   => $this->admin_m->id,
        ];

        $status = $this->balance_spend_m->set_post_type()->update($id, $data_changes);

        try
        {
            $this->contract_m->set_contract($args['contractId']);
            $this->contract_m->sync_all_amount();
        }
        catch (Exception $e)
        {
            parent::responseHandler(null, 'Quá trình xử lý không thành công - 500, Vui lòng thử lại !', 'error', 500);
        }

        parent::responseHandler($id, "#{$id} đã được cập nhật thành công");
    }

    /**
     * Delete the Insight Record
     *
     * @param      int   $id     The identifier
     */
    public function balance_spend_delete($contractId = 0, int $id = 0)
    {
        $user_id_allow_change_balance_spend = $this->config->item('user_id_allow_change_balance_spend');
        if(!in_array($this->admin_m->id, $user_id_allow_change_balance_spend)){
            parent::responseHandler(null, 'Không có quyền xoá cân bằng thủ công', 'error', 401);
        }

        $args = [
            'post_id' => $id,
            'contractId' => $contractId,
            'post_author' => $this->admin_m->id
        ];

        $rules = array(
            'post_id' => [
                'field' => 'post_id',
                'label' => 'ID phiếu',
                'rules' => [
                    'required',
                    'numeric',
                    array('id_check', array($this->balance_spend_m, 'id_check')),
                    array('editable_mode_check', array($this->balance_spend_m, 'editable_mode_check'))
                ]
            ],
            'contractId' => [
                'field' => 'contractId',
                'label' => 'Mã hợp đồng',
                'rules' => [
                    'required',
                    'numeric',
                    array('contract_id_check', array($this->balance_spend_m, 'contract_id_check'))
                ]
            ]
        );

        $this->load->library('form_validation');
        $this->form_validation->set_data($args);
        $this->form_validation->set_rules($rules);

        if( FALSE === $this->form_validation->run())
        {
            parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                  && FALSE == (bool)get_term_meta_value($args['contractId'], 'is_manipulation_locked');
        if($is_manipulation_locked)
        {
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
            
            $balance_spend = $this->balance_spend_m->set_post_type()->as_array()->get_by(['post_id' => $id]);
            $time = end_of_day($balance_spend['end_date']);
            if($time < $manipulation_locked_at)
            {
                parent::response([
                    'code' => 400,
                    'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                ]);
            }
        }

        $this->balance_spend_m->delete($id);

        try
        {
            $this->contract_m->set_contract($args['contractId']);
            $this->contract_m->sync_all_amount();
        }
        catch (Exception $e)
        {
            parent::responseHandler(null, 'Quá trình xử lý không thành công - 500, Vui lòng thử lại !', 'error', 500);
        }

        parent::responseHandler($id, "#{$id} đã được xóa thành công");
    }
}
/* End of file Resource.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Resource.php */