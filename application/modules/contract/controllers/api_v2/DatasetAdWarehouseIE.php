<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Enums\DatatableAdWarehouseSearchFields;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class DatasetAdWarehouseIE extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'fact_warehouse_exchange_m';
        
        parent::__construct();
        
        $this->config->load('contract/ad_warehouse_fields');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data'=>[]);
        if( ! has_permission('admin.adwarehouse.access'))
        {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', "error", 401);
        }


        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args( parent::get(), $defaults);

        $args['columns'] = $this->config->item('default_columns', 'datasource');
        $columns = $this->config->item('datasource');

        if( ! empty($args['columns']))
        {
            foreach ($args['columns'] as $key)
            {
                $this->datatable_builder->add_column($columns['columns'][$key]['name'], $columns['columns'][$key]);
            }    
        }

        $data = $this->data;
        $this->filtering();

        $data['content'] = $this->datatable_builder
        ->set_filter_position(FILTER_TOP_INNER)
        ->setOutputFormat('JSON')

        ->select('fact_warehouse_exchange.id as fact_warehouse_exchange_id')
        ->add_search('fact_warehouse_exchange_id')
        ->add_search('fact_warehouse_exchange_begin_balance')
        ->add_search('fact_warehouse_exchange_import')
        ->add_search('fact_warehouse_exchange_export_use')
        ->add_search('fact_warehouse_exchange_export_suspend')
        ->add_search('fact_warehouse_exchange_balance')
        ->add_search('fact_warehouse_exchange_time_id')
        ->add_search('fact_warehouse_exchange_ad_warehouse_id')
        ->add_search('fact_warehouse_exchange_ad_bm_id')
        ->add_search('fact_warehouse_exchange_ad_account_id')
        ->add_search('fact_warehouse_exchange_contract_id')
        ->add_search('ad_warehouse_id')
        ->add_search('ad_warehouse_name')
        ->add_search('ad_warehouse_status')
        ->add_search('ad_warehouse_quantity')
        ->add_search('ad_warehouse_created_at')
        ->add_search('ad_warehouse_updated_at')
        ->add_search('ad_warehouse_bm_id')
        ->add_search('ad_warehouse_original_bm_term_id')
        ->add_search('ad_business_manager_id')
        ->add_search('ad_business_manager_name')
        ->add_search('ad_business_manager_created_by')
        ->add_search('ad_business_manager_created_time')
        ->add_search('ad_business_manager_extended_updated_time')
        ->add_search('ad_business_manager_is_hidden')
        ->add_search('ad_business_manager_verification_status')
        ->add_search('ad_business_manager_payment_account_id')
        ->add_search('ad_business_manager_timezone_id')
        ->add_search('ad_business_manager_created_at')
        ->add_search('ad_business_manager_updated_at')
        ->add_search('ad_business_manager_original_term_id')
        ->add_search('ad_business_manager_business_id')
        ->add_search('ad_account_id')
        ->add_search('ad_account_created_at')
        ->add_search('ad_account_updated_at')
        ->add_search('ad_account_name')
        ->add_search('ad_account_account_id')
        ->add_search('ad_account_status')
        ->add_search('ad_account_amount_spent')
        ->add_search('ad_account_age')
        ->add_search('ad_account_created_time')
        ->add_search('ad_account_stock_status')
        ->add_search('ad_account_type_of_ownership')
        ->add_search('ad_account_creation_type')
        ->add_search('ad_account_currency')
        ->add_search('ad_account_funding_source')
        ->add_search('ad_account_funding_source_details')
        ->add_search('ad_account_spend_cap')
        ->add_search('ad_account_min_daily_budget')
        ->add_search('ad_account_balance')
        ->add_search('ad_account_disable_reason')
        ->add_search('ad_account_original_term_id')
        ->add_search('ad_account_bank_id')

        ->select('fact_warehouse_exchange.id as fact_warehouse_exchange_id')

        ->select('MIN(fact_warehouse_exchange.begin_balance) as fact_warehouse_exchange_begin_balance')
        ->select('SUM(fact_warehouse_exchange.import) AS fact_warehouse_exchange_import')
        ->select('SUM(fact_warehouse_exchange.export_use) AS fact_warehouse_exchange_export_use')
        ->select('SUM(fact_warehouse_exchange.export_suspend) AS fact_warehouse_exchange_export_suspend')
        ->select('(MIN(fact_warehouse_exchange.begin_balance) + SUM( fact_warehouse_exchange.import ) - SUM( fact_warehouse_exchange.export_use )) AS fact_warehouse_exchange_balance')

        ->select('fact_warehouse_exchange.time_id as fact_warehouse_exchange_time_id')
        ->select('fact_warehouse_exchange.ad_warehouse_id as fact_warehouse_exchange_ad_warehouse_id')
        ->select('fact_warehouse_exchange.ad_bm_id as fact_warehouse_exchange_ad_bm_id')
        ->select('fact_warehouse_exchange.ad_account_id as fact_warehouse_exchange_ad_account_id')
        ->select('fact_warehouse_exchange.contract_id as fact_warehouse_exchange_contract_id')
        ->select('ad_warehouse.id as ad_warehouse_id')
        ->select('ad_warehouse.name as ad_warehouse_name')
        ->select('ad_warehouse.status as ad_warehouse_status')
        ->select('ad_warehouse.quantity as ad_warehouse_quantity')
        ->select('ad_warehouse.created_at as ad_warehouse_created_at')
        ->select('ad_warehouse.updated_at as ad_warehouse_updated_at')
        ->select('ad_warehouse.bm_id as ad_warehouse_bm_id')
        ->select('ad_warehouse.original_bm_term_id as ad_warehouse_original_bm_term_id')
        ->select('ad_business_manager.id as ad_business_manager_id')
        ->select('ad_business_manager.name as ad_business_manager_name')
        ->select('ad_business_manager.created_by as ad_business_manager_created_by')
        ->select('ad_business_manager.created_time as ad_business_manager_created_time')
        ->select('ad_business_manager.extended_updated_time as ad_business_manager_extended_updated_time')
        ->select('ad_business_manager.is_hidden as ad_business_manager_is_hidden')
        ->select('ad_business_manager.verification_status as ad_business_manager_verification_status')
        ->select('ad_business_manager.payment_account_id as ad_business_manager_payment_account_id')
        ->select('ad_business_manager.timezone_id as ad_business_manager_timezone_id')
        ->select('ad_business_manager.created_at as ad_business_manager_created_at')
        ->select('ad_business_manager.updated_at as ad_business_manager_updated_at')
        ->select('ad_business_manager.original_term_id as ad_business_manager_original_term_id')
        ->select('ad_business_manager.business_id as ad_business_manager_business_id')
        ->select('ad_account.id as ad_account_id')
        ->select('ad_account.created_at as ad_account_created_at')
        ->select('ad_account.updated_at as ad_account_updated_at')
        ->select('ad_account.name as ad_account_name')
        ->select('ad_account.account_id as ad_account_account_id')
        ->select('ad_account.status as ad_account_status')
        ->select('ad_account.amount_spent as ad_account_amount_spent')
        ->select('ad_account.age as ad_account_age')
        ->select('ad_account.created_time as ad_account_created_time')
        ->select('ad_account.stock_status as ad_account_stock_status')
        ->select('ad_account.type_of_ownership as ad_account_type_of_ownership')
        ->select('ad_account.creation_type as ad_account_creation_type')
        ->select('ad_account.currency as ad_account_currency')
        ->select('ad_account.funding_source as ad_account_funding_source')
        ->select('ad_account.funding_source_details as ad_account_funding_source_details')
        ->select('ad_account.spend_cap as ad_account_spend_cap')
        ->select('ad_account.min_daily_budget as ad_account_min_daily_budget')
        ->select('ad_account.balance as ad_account_balance')
        ->select('ad_account.disable_reason as ad_account_disable_reason')
        ->select('ad_account.original_term_id as ad_account_original_term_id')
        ->select('ad_account.bank_id as ad_account_bank_id')

        ->from('fact_warehouse_exchange')
        ->join('ad_warehouse', 'ad_warehouse.id = ad_warehouse_id')
        ->join('ad_business_manager', 'ad_business_manager.id = ad_bm_id')
        ->join('ad_account', 'ad_account.id = ad_account_id')
        ->group_by('ad_warehouse.id,  fact_warehouse_exchange.ad_account_id');

        $pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);
        $data['config'] = $columns['columns'];

        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->exportXlsx($last_query);
            return TRUE;
        }

        $this->template->title->append('Kho tài khoản');
        return parent::response($data);
    }

    public function config_get()
    {
		parent::response(array('msg' => 'Dữ liệu tải thành công','data' => $this->config->item('datasource')));        
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering($args = array())
    {
        restrict('admin.adwarehouse.access');

        $args = parent::get(null, true);

        //if(empty($args) && parent::get('search')) $args = parent::get();  
        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

        if( ! empty($args['where']))
        {
            foreach($args['where'] as $key => $value)
            {
                $isHaving = in_array($key, ['fact_warehouse_exchange_begin_balance', 'fact_warehouse_exchange_import', 'fact_warehouse_exchange_export_use', 'fact_warehouse_exchange_export_suspend', 'fact_warehouse_exchange_balance']);

                $key = $isHaving ? $key : DatatableAdWarehouseSearchFields::name($key);

                if(empty($value)) continue;

                if(strpos($value, '%') !== FALSE)
                {
                    $_queryMethod = $isHaving ? 'having' : 'like';

                    $this->datatable_builder->{$_queryMethod}( $key, preg_replace("/[^A-Za-z0-9 ]/", '', $value));
                    continue;
                }

                $_queryMethod = $isHaving ? 'having' : 'where';

                $operator = trim(preg_replace('/[^\D]/', '', $value));
                if(in_array($operator, [ '>', '>=', '=', '<', '<=', '=!' ]))
                {
                    $this->datatable_builder->{$_queryMethod}("{$key} {$operator}", (double) preg_replace("/[^0-9.]/", "", $value));
                    continue;
                }

                $this->datatable_builder->{$_queryMethod}( $key, $value);
            }
        }

        if(!empty($args['order_by'])) 
        {
            foreach ($args['order_by'] as $key => $value)
            {
                $this->datatable_builder->order_by(DatatableAdWarehouseSearchFields::name($key), $value);
            }
        }
    }

    /**
     * Xuất file excel
     */
    public function exportXlsx($query = '')
    {
        if(empty($query)) return FALSE;

        $pos = strpos($query, 'LIMIT');
        if(FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $data = $this->fact_warehouse_exchange_m->query($query)->result();
        if( ! $data)
        {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(),'refresh');
        }

        $args = parent::get(null, true);
        $columns = $this->config->item('columns', 'datasource');
        $selected_columns = explode(',', $args['columns']);
        $selected_columns = array_reduce($selected_columns, function($result, $item) use ($columns){
            if(empty($columns[$item])) return $result;

            $result[$item] = $columns[$item];

            return $result;
        }, []);

        $spreadsheet = new Spreadsheet();
        $spreadsheet
        ->getProperties()
        ->setCreator('ADSPLUS.VN')
        ->setLastModifiedBy('ADSPLUS.VN')
        ->setTitle(time());

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray(array_column(array_values($selected_columns), 'label'), NULL, 'A1');

        $rowIndex = 2;

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($selected_columns as $key => $column)
            {
                $value = $item->$key;
                switch ($column['type'])
                {
                    case 'number': 
                        $value = (int) $value;
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        $sheet->setCellValueExplicitByColumnAndRow($colIndex, $rowIndex, $value, DataType::TYPE_NUMERIC);
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $value);

                        break;

                    default:
                        $sheet->setCellValueExplicitByColumnAndRow($colIndex, $rowIndex, $value, DataType::TYPE_STRING);
                        break;
                }

                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/ad-warehouse';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $title = time() . '-ad-warehouse-ie';
        $fileName = "{$folder_upload}/{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetAdWarehouseIE.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetAdWarehouseIE.php */