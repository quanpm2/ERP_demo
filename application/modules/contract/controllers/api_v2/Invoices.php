<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices extends MREST_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model([
			'contract/invoice_m',
			'contract/invoice_item_m'
		]);
	}

	public function index_get()
	{
		$default 	= array('contract_id' => '', 'meta' => '', 'field' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if( empty($args['contract_id'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		$response 		= array('msg' => 'Xử lý không thành công', 'data' => []);
		$model_names 	= array('term_m','termmeta_m', 'staffs/admin_m', 'contract/contract_m', 'webbuild/webbuild_m', 'term_users_m');
		$this->load->model($model_names);

		$this->contract_m->set_contract($args['contract_id']);

		if(FALSE === $this->contract_m->can( 'admin.contract.view'))
		{
			parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
		}

		$model_names = array('contract/invoice_m','contract/invoice_item_m','post_m','contract/base_contract_m');
		$this->load->model($model_names);

		$invoices = $this->term_posts_m
		->get_term_posts($args['contract_id'], $this->invoice_m->post_type, [
			'orderby' => 'posts.end_date',
			'fields' => 'term_id,posts.post_id,post_title,start_date,end_date,post_status'
		]) ?: [];

		if( ! $invoices) parent::response('Không tìm thấy kết quả phù hợp !', parent::HTTP_NO_CONTENT);

        $contract = $this->contract_m->get_contract();
        
		$vat_rate = (double) get_term_meta_value($args['contract_id'], 'vat');
		$response['data'] = array_values(array_map(function($x) use($vat_rate){
            $total = $this->invoice_item_m->get_total($x->post_id, 'total');

			$x->price_total = cal_vat($total, $vat_rate);
			return $x;
		}, $invoices));

		parent::response($response);
	}
}
/* End of file Invoices.php */
/* Location: ./application/modules/contract/controllers/api/Invoices.php */