<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'vendor/autoload.php';

class Fix extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['models'][] = 'contract/contract_m';

        parent::__construct();
    }

    /**
     * API dữ liệu các đợt thanh toán
     */
    public function migrate_exchange_rate_get()
    {
        $request_id = md5( __CLASS__ . __METHOD__ . time());
        log_message('debug', "[{$request_id}] Khởi tạo tiến trình migrate insight metadata mang giá trị 'Tỷ giá chuyển đổi'");

        $contracts = $this->contract_m->set_term_type()

        ->join('term_posts AS tu_ads_segment', 'tu_ads_segment.term_id = term.term_id')
        ->join('posts AS ads_segment', "ads_segment.post_id = tu_ads_segment.post_id AND ads_segment.post_type = 'ads_segment'")
        ->join('term_posts AS tu_adaccount', 'tu_adaccount.post_id = ads_segment.post_id')
        ->join('term AS accounts', "accounts.term_id = tu_adaccount.term_id AND accounts.term_type IN ('mcm_account', 'adaccount')")
        ->join('term_posts AS tu_insight_segment', 'accounts.term_id = tu_insight_segment.term_id')
        ->join('posts AS insight_segment', "insight_segment.post_id = tu_insight_segment.post_id AND insight_segment.post_type = 'insight_segment' 
            AND insight_segment.post_name = 'day' 
            AND insight_segment.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, '%Y-%m-%d 00:00:00')) 
            AND insight_segment.start_date <= IF(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP(), ads_segment.end_date)
        ")

        ->select('term.term_id AS contract_id, insight_segment.post_id AS insight_id, insight_segment.start_date AS start_date, insight_segment.end_date AS end_date')

        ->group_by('term.term_id, tu_ads_segment.post_id, accounts.term_id, insight_segment.post_id')

        ->as_array()
        ->get_all();

        $summary = [
            'records_found' => count($contracts),
            'ignored' => 0,
            'updated' => 0, 
        ];
        log_message('debug', "[{$request_id}] Output : " . json_encode($summary));

        foreach ($contracts as $contract)
        {
            $account_currency = get_post_meta_value($contract['insight_id'], 'account_currency');
            if(empty($account_currency) || $account_currency == 'VND')
            {
                $summary['ignored']++;
                continue;
            }

            log_message('debug', "[{$request_id}] Output : " . json_encode([
                'contractId' => $contract['contract_id'],
                'currency' => $account_currency
            ]));

            $exchange_rate = 0;
            (empty($exchange_rate)) AND ($account_currency == 'USD') AND $exchange_rate = get_term_meta_value($contract['contract_id'], 'dollar_exchange_rate');
            (empty($exchange_rate)) AND $exchange_rate = get_term_meta_value($contract['contract_id'], strtolower("exchange_rate_{$account_currency}_to_vnd"));
            (empty($exchange_rate)) AND ($account_currency == 'USD') AND $exchange_rate = get_post_meta_value($contract['insight_id'], 'dollar_exchange_rate');
            (empty($exchange_rate)) AND $exchange_rate = get_post_meta_value($contract['insight_id'], strtolower("exchange_rate_{$account_currency}_to_vnd"));
            (empty($exchange_rate)) AND $exchange_rate = get_post_meta_value($contract['insight_id'], 'exchange_rate');

            (empty($exchange_rate)) AND $exchange_rate = get_post_meta_value($contract['insight_id'], get_exchange_rate($account_currency));

            log_message('debug', "[{$request_id}] Process : " . json_encode([
                'contractId' => $contract['contract_id'],
                'currency' => $account_currency,
                'exchange_rate' => $exchange_rate,
            ]));
 
            update_post_meta($contract['insight_id'], 'exchange_rate', $exchange_rate);
            
            $summary['updated']++;
        }

        log_message('debug', "[{$request_id}] COMPLETED WITH RESULT : " . json_encode($summary));
    }

    /**
     * Tiến trình kiểm tra và migrate dữ liệu từ metadata chứa tỷ giá chuyển đổi ngoại tệ
     * 
     * @return [type]
     */
    public function migrate_contract_exchange_rate_get()
    {
        $request_id = md5( __CLASS__ . __METHOD__ . time());
        log_message('debug', "[{$request_id}] Khởi tạo tiến trình migrate term metadata mang giá trị 'Tỷ giá chuyển đổi'");

        $contractIds = $this->termmeta_m->where('meta_key', 'dollar_exchange_rate')->select('term_id')->group_by('term_id')->get_all();
        $contractIds AND $contractIds = array_column($contractIds, 'term_id');

        $contracts = $this->contract_m->where_in('term_type', ['google-ads', 'facebook-ads'])->select('term_id')
        ->where_in('term_id', $contractIds)
        ->as_array()
        ->get_all();

        $total_contracts = count( array_column($contracts, 'term_id'));
        log_message('debug', "[{$request_id}] Tìm thấy {$total_contracts} có chứa meta 'dollar_exchange_rate'");

        if(empty($total_contracts))
        {
            log_message('debug', "[{$request_id}] đóng tiến trình vì không thỏa điều kiện yêu cầu");
            die();
        }

        $contractIds = array_column($contracts, 'term_id');
        $summary = [
            'total' => $total_contracts,
            'continue' => 0,
            'updated' => 0
        ];

        foreach($contractIds as $contractId)
        {
            $exchange_rate_usd_to_vnd = (int) get_term_meta_value($contractId, 'exchange_rate_usd_to_vnd');
            if( ! empty($exchange_rate_usd_to_vnd))
            {
                $summary['continue']++;
                continue;
            }

            $summary['updated']++;
            $oValue = get_term_meta_value($contractId, 'dollar_exchange_rate');
            if(empty($oValue))
            {
                $oValue = get_exchange_rate();
                update_term_meta($contractId, 'exchange_rate_usd_to_vnd', $oValue);
                update_term_meta($contractId, 'dollar_exchange_rate', $oValue);
                log_message('debug', "[{$request_id}] update new exchange rate for both termmeta value - with value of contract Id {$contractId} update to '{$oValue}'");    
                continue;
            }

            update_term_meta($contractId, 'exchange_rate_usd_to_vnd', $oValue);

            log_message('debug', "[{$request_id}] 'exchange_rate_usd_to_vnd' value of contract Id {$contractId} update to '{$oValue}'");
        }

        log_message('debug', "[{$request_id}] RESULT : " . json_encode($summary));
    }

    /**
     * Tiến trình kiểm tra và migrate dữ liệu từ metadata chứa tỷ giá chuyển đổi ngoại tệ
     * Điều kiện kiểm tra:
     * - Các hợp đồng bắt đầu từ ngày 00:00:00 01/05/2022 đến hiện tại
     * - Chưa có tỉ giá USD, AUD
     * - Nếu Đã cấu hình tài khoản
     *   - Insight tài khoản có tỉ giá khác VND
     * 
     * @return [type]
     */
    public function migrate_exchange_rate_from_20220501_get()
    {
        $request_id = md5( __CLASS__ . __METHOD__ . time());
        log_message('debug', "[{$request_id}] Khởi tạo tiến trình migrate term metadata mang giá trị 'Tỷ giá chuyển đổi'");


        $start_date = start_of_day(strtotime('2022/05/01'));

        $contracts = $this->contract_m->where_in('term_type', ['google-ads', 'facebook-ads', 'tiktok-ads'])
        ->join('termmeta', 'termmeta.term_id = term.term_id AND termmeta.meta_key = "start_service_time"')
        ->select('term.term_id')
        ->where('termmeta.meta_value >=', $start_date)
        ->group_by('term.term_id')
        ->order_by('term.term_id', 'DESC')
        ->as_array()
        ->get_all();

        $total_contracts = count(array_column($contracts, 'term_id'));
        log_message('debug', "[{$request_id}] Tìm thấy {$total_contracts} bắt đầu từ ngày 2022/05/01");

        if(empty($total_contracts))
        {
            log_message('debug', "[{$request_id}] đóng tiến trình vì không thỏa điều kiện yêu cầu");
            die();
        }

        $summary = [
            'total' => $total_contracts,
            'continue' => 0,
            'updated' => 0
        ];

        foreach($contracts as $contract)
        {
            $contractId = $contract['term_id'];
            $option_exchange_rate = $this->option_m->get_value('exchange_rate', TRUE);

            $is_updated = FALSE;

            $exchange_rate_usd_to_vnd = (int) get_term_meta_value($contractId, 'exchange_rate_usd_to_vnd');
            if(empty($exchange_rate_usd_to_vnd))
            {
                $is_updated = TRUE;

                update_term_meta($contractId, 'exchange_rate_usd_to_vnd', $option_exchange_rate['usd_to_vnd'] ?? 0);
            }

            $exchange_rate_aud_to_vnd = (int) get_term_meta_value($contractId, 'exchange_rate_aud_to_vnd');
            if(empty($exchange_rate_aud_to_vnd))
            {
                $is_updated = TRUE;

                update_term_meta($contractId, 'exchange_rate_aud_to_vnd', $option_exchange_rate['aud_to_vnd'] ?? 0);
            }

            $adaccount_status = get_term_meta_value($contractId, 'adaccount_status');
            if('APPROVED' != $adaccount_status) {
                if($is_updated) $summary['updated']++;
                else $summary['continue']++;

                continue;
            };

            $account_currencies = $this->term_posts_m->where('term_posts.term_id', $contractId)
            ->join('posts AS ads_segment', 'ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = "ads_segment"')
            ->join('term_posts AS tp_adaccount', 'tp_adaccount.post_id = ads_segment.post_id')
            ->join('term AS adaccount', 'adaccount.term_id = tp_adaccount.term_id AND adaccount.term_type IN ("mcm_account", "adaccount")', 'LEFT')
            ->join('term_posts AS tp_insight_segment', 'adaccount.term_id = tp_insight_segment.term_id', 'LEFT')
            ->join('posts AS insight_segment', "insight_segment.post_id = tp_insight_segment.post_id AND insight_segment.post_type = 'insight_segment' 
                AND insight_segment.post_name = 'day' 
                AND insight_segment.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, '%Y-%m-%d 00:00:00')) 
                AND insight_segment.start_date <= IF(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP(), ads_segment.end_date)
            ", 'LEFT')
            ->join('postmeta', 'postmeta.post_id = insight_segment.post_id AND postmeta.meta_key = "account_currency" AND postmeta.meta_value != "VND"', 'LEFT')
            ->select('DISTINCT(postmeta.meta_value) AS "account_currency"')
            ->group_by('term_posts.term_id, ads_segment.post_id, adaccount.term_id, insight_segment.post_id')
            ->having('account_currency != ""')
            ->order_by('insight_segment.post_id', 'DESC')
            ->as_array()
            ->get_all();
            if(empty($account_currencies)){
                if($is_updated) $summary['updated']++;
                else $summary['continue']++;

                continue;
            }

            $account_currencies = array_column($account_currencies, 'account_currency');

            foreach($account_currencies as $account_currency){
                $meta_key = 'exchange_rate_' . strtolower($account_currency) . '_to_vnd';
                $exchange_rate = (int) get_term_meta_value($contractId, $meta_key);

                if(empty($exchange_rate))
                {
                    $option_exchange_rate_key = strtolower($account_currency) . '_to_vnd';

                    $is_updated = TRUE;

                    update_term_meta($contractId, $meta_key, $option_exchange_rate[$option_exchange_rate_key] ?? 0);
                }
            }

            if($is_updated) $summary['updated']++;
            else $summary['continue']++;
        }

        log_message('debug', "[{$request_id}] RESULT : " . json_encode($summary));
    }

    /**
     * Tiến trình cập nhật lại spend cho các tài khoản bị mất kết nối tài khoản
     * Điều kiện kiểm tra:
     * - Các Insight bắt đầu từ ngày 00:00:00 01/05/2022 đến hiện tại
     * - Tỉ giá khác VND
     * - Lấy tỉ giá theo thứ tự insight > contract > option
     * - Nếu hợp đồng chưa có tỉ giá, lưu lại tỉ giá vào hợp đồng
     * - Nếu insight chưa có tỉ giá, lưu lại tỉ giá vào insight
     * 
     * @return [type]
     */
    public function migrate_insight_spend_from_20220501_get()
    {
        $request_id = md5( __CLASS__ . __METHOD__ . time());
        log_message('debug', "[{$request_id}] Khởi tạo tiến trình migrate insight spend có tỉ giá khác VND");

        $start_date = start_of_day(strtotime('2022/05/01'));

        $contracts = $this->contract_m->where_in('term_type', ['google-ads', 'facebook-ads'])
        ->join('termmeta', 'termmeta.term_id = term.term_id AND termmeta.meta_key = "start_service_time"')
        ->select('term.term_id')
        ->select('term.term_type')
        ->where('termmeta.meta_value >=', $start_date)
        ->group_by('term.term_id')
        ->order_by('term.term_id', 'DESC')
        ->as_array()
        ->get_all();

        $total_contracts = count(array_column($contracts, 'term_id'));
        log_message('debug', "[{$request_id}] Tìm thấy {$total_contracts} bắt đầu từ ngày 2022/05/01");

        if(empty($total_contracts))
        {
            log_message('debug', "[{$request_id}] đóng tiến trình vì không thỏa điều kiện yêu cầu");
            die();
        }

        $summary = [
            'total' => $total_contracts,
            'continue' => 0,
            'updated' => 0
        ];

        foreach($contracts as $contract)
        {
            $contractId = $contract['term_id'];

            $insights = $this->term_posts_m->where('term_posts.term_id', $contractId)
            ->join('posts AS ads_segment', 'ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = "ads_segment"')
            ->join('term_posts AS tp_adaccount', 'tp_adaccount.post_id = ads_segment.post_id')
            ->join('term AS adaccount', 'adaccount.term_id = tp_adaccount.term_id AND adaccount.term_type IN ("mcm_account", "adaccount")', 'LEFT')
            ->join('term_posts AS tp_insight_segment', 'adaccount.term_id = tp_insight_segment.term_id', 'LEFT')
            ->join('posts AS insight_segment', "insight_segment.post_id = tp_insight_segment.post_id AND insight_segment.post_type = 'insight_segment' 
                AND insight_segment.post_name = 'day' 
                AND insight_segment.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, '%Y-%m-%d 00:00:00')) 
                AND insight_segment.start_date <= IF(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP(), ads_segment.end_date)
            ", 'LEFT')
            ->join('postmeta', 'postmeta.post_id = insight_segment.post_id AND postmeta.meta_key IN ("account_currency", "spend")', 'LEFT')
            ->select('insight_segment.post_id')
            ->select('MAX(IF(postmeta.meta_key = "account_currency", postmeta.meta_value, NULL)) AS account_currency')
            ->select('MAX(IF(postmeta.meta_key = "spend", postmeta.meta_value, NULL)) AS spend')
            ->group_by('term_posts.term_id, ads_segment.post_id, adaccount.term_id, insight_segment.post_id')
            ->having('account_currency !=', 'VND')
            ->having('account_currency IS NOT NULL', NULL)
            ->having('(spend = 0 OR spend IS NULL)', NULL)
            ->order_by('insight_segment.post_id', 'DESC')
            ->as_array()
            ->get_all();

            if(empty($insights)) {
                $summary['continue']++;
                continue;
            };

            $is_updated = FALSE;

            foreach($insights as $insight){
                $insight_id = $insight['post_id'];
                
                $exchange_rate = (float) get_post_meta_value($insight_id, 'exchange_rate') ?: 0;
                if(empty($exchange_rate)){
                    $exchange_rate_key = strtolower($insight['account_currency']) . '_to_vnd';

                    $exchange_rate = (float) get_term_meta_value($contractId, 'exchange_rate_' . $exchange_rate_key) ?: 0;
                    if(empty($exchange_rate)){
                        $exchange_rate_option = $this->option_m->get_value('exchange_rate', TRUE);
                        $exchange_rate = $exchange_rate_option[$exchange_rate_key] ?? 0;

                        update_term_meta($contractId, 'exchange_rate_' . $exchange_rate_key, $exchange_rate);
                    }

                    update_post_meta($insight_id, 'exchange_rate', $exchange_rate);
                }

                if('google-ads' == $contract['term_type']){
                    $cost = (float) get_post_meta_value($insight_id, 'cost');
                    $spend = ($cost / 1000000) * $exchange_rate;
                    
                    update_post_meta($insight_id, 'spend', $spend);
                    $is_updated = TRUE;
                    continue;
                }

                if('facebook-ads' == $contract['term_type']){
                    $cost = (float) get_post_meta_value($insight_id, 'cost');
                    $spend = $cost * $exchange_rate;
                    
                    update_post_meta($insight_id, 'spend', $spend);
                    $is_updated = TRUE;
                    continue;
                }
            }

            if($is_updated) $summary['updated']++;
            else $summary['continue']++;
        }

        log_message('debug', "[{$request_id}] RESULT : " . json_encode($summary));
    }

    /**
     * Tiến trình cập nhật lại %VAT cho hợp đồng GURU
     * Điều kiện kiểm tra:
     * - Hợp đồng chạy từ 01/07/2022
     * - %VAT != 8
     * Điều chỉnh %VAT = 8%, tính toán lại các phiếu thanh toán
     * 
     * @return [type]
     */
    public function migrate_payment_guru_vat_get()
    {
        $request_id = md5( __CLASS__ . __METHOD__ . time());
        log_message('debug', "[{$request_id}] Khởi tạo tiến trình migrate %VAT hợp đồng GURU");

        $start_date = start_of_day(strtotime('2022/07/01'));

        $receipt_payments = $this->contract_m->where_in('term_type', 'courseads')
        ->join('term_posts AS tp_receipt_payments', 'tp_receipt_payments.term_id = term.term_id')
        ->join('posts AS receipt_payments', 'receipt_payments.post_id = tp_receipt_payments.post_id AND receipt_payments.post_type = "receipt_payment"')
        ->join('postmeta AS vat', 'vat.post_id = receipt_payments.post_id AND vat.meta_key = "vat"', 'LEFT')
        ->where('receipt_payments.end_date >=', $start_date)
        ->where('(vat.meta_value IS NULL OR vat.meta_value = 0)', NULL)
        ->group_by('receipt_payments.post_id')
        ->select('term.term_id')
        ->select('receipt_payments.post_id')
        ->select('vat.meta_value AS vat')
        ->as_array()
        ->get_all();

        $total_receipt_payments = count(array_column($receipt_payments, 'term_id'));
        log_message('debug', "[{$request_id}] Tìm thấy {$total_receipt_payments} bắt đầu từ ngày 2022/07/01");

        if(empty($total_receipt_payments))
        {
            log_message('debug', "[{$request_id}] đóng tiến trình vì không thỏa điều kiện yêu cầu");
            die();
        }

        $summary = [
            'total' => $total_receipt_payments,
            'continue' => 0,
            'updated' => 0
        ];

        $this->load->model('contract/receipt_courses_m');
        foreach($receipt_payments as $receipt_payment)
        {
            $receipt_payment_id = $receipt_payment['post_id'];
            
            $_receipt_payment = $this->receipt_courses_m->set_post_type()
            ->where('post_id =', $receipt_payment_id)
            ->get_by();
            
            if(empty($_receipt_payment)){
                $summary['continue']++;
                continue;
            }

            update_post_meta($receipt_payment_id, 'vat', 8);
            $_receipt_payment->calc_amount_callback($receipt_payment_id);
            $summary['updated']++;
        }

        log_message('debug', "[{$request_id}] RESULT : " . json_encode($summary));
    }

    /**
     * API dữ liệu Kinh doanh phiếu thah toán
     */
    public function migrate_receipt_exchange_rate_get()
    {
        $request_id = md5( __CLASS__ . __METHOD__ . time());
        log_message('debug', "[{$request_id}] Khởi tạo tiến trình migrate receipt meta_key 'staff_business_id' với value là meta 'staff_bussiness' của hợp đồng");

        $this->load->model('contract/receipt_m');

        $receipts = $this->receipt_m->set_post_type()
            ->select('post_id')
            ->as_array()
            ->get_all();
        
        $stat = [
            'total' => count($receipts),
            'success' => 0,
            'ignored' => 0,
            'detail' => [
                'success' => [],
                'ignored' => [],
            ]
        ];

        foreach($receipts as $receipt)
        {
            $staff_bussiness_id = get_post_meta_value($receipt['post_id'], 'staff_business_id');
            if(!empty($staff_bussiness_id))
            {
                $stat['ignored'] += 1;
                $stat['detail']['ignored'][] = $receipt['post_id'];

                continue;
            }

            $receipt_m = new receipt_m();
            $receipt_m->post_id = $receipt['post_id'];
            $contract = (array)$receipt_m->contract();
            if(empty($contract))
            {
                $stat['ignored'] += 1;
                $stat['detail']['ignored'][] = $receipt['post_id'];

                continue;
            }
            
            $staff_bussiness_id = get_term_meta_value($contract['term_id'], 'staff_business');
            if(empty($staff_bussiness_id))
            {
                $stat['ignored'] += 1;
                $stat['detail']['ignored'][] = $receipt['post_id'];

                continue;
            }

            update_post_meta($receipt['post_id'], 'staff_business_id', $staff_bussiness_id);

            $stat['success'] += 1;
            $stat['detail']['success'][] = $receipt['post_id'];
        }

        log_message('debug', "[{$request_id}] COMPLETED WITH RESULT : " . json_encode($stat));

        parent::response(['status'=>TRUE, 'data' => $stat]);
    }
}
/* End of file Index.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Index.php */