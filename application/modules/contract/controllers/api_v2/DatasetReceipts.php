<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetReceipts extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';

        parent::__construct();

        $this->load->config('contract/receipt');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
        if( ! has_permission('contract.receipt.access'))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args( parent::get(), $defaults);

        $this->load->config('contract');
        $this->load->config('contract/receipt');
        $this->load->config('customer/customer');

        $this->load->model('staffs/department_m');
        $this->load->model('staffs/user_group_m');
        $this->load->model('staffs/term_users_m');
        $this->load->model('staffs/sale_m');

        $sales = $this->sale_m->set_user_type()->set_role()->get_all();
        $sales = $sales ? key_value($sales,'user_id','user_email') : [];

        $data = $this->data;
        $this->search_filter_receipt();
        
        $sum_amount_not_vat = 0;
        $receipt_status = $this->config->item('status','receipt');

        /* Lấy tất cả phòng ban và convert về dạng array với key = id và value = tên phòng ban */
        $departments = $this->department_m->select('term_id,term_name')->set_term_type()->as_array()->get_all();
        $departments = $departments ? key_value($departments, 'term_id', 'term_name') : [];
        /* 
         * Lấy tất cả nhóm và convert về dạng array với key = id và value = tên phòng ban
         * Đồng thời group từng nhóm lại theo đúng tên phòng ban chứa nhóm đấy
         */
        $user_groups = $this->user_group_m->select('term_id,term_name,term_parent')->set_term_type()->as_array()->get_all();
        if( ! empty($user_groups))
        {
            $_user_groups = array();
            foreach (array_group_by($user_groups, 'term_parent') as $department_id => $groups)
            {
                if(empty($departments[$department_id])) continue;

                $_user_groups[$departments[$department_id]] = key_value($groups, 'term_id', 'term_name');
            }

            $user_groups = $_user_groups;
        }

        $receipt_enums = [ 'receipt_payment_all' => 'Thanh toán (+ thu chi hộ)'] + $this->config->item('type', 'receipt');

        $data['content'] = $this->datatable_builder
        ->set_filter_position(FILTER_TOP_INNER)
        ->setOutputFormat('JSON')
        ->select('posts.post_id,posts.post_author,posts.post_title')    

        // ->add_column('all_checkbox',array('title'=> '<input type="checkbox" name="all-checkbox" value="" id="all-checkbox" class="minimal"/>','set_order'=> FALSE, 'set_select' => FALSE))
        ->add_search('end_date',['placeholder'=>'Ngày thanh toán','class'=>'form-control set-datepicker'])
        ->add_search('contract_code',['placeholder'=>'Số hợp đồng'])
        ->add_search('contract_status',['content'=> form_dropdown(['name'=>'where[contract_status]','class'=>'form-control select2'],prepare_dropdown($this->config->item('contract_status'),'Trạng thái hợp đồng'),parent::get('where[contract_status]'))])
        ->add_search('service_fee_payment_type', array('content'=> form_dropdown(['name'=>'where[service_fee_payment_type]','class'=>'form-control select2'],prepare_dropdown($this->config->item('service_fee_payment_type'),'Phương thức thanh toán phí DV'),parent::get('where[service_fee_payment_type]'))))

        ->add_search('vat',array('content'=> form_dropdown(['name'=>'where[vat]','class'=>'form-control select2'],prepare_dropdown([0 => '0%', 8 => '8%', 10=>'10%'],'VAT'),parent::get('where[vat]'))))
        ->add_search('term_type',array('content'=> form_dropdown(['name'=>'where[term_type]','class'=>'form-control select2'],prepare_dropdown($this->config->item('taxonomy'),'Loại dịch vụ'),parent::get('where[term_type]'))))
        ->add_search('post_type',array('content'=> form_dropdown(['name'=>'where[post_type]','class'=>'form-control select2'],prepare_dropdown($receipt_enums,'Loại thanh toán'),parent::get('where[post_type]'))))
        ->add_search('post_status',array('content'=> form_dropdown(['name'=>'where[post_status]','class'=>'form-control select2'],prepare_dropdown($receipt_status['all'],'Tình trạng'),parent::get('where[post_status]'))))
        ->add_search('transfer_type',array('content'=> form_dropdown(['name'=>'where[transfer_type]','class'=>'form-control select2'],prepare_dropdown($this->config->item('transfer_type','receipt'),'Hình thức chuyển'),parent::get('where[transfer_type]'))))
        ->add_search('departments',array('content'=> form_dropdown(['name'=>'where[departments]','class'=>'form-control select2'],prepare_dropdown($departments,'Phòng ban'),parent::get('where[departments]'))))
        ->add_search('user_groups',array('content'=> form_dropdown(['name'=>'where[user_groups]','class'=>'form-control select2'],prepare_dropdown($user_groups,'Nhóm'),parent::get('where[user_groups]'))))
        ->add_search('action',['content'=>form_button(['name'=>'notification_of_payment','class'=>'btn btn-info', 'id'=>'send-mail-payment'],'<i class="fa fa-fw fa-send"></i> Email')])
    
        ->add_column('contract_code', array('title'=>'Hợp đồng','set_select'=>FALSE,'set_order'=>FALSE))
        ->add_column('contract_status', array('title'=>'Trạng thái hợp đồng','set_select'=>FALSE,'set_order'=>FALSE))
        ->add_column('service_fee_payment_type', array('title'=>'Phương thức thanh toán phí DV','set_select'=>FALSE,'set_order'=>FALSE))
        ->add_column('posts.end_date', array('title'=> 'Ngày thanh toán','set_order'=> TRUE))
        ->add_column('amount', array('title'=> 'Số tiền(vnđ)','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('vat', array('title'=> 'VAT','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('amount_not_vat', array('title'=> 'Doanh thu(vnđ)','set_order'=> FALSE,'set_select'=>FALSE))

        ->add_column('vat_amount', array('title'=> 'Thuế','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('service_fee_rate', array('title'=> '% Phí dịch vụ','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('service_fee', array('title'=> 'Phí dịch vụ','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('service_provider_tax', array('title'=> 'Thuế Google thu','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('fct_amount', array('title'=> 'Thuế nhà thầu','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('actual_budget', array('title'=> 'Ngân sách thực','set_order'=> TRUE,'set_select'=>FALSE))

        ->add_column('term_type', array('title'=> 'Dịch vụ','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('posts.post_type', array('title'=> 'Loại','set_order'=> TRUE))
        ->add_column('posts.post_status', array('title'=> 'Trạng thái','set_order'=> TRUE))
        ->add_column('is_order_printed', array('title'=> 'In HĐ','set_order'=> TRUE,'set_select'=>FALSE))
        ->add_column('transfer_type', array('title'=> 'Hình thức','set_order'=> TRUE,'set_select'=>FALSE))

        ->add_column_callback('post_id',function($data,$row_name) use ($sales) {

            $post_id = $data['post_id'];
            $data['post_id'] = (int) $data['post_id'];
            $data['end_date'] = (int) $data['end_date'];

            $data['has_send_mail_payment'] = (bool) get_post_meta_value($post_id, 'has_send_mail_payment');

            $data['customer'] = $data['sale'] = $data['author'] = null;

            $taxonomies = $this->config->item('taxonomy');
            $taxonomies = array_keys($taxonomies);
            $contract = $this->term_posts_m->get_post_terms($post_id, $taxonomies);
            $contract AND $contract = array_filter($contract);
            $contract AND $contract = reset($contract);
            $contract AND $contract = reset($contract);
            if(empty($contract)) return;
            $term_id = $contract->term_id;

            $data['term_id'] = (int) $term_id;

            $data['contract_code'] = get_term_meta_value($term_id,'contract_code');
            
            $config_contract_status = $this->config->item('contract_status');
            $data['contract_status'] = $config_contract_status[$contract->term_status] ?? '--';

            $service_fee_payment_type = $this->config->item('service_fee_payment_type');
            $data['service_fee_payment_type'] = $service_fee_payment_type[get_term_meta_value($term_id,'service_fee_payment_type')] ?? '--';

            $sale_id = (int) get_term_meta_value($term_id,'staff_business');
            if( ! empty($sale_id))
            {
                $data['sale'] = elements(['user_id', 'display_name', 'user_email'], $this->admin_m->get_field_by_id($sale_id));
                $data['sale']['departments'] = null;
                $departments = $this->term_users_m->get_user_terms($sale_id, $this->department_m->term_type);
                empty($departments) OR $data['sale']['departments'] = array_column($departments, 'term_name');

                $data['sale']['user_groups'] = null;
                $user_groups = $this->term_users_m->get_user_terms($sale_id, $this->user_group_m->term_type);
                empty($user_groups) OR $data['sale']['user_groups'] = array_column($user_groups, 'term_name');
            }

            // Get customer display name
            if($customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company','system']))
            {
                $customer       = end($customers);
                $data['customer'] = elements(['user_id', 'display_name', 'user_email'], (array) $customer);
                $data['customer']['cid'] = cid($customer->user_id, $customer->user_type);
            }

            // Hình thức thanh toán
            $this->load->config('contract/receipt');
            $def_transfer_type = $this->config->item('transfer_type','receipt');
            $transfer_type = get_post_meta_value($post_id,'transfer_type');

            $data['transfer_type'] = $def_transfer_type[$transfer_type] ?? reset($def_transfer_type);

            // tài khoản
            'account' == $transfer_type AND $data['transfer_account'] = get_post_meta_value($post_id,'transfer_account');

            $data['amount']         = (double) get_post_meta_value($post_id,'amount');
            $data['vat_amount']     = (int) get_post_meta_value($post_id,'vat_amount');
            $data['service_fee_rate'] = (int) get_post_meta_value($post_id,'service_fee_rate');
            $data['service_fee']    = (double) get_post_meta_value($post_id,'service_fee');
            $data['fct_amount']     = (double) get_post_meta_value($post_id,'fct_amount');
            $data['actual_budget'] = (double) get_post_meta_value($post_id,'actual_budget');
            $data['service_provider_tax'] = (double) get_post_meta_value($post_id,'service_provider_tax');

            switch ($data['post_type']) {
                
                case 'receipt_payment_on_behaft':
                    $data['vat'] = 0;
                    break;
                
                default:
                    $data['vat'] = (int)get_post_meta_value($post_id, 'vat');
                    $data['amount_not_vat'] = ((double) div($data['amount'], (1+div($data['vat'], 100))));
                    break;
            }

            $data['term_type'] = $this->config->item($contract->term_type, 'taxonomy');

            // Ngày thanh toán
            $payment_time = get_post_meta_value($post_id,'payment_time');
            $data['payment_time'] = my_date($payment_time,'d/m/Y');
            $data['post_author'] = $this->admin_m->get_field_by_id($data['post_author'],'user_email');
            $def_post_status = $this->config->item('status','receipt');
            $data['post_status'] = $def_post_status[$data['post_type']][$data['post_status']];

            $def_post_type = $this->config->item('type','receipt');
            $post_type = $data['post_type'];
            $is_promotion = (bool)get_post_meta_value($post_id,'is_promotion');
            $is_promotion ? $data['post_type'] = $def_post_type['promotion'] : $data['post_type'] = $def_post_type[$post_type];

            $data['is_order_printed'] = (bool) get_post_meta_value($post_id,'is_order_printed');

            if($post_type == 'receipt_caution')
            {
                $data['end_date'].= br().'<span class="text-yellow">'.$def_post_type[$post_type].'</span>';
            }
            return $data;
        },FALSE)

        ->from('posts')
        ->where('posts.post_status !=','draft')
        ->where_in('posts.post_type',['receipt_payment','receipt_caution', 'receipt_payment_on_behaft'])
        ->group_by('posts.post_id')
        ->order_by('posts.post_id');

        $pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

        $data['sum_amount'] = array(
            'amount'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'amount'),array('amount'=>0)),
        );

        $data = $this->datatable_builder->generate($pagination_config);
        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->export_receipt($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function search_filter_receipt($args = array())
    {
        restrict('contract.revenue.access');

        $args = parent::get();    
        //if(empty($args) && parent::get('search')) $args = parent::get();  
        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);
        
        //departments FILTERING & SORTING
        $filter_departments = $args['where']['departments'] ?? FALSE;
        $filter_user_groups = $args['where']['user_groups'] ?? FALSE;

        if(!empty($filter_departments) || !empty($filter_user_groups)){
            
            $user_ids = array();
            if( ! empty($filter_departments) && $_users = $this->term_users_m->get_the_users($filter_departments, 'admin'))
            {
                $user_ids = array_column($_users, 'user_id');
            }

            if( ! empty($filter_user_groups))
            {
                $_args  = $user_ids ?: array('where_in'=>['term_users.user_id'=>$user_ids]);
                $_users = $this->term_users_m->get_term_users($filter_user_groups, 'admin', $_args);
                $user_ids = $_users ? array_column($_users, 'user_id') : [];
            }

            if(empty($user_ids)) $this->datatable_builder->where('post.post_id', uniqid());

            $alias_post_contract    = uniqid('term_posts_');
            $alias_contract_users   = uniqid('term_users_');
            $alias_meta = uniqid('staff_business_');

            $this->datatable_builder
            ->join("term_posts {$alias_post_contract}","posts.post_id = {$alias_post_contract}.post_id")
            ->join("termmeta {$alias_meta}","{$alias_meta}.term_id = {$alias_post_contract}.term_id and {$alias_meta}.meta_key = 'staff_business'")
            ->where_in("{$alias_meta}.meta_value", $user_ids);
        }   

        // contract_daterange FILTERING & SORTING
        $filter_contract_daterange = $args['where']['contract_daterange'] ?? FALSE;
        $sort_contract_daterange = $args['order_by']['contract_daterange'] ?? FALSE;
        if($filter_contract_daterange || $sort_contract_daterange)
        {
            $alias = uniqid('contract_daterange_');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

            if($sort_contract_daterange)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_daterange);
                unset($args['order_by']['contract_daterange']);
            }
        }

        // post_title FILTERING & SORTING
        empty($args['where']['post_title']) OR $this->datatable_builder->like('posts.post_title', trim($args['where']['post_title']));

        // Contract_begin FILTERING & SORTING
        if( ! empty($args['where']['end_date']))
        {
            $dates = explode(' - ', $args['where']['end_date']);
            $this->datatable_builder
                ->where("posts.end_date >=", $this->mdate->startOfDay(reset($dates)))
                ->where("posts.end_date <=", $this->mdate->endOfDay(end($dates)));
        }
        
        if( ! empty($args['order_by']['end_date']))
        {
            $this->datatable_builder->order_by("posts.end_date",$args['order_by']['end_date']);
        }

        // Contract_code FILTERING & SORTING
        if(!empty($args['where']['contract_code']))
        {
            $termmetas = $this->termmeta_m->select('term_id')
                ->like('meta_value',$args['where']['contract_code'])
                ->get_many_by(['meta_key'=>'contract_code']);

            $this->datatable_builder
                ->join('term_posts','term_posts.post_id = posts.post_id')
                ->where_in('term_posts.term_id', ($termmetas ? array_column($termmetas, 'term_id') : [0]));
        }

        if(!empty($args['where']['service_fee_payment_type']))
        {
            $termmetas = $this->termmeta_m->select('term_id')
                ->like('meta_value',$args['where']['service_fee_payment_type'])
                ->get_many_by(['meta_key'=>'service_fee_payment_type']);

            $this->datatable_builder
                ->join('term_posts','term_posts.post_id = posts.post_id')
                ->where_in('term_posts.term_id', ($termmetas ? array_column($termmetas, 'term_id') : [0]));
        }

        // Customer display_name FILTERING & SORTING
        if(!empty($args['where']['customer']))
        {

            $matches = [];
            preg_match('/^(PE|CO)(\d*)$/i', trim($args['where']['customer']), $matches);
            if( ! empty($matches))
            {    
                $matches = end($matches);
                $this->datatable_builder
                ->join('term_posts','term_posts.post_id = posts.post_id')
                ->join('term_users','term_users.term_id = term_posts.term_id')
                ->where_in('term_users.user_id', (int) $matches);
            }
            else
            {
                $this->load->model('customer/customer_m');
                $customers = $this->customer_m
                    ->select('user.user_id')
                    ->where_in('user.user_type',$this->customer_m->customer_types)
                    ->like('user.display_name', trim($args['where']['customer']))
                    ->get_many_by();

                $this->datatable_builder
                    ->join('term_posts','term_posts.post_id = posts.post_id')
                    ->join('term_users','term_users.term_id = term_posts.term_id')
                    ->where_in('term_users.user_id', ($customers ? array_column($customers, 'user_id') : [0]));
            }
        }

        // Email kinh doanh FILTERING & SORTING
        $filter_sale_email = $args['where']['sale_email'] ?? FALSE;
        $sort_sale_email = $args['order_by']['sale_email'] ?? FALSE;
        if($filter_sale_email || $sort_sale_email)
        {
            $alias = uniqid('staff_business_');
            $this->datatable_builder
                ->join('term_posts','term_posts.post_id = posts.post_id')
                ->join("termmeta {$alias}","{$alias}.term_id = term_posts.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT');

            if($filter_sale_email)
            {   
                $sales = $this->admin_m
                ->select('user.user_id')
                ->set_get_admin()
                ->like('user.user_email',trim($args['where']['sale_email']))
                ->get_many_by();

                $this->datatable_builder->where_in("{$alias}.meta_value",($sales ? array_column($sales, 'user_id') : ['-1']));
            }

            if($sort_sale_email)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_sale_email);
            }
        }

        // Email kinh doanh FILTERING & SORTING
        $filter_vat = $args['where']['vat'] ?? FALSE;
        $sort_vat = $args['order_by']['vat'] ?? FALSE;
        if(is_numeric($filter_vat) || $sort_vat)
        {
            $post_alias = uniqid('post_vat_');
            $this->datatable_builder
                ->join("postmeta {$post_alias}","{$post_alias}.post_id = posts.post_id and {$post_alias}.meta_key = 'vat'",'LEFT');

            if(is_numeric($filter_vat))
            {
                $this->datatable_builder->where("{$post_alias}.meta_value", $filter_vat);
            }

            if($sort_vat)
            {
                $this->datatable_builder->order_by("{$post_alias}.meta_value",$sort_vat);
            }
        }

        // Tình trạng In hóa đơn FILTERING & SORTING
        $filter_post_type = $args['where']['post_type'] ?? FALSE;
        if($filter_post_type)
        {
            $_condition = 'where';
            if('receipt_payment_all' == $args['where']['post_type'])
            {
                $_condition         = 'where_in';
                $filter_post_type   = ['receipt_payment', 'receipt_payment_on_behaft'];
            }

            $this->datatable_builder->{$_condition}('posts.post_type', $filter_post_type);
        }

        empty($args['order_by']['post_type']) OR $this->datatable_builder->order_by('posts.post_type', $args['order_by']['post_type']);

        // Trạng thái thanh toán FILTERING & SORTING
        if( ! empty($args['where']['post_status']))
        {
            $this->datatable_builder->where('posts.post_status',$args['where']['post_status']);
        }

        if( ! empty($args['where']['post_status']))
        {
            $this->datatable_builder->order_by('posts.post_status',$args['where']['post_status']);
        }

        // Tình trạng In hóa đơn FILTERING & SORTING
        $filter_is_order_printed = $args['where']['is_order_printed'] ?? FALSE;
        $sort_is_order_printed = $args['order_by']['is_order_printed'] ?? FALSE;
        if($filter_is_order_printed || $sort_is_order_printed)
        {
            $alias = uniqid('is_order_printed_');
            $this->datatable_builder
                ->join('term_posts','term_posts.post_id = posts.post_id')
                ->join("termmeta {$alias}","{$alias}.term_id = term_posts.term_id and {$alias}.meta_key = 'is_order_printed'",'LEFT');

            if($filter_is_order_printed)
            {   
                $this->datatable_builder->where("{$alias}.meta_value",$filter_is_order_printed);
            }

            if($sort_is_order_printed)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_is_order_printed);
            }
        }


        // Hình thức chuyển tiền FILTERING & SORTING
        $filter_transfer_type = $args['where']['transfer_type'] ?? FALSE;
        $sort_transfer_type = $args['order_by']['transfer_type'] ?? FALSE;
        if($filter_transfer_type || $sort_transfer_type)
        {
            $alias = uniqid('transfer_type_');
            $this->datatable_builder
            ->join("postmeta {$alias}","{$alias}.post_id = posts.post_id and {$alias}.meta_key = 'transfer_type'",'LEFT');

            if($filter_transfer_type)
            {   
                $this->datatable_builder->where("{$alias}.meta_value",$filter_transfer_type);
            }

            if($sort_transfer_type)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_transfer_type);
            }
        }

        // Loại hình dịch vụ FILTERING & SORTING
        $filter_term_type = $args['where']['term_type'] ?? FALSE;
        $sort_term_type = $args['order_by']['term_type'] ?? FALSE;
        if($filter_term_type || $sort_term_type)
        {
            $this->datatable_builder
            ->join('term_posts','term_posts.post_id = posts.post_id')
            ->join('term','term.term_id = term_posts.term_id','LEFT');

            if($filter_term_type)
            {   
                $this->datatable_builder->where('term.term_type',$filter_term_type);
            }

            if($sort_term_type)
            {
                $this->datatable_builder->order_by('term.term_type',$sort_term_type);
            }
        }

        // Loại hình dịch vụ FILTERING & SORTING
        $filter_contract_status = $args['where']['contract_status'] ?? FALSE;
        $sort_contract_status = $args['order_by']['contract_status'] ?? FALSE;
        if($filter_contract_status || $sort_contract_status)
        {
            $taxonomies = $this->config->item('taxonomy');

            $this->datatable_builder
            ->join('term_posts', 'term_posts.post_id = posts.post_id')
            ->join('term', 'term.term_id = term_posts.term_id', 'LEFT')
            ->where_in('term.term_type', $taxonomies);

            if($filter_contract_status)
            {   
                $this->datatable_builder->where('term.term_status',$filter_contract_status);
            }

            if($sort_contract_status)
            {
                $this->datatable_builder->order_by('term.term_status',$sort_contract_status);
            }
        }

        if( ! empty($args['order_by']['amount']))
        {
            $alias = uniqid('amount_');
            $this->datatable_builder->join("postmeta {$alias}","{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'amount'")->order_by("({$alias}.meta_value * 1)",$args['order_by']['amount']);
        }

        if( ! empty($args['order_by']['vat_amount']))
        {
            $alias = uniqid('vat_amount_');
            $this->datatable_builder->join("postmeta {$alias}","{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'vat_amount'")->order_by("({$alias}.meta_value * 1)",$args['order_by']['vat_amount']);
        }

        if( ! empty($args['order_by']['service_fee']))
        {
            $alias = uniqid('service_fee_');
            $this->datatable_builder->join("postmeta {$alias}","{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'service_fee'")->order_by("({$alias}.meta_value * 1)",$args['order_by']['service_fee']);
        }

        if( ! empty($args['order_by']['actual_budget']))
        {
            $alias = uniqid('actual_budget_');
            $this->datatable_builder->join("postmeta {$alias}","{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'actual_budget'")->order_by("({$alias}.meta_value * 1)",$args['order_by']['actual_budget']);
        }

        if( ! empty($args['order_by']['fct_amount']))
        {
            $alias = uniqid('fct_amount_');
            $this->datatable_builder->join("postmeta {$alias}","{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'fct_amount'")->order_by("({$alias}.meta_value * 1)",$args['order_by']['fct_amount']);
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function export_receipt($query = '')
    {
        if(empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if(FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $posts = $this->receipt_m->query($query)->result();
        if( ! $posts)
        {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(),'refresh');
        }

        $this->load->library('excel');
        $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
        ->getProperties()
        ->setCreator("ADSPLUS.VN")
        ->setLastModifiedBy("ADSPLUS.VN")
        ->setTitle(uniqid('Danh sách thanh toán __'));

        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/receipt/receipt-list-export.xlsx');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $receipt_type_default       = $this->config->item('type','receipt');
        $transfer_type_default      = $this->config->item('transfer_type','receipt');
        $transfer_account_default   = $this->config->item('transfer_account','receipt');

        $row_index = 3;

        $posts = array_map(function($x){
            $x->term_id = null;
            $term_ids   = $this->term_posts_m->select('term_id')->get_the_terms($x->post_id)
            AND $x->term_id = reset($term_ids);
            return $x;
        }, $posts);

        $contracts = $this->contract_m->set_term_type()->where_in('term_id', array_unique(array_column($posts, 'term_id')))->get_many_by();
        ! empty($contracts) AND $contracts = array_column($contracts, NULL, 'term_id');

        $typeContracts      = array_group_by($contracts, 'term_type');
        $webdoctorContracts = [];

        ! empty($typeContracts['webdesign']) AND $webdoctorContracts = array_merge($typeContracts['webdesign']);
        ! empty($typeContracts['oneweb']) AND $webdoctorContracts = array_merge($typeContracts['oneweb']);

        $this->load->model('webgeneral/webgeneral_kpi_m');
        $kpis = array();

        if( ! empty($webdoctorContracts)
            && $targets = $this->webgeneral_kpi_m->select('kpi_id, term_id, user_id, kpi_type')->order_by('kpi_datetime')->where_in('term_id', array_column($webdoctorContracts, 'term_id'))->as_array()->get_all())
        {
            $targets = array_group_by($targets, 'term_id');

            foreach ($targets as $_term_id => $kpi)
            {
                $typeKpi = array_group_by($kpi, 'kpi_type');
                foreach ($typeKpi as $type => $_kpis)
                {
                    $contracts[$_term_id]->{$type} = implode(', ', array_map(function($_kpi){
                        return $this->admin_m->get_field_by_id($_kpi['user_id'], 'display_name') ?: $this->admin_m->get_field_by_id($_kpi['user_id'], 'user_email');
                    }, $_kpis));
                }
            }
        }

        $service_fee_payment_type = $this->config->item('service_fee_payment_type');
        foreach ($posts as $key => &$post)
        {
            $term = $contracts[$post->term_id];

            
            // Get customer display name
            $customer           = '';
            $cid                = null;
            $customer_tax       = null;
            $customer_address   = null;
            if($term_users      = $this->term_users_m->get_term_users($post->term_id, ['customer_person','customer_company','system']))
            {
                $customer           = end($term_users);
                $cid                = cid($customer->user_id, $customer->user_type);
                $customer_tax       = get_user_meta_value($customer->user_id, 'customer_tax');
                $customer_address   = get_user_meta_value($customer->user_id, 'customer_address');

                $customer           = mb_ucwords($this->admin_m->get_field_by_id($customer->user_id,'display_name'));
            }

            // Get contract code
            $contract_code = get_term_meta_value($post->term_id,'contract_code');
            
            $config_contract_status = $this->config->item('contract_status');
            $contract_status = $config_contract_status[$term->term_status] ?? '--';

            /* Get New|Renewal Contract */
            $is_first_contract = (bool) get_term_meta_value($term->term_id, 'is_first_contract');

            // Nhân viên kinh doanh phụ trách
            $sale_id    = (int) get_term_meta_value($post->term_id,'staff_business');
            $sale       = $this->admin_m->get_field_by_id($sale_id,'display_name') ?: ($this->admin_m->get_field_by_id($sale_id,'user_email') ?: '--');

            // Số tiền đã thu
            $post->amount = (double) get_post_meta_value($post->post_id,'amount');

            switch ($post->post_type) {
                
                case 'receipt_payment_on_behaft':
                    $vat = 0; /* VAT của hợp đồng */
                    $post->amount_not_vat = $post->amount; /* Doanh thu (-VAT) */
                    break;
                
                default:
                    $vat = (int) get_term_meta_value($post->term_id,'vat'); /* VAT của hợp đồng */
                    $post->amount_not_vat = div($post->amount, (1+div($vat, 100))); /* Doanh thu (-VAT) */
                    break;
            }


            // Loại hình thanh toán
            $transfer_type = get_post_meta_value($post->post_id,'transfer_type');
            $transfer_account = get_post_meta_value($post->post_id,'transfer_account');
            $transfer_by = $transfer_type_default[$transfer_type].' '.($transfer_account_default[$transfer_account] ?? $transfer_account);

            $row_number = $row_index + $key;

            $date = $this->mdate->endOfDay($post->end_date);

            $i = 0;

            // Set Cell
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, PHPExcel_Shared_Date::PHPToExcel($date));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $customer);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $cid);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $customer_tax);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $customer_address);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_code);

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_status);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $service_fee_payment_type[get_term_meta_value($post->term_id,'service_fee_payment_type')] ?? '--');

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $post->amount);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $sale);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, div($vat,100));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $post->amount_not_vat);

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (double) get_post_meta_value($post->post_id, 'vat_amount'));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (double) get_post_meta_value($post->post_id, 'service_fee_rate'));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (double) get_post_meta_value($post->post_id, 'service_fee'));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (double) get_post_meta_value($post->post_id, 'service_provider_tax'));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (double) get_post_meta_value($post->post_id, 'fct_amount'));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, (double) get_post_meta_value($post->post_id, 'actual_budget'));

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $this->config->item($term->term_type,'taxonomy'));
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $transfer_by);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $receipt_type_default[$post->post_type]);

            if(in_array($term->term_type, ['webdesign', 'oneweb']))
            {
                $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $term->tech ?? '');
                $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $term->design ?? '');
            }
            else $i+=2;

            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $is_first_contract ? 'Ký mới' : 'Tái Ký' );
        }

        $numbers_of_post = count($posts);

        // Set Cells style for Date
        $objPHPExcel->getActiveSheet()
            ->getStyle('A'.$row_index.':A'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY); // Set Cells style for Date
        $objPHPExcel->getActiveSheet()
            ->getStyle('A'.$row_index.':A'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        // Set Cells Style For Amount
        $objPHPExcel->getActiveSheet()
            ->getStyle('I'.$row_index.':I'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        // Set Cells Style For VAT
        $objPHPExcel->getActiveSheet()
            ->getStyle('K'.$row_index.':K'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);

        // Set Cells Style For Amount without VAT
        $objPHPExcel->getActiveSheet()
            ->getStyle('L'.$row_index.':Q'.($numbers_of_post+$row_index))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        // Calc sum amount rows and sum amout not vat rows
        $total_amount = array_sum(array_column($posts, 'amount'));
        $total_amount_not_vat = array_sum(array_column($posts, 'amount_not_vat'));

        // Set cell label summary
        $objWorksheet->setCellValueByColumnAndRow(1, ($row_index + $numbers_of_post + 2), 'Tổng cộng');

        // Set cell sumary amount and amount not vat on TOP
        $objWorksheet->setCellValueByColumnAndRow(8, 1, $total_amount);
        $objWorksheet->setCellValueByColumnAndRow(11, 1, $total_amount_not_vat);

        // Set cell sumary amount and amount not vat on BOTTOM
        $objWorksheet->setCellValueByColumnAndRow(8, ($row_index + $numbers_of_post + 2), $total_amount);
        $objWorksheet->setCellValueByColumnAndRow(11, ($row_index + $numbers_of_post + 2), $total_amount_not_vat);
        
        // Set Cells Style For Amount without VAT
        $objPHPExcel->getActiveSheet()
            ->getStyle('I'.($numbers_of_post+$row_index+2))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        // Set Cells Style For Amount without VAT
        $objPHPExcel->getActiveSheet()
            ->getStyle('L'.($numbers_of_post+$row_index+2))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $file_name = "tmp/export-du-thu.xlsx";
        $objWriter->save($file_name);

        try { $objWriter->save($file_name); }
        catch (Exception $e) { trigger_error($e->getMessage()); return FALSE;  }

        if(file_exists($file_name))
        {   
            $this->load->helper('download');
            force_download($file_name,NULL);
        }

        return FALSE;
    }
}
/* End of file DatasetReceipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetReceipts.php */