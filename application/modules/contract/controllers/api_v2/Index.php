<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'vendor/autoload.php';

class Index extends MREST_Controller
{
    /**
     * API dữ liệu các đợt thanh toán
     */
    public function receipts_get()
    {
        if( ! has_permission('contract.receipt.access')) parent::response(['status'=>true,'data' => []]);

    	$this->load->model('contract/receipt_m');

        $endDateStart  = parent::get('endDateStart', TRUE) ? start_of_day(parent::get('endDateStart', TRUE)) : start_of_month();
        $endDateEnd    = parent::get('endDateEnd', TRUE) ? end_of_day(parent::get('endDateEnd', TRUE)) : end_of_month();

    	$receipts = $this->receipt_m
    	->select('posts.post_id,post_author,post_title,post_status,created_on,updated_on,post_type,end_date')
    	->select('term_posts.term_id')
    	->join('term_posts', 'term_posts.post_id = posts.post_id')
    	->where('end_date >=', $endDateStart)
    	->where('end_date <=', $endDateEnd)
    	->where('post_status', 'paid')
    	->set_post_type()
    	->as_array()
    	->get_all();

    	if( ! $receipts) parent::response(['status'=>true,'data'=>[]]);

    	$this->load->config('contract/receipt');
    	$this->load->config('contract/contract');
    	$this->load->model('contract/contract_m');
    	
    	$contract_ids 	= array_unique(array_column($receipts, 'term_id'));
    	$contracts 		= $this->contract_m->select('term_id,term_type')->set_term_type()->where_in('term_id', $contract_ids)->as_array()->get_all();

		$contracts 		= $contracts ? array_column($contracts, NULL, 'term_id') : [];

		$receipt_type_default = $this->config->item('type','receipt');
        $transfer_type_default = $this->config->item('transfer_type','receipt');
		$transfer_account_default = $this->config->item('transfer_account','receipt');

		$this->load->model('staffs/department_m');
		$this->load->model('staffs/user_group_m');

		/* Lấy tất cả phòng ban và convert về dạng array với key = id và value = tên phòng ban */
		$department_cache_key = 'modules/staffs/departments';
		$departments = $this->scache->get($department_cache_key);
		$departments = $departments ? unserialize($departments) : [];
		if( ! $departments && $departments = $this->department_m->select('term_id,term_name')->set_term_type()->as_array()->get_all())
		{
			$departments = $departments ? key_value($departments, 'term_id', 'term_name') : [];
			$this->scache->write(serialize($departments), $department_cache_key, 3600);
		}

		/* 
		 * Lấy tất cả nhóm và convert về dạng array với key = id và value = tên phòng ban
		 * Đồng thời group từng nhóm lại theo đúng tên phòng ban chứa nhóm đấy
		 */
		$user_groups_cache_key = 'modules/staffs/user_groups';
		$user_groups = $this->scache->get($user_groups_cache_key);
		$user_groups = $user_groups ? unserialize($user_groups) : [];
		if( ! $user_groups && $user_groups = $this->user_group_m->select('term_id,term_name,term_parent')->set_term_type()->as_array()->get_all())
		{
			$_user_groups = array();
			foreach (array_group_by($user_groups, 'term_parent') as $department_id => $groups)
			{
				if(empty($departments[$department_id])) continue;

				$_user_groups[$departments[$department_id]] = key_value($groups, 'term_id', 'term_name');
			}

			$user_groups = $_user_groups;
			$this->scache->write(serialize($user_groups), $user_groups_cache_key, 3600);
		}

    	foreach ($receipts as &$receipt)
    	{
    		$receipt['amount'] = (double) get_post_meta_value($receipt['post_id'], 'amount');
    		$receipt['contract_code'] = get_term_meta_value($receipt['term_id'], 'contract_code');

    		if($sale_id = (int) get_term_meta_value($receipt['term_id'], 'staff_business'))
    		{
    			$receipt['sale_id'] = (int) $sale_id;
    			$receipt['sale_email'] = $this->admin_m->get_field_by_id($sale_id, 'user_email');
    			$receipt['sale_display_name'] = $this->admin_m->get_field_by_id($sale_id, 'display_name');
    		}

    		$receipt['vat'] = (int) get_term_meta_value($receipt['term_id'], 'vat');
			$receipt['amount_not_vat'] = $receipt['amount'] - $receipt['amount']*div($receipt['vat'], 100); // Doanh thu (-VAT)
			$receipt['contract_type'] = $this->config->item($contracts[$receipt['term_id']]['term_type'], 'taxonomy');

            $receipt['service_fee']     = (int) get_post_meta_value($receipt['post_id'], 'service_fee');
            $receipt['actual_budget']   = (int) get_post_meta_value($receipt['post_id'], 'actual_budget');
            $receipt['vat_amount']      = (int) get_post_meta_value($receipt['post_id'], 'vat_amount');
            $receipt['amount_not_vat']  = $receipt['amount'] - $receipt['vat_amount'];
            $receipt['is_seftpay_customer'] = (int) ('customer' == get_term_meta_value($receipt['term_id'], 'contract_budget_payment_type') && 'normal' == get_term_meta_value($receipt['term_id'], 'contract_budget_customer_payment_type'));

			// Loại hình thanh toán
			$receipt['transfer_by'] 		= get_post_meta_value($receipt['post_id'], 'transfer_type');
			$receipt['transfer_account'] 	= get_post_meta_value($receipt['post_id'], 'transfer_account');
			$receipt['transfer_by'] 		= $transfer_type_default[$receipt['transfer_by']].' '.($transfer_account_default[$receipt['transfer_account']] ?? $receipt['transfer_account']);

			$receipt['receipt_type'] = $receipt_type_default[$receipt['post_type']];

			$user_groups = $this->term_users_m->get_user_terms($sale_id, $this->user_group_m->term_type);
			if( ! empty($user_groups)) $receipt['sale_group'] = implode(', ', array_column($user_groups, 'term_name'));

			$departments = $this->term_users_m->get_user_terms($sale_id, $this->department_m->term_type);
			if( ! empty($departments)) $receipt['sale_department'] = implode(', ', array_column($departments, 'term_name'));
    	}

    	parent::response(['status'=>TRUE, 'data' => $receipts]);
    }

    public function view_get($contracts = 0)
    {
        $this->load->model('contract/contract_m');

        $this->load->library('form_validation');
        $this->form_validation->set_data(parent::get(NULL, TRUE));
        $this->form_validation->set_rules('contracts', 'ID Hợp đồng', 'required|integer');

        if ($this->form_validation->run() == FALSE)
        {
            $_errors = $this->form_validation->error_array();
            $_errors = reset($_errors);
            parent::response([
                'code' => 400, 
                'error' => $_errors
            ], 200);
        }

        $contractId = (int) parent::get('contracts', TRUE);

        if( ! $this->contract_m->set_contract($contractId)) parent::response('Truy xuất không khả dụng [1]', parent::HTTP_NOT_FOUND);

        if( ! $this->contract_m->can('admin.contract.view')) parent::response('Không có quyền truy cập thông tin hợp đồng này', parent::HTTP_UNAUTHORIZED);


        $contract = $this->contract_m->get_contract();
        $contract->is_editable_locked  = (bool) get_term_meta_value($contractId,'lock_editable');

        $contract->contract_code = get_term_meta_value($contractId, 'contract_code');

        parent::response(['status'=>TRUE, 'data' => $contract]);
    }

    /**
     * API dữ liệu hợp đồng
     */
    public function list_get()
    {
        if( ! has_permission('admin.contract.view')) parent::response(['status' => TRUE, 'data' => []]);

        $relate_users = $this->admin_m->get_all_by_permissions('admin.contract.view');
        if($relate_users === FALSE) parent::response(['status' => TRUE, 'data' => []]); // LOGGED USER NOT HAS PERMISSION TO ACCESS
        
        $this->load->model('contract/contract_m');
        is_array($relate_users) AND $this->contract_m->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);

        $startServiceTimeStart  = parent::get('start_service_time_start', TRUE) ? start_of_day(parent::get('start_service_time_start', TRUE)) : start_of_month();
        $startServiceTimeEnd    = parent::get('start_service_time_end', TRUE) ? end_of_day(parent::get('start_service_time_end', TRUE)) : end_of_month();
        $m_args = ['key' => 'start_service_time', 'value' => [$startServiceTimeStart, $startServiceTimeEnd], 'compare' => 'BETWEEN'];

    	$contracts = $this->contract_m->set_term_type()
    	->select('term.term_id,term_name,term_type,term_parent,term_status')
    	->m_find(['key'=>'start_service_time', 'value'=>[$startServiceTimeStart, $startServiceTimeEnd], 'compare'=>'BETWEEN'])
    	->as_array()
    	->get_all();

    	if( ! $contracts) parent::response(['status'=>true,'data'=>[]]);

        $this->load->model('staffs/user_group_m');
        $this->load->model('staffs/department_m');
    	$this->config->load('contract/contract');

    	foreach ($contracts as &$contract)
    	{
    		$term_id = $contract['term_id'];

    		$contract['start_service_time_year'] 	= my_date($contract['start_service_time'], 'Y');
    		$contract['start_service_time_month'] 	= my_date($contract['start_service_time'], 'm');
    		$contract['start_service_time_week'] 	= my_date($contract['start_service_time'], 'W');
    		$contract['start_service_time_date'] 	= my_date($contract['start_service_time'], 'Ymd');

    		$contract['contract_type']    = $this->config->item($contract['term_type'], 'taxonomy');
    		$contract['contract_code']    = get_term_meta_value($term_id, 'contract_code');
    		$contract['contract_budget']  = (double) get_term_meta_value($term_id, 'contract_budget');
    		$contract['contract_value']	= (double) get_term_meta_value($term_id, 'contract_value');
    		$contract['service_fee']	= (double) get_term_meta_value($term_id, 'service_fee');
    		$contract['is_first_contract']	= (bool) get_term_meta_value($term_id, 'is_first_contract') ? 'Ký mới' : 'Tái ký';

    		$sale_id = (int) get_term_meta_value($term_id, 'staff_business');
    		$contract['sale'] = array( 'id' => $sale_id);

    		if( ! empty($sale_id))
    		{
				$contract['sale']['id'] = (int) get_term_meta_value($term_id, 'staff_business');
				$contract['sale']['display_name'] = $this->admin_m->get_field_by_id($sale_id, 'display_name');
				$contract['sale']['email'] = $this->admin_m->get_field_by_id($sale_id, 'user_email');
    		}

    		$contract['contract_begin'] = (int) get_term_meta_value($term_id, 'contract_begin');
            $contract['contract_end']   = (int) get_term_meta_value($term_id, 'contract_end');

            $contract['vat']   = (int) get_term_meta_value($term_id, 'vat');

            $contract['contract_begin_date'] = my_date($contract['contract_begin'], 'Ymd');
    		$contract['contract_end_date'] 	= my_date($contract['contract_end'], 'Ymd');

    		$_contract_begin 	= ( new DateTime())->setTimestamp($contract['contract_begin']);
			$_contract_end 		= ( new DateTime())->setTimestamp($contract['contract_end']);
			$interval = $_contract_end->diff($_contract_begin);

			$contract['contract_days'] = (int) $interval->days;
			$contract['contract_budget_per_month'] = (double) div($contract['contract_budget'], $contract['contract_days']);

            $user_groups = $this->term_users_m->get_user_terms($sale_id, $this->user_group_m->term_type);
            if( ! empty($user_groups)) $contract['sale_group'] = implode(', ', array_column($user_groups, 'term_name'));

            $departments = $this->term_users_m->get_user_terms($sale_id, $this->department_m->term_type);
            if( ! empty($departments)) $contract['sale_department'] = implode(', ', array_column($departments, 'term_name'));
    	}

    	parent::response(['status'=>TRUE, 'data' => $contracts]);
    }

    public function content_get($contracts)
    {
        $this->load->model('contract/contract_m');
        $this->load->model('contract/content_m');

        $this->load->library('form_validation');
        $this->form_validation->set_data(parent::get(NULL, TRUE));
        $this->form_validation->set_rules('contracts', 'ID Hợp đồng', 'required|integer');

        if ($this->form_validation->run() == FALSE)
        {
            $_errors = $this->form_validation->error_array();
            $_errors = reset($_errors);
            parent::response([
                'code' => 400, 
                'error' => $_errors
            ], 200);
        }

        $contractId = parent::get('contracts', TRUE);
        $this->contract_m->set_contract($contractId);

        if( ! $this->contract_m->set_contract($contractId) || ! $this->contract_m->can('admin.contract.view'))
        {
            parent::response('Truy xuất không khả dụng [1]', parent::HTTP_UNAUTHORIZED);
        }

        $posts = $this->term_posts_m->get_term_posts($contractId, $this->content_m->post_type);
        if( ! $posts)
        {
            parent::response(['status'=>TRUE, 'data' => 204], parent::HTTP_NO_CONTENT);
        }

        parent::response(['status'=>TRUE, 'data' => reset($posts)]);
    }

    public function content_put($contracts, $contents)
    {
        $this->load->model('contract/contract_m');
        $this->load->model('contract/content_m');

        $this->load->library('form_validation');

        /**
         * Cập nhật Post 
         * Cập nhật meta -> ghi chú giá trị đã thay đổi
         */
        $_rules = array(
            [
                'field' => 'content',
                'label' => 'Nội dung',
                'rules' => 'required'
            ],
            [
                'field' => 'id',
                'label' => 'Id Section',
                'rules' => 'required'
            ]
        );

        $data = array_merge(parent::get(null, true), parent::put(null, true));

        $postContents = $data['content'] ?? [];

        foreach ($postContents as $_postContent)
        {
            $this->form_validation->set_data($_postContent);
            $this->form_validation->set_rules($_rules);

            if ($this->form_validation->run() == FALSE)
            {
                $_errors = $this->form_validation->error_array();
                $_errors = reset($_errors);
                parent::response([ 'code' => 400, 'error' => $_errors ], 200);
            }
        }

        $contractId = parent::get('contracts', TRUE);
        $contentId  = parent::get('contents', TRUE);
        if(empty($contentId))
        {
            $contentId = $this->content_m->insert([
                'post_title' => get_term_meta_value($contractId, 'contract_code'),
                'post_type' => $this->content_m->post_type,
                'created_on' => time(),
                'post_author' => $this->admin_m->id
            ]);

            $_contract_content = $this->content_m->get($contentId);

            $this->term_posts_m->set_term_posts( $contractId, array($contentId), $this->content_m->post_type, FALSE);
        }

        $post       = $this->content_m->set_type()->get($contentId);

        if( ! $post || ! $this->contract_m->set_contract($contractId))
        {
            parent::response('Truy xuất không khả dụng [0]', parent::HTTP_UNAUTHORIZED);
        }

        $this->contract_m->set_contract($contractId);

        $contract = $this->term_posts_m->get_post_terms($contentId, $this->contract_m->get_contract()->term_type);
        if( ! $contract)
        {
            parent::response('Truy xuất không khả dụng [1]', parent::HTTP_UNAUTHORIZED);
        }

        $contract AND $contract = reset($contract);

        if(FALSE === $this->contract_m->has_permission($contract->term_id, 'admin.contract.view'))
        {
            parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng [1]', parent::HTTP_UNAUTHORIZED);
        }

        $this->content_m->update($contentId, [
            'post_content'  => serialize($postContents),
            'updated_on'    => time()
        ]);

        update_term_meta($contractId, 'is_rewrited', 1);

        list( $folder_upload, $i) = modules::find('contract/admin/preview/custom/index', 'contract', 'views/');

        $custom_printable_view_path  = "contract/admin/preview/custom/index_{$contractId}";
        
        list($path, $view) = modules::find($custom_printable_view_path, 'contract', 'views/');
            
        // If file not exist , then clone new one from original template
        if(empty($path))
        {
            $this->load->model('contract/contract_m');
            $this->contract_m->set_contract($contractId);
            $data = $this->contract_m->prepare_preview();

            $_view_file = $data['view_file'];

            // Read Existed File
            list($_path, $_view)    = modules::find($_view_file,'contract', 'views/');
            $phpFile                = implode([ $_path, $_view, '.php']);
            $customPHP              = read_file($phpFile);

            // Create new template
            if( ! is_dir($folder_upload))
            {
                try 
                {
                    $oldmask = umask(0);
                    mkdir($folder_upload, 0777, TRUE);
                    umask($oldmask);
                }
                catch (Exception $e)
                {
                    trigger_error($e->getMessage());
                    return FALSE;
                }
            }

            $new_filename = "index_{$contractId}";

            write_file("{$folder_upload}{$new_filename}.php", $customPHP);

            $custom_printable_view_path = "contract/admin/preview/custom/{$new_filename}";

            update_term_meta($contractId, 'custom_printable_view_path', $custom_printable_view_path);
        }

        list($path, $view)  = modules::find($custom_printable_view_path, 'contract', 'views/');
        $phpFile            = implode([ $path, $view, '.php']);
        $content_rewrited   = read_file($phpFile);

        if( ! empty($postContents))
        {
            foreach ($postContents as $content)
            {
                $matches = [];
                preg_match('/contenteditable="true" id="'.$content['id'].'">(.*?)<\/div>/s', $content_rewrited, $matches);
                if(empty($matches)) continue;

                $content_rewrited = str_replace($matches[1], $content['content'], $content_rewrited);
            }
        }

        $new_filename = "index_{$contractId}";
        write_file("{$folder_upload}{$new_filename}.php", $content_rewrited);

        $this->scache->delete_group("term/{$contract->term_id}_posts_");

        parent::response([ 'code' => 200, 'message' => 'Xử lý thành công' ], 200); 
    }

    public function content_delete($contracts)
    {
        $this->load->library('form_validation');
        $this->load->model('contract/contract_m');

        $contractId = parent::get('contracts', TRUE);
        if( FALSE == $this->contract_m->set_contract($contractId))
        {
            parent::response('Truy xuất không khả dụng [0]', parent::HTTP_UNAUTHORIZED);
        }

        $contract = $this->contract_m->get_contract();

        if(FALSE === $this->contract_m->has_permission($contract->term_id, 'admin.contract.view'))
        {
            parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng [1]', parent::HTTP_UNAUTHORIZED);
        }

        list( $folder_upload, $i) = modules::find('contract/admin/preview/custom/index', 'contract', 'views/');

        $custom_printable_view_path  = "contract/admin/preview/custom/index_{$contractId}";
        
        list($path, $view) = modules::find($custom_printable_view_path, 'contract', 'views/');

        $phpFile = implode([ $path, $view, '.php']);

        if (is_readable($phpFile) && unlink($phpFile))
        {
            update_term_meta($contractId, 'custom_printable_view_path', '');
            update_term_meta($contractId, '', 'is_rewrited', 0);
            parent::response([ 'code' => 200, 'message' => 'Khôi phục phiên bản gốc thành công' ], parent::HTTP_OK);
        }

        parent::response([ 'code' => 500, 'error' => "Khôi phục không thành công. Vui lòng thử lại." ], 200);
    }
}
/* End of file Index.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Index.php */