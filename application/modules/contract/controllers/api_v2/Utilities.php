<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Utilities extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model([ 'googleads/googleads_m']);
        $this->load->model('contract/contract_m');
    }

    /**
     * /GET
     *
     * @param      string  $start  The start
     * @param      string  $end    The end
     */
    public function novat_get($start = '', $end = '')
    {
        $start_date = DateTime::createFromFormat('Y-m-d', $start);
        $end_date   = DateTime::createFromFormat('Y-m-d', $end);
        if(empty($start_date) || empty($start_date)) parent::response(NULL, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);

        parent::response(['code' => REST_Controller::HTTP_OK, 'data' => self::get_novat_contracts(start_of_day($start_date->getTimestamp()), end_of_day($end_date->getTimestamp())) ]);
    }

    /**
     * /PUT
     *
     * @param      string   $start  The start
     * @param      string   $end    The end
     * @param      integer  $value  The value
     */
    public function novat_put($start = '', $end = '', $value = 10)
    {
        $start_date = DateTime::createFromFormat('Y-m-d', $start);
        $end_date   = DateTime::createFromFormat('Y-m-d', $end);
        if(empty($start_date) || empty($start_date)) parent::response(NULL, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);

        $contracts = self::get_novat_contracts(start_of_day($start_date->getTimestamp()), end_of_day($end_date->getTimestamp()));

        if( ! $contracts) parent::response(['code' => REST_Controller::HTTP_OK]);
        foreach ($contracts as $contract)
        {
            update_term_meta($contract->term_id, 'vat', $value);
        }

        parent::response(['code' => REST_Controller::HTTP_OK]);
    }

    /**
     * Gets the novat contracts.
     *
     * @param      <type>  $start  The start
     * @param      <type>  $end    The end
     *
     * @return     <type>  The novat contracts.
     */
    protected function get_novat_contracts($start, $end)
    {
        return $this->googleads_m
        ->select('term.term_id, term.term_type, term.term_status')
        ->set_term_type()
        ->m_find([  [ 'key' => 'contract_begin', 'compare' => 'BETWEEN', 'value' => [ $start, $end ] ], [ 'key' => 'vat', 'compare' => '=', 'value' => 0 ] ])
        ->get_all();
    }

    /**
     * @param mixed $value
     * 
     * @return [type]
     */
    public function get_contracts_by_contract_code_get()
    {
        $value = parent::get('value', TRUE);

        $this->load->model('contract/contract_m');

        $isExisted = $this->termmeta_m
        ->where('meta_key', 'contract_code')
        ->like('meta_value', $value)
        ->count_by() > 0;

        if( ! $isExisted)
        {
            parent::responseHandler(false, 'Không có kết quả phù hợp', 'error', 404);
        }

        $found = $this->termmeta_m
        ->select('term_id')
        ->where('meta_key', 'contract_code')
        ->like('meta_value', $value)
        ->get_all();

        $foundIds = array_column($found, 'term_id');

        $contracts = $this->contract_m
        ->select('term_id,term_type,term_status')
        ->set_term_type()
        ->where_in('term_id', $foundIds)
        ->order_by('term_id', 'desc')
        ->get_all();

        $contracts = array_map(function($x){
            $x->term_id = (int) $x->term_id;
            $x->contract_code = get_term_meta_value($x->term_id, 'contract_code');
            $x->service_fee_rate_actual = (double) get_term_meta_value($x->term_id, 'service_fee_rate_actual');
            $x->contract_budget_payment_type = get_term_meta_value($x->term_id, 'contract_budget_payment_type') ?: 'normal';
            $x->vat = (double) div((int) get_term_meta_value($x->term_id, 'vat'), 100);
            $x->fct = (double) div((int) get_term_meta_value($x->term_id, 'fct'), 100);
            $x->service_provider_tax_rate = (float) get_term_meta_value($x->term_id, 'service_provider_tax_rate');
            return $x;
        }, $contracts);

        parent::responseHandler($contracts, 'OK');
    }

    public function latest_contracts_get()
    {
        $contracts = $this->contract_m
        ->set_term_type()
        ->select('term_id,term_type,term_status')
        ->where_not_in('term.term_status', [ 'draft', 'unverified' ])
        ->order_by('term_id', 'desc')
        ->limit(500)
        ->get_all();

        $this->load->model('contract/receipt_m');
        $contract_ids = array_unique(array_column($contracts, 'term_id'));
        $num_of_receipts_by_contract = $this->receipt_m
        ->set_post_type()
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->where_in('term_posts.term_id', $contract_ids)
        ->group_by('term_posts.term_id')
        
        ->select('term_posts.term_id')
        ->select('COUNT(posts.post_id) AS num_of_receipt')

        ->get_all('posts.post_id');
        $num_of_receipts_by_contract = key_value($num_of_receipts_by_contract, 'term_id', 'num_of_receipt');

        $this->load->model('staffs/department_m');
        $contracts = array_map(function($x) use ($num_of_receipts_by_contract){
            $x->term_id = (int) $x->term_id;
            $x->contract_code = get_term_meta_value($x->term_id, 'contract_code');
            $x->contract_budget_payment_type = get_term_meta_value($x->term_id, 'contract_budget_payment_type') ?: 'normal';
            $x->vat = (double) div((int) get_term_meta_value($x->term_id, 'vat'), 100);
            $x->fct = (double) div((int) get_term_meta_value($x->term_id, 'fct'), 100);
            
            $x->service_provider_tax_rate = (float) get_term_meta_value($x->term_id, 'service_provider_tax_rate');

            $x->service_fee_payment_type = get_term_meta_value($x->term_id, 'service_fee_payment_type') ?: 'full';

            $actual_result = (double) get_term_meta_value($x->term_id, 'actual_result') ?: 0;
            $balance_spend = (double) get_term_meta_value($x->term_id, 'balance_spend') ?: 0;
            $result = $actual_result + $balance_spend;
            $x->actual_result = $result;

            $x->service_fee_rate_actual = (double) get_term_meta_value($x->term_id, 'service_fee_rate_actual') ?: 0;
            $x->service_fee = (double) get_term_meta_value($x->term_id, 'service_fee') ?: 0;

            $x->sale = null;
            $x->department_staffs = (array) [];
            $x->can_select_staff_business = (bool) (($num_of_receipts_by_contract[$x->term_id] ?? 0) > 0);
            
            $staff_business = get_term_meta_value($x->term_id, 'staff_business') ?: 0;
            if(!empty($staff_business))
            {
                $sale_data = elements(
                    ['user_id', 'display_name', 'user_email'], 
                    $this->admin_m->get_field_by_id($staff_business)
                );

                $sale_data['user_id'] = (int) $sale_data['user_id'];

                $sale_data['departments'] = null;
                $departments = $this->term_users_m->get_user_terms($staff_business, $this->department_m->term_type);
                if(!empty($departments))
                {
                    $sale_data['departments'] = array_column($departments, 'term_name');
                    $sale_data['departments'] = reset($sale_data['departments']);
                }
                
                $x->sale = $sale_data;
                
                if( $x->can_select_staff_business 
                    && !empty($departments))
                {
                    $department_id = array_keys($departments);
                    $department_id = reset($department_id);
                    $x->department_staffs = $this->department_m->get_staffs($department_id);
                }
            }

            return $x;
        }, $contracts);

        parent::responseHandler($contracts, 'OK');
    }
}
/* End of file Index.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Index.php */