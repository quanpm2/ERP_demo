<?php defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Customers extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'googleads/googleads_kpi_m';
        $this->autoload['models'][] = 'customer/customer_m';
        parent::__construct($config);

        $this->load->config('contract/receipt');

    }

    /**
     * Export List Customer With First Contracts 
     *
     * @return     bool  ( description_of_the_return_value )
     */
    public function index_get()
    {
        /* Enable Benchmark to view statistic */
        ENVIRONMENT !== 'production' AND $this->output->enable_profiler(TRUE);

        $cacheKey   = "modules/contracts/googleads/" . start_of_day();
        $taxonomies = [ 'google-ads', 'facebook-ads' ];

        $data = $this->scache->get($cacheKey);

        if(empty($data))
        {   
            $customers      = $this->customer_m
            ->select('user.user_id, user_type, display_name, user_email, user_status')
            ->set_user_type()
            ->get_all();

            if(empty($customers)) return false;

            $contracts = $this->contract_m
            ->select('term.term_id, term_type, term_status, term_users.user_id')
            ->join('term_users', 'term_users.term_id = term.term_id')
            ->where_in('term_type', $taxonomies)
            ->where_in('term_users.user_id', array_column($customers, 'user_id'))
            ->get_all();

            $contractsGroupByUserId = array_group_by($contracts, 'user_id');

            $data = array();

            foreach ($customers as $key => $customer)
            {
                if(empty($contractsGroupByUserId[$customer->user_id])) continue;

                $_contracts = array_map(function($x){
                    
                    $x->start_service_time = (int) get_term_meta_value($x->term_id, 'start_service_time');

                    switch ($x->term_type) {
                        case 'facebook-ads':
                            $x->advertise_end_time = (int) get_term_meta_value($x->term_id, 'advertise_end_time');
                            break;
                        default:
                            $x->advertise_end_time = (int) get_term_meta_value($x->term_id, 'googleads-end_time');
                            break;
                    }

                    $x->contract_code = get_term_meta_value($x->term_id, 'contract_code');
                    return $x;
                }, $contractsGroupByUserId[$customer->user_id]);

                $_contracts = array_filter($_contracts, function($x){
                    if( ! in_array($x->term_status, ['pending', 'publish', 'ending', 'liquidation'])) return false;
                    return ! empty($x->start_service_time);
                });

                if(empty($_contracts)) continue;

                $_contracts = array_group_by($_contracts, 'term_type');

                $customer->contracts = array();

                foreach ($_contracts as $_type => $items)
                {
                    usort($items, function($a, $b){
                        if(empty($a->start_service_time)) return 0;
                        return $a->start_service_time < $b->start_service_time ? -1 : 1;
                    });

                    $_firstContract             = reset($items);
                    $_firstContract->lifeCircle = 'isFirstStarted';
                    $customer->contracts[]      = $_firstContract;


                    $_completedContracts        = array_filter($items, function($x){ return in_array($x->term_status, ['ending', 'liquidation']); });
                    if(empty($_completedContracts)) continue;

                    usort($_completedContracts, function($a, $b){
                        if(empty($a->advertise_end_time)) return 0;
                        return $a->advertise_end_time < $b->advertise_end_time ? -1 : 1;
                    });

                    $_lastContract              = end($items);
                    $_lastContract->lifeCircle = 'isLastCompleted';
                    $customer->contracts[]      = $_lastContract;
                }

                $data[] = $customer;
            }

            $this->scache->write($data, $cacheKey, 900);
        }

        $title          = 'Export dữ liệu hợp đồng đầu tiên của khách hàng - Ngày tạo ' . my_date(time(), 'Y-m-d');
        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));
        
        $sheet = $spreadsheet->getActiveSheet();
        $rowIndex = 1;

        $headings = [
            'Mã Khách Hàng',
            'Tên Khách hàng',
            'Loại',
            'EMail Khách hàng',
        ];

        foreach ($taxonomies as $taxonomy)
        {
            $headings[] = "[$taxonomy] Mã HĐ";
            $headings[] = "[$taxonomy] Ngày Kích hoạt";
            $headings[] = "[$taxonomy] Mã HĐ";
            $headings[] = "[$taxonomy] Ngày Kết ";
        }

        $sheet->fromArray($headings, NULL, "A{$rowIndex}");

        $rowIndex++;

        foreach ($data as $item)
        {
            $col = 1;

            $customer_email = get_user_meta_value($item->user_id, 'customer_email');
            $customer_phone = get_user_meta_value($item->user_id, 'customer_phone');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, cid($item->user_id, $item->user_type));
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $item->display_name);
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $item->user_type == 'customer_company' ? 'Doanh nghiệp' : 'Cá Nhân');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $customer_email);

            $contracts = array_group_by($item->contracts, 'term_type');
            $_max_cols = 4;
            
            foreach ($taxonomies as $taxonomy)
            {
                if(empty($contracts[$taxonomy]))
                {
                    $col+=$_max_cols;
                    continue;
                }

                $_contracts = $contracts[$taxonomy];
                $_contracts = array_column($_contracts, null, 'lifeCircle');

                if(empty($_contracts['isFirstStarted'])) $col+=2;
                else
                {
                    $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $_contracts['isFirstStarted']->contract_code);
                    $sheet->setCellValueByColumnAndRow($col++, $rowIndex, my_date($_contracts['isFirstStarted']->start_service_time));
                }

                if(empty($_contracts['isLastCompleted'])) $col+=2;
                else
                {
                    $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $_contracts['isLastCompleted']->contract_code);
                    $sheet->setCellValueByColumnAndRow($col++, $rowIndex, my_date($_contracts['isLastCompleted']->advertise_end_time));
                }
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/customers/contract/report/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file Customers.php */
/* Location: ./application/modules/contract/controllers/api_v2/Customers.php */