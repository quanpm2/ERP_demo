<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices extends Admin_Controller
{	
	function __construct()
	{
		parent::__construct();

		$models = ['invoice_m','invoice_item_m','base_contract_m','staffs/sale_m'];
		$this->load->model($models);

		$this->config->load('invoice');
		$this->config->load('contract');
	}

	public function index()
	{
		// restrict('Invoices.Queue_waiting');
		$data = array();

		$this->search_filter();
		$invoice_status = $this->config->item('status','invoice');

		$this->admin_ui
		->select('posts.post_id,post_type,term.term_id,term.term_status')
		->add_search('term.term_id')
		->add_search('term.term_name')
		->add_search('posts.post_title')
		
		->add_search('start_date',
				array('content'=> form_input(array(
					'name'	=>	'where[start_date]',
					'class'=>'form-control 	input_daterange'),
					$this->input->get('where[start_date]')))
				)
		->add_search('end_date',
				array('content'=> form_input(array(
					'name'	=>	'where[end_date]',
					'class'=>'form-control 	input_daterange'),
					$this->input->get('where[end_date]')))
				)
		
		->add_column('term.term_id', '#ID HĐ')

		->add_column('posts.post_title', 'Tiêu đề')
		->add_column_callback('start_date',function($data,$row_name){

			$post_id = $data['post_id'];
			$post_title = $data['post_title'];
			$result = anchor(module_url('invoices/view/'.$post_id), ' '.$post_title);

			$post_status = $data['post_status'];
			if($post_status == 'unpaid')
			{	
				$result.= br()."<span class='label label-warning'>Không thanh toán</span>";
				$data['post_title'] = $result;
				return $data;
			}

			if($post_status == 'paid')
			{
				$invoice_paid_date = '';
				if($invoice_paid_time = (int) get_post_meta_value($post_id,'invoice_paid_time'))
				{
					$invoice_paid_date = my_date($invoice_paid_time,'d/m/Y');
				}
				
				$result.= br()."<span class='label label-primary'>Đã thanh toán {$invoice_paid_date}</span>";
				$data['post_title'] = $result;
				return $data;
			}
			
			$time = time();
			$message_status = '';
			$end_date = $data['end_date'];
			if(!$end_date)
			{
				$result.= br()."<span class='label label-danger'>Đã quá hạn</span>";
				$data['post_title'] = $result;
				return $data;
			}

			$diff_timestamp = strtotime("-{$time} seconds",$end_date);
			$past = FALSE;

			if($diff_timestamp < 0)
			{
				$past = TRUE;
				$diff_timestamp = absint($diff_timestamp);
			} 

			$time_rounds = array (
				31536000 => 'năm',
				2592000 => 'tháng',
				86400 => 'ngày',
				3600 => 'giờ',
				60 => 'phút'
				);

			if(!$past)
			{
				$label = $diff_timestamp - (3*24*60*60) > 0 ? 'label-default':'label-warning';
				$message_status = '<span class="label '.$label.'">Còn lại {left_time}</span>';
			}
			else
			{
				$message_status = '<span class="label label-danger">Đã quá hạn {left_time}</span>';
			}

			$left_time = '';

			foreach ($time_rounds as $key => $value)
			{
				if($diff_timestamp < $key) 
					continue;

				$var = floor($diff_timestamp / $key);
				$diff_timestamp = $diff_timestamp - $key*$var;
				$left_time .= $var . ' ' . $value . ' ';
			}

			$message_status = str_replace('{left_time}', $left_time, $message_status);
			$result.= br().$message_status;

			$data['post_title'] = $result;
			return $data;
			},FALSE)
		
		->add_column('posts.start_date', 'Ngày bắt đầu')
		->add_column_callback('start_date',function($data,$row_name){
			$data['start_date'] = $data['start_date'] ? my_date($data['start_date'],'d/m/Y') : '--';
			return $data;
			},FALSE)

		->add_column('posts.end_date', 'Ngày kết thúc')
		->add_column_callback('end_date',function($data,$row_name){
			$data['end_date'] = $data['end_date'] ? my_date($data['end_date'],'d/m/Y') : '--';
			return $data;
			},FALSE)

		->add_column('price_total', array('set_select'=>FALSE, 'title'=>'Số tiền'))
		->add_column_callback('price_total',function($data,$row_name){

			$post_id = $data['post_id'];
			$price_total = $this->invoice_item_m->get_total($post_id, 'total');
			$data['price_total'] = currency_numberformat($price_total,'');
			return $data;

			},FALSE)

		->add_column('posts.post_status', 'Trạng thái')
		->add_column_callback('post_status',function($data,$row_name){

			$post_id = $data['post_id'];
			$post_status = $data['post_status'];
			$default_status = $this->config->item('status', 'invoice');
			$ajax_call_url = module_url("invoices/ajax_dipatcher/ajax_edit/{$post_id}");

			$data['post_status'] = anchor('#', $this->invoice_m->status_label($post_status), 
				"data-original-title='Trạng thái' 
				data-pk='{$post_id}' 
				data-type-data='field'
				data-name='post_status'
				data-type='select' 
				data-value='{$post_status}'
				data-ajax_call_url='{$ajax_call_url}'
				data-source=\"" . str_replace('"', '\'', json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},
					array_keys($default_status), $default_status))) . "\"
				class='myeditable editable'");

			$data['post_status'].= br();

			$inv_status_updated_on = get_post_meta_value($post_id,'inv_status_updated_on');
			$inv_status_updated_on = $inv_status_updated_on ? my_date($inv_status_updated_on,'d-m-Y') : '--';

			$data['post_status'].= anchor('#', $inv_status_updated_on, 
				"data-original-title='Thời điểm' 
				data-pk='{$post_id}' 
				data-type-data='meta_data' 
				data-type='combodate' 
				data-name='inv_status_updated_on' 
				data-value='{$inv_status_updated_on}' 
				data-ajax_call_url='{$ajax_call_url}' 
				data-format = 'DD-MM-YYYY'  
				data-viewformat = 'DD-MM-YYYY' 
				data-template = 'D MM YYYY' 
				class='myeditable editable'");

			return $data;

			},FALSE)

		->add_search('post_status',array(
			'content'=> form_dropdown(
				array('name'=>'where[post_status]','class'=>'form-control select2'), 
				prepare_dropdown($invoice_status),
				$this->input->get('where[post_status]'))))

		->add_search('term_type',array(
			'content'=> form_dropdown(
				array('name'=>'where[term_type]','class'=>'form-control select2'), 
				prepare_dropdown($this->config->item('services')),
				$this->input->get('where[term_type]'))))

		// ->add_column('posts.post_id', '#ID')

		->add_column('term.term_type', 'Dịch vụ')
		->add_column_callback('term_type',function($data,$row_name){

			$data['term_type'] = $this->config->item($data['term_type'],'services');
			return $data;

			},FALSE)
		

		->add_column('term.term_name', 'Website')
		->add_column_callback('term_name',function($data,$row_name){

			$term_id = $data['term_id'];
			$term_name = '';
			if($domain = $data['term_name'])
			{
				$term_name = anchor(module_url("create_wizard/{$term_id}"), $domain,'title="Cập nhật hợp đồng"');
			}

			$status_colors = array( 
				'draft' => 'label-default',
				'waitingforapprove' => 'label-primary',
				'pending' => 'label-danger',
				'publish' => 'label-success',
				'ending'	=>	'label-info',
				'liquidation'=>'label-warning',
				'remove' => 'label-default disable'
			);

			$term_status = $data['term_status'];

			if(!empty($status_colors[$term_status]))
			{
				$term_status_color = $status_colors[$term_status];
				$term_status = $this->config->item($term_status,'contract_status');
				$term_name.= '<br/><span class="label '.$term_status_color.'">'.$term_status.'</span>';
			}


			$data['term_name'] = $term_name;
			return $data;
			},FALSE)

		->from('posts')
		->join('term_posts','term_posts.post_id = posts.post_id')
		->join('term','term.term_id = term_posts.term_id')
		->where('posts.post_type',$this->invoice_m->post_type)
		->where('term.term_type !=', 'website')
		->where_in('posts.post_status',array_keys($invoice_status));

		$data['content'] = $this->admin_ui->generate(array('per_page' => 100));
		parent::render($data, 'invoices/index');
	}

	public function queue_waiting()
	{
		restrict('Invoices.Queue_waiting');
		$data = array();
		$invoice_status = $this->config->item('status','invoice');
		unset($invoice_status['paid']);

		$data['invoices'] = $this->invoice_m
		->where_in('posts.post_status',array_keys($invoice_status))
		->join('term_posts','term_posts.post_id = posts.post_id')
		->join('term','term.term_id = term_posts.term_id')
		->get_posts(array(
			'select' => 'posts.post_id,post_title,post_type,start_date,end_date,posts.post_status,term.term_name,term.term_type',
			'post_type' => $this->invoice_m->post_type,
			'where_in' => array('post_status' => array_keys($invoice_status)),
			'where' => array('start_date <='=>strtotime("+1 month"))
			));
		
		$this->load->config("contract");
		$this->template->title->set('Hóa đơn chờ thu trong tháng '.my_date(0,'m'));
		$data['invoice_status'] = $invoice_status;

		parent::render($data, 'invoices/queue_waiting');
	}

	function revenue()
	{
		$data = $this->data;
		$start_time = $this->mdate->startOfMonth();
		$end_time = $this->mdate->endOfMonth();

		if($post = $this->input->post())
		{
			$filter_date = $post['datefilter'];

			if(!empty($filter_date))
			{
				$data['datefilter'] = $filter_date;
				$filter_date = explode(' ', $filter_date);
				$start_time = $this->mdate->startOfDay($filter_date[0]);
				$end_time = $this->mdate->endOfDay($filter_date[1]);
			}

			if($this->input->post('revenue_excel_export') !== NULL)
			{
				$invoice_paid_ids = $this->postmeta_m
				->select('post_id')
				->where('meta_key','inv_status_updated_on')
				->where('meta_value >=',$start_time)
				->where('meta_value <=',$end_time)
				->as_array()
				->get_many_by();


				$invoice_paid_ids = array_column($invoice_paid_ids, 'post_id');

				$file_name = $this->invoice_m
				->where_in('posts.post_id',$invoice_paid_ids)
				->where('post_status','paid')
				->export_excel($start_time,$end_time);
				
				$this->invoice_m->_database->reset_query();

				if($file_name)
				{	
					$this->load->helper('download');
					force_download($file_name,NULL);
					redirect(module_url('invoices/revenue'),'refresh');
				}

			}
		}

		$data['inv_data'] = array();

		$invoice_paid_ids = $this->postmeta_m
		->select('post_id')
		->where('meta_key','inv_status_updated_on')
		->where('meta_value >=',$start_time)
		->where('meta_value <=',$end_time)
		->as_array()
		->get_many_by();

		if(!$invoice_paid_ids)
		{
			return parent::render($data,'invoices/revenue');
		}

		$invoice_paid_ids = array_column($invoice_paid_ids, 'post_id');

		$inv_data = $this->invoice_m
		->where_in('posts.post_id',$invoice_paid_ids)
		->where('post_status','paid')
		->list_all_invoices($start_time,$end_time);

		if(!$inv_data)
		{
			return parent::render($data,'invoices/revenue');
		}

		$total = array_sum(array_column($inv_data,3));
		$total_revenue = array_sum(array_column($inv_data,6));
		$sumary_row = array(
			array('data'=>'Tổng cộng','colspan'=>3),currency_numberformat($total,'VNĐ'),NULL,NULL,currency_numberformat($total_revenue,'VNĐ'),NULL);

		$inv_data = array_map(function($row){
			$row['3'] = currency_numberformat($row['3']);
			$row['6'] = currency_numberformat($row['6']);
			return $row;
		}, $inv_data);

		$inv_data[] = $sumary_row;
		$data['inv_data'] = $inv_data;
		
		parent::render($data,'invoices/revenue');
	}

	public function add($contract_id = 0)
	{
		if($this->input->post('add_invoice') === null)
			return false;

		if($contract_id <=0)
			return $this->render404('ID ko tồn tại');

		$contract = $this->term_m->get($contract_id);

		if(!$contract)
			return $this->render404('Không có hợp đồng này.');

		$data = array();
		$title = strip_tags($this->input->post('title'));
		$description = strip_tags($this->input->post('description'));
		$tax = $this->input->post('tax');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');

		$data['post_title'] = $title;
		$data['post_content'] = $description;
		$data['start_date'] = strtotime($start_date);
		$data['end_date'] = strtotime($end_date);
		$data['post_type'] = $this->invoice_m->post_type;
		$inv_id = $this->invoice_m->insert($data);
		update_post_meta($inv_id, 'invoice_tax', $tax);
		$this->term_posts_m->set_post_terms($inv_id, $contract_id, $contract->term_type);

		$this->messages->success('Thêm mới hóa đơn thành công.');
		redirect(current_url(),'refresh');
	}

	public function view($id = 0)
	{
		$data = array();

		$this->submit($id);

		$invoice = $this->invoice_m->get($id);

		if(!$invoice){

			$this->messages->error('Hóa đơn không tồn tại');

			return parent::render404($data);
		}

		$data['invoice_id'] = $this->invoice_m->generate_id($invoice);

		$data['invoice'] = $invoice;

		$data['id'] = $id;

		$data['items'] = $this->invoice_item_m->get_many_by('inv_id', $id);

		$contract = $this->term_posts_m->get_the_terms($id, array_keys($this->config->item('services')));

		$data['contract_id'] = !empty($contract) ? end($contract) : $contract;


		/* Load danh sách nhân viên kinh doanh */
		$staffs = $this->sale_m->select('user_id,user_email,display_name')->set_user_type()->set_role()->as_array()->get_all();
		$staffs = array_map(function($x){ $x['display_name'] = $x['display_name'] ?: $x['user_email']; return $x; }, $staffs);
		$data['staffs'] = key_value($staffs, 'user_id', 'display_name');

		$this->load->config('staffs/group');
		$this->template->title->set('Hóa đơn #'.$id);

		parent::render($data, 'invoices/view');
	}

	private function submit($inv_id = 0)
	{
		$post = $this->input->post();
		if( ! $post) return FALSE;

		if($this->input->post('add_item') !== NULL)
		{
			$data = array();
			$item_id = $post['item_id'];
			$data['invi_title'] = $post['title'];
			$data['inv_id'] = $inv_id;
			$data['invi_description'] = $post['description'];
			$data['invi_status'] = $post['status'];
			$data['price'] = $post['price'];
			$data['quantity'] = $post['quantity'];
			$data['invi_rate'] = $post['rate'];

			foreach($data as &$r)
			{
				$r = strip_tags($r);
				$r = trim($r);
			}

			$data['price'] = str_replace(array(',','.'),'', $data['price']);
			$data['total'] = ($data['price']*$data['quantity']*$data['invi_rate']) /100;
			if($item_id > 0)
			{
				// UPDATE INVOICE ITEM BY $ITEM_ID
				$this->invoice_item_m->update($item_id, $data);
				$this->messages->success("Cập nhật #{$item_id} thành công.");
			}
			
			else
			{
				// CREATE NEW INVOICE ITEM
				$insert_id = $this->invoice_item_m->insert($data);
				$this->messages->success("Thêm mới chi tiết thanh toán thành công.");
			}

			// RECALC AMOUNT DATA OF TERM
			if(!empty($post['term_id']))
			{
				$this->base_contract_m->sync_all_amount($post['term_id']);
				$this->messages->success("Dữ liệu đã được đồng bộ.");
			}
			redirect(current_url(),'refresh');
		}
	}

	public function ajax($type = '', $id =0)
	{
		if(empty($type)) die();

		if($type == 'item-edit')
		{
			$invoice_item = $this->invoice_item_m->get($id);
			if(!$invoice_item) die();

			$result = array(
				'id' => $invoice_item->invi_id,
				'title' => $invoice_item->invi_title,
				'description' => $invoice_item->invi_description,
				'status' => $invoice_item->invi_status,
				'rate' => $invoice_item->invi_rate,
				'price' => $invoice_item->price,
				'quantity' => $invoice_item->quantity,
				'total' => $invoice_item->total,
				);

			die(json_encode($result));
		}

		if($type == 'item-delete')
		{
			$invoice_item = $this->invoice_item_m->get($id);
			if(!$invoice_item) die();

			$term_ids = $this->term_posts_m->get_the_terms($invoice_item->inv_id,array_keys($this->config->item('services')));
			$this->invoice_item_m->delete($id);

			if(!empty($term_ids))
			{
				$term_id = end($term_ids);
				$this->base_contract_m->sync_all_amount($term_id);
			}

			die('OK');
		}
	}

	public function delete($id = 0)
	{
		if($id <=0 ) die('ID ko đúng');

		$invoice = $this->invoice_m->get($id);
		if(empty($invoice)) die('ID ko đúng');

		$term_ids = $this->term_posts_m->get_the_terms($invoice->post_id,array_keys($this->config->item('services')));
		$this->invoice_m->delete($id);

		if(!empty($term_ids))
		{
			$term_id = end($term_ids);
			$this->base_contract_m->sync_all_amount($term_id);
		}

		die('OK');

	}
	public function ajax_dipatcher($callable = ''){

		// restrict('Admin.Contract');

		$post = $this->input->post();

		if(empty($post)) return FALSE;

		$this->hook->add_filter('ajax_ajax_edit',function($args){

			$post = array('edit' => array('post_id' => $args['pk'], 'meta' => array()));

			$this->load->library('form_validation');

			$args['value'] = trim($args['value']);

			if($args['value'] == ''){

				return array('msg'=>'Vui lòng nhập trường này','success'=>FALSE);
			}

			switch ($args['name']) {

				case 'invoice_tax':

					if($args['value'] == 0) break;

					update_post_meta($post['edit']['post_id'], $args['name'], $args['value']);
					$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));
					$post['response'] = array('jscallback'=> $jsfunction);

					return $post;

					break;

				case 'start_date':
					$args['value'] = strtotime($args['value']);
					break;

				case 'end_date':
					$args['value'] = strtotime($args['value']);
					break;	

				case 'inv_status_updated_on':
					$args['value'] = strtotime($args['value']);
					break;	
			}

			switch ($args['type'])
			{
				case 'meta_data':
					$post['edit']['meta'] = array($args['name']	=> $args['value']);
					break;

				case 'field':
					$post['edit'][$args['name']] = $args['value'];
					break;
			}

			return $post;
		});

		if($this->hook->has_filter("ajax_{$callable}")){

			$this->hook->add_filter("ajax_{$callable}",function($post){

				$result = array('response'=>$post, 'msg'=>'Cập nhật nhật thành công','success'=>TRUE);

				if( ! empty($post['msg'])){

					$result['msg'] = $post['msg'];

					unset($post['msg']);
				}

				if( isset($post['success'])){

					$result['success'] = $post['success'];

					unset($post['success']);
				}

				if( ! empty($post['edit']['meta'])) foreach ($post['edit']['meta'] as $key => $value) {

					update_post_meta($post['edit']['post_id'], $key, $value);
				}

				unset($post['edit']['meta']);

				if( ! empty($post['edit']))
				{
					$this->invoice_m->update($post['edit']['post_id'], $post['edit']);
				}

				return $result;
			});
		}

		$result = $this->hook->apply_filters("ajax_{$callable}", $post);

		if(!is_array($result)){

			$result = array($result);
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));

		if(@$result['success'] === FALSE){

			$this->output
			->set_status_header(400,'reponseText content')
			->set_output($result['msg']);
		}

		return $this->output;
	}

	public function copy($inv_id = 0, $term_id = 0)
	{
		if($inv_id <=0 || $term_id <=0)
		{
			return $this->render404('Lỗi không tồn tại nên không thể copy được hóa đơn');
		}

		$inv = $this->invoice_m->get($inv_id);
		if(!$inv) return '';
		$copy = $inv;

		$copy->post_title .= " - copy {$copy->post_id}";
		unset($copy->post_id);
		$copy->post_author = $this->admin_m->id;
		$copy->created_on = time();
		$copy->post_status = 'publish';

		$new_inv_id = $this->invoice_m->insert($copy);
		$items = $this->invoice_item_m->get_many_by('inv_id', $inv_id);
		if($items)
		{
			foreach($items as $item)
			{
				unset($item->invi_id);
				$item->inv_id =  $new_inv_id;
				$this->invoice_item_m->insert($item);
			}
		}

		$post_terms = $this->term_posts_m->get_the_terms($inv_id);
		if($post_terms)
		{
			foreach($post_terms as $pt)
			{
				$this->term_posts_m->insert(array('term_id' => $pt, 'post_id'=> $new_inv_id));
			}
		}

		//insert post meta
		$post_metas = $this->postmeta_m->get_many_by('post_id',$inv_id);
		if($post_metas)
		{
			foreach($post_metas as $meta)
			{
				update_post_meta($new_inv_id, $meta->meta_key, $meta->meta_value);
			}
		}

		$this->messages->success('Copy hóa đơn thành công.');
		redirect(module_url('edit/'.$term_id),'refresh');
	}

	protected function search_filter($search_args = array())
	{
		if(empty($search_args) && $this->input->get('search'))
			$search_args = $this->input->get();

		if(empty($search_args)) return FALSE;

		$this->hook->add_filter('search_filter', function($args){

			if(!empty($args['where']['start_date']))
			{
				$con_args = $args['where']['start_date'];
				$con_args = explode(' - ', $con_args);
				if(!empty($con_args))
				{
					$start_time = reset($con_args);
					$start_time = $this->mdate->startOfDay($start_time);

					$end_time = end($con_args);
					$end_time = $this->mdate->endOfDay($end_time);

					$this->admin_ui
					->where('posts.start_date >=',$start_time)
					->where('posts.start_date <=',$end_time);
				}
				unset($args['where']['start_date']);
			}

			if(!empty($args['where']['end_date']))
			{
				$con_args = $args['where']['end_date'];
				$con_args = explode(' - ', $con_args);
				if(!empty($con_args))
				{
					$start_time = reset($con_args);
					$start_time = $this->mdate->startOfDay($start_time);

					$end_time = end($con_args);
					$end_time = $this->mdate->endOfDay($end_time);

					$this->admin_ui
					->where('posts.end_date >=',$start_time)
					->where('posts.end_date <=',$end_time);
				}
				unset($args['where']['end_date']);
			}

			if(!empty($args['order_by']['price_total']))
			{
				$order = $args['order_by']['price_total'];

				$this->admin_ui
				->select('sum(invoice_items.total)')
				->join('invoice_items','invoice_items.inv_id = posts.post_id','LEFT OUTER')
				->group_by('posts.post_id')
				->order_by('sum(invoice_items.total)',$order);

				unset($args['order']['price_total']);
			}

			if(!empty($args['order_by']['term_type']))
			{
				$order = $args['order_by']['term_type'];
				$this->admin_ui->order_by('term.term_type',$args['order_by']['term_type']);
				unset($args['order']['term_type']);
			}

			if(!empty($args['where']['post_status']))
			{
				$this->admin_ui->where('post_status',$args['where']['post_status']);
				unset($args['where']['post_status']);
			}

			if(!empty($args['where']['term_status']))
			{
				$this->admin_ui->where('term_status',$args['where']['term_status']);
				unset($args['where']['term_status']);
			}

			return $args;			
		});

		parent::search_filter($search_args);
	}
}
/* End of file Invoices.php */
/* Location: ./application/modules/contract/controllers/Invoices.php */