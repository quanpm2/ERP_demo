<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Create_wizard extends Admin_Controller {

	public $model = 'contract_m';

	public function __construct(){

		parent::__construct();

		$models = array(
			'term_users_m',
			'contract/invoice_m',
			'customer/customer_m',
			'contract/contract_m',
			'contract/contract_wizard_m',
			'contract/base_contract_m',
			'contract/category_m',
			'contract/term_categories_m'
			);

		$this->load->model($models);

		$this->load->config('contract');
		$this->load->config('staffs/group');
		$this->load->config('invoice');

		// $this->load->add_package_path(APPPATH.'third_party/google-analytics/');
		// $this->load->library('ga');

		$this->data['step'] = $this->input->get('step') ? $this->input->get('step') : 0 ;
	}

	/**
	 * Check term_id is exists . If not exists , then create the new one by logged user
	 * otherwise, get contract data and return to index
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function init($term_id = 0)
	{
		restrict('admin.contract.view');

		// If term_id is empty , then create a new draft contract
		if(empty($term_id))
		{
			restrict('admin.contract.add');

			$draft_term = $this->contract_m->select('term.term_id')
			->join('term_users','term_users.term_id = term.term_id')
			->get_by(['term_status'=>'draft','term_users.user_id'=>$this->admin_m->id]);

			if( ! $draft_term)
			{
				$insert_id = $this->contract_m->insert(['term_status'=>'draft']);
				$this->term_users_m->set_relations_by_term($insert_id,[$this->admin_m->id],'admin');
				if( ! has_permission('admin.contract.manage'))
				{
					update_term_meta($insert_id,'staff_business',$this->admin_m->id);
				}

				update_term_meta($insert_id,'created_on',time());
				update_term_meta($insert_id,'created_by',$this->admin_m->id);

				redirect(module_url("create_wizard/index/{$insert_id}"),'refresh');
			}

			redirect(module_url("create_wizard/index/{$draft_term->term_id}"), 'refresh');
		}

		// LOGGED USER NOT HAS PERMISSION TO ACCESS
		if( ! $this->contract_m->has_permission($term_id, 'admin.contract.update'))
		{
			$this->messages->error('Truy xuất dữ liệu hợp đồng không hợp lệ.');
			redirect(module_url(), 'refresh');
		}

		return TRUE;
	}

	/**
	 * The create a blank contract with step by step
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function index($term_id = 0)
	{
		restrict('admin.contract.view');

		$this->init($term_id);

		$data = $this->data;
		$term = $this->contract_m->get($term_id);

		if($term->term_status !== 'draft')
		{
			redirect(module_url("edit/{$term_id}"),'refresh');
		}
		
		/* Load dữ liệu kinh doanh */
		$staffs = array();
		$sale_staffs = $this->admin_m->select('user_id,display_name,user_email')
		->set_get_active()->set_role($this->config->item('salesexecutive','groups'))->get_many_by();

		if($sale_staffs)
		{
			foreach ($sale_staffs as $user)
			{
				$staffs[$user->user_id] = $user->display_name ?: $user->user_email;
			}
		}
		$data['staffs'] = $staffs;

		/* Load thông tin thêm của hợp đồng */
		$term->extra = ($extra = get_term_meta_value($term_id,'extra')) ? unserialize($extra) : array();

		/*Load dữ liệu khách hàng*/

		$is_system = FALSE;
		$is_manager = has_permission('admin.contract.manage');
		if($is_manager)
		{
			$is_system = FALSE;
			$customers = $this->customer_m->set_get_customer()->order_by('display_name')->get_many_by();
		}
		else
		{
			# Load các khách hàng có liên kết đến tất cả hợp đồng và tất cả khách hàng liên quan đến hợp đồng
			$customer 		= NULL;
			$customers 		= array();

			$taxonomies = $this->config->item('taxonomy');
			$key_taxonomies = array_keys($taxonomies);
			$key_taxonomies[] = '';

			# Load tất cả các khách hàng mà user có liên quan đến hiện tại
			$owners_terms = $this->term_users_m->get_the_terms($this->admin_m->id,$key_taxonomies);
			if( ! empty($owners_terms))
			{
				$owners_terms_customers = $this->customer_m
				->set_user_type()
				->distinct('user.user_id')
				->select('user.user_id,user.user_type,user.display_name')
				->join('term_users','term_users.user_id = user.user_id')
				->join('termmeta tm_sale','tm_sale.term_id = term_users.term_id AND tm_sale.meta_key = "staff_business"')
				->group_start()
				->where_in('term_users.term_id',$owners_terms)
				->or_where('tm_sale.meta_value',$this->admin_m->id)
				->group_end()				
				->get_all(['user_type'=>'admin']);

				if( ! empty($owners_terms_customers))
				{
					foreach ($owners_terms_customers as $i)
					{
						$customers[$i->user_id] = $i;
					}
				}
			}


			# Chưa tối ưu source truy xuất dữ liệu

			$ownered_customers = $this->customer_m
			->select('user.user_id,user.display_name,user.user_type,user.user_status')
			->set_user_type()
			->join('usermeta',"usermeta.user_id = user.user_id")
			->where_in('usermeta.meta_key',['assigned_to','created_by'])
			->where('usermeta.meta_value',$this->admin_m->id)
			->group_by('user.user_id')
			->get_all();

			if( ! empty($ownered_customers))
			{
				foreach ($ownered_customers as $i)
				{
					$customers[$i->user_id] = $i;
				}
			}
		}

		$data['is_system']	= $is_system;
		$data['is_manager']	= $is_manager;
		$data['customers']	= key_value($customers,'user_id','display_name');


		/* Load dữ liệu website của khách hàng được chọn */
		$customer 		= NULL;
		$websites	 	= array();
		$term_customers = $this->term_users_m->get_term_users($term_id,['customer_person','customer_company','system']);
		if( ! empty($term_customers))
		{
			$customer = reset($term_customers);

			$customer_websites = $this->term_m
			->select('term.term_id,term.term_name')
			->join('term_users','term_users.term_id = term.term_id')
			->get_many_by(['term_users.user_id'=>$customer->user_id,'term.term_type'=>'website']);

			if($customer_websites)
			{
				$websites = array_column($customer_websites, 'term_name','term_id');
			}
		}
		
		/**
		 * Load danh sách các ngành nghề theo Hierachy
		 */
		$term_categories = $this->term_categories_m->get_categories($term->term_id) ?: array();
		$categories	 = $this->category_m->set_term_type()->order_by('term_parent,term_name')->get_many_by(['term_status'=>'publish']);
		$hcategories = $this->category_m->get_recursive($categories);
		$categories  = $this->category_m->get_recursive_array($hcategories,0,'_');

		$data['term_categories'] = array_column($term_categories,'term_id');
		$data['hcategories'] = $hcategories;
		$data['categories'] = $categories;

		$data['edit']		= $term;
		$data['websites']	= $websites;
		$data['customer']	= $customer;

		/* Init các thành phần trong trang */
		$wizard_tabs = array(
			['id'=>'tab-init','content'=>$this->load->view('admin/create_wizard/initial',$data,TRUE),'title'=>'Dịch vụ'],
			['id'=>'tab-customer','content'=>$this->load->view('admin/create_wizard/customer',$data,TRUE),'title'=>'Khách hàng'],
			['id'=>'tab-website','content'=>$this->load->view('admin/create_wizard/website',$data,TRUE),'title'=>'Website thực hiện'],
			['id'=>'tab-detail','content'=>$this->load->view('admin/create_wizard/detail',$data,TRUE),'title'=>'Chi tiết'],
			['id'=>'tab-service','content'=>'<div id="service_tab"></div>','title'=>''],
			['id'=>'tab-finish','content'=>$this->load->view('admin/create_wizard/finish',$data,TRUE),'title'=>'Review'],
		);

		/* Nếu tồn tại trang edit tương tứng của mỗi dịch vụ => OVERRIDE view theo dịch vụ */
		if($term->term_type)
		{
			$partial_service	= '';
			$service_module 	= str_replace('-', '',$term->term_type);
			$service_module		= ($service_module == 'webdesign') ? 'webbuild' : $service_module;

			if(file_exists(APPPATH . "modules/{$service_module}/views/contract/wizard_partial" . EXT))
			{
				$wizard_tabs[4]['title']	= $this->config->item($term->term_type, 'services');
				$wizard_tabs[4]['content']	= $this->load->view("{$service_module}/contract/wizard_partial",$data,TRUE);
			}

			if(file_exists(APPPATH . "modules/{$service_module}/views/contract/wizard_review" . EXT))
			{
				$wizard_tabs[5]['content'] = $this->load->view("{$service_module}/contract/wizard_review",$data,TRUE);
			}
		}
		
		$data['wizard_tabs'] = $wizard_tabs;
		parent::render($data,'create_wizard/index');
	}

	public function ajax_dipatcher($callable = '')
	{
		restrict('admin.contract.edit,order_contact.contract.add');

		$post = $this->input->post();
		if(empty($post)) return FALSE;

		$term_id 		= $post['edit']['term_id'] ?? 0;

		$contract 		= empty(get_term_meta_value($term_id, 'wizard_step')) ? $this->contract_m->get($term_id) : $this->contract_m->set_term_type()->get($term_id);
		$model_name 	= $this->get_model($contract);
		$_model_name 	= explode('/', $model_name);
		$_model_name 	= count($_model_name) > 1 ? end($_model_name) : $model_name;
		$_model_name 	= strtolower($_model_name);

		$this->load->model($model_name, $_model_name);
		if(method_exists($this->{$_model_name}, "wizard_{$callable}")) $this->{$_model_name}->{'wizard_'.$callable}($post);

		$this
		->add_filter_upload_file($post)
		->add_filter_ajax_edit($post)
		->add_filter_upload_real_file($post);
		
		if($this->hook->has_filter("ajax_{$callable}"))
		{
			$this->hook->add_filter("ajax_{$callable}",function($post){

				$result = array('response'=>$post, 'msg'=>'Cập nhật nhật thành công','success'=>TRUE);

				if( ! empty($post['msg'])){

					$result['msg'] = $post['msg'];

					unset($post['msg']);
				}

				if( isset($post['success'])){

					$result['success'] = $post['success'];

					unset($post['success']);
				}

				if( ! empty($post['edit']['meta'])) 
				{
					foreach ($post['edit']['meta'] as $key => $value)
					{
						$this->termmeta_m->update_meta($post['edit']['term_id'], $key, $value);
					}
				}

				unset($post['edit']['meta']);

				if( ! empty($post['edit'])){

					if(!empty($post['edit']['term_status']) && $post['edit']['term_status'] == 'publish'){

						$order_id = get_term_meta_value($post['edit']['term_id'], 'order_id');
						if(!empty($order_id)) 
							$this->post_m->update($order_id,array('post_status'=>'publish'));
					}

					$this->term_m->update($post['edit']['term_id'], $post['edit']);
				}

				return $result;
			});
		}

		$result = $this->hook->apply_filters("ajax_{$callable}", $post);

		if(!is_array($result)){

			$result = array($result);
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));

		return $this->output;
	}

	public function add_filter_upload_file($post){

		$this->hook->add_filter('ajax_act_upload_file',	function($post){

			$result = array('msg'=>'File upload không hợp lệ !','success'=>FALSE);

			$post = $this->input->post();
			if(empty($post)) 
				return $result;

			$upload_config = $this->config->item('upload_config');
			$upload_config['file_name']	= $post['term_name'] . '.' . $post['term_id'] . '.' . time();
			$upload_config['file_name'] = str_replace('.', '_', $upload_config['file_name']);
			$upload_config['allowed_types'] = 'doc|docx';
			$upload_config['upload_path'].= date('Y') . '/' . date('m') . '/';

			if(!is_dir($upload_config['upload_path']))
				mkdir($upload_config['upload_path'], '0777', TRUE);

			$this->load->library('upload');
			$this->upload->initialize($upload_config);

			if ($this->upload->do_upload('fileinput')){

				$data = $this->upload->data();

				$_tmp_queue_uploaded = @unserialize(get_term_meta_value($post['term_id'], '_tmp_file_upload'));
				if(empty($_tmp_queue_uploaded) || !is_array($_tmp_queue_uploaded))
					$_tmp_queue_uploaded = array();

				$_tmp_queue_uploaded[] = array('file_name'=>$data['file_name'],'file_path'=>$upload_config['upload_path'].$data['file_name']);
				$_tmp_queue_uploaded = arrayUnique($_tmp_queue_uploaded);

				$this->termmeta_m->update_meta($post['term_id'],'_tmp_file_upload', serialize($_tmp_queue_uploaded));

				$_tmp_queue_uploaded = array(array('file_name'=>$data['file_name'],'file_path'=>$upload_config['upload_path'].$data['file_name']));

				$result['ret_obj'] = $_tmp_queue_uploaded;
				$result['msg'] = 'Upload file hợp đồng thành công !';
				$result['success'] = TRUE;
			}

			return $result;
		});

		return $this;
	}

	public function add_filter_upload_real_file($post){

		$this->hook->add_filter('ajax_act_upload_real_file', function($post){

			$result = array('msg'=>'File upload không hợp lệ !','success'=>FALSE);

			$post = $this->input->post();

			if(empty($post)) return $result;

			$upload_config = $this->config->item('upload_config');

			$upload_config['file_name']	= $post['term_name'] . '.' . $post['term_id'] . '.' . time();

			$upload_config['file_name'] = str_replace('.', '_', $upload_config['file_name']);

			$upload_config['allowed_types'] = 'doc|docx';

			$upload_config['upload_path'].= date('Y') . '/' . date('m') . '/';

			if(!is_dir($upload_config['upload_path'])){

				mkdir($upload_config['upload_path'], '0777', TRUE);
			}

			$this->load->library('upload');

			$this->upload->initialize($upload_config);

			if ($this->upload->do_upload('fileinput')){

				$data = $this->upload->data();

				$_attachment_files = @unserialize(get_term_meta_value($post['term_id'], '_attachment_files'));

				if(empty($_attachment_files)) $_attachment_files = array();

				$_attachment_files[] = array('file_name'=>$data['file_name'],'file_path'=> $upload_config['upload_path'] . $data['file_name']);

				$_attachment_files = arrayUnique($_attachment_files);

				$this->termmeta_m->update_meta($post['term_id'],'_attachment_files', serialize($_attachment_files));

				$result['ret_obj'] = array(array('file_name'=>$data['file_name'],'file_path'=> $upload_config['upload_path'] . $data['file_name']));

				$result['msg'] = 'Upload file hợp đồng thành công !';

				$result['success'] = TRUE;
			}

			return $result;
		});

		return $this;
	}

	public function add_filter_ajax_edit($post){
		$this->hook->add_filter('ajax_ajax_edit', function($args){

			$post = array('edit' => array('term_id' => $args['pk'], 'meta' => array()));

			$this->load->library('form_validation');

			if(isset($args['value']) && is_string($args['value']))
			{	
				$args['value'] = trim($args['value']);
				if($args['value'] == '')
					return array('msg'=>'Vui lòng nhập trường này','success'=>FALSE);
			}

			switch ($args['name']) {

				case 'term_parent':
					if($args['value'] == 0) break;
					if($web_term = $this->term_m->get($args['value']))
						$this->term_m->update($post['edit']['term_id'], array('term_name'=>$web_term->term_name));	
					break;

				case 'representative_email':
					if(!$this->form_validation->valid_email($args['value']))
						return array('msg'=>'Địa chỉ email không hợp lệ','success'=>FALSE);
					break;

				case 'exchange_rate_usd_to_vnd':
					if(!$this->form_validation->is_natural($args['value']))
						return array('msg'=>'Số tiền không hợp lệ','success'=>FALSE);
					break;

                case 'exchange_rate_aud_to_vnd':
                    if(!$this->form_validation->is_natural($args['value']))
                        return array('msg'=>'Số tiền không hợp lệ','success'=>FALSE);
                    break;

				case 'network_type':
					if(empty($args['value'])) 
						return array('msg'=>'Vui lòng chọn ít nhất 1 kênh quảng cáo','success'=>FALSE);
					$network_types = $args['value'];

					$args['value'] = implode(',', $network_types);

					break;

				case 'payment_bank_account':
					if(empty($args['value'])) 
						return array('msg'=>'Vui lòng chọn một','success'=>FALSE);
					$network_types = $args['value'];
					
					$args['value'] = implode(',', $network_types);

					break;	

				 case 'payment_banner':
					if(empty($args['value'])) 
					{
						return array('msg'=>'Vui lòng chọn một','success'=>FALSE);
					}
					if (count($args['value'])>1) {
						return array('msg'=>'Chỉ được chọn một hình thức thanh toán','success'=>FALSE);
					}
					
					$network_types = $args['value'];	
					$args['value'] = implode(',', $network_types);

					break;		

				case 'user_id':

					$this->term_users_m
					->set_term_users($post['edit']['term_id'],array($args['value']),array('customer_person','customer_company'),array('command'=>'replace'));

					$this->term_m->update($post['edit']['term_id'], array('term_parent'=>0,'term_name'=>''));

					$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));

					$post['response'] = array('jscallback'=> $jsfunction);
					return $post;

					break;

				case 'term_type':

					$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));
					$post['response'] = array('jscallback'=> $jsfunction);
					break;
			}

			switch ($args['type']) {

				case 'meta_data':

					$timestamp_fields = array('contract_begin',
						'contract_end',
						'googleads-end_time',
						'googleads-begin_time',
						'started_service',
						'start_service_time',
						'end_service_time'
						);


					$is_field_type_timestamp = in_array($args['name'], $timestamp_fields);
					if($is_field_type_timestamp)
						$args['value'] = strtotime($args['value']);

					// CẬP NHẬT LẠI GÓI HOSTING
					if($args['name'] == 'hosting_service_package')
					{
						$this->config->load('hosting/hosting');
						$hosting_package_default = $this->config->item('service','packages');

						$selected_package = $hosting_package_default[$args['value']] ?? FALSE;
						update_term_meta($post['edit']['term_id'],'hosting_disk',($selected_package['disk']??0));
						update_term_meta($post['edit']['term_id'],'hosting_bandwidth',($selected_package['bandwidth']??0));
						update_term_meta($post['edit']['term_id'],'hosting_price',($selected_package['price']??0));
						update_term_meta($post['edit']['term_id'],'hosting_email_webmail',($selected_package['email_webmail']??0));

						$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));
						$post['response'] = array('jscallback'=> $jsfunction);
					}

					// CẬP NHẬT LẠI GÓI DOMAIN
					elseif($args['name'] == 'domain_service_package')
					{
						$this->config->load('domain/domain');

						$domain_package_default = $this->config->item('service','packages');

						$selected_package = $domain_package_default[$args['value']] ?? FALSE;
	
						// CẬP NHẬT LẠI DOMAIN
						update_term_meta($post['edit']['term_id'],'domain_fee_initialization',($selected_package['fee_initialization'] ?? 0));
						update_term_meta($post['edit']['term_id'],'domain_fee_maintain',($selected_package['fee_maintain'] ?? 0));
						update_term_meta($post['edit']['term_id'],'domain_fee_initialization_maintain',($selected_package['fee_maintain'] + $selected_package['fee_initialization'] ?? 0));

						// PHÍ KHỞI TẠO
					    $domain_fee_initialization  = get_term_meta_value($post['edit']['term_id'], 'domain_fee_initialization') ;

					    // PHÍ DUY TRÌ
					    $domain_fee_maintain        = get_term_meta_value($post['edit']['term_id'], 'domain_fee_maintain');

					    // PHÍ DUY TRÌ VÀ KHỞI TẠO NĂM ĐẦU TIÊN
					    $domain_fee_initialization_maintain = get_term_meta_value($post['edit']['term_id'], 'domain_fee_initialization_maintain');

					    // SỐ NĂM ĐĂNG KÝ
					    $domain_number_year_register       = get_term_meta_value($post['edit']['term_id'], 'domain_number_year_register') ?: 1 ;

					    // KIỂM TRA ĐÃ GIA HẠN HAY CHƯA
					    $has_domain_renew  = get_term_meta_value($post['edit']['term_id'], 'has_domain_renew') ?: 0;

					    // CẬP NHẬT GIÁ TRỊ HỢP ĐỒNG
					    $contract_value             = $domain_fee_initialization_maintain + ($domain_fee_maintain * ($domain_number_year_register - 1)) ;
						if($has_domain_renew == 1) $contract_value =  $domain_fee_maintain * $domain_number_year_register;
					    update_term_meta($post['edit']['term_id'], 'contract_value', $contract_value);

					    // THỜI GIAN BẮT ĐẦU HỢP ĐỒNG
					    $contract_begin             = get_term_meta_value($post['edit']['term_id'], 'contract_begin') ;

					    // CẬP NHẬT THỜI GIAN KẾT THÚC
					    $contract_end               = strtotime("+ $domain_number_year_register year", $contract_begin) ;
					    update_term_meta($post['edit']['term_id'], 'contract_end', $contract_end) ;
					
						$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));
						$post['response'] = array('jscallback'=> $jsfunction);
					}

					// CẬP NHẬT LẠI GÓI DOMAIN KHI CHỌN LẠI SỐ NĂM
					elseif($args['name'] == 'domain_number_year_register')
					{
						$this->config->load('domain/domain');

						// THÔNG TIN CÁC GÓI DOMAIN
			            $services = $this->config->item('service', 'packages') ;

			            // GÓI DOMAIN ĐƯỢC CHỌN
						$domain_service_package = get_term_meta_value($post['edit']['term_id'],'domain_service_package');

						// SỐ NĂM ĐĂNG KÝ
						$domain_number_year_register = $args['value'];

						// PHÍ KHỞI TẠO
					    $domain_fee_initialization  = get_term_meta_value($post['edit']['term_id'], 'domain_fee_initialization') ?: $services[$domain_service_package]['fee_initialization'];

					    // PHÍ DUY TRÌ
					    $domain_fee_maintain        = get_term_meta_value($post['edit']['term_id'], 'domain_fee_maintain') ?: $services[$domain_service_package]['fee_maintain'];

					    // PHÍ KHỞI TẠO VÀ DUY TRÌ NĂM ĐẦU TIÊN
					    $domain_fee_initialization_maintain = get_term_meta_value($post['edit']['term_id'], 'domain_fee_initialization_maintain') ?: $domain_fee_maintain + $domain_fee_initialization;

					    // KIỂM TRA ĐÃ GIA HẠN HAY CHƯA
					    $has_domain_renew  = get_term_meta_value($post['edit']['term_id'], 'has_domain_renew') ?: 0;

					    // CẬP NHẬT GIÁ TRỊ HỢP ĐỒNG
					    $contract_value             = $domain_fee_initialization_maintain + ($domain_fee_maintain * ($domain_number_year_register - 1)) ;
						if($has_domain_renew == 1) $contract_value =  $domain_fee_maintain * $domain_number_year_register;
    					update_term_meta($post['edit']['term_id'], 'contract_value', $contract_value);
						
						// THỜI GIAN BẮT ĐẦU HỢP ĐỒNG
					    $contract_begin             = get_term_meta_value($post['edit']['term_id'], 'contract_begin') ;

					    // CẬP NHẬT THỜI GIAN KẾT THÚC
					    $contract_end               = strtotime("+ $domain_number_year_register year", $contract_begin) ;
					    update_term_meta($post['edit']['term_id'], 'contract_end', $contract_end) ;

						$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));
						$post['response'] = array('jscallback'=> $jsfunction);
					}					

					$post['edit']['meta'] = array($args['name']	=> $args['value']);
					break;

				case 'extra':

					$extra = @unserialize(get_term_meta_value($post['edit']['term_id'], 'extra'));
					if(empty($extra)) 
						$extra = array();
					$extra[$args['name']] = $args['value'];

					$post['edit']['meta']['extra'] = serialize($extra);
					break;

				case 'field':

					$post['edit'][$args['name']] = $args['value'];
					break;
			}

			return $post;
		});

		return $this;
	}

	public function render_customer_info($user_id)
	{

		$user = $this->customer_m->like('user_type', 'customer_', 'after')->where('user_id',$user_id)->get_by();
		if(empty($user)) return ;

		$html = '';
		$data 				= array();
		$data['customer']	= $user;

		$html = '<div class="clearfix"></div>';
		$html.= $this->admin_form->hidden('','edit[meta][customer_id]',$user->user_id);
		$html.= $this->admin_form->formGroup_begin(0,' ');
		$html.= $this->admin_form->set_col(6)->box_open('Thông tin khách hàng');

		$this->table->add_row('Loại hình',($user->user_type == 'customer_person' ? 'Cá nhân' : 'Tổ chức'));

		if($user->user_type == 'customer_company')
		{
			$customer_gender = get_user_meta_value($user->user_id,'customer_gender');
			$display_name = ($customer_gender == 1 ? 'Ông' : 'Bà').' '.$user->display_name;

			$this->table->add_row('Tên tổ chức',$user->display_name);
			$this->table->add_row('Người đại diện',(get_user_meta_value($user->user_id,'customer_gender') == 1 ? 'Ông ' : 'Bà ') . 
				get_user_meta_value($user->user_id,'customer_name'));

			$this->table->add_row('Địa chỉ',get_user_meta_value($user->user_id,'customer_address'));
			$this->table->add_row('E-mail',get_user_meta_value($user->user_id,'customer_email'));
			$this->table->add_row('Số điện thoại',get_user_meta_value($user->user_id,'customer_phone'));
			$this->table->add_row('MST',get_user_meta_value($user->user_id,'customer_tax'));
		}
		else if($user->user_type == 'customer_person')
		{
			$customer_gender = get_user_meta_value($user->user_id,'customer_gender');
			$display_name = ($customer_gender == 1 ? 'Ông' : 'Bà').' '.$user->display_name;

			$this->table->add_row('Người đại diện',$display_name);
			$this->table->add_row('Địa chỉ',get_user_meta_value($user->user_id,'customer_address'));
			$this->table->add_row('E-mail',get_user_meta_value($user->user_id,'customer_email'));
			$this->table->add_row('Số điện thoại',get_user_meta_value($user->user_id,'customer_phone'));
		}

		$html.= $this->table->generate();
		$html.= $this->admin_form->box_close();

		/* Box thông tin lịch sử sử dụng dịch vụ của khách hàng này */
		$html.= $this->admin_form->set_col(6)->box_open('Lịch sử dịch vụ');
		$this->table->clear();


		/* Thông tin về website đang sử hữu */
		$taxonomies = $this->config->item('taxonomy');
		$taxonomies['website'] = 'Website';

		$website_hrefs = 'Chưa có';
		$terms_related = $this->term_m
		->select('term.term_id,term_name,term_type,term_status')
		->join('term_users','term_users.term_id = term.term_id')
		->where_in('term_type',array_keys($taxonomies))
		->get_many_by(['term_users.user_id'=>$user->user_id]);
		$terms_related = array_group_by($terms_related,'term_type');
		
		if(!empty($terms_related['website']))
		{
			$website_hrefs = implode('<br/>',array_map(function($x){ return anchor_popup(prep_url($x->term_name));},$terms_related['website']));
			unset($terms_related['website']);
		}
		$this->table->add_row('Website',$website_hrefs);

		$services_used_label = 'Chưa có';
		if(!empty($terms_related))
		{
			$services_used_label = implode(', ',array_unique(array_map(function($x){ return $this->config->item($x,'taxonomy');},array_keys($terms_related))));
		}

		$this->table->add_row('Dịch vụ đã dùng',$services_used_label);
		$this->table->add_row('Số lượng hợp đồng', count(array_values($terms_related)));

        $staff_business_ids = get_user_meta($user_id, 'assigned_to', FALSE, TRUE);
        $staff_business_name = [];
        if(!empty($staff_business_ids)){
            foreach($staff_business_ids as $staff_business_id)
            {
                $staff_business_name[] = $this->admin_m->get_field_by_id($staff_business_id, 'display_name');
            }
        }
        $staff_business_name = array_filter(array_unique($staff_business_name));
        $this->table->add_row('Kinh doanh phụ trách', implode(', ', $staff_business_name));

		/* Dịch vụ đang thực hiện */


		$html.= $this->table->generate();
		$html.= $this->admin_form->box_close();
		$html.= $this->admin_form->formGroup_end();

		if($this->input->is_ajax_request())
		{
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('content' => $html)));	

			return;
		}

		// die($html);

		return $html;
	}

	public function check_domain()
	{
		$url = $this->input->post('domain');
		$result = $this->contract_m->check_domain($url);
		echo ($result ? 'true' : 'false');
	}
	
	public function submit($edit_id = 0)
	{
		// XUẤT FILE EXCEL
		if($this->input->get('Export'))
		{
			$last_query = $this->scache->get("modules/contract/index-last-query-{$this->admin_m->id}");
			if( ! $last_query)
			{
				$this->messages->info('Không tìm thấy lệnh đã truy vấn trước đó , vui lòng thử lại sau');
				redirect(current_url(),'refresh');	
			}

			$terms = $this->term_m->query($last_query)->result();
			if( ! $terms)
			{
				$this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
				redirect(current_url(),'refresh');
			}

			$file_name = $this->export_excel($terms);
			if($file_name)
			{
				$this->load->helper('download');
				force_download($file_name,NULL);
			}

			redirect($this->uri->uri_string(),'refresh');
		}

		if(!$this->input->post()) return FALSE;

		// CẬP NHẬT KHÁCH HÀNG
		if($this->input->post('update_customer') !== NULL){

			$post = $this->input->post();

			$term = $this->term_m->get($post['edit']['term_id']);

			$exists_term_users = $this->term_users_m->get_term_users($term->term_id, array('customer_person', 'customer_company','system'));
			$exists_term_users = empty($exists_term_users) ? array() : array_map(function($x){return $x->user_id;},$exists_term_users);

			if(!in_array($post['edit']['user_id'], $exists_term_users)){

				if(!empty($exists_term_users))
					$this->term_users_m->where_in('term_users.user_id', $exists_term_users);

				$this->term_users_m
				->where('term_users.term_id', $post['edit']['term_id'])
				->delete_by();	

				$this->term_users_m->set_term_users($post['edit']['term_id'], array($post['edit']['user_id']), array('customer_person', '		customer_company','system'), array('command'=>'replace'));

				$this->messages->success('Thông tin khách hàng đã được cập nhật thành công vào hợp đồng');
				// Mapping website to customer
				if(!empty($term->term_parent)){

					$owner = $this->term_users_m->get_term_users($term->term_parent,array('customer_person','customer_company','system'));
					if(!empty($owner)){

						if(is_array($owner))
							$owner = end($owner);

						if($owner->user_type == 'system'){

							$this->term_users_m
							->where('term_id',$term->term_parent)
							->where('user_id',$owner->user_id)
							->delete_by();

							$this->term_users_m
							->set_term_users($term->term_parent, 
								array($post['edit']['user_id']), 
								array('customer_person', 'customer_company','system'), 
								array('command'=>'replace'));

							$this->messages->success('Website đã được cập nhật từ "Hệ thống" vào khách hàng');
						}
					}
				}
			}

			redirect(module_url("edit/{$post['edit']['term_id']}"));
		}

		// Tạo hóa đơn mặc định theo từng loại hợp đồng
		if($this->input->post('create_default_invoice') !== NULL)
		{
			$term = $this->contract_m->get($edit_id);

			// Nếu hợp đồng không tồn tại thì thông báo và trả về trang thông tin chi tiết
			if(!$term)
			{
				$this->messages->error('Dịch vụ không tìm thấy hoặc đã bị xóa');
				redirect(module_url("edit/{$edit_id}"),'refresh');
			}

			# check for fit contract type , like : webgeneral|googleads|...
			$model = $this->contract_m->get_contract_model($term);
			if(empty($model))
			{
				$this->messages->error('Dịch vụ không tìm thấy hoặc đã bị xóa');
				redirect(module_url("edit/{$edit_id}"),'refresh');
			}

			$this->load->model($model,'tmp_contract_m');
			$is_created = $this->tmp_contract_m->create_default_invoice($term);

			// Tính toán lại số liệu "Dự thu" "Dự thu quá hạn" "Đã thu" "Còn phải thu"
			$this->base_contract_m->sync_all_amount($term->term_id);
			$this->messages->success('Số liệu thu chi đã được đồng bộ');

			if( ! $is_created)
			{
				$this->messages->error('Quá trình tạo hóa đơn bị gián đoạn.');
				redirect(module_url("edit/{$edit_id}"),'refresh');
			}

			$this->messages->success('Hóa đơn đã được tạo !');

			redirect(module_url("edit/{$edit_id}"),'refresh');
		}

		if($this->input->post('add_invoice') !== NULL)
		{
			Modules::run('contract/invoices/add', $edit_id);
		}

		// CHẠY DỊCH VỤ
		if($this->input->post('start_service') !== NULL)
		{
			$term = $this->contract_m->get($edit_id);

			$this->term_m->update($edit_id, array('term_status'=> 'publish'));
			update_term_meta($edit_id,'started_service', time());

			/* Log started_service action*/
			$this->log_m->insert(array(
				'log_type' =>'started_service',
				'user_id' => $this->admin_m->id,
				'term_id' => $edit_id,
				'log_content' => date('Y/m/d H:i:s')
				));		

			$this->messages->success('Hợp đồng đã chuyển sang trạng thái thực hiện');

			$model = $this->contract_m->get_contract_model($term);
			if(!$model) redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
			
			// Xử lý các tác vụ ngầm sau khi "Chạy dịch vụ" tùy từng dịch vụ như gửi mail kết nối nội bộ ,...
			$this->load->model($model,'tmp_contract_m');
			if( ! method_exists($this->tmp_contract_m,'start_service')) 
			{
				redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
			}

			$this->tmp_contract_m->start_service($term);
			$this->messages->info('Hệ thống gửi mail kết nối thành công .');

			redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
		}
		
		// KÊT THÚC HỢP ĐỒNG
		if($this->input->post('stop_contract') !== NULL){
			$result = $this->contract_m->stop_contract($edit_id);
			if($result) $this->messages->success('Xử lý tác vụ thành công.');

			$term = $this->contract_m->get($edit_id);
			if($term->term_type == 'domain')
			{
				$model = $this->contract_m->get_contract_model($term);

				if(!$model) redirect(admin_url("contract/edit/{$edit_id}"),'refresh');

				$this->load->model($model,'tmp_contract_m');
				if( ! method_exists($this->tmp_contract_m,'stop_service')) 
				{
					redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
				}

				$stat = $this->tmp_contract_m->stop_service($term);
				if(TRUE === $stat) $this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
			}
	
			redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
		}


		// HỦY BỎ HỢP ĐỒNG
		if($this->input->post('remove_contract') !== NULL)
		{
			$this->log_m->insert(array(
				'log_type' =>'remove_contract_time',
				'user_id' => $this->admin_m->id,
				'term_id' => $edit_id,
				'log_content' => my_date(time(),'Y/m/d H:i:s')
			));

			$this->contract_m->update($edit_id, array('term_status'=>'remove'));	
			$this->messages->success('Hợp đồng đã được hủy bỏ.');
		}

		// KHÓA HĐ
		if($this->input->post('lock_editable') !== NULL){
			$lock = get_term_meta_value($edit_id,'lock_editable');
			update_term_meta($edit_id,'lock_editable',force_var($lock,1,0));
		}


		// CẬP NHẬT CHI TIẾT HỢP DỒNG THIẾT KẾ BANNER
		if($this->input->post('addition_service_submit') !== NULL)
		{
			//them hop dong thiet ke banner cho facebook updateBY HungLam---------
			$facebook_nomal_banner = $this->input->post('meta[facebook_nomal_banner]');
			//-------------------------------------------------------------------------
			$desigh_nomal_banner=$this->input->post('meta[desigh_nomal_banner]');
			$resize_nomal_banner=$this->input->post('meta[resize_nomal_banner]');
			$combo_nomal_banner=$this->input->post('meta[combo_nomal_banner]');
			$desigh_banner=$this->input->post('meta[desigh_banner]');
			$resize_banner=$this->input->post('meta[resize_banner]');
			$combo_banner=$this->input->post('meta[combo_banner]');

			//them hop dong thiet ke banner cho facebook updateBY HungLam---------
			if($facebook_nomal_banner==FALSE)
			{
				update_term_meta($edit_id,'facebook_nomal_banner','');	
			}
			//-------------------------------------------------------------------------
			if ($desigh_nomal_banner==FALSE) 
			{
				update_term_meta($edit_id,'desigh_nomal_banner','');
			}
			if($resize_nomal_banner==FALSE)
			{
				update_term_meta($edit_id,'resize_nomal_banner','');
			}
			if ($combo_nomal_banner==FALSE) 
			{
				update_term_meta($edit_id,'combo_nomal_banner','');
			}
			if ($desigh_banner==FALSE)
			{
			 	update_term_meta($edit_id,'desigh_banner','');
			}
			if ($resize_banner==FALSE)
			{
				update_term_meta($edit_id,'resize_banner','');
			}
			if ($combo_banner==FALSE)
			{
				update_term_meta($edit_id,'combo_banner','');
			}
		}

		// CHI TIẾT DỊCH VỤ CHO TỪNG HĐ
		if($this->input->post('addition_service_submit') !== NULL)
		{
			$meta = $this->input->post('meta');
			if(!empty($meta))
			{
				// Cập nhật lại thời gian kết thúc hợp đồng khi thay đổi số tháng hosting
				if(isset($meta['hosting_months'])) 
				{
					$meta['contract_end']  = strtotime('+' . $meta['hosting_months'] . ' month', get_term_meta_value($edit_id, 'contract_begin')) ;
				}

				// CẬP NHẬT PHÍ KHỞI TẠO VÀ DUY TRÌ NĂM ĐẦU TIÊN
				if(isset($meta['domain_fee_initialization_maintain'])) 
				{
					$meta['domain_fee_initialization_maintain']  = $meta['domain_fee_initialization'] + $meta['domain_fee_maintain'];
				}

				// CẬP NHẬT THỜI GIAN KẾT THÚC HỢP ĐỒNG :  
				if(isset($meta['domain_number_year_register'])) 
				{
					$domain_number_year_register = $meta['domain_number_year_register'] ;				
					$meta['contract_end'] = strtotime("+ $domain_number_year_register year", get_term_meta_value($edit_id, 'contract_begin')) ;
				}


				if(!empty($meta['addition_hosting']['start_time']))
				{
					$meta['addition_hosting']['start_time'] = strtotime($meta['addition_hosting']['start_time']);
				}

				if(!empty($meta['addition_domain']['start_time']))
				{
					$meta['addition_domain']['start_time'] = strtotime($meta['addition_domain']['start_time']);
				}

				foreach ($meta as $key => $value) 
				{
					$value = is_array($value) ? serialize($value) : $value;
					update_term_meta($edit_id,$key,$value);
				}
			}

			// Tính lại giá trị hợp đồng theo từng loại hợp đồng
			$term   = $this->contract_m->get($edit_id);			# check for fit contract type , like : webgeneral|googleads|...

			$model  = $this->contract_m->get_contract_model($term);
			
			if(!empty($model))
			{
				$this->load->model($model, 'tmp_contract_m');

				if(method_exists($this->tmp_contract_m,'calc_contract_value'))
				{
					$contract_value = $this->tmp_contract_m->calc_contract_value($edit_id);
					update_term_meta($edit_id,'contract_value',$contract_value);
					redirect(module_url("edit/{$edit_id}"),'refresh');
				}
			}

			redirect(module_url("edit/{$edit_id}"),'refresh');
		}

		// TẠM ẨN 2 BUTTON: NGỪNG KÍCH HOẠT + KÍCH HOẠT DOMAIN
		// // KÍCH HOẠT: DOMAIN
		// if($this->input->post('start_process') !== NULL)
		// {
		// 	$term 				  = $this->term_m->get($edit_id);
		// 	$model = $this->contract_m->get_contract_model($term);

		// 	if(!$model) redirect(admin_url("contract/edit/{$edit_id}"),'refresh');

		// 	$this->load->model($model,'tmp_contract_m');
		// 	if( ! method_exists($this->tmp_contract_m,'proc_service')) 
		// 	{
		// 		redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
		// 	}

		// 	$status_proc_service  = $this->tmp_contract_m->proc_service($term);

		// 	if(FALSE === $status_proc_service) $this->messages->error('Kích hoạt domain không thành công!') ;
		// 	if(TRUE === $status_proc_service) $this->messages->success('Hệ thống gửi mail kích hoạt domain thành công');
		// 	redirect(admin_url("contract/edit/{$edit_id}"),'refresh');	
		// }

		// // NGỪNG KÍCH HOẠT: DOMAIN
		// if($this->input->post('end_process') !== NULL)
		// {
		// 	$term 				  = $this->term_m->get($edit_id);
		// 	$model = $this->contract_m->get_contract_model($term);

		// 	if(!$model) redirect(admin_url("contract/edit/{$edit_id}"),'refresh');

		// 	$this->load->model($model,'tmp_contract_m');
		// 	if( ! method_exists($this->tmp_contract_m,'stop_service')) 
		// 	{
		// 		redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
		// 	}

		// 	$stat = $this->tmp_contract_m->stop_service($term);
		// 	if(TRUE === $stat) $this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
		// 	redirect(admin_url("contract/edit/{$edit_id}"),'refresh');	
		// }


		if($this->input->post('service_list_submit') !== NULL)
		{
			update_term_meta($edit_id,'services_pricetag', serialize($this->input->post('services_pricetag')));

			$term = $this->contract_m->get($edit_id);
			$is_webgeneral = !empty($term) && $term->term_type == 'webgeneral';
			if($is_webgeneral)
			{
				$this->load->model('webgeneral/webgeneral_contract_m');
				$contract_value = get_term_meta_value($term->term_id,'contract_value');
				$recalc_contract_value = $this->webgeneral_contract_m->calc_contract_value($term);
				if($contract_value != $recalc_contract_value)
					update_term_meta($term->term_id,'contract_value',$recalc_contract_value);
			}

			redirect(module_url("edit/{$edit_id}"),'refresh');
		}

		// GIA HẠN: HOSTING + DOMAIN
		if($this->input->post('btn_renew') !== NULL)
		{
			$term = $this->contract_m->get($edit_id);
			$term_type = $term->term_type ?: 'hosting';

			switch ($term_type)
			{
				case 'hosting':
					$hosting_contract_end  = $this->input->post('hosting_contract_end');			
					$hosting_contract_end  = strtotime($hosting_contract_end);
					$hosting_months        = $this->input->post('hosting_months');
					if($hosting_contract_end !== NULL && $hosting_months !== NULL) { 
						$data = array('hosting_contract_end' => $hosting_contract_end, 'hosting_months' => $hosting_months) ;
					}

					break;
				case 'domain':
					$domain_contract_end  		  = $this->input->post('domain_contract_end');			
					$domain_contract_end  		  = strtotime($domain_contract_end);
					$domain_number_year_register  = $this->input->post('domain_number_year_register');
					if($domain_contract_end !== NULL) { 
						$data = array('domain_contract_end' => $domain_contract_end, 'domain_number_year_register' => $domain_number_year_register) ;
					}
					break;

				default:
					# code...
					break;
			}

			$model 		 = $this->contract_m->get_contract_model($term);
			if(!empty($model))
			{
				$this->load->model($model, 'model_temp');
				$clone_id 	= $this->model_temp->renew($edit_id, $data);
				redirect(module_url("edit/{$clone_id}"),'refresh');
			}
		}

		redirect(module_url("edit/{$edit_id}"),'refresh');
	}

	private function hooks_to_edit(){

		$this->hook->add_action('fetch_contract_source', function(){
			$contract_status = $this->config->item('contract_status');
			$role_id = $this->admin_m->role_id;

			unset($contract_status['publish']);

			if(in_array($role_id, $this->config->item('salesexecutive','groups'))){
				unset($contract_status['pending']);
				// unset($contract_status['publish']);
				unset($contract_status['ending']);
				unset($contract_status['liquidation']);
			}

			if(!empty($contract_status))
				$contract_status = str_replace('"','\'',json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},
				array_keys($contract_status) , $contract_status)));
			
			return $contract_status;
		});
	}

	protected function get_customer_by_role($as_array = FALSE){

		$user_type = 'system';
		$role_id = $this->admin_m->role_id;
		$allow_roles = $this->config->item('read_customers_role_id');
		$this->data['is_system'] = TRUE;

		if(in_array($role_id, $allow_roles)){
			$user_type = 'customer_';
			$this->data['is_system'] = FALSE;
		}

		$users = $this->customer_m->set_get_customer($user_type)->order_by('display_name')->get_many_by();

		if($as_array){

			$array_key_values = array();

			if(!empty($users))
				foreach ($users as $user)
					$array_key_values[$user->user_id] = $user->display_name;

			return $array_key_values;
		}

		return $users;
	}

	/**
	 * Gets the contract model based on.
	 *
	 * @param      <type>  $term   The term
	 *
	 * @return     mixed  The contract model if EXISTS othersise return FALSE.
	 */
	public function get_model($term)
	{
		$model = FALSE;

		switch ($term->term_type) 
		{
			case 'hosting' : $model = 'hosting/hosting_wizard_m'; break;
			case 'onead' : $model = 'onead/onead_wizard_m'; break;
			case 'oneweb' : $model = 'oneweb/oneweb_wizard_m'; break;
			case 'pushdy' : $model = 'pushdy/pushdy_wizard_m'; break;
			case 'fbcontent' : $model = 'fbcontent/fbcontent_wizard_m'; break;
			default: $model = 'contract/contract_wizard_m'; break;
		}

		return $model;
	}
}
/* End of file Create_wizard.php */
/* Location: ./application/modules/contract/controllers/Create_wizard.php */