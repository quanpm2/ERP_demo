<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		restrict('contract.revenue.manage');

		$data = $this->data;
		$this->template->is_box_open->set(1);
		$this->template->title->set('Thống kê chi tiết dự thu');
		$this->template->description->set('Tất cả hợp đồng đã cấp số và đang thực hiện đến ngày '.my_date(time(),'d/m/Y'));

		$user_ids = has_permission('contract.revenue') ? [] : [$this->admin_m->id];

		$data['user_ids'] 		= $user_ids;

		$data['ajaxUrl'] = array(
			'usersGroupsRevenue' 	=> admin_url('contract/ajax/dataset/revenueUsersGroups'),
			'usersRevenue' 			=> admin_url('contract/ajax/dataset/revenueUsers'),
			'terms' 				=> admin_url('contract/ajax/dataset/usersrevenue')
		);

		$data['jsobjectdata'] 	= json_encode($data);
		
		parent::render($data,'dashboard/index');
	}

	/**
	 * Trang tổng quan hợp đồng theo ngành nghề
	 */
	public function service()
	{
		restrict('admin.contract.manage,admin.contract.access');

		$data = $this->data;
		parent::render($data,'dashboard/category');
	}

	/**
	 * Trang tổng quan hợp đồng theo ngành nghề
	 */
	public function category()
	{
		restrict('admin.contract.manage,admin.contract.access,contract.category.manage,contract.category.access');

		$data = $this->data;
		parent::render($data,'dashboard/category');
	}
}
/* End of file Dashboard.php */
/* Location: ./application/modules/contract/controllers/Dashboard.php */