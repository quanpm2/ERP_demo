<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contract extends Admin_Controller {

	public $model = 'contract_m';

	public function __construct(){

		parent::__construct();

		$models = array(
			'term_users_m',
			'invoice_m',
			'customer/customer_m',
			'contract_m',
			'contract_wizard_m',
			'contract/contract_report_m',
			'contract/category_m',
			'base_contract_m',
			'staffs/sale_m',
			);

		$this->load->model($models);

		$this->load->config('contract');
		$this->load->config('staffs/group');
		$this->load->config('invoice');

		// $this->load->add_package_path(APPPATH.'third_party/google-analytics/');
		// $this->load->library('ga');

		$this->data['step'] = $this->input->get('step') ? $this->input->get('step') : 0 ;
	}
	
	/**
	 * Trang tổng quan - thống kê chi tiết hợp đồng
	 */
	public function dashboard()
	{
		$data = $this->data;
		parent::render($data);
	}


	/**
	 * DATATABLE LIST ALL CONTRACTS
	 */
	public function index()
	{
		restrict('admin.contract.view');

		$this->template->is_box_open->set(1);
		$this->template->title->set('Quản lý hợp đồng');
		$this->template->description->set('Trang danh sách tất cả các hợp đồng');

		parent::render($this->data);
	}
	
	public function receipt()
	{	
		restrict('contract.receipt.access');
		
		$this->template->is_box_open->set(1);
		$this->template->title->set('Danh sách thu tiền');
		$this->template->javascript->add(base_url("dist/vContractReceiptsAdminIndexPage.js?v=2"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		$this->template->stylesheet->add(base_url('node_modules/vue2-dropzone/dist/vue2Dropzone.min.css'));
		$this->template->publish();
	}


    public function audit()
	{
		$this->template->is_box_open->set(1);
		$this->template->title->set('Data log');
        $this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
        $this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->javascript->add(base_url("dist/vContractAuditIndex.js?v=2"));
		$this->template->publish();
    }

    public function report()
	{	
		restrict('admin.report.access');
	
		$this->template->is_box_open->set(1);
		$this->template->title->set('Báo cáo tổng quan');
		$this->template->javascript->add(base_url("dist/vContractReport.js?v=12"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		$this->template->stylesheet->add(base_url('node_modules/vue2-dropzone/dist/vue2Dropzone.min.css'));
		$this->template->publish();
	}

	/**
	 * Contract edit|view|action_handlers page
	 *
	 * @param      integer  $edit_id  The Contract identifier
	 */
	public function edit($edit_id = 0)
	{
		restrict('admin.contract.view');

		$contract = $this->contract_m->set_term_type()->get($edit_id);
		if(FALSE === $this->contract_m->has_permission($edit_id, 'admin.contract.view') || !$contract)
		{
			$this->messages->error('Không có quyền truy xuất hoặc hợp đồng không khả dụng !');
		redirect(module_url(), 'refresh');
		}		

		$this->submit($edit_id);

		$data = $this->data;
		$contract_code = get_term_meta_value($contract->term_id, 'contract_code') ?: $contract->term_name;
		$this->template->title->set($contract_code.' | Hợp đồng '.$this->config->item($contract->term_type, 'services'));
		$this->hooks_to_edit();

		$term = $this->contract_m->set_term_type()->get($edit_id);
		$term->extra = @unserialize(get_term_meta_value($term->term_id, 'extra'));

		/* Load danh sách nhân viên kinh doanh */
		$staffs = $this->sale_m->select('user_id,user_email,display_name')->set_user_type()->set_role()->as_array()->order_by('user_email')->get_all();
		$staffs = array_map(function($x){ $x['display_name'] = $x['display_name'] ?: $x['user_email']; return $x; }, $staffs);
		$data['staffs'] = key_value($staffs, 'user_id', 'display_name');

		if($term->term_parent > 0)
		{
			$website = $this->term_m->get($term->term_parent);
			if($website)
			{
				$website->term_name = prep_url($website->term_name);
			}

			$term->website = $website;
		}

		$customers = $this->term_users_m->get_term_users($term->term_id, array('customer_person','customer_company','system'));
		if(!empty($customers))
		{
			
			$term->customer = end($customers);
			$data['cus_belongs'] = array_map(function($x){return $x->user_id;}, $customers);
		}

		$data['websites'] = array();
		$data['customers'] = $this->get_customer_by_role(TRUE);

		if(!empty($data['cus_belongs']))
		{
			$_webs = $this->term_users_m
			->where(array('term_users.user_id' => reset($data['cus_belongs']),'term.term_type'=>'website'))
			->join('term','term.term_id = term_users.term_id')
			->get_many_by();

			if(!empty($_webs))
			{
				foreach ($_webs as $w)
				{
					$data['websites'][$w->term_id] = $w->term_name;
				}
			}
		}

		if(!empty($term->term_parent))
		{
			$bonus_website = $this->contract_m->get($term->term_parent);
			if(!empty($bonus_website))
			{
				$data['websites'][$bonus_website->term_id] = $bonus_website->term_name;
			}
		}

		/* Load thông tin giảm giá */
		$this->load->model('contract/discount_m');
		$discount_m = $this->discount_m->set_contract($term)->get_instance();
		$dpackages 	= $discount_m->get_packages();

		/* Load danh sách các ngành nghề */
		$categories	 = $this->category_m->set_term_type()->order_by('term_parent,term_name')->get_all();
		$hcategories = $this->category_m->get_recursive($categories);
		$categories  = $this->category_m->get_recursive_array($hcategories,0,'_');

		// Load thông tin ngành nghề được mapping với hợp đồng
		$term_categories 			= $this->term_categories_m->get_categories($term->term_id) ?: array();
		$data['term_categories']	= array_column($term_categories,'term_id');
		$data['categories'] 		= $categories;

		$data['edit']			= $term;
		$data['is_manage']		= has_permission('admin.contract.manage');
		$data['ajax_segment']	= "contract/ajax_dipatcher/ajax_edit/{$term->term_id}";
		$data['invoice_url']	= module_url('invoices/');

		$data['is_lock_editable']                    = (bool) get_term_meta_value($edit_id, 'lock_editable');
		$data['has_edit_permission']                 = $this->contract_m->has_permission($edit_id, 'admin.contract.edit');
		$data['has_verification_permission']          = $this->contract_m->has_verification_permission($edit_id);
		$data['has_start_contract_permission']       = $this->contract_m->has_start_contract_permission($edit_id);
		$data['has_stop_contract_permission']        = $this->contract_m->has_stop_contract_permission($edit_id);
		$data['has_renew_contract_permission']       = $this->contract_m->has_renew_contract_permission($edit_id);
		$data['has_lock_permission']                 = $this->contract_m->has_lock_permission($edit_id);
		$data['has_destroy_permission']              = $this->contract_m->has_destroy_permission($edit_id);
		$data['has_manipulation_lock_permission']    = $this->contract_m->has_manipulation_lock_permission($edit_id);
		$data['manipulation_locked']                 = $this->option_m->get_value('manipulation_locked', TRUE);

		parent::render($data);
	}

	
	/**
	 * SETTING PAGE FOR ADMIN
	 */
	public function setting()
	{
		restrict('contract.setting.access');

		$this->load->config('staffs/staffs');

		$this->template->is_box_open->set(1);
		$this->template->title->append('Cấu hình hạn mức bảo lãnh');

		$data = $this->data;
		$data['bailment_levels'] 	= $this->config->item('bailment_levels');
		$data['ajaxUrl'] 			= array('settingUserBailment' => admin_url('staffs/ajax/bailment/update_userdata'));
		$data['jsdataobject'] 		= json_encode($data);

		parent::render($data);
	}

	public function ajax_dipatcher($callable = '')
	{
		restrict('admin.contract.edit,order_contact.contract.add');

		$post = $this->input->post();

		if(empty($post)) return FALSE;
		
		if(method_exists($this->contract_wizard_m, "wizard_{$callable}"))
			$this->contract_wizard_m->{'wizard_'.$callable}($post);

		$this
		->add_filter_upload_file($post)
		->add_filter_ajax_edit($post)
		->add_filter_upload_real_file($post);
		
		if($this->hook->has_filter("ajax_{$callable}"))
		{
			$this->hook->add_filter("ajax_{$callable}",function($post){

				$result = array('response'=>$post, 'msg'=>'Cập nhật nhật thành công','success'=>TRUE);

				if( ! empty($post['msg'])){

					$result['msg'] = $post['msg'];

					unset($post['msg']);
				}

				if( isset($post['success'])){

					$result['success'] = $post['success'];

					unset($post['success']);
				}

				if( ! empty($post['edit']['meta'])) 
				{
					foreach ($post['edit']['meta'] as $key => $value)
					{
						$this->termmeta_m->update_meta($post['edit']['term_id'], $key, $value);
					}
				}

				unset($post['edit']['meta']);

				if( ! empty($post['edit']))
				{
					
					if(!empty($post['edit']['term_status']) && $post['edit']['term_status'] == 'publish'){

						$order_id = get_term_meta_value($post['edit']['term_id'], 'order_id');
						if(!empty($order_id)) 
							$this->post_m->update($order_id,array('post_status'=>'publish'));
					}

					$this->term_m->update($post['edit']['term_id'], $post['edit']);
				}

				return $result;
			});
		}

		$result = $this->hook->apply_filters("ajax_{$callable}", $post);

		if(!is_array($result)){

			$result = array($result);
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));

		return $this->output;
	}

	public function add_filter_upload_file($post){

		$this->hook->add_filter('ajax_act_upload_file',	function($post){

			$result = array('msg'=>'File upload không hợp lệ !','success'=>FALSE);

			$post = $this->input->post();
			if(empty($post))  return $result;

			if( ! $this->contract_m->has_permission($post['term_id'], 'admin.contract.edit'))
			{
				return array('msg'=>'Không thể thực hiện tác vụ, quyền truy cập bị hạn chế','success'=>FALSE);
			}

			$upload_config = $this->config->item('upload_config');
			$upload_config['file_name']	= $post['term_name'] . '.' . $post['term_id'] . '.' . time();
			$upload_config['file_name'] = str_replace('.', '_', $upload_config['file_name']);
			$upload_config['allowed_types'] = 'doc|docx';
			$upload_config['upload_path'].= date('Y') . '/' . date('m') . '/';

			if(!is_dir($upload_config['upload_path']))
				mkdir($upload_config['upload_path'], '0777', TRUE);

			$this->load->library('upload');
			$this->upload->initialize($upload_config);

			if ($this->upload->do_upload('fileinput')){

				$data = $this->upload->data();

				$_tmp_queue_uploaded = @unserialize(get_term_meta_value($post['term_id'], '_tmp_file_upload'));
				if(empty($_tmp_queue_uploaded) || !is_array($_tmp_queue_uploaded))
					$_tmp_queue_uploaded = array();

				$_tmp_queue_uploaded[] = array('file_name'=>$data['file_name'],'file_path'=>$upload_config['upload_path'].$data['file_name']);
				$_tmp_queue_uploaded = arrayUnique($_tmp_queue_uploaded);

				$this->termmeta_m->update_meta($post['term_id'],'_tmp_file_upload', serialize($_tmp_queue_uploaded));

				$_tmp_queue_uploaded = array(array('file_name'=>$data['file_name'],'file_path'=>$upload_config['upload_path'].$data['file_name']));

				$result['ret_obj'] = $_tmp_queue_uploaded;
				$result['msg'] = 'Upload file hợp đồng thành công !';
				$result['success'] = TRUE;
			}

			return $result;
		});

		return $this;
	}

	public function add_filter_upload_real_file($post){

		$this->hook->add_filter('ajax_act_upload_real_file', function($post){

			$result = array('msg'=>'File upload không hợp lệ !','success'=>FALSE);

			$post = $this->input->post();

			if(empty($post)) return $result;

			if( ! $this->contract_m->has_permission($post['term_id'], 'admin.contract.edit'))
			{
				return array('msg'=>'Không thể thực hiện tác vụ, quyền truy cập bị hạn chế','success'=>FALSE);
			}

			$upload_config = $this->config->item('upload_config');

			$upload_config['file_name']	= $post['term_name'] . '.' . $post['term_id'] . '.' . time();

			$upload_config['file_name'] = str_replace('.', '_', $upload_config['file_name']);

			$upload_config['allowed_types'] = 'doc|docx';

			$upload_config['upload_path'].= date('Y') . '/' . date('m') . '/';

			if(!is_dir($upload_config['upload_path'])){

				mkdir($upload_config['upload_path'], '0777', TRUE);
			}

			$this->load->library('upload');

			$this->upload->initialize($upload_config);

			if ($this->upload->do_upload('fileinput')){

				$data = $this->upload->data();

				$_attachment_files = @unserialize(get_term_meta_value($post['term_id'], '_attachment_files'));

				if(empty($_attachment_files)) $_attachment_files = array();

				$_attachment_files[] = array('file_name'=>$data['file_name'],'file_path'=> $upload_config['upload_path'] . $data['file_name']);

				$_attachment_files = arrayUnique($_attachment_files);

				$this->termmeta_m->update_meta($post['term_id'],'_attachment_files', serialize($_attachment_files));

				$result['ret_obj'] = array(array('file_name'=>$data['file_name'],'file_path'=> $upload_config['upload_path'] . $data['file_name']));

				$result['msg'] = 'Upload file hợp đồng thành công !';

				$result['success'] = TRUE;
			}

			return $result;
		});

		return $this;
	}

	public function add_filter_ajax_edit($post)
	{
		$this->hook->add_filter('ajax_ajax_edit', function($args){
			
			$post = array('edit' => array('term_id' => $args['pk'], 'meta' => array()));

			if( ! $this->contract_m->has_permission($post['edit']['term_id'], 'admin.contract.edit'))
			{
				return array('msg'=>'Không thể thực hiện tác vụ, quyền truy cập bị hạn chế','success'=>FALSE);
			}

			$this->load->library('form_validation');

			if(isset($args['value']) && is_string($args['value']))
			{	
				$args['value'] = trim($args['value']);
				if($args['value'] == '')  return array('msg'=>'Vui lòng nhập trường này','success'=>FALSE);
			}

			switch ($args['name']) {

				case 'term_parent':
					if($args['value'] == 0) break;
					if($web_term = $this->term_m->get($args['value']))
						$this->term_m->update($post['edit']['term_id'], array('term_name'=>$web_term->term_name));	
					break;

				case 'representative_email':
					if(!$this->form_validation->valid_email($args['value']))
						return array('msg'=>'Địa chỉ email không hợp lệ','success'=>FALSE);
					break;

				case 'exchange_rate_usd_to_vnd':
					if(!$this->form_validation->is_natural($args['value']))
						return array('msg'=>'Số tiền không hợp lệ','success'=>FALSE);
					break;

                case 'exchange_rate_aud_to_vnd':
                    if(!$this->form_validation->is_natural($args['value']))
                        return array('msg'=>'Số tiền không hợp lệ','success'=>FALSE);
                    break;

				case 'network_type':
					if(empty($args['value'])) 
						return array('msg'=>'Vui lòng chọn ít nhất 1 kênh quảng cáo','success'=>FALSE);
					$network_types = $args['value'];

					$args['value'] = implode(',', $network_types);

					break;

				case 'payment_bank_account':
					if(empty($args['value'])) 
						return array('msg'=>'Vui lòng chọn một','success'=>FALSE);
					$network_types = $args['value'];
					
					$args['value'] = implode(',', $network_types);

					break;	

				 case 'payment_banner':
					if(empty($args['value'])) 
					{
						return array('msg'=>'Vui lòng chọn một','success'=>FALSE);
					}
					if (count($args['value'])>1) {
						return array('msg'=>'Chỉ được chọn một hình thức thanh toán','success'=>FALSE);
					}
					
					$network_types = $args['value'];	
					$args['value'] = implode(',', $network_types);

					break;		

				case 'user_id':

					$this->term_users_m
					->set_term_users($post['edit']['term_id'],array($args['value']),array('customer_person','customer_company'),array('command'=>'replace'));

					$this->term_m->update($post['edit']['term_id'], array('term_parent'=>0,'term_name'=>''));

					$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));

					$post['response'] = array('jscallback'=> $jsfunction);
					return $post;

					break;

				case 'term_type':

					$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));
					$post['response'] = array('jscallback'=> $jsfunction);
					break;
			}

			switch ($args['type']) {

				case 'meta_data':

					$timestamp_fields = array('contract_begin',
						'contract_end',
						'googleads-end_time',
						'googleads-begin_time',
						'started_service',
						'start_service_time',
						'end_service_time'
						);


					$is_field_type_timestamp = in_array($args['name'], $timestamp_fields);
					if($is_field_type_timestamp)
						$args['value'] = strtotime($args['value']);

					// CẬP NHẬT LẠI GÓI HOSTING
					if($args['name'] == 'hosting_service_package')
					{
						$this->config->load('hosting/hosting');
						$hosting_package_default = $this->config->item('service','packages');

						$selected_package = $hosting_package_default[$args['value']] ?? FALSE;
						update_term_meta($post['edit']['term_id'],'hosting_disk',($selected_package['disk']??0));
						update_term_meta($post['edit']['term_id'],'hosting_bandwidth',($selected_package['bandwidth']??0));
						update_term_meta($post['edit']['term_id'],'hosting_price',($selected_package['price']??0));
						update_term_meta($post['edit']['term_id'],'hosting_email_webmail',($selected_package['email_webmail']??0));

						$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));
						$post['response'] = array('jscallback'=> $jsfunction);
					}

					// CẬP NHẬT LẠI NHÂN VIÊN KINH DOANH PHỤ TRÁCH
					elseif($args['name'] == 'staff_business' && !empty($args['value']))
					{
						$term_id 		= $post['edit']['term_id'];
						$staff_business = $args['value'];

						$created_by 	= get_term_meta_value($term_id, 'created_by') ?: $this->admin_m->id;

						$users = array($created_by, $staff_business);

						$this->term_users_m->set_relations_by_term($term_id, $users, 'admin', array(), TRUE);
					}					

					$post['edit']['meta'] = array($args['name']	=> $args['value']);
					break;

				case 'extra':

					$extra = @unserialize(get_term_meta_value($post['edit']['term_id'], 'extra'));
					if(empty($extra)) 
						$extra = array();
					$extra[$args['name']] = $args['value'];

					$post['edit']['meta']['extra'] = serialize($extra);
					break;

				case 'field':

					$post['edit'][$args['name']] = $args['value'];
					break;
			}

			return $post;
		});

		return $this;
	}

	public function render_customer_info($user_id){

		$this->load->model('customer/customer_m');

		$user = $this->customer_m->like('user_type', 'customer_', 'after')->where('user_id',$user_id)->get_by();

		if(empty($user)) return ;

		$html = '';

		if($user->user_type == 'customer_company'){

			$html.= $this->admin_form->hidden('','edit[meta][customer_id]',$user->user_id) .

			$this->admin_form->input('Tên tổ chức', '', $user->display_name,'',array('disabled'=>true)) .

			$this->admin_form->input('Người đại diện', '', 
				($this->usermeta_m->get_meta_value($user->user_id,'customer_gender') == 1 ? 'Ông ' : 'Bà ') . 
				$this->usermeta_m->get_meta_value($user->user_id,'customer_name'),'',array('disabled'=>true)) .

			$this->admin_form->input('Địa chỉ', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_address'),'',array('disabled'=>'disabled')) .

			$this->admin_form->input('E-mail', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_email'),'',array('disabled'=>'disabled')) .

			$this->admin_form->input('Số điện thoại', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_phone'),'',array('disabled'=>'disabled')) .

			$this->admin_form->input('Mã số thuế','',$this->usermeta_m->get_meta_value($user->user_id,'customer_tax'),'',array('disabled'=>'disabled'));

		}
		else if($user->user_type == 'customer_person'){

			$html.= $this->admin_form->hidden('','edit[meta][customer_id]',$user->user_id	) .

			$this->admin_form->input('Người đại diện', '', 
				($this->usermeta_m->get_meta_value($user->user_id,'customer_gender') == 1 ? 'Ông ' : 'Bà ') . 
				$user->display_name,'',array('disabled'=>'disabled')) .

			$this->admin_form->input('Địa chỉ', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_address'),'',array('disabled'=>'disabled')) .

			$this->admin_form->input('E-mail', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_email'),'',array('disabled'=>'disabled')) .

			$this->admin_form->input('Số điện thoại', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_phone'),'',array('disabled'=>'disabled'));
		}

		if($this->input->is_ajax_request())
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array('content' => $html)));

			return;
		}

		return $html;
	}

	public function check_domain()
	{
		$url = $this->input->post('domain');
		$result = $this->contract_m->check_domain($url);
		echo ($result ? 'true' : 'false');
	}

	/* Clone Hợp đồng */
	public function clone($term_id = 0)
	{
		try
		{
			$insert_id = (new contract_m())->set_contract($term_id)->get_behaviour_m()->clone();
			$this->messages->success('Quá trình tạo bản sao thành công.');
			redirect("{$this->data['url_edit']}/{$insert_id}", 'refresh');
		} 
		catch(Exception $e)
		{
			$this->messages->error($e->getMessage());
			redirect(module_url(), 'refresh');
		}
	}

    /**
     * Preview Contract Printable 
     *
     * @param      integer  $term_id  The term identifier
     */
    public function preview($term_id)
    {
        restrict('admin.contract.view');

        if( ! $this->contract_m->has_permission($term_id, 'admin.contract.view'))
        {
        	$this->messages->error('Hợp đồng này bị giới hạn quyền truy cập.');
			redirect(module_url(), 'refresh');	
        }

        $this->contract_m->set_contract($term_id);

        if( ! $this->contract_m->get_contract())
        {
        	return parent::render404('Hợp đồng không tồn tại khoặc đã bị xóa');
        }

        $data 		= $this->contract_m->prepare_preview();

        $custom_printable_view_path = get_term_meta_value($term_id, 'custom_printable_view_path') ;
        
        $view_file = $custom_printable_view_path ?: ($data['view_file'] ?? 'admin/printable');

        $this->load->view($view_file, $data);
    }
 
    public function printable($term_id)
    {
    	$this->load->model('contract/content_m');

		$this->contract_m->set_contract($term_id);

		$data 		= $this->contract_m->prepare_preview();
		$view_file 	= $data['view_file'] ?? 'admin/printable';

        $content 	= $this->load->view($view_file, $data, TRUE);

        $_contract_content = $this->term_posts_m->get_term_posts($term_id, 'contract_content');
        $_contract_content AND $_contract_content = reset($_contract_content);

        $custom_printable_view_path = get_term_meta_value($term_id, 'custom_printable_view_path');

        if(empty($_contract_content))
        {
        	$postId = $this->content_m->insert([
        		'post_title' => get_term_meta_value($term_id, 'contract_code'),
        		'post_content' => $this->load->view($view_file, $data, TRUE),
        		'post_type' => $this->content_m->post_type,
        		'created_on' => time(),
        		'post_author' => $this->admin_m->id
        	]);

        	$_contract_content = $this->content_m->get($postId);

        	$this->texrm_posts_m->set_term_posts( $term_id, array($postId), $this->content_m->post_type, FALSE);
        }
        else
        {
        	// die($this->load->view($view_file, $data, TRUE));
        	// $_contract_content->post_content = $this->load->view($view_file, $data, TRUE);
        	// $this->content_m->update($_contract_content->post_id, ['post_content' => $_contract_content->post_content]);
        	// $this->term_posts_m->delete_posts_cached($term_id);
        	// die($_contract_content->post_content);
        }

        $_contract_content = $this->content_m->get($_contract_content->post_id);

        $this->load->view('admin/view', ['content' => $_contract_content->post_content]);
    }


	public function submit($edit_id = 0)
	{
		if( ! $this->input->post()) return FALSE;

		// CẬP NHẬT KHÁCH HÀNG
		if($this->input->post('update_customer') !== NULL 
			&& $this->contract_m->has_permission($edit_id,'admin.contract.edit'))
		{
			$post = $this->input->post();

			$term = $this->term_m->get($post['edit']['term_id']);

			$exists_term_users = $this->term_users_m->get_term_users($term->term_id, array('customer_person', 'customer_company','system'));
			$exists_term_users = empty($exists_term_users) ? array() : array_map(function($x){return $x->user_id;},$exists_term_users);

			if(!in_array($post['edit']['user_id'], $exists_term_users)){

				if(!empty($exists_term_users))
					$this->term_users_m->where_in('term_users.user_id', $exists_term_users);

				$this->term_users_m
				->where('term_users.term_id', $post['edit']['term_id'])
				->delete_by();	

				$this->term_users_m->set_term_users($post['edit']['term_id'], array($post['edit']['user_id']), array('customer_person', '		customer_company','system'), array('command'=>'replace'));

				$this->messages->success('Thông tin khách hàng đã được cập nhật thành công vào hợp đồng');
				// Mapping website to customer
				if(!empty($term->term_parent)){

					$owner = $this->term_users_m->get_term_users($term->term_parent,array('customer_person','customer_company','system'));
					if(!empty($owner)){

						if(is_array($owner))
							$owner = end($owner);

						if($owner->user_type == 'system'){

							$this->term_users_m
							->where('term_id',$term->term_parent)
							->where('user_id',$owner->user_id)
							->delete_by();

							$this->term_users_m
							->set_term_users($term->term_parent, 
								array($post['edit']['user_id']), 
								array('customer_person', 'customer_company','system'), 
								array('command'=>'replace'));

							$this->messages->success('Website đã được cập nhật từ "Hệ thống" vào khách hàng');
						}
					}
				}
			}

			redirect(module_url("edit/{$post['edit']['term_id']}"));
		}

		// Tạo hóa đơn mặc định theo từng loại hợp đồng
		if($this->input->post('create_default_invoice') !== NULL 
			&& $this->contract_m->has_permission($edit_id,'admin.contract.edit'))
		{
			$term = $this->contract_m->get($edit_id);

			// Nếu hợp đồng không tồn tại thì thông báo và trả về trang thông tin chi tiết
			if(!$term)
			{
				$this->messages->error('Dịch vụ không tìm thấy hoặc đã bị xóa');
				redirect(module_url("edit/{$edit_id}"),'refresh');
			}

			# check for fit contract type , like : webgeneral|googleads|...
			$model = $this->contract_m->get_contract_model($term);
			if(empty($model))
			{
				$this->messages->error('Dịch vụ không tìm thấy hoặc đã bị xóa');
				redirect(module_url("edit/{$edit_id}"),'refresh');
			}

			$this->load->model($model,'tmp_contract_m');
			$is_created = $this->tmp_contract_m->create_default_invoice($term);

			// Tính toán lại số liệu "Dự thu" "Dự thu quá hạn" "Đã thu" "Còn phải thu"
			$this->base_contract_m->sync_all_amount($term->term_id);
			$this->messages->success('Số liệu thu chi đã được đồng bộ');

			if( ! $is_created)
			{
				$this->messages->error('Quá trình tạo hóa đơn bị gián đoạn.');
				redirect(module_url("edit/{$edit_id}"),'refresh');
			}

			$this->messages->success('Hóa đơn đã được tạo !');

			redirect(module_url("edit/{$edit_id}"),'refresh');
		}

		if($this->input->post('add_invoice') !== NULL 
			&& $this->contract_m->has_permission($edit_id,'admin.contract.edit'))
		{
			Modules::run('contract/invoices/add', $edit_id);
		}

		if($this->input->post('verify_contract') !== NULL)
		{
			if( ! has_permission('admin.contract.manage'))
			{
				$this->messages->error('Không đủ quyền để thực hiện tác vụ này !');
				redirect(module_url("edit/{edit_id}"),'refresh');
			}

			try
			{
				$_contract = new contract_m();
				$_contract->set_contract($edit_id);
				$_contract->get_behaviour_m()->proc_verify();

				/* Add Deal Hubspot Job for another service queue work */
				$this->log_m->insert(array(
					'log_type' =>'callDealHubspotApi',
					'user_id' => $this->admin_m->id,
					'log_status' => 0,
					'log_content' => serialize([
						'action' => 'DealConfirmed',
						'term_id' => $edit_id
					])
				));
			}
			catch (Exception $e)
			{
				$this->messages->error($e->getMessage());
				redirect(module_url("edit/{edit_id}"),'refresh');
			}
			
			$this->messages->success('Hợp đồng đã được xét duyệt và cấp số, sau khi KH thanh toán chi phí thì có thể tiến hành thực hiện hợp đồng !');

			$this->messages->info('Hợp đồng đã chuyển sang trạng thái \"khóa\" - không thể chỉnh sửa thông tin !');

			redirect(module_url("edit/{$edit_id}"),'refresh');
		}

		// CHẠY DỊCH VỤ - Hợp đồng sau khi được cấp số , kết toán (hoặc người có thẩm quyền sẽ tiến hành email thực hiện hợp đồng)
		if($this->input->post('start_service') !== NULL)
		{
			$this->contract_m->set_contract($edit_id);
			$term = $this->contract_m->get_contract();

			$this->term_m->update($edit_id, array('term_status'=> 'publish'));
			update_term_meta($edit_id,'started_service', time());

			/* Log started_service action*/
			$this->log_m->insert(array(
				'log_type' =>'started_service',
				'user_id' => $this->admin_m->id,
				'term_id' => $edit_id,
				'log_content' => date('Y/m/d H:i:s')
				));		

			$this->messages->success('Hợp đồng đã chuyển sang trạng thái thực hiện');

			$model = $this->contract_m->get_contract_model($term);
			if(!$model) redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
			
			// Xử lý các tác vụ ngầm sau khi "Chạy dịch vụ" tùy từng dịch vụ như gửi mail kết nối nội bộ ,...
			if(is_string($model))
			{
				$this->load->model($model,'tmp_contract_m');
				if( ! method_exists($this->tmp_contract_m, 'start_service')) redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
				$this->tmp_contract_m->start_service($term);
			}
			else
			{
				$model->start_service();
			}

			$this->messages->info('Hệ thống gửi mail kết nối thành công .');

			$this->load->model('contract/common_report_m');
			try /* Email truyền thông chúc mừng nhân viên kinh doanh đã ký được hợp đồng */
			{
				$this->common_report_m->init($term)->send_new_contract_congratulation_email();
			}
			catch (Exception $e) { $this->messages->error($e->getMessage()); }

			/* truyền thông chúc mừng nhân viên kinh doanh đã ký được hợp đồng */
			try 
			{
				$this->contract_m->set_contract($edit_id);
				$this->contract_m->mutateIsRenewal();

				$this->common_report_m->init($term)->postSlackNewContractMessage();
			}
			catch (Exception $e)
			{
				$this->messages->error($e->getMessage());
			}

			redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
		}
		
		// KÊT THÚC HỢP ĐỒNG
		if($this->input->post('stop_contract') !== NULL){
			$result = $this->contract_m->stop_contract($edit_id);
			if($result) $this->messages->success('Xử lý tác vụ thành công.');

			$term = $this->contract_m->get($edit_id);
			if($term->term_type == 'domain')
			{
				$model = $this->contract_m->get_contract_model($term);

				if(!$model) redirect(admin_url("contract/edit/{$edit_id}"),'refresh');

				$this->load->model($model,'tmp_contract_m');
				if( ! method_exists($this->tmp_contract_m,'stop_service')) 
				{
					redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
				}

				$stat = $this->tmp_contract_m->stop_service($term);
				if(TRUE === $stat) $this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
			}
	
			redirect(admin_url("contract/edit/{$edit_id}"),'refresh');
		}


		// HỦY BỎ HỢP ĐỒNG
		if($this->input->post('remove_contract') !== NULL)
		{
			$this->log_m->insert(array(
				'log_type' =>'remove_contract_time',
				'user_id' => $this->admin_m->id,
				'term_id' => $edit_id,
				'log_content' => my_date(time(),'Y/m/d H:i:s')
			));

			$this->contract_m->update($edit_id, array('term_status'=>'remove'));	
			$this->messages->success('Hợp đồng đã được hủy bỏ.');
		}

		// KHÓA HĐ
		if($this->input->post('lock_editable') !== NULL){
			$lock = get_term_meta_value($edit_id,'lock_editable');
			update_term_meta($edit_id,'lock_editable',force_var($lock,1,0));
		}

        // KHÓA THAO TÁC HĐ
		if($this->input->post('manipulation_locked') !== NULL){
			$is_locked = (bool) get_term_meta_value($edit_id, 'is_manipulation_locked');
            $_is_locked = force_var($is_locked, 1, 0);
            
            update_term_meta($edit_id, 'is_manipulation_locked', $_is_locked);

            $this->log_m->insert([
                'log_type' =>'lock_manipulation', 
                'user_id' => $this->admin_m->id,
                'term_id' => $edit_id,
                'log_content' => $_is_locked,
                'log_time_create' => my_date(time(),'Y/m/d H:i:s')
            ]);
		}

		// CẬP NHẬT CHI TIẾT HỢP DỒNG THIẾT KẾ BANNER
		if($this->input->post('addition_service_submit') !== NULL) $this->addition_service_submit($edit_id);

		if($this->input->post('service_list_submit') !== NULL
			&& $this->contract_m->has_permission($edit_id,'admin.contract.edit'))
		{
			update_term_meta($edit_id,'services_pricetag', serialize($this->input->post('services_pricetag')));

			$term = $this->contract_m->get($edit_id);
			$is_webgeneral = !empty($term) && $term->term_type == 'webgeneral';
			if($is_webgeneral)
			{
				$this->load->model('webgeneral/webgeneral_contract_m');
				$contract_value = get_term_meta_value($term->term_id,'contract_value');
				$recalc_contract_value = $this->webgeneral_contract_m->calc_contract_value($term);
				if($contract_value != $recalc_contract_value)
					update_term_meta($term->term_id,'contract_value',$recalc_contract_value);
			}

			redirect(module_url("edit/{$edit_id}"),'refresh');
		}

		// GIA HẠN: HOSTING + DOMAIN
		if($this->input->post('btn_renew') !== NULL
			&& $this->contract_m->has_permission($edit_id,'admin.contract.edit'))
		{
			$term = $this->contract_m->get($edit_id);
			$term_type = $term->term_type ?: 'hosting';

			switch ($term_type)
			{
				case 'hosting':
					$hosting_contract_end  = $this->input->post('hosting_contract_end');			
					$hosting_contract_end  = strtotime($hosting_contract_end);
					$hosting_months        = $this->input->post('hosting_months');
					if($hosting_contract_end !== NULL && $hosting_months !== NULL) { 
						$data = array('hosting_contract_end' => $hosting_contract_end, 'hosting_months' => $hosting_months) ;
					}

					break;
				case 'domain':
					$domain_contract_end  		  = $this->input->post('domain_contract_end');			
					$domain_contract_end  		  = strtotime($domain_contract_end);
					$domain_number_year_register  = $this->input->post('domain_number_year_register');
					if($domain_contract_end !== NULL) { 
						$data = array('domain_contract_end' => $domain_contract_end, 'domain_number_year_register' => $domain_number_year_register) ;
					}
					break;

				default:
					# code...
					break;
			}

			$model 		 = $this->contract_m->get_contract_model($term);
			if(!empty($model))
			{
				$this->load->model($model, 'model_temp');
				$clone_id 	= $this->model_temp->renew($edit_id, $data);
				redirect(module_url("edit/{$clone_id}"),'refresh');
			}
		}

		redirect(module_url("edit/{$edit_id}"),'refresh');
	}

	
	/*
	 * Tạm tách để phân flow xử lý cập nhật thông tin chi tiết dịch vụ
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	private function addition_service_submit($term_id = 0)
	{
		/* Kiểm tra quyền thực hiện tác vụ */
		if( ! $this->contract_m->has_permission($term_id,'admin.contract.edit'))
		{
			$this->messages->error('Không có quyền thực hiện tác vụ này.');
			redirect(module_url("edit/{$term_id}"), 'refresh');
		}

		/* Kiểm tra hợp đồng có tồn tại hay không */
		$contract = $this->contract_m->select('term_id,term_name,term_type,term_status')->set_term_type()->get($term_id);
		if( ! $contract) 
		{
			$this->messages->error('Hợp đồng không tồn tại hoặc đã bị xóa');
			redirect(module_url("edit/{$term_id}"), 'refresh');
		}
		
		$post 		= $this->input->post(NULL, TRUE);
		$metadata 	= $this->input->post('meta', TRUE);
		if(empty($metadata))
		{
			$this->messages->error('Không tìm thấy dữ liệu cần thay đổi.');
			redirect(module_url("edit/{$term_id}"), 'refresh');
		}

		/* PARSE & UPDATE METADATAS DATA CHANGED */
		switch ($contract->term_type)
		{
			/* Hợp đồng khóa học GURU */
			case 'courseads': 

				$this->load->model('courseads/courseads_m');

				$course_ids = $metadata['courses']['post_id'];
				if(empty($course_ids)) break;

				/* Tiến hành lưu trữ chi tiết dữ liệu khóa học của hợp đồng */
				$this->load->model('courseads/course_m');

				$courses = $this->course_m->set_post_type()->as_array()
				->select('post_id,post_title,post_status,start_date,end_date')
				->get_many_by('post_id', $course_ids);
				if( ! $courses) break;

				/* Cập nhật tên hợp đồng tạp thời theo tên khóa học */
				$term_name = implode('-', array_map(function($x){ 
					return strtoupper(create_slug($x['post_title']));
				}, $courses));

				$this->courseads_m->update($post['edit']['term_id'], ['term_name' => $term_name]);

				$courses = array_column(array_map(function($x){ 
						$x['course_value'] = (int) get_post_meta_value($x['post_id'], 'course_value'); 
						return $x; 
				}, $courses), NULL, 'post_id');


				foreach($post['meta']['courses']['post_id'] as $key => $_post_id)
				{
					/* Set default value of course post data */
					$courses[$_post_id]['quantity'] = $post['meta']['courses']['quantity'][$key] ?? 1;
					$courses[$_post_id]['course_value'] = $post['meta']['courses']['course_value'][$key] ?? 0;
				}

				$this->load->model('term_posts_m');
				$this->term_posts_m->set_term_posts($post['edit']['term_id'], array_column($courses, 'post_id'), $this->course_m->post_type, FALSE);
				update_term_meta($post['edit']['term_id'], 'courses_items', serialize($courses));

				update_term_meta($post['edit']['term_id'], 'discount_amount', (double) $post['meta']['discount_amount']);

				$contract = new courseads_m();
				$contract->set_contract($term_id);

				update_term_meta($post['edit']['term_id'], 'contract_value', $contract->calc_contract_value());

				// dd($contract->calc_contract_value(), get_term_meta_value($post['edit']['term_id'], 'contract_value'));

				$contract->delete_all_invoices(); //  Delete all invoices available
				$contract->create_invoices(); // Create new contract's invoices
				$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

				$contract->sync_all_amount(); // Update all invoices's amount & receipt's amount
				$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';

				/* Đồng bộ thời gian hợp đôgnf */
				// $contract->get_behaviour_m()->remap_contract_dates();
				/* Cập nhật số lượng học viên */
				$contract->get_behaviour_m()->count_customers();

				redirect(module_url("edit/{$term_id}"), 'refresh');

				break;

			default: 
				
				// Cập nhật lại thời gian kết thúc hợp đồng khi thay đổi số tháng hosting
				if(isset($metadata['hosting_months'])) 
				{
					$_contract_begin = (int) get_term_meta_value($term_id, 'contract_begin');
					$metadata['contract_end'] = strtotime("+{$metadata['hosting_months']} months", $_contract_begin);
				}

				// Cập nhật phí khởi tạo và duy trì năm đầu tiên dịch vụ domain
				if(isset($metadata['domain_fee_initialization_maintain'])) 
				{
					$metadata['domain_fee_initialization_maintain']  = $metadata['domain_fee_initialization'] + $metadata['domain_fee_maintain'];
				}

				// Cập nhật thời gian kết thúc hợp đồng dịch vụ DOMAIN
				if(isset($metadata['domain_number_year_register'])) 
				{
					$domain_number_year_register = $metadata['domain_number_year_register'] ;				
					$metadata['contract_end'] = strtotime("+ $domain_number_year_register year", get_term_meta_value($term_id, 'contract_begin')) ;
				}
				
				if(!empty($metadata['addition_hosting']['start_time']))
				{
					$metadata['addition_hosting']['start_time'] = strtotime($metadata['addition_hosting']['start_time']);
				}

				if(!empty($metadata['addition_domain']['start_time']))
				{
					$metadata['addition_domain']['start_time'] = strtotime($metadata['addition_domain']['start_time']);
				}

				$package_new = array();
				$value_new = array();
				if(!empty($post['edit']["meta"]['package']))
				{
					$metadata['service_package'] = $post['edit']["meta"]['package'];
				}
				if(!empty($post['edit']["meta"]['package_new']['name'])){
					$package_new = $post['edit']["meta"]['package_new']['name'];
					$value_new = $post['edit']["meta"]['value_new']['price'];
				}
				
				$packages = array();
				if(!empty($package_new) && !empty($value_new) ){
					$contract_value = $metadata['webdesign_price'];
					if(count($package_new) > 0 && count($value_new)>0) {
						for ($i=0; $i < count($package_new) ; $i++) {
							if(empty($package_new[$i]) || empty($value_new[$i])) continue;
							$packages['package_new'][$package_new[$i]] = $value_new[$i];
							//$contract_value += $value_new[$i];
						}
					}
				}

				$metadata['packages_new'] = $packages;

				foreach ($metadata as $key => $value) 
				{
					$value = is_array($value) ? serialize($value) : $value;
					update_term_meta($term_id, $key, $value);
				}

				break;
		}

		/* Lấy model mapp */
		$model = $this->contract_m->get_contract_model($contract);
		if(empty($model))
		{
			$this->load->model($model, 'tmp_contract_m');
			$this->messages->info('Không tìm thấy Module dịch vụ');
			redirect(module_url("edit/{$term_id}"), 'refresh');
		}

		$this->load->model($model, 'tmp_contract_m');
		if( ! method_exists($this->tmp_contract_m, 'calc_contract_value'))
		{
			$this->messages->info('Không tìm thấy Module dịch vụ');
			redirect(module_url("edit/{$term_id}"), 'refresh');
		}

		/* Re-calculate contract value */
		$contract_value = $this->tmp_contract_m->calc_contract_value($term_id);
		update_term_meta($term_id, 'contract_value', $contract_value);

		$this->messages->success('Chi tiết hợp đồng đã được cập nhật thành công.');

		redirect(module_url("edit/{$term_id}"), 'refresh');
	}

	private function hooks_to_edit(){

		$this->hook->add_action('fetch_contract_source', function(){
			$contract_status = $this->config->item('contract_status');
			$role_id = $this->admin_m->role_id;

			unset($contract_status['publish']);

			if(in_array($role_id, $this->config->item('salesexecutive','groups'))){
				unset($contract_status['pending']);
				// unset($contract_status['publish']);
				unset($contract_status['ending']);
				unset($contract_status['liquidation']);
			}

			if(!empty($contract_status))
				$contract_status = str_replace('"','\'',json_encode(array_map(function($k,$v){return array('value'=>$k,'text'=>$v);},
				array_keys($contract_status) , $contract_status)));
			
			return $contract_status;
		});
	}

	protected function get_customer_by_role($as_array = FALSE){

		$user_type = 'system';
		$role_id = $this->admin_m->role_id;
		$allow_roles = $this->config->item('read_customers_role_id');
		$this->data['is_system'] = TRUE;

		if(in_array($role_id, $allow_roles)){
			$user_type = 'customer_';
			$this->data['is_system'] = FALSE;
		}

		$users = $this->customer_m->set_get_customer($user_type)->order_by('display_name')->get_many_by();

		if($as_array){

			$array_key_values = array();

			if(!empty($users))
				foreach ($users as $user)
					$array_key_values[$user->user_id] = $user->display_name;

			return $array_key_values;
		}

		return $users;
	}

	/**
	 * CRUD CATEGORIES
	 */
	public function update_categories()
	{
		restrict('contract.category.access');

		$data = $this->data;
		parent::render();
	}
    
    /**
     * sync_customer
     *
     * @return void
     */
    public function sync_customer($term_id)
    {
        $is_lock_editable 	= (bool) get_term_meta_value($term_id, 'lock_editable');
        if($is_lock_editable)
        {
            $this->messages->error('Hợp đồng đang bị khoá, không thể thực hiện tác vụ. Vui lòng mở khoá và thao tác lại.');
            return redirect(admin_url("contract/edit/{$term_id}"),'refresh');
        }

        $has_permission = has_permission('customer.overwrite.update')
                          && has_permission('customer.overwrite.manage');
        if(! $has_permission)
        {
            $this->messages->error('Không có quyền thực hiện tác vụ.');
            return redirect(admin_url("contract/edit/{$term_id}"),'refresh');
        }

        $customer = $this->term_users_m->get_the_users($term_id, [ 'customer_person', 'customer_company' ]);
        if(empty($customer))
        {
            $this->messages->error('Không tìm thấy thông tin khách hàng. Vui lòng liên hệ bộ phận Công nghệ để được hỗ trợ.');
            return redirect(admin_url("contract/edit/{$term_id}"),'refresh');
        }

        $customer = reset($customer);
        $customer = (array) $customer;
        
        $representative_name = $customer['display_name'];
        if('customer_company' == $customer['user_type'])
        {
            $representative_name = get_user_meta_value($customer['user_id'], 'customer_name');
        }

        $representative = [
            'representative_name' => $representative_name,
            'representative_email' => get_user_meta_value($customer['user_id'], 'customer_email'),
            'representative_phone' => get_user_meta_value($customer['user_id'], 'customer_phone'),
            'representative_address' => get_user_meta_value($customer['user_id'], 'customer_address'),
            'representative_gender' => get_user_meta_value($customer['user_id'], 'customer_gender'),
            'representative_zone' => get_user_meta_value($this->admin_m->id, 'user_zone'),
            'representative_position' => '',
            'representative_loa' => ''
        ];
        foreach($representative as $meta_key => $meta_value)
        {
            update_term_meta($term_id, $meta_key, $meta_value);
        }

        $this->messages->success('Đồng bộ thông tin khách hàng thành công');
        return redirect(admin_url("contract/edit/{$term_id}"),'refresh');
    }
}
/* End of file Contract.php */
/* Location: ./application/modules/contract/controllers/Contract.php */