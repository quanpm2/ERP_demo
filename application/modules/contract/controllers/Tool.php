<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Tool extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
		
        $this->load->model('contract_m');

        in_array($this->admin_m->role_id, [1, 26]) OR redirect(base_url('admin'));

		$this->config->load('contract/contract');
	}

    /**
     * Export dữ liệu Hợp Đồng & Khách Hàng không tái ký trong thời gian 10 ngày
     * kể từ ngày hiện tại
     * 1. Hợp đồng có thời gian kết thúc cho đến hiện tại > 10 ngày
     * 2. Hợp đồng không phát sinh hợp đồng tái ký 
     * Where: . Script
     */
    public function export10DNotRenewal()
    {
        $this->load->model('googleads/googleads_m');
        $contracts = $this->googleads_m
        ->select('term.term_id, term.term_name, term.term_parent, term_status, term_type')
        ->set_term_type()
        ->where('term.term_status', 'ending')
        ->m_find([ 'key' => 'end_service_time', 'compare' => 'BETWEEN', 'value' => [strtotime('-30 days'), strtotime('-10 days')] ])
        ->get_all();

        $this->load->model('customer/website_m');

        $relatedContracts = $this->googleads_m
        ->select('term.term_id, term.term_name, term.term_parent, term_status')
        ->set_term_type()
        ->where_not_in('term.term_id', array_column($contracts, 'term_id'))
        ->where_in('term.term_parent', array_column($contracts, 'term_parent'))
        ->get_all();

        $relatedContracts AND $relatedContracts = array_group_by($relatedContracts, 'term_parent');

        $contracts = array_filter($contracts, function($contract) use($relatedContracts){

            $_relatedContracts = $relatedContracts[$contract->term_parent] ?? null;
            if( ! $_relatedContracts) return true;

            $_verifiedContracts = array_map(function($y){
                $y->verified_on = (int) get_term_meta_value($y->term_id, 'verified_on');
                return $y;
            }, $_relatedContracts);

            if($contract->end_service_time > max($_verifiedContracts)) return true;

            return false;
        });

        if( ! $contracts) die('204 No Content');

        $title  = 'Export dữ liệu Hợp Đồng & Khách Hàng không tái ký trong thời gian 10 ngày kể từ ngày hiện tại';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('WEBDOCTOR.VN')->setLastModifiedBy('WEBDOCTOR.VN')->setTitle(uniqid("{$title} "));

        $sheet          = $spreadsheet->getActiveSheet();

        $callback       = $this->googleads_m->get_field_callback();
        $field_config   = $this->googleads_m->get_field_config();

        $columns    = array(
            'id',                   // ID Hợp đồng 
            'website',              // Website thực hiện 
            'status',               // Trạng thái hợp đồng 
            'contract_code',        // Mã Hợp đồng 
            'representative_name',  // Tên người đại diện ký hợp đồng 
            'representative_email', // Email người đại diện ký hợp đồngs 
            'representative_phone', // Số điện thoại người đại diện ký hợp đồngs 
            'contract_note',        // Ghi chú hợp đồng 
            'created_on',           // Ngày tạo 
            'created_by',           // Người tạo 
            'verified_on',          // Xác nhận cấp số vào lúc 
            'contract_begin',       // Thời gian bắt đầu hợp đồng 
            'contract_end',         // Thời gian kết thúc hợp đồng 
            'contract_daterange',   // Thời gian hợp đồng 
            'end_service_time',     // Thời gian kết thúc dịch vụ 
            'start_service_time',   // Thời gian kích hoạt dịch vụ 
            'staff_business',       // Nhân viên kinh doanh phụ trách 
            'staff_advertise',      // Nhân viên kỹ thuật phụ trách 
            'fb_staff_advertise',   // Nhân viên kỹ thuật Facebook Ads 
            'type',                 // Loại Hình 
            'contract_value',       // Giá trị hợp đồng 
            'contract_budget',      // Ngân sách quảng cáo 
            'service_fee',          // Phí dịch vụ 
            'vat',                  // Thuế VAT
            'is_first_contract',    // Ký mới | Tái ký 
            'cid'     // Customer Id
        );

        /* Set Heading Cells at A1 index */
        $sheet->fromArray(array_map(function($x) use($field_config){ return $field_config['columns'][$x]['title']??$x; }, $columns), NULL, 'A1');

        $row_index = 2;
        $contracts = array_values($contracts);

        foreach ($contracts as $key => &$contract)
        {
            $contract = (array) $contract;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;
                if(empty($callback[$column])) continue;

                $contract = call_user_func_array($callback[$column]['func'], array($contract, $column, []));
            }

            $i = 0;
            $row_number = $row_index + $key;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;

                $value = $contract["{$column}_raw"] ?? ($contract["{$column}"] ?? '');
                $i++;
                $col_number = $i;

                switch ($field_config['columns'][$column]['type'])
                {
                    case 'string' : 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
                        break;

                    case 'number': 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                $sheet->setCellValueByColumnAndRow($col_number, $row_number, $value);
            }
        }

        $folder_upload  = 'files/contracts/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }

    /**
     * Export dữ liệu Hợp Đồng & Khách Hàng không tái ký trong thời gian 1 ngày
     * kể từ ngày hiện tại
     * 1. Hợp đồng có thời gian kết thúc cho đến hiện tại > 1 ngày
     * 2. Hợp đồng không phát sinh hợp đồng tái ký 
     * Where: . Script
     */
    public function export1DNotRenewal()
    {
        $this->load->model('googleads/googleads_m');
        $contracts = $this->googleads_m
        ->select('term.term_id, term.term_name, term.term_parent, term_status, term_type')
        ->set_term_type()
        ->where('term.term_status', 'ending')
        ->m_find([ 'key' => 'end_service_time', 'compare' => 'BETWEEN', 'value' => [strtotime('-30 days'), strtotime('-1 days')] ])
        ->get_all();

        $this->load->model('customer/website_m');

        $relatedContracts = $this->googleads_m
        ->select('term.term_id, term.term_name, term.term_parent, term_status')
        ->set_term_type()
        ->where_not_in('term.term_id', array_column($contracts, 'term_id'))
        ->where_in('term.term_parent', array_column($contracts, 'term_parent'))
        ->get_all();

        $relatedContracts AND $relatedContracts = array_group_by($relatedContracts, 'term_parent');

        $contracts = array_filter($contracts, function($contract) use($relatedContracts){

            $_relatedContracts = $relatedContracts[$contract->term_parent] ?? null;
            if( ! $_relatedContracts) return true;

            $_verifiedContracts = array_map(function($y){
                $y->verified_on = (int) get_term_meta_value($y->term_id, 'verified_on');
                return $y;
            }, $_relatedContracts);

            if($contract->end_service_time > max($_verifiedContracts)) return true;

            return false;
        });

        if( ! $contracts) die('204 No Content');

        $title  = 'Export dữ liệu Hợp Đồng & Khách Hàng không tái ký trong thời gian 1 ngày kể từ ngày hiện tại';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('WEBDOCTOR.VN')->setLastModifiedBy('WEBDOCTOR.VN')->setTitle(uniqid("{$title} "));

        $sheet          = $spreadsheet->getActiveSheet();

        $callback       = $this->contract_m->get_field_callback();
        $field_config   = $this->contract_m->get_field_config();

        $columns    = array(
            'id',                   // ID Hợp đồng 
            'website',              // Website thực hiện 
            'status',               // Trạng thái hợp đồng 
            'contract_code',        // Mã Hợp đồng 
            'representative_name',  // Tên người đại diện ký hợp đồng 
            'representative_email', // Email người đại diện ký hợp đồngs 
            'representative_phone', // Số điện thoại người đại diện ký hợp đồngs 
            'contract_note',        // Ghi chú hợp đồng 
            'created_on',           // Ngày tạo 
            'created_by',           // Người tạo 
            'verified_on',          // Xác nhận cấp số vào lúc 
            'contract_begin',       // Thời gian bắt đầu hợp đồng 
            'contract_end',         // Thời gian kết thúc hợp đồng 
            'contract_daterange',   // Thời gian hợp đồng 
            'end_service_time',     // Thời gian kết thúc dịch vụ 
            'start_service_time',   // Thời gian kích hoạt dịch vụ 
            'staff_business',       // Nhân viên kinh doanh phụ trách 
            'staff_advertise',      // Nhân viên kỹ thuật phụ trách 
            'fb_staff_advertise',   // Nhân viên kỹ thuật Facebook Ads 
            'type',                 // Loại Hình 
            'contract_value',       // Giá trị hợp đồng 
            'contract_budget',      // Ngân sách quảng cáo 
            'service_fee',          // Phí dịch vụ 
            'vat',                  // Thuế VAT
            'is_first_contract'     // Ký mới | Tái ký 
        );

        /* Set Heading Cells at A1 index */
        $sheet->fromArray(array_map(function($x) use($field_config){ return $field_config['columns'][$x]['title']??$x; }, $columns), NULL, 'A1');

        $row_index = 2;
        $contracts = array_values($contracts);

        foreach ($contracts as $key => &$contract)
        {
            $contract = (array) $contract;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;
                if(empty($callback[$column])) continue;

                $contract = call_user_func_array($callback[$column]['func'], array($contract, $column, []));
            }

            $i = 0;
            $row_number = $row_index + $key;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;

                $value = $contract["{$column}_raw"] ?? ($contract["{$column}"] ?? '');
                $i++;
                $col_number = $i;

                switch ($field_config['columns'][$column]['type'])
                {
                    case 'string' : 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
                        break;

                    case 'number': 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                $sheet->setCellValueByColumnAndRow($col_number, $row_number, $value);
            }
        }

        $folder_upload  = 'files/contracts/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }

    /**
     * Export dữ liệu các hợp đồng có đã có số lần ký >= 2 cho đến hiện tại vẫn đang thực hiện dịch vụ
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function exportRenewal()
    {
        $this->load->model('googleads/googleads_m');
        $contracts = $this->googleads_m
        ->select('term.term_id, term.term_name, term.term_parent, term_status, term_type')
        ->set_term_type()
        ->where_in('term.term_status', [ 'pending' /* Ngưng hoạt động */ ,'publish' /* Thực hiện */])
        ->m_find([ 'key' => 'started_service', 'compare' => 'BETWEEN', 'value' => [
            start_of_day(strtotime($this->input->get('startAt'))), /* Biên thấp nhất */
            end_of_day(strtotime($this->input->get('endAt'))), /* Biên cao nhất */
        ]])->get_all();
        
        $contracts = array_filter(array_map(function($x){
            /* Re-map property "Is-First-Contract" */
            $x->is_first_contract = (int) get_term_meta_value($x->term_id, 'is_first_contract');
            return $x;
        }, $contracts), function($x) {
            /* Filter Only Contract REnewall */
            return empty($x->is_first_contract);
        });

        if( ! $contracts) die('204 No Content');

        $this->load->model('customer/website_m');

        $relatedContracts = $this->googleads_m
        ->select('term.term_id, term.term_name, term.term_parent, term_status, term_type')
        ->set_term_type()
        ->where_in('term.term_parent', array_column($contracts, 'term_parent'))
        ->where_in('term.term_status', [
            'pending' /* Ngưng hoạt động */,
            'publish' /* Thực hiện */,
            'ending' /* Đã kết thúc */,
            'liquidation' /* Thanh lý */
        ])
        ->get_all();
        

        $relatedContracts AND $relatedContracts = array_group_by($relatedContracts, 'term_parent');

        $latestContracts = array();
        foreach ($relatedContracts as $term_parent => $_contracts)
        {
            $_maxTermId = max(array_column($_contracts, 'term_id'));
            $_contracts = array_column($_contracts, NULL, 'term_id');

            $_numOfRenewal = count($_contracts);
            if($_numOfRenewal < 2) continue;

            $_contracts[$_maxTermId]->numOfRenewal = count($_contracts);
            array_push($latestContracts, $_contracts[$_maxTermId]);
        }

        if( ! $latestContracts) die('204 No Content');

        $title  = 'Export dữ liệu Hợp Đồng & Khách Hàng có số lần tái ký >= 2 & đang thực hiện hợp đồng cho đến hiện tại';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('WEBDOCTOR.VN')->setLastModifiedBy('WEBDOCTOR.VN')->setTitle(uniqid("{$title} "));

        $sheet          = $spreadsheet->getActiveSheet();

        $callback       = $this->contract_m->get_field_callback();
        $field_config   = $this->contract_m->get_field_config();

        /* Customr Fields Config */
        $field_config['columns']['numOfRenewal'] = [
            'name' => 'numOfRenewal',
            'label' => 'Số lần tái ký',
            'title' => 'Số lần tái ký',
            'type' => 'string',
        ];

        $columns    = array(
            'id',                   // ID Hợp đồng 
            'website',              // Website thực hiện 
            'status',               // Trạng thái hợp đồng 
            'contract_code',        // Mã Hợp đồng 
            'representative_name',  // Tên người đại diện ký hợp đồng 
            'representative_email', // Email người đại diện ký hợp đồngs 
            'representative_phone', // Số điện thoại người đại diện ký hợp đồngs 
            'contract_note',        // Ghi chú hợp đồng 
            'created_on',           // Ngày tạo 
            'created_by',           // Người tạo 
            'verified_on',          // Xác nhận cấp số vào lúc 
            'contract_begin',       // Thời gian bắt đầu hợp đồng 
            'contract_end',         // Thời gian kết thúc hợp đồng 
            'contract_daterange',   // Thời gian hợp đồng 
            'end_service_time',     // Thời gian kết thúc dịch vụ 
            'start_service_time',   // Thời gian kích hoạt dịch vụ 
            'staff_business',       // Nhân viên kinh doanh phụ trách 
            'staff_advertise',      // Nhân viên kỹ thuật phụ trách 
            'fb_staff_advertise',   // Nhân viên kỹ thuật Facebook Ads 
            'type',                 // Loại Hình 
            'contract_value',       // Giá trị hợp đồng 
            'contract_budget',      // Ngân sách quảng cáo 
            'service_fee',          // Phí dịch vụ 
            'vat',                  // Thuế VAT
            'is_first_contract',    // Ký mới | Tái ký 
            'numOfRenewal'          // Số lần tái ký
        );

        /* Set Heading Cells at A1 index */
        $sheet->fromArray(array_map(function($x) use($field_config){ return $field_config['columns'][$x]['title']??$x; }, $columns), NULL, 'A1');

        $rowStartIndex = 2;
        foreach ($latestContracts as $key => &$_contract)
        {
            $_contract = (array) $_contract;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;
                if(empty($callback[$column])) continue;

                $_contract = call_user_func_array($callback[$column]['func'], array($_contract, $column, []));
            }

            $colIndex = 1;
            $rowIndex = $rowStartIndex + $key;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;

                $value = $_contract["{$column}_raw"] ?? ($_contract["{$column}"] ?? '');

                switch ($field_config['columns'][$column]['type'])
                {
                    case 'string' : 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
                        break;

                    case 'number': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $value);

                $colIndex++;
            }
        }

        $folder_upload  = 'files/contracts/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $this->load->helper('url');
        $this->load->helper('text');
        $title = url_title(convert_accented_characters($title));
        $fileName = "{$folder_upload}{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }

    public function contracts()
    {
        $models = array(
            'term_users_m',
            'contract/category_m',
            'contract/term_categories_m',
            'contract/receipt_m',
            'contract/contract_m',
        );

        $this->load->model($models);
        $this->load->library('datatable_builder');

        $response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

        if( ! has_permission('admin.contract.view'))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }
        
        $this->config->load('contract/fields');
        $default = array(
            'offset' => 0,
            'per_page' => 50,
            'cur_page' => 1,
            'is_filtering' => TRUE,
            'is_ordering' => TRUE,
            'columns' => implode(',', $this->config->item('default_columns', 'datasource'))
        );
        $args       = wp_parse_args( $this->input->get(), $default);
        $args['columns'] = explode(',', $args['columns']);

        $data = $this->data; // remove-after

        $relate_users   = $this->admin_m->get_all_by_permissions('admin.contract.view');

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        if($relate_users === FALSE)
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        if(is_array($relate_users))
        {
            $this->datatable_builder->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
        }

        /* Applies get query params for filter */
        $this->search_filter();

        $this->datatable_builder->set_filter_position(FILTER_TOP_OUTTER)
        ->select('term.term_id,term.term_name, term.term_type, term.term_status')
        ->add_search('contract_code',['placeholder'=>'Số hợp đồng'])
        ->add_search('term.term_name',['placeholder'=>'Website'])
        ->add_search('created_on',['placeholder'=>'Ngày tạo','class'=>'form-control input_daterange'])
        ->add_search('verified_on',['placeholder'=>'Ngày Cấp số','class'=>'form-control input_daterange'])
        ->add_search('contract_begin',['placeholder'=>'Ngày bắt đầu HĐ','class'=>'form-control input_daterange'])
        ->add_search('contract_end',['placeholder'=>'Ngày kết thúc HĐ','class'=>'form-control input_daterange'])
        ->add_search('start_service_time',['placeholder'=>'T/g Thực hiện','class'=>'form-control input_daterange'])     

        ->add_search('staff_business',['placeholder'=>'Kinh doanh phụ trách'])
        ->add_search('created_by',['placeholder'=>'Người tạo'])

        ->add_search('term_type',array('content'=> form_dropdown(array('name'=>'where[term_type]','class'=>'form-control select2'),prepare_dropdown($this->config->item('services'),'Tất cả dịch vụ'),$this->input->get('where[term_type]'))))

        ->add_search('is_first_contract',array('content'=> form_dropdown(array('name'=>'where[is_first_contract]','class'=>'form-control select2'),[ ''=>'Tất cả',1=>'Ký mới'],$this->input->get('where[is_first_contract]'))))

        ->add_search('term_status',array('content'=> form_dropdown(array('name'=>'where[term_status]','class'=>'form-control select2'),prepare_dropdown($this->config->item('contract_status'),'Trạng thái HĐ : Tất cả'), $this->input->get('where[term_status]'))));

        $columns = $this->config->item('columns', 'datasource');
        foreach ($args['columns'] as $key)
        {
            if(empty($columns[$key])) continue;

            $this->datatable_builder->add_column($key, $columns[$key]);
        }

        $this->datatable_builder->add_column('action', array('set_select'=>FALSE,'title'=>'Actions','set_order'=>FALSE));

        foreach ($this->contract_m->get_field_callback() as $callback)
        {
            $this->datatable_builder->add_column_callback($callback['field'], $callback['func'], $callback['row_data']);
        }

        $this->datatable_builder->add_column_callback('action', function($data, $row_data){

            $term_id = $data['term_id'];

            if(has_permission('admin.contract.edit'))
            {
                $data['action'] = anchor(module_url("create_wizard/index/{$term_id}"),'<i class="fa fa-fw fa-edit"></i>','title="Cập nhật hợp đồng" class="btn btn-default btn-xs"');
            }

            if(has_permission('admin.contract.add'))
            {
                $is_type_adsplus = in_array($data['term_type'], array('google-ads'));
                $is_contract_active = in_array($data['term_status'], array('liquidation','publish','pending','ending'));
                if($is_type_adsplus && $is_contract_active)
                {
                    $data['action'].= anchor(module_url("clone/{$data['term_id']}"),'<i class="fa fa-fw fa-copy"></i>','title="Tạo bản sao chép" class="btn btn-default btn-xs" data-toggle="confirmation"');
                }
            }

            return $data;

        },  FALSE);

        $status_list = array_keys($this->config->item('contract_status'));
        array_unshift($status_list, 1);

        $this->datatable_builder
        ->where_in('term_status', ['pending', 'publish', 'ending', 'liquidation'])
        ->where_in('term_type', ['webdoctor', 'seo-traffic', 'seo-top', 'webgeneral', 'webdesign', 'hosting', 'domain', 'weboptimize', 'banner', 'webcontent'])
        ->from('term')
        ->group_by('term.term_id');

        $pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];
        
        $data = $this->datatable_builder->generate($pagination_config);

        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->exportContracts($last_query);
            return TRUE;
        }

        $this->template->title->set('Quản lý hợp đồng');
        $this->template->description->set('Trang danh sách tất cả các hợp đồng');

        return parent::renderJson($data);
    }

    /**
     * Export Xlsx file from Query
     *
     * @param      string  $query  The query
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function exportContracts($query = '')
    {
        if(empty($query)) return FALSE;

        // remove limit in query string
        $query = explode('LIMIT', $query);
        $query = reset($query);
        
        $terms = $this->contract_m->query($query)->result();
        if( ! $terms) return FALSE;

        $this->load->model('customer/customer_m');

        /* Load All Customers Related To The Contracts Filtered */ 
        $customers = array_column($this->customer_m->set_user_type()
        ->select('user.*, term_users.term_id')
        ->join('term_users', 'term_users.user_id = user.user_id')
        ->where_in('term_users.term_id', array_column($terms, 'term_id'))
        ->get_all(), NULL, 'term_id');

        $this->config->load('contract/fields');
        $default    = array('columns' => implode(',', $this->config->item('default_columns', 'datasource')));
        $args       = wp_parse_args( $this->input->get(), $default);
        $args['columns'] = explode(',', $args['columns']);

        $this->load->library('excel');
        $cacheMethod    = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings  = array( 'memoryCacheSize' => '1024MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("WEBDOCTOR.VN")->setLastModifiedBy("WEBDOCTOR.VN")->setTitle(uniqid('Danh sách hợp đồng __'));
        $objPHPExcel->setActiveSheetIndex(0);

        $objWorksheet = $objPHPExcel->getActiveSheet();

        $callback       = $this->contract_m->get_field_callback();
        $field_config   = $this->contract_m->get_field_config();

        $headings = array_map(function($x) use($field_config){ return $field_config['columns'][$x]['title']??$x;}, $args['columns']);
        array_push($headings, 'Tên Doanh Nghiệp|Cá Nhân', 'E-Mail', 'Phone', 'Địa chỉ');
        $objWorksheet->fromArray($headings, NULL, 'A1');

        $row_index = 2;
        foreach ($terms as $key => &$term)
        {   
            $term = (array) $term;
            foreach ($args['columns'] as $column)
            {
                if(empty($field_config['columns'][$column])) continue;
                if(empty($callback[$column])) continue;

                $term = call_user_func_array($callback[$column]['func'], array($term, $column, []));
            }

            $i = 0;
            $row_number = $row_index + $key;

            foreach ($args['columns'] as $column)
            {
                if(empty($field_config['columns'][$column])) continue;

                $value = $term["{$column}_raw"] ?? ($term["{$column}"] ?? '');
                $col_number = $i;
                $i++;

                switch ($field_config['columns'][$column]['type'])
                {
                    case 'string' : 
                        $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
                        break;

                    case 'number': 
                        $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = PHPExcel_Shared_Date::PHPToExcel($value);
                        $objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                $objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $value);

                $objWorksheet->setCellValueByColumnAndRow(++$col_number, $row_number, $customers[$term['term_id']]->display_name);
                $objWorksheet->setCellValueByColumnAndRow(++$col_number, $row_number, $customers[$term['term_id']]->user_email);
                $objWorksheet->setCellValueByColumnAndRow(++$col_number, $row_number, get_user_meta_value($customers[$term['term_id']]->user_id, 'customer_phone'));
                $objWorksheet->setCellValueByColumnAndRow(++$col_number, $row_number, get_user_meta_value($customers[$term['term_id']]->user_id, 'customer_address'));
            }
        }

        $num_rows = count($terms);
        
        // We'll be outputting an excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $folder_upload = "files/contracts/";
        if(!is_dir($folder_upload))
        {
            try 
            {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $created_datetime   = my_date(time(),'Y-m-d-H-i-s');
        $file_name          = "{$folder_upload}danh-sach-hop-dong-{$created_datetime}.xlsx";

        try { $objWriter->save($file_name); }
        catch (Exception $e) { trigger_error($e->getMessage()); return FALSE;  }

        $this->load->helper('download');
        force_download($file_name, NULL);
        return TRUE;
    }


    /**
     * { function_description }
     *
     * @param      integer  $day    The day
     * @param      string   $type   The type
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function exportAdsPending($day = 3)
    {
        $day = abs((int) $day);
        $this->load->model('googleads/googleads_m');
        $contracts = $this->googleads_m
        ->select('term.term_id, term.term_name, term.term_parent, term_status, term_type')
        ->set_term_type()
        ->where('term.term_status', 'pending')
        ->m_find([ 'key' => 'result_updated_on', 'compare' => '<=', 'value' => end_of_day(strtotime("-{$day} days")) ])
        ->get_all();
        $this->load->model('customer/website_m');

        if( ! $contracts) die('204 No Content');

        $title  = "Export dữ liệu Hợp Đồng & Khách Hàng tạm ngưng trong thời gian {$day} ngày kể từ ngày hiện tại";

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('WEBDOCTOR.VN')->setLastModifiedBy('WEBDOCTOR.VN')->setTitle(uniqid("{$title} "));

        $sheet          = $spreadsheet->getActiveSheet();

        $callback       = $this->googleads_m->get_field_callback();
        $field_config   = $this->googleads_m->get_field_config();

        $columns    = array(
            'id',                   // ID Hợp đồng 
            'website',              // Website thực hiện 
            'status',               // Trạng thái hợp đồng 
            'result_updated_on',    // Cập nhật lúc
            'result_pending_days',  // Số ngày ngưng phát sinh chi phí
            'contract_code',        // Mã Hợp đồng 
            'representative_name',  // Tên người đại diện ký hợp đồng 
            'representative_email', // Email người đại diện ký hợp đồngs 
            'representative_phone', // Số điện thoại người đại diện ký hợp đồngs 
            'contract_note',        // Ghi chú hợp đồng 
            'created_on',           // Ngày tạo 
            'created_by',           // Người tạo 
            'verified_on',          // Xác nhận cấp số vào lúc 
            'contract_begin',       // Thời gian bắt đầu hợp đồng 
            'contract_end',         // Thời gian kết thúc hợp đồng 
            'contract_daterange',   // Thời gian hợp đồng 
            'end_service_time',     // Thời gian kết thúc dịch vụ 
            'start_service_time',   // Thời gian kích hoạt dịch vụ 
            'staff_business',       // Nhân viên kinh doanh phụ trách 
            'staff_advertise',      // Nhân viên kỹ thuật phụ trách 
            'fb_staff_advertise',   // Nhân viên kỹ thuật Facebook Ads 
            'type',                 // Loại Hình 
            'contract_value',       // Giá trị hợp đồng 
            'contract_budget',      // Ngân sách quảng cáo 
            'service_fee',          // Phí dịch vụ 
            'vat',                  // Thuế VAT
            'is_first_contract',    // Ký mới | Tái ký 
            'cid'                   // Customer Id
        );

        /* Set Heading Cells at A1 index */
        $sheet->fromArray(array_map(function($x) use($field_config){ return $field_config['columns'][$x]['title']??$x; }, $columns), NULL, 'A1');

        $row_index = 2;
        $contracts = array_values($contracts);

        foreach ($contracts as $key => &$contract)
        {
            $contract = (array) $contract;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;
                if(empty($callback[$column])) continue;

                $contract = call_user_func_array($callback[$column]['func'], array($contract, $column, []));
            }

            $i = 0;
            $row_number = $row_index + $key;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;

                $value = $contract["{$column}_raw"] ?? ($contract["{$column}"] ?? '');
                $i++;
                $col_number = $i;

                switch ($field_config['columns'][$column]['type'])
                {
                    case 'string' : 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
                        break;

                    case 'number': 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                $sheet->setCellValueByColumnAndRow($col_number, $row_number, $value);
            }
        }

        $folder_upload  = 'files/contracts/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file Tool.php */
/* Location: ./application/modules/contract/controllers/Tool.php */