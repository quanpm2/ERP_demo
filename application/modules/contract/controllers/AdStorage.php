<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdStorage extends Admin_Controller {

	public function __construct(){

		$this->autoload['models'][] = 'term_users_m';
		$this->autoload['models'][] = 'customer/customer_m';

		parent::__construct();
	}

	public function done()
	{	
		$this->template->is_box_open->set(1);
		$this->template->title->set('Kho tài khoản');
		$this->template->javascript->add(base_url("dist/vAdStorageAdminIndexPage.js"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		$this->template->publish();
	}

	public function datawarehouse()
	{	
		restrict('admin.adwarehouse.access');
		
		$this->template->is_box_open->set(1);
		$this->template->title->set('Kho tài khoản FB - BM');
        $this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
        $this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->javascript->add(base_url("dist/vAdStorageDatawarehouseAdminIndexPage.js"));
		$this->template->publish();
	}
}
/* End of file AdStorage.php */
/* Location: ./application/modules/contract/controllers/AdStorage.php */