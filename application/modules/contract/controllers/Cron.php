<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends Public_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$models = array(
            'contract/base_contract_m',
			'contract/contract_m',
			'contract/contract_report_m',
            'onead/onead_m',
            'oneweb/oneweb_m',
            'hosting/hosting_m',
            'domain/domain_m',
            'log_m',
        );

		$this->load->model($models);
	}

	public function index() 
	{
        if( ! parent::check_token())
        {
            log_message('error',"#fbamod/cron/index has been illegal access to");
            return FALSE;
        }

		if(ENVIRONMENT != 'production') return FALSE;

		$hour = date('G');

        if($hour == 10) $this->send_mail_report_collectmoney();

        $this->checkQueuePostSlackNewContractMessage();
        $this->checkQueuePostSlackNewCustomerPayment();
	}

    /**
     * check Queue of "Post Slack New Contract Messages FAIL" for RE-TRY
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    private function checkQueuePostSlackNewContractMessage()
    {
        $this->load->model('log_m');        
        $failJobs = $this->log_m->select('log_id,term_id')->order_by('log_id', 'desc')->get_many_by(['log_status' => 0, 'log_type' => 'contract-postSlackNewContractMessage']);
        if(empty($failJobs)) return FALSE;

        $this->load->model('contract/common_report_m');
        foreach ($failJobs as $job)
        {
            try
            {
                $this->common_report_m->init($job->term_id)->postSlackNewContractMessage();
                $this->common_report_m->clear();
                $this->log_m->delete($job->log_id);
            }
            catch (Exception $e) {
                continue;    
            }
        }
    }


    /**
     * check Queue of "Post Slack New Customer Payment Messages FAIL" for RE-TRY
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    private function checkQueuePostSlackNewCustomerPayment()
    {
        $this->load->model('log_m');        
        $failJobs = $this->log_m->select('log_id,term_id')->order_by('log_id', 'desc')->get_many_by(['log_status' => 0, 'log_type' => 'contract-postSlackNewCustomerPayment']);
        if(empty($failJobs)) return FALSE;

        $this->load->model('contract/common_report_m');
        foreach ($failJobs as $job)
        {
            try
            {    
                $this->common_report_m->init($job->term_id);
                $this->common_report_m->setReceipt((int) get_log_meta_value($job->log_id, 'receiptId'));
                $this->common_report_m->postSlackNewCustomerPayment();
                $this->common_report_m->clear();

                $this->log_m->delete($job->log_id);
            }
            catch (Exception $e) {
                continue;    
            }
        }
    }

    /**
     * Sends a mail report collectmoney.
     */
    public function send_mail_report_collectmoney()
    {
        $start_time = $this->mdate->startOfDay(strtotime('yesterday'));
        $end_time   = $this->mdate->endOfDay($start_time);
        $this->contract_report_m->send_mail_report_collectmoney($start_time,$end_time);
    }


	public function send_mail_alert_receipt_deadline()
	{
		$terms = $this->contract_m
        ->set_term_type()
        ->select('term.term_id')
        ->select('posts.end_date')
        ->select('SUM(postmeta.meta_value)*COUNT(DISTINCT posts.post_id)/COUNT(postmeta.post_id) AS "amount"')
        ->join('term_posts','term.term_id = term_posts.term_id')
        ->join('posts','posts.post_id = term_posts.post_id')
        ->join('postmeta', 'postmeta.post_id = posts.post_id AND postmeta.meta_key = "amount"','LEFT')
        ->where('posts.post_type','receipt_caution')
        ->where('posts.post_status','publish')
        // ->where('posts.end_date <=',$this->mdate->startOfDay())
        ->where_in('term.term_status',['waitingforapprove','pending','publish'])
        ->group_by('term.term_id')
        ->get_many_by();

        if(empty($terms)) return FALSE;

        foreach ($terms as $term)
        {
        	$this->contract_report_m->send_mail_alert_receipt_deadline($term->term_id);
        }
	}

    public function send_mail_notice_renew_onead_service()
    {
        // Onead not send renew
        return TRUE;

        $this->load->config('amqps');
		$amqps_host 	= $this->config->item('host', 'amqps');
		$amqps_port 	= $this->config->item('port', 'amqps');
		$amqps_user 	= $this->config->item('user', 'amqps');
		$amqps_password = $this->config->item('password', 'amqps');
		$queue          = 'mail_auto.event.default';
                
		$connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
		$channel 	= $connection->channel();
		$channel->queue_declare($queue, false, true, false, false);

        $now = time();

        $archor_time = (60 * 60 * 24 * 30); // 30 days

        $oneads = $this->onead_m->set_term_type()
            ->join('termmeta', "term.term_id = termmeta.term_id AND termmeta.meta_key= 'contract_end'", 'LEFT')
            ->where('term.term_status', 'publish')
            ->where("(termmeta.meta_value - {$now}) > 0")
            ->where("(termmeta.meta_value - {$now}) <= {$archor_time}")
            ->as_array()
            ->get_all();

        $onead_ids = array_column($oneads, 'term_id');
        $start_time = strtotime(date('Y-01-01 00:00:00'));
        $end_time = strtotime(date('Y-12-31 23:59:59'));
        $logs = $this->log_m->where('log_type', 'mail_event_onead_notice_renew')
            ->where_in('term_id', $onead_ids)
            ->where("UNIX_TIMESTAMP(log_time_create) >=", $start_time)
            ->where("UNIX_TIMESTAMP(log_time_create) <=", $end_time)
            ->as_array()
            ->get_all();

        // Remove pendding or running or success or finish contract
        $success_log = array_filter($logs, function ($item) {
            return $item['log_status'] != $this->log_m->status_error;
        });
        if (!empty($success_log)) {
            $log_term_ids = array_column($success_log, 'term_id');

            $oneads = array_filter($oneads, function ($onead) use ($log_term_ids) {
                return !in_array($onead['term_id'], $log_term_ids);
            });
        }

        // Retry error mail
        $error_logs = array_filter($logs, function ($item) {
            return $item['log_status'] == $this->log_m->status_error;
        });
        if (!empty($error_logs)) {
            foreach ($error_logs as $log) {
                $excute_time = (int)get_log_meta_value($log['log_id'], 'excute_time');
                if ($excute_time > 3) {
                    $this->log_m->update($log['log_id'], ['log_status' => $this->log_m->status_finish]);
                    continue;
                } else {
                    $this->log_m->update($log['log_id'], ['log_status' => $this->log_m->status_pending]);
                    
                    // dispatch job
                    $payload = [
                        'event' 	=> 'onead.notice.renew',
                        'log_id' => (int) $log['log_id']
                    ];
        
                    $message = new \PhpAmqpLib\Message\AMQPMessage(
                        json_encode($payload),
                        array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                    );
                    $channel->basic_publish($message, '', $queue);	
                }

                $log_term_id = $log['term_id'];
                $oneads = array_filter($oneads, function ($onead) use ($log_term_id) {
                    return $onead['term_id'] != $log_term_id;
                });
            }
        }

        // Remove retry overtimes mail
        if (!empty($oneads)) {
            foreach ($oneads as $onead) {
                $log_id = $this->log_m->insert([
                    'term_id' => $onead['term_id'],
                    'log_type' => 'mail_event_onead_notice_renew',
                    'log_title' => '[RabbitMQ] Gửi mail nhắc gia hạn hợp đồng 1Ad',
                    'log_status' => $this->log_m->status_pending,
                ]);
                if (!empty($log_id)) {
                    update_log_meta($log_id, 'excute_time', 0);

                    // dispatch job
                    $payload = [
                        'event' 	=> 'onead.notice.renew',
                        'log_id' => (int) $log_id
                    ];
        
                    $message = new \PhpAmqpLib\Message\AMQPMessage(
                        json_encode($payload),
                        array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                    );
                    $channel->basic_publish($message, '', $queue);	
                }
            }
        }

        $channel->close();
		$connection->close();
    }

    public function send_mail_notice_renew_hosting_service()
    {
        $this->load->config('amqps');
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        $queue          = 'mail_auto.event.default';

        $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $channel     = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $now = time();

        $archor_time = (60 * 60 * 24 * 30); // 30 days

        $hostings = $this->hosting_m->set_term_type()
            ->join('termmeta', "term.term_id = termmeta.term_id AND termmeta.meta_key= 'contract_end'", 'LEFT')
            ->where('term.term_status', 'publish')
            ->where("(termmeta.meta_value - {$now}) > 0")
            ->where("(termmeta.meta_value - {$now}) <= {$archor_time}")
            ->as_array()
            ->get_all();

        $hosting_ids = array_column($hostings, 'term_id');
        $start_time = strtotime(date('Y-01-01 00:00:00'));
        $end_time = strtotime(date('Y-12-31 23:59:59'));
        $logs = $this->log_m->where('log_type', 'mail_event_hosting_notice_renew')
            ->where_in('term_id', $hosting_ids)
            ->where("UNIX_TIMESTAMP(log_time_create) >=", $start_time)
            ->where("UNIX_TIMESTAMP(log_time_create) <=", $end_time)
            ->as_array()
            ->get_all();

        // Remove pendding or running or success or finish contract
        $success_log = array_filter($logs, function ($item) {
            return $item['log_status'] != $this->log_m->status_error;
        });
        if (!empty($success_log)) {
            $log_term_ids = array_column($success_log, 'term_id');

            $hostings = array_filter($hostings, function ($hosting) use ($log_term_ids) {
                return !in_array($hosting['term_id'], $log_term_ids);
            });
        }

        // Retry error mail
        $error_logs = array_filter($logs, function ($item) {
            return $item['log_status'] == $this->log_m->status_error;
        });
        if (!empty($error_logs)) {
            foreach ($error_logs as $log) {
                $excute_time = (int)get_log_meta_value($log['log_id'], 'excute_time');
                if ($excute_time > 3) {
                    $this->log_m->update($log['log_id'], ['log_status' => $this->log_m->status_finish]);
                    continue;
                } else {
                    $this->log_m->update($log['log_id'], ['log_status' => $this->log_m->status_pending]);

                    // dispatch job
                    $payload = [
                        'event' 	=> 'hosting.notice.renew',
                        'log_id' => (int) $log['log_id']
                    ];
        
                    $message = new \PhpAmqpLib\Message\AMQPMessage(
                        json_encode($payload),
                        array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                    );
                    $channel->basic_publish($message, '', $queue);	
                }

                $log_term_id = $log['term_id'];
                $hostings = array_filter($hostings, function ($hosting) use ($log_term_id) {
                    return $hosting['term_id'] != $log_term_id;
                });
            }
        }

        // Remove retry overtimes mail
        if (!empty($hostings)) {
            foreach ($hostings as $hosting) {
                $log_id = $this->log_m->insert([
                    'term_id' => $hosting['term_id'],
                    'log_type' => 'mail_event_hosting_notice_renew',
                    'log_title' => '[RabbitMQ] Gửi mail nhắc gia hạn hợp đồng Hosting',
                    'log_status' => $this->log_m->status_pending,
                ]);
                if (!empty($log_id)) {
                    update_log_meta($log_id, 'excute_time', 0);

                    // dispatch job
                    $payload = [
                        'event' 	=> 'hosting.notice.renew',
                        'log_id' => (int) $log_id
                    ];
        
                    $message = new \PhpAmqpLib\Message\AMQPMessage(
                        json_encode($payload),
                        array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                    );
                    $channel->basic_publish($message, '', $queue);	
                }
            }
        }

        $channel->close();
        $connection->close();
    }

    public function send_mail_notice_renew_domain_service()
    {
        $this->load->config('amqps');
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        $queue          = 'mail_auto.event.default';

        $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $channel     = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $now = time();

        $archor_time = (60 * 60 * 24 * 30); // 30 days

        $domains = $this->domain_m->set_term_type()
            ->join('termmeta', "term.term_id = termmeta.term_id AND termmeta.meta_key= 'contract_end'", 'LEFT')
            ->where('term.term_status', 'publish')
            ->where("(termmeta.meta_value - {$now}) > 0")
            ->where("(termmeta.meta_value - {$now}) <= {$archor_time}")
            ->as_array()
            ->get_all();

        $domain_ids = array_column($domains, 'term_id');
        $start_time = strtotime(date('Y-01-01 00:00:00'));
        $end_time = strtotime(date('Y-12-31 23:59:59'));
        $logs = $this->log_m->where('log_type', 'mail_event_domain_notice_renew')
            ->where_in('term_id', $domain_ids)
            ->where("UNIX_TIMESTAMP(log_time_create) >=", $start_time)
            ->where("UNIX_TIMESTAMP(log_time_create) <=", $end_time)
            ->as_array()
            ->get_all();

        // Remove pendding or running or success or finish contract
        $success_log = array_filter($logs, function ($item) {
            return $item['log_status'] != $this->log_m->status_error;
        });
        if (!empty($success_log)) {
            $log_term_ids = array_column($success_log, 'term_id');

            $domains = array_filter($domains, function ($domain) use ($log_term_ids) {
                return !in_array($domain['term_id'], $log_term_ids);
            });
        }

        // Retry error mail
        $error_logs = array_filter($logs, function ($item) {
            return $item['log_status'] == $this->log_m->status_error;
        });
        if (!empty($error_logs)) {
            foreach ($error_logs as $log) {
                $excute_time = (int)get_log_meta_value($log['log_id'], 'excute_time');
                if ($excute_time > 3) {
                    $this->log_m->update($log['log_id'], ['log_status' => $this->log_m->status_finish]);
                    continue;
                } else {
                    $this->log_m->update($log['log_id'], ['log_status' => $this->log_m->status_pending]);

                    // dispatch job
                    $payload = [
                        'event' 	=> 'domain.notice.renew',
                        'log_id' => (int) $log['log_id']
                    ];
        
                    $message = new \PhpAmqpLib\Message\AMQPMessage(
                        json_encode($payload),
                        array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                    );
                    $channel->basic_publish($message, '', $queue);	
                }

                $log_term_id = $log['term_id'];
                $domains = array_filter($domains, function ($id) use ($log_term_id) {
                    return $id != $log_term_id;
                });
            }
        }

        // Remove retry overtimes mail
        if (!empty($domains)) {
            foreach ($domains as $domain) {
                $log_id = $this->log_m->insert([
                    'term_id' => $domain['term_id'],
                    'log_type' => 'mail_event_domain_notice_renew',
                    'log_title' => '[RabbitMQ] gửi mail nhắc gia hạn hợp đồng Domain',
                    'log_status' => $this->log_m->status_pending,
                ]);
                if (!empty($log_id)) {
                    update_log_meta($log_id, 'excute_time', 0);

                    // dispatch job
                    $payload = [
                        'event' 	=> 'domain.notice.renew',
                        'log_id' => (int) $log_id
                    ];
        
                    $message = new \PhpAmqpLib\Message\AMQPMessage(
                        json_encode($payload),
                        array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                    );
                    $channel->basic_publish($message, '', $queue);	
                }
            }
        }

        $channel->close();
        $connection->close();
    }

    public function send_mail_notice_renew_oneweb_service()
    {
        $this->load->config('amqps');
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        $queue          = 'mail_auto.event.default';

        $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $channel     = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $now = time();

        $archor_time = (60 * 60 * 24 * 30); // 30 days

        $onewebs = $this->oneweb_m->set_term_type()
            ->join('termmeta', "term.term_id = termmeta.term_id AND termmeta.meta_key= 'contract_end'", 'LEFT')
            ->where('term.term_status', 'publish')
            ->where("(termmeta.meta_value - {$now}) > 0")
            ->where("(termmeta.meta_value - {$now}) <= {$archor_time}")
            ->as_array()
            ->get_all();

        $oneweb_ids = array_column($onewebs, 'term_id');
        $start_time = strtotime(date('Y-01-01 00:00:00'));
        $end_time = strtotime(date('Y-12-31 23:59:59'));
        $logs = $this->log_m->where('log_type', 'mail_event_oneweb_notice_renew')
            ->where_in('term_id', $oneweb_ids)
            ->where("UNIX_TIMESTAMP(log_time_create) >=", $start_time)
            ->where("UNIX_TIMESTAMP(log_time_create) <=", $end_time)
            ->as_array()
            ->get_all();

        // Remove pendding or running or success or finish contract
        $success_log = array_filter($logs, function ($item) {
            return $item['log_status'] != $this->log_m->status_error;
        });
        if (!empty($success_log)) {
            $log_term_ids = array_column($success_log, 'term_id');

            $onewebs = array_filter($onewebs, function ($oneweb) use ($log_term_ids) {
                return !in_array($oneweb['term_id'], $log_term_ids);
            });
        }

        // Retry error mail
        $error_logs = array_filter($logs, function ($item) {
            return $item['log_status'] == $this->log_m->status_error;
        });
        if (!empty($error_logs)) {
            foreach ($error_logs as $log) {
                $excute_time = (int)get_log_meta_value($log['log_id'], 'excute_time');
                if ($excute_time > 3) {
                    $this->log_m->update($log['log_id'], ['log_status' => $this->log_m->status_finish]);
                    continue;
                } else {
                    $this->log_m->update($log['log_id'], ['log_status' => $this->log_m->status_pending]);

                    // dispatch job
                    $payload = [
                        'event' 	=> 'oneweb.notice.renew',
                        'log_id' => (int) $log['log_id']
                    ];
        
                    $message = new \PhpAmqpLib\Message\AMQPMessage(
                        json_encode($payload),
                        array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                    );
                    $channel->basic_publish($message, '', $queue);
                }

                $log_term_id = $log['term_id'];
                $onewebs = array_filter($onewebs, function ($id) use ($log_term_id) {
                    return $id != $log_term_id;
                });
            }
        }

        // Remove retry overtimes mail
        if (!empty($onewebs)) {
            foreach ($onewebs as $oneweb) {
                $log_id = $this->log_m->insert([
                    'term_id' => $oneweb['term_id'],
                    'log_type' => 'mail_event_oneweb_notice_renew',
                    'log_title' => '[RabbitMQ] gửi mail nhắc gia hạn hợp đồng Oneweb',
                    'log_status' => $this->log_m->status_pending,
                ]);
                if (!empty($log_id)) {
                    update_log_meta($log_id, 'excute_time', 0);

                    // dispatch job
                    $payload = [
                        'event' 	=> 'oneweb.notice.renew',
                        'log_id' => (int) $log_id
                    ];
        
                    $message = new \PhpAmqpLib\Message\AMQPMessage(
                        json_encode($payload),
                        array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                    );
                    $channel->basic_publish($message, '', $queue);
                }
            }
        }

        $channel->close();
        $connection->close();
    }
}
/* End of file Crond.php */
/* Location: ./application/modules/facebookads/controllers/Crond.php */