<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Revenue extends Admin_Controller {
	
	function __construct()
	{
		parent::__construct();

		$models = array('contract/receipt_m','term_users_m','contract/base_contract_m','staffs/sale_m');

		$this->load->model($models);
	}

	public function index()
	{
		restrict('contract.revenue.access');
		
		$data = $this->data;
		$this->template->title->set('Dự thu hợp đồng');

		// LOGGED USER NOT HAS PERMISSION TO ACCESS
		$relate_users 	= $this->admin_m->get_all_by_permissions('contract.revenue.access');
		if($relate_users === FALSE)
		{
			return parent::render($data, 'revenue');
		}

		if(is_array($relate_users))
		{
			$this->admin_ui->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
		}

		$this->load->config('contract');
		$services = $this->config->item('services');

		/* Load danh sách nhân viên kinh doanh */
		$sales = $this->sale_m->select('user_id,user_email,display_name')->set_user_type()->set_role()->as_array()->get_all();
		$sales = array_map(function($x){ $x['display_name'] = $x['display_name'] ?: $x['user_email']; return $x; }, $sales);
		$sales = key_value($sales, 'user_id', 'display_name');

		$money_range = array(
			'0-1000000'	=> '1.000.000 <',
			'1000000-3000000' 	=> '1.000.000 < 3.000.000',
			'3000000-7000000' 	=> '3.000.000 < 7.000.000',
			'7000000-10000000' 	=> '7.000.000 < 10.000.000',
			'10000000-15000000' => '10.000.000 < 15.000.000',
			'15000000-25000000' => '15.000.000 < 25.000.000',
			'25000000-2147483647'	=> ' >= 25.000.000',
			);

		$term_status_def = $this->config->item('contract_status');
		unset($term_status_def['draft'],$term_status_def['remove'],$term_status_def['unverified']);

		$this->submit();
		$this->search_filter();

		$data['content'] = $this->admin_ui
		->set_filter_position(FILTER_TOP_OUTTER)
		->select('term.term_id,term.term_type,term.term_status')
		
		->add_search('contract_code',['placeholder'=>'Số hợp đồng'])
		->add_search('contract_begin',['placeholder'=>'T/g HĐ bắt đầu','class'=>'form-control set-datepicker'])
		->add_search('contract_end',['placeholder'=>'T/g HĐ Kết thúc','class'=>'form-control set-datepicker'])
		->add_search('start_service_time',['placeholder'=>'T/g thực hiện','class'=>'form-control set-datepicker'])
		->add_search('caution_end_time',['placeholder'=>'Hạn bảo lãnh','class'=>'form-control set-datepicker'])
		->add_search('latest_receipt_date',['placeholder'=>'Ngày thanh toán','class'=>'form-control set-datepicker'])
		->add_search('customer',['placeholder'=>'Khách hàng'])

		->add_search('vat',array('content'=> form_dropdown(array('name'=>'where[vat]','class'=>'form-control select2','multiple'=>'multiple','placeholder'=>'abc'),prepare_dropdown([10=>'10'],'VAT'),$this->input->get('where[vat]'))))

		->add_search('invs_amount',array('content'=> form_dropdown(array('name'=>'where[invs_amount][]','class'=>'form-control select2','multiple'=>'multiple'),prepare_dropdown($money_range,'GT dự thu'),$this->input->get('where[invs_amount][]'))))
		
		->add_search('invs_amount_expired',array('content'=> form_dropdown(array('name'=>'where[invs_amount_expired][]','class'=>'form-control select2','multiple'=>'multiple'),prepare_dropdown($money_range,'GT dự thu quá hạn'),$this->input->get('where[invs_amount_expired]'))))

		->add_search('payment_amount',array('content'=> form_dropdown(array('name'=>'where[payment_amount][]','class'=>'form-control select2','multiple'=>'multiple'),prepare_dropdown($money_range,'GT đã thu'),$this->input->get('where[payment_amount]'))))
		
		->add_search('payment_amount_remaining',array('content'=> form_dropdown(array('name'=>'where[payment_amount_remaining]','class'=>'form-control select2','multiple'=>'multiple'),prepare_dropdown($money_range,'GT còn phải thu'),$this->input->get('where[payment_amount_remaining]'))))

		->add_search('contract_value',array('content'=> form_dropdown(array('name'=>'where[contract_value][]','class'=>'form-control select2','multiple'=>'multiple'),prepare_dropdown($money_range,'Tất cả GT trước VAT'),$this->input->get('where[contract_value]'))))

		->add_search('staff_business',array('content'=> form_dropdown(array('name'=>'where[staff_business]','class'=>'form-control select2','multiple'=>'multiple'),prepare_dropdown($sales,'Tất cả NVKD'),$this->input->get('where[staff_business]'))))

		->add_search('term_type',array('content'=> form_dropdown(array('name'=>'where[term_type]','class'=>'form-control select2','multiple'=>'multiple'),prepare_dropdown($this->config->item('taxonomy'),'Tất cả dịch vụ'),$this->input->get('where[term_type]'))))
		
		->add_search('term_status',array('content'=> form_dropdown(array('name'=>'where[term_status][]','class'=>'form-control select2','multiple'=>'multiple'),prepare_dropdown($term_status_def,'Trạng thái HĐ'),$this->input->get('where[term_status]'))))

		->add_search('action',array('content'=> $this->admin_form->submit('search','Tìm kiếm') . $this->admin_form->submit(['name'=>'export_list_revenue_type_excel','class'=>'btn btn-success'],'Export .xls')))

		->add_column('contract_code', array('set_select'=> FALSE, 'title'=> 'Số HĐ','set_order'=> TRUE))
		->add_column('contract_value', array('set_select'=> FALSE, 'title'=> 'Giá Trị trước VAT','set_order'=> TRUE))
		->add_column('service_fee', array('set_select'=> FALSE, 'title'=> 'Phí DV','set_order'=> TRUE))
		->add_column('vat', array('set_select'=> FALSE, 'title'=> 'VAT','set_order'=> TRUE))
		->add_column('invs_amount', array('set_select'=> FALSE, 'title'=> 'Dự thu','set_order'=> TRUE))
		->add_column('invs_amount_expired', array('set_select'=> FALSE, 'title'=> 'Dự thu quá hạn','set_order'=> TRUE))
		->add_column('payment_amount', array('set_select'=> FALSE, 'title'=> 'Đã thu','set_order'=> TRUE))
		->add_column('payment_amount_remaining', array('set_select'=> FALSE, 'title'=> 'Còn phải thu','set_order'=> TRUE))
		->add_column('caution_end_time', array('set_select'=> FALSE, 'title'=> 'Hạn bảo lãnh','set_order'=> TRUE))
		->add_column('latest_receipt_date', array('set_select'=> FALSE, 'title'=> 'Ngày thanh toán','set_order'=> TRUE))
		->add_column('action', array('set_select'=> FALSE, 'title'=> 'Action','set_order'=> TRUE))
		->add_column_callback('term_id',function($data,$row_name) use ($sales){

			$term_id = $data['term_id'];
			
			$contract_code = get_term_meta_value($term_id,'contract_code') ?: '<code>HĐ chưa được cấp số</code>';
			$contract_link = anchor(admin_url("contract/edit/{$term_id}"),$contract_code,['target'=>'_blank']);
			$contract_code = "<b>{$contract_link}</b>";
			$contract_code.= '<small><em> ('.$this->config->item($data['term_status'],'contract_status').')</em></small>';

			// Get contract date range
			$contract_begin = get_term_meta_value($term_id,'contract_begin');
			$contract_end = get_term_meta_value($term_id,'contract_end');
			$contract_daterange = my_date($contract_begin,'d/m/Y').' - '.my_date($contract_end,'d/m/Y');

			$contract_code.= br()."<span class='text-muted col-xs-6' data-toggle='tooltip' title='Thời gian trên HĐ'><i class='fa fa-fw fa-adjust'></i>{$contract_daterange}</span>";

			// Get start_service_time
			$start_service_time = get_term_meta_value($term_id,'start_service_time');
			$start_service_date = my_date($start_service_time,'d/m/Y');
			$contract_code.= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Thời gian Kích hoạt'><i class='fa fa-fw fa-play'></i>{$start_service_date}</span>";

			// Get Nhân viên kinh doanh phụ trách hợp đồng
			$sale_id = get_term_meta_value($data['term_id'],'staff_business');
			$staff_business = !empty($sales[$sale_id]) ? $sales[$sale_id] : '--';
			$contract_code.= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Nhân viên kinh doanh phụ trách'><i class='fa fa-fw fa-user'></i>{$staff_business}</span>";

			// Get service label by term_type
			$service_label = $this->config->item($data['term_type'],'services');
			$contract_code.= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Dịch vụ'><i class='fa fa-fw fa-cubes'></i>{$service_label}</span>";

			if($customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company','system']))
			{
				$customer = end($customers);
				$display_name = mb_ucwords(mb_strtolower($this->admin_m->where('user_id',$customer->user_id)->get_by()->display_name));
				$contract_code.= br()."<span class='text-muted col-xs-12' data-toggle='tooltip' title='Khách hàng'><i class='fa fa-fw fa-user-secret'></i>{$display_name}</span>";
			}

			$data['contract_code'] = $contract_code;

			/*Callback các chỉ số liên quan doanh thu*/

			// Get phí dịch vụ
			$service_fee = (int) get_term_meta_value($term_id,'service_fee');

			// Get tax
			$vat = (int) get_term_meta_value($term_id,'vat');

			// Get tổng giá trị trước VAT
			$contract_value = (int) get_term_meta_value($term_id,'contract_value');

			$invs_amount = (int) get_term_meta_value($term_id,'invs_amount');

			$payment_amount = (int) get_term_meta_value($term_id,'payment_amount');

			$payment_amount_remaining = $invs_amount - $payment_amount;
			$payment_amount_remaining = max(0, $payment_amount_remaining);

			// Tính dự thu quá hạn
			$invs_amount_expired = (int)get_term_meta_value($term_id,'invs_amount_expired');

			$data['vat'] = "{$vat}%";
			$data['service_fee'] = currency_numberformat($service_fee,'<i>đ</i>');
			$data['contract_value'] = currency_numberformat($contract_value,'<i>đ</i>');
			$data['invs_amount'] = currency_numberformat($invs_amount,'<i>đ</i>');
			$data['invs_amount_expired'] = currency_numberformat($invs_amount_expired,'<i>đ</i>');
			$data['payment_amount'] = currency_numberformat($payment_amount,'<i>đ</i>');
			$data['payment_amount_remaining'] = currency_numberformat($payment_amount_remaining,'<i>đ</i>');

			$receipts = $this->receipt_m
			->select('posts.post_id,posts.post_author,posts.post_type,posts.end_date,posts.post_status')
			->join('term_posts','term_posts.post_id = posts.post_id','LEFT')
			->where('term_posts.term_id',$term_id)
			->where('posts.post_status !=','draft')
			->where_in('posts.post_type',['receipt_payment','receipt_caution'])
			->order_by('posts.end_date','desc')
			->get_many_by();
			$receipts = array_group_by($receipts,'post_type');

			// Xử lý data 'hạn bảo lãnh'
			$receipt_caution = NULL;
			$caution_end_time = '';
			if(!empty($receipts['receipt_caution']))
			{
				$receipts_caution = $receipts['receipt_caution'];
				foreach ($receipts_caution as $item)
				{
					if($item->post_status == 'paid') continue;
					$receipt_caution = $item;
				}
			}

			$prev_amount = 0;
			$end_date = '';
			$end_time = '';
			if( ! empty($receipt_caution))
			{
				$prev_amount = (int) get_post_meta_value($receipt_caution->post_id,'amount');
				$end_date = my_date($receipt_caution->end_date,'d-m-Y');

				$end_time = strtotime('-8 hours',$receipt_caution->end_date);
			}

			$data['caution_end_time'] = 
			"<term-bailment-component
				:prev_amount='{$prev_amount}'
				end_time='{$end_time}'
				end_date='{$end_date}'
				contract_code='".get_term_meta_value($term_id,'contract_code')."'
				sale_displayname='".($sales[$sale_id] ?? '')."'
				user_id='{$sale_id}'
				term_id='{$term_id}'
				post_id='".($receipt_caution->post_id??0)."'
				></term-bailment-component>";

			// 'Ngày thanh toán'
			$data['latest_receipt_date'] = '';
			if(!empty($receipts['receipt_payment']))
			{
				$receipts['receipt_payment'] = array_group_by($receipts['receipt_payment'],'post_status');
				if(!empty($receipts['receipt_payment']['paid']))
				{
					$receipt_payment_paid = $receipts['receipt_payment']['paid'];
					$receipt_payment_newest = reset($receipt_payment_paid);

					$data['latest_receipt_date'] = my_date($receipt_payment_newest->end_date,'d/m/Y');
				}
			}

			// Render 'Actions' column
			if(has_permission('contract.receipt.access') || has_permission('contract.receipt.manage'))
			{
				$data['action'] = anchor(module_url("ajax/receipt/index/{$data['term_id']}"),'<i class="fa fa-fw fa-money"></i>','title="Danh sách đã thu" class="btn btn-default btn-xs ajax-receipt-list"');
			}

			return $data;

		},FALSE)

		->from('term')
		->where_in('term.term_type',array_keys($this->config->item('taxonomy')))
		->where_in('term.term_status',array_keys($term_status_def))
		->group_by('term.term_id')
		->generate(['per_page' => 10]);

		// prepare DB Query for action 'Export .Xls'
		$this->scache->delete("modules/contract/query_list_revenue_{$this->admin_m->id}");
		
		$last_query = $this->admin_ui->last_query;
		$query = '' ;
		if( !empty($last_query) ) 
		{
			$last_query	=	explode(" LIMIT ", $last_query) ; 
			$query		= 	$last_query[0] ;
		}

		$this->scache->write($query,"modules/contract/query_list_revenue_{$this->admin_m->id}");

		parent::render($data,'revenue');
	}

	protected function search_filter($args = array())
	{
		restrict('contract.revenue.access');
		if(empty($args) && $this->input->get('search'))  
			$args = $this->input->get();	

		// Contract_code FILTERING & SORTING
		$filter_contract_code = $args['where']['contract_code'] ?? FALSE;
		$sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
		if($filter_contract_code || $sort_contract_code)
		{
			$alias = uniqid('contract_code_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

			if($filter_contract_code)
			{	
				$this->admin_ui->like("{$alias}.meta_value",$filter_contract_code);
			}

			if($sort_contract_code)
			{
				$this->admin_ui->order_by("({$alias}.meta_value * 1)",$sort_contract_code);
			}
		}

		// Contract_begin FILTERING & SORTING
		$filter_contract_begin 	= $args['where']['contract_begin'] ?? FALSE;
		$sort_contract_begin 	= $args['order_by']['contract_begin'] ?? FALSE;
		if($filter_contract_begin || $sort_contract_begin)
		{
			$alias = uniqid('contract_begin_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

			if($filter_contract_begin)
			{	
				$dates = explode(' - ', $filter_contract_begin);
				$this->admin_ui->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->admin_ui->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
			}

			if($sort_contract_begin)
			{
				$this->admin_ui->order_by("{$alias}.meta_value",$sort_contract_begin);
			}
		}

		// Contract_begin FILTERING & SORTING
		$filter_contract_end = $args['where']['contract_end'] ?? FALSE;
		$sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
		if($filter_contract_end || $sort_contract_end)
		{
			$alias = uniqid('contract_end_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_end'", 'LEFT');

			if($filter_contract_end)
			{	
				$dates = explode(' - ', $filter_contract_end);
				$this->admin_ui->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->admin_ui->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
			}

			if($sort_contract_end)
			{
				$this->admin_ui->order_by("{$alias}.meta_value",$sort_contract_end);
			}
		}

		// Contract_begin FILTERING & SORTING
		$filter_start_service_time = $args['where']['start_service_time'] ?? FALSE;
		$sort_start_service_time = $args['order_by']['start_service_time'] ?? FALSE;
		if($filter_start_service_time || $sort_start_service_time)
		{
			$alias = uniqid('start_service_time_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'start_service_time'", 'LEFT');

			if($filter_start_service_time)
			{	
				$dates = explode(' - ', $filter_start_service_time);
				$this->admin_ui->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->admin_ui->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
			}

			if($sort_start_service_time)
			{
				$this->admin_ui->order_by("{$alias}.meta_value",$sort_start_service_time);
			}
		}

		// Contract_code FILTERING & SORTING
		$filter_customer = $args['where']['customer'] ?? FALSE;
		$sort_customer = $args['order_by']['customer'] ?? FALSE;
		if($filter_customer || $sort_customer)
		{

			// get_customers
			$alias = uniqid('customer_');
			$this->admin_ui->join("term_users {$alias}","{$alias}.term_id = term.term_id", 'LEFT');
			$this->admin_ui->join("user","{$alias}.user_id = user.user_id and user.user_type IN ('customer_company','customer_person')", 'LEFT');

			if($filter_customer)
			{	
				$this->admin_ui->like("user.display_name",$filter_customer);
			}

			if($sort_customer)
			{
				$this->admin_ui->order_by("user.display_name",$sort_customer);
			}
		}

		// Caution FILTERING & SORTING
		$filter_caution_end_time = $args['where']['caution_end_time'] ?? FALSE;
		$sort_caution_end_time = $args['order_by']['caution_end_time'] ?? FALSE;
		if($filter_caution_end_time || $sort_caution_end_time)
		{
			$alias = uniqid('');
			$this->admin_ui->join("term_posts term_posts_{$alias}","term_posts_{$alias}.term_id = term.term_id", 'LEFT');
			$this->admin_ui->join("posts posts_{$alias}","posts_{$alias}.post_id = term_posts_{$alias}.post_id AND posts_{$alias}.post_type = 'receipt_caution' AND posts_{$alias}.post_status IN ('publish','paid')", 'LEFT');

			if($filter_caution_end_time)
			{	
				$dates = explode(' - ', $filter_caution_end_time);
				$this->admin_ui->where("posts_{$alias}.end_date >=", $this->mdate->startOfDay(reset($dates)));
				$this->admin_ui->where("posts_{$alias}.end_date <=", $this->mdate->endOfDay(end($dates)));
			}

			if($sort_start_service_time)
			{
				$this->admin_ui->order_by("posts_{$alias}.end_date",$sort_start_service_time);
			}
		}

		// VAT FILTERING & SORTING
		$filter_vat = $args['where']['vat'] ?? FALSE;
		$sort_vat = $args['order_by']['vat'] ?? FALSE;
		if($filter_vat || $sort_vat)
		{
			$alias = uniqid('vat_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'vat'", 'LEFT');

			if($filter_vat)
			{	
				$this->admin_ui->where("{$alias}.meta_value", $filter_vat);
			}

			if($sort_vat)
			{
				$this->admin_ui->order_by("{$alias}.meta_value",$sort_vat);
			}
		}

		// INVOICES AMOUNT FILTERING & SORTING
		$filter_invs_amount = !empty($args['where']['invs_amount']) ? array_filter($args['where']['invs_amount']) : FALSE;
		$sort_invs_amount = $args['order_by']['invs_amount'] ?? FALSE;
		if(!empty($filter_invs_amount) || $sort_invs_amount)
		{
			$alias = uniqid('invs_amount_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'invs_amount'", 'LEFT');

			if($filter_invs_amount)
			{	
				$money_range = array();
				foreach ($filter_invs_amount as $money_pair)
				{
					$money_pair = explode('-', $money_pair);
					array_push($money_range,(int) reset($money_pair),(int) end($money_pair));
				}
				$this->admin_ui->where("{$alias}.meta_value >", min($money_range));
				$this->admin_ui->where("{$alias}.meta_value <", max($money_range));
			}

			if($sort_invs_amount)
			{
				$this->admin_ui->order_by("({$alias}.meta_value * 1)",$sort_invs_amount);
			}
		}

		// INVOICES AMOUNT EXPIRED FILTERING & SORTING
		$filter_invs_amount_expired = !empty($args['where']['invs_amount_expired'])?array_filter($args['where']['invs_amount_expired']):FALSE;
		$sort_invs_amount_expired = $args['order_by']['invs_amount_expired'] ?? FALSE;
		if(!empty($filter_invs_amount_expired) || $sort_invs_amount_expired)
		{
			$alias = uniqid('invs_amount_expired_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'invs_amount_expired'", 'LEFT');

			if($filter_invs_amount_expired)
			{	
				$money_range = array();
				foreach ($filter_invs_amount_expired as $money_pair)
				{
					$money_pair = explode('-', $money_pair);
					array_push($money_range,(int) reset($money_pair),(int) end($money_pair));
				}

				$this->admin_ui->where("{$alias}.meta_value >", min($money_range));
				$this->admin_ui->where("{$alias}.meta_value <", max($money_range));
			}

			if($sort_invs_amount_expired)
			{
				$this->admin_ui->order_by("({$alias}.meta_value * 1)",$sort_invs_amount_expired);
			}
		}

		// PAYMENT AMOUNT FILTERING & SORTING
		$filter_payment_amount = !empty($args['where']['payment_amount'])?array_filter($args['where']['payment_amount']) : FALSE;
		$sort_payment_amount = $args['order_by']['payment_amount'] ?? FALSE;
		if(!empty($filter_payment_amount) || $sort_payment_amount)
		{
			$alias = uniqid('payment_amount_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_amount'", 'LEFT');

			if($filter_payment_amount)
			{	
				$money_range = array();
				foreach ($filter_payment_amount as $money_pair)
				{
					$money_pair = explode('-', $money_pair);
					array_push($money_range,(int) reset($money_pair),(int) end($money_pair));
				}
				$this->admin_ui->where("{$alias}.meta_value >", min($money_range));
				$this->admin_ui->where("{$alias}.meta_value <", max($money_range));
			}

			if($sort_payment_amount)
			{
				$this->admin_ui->order_by("({$alias}.meta_value * 1)",$sort_payment_amount);
			}
		}

		// PAYMENT AMOUNT REMAILING FILTERING & SORTING
		$filter_payment_amount_remaining = !empty($args['where']['payment_amount_remaining'])?array_filter($args['where']['payment_amount_remaining']) : FALSE;
		$sort_payment_amount_remaining = $args['order_by']['payment_amount_remaining'] ?? FALSE;
		if(!empty($filter_payment_amount_remaining) || $sort_payment_amount_remaining)
		{
			$alias = uniqid('payment_amount_remaining_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_amount_remaining'", 'LEFT');

			if($filter_payment_amount_remaining)
			{	
				$money_range = array();
				foreach ($filter_payment_amount_remaining as $money_pair)
				{
					$money_pair = explode('-', $money_pair);
					array_push($money_range,(int) reset($money_pair),(int) end($money_pair));
				}
				$this->admin_ui->where("{$alias}.meta_value >", min($money_range));
				$this->admin_ui->where("{$alias}.meta_value <", max($money_range));
			}

			if($sort_payment_amount_remaining)
			{
				$this->admin_ui->order_by("({$alias}.meta_value * 1)",$sort_payment_amount_remaining);
			}
		}

		// PAYMENT AMOUNT REMAILING FILTERING & SORTING
		$filter_contract_value = !empty($args['where']['contract_value'])? array_filter($args['where']['contract_value']) : FALSE;
		$sort_contract_value = $args['order_by']['contract_value'] ?? FALSE;
		if(!empty($filter_contract_value) || $sort_contract_value)
		{
			$alias = uniqid('contract_value_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_value'", 'LEFT');

			if($filter_contract_value)
			{	
				$money_range = array();
				foreach ($filter_contract_value as $money_pair)
				{
					$money_pair = explode('-', $money_pair);
					array_push($money_range,(int) reset($money_pair),(int) end($money_pair));
				}
				$this->admin_ui->where("{$alias}.meta_value >", min($money_range));
				$this->admin_ui->where("{$alias}.meta_value <", max($money_range));
			}

			if($sort_contract_value)
			{
				$this->admin_ui->order_by("({$alias}.meta_value * 1)",$sort_contract_value);
			}
		}

		// PAYMENT AMOUNT REMAILING FILTERING & SORTING
		$filter_service_fee = !empty($args['where']['service_fee'])?array_filter($args['where']['service_fee']) : FALSE;
		$sort_service_fee = $args['order_by']['service_fee'] ?? FALSE;
		if(!empty($filter_service_fee) || $sort_service_fee)
		{
			$alias = uniqid('service_fee_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'service_fee'", 'LEFT');

			if($filter_service_fee)
			{	
				$money_range = array();
				foreach ($filter_service_fee as $money_pair)
				{
					$money_pair = explode('-', $money_pair);
					array_push($money_range,(int) reset($money_pair),(int) end($money_pair));
				}
				$this->admin_ui->where("{$alias}.meta_value >", min($money_range));
				$this->admin_ui->where("{$alias}.meta_value <", max($money_range));
			}

			if($sort_service_fee)
			{
				$this->admin_ui->order_by("({$alias}.meta_value * 1)",$sort_service_fee);
			}
		}

		// Contract_code FILTERING & SORTING
		$filter_staff_business = $args['where']['staff_business'] ?? FALSE;
		$sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
		if($filter_staff_business || $sort_staff_business)
		{
			$alias = uniqid('staff_business_');
			$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT');

			if($filter_staff_business)
			{	
				$this->admin_ui->like("{$alias}.meta_value",$filter_staff_business);
			}

			if($sort_staff_business)
			{
				$this->admin_ui->order_by("{$alias}.meta_value",$sort_staff_business);
			}
		}

		// TERM_TYPE FILTERING & SORTING
		$filter_term_type = $args['where']['term_type'] ?? FALSE;
		$sort_term_type = $args['order_by']['term_type'] ?? FALSE;
		if($filter_term_type || $sort_term_type)
		{
			if($filter_term_type)
			{	
				$this->admin_ui->where_in('term.term_type',$filter_term_type);
			}

			if($sort_term_type)
			{
				$this->admin_ui->order_by("term.term_type",$sort_term_type);
			}
		}

		// TERM_STATUS FILTERING & SORTING
		$filter_term_status = !empty($args['where']['term_status'])?array_filter($args['where']['term_status']) : FALSE;
		$sort_term_status = $args['order_by']['term_status'] ?? FALSE;
		if(!empty($filter_term_status) || $sort_term_status)
		{
			if(!empty($filter_term_status))
			{	
				$this->admin_ui->where_in('term.term_status',$filter_term_status);
			}

			if($sort_term_status)
			{
				$this->admin_ui->order_by("term.term_status",$sort_term_status);
			}
		}

		// RECEIPT LAST PAYMENT FILTERING & SORTING
		$filter_latest_receipt_date = $args['where']['latest_receipt_date'] ?? FALSE;
		$sort_latest_receipt_date = $args['order_by']['latest_receipt_date'] ?? FALSE;
		if($filter_latest_receipt_date || $sort_latest_receipt_date)
		{

			$alias = uniqid('receipts_payment');
			$this->admin_ui->join("term_posts term_posts_{$alias}","term_posts_{$alias}.term_id = term.term_id");
			$this->admin_ui->join("posts posts_{$alias}","posts_{$alias}.post_id = term_posts_{$alias}.post_id AND posts_{$alias}.post_type = 'receipt_payment' AND posts_{$alias}.post_status IN ('paid')", 'LEFT');

			if($filter_latest_receipt_date)
			{	
				$dates = explode(' - ', $filter_latest_receipt_date);
				$this->admin_ui->where("posts_{$alias}.end_date >=", $this->mdate->startOfDay(reset($dates)));
				$this->admin_ui->where("posts_{$alias}.end_date <=", $this->mdate->endOfDay(end($dates)));
			}

			if($sort_latest_receipt_date)
			{
				$this->admin_ui->order_by("posts_{$alias}.end_date",$sort_latest_receipt_date);
			}
		}

		// CAUTION RECEIPT FILTERING & SORTING
		$filter_caution_end_time = $args['where']['caution_end_time'] ?? FALSE;
		$sort_caution_end_time = $args['order_by']['caution_end_time'] ?? FALSE;
		if($filter_caution_end_time || $sort_caution_end_time)
		{
			$alias = uniqid('receipt_caution');
			$this->admin_ui->join("term_posts term_posts_{$alias}","term_posts_{$alias}.term_id = term.term_id");
			$this->admin_ui->join("posts posts_{$alias}","posts_{$alias}.post_id = term_posts_{$alias}.post_id AND posts_{$alias}.post_type = 'receipt_caution' AND posts_{$alias}.post_status IN ('publish','paid')", 'LEFT');

			if($filter_caution_end_time)
			{	
				$dates = explode(' - ', $filter_caution_end_time);
				$this->admin_ui->where("posts_{$alias}.end_date >=", $this->mdate->startOfDay(reset($dates)));
				$this->admin_ui->where("posts_{$alias}.end_date <=", $this->mdate->endOfDay(end($dates)));
			}

			if($sort_caution_end_time)
			{
				$this->admin_ui->order_by("posts_{$alias}.end_date",$sort_caution_end_time);
			}
		}
	}

	/**
	 * Import payment info from xlsx files to receive
	 */
	public function import_payment()
	{
		redirect(module_url(),'refresh');
		restrict('contract.revenue.manage');
		$this->submit();

		$data = $this->data;
		$cache_key = "user-revenue-data-parsed-{$this->admin_m->id}";
		$data_parsed = $this->scache->get($cache_key);
		if(!empty($data_parsed))
		{
			$this->table->set_heading('STAT','NGÀY THU','KHÁCH HÀNG','SỐ HĐ','SỐ TIỀN','NVKD','VAT','DOANH THU','DỊCH VỤ','TK');
			foreach ($data_parsed as $row)
			{
				$is_exist_concode = !empty(end($row)) ? '' : 'Không tìm thấy';
				$row['A'] = my_date($row['A']);
				$row['B'] = mb_ucwords($row['B']);
				$row['F'] = ($row['F']*100).'%';
				array_pop($row);
				$row = array_replace([$is_exist_concode], $row);
				$this->table->add_row($row);
			}

			$data['content'] = $this->table->generate();
		}

		parent::render($data);
	}

	protected function submit()
	{
		$post = $this->input->post();
		$post = array_merge($post, $this->input->get());

		if(empty($post)) return FALSE;
		
		if(!empty($post['btnImportFile']))
		{
			$upload_cfg = array(
				'upload_path' => './tmp/',
				'allowed_types' => 'xlsx',
				'max_size'	=> '4096',
				'file_name' => md5(time())
				);

			if(!is_dir($upload_cfg['upload_path'])) 
				mkdir($upload_cfg['upload_path'], '0777', TRUE);

			$this->load->library('upload');
			$this->upload->initialize($upload_cfg);

			$response = ['msg'=>'Tệp không hợp lệ , vui lòng thử lại!','success' => FALSE];
			$this->upload->do_upload('inputFile');
			if(!$this->upload->do_upload('inputFile'))
			{
				$this->messages->error('Tệp không hợp lệ , vui lòng thử lại!');
				redirect(module_url('revenue/import_payment'),'refresh');
			}

			$data = $this->upload->data();
			$fileName = $upload_cfg['upload_path'].$data['file_name'];

			$response = ['msg'=>'Upload file danh sách thanh toán thành công !','success'=>TRUE,'notification'=>'Dữ liệu không hợp lệ !'];

			/* PROCESS EXCEL FILE TO DATA */
			$this->load->library('excel');
			$objReader = PHPExcel_IOFactory::createReaderForFile($fileName);
			$objReader->setReadDataOnly(TRUE); //set this, to not read all excel properties, just data
			$objPHPExcel = $objReader->load($fileName);

			$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,TRUE,FALSE,TRUE);
			$sheetHeading = array_shift($sheetData);
			$sheetHeading = array_shift($sheetData);

			$sheetData = array_filter($sheetData,function($row){return !empty($row['C']);});
			
			$objPHPExcel->disconnectWorksheets();
			unset($objPHPExcel);
			@unlink($fileName);

			if(empty($sheetData))
			{
				$this->messages->error('Dữ liệu không hợp lệ , vui lòng nhập lại!');
				redirect(module_url('revenue/import_payment'),'refresh');
			}

			$contracts = array_group_by($sheetData,'C');
			$contract_codes = array_keys($contracts);

			$terms = $this->termmeta_m->select('term_id,meta_value')
			->where('meta_key','contract_code')
			->where_in('meta_value',$contract_codes)
			->get_many_by();

			$exist_contract_code = array();
			if(!empty($terms))
			{
				foreach ($terms as $term)
				{
					$key = mb_strtolower($term->meta_value);
					$exist_contract_code[$key] = $term;
				}
			}

			foreach ($sheetData as &$row)
			{	
				$row['A'] = PHPExcel_Shared_Date::ExcelToPHP($row['A']);
				$contract_code = strtolower(trim($row['C']));
				$is_exist_concode = !empty($exist_contract_code[$contract_code]);
				$row['J'] = $is_exist_concode;
				$row['K'] = $exist_contract_code[$contract_code]->term_id ?? '';
			}

			if(!empty($sheetData))
			{
				usort($sheetData, function($a,$b){
					return $a['J'] > $b['J'] ? TRUE : FALSE;
				});
				$this->scache->write($sheetData,"user-revenue-data-parsed-{$this->admin_m->id}",3600);
			}

			$this->messages->success('Dữ liệu load thành công!');
			$this->messages->success('SCROLL xuống cuối trang để LƯU DỮ LIỆU !');
			
			redirect(module_url('revenue/import_payment'),'refresh');
		}

		// Export payment row mismatch exists contract
		if(!empty($post['export_mismatch']))
		{
			$data = $this->scache->get("user-revenue-data-parsed-{$this->admin_m->id}");
			if(empty($data))
			{
				$this->messages->error('Dữ liệu rỗng , không tồn tại dữ liệu không hợp lệ');
				redirect(module_url('revenue/import_payment'),'refresh');
			}

			$data_group = array_group_by($data,'J');
			if(empty($data_group[0]))
			{
				$this->messages->error('Không tồn tại dữ liệu không hợp lệ');
				redirect(module_url('revenue/import_payment'),'refresh');
			}
			$data_mismatch = $data_group[0];

			$this->load->library('excel');
			$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	        $cacheSettings = array( 'memoryCacheSize' => '512MB');
	        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	        $objPHPExcel = new PHPExcel();
	        $objPHPExcel
	        ->getProperties()
	        ->setCreator("WEBDOCTOR.VN")
	        ->setLastModifiedBy("WEBDOCTOR.VN")
	        ->setTitle(uniqid('Danh sách thanh toán không hợp lệ __'));

	        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/invoices/payment-list-export.xlsx');

	        $objPHPExcel->setActiveSheetIndex(0);
	        $objWorksheet = $objPHPExcel->getActiveSheet();

	        $data_mismatch = $data_group[0];
	        foreach ($data_mismatch as $key => $row)
	        {
	        	$row_index = $key + 3;
	    		$objWorksheet->setCellValueByColumnAndRow(0, $row_index, PHPExcel_Shared_Date::PHPToExcel($row['A']));
	    		$objWorksheet->setCellValueByColumnAndRow(1, $row_index, $row['B']);
	    		$objWorksheet->setCellValueByColumnAndRow(2, $row_index, $row['C']);
	    		$objWorksheet->setCellValueByColumnAndRow(3, $row_index, $row['D']);
	    		$objWorksheet->setCellValueByColumnAndRow(4, $row_index, $row['E']);
	    		$objWorksheet->setCellValueByColumnAndRow(5, $row_index, $row['F']);
	    		$objWorksheet->setCellValueByColumnAndRow(6, $row_index, $row['G']);
	    		$objWorksheet->setCellValueByColumnAndRow(7, $row_index, $row['H']);
	    		$objWorksheet->setCellValueByColumnAndRow(8, $row_index, $row['I']);
	        }
	        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

	        $file_name = "tmp/mismatch-payment-excel.xlsx";
	        $objWriter->save($file_name);

	        if(file_exists($file_name))
	        {	
				$this->load->helper('download');
				force_download($file_name,NULL);
	        }

	        redirect(module_url('revenue/import_payment'),'refresh');	
		}

		if(!empty($post['update_data']))
		{
			$cache_key = "user-revenue-data-parsed-{$this->admin_m->id}";
			$data_parsed = $this->scache->get($cache_key);
			if(empty($data_parsed))
			{
				$this->messages->error('Dữ liệu không hợp lệ!');
				redirect(module_url('revenue/import_payment'),'refresh');
			}
			
			$data_parsed = array_filter($data_parsed,function($x){ return !empty($x['J']); });

			$this->load->model('contract/receipt_m');
			$receipts = $this->receipt_m
			->join('term_posts','term_posts.post_id = posts.post_id')
			->where_in('term_posts.term_id',array_column($data_parsed, 'K'))
			->get_many_by(['posts.post_type'=>'receipt_payment','posts.post_author'=>$this->admin_m->id]);

			$time = time();
			$receipts = array_group_by($receipts,'term_id');
			$data_parsed = array_group_by($data_parsed,'K');

			foreach ($data_parsed as $key => $items)
			{
				$post_ids = array();

				// Nếu hoàn toàn chưa có đợt thanh toán nào phát sinh với hợp đồng hợp đồng thì tạo mới hoàn toàn
				if(empty($receipts[$key]))
				{
					foreach ($items as $item)
					{
						$insert_data = array(
							'post_title'=>"{$item['B']} {$item['D']} {$item['E']} {$item['H']} {$item['I']}",
							'post_author'=>$this->admin_m->id,
							'post_status'=>'paid',
							'created_on'=>$time,
							'post_type'=>'receipt_payment',
							'end_date'=>$this->mdate->startOfDay($item['A'])
							);

						$insert_id = $this->receipt_m->insert($insert_data);
						$post_ids[] = $insert_id;

						$transfer_type = strcasecmp(trim($item['I']),'TM') == 0 ? 'cash' : 'account';

						update_post_meta($insert_id,'transfer_account',$item['I']);
						update_post_meta($insert_id,'transfer_type',$transfer_type);
						update_post_meta($insert_id,'amount',$item['D']);
					}

					$this->term_posts_m->set_term_posts($key,$post_ids);
					continue;
				}

				$payments = array_column($receipts[$key], NULL,'end_date');
				foreach ($items as $item)
				{
					// Nếu chưa có phát sinh đợt thanh toán nào trong hợp đồng thì tạo mới hoàn toàn
					if(empty($payments[$item['A']]))
					{
						$insert_data = array(
							'post_title'=>"{$item['B']} {$item['D']} {$item['E']} {$item['H']} {$item['I']}",
							'post_author'=>$this->admin_m->id,
							'post_status'=>'paid',
							'created_on'=>$time,
							'post_type'=>'receipt_payment',
							'end_date'=>$this->mdate->startOfDay($item['A'])
							);

						$insert_id = $this->receipt_m->insert($insert_data);
						$post_ids[] = $insert_id;

						$transfer_type = strcasecmp(trim($item['I']),'TM') == 0 ? 'cash' : 'account';

						update_post_meta($insert_id,'transfer_account',$item['I']);
						update_post_meta($insert_id,'transfer_type',$transfer_type);
						update_post_meta($insert_id,'amount',$item['D']);
						continue;
					}

					// Nếu phát sinh 2 đợt thanh toán trong cùng 1 ngày sẽ bỏ qua không xử lý
					if(count($payments[$item['A']]) > 1) continue;

					// Nếu chỉ phát sinh 1 đợt thanh toán trong 1 ngày thì cập nhật
					$last_payment = is_array($payments[$item['A']]) ? reset($payments[$item['A']]) : $payments[$item['A']];

					$update_data = array(
							'post_title'=>"{$item['B']} {$item['D']} {$item['E']} {$item['H']} {$item['I']}",
							'post_author'=>$this->admin_m->id,
							'post_status'=>'paid',
							'created_on'=>$time,
							'post_type'=>'receipt_payment',
							'end_date'=>$this->mdate->startOfDay($item['A'])
							);

					$this->receipt_m->update($last_payment->post_id,$update_data);

					$transfer_type = strcasecmp(trim($item['I']),'TM') == 0 ? 'cash' : 'account';

					update_post_meta($last_payment->post_id,'transfer_account',$item['I']);
					update_post_meta($last_payment->post_id,'transfer_type',$transfer_type);
					update_post_meta($last_payment->post_id,'amount',$item['D']);					
				}

				$this->term_posts_m->set_term_posts($key,$post_ids);
			}

			$terms_updated = array_unique(array_keys($data_parsed));
			foreach ($terms_updated as $term_id) 
			{
				// Tính toán lại các số liệu thanh toán
				$this->load->model('contract/base_contract_m');
				$payment_amount = $this->base_contract_m->calc_payment_amount($term_id);
				update_term_meta($term_id,'payment_amount',(int)$payment_amount);

				$invs_amount = $this->base_contract_m->calc_invs_value($term_id);
				update_term_meta($term_id,'invs_amount',(int)$invs_amount);

				// Tính dự thu quá hạn
				$invs_amount_expired = $this->base_contract_m->calc_payment_amount_expired($term_id,$this->mdate->startOfMonth());
				update_term_meta($term_id,'invs_amount_expired',(int)$invs_amount_expired);

				$payment_amount_remaining = max(0, ($invs_amount - $payment_amount));
				update_term_meta($term_id,'payment_amount_remaining',(int)$payment_amount_remaining);
			}

			$this->scache->delete($cache_key);
			$this->messages->success('Cập nhật dữ liệu thành công!');
			redirect(module_url('revenue/import_payment'),'refresh');
		}

		// XUẤT FILE EXCEL DỰ THU
		if( !empty($post['export_list_revenue_type_excel']))
		{
			$this->messages->clear() ;
			$last_query = $this->scache->get("modules/contract/query_list_revenue_{$this->admin_m->id}");

			if( ! $last_query )
			{
				$this->messages->info('Không tìm thấy lệnh đã truy vấn trước đó , vui lòng thử lại sau');
				redirect(current_url(),'refresh');	
			}

			$revenues = $this->term_m->query($last_query)->result();	

			if(empty($revenues))
			{
				$this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
				redirect(current_url(),'refresh');
			}

			$this->export_list_revenue_type_excel($revenues) ;
			$this->scache->delete("modules/contract/query_list_revenue_{$this->admin_m->id}");			
		}
	}

	public function export_list_revenue_type_excel($revenues = array())
	{
		if(empty($revenues)) return FALSE ;

		$this->load->helper('file');
		$this->load->library('excel');
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
        ->getProperties()
        ->setCreator("WEBDOCTOR.VN")
        ->setLastModifiedBy("WEBDOCTOR.VN")
        ->setTitle(uniqid('Danh sách dự thu __'));

        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        
        $headings = array('STT','SỐ HĐ','TRƯỚC VAT','PHÍ DV','VAT','DỰ THU', 'DỰ THU QUÁ HẠN','ĐÃ THU','CÒN PHẢI THU','HẠN BẢO LÃNH','NGÀY THANH TOÁN','TRẠNG THÁI', 'THỜI GIAN  HỢP ĐỒNG', 'NGÀY BẮT ĐẦU', 'NHÂN VIÊN KINH DOANH', 'GÓI DỊCH VỤ', 'KHÁCH HÀNG');
        $objWorksheet->fromArray($headings, NULL, 'A3');

       	$row_index = 4;
        foreach ($revenues as $key => &$revenue)
		{	
			$row_number = $row_index + $key;

			$term_id	= $revenue->term_id ;

			$receipts = $this->receipt_m
							->select('posts.post_id,posts.post_author,posts.post_type,posts.end_date,posts.post_status')
							->join('term_posts','term_posts.post_id = posts.post_id','LEFT')
							->where('term_posts.term_id',$term_id)
							->where('posts.post_status !=','draft')
							->where_in('posts.post_type',['receipt_payment','receipt_caution'])
							->order_by('posts.end_date','desc')
							->get_many_by();

    		$objWorksheet->setCellValueByColumnAndRow(0, $row_number, ($key+1));

    		// SỐ HĐ
			$contract_code		= get_term_meta_value($term_id, 'contract_code') ;
    		$objWorksheet->setCellValueByColumnAndRow(1, $row_number, $contract_code);
    		
    		// TRƯỚC VAT
			$contract_value     = (double) get_term_meta_value($term_id, 'contract_value') ;
			$revenue->contract_value	=	$contract_value;	
    		$objWorksheet->setCellValueByColumnAndRow(2, $row_number, $contract_value);

    		// PHÍ DV
			$service_fee 			= (double) get_term_meta_value($term_id,'service_fee');
			$revenue->service_fee	=	$service_fee;
    		$objWorksheet->setCellValueByColumnAndRow(3, $row_number, $service_fee);

    		// VAT
			$vat = (int) get_term_meta_value($term_id,'vat');		
			$objWorksheet->setCellValueByColumnAndRow(4, $row_number, div($vat,100));
    		//$objWorksheet->setCellValueByColumnAndRow(4, $row_number, $vat);

			// DỰ THU
			$invs_amount = (double) get_term_meta_value($term_id,'invs_amount');
			$revenue->invs_amount	=	$invs_amount;
			$objWorksheet->setCellValueByColumnAndRow(5, $row_number, $invs_amount);

    		// DỰ THU QUÁ HẠN
			$invs_amount_expired = (double) get_term_meta_value($term_id,'invs_amount_expired');
			$revenue->invs_amount_expired	=	$invs_amount_expired;
    		$objWorksheet->setCellValueByColumnAndRow(6, $row_number, $invs_amount_expired);

    		// ĐÃ THU
			$payment_amount = (double) get_term_meta_value($term_id,'payment_amount');
			$revenue->payment_amount	=	$payment_amount;
    		$objWorksheet->setCellValueByColumnAndRow(7, $row_number, $payment_amount);

    		// CÒN PHẢI THU
			$payment_amount_remaining = (double) ($invs_amount - $payment_amount);
			$payment_amount_remaining = max(0, $payment_amount_remaining);
			$revenue->payment_amount_remaining	=	$payment_amount_remaining;
    		$objWorksheet->setCellValueByColumnAndRow(8, $row_number, $payment_amount_remaining);
    		
			// HẠN BẢO LÃNH
			$receipts = array_group_by($receipts,'post_type');
			
			$receipt_caution = NULL;
			$caution_end_time = '';
			$late_days = 0;
			if(!empty($receipts['receipt_caution']))
			{
				$receipts_caution = $receipts['receipt_caution'];
				foreach ($receipts_caution as $item)
				{
					if($item->post_status == 'paid') continue;

					$receipt_caution = $item;
					if($item->end_date > $this->mdate->startOfDay()) break;

					$late_days = number_format(diffInDates($item->end_date,time()),1) ;
				}
			}
			
			$date_caution_end_time = (!empty($receipt_caution->end_date) ? $receipt_caution->end_date : '') ;
			if(!empty($date_caution_end_time))
			{
				$objWorksheet->setCellValueByColumnAndRow(9, $row_number, PHPExcel_Shared_Date::PHPToExcel($date_caution_end_time));
			}
			else {
				$objWorksheet->setCellValueByColumnAndRow(9, $row_number, '');
			}
    		
    		// NGÀY THANH TOÁN
    		$latest_receipt_date = '';
			if(!empty($receipts['receipt_payment']))
			{
				$receipts['receipt_payment'] = array_group_by($receipts['receipt_payment'],'post_status');
				if(!empty($receipts['receipt_payment']['paid']))
				{
					$receipt_payment_paid = $receipts['receipt_payment']['paid'];
					$receipt_payment_newest = reset($receipt_payment_paid);

					$latest_receipt_date = $receipt_payment_newest->end_date;
					if(!empty($latest_receipt_date))
					{
						$latest_receipt_date = PHPExcel_Shared_Date::PHPToExcel($latest_receipt_date) ;
					}
				}
			}
			$objWorksheet->setCellValueByColumnAndRow(10, $row_number, $latest_receipt_date);
			
			// TRẠNG THÁI
			$status = $this->config->item($revenue->term_status,'contract_status');
			$objWorksheet->setCellValueByColumnAndRow(11, $row_number, $status);
			
			// NGÀY CHẠY DỊCH VỤ
			$contract_begin = get_term_meta_value($term_id,'contract_begin');
			$contract_end = get_term_meta_value($term_id,'contract_end');
			$contract_daterange = my_date($contract_begin,'d/m/Y').' - '.my_date($contract_end,'d/m/Y');
			$objWorksheet->setCellValueByColumnAndRow(12, $row_number, $contract_daterange);

			// NGÀY BẮT ĐẦU
			$start_service_time = get_term_meta_value($term_id,'start_service_time');
			if(!empty($start_service_time))
			{
				$objWorksheet->setCellValueByColumnAndRow(13, $row_number, PHPExcel_Shared_Date::PHPToExcel($start_service_time));
			}
			else {
				$objWorksheet->setCellValueByColumnAndRow(13, $row_number, '');
			}
			
			// NHÂN VIÊN KINH DOANH			
			$sale_id = get_term_meta_value($term_id,'staff_business');
			$staff_business = !empty($sales[$sale_id]) ? $sales[$sale_id] : '--';
			$objWorksheet->setCellValueByColumnAndRow(14, $row_number, $staff_business);
			
			// GÓI
			$service_label = $this->config->item($revenue->term_type,'services');
			$objWorksheet->setCellValueByColumnAndRow(15, $row_number, $service_label);

			// TÊN KHÁCH HÀNG
			$display_name = '';
			if($customers = $this->term_users_m->get_term_users($term_id, ['customer_person','customer_company','system']))
			{
				$customer = end($customers);
				$display_name = mb_ucwords(mb_strtolower($this->admin_m->where('user_id',$customer->user_id)->get_by()->display_name));	
			}
			$objWorksheet->setCellValueByColumnAndRow(16, $row_number, $display_name);
		}

		$num_rows = count($revenues);

		$objPHPExcel->getActiveSheet()
	        	->getStyle('C'.$row_index.':C'.($num_rows+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode("#,##0");

		$objPHPExcel->getActiveSheet()
	        	->getStyle('D'.$row_index.':D'.($num_rows+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode("#,##0");
	
		$objPHPExcel->getActiveSheet()
	        	->getStyle('F'.$row_index.':F'.($num_rows+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode("#,##0");

		$objPHPExcel->getActiveSheet()
	        	->getStyle('G'.$row_index.':G'.($num_rows+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode("#,##0");

		$objPHPExcel->getActiveSheet()
	        	->getStyle('H'.$row_index.':H'.($num_rows+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode("#,##0");     

		$objPHPExcel->getActiveSheet()
	        	->getStyle('I'.$row_index.':I'.($num_rows+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode("#,##0");      		    	

		// Set Cells style for Date
	    $objPHPExcel->getActiveSheet()
        	->getStyle('J'.$row_index.':J'.($num_rows+$row_index))
        	->getNumberFormat()
	    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

		// Set Cells style for Date
	    $objPHPExcel->getActiveSheet()
        	->getStyle('K'.$row_index.':K'.($num_rows+$row_index))
        	->getNumberFormat()
	    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

	    $objPHPExcel->getActiveSheet()
     	   	->getStyle('N'.$row_index.':N'.($num_rows+$row_index))
     	   	->getNumberFormat()
			->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
						
		// Set Cells Style For VAT
		$objPHPExcel->getActiveSheet()
		->getStyle('E'.$row_index.':E'.($num_rows+$row_index))
		->getNumberFormat()
		->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
		
		$total_contract_value 			= array_sum(array_column($revenues, 'contract_value'));
		$total_service_fee 				= array_sum(array_column($revenues, 'service_fee'));
		$total_invs_amount				= array_sum(array_column($revenues, 'invs_amount'));
		$total_invs_amount_expired		= array_sum(array_column($revenues, 'invs_amount_expired'));
		$total_payment_amount_remaining	= array_sum(array_column($revenues, 'payment_amount_remaining'));
		$total_payment_amount			= array_sum(array_column($revenues, 'payment_amount'));
		
		$objWorksheet->setCellValueByColumnAndRow(0, 1, 'THỐNG KÊ');
		$objWorksheet->setCellValueByColumnAndRow(2, 1, $total_contract_value);
		$objPHPExcel->getActiveSheet()
	        		->getStyle('C1:C1')
	        		->getNumberFormat()
	 		    	->setFormatCode("#,##0")
		    		->applyFromArray(array('font' => array('size' => 24,'bold' => true,'color' => array('rgb' => '000000'))));
		    		//->setBold();
		$objWorksheet->setCellValueByColumnAndRow(3, 1, $total_service_fee);
		$objPHPExcel->getActiveSheet()
	        		->getStyle('D1:D1')
	        		->getNumberFormat()
		    		->setFormatCode("#,##0");
		$objWorksheet->setCellValueByColumnAndRow(5, 1, $total_invs_amount);
		$objPHPExcel->getActiveSheet()
	        		->getStyle('F1:F1')
	        		->getNumberFormat()
		    		->setFormatCode("#,##0");
		$objWorksheet->setCellValueByColumnAndRow(6, 1, $total_invs_amount_expired);
		$objPHPExcel->getActiveSheet()
	        		->getStyle('G1:G1')
	        		->getNumberFormat()
		    		->setFormatCode("#,##0");
		$objWorksheet->setCellValueByColumnAndRow(7, 1, $total_payment_amount);
		$objPHPExcel->getActiveSheet()
	        		->getStyle('H1:H1')
	        		->getNumberFormat()
		    		->setFormatCode("#,##0");
		$objWorksheet->setCellValueByColumnAndRow(8, 1, $total_payment_amount_remaining);
		$objPHPExcel->getActiveSheet()
	        		->getStyle('I1:I1')
	        		->getNumberFormat()
		    		->setFormatCode("#,##0");

        // We'll be outputting an excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $folder_upload = "files/contracts/";
        if(!is_dir($folder_upload))
        {
            try 
            {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $file_name = $folder_upload . 'export-list-revenue-' . my_date(time(), 'd-m-Y') . '.xlsx';

       	try 
        {
        	$this->load->helper('download');
        	$this->load->helper('file');

			delete_files($file_name);
			
            $objWriter->save($file_name); 	
            force_download($file_name,NULL);
        }
        catch (Exception $e) 
        {
            trigger_error($e->getMessage());
            return FALSE; 
        }

	}
}
/* End of file Revenue.php */
/* Location: ./application/modules/contract/controllers/Revenue.php */