<?php defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class NonRenewal extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
		
        $this->load->model('contract_m');

        in_array($this->admin_m->role_id, [1, 26]) OR redirect(base_url('admin'));

		$this->config->load('contract/contract');
        $this->load->helper('text');
	}

    /**
     * Export dữ liệu Hợp Đồng & Khách Hàng không tái ký trong thời gian x ngày
     * kể từ ngày hiện tại
     * 1. Hợp đồng có thời gian kết thúc cho đến hiện tại > x ngày
     * 2. Hợp đồng không phát sinh hợp đồng tái ký 
     * Where: . Script
     */
    public function googleads($days = 10, $max_days = 30)
    {
        restrict('Reports.nonrenewal.Access');

        $this->load->model('googleads/googleads_m');

        $contracts = $this->googleads_m
        ->select('term.term_id, term.term_name, term.term_parent, term_status, term_type')
        ->set_term_type()
        ->where_in('term.term_status', ['ending', 'liquidation'])
        ->m_find([
            'key' => 'end_service_time',
            'compare' => 'BETWEEN',
            'value' => [ start_of_day("-{$max_days} days"), end_of_day("-{$days} days") ]
        ])->get_all();

        $relatedContracts = $this->googleads_m
        ->select('term.term_id, term.term_name, term.term_parent, term_status')
        ->set_term_type()
        ->where_not_in('term.term_id', array_unique(array_column($contracts, 'term_id')))
        ->where_in('term.term_parent', array_unique(array_column($contracts, 'term_parent')))
        ->get_all();

        $relatedContracts AND $relatedContracts = array_group_by($relatedContracts, 'term_parent');

        $contracts = array_filter($contracts, function($contract) use($relatedContracts){

            $_relatedContracts = $relatedContracts[$contract->term_parent] ?? null;
            if( ! $_relatedContracts) return true;

            $_verifiedContracts = array_map(function($y){
                $y->verified_on = (int) get_term_meta_value($y->term_id, 'verified_on');
                return $y;
            }, $_relatedContracts);

            if($contract->end_service_time > max($_verifiedContracts)) return true;

            return false;
        });

        if( ! $contracts) die('204 No Content');

        $title          = "Export dữ liệu Hợp Đồng & Khách Hàng không tái ký trong thời gian {$days} ngày kể từ ngày hiện tại";
        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('WEBDOCTOR.VN')->setLastModifiedBy('WEBDOCTOR.VN')->setTitle(uniqid("{$title} "));

        $sheet          = $spreadsheet->getActiveSheet();
        $callback       = $this->googleads_m->get_field_callback();
        $field_config   = $this->googleads_m->get_field_config();

        $columns = array(
            'id', // ID Hợp đồng 
            'website', // Website thực hiện 
            'status', // Trạng thái hợp đồng 
            'contract_code', // Mã Hợp đồng 
            'customer_code',        // Mã Khách hàng
            'representative_name', // Tên người đại diện ký hợp đồng 
            'representative_email', // Email người đại diện ký hợp đồngs 
            'representative_phone', // Số điện thoại người đại diện ký hợp đồngs 
            'contract_note', // Ghi chú hợp đồng 
            'created_on', // Ngày tạo 
            'created_by', // Người tạo 
            'verified_on', // Xác nhận cấp số vào lúc 
            'contract_begin', // Thời gian bắt đầu hợp đồng 
            'contract_end', // Thời gian kết thúc hợp đồng 
            'contract_daterange', // Thời gian hợp đồng 
            'end_service_time', // Thời gian kết thúc dịch vụ 
            'start_service_time', // Thời gian kích hoạt dịch vụ 
            'staff_business', // Nhân viên kinh doanh phụ trách 
            'staff_advertise', // Nhân viên kỹ thuật phụ trách 
            'fb_staff_advertise', // Nhân viên kỹ thuật Facebook Ads 
            'type', // Loại Hình 
            'contract_value', // Giá trị hợp đồng 
            'contract_budget', // Ngân sách quảng cáo 
            'service_fee', // Phí dịch vụ 
            'vat', // Thuế VAT
            'is_first_contract', // Ký mới | Tái ký 
            'cid'  // Customer Id
        );

        /* Set Heading Cells at A1 index */
        $sheet->fromArray(array_map(function($x) use($field_config){ return $field_config['columns'][$x]['title']??$x; }, $columns), NULL, 'A1');

        $row_index = 2;
        $contracts = array_values($contracts);

        foreach ($contracts as $key => &$contract)
        {
            $contract = (array) $contract;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;
                if(empty($callback[$column])) continue;

                $contract = call_user_func_array($callback[$column]['func'], array($contract, $column, []));
            }

            $i = 0;
            $row_number = $row_index + $key;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;

                $value = $contract["{$column}_raw"] ?? ($contract["{$column}"] ?? '');
                $i++;
                $col_number = $i;

                switch ($field_config['columns'][$column]['type'])
                {
                    case 'string' : 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
                        break;

                    case 'number': 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                $sheet->setCellValueByColumnAndRow($col_number, $row_number, $value);
            }
        }

        $folder_upload  = 'files/contracts/reports/non-renewal/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = $folder_upload . time() . '-' . url_title(convert_accented_characters(mb_strtolower($title))).'.xlsx';

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }

    /**
     * Export dữ liệu Hợp Đồng & Khách Hàng không tái ký trong thời gian x ngày
     * kể từ ngày hiện tại
     * 1. Hợp đồng có thời gian kết thúc cho đến hiện tại > x ngày
     * 2. Hợp đồng không phát sinh hợp đồng tái ký 
     * Where: . Script
     */
    public function facebookads($days = 10, $max_days = 30)
    {
        restrict('Reports.nonrenewal.Access');

        $this->load->model('facebookads/facebookads_m');

        $contracts = $this->facebookads_m
        ->select('term.term_id, term.term_name, term.term_parent, term_status, term_type')
        ->set_term_type()
        ->where_in('term.term_status', ['ending', 'liquidation'])
        ->m_find([
            'key' => 'end_service_time',
            'compare' => 'BETWEEN',
            'value' => [ start_of_day("-{$max_days} days"), end_of_day("-{$days} days") ]
        ])->get_all();

        $relatedContracts = $this->facebookads_m
        ->select('term.term_id, term.term_name, term.term_parent, term_status')
        ->set_term_type()
        ->where_not_in('term.term_id', array_unique(array_column($contracts, 'term_id')))
        ->where_in('term.term_parent', array_unique(array_column($contracts, 'term_parent')))
        ->get_all();

        $relatedContracts AND $relatedContracts = array_group_by($relatedContracts, 'term_parent');

        $contracts = array_filter($contracts, function($contract) use($relatedContracts){

            $_relatedContracts = $relatedContracts[$contract->term_parent] ?? null;
            if( ! $_relatedContracts) return true;

            $_verifiedContracts = array_map(function($y){
                $y->verified_on = (int) get_term_meta_value($y->term_id, 'verified_on');
                return $y;
            }, $_relatedContracts);

            if($contract->end_service_time > max($_verifiedContracts)) return true;

            return false;
        });

        if( ! $contracts) die('204 No Content');

        $title          = "Export dữ liệu Hợp Đồng & Khách Hàng không tái ký trong thời gian {$days} ngày kể từ ngày hiện tại";
        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('WEBDOCTOR.VN')->setLastModifiedBy('WEBDOCTOR.VN')->setTitle(uniqid("{$title} "));

        $sheet          = $spreadsheet->getActiveSheet();
        $callback       = $this->facebookads_m->get_field_callback();
        $field_config   = $this->facebookads_m->get_field_config();

        $columns = array(
            'id', // ID Hợp đồng 
            'website', // Website thực hiện 
            'status', // Trạng thái hợp đồng 
            'contract_code', // Mã Hợp đồng 
            'customer_code',        // Mã Khách hàng
            'representative_name', // Tên người đại diện ký hợp đồng 
            'representative_email', // Email người đại diện ký hợp đồngs 
            'representative_phone', // Số điện thoại người đại diện ký hợp đồngs 
            'contract_note', // Ghi chú hợp đồng 
            'created_on', // Ngày tạo 
            'created_by', // Người tạo 
            'verified_on', // Xác nhận cấp số vào lúc 
            'contract_begin', // Thời gian bắt đầu hợp đồng 
            'contract_end', // Thời gian kết thúc hợp đồng 
            'contract_daterange', // Thời gian hợp đồng 
            'end_service_time', // Thời gian kết thúc dịch vụ 
            'start_service_time', // Thời gian kích hoạt dịch vụ 
            'staff_business', // Nhân viên kinh doanh phụ trách 
            'staff_advertise', // Nhân viên kỹ thuật phụ trách 
            'fb_staff_advertise', // Nhân viên kỹ thuật Facebook Ads 
            'type', // Loại Hình 
            'contract_value', // Giá trị hợp đồng 
            'contract_budget', // Ngân sách quảng cáo 
            'service_fee', // Phí dịch vụ 
            'vat', // Thuế VAT
            'is_first_contract', // Ký mới | Tái ký 
        );

        /* Set Heading Cells at A1 index */
        $sheet->fromArray(array_map(function($x) use($field_config){ return $field_config['columns'][$x]['title']??$x; }, $columns), NULL, 'A1');

        $row_index = 2;
        $contracts = array_values($contracts);

        foreach ($contracts as $key => &$contract)
        {
            $contract = (array) $contract;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;
                if(empty($callback[$column])) continue;

                $contract = call_user_func_array($callback[$column]['func'], array($contract, $column, []));
            }

            $i = 0;
            $row_number = $row_index + $key;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;

                $value = $contract["{$column}_raw"] ?? ($contract["{$column}"] ?? '');
                $i++;
                $col_number = $i;

                switch ($field_config['columns'][$column]['type'])
                {
                    case 'string' : 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
                        break;

                    case 'number': 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                $sheet->setCellValueByColumnAndRow($col_number, $row_number, $value);
            }
        }

        $folder_upload  = 'files/contracts/reports/non-renewal/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = $folder_upload . time() . '-' . url_title(convert_accented_characters(mb_strtolower($title))).'.xlsx';

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file NonRenewal.php */
/* Location: ./application/modules/contract/controllers/Reports/NonRenewal.php */