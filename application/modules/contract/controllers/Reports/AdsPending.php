<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class AdsPending extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
		
        $this->load->model('contract/contract_m');

        in_array($this->admin_m->role_id, [1, 26]) OR redirect(base_url('admin'));

		$this->config->load('contract/contract');
        $this->load->helper('text');
	}

    /**
     * { function_description }
     *
     * @param      integer  $day    The day
     * @param      string   $type   The type
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function googleads($day = 3, $deviation = 10)
    {
        restrict('Reports.adspending.Access');

        $day = abs((int) $day);
        $deviation = abs((int) $deviation);
        $this->load->model('googleads/googleads_m');

        $end_date = end_of_day(strtotime("-{$day} days", time()));
        $start_date = start_of_day(strtotime("-{$deviation} days", $end_date));

        $contracts = $this->googleads_m->set_term_type()
        ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
        ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'LEFT')  
        ->join('term_posts AS tp_segment_adaccount', 'tp_segment_adaccount.post_id = ads_segment.post_id', 'LEFT')  
        ->join('term AS adaccount', 'tp_segment_adaccount.term_id = adaccount.term_id AND adaccount.term_type = "mcm_account"', 'LEFT')  
        ->join('termmeta AS adaccount_metadata', 'adaccount_metadata.term_id = adaccount.term_id AND meta_key = "source"', 'LEFT')  
        ->join('term_posts AS tp_adaccount_insights', 'tp_adaccount_insights.term_id = adaccount.term_id', 'LEFT')  
        ->join('posts AS insights', '
            tp_adaccount_insights.post_id = insights.post_id 
            AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) 
            AND insights.start_date <= if(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) 
            AND insights.post_type = "insight_segment" 
            AND insights.post_name = "day"', 
            'LEFT'
        )

        ->select('
            term.term_id,
            term.term_name,
            term.term_parent,
            term.term_status,
            term.term_type,
            MAX(insights.start_date) AS latest_spend_date,
        ')

        ->group_by('term.term_id')
        ->where_in('term.term_status', ['publish', 'pending'])
        ->having('latest_spend_date >=', $start_date)
        ->having('latest_spend_date <=', $end_date)
        ->get_all();
        $this->load->model('customer/website_m');

        if( ! $contracts) die('204 No Content');

        $title  = sprintf(
            "Export hợp đồng Google Ads tạm ngưng %s ngày từ ngày %s đến ngày %s",
            $day,
            my_date($start_date, 'd-m-Y'),
            my_date($end_date, 'd-m-Y')
        );

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('WEBDOCTOR.VN')->setLastModifiedBy('WEBDOCTOR.VN')->setTitle(uniqid("{$title} "));

        $sheet          = $spreadsheet->getActiveSheet();

        $callback       = $this->googleads_m->get_field_callback();
        $field_config   = $this->googleads_m->get_field_config();

        $columns    = array(
            'id',                   // ID Hợp đồng 
            'website',              // Website thực hiện 
            'status',               // Trạng thái hợp đồng 
            'latest_spend_date',   // Ngày ghi nhận spend mới nhất
            'latest_spend_days',   // Số ngày không phát sinh từ ngày mới nhất
            'contract_code',        // Mã Hợp đồng 
            'customer_code',        // Mã Khách hàng
            'representative_name',  // Tên người đại diện ký hợp đồng 
            'representative_email', // Email người đại diện ký hợp đồngs 
            'representative_phone', // Số điện thoại người đại diện ký hợp đồngs 
            'contract_note',        // Ghi chú hợp đồng 
            'created_on',           // Ngày tạo 
            'created_by',           // Người tạo 
            'verified_on',           // Xác nhận cấp số vào lúc 
            'contract_begin',       // Thời gian bắt đầu hợp đồng 
            'contract_end',         // Thời gian kết thúc hợp đồng 
            'contract_daterange',   // Thời gian hợp đồng 
            'end_service_time',     // Thời gian kết thúc dịch vụ 
            'start_service_time',   // Thời gian kích hoạt dịch vụ 
            'staff_business',       // Nhân viên kinh doanh phụ trách 
            'staff_advertise',      // Nhân viên kỹ thuật phụ trách 
            'fb_staff_advertise',   // Nhân viên kỹ thuật Facebook Ads 
            'type',                 // Loại Hình 
            'contract_value',       // Giá trị hợp đồng 
            'contract_budget',      // Ngân sách quảng cáo 
            'service_fee',          // Phí dịch vụ 
            'vat',                  // Thuế VAT
            'is_first_contract',     // Ký mới | Tái ký 
            'cid'                   // Customer Id
        );

        /* Set Heading Cells at A1 index */
        $sheet->fromArray(array_map(function($x) use($field_config){ return $field_config['columns'][$x]['title']??$x; }, $columns), NULL, 'A1');

        $row_index = 2;
        $contracts = array_values($contracts);

        foreach ($contracts as $key => &$contract)
        {
            $contract = (array) $contract;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;
                if(empty($callback[$column])) continue;

                $contract = call_user_func_array($callback[$column]['func'], array($contract, $column, []));
            }

            $i = 0;
            $row_number = $row_index + $key;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;

                $value = $contract["{$column}_raw"] ?? ($contract["{$column}"] ?? '');
                $i++;
                $col_number = $i;

                switch ($field_config['columns'][$column]['type'])
                {
                    case 'string' : 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
                        break;

                    case 'number': 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                @$sheet->setCellValueByColumnAndRow($col_number, $row_number, $value);
            }
        }

        $folder_upload  = 'files/contracts/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = $folder_upload . time() . '-' . url_title(convert_accented_characters(mb_strtolower($title))).'.xlsx';

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }


     /**
     * { function_description }
     *
     * @param      integer  $day    The day
     * @param      string   $type   The type
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function facebookads($day = 3, $deviation = 10)
    {
        restrict('Reports.adspending.Access');

        $day = abs((int) $day);
        $deviation = abs((int) $deviation);
        $this->load->model('facebookads/facebookads_m');

        $end_date = end_of_day(strtotime("-{$day} days", time()));
        $start_date = start_of_day(strtotime("-{$deviation} days", $end_date));

        $contracts = $this->facebookads_m->set_term_type()
        ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
        ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'LEFT')  
        ->join('term_posts AS tp_segment_adaccount', 'tp_segment_adaccount.post_id = ads_segment.post_id', 'LEFT')  
        ->join('term AS adaccount', 'tp_segment_adaccount.term_id = adaccount.term_id AND adaccount.term_type = "adaccount"', 'LEFT')  
        ->join('termmeta AS adaccount_metadata', 'adaccount_metadata.term_id = adaccount.term_id AND meta_key = "source"', 'LEFT')  
        ->join('term_posts AS tp_adaccount_insights', 'tp_adaccount_insights.term_id = adaccount.term_id', 'LEFT')  
        ->join('posts AS insights', '
            tp_adaccount_insights.post_id = insights.post_id 
            AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) 
            AND insights.start_date <= if(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) 
            AND insights.post_type = "insight_segment" 
            AND insights.post_name = "day"', 
            'LEFT'
        )

        ->select('
            term.term_id,
            term.term_name,
            term.term_parent,
            term.term_status,
            term.term_type,
            MAX(insights.start_date) AS latest_spend_date,
        ')

        ->group_by('term.term_id')
        ->where_in('term.term_status', ['publish', 'pending'])
        ->having('latest_spend_date >=', $start_date)
        ->having('latest_spend_date <=', $end_date)
        ->get_all();

        $this->load->model('customer/website_m');

        if( ! $contracts) die('204 No Content');

        $title  = sprintf(
            "Export hợp đồng Facebook Ads tạm ngưng %s ngày từ ngày %s đến ngày %s",
            $day,
            my_date($start_date, 'd-m-Y'),
            my_date($end_date, 'd-m-Y')
        );

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('WEBDOCTOR.VN')->setLastModifiedBy('WEBDOCTOR.VN')->setTitle(uniqid("{$title} "));

        $sheet          = $spreadsheet->getActiveSheet();

        $callback       = $this->facebookads_m->get_field_callback();
        $field_config   = $this->facebookads_m->get_field_config();

        $columns    = array(
            'id',                   // ID Hợp đồng 
            'website',              // Website thực hiện 
            'status',               // Trạng thái hợp đồng 
            'latest_spend_date',   // Ngày ghi nhận spend mới nhất
            'latest_spend_days',   // Số ngày không phát sinh từ ngày mới nhất
            'contract_code',        // Mã Hợp đồng 
            'customer_code',        // Mã Khách hàng
            'representative_name',  // Tên người đại diện ký hợp đồng 
            'representative_email', // Email người đại diện ký hợp đồngs 
            'representative_phone', // Số điện thoại người đại diện ký hợp đồngs 
            'contract_note',        // Ghi chú hợp đồng 
            'created_on',           // Ngày tạo 
            'created_by',           // Người tạo 
            'verified_on',           // Xác nhận cấp số vào lúc 
            'contract_begin',       // Thời gian bắt đầu hợp đồng 
            'contract_end',         // Thời gian kết thúc hợp đồng 
            'contract_daterange',   // Thời gian hợp đồng 
            'end_service_time',     // Thời gian kết thúc dịch vụ 
            'start_service_time',   // Thời gian kích hoạt dịch vụ 
            'staff_business',       // Nhân viên kinh doanh phụ trách 
            'staff_advertise',      // Nhân viên kỹ thuật phụ trách 
            'fb_staff_advertise',   // Nhân viên kỹ thuật Facebook Ads 
            'type',                 // Loại Hình 
            'contract_value',       // Giá trị hợp đồng 
            'contract_budget',      // Ngân sách quảng cáo 
            'service_fee',          // Phí dịch vụ 
            'vat',                  // Thuế VAT
            'is_first_contract',     // Ký mới | Tái ký 
            'cid'                   // Customer Id
        );
        
        /* Set Heading Cells at A1 index */
        $sheet->fromArray(array_map(function($x) use($field_config){ return $field_config['columns'][$x]['title']??$x; }, $columns), NULL, 'A1');

        $row_index = 2;
        $contracts = array_values($contracts);

        foreach ($contracts as $key => &$contract)
        {
            $contract = (array) $contract;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;
                if(empty($callback[$column])) continue;

                $contract = call_user_func_array($callback[$column]['func'], array($contract, $column, []));
            }

            $i = 0;
            $row_number = $row_index + $key;
            foreach ($columns as $column)
            {
                if(empty($field_config['columns'][$column])) continue;

                $value = $contract["{$column}_raw"] ?? ($contract["{$column}"] ?? '');
                $i++;
                $col_number = $i;

                switch ($field_config['columns'][$column]['type'])
                {
                    case 'string' : 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
                        break;

                    case 'number': 
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                @$sheet->setCellValueByColumnAndRow($col_number, $row_number, $value);
            }
        }



        $folder_upload  = 'files/contracts/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = $folder_upload . time() . '-' . url_title(convert_accented_characters(mb_strtolower($title))).'.xlsx';

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file AdsPending.php */
/* Location: ./application/modules/contract/controllers/Reports/AdsPending.php */