<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receipt extends REST_Controller
{
	function __construct()
	{
		parent::__construct('contract/rest');

		$model_names = array(
			'usermeta_m',
			'permission_m',
			'term_m',
			'termmeta_m',
			'staffs/admin_m',
			'contract/contract_m',
			'webbuild/webbuild_m',
			'term_users_m');

		$this->load->model($model_names);
	}

	public function index_get()
	{
		$default 	= array('id' => '', 'meta' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if( empty($args['id'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		$data 			= array('msg' => 'Xử lý không thành công', 'data' => []);
		$model_names 	= array('term_m','termmeta_m', 'staffs/admin_m', 'contract/contract_m', 'webbuild/webbuild_m', 'term_users_m');
		$this->load->model($model_names);
		if(FALSE === $this->contract_m->has_permission($args['id'], 'admin.contract.view'))
		{
			$data['msg'] = 'Không có quyền truy xuất hoặc hợp đồng không khả dụng !';
			parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
		}

		$data = $this->contract_m->set_term_type()->as_array()->get($args['id']);
		$data['id'] = $args['id'];

		$args['meta'] = is_string($args['meta']) ? explode(',', $args['meta']) : $args['meta'];

		if(in_array('customer', $args['meta']))
		{
			$customers = $this->term_users_m->get_the_users($args['id'],'customer_person,customer_company',['fields'=>'user.user_id,user.display_name']);
			$data['customer'] = $customers ? reset($customers) : '';
			$key = array_search('customer', $args['meta']);
			unset($args['meta'][$key]);
		}

		if( ! empty($args['meta']))
		{
			foreach ($args['meta'] as $key => $meta)
			{
				$_value 		= get_term_meta_value($args['id'], $meta);

				if (is_serialized($_value)) $_value = unserialize($_value);
				elseif (is_numeric($_value)) $_value = (double) $_value;

				$data[$meta] = $_value;
			}
		}
		$response['data'] = $data;
		parent::response($response);
	}

	public function update_post()
	{
		$args = wp_parse_args(parent::post(NULL, TRUE), ['id' => 0]);
		if( empty($args['pk']) || empty($args['name'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		if(FALSE === $this->contract_m->has_permission($args['pk'], 'admin.contract.update'))
		{
			parent::response('Không thể thực hiện tác vụ, quyền truy cập bị hạn chế', parent::HTTP_NOT_ACCEPTABLE);
		}

		$edit = array();
		$metadata = array();

		switch ($args['type']) {
			case 'meta':
				$metadata[$args['name']] = $args['value'];
				break;
			case 'field' : 
				$edit[$args['name']] = $args['value'];
				break;
		}

		$messages = ['Cập nhật dữ liệu thành công'];

		switch ($args['name'])
		{
			case 'fct':

				try
				{
					if($args['value'] > 5 || $args['value'] < 0) throw new Exception('Phí nhà thầu vượt quá phạm vi 0~5%');

					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified
					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'service_fee_payment_type':

				try
				{
					$this->load->config('googleads/contract');
					if( ! $this->config->item($args['value'], 'service_fee_payment_type')) throw new Exception('Giá trị không hợp lệ.');

					if($args['value'])
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified
					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;
			case 'term_parent':
				
				if( ! in_array($this->admin_m->role_id, array(1,5,24)))	parent::response('Không có quyền cập nhật giá trị này. [1]', parent::HTTP_NOT_ACCEPTABLE);

				break;

			case 'staff_business':
				
				if( ! has_permission('admin.contract.manage')) parent::response('Không có quyền cập nhật giá trị này. [0]', parent::HTTP_NOT_ACCEPTABLE);
				break;

			case 'number_of_payments':

				if( empty($args['value'])) parent::response('Số đợt thanh toán phải >=1', parent::HTTP_NOT_ACCEPTABLE);

				update_term_meta($args['pk'], $args['name'], $args['value']);
				unset($metadata[$args['name']]);

				$this->contract_m->set_contract($args['pk']);
				$this->contract_m->delete_all_invoices(); //  Delete all invoices available
				
				$this->contract_m->create_invoices(); // Create new contract's invoices
				$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

				$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
				$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
				break;

			case 'contract_price_weboptimize':
				
				if( empty($args['value'])) parent::response('Giá trị dịch vụ nhập vào không hợp lệ. [0]', parent::HTTP_NOT_ACCEPTABLE);

				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']);
					$contract_value = $this->contract_m->calc_contract_value();

					if(empty($contract_value)) parent::response('Giá trị dịch vụ nhập vào không hợp lệ [1].', parent::HTTP_NOT_ACCEPTABLE);

					update_term_meta($args['pk'], 'contract_value', $contract_value); // Update Contract value

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'contract_budget':
				
				if( empty($args['value'])) parent::response('Giá trị dịch vụ nhập vào không hợp lệ. [0]', parent::HTTP_NOT_ACCEPTABLE);

				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']);
					$contract_value = $this->contract_m->calc_contract_value();

					if(empty($contract_value)) parent::response('Giá trị dịch vụ nhập vào không hợp lệ [1].', parent::HTTP_NOT_ACCEPTABLE);

					update_term_meta($args['pk'], 'contract_value', $contract_value); // Update Contract value

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'service_fee':
				
				if( empty($args['value'])) parent::response('Giá trị dịch vụ nhập vào không hợp lệ. [0]', parent::HTTP_NOT_ACCEPTABLE);

				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']);
					$contract_value = $this->contract_m->calc_contract_value();

					if(empty($contract_value)) parent::response('Giá trị dịch vụ nhập vào không hợp lệ [1].', parent::HTTP_NOT_ACCEPTABLE);

					update_term_meta($args['pk'], 'contract_value', $contract_value); // Update Contract value

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'vat':

				$contract = $this->contract_m->set_term_type()->get($args['pk']);
				if($contract->term_type == 'webdesign' && ! empty($args['value']))
				{
					parent::response('Hợp đồng Thiết kế web thì thuế VAT = 0', parent::HTTP_NOT_ACCEPTABLE);
				}

				$extra = get_term_meta_value($args['pk'], 'extra');
				$extra = is_serialized($extra) ? unserialize($extra) : $extra;
				if(empty($extra['customer_tax'])) parent::response('Hợp đồng có thuế VAT thì Mã Số Thuế là bắt buộc.', parent::HTTP_NOT_ACCEPTABLE);

				break;

			case 'customer_tax':

				$vat = (int) get_term_meta_value($args['pk'], 'vat');
				if($vat > 0 && empty($args['value'])) parent::response('Hợp đồng có thuế VAT thì Mã Số Thuế là bắt buộc.', parent::HTTP_NOT_ACCEPTABLE);

                $isValidTin = tin_validator($args['value']);
                if(!$isValidTin) parent::response('Mã số thuế không đúng định dạng.', parent::HTTP_NOT_ACCEPTABLE);

				$extra = get_term_meta_value($args['pk'], 'extra');
				$extra = is_serialized($extra) ? unserialize($extra) : [];
				$extra['customer_tax'] = $args['value'];

				unset($metadata['customer_tax']);
				$metadata['extra'] = serialize($extra);

				break;

			case 'bank_id':
				$extra = get_term_meta_value($args['pk'], 'extra');
				$extra = is_serialized($extra) ? unserialize($extra) : [];
				$extra['bank_id'] = $args['value'];

				unset($metadata['bank_id']);
				$metadata['extra'] = serialize($extra);

				break;

			case 'bank_owner':
				$extra = get_term_meta_value($args['pk'], 'extra');
				$extra = is_serialized($extra) ? unserialize($extra) : [];
				$extra['bank_owner'] = $args['value'];

				unset($metadata['bank_owner']);
				$metadata['extra'] = serialize($extra);

				break;

			case 'bank_name':
				$extra = get_term_meta_value($args['pk'], 'extra');
				$extra = is_serialized($extra) ? unserialize($extra) : [];
				$extra['bank_name'] = $args['value'];

				unset($metadata['bank_name']);
				$metadata['extra'] = serialize($extra);

				break;

			case 'contract_begin': 

				$has_service_started = get_term_meta_value($args['pk'], 'started_service', 1);
				if($has_service_started && !in_array($this->admin_m->role_id, array(1,5,25)))
				{
					parent::response('Không thể cập nhật vì HĐ đã được thực hiện.', parent::HTTP_NOT_ACCEPTABLE);
				}

				$metadata[$args['name']] = strtotime($args['value']);

				$contract_end = (int) get_term_meta_value($args['pk'], 'contract_end');
				if($metadata[$args['name']] >= $contract_end)
				{
					parent::response('Ngày bắt đầu hợp đồng không thể lớn hơn ngày kết thúc hợp đồng.', parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'contract_end': 

				$has_service_started = get_term_meta_value($args['pk'], 'started_service', 1);
				if($has_service_started && !in_array($this->admin_m->role_id, array(1,5,25)))
				{
					parent::response('Không thể cập nhật vì HĐ đã được thực hiện.', parent::HTTP_NOT_ACCEPTABLE);
				}

				$metadata[$args['name']] = strtotime($args['value']); 

				$contract_begin = (int) get_term_meta_value($args['pk'], 'contract_begin');
				if($metadata[$args['name']] <= $contract_begin)
				{
					parent::response('Ngày bắt đầu hợp đồng không thể lớn hơn ngày kết thúc hợp đồng.', parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'googleads-end_time': $metadata[$args['name']] = strtotime($args['value']); break;
			case 'googleads-begin_time': $metadata[$args['name']] = strtotime($args['value']); break;
			case 'started_service': $metadata[$args['name']] = strtotime($args['value']); break;
			case 'start_service_time': $metadata[$args['name']] = strtotime($args['value']); break;
			case 'end_service_time': $metadata[$args['name']] = strtotime($args['value']); break;
				
			default:
				# code...
				break;
		}

		if( ! empty($edit)) $this->contract_m->update($args['pk'], $edit);
		if( ! empty($metadata)) 
		{
			foreach ($metadata as $key => $value)
			{
				update_term_meta($args['pk'], $key, $value);
			}
		}

		parent::response(['msg'=>$messages]);
	}

	/**
	 * Sync all invoices & all amount data from 
	 */
	public function sync_invoices_post()
	{
		$args 		= wp_parse_args(parent::post(NULL, TRUE), ['id' => 0]);
		if( empty($args['id'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		if(FALSE === $this->contract_m->has_permission($args['id'], 'admin.contract.view'))
		{
			parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
		}

		$messages = array();
		$contract = $this->contract_m->set_contract($args['id']);
		try
		{
			$this->contract_m->delete_all_invoices(); //  Delete all invoices available

			$this->contract_m->create_invoices(); // Create new contract's invoices
			$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

			$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
			$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
		}
		catch (Exception $e)
		{	
			parent::response($e->getMessage(), parent::HTTP_NO_CONTENT);
		}

		parent::response(['msg' => $messages]);
	}
}
/* End of file Receipt.php */
/* Location: ./application/modules/contract/controllers/api/Receipt.php */