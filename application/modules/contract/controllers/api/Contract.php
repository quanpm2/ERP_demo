<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Crawler\TaxCode\TaxCode;
use VerifyEmail\VerifyEmail;
use VerifyEmail\Enums\Status;

class Contract extends REST_Controller
{
	function __construct()
	{
		parent::__construct('contract/rest');

		$model_names = array(
			'usermeta_m',
			'permission_m',
			'term_m',
			'termmeta_m',
			'staffs/admin_m',
			'contract/contract_m',
			'webbuild/webbuild_m',
			'term_users_m',
			'option_m',
            'customer/customer_m'
			);

		$this->load->model($model_names);
	}

	public function index_get()
	{
		$default 	= array('id' => '', 'meta' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if( empty($args['id'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		$data 			= array('msg' => 'Xử lý không thành công', 'data' => []);
		$model_names 	= array('term_m','termmeta_m', 'staffs/admin_m', 'contract/contract_m', 'webbuild/webbuild_m', 'term_users_m');
		$this->load->model($model_names);
		if(FALSE === $this->contract_m->has_permission($args['id'], 'admin.contract.view'))
		{
			$data['msg'] = 'Không có quyền truy xuất hoặc hợp đồng không khả dụng !';
			parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
		}

		$data = $this->contract_m->set_term_type()->as_array()->get($args['id']);
		$data['id'] = $args['id'];

		$args['meta'] = is_string($args['meta']) ? explode(',', $args['meta']) : $args['meta'];

		if(in_array('customer', $args['meta']))
		{
			$customers = $this->term_users_m->get_the_users($args['id'],'customer_person,customer_company',['fields'=>'user.user_id,user.display_name']);
			$data['customer'] = $customers ? reset($customers) : '';
			$key = array_search('customer', $args['meta']);
			unset($args['meta'][$key]);
		}

		if( ! empty($args['meta']))
		{
			foreach ($args['meta'] as $key => $meta)
			{
				$_value 		= get_term_meta_value($args['id'], $meta);

				if (is_serialized($_value)) $_value = unserialize($_value);
				elseif (is_numeric($_value)) $_value = (double) $_value;

				$data[$meta] = $_value;
			}
		}
		$response['data'] = $data;
		parent::response($response);
	}

	public function update_post()
	{
		$args = wp_parse_args(parent::post(NULL, TRUE), ['id' => 0]);
		if( empty($args['pk']) || empty($args['name'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		if(FALSE === $this->contract_m->has_permission($args['pk'], 'admin.contract.update'))
		{
			parent::response('Không thể thực hiện tác vụ, quyền truy cập bị hạn chế', parent::HTTP_NOT_ACCEPTABLE);
		}

		$edit = array();
		$metadata = array();

		switch ($args['type']) {
			case 'meta':
				$metadata[$args['name']] = $args['value'] ?? '';
				break;
			case 'field' : 
				$edit[$args['name']] = $args['value'];
				break;
		}

		$messages = ['Cập nhật dữ liệu thành công'];

        $manipulation_lock_fields = [
            'vat',
            'isAccountForRent',
            'contract_budget_payment_type',
            'contract_budget_customer_payment_type',
            'service_fee_payment_type',
            'deposit_amount',
            'contract_budget',
            'original_service_fee_rate',
            'service_fee',
            'service_fee_plan',
            'fct',
            'price',
            'quantity',
            'discount_amount',
            'service_package',
            'hosting_service_package',
            'hosting_months',
            'domain_service_package',
            'domain_number_year_register',
            'domain_fee_maintain',
            'domain_fee_initialization_maintain',
            'domain_fee_maintain_years'
        ];
        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                  && FALSE == (bool)get_term_meta_value($args['pk'], 'is_manipulation_locked');
        $started_service = (int) get_term_meta_value($args['pk'], 'started_service');
        if( ! empty($started_service)
            && $is_manipulation_locked
            && in_array($args['name'], $manipulation_lock_fields))
        {
            $started_service = end_of_day($started_service);
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
            if($manipulation_locked_at > $started_service)
            {
                $manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                return parent::response("Hợp đồng đã khoá thao tác lúc {$manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá.", parent::HTTP_NOT_ACCEPTABLE);
            }
        }

		switch ($args['name'])
		{
			case 'term_parent':
				$this->load->model('customer/website_m');
				$website_m = $this->website_m->set_term_type()->get($args['value']);
				if( ! $website_m) parent::response('Model is allowed', parent::HTTP_NOT_ACCEPTABLE);

				$edit['term_name'] = $website_m->term_name;
				break;

			/* Khách tự thanh toán || ADSPLUS thu & chi hộ */
			case 'contract_budget_customer_payment_type':
				try
				{
					if('customer' != get_term_meta_value($args['pk'], 'contract_budget_payment_type')) throw new Exception('Chỉ có Hợp Đồng khách tự thanh toán mới cập nhật được');

					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					/* Init & dectect contract specified */
					$this->contract_m->set_contract($args['pk']);

					update_term_meta($args['pk'], 'typeOfService', $this->contract_m->getTypeOfService());

					$this->contract_m->update_budget_metrics();

					$contract_value = $this->contract_m->calc_contract_value();
					if(empty($contract_value)) parent::response('Giá trị dịch vụ nhập vào không hợp lệ [1].', parent::HTTP_NOT_ACCEPTABLE);
					update_term_meta($args['pk'], 'contract_value', $contract_value); // Update Contract value

					update_term_meta($args['pk'], 'service_fee_rate_actual', $this->contract_m->calc_service_fee_rate_actual());

					/* Delete all invoices available */ 
					$this->contract_m->delete_all_invoices(); 

					/* Create new contract's invoices */ 
					$this->contract_m->create_invoices(); 
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					/* Update all invoices's amount & receipt's amount */ 
					$this->contract_m->sync_all_amount(); 
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;

			/* Khách tự thanh toán || ADSPLUS thu & chi hộ */
			case 'contract_budget_payment_type':
				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);

					if( 'normal' == $args['value']) update_term_meta($args['pk'], 'contract_budget_customer_payment_type', null);
					else update_term_meta($args['pk'], 'contract_budget_customer_payment_type', 'normal');
					
					unset($metadata[$args['name']]);
					
					/* Init & dectect contract specified */
					$this->contract_m->set_contract($args['pk']);

					update_term_meta($args['pk'], 'typeOfService', $this->contract_m->getTypeOfService());

					/* Cập nhật lại giá trị hợp đồng */
					$contract_value = $this->contract_m->calc_contract_value();
					update_term_meta($args['pk'], 'contract_value', $contract_value);

					update_term_meta($args['pk'], 'service_fee_rate_actual', $this->contract_m->calc_service_fee_rate_actual());

					/* Delete all invoices available */ 
					$this->contract_m->delete_all_invoices(); 

					/* Create new contract's invoices */ 
					$this->contract_m->create_invoices(); 
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					/* Update all invoices's amount & receipt's amount */ 
					$this->contract_m->sync_all_amount(); 
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;

			case 'hosting_service_package':
				try
				{
					$this->config->load('hosting/hosting');
					$service_packages = $this->config->item('service','packages');

					if(empty($service_packages[$args['value']])) parent::response('Gói Hosting không hợp lệ.', parent::HTTP_NOT_ACCEPTABLE);

					$service_packages[$args['value']]['month'] = (int) get_term_meta_value($args['pk'], 'hosting_months');
					update_term_meta($args['pk'], 'buy2get1', ( ! empty($service_packages[$args['value']]['buy2get1'])));
					update_term_meta($args['pk'], 'hosting_price', ($service_packages[$args['value']]['price']?:0));
					update_term_meta($args['pk'], 'hosting_service_package', $args['value']);
                    if('hosting_custom' != $args['value']){
                        update_term_meta($args['pk'], 'service_package', serialize($service_packages[$args['value']]));
                    }

					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified
					$contract_value = $this->contract_m->calc_contract_value();
					update_term_meta($args['pk'], 'contract_value', $contract_value);

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;

			case 'hosting_months':
				try
				{
					$service_package = get_term_meta_value($args['pk'], 'service_package');
					$service_package = is_serialized($service_package) ? unserialize($service_package) : [];
					$service_package['month'] = (int) $args['value'];

					update_term_meta($args['pk'], 'service_package', serialize($service_package));
					update_term_meta($args['pk'], $args['name'], $args['value']);
					
					$contract_begin = (int) get_term_meta_value($args['pk'], 'contract_begin');
					update_term_meta($args['pk'], 'contract_end', strtotime("+{$args['value']} months", $contract_begin));

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified
					$contract_value = $this->contract_m->calc_contract_value();
					update_term_meta($args['pk'], 'contract_value', $contract_value);

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;

			case 'domain_service_package':
				try
				{
					$this->config->load('domain/domain');
					$domain_package_default = $this->config->item('service','packages');
					$selected_package = $domain_package_default[$args['value']] ?? FALSE;

					update_term_meta($args['pk'], 'domain_fee_initialization',($selected_package['fee_initialization'] ?? 0));
					update_term_meta($args['pk'], 'domain_fee_maintain',($selected_package['fee_maintain'] ?? 0));
					update_term_meta($args['pk'], 'domain_fee_initialization_maintain',($selected_package['fee_maintain'] + $selected_package['fee_initialization'] ?? 0));

					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified

					$contract_value = $this->contract_m->calc_contract_value();
					update_term_meta($args['pk'], 'contract_value', $contract_value);

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;

			case 'domain_fee_initialization':
				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified

					$contract_value = $this->contract_m->calc_contract_value();
					update_term_meta($args['pk'], 'contract_value', $contract_value);

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;

			case 'domain_fee_maintain':
				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified

					$contract_value = $this->contract_m->calc_contract_value();
					update_term_meta($args['pk'], 'contract_value', $contract_value);

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;

			case 'domain_fee_initialization_maintain':
				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified

					$contract_value = $this->contract_m->calc_contract_value();
					update_term_meta($args['pk'], 'contract_value', $contract_value);

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;

			case 'domain_number_year_register':
				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					// THỜI GIAN BẮT ĐẦU HỢP ĐỒNG
				    $contract_begin             = get_term_meta_value($args['pk'], 'contract_begin');
				    $contract_end = strtotime("+ {$args['value']} year", $contract_begin) ;
				    update_term_meta($args['pk'], 'contract_end', $contract_end); // CẬP NHẬT THỜI GIAN KẾT THÚC

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified

					$contract_value = $this->contract_m->calc_contract_value();
					update_term_meta($args['pk'], 'contract_value', $contract_value);

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;

			case 'fct':

				try
				{
					if($args['value'] > 5 || $args['value'] < 0) throw new Exception('Phí nhà thầu vượt quá phạm vi 0~5%');

					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified
					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'isAccountForRent':

				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified

					update_term_meta($args['pk'], 'typeOfService', $this->contract_m->getTypeOfService());

					$this->contract_m->update_budget_metrics();
					$contract_value = $this->contract_m->calc_contract_value();
					if(empty($contract_value)) parent::response('Giá trị dịch vụ nhập vào không hợp lệ [1].', parent::HTTP_NOT_ACCEPTABLE);

					update_term_meta($args['pk'], 'contract_value', $contract_value); // Update Contract value
					update_term_meta($args['pk'], 'service_fee_rate_actual', $this->contract_m->calc_service_fee_rate_actual());

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
					
					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';

					
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'service_fee_payment_type':

				try
				{
					$this->load->config('googleads/contract');
					if( ! $this->config->item($args['value'], 'service_fee_payment_type')) throw new Exception('Giá trị không hợp lệ.');

					if($args['value'])
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified
					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;

				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'service_package':
				try
				{
					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified
					$contract = $this->contract_m->get_contract();
					if( !in_array($contract->term_type, ['onead', 'hosting'])) throw new Exception("ERROR UNKNOWN");

                    switch($contract->term_type){
                        case 'hosting':
                            foreach(['price', 'bandwidth', 'disk'] as $field){
                                if(empty($args['value'][$field])){
                                    throw new \Exception('Dữ liệu không hợp lệ !!!');
                                }
                            }
                            update_term_meta($args['pk'], 'service_package', '' . serialize($args['value']));
                            unset($metadata[$args['name']]);
                            break;
                        case 'onead':
                            update_term_meta($args['pk'], $args['name'], $args['value']);
                            unset($metadata[$args['name']]);
        
                            $this->load->config('onead/onead');
                            $service_packages = $this->config->item('service', 'packages');
                            empty($service_packages[$args['value']]['items']) OR update_term_meta($args['pk'], 'service_package_items', serialize($service_packages[$args['value']]['items']));
        
                            $price = $service_packages[$args['value']]['price'] ?? (int) get_term_meta_value($args['pk'], $args['name']);
                            $discount_amount = $service_packages[$args['value']]['discount_amount_default'] ?? (int) get_term_meta_value($args['pk'], 'discount_amount');
        
                            update_term_meta($args['pk'], 'price', $price);
                            update_term_meta($args['pk'], 'discount_amount', (int) $discount_amount);
                            update_term_meta($args['pk'], 'contract_value', $this->contract_m->calc_contract_value());
                            
                            $this->contract_m->delete_all_invoices(); //  Delete all invoices available
                            $this->contract_m->create_invoices(); // Create new contract's invoices
        
                            $messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
        
                            $this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
                            $messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
                            break;
                        default:
                            break;
                    }
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'discount_amount':

				try
				{
					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified
					$contract = $this->contract_m->get_contract();
					if( 'onead' != $contract->term_type) parent::response('Loại dịch vụ không được hỗ trợ', parent::HTTP_NOT_ACCEPTABLE);
					
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					update_term_meta($args['pk'], 'contract_value', $this->contract_m->calc_contract_value());

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices

					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'quantity':

				try
				{
					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified
					$contract = $this->contract_m->get_contract();
					if( 'onead' != $contract->term_type) throw new Exception("ERROR UNKNOWN");

					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					update_term_meta($args['pk'], 'contract_value', $this->contract_m->calc_contract_value());
					
					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices

					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'price':
			
				try
				{
					$this->contract_m->set_contract($args['pk']); // Init & dectect contract specified
					$contract = $this->contract_m->get_contract();
					if( 'onead' != $contract->term_type) throw new Exception("ERROR UNKNOWN");

					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					update_term_meta($args['pk'], 'contract_value', $this->contract_m->calc_contract_value());
					
					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices

					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
					break;
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;
			
			case 'term_parent':
				
				if( ! in_array($this->admin_m->role_id, array(1,5,24)))	parent::response('Không có quyền cập nhật giá trị này. [1]', parent::HTTP_NOT_ACCEPTABLE);

				break;

			case 'staff_business':
				
				if( ! has_permission('admin.contract.manage')) parent::response('Không có quyền cập nhật giá trị này. [0]', parent::HTTP_NOT_ACCEPTABLE);

				$group_sales_ids = $this->option_m->get_value('group_sales_ids', TRUE) ?? [];
				if( ! in_array($this->admin_m->get_field_by_id($args['value'], 'role_id'), $group_sales_ids))
				{
					parent::response('Nhân viên cần thay đổi không thuộc bộ phận nhân viên kinh doanh.', parent::HTTP_BAD_REQUEST);
				}

                $staff_business_ids = [];

				$users = $this->term_users_m->get_the_users($args['pk'], 'admin');
				$staff_business = get_term_meta_value($args['pk'], 'staff_business');
                if($args['is_overwrite'])
                {
                    unset($users[$staff_business]);
                }
                else 
                {
                    $staff_business_ids[] = $staff_business;
                }

				$users 	= array_keys($users);
				$created_by = get_term_meta_value($args['pk'], 'created_by') ?: $this->admin_m->id;
				if( ! empty($created_by) && !in_array($created_by, $users)) $users[] = $created_by;

				$users[] = $args['value'];
                $staff_business_ids[] = $args['value'];

				$this->term_users_m->set_relations_by_term($args['pk'], array_unique($users), 'admin', array(), TRUE);

                $customer = $this->term_users_m->get_term_users($args['pk'], ['customer_person', 'customer_company']);
                if(empty($customer))
                {
                    parent::response('Không tìm thấy hồ sơ khách hàng.', parent::HTTP_BAD_REQUEST);

                    break;
                }

                $customer = reset($customer);
                $this->usermeta_m->delete_meta($customer->user_id, 'assigned_to');
                foreach($staff_business_ids as $staff_business_id)
                {
                    $this->usermeta_m->add_meta($customer->user_id, 'assigned_to', $staff_business_id);
                }

				break;

			case 'number_of_payments':

				if( empty($args['value'])) parent::response('Số đợt thanh toán phải >=1', parent::HTTP_NOT_ACCEPTABLE);

				update_term_meta($args['pk'], $args['name'], $args['value']);
				unset($metadata[$args['name']]);

				$this->contract_m->set_contract($args['pk']);
				$this->contract_m->delete_all_invoices(); //  Delete all invoices available
				
				$this->contract_m->create_invoices(); // Create new contract's invoices
				$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

				$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
				$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
				break;

			case 'contract_price_weboptimize':
				
				if( empty($args['value'])) parent::response('Giá trị dịch vụ nhập vào không hợp lệ. [0]', parent::HTTP_NOT_ACCEPTABLE);

				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']);
					$contract_value = $this->contract_m->calc_contract_value();

					if(empty($contract_value)) parent::response('Giá trị dịch vụ nhập vào không hợp lệ [1].', parent::HTTP_NOT_ACCEPTABLE);

					update_term_meta($args['pk'], 'contract_value', $contract_value); // Update Contract value

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'contract_budget':
				
				if( empty($args['value'])) parent::response('Giá trị dịch vụ nhập vào không hợp lệ. [0]', parent::HTTP_NOT_ACCEPTABLE);

				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']);
					$this->contract_m->update_budget_metrics($args['value']);

					$contract_value = $this->contract_m->calc_contract_value();
					if(empty($contract_value)) parent::response('Giá trị dịch vụ nhập vào không hợp lệ [1].', parent::HTTP_NOT_ACCEPTABLE);

					update_term_meta($args['pk'], 'contract_value', $contract_value); // Update Contract value

					update_term_meta($args['pk'], 'service_fee_rate_actual', $this->contract_m->calc_service_fee_rate_actual());

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'original_service_fee_rate':
				
				if( empty($args['value'])) parent::response('% phí dịch vụ áp dụng không hợp lệ. [0]', parent::HTTP_NOT_ACCEPTABLE);

				try
				{
					$this->contract_m->set_contract($args['pk']);
					$original_rule = $this->contract_m->get_service_rule();

					$value 					= div($args['value'], 100); 
					$original_rule['rate'] 	= (double) $original_rule['rate'];
					
					if($value < $original_rule['rate']) parent::response('% phí dịch vụ áp dụng chỉ có thể lớn hoặc bằng % phí dịch mặc định [1].', parent::HTTP_NOT_ACCEPTABLE);

					$this->contract_m->update_budget_metrics(0, $value);

					$contract_value = $this->contract_m->calc_contract_value();
					if(empty($contract_value)) parent::response('Giá trị dịch vụ nhập vào không hợp lệ [1].', parent::HTTP_NOT_ACCEPTABLE);

					update_term_meta($args['pk'], 'contract_value', $contract_value); // Update Contract value
					update_term_meta($args['pk'], 'service_fee_rate_actual', $this->contract_m->calc_service_fee_rate_actual()); // Cập nhật % phí dịch vụ thực tế

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';

					unset($metadata[$args['name']]);
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'service_fee':
				
				if( empty($args['value'])) parent::response('Giá trị dịch vụ nhập vào không hợp lệ. [0]', parent::HTTP_NOT_ACCEPTABLE);

				try
				{
					update_term_meta($args['pk'], $args['name'], $args['value']);
					unset($metadata[$args['name']]);

					$this->contract_m->set_contract($args['pk']);
					$contract_value = $this->contract_m->calc_contract_value();

					if(empty($contract_value)) parent::response('Giá trị dịch vụ nhập vào không hợp lệ [1].', parent::HTTP_NOT_ACCEPTABLE);

					update_term_meta($args['pk'], 'contract_value', $contract_value); // Update Contract value
					update_term_meta($args['pk'], 'service_fee_rate_actual', $this->contract_m->calc_service_fee_rate_actual());

					$this->contract_m->delete_all_invoices(); //  Delete all invoices available
					$this->contract_m->create_invoices(); // Create new contract's invoices
					$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

					$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
					$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'vat':

				$contract = $this->contract_m->set_term_type()->get($args['pk']);

				if($contract->term_type == 'courseads')
				{
					break;
				}
				else if($contract->term_type == 'webdesign' && ! empty($args['value']))
				{
					parent::response('Hợp đồng Thiết kế web thì thuế VAT = 0', parent::HTTP_NOT_ACCEPTABLE);
				}

				$extra = get_term_meta_value($args['pk'], 'extra');
				$extra = is_serialized($extra) ? unserialize($extra) : $extra;

                $is_required = true;
                $owner = $this->term_users_m->get_term_users($contract->term_parent,array('customer_person'));
                if( ! empty($owner))
		        {
		        	$customer = reset($owner);
		        	$is_required = 'customer_person' != $customer->user_type;
		        }

				if($is_required && empty($extra['customer_tax'])) parent::response('Hợp đồng có thuế VAT thì Mã Số Thuế là bắt buộc.', parent::HTTP_NOT_ACCEPTABLE);

				break;

			case 'customer_tax':

				$vat = (int) get_term_meta_value($args['pk'], 'vat');
				if( $vat > 0 
                    && empty($args['value'])
                ) 
                {
                    parent::response('Hợp đồng có thuế VAT thì Mã Số Thuế là bắt buộc.', parent::HTTP_NOT_ACCEPTABLE);
                }

                $args['value'] = trim($args['value']);

                if(has_permission('Admin.Customer.manage'))
				{
					$extra = get_term_meta_value($args['pk'], 'extra');
					$extra = is_serialized($extra) ? unserialize($extra) : [];
					$extra['customer_tax'] = $args['value'];

					unset($metadata['customer_tax']);
					$metadata['extra'] = serialize($extra);

					break;
				}

				$isValidTin = tin_validator($args['value']);
                if(!$isValidTin) 
                {
                    parent::response('Mã số thuế không đúng định dạng.', parent::HTTP_NOT_ACCEPTABLE);
                }

                $customers = $this->term_users_m->get_term_users($args['pk'], ['customer_person', 'customer_company'], ['fields' => 'user.user_id']);
                $customer_ids = array_map(function($item){
                    return $item->user_id; 
                }, $customers);

                $isExist = $this->usermeta_m->where_not_in('user_id', $customer_ids)
                    ->where('meta_key', 'customer_tax')
                    ->where('meta_value', $args['value'])
                    ->count_by() > 0;
                if( $isExist) 
                {
                    parent::response('Mã số thuế của doanh nghiệp đã tồn tại.', parent::HTTP_NOT_ACCEPTABLE);
                }

                $is_active_tin_check = (bool) $this->option_m->get_value('is_active_tin_check');
                if( $is_active_tin_check 
                    && empty((new TaxCode())->find($args['value'])) 
                    && !has_permission('Admin.Customer.manage')
                )
                {
                    parent::response('Mã số thuế của doanh nghiệp chưa được xác nhận bởi cơ quan thuế', parent::HTTP_NOT_ACCEPTABLE);
                }

				$extra = get_term_meta_value($args['pk'], 'extra');
				$extra = is_serialized($extra) ? unserialize($extra) : [];
				$extra['customer_tax'] = $args['value'];

				unset($metadata['customer_tax']);
				$metadata['extra'] = serialize($extra);

				break;

			case 'bank_id':
				$extra = get_term_meta_value($args['pk'], 'extra');
				$extra = is_serialized($extra) ? unserialize($extra) : [];
				$extra['bank_id'] = $args['value'];

				unset($metadata['bank_id']);
				$metadata['extra'] = serialize($extra);

				break;

			case 'bank_owner':
				$extra = get_term_meta_value($args['pk'], 'extra');
				$extra = is_serialized($extra) ? unserialize($extra) : [];
				$extra['bank_owner'] = $args['value'];

				unset($metadata['bank_owner']);
				$metadata['extra'] = serialize($extra);

				break;

			case 'bank_name':
				$extra = get_term_meta_value($args['pk'], 'extra');
				$extra = is_serialized($extra) ? unserialize($extra) : [];
				$extra['bank_name'] = $args['value'];

				unset($metadata['bank_name']);
				$metadata['extra'] = serialize($extra);

				break;

			case 'contract_begin': 

				$has_service_started = get_term_meta_value($args['pk'], 'started_service', 1);
				if($has_service_started && !in_array($this->admin_m->role_id, array(1,5,25)))
				{
					parent::response('Không thể cập nhật vì HĐ đã được thực hiện.', parent::HTTP_NOT_ACCEPTABLE);
				}

				$metadata[$args['name']] = strtotime($args['value']);

				$contract_end = (int) get_term_meta_value($args['pk'], 'contract_end');
				if($metadata[$args['name']] >= $contract_end)
				{
					parent::response('Ngày bắt đầu hợp đồng không thể lớn hơn ngày kết thúc hợp đồng.', parent::HTTP_NOT_ACCEPTABLE);
				}

				break;

			case 'contract_end': 

				$has_service_started = get_term_meta_value($args['pk'], 'started_service', 1);
				if($has_service_started && !in_array($this->admin_m->role_id, array(1,5,25)))
				{
					parent::response('Không thể cập nhật vì HĐ đã được thực hiện.', parent::HTTP_NOT_ACCEPTABLE);
				}

				// IF CONTRACT IS WEBBUILD => NOT ALLOWED MODIFY CONTRACT DATE
				if($contract = $this->contract_m->where('term_type', 'webdesign')->get($args['pk']))
				{
					parent::response('Hợp đồng Thiết Kế Website không thể cập nhật ngày kết thúc trực tiếp [0]', parent::HTTP_NOT_ACCEPTABLE);
				}

				$metadata[$args['name']] = strtotime($args['value']); 

				$contract_begin = (int) get_term_meta_value($args['pk'], 'contract_begin');
				if($metadata[$args['name']] <= $contract_begin)
				{
					parent::response('Ngày bắt đầu hợp đồng không thể lớn hơn ngày kết thúc hợp đồng.', parent::HTTP_NOT_ACCEPTABLE);
				}
				
				break;

			case 'network_type': // Core channel to display advertise
				if(empty($args['value'])) parent::response('Vui lòng chọn ít nhất 1 kênh quảng cáo', parent::HTTP_NOT_ACCEPTABLE);
				$metadata[$args['name']] = implode(',', $args['value']);
				break;

            /* Khách tự thanh toán || ADSPLUS thu & chi hộ */
			case 'service_fee_plan':
				try
				{
                    unset($metadata[$args['name']]);
					$messages[] = 'Mức % phí dịch vụ cập nhật thành công !';
					break;
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;

			case 'googleads-end_time': $metadata[$args['name']] = strtotime($args['value']); break;
			case 'googleads-begin_time': $metadata[$args['name']] = strtotime($args['value']); break;
			case 'advertise_start_time': $metadata[$args['name']] = strtotime($args['value']); break;
			case 'advertise_end_time': $metadata[$args['name']] = strtotime($args['value']); break;
			case 'started_service': $metadata[$args['name']] = strtotime($args['value']); break;
			case 'start_service_time': $metadata[$args['name']] = strtotime($args['value']); break;
			case 'end_service_time': $metadata[$args['name']] = strtotime($args['value']); break;
			case 'exchange_rate_usd_to_vnd': $metadata[$args['name']] = $args['value']; break;
			case 'exchange_rate_aud_to_vnd': $metadata[$args['name']] = $args['value']; break;
            case 'representative_phone': 
                $args['value'] = $phone = trim($args['value']);

                if(has_permission('Admin.Customer.manage')) break;

                try
				{
                    if(empty($phone)) 
                        throw new Exception('Số điện thoại là bắt buộc');

                    $args['value'] = trim($args['value']);

                    $isValidPhone = phone_validator($phone);
                    if(!$isValidPhone) 
                        throw new Exception('Số điện thoại không đúng định dạng');

                    $contract = $this->contract_m->set_term_type()->get($args['pk']);
                    $customer = $this->term_users_m->get_term_users($contract->term_id, ['customer_person', 'customer_company']);
                    if(!empty($customer))
                    {
                        $customer = reset($customer);
                        $isPhoneUnique = $this->customer_m->phone_uniquecheck($phone, $customer->user_type, $customer->user_id);

                        if(!$isPhoneUnique) throw new Exception('Số điện thoại khách hàng đã tồn tại.');
                    }
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;
            case 'representative_email':
                $args['value'] = $email = trim($args['value']);

                if(has_permission('Admin.Customer.manage')) break;
                
                try
				{					
                    if(empty($email)) 
                        throw new Exception('Email là bắt buộc');

                    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                        throw new Exception('Email không đúng định dạng');
                    
                    $contract = $this->contract_m->set_term_type()->get($args['pk']);
                    $customer = $this->term_users_m->get_term_users($contract->term_id, ['customer_person', 'customer_company']);
                    if(!empty($customer))
                    {
                        $customer = reset($customer);
                        $isEmailUnique = $this->customer_m->email_uniquecheck($email, $customer->user_type, $customer->user_id);

                        if(!$isEmailUnique) throw new Exception('Email khách hàng đã tồn tại.');
                    }
                    
                    $isValid = (new VerifyEmail())->setEmail($email)->verify();
                    $has_manage_customer = has_permission('Admin.Customer.mdeparment') || has_permission('Admin.Customer.mgroup');
                    if( Status::SUCCESS != $isValid 
                        && !$has_manage_customer
                    )
                    {
                        throw new Exception(Status::name($isValid));
                    }
				}
				catch (Exception $e)
				{
					parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
				}
				break;
            case 'hubspot_link':
                try
                {
                    $hubspot_link = trim($args['value']);
                    if(empty($hubspot_link)) throw new Exception('Link Hubspot là bắt buộc');

                    $regex = '/(http|https):\/\/app.hubspot.com\/.*/';
                    if(!preg_match($regex, $hubspot_link)){
                        throw new Exception('Link Hubspot không đúng định dạng');
                    }
                }
                catch (Exception $e)
                {
                    parent::response($e->getMessage(), parent::HTTP_NOT_ACCEPTABLE);
                }
                break;
		}


		if( ! empty($edit)) $this->contract_m->update($args['pk'], $edit);
		if( ! empty($metadata)) 
		{
			foreach ($metadata as $key => $value)
			{
				update_term_meta($args['pk'], $key, $value);
			}
		}

		parent::response(['msg'=>$messages]);
	}

	/**
	 * Sync all invoices & all amount data from 
	 */
	public function sync_invoices_post()
	{
		$args 		= wp_parse_args(parent::post(NULL, TRUE), ['id' => 0]);
		if( empty($args['id'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		if(FALSE === $this->contract_m->has_permission($args['id'], 'admin.contract.view'))
		{
			parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
		}

        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                            && FALSE == (bool)get_term_meta_value($args['id'], 'is_manipulation_locked');
        if($is_manipulation_locked)
        {
            $started_service = (int) get_term_meta_value($args['id'], 'started_service') ?: time();
            $started_service = end_of_day($started_service);
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
            if($manipulation_locked_at > $started_service)
            {
                $manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                parent::response("Hợp đồng đã khoá thao tác lúc {$manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá.", parent::HTTP_BAD_REQUEST);
            }
        }

		$messages = array();
		$contract = $this->contract_m->set_contract($args['id']);
		try
		{
			$this->contract_m->delete_all_invoices(); //  Delete all invoices available
			$this->contract_m->create_invoices(); // Create new contract's invoices
			$messages[] = 'Đợt thanh toán đã đồng bộ thành công !';

			$this->contract_m->sync_all_amount(); // Update all invoices's amount & receipt's amount
			$messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
		}
		catch (Exception $e)
		{	
			parent::response($e->getMessage(), parent::HTTP_NO_CONTENT);
		}

		parent::response(['msg' => $messages]);
	}

	
}
/* End of file Invoices.php */
/* Location: ./application/modules/contract/controllers/api/Invoices.php */
