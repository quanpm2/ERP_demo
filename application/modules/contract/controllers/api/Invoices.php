<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices extends REST_Controller
{
	function __construct()
	{
		parent::__construct('contract/rest');
		$this->load->model('usermeta_m');
		$this->load->model('permission_m');
	}

	public function index_get()
	{
		$default 	= array('contract_id' => '', 'meta' => '', 'field' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if( empty($args['contract_id'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		$response 		= array('msg' => 'Xử lý không thành công', 'data' => []);
		$model_names 	= array('term_m','termmeta_m', 'staffs/admin_m', 'contract/contract_m', 'webbuild/webbuild_m', 'term_users_m');
		$this->load->model($model_names);

		if(FALSE === $this->contract_m->has_permission($args['contract_id'], 'admin.contract.view'))
		{
			parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng !', parent::HTTP_UNAUTHORIZED);
		}

		$model_names = array('contract/invoice_m','contract/invoice_item_m','post_m','contract/base_contract_m');
		$this->load->model($model_names);

		$invoices = $this->invoice_m->select('term_id,posts.post_id,post_title,start_date,end_date,post_status')
		->join('term_posts','term_posts.post_id = posts.post_id')
		->where('term_posts.term_id', $args['contract_id'])
		->where('post_type', $this->invoice_m->post_type)
		->get_all();
		if( ! $invoices) parent::response('Không tìm thấy kết quả phù hợp !', parent::HTTP_NO_CONTENT);

		foreach ($invoices as &$invoice)
		{
			$invoice->price_total = (int) $this->invoice_item_m->get_total($invoice->post_id, 'total');
		}
		$response['data'] = array_values($invoices);

		parent::response($response);
	}
}
/* End of file Invoices.php */
/* Location: ./application/modules/contract/controllers/api/Invoices.php */