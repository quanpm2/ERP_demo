<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Cron_receipt extends Public_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$models = array(
			'contract/contract_m',
			'contract/contract_report_m',
            'contract/receipt_m'
        );

		$this->load->model($models);
	}

	public function index() 
	{
        if( ! $this->log_m->where([ 'log_type' => 'queue-receipt-sync-metadata', 'log_status' => 0 ])->count_by() > 0) return false;

        $jobs = $this->log_m
        ->select('log_id, log_status, log_content')
        ->order_by('log_id', 'desc')
        ->get_many_by([ 'log_type' => 'queue-receipt-sync-metadata', 'log_status' => 0 ]);

        if( empty($jobs)) return true;

        $this->load->config('amqps');
		$amqps_host 	= $this->config->item('host', 'amqps');
		$amqps_port 	= $this->config->item('port', 'amqps');
		$amqps_user 	= $this->config->item('user', 'amqps');
		$amqps_password = $this->config->item('password', 'amqps');
		
        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];

		$connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
		$channel 	= $connection->channel();
		$channel->queue_declare($queue, false, true, false, false);

        foreach ($jobs as $job)
        {
            $payload = [
				'event' => 'contract_payment.sync.metadata',
				'paymentId'	=> (int) $job->log_content,
				'log_id' => (int) $job->log_id
			];

            $message = new \PhpAmqpLib\Message\AMQPMessage(
                json_encode($payload),
                array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
            );
            $channel->basic_publish($message, '', $queue);	
        }

		$channel->close();
		$connection->close();

        return true;
	}
}
/* End of file Cron_receipt.php */
/* Location: ./application/modules/Cron/controllers/Cron_receipt.php */
