<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Admin_Controller {

	/**
	 * Sends a mail notification payment.
	 *
	 * @return     JSON  The result
	 */
	public function send_email_successful_payment()
	{
		$reponse = array('status' => 0, 'data' => null, 'msg' => 'Gửi mail thông báo không thành công!');

		$post_id = $this->input->post('post_id');
		if(empty($post_id)) return parent::renderJson($reponse);

		// Restrict permission
		if( ! has_permission('contract.notification_payment.manage'))
		{
			$reponse['msg'] = 'Tài khoản của bạn không đủ quyền hạn để thực hiện tác vụ này .';
			return parent::renderJson($reponse);
		}

		// Check the receipt mail notify is sent
		$has_send_mail_payment = get_post_meta_value($post_id, 'has_send_mail_payment');
		if($has_send_mail_payment) 
		{
			$reponse['msg'] = 'Hợp đồng này đã được gửi mail';
			return parent::renderJson($reponse);
		}

		/* Check the receipt is paid. If not then return , else continue */
		$this->load->model('contract/receipt_m');
		$receipt = $this->receipt_m->select('post_id')->where('post_status','paid')->get($post_id);
		if( ! $receipt)
		{
			$reponse['msg'] = 'Trạng thái hóa đơn chưa được thanh toán';
			return parent::renderJson($reponse);
		}

		// Send send_email_successful_payment to all customer + technical + sale + manager
		$this->load->model('contract/contract_report_m');
		$status = $this->contract_report_m->send_email_successful_payment($post_id);
		if( ! $status)
		{
			$reponse['msg'] = 'Quá trình xử lý bị gián đoạn , xin vui lòng thử gửi lại sau 5 phút!';
			return parent::renderJson($reponse);
		}

		/* Send SMS thông báo khách hàng */
		$this->contract_report_m->send_sms_successful_payment($post_id);

		update_post_meta($post_id, 'has_send_mail_payment', 1);

		$reponse['status'] 	= TRUE;
		$reponse['msg']		= 'Email thông báo đã gửi thành công .';
		return parent::renderJson($reponse);
	}
}
/* End of file Report.php */
/* Location: ./application/modules/contract/controllers/ajax/Report.php */