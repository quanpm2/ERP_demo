<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'third_party/vendor/autoload.php';
class Category extends  API_Controller
{
	public function __construct()
	{
		parent::__construct() ;
		$this->load->model('term_m') ;
	}

	public function delete_term() {
		$response = array('status' => 0, 'msg' => 'Kết nối thất bại', 'data' => NULL) ;

		$term_id  = $this->input->post('term_id') ;
		if(empty($term_id)) return $this->render($response) ;

		$term 	  = $this->term_m->update($term_id, array('term_status' => 'ending'));

		if(empty($term)) return $this->render($response) ;
		return $this->render(array('status' => 1, 'msg' => 'Thành công', 'data' => $term));
	}
}