<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Receipt extends Admin_Controller
{
    function __construct()
    {
        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/term_users_m';
        $this->autoload['models'][] = 'contract/promotion_m';

        parent::__construct();

        $this->load->config('contract/receipt');
    }

    public function index($term_id = 0)
    {
        $result = ['success' => FALSE, 'msg' => 'Xử lý không thành công', 'data' => ''];
        if (!has_permission('contract.receipt.access')) {
            log_message('error', "Permission illegal request #{$this->admin_m->id} try to access contract/ajax/receipt/index/{$term_id}");
            $result['msg'] = 'Quyền truy cập bị hạn chế !';

            return parent::renderJson($result, 500);
        }

        $post_types = $this->config->item('type', 'receipt');
        $post_types and $post_types = array_keys($post_types);
        $post_types[] = $this->promotion_m->post_type;

        $data = $this->data;
        $data['content'] = $this->admin_ui
            ->select('posts.post_id')
            ->add_column('posts.post_title', array('title' => 'Ghi chú', 'set_order' => FALSE))
            ->add_column('transfer_type', array('title' => 'Hình thức chuyển', 'set_order' => FALSE, 'set_select' => FALSE))
            ->add_column('amount', array('title' => 'Số tiền', 'set_order' => FALSE, 'set_select' => FALSE))
            ->add_column('posts.end_date', array('title' => 'T/g thanh toán/<br> Hạn bảo lãnh', 'set_order' => FALSE))
            ->add_column('posts.post_type', array('title' => 'Loại', 'set_order' => FALSE))
            ->add_column('posts.created_on', array('title' => 'T/g tạo', 'set_order' => FALSE))
            ->add_column('posts.post_author', array('title' => 'Người tạo', 'set_order' => FALSE))
            ->add_column('posts.post_status', array('title' => 'Trạng thái', 'set_order' => FALSE))
            ->add_column('action', array('set_select' => FALSE, 'title' => 'Action', 'set_order' => FALSE))
            ->add_column_callback('post_id', function ($data, $row_name) use ($term_id) {

                $post_id = $data['post_id'];
                $data['post_title'] = (!empty(get_post_meta_value($post_id, 'is_order_printed')) ? 'Đã In hóa đơn' : '') . $data['post_title'];

                // Hình thức thanh toán
                $def_transfer_type = $this->config->item('transfer_type', 'receipt');
                $transfer_type = get_post_meta_value($post_id, 'transfer_type');
                $data['transfer_type'] = $def_transfer_type[$transfer_type] ?? reset($def_transfer_type);

                // tài khoản
                if ($transfer_type == 'account') {
                    $transfer_account = get_post_meta_value($post_id, 'transfer_account');
                    $data['transfer_type'] .= br() . '<span>' . $transfer_account . '</span>';
                }

                // Số tiền thanh toán
                $amount = get_post_meta_value($post_id, 'amount');
                $data['amount'] = currency_numberformat($amount, ' đ');

                $data['created_on'] = my_date($data['created_on'], 'd/m/Y H:i:s');

                // Ngày thanh toán
                $data['end_date'] = my_date($data['end_date'], 'd/m/Y');

                $data['post_author'] = $this->admin_m->get_field_by_id($data['post_author'], 'user_email');

                $def_post_status = $this->config->item('status', 'receipt');
                $data['post_status'] = $def_post_status[$data['post_type']][$data['post_status']];

                if (has_permission('contract.receipt.update')) {
                    $data['action'] = anchor(module_url("ajax/receipt/edit/{$data['post_id']}/{$term_id}"), '<i class="fa fa-fw fa-edit"></i>', "title='Cập nhật' class='btn btn-info btn-xs receipt-edit'");
                }

                // Render 'Actions' columns
                if (has_permission('contract.notification_payment') && $data['post_type'] != 'receipt_payment_on_behaft') {
                    $btn_send_mail = '<button type="button" class="btn btn-info btn-xs btn-send-mail receipt-' . $post_id . '" data-post-id="' . $post_id . '" title="Gửi mail" ><i class="fa fa-envelope-o" aria-hidden="true"></i></button>';

                    $has_send_mail_payment         = get_post_meta_value($post_id, 'has_send_mail_payment');
                    if ($has_send_mail_payment     == 1) {
                        $btn_send_mail = '<button type="button" class="btn btn-info btn-xs disabled receipt-' . $post_id . '" data-post-id="' . $post_id . '" title="Gửi mail" ><i class="fa fa-envelope-o" aria-hidden="true"></i></button>';
                    }

                    $data['action'] .= $btn_send_mail;
                }

                $def_post_type = $this->config->item('type', 'receipt');
                $def_post_type[$this->promotion_m->post_type] = 'Ngân sách khuyến mãi';
                $data['post_type'] = $def_post_type[$data['post_type']];

                return $data;
            }, FALSE)

            ->join('term_posts', 'term_posts.post_id = posts.post_id')
            ->where('term_posts.term_id', $term_id)
            ->where_in('posts.post_type', $post_types)
            ->from('posts')
            ->generate(['per_page' => 1000]);

        $data['term_id'] = $term_id;
        $data['contract_code'] = get_term_meta_value($term_id, 'contract_code');

        $content = (string) $this->template->list->view('receipt/ajax-index', $data);
        return parent::renderJson(['success' => TRUE, 'msg' => 'Xử lý dữ liệu thành công', 'data' => $content]);
    }

    public function edit($post_id = 0, $term_id = 0)
    {
        $result = ['success' => FALSE, 'msg' => 'Xử lý không thành công', 'data' => ''];
        if (!has_permission('contract.receipt.update')) {
            log_message('error', "Permission illegal request #{$this->admin_m->id} access contract/ajax/receipt/edit/{$post_id}/{$term_id}");
            $result['msg'] = 'Quyền truy cập bị hạn chế !';
            return parent::renderJson($result, 500);
        }

        $is_manipulation_locked = (bool) get_term_meta_value($term_id, 'is_manipulation_locked');
        
        $manipulation_locked_at = 0;
        $is_manipulation_locked AND $manipulation_locked_at = get_term_meta_value($term_id, 'manipulation_locked_at') ?: time();

        $post_id = absint($post_id);

        if (!has_permission('contract.receipt.manage')) {
            $this->receipt_m->where('post_author', $this->admin_m->id);
        }

        $receipt = $this->receipt_m->set_post_type()
            ->join('term_posts', "term_posts.post_id = posts.post_id AND term_posts.term_id = {$term_id}")
            ->get_by(['posts.post_id' => $post_id]);
        if (!$receipt) 
        {
            if($is_manipulation_locked)
            {
                $_manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                $result['msg'] = "Hợp đồng đã khoá thao tác lúc {$_manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá.";
                return parent::renderJson($result);
            }

            $insert_id     = $this->receipt_m->insert(['created_on' => time(), 'post_author' => $this->admin_m->id, 'post_status' => 'draft']);
            $receipt     = $this->receipt_m->get($insert_id);
            $this->term_posts_m->set_term_posts($term_id, [$insert_id]);
        }

        $result = array('success' => TRUE, 'msg' => '', 'data' => '');

        if ($post = $this->input->post()) {
            $post = $this->input->post();

            if (empty($post['post_id'])) {
                $post['post_id'] = $receipt->post_id;
            }

            if (!empty($post['edit']['end_date'])) {
                $post['edit']['end_date'] = $this->mdate->startOfDay($post['edit']['end_date']);
            }


            if (!empty($post['meta'])) 
            {
                if($receipt->end_date < $manipulation_locked_at)
                {
                    $_manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                    $result['success'] = FALSE;
                    $result['msg'] = "Hợp đồng đã khoá thao tác lúc {$_manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá.";
                    return parent::renderJson($result);
                }

                $post['meta']['is_order_printed'] = (int) !empty($post['meta']['is_order_printed']);
                foreach ($post['meta'] as $key => $value) {
                    update_post_meta($post['post_id'], $key, $value);
                }
            }

            if (!empty($post['edit'])) 
            {
                if($receipt->end_date < $manipulation_locked_at)
                {
                    $_manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                    $result['success'] = FALSE;
                    $result['msg'] = "Hợp đồng đã khoá thao tác lúc {$_manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá.";
                    return parent::renderJson($result);
                }

                $this->receipt_m->update($post['post_id'], $post['edit']);
            }

            // END :: Post Slack
            if (!empty($post['edit']['post_status']) && in_array($post['edit']['post_status'], ['paid', 'publish'])) {
                try {

                    /* Add Deal Hubspot Job for another service queue work */
                    $this->log_m->insert(array(
                        'log_type' => 'callDealHubspotApi',
                        'user_id' => $this->admin_m->id,
                        'log_status' => 0,
                        'log_content' => serialize([
                            'action' => 'DealPaymented',
                            'term_id' => $term_id,
                            'post_id' => $post['post_id']
                        ])
                    ));

                    /* POST SLACK */
                    $this->load->model('contract/common_report_m');
                    $this->common_report_m->init($term_id);
                    $this->common_report_m->setReceipt($post['post_id']);
                    $this->common_report_m->postSlackNewCustomerPayment();

                    /* Post Money24h App */
                    $this->common_report_m->init($term_id);
                    $this->common_report_m->setReceipt($post['post_id']);
                    $this->common_report_m->postMoney24HNewCustomerPayment();
                } catch (Exception $e) {
                }
            }
            // END :: Post Slack 


            $result['msg'] = 'Xử lý thành công';
            $result['data'] = ['refresh_url' => module_url("ajax/receipt/index/{$term_id}")];
            return parent::renderJson($result);
        }

        $post_types = $this->config->item('type', 'receipt');
        $post_types[$this->promotion_m->post_type] = 'Ngân sách khuyến mãi';

        $data = $this->data;
        $data['edit'] = $receipt;
        $data['term_id'] = $term_id;
        $data['post_type_enums'] = $post_types;

        $result['data'] = (string) $this->template->list->view('receipt/ajax-edit', $data);
        return parent::renderJson($result);
    }

    public function getGroupByParent($term_id = 0)
    {
        $term_id = (int)$term_id;
        $this->load->model('staffs/user_group_m');
        $response = array('status' => FALSE, 'msg' => 'Xử lý không thành công', 'data' => []);
        $user_groups = array();
        if ($term_id == 0) {
            $query = $this->term_m->select('term.term_id,term.term_name,department.term_name as department')
                ->join('term department', 'term.term_parent = department.term_id')
                ->where('term.term_type', 'user_group')
                ->order_by('department')
                ->as_array()
                ->get_many_by();

            $groups_in_departments = array_group_by($query, 'department');

            foreach ($groups_in_departments as $department => $groups) {
                foreach ($groups as $key => $value) {
                    $user_groups[$department][$value['term_id']] = $value['term_name'];
                }
            }
        } else {
            $user_groups = $this->user_group_m->select('term_id,term_name')->where('term_parent', $term_id)->as_array()->get_many_by();
        }
        if (empty($user_groups)) {
            return parent::renderJson($response);
        }
        $response['status'] = TRUE;
        $response['msg'] = 'Xử lý thành công';
        $response['data'] = $user_groups;
        return parent::renderJson($response, 200);
    }


    /**
     * Cập nhật phiếu bảo lãnh
     *
     * @param      integer  $post_id  The post identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function updateReceiptCaution($post_id = 0)
    {
        $result = ['success' => FALSE, 'msg' => 'Xử lý không thành công', 'data' => ''];
        if (!has_permission('contract.receipt.update')) {
            $result['msg'] = 'Quyền truy cập bị hạn chế !';
            return parent::renderJson($result, 500);
        }

        $post         = $this->input->post();
        $post_id     = absint($post_id);
        $term_id     = $post['term_id'] ?? 0;
        $user_id     = $post['user_id'] ?? 0;

        if (empty($term_id) || empty($user_id) || $user_id != get_term_meta_value($term_id, 'staff_business')) {
            $result['msg'] = 'Dữ liệu đầu vào không hợp lệ !';
            return parent::renderJson($result);
        }

        /* Check Bailment sale quota */
        $this->load->model('staffs/sale_m');

        $bailment_used                 = $this->sale_m->get_bailment_used($user_id);
        $defined_bailment_budget     = (int) get_user_meta_value($user_id, 'bailment_budget');
        $defined_bailment_quantity     = (int) get_user_meta_value($user_id, 'bailment_quantity');

        /* INSERT RECEIPT CAUTION */
        if (empty($post_id)) {
            /* KIỂM TRA SỐ DƯ CỦA SALE TRƯỚC KHI LƯU DỮ LIỆU VÀO DB */
            $balance             = $defined_bailment_budget - $bailment_used['budget'];
            $connum_remained     = $defined_bailment_quantity - $bailment_used['quantity'];

            /* Trường hợp số dư hoặc số lượng hợp đồng có thể bảo lãnh vượt mức cho phép thì không tiếp tục xử lý */
            if ($balance < (int)$post['meta']['amount'] || $connum_remained <= 0) {
                $result['msg'] = 'Không thể bảo lãnh vì đã kinh doanh đã vượt quá hạn mức bảo lãnh cho phép !';
                return parent::renderJson($result);
            }

            $insert_data = array(
                'end_date'         => $this->mdate->endOfDay($post['edit']['end_date']),
                'post_author'     => $this->admin_m->id,
                'post_title'     => 'Phiếu bảo lãnh',
                'post_type'     => 'receipt_caution',
                'post_status'     => $post['edit']['post_status'] ?? 'publish',
                'end_date'         => $this->mdate->convert_time($post['edit']['end_date']),
                'created_on'     => time()
            );

            $insert_id = $this->receipt_m->insert($insert_data);
            $this->term_posts_m->set_term_posts($term_id, [$insert_id]);
            if (!empty($post['meta'])) {
                foreach ($post['meta'] as $meta_key => $meta_value) {
                    update_post_meta($insert_id, $meta_key, $meta_value);
                }
            }

            /* Đồng bộ các chỉ số bảo lãnh vào hợp đồng */
            $this->base_contract_m->sync_bailment_amount($term_id);

            $result['data'] = array('post_id' => $insert_id);
            $result['msg'] = 'Phiếu bảo lãnh đã được tạo thành công';
            $result['status'] = TRUE;
            return parent::renderJson($result);
        }

        /* UPDATE RECEIPT CATION */
        if (!has_permission('contract.receipt.manage')) {
            $this->receipt_m->where('post_author', $this->admin_m->id);
        }

        $receipt = $this->receipt_m
            ->select('posts.post_id')
            ->join('term_posts', 'term_posts.post_id = posts.post_id')
            ->where('post_type', 'receipt_caution')
            ->where('term_posts.term_id', $term_id)
            ->get_by(['posts.post_id' => $post_id]);

        if (!$receipt) {
            $result['msg'] = 'Không có quyền truy cập vào dữ liệu này , liên hệ quản trị để biết thêm thông tin !';
            return parent::renderJson($result);
        }

        if (!empty($post['edit']['end_date'])) {
            $post['edit']['end_date'] = $this->mdate->endOfDay($post['edit']['end_date']);
        }

        if (!empty($post['edit'])) {
            $this->receipt_m->update($post_id, $post['edit']);
        }

        /* Nếu có sự thay đổi vê giá trị bảo lãnh */
        if (!empty($post['meta']['amount'])) {
            $amount = (int) $post['meta']['amount'];

            /* KIỂM TRA SỐ DƯ CỦA SALE TRƯỚC KHI LƯU DỮ LIỆU VÀO DB */
            $prev_amount             = (int) get_post_meta_value($post_id, 'amount');
            $balance_receipt_amount = $amount - $prev_amount;

            /* Trường hợp số dư hoặc số lượng hợp đồng có thể bảo lãnh vượt mức cho phép thì không tiếp tục xử lý */
            $balance = $defined_bailment_budget - ($bailment_used['budget'] + $balance_receipt_amount);
            if ($balance < 0) {
                $result['msg'] = 'Không thể cập nhật giá trị vì vượt quá hạn mức bảo lãnh cho phép !';
                return parent::renderJson($result);
            }

            update_post_meta($post_id, 'amount', $amount);
        }

        /* Đồng bộ dữ liệu bảo lãnh lên hợp đồng */
        $this->base_contract_m->sync_bailment_amount($term_id);

        $result['data'] = array('post_id' => $post_id);
        $result['msg'] = 'Dữ liệu được lưu trữ thành công';
        $result['status'] = TRUE;
        return parent::renderJson($result);
    }

    public function send_email_receipt_payment_paid()
    {
        $response     = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        $args         = wp_parse_args($this->input->post(NULL, TRUE), ['post_ids' => NULL]);
        if (!has_permission('Contract.notification_payment.manage')) {
            $response['msg'] = 'Bạn không có quyền truy cập thực hiện tác vụ này .';
            return parent::renderJson($response);
        }

        if (empty($args['post_ids'])) {
            $response['msg'] = 'Thông tin thanh toán không hợp lệ.';
            return parent::renderJson($response);
        }

        $args['post_ids'] = is_array($args['post_ids']) ? $args['post_ids'] : array($args['post_ids']);

        $receipts = $this->receipt_m
            ->select('post_id')
            ->where_in('post_id', $args['post_ids'])
            ->get_many_by(['post_status' => 'paid', 'post_type' => 'receipt_payment']);

        if (!$receipts) {
            $response['msg'] = 'Thông tin thanh toán không được tìm thấy.';
            return parent::renderJson($response);
        }

        $this->load->model('contract/contract_report_m');
        foreach ($receipts as $receipt) {
            $has_sent = (int) get_post_meta_value($receipt->post_id, 'has_send_mail_payment');
            if ($has_sent) continue;

            $result = $this->contract_report_m->send_email_successful_payment($receipt->post_id);
            if (!$result) continue;

            update_post_meta($receipt->post_id, 'has_send_mail_payment', 1);

            $this->contract_report_m->send_sms_successful_payment($receipt->post_id);
        }

        $response['msg']     = 'Tiến trình đã được xử lý .';
        $response['status'] = TRUE;
        return parent::renderJson($response);
    }
}
/* End of file Receipt.php */
/* Location: ./application/modules/contract/controllers/ajax/Receipt.php */