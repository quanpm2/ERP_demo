<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Create_wizard extends Admin_Controller
{
	function __construct()
	{
		parent::__construct();

		// if( ! $this->input->is_ajax_request()) exit();

		$models = array(
			'term_users_m',
			'invoice_m',
			'customer/customer_m',
			'contract_m',
			'contract_wizard_m',
			'base_contract_m',
			'contract/contract_m',
			'contract/contract_report_m'
			);

		$this->load->model($models);

		$this->load->config('contract/contract');
		$this->load->config('staffs/group');
		$this->load->config('contract/invoice');

		$this->data['step'] = $this->input->get('step') ? $this->input->get('step') : 0 ;
	}

	public function complete($term_id = 0)
	{
		$response = array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);
		if( ! has_permission('admin.contract.add,admin.contract.edit'))
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ .';
			return parent::renderJson($response,500);
		}

		$term = $this->contract_m
		->set_term_type()
		->select('term.term_id,term_status,term_type')
		->join('term_users','term_users.term_id = term.term_id')
		->get_by(['term.term_id'=>$term_id,'term_users.user_id'=>$this->admin_m->id,'term_status'=>'draft']);
		
		if( ! $term)
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ [1].';
			return parent::renderJson($response,500);
		}

		update_term_meta($term_id,'wizard_step',6);
		$this->contract_m->update($term_id,['term_status'=>'unverified']);

		/* Gửi email thông báo đến kinh doanh */
		$this->contract_report_m->send_mail_contract_composed_2admin($term_id);

		$response['success']	= TRUE;
		$response['msg'] 		= 'Hợp đồng đã được chuyển sang trạng thái "Chờ cấp số" thành công.';
		$response['data']['term'] 			= $term;
		$response['data']['redirect_url']	= admin_url('contract') . "/edit/{$term_id}";

		return parent::renderJson($response);
	}

	public function ajax_dipatcher($callable = '')
	{
		restrict('admin.contract.edit,order_contact.contract.add');

		$post = $this->input->post();

		if(empty($post)) return FALSE;
		
		if(method_exists($this->contract_wizard_m, "wizard_{$callable}"))
			$this->contract_wizard_m->{'wizard_'.$callable}($post);

		$this
		->add_filter_upload_file($post)
		->add_filter_ajax_edit($post)
		->add_filter_upload_real_file($post);
		
		if($this->hook->has_filter("ajax_{$callable}"))
		{
			$this->hook->add_filter("ajax_{$callable}",function($post){

				$result = array('response'=>$post, 'msg'=>'Cập nhật nhật thành công','success'=>TRUE);

				if( ! empty($post['msg'])){

					$result['msg'] = $post['msg'];

					unset($post['msg']);
				}

				if( isset($post['success'])){

					$result['success'] = $post['success'];

					unset($post['success']);
				}

				if( ! empty($post['edit']['meta'])) 
				{
					foreach ($post['edit']['meta'] as $key => $value)
					{
						$this->termmeta_m->update_meta($post['edit']['term_id'], $key, $value);
					}
				}

				unset($post['edit']['meta']);

				if( ! empty($post['edit'])){

					if(!empty($post['edit']['term_status']) && $post['edit']['term_status'] == 'publish'){

						$order_id = get_term_meta_value($post['edit']['term_id'], 'order_id');
						if(!empty($order_id)) 
							$this->post_m->update($order_id,array('post_status'=>'publish'));
					}

					$this->term_m->update($post['edit']['term_id'], $post['edit']);
				}

				return $result;
			});
		}

		$result = $this->hook->apply_filters("ajax_{$callable}", $post);

		if(!is_array($result)){

			$result = array($result);
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));

		return $this->output;
	}

	public function add_filter_upload_file($post){

		$this->hook->add_filter('ajax_act_upload_file',	function($post){

			$result = array('msg'=>'File upload không hợp lệ !','success'=>FALSE);

			$post = $this->input->post();
			if(empty($post)) 
				return $result;

			$upload_config = $this->config->item('upload_config');
			$upload_config['file_name']	= $post['term_name'] . '.' . $post['term_id'] . '.' . time();
			$upload_config['file_name'] = str_replace('.', '_', $upload_config['file_name']);
			$upload_config['allowed_types'] = 'doc|docx';
			$upload_config['upload_path'].= date('Y') . '/' . date('m') . '/';

			if(!is_dir($upload_config['upload_path']))
				mkdir($upload_config['upload_path'], '0777', TRUE);

			$this->load->library('upload');
			$this->upload->initialize($upload_config);

			if ($this->upload->do_upload('fileinput')){

				$data = $this->upload->data();

				$_tmp_queue_uploaded = @unserialize(get_term_meta_value($post['term_id'], '_tmp_file_upload'));
				if(empty($_tmp_queue_uploaded) || !is_array($_tmp_queue_uploaded))
					$_tmp_queue_uploaded = array();

				$_tmp_queue_uploaded[] = array('file_name'=>$data['file_name'],'file_path'=>$upload_config['upload_path'].$data['file_name']);
				$_tmp_queue_uploaded = arrayUnique($_tmp_queue_uploaded);

				$this->termmeta_m->update_meta($post['term_id'],'_tmp_file_upload', serialize($_tmp_queue_uploaded));

				$_tmp_queue_uploaded = array(array('file_name'=>$data['file_name'],'file_path'=>$upload_config['upload_path'].$data['file_name']));

				$result['ret_obj'] = $_tmp_queue_uploaded;
				$result['msg'] = 'Upload file hợp đồng thành công !';
				$result['success'] = TRUE;
			}

			return $result;
		});

		return $this;
	}

	public function add_filter_upload_real_file($post){

		$this->hook->add_filter('ajax_act_upload_real_file', function($post){

			$result = array('msg'=>'File upload không hợp lệ !','success'=>FALSE);

			$post = $this->input->post();

			if(empty($post)) return $result;

			$upload_config = $this->config->item('upload_config');

			$upload_config['file_name']	= $post['term_name'] . '.' . $post['term_id'] . '.' . time();

			$upload_config['file_name'] = str_replace('.', '_', $upload_config['file_name']);

			$upload_config['allowed_types'] = 'doc|docx';

			$upload_config['upload_path'].= date('Y') . '/' . date('m') . '/';

			if(!is_dir($upload_config['upload_path'])){

				mkdir($upload_config['upload_path'], '0777', TRUE);
			}

			$this->load->library('upload');

			$this->upload->initialize($upload_config);

			if ($this->upload->do_upload('fileinput')){

				$data = $this->upload->data();

				$_attachment_files = @unserialize(get_term_meta_value($post['term_id'], '_attachment_files'));

				if(empty($_attachment_files)) $_attachment_files = array();

				$_attachment_files[] = array('file_name'=>$data['file_name'],'file_path'=> $upload_config['upload_path'] . $data['file_name']);

				$_attachment_files = arrayUnique($_attachment_files);

				$this->termmeta_m->update_meta($post['term_id'],'_attachment_files', serialize($_attachment_files));

				$result['ret_obj'] = array(array('file_name'=>$data['file_name'],'file_path'=> $upload_config['upload_path'] . $data['file_name']));

				$result['msg'] = 'Upload file hợp đồng thành công !';

				$result['success'] = TRUE;
			}

			return $result;
		});

		return $this;
	}

	public function add_filter_ajax_edit($post){

		$this->hook->add_filter('ajax_ajax_edit', function($args){

			$post = array('edit' => array('term_id' => $args['pk'], 'meta' => array()));

			$this->load->library('form_validation');

			if(isset($args['value']) && is_string($args['value']))
			{	
				$args['value'] = trim($args['value']);
				if($args['value'] == '')
					return array('msg'=>'Vui lòng nhập trường này','success'=>FALSE);
			}

			switch ($args['name']) {

				case 'term_parent':
					if($args['value'] == 0) break;
					if($web_term = $this->term_m->get($args['value']))
						$this->term_m->update($post['edit']['term_id'], array('term_name'=>$web_term->term_name));	
					break;

				case 'representative_email':
					if(!$this->form_validation->valid_email($args['value']))
						return array('msg'=>'Địa chỉ email không hợp lệ','success'=>FALSE);
					break;

				case 'exchange_rate_usd_to_vnd':
					if(!$this->form_validation->is_natural($args['value']))
						return array('msg'=>'Số tiền không hợp lệ','success'=>FALSE);
					break;

                case 'exchange_rate_aud_to_vnd':
                    if(!$this->form_validation->is_natural($args['value']))
                        return array('msg'=>'Số tiền không hợp lệ','success'=>FALSE);
                    break;

				case 'network_type':
					if(empty($args['value'])) 
						return array('msg'=>'Vui lòng chọn ít nhất 1 kênh quảng cáo','success'=>FALSE);
					$network_types = $args['value'];

					$args['value'] = implode(',', $network_types);

					break;

				case 'payment_bank_account':
					if(empty($args['value'])) 
						return array('msg'=>'Vui lòng chọn một','success'=>FALSE);
					$network_types = $args['value'];
					
					$args['value'] = implode(',', $network_types);

					break;	

				 case 'payment_banner':
					if(empty($args['value'])) 
					{
						return array('msg'=>'Vui lòng chọn một','success'=>FALSE);
					}
					if (count($args['value'])>1) {
						return array('msg'=>'Chỉ được chọn một hình thức thanh toán','success'=>FALSE);
					}
					
					$network_types = $args['value'];	
					$args['value'] = implode(',', $network_types);

					break;		

				case 'user_id':

					$this->term_users_m
					->set_term_users($post['edit']['term_id'],array($args['value']),array('customer_person','customer_company'),array('command'=>'replace'));

					$this->term_m->update($post['edit']['term_id'], array('term_parent'=>0,'term_name'=>''));

					$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));

					$post['response'] = array('jscallback'=> $jsfunction);
					return $post;

					break;

				case 'term_type':

					$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));
					$post['response'] = array('jscallback'=> $jsfunction);
					break;
			}

			switch ($args['type']) {

				case 'meta_data':

					$timestamp_fields = array('contract_begin',
						'contract_end',
						'googleads-end_time',
						'googleads-begin_time',
						'started_service',
						'start_service_time',
						'end_service_time'
						);


					$is_field_type_timestamp = in_array($args['name'], $timestamp_fields);
					if($is_field_type_timestamp)
						$args['value'] = strtotime($args['value']);

					// CẬP NHẬT LẠI GÓI HOSTING
					if($args['name'] == 'hosting_service_package')
					{
						$this->config->load('hosting/hosting');
						$hosting_package_default = $this->config->item('service','packages');

						$selected_package = $hosting_package_default[$args['value']] ?? FALSE;
						update_term_meta($post['edit']['term_id'],'hosting_disk',($selected_package['disk']??0));
						update_term_meta($post['edit']['term_id'],'hosting_bandwidth',($selected_package['bandwidth']??0));
						update_term_meta($post['edit']['term_id'],'hosting_price',($selected_package['price']??0));
						update_term_meta($post['edit']['term_id'],'hosting_email_webmail',($selected_package['email_webmail']??0));

						$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));
						$post['response'] = array('jscallback'=> $jsfunction);
					}

					// CẬP NHẬT LẠI GÓI DOMAIN
					elseif($args['name'] == 'domain_service_package')
					{
						$this->config->load('domain/domain');

						$domain_package_default = $this->config->item('service','packages');

						$selected_package = $domain_package_default[$args['value']] ?? FALSE;
	
						// CẬP NHẬT LẠI DOMAIN
						update_term_meta($post['edit']['term_id'],'domain_fee_initialization',($selected_package['fee_initialization'] ?? 0));
						update_term_meta($post['edit']['term_id'],'domain_fee_maintain',($selected_package['fee_maintain'] ?? 0));
						update_term_meta($post['edit']['term_id'],'domain_fee_initialization_maintain',($selected_package['fee_maintain'] + $selected_package['fee_initialization'] ?? 0));

						// PHÍ KHỞI TẠO
					    $domain_fee_initialization  = get_term_meta_value($post['edit']['term_id'], 'domain_fee_initialization') ;

					    // PHÍ DUY TRÌ
					    $domain_fee_maintain        = get_term_meta_value($post['edit']['term_id'], 'domain_fee_maintain');

					    // PHÍ DUY TRÌ VÀ KHỞI TẠO NĂM ĐẦU TIÊN
					    $domain_fee_initialization_maintain = get_term_meta_value($post['edit']['term_id'], 'domain_fee_initialization_maintain');

					    // SỐ NĂM ĐĂNG KÝ
					    $domain_number_year_register       = get_term_meta_value($post['edit']['term_id'], 'domain_number_year_register') ?: 1 ;

					    // KIỂM TRA ĐÃ GIA HẠN HAY CHƯA
					    $has_domain_renew  = get_term_meta_value($post['edit']['term_id'], 'has_domain_renew') ?: 0;

					    // CẬP NHẬT GIÁ TRỊ HỢP ĐỒNG
					    $contract_value             = $domain_fee_initialization_maintain + ($domain_fee_maintain * ($domain_number_year_register - 1)) ;
						if($has_domain_renew == 1) $contract_value =  $domain_fee_maintain * $domain_number_year_register;
					    update_term_meta($post['edit']['term_id'], 'contract_value', $contract_value);

					    // THỜI GIAN BẮT ĐẦU HỢP ĐỒNG
					    $contract_begin             = get_term_meta_value($post['edit']['term_id'], 'contract_begin') ;

					    // CẬP NHẬT THỜI GIAN KẾT THÚC
					    $contract_end               = strtotime("+ $domain_number_year_register year", $contract_begin) ;
					    update_term_meta($post['edit']['term_id'], 'contract_end', $contract_end) ;
					
						$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));
						$post['response'] = array('jscallback'=> $jsfunction);
					}

					// CẬP NHẬT LẠI GÓI DOMAIN KHI CHỌN LẠI SỐ NĂM
					elseif($args['name'] == 'domain_number_year_register')
					{
						$this->config->load('domain/domain');

						// THÔNG TIN CÁC GÓI DOMAIN
			            $services = $this->config->item('service', 'packages') ;

			            // GÓI DOMAIN ĐƯỢC CHỌN
						$domain_service_package = get_term_meta_value($post['edit']['term_id'],'domain_service_package');

						// SỐ NĂM ĐĂNG KÝ
						$domain_number_year_register = $args['value'];

						// PHÍ KHỞI TẠO
					    $domain_fee_initialization  = get_term_meta_value($post['edit']['term_id'], 'domain_fee_initialization') ?: $services[$domain_service_package]['fee_initialization'];

					    // PHÍ DUY TRÌ
					    $domain_fee_maintain        = get_term_meta_value($post['edit']['term_id'], 'domain_fee_maintain') ?: $services[$domain_service_package]['fee_maintain'];

					    // PHÍ KHỞI TẠO VÀ DUY TRÌ NĂM ĐẦU TIÊN
					    $domain_fee_initialization_maintain = get_term_meta_value($post['edit']['term_id'], 'domain_fee_initialization_maintain') ?: $domain_fee_maintain + $domain_fee_initialization;

					    // KIỂM TRA ĐÃ GIA HẠN HAY CHƯA
					    $has_domain_renew  = get_term_meta_value($post['edit']['term_id'], 'has_domain_renew') ?: 0;

					    // CẬP NHẬT GIÁ TRỊ HỢP ĐỒNG
					    $contract_value             = $domain_fee_initialization_maintain + ($domain_fee_maintain * ($domain_number_year_register - 1)) ;
						if($has_domain_renew == 1) $contract_value =  $domain_fee_maintain * $domain_number_year_register;
    					update_term_meta($post['edit']['term_id'], 'contract_value', $contract_value);
						
						// THỜI GIAN BẮT ĐẦU HỢP ĐỒNG
					    $contract_begin             = get_term_meta_value($post['edit']['term_id'], 'contract_begin') ;

					    // CẬP NHẬT THỜI GIAN KẾT THÚC
					    $contract_end               = strtotime("+ $domain_number_year_register year", $contract_begin) ;
					    update_term_meta($post['edit']['term_id'], 'contract_end', $contract_end) ;

						$jsfunction = array(array('function_to_call'=>'reload_page','data'=> ''));
						$post['response'] = array('jscallback'=> $jsfunction);
					}					

					$post['edit']['meta'] = array($args['name']	=> $args['value']);
					break;

				case 'extra':

					$extra = @unserialize(get_term_meta_value($post['edit']['term_id'], 'extra'));
					if(empty($extra)) 
						$extra = array();
					$extra[$args['name']] = $args['value'];

					$post['edit']['meta']['extra'] = serialize($extra);
					break;

				case 'field':

					$post['edit'][$args['name']] = $args['value'];
					break;
			}

			return $post;
		});

		return $this;
	}

	public function render_customer_info($user_id){

		$this->load->model('customer/customer_m');

		$user = $this->customer_m->like('user_type', 'customer_', 'after')->where('user_id',$user_id)->get_by();

		if(empty($user)) return ;

		$html = '';

		if($user->user_type == 'customer_company')
		{
			$html.= $this->admin_form->hidden('','edit[meta][customer_id]',$user->user_id) .

			$this->admin_form->input('Tên tổ chức', '', $user->display_name,'',array('disabled'=>true)) .

			$this->admin_form->input('Người đại diện', '', 
				($this->usermeta_m->get_meta_value($user->user_id,'customer_gender') == 1 ? 'Ông ' : 'Bà ') . 
				$this->usermeta_m->get_meta_value($user->user_id,'customer_name'),'',array('disabled'=>true)) .

			$this->admin_form->input('Địa chỉ', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_address'),'',array('disabled'=>'disabled')) .

			$this->admin_form->input('E-mail', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_email'),'',array('disabled'=>'disabled')) .

			$this->admin_form->input('Số điện thoại', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_phone'),'',array('disabled'=>'disabled')) .

			$this->admin_form->input('Mã số thuế','',$this->usermeta_m->get_meta_value($user->user_id,'customer_tax'),'',array('disabled'=>'disabled'));

		}
		else if($user->user_type == 'customer_person'){

			$html.= $this->admin_form->hidden('','edit[meta][customer_id]',$user->user_id	) .

			$this->admin_form->input('Người đại diện', '', 
				($this->usermeta_m->get_meta_value($user->user_id,'customer_gender') == 1 ? 'Ông ' : 'Bà ') . 
				$user->display_name,'',array('disabled'=>'disabled')) .

			$this->admin_form->input('Địa chỉ', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_address'),'',array('disabled'=>'disabled')) .

			$this->admin_form->input('E-mail', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_email'),'',array('disabled'=>'disabled')) .

			$this->admin_form->input('Số điện thoại', '', $this->usermeta_m->get_meta_value($user->user_id,'customer_phone'),'',array('disabled'=>'disabled'));
		}

		if($this->input->is_ajax_request())
		{
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('content' => $html)));	

			return;
		}

		return $html;
	}

	public function check_domain()
	{
		$url = $this->input->post('domain');
		$result = $this->contract_m->check_domain($url);
		echo ($result ? 'true' : 'false');
	}
}
/* End of file Create_wizard.php */
/* Location: ./application/modules/contract/controllers/ajax/Create_wizard.php */