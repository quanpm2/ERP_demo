<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataset extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$models = array(
			'term_users_m',
			'contract/category_m',
			'contract/term_categories_m',
			'contract/receipt_m',
			'contract/contract_m',
		);

		$this->load->model($models);
		$this->load->library('datatable_builder');

		$this->load->config('googleads/contract');
	}

	/**
	 * Request bussiness category by contract value
	 *
	 * @return     json  chart-result
	 */
	public function categoriesValues()
	{
		$response 	= array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);
		
		$defaults	= array('start_time' => $this->mdate->startOfYear(),'end_time' => $this->mdate->endOfYear());
		$args 		= wp_parse_args( $this->input->get(), $defaults );
		$data 		= array();

		$key_cache = 'modules/contract/dataset-categoriesdetail-'.md5(json_encode($args));
		$dataset = $this->scache->get($key_cache);
		if(!$dataset)
		{
			$dataset = $this->term_categories_m
			->select('FROM_UNIXTIME(tmsst.meta_value,"%Y/%m/01") AS start_service_month')
			->select('COUNT(DISTINCT contract.term_id) AS term_quantity')
			->select('SUM(tmcv.meta_value)*COUNT(DISTINCT contract.term_id)/COUNT(contract.term_id) AS total_value')
			->select('SUM(tmpma.meta_value) * COUNT(DISTINCT contract.term_id) / COUNT(contract.term_id) AS total_payment_amount')
			->select('category.term_id AS cat_id')
			->select('category.term_name AS cat_name')
			->select('category.term_parent AS cat_parent_id')
			->select('tmsst.meta_value')

			->join('term category','category.term_id = term_categories.cat_id')
			->join('term contract','contract.term_id = term_categories.term_id')
			->join('termmeta tmcv','tmcv.term_id = contract.term_id AND tmcv.meta_key = "contract_value"')
			->join('termmeta tmsst','tmsst.term_id = contract.term_id AND tmsst.meta_key = "start_service_time"')
			->join('termmeta tmpma','tmpma.term_id = contract.term_id AND tmpma.meta_key = "payment_amount"')

			->where_in('contract.term_type',array_keys($this->config->item('taxonomy')))
			->where('tmsst.meta_value >=',$args['start_time'])
			->where('tmsst.meta_value <=',$args['end_time'])

			->group_by('FROM_UNIXTIME(tmsst.meta_value,"%Y-%m-01"),category.term_id')
			->order_by('start_service_month, category.term_id,category.term_name')
			->get_all();

			if(!empty($dataset)) $this->scache->write($dataset,$key_cache);
		}

		$start_time = $args['start_time'];
		$end_time 	= ($args['end_time'] < time()) ? $args['end_time'] : $this->mdate->endOfDay();

		$chart_categories		= array();
		$chart_series 			= array();
		$chart_series_default	= array();
		while ($start_time < $end_time)
		{
			$chart_categories[$start_time] = my_date($start_time,'M');
			$chart_series_default[$start_time] = 0;
			$start_time = strtotime('+1 month',$start_time);
		}

		$group_by_category = array_group_by($dataset,'cat_id');
		foreach ($group_by_category as $cat_id => $row)
		{
			$category = reset($row);
			$obj = array(
					'name' => $category->cat_name,
					'data' => $chart_series_default,
					'tooltip' => ['valueSuffix' => ' đ']
				);

			foreach ($row as $item)
			{
				$_date = $this->mdate->startOfDay($item->start_service_month);
				$obj['data'][$_date] = (int)$item->total_value;
			}

			$obj['data'] = array_values($obj['data']);

			$chart_series[] = $obj;
		}
		

		$data['chart']['series'] 	 = $chart_series;
		$data['chart']['title'] 	 = 'Xu hướng giá trị hợp đồng thực hiện theo ngành nghề từ '.my_date($args['start_time'],'m/Y').' đến '.my_date($args['end_time'],'m/Y');
		$data['chart']['categories'] = array_values($chart_categories);

		$response['data'] 		= $data;
		$response['msg']		= 'Xử lý dữ liệu thành công !';
		$response['success']	= TRUE;

		return parent::renderJson($response);
	}


	/**
	 * Request bussiness category by contract's quantity
	 *
	 * @return     json  chart-result
	 */
	public function categoriesQuantity()
	{
		$response 	= array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);
		
		$defaults	= array('start_time' => $this->mdate->startOfYear(),'end_time' => $this->mdate->endOfYear());
		$args 		= wp_parse_args( $this->input->get(), $defaults );
		$data 		= array();

		$key_cache = 'modules/contract/dataset-categoriesdetail-'.md5(json_encode($args));
		$dataset = $this->scache->get($key_cache);
		if(!$dataset)
		{
			$dataset = $this->term_categories_m

			->select('FROM_UNIXTIME(tmsst.meta_value,"%Y/%m/01") AS start_service_month')
			->select('COUNT(DISTINCT contract.term_id) AS term_quantity')
			->select('SUM(tmcv.meta_value)*COUNT(DISTINCT contract.term_id)/COUNT(contract.term_id) AS total_value')
			->select('SUM(tmpma.meta_value) * COUNT(DISTINCT contract.term_id) / COUNT(contract.term_id) AS total_payment_amount')
			->select('category.term_id AS cat_id')
			->select('category.term_name AS cat_name')
			->select('category.term_parent AS cat_parent_id')
			->select('tmsst.meta_value')

			->join('term category','category.term_id = term_categories.cat_id')
			->join('term contract','contract.term_id = term_categories.term_id')
			->join('termmeta tmcv','tmcv.term_id = contract.term_id AND tmcv.meta_key = "contract_value"')
			->join('termmeta tmsst','tmsst.term_id = contract.term_id AND tmsst.meta_key = "start_service_time"')
			->join('termmeta tmpma','tmpma.term_id = contract.term_id AND tmpma.meta_key = "payment_amount"')

			->where_in('contract.term_type',array_keys($this->config->item('taxonomy')))
			->where('tmsst.meta_value >=',$args['start_time'])
			->where('tmsst.meta_value <=',$args['end_time'])

			->group_by('FROM_UNIXTIME(tmsst.meta_value,"%Y-%m-01"),category.term_id')
			->order_by('start_service_month, category.term_id,category.term_name')
			->get_all();
			if(!empty($dataset)) $this->scache->write($dataset,$key_cache);
		}

		$start_time = $args['start_time'];
		$end_time 	= ($args['end_time'] < time()) ? $args['end_time'] : $this->mdate->endOfDay();

		$chart_categories		= array();
		$chart_series 			= array();
		$chart_series_default	= array();
		while ($start_time < $end_time)
		{
			$chart_categories[$start_time] = my_date($start_time,'M');
			$chart_series_default[$start_time] = 0;
			$start_time = strtotime('+1 month',$start_time);
		}

		$group_by_category = array_group_by($dataset,'cat_id');
		foreach ($group_by_category as $cat_id => $row)
		{
			$category = reset($row);
			$obj = array(
					'name' => $category->cat_name,
					'data' => $chart_series_default
				);

			foreach ($row as $item)
			{
				$_date = $this->mdate->startOfDay($item->start_service_month);
				$obj['data'][$_date] = (int)$item->term_quantity;
			}

			$obj['data'] = array_values($obj['data']);

			$chart_series[] = $obj;
		}
		

		$data['chart']['series'] 	 = $chart_series;
		$data['chart']['title'] 	 = 'Xu hướng Số lượng hợp đồng thực hiện theo ngành nghề từ '.my_date($args['start_time'],'m/Y').' đến '.my_date($args['end_time'],'m/Y');
		$data['chart']['categories'] = array_values($chart_categories);

		$response['data'] 		= $data;
		$response['msg']		= 'Xử lý dữ liệu thành công !';
		$response['success']	= TRUE;
		return parent::renderJson($response);
	}

	public function categoriesDetail()
	{
		$response 	= array('success'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);
		
		$defaults	= array('start_time' => $this->mdate->startOfYear(),'end_time' => $this->mdate->endOfYear());
		$args 		= wp_parse_args( $this->input->get(), $defaults );
		$data 		= array();

		$key_cache = 'modules/contract/dataset-categoriesdetail-'.md5(json_encode($args));
		$dataset = $this->scache->get($key_cache);
		if(!$dataset)
		{
			$dataset = $this->term_categories_m

			->select('FROM_UNIXTIME(tmsst.meta_value,"%Y/%m/01") AS start_service_month')
			->select('COUNT(DISTINCT contract.term_id) AS term_quantity')
			->select('SUM(tmcv.meta_value)*COUNT(DISTINCT contract.term_id)/COUNT(contract.term_id) AS total_value')
			->select('SUM(tmpma.meta_value) * COUNT(DISTINCT contract.term_id) / COUNT(contract.term_id) AS total_payment_amount')
			->select('category.term_id AS cat_id')
			->select('category.term_name AS cat_name')
			->select('category.term_parent AS cat_parent_id')
			->select('tmsst.meta_value')

			->join('term category','category.term_id = term_categories.cat_id')
			->join('term contract','contract.term_id = term_categories.term_id')
			->join('termmeta tmcv','tmcv.term_id = contract.term_id AND tmcv.meta_key = "contract_value"')
			->join('termmeta tmsst','tmsst.term_id = contract.term_id AND tmsst.meta_key = "start_service_time"')
			->join('termmeta tmpma','tmpma.term_id = contract.term_id AND tmpma.meta_key = "payment_amount"')

			->where_in('contract.term_type',array_keys($this->config->item('taxonomy')))
			->where('tmsst.meta_value >=',$args['start_time'])
			->where('tmsst.meta_value <=',$args['end_time'])

			->group_by('FROM_UNIXTIME(tmsst.meta_value,"%Y-%m-01"),category.term_id')
			->order_by('start_service_month, category.term_id,category.term_name')
			->get_all();
			
			if(!empty($dataset)) $this->scache->write($dataset,$key_cache);
		}

		$start_time = $args['start_time'];
		$end_time 	= ($args['end_time'] < time()) ? $args['end_time'] : $this->mdate->endOfDay();

		$chart_categories		= array();
		$chart_series 			= array();
		$chart_series_default	= array();
		while ($start_time < $end_time)
		{
			$chart_categories[$start_time] = my_date($start_time,'M');
			$chart_series_default[$start_time] = 0;
			$start_time = strtotime('+1 month',$start_time);
		}

		$table_tpl = array(
			'table_open' => '<table id="table-sparkline">',
			'tbody_open' => '<tbody id="tbody-sparkline">'
			);
		$this->table->set_template($table_tpl);
		$this->table->set_heading(['Ngành nghề','SL HĐ','Phân bổ','Doanh Thu','Phân bổ','Đã thu','Phân bổ']);

		$group_by_category = array_group_by($dataset,'cat_id');

		foreach ($group_by_category as $cat_id => $row)
		{
			$category = reset($row);

			$obj = array(
				'name' => $category->cat_name,
				'data' => [
					'total_value'=>$chart_series_default,
					'term_quantity'=>$chart_series_default,
					'total_payment_amount'=>$chart_series_default
					]
				);

			foreach ($row as $item)
			{
				$_date = $this->mdate->startOfDay($item->start_service_month);
				$obj['data']['total_value'][$_date] = (int)$item->total_value;
				$obj['data']['term_quantity'][$_date] = (int)$item->term_quantity;
				$obj['data']['total_payment_amount'][$_date] = (int)$item->total_payment_amount;
			}

			$total_term_quantity 	= array_sum(array_values($obj['data']['term_quantity']));
			$total_value 			= array_sum(array_values($obj['data']['total_value']));
			$total_payment_amount 	= array_sum(array_values($obj['data']['total_payment_amount'])); 
			$total_payment_amount_icon = $total_payment_amount >= $total_value ? '<i style="color:green" class="fa fa-fw fa-smile-o"></i>' : '<i style="color:red" class="fa fa-fw fa-meh-o"></i>';

			$tbrow = array(
				'category' => $category->cat_name,
				'term_quantity' => currency_numberformat($total_term_quantity,''),
				array('data-sparkline'=>implode(', ', $obj['data']['term_quantity']).'; line'),

				'total_value' => currency_numberformat($total_value,' đ'),
				array('data-sparkline'=>implode(', ', $obj['data']['total_value'])),

				'total_payment_amount' => currency_numberformat($total_payment_amount," đ {$total_payment_amount_icon}"),
				array('data-sparkline'=>implode(', ', $obj['data']['total_payment_amount']).'; column'),
			);

			$this->table->add_row($tbrow);
			$chart_series[] = $obj;
		}

		// die($this->table->generate());
		$data['chart']['tablechart'] = $this->table->generate();
		$data['chart']['series'] 	 = $chart_series;

		$response['data'] 		= $data;
		$response['msg']		= 'Xử lý dữ liệu thành công !';
		$response['success']	= TRUE;
		return parent::renderJson($response);
	}


	public function usersRevenue()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công, vui lòng thử lại !','data'=>[]);

		$users_terms = $this->prepare_userterms_revenue();
		if(empty($users_terms)) 
		{
			$response['msg'] = 'Dữ liệu không được tìm thấy .';
			return parent::renderJson($response);
		}

		$defaults = array('user_id' => '', 'group_id'=>'');
		$args = wp_parse_args( $this->input->get(), $defaults );

		$has_group_filter 	= $args['group_id'] != 'all' && !empty($args['group_id']);
		$has_user_filter 	= $args['user_id'] 	!= 'all' && !empty($args['user_id']);

		$terms 		= array();
		$user_ids 	= $args['user_id'];
		$group_ids  = $args['group_id'];

		if($has_user_filter && !is_array($args['user_id'])) $user_ids = array($args['user_id']);
		if($has_group_filter && !is_array($args['group_id'])) $group_ids = array($args['group_id']);

		foreach ($users_terms as $user_term)
		{
			/* Ignore user-term if its not belongs to any groups allowed */
			if($has_group_filter && !in_array($user_term['group_id'], $group_ids)) continue;

			/* Ignore user-term if its not belongs to any groups allowed */
			if($has_user_filter && !in_array($user_term['user_id'], $user_ids)) continue;

			$terms[] = $user_term;
		}

		/* User input không liên kết đến bất kỳ hơp đồng nào */
		if(empty($terms))
		{
			$response['msg'] = 'Dữ liệu không được tìm thấy .';
			return parent::renderJson($response);
		}

		/*Phân loại hợp đồng theo chờ duyệt | đang chạy | đang bảo lãnh | quá hạn bảo lãnh*/
		$terms = $this->terms_categorized($terms);

		$_total_invs_amount = array_sum(array_column($terms['all'], 'invs_amount'));
		$_total_payment_amount = array_sum(array_column($terms['all'], 'payment_amount'));
		$_total_payment_amount_remaining = array_sum(array_column($terms['all'], 'payment_amount_remaining'));

		$data = array(
			'summary' => array(
				'total' => [
					'payment_rate'	=> div($_total_payment_amount,$_total_invs_amount)*100,
					'invs_amount' 	=> $_total_invs_amount,
					'payment_amount'=> $_total_payment_amount,
					'payment_amount_remaining' 	=> $_total_payment_amount_remaining,
				],
				'bailment'			=> ['payment_amount_remaining' => 0,'payment_amount' => 0,'invs_amount' => 0, 'payment_rate' => 0],
				'bailment_expired'	=> ['payment_amount_remaining' => 0,'payment_amount' => 0,'invs_amount' => 0, 'payment_rate' => 0],
				'waitingforapprove' => ['payment_amount_remaining' => 0,'payment_amount' => 0,'invs_amount' => 0, 'payment_rate' => 0]
			),
			'terms' => array('waitingforapprove' => [],'bailment' => [],'bailment_expired' => [])
		);

		/* Filter hợp đồng đang chờ duyệt */
		if(!empty($terms['waitingforapprove']))
		{
			$_total_invs_amount = array_sum(array_column($terms['waitingforapprove'], 'invs_amount'));
			$_total_payment_amount = array_sum(array_column($terms['waitingforapprove'], 'payment_amount'));
			$_total_payment_amount_remaining = array_sum(array_column($terms['waitingforapprove'], 'payment_amount_remaining'));

			$data['summary']['waitingforapprove'] = array(
				'payment_rate'	=> div($_total_payment_amount,$_total_invs_amount)*100,
				'invs_amount' 	=> $_total_invs_amount,
				'payment_amount' => $_total_payment_amount,
				'payment_amount_remaining' 	=> $_total_payment_amount_remaining,
			);
			
			$data['terms']['waitingforapprove'] = $terms['waitingforapprove'];
		}


		/* Filter hợp đồng đang chạy và còn hạn bảo lãnh */
		if(!empty($terms['bailment']))
		{
			$_total_invs_amount = array_sum(array_column($terms['bailment'], 'invs_amount'));
			$_total_payment_amount = array_sum(array_column($terms['bailment'], 'payment_amount'));
			$_total_payment_amount_remaining = array_sum(array_column($terms['bailment'], 'payment_amount_remaining'));

			$data['summary']['bailment'] = array(
				'payment_rate'	=> div($_total_payment_amount,$_total_invs_amount)*100,
				'invs_amount' 	=> $_total_invs_amount,
				'payment_amount'=> $_total_payment_amount,
				'payment_amount_remaining' 	=> $_total_payment_amount_remaining,
			);
			
			$data['terms']['bailment'] = $terms['bailment'];
		}


		/* Filter hợp đồng đang chạy và đã quá hạn bảo lãnh */
		if(!empty($terms['bailment_expired']))
		{
			$_total_invs_amount = array_sum(array_column($terms['bailment_expired'], 'invs_amount'));
			$_total_payment_amount = array_sum(array_column($terms['bailment_expired'], 'payment_amount'));
			$_total_payment_amount_remaining = array_sum(array_column($terms['bailment_expired'], 'payment_amount_remaining'));

			$data['summary']['bailment_expired'] = array(
				'payment_rate'	=> div($_total_payment_amount,$_total_invs_amount)*100,
				'invs_amount' 	=> $_total_invs_amount,
				'payment_amount'=> $_total_payment_amount,
				'payment_amount_remaining' 	=> $_total_payment_amount_remaining,
			);
			
			$data['terms']['bailment_expired'] = $terms['bailment_expired'];
		}

		$response['status'] = TRUE;
		$response['msg'] = 'Dữ liệu tải thành công !';
		$response['data'] = $data;
		return parent::renderJson($response);
	}

	public function exportUsersRevenue()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công, vui lòng thử lại !','data'=>[]);

		$users_terms = $this->prepare_userterms_revenue();
		if(empty($users_terms)) 
		{
			$response['msg'] = 'Dữ liệu không được tìm thấy .';
			return parent::renderJson($response);
		}

		$defaults 		= array('user_id' => 'all','create_time'=>$this->mdate->startOfDay());
		$args 			= wp_parse_args( $this->input->get(), $defaults );		
		$args_encrypt 	= md5(json_encode($args));

		$terms = array();
		if($args['user_id'] == 'all')
		{
			$terms = $users_terms;
		}
		else if(!empty($args['user_id']))
		{
			$user_ids = $args['user_id'];
			$terms = array_filter($users_terms,function($x) use($user_ids) { return in_array($x['user_id'], $user_ids); });
		}

		/* User input không liên kết đến bất kỳ hơp đồng nào */
		if(empty($terms))
		{
			$response['msg'] = 'Dữ liệu không được tìm thấy .';
			return parent::renderJson($response);
		}

		/* Phân loại hợp đồng theo chờ duyệt | đang chạy | đang bảo lãnh | quá hạn bảo lãnh */
		$terms = $this->terms_categorized($terms);

		$this->load->library('excel');
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("WEBDOCTOR.VN")->setLastModifiedBy("WEBDOCTOR.VN")->setTitle(uniqid('Chi tiết dự thu __'));

        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $row_index = 3;

        $total_bailment 		 = (int)array_sum(array_column($terms['bailment'],'payment_amount_remaining'));
        $total_bailment_expired  = (int)array_sum(array_column($terms['bailment_expired'],'payment_amount_remaining'));
        $total_waitingforapprove = (int)array_sum(array_column($terms['waitingforapprove'],'payment_amount_remaining'));
        $total 					 = $total_bailment + $total_bailment_expired + $total_waitingforapprove;

        $total_summary = array(
        	array('Tổng cộng dự thu',$total ?: 0),
        	array('Dự thu hợp đồng mới (chờ duyệt)(1)',$total_waitingforapprove ?: 0),
        	array('Dự thu đang chạy (bảo lãnh đến hạn)(2)',$total_bailment ?: 0),
        	array('Dự thu hợp đồng mới (bảo lãnh quá hạn)(3)',$total_bailment_expired ?: 0)
        );
        $objWorksheet->fromArray($total_summary, NULL, "B{$row_index}");

        $row_ranges = $row_index + count($total_summary);
	    $objPHPExcel->getActiveSheet()->getStyle("C{$row_index}:C{$row_ranges}")->getNumberFormat()->setFormatCode("#,##0");
	    $objPHPExcel->getActiveSheet()->getStyle("B{$row_index}:C{$row_ranges}")
	    ->applyFromArray(['borders'=> ['allborders'=> ['style'=> PHPExcel_Style_Border::BORDER_THIN]]]);

        $row_index+= $row_ranges + 2;

        /* Dự thu hợp đồng mới - chờ duyệt */
        $headings = ['STT','Số hợp đồng','Khách hàng','Giá trị trước VAT','Phí dịch vụ','Thuế VAT','Dự thu','Còn phải thu','Tình trạng','Hạn bảo lãnh'];

        $objWorksheet->setCellValueByColumnAndRow(1, $row_index++, 'Chi tiết');

        if(!empty($terms['waitingforapprove']))
        {
        	$i = 1;
        	$data_waitingforapprove = array($headings);
        	$objWorksheet->setCellValueByColumnAndRow(1, $row_index++, 'Dự thu hơp đồng mới (chờ duyệt)');

        	foreach($terms['waitingforapprove'] as $term)
        	{
        		$data_waitingforapprove[] = array(
        			$i++,
        			$term['contract_code'],
        			$term['customer_name'],
        			$term['contract_value'],
        			$term['service_fee'],
        			$term['vat'],
        			$term['invs_amount'],
        			$term['payment_amount_remaining'],
        			'Chờ duyệt',
        			!empty($term['receipt_end_date']) ? PHPExcel_Shared_Date::PHPToExcel($term['receipt_end_date']) : ''
        		);
        	}

        	$data_waitingforapprove[] = array(
        		'','','Tổng cộng',
        		array_sum(array_column($terms['waitingforapprove'],'contract_value')),
        		array_sum(array_column($terms['waitingforapprove'],'service_fee')),
        		'',
        		array_sum(array_column($terms['waitingforapprove'],'invs_amount')),
        		array_sum(array_column($terms['waitingforapprove'],'payment_amount_remaining')),
        		'',
        		''
        	);

        	/* Append vào file excel table 'Dự thu hợp đồng mới' */
        	$objWorksheet->fromArray($data_waitingforapprove, NULL, "A{$row_index}");

        	$row_ranges = $row_index + count($data_waitingforapprove);
        	$objPHPExcel->getActiveSheet()->getStyle("A{$row_index}:J{$row_ranges}")->getNumberFormat()->setFormatCode("#,##0");
		    $objPHPExcel->getActiveSheet()->getStyle("A{$row_index}:J{$row_ranges}")
		    ->applyFromArray(['borders'=> ['allborders'=> ['style'=> PHPExcel_Style_Border::BORDER_THIN]]]);

		    $objPHPExcel->getActiveSheet()->getStyle("A{$row_index}:J{$row_index}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');

		    $objPHPExcel->getActiveSheet()->getStyle('A'.($row_ranges-1).':J'.($row_ranges-1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');

		    $objPHPExcel->getActiveSheet()->getStyle("J{$row_index}:J{$row_ranges}")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        	$row_index+= count($data_waitingforapprove) + 1;
        }


        if(!empty($terms['bailment']))
        {
        	$i = 1;
        	$data_bailment = array($headings);
        	$objWorksheet->setCellValueByColumnAndRow(1, $row_index++, 'Dự thu hơp đồng mới (chờ duyệt)');

        	foreach($terms['bailment'] as $term)
        	{
        		$data_bailment[] = array(
        			$i++,
        			$term['contract_code'],
        			$term['customer_name'],
        			$term['contract_value'],
        			$term['service_fee'],
        			$term['vat'],
        			$term['invs_amount'],
        			$term['payment_amount_remaining'],
        			'Đang chạy',
        			!empty($term['receipt_end_date']) ? PHPExcel_Shared_Date::PHPToExcel($term['receipt_end_date']) : ''
        		);
        	}

        	$data_bailment[] = array(
        		'','','Tổng cộng',
        		array_sum(array_column($terms['bailment'],'contract_value')),
        		array_sum(array_column($terms['bailment'],'service_fee')),
        		'',
        		array_sum(array_column($terms['bailment'],'invs_amount')),
        		array_sum(array_column($terms['bailment'],'payment_amount_remaining')),
        		'',
        		''
        	);

        	/* Append vào file excel table 'Dự thu hợp đồng mới' */
        	$objWorksheet->fromArray($data_bailment, NULL, "A{$row_index}");
        	
        	$row_ranges = $row_index + count($data_bailment);
        	$objPHPExcel->getActiveSheet()->getStyle("A{$row_index}:J{$row_ranges}")->getNumberFormat()->setFormatCode("#,##0");
		    $objPHPExcel->getActiveSheet()->getStyle("A{$row_index}:J{$row_ranges}")
		    ->applyFromArray(['borders'=> ['allborders'=> ['style'=> PHPExcel_Style_Border::BORDER_THIN]]]);

		    $objPHPExcel->getActiveSheet()->getStyle("A{$row_index}:J{$row_index}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');

		    $objPHPExcel->getActiveSheet()->getStyle('A'.($row_ranges-1).':J'.($row_ranges-1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');

		    $objPHPExcel->getActiveSheet()->getStyle("J{$row_index}:J{$row_ranges}")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        	$row_index+= count($data_bailment) + 1;
        }


        if(!empty($terms['bailment_expired']))
        {
        	$i = 1;
        	$data_bailment_expired = array($headings);
        	$objWorksheet->setCellValueByColumnAndRow(1, $row_index++, 'Dự thu hơp đồng mới (chờ duyệt)');

        	foreach($terms['bailment_expired'] as $term)
        	{
        		$data_bailment_expired[] = array(
        			$i++,
        			$term['contract_code'],
        			$term['customer_name'],
        			$term['contract_value'],
        			$term['service_fee'],
        			$term['vat'],
        			$term['invs_amount'],
        			$term['payment_amount_remaining'],
        			'Đang chạy',
        			!empty($term['receipt_end_date']) ? PHPExcel_Shared_Date::PHPToExcel($term['receipt_end_date']) : ''
        		);
        	}

        	$data_bailment_expired[] = array(
        		'','','Tổng cộng',
        		array_sum(array_column($terms['bailment_expired'],'contract_value')),
        		array_sum(array_column($terms['bailment_expired'],'service_fee')),
        		'',
        		array_sum(array_column($terms['bailment_expired'],'invs_amount')),
        		array_sum(array_column($terms['bailment_expired'],'payment_amount_remaining')),
        		'',
        		''
        	);

        	/* Append vào file excel table 'Dự thu hợp đồng mới' */
        	$objWorksheet->fromArray($data_bailment_expired, NULL, "A{$row_index}");

        	$row_ranges = $row_index + count($data_bailment_expired);
        	$objPHPExcel->getActiveSheet()->getStyle("A{$row_index}:J{$row_ranges}")->getNumberFormat()->setFormatCode("#,##0");
		    $objPHPExcel->getActiveSheet()->getStyle("A{$row_index}:J{$row_ranges}")
		    ->applyFromArray(['borders'=> ['allborders'=> ['style'=> PHPExcel_Style_Border::BORDER_THIN]]]);

		    $objPHPExcel->getActiveSheet()->getStyle("A{$row_index}:J{$row_index}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');

		    $objPHPExcel->getActiveSheet()->getStyle('A'.($row_ranges-1).':J'.($row_ranges-1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');

		    $objPHPExcel->getActiveSheet()->getStyle("J{$row_index}:J{$row_ranges}")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        	$row_index+= count($data_bailment_expired) + 1;
        }


        foreach(range('A','J') as $columnID)
        {
		    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

        // We'll be outputting an excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $folder_upload = "files/contracts/";
        if(!is_dir($folder_upload))
        {
            try 
            {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }


        $file_name = $folder_upload.'danh-sach-du-thu-hop-dong-'.my_date(time(),'Y-m-d')."{$args_encrypt}.xlsx";
       	try 
        {
            $objWriter->save($file_name);
        }
        catch (Exception $e) 
        {
            return parent::renderJson($response);
        }

		$response['status'] = TRUE;
		$response['msg'] = 'Dữ liệu tải thành công !';
		$response['data'] = $file_name;
		return parent::renderJson($response);
	}

	public function revenueUsersGroups()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công, vui lòng thử lại !','data'=>[]);

		$users_terms = $this->prepare_userterms_revenue();
		if(empty($users_terms)) 
		{
			$response['msg'] = 'Dữ liệu không được tìm thấy .';
			return parent::renderJson($response);
		}

		$user_group_terms = array_group_by($users_terms,'group_id');
		$data = array();
		$i = 0;
		foreach ($user_group_terms as $user_id => $user_terms)
		{
			$user_group_ins = reset($user_terms);
			$user_ins = reset($user_terms);

			$terms_categorized = $this->terms_categorized($user_terms);

			$payment_amount_remaining_waitingforapprove	= $terms_categorized['waitingforapprove'] ? array_sum(array_column($terms_categorized['waitingforapprove'], 'payment_amount_remaining')) : 0;

			$payment_amount_remaining_bailment	= $terms_categorized['waitingforapprove'] ? array_sum(array_column($terms_categorized['bailment'], 'payment_amount_remaining')) : 0;

			$payment_amount_remaining_bailment_expired	= $terms_categorized['waitingforapprove'] ? array_sum(array_column($terms_categorized['bailment_expired'], 'payment_amount_remaining')) : 0;

			$data[] = array(
				
				'increment' 	=> ++$i,
				'user_id' 		=> $user_id,
				'group_id'		=> $user_group_ins['group_id'],
				'group_name' 	=> $user_group_ins['group_name'],
				'group_count' 	=> $user_group_ins['group_count'],
				'group_description' => $user_group_ins['group_description'],

				'payment_amount_remaining_bailment' 			=> $payment_amount_remaining_bailment,
				'payment_amount_remaining_bailment_expired' 	=> $payment_amount_remaining_bailment_expired,
				'payment_amount_remaining_waitingforapprove'	=> $payment_amount_remaining_waitingforapprove,
			);
		}
		
		$response['status'] = TRUE;
		$response['msg'] = 'Dữ liệu tải thành công !';
		$response['data'] = $data;
		return parent::renderJson($response);
	}


	public function revenueUsers()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công, vui lòng thử lại !','data'=>[]);

		$users_terms = $this->prepare_userterms_revenue();
		if(empty($users_terms)) 
		{
			$response['msg'] = 'Dữ liệu không được tìm thấy .';
			return parent::renderJson($response);
		}

		/**
		 * Filter input get params by user_ids and group_ids
		 */
		$defaults = array('user_id' => '', 'group_id'=>'');
		$args = wp_parse_args( $this->input->get(), $defaults );

		$has_group_filter 	= $args['group_id'] != 'all' && !empty($args['group_id']);
		$has_user_filter 	= $args['user_id'] 	!= 'all' && !empty($args['user_id']);

		$terms 		= array();
		$user_ids 	= $args['user_id'];
		$group_ids  = $args['group_id'];

		if($has_user_filter && !is_array($args['user_id'])) $user_ids = array($args['user_id']);
		if($has_group_filter && !is_array($args['group_id'])) $group_ids = array($args['group_id']);

		foreach ($users_terms as $key => $user_term)
		{
			/* Ignore user-term if its not belongs to any groups allowed */
			if($has_group_filter && !in_array($user_term['group_id'], $group_ids))
			{
				unset($users_terms[$key]);
				continue;
			}

			/* Ignore user-term if its not belongs to any groups allowed */
			if($has_user_filter && !in_array($user_term['user_id'], $user_ids))
			{
				unset($users_terms[$key]);
				continue;
			}
		}

		$users_terms = array_group_by($users_terms,'user_id');

		$data = array();
		$i = 0;
		foreach ($users_terms as $user_id => $user_terms)
		{
			$user_ins = reset($user_terms);

			$terms_waitingforapprove = array_filter($user_terms,function($x){ return ($x['term_status']=='waitingforapprove' ? TRUE : FALSE); });
			$term_running = array_filter($user_terms,function($x){ return ($x['term_status'] != 'waitingforapprove' ? TRUE : FALSE); });

			$terms_bailment = array_filter($term_running,function($x){ return !$x['is_bailment_expired']; });
			$terms_bailment_expired = array_filter($term_running,function($x){ return $x['is_bailment_expired']; });

			$data[] = array(
				
				'increment' 	=> ++$i,
				'user_id' 		=> $user_id,
				'display_name' 	=> $user_ins['display_name'],

				'payment_amount_remaining_waitingforapprove'	=> array_sum(array_column($terms_waitingforapprove, 'payment_amount_remaining')),
				'payment_amount_remaining_bailment' 			=> array_sum(array_column($terms_bailment, 'payment_amount_remaining')),
				'payment_amount_remaining_bailment_expired' 	=> array_sum(array_column($terms_bailment_expired, 'payment_amount_remaining')),
			);
		}
		
		$response['status'] = TRUE;
		$response['msg'] = 'Dữ liệu tải thành công !';
		$response['data'] = $data;
		return parent::renderJson($response);
	}


	/**
	 * Query All terms by all user with revenue condition
	 *
	 * @return     array The result
	 */
	public function prepare_userterms_revenue()
	{
		$cache_key = 'modules/contract/users-terms-revenue-all';
		$users_terms = $this->scache->get($cache_key);
		// if( ! empty($users_terms)  && 1==2) return $users_terms;
		if( ! empty($users_terms)) return $users_terms;

		$terms = $this->contract_m->set_term_type()
		->select('term.term_id,term.term_name,term.term_status')
		->select('tinvs_amount.meta_value as invs_amount')
		->select('tinvs_amount_expired.meta_value as invs_amount_expired')
		->select('tpayment_amount.meta_value as payment_amount')
		->select('tpayment_percentage.meta_value as payment_percentage')

		->join('termmeta tpayment_percentage','tpayment_percentage.term_id = term.term_id AND tpayment_percentage.meta_key="payment_percentage"','LEFT')
		->join('termmeta tpayment_amount','tpayment_amount.term_id = term.term_id AND tpayment_amount.meta_key="payment_amount"','LEFT')
		->join('termmeta tinvs_amount_expired','tinvs_amount_expired.term_id = term.term_id AND tinvs_amount_expired.meta_key="invs_amount_expired"','LEFT')
		->join('termmeta tinvs_amount','tinvs_amount.term_id = term.term_id AND tinvs_amount.meta_key="invs_amount"','LEFT')
		->join('termmeta tcontract_end','tcontract_end.term_id = term.term_id AND tcontract_end.meta_key="contract_begin"','LEFT')
		->where('tcontract_end.meta_value >', $this->mdate->startOfYear())

		->where_in('term.term_status',['waitingforapprove','pending','publish'])
		
		->order_by('term.term_id','desc')
		->as_array()
		->get_all();

		if(empty($terms)) return FALSE;
		$term_ids = array_column($terms, 'term_id');

		/* List danh sách tất cả các bảo lãnh của các hơp đồng còn hiệu lực */
		$receipts = $this->receipt_m
		->select('posts.post_id, posts.end_date, term_posts.term_id')
		->select('postmeta.meta_value as amount')
		->join('term_posts', 'term_posts.post_id = posts.post_id', 'LEFT')
		->join('postmeta', 'postmeta.post_id = posts.post_id AND postmeta.meta_key = "amount"')
		->where('posts.post_status', 'publish')
		->where('posts.post_type', 'receipt_caution')
		->where_in('term_posts.term_id',$term_ids)
		->order_by('posts.end_date', 'desc')
		->as_array()
		->get_many_by();

		$is_bailment = !empty($receipts);

		/* Group các bảo lãnh theo hợp đồng */
		$terms_receipts = array_group_by($receipts,'term_id');


		/* TRUY VẤN DỮ LIỆU KHÁCH HÀNG THEO CÁC HỢP ĐỒNG ĐƯỢC FILTER */
		$this->load->model('term_users_m');
		$term_customers = $this->term_users_m
		->select('term_users.term_id, term_users.user_id')
		->join('user','user.user_id = term_users.user_id')
		->where_in('term_users.term_id',$term_ids)
		->where_in('user.user_type',['customer_person','customer_company'])
		->get_all();

		$term_customers = $term_customers ? key_value($term_customers,'term_id','user_id') : array();

		/* TRUY VẤN DỮ LIỆU KHÁCH HÀNG THEO CÁC HỢP ĐỒNG ĐƯỢC FILTER */
		$user_groups =$this->term_users_m
		->select('term_users.term_id, term_users.user_id,term.term_name,term.term_count,term.term_description')
		->join('term','term.term_id = term_users.term_id')
		->where_in('term.term_type','user_group')
		->get_all();

		$user_groups = $user_groups ? array_column($user_groups, NULL,'user_id') : array();
		foreach ($terms as $key => &$term)
		{
			$term_id = $term['term_id'];

			$staff_business 		= (int) get_term_meta_value($term_id, 'staff_business');
			$term['customer_id'] 	= $term_customers[$term_id] ?? 0;
			$term['customer_name'] 	= $this->admin_m->get_field_by_id($term['customer_id'],'display_name');

			$term['user_id'] 		= $staff_business;
			$term['display_name'] 	= $this->admin_m->get_field_by_id($staff_business,'display_name');
			$term['user_email'] 	= $this->admin_m->get_field_by_id($staff_business,'user_email');

			$term['group_id']		= 0;
			$term['group_name']		= 0;
			$term['group_count']	= 0;
			$term['group_description'] = '';
			if(!empty($user_groups[$staff_business]))
			{
				$term['group_id']		= $user_groups[$staff_business]->term_id;
				$term['group_name']		= $user_groups[$staff_business]->term_name;
				$term['group_count']	= $user_groups[$staff_business]->term_count;
				$term['group_description']	= $user_groups[$staff_business]->term_description;
			}

			$term['vat'] 			= (double) get_term_meta_value($term_id, 'vat');
			$term['service_fee'] 	= (double) get_term_meta_value($term_id, 'service_fee');
			$term['contract_code'] 	= get_term_meta_value($term_id, 'contract_code');
			$term['contract_value'] = (double) get_term_meta_value($term_id, 'contract_value');

			$term['payment_amount'] 		= (double) $term['payment_amount'];
			$term['payment_percentage'] 	= (double) $term['payment_percentage'];
			$term['invs_amount'] 			= (double) $term['invs_amount'];
			$term['invs_amount_expired']	= (double) $term['invs_amount_expired'];
			$term['bailment_amount']		= 0;

			$term['receipt_end_date']		= 0;

			$term['is_bailment']				= 0;
			$term['is_bailment_expired']		= 0;
			$term['is_waitingforapprove']		= $term['term_status'] == 'waitingforapprove' ? 1 : 0;
			$term['payment_amount_remaining'] 	= $term['invs_amount'] - $term['payment_amount'];


			if( ! $is_bailment) continue;
			if(empty($terms_receipts[$term_id])) continue;

			$term['is_bailment'] 		= 1;
			$term['bailment_amount']	= array_sum(array_column($terms_receipts[$term_id],'amount'));
			
			$receipt_instance 			= reset($terms_receipts[$term_id]);
			$term['receipt_end_date']	= $receipt_instance['end_date'] < time() ? 1 : $receipt_instance['end_date'];
		}

		$this->scache->write($terms,$cache_key,3600);

		return $terms;
	}


	/**
	 * Categorize the terms by all | waitingforapprove | bailment | bailment_expired
	 *
	 * @param      array  $terms  The terms
	 *
	 * @return     array  The result
	 */
	private function terms_categorized($terms = array())
	{
		if(empty($terms)) return FALSE;

		$data = array('all' => $terms,'waitingforapprove'=>[],'running'=>[],'bailment'=>[],'bailment_expired'=>[]);

		foreach ($terms as $term)
		{
			if($term['term_status'] == 'waitingforapprove')
			{
				$data['waitingforapprove'][] = $term;
				continue;
			}

			$data['running'] = $term;

			if($term['is_bailment_expired'])
			{
				$data['bailment_expired'][] = $term;
				continue;
			}

			$data['bailment'][] = $term;
		}

		return $data;
	}

	/**
	 * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
	 *
	 * @param      string  $query  The query
	 *
	 * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
	 */
	public function export_receipt($query = '')
	{
		if(empty($query)) return FALSE;

		// remove limit in query string
		$pos = strpos($query, 'LIMIT');
		if(FALSE !== $pos) $query = mb_substr($query, 0, $pos);

		$posts = $this->receipt_m->query($query)->result();
		if( ! $posts)
		{
			$this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
			redirect(current_url(),'refresh');
		}

		$this->load->library('excel');
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
        ->getProperties()
        ->setCreator("WEBDOCTOR.VN")
        ->setLastModifiedBy("WEBDOCTOR.VN")
        ->setTitle(uniqid('Danh sách thanh toán __'));

        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/receipt/receipt-list-export.xlsx');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $receipt_type_default 		= $this->config->item('type','receipt');
        $transfer_type_default 		= $this->config->item('transfer_type','receipt');
        $transfer_account_default 	= $this->config->item('transfer_account','receipt');

        $row_index = 3;

        $posts = array_map(function($x){
        	$x->term_id = null;
        	$term_ids 	= $this->term_posts_m->select('term_id')->get_the_terms($x->post_id)
        	AND $x->term_id = reset($term_ids);
        	return $x;
        }, $posts);

        $contracts = $this->contract_m->set_term_type()->where_in('term_id', array_unique(array_column($posts, 'term_id')))->get_many_by();
        ! empty($contracts) AND $contracts = array_column($contracts, NULL, 'term_id');

        $typeContracts 		= array_group_by($contracts, 'term_type');
        $webdoctorContracts = [];

        ! empty($typeContracts['webdesign']) AND $webdoctorContracts = array_merge($typeContracts['webdesign']);
        ! empty($typeContracts['oneweb']) AND $webdoctorContracts = array_merge($typeContracts['oneweb']);

        $this->load->model('webgeneral/webgeneral_kpi_m');
        $kpis = array();

        if( ! empty($webdoctorContracts)
        	&& $targets = $this->webgeneral_kpi_m->select('kpi_id, term_id, user_id, kpi_type')->order_by('kpi_datetime')->where_in('term_id', array_column($webdoctorContracts, 'term_id'))->as_array()->get_all())
        {
        	$targets = array_group_by($targets, 'term_id');

        	foreach ($targets as $_term_id => $kpi)
        	{
        		$typeKpi = array_group_by($kpi, 'kpi_type');
        		foreach ($typeKpi as $type => $_kpis)
        		{
        			$contracts[$_term_id]->{$type} = implode(', ', array_map(function($_kpi){
        				return $this->admin_m->get_field_by_id($_kpi['user_id'], 'display_name') ?: $this->admin_m->get_field_by_id($_kpi['user_id'], 'user_email');
        			}, $_kpis));
        		}
        	}
        }

        foreach ($posts as $key => &$post)
        {
        	$term = $contracts[$post->term_id];

        	// Get customer display name
        	$customer = '';
			if($term_users = $this->term_users_m->get_term_users($post->term_id, ['customer_person','customer_company','system']))
			{
				$customer = end($term_users);
				$customer = mb_ucwords($this->admin_m->get_field_by_id($customer->user_id,'display_name'));
			}

			// Get contract code
			$contract_code = get_term_meta_value($post->term_id,'contract_code');

			// Nhân viên kinh doanh phụ trách
			$sale_id = (int) get_term_meta_value($post->term_id,'staff_business');
			$sale = $this->admin_m->get_field_by_id($sale_id,'user_email') ?: '--';

			// Số tiền đã thu
			$post->amount = (double) get_post_meta_value($post->post_id,'amount');

			// VAT của hợp đồng
			$vat = (int) get_term_meta_value($post->term_id,'vat');

			// Doanh thu (-VAT)
			$post->amount_not_vat = div($post->amount, (1+div($vat, 100)));
			// $post->amount_not_vat = $post->amount - $post->amount*(div($vat,100));

			// Loại hình thanh toán
			$transfer_type = get_post_meta_value($post->post_id,'transfer_type');
			$transfer_account = get_post_meta_value($post->post_id,'transfer_account');
			$transfer_by = $transfer_type_default[$transfer_type].' '.($transfer_account_default[$transfer_account] ?? $transfer_account);

			$row_number = $row_index + $key;

			$date = $this->mdate->endOfDay($post->end_date);

			// Set Cell
    		$objWorksheet->setCellValueByColumnAndRow(0, $row_number, PHPExcel_Shared_Date::PHPToExcel($date));
    		$objWorksheet->setCellValueByColumnAndRow(1, $row_number, $customer);
    		$objWorksheet->setCellValueByColumnAndRow(2, $row_number, $contract_code);
    		$objWorksheet->setCellValueByColumnAndRow(3, $row_number, $post->amount);
    		$objWorksheet->setCellValueByColumnAndRow(4, $row_number, $sale);
    		$objWorksheet->setCellValueByColumnAndRow(5, $row_number, div($vat,100));
    		$objWorksheet->setCellValueByColumnAndRow(6, $row_number, $post->amount_not_vat);
    		$objWorksheet->setCellValueByColumnAndRow(7, $row_number, $this->config->item($term->term_type,'taxonomy'));
    		$objWorksheet->setCellValueByColumnAndRow(8, $row_number, $transfer_by);
    		$objWorksheet->setCellValueByColumnAndRow(9, $row_number, $receipt_type_default[$post->post_type]);

    		if(in_array($term->term_type, ['webdesign', 'oneweb']))
    		{
    			$objWorksheet->setCellValueByColumnAndRow(10, $row_number, $term->tech ?? '');
    			$objWorksheet->setCellValueByColumnAndRow(11, $row_number, $term->design ?? '');
    		}
        }

        $numbers_of_post = count($posts);

        // Set Cells style for Date
        $objPHPExcel->getActiveSheet()
        	->getStyle('A'.$row_index.':A'.($numbers_of_post+$row_index))
        	->getNumberFormat()
	    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY); // Set Cells style for Date
        $objPHPExcel->getActiveSheet()
        	->getStyle('A'.$row_index.':A'.($numbers_of_post+$row_index))
        	->getNumberFormat()
	    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

	    // Set Cells Style For Amount
	    $objPHPExcel->getActiveSheet()
        	->getStyle('D'.$row_index.':D'.($numbers_of_post+$row_index))
        	->getNumberFormat()
	    	->setFormatCode("#,##0");

	    // Set Cells Style For VAT
	    $objPHPExcel->getActiveSheet()
        	->getStyle('F'.$row_index.':F'.($numbers_of_post+$row_index))
        	->getNumberFormat()
	    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);

	    // Set Cells Style For Amount without VAT
	    $objPHPExcel->getActiveSheet()
        	->getStyle('G'.$row_index.':G'.($numbers_of_post+$row_index))
        	->getNumberFormat()
	    	->setFormatCode("#,##0");

	    // Calc sum amount rows and sum amout not vat rows
	    $total_amount = array_sum(array_column($posts, 'amount'));
	    $total_amount_not_vat = array_sum(array_column($posts, 'amount_not_vat'));

	    // Set cell label summary
	    $objWorksheet->setCellValueByColumnAndRow(1, ($row_index + $numbers_of_post + 2), 'Tổng cộng');

	    // Set cell sumary amount and amount not vat on TOP
	    $objWorksheet->setCellValueByColumnAndRow(3, 1, $total_amount);
	    $objWorksheet->setCellValueByColumnAndRow(6, 1, $total_amount_not_vat);

	    // Set cell sumary amount and amount not vat on BOTTOM
	    $objWorksheet->setCellValueByColumnAndRow(3, ($row_index + $numbers_of_post + 2), $total_amount);
	    $objWorksheet->setCellValueByColumnAndRow(6, ($row_index + $numbers_of_post + 2), $total_amount_not_vat);
	    
	    // Set Cells Style For Amount without VAT
	    $objPHPExcel->getActiveSheet()
        	->getStyle('D'.($numbers_of_post+$row_index+2))
        	->getNumberFormat()
	    	->setFormatCode("#,##0");

		// Set Cells Style For Amount without VAT
	    $objPHPExcel->getActiveSheet()
        	->getStyle('G'.($numbers_of_post+$row_index+2))
        	->getNumberFormat()
	    	->setFormatCode("#,##0");

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $file_name = "tmp/export-du-thu.xlsx";
        $objWriter->save($file_name);

       	try { $objWriter->save($file_name); }
        catch (Exception $e) { trigger_error($e->getMessage()); return FALSE;  }

		if(file_exists($file_name))
        {	
			$this->load->helper('download');
			force_download($file_name,NULL);
        }

		return FALSE;
	}

	public function export($query = '')
	{
		if(empty($query)) return FALSE;

		// remove limit in query string
		$query = explode('LIMIT', $query);
		$query = reset($query);
		
		$terms = $this->contract_m->query($query)->result();
		if( ! $terms) return FALSE;

		$this->config->load('contract/fields');
		$default 	= array('columns' => implode(',', $this->config->item('default_columns', 'datasource')));
		$args 		= wp_parse_args( $this->input->get(), $default);
		$args['columns'] = explode(',', $args['columns']);

		$this->load->library('excel');
		$cacheMethod 	= PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings 	= array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("WEBDOCTOR.VN")->setLastModifiedBy("WEBDOCTOR.VN")->setTitle(uniqid('Danh sách hợp đồng __'));
        $objPHPExcel->setActiveSheetIndex(0);

        $objWorksheet = $objPHPExcel->getActiveSheet();

        $callback 		= $this->contract_m->get_field_callback();
		$field_config 	= $this->contract_m->get_field_config();

        $headings = array_map(function($x) use($field_config){ return $field_config['columns'][$x]['title']??$x;}, $args['columns']);
        $objWorksheet->fromArray($headings, NULL, 'A1');

        $row_index = 2;
        foreach ($terms as $key => &$term)
		{	
			$term = (array) $term;
			foreach ($args['columns'] as $column)
			{
				if(empty($field_config['columns'][$column])) continue;
				if(empty($callback[$column])) continue;

				$term = call_user_func_array($callback[$column]['func'], array($term, $column, []));
			}

			$i = 0;
			$row_number = $row_index + $key;

			foreach ($args['columns'] as $column)
			{
				if(empty($field_config['columns'][$column])) continue;

				$value = $term["{$column}_raw"] ?? ($term["{$column}"] ?? '');
				$col_number = $i;
				$i++;

				switch ($field_config['columns'][$column]['type'])
				{
					case 'string' : 
						$objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
						break;

					case 'number': 
						$objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
						break;

					case 'timestamp': 
						$value = PHPExcel_Shared_Date::PHPToExcel($value);
						$objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
						break;

					default: break;
				}

				@$objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $value);
			}
		}

		$num_rows = count($terms);
		
        // We'll be outputting an excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $folder_upload = "files/contracts/";
        if(!is_dir($folder_upload))
        {
            try 
            {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $created_datetime 	= my_date(time(),'Y-m-d-H-i-s');
        $file_name 			= "{$folder_upload}danh-sach-hop-dong-{$created_datetime}.xlsx";

       	try { $objWriter->save($file_name); }
        catch (Exception $e) { trigger_error($e->getMessage()); return FALSE;  }

		$this->load->helper('download');
		force_download($file_name, NULL);
		return TRUE;
	}


	/**
	 * API cung cấp dữ liệu phiếu thanh toán
	 *
	 * @return     JSON
	 */
	public function receipt()
	{
		$response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

		if( ! has_permission('contract.receipt.access'))
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		$defaults 	= ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
		$args 		= wp_parse_args( $this->input->get(), $defaults);

		$this->load->config('contract');
		$this->load->config('contract/receipt');
		$this->load->config('customer/customer');

		$this->load->model('staffs/department_m');
		$this->load->model('staffs/user_group_m');
		$this->load->model('staffs/term_users_m');
		$this->load->model('staffs/sale_m');

		$sales = $this->sale_m->set_user_type()->set_role()->get_all();
		$sales = $sales ? key_value($sales,'user_id','user_email') : [];

		$data = $this->data;
		$this->search_filter_receipt();
		
		$sum_amount_not_vat = 0;
		$receipt_status = $this->config->item('status','receipt');

		/* Lấy tất cả phòng ban và convert về dạng array với key = id và value = tên phòng ban */
		$departments = $this->department_m->select('term_id,term_name')->set_term_type()->as_array()->get_all();
		$departments = $departments ? key_value($departments, 'term_id', 'term_name') : [];
		/* 
		 * Lấy tất cả nhóm và convert về dạng array với key = id và value = tên phòng ban
		 * Đồng thời group từng nhóm lại theo đúng tên phòng ban chứa nhóm đấy
		 */
		$user_groups = $this->user_group_m->select('term_id,term_name,term_parent')->set_term_type()->as_array()->get_all();
		if( ! empty($user_groups))
		{
			$_user_groups = array();
			foreach (array_group_by($user_groups, 'term_parent') as $department_id => $groups)
			{
				if(empty($departments[$department_id])) continue;

				$_user_groups[$departments[$department_id]] = key_value($groups, 'term_id', 'term_name');
			}

			$user_groups = $_user_groups;
		}
		
		$data['content'] = $this->datatable_builder
		->set_filter_position(FILTER_TOP_OUTTER)
		->select('posts.post_id,posts.post_author,posts.post_title')	

		->add_column('all_checkbox',array('title'=> '<input type="checkbox" name="all-checkbox" value="" id="all-checkbox" class="minimal"/>','set_order'=> FALSE, 'set_select' => FALSE))
		->add_search('end_date',['placeholder'=>'Ngày thanh toán','class'=>'form-control set-datepicker'])
		->add_search('contract_code',['placeholder'=>'Số hợp đồng'])
		->add_search('customer',['placeholder'=>'Khách hàng'])
		->add_search('sale_email',['placeholder'=>'Email kinh doanh'])

		->add_search('term_type',array('content'=> form_dropdown(array('name'=>'where[term_type]','class'=>'form-control select2'),prepare_dropdown($this->config->item('taxonomy'),'Loại dịch vụ'),$this->input->get('where[term_type]'))))

		->add_search('post_type',array('content'=> form_dropdown(array('name'=>'where[post_type]','class'=>'form-control select2'),prepare_dropdown($this->config->item('type','receipt'),'Loại thanh toán'),$this->input->get('where[post_type]'))))

		->add_search('post_status',array('content'=> form_dropdown(array('name'=>'where[post_status]','class'=>'form-control select2'),prepare_dropdown($receipt_status['all'],'Tình trạng'),$this->input->get('where[post_status]'))))

		->add_search('transfer_type',array('content'=> form_dropdown(array('name'=>'where[transfer_type]','class'=>'form-control select2'),prepare_dropdown($this->config->item('transfer_type','receipt'),'Hình thức chuyển'),$this->input->get('where[transfer_type]'))))

		->add_search('departments',array('content'=> form_dropdown(array('name'=>'where[departments]','class'=>'form-control select2'),prepare_dropdown($departments,'Phòng ban'),$this->input->get('where[departments]'))))

		->add_search('user_groups',array('content'=> form_dropdown(array('name'=>'where[user_groups]','class'=>'form-control select2'),prepare_dropdown($user_groups,'Nhóm'),$this->input->get('where[user_groups]'))))

		->add_search('action',['content'=>form_button(['name'=>'notification_of_payment','class'=>'btn btn-info', 'id'=>'send-mail-payment'],'<i class="fa fa-fw fa-send"></i> Email')])

		->add_column('posts.end_date',array('title'=> 'Ngày thanh toán','set_order'=> TRUE))
		->add_column('contract_code',array('title'=>'Hợp đồng','set_select'=>FALSE,'set_order'=>FALSE))
		->add_column('amount',array('title'=> 'Số tiền(vnđ)','set_order'=> TRUE,'set_select'=>FALSE))
		->add_column('vat',array('title'=> 'VAT','set_order'=> FALSE,'set_select'=>FALSE))
		->add_column('amount_not_vat',array('title'=> 'Doanh thu(vnđ)','set_order'=> FALSE,'set_select'=>FALSE))
		->add_column('term_type',array('title'=> 'Dịch vụ','set_order'=> TRUE,'set_select'=>FALSE))
		->add_column('posts.post_type',array('title'=> 'Loại','set_order'=> TRUE))
		->add_column('posts.post_status',array('title'=> 'Trạng thái','set_order'=> TRUE))
		->add_column('is_order_printed',array('title'=> 'In HĐ','set_order'=> TRUE,'set_select'=>FALSE))
		->add_column('transfer_type',array('title'=> 'Hình thức','set_order'=> TRUE,'set_select'=>FALSE))

		->add_column_callback('post_id',function($data,$row_name) use ($sales) {

			$post_id = $data['post_id'];
			
			// Check send mail
			$data['all_checkbox'] = '<input type="checkbox" name="post_ids[]" value="'.$post_id.'" class="minimal"/>' ;
			$has_send_mail_payment = get_post_meta_value($post_id, 'has_send_mail_payment');
			if($has_send_mail_payment == 1)
			{
				$data['all_checkbox'] = '<input type="checkbox" name="nocheck-post_ids[]" value="" class="minimal" checked="checked" disabled="disabled"/>' ;	
			}
		
			// Ngày thanh toán | Hạn chót bảo lãnh
			$data['end_date'] = my_date($data['end_date'],'d/m/Y');

			$taxonomies = $this->config->item('taxonomy');
			$taxonomies = array_keys($taxonomies);
			$term_id 	= $this->term_posts_m->get_the_terms($post_id, $taxonomies);
			if( ! empty($term_id)) $term_id = reset($term_id);

			$data['contract_code'] = '<b>'.get_term_meta_value($term_id,'contract_code').'</b><span class="clearfix"></span>';
			$sale_id = (int) get_term_meta_value($term_id,'staff_business');

			$user_group = '--';
			$department = '--';
			if( ! empty($sale_id))
			{
				$user_groups = $this->term_users_m->get_user_terms($sale_id, $this->user_group_m->term_type);
				if( ! empty($user_groups)) $user_group = implode(', ', array_column($user_groups, 'term_name'));

				$departments = $this->term_users_m->get_user_terms($sale_id, $this->department_m->term_type);
				if( ! empty($departments)) $department = implode(', ', array_column($departments, 'term_name'));
			}

			// $sale_email = $sales[$sale_id] ?? '--';
			$info = !empty($sales[$sale_id])? $sales[$sale_id] .' / ' . $user_group .' / '. $department : '--';
			$data['contract_code'].= "<span class='text-muted col-xs-12' data-toggle='tooltip' title='NVKD / Nhóm / Phòng ban'><i class='fa fa-fw fa-user'></i>{$info}</span>";

			// Get customer display name
			if($customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company','system']))
			{
				$customer = end($customers);
				$display_name = mb_ucwords($this->admin_m->get_field_by_id($customer->user_id,'display_name'));
				$data['contract_code'].= br()."<span class='text-muted col-xs-12' data-toggle='tooltip' title='Khách hàng'><i class='fa fa-fw fa-user-secret'></i>{$display_name}</span>";
			}

			// Hình thức thanh toán
			$this->load->config('contract/receipt');
			$def_transfer_type = $this->config->item('transfer_type','receipt');
			$transfer_type = get_post_meta_value($post_id,'transfer_type');

			$data['transfer_type'] = $def_transfer_type[$transfer_type] ?? reset($def_transfer_type);
			// tài khoản
			if($transfer_type == 'account')
			{
				$transfer_account = get_post_meta_value($post_id,'transfer_account');
				$transferAccountEnums = $this->config->item('transfer_account', 'receipt');
				$data['transfer_type'] = $transferAccountEnums[$transfer_account] ?? '';
			}

			// Số tiền thanh toán
			$amount = get_post_meta_value($post_id,'amount');
			$data['amount'] = currency_numberformat($amount,'');



			$vat = (int) get_term_meta_value($term_id,'vat');
			// $data['vat'] = currency_numberformat($vat,'%');
			$data['vat'] = empty($vat) ? 'KCT' : "{$vat}%";
			
			$amount_not_vat = div($amount, (1+div($vat, 100)));
			$data['amount_not_vat'] = currency_numberformat($amount_not_vat,'');

			$term = $this->term_m->get($term_id);
			$data['term_type'] = $this->config->item($term->term_type,'taxonomy');

			// Ngày thanh toán
			$payment_time = get_post_meta_value($post_id,'payment_time');
			$data['payment_time'] = my_date($payment_time,'d/m/Y');

			$data['post_author'] = $this->admin_m->get_field_by_id($data['post_author'],'user_email');

			$def_post_status = $this->config->item('status','receipt');
			$data['post_status'] = $def_post_status[$data['post_type']][$data['post_status']];

			$post_type = $data['post_type'];
			$def_post_type = $this->config->item('type','receipt');
			$data['post_type'] = $def_post_type[$post_type];

			$is_order_printed = (bool) get_post_meta_value($post_id,'is_order_printed');
			$data['is_order_printed'] = $is_order_printed ? '<i class="fa fw fa-check-square-o"></i>' : '<i class="fa fw fa-square-o"></i>';

			if($post_type == 'receipt_caution')
			{
				$data['end_date'].= br().'<span class="text-yellow">'.$def_post_type[$post_type].'</span>';
			}

			return $data;

		},FALSE)

		->from('posts')
		->where('posts.post_status !=','draft')
		->where_in('posts.post_type',['receipt_payment','receipt_caution'])
		//->generate(['per_page' => 100]);
		->group_by('posts.post_id');

		$pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

		// $data['sum_amount'] = array('amount'=> $this->datatable_builder->sum('postmeta','post_id','meta_value',array('meta_key'=>'amount'),array('amount'=>0))) ;

		$data = $this->datatable_builder->generate($pagination_config);

		// OUTPUT : DOWNLOAD XLSX
		if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
		{
			$this->export_receipt($last_query);
			return TRUE;
		}

		$this->template->title->append('Danh sách các đợt thanh toán đã thu');
		return parent::renderJson($data);
	}

	/**
	 * Xử lý các điều kiện filter từ GET
	 */
	protected function search_filter_receipt($args = array())
	{
		restrict('contract.revenue.access');

		$args = $this->input->get();	
		//if(empty($args) && $this->input->get('search')) $args = $this->input->get();	
		if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);
		
		//departments FILTERING & SORTING
		$filter_departments = $args['where']['departments'] ?? FALSE;
		$filter_user_groups = $args['where']['user_groups'] ?? FALSE;

		if(!empty($filter_departments) || !empty($filter_user_groups)){
			
			$user_ids = array();
			if( ! empty($filter_departments) && $_users = $this->term_users_m->get_the_users($filter_departments, 'admin'))
			{
				$user_ids = array_column($_users, 'user_id');
			}

			if( ! empty($filter_user_groups))
			{
				$_args 	= $user_ids ?: array('where_in'=>['term_users.user_id'=>$user_ids]);
				$_users = $this->term_users_m->get_term_users($filter_user_groups, 'admin', $_args);
				$user_ids = $_users ? array_column($_users, 'user_id') : [];
			}

			if(empty($user_ids)) $this->datatable_builder->where('post.post_id', uniqid());

			$alias_post_contract 	= uniqid('term_posts_');
			$alias_contract_users 	= uniqid('term_users_');
			$alias_meta = uniqid('staff_business_');

			$this->datatable_builder
			->join("term_posts {$alias_post_contract}","posts.post_id = {$alias_post_contract}.post_id")
			->join("termmeta {$alias_meta}","{$alias_meta}.term_id = {$alias_post_contract}.term_id and {$alias_meta}.meta_key = 'staff_business'")
			->where_in("{$alias_meta}.meta_value", $user_ids);
		}	

		// contract_daterange FILTERING & SORTING
		$filter_contract_daterange = $args['where']['contract_daterange'] ?? FALSE;
		$sort_contract_daterange = $args['order_by']['contract_daterange'] ?? FALSE;
		if($filter_contract_daterange || $sort_contract_daterange)
		{
			$alias = uniqid('contract_daterange_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

			if($sort_contract_daterange)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_daterange);
				unset($args['order_by']['contract_daterange']);
			}
		}

		// Contract_begin FILTERING & SORTING
		if( ! empty($args['where']['end_date']))
		{
			$dates = explode(' - ', $args['where']['end_date']);
			$this->datatable_builder
				->where("posts.end_date >=", $this->mdate->startOfDay(reset($dates)))
				->where("posts.end_date <=", $this->mdate->endOfDay(end($dates)));
		}
		
		if( ! empty($args['order_by']['end_date']))
		{
			$this->datatable_builder->order_by("posts.end_date",$args['order_by']['end_date']);
		}

		// Contract_code FILTERING & SORTING
		if(!empty($args['where']['contract_code']))
		{
			$termmetas = $this->termmeta_m->select('term_id')
				->like('meta_value',$args['where']['contract_code'])
				->get_many_by(['meta_key'=>'contract_code']);

			$this->datatable_builder
				->join('term_posts','term_posts.post_id = posts.post_id')
				->where_in('term_posts.term_id', ($termmetas ? array_column($termmetas, 'term_id') : [0]));
		}
		// Customer display_name FILTERING & SORTING
		if(!empty($args['where']['customer']))
		{
			$this->load->model('customer/customer_m');
			$customers = $this->customer_m
				->select('user.user_id')
				->where_in('user.user_type',$this->customer_m->customer_types)
				->like('user.display_name',trim($args['where']['customer']))
				->get_many_by();

			$this->datatable_builder
				->join('term_posts','term_posts.post_id = posts.post_id')
				->join('term_users','term_users.term_id = term_posts.term_id')
				->where_in('term_users.user_id', ($customers ? array_column($customers, 'user_id') : [0]));
		}

		// Email kinh doanh FILTERING & SORTING
		$filter_sale_email = $args['where']['sale_email'] ?? FALSE;
		$sort_sale_email = $args['order_by']['sale_email'] ?? FALSE;
		if($filter_sale_email || $sort_sale_email)
		{
			$alias = uniqid('staff_business_');
			$this->datatable_builder
				->join('term_posts','term_posts.post_id = posts.post_id')
				->join("termmeta {$alias}","{$alias}.term_id = term_posts.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT');

			if($filter_sale_email)
			{	
				$sales = $this->admin_m
				->select('user.user_id')
				->set_get_admin()
				->like('user.user_email',trim($args['where']['sale_email']))
				->get_many_by();

				$this->datatable_builder->where_in("{$alias}.meta_value",($sales ? array_column($sales, 'user_id') : ['-1']));
			}

			if($sort_sale_email)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_sale_email);
			}
		}

		// Email kinh doanh FILTERING & SORTING
		$filter_vat = $args['where']['vat'] ?? FALSE;
		$sort_vat = $args['order_by']['vat'] ?? FALSE;
		if($filter_vat || $sort_vat)
		{
			$alias = uniqid('vat_');
			$this->datatable_builder
				->join('term_posts','term_posts.post_id = posts.post_id')
				->join("termmeta {$alias}","{$alias}.term_id = term_posts.term_id and {$alias}.meta_key = 'vat'",'LEFT');

			if($filter_vat)
			{	
				$this->datatable_builder->where("{$alias}.meta_value",$filter_vat);
			}

			if($sort_vat)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_vat);
			}
		}

		// Loại thanh toán FILTERING & SORTING
		if( ! empty($args['where']['post_type']))
		{
			$this->datatable_builder->where('posts.post_type',$args['where']['post_type']);
		}

		if( ! empty($args['where']['post_type']))
		{
			$this->datatable_builder->order_by('posts.post_type',$args['where']['post_type']);
		}


		// Trạng thái thanh toán FILTERING & SORTING
		if( ! empty($args['where']['post_status']))
		{
			$this->datatable_builder->where('posts.post_status',$args['where']['post_status']);
		}

		if( ! empty($args['where']['post_status']))
		{
			$this->datatable_builder->order_by('posts.post_status',$args['where']['post_status']);
		}

		// Tình trạng In hóa đơn FILTERING & SORTING
		$filter_is_order_printed = $args['where']['is_order_printed'] ?? FALSE;
		$sort_is_order_printed = $args['order_by']['is_order_printed'] ?? FALSE;
		if($filter_is_order_printed || $sort_is_order_printed)
		{
			$alias = uniqid('is_order_printed_');
			$this->datatable_builder
				->join('term_posts','term_posts.post_id = posts.post_id')
				->join("termmeta {$alias}","{$alias}.term_id = term_posts.term_id and {$alias}.meta_key = 'is_order_printed'",'LEFT');

			if($filter_is_order_printed)
			{	
				$this->datatable_builder->where("{$alias}.meta_value",$filter_is_order_printed);
			}

			if($sort_is_order_printed)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_is_order_printed);
			}
		}


		// Hình thức chuyển tiền FILTERING & SORTING
		$filter_transfer_type = $args['where']['transfer_type'] ?? FALSE;
		$sort_transfer_type = $args['order_by']['transfer_type'] ?? FALSE;
		if($filter_transfer_type || $sort_transfer_type)
		{
			$alias = uniqid('transfer_type_');
			$this->datatable_builder
			->join("postmeta {$alias}","{$alias}.post_id = posts.post_id and {$alias}.meta_key = 'transfer_type'",'LEFT');

			if($filter_transfer_type)
			{	
				$this->datatable_builder->where("{$alias}.meta_value",$filter_transfer_type);
			}

			if($sort_transfer_type)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_transfer_type);
			}
		}

		// Loại hình dịch vụ FILTERING & SORTING
		$filter_term_type = $args['where']['term_type'] ?? FALSE;
		$sort_term_type = $args['order_by']['term_type'] ?? FALSE;
		if($filter_term_type || $sort_term_type)
		{
			$this->datatable_builder
			->join('term_posts','term_posts.post_id = posts.post_id')
			->join('term','term.term_id = term_posts.term_id','LEFT');

			if($filter_term_type)
			{	
				$this->datatable_builder->where('term.term_type',$filter_term_type);
			}

			if($sort_term_type)
			{
				$this->datatable_builder->order_by('term.term_type',$sort_term_type);
			}
		}

		if( ! empty($args['order_by']['amount']))
		{
			$alias = uniqid('amount_');
			$this->datatable_builder
				->join("postmeta {$alias}","{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'amount'")
				->order_by("({$alias}.meta_value * 1)",$args['order_by']['amount']);
		}
	}
	/**
	 * SUBMIT HANDER
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	protected function submit()
	{
		$get_params = $this->input->get();

		if($this->input->get('export') !== NULL)
		{
			$last_query = $this->scache->get("modules/contract/receipt/index-last-query-{$this->admin_m->id}");
			if( ! $last_query)
			{
				$this->messages->info('Không tìm thấy lệnh đã truy vấn trước đó , vui lòng thử lại sau');
				redirect(current_url(),'refresh');	
			}

			$posts = $this->datatable_builder->query($last_query)->result();
			if( ! $posts)
			{
				$this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
				redirect(current_url(),'refresh');
			}

			$this->load->library('excel');
			$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	        $cacheSettings = array( 'memoryCacheSize' => '512MB');
	        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	        $objPHPExcel = new PHPExcel();
	        $objPHPExcel
	        ->getProperties()
	        ->setCreator("WEBDOCTOR.VN")
	        ->setLastModifiedBy("WEBDOCTOR.VN")
	        ->setTitle(uniqid('Danh sách thanh toán __'));

	        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/receipt/receipt-list-export.xlsx');

	        $objPHPExcel->setActiveSheetIndex(0);
	        $objWorksheet = $objPHPExcel->getActiveSheet();

	        $receipt_type_default = $this->config->item('type','receipt');
	        $transfer_type_default = $this->config->item('transfer_type','receipt');
	        $transfer_account_default = $this->config->item('transfer_account','receipt');

	        $row_index = 3;

	        foreach ($posts as $key => &$post)
	        {
	        	$term_id = 0;
				if($term_ids = $this->term_posts_m->select('term_id')->get_the_terms($post->post_id))
				{
					$term_id = reset($term_ids);
				}

				$term = $this->term_m->get($term_id);

	        	// Get customer display name
	        	$customer = '';
				if($term_users = $this->term_users_m->get_term_users($term_id, ['customer_person','customer_company','system']))
				{
					$customer = end($term_users);
					$customer = mb_ucwords($this->admin_m->get_field_by_id($customer->user_id,'display_name'));
				}

				// Get contract code
				$contract_code = get_term_meta_value($term_id,'contract_code');

				// Nhân viên kinh doanh phụ trách
				$sale_id = (int) get_term_meta_value($term_id,'staff_business');
				$sale = $this->admin_m->get_field_by_id($sale_id,'user_email') ?: '--';

				// Số tiền đã thu
				$post->amount = (double) get_post_meta_value($post->post_id,'amount');

				// VAT của hợp đồng
				$vat = (int) get_term_meta_value($term_id,'vat');

				// Doanh thu (-VAT)
				$post->amount_not_vat = $post->amount - $post->amount*(div($vat,100));

				// Loại hình thanh toán
				$transfer_type = get_post_meta_value($post->post_id,'transfer_type');
				$transfer_account = get_post_meta_value($post->post_id,'transfer_account');
				$transfer_by = $transfer_type_default[$transfer_type].' '.($transfer_account_default[$transfer_account] ?? $transfer_account);

				$row_number = $row_index + $key;

				$date = $this->mdate->endOfDay($post->end_date);

				// Set Cell
	    		$objWorksheet->setCellValueByColumnAndRow(0, $row_number, PHPExcel_Shared_Date::PHPToExcel($date));
	    		$objWorksheet->setCellValueByColumnAndRow(1, $row_number, $customer);
	    		$objWorksheet->setCellValueByColumnAndRow(2, $row_number, $contract_code);
	    		$objWorksheet->setCellValueByColumnAndRow(3, $row_number, $post->amount);
	    		$objWorksheet->setCellValueByColumnAndRow(4, $row_number, $sale);
	    		$objWorksheet->setCellValueByColumnAndRow(5, $row_number, div($vat,100));
	    		$objWorksheet->setCellValueByColumnAndRow(6, $row_number, $post->amount_not_vat);
	    		$objWorksheet->setCellValueByColumnAndRow(7, $row_number, $this->config->item($term->term_type,'taxonomy'));
	    		$objWorksheet->setCellValueByColumnAndRow(8, $row_number, $transfer_by);
	    		$objWorksheet->setCellValueByColumnAndRow(9, $row_number, $receipt_type_default[$post->post_type]);
	        }

	        $numbers_of_post = count($posts);

	        // Set Cells style for Date
	        $objPHPExcel->getActiveSheet()
	        	->getStyle('A'.$row_index.':A'.($numbers_of_post+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

		    // Set Cells Style For Amount
		    $objPHPExcel->getActiveSheet()
	        	->getStyle('D'.$row_index.':D'.($numbers_of_post+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode("#,##0");

		    // Set Cells Style For VAT
		    $objPHPExcel->getActiveSheet()
	        	->getStyle('F'.$row_index.':F'.($numbers_of_post+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);

		    // Set Cells Style For Amount without VAT
		    $objPHPExcel->getActiveSheet()
	        	->getStyle('G'.$row_index.':G'.($numbers_of_post+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode("#,##0");

		    // Calc sum amount rows and sum amout not vat rows
		    $total_amount = array_sum(array_column($posts, 'amount'));
		    $total_amount_not_vat = array_sum(array_column($posts, 'amount_not_vat'));

		    // Set cell label summary
		    $objWorksheet->setCellValueByColumnAndRow(1, ($row_index + $numbers_of_post + 2), 'Tổng cộng');

		    // Set cell sumary amount and amount not vat on TOP
		    $objWorksheet->setCellValueByColumnAndRow(3, 1, $total_amount);
		    $objWorksheet->setCellValueByColumnAndRow(6, 1, $total_amount_not_vat);

		    // Set cell sumary amount and amount not vat on BOTTOM
		    $objWorksheet->setCellValueByColumnAndRow(3, ($row_index + $numbers_of_post + 2), $total_amount);
		    $objWorksheet->setCellValueByColumnAndRow(6, ($row_index + $numbers_of_post + 2), $total_amount_not_vat);
		    
		    // Set Cells Style For Amount without VAT
		    $objPHPExcel->getActiveSheet()
	        	->getStyle('D'.($numbers_of_post+$row_index+2))
	        	->getNumberFormat()
		    	->setFormatCode("#,##0");

			// Set Cells Style For Amount without VAT
		    $objPHPExcel->getActiveSheet()
	        	->getStyle('G'.($numbers_of_post+$row_index+2))
	        	->getNumberFormat()
		    	->setFormatCode("#,##0");

	        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

	        $file_name = "tmp/export-du-thu.xlsx";
	        $objWriter->save($file_name);

	        if(file_exists($file_name))
	        {	
				$this->load->helper('download');
				force_download($file_name,NULL);
	        }

			return FALSE;
		}

		if($this->input->get('notification_of_payment') !== NULL)
		{
			restrict('Contract.notification_payment.manage');

			unset($get_params['notification_of_payment']);
			$current_query 	= http_build_query($get_params);
			
			$post_ids 		= $get_params['post_ids'] ?? '';
			if(empty($post_ids)) 
			{
				$this->messages->error('Dữ liệu input không hợp lệ.');
				redirect(module_url("receipt?{$current_query}"), 'refresh');
				return FALSE ;
			}

			$this->load->model('contract/contract_report_m');
			foreach ($post_ids as $post_id)
			{
				$result = $this->contract_report_m->send_email_successful_payment($post_id);
				if($result) update_post_meta($post_id, 'has_send_mail_payment', 1);

				/* Send SMS thông báo khách hàng */
				$this->contract_report_m->send_sms_successful_payment($post_id);
			}

			$this->messages->success('Hệ thống đã gửi email thành công !');
			redirect(module_url("receipt?{$current_query}"), 'refresh');
		}
	}

	/**
	 * Liệt kê danh sách hợp đồng
	 *
	 * @return     json
	 */
	public function index()
	{
		$response = array('status'=>FALSE, 'msg'=>'Quá trình xử lý không thành công.', 'data'=>[]);

		if( ! has_permission('admin.contract.view'))
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}
		
		$this->config->load('contract/fields');
		$default = array(
			'offset' => 0,
			'per_page' => 50,
			'cur_page' => 1,
			'is_filtering' => TRUE,
			'is_ordering' => TRUE,
			'columns' => implode(',', $this->config->item('default_columns', 'datasource'))
		);
		$args 		= wp_parse_args( $this->input->get(), $default);
		$args['columns'] = explode(',', $args['columns']);
		
		$data = $this->data; // remove-after

		$relate_users 	= $this->admin_m->get_all_by_permissions('admin.contract.view');

		// LOGGED USER NOT HAS PERMISSION TO ACCESS
		if($relate_users === FALSE)
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		if(is_array($relate_users))
		{
			$this->datatable_builder->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
		}

		$scopedTermTypes = array_filter($this->config->item('remap_term_types'), function($taxonomy, $type){
			return has_permission("contract.{$taxonomy}.access");
		}, ARRAY_FILTER_USE_BOTH);

		$scopedTermTypes OR $scopedTermTypes = $this->config->item('remap_term_types');

		$this->config->load('customer/customer');
		
		/* Applies get query params for filter */
		$this->search_filter();

		$this->datatable_builder->set_filter_position(FILTER_TOP_OUTTER)
		->select('term.term_id,term.term_name, term.term_type, term.term_status')
		->add_search('contract_code',['placeholder'=>'Số hợp đồng'])
		->add_search('term.term_name',['placeholder'=>'Website'])
		->add_search('customer_code',['placeholder'=>'Mã Khách hàng'])
		->add_search('created_on',['placeholder'=>'Ngày tạo','class'=>'form-control input_daterange'])
		->add_search('verified_on',['placeholder'=>'Ngày Cấp số','class'=>'form-control input_daterange'])
		->add_search('contract_begin',['placeholder'=>'Ngày bắt đầu HĐ','class'=>'form-control input_daterange'])
		->add_search('contract_end',['placeholder'=>'Ngày kết thúc HĐ','class'=>'form-control input_daterange'])
		->add_search('start_service_time',['placeholder'=>'T/g Thực hiện','class'=>'form-control input_daterange'])		
		->add_search('end_service_time', ['placeholder'=>'Ngày kết thúc dịch vụ','class'=>'form-control input_daterange'])
		->add_search('staff_business',['placeholder'=>'Kinh doanh phụ trách'])
		->add_search('created_by',['placeholder'=>'Người tạo'])

		->add_search('term_type', array(
			'content'=> form_dropdown(array(
				'name'=>'where[term_type]','class'=>'form-control select2'
			), prepare_dropdown($this->config->item('services'), 'Tất cả dịch vụ'),$this->input->get('where[term_type]'))))

		->add_search('typeOfService', array(
				'content'=> form_dropdown(array(
					'name'=>'where[typeOfService]','class'=>'form-control select2'
				), prepare_dropdown($this->config->item('items', 'typeOfServices'), 'Loại Hình : Tất cả'),$this->input->get('where[typeOfService]'))))

		->add_search('is_first_contract',array('content'=> form_dropdown(array('name'=>'where[is_first_contract]','class'=>'form-control select2'),[ ''=>'Tất cả',1=>'Ký mới'],$this->input->get('where[is_first_contract]'))))

		->add_search('term_status',array('content'=> form_dropdown(array('name'=>'where[term_status]','class'=>'form-control select2'),prepare_dropdown($this->config->item('contract_status'),'Trạng thái HĐ : Tất cả'), $this->input->get('where[term_status]'))))

		->add_search('service_fee_payment_type',array('content'=> form_dropdown(array('name'=>'where[service_fee_payment_type]','class'=>'form-control select2'),prepare_dropdown($this->config->item('service_fee_payment_type'),'PTTT.PDV: Tất cả'), $this->input->get('where[service_fee_payment_type]'))));

		$columns = $this->config->item('columns', 'datasource');
		foreach ($args['columns'] as $key)
		{
			if(empty($columns[$key])) continue;

			$this->datatable_builder->add_column($key, $columns[$key]);
		}

		$this->datatable_builder->add_column('action', array('set_select'=>FALSE,'title'=>'Actions','set_order'=>FALSE));

		foreach ($this->contract_m->get_field_callback() as $callback)
		{
			$this->datatable_builder->add_column_callback($callback['field'], $callback['func'], $callback['row_data']);
		}

		$this->datatable_builder->add_column_callback('action', function($data, $row_data){

			$term_id = $data['term_id'];

			if(has_permission('admin.contract.edit'))
			{
				$data['action'] = anchor(module_url("create_wizard/index/{$term_id}"),'<i class="fa fa-fw fa-edit"></i>','title="Cập nhật hợp đồng" class="btn btn-default btn-xs"');
			}

			if(has_permission('admin.contract.add'))
			{
				$is_type_adsplus = in_array($data['term_type'], array('google-ads'));
				$is_contract_active = in_array($data['term_status'], array('liquidation','publish','pending','ending'));
				if($is_type_adsplus && $is_contract_active)
				{
					$data['action'].= anchor(module_url("clone/{$data['term_id']}"),'<i class="fa fa-fw fa-copy"></i>','title="Tạo bản sao chép" class="btn btn-default btn-xs" data-toggle="confirmation"');
				}
			}

			return $data;

		},  FALSE);

		$status_list = array_keys($this->config->item('contract_status'));
		array_unshift($status_list, 1);

		$this->datatable_builder
		->where_in('term_status', $status_list)
		->where_in('term_type', array_keys($scopedTermTypes))
		->from('term')
		->group_by('term.term_id')
        ->order_by('term.term_id', 'DESC');

		$pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

		$data = $this->datatable_builder->generate($pagination_config);
		
		// OUTPUT : DOWNLOAD XLSX
		if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
		{
			$this->export($last_query);
			return TRUE;
		}


		$this->template->title->set('Quản lý hợp đồng');
		$this->template->description->set('Trang danh sách tất cả các hợp đồng');

		return parent::renderJson($data);
	}

	protected function search_filter($search_args = array())
	{
		$args = $this->input->get(NULL, TRUE);

		if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

		// Loại hình dịch vụ FILTERING & SORTING
		$filter_typeOfService = $args['where']['typeOfService'] ?? FALSE;
		$sort_typeOfService = $args['order_by']['typeOfService'] ?? FALSE;
		if($filter_typeOfService || $sort_typeOfService)
		{
			$alias = uniqid('typeOfService_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'typeOfService'", 'LEFT');

			if($filter_typeOfService)
			{	
				$this->datatable_builder->where("{$alias}.meta_value", $filter_typeOfService);
				unset($args['where']['typeOfService']);
			}

			if($sort_typeOfService)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value", $sort_typeOfService);
				unset($args['order_by']['typeOfService']);
			}
		}
		
		// contract_daterange FILTERING & SORTING
		$filter_contract_daterange = $args['where']['contract_daterange'] ?? FALSE;
		$sort_contract_daterange = $args['order_by']['contract_daterange'] ?? FALSE;
		if($filter_contract_daterange || $sort_contract_daterange)
		{
			$alias = uniqid('contract_daterange_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

			if($sort_contract_daterange)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_daterange);
				unset($args['order_by']['contract_daterange']);
			}
		}

		// Contract_code FILTERING & SORTING
		$filter_contract_code = $args['where']['contract_code'] ?? FALSE;
		$sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
		if($filter_contract_code || $sort_contract_code)
		{
			$alias = uniqid('contract_code_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

			if($filter_contract_code)
			{	
				$this->datatable_builder->like("{$alias}.meta_value",$filter_contract_code);
			}

			if($sort_contract_code)
			{
				$this->datatable_builder->order_by("({$alias}.meta_value * 1)",$sort_contract_code);
			}

			unset($args['where']['contract_code'],$args['order_by']['contract_code']);
		}


		// Contract_begin FILTERING & SORTING
		$filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
		$sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
		if($filter_contract_begin || $sort_contract_begin)
		{
			$alias = uniqid('contract_begin_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

			if($filter_contract_begin)
			{	
				$dates = explode(' - ', $filter_contract_begin);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['contract_begin']);
			}

			if($sort_contract_begin)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_begin);
				unset($args['order_by']['contract_begin']);
			}
		}

		// Contract_end FILTERING & SORTING
		$filter_contract_end = $args['where']['contract_end'] ?? FALSE;
		$sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
		if($filter_contract_end || $sort_contract_end)
		{
			$alias = uniqid('contract_end_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_end'", 'LEFT');

			if($filter_contract_end)
			{	
				$dates = explode(' - ', $filter_contract_end);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['contract_end']);
			}

			if($sort_contract_end)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_end);
				unset($args['order_by']['contract_end']);
			}
		}

		// Contract_begin FILTERING & SORTING
		$filter_start_service_time = $args['where']['start_service_time'] ?? FALSE;
		$sort_start_service_time = $args['order_by']['start_service_time'] ?? FALSE;
		if($filter_start_service_time || $sort_start_service_time)
		{
			$alias = uniqid('start_service_time_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'start_service_time'", 'LEFT');

			if($filter_start_service_time)
			{	
				$dates = explode(' - ', $filter_start_service_time);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['start_service_time']);
			}

			if($sort_start_service_time)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_start_service_time);
				unset($args['order_by']['start_service_time']);
			}
		}

		// end_service_time FILTERING & SORTING
		$filter_end_service_time = $args['where']['end_service_time'] ?? FALSE;
		$sort_end_service_time = $args['order_by']['end_service_time'] ?? FALSE;
		if($filter_end_service_time || $sort_end_service_time)
		{
			$alias = uniqid('end_service_time_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'end_service_time'", 'LEFT');

			if($filter_end_service_time)
			{	
				$dates = explode(' - ', $filter_end_service_time);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['end_service_time']);
			}

			if($sort_end_service_time)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_end_service_time);
				unset($args['order_by']['end_service_time']);
			}
		}

		// Created_on FILTERING & SORTING
		$filter_created_on = $args['where']['created_on'] ?? FALSE;
		$sort_created_on = $args['order_by']['created_on'] ?? FALSE;
		if($filter_created_on || $sort_created_on)
		{
			$alias = uniqid('created_on_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'created_on'", 'LEFT');

			if($filter_created_on)
			{	
				$dates = explode(' - ', $filter_created_on);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['created_on']);
			}

			if($sort_created_on)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_created_on);
				unset($args['order_by']['created_on']);
			}
		}
		
		// verified_on FILTERING & SORTING
		$filter_verified_on = $args['where']['verified_on'] ?? FALSE;
		$sort_verified_on = $args['order_by']['verified_on'] ?? FALSE;
		if($filter_verified_on || $sort_verified_on)
		{
			$alias = uniqid('verified_on_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'verified_on'", 'LEFT');

			if($filter_verified_on)
			{	
				$dates = explode(' - ', $filter_verified_on);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['verified_on']);
			}

			if($sort_verified_on)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_verified_on);
				unset($args['order_by']['verified_on']);
			}
		}


		// Contract_code FILTERING & SORTING
		$filter_staff_business = $args['where']['staff_business'] ?? FALSE;
		$sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
		if($filter_staff_business || $sort_staff_business)
		{
			$alias = uniqid('staff_business_');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT')
			->join("user tblcustomer","{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

			if($filter_staff_business)
			{	
				$this->datatable_builder->like("tblcustomer.display_name",$filter_staff_business);
				unset($args['where']['staff_business']);
			}

			if($sort_staff_business)
			{
				$this->datatable_builder->order_by('tblcustomer.display_name',$filter_staff_business);
				unset($args['order_by']['staff_business']);
			}
		}


		// Contract_code FILTERING & SORTING
		$filter_created_by = $args['where']['created_by'] ?? FALSE;
		$sort_created_by = $args['order_by']['created_by'] ?? FALSE;
		if($filter_created_by || $sort_created_by)
		{
			$alias = uniqid('created_by_');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'created_by'", 'LEFT')
			->join("user tblcreateby","{$alias}.meta_value = tblcreateby.user_id", 'LEFT');

			if($filter_created_by)
			{	
				$this->datatable_builder->like("tblcreateby.display_name",$filter_created_by);
				unset($args['where']['created_by']);
			}

			if($sort_created_by)
			{
				$this->datatable_builder->order_by('tblcreateby.display_name',$filter_created_by);
				unset($args['order_by']['created_by']);
			}
		}

		// customer_code FILTERING & SORTING
        $filter_customer_code = $args['where']['customer_code'] ?? FALSE;
        $sort_customer_code   = $args['order_by']['customer_code'] ?? FALSE;
        if($filter_customer_code || $sort_customer_code)
        {
            $alias = uniqid('customer_code_');
            $this->datatable_builder
            ->join('term_users AS tu_customer', 'tu_customer.term_id = term.term_id', 'LEFT')
            ->join('user AS customer', 'customer.user_id = tu_customer.user_id AND customer.user_type IN ("customer_person", "customer_company")', 'LEFT')
            ->join("usermeta {$alias}","{$alias}.user_id = customer.user_id and {$alias}.meta_key = 'cid'", 'LEFT');

            if($filter_customer_code)
            {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_customer_code);
                unset($args['where']['customer_code']);
            }

            if($sort_customer_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_customer_code);
                unset($args['order_by']['customer_code']);
            }
        }

        // service_fee_payment_type FILTERING & SORTING
        $filter_service_fee_payment_type = $args['where']['service_fee_payment_type'] ?? FALSE;
        if($filter_service_fee_payment_type)
        {
            $alias = uniqid('service_fee_payment_type_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'service_fee_payment_type'", 'LEFT');
			$this->datatable_builder->where("{$alias}.meta_value", $filter_service_fee_payment_type);

            unset($args['where']['service_fee_payment_type']);
        }


		if(!empty($args['order_by']['contract_value']))
		{
			$this->datatable_builder->join('termmeta AS termmeta_contract_value', 'termmeta_contract_value.term_id = term.term_id', 'LEFT OUTER');
			$this->datatable_builder->where('termmeta_contract_value.meta_key', 'contract_value');
			$args['order_by']['(termmeta_contract_value.meta_value * 1)'] = $args['order_by']['contract_value'];
			unset($args['order_by']['contract_value']);
		};

		if(!empty($args['order_by']['vat']))
		{
			$alias = uniqid('vat_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'vat'", 'LEFT');
			$this->datatable_builder->order_by("({$alias}.meta_value * 1)",$args['order_by']['vat']);
			unset($args['order_by']['vat']);
		};


		$sort_contract_value = $args['order_by']['contract_value'] ?? FALSE;
		if($sort_contract_value)
		{
			$alias = uniqid('contract_value_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_value'", 'LEFT');
			$this->datatable_builder->order_by("({$alias}.meta_value * 1)",$sort_contract_value);
			unset($args['order_by']['contract_value']);
		}

		$sort_status = $args['order_by']['status'] ?? FALSE;
		if($sort_status)
		{
			$this->datatable_builder->order_by('term.term_status', $sort_status);
			unset($args['order_by']['status']);
		}

		$sort_type = $args['order_by']['type'] ?? FALSE;
		if($sort_type)
		{
			$this->datatable_builder->order_by('term.term_type', $sort_type);
			unset($args['order_by']['type']);
		}

		$sort_website = $args['order_by']['website'] ?? FALSE;
		if($sort_website)
		{
			$this->datatable_builder->order_by('term.term_name', $sort_website);
			unset($args['order_by']['website']);
		}

		if(!empty($args['where']['is_first_contract']))
		{
			$alias = uniqid('is_first_contract_');

			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'is_first_contract'", 'LEFT')
			->where("{$alias}.meta_value", $args['where']['is_first_contract']);
			
			unset($args['where']['is_first_contract']);
		}

		// APPLY DEFAULT FILTER BY MUTATE FIELD		
		$args = $this->datatable_builder->parse_relations_searches($args);
		if( ! empty($args['where']))
		{
			foreach ($args['where'] as $key => $value)
			{
				if(empty($value)) continue;

				if(empty($key))
				{
					$this->datatable_builder->add_filter($value, '');
					continue;
				}

				$this->datatable_builder->add_filter($key, $value);
			}
		}

		if(!empty($args['order_by'])) 
		{
			foreach ($args['order_by'] as $key => $value)
			{
				$this->datatable_builder->order_by($key, $value);
			}
		}
	}
}
/* End of file Dataset.php */
/* Location: ./application/modules/contract/controllers/ajax/Dataset.php */