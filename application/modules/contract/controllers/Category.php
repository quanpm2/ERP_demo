<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends Admin_Controller {
	public function __construct() 
	{
		parent::__construct() ;
		$this->load->model('contract/category_m')  ;
	}


	public function index() 
	{
		restrict('contract.category.access');


		$this->template->title->append('DANH MỤC NGÀNH HÀNG');

		$categories = $this->category_m->get_categories();
		$this->render_table($categories);

		$data['content'] 		= $this->table->generate();
		$data['categories']		= $categories ;

		parent::render($data,'category/index');
	}


	public function render_table($categories, $parent_id = 0, $char = '', $option = null)
	{
		$this->table->set_heading('ID', 'Tên danh mục', 'Đường dẫn', 'Ngày tạo', 'Người tạo', 'Ngày chỉnh sửa', 'Người chỉnh sửa', 'Trạng thái', 'Action');
	
		if(empty($categories)) return FALSE ;

		foreach ($categories as $key => $cate) {
			$term_id      = $cate->term_id   ;
		    $term_name    = $cate->term_name ;
		    $term_slug    = $cate->term_slug ;
		    $term_parent  = $cate->term_parent;
		    $term_status  = $cate->term_status;

		    $term_status  = ($term_status == 'publish') ? 'Hiển thị' : 'Không' ;

			$created 	  = (! get_term_meta_value($term_id,'created')) ? '--' : date('d-m-Y', get_term_meta_value($term_id,'created')) ;
			$user_id_created		= get_term_meta_value($term_id, 'created_by') ?: '' ;
			$created_by				= '';

			if(!empty($user_id_created))
			{
				$created_by 		= $this->admin_m->get_field_by_id($user_id_created,'display_name');
			}

			$modified 				= (! get_term_meta_value($term_id,'modified')) ? '--' : date('d-m-Y', get_term_meta_value($term_id,'modified')) ;
			$user_id_modified 		= get_term_meta_value($term_id, 'modified_by') ?: '' ;
			$modified_by			= '';

			if(!empty($user_id_modified))
			{
				$modified_by 		= $this->admin_m->get_field_by_id($user_id_modified,'display_name');
			}

			$action 	   = '' ;
			if(has_permission('contract.category.delete'))
			{
				$action    = '<button type="button" class="delete-category btn btn-danger btn-xs" data-term-id="'.$term_id.'"><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
			}

			if(has_permission('contract.category.update'))
			{
				$action    .= anchor('contract/category/form/'.$term_id,' <i class="fa fa-pencil-square-o" aria-hidden="true"></i>', 'data-toggle="tooltip" title="Cập nhật" class="edit-category btn btn-info btn-xs pull-right" data-term-id="'.$term_id.'"');
			}

			if($term_parent == $parent_id)
	        {
	            $term_name  = $char . $term_name ;
	            $this->table->add_row($term_id, $term_name, $term_slug, $created, $created_by, $modified, $modified_by, $term_status, $action) ;
	            unset($categories[$key]);
	            $this->render_table($categories, $cate->term_id, $char . '	|--- '); 
	        }
		}
	}

	public function get_term($term_id = 0) {
		if(empty($term_id)) return FALSE ;
		$term 	  = $this->term_m->get_by(array('term_type' => 'category', 'term_status' => array('publish', 'ending'), 'term_id' => $term_id));

		if(empty($term)) return array();
		return $term;
	}

	public function form($term_id = 0) 
	{
		restrict('contract.category.add');
		restrict('contract.category.update');

		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $post 					= $this->input->post() ?: array();

		$categories = $this->category_m
		->select('term_id,term_name')
		->set_term_type()
		->get_many_by(['term_parent'=>0,'term_status'=>'publish']);

		$categories = key_value($categories,'term_id','term_name');

		$data['categories']     = $categories ;
		$data['term']			= $this->get_term($term_id) ;
		$data['term_id']    	= $term_id;

		if($term_id > 0) 
		{
			$this->template->title->append('CHỈNH SỬA NGÀNH HÀNG');
		}
		else {
			$this->template->title->append('THÊM NGÀNH HÀNG');
		}

		$config = array(
	        array(
	                'field' => 'term_name',
	                'label' => 'Tên ngành hàng',
	                'rules' => 'required',
	                'errors' => array(
	                     'required' => 'Chưa nhập Tên ngành hàng',
	                ),
	        )
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
        {
			$errors = $this->form_validation->error_array() ;
			foreach($errors as $error) 
			{
				$this->messages->error($error);
			} 
            return $this->render($data, 'category/form'); 
        }

		if( !empty($post['action']) ) 
		{
			unset($post['action']) ;
			if( empty($term_id) || $term_id == 0 ) 
			{
				$category_id = $this->category_m->add($post) ;
				if(!empty($category_id)) $this->messages->success('Đã thêm thành công ngành hàng');
				redirect(module_url('category/index')) ;
			}
			else
			{
				$status = $this->category_m->edit($term_id, $post) ;
				if(TRUE === $status) $this->messages->success('Đã chỉnh sửa thành công ngành hàng');
				redirect(module_url('category/form/' . $term_id)) ;
			}
		}

		$this->render($data, 'category/form');
	}
}