<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Receipt_m.php');

class Receipt_gws_m extends Receipt_m
{
    // Số tiền nhập = (1 + %VAT) * (Giá trị gốc * (1 + %Phí DV) - Giảm giá)

	/**
	 * Calculates the VAT amount
	 *
	 * @return     integer  The vat amount.
	 */
	public function calc_vat_amount()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$amount = (double) get_post_meta_value($this->post_id, 'amount');
		$vat = div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		return div($amount, 1 + $vat) * $vat;
	}

	/**
	 * Calculates the fct.
	 *
	 * @param      integer    $percent  The percent
	 *c
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The fct.
	 */
	public function calc_fct()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		if('receipt_payment_on_behaft' == $this->post_type) return 0;

        $service_price = $this->get_service_price();
        $fct_rate = div((int) get_term_meta_value($this->contract->term_id, 'fct'), 100);

        return $service_price * $fct_rate;
	}


	/**
	 * Calculates the actual budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The actual budget.
	 */
	public function calc_actual_budget()
	{
		return 0;
	}

	/**
	 * Calculates the service fee.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_fee()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		if('receipt_payment_on_behaft' == $this->post_type) return 0;
        
        $service_fee_rate = div((int) get_term_meta_value($this->contract->term_id, 'service_fee_rate'), 100);
        $service_price = $this->get_service_price();

        $service_fee = $service_price * $service_fee_rate;

        return $service_fee;
	}

	/* phí giảm thỏa thuận (bao gồm VAT, FCT - nếu có)*/
	public function calc_deal_reduction_fee()
	{
		return 0;
	}
    
    /**
     * get_service_price
     * 
     * Giá trị gốc = Số tiền trước thuế * (1 / (1 + %Phí DV)) + Giảm giá / (1 + %Phí DV)
     *
     * @return void
     */
    private function get_service_price(){
        $amount = (double) get_post_meta_value($this->post_id, 'amount');

        $vat = div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
        $amount_not_vat = div($amount, 1 + $vat);

        $service_fee_rate = div((int) get_term_meta_value($this->contract->term_id, 'service_fee_rate'), 100);
        $service_price = $amount_not_vat * div(1, (1 + $service_fee_rate));

        $receipts = $this->receipt_m
        ->select('posts.post_id')
        ->select('term_posts.term_id')
        ->join('term_posts', "term_posts.post_id = posts.post_id AND term_posts.term_id = {$this->contract->term_id}")
        ->where('end_date <=', $this->end_date)
        ->where('post_status', 'paid')
        ->where('post_type', 'receipt_payment')
        ->where('posts.post_id !=', $this->post_id)
        ->set_post_type()
        ->as_array()
        ->order_by([ 'end_date' => 'asc', 'posts.post_id' => 'asc'])
        ->get_all();
        
        if(count($receipts) < 1){
            $discount_amount = (int) get_term_meta_value($this->contract->term_id, 'discount_amount');
            $discount_amount > 0 AND $service_price += div($discount_amount, 1 + $service_fee_rate);
        }

        return $service_price;
    }
}
/* End of file receipt_courses_m.php */
/* Location: ./application/modules/contract/models/receipt_courses_m.php */