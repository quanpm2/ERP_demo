<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract_wizard_m extends Base_contract_m {

	function __construct()
	{
		parent::__construct();

		$models = array(
			'contract/contract_m',
			'contract/term_categories_m',
			'termmeta_m',
			'term_users_m',
			'customer_m',
			'staffs/sale_m',
			'invoice_m',
			'banner/banner_contract_m',
			'banner/banner_kpi_m'
			);

		$this->load->model($models);
	}

	public function wizard_act_step1($post){

		$this->hook->add_filter('ajax_act_step1', function($post){

			update_term_meta($post['edit']['term_id'], 'wizard_step', 1);

			$contract = $this->contract_m->get($post['edit']['term_id']);

			$is_term_type_changed = ($contract->term_type != $post['edit']['term_type']);
			if($is_term_type_changed) 
				$this->termmeta_m->delete_meta($contract->term_id,'contract_code');
			
			return $post;
		});
	}

	public function wizard_act_step2($post){

		$this->hook->add_filter('ajax_act_step2', function($post){
			
			$step = 2;
			$term = $this->contract_m->set_term_type()->get($post['edit']['term_id']);
			if($term == 'courseads') $step = 3;

			update_term_meta($post['edit']['term_id'], 'wizard_step', $step);

			# is user_id not changed
			if(empty($post['user_updated'])) return NULL;
			
			$exists_term_users = $this->term_users_m->get_term_users($term->term_id, array('customer_person', 'customer_company','system'));
			$exists_term_users = empty($exists_term_users) ? array() : array_map(function($x){return $x->user_id;},$exists_term_users);

			$is_customer_changed = !in_array($post['edit']['user_id'], $exists_term_users);
			if($is_customer_changed){

				if(!empty($exists_term_users)) 
					$this->term_users_m->where_in('term_users.user_id', $exists_term_users);

				$this->term_users_m
					->set_term_users($post['edit']['term_id'], 
						array($post['edit']['user_id']), 
						array('customer_person', 'customer_company','system'), 
						array('command'=>'replace'));

				$this->termmeta_m->delete_meta( $term->term_id, 'representative_gender');
				$this->termmeta_m->delete_meta( $term->term_id, 'representative_name');
				$this->termmeta_m->delete_meta( $term->term_id, 'representative_email');
				$this->termmeta_m->delete_meta( $term->term_id, 'representative_address');
				$this->termmeta_m->delete_meta( $term->term_id, 'representative_phone');
				$this->termmeta_m->delete_meta( $term->term_id, 'extra');
				$this->termmeta_m->delete_meta( $term->term_id, 'contract_code');

				#set website empty if customer has changed
				$this->contract_m->update($term->term_id, array('term_parent'=>0));
			}

			$websites = array(0 => 'None');

			$_webs = $this->term_users_m
				->where(array('term_users.user_id' => $post['edit']['user_id'],'term.term_type'=>'website'))
				->join('term','term.term_id = term_users.term_id')
				->get_many_by();
			if(!empty($_webs)) 
				foreach ($_webs as $w)
					$websites[$w->term_id] = $w->term_name;

			if(!empty($term->term_parent)){
				$_tmp_website = $this->contract_m->get($term->term_parent);
				if(!empty($_tmp_website))
					$websites[$_tmp_website->term_id] = $_tmp_website->term_name;
			}

			$term->extra = @unserialize($this->termmeta_m->get_meta_value($post['edit']['term_id'], 'extra'));

			$customer = $this->customer_m->get($post['edit']['user_id']);

			$jsfunction = array();
			$jsfunction[] = array('function_to_call'=>'refresh_customer_websites','data'=>$websites);
			
			/* Load danh sách nhân viên kinh doanh */
			$staffs = $this->sale_m->select('user_id,user_email,display_name')->set_user_type()->set_role()->as_array()->get_all();
			$staffs = array_map(function($x){ $x['display_name'] = $x['display_name'] ?: $x['user_email']; return $x; }, $staffs);
			$staffs = key_value($staffs, 'user_id', 'display_name');

			$render_detail_page = $this->load->view('contract/admin/wizard_step_detail',array('edit'=> $term,'customer'=>$customer,'staffs'=>$staffs), TRUE);

			$jsfunction[] = array('function_to_call'=>'reload_detail_panel','data'=> $render_detail_page);

			return array('jscallback'=> $jsfunction);
		});
	}


	/**
	 * Hander ajax action for form' website tab
	 */
	public function wizard_act_step3($post){

		/*
		 * Add Filter xử lý tác vụ cập nhật website và dòng ngành thực hiện
		 */	
		$this->hook->add_filter('ajax_act_step3', function($post){ 

			$term_id = $post['edit']['term_id'];

			/* 
			 * Cập nhật thay đổi giữa hợp đồng và danh mục ngành nghề
			 */
			if( ! empty($post['term_categories']))
			{
				$post['term_categories'] = array_unique($post['term_categories']);

				$term_categories = array();
				if($_term_categories = $this->term_categories_m->get_categories($term_id))
				{
					// Lấy dữ liệu term_categories tồn tại trong DB
					$term_categories = array_unique(array_column($_term_categories, 'term_id'));
				}

				sort($term_categories);
				sort($post['term_categories']);

				/* 
				 * Nếu dữ liệu user input và dữ liệu trong DB không khớp 
				 * nghĩa là có cập nhật cần thay đổi , => cập nhật lại term_categories
				 */
				if($term_categories != $post['term_categories'])
				{
					$this->term_categories_m->set_relations($term_id,$post['term_categories'],[],TRUE);
				}
			}

			if( ! empty($post['edit']['term_parent']))
			{
				$this->load->model('customer/website_m');

				$website_ids 	= array_unique($post['edit']['term_parent']); 
				$websites 		= $this->website_m->select('term_id, term_name')->get_many($website_ids);

				if(empty($websites))
				{
					unset($post['edit']['term_parent']);
					return $post;
				}

				$this->termmeta_m->delete_meta($term_id, 'websites');

				$website_ids = array_column($websites, 'term_id');
				array_walk($website_ids, function($x) use($term_id){
					$this->termmeta_m->add_meta($term_id, 'websites', $x);
				});

				$primary_website = reset($websites);
				$post['edit']['term_name'] 		= $primary_website->term_name;
				$post['edit']['term_parent']	= $primary_website->term_id;
			}

			/* Update finished step form field website */
			update_term_meta($post['edit']['term_id'], 'wizard_step', 3);

			return $post;
		});
	}

	public function wizard_act_step4($post){

		$this->hook->add_filter('ajax_act_step4', function($post){

			update_term_meta($post['edit']['term_id'], 'wizard_step', 4);

			$is_contract_draft = ($post['edit']['term_status'] == 'draft');
			if($is_contract_draft)
				unset($post['edit']['term_status']);

			$post['edit']['meta']['extra'] = serialize($post['edit']['extra']);
			unset($post['edit']['extra']);

			# Convert thời gian hợp đồng
			if(!empty($post['edit']['meta']['contract_daterange'])){	

				$contract_daterange = explode(' - ', $post['edit']['meta']['contract_daterange']);

				$post['edit']['meta']['contract_begin']	= $this->mdate->startOfDay(reset($contract_daterange));
				$post['edit']['meta']['contract_end']	= $this->mdate->startOfDay(end($contract_daterange));
				unset($post['edit']['meta']['contract_daterange']);	
			}

			if(!empty($post['edit']['meta']['services_pricetag']))
				$post['edit']['meta']['services_pricetag'] = serialize($post['edit']['meta']['services_pricetag']);

			$_tmp_attactment_files = @unserialize($this->termmeta_m->get_meta_value($post['edit']['term_id'], '_tmp_file_upload'));
			if($_tmp_attactment_files){

				if(empty($_tmp_attactment_files) || !is_array($_tmp_attactment_files)) 
					return $post;
				$attactment_files = @unserialize($this->termmeta_m->get_meta_value($post['edit']['term_id'], '_attachment_files'));

				if(empty($attactment_files) || !is_array($attactment_files)) 
					$attactment_files = array();

				$attactment_files = array_merge($attactment_files, $_tmp_attactment_files);

				$this->termmeta_m->delete_meta($post['edit']['term_id'], '_tmp_file_upload');
				$this->termmeta_m->update_meta($post['edit']['term_id'], '_attachment_files', serialize($attactment_files));
			}

			# Nhân viên kinh doanh phụ trách
			if( ! empty($post['edit']['meta']['staff_business']))
			{
                $term_id 		= $post['edit']['term_id'];
				$staff_business = $post['edit']['meta']['staff_business'];

                $created_by = get_term_meta_value($term_id, 'created_by') ?: $this->admin_m->id;
				$users      = array($created_by, $staff_business);
                
                $customer_id = $post['customer_id'];
                $staff_assigned_to = get_user_meta($customer_id, 'assigned_to', FALSE, TRUE);

                if(!in_array($staff_business, $staff_assigned_to))
                {
                    if(isset($post['is_overwrite_assigned_to']))
                    {
                        $this->usermeta_m->delete_meta($customer_id, 'assigned_to');
                    }
    
                    $this->usermeta_m->add_meta($customer_id, 'assigned_to', $staff_business);
                }

				$this->term_users_m->set_relations_by_term($term_id, $users, 'admin', array(), TRUE);
			}

			return $post;
		});

		$this->hook->add_filter('ajax_act_step4', function($post){

			if(!empty($post['response']['edit'])) {

				$partial_data = array('edit'=>$term = $this->term_m->get($post['response']['edit']['term_id']));

				$jsfunction = array();

				$render_partial = '<div id="service_tab"></div>';

				switch ($partial_data['edit']->term_type) {

					case 'webdoctor':
						$partial_data['edit']->extra = @unserialize($this->termmeta_m->get_meta_value($post['edit']['term_id'], 'extra'));
						$review_partial = $this->load->view("webdoctor/contract/wizard_review", $partial_data, TRUE);	
						$jsfunction[] = array('function_to_call'=>'reload_review_panel','data'=>$review_partial);
						break;

					case 'seo-traffic':
						$this->load->model('seotraffic/seotraffic_report_m');
						$partial_data['ga_accounts'] = $this->seotraffic_report_m->reload_account_ga(true);
						$render_partial = $this->load->view("seotraffic/contract/wizard_partial", $partial_data, TRUE);
						break;

					case 'google-ads':
						$partial_data['edit']->extra = @unserialize($this->termmeta_m->get_meta_value($term->term_id, 'extra'));
						$render_partial = $this->load->view("googleads/contract/wizard_partial", $partial_data, TRUE);
						break;

					case 'webgeneral':
						$render_partial = $this->load->view("webgeneral/contract/wizard_partial", $partial_data, TRUE);
						break;

					case 'webdesign':
						$render_partial = $this->load->view("webbuild/contract/wizard_partial", $partial_data, TRUE);
						break;
						
					case 'hosting':
						$render_partial = $this->load->view("hosting/contract/wizard_partial", $partial_data, TRUE);
						break;

					case 'facebook-ads':
						$render_partial = $this->load->view("facebookads/contract/wizard_partial", $partial_data, TRUE);
						break;

					case 'weboptimize':
						$render_partial = $this->load->view("weboptimize/contract/wizard_partial", $partial_data, TRUE);
						break;		

					case 'domain':
						$render_partial = $this->load->view("domain/contract/wizard_partial", $partial_data, TRUE);
						break;

					case 'banner':
						$render_partial = $this->load->view("banner/contract/wizard_partial", $partial_data, TRUE);
						break;		
	
                    case 'zalo-ads':
                        $render_partial = $this->load->view("zaloads/contract/wizard_partial", $partial_data, TRUE);
                        break;
				}

				$jsfunction[] = array('function_to_call'=>'reload_service_partial','data'=> $render_partial);
				$post['response'] = array('jscallback'=> $jsfunction);
			}

			return $post;
		}, 11);
	}

	
	/**
	 * Process xử lý step chi tiết dịch vụ
	 *
	 * @param      Array  $post   The post
	 */
	public function wizard_act_service($post)
	{
		$this->hook->add_filter('ajax_act_service',	function($post){
			$metadata = $post['edit']['meta'] ?? [];

            if(!empty($metadata['service_fee_payment_type']) && 'range' != $metadata['service_fee_payment_type']){
                $metadata['service_fee_plan'] = [];
            }

			if(!empty($metadata['service_package']))
			{
				$this->load->config('webgeneral/webgeneral'); 
  				$package_config = $this->config->item('service','packages');
  				$service_package = $metadata['service_package'];
				$service_package = isset($package_config[$service_package]) ? $service_package : $this->config->item('default','packages');
				$metadata['service_package_price'] = $package_config[$service_package]['price'];
			}

			empty($metadata['googleads-begin_time'])	OR $metadata['googleads-begin_time']	= convert_time($metadata['googleads-begin_time']);
			empty($metadata['googleads-end_time'])		OR $metadata['googleads-end_time']		= convert_time($metadata['googleads-end_time']);
			empty($metadata['advertise_start_time'])	OR $metadata['advertise_start_time']	= convert_time($metadata['advertise_start_time']);
			empty($metadata['advertise_end_time'])		OR $metadata['advertise_end_time']		= convert_time($metadata['advertise_end_time']);

			// FILTER TERMMETA::GOOGLE-CURATORS
			$this->load->library('form_validation');

			if( ! empty($metadata['promotions']))
			{
                if(!is_string($metadata['promotions'])) 
                {
                    $_promotions = array();
                    foreach ($metadata['promotions']['value'] as $key => $value)
                    {
                        if(empty($value)) continue;
    
                        $_promotions[] = array(
                            'name' 	=> $metadata['promotions']['name'][$key],
                            'value' => $metadata['promotions']['value'][$key],
                            'unit'	=> $metadata['promotions']['unit'][$key]
                        );
                    }
    
                    $metadata['promotions'] = serialize($_promotions);
                }
			}

			$metadata['is_display_promotions_discount'] = (int) ($metadata['is_display_promotions_discount'] ?? 0);

			if( ! empty($metadata['contract_curators']) && !empty($metadata['contract_curators']['curator_name']))
			{
				$contract_curators = array();
				$names = $metadata['contract_curators']['curator_name'];
				$phones = $metadata['contract_curators']['curator_phone'];
				$emails = $metadata['contract_curators']['curator_email'];
				foreach ($names as $key => $name) 
				{
					$name = trim($name);
					if(empty($name)) continue;

					$phone = preg_replace('/\s+/', '', $phones[$key]);
					$email = preg_replace('/\s+/', '', $emails[$key]);
					$email = $this->form_validation->valid_email($email) ? strtolower($email) : '';

					$contract_curators[] = ['name'=>$name,'phone'=>$phone,'email'=>$email];
				}

				$metadata['contract_curators'] = serialize($contract_curators);
			}

			if( ! empty($metadata['curator_name']))
			{
				$curators = array();
				$names = $metadata['curator_name'];
				$phones = $metadata['curator_phone'];
				$emails = $metadata['curator_email'];
				foreach ($names as $key => $name) 
				{
					$name = trim($name);
					if(empty($name)) continue;

					$phone = preg_replace('/\s+/', '', $phones[$key]);
					$email = preg_replace('/\s+/', '', $emails[$key]);
					$email = $this->form_validation->valid_email($email) ? strtolower($email) : '';

					$curators[] = ['name'=>$name,'phone'=>$phone,'email'=>$email];
				}

				$metadata['contract_curators'] = serialize($curators);
				unset($metadata['curator_name'],$metadata['curator_phone'],$metadata['curator_email']);
			}

			if(!empty($metadata['addition_hosting']['start_time']))
			{
				$metadata['addition_hosting']['start_time'] = strtotime($metadata['addition_hosting']['start_time']);
			}

			if(!empty($metadata['addition_domain']['start_time']))
			{
				$metadata['addition_domain']['start_time'] = strtotime($metadata['addition_domain']['start_time']);
			}

			if(!empty($metadata['start_service_time']))
			{
				$metadata['start_service_time'] = strtotime($metadata['start_service_time']);
			}

			if(!empty($metadata['end_service_time']))
			{
				$metadata['end_service_time'] = strtotime($metadata['end_service_time']);
			}

            if(!empty($metadata['service_fee_plan']))
			{
                $_service_fee_plan = [];

				foreach ($metadata['service_fee_plan']['service_fee_rate'] as $key => $value)
				{
					if(empty($value)) continue;

                    if(0 == $key){
                        $metadata['original_service_fee_rate'] = div($metadata['service_fee_plan']['service_fee_rate'][$key], 100);
                        $metadata['contract_budget'] = $metadata['service_fee_plan']['from'][$key];
                        $metadata['service_fee'] = $metadata['contract_budget'] * $metadata['original_service_fee_rate'];
                    }

					$_service_fee_plan[] = [
                        'service_fee_rate' => $metadata['service_fee_plan']['service_fee_rate'][$key],
                        'from' => $metadata['service_fee_plan']['from'][$key],
                        'to' => $metadata['service_fee_plan']['to'][$key],
                    ];
				}

				$metadata['service_fee_plan'] = serialize($_service_fee_plan);
			}

            $regex = '/(http|https):\/\/app.hubspot.com\/.*/';
            if(! empty($metadata['hubspot_link']) && !preg_match($regex, $metadata['hubspot_link']))
			{
                $metadata['hubspot_link'] = '';
			}

			if(!empty($metadata)) 
			{
				foreach ($metadata as $key => $value)
				{
					$value = is_array($value) ? serialize($value) : $value;
					update_term_meta($post['edit']['term_id'], $key, $value);
				}
			}

			$term = $this->term_m->get($post['edit']['term_id']);

			if( !empty($term) ) 
			{
				switch ($term->term_type) {

					// Facebook	
					case 'facebook-ads':	

						$this->load->model('facebookads/facebookads_m');
		  				$contract 			= (new facebookads_m)->set_contract($term);
		  				$budget 			= (double) get_term_meta_value($term->term_id, 'contract_budget');
						$service_fee 		= (double) get_term_meta_value($term->term_id, 'service_fee');
						$disacount_amount 	= (double) $contract->get_behaviour_m()->calc_disacount_amount();

						update_term_meta($term->term_id, 'original_service_fee_rate', div($service_fee, $budget));
						update_term_meta($term->term_id, 'discount_amount', $disacount_amount);
						update_term_meta($term->term_id, 'contract_value', $contract->get_behaviour_m()->calc_contract_value());
	
						$service_fee_rate_actual = div(max([$service_fee - $disacount_amount, 0]), $budget);
						update_term_meta($term->term_id, 'service_fee_rate_actual', $service_fee_rate_actual);
						update_term_meta($term->term_id, 'typeOfService', $contract->getTypeOfService());
						break;

					// Facebook	
					case 'tiktok-ads':

						$this->config->load('tiktokads/tiktokads');
                        $this->load->model('tiktokads/tiktokads_m');

		  				$contract = (new tiktokads_m)->set_contract($term);

						$budget 		= (double) get_term_meta_value($term->term_id, 'contract_budget');
						$service_fee 	= (double) get_term_meta_value($term->term_id, 'service_fee');
						$disacount_amount 	= (double) $contract->get_behaviour_m()->calc_disacount_amount();

						update_term_meta($term->term_id, 'original_service_fee_rate', div($service_fee, $budget));
						update_term_meta($term->term_id, 'discount_amount', $disacount_amount);
						update_term_meta($term->term_id, 'contract_value', $contract->get_behaviour_m()->calc_contract_value());
						
						$service_fee_rate_actual = div(max([$service_fee - $disacount_amount, 0]), $budget);
						update_term_meta($term->term_id, 'service_fee_rate_actual', $service_fee_rate_actual);
                        update_term_meta($term->term_id, 'typeOfService', $contract->getTypeOfService());
						
						break;

					// Tối ưu		
					case 'weboptimize':
						$contract_price_weboptimize  = get_term_meta_value($post['edit']['term_id'], 'contract_price_weboptimize') ?: 0;
  						update_term_meta($post['edit']['term_id'], 'contract_value', $contract_price_weboptimize );
						break;

					// Dịch vụ quảng cáo Adword CPC | QLTK	
					case 'google-ads':

						$contract = (new contract_m)->set_contract($term);

						$budget 		= (double) get_term_meta_value($term->term_id, 'contract_budget');
						$service_fee 	= (double) get_term_meta_value($term->term_id, 'service_fee');
						$disacount_amount 	= (double) $contract->get_behaviour_m()->calc_disacount_amount();
						
						// update_term_meta($term->term_id, 'service_provider_tax_rate', 0.05);
						// update_term_meta($term->term_id, 'service_provider_tax', $budget* 0.05);

						update_term_meta($term->term_id, 'original_service_fee_rate', div($service_fee, $budget));
						update_term_meta($term->term_id, 'discount_amount', $disacount_amount);
						update_term_meta($term->term_id, 'contract_value', $contract->get_behaviour_m()->calc_contract_value());

						$service_fee_rate_actual = div(max($service_fee - $disacount_amount, 0), $budget);
						update_term_meta($term->term_id, 'service_fee_rate_actual', $service_fee_rate_actual);

						update_term_meta($term->term_id, 'typeOfService', $contract->getTypeOfService());

						break;

                    // Dịch vụ quảng cáo Zalo ads
					case 'zalo-ads':

						$contract = (new contract_m)->set_contract($term);

						$budget 		= (double) get_term_meta_value($term->term_id, 'contract_budget');
						$service_fee 	= (double) get_term_meta_value($term->term_id, 'service_fee');
						$disacount_amount 	= (double) $contract->get_behaviour_m()->calc_disacount_amount();
						
						update_term_meta($term->term_id, 'original_service_fee_rate', div($service_fee, $budget));
						update_term_meta($term->term_id, 'discount_amount', $disacount_amount);
						update_term_meta($term->term_id, 'contract_value', $contract->get_behaviour_m()->calc_contract_value());

						$service_fee_rate_actual = div(max($service_fee - $disacount_amount, 0), $budget);
						update_term_meta($term->term_id, 'service_fee_rate_actual', $service_fee_rate_actual);

						update_term_meta($term->term_id, 'typeOfService', $contract->getTypeOfService());

						break;

					case 'banner':

							$banners_selected = array();
							foreach ($metadata['banners_selected'] as $key => &$packages)
							{
								foreach ($packages as &$p)
								{
									if(empty($p['checked'])) continue;

									unset($p['checked']);
									
									$banners_selected = wp_parse_args($p, $banners_selected);
								}
							}

							update_term_meta($post['edit']['term_id'], 'banners_selected', serialize($banners_selected));

							$contract_value = $this->contract_m->set_contract($post['edit']['term_id'])->calc_contract_value();
							update_term_meta($post['edit']['term_id'], 'contract_value', $contract_value);

						break;

					case 'webcontent':
							
							$service = $post['edit']['meta'];
							
							$this->load->config('webcontent/content');
							$packages_content_config = $this->config->item('packages_content');

							$term_id = $post['edit']['term_id'];

							$sum_total = 0;

							foreach ($service as $key => &$packages) 
							{
								if(empty($packages)) 
								{
									continue;
								} 
								foreach ($packages as $k => &$values) 
								{
									if(empty($values['checkbox_'.$k]))
									{
										unset($packages[$k]);// gói nào không được check thì xóa khỏi array
										continue;
									}
									if(empty($packages_content_config[$key][$k])) continue;

									$sale_off = $packages_content_config[$key][$k]['sale_off'];
									$values['sale_off'] = 0;

									if($values[$packages_content_config[$key][$k]['number']]>=$sale_off['min'])
									{
										$values['sale_off'] = $sale_off['value'];
									}
									$quanties = $values[$packages_content_config[$key][$k]['number']];
					                 $price = $values[$packages_content_config[$key][$k]['name']];

					                $total = 0;
					                if($values['sale_off'] !=0){
					                  $total = ($price * $quanties) - ((($price * $quanties)*$values['sale_off'])/100);
					                  // (price * quanties)-(((price * quanties)*parseInt(sale['value']))/100);
					                }else{
					                  $total = $price * $quanties;
					                }
					                $total = $total/1000;
					                $total = round($total)*1000;
									$sum_total += $total;
									
								}
							}
							
							$meta_value = serialize($service);

							$meta_key = 'contents_selected';
							
							update_term_meta($term_id, $meta_key, $meta_value);// luu tac vu thiet ke content
							update_term_meta($term_id, 'contract_value', $sum_total);// Update Gtri hop dong
						break;

					case 'courseads':

						$term_id = $post['edit']['term_id'];

						$this->load->model('courseads/courseads_m');

						update_term_meta($post['edit']['term_id'], 'discount_amount', $post['meta']['discount_amount']??0);

						/* Trong trường hợp user chưa nhập bất kì khóa học nào */
						$course_ids = $post['meta']['courses']['post_id'] ?? '';
						if(empty($course_ids)) break;

						/* Tiến hành lưu trữ chi tiết dữ liệu khóa học của hợp đồng */
						$this->load->model('courseads/course_m');

						$courses = $this->course_m->set_post_type()->as_array()
						->select('post_id,post_title,post_status,start_date,end_date')
						->get_many_by('post_id', $course_ids);

						if( ! $courses) break;

						/* Cập nhật tên hợp đồng tạp thời theo tên khóa học */
						$term_name = implode('-', array_map(function($x){ 
							return strtoupper(create_slug($x['post_title']));
						}, $courses));

						$this->courseads_m->update($post['edit']['term_id'], ['term_name' => $term_name]);

						$courses = array_column(array_map(function($x){ 
								$x['course_value'] = (int) get_post_meta_value($x['post_id'], 'course_value'); 
								return $x; 
						}, $courses), NULL, 'post_id');


						foreach($post['meta']['courses']['post_id'] as $key => $_post_id)
						{
							/* Set default value of course post data */
							$courses[$_post_id]['quantity'] = $post['meta']['courses']['quantity'][$key] ?? 1;
							$courses[$_post_id]['course_value'] = $post['meta']['courses']['course_value'][$key] ?? 0;
						}

						$this->load->model('term_posts_m');
						$this->term_posts_m->set_term_posts($post['edit']['term_id'], array_column($courses, 'post_id'), $this->course_m->post_type, FALSE);
						update_term_meta($post['edit']['term_id'], 'courses_items', serialize($courses));

						/* Đồng bộ thời gian hợp đôgnf */
						$contract = new courseads_m();
						$contract->set_contract($term_id);
						$contract->get_behaviour_m()->remap_contract_dates();
						
						$contract_value = $contract->calc_contract_value();
						update_term_meta($term_id, 'contract_value', $contract_value);

						$contract->delete_all_invoices(); //  Delete all invoices available
						$contract->create_invoices(); // Create new contract's invoices

						$staff_business = get_term_meta_value($post['edit']['term_id'], 'staff_business');
						if(empty($staff_business) && $created_by = get_term_meta_value($post['edit']['term_id'], 'created_by'))
						{
							update_term_meta($post['edit']['term_id'], 'staff_business', $created_by);
						}

						break;

                    case 'gws':
                        $term_id = $post['edit']['term_id'];

                        $service_package_detail = get_term_meta_value($term_id, 'service_package_detail');
                        $num_of_term = get_term_meta_value($term_id, 'num_of_term') ?: 1;
                        $service_package_detail = @json_decode($service_package_detail, TRUE) ?: [];

                        $term_unit = $service_package_detail['term_unit'];
                        $month = 1;
                        'year' == $term_unit AND $month = 12;

                        $num_of_month = $num_of_term * $month;

                        $contract_begin = (int)get_term_meta_value($term_id, 'contract_begin');
                        $contract_end = strtotime("+{$num_of_month} months", $contract_begin);
                        update_term_meta($term_id, 'contract_end', $contract_end);

                        $email_accounts = @$post['edit']['meta']['email_accounts'];
                        $email_accounts = json_decode($email_accounts, TRUE);
                        $email_accounts = array_unique($email_accounts);
                        $this->termmeta_m->delete_meta($term_id, 'email_accounts');
                        if(!empty($email_accounts)){    
                            array_walk($email_accounts, function($email_account) use ($term_id){
                                $this->termmeta_m->add_meta($term_id, 'email_accounts', $email_account);
                            });
                        }

                        break;
					default:
						# code...
						break;
				}
			}
			
			update_term_meta($post['edit']['term_id'], 'wizard_step', 5);

			$term = $this->term_m->get($post['edit']['term_id']);
			$term->extra = @unserialize($this->termmeta_m->get_meta_value($post['edit']['term_id'], 'extra'));

			$partial_review = '';

			if(!empty($term->term_type))
			{
				$service_module = str_replace('-','',$term->term_type);
				if(file_exists(APPPATH . 'modules/' . $service_module . '/views/contract/wizard_review' . EXT))
					$partial_review = $this->load->view( $service_module . '/contract/wizard_review', array('edit'=>$term),TRUE);
			}

			if($term->term_type == 'webdesign')
			{
				if(file_exists(APPPATH . 'modules/webbuild/views/contract/wizard_review' . EXT))
					$partial_review = $this->load->view('webbuild/contract/wizard_review', array('edit'=>$term),TRUE);
			}

			if(empty($partial_review))
				$partial_review = $this->load->view('contract/admin/wizard_step_finish', array('edit'=>$term), TRUE);

			$jsfunction = array(
				array('function_to_call'=>'reload_review_panel','data'=>$partial_review));

			return array('jscallback'=> $jsfunction);
		});
	}
	
	public function wizard_act_finish($post)
	{	
		# trace tab-pane log
		$this->hook->add_filter('ajax_act_finish',	function($post){
			update_term_meta($post['edit']['term_id'], 'wizard_step', 6);
			return $post;
		});

		# add filter :check owner website and mapping website to customer
		$this->hook->add_filter('ajax_act_finish',	function($post){
			$term_id = $post['edit']['term_id'];
			$term_user = $this->term_users_m->get_term_users($term_id,array('customer_person','customer_company','system'));
			$ownner = end($term_user);

			if($ownner->user_type != 'system')
				return $post;

			//update post 
			$term = $this->contract_m->get($term_id);

			# add filter : disable order_type to appoval			
			$post_term = $this->term_posts_m	
			->where('term_posts.term_id',$term->term_id)
			->where_in('posts.post_type',array('order-service'))
			->join('posts','posts.post_id = term_posts.post_id')
			->get_by();

			if(!empty($post_term)) $this->post_m->update($post_term->post_id,array('post_status'=>'approval'));

			# updated mapping website to customer
			if(!empty($term->term_parent))
			{
				$website = $this->term_m->get($term->term_parent);
				$term_user = $this->term_users_m->get_term_users($website->term_id,array('customer_person','customer_company','system'));
				$customer_owner = end($term_user);
				if(!empty($customer_owner) && $ownner->user_id != $customer_owner->user_id){

					// delete mapping system_user vs contract_term
					$this->term_users_m->where('user_id',$ownner->user_id)->where('term_id',$term_id)->delete_by();

					// mapping customer_user vs contract_term
					$this->term_users_m->set_term_users($term_id, array($customer_owner->user_id), array('customer_person', 'customer_company','system'), array('command'=>'replace'));						
				}
			}	
			 
			return $post;
		});
	}

	public function wizard_act_create_customer($post)
	{
		$this->hook->add_filter('ajax_act_create_customer',	function($post){

			$usermeta = array();

			if(!empty($post['edit']['meta'])){
				$usermeta = $post['edit']['meta'];
				unset($post['edit']['meta']);
			}

			$insert_id = $this->customer_m->insert($post['edit']);

			foreach ($usermeta as $key => $value)
				$this->usermeta_m->update_meta($insert_id, $key, $value);

			return array('id'=>$insert_id, 'text'=>$post['edit']['display_name']);
		});
	}

	public function wizard_act_create_website($post){

		$this->hook->add_filter('ajax_act_create_website',	function($post){

			$result = array('msg'=>'Tên miền không hợp lệ hoặc đã được sử dụng !','success'=>FALSE);

			$post['edit']['term_name'] = strtolower($post['edit']['term_name']);
			$post['edit']['term_name'] = preg_replace('/\s+/', '', $post['edit']['term_name']);

			if($post['edit']['term_name'] = parse_url(prep_url($post['edit']['term_name']),PHP_URL_HOST)){

				if(strpos($post['edit']['term_name'], 'www.') === 0)
					$post['edit']['term_name'] = str_replace('www.', '', $post['edit']['term_name']);

				$is_exists = $this->term_m->where(array('term_name'=>$post['edit']['term_name'],'term_type'=>'website'))->get_by();

				if(empty($is_exists)){

					$post['edit']['term_type'] = 'website';

					$user_id = $post['user_id'];
					unset($post['user_id']);

					$term_id = $this->term_m->insert($post['edit']);
					$this->termmeta_m->update_meta( $term_id, 'create_time', time());
					$this->term_users_m->set_term_users( $term_id, array($user_id));

					$result['ret_obj'] = array('id'=>$term_id,'text'=>$post['edit']['term_name']);
					$result['msg'] = 'Thêm mới website thành công !';
					$result['success'] = TRUE;
				}
				else{

					$role_id = $this->admin_m->role_id;
					$allow_roles = $this->config->item('read_customers_role_id');
					$website = $is_exists;

					if(!in_array($role_id, $allow_roles)){
						$result['ret_obj'] = array('id'=>$website->term_id,'text'=>$website->term_name);
						$result['msg'] = 'Mapping website thành công !';
						$result['success'] = TRUE;
					}
				}
			}

			return $result;
		});
	}
}
/* End of file Contract_wizard_m.php */
/* Location: ./application/modules/contract/models/Contract_wizard_m.php */