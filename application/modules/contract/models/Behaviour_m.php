<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Behaviour_m extends Base_Model {

	use \AdsService\Behaviours\CustomerPaymentTrait;
	
	public $data = array();
	protected $contract = NULL;

	function __construct() 
	{
		parent::__construct();
		$models = array('contract/contract_m','contract/invoice_m', 'term_posts_m');
		
		$this->load->model($models);
		$this->load->config('contract/contract');
	}

	/**
	 * Gets the instance.
	 *
	 * @return     self  The instance.
	 */
	public function get_instance()
	{
		if( ! $this->contract) return FALSE;
		
		$model = $this->config->item($this->contract->term_type, 'behaviour_models');
		if( ! $model) return $this;

		$this->load->model($model);
		$model 		= explode('/', $model);
		$model_name = end($model);
		$_instance = new $model_name();
		$_instance->set_contract($this->contract);
		return $_instance;
	}

	/**
	 * Sets the contract.
	 *
	 * @param      Contract_m  $contract  The contract
	 *
	 * @return     self    ( description_of_the_return_value )
	 */
	public function set_contract($contract = NULL)
	{
		if(is_numeric($contract))
		{
			$this->load->model('contract/contract_m');
			$contract = $this->contract_m->set_term_type()->get($contract);
		}

		if( ! $contract) return FALSE;

		$this->contract = $contract;
		return $this;
	}

	public function get_contract()
	{
		return $this->contract;
	}

	/**
	 * Determines if exist contract.
	 */
	protected function is_exist_contract()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");
		return TRUE;
	}

	/**
	 * Creates invoices.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     ( description_of_the_return_value )
	 */
	public function create_invoices()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");
		$term_id 		= $this->contract->term_id;
		$contract_value = get_term_meta_value($term_id, 'contract_value') ?: 0;
		if(empty($contract_value)) throw new Exception("Giá trị hợp đồng ({$contract_value}) không hợp lệ.");


		$number_of_payments = (int) get_term_meta_value($term_id, 'number_of_payments');
		$number_of_payments = $number_of_payments ?: 1;

		$contract_begin = get_term_meta_value($term_id, 'contract_begin');
		$contract_end 	= get_term_meta_value($term_id, 'contract_end');
		$num_dates 		= diffInDates($contract_begin,$contract_end);

		$num_days4inv 			= ceil(div($num_dates,$number_of_payments));
		$amount_per_payments 	= div($contract_value,$number_of_payments);

		$start_date 	= $contract_begin;
		$invoice_items 	= array();

		for($i = 0 ; $i < $number_of_payments; $i++)
		{	
			if($num_days4inv == 0) break;

			$end_date 	= end_of_day(strtotime("+{$num_days4inv} day -1 second", $start_date));
			$_inv_data 	= array(
				'post_title' 	=> 'Thu tiền đợt '. ($i + 1),
				'post_content' 	=> '',
				'start_date' 	=> $start_date,
				'end_date' 		=> $end_date,
				'post_type' 	=> $this->invoice_m->post_type
			);


			$inv_id = $this->invoice_m->insert($_inv_data);
			if(empty($inv_id)) continue;

			$this->term_posts_m->set_post_terms($inv_id, $term_id, $this->contract->term_type);

			$quantity = $num_days4inv;

			$this->invoice_item_m->insert(array(
					'invi_title' => 'Giá dịch vụ',
					'inv_id' => $inv_id,
					'invi_description' => '',
					'invi_status' => 'publish',
					'price' => $amount_per_payments,
					'quantity' => 1,
					'invi_rate' => 100,
					'total' => $this->invoice_item_m->calc_total_price($amount_per_payments, 1, 100)
				));

			$start_date = strtotime('+1 second', $end_date);

			$day_end = $num_dates - $num_days4inv;
			if($day_end < $num_days4inv) $num_days4inv = $day_end;
		}

		return TRUE;
	}


	/**
	 * { function_description }
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     ( description_of_the_return_value )
	 */
	public function delete_all_invoices()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$term_id = $this->contract->term_id;

		$invoices = $this->term_posts_m->get_term_posts($term_id, $this->invoice_m->post_type);
		if( ! $invoices) return FALSE;

		$invoice_ids = array_column($invoices, 'post_id');
		$this->post_m->delete_many($invoice_ids);// Xóa tất cả invoice cũ		
		$this->invoice_item_m->where_in('inv_id', $invoice_ids)->delete_by(); // Xóa tất cả invoice items cũ
		$this->term_posts_m->delete_term_posts($term_id, $invoice_ids); // Xóa liên kết hợp đồng - đợt thanh toán

		return TRUE;
	}

	/**
	 * Sync All amount value of this contract
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function sync_all_amount()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$term_id = $this->contract->term_id;

		// Tính giá trị đã thanh toán
		$payment_amount = (double) $this->calc_payment_amount();
		update_term_meta($term_id, 'payment_amount', $payment_amount);

		// Tính giá trị cân bằng đã thanh toán
		$balance_spend_amount = (double) $this->calc_balance_spend();
		update_term_meta($term_id, 'balance_spend', $balance_spend_amount);

		// Tính tổng hóa đơn cần thanh toán
		$invs_amount = $this->calc_invs_value();
		update_term_meta($term_id,'invs_amount', $invs_amount);

		// Tính dự thu quá hạn
		$invs_amount_expired = $this->calc_payment_amount_expired($this->mdate->startOfMonth());
		update_term_meta($term_id,'invs_amount_expired', (double) $invs_amount_expired);

		// Tính dự thu còn lại
		$payment_amount_remaining = $invs_amount - $payment_amount;
		update_term_meta($term_id,'payment_amount_remaining', (double) $payment_amount_remaining);

		// Tiến độ thanh toán
		$payment_percentage = div($payment_amount,$invs_amount);
		update_term_meta($term_id,'payment_percentage', $payment_percentage);
		return TRUE;
	}

	/**
	  * Tính tổng số ngày chạy dịch vụ dựa theo ngày hợp đồng
	  *
	  * @return     double  The real progress.
	  */
	public function calc_contract_days()
	{
		$this->is_exist_contract();

        $contract_begin = (int) get_term_meta_value($this->contract->term_id,'contract_begin');
        $contract_end 	= (int) get_term_meta_value($this->contract->term_id,'contract_end');
        return diffInDates(start_of_day($contract_begin), start_of_day($contract_end));
	}

	/**
	 * Calculates the contract value.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  The contract value.
	 */
	public function calc_contract_value(){ return 0; }

	/**
	 * Calculates the payment amount expired.
	 *
	 * @param      <type>     $start_time  The start time
	 * @param      <type>     $end_time    The end time
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The payment amount expired.
	 */
	public function calc_payment_amount_expired($start_time = FALSE,$end_time = FALSE)
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");
		$term_id = $this->contract->term_id;

		$payment_amount = $this->calc_payment_amount($term_id, FALSE, $end_time);
		$invs_value 	= $this->calc_invs_value($term_id, FALSE, $start_time);
		
		$ret = 0;
		if($payment_amount >= $invs_value) return $ret;

		$ret = $invs_value - $payment_amount;
		return $ret;
	}

	/**
	 * Calculates the total contract value.
	 *
	 * @param      <type>     $start_time  The start time
	 * @param      <type>     $end_time    The end time
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The total contract value.
	 */
	public function calc_invs_value($start_time = FALSE,$end_time = FALSE)
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");
		$term_id = $this->contract->term_id;

		if($start_time || $end_time)
		{
			$this->invoice_m->group_start();
			if($start_time)
			{
				$this->invoice_m
				->group_start()
					->where('start_date >=',$start_time)
					->where('start_date <=',$end_time)
				->group_end();
			}

			if($end_time)
			{
				$this->invoice_m
				->or_group_start()
					->where('end_date >=',$start_time)
					->where('end_date <',$end_time)
				->group_end();
			}
			$this->invoice_m->group_end();
		}

		$invoices = $this->invoice_m
		->select('posts.post_id, post_title,post_status,start_date,post_content,end_date')
		->join('term_posts','term_posts.post_id = posts.post_id')
		->order_by('posts.start_date')
		->get_many_by(['term_posts.term_id'=>$term_id,'posts.post_type'=>'contract-invoices']);

		if(empty($invoices)) return 0;

		$ret = 0;
		foreach ($invoices as $inv)
		{
			$ret+= (double) $this->invoice_item_m->get_total($inv->post_id,'total');
		}

		// Add tax amount
		if($tax = (int) get_term_meta_value($term_id,'vat'))
		{
			$ret+= ($ret*div($tax,100));
		}

		return $ret;
	}

	public function calc_service_provider_tax($budget = 0, $rate = 0.05)
	{
		parent::is_exist_contract(); // Determines if exist contract.
		return 0;
	}

	/**
	 * Calculates the total payment amount.
	 *
	 * @param      <type>     $start_time  The start time
	 * @param      <type>     $end_time    The end time
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The total payment amount.
	 */
	public function calc_payment_amount($start_time = FALSE,$end_time = FALSE)
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$term_id = $this->contract->term_id;

		$this->load->model('contract/receipt_m');

		if(!empty($start_time)) $this->receipt_m->where('posts.end_date >=',$start_time);

		$receipts = $this->receipt_m
		->select('posts.post_id')
		->join('term_posts','term_posts.post_id = posts.post_id','LEFT')
		->where('term_posts.term_id',$term_id)
		->where('posts.post_status','paid')
		->where_in('posts.post_type','receipt_payment')
		->order_by('posts.end_date','desc')
		->get_many_by();

		if(empty($receipts)) return 0;

		return array_sum(array_map(function($x){ return (double) get_post_meta_value($x->post_id,'amount'); }, $receipts));
	}

	/**
	 * Calculates the total payment amount.
	 *
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The total balance spend amount.
	 */
	public function calc_balance_spend()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$this->load->model('balance_spend_m');
		$balance_spend_items = $this->term_posts_m->get_term_posts($this->contract->term_id, $this->balance_spend_m->post_type);
		if(empty($balance_spend_items)) return 0;

		return array_sum(array_map('doubleval', array_column($balance_spend_items, 'post_content')));
	}

	/**
	 * Generate contract code by taxonomy
	 *
	 * @param      term_m  $term   The term
	 *
	 * @return     string  contract code
	 */
	public function gen_contract_code()
	{
		if( ! $this->contract) return FALSE;

		$contract 		= $this->contract;
		$contract_begin = get_term_meta_value($contract->term_id, 'contract_begin');
		$contract_begin = empty($contract_begin) ? my_date(time(), 'my') : my_date($contract_begin, 'my');

		$this->load->config('contract/contract');
		$taxonomy = $this->config->item($contract->term_type, 'taxonomy');

		$contract->term_name = strtoupper($contract->term_name);
		
		return sprintf("%s/%s/%s/%s",$contract->term_id, $contract_begin, $taxonomy, $contract->term_name);
	}

	/**
	 * Prepare the contract data for printable version
	 *
	 * @return     Array  The result data
	 */
	public function prepare_preview()
	{
		if( ! $this->contract) return FALSE;
		
		$contract 		= $this->contract;
		$term_id 		= $this->contract->term_id;
		$verified_on 	= get_term_meta_value($term_id, 'verified_on') ?: time();

		$this->load->model('term_users_m');
		$customers 	= $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company']);
		$customer 	= $customers ? end($customers) : (object) [];
		$gender 	= get_term_meta_value($term_id, 'representative_gender') == 1 ? 'Ông' : 'Bà';

		$vat 			= (int) get_term_meta_value($term_id, 'vat');
		$tranfer_type 	= $vat > 0 ? 'company' : 'person';
		$bank_info 		= $this->config->item($tranfer_type, 'bank_infos');

		
		$representative_zone = get_term_meta_value($term_id, 'representative_zone') ?: 'hcm';
		switch ($tranfer_type) {

			case 'person':
				$bank_info = array_map(function($bankInfo) {
					if(isset($bankInfo['representative_zone'])) unset($bankInfo['representative_zone']);
					return $bankInfo;
				}, array_filter($bank_info, function($x) use ($representative_zone){
					return $x['representative_zone'] == $representative_zone;
				}));
				break;

			default:

				/* Sau ngày 21.09 , thông tin chuyển khoản của miền Bắc sẽ chuyển thành TPBank */
				if($verified_on > start_of_day('2020/09/22') && $representative_zone == 'hn')
				{
					$_bank = $this->config->item('TPB-66852888203-CH', 'banks');
					$bank_info['Chủ tài khoản'] = $_bank['account'];
					$bank_info['Số tài khoản'] 	= $_bank['id'];
					$bank_info['Ngân hàng'] 	= $_bank['name'];
				}

				break;
		}

		$contract->extra 	= unserialize(get_term_meta_value($term_id,'extra'));
		$contract_begin		= get_term_meta_value($term_id, 'contract_begin');
		$contract_end		= get_term_meta_value($term_id, 'contract_end');

		$customer->representative = array(
			'address' 	=> get_term_meta_value($term_id, 'representative_address'),
			'phone' 	=> get_term_meta_value($term_id, 'representative_phone'),
			'name' 		=> get_term_meta_value($term_id, 'representative_name'),
			'position' 	=> get_term_meta_value($term_id, 'representative_position'),
			'email' 	=> get_term_meta_value($term_id, 'representative_email'),
			'loa' 		=> get_term_meta_value($term_id, 'representative_loa')
		);

		$customer->tax = $contract->extra['customer_tax'] ?? '';

		$websiteIds = get_term_meta($term_id, 'websites', FALSE, TRUE) ?: [];
		$websiteIds AND $websiteIds = array_map('intval', $websiteIds);
		$websiteIds = array_unique(array_merge($websiteIds, [ $this->contract->term_parent ]));

		$this->load->model('customer/website_m');
		$websites = $this->website_m->select('term_id, term_name')->set_term_type()->get_many($websiteIds);

		$data = array(
			'term' 			=> $contract,
			'term_id' 		=> $term_id,
			'is_rewrited'	=> (bool) get_term_meta_value($term_id, 'is_rewrited'),
			'contract_code' => get_term_meta_value($term_id, 'contract_code'),
			'representative_zone' => $representative_zone,
			/* Thông tin khách hàng */
			'customer'		=> $customer,
			'data_customer' => [
				'Địa chỉ' 		=> $customer->representative['address'],
				'Điện thoại' 	=> $customer->representative['phone'],
				'Mã số thuế' 	=> $customer->tax,
				'Đại diện' 		=> "{$gender} {$customer->representative['name']}",
				'Chức vụ' 		=> $customer->representative['position'],
				'Theo giấy ủy quyền số' => $customer->representative['loa']
				
			],

			/* Thông tin hợp đồng */
			'time' 				=> time(),
			'vat' 				=> (int) $vat,
			'verified_on' 		=> $verified_on,
			'contract_begin' 	=> $contract_begin,
			'contract_end' 		=> $contract_end,
			'contract_days' 	=> diffInDates($contract_begin, $contract_end),
			'contract_value' 	=> (int) get_term_meta_value($term_id, 'contract_value'),
			'bank_info'			=> $bank_info,

			'company_name'		=> $this->config->item('company','contract_represent'),

			'data_represent' 	=> array(
				'Địa chỉ' 		=> $this->config->item('address','contract_represent'),
				'Điện thoại' 	=> $this->config->item('phone','contract_represent'),
				'Mã số thuế' 	=> $this->config->item('tax','contract_represent'),
				'Đại diện' 		=> $this->config->item('name','contract_represent'),
				'Chức vụ' 		=> $this->config->item('position','contract_represent'),
			),

			'office' => $this->config->item('hcm', 'offices'),

			'printable_title'	=> $this->config->item($contract->term_type,'printable_title'),

			'websiteIds' => $websiteIds,
			'websites'	=> $websites,
			'websites_text' => implode(', ', array_column($websites, 'term_name')),
			'is_display_promotions_discount' => (int) get_term_meta_value($term_id, 'is_display_promotions_discount')
		);

		/* Người đại diện ký hợp đồng */
		$con_signature_config = $this->option_m->get_value('con_signature_config', TRUE);
		$con_signature_config = $con_signature_config ? array_values($con_signature_config) : [];
		if( empty($con_signature_config))
		{
			$_default = $this->config->item('default', 'con_signature_config');
			$con_signature_config = [$this->config->item($_default, 'con_signature_config')];
		}

		if( ! empty($con_signature_config))
		{
			$con_signature_config = array_column($con_signature_config, NULL, 'key');	
		}

		$staff_business = (int) get_term_meta_value($term_id, 'staff_business');
		if($staff_business)
		{
			$this->load->model('staffs/department_m');
			$this->load->model('term_users_m');

			$departments = $this->term_users_m->get_user_terms($staff_business, $this->department_m->term_type);
			if( ! empty($departments))
			{
				$department 	= reset($departments);
				$department_id 	= $department->term_id;
				if( ! empty($con_signature_config[$department_id]))
				{
					$con_signature_config = $con_signature_config[$department_id];
					$data['data_represent']['Điện thoại'] = $con_signature_config['phone'];
					$data['data_represent']['Địa chỉ'] = $con_signature_config['address'];
					$data['data_represent']['Đại diện'] = $con_signature_config['name'];
					$data['data_represent']['Chức vụ'] = $con_signature_config['title'];

					if( ! empty($con_signature_config['loa'])) $data['data_represent']['Theo giấy ủy quyền số'] = $con_signature_config['loa'];
				}
			}
		}

		$this->data = $data;
		return $this->data;
	}

	/* Clone new contract */
	public function clone()
	{
		$this->is_exist_contract();

		$term = $this->contract;

		$this->load->model('term_users_m');
		$this->load->model('customer/customer_m');

		$insert_data = (array) $term;
		$insert_data['term_status'] = 'unverified';
		unset($insert_data['term_id']);

		$insert_id = $this->contract_m->insert($insert_data);
		if( ! $insert_id) throw new Exception('Quá trình thêm mới dữ liệu không thành công . ');

		/* Clone Khách hàng */
		$customers = $this->term_users_m->get_the_users($term->term_id, $this->customer_m->customer_types);
		if( ! empty($customers))
		{
			$this->term_users_m->set_relations_by_term($insert_id, array_column($customers, 'user_id'), $this->customer_m->customer_types);
		}

		/* Clone các trường mặc định của hợp đồng */
		$metadata = get_term_meta_value($term->term_id);
		$metadata = key_value($metadata, 'meta_key', 'meta_value');

		$_contract_days = $this->calc_contract_days();
		$metadata['contract_begin'] = $metadata['contract_end'] < time() ? time() : $metadata['contract_end'];
		$metadata['contract_end'] 	= strtotime("+{$_contract_days} days", $metadata['contract_begin']);
		
		$_contract_m = new contract_m();
		$_contract_m->set_contract($insert_id);

		$metadata['contract_code'] 	= (new contract_m())->set_contract($insert_id)->get_contract_code();
		
		$metadata['created_on'] 	= time();
		$metadata['created_by'] 	= $this->admin_m->id;

		$this->config->load('contract/contract');
		$fields = array_intersect(array_keys($metadata), $this->config->item('metadata_contract'));
		if( ! empty($fields))
		{
			foreach ($fields as $f)
			{
				if(empty($metadata[$f])) continue;
				update_term_meta($insert_id, $f, $metadata[$f]);
			}
		}
		return $insert_id;
	}

	/**
	 * 
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     boolean    ( description_of_the_return_value )
	 */
	public function proc_verify()
	{
		if( ! $this->contract) return FALSE;

		$contract = $this->contract;

		if( ! in_array($contract->term_status, [ 'unverified' ])) throw new Exception("Trạng thái hợp đồng hiện tại không phù hợp để cấp số.");
        
        // Lock update customer
        $customer = $this->term_users_m->get_the_users($contract->term_id, [ 'customer_person', 'customer_company' ]);
        if(empty($customer)) throw new Exception('Không tìm thấy Thông tin khách hàng. Liên hệ quản trị viên để hướng dẫn xử lý');

        $customer = reset($customer);
        $this->load->model('customer_m');
        $_customer_m = new customer_m();
        $_customer_m->set_customer($customer);
        $_customer_m->lock();

		$this->contract_m
			->set_term_type()
			->update($contract->term_id, ['term_status' => 'waitingforapprove']);
		
		$contract_code = $this->gen_contract_code();
		
		update_term_meta($contract->term_id, 'contract_code', $contract_code);	
		update_term_meta($contract->term_id, 'verfied_by', $this->admin_m->id);
		update_term_meta($contract->term_id, 'verified_on', time());
		update_term_meta($contract->term_id, 'lock_editable', 1);

		$this->replace_contract_code();

		$this->load->model('contract/common_report_m');
		$this->common_report_m->init($contract);
		$this->common_report_m->send_contract_verified_email();

		// Detect if contract is the first signature (tái ký | ký mới)
		$this->load->model('contract/base_contract_m');
		$this->base_contract_m->detect_first_contract($contract->term_id);

		return true;
	}

	/**
	 * replace Writable Marked in Contract File
	 *
	 * @return     boolean  result
	 */
	public function replace_contract_code()
	{
		if( ! $this->contract) return FALSE;

		$contract = $this->contract;

		$this->load->model('contract/content_m');
		$_contract_content = $this->term_posts_m->get_term_posts($contract->term_id, 'contract_content');

		if(empty($_contract_content)) return true;

		$contract_code = get_term_meta_value($contract->term_id, 'contract_code');
		if(empty($contract_code)) return true;

		$_contract_content = reset($_contract_content);
		$_contract_content = $this->content_m
			->select('post_id, post_content')
			->get($_contract_content->post_id);

		$editableMarkedText = "( Số:  {$contract_code})";
		$is_rewrited 		= (bool) get_post_meta_value($_contract_content->post_id, 'is_rewrited');

		TRUE == $is_rewrited AND $editableMarkedText = "[ Số:  {$contract_code}]";

		$doc = new DomDocument;
        $doc->validateOnParse = true;
        @$doc->loadHtml($_contract_content->post_content);

        $domElement = $doc->getElementById('contract-code');

        if( ! $domElement || $domElement->textContent == $editableMarkedText) return true;

        $content = str_replace($domElement->textContent, $editableMarkedText, $_contract_content->post_content);

        $result = $this->content_m->update($_contract_content->post_id, [ 'post_content' => $content ]);

        $this->scache->delete_group("term/{$contract->term_id}_posts_");

        if( ! $result) return false;

        return true;
	}


    /**
	 * Compute service fee rate actual in scope type
	 */
	public function calc_range_service_fee_rate_actual($start_time = FALSE, $end_time = FALSE)
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$term_id = $this->contract->term_id;

        $service_fee_payment_type = get_term_meta_value($term_id, 'service_fee_payment_type');
        if('range' != $service_fee_payment_type){
            throw new Exception('Phương thức thanh toán phí dịch vụ	không phải là Thu phí dịch vụ theo mức ngân sách.', 1);
        }

        $service_fee_plan = get_term_meta_value($term_id, 'service_fee_plan');
        $service_fee_plan = is_serialized($service_fee_plan) ? unserialize($service_fee_plan) : [];
        if(empty($service_fee_plan)){
            throw new Exception('Hợp đồng chưa cấu hình mức % phí dịch vụ', 1);
        }

        $this->get_the_progress($start_time, $end_time);
        $actual_result = (double) get_term_meta_value($term_id, 'actual_result') ?: 0;
        $balance_spend = (double) get_term_meta_value($term_id, 'balance_spend') ?: 0;
        $result = $actual_result + $balance_spend;
        
        $service_fee_rate = array_column($service_fee_plan, 'service_fee_rate');
        $service_fee_rate = array_map(function($item){
            return (int)$item;
        }, $service_fee_rate);
        $service_fee_rate_actual = div(max($service_fee_rate), 100);
        if($result > 0){
            foreach ($service_fee_plan as $service_plan) {
                $from = (int)$service_plan['from'];
                $to = (int)$service_plan['to'];
                if ($result < $from) {
                    $service_fee_rate_actual = div((int)$service_plan['service_fee_rate'], 100);
                    break;
                }
    
                if ($from <= $result && $result < $to) {
                    $service_fee_rate_actual = div((int)$service_plan['service_fee_rate'], 100);
                    break;
                }

                $service_fee_rate_actual = div((int)$service_plan['service_fee_rate'], 100);
            }
        }

        update_term_meta($term_id, 'service_fee_rate_actual', $service_fee_rate_actual);

		return $service_fee_rate_actual;
	}

    /**
	 * Compute service fee actual in range type
	 */
	public function calc_range_service_fee($start_time = FALSE, $end_time = FALSE)
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$term_id = $this->contract->term_id;

        $service_fee_payment_type = get_term_meta_value($term_id, 'service_fee_payment_type');
        if('range' != $service_fee_payment_type){
            throw new Exception('Phương thức thanh toán phí dịch vụ	không phải là Thu phí dịch vụ theo mức ngân sách.', 1);
        }

		$this->get_the_progress($start_time, $end_time);
        $actual_result = (double) get_term_meta_value($term_id, 'actual_result') ?: 0;
        $balance_spend = (double) get_term_meta_value($term_id, 'balance_spend') ?: 0;
        $result = $actual_result + $balance_spend;
		$service_fee_rate_actual = (float) $this->calc_range_service_fee_rate_actual($start_time, $end_time);
        
        $service_fee = $result * $service_fee_rate_actual;
        update_term_meta($term_id, 'service_fee', $service_fee);

		return $service_fee;
	}
}
/* End of file Behaviour_m.php */
/* Location: ./application/modules/contract/models/Behaviour_m.php */