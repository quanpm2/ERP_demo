<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * A base model with a series of CRUD functions (powered by CI's query builder),
 * validation-in-model support, event callbacks and more.
 * 
 * @copyright Copyright (c) 2017, Thọ Nguyễn Hữu <thonh@webdoctor.vn>
 */

class Term_categories_m extends Base_model
{
	/**
     * This model's default primary key or unique identifier.
     * Used by the get(), update() and delete() functions.
     */
	public $primary_key = 'term_id';


	/**
     * This model's default database table. Automatically
     * guessed by pluralising the model name.
     */
	public $_table = "term_categories";


	/* --------------------------------------------------------------
     * GENERIC METHODS
     * ------------------------------------------------------------ */


	/**
     * Initialise the model
     */
    public function __construct()
    {
        parent::__construct();
    }

	
	/**
	 * Gets the categories.
	 *
	 * @param      integer  $term_id  The term identifier
	 * @param      array    $args     The arguments
	 *
	 * @return     array   The categories.
	 */
	function get_categories($term_id = 0, $args = array())
	{
		$term_id = (int) $term_id;

		$defaults 			= array('orderby' => 'term.term_id', 'order' => 'ASC', 'fields' => 'all', 'where'=>'');
		$args 				= wp_parse_args( $args, $defaults );
		$args_encrypt		= md5(json_encode($args));

		$key_cache  = "term/{$term_id}_categories_{$args_encrypt}";
		$result		= $this->scache->get($key_cache);
		if($result) return $result;

		if($args['fields'] !='all') $this->select($args['fields']);
		if(!empty($args['where'])) $this->where($args['where']);
		if(!empty($args['where_in'])) $this->where_in($args['where_in']);
		
		$categories = $this
		->join('term',"term.term_id = {$this->_table}.cat_id")
		->where("{$this->_table}.term_id",$term_id)
		->where_in('term.term_type','category')
		->order_by($args['orderby'],$args['order'])
		->get_all();

		if( ! $categories) return FALSE;

		$categories = array_column($categories, NULL,'term_id');
		$this->scache->write($categories,$key_cache);

		return $categories;
	}


	/**
	 * Sets the relations.
	 *
	 * @param      integer  $term_id     The term identifier
	 * @param      array    $categories  The categories
	 * @param      array    $args        The arguments
	 * @param      bool   	$replace     The replace
	 *
	 * @return     bool   	return TRUE if process sucess , otherwise return FALSE;
	 */
	function set_relations($term_id = 0,$categories = array(),$args = array(),$replace = FALSE)
	{
		$term_id = (int) $term_id;
		if(empty($term_id) || empty($categories)) return FALSE;

		$exists_categories 	= $this->get_categories($term_id,$args);
		$category_ids	= $exists_categories ? array_column($exists_categories, 'term_id') : array();

		// Replace all category relations if $replace set TRUE
		// then clean all exists $category_ids
		if($replace && !empty($category_ids))
		{
			$this->where_in('term_categories.cat_id', $category_ids)->where('term_id',$term_id)->delete_by();
			$category_ids = array();
		}

		foreach ($categories as $cat_id)
		{
			// if the relations exists in DB and $replace = FALSE then do nothing
			if(in_array($cat_id, $category_ids)) continue;

			$this->insert(['cat_id'=>$cat_id,'term_id'=>$term_id]);
		}

		// Clear All cache related
		$this->scache->delete_group("term/{$term_id}_categories_");	

		return TRUE;
	}

    function clear_relations($term_id = 0)
	{
		$term_id = (int) $term_id;
		if(empty($term_id)) return FALSE;

        $this->where('term_id',$term_id)->delete_by();

		// Clear All cache related
		$this->scache->delete_group("term/{$term_id}_categories_");	

		return TRUE;
	}
}
/* End of file Term_categories_m.php */
/* Location: ./application/modules/contract/models/Term_categories_m.php */