<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_contract_m extends Base_Model {

	public $data = array();

	function __construct() 
	{
		parent::__construct();
		$models = array('contract/contract_m','contract/invoice_m', 'term_posts_m');
		
		$this->load->model($models);
	}

	/**
	 * Determines if the Contract is first contract.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  True if first contract, False otherwise.
	 */
	public function detect_first_contract($term_id = 0)
	{
		$this->load->model('term_users_m');
		$this->load->model('customer/customer_m');

		$contract = $this->contract_m->select('term_id,term_parent,term_name,term_status,term_type')->set_term_type()->get($term_id);
		if( ! $contract) return FALSE;

		$all_contracts = $this->contract_m
		->select('term.term_id,term_parent,term_name,term_status')
		->order_by('term_id','asc')
		->get_many_by(['term_type'=> $contract->term_type, 'term_parent' => $contract->term_parent]);

		# Filter & remove all not started
		foreach ($all_contracts as $k => $term)
        {
        	# CHECK TO FIND CONTRACT WAS FIRST CONTRACT
        	# If TRUE , stop process and return FALSE
        	$is_first_contract = get_term_meta_value($term->term_id, 'is_first_contract');
        	if( ! empty($is_first_contract))
        	{
        		return FALSE;
        	}

            $start_service_time = get_term_meta_value($term->term_id, 'start_service_time');
            if(empty($start_service_time)) 
            {
            	unset($all_contracts[$k]);
            	continue;
            }

            $all_contracts[$k]->start_service_time = $start_service_time;
        }

        if(empty($all_contracts))
        {
        	update_term_meta($term_id, 'is_first_contract', 1);
        	return FALSE;
        }

        # SORT BY START_SERVICE_TIME ASC
        usort($all_contracts, function($x,$y) { return $x->start_service_time > $y->start_service_time ? TRUE : FALSE; });

        $first_contract = reset($all_contracts);
        update_term_meta($first_contract->term_id, 'is_first_contract', 1);
        return TRUE;
	}

	/**
	 * Creates a default invoice for webbuild contract.
	 * Required numbers_of_payment and contract_value
	 *
	 * @param      term_m  $term   The term
	 *
	 * @return     boolean  status of process
	 */
	public function create_default_invoice($term)
	{
		$contract_value = get_term_meta_value($term->term_id,'contract_value') ?: 0;
		if(empty($contract_value)) return FALSE; 

		$number_of_payments = get_term_meta_value($term->term_id,'number_of_payments') ?: 1;

		$contract_begin = get_term_meta_value($term->term_id,'contract_begin');
		$contract_end = get_term_meta_value($term->term_id,'contract_end');
		$num_dates = diffInDates($contract_begin,$contract_end);

		$num_days4inv = ceil(div($num_dates,$number_of_payments));
		$amount_per_payments = div($contract_value,$number_of_payments);

		$start_date = $contract_begin;
		$invoice_items = array();

		for($i = 0 ; $i < $number_of_payments; $i++)
		{	
			if($num_days4inv == 0) break;

			$end_date = $this->mdate->endOfDay(strtotime("+{$num_days4inv} day -1 second", $start_date));

			$inv_id = $this->invoice_m
			->insert(array(
				'post_title' => "Thu tiền đợt ". ($i + 1),
				'post_content' => "",
				'start_date' => $start_date,
				'end_date' => $end_date,
				'post_type' => $this->invoice_m->post_type
				));

			if(empty($inv_id)) continue;

			$this->term_posts_m->set_post_terms($inv_id, $term->term_id, $term->term_type);

			$quantity = $num_days4inv;

			$this->invoice_item_m->insert(array(
					'invi_title' => 'Giá dịch vụ',
					'inv_id' => $inv_id,
					'invi_description' => '',
					'invi_status' => 'publish',
					'price' => $amount_per_payments,
					'quantity' => 1,
					'invi_rate' => 100,
					'total' => $this->invoice_item_m->calc_total_price($amount_per_payments, 1, 100)
				));

			$start_date = strtotime('+1 second', $end_date);

			$day_end = $num_dates - $num_days4inv;
			if($day_end < $num_days4inv)
			{
				$num_days4inv = $day_end;
			}
		}

		return TRUE;
	}
	
	public function start_service($term){}

	/**
	 * Calculates the total payment amount.
	 *
	 * @param      <type>   $term_id  The term identifier
	 *
	 * @return     integer  The total payment amount.
	 */
	public function calc_payment_amount($term_id,$start_time = FALSE,$end_time = FALSE)
	{
		$this->load->model('contract/receipt_m');

		if(!empty($start_time))
		{
			$this->receipt_m->where('posts.end_date >=',$start_time);	
		}

		$receipts = $this->receipt_m
		->select('posts.post_id,posts.post_author,posts.post_type,posts.end_date,posts.post_status')
		->join('term_posts','term_posts.post_id = posts.post_id','LEFT')
		->where('term_posts.term_id',$term_id)
		->where('posts.post_status','paid')
		// ->where_in('posts.post_type',['receipt_payment','receipt_caution'])
		->where_in('posts.post_type','receipt_payment')
		->order_by('posts.end_date','desc')
		->get_many_by();

		if(empty($receipts)) return 0;

		$ret = 0;
		foreach ($receipts as $receipt)
		{
			$ret+= (double) get_post_meta_value($receipt->post_id,'amount');
		}

		return $ret;
	}

	/**
	 * Calculates the total contract value.
	 *
	 * @param      <type>   $term_id     The term identifier
	 * @param      <type>   $start_time  The start time
	 * @param      <type>   $end_time    The end time
	 *
	 * @return     integer  The total contract value.
	 */
	public function calc_invs_value($term_id,$start_time = FALSE,$end_time = FALSE)
	{
		$this->load->model('contract/invoice_m');

		if($start_time || $end_time)
		{
			$this->invoice_m->group_start();
			if($start_time)
			{
				$this->invoice_m
				->group_start()
					->where('start_date >=',$start_time)
					->where('start_date <=',$end_time)
				->group_end();
			}

			if($end_time)
			{
				$this->invoice_m
				->or_group_start()
					->where('end_date >=',$start_time)
					->where('end_date <',$end_time)
				->group_end();
			}
			$this->invoice_m->group_end();
		}

		$invoices = $this->invoice_m
		->select('posts.post_id, post_title,post_status,start_date,post_content,end_date')
		->join('term_posts','term_posts.post_id = posts.post_id')
		->order_by('posts.start_date')
		->get_many_by(['term_posts.term_id'=>$term_id,'posts.post_type'=>'contract-invoices']);

		if(empty($invoices)) return 0;

		$ret = 0;
		foreach ($invoices as $inv)
		{
			$ret+= (double) $this->invoice_item_m->get_total($inv->post_id,'total');
		}

		// Add tax amount
		if($tax = (int) get_term_meta_value($term_id,'vat'))
		{
			$ret+= ($ret*div($tax,100));
		}

		return $ret;
	}

	/**
	 * Calculates the payment amount expired.
	 *
	 * @param      <type>   $term_id     The term identifier
	 * @param      <type>   $start_time  The start time
	 * @param      <type>   $end_time    The end time
	 *
	 * @return     integer  The payment amount expired.
	 */
	public function calc_payment_amount_expired($term_id,$start_time = FALSE,$end_time = FALSE)
	{
		$payment_amount = $this->calc_payment_amount($term_id,FALSE,$end_time);
		$invs_value = $this->calc_invs_value($term_id,FALSE,$start_time);

		$ret = 0;
		if($payment_amount >= $invs_value) return $ret;

		$ret = $invs_value - $payment_amount;
		return $ret;
	}

	public function sync_all_amount($term_id = 0)
	{
		if(empty($term_id)) return FALSE;

		// Tính giá trị đã thanh toán
		$payment_amount = $this->calc_payment_amount($term_id);
		update_term_meta($term_id,'payment_amount',(int)$payment_amount);

		// Tính tổng hóa đơn cần thanh toán
		$invs_amount = $this->calc_invs_value($term_id);
		update_term_meta($term_id,'invs_amount',$invs_amount);

		// Tính dự thu quá hạn
		$invs_amount_expired = $this->calc_payment_amount_expired($term_id, $this->mdate->startOfMonth());
		update_term_meta($term_id, 'invs_amount_expired',(int) $invs_amount_expired);

		// Tính dự thu còn lại
		$payment_amount_remaining = $invs_amount - $payment_amount;
		update_term_meta($term_id, 'payment_amount_remaining', (int)$payment_amount_remaining);

		// Tiến độ thanh toán
		$payment_percentage = div($payment_amount,$invs_amount);
		update_term_meta($term_id, 'payment_percentage', $payment_percentage);

		return TRUE;
	}


	/**
	 * Sync bailment amount to contract
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     integer  ( description_of_the_return_value )
	 */
	public function sync_bailment_amount($term_id = 0)
	{
		$this->load->model('contract/receipt_m');
		$receipts = $this->receipt_m
		->select('posts.post_id,posts.end_date')
		->join('term_posts','term_posts.post_id = posts.post_id')
		->order_by('posts.end_date','desc')
		->get_many_by(['posts.post_type'=>'receipt_caution','posts.post_status'=>'publish','term_posts.term_id'=>$term_id]);

		if( ! $receipts )
		{
			update_term_meta($term_id,'bailment_amount',0);
			update_term_meta($term_id,'bailment_end_date',0);
			return TRUE;
		}

		$ret = 0;
		foreach ($receipts as $receipt)
		{
			$ret+= (double) get_post_meta_value($receipt->post_id,'amount');
		}

		$ins_receipt = reset($receipts);
		update_term_meta($term_id,'bailment_amount',$ret);
		update_term_meta($term_id,'bailment_end_date',$ins_receipt->end_date);

		return $ret;
	}

	/**
	 * Create new contract from exists contract
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function renew($term_id = 0)
	{
		return TRUE;
	}

    public function calc_service_fee($term_id = 0){
        if(empty($term_id)){
            return FALSE;
        }
        
        $service_fee = $this->calc_payment_amount($term_id);

        if ($vat = (int) get_term_meta_value($term_id, 'vat')) {
            $service_fee -= ($service_fee * div($vat, 100));
        }

        return max($service_fee, 0);
    }
}
/* End of file Base_contract_m.php */
/* Location: ./application/modules/contract/models/Base_contract_m.php */