<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Base_report_m.php');

class Common_report_m extends Base_report_m {

    public $receipt = NULL;

    /* Allow Include departments emails to any Email */
    protected   $inc_departments    = TRUE;

    /**
     * Sends a mail contract verified 2 admin.
     *
     * @return     Boolean  The result
     */
    public function send_contract_verified_email()
    {
        if( ! $this->term) return FALSE;

        $this->autogen_msg  = FALSE;

        $term           = $this->term;
        $term_id        = $this->term->term_id;
        $trading_type   = get_term_meta_value($term_id, 'trading_type');

        if($trading_type == 'agency') return FALSE;

        $this->set_mail_to('admin');

        $contract_code = get_term_meta_value($term_id,'contract_code');

        $created_by = get_term_meta_value($term_id, 'created_by');
        if( ! empty($created_by)) $this->add_to($this->admin_m->get_field_by_id($created_by, 'user_email'));

        $verfied_by = get_term_meta_value($term_id, 'verfied_by');
        if( ! empty($created_by)) $this->add_to($this->admin_m->get_field_by_id($verfied_by, 'user_email'));

        $this->config->load('contract/contract');
        $this->config->load('table_mail');

        $this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Thông tin hợp đồng');

        $this->table
        ->add_row('Link xem nhanh:', anchor(module_url("edit/{$term_id}")))
        ->add_row('Mã hợp đồng:', $contract_code)
        ->add_row('Dịch vụ:', $this->config->item($term->term_type, 'services'))
        ->add_row('Ngày tạo:', my_date(get_term_meta_value($term_id, 'created_on'),'d/m/Y'))
        ->add_row('Người tạo:', $this->admin_m->get_field_by_id($created_by, 'display_name'))
        ->add_row('Người duyệt:', $this->admin_m->get_field_by_id($verfied_by, 'display_name'));

        if( ! empty($this->sale)) $this->table->add_row('Kinh doanh phụ trách:', "#{$this->sale['user_id']} - {$this->sale['display_name']}");

        $this->table->add_row('Ghi chú (nguồn khách hàng) :',get_term_meta_value($term_id,'contract_note'));

        $contract_value = (int) get_term_meta_value($term_id,'contract_value');
        $this->table->add_row('Giá trị HĐ: ', currency_numberformat($contract_value));

        $vat = (int)get_term_meta_value($term_id,'vat');
        $this->table->add_row('VAT: ', currency_numberformat($vat,'%'));
        $this->table->add_row('Tổng cộng: ', currency_numberformat(($contract_value+$contract_value*div($vat,100))));

    	$content = "Hợp đồng {$contract_code} đã được cấp số" . $this->table->generate();

        $this->table->set_caption('Thông tin khách hàng');
        $content.= $this->table
        ->add_row('Người đại diện', get_term_meta_value($term_id,'representative_name'))
        ->add_row('Địa chỉ', get_term_meta_value($term_id,'representative_address'))
        ->add_row('Website', $term->term_name)
        ->add_row('Mail liên hệ', mailto(get_term_meta_value($term_id,'representative_email')))
        ->add_row('Số điện thoại', get_user_meta_value($this->customer->user_id ?? 0,'customer_phone'))
        ->generate();

        $this->data['subject'] = "[CẤP SỐ] {$contract_code} SSID:".time();
        $this->data['content'] = $content;

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        return $this->send_email();
    }

    /**
     * Send mail alert START-SERVICED to admin
     *
     * @param      integer  $term_id  The term identifier
     */
    public function send_new_contract_congratulation_email()
    {  
        if( ! $this->term) return FALSE;

        $this->autogen_msg  = FALSE;

        $term           = $this->term;
        $term_id        = $this->term->term_id;
        $trading_type   = get_term_meta_value($term_id, 'trading_type');

        if($trading_type == 'agency') throw new Exception('Hợp đồng này thuộc danh sách không thể gửi email [A]');
        if(empty($this->sale)) throw new Exception('Hợp đồng này hiện tại chưa được gán cho nhân viên kinh doanh phụ trách');
        
        $this->set_mail_to('admin');

        $user_group     = !empty($this->user_groups) ? reset($this->user_groups) : NULL;
        $display_name   = $this->sale['display_name'] ?: $this->sale['user_email'];

        $contract_type  = $this->config->item($term->term_type, 'taxonomy'); 
        $contract_value = get_term_meta_value($term->term_id, 'contract_value');
        $contract_value = currency_numberformat($contract_value, ' đ');
        $contract_code  = get_term_meta_value($term->term_id, 'contract_code');

        $gender     = get_user_meta_value($this->sale['user_id'], 'gender') ? 'anh' : 'chị';
        $subject    = mb_strtoupper("Chúc mừng {$gender} {$display_name} vừa ký được hợp đồng {$contract_type}");

        $this->data = wp_parse_args(array(
                'subject'        => $subject,
                'gender'         => $gender,
                'group_name'     => $user_group->term_name ?? '[Chưa xác định]',
                'display_name'   => $display_name,
                'user_avatar'    => $this->sale['user_avatar'],
                'contract_type'  => $contract_type,
                'contract_value' => $contract_value,
                'contract_code'  => $contract_code
        ), $this->data);

        // Randome template for this email
        $templates = array(
            'email/send_mail_start_service_congratulation_2admin_1',
            'email/send_mail_start_service_congratulation_2admin_2',
            'email/send_mail_start_service_congratulation_2admin_3');

        $this->data['content'] = $this->render_content($templates[array_rand($templates)], 'email/yellow_tp_2');

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        return parent::send_email();
    }

    /**
     * Posts a slack new contract message.
     *
     * @throws     Exception  (description)
     *
     * @return     integer    ( description_of_the_return_value )
     */
    public function postSlackNewContractMessage()
    {   
        if( ! $this->term) return FALSE;

        $term           = $this->term;
        $term_id        = $this->term->term_id;
        $trading_type   = get_term_meta_value($term_id, 'trading_type');

        if($trading_type == 'agency') throw new Exception('Hợp đồng này thuộc danh sách không thể gửi email [A]');
        if(empty($this->sale)) throw new Exception('Hợp đồng này hiện tại chưa được gán cho nhân viên kinh doanh phụ trách');

        $display_name   = $this->sale['display_name'] ?: $this->sale['user_email'];
        $gender         = get_user_meta_value($this->sale['user_id'], 'gender') ? 'anh' : 'chị';
        $user_avatar    = $this->sale['user_avatar'];

        $user_group     = !empty($this->user_groups) ? reset($this->user_groups) : NULL;
        $group_name     = $user_group->term_name ?? '[Chưa xác định]';

        $contract_type  = $this->config->item($term->term_type, 'taxonomy');
        $contract_value = currency_numberformat((int) get_term_meta_value($term->term_id, 'contract_value'), ' đ');
        
        $renewalType = get_term_meta_value($term->term_id, 'is_first_contract') ? 'ký chính thức' : 'tái ký';
        $message     = "Chúc mừng {$gender} *{$display_name}* (nhóm {$group_name}) \nKhách hàng *{$contract_type}* được {$gender} phục vụ chu đáo vừa *{$renewalType}* rồi ạ\nGiá trị hợp đồng: *{$contract_value}*";

        $blocks = array(
            [
                'type' => 'section',
                'text' => [
                    'type' => 'mrkdwn',
                    'text' => "Chúc mừng {$gender} *{$display_name}* (nhóm {$group_name}) \nKhách hàng được {$gender} phục vụ chu đáo vừa *{$renewalType}* rồi ạ"
                ]
            ],
            [
                'type' => 'section',
                'text' => [
                    'type' => 'mrkdwn',
                    'text' => "*Khách hàng:* {$contract_type}\n*Hợp đồng:* {$renewalType}\n*Giá trị hợp đồng: {$contract_value}*"
                ],
                'accessory' => [
                    'type' => 'image',
                    'image_url' => $user_avatar,
                    'alt_text' => "employee thumbnail"
                ]
            ],
            [
                'type' => "divider"
            ]
        );

        $logInserted = array(
            'log_type'      => 'contract-postSlackNewContractMessage',
            'log_status'    => 1,
            'term_id'       => $term->term_id,
            'log_title'     => $message
        );

        $is_payment_notification_slack_enabled = $this->option_m->get_value('is_payment_notification_slack_enabled');
        if( ! $is_payment_notification_slack_enabled)
        {
            $logInserted['log_status']  = 0;
            $logInserted['log_content'] = "thonh";
            $insertedId = $this->log_m->insert($logInserted);
            return $logInserted['log_status'];
        }

        try
        {
            $this->config->load('slack');
            $slack = new wrapi\slack\slack($this->config->item('token', 'slack'));
            $slack->chat->postMessage([
                'channel'   => 'general',
                'text'      => "Chúc mừng {$gender} *{$display_name}* (nhóm {$group_name}) \nKhách hàng được {$gender} phục vụ chu đáo vừa *{$renewalType}* rồi ạ",
                'blocks'    => json_encode($blocks),
                'icon_emoji' => ':flying-money:',
                'username'   => 'Tiền về :3'
            ]);
        }
        catch (Exception $e)
        {
            $logInserted['log_status']  = 0;
            $logInserted['log_content'] = $e->getMessage();
        }

        $this->log_m->insert($logInserted);

        return $logInserted['log_status'];
    }

    /**
     * Posts a slack new contract message.
     *
     * @throws     Exception  (description)
     *
     * @return     integer    ( description_of_the_return_value )
     */
    public function postSlackNewCustomerPayment()
    {
        if( ! $this->term) return FALSE;

        if(empty($this->sale)) throw new Exception('Hợp đồng này hiện tại chưa được gán cho nhân viên kinh doanh phụ trách');
        if(empty($this->receipt)) throw new Exception('Dữ liệu thanh toán không tồn tại. ');
        if( ! in_array($this->receipt->post_status, ['paid', 'publish'])) throw new Exception('Trạng thái đợt thanh toán đang soạn, không thể gửi. ');

        $term           = $this->term;
        $term_id        = $this->term->term_id;

        $user_group     = !empty($this->user_groups) ? reset($this->user_groups) : NULL;
        $display_name   = $this->sale['display_name'] ?: $this->sale['user_email'];
        $user_avatar    = $this->sale['user_avatar'];
        $contract_type  = $this->config->item($term->term_type, 'taxonomy'); 
        
        
        $gender     = get_user_meta_value($this->sale['user_id'], 'gender') ? 'anh' : 'chị';
        $group_name = $user_group->term_name ?? '[Chưa xác định]';

        $receiptAmount  = currency_numberformat((int) get_post_meta_value($this->receipt->post_id, 'amount'), ' đ');
        $contract_value = currency_numberformat((int) get_term_meta_value($term->term_id, 'contract_value'), ' đ');

        $renewalType    = get_term_meta_value($term->term_id, 'is_first_contract') ? 'ký mới' : 'tái ký';

        

        $message = "Chúc mừng {$gender} *{$display_name}* (nhóm {$group_name}), \nKhách hàng *{$contract_type}* *đã thanh toán* số tiền *{$receiptAmount}*\nGiá trị hợp đồng *{$contract_value}* ({$renewalType})";
        $blockSectionMessage = "Chúc mừng {$gender} *{$display_name}* (nhóm {$group_name})\nKhách hàng *đã thanh toán* số tiền *{$receiptAmount}*";

        if('receipt_caution' == $this->receipt->post_type)
        {
            $message = "Chúc mừng {$gender} *{$display_name}* (nhóm {$group_name}), \nHợp đồng của khách hàng *{$contract_type}* đã được *xác nhận kinh doanh bảo lãnh* số tiền *{$receiptAmount}*\nGiá trị hợp đồng *{$contract_value}* ({$renewalType})";
            $blockSectionMessage = "Chúc mừng {$gender} *{$display_name}* (nhóm {$group_name})\nHợp đồng của khách hàng đã được *xác nhận kinh doanh bảo lãnh* số tiền *{$receiptAmount}*";
        }

        $blocks = array(
            [
                'type' => 'section',
                'text' => [
                    'type' => 'mrkdwn',
                    'text' => $blockSectionMessage
                ]
            ],
            [
                'type' => 'section',
                'text' => [
                    'type' => 'mrkdwn',
                    'text' => "*Khách hàng:* {$contract_type}\n*Hợp đồng:* {$renewalType}\n*Giá trị hợp đồng: {$contract_value}*"
                ],
                'accessory' => [
                    'type' => 'image',
                    'image_url' => $user_avatar,
                    'alt_text' => "employee thumbnail"
                ]
            ],
            [
                'type' => "divider"
            ]
        );

        $logInserted = array(
            'log_type'      => 'contract-postSlackNewCustomerPayment',
            'log_status'    => 1,
            'term_id'       => $term->term_id,
            'log_title'     => $message
        );

        $is_payment_notification_slack_enabled = $this->option_m->get_value('is_payment_notification_slack_enabled');
        if( ! $is_payment_notification_slack_enabled)
        {
            $logInserted['log_status']  = 0;
            $logInserted['log_content'] = "thonh";
            $insertedId = $this->log_m->insert($logInserted);
            update_log_meta($insertedId, 'receiptId', $this->receipt->post_id);
            $this->clear();
            return $logInserted['log_status'];
        }

        try
        {
            $this->config->load('slack');
            $slack = new wrapi\slack\slack($this->config->item('token', 'slack'));
            $slack->chat->postMessage([
                'channel'   => 'general',
                'text'      => $message,
                'blocks'    => json_encode($blocks),
                'icon_emoji' => ':flying-money:',
                'username'   => 'Tiền về :3'
            ]);
        }
        catch (Exception $e)
        {
            $logInserted['log_status']  = 0;
            $logInserted['log_content'] = $e->getMessage();
        }

        $insertedId = $this->log_m->insert($logInserted);
        update_log_meta($insertedId, 'receiptId', $this->receipt->post_id);
        $this->clear();

        return $logInserted['log_status'];
    }

    /**
     * Posts a slack new contract message.
     *
     * @throws     Exception  (description)
     *
     * @return     integer    ( description_of_the_return_value )
     */
    public function postMoney24HNewCustomerPayment()
    {
        if( ! $this->term) return FALSE;
        
        if(empty($this->receipt)) throw new Exception('Dữ liệu thanh toán không tồn tại. ');
        if( ! in_array($this->receipt->post_status, ['paid', 'publish'])) throw new Exception('Trạng thái đợt thanh toán đang soạn, không thể gửi. ');

        $term           = $this->term;
        $term_id        = $this->term->term_id;

        $this->load->model('customer/customer_m');
        $customers = $this->term_users_m->get_the_users($this->term->term_id, $this->customer_m->customer_types);
        if(empty($customers)) throw new Exception('Thông tin khách hàng không hợp lệ.');

        $phones = array_map(function($x){
            return get_user_meta_value($x->user_id, 'customer_phone');
        }, $customers);

        $curators = get_term_meta_value($term_id, 'contract_curators');
        $curators = @unserialize($curators); 
        if( ! empty($curators))
        {
            $phones = array_merge($phones, array_map(function($x){ return preg_replace('/\s+/', '', $x['phone']); }, $curators));
            $phones = array_filter($phones);
        }

        $receiptAmount  = currency_numberformat((int) get_post_meta_value($this->receipt->post_id, 'amount'), ' đ');
        $heading        = "Adsplus xác nhận quý khách đã thanh toán {$receiptAmount}";
        $message        = "Cảm ơn quý khách đã tin tưởng dịch vụ Adsplus. Hệ thống xác nhận quý khách đã thanh toán số tiền {$receiptAmount}.";

        if('receipt_caution' == $this->receipt->post_type)
        {
            $heading = "Adsplus xác nhận quý khách đã được bảo lãnh số tiền {$receiptAmount}";
            $message = "Hợp đồng của khách hàng đã được xác nhận kinh doanh bảo lãnh số tiền {$receiptAmount}";
        }

        /* Push số liệu lên Money24h App */
        foreach ($phones as $phone)
        {
            try
            {
                (new \Money24h\Notifications())->send($heading, $message, $phone);
            }
            catch (Exception $e){}  
        }

        $logInserted = array(
            'log_type'      => 'contract-postMoney24hNewCustomerPayment',
            'log_status'    => 1,
            'term_id'       => $term->term_id,
            'log_title'     => $message
        );

        $insertedId = $this->log_m->insert($logInserted);
        update_log_meta($insertedId, 'receiptId', $this->receipt->post_id);

        $this->clear();

        return $logInserted['log_status'];
    }

    /**
     * Render Send content;
     *
     * @param      <type>  $content_view  The content view
     * @param      string  $layout        The layout
     *
     * @return     Mixed  Result
     */
    public function render_content($content_view, $layout = 'report/template/email/adsplus')
    {
        if( ! $content_view || ! $layout) return FALSE;

        $data = $this->data;
        if( ! is_array($data)) $data = array($data);

        $data['content'] = $this->load->view($content_view, $data, TRUE);
        $content        = $this->load->view($layout, $data, TRUE);
        if( ! $content) return FALSE;
        return $content;
    }

    public function setReceipt($receipt)
    {
        if(is_numeric($receipt))
        {
            $this->load->model('contract/receipt_m');
            $receipt = $this->receipt_m->set_post_type()->get($receipt);
        }

        if(empty($receipt)) return FALSE;

        $this->receipt = $receipt;
        return $this;
    }

    /**
     * Clear all data relate with contract
     *
     * @return     self
     */
    public function clear()
    {
        $this->receipt = null;

        return parent::clear();
    }
}
/* End of file Common_report_m.php */
/* Location: ./application/modules/contract/models/Common_report_m.php */