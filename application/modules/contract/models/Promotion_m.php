<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Receipt_m.php');

class Promotion_m extends Receipt_m
{
	public $post_type  = 'promotion';

	public $before_create = array('init_default_data');
	public $before_update = array('timestamp');

	// public $after_create = array('calc_amount_callback');
	public $after_update = array('recacl_amount_callback');

	final protected function recacl_amount_callback($data,$status = TRUE)
	{
		if(empty($data)) return FALSE;

		if(empty($data[1])) return FALSE;

		$post_id = end($data);
		if(empty($post_id)) return FALSE;

		$update_data = reset($data);
		if(empty($update_data['post_type']) || $update_data['post_type'] != $this->post_type) return FALSE;

		$this->load->model('term_posts_m');
		$post_terms = $this->term_posts_m->get_the_terms($post_id);	
		$term_id 	= reset($post_terms);
		
		if(empty($term_id)) return FALSE;
		
		/* Đồng bộ dữ liệu thanh toán phía hợp đồng */
		$this->load->model('contract/contract_m');
		$this->contract_m->set_contract($term_id);
		$this->contract_m->sync_all_amount();

		return TRUE;
	}

	final protected function init_default_data($data)
	{
		$data['post_status'] 	= $data['post_status'] ?? 'draft';
		$data['post_type']		= $this->post_type;
		return $data;	
	}


	/**
	 * Sets the post type.
	 *
	 * @return     self  set default post type query for this model
	 */
	final public function set_post_type()
	{
		$this->where('posts.post_type', $this->post_type);
		return $this;
	}


	/**
	 * Calculates the actual budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The actual budget.
	 */
	final public function calc_actual_budget()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		$this->amount 	= (double) get_post_meta_value($this->post_id, 'amount');
		return $this->amount;
	}

	/**
	 * Calculates the service fee.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	final public function calc_service_fee() { return 0; }

	/* phí giảm thỏa thuận (bao gồm VAT, FCT - nếu có)*/
	final public function calc_deal_reduction_fee() { return 0; }
	
	/**
	 * Gets the instance.
	 *
	 * @return     self  The instance.
	 */
	final public function get_instance()
	{
		if( ! $this->contract) return FALSE;
		return $this;
	}
}
/* End of file Receipt_m.php */
/* Location: ./application/modules/contract/models/Receipt_m.php */