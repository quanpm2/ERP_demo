<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Receipt_ads_m.php');

class Receipt_zaloads_m extends Receipt_ads_m
{
	/**
	 * Calculates the VAT amount
	 *
	 * @return     integer  The vat amount.
	 */
	public function calc_vat_amount()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$amount = (double) get_post_meta_value($this->post_id, 'amount');
		$vat 	= div((int) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		return div($amount, 1+ $vat)*$vat;
	}

	/**
	 * Calculates the fct.
	 * Đối với nhà cung cấp là doanh nghiệp trong nước thì thuế nhà thầu sẽ bằng 0
	 *
	 * @param      integer    $percent  The percent
	 *c
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The fct.
	 */
	public function calc_fct()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		return 0;
	}


	/**
	 * Calculates the actual budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The actual budget.
	 */
	public function calc_actual_budget()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		$this->amount 	= (double) get_post_meta_value($this->post_id, 'amount');
		if('receipt_payment_on_behaft' == $this->post_type) return $this->amount;
		if($this->post_type != 'receipt_payment') return FALSE;

		/* Số phí dịch vụ giảm giá */
		$contract = (new contract_m);
		$contract->set_contract($this->contract);
		$behaviour_m = $contract->get_behaviour_m();
		$discount_amount = (int) $behaviour_m->calc_disacount_amount();

		/* contract_budget_payment_types */
		$contract_budget_payment_type = get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type');
		if('customer' == $contract_budget_payment_type)
		{
			// Khách hàng tự thanh toán 
			$contract_budget_customer_payment_type = get_term_meta_value($this->contract->term_id, 'contract_budget_customer_payment_type') ?: 'normal';
			if('normal' == $contract_budget_customer_payment_type)
			{
				$service_fee 		= (double) get_term_meta_value($this->contract->term_id, 'service_fee');
				$contractBudget 	= $behaviour_m->calc_budget();
				$sfee_rate 			= div($service_fee, $contractBudget);

				// Trong trường hợp hợp đồng có giảm giá phí dịch vụ , cần được cộng dồn vào để tính đúng ngân sách thực tế sẽ chạy cho khách hàng . 
				if($discount_amount > 0)
				{
					$service_fee-= $discount_amount;
					$sfee_rate = div(max($service_fee, 0), $contractBudget);
				}

				return div($this->calc_service_fee(), $sfee_rate);
			}

			return 0;
		}

		$vat 			= div((int) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		$payment_amount = div($this->amount, 1 + $vat); /* Giá trị sau khi đã trừ VAT */

		$service_fee_payment_type = get_term_meta_value($this->contract->term_id , 'service_fee_payment_type');
		if('devide' == $service_fee_payment_type)
		{
			$service_fee 	= (int) get_term_meta_value($this->contract->term_id, 'service_fee');
			$discount_amount > 0 AND $service_fee-= $discount_amount;

			$contractBudget = (new contract_m)->set_contract($this->contract)->get_behaviour_m()->calc_budget();
			$sfee_rate 		= div(max($service_fee, 0), $contractBudget);

			return div($payment_amount, 1 + $sfee_rate);
		}

		return max([ ($payment_amount - $this->calc_service_fee()), 0 ]);
	}

	/**
	 * Calculates the service fee.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_fee()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$this->amount = (double) get_post_meta_value($this->post_id, 'amount');

		$vat = div((int) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);

		$payment_amount = div($this->amount, 1 + $vat); /* Giá trị sau khi đã trừ VAT */

		/* contract_budget_payment_types */
		$contract_budget_payment_type = get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type');
		if('customer' == $contract_budget_payment_type) return $payment_amount;

		/* NORMAL PAYMENT */
		$service_fee = (int) get_term_meta_value($this->contract->term_id, 'service_fee');
		
		/* Số phí dịch vụ giảm giá */
		$contract = (new contract_m);
		$contract->set_contract($this->contract);
		$behaviour_m = $contract->get_behaviour_m();
		$discount_amount = (int) $behaviour_m->calc_disacount_amount();
		$discount_amount > 0 AND $service_fee-= $discount_amount;

		$service_fee_payment_type = get_term_meta_value($this->contract->term_id , 'service_fee_payment_type');

		$contract = (new contract_m)->set_contract($this->contract);
		if('devide' == $service_fee_payment_type)
		{
			$contractBudget = $contract->get_behaviour_m()->calc_budget();
			$sfee_rate 		= div(max($service_fee, 0), $contractBudget);
			return $this->calc_actual_budget()*$sfee_rate;
		}
		
		/* Lấy ra danh sách tất cả các thanh toán trước post hiện tại */
		$receipts = $this->receipt_m
    	->select('posts.post_id,post_author,post_title,post_status,created_on,updated_on,post_type,end_date')
    	->select('term_posts.term_id')
    	->join('term_posts', "term_posts.post_id = posts.post_id AND term_posts.term_id = {$this->contract->term_id}")
    	->where('end_date <=', $this->end_date)
    	->where('post_status', 'paid')
    	->where('post_type', 'receipt_payment')
    	->set_post_type()
    	->as_array()
    	->order_by([ 'end_date' => 'asc', 'posts.post_id' => 'asc'])
    	->get_all();

    	if( 1 == count($receipts)) 
    	{
    		return min([$payment_amount, $service_fee]);
    	}

    	$_preReceipts = array_slice($receipts, 0, (int) array_search($this->post_id, array_column($receipts, 'post_id')));
    	if(empty($_preReceipts)) return $service_fee;

    	$_prePaymentAmount 	= array_sum(array_map(function($x) use ($vat){
    		return div((double) get_post_meta_value($x['post_id'], 'amount'), 1 + $vat);
    	}, $_preReceipts));

    	$result = max([ ($service_fee - $_prePaymentAmount), 0 ]);
    	return $result > $payment_amount ? $payment_amount : $result;
	}

	/* phí giảm thỏa thuận (bao gồm VAT, FCT - nếu có)*/
	public function calc_deal_reduction_fee()
	{
		/* Khách tự thanh toán hoặc thu hộ chi hộ */
		$isVatCompanyPay = (bool) get_term_meta_value($this->contract->term_id, 'isVatCompanyPay');
		if( ! $isVatCompanyPay) return 0;
		
		return $this->calc_vat_amount();
	}
}
/* End of file Receipt_m.php */
/* Location: ./application/modules/contract/models/Receipt_m.php */
