<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Receipt_m.php');

class Receipt_ads_manual_m extends Receipt_m
{
	/**
	 * Calculates the VAT amount
	 *
	 * @return     integer  The vat amount.
	 */
	public function calc_vat_amount()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$amount = (double) get_post_meta_value($this->post_id, 'amount');
		$vat 	= div((int) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		return div($amount, 1+ $vat)*$vat;
	}

	/**
	 * Calculates the fct.
	 *
	 * @param      integer    $percent  The percent
	 *c
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The fct.
	 */
	public function calc_fct()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		$contract_budget_payment_type = get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type');
		if('customer' == $contract_budget_payment_type) return false;
		if($this->post_type != 'receipt_payment') return FALSE;

		if('manual' != get_post_meta_value($this->post_id, 'input_method')) return false;

		$amount 		= (double) get_post_meta_value($this->post_id, 'amount');
		$amountNotVat 	= $amount - $this->calc_vat_amount();
		
		return (int) ($amountNotVat - (double) $this->calc_actual_budget() - (double) $this->calc_service_provider_tax() - (double) $this->calc_service_fee());
	}

	/**
	 * Calculates the actual budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The actual budget.
	 */
	public function calc_actual_budget()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		$contract_budget_payment_type = get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type');
		if('customer' == $contract_budget_payment_type) return false;
		if($this->post_type != 'receipt_payment') return FALSE;

		if('manual' != get_post_meta_value($this->post_id, 'input_method')) return false;

		$formula_input 	= get_post_meta_value($this->post_id, 'formula_input');
		$service_fee 	= (double) get_post_meta_value($this->post_id, 'service_fee');
		$amount 		= (double) get_post_meta_value($this->post_id, 'amount');
		$amountNotVat 	= $amount - $this->calc_vat_amount();

        // calc fct_rate = fct/(1-fct)
        $fct = div((int) get_post_meta_value($this->post_id, 'fct'), 100);
        $fct_rate = $fct > 0 ? div($fct, 1 - $fct) : 0;

		$service_provider_tax_rate = (float) get_post_meta_value($this->post_id, 'service_provider_tax_rate');

		if('service_fee' == $formula_input)
		{
            $actual_budget = div($amountNotVat - $service_fee, 1 + $service_provider_tax_rate + $fct_rate);
            
            update_post_meta($this->post_id, 'service_fee_rate', div($service_fee, $actual_budget) * 100);

			return $actual_budget;
		}

		$service_fee_rate_actual = (double) get_term_meta_value($this->contract->term_id, 'service_fee_rate_actual');
        update_post_meta($this->post_id, 'service_fee_rate', $service_fee_rate_actual * 100);

		return div($amountNotVat, 1 + $service_fee_rate_actual + $service_provider_tax_rate + $fct_rate);
	}

	/**
	 * Calculates the service fee.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_fee()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		$contract_budget_payment_type = get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type');
		if('customer' == $contract_budget_payment_type) return false;
		if($this->post_type != 'receipt_payment') return FALSE;
		
		if('manual' != get_post_meta_value($this->post_id, 'input_method')) return false;

		$formula_input 	= get_post_meta_value($this->post_id, 'formula_input');
		$service_fee 	= (double) get_post_meta_value($this->post_id, 'service_fee');
		if('service_fee' == $formula_input) {
            $actual_budget = $this->calc_actual_budget();
            
            update_post_meta($this->post_id, 'service_fee_rate', div($service_fee, $actual_budget) * 100);

            return $service_fee;
        }

		$service_fee_rate_actual = (double) get_term_meta_value($this->contract->term_id, 'service_fee_rate_actual');
        update_post_meta($this->post_id, 'service_fee_rate', $service_fee_rate_actual * 100);

		return $this->calc_actual_budget() * $service_fee_rate_actual;
	}

	/**
	 * Calculates the service provider tax.
	 * 
	 * Exp. Google Tax Fee apply after 01.11.2022
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_provider_tax()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		/**
		 * Phí của nhà cung cấp dịch vụ yêu cầu bắt buộc
		 * Tạm gọi tắt là "SPT"
		 * Ví dụ như thuế Google thu 5% áp dụng từ ngày 01.11.2022
		 */
		$service_provider_tax_rate = (float) get_post_meta_value($this->post_id, 'service_provider_tax_rate');
		return $this->calc_actual_budget() * $service_provider_tax_rate;
	}

	/* phí giảm thỏa thuận (bao gồm VAT, FCT - nếu có)*/
	public function calc_deal_reduction_fee()
	{
		return 0;
	}
}
/* End of file Receipt_m.php */
/* Location: ./application/modules/contract/models/Receipt_m.php */