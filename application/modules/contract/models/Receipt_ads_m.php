<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Receipt_m.php');

class Receipt_ads_m extends Receipt_m
{
	/**
	 * Calculates the VAT amount
	 *
	 * @return     integer  The vat amount.
	 */
	public function calc_vat_amount()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$amount = (double) get_post_meta_value($this->post_id, 'amount');
		$vat 	= div((int) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		return div($amount, 1+ $vat)*$vat;
	}

	/**
	 * Calculates the fct.
	 *
	 * @param      integer    $percent  The percent
	 *c
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The fct.
	 */
	public function calc_fct()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$fct = $this->get_fct_rate();
		if(empty($fct)) return 0;

		return (new contract_m)->set_contract($this->contract)->get_behaviour_m()->calc_fct($fct, $this->calc_actual_budget());
	}

	/**
	 * Get Fct of Contract
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The actual budget.
	 */
	public function get_fct_rate()
	{
		return (float) div((int) get_term_meta_value($this->contract->term_id, 'fct'), 100);
	}

	/**
	 * Calculates the actual budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The actual budget.
	 */
	public function calc_actual_budget()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		$this->amount 	= (double) get_post_meta_value($this->post_id, 'amount');
		if('receipt_payment_on_behaft' == $this->post_type) return $this->amount;
		if($this->post_type != 'receipt_payment') return FALSE;

		/* Số phí dịch vụ giảm giá */
		$contract = (new contract_m);
		$contract->set_contract($this->contract);
		$behaviour_m = $contract->get_behaviour_m();
		$discount_amount = (int) $behaviour_m->calc_disacount_amount();

		/* contract_budget_payment_types */
		$contract_budget_payment_type = get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type');
		if('customer' == $contract_budget_payment_type)
		{
			// Khách hàng tự thanh toán 
			$contract_budget_customer_payment_type = get_term_meta_value($this->contract->term_id, 'contract_budget_customer_payment_type') ?: 'normal';
			if('normal' == $contract_budget_customer_payment_type)
			{
				$service_fee 		= (double) get_term_meta_value($this->contract->term_id, 'service_fee');
				$contractBudget 	= $behaviour_m->calc_budget();
				$sfee_rate 			= div($service_fee, $contractBudget);

				// Trong trường hợp hợp đồng có giảm giá phí dịch vụ , cần được cộng dồn vào để tính đúng ngân sách thực tế sẽ chạy cho khách hàng . 
				if($discount_amount > 0)
				{
					$service_fee-= $discount_amount;
					$sfee_rate = div(max($service_fee, 0), $contractBudget);
				}

                update_post_meta($this->post_id, 'service_fee_rate', $sfee_rate * 100);

				return div($this->calc_service_fee(), $sfee_rate);
			}

            update_post_meta($this->post_id, 'service_fee_rate', 0);

			return 0;
		}

		$fct 			= $this->get_fct_rate();
		$vat 			= div((int) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		$payment_amount = div($this->amount, 1 + $vat); /* Giá trị sau khi đã trừ VAT */

		$service_fee_payment_type = get_term_meta_value($this->contract->term_id , 'service_fee_payment_type');

		/**
		 * Phí của nhà cung cấp dịch vụ yêu cầu bắt buộc
		 * Tạm gọi tắt là "SPT"
		 * Ví dụ như thuế Google thu 5% áp dụng từ ngày 01.11.2022
		 */
		$service_provider_tax_rate = (float) get_term_meta_value($this->contract->term_id, 'service_provider_tax_rate');

		/**
		 * Trường hợp thanh toán phí dịch vụ theo đợt 
		 * 
		 * Nghĩa là Ngân sách sẽ được tính dựa trên % phí dịch vụ thực tế từ số tiền khách hàng thanh toán mỗi đợt
		 */
		if('devide' == $service_fee_payment_type)
		{
			$service_fee 	= (int) get_term_meta_value($this->contract->term_id, 'service_fee');
			$discount_amount > 0 AND $service_fee-= $discount_amount;
			$contractBudget = (new contract_m)->set_contract($this->contract)->get_behaviour_m()->calc_budget();
			
			/** Tính ra % phí dịch vụ thực tế sau khi đã trừ ra phần giảm giá phí dịch vụ */
			$sfee_rate = div(max($service_fee, 0), $contractBudget);
            update_post_meta($this->post_id, 'service_fee_rate', $sfee_rate * 100);

			/**
			 * Ngân sách thực tế = Số tiền sau thuế sau thuế / ( 1 + { % VAT } + { % SPT } )
			 */
			return div($payment_amount, 1 + $sfee_rate + $service_provider_tax_rate + ($fct / (1 - $fct)) );
		}

        update_post_meta($this->post_id, 'service_fee_rate', div($this->calc_service_fee(), $payment_amount) * 100);
		$result = max([ ($payment_amount - $this->calc_service_fee()), 0 ]);
		if(empty($result)) return $result;

		/**
		 * Số tiền còn lại sau khi trừ đi phí dịch vụ cam kết trên hợp đồng
		 * Bao gồm : 
		 * - Ngân sách thực
		 * - Phí Google Áp dụng
		 * - Phí Nhà thầu ( sẽ hủy bỏ sau ngày 01.11.2022)
		 * 
		 * Ngân sách thực = Số tiền còn lại / ( 1 + {% SPT} + FCT (nếu có) )
		 */
		return div($result, 1 + $service_provider_tax_rate + ($fct / (1 - $fct)));
	}

	/**
	 * Calculates the service fee.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_fee()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$this->amount = (double) get_post_meta_value($this->post_id, 'amount');

		$vat = div((int) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);

		$payment_amount = div($this->amount, 1 + $vat); /* Giá trị sau khi đã trừ VAT */

		/* contract_budget_payment_types */
		$contract_budget_payment_type = get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type');
		if('customer' == $contract_budget_payment_type) return $payment_amount;

		/* NORMAL PAYMENT */
		$service_fee = (int) get_term_meta_value($this->contract->term_id, 'service_fee');
		
		/* Số phí dịch vụ giảm giá */
		$contract = (new contract_m);
		$contract->set_contract($this->contract);
		$behaviour_m = $contract->get_behaviour_m();
		$discount_amount = (int) $behaviour_m->calc_disacount_amount();
		$discount_amount > 0 AND $service_fee-= $discount_amount;

		$service_fee_payment_type = get_term_meta_value($this->contract->term_id , 'service_fee_payment_type');

		$contract = (new contract_m)->set_contract($this->contract);
		if('devide' == $service_fee_payment_type)
		{
			$contractBudget = $contract->get_behaviour_m()->calc_budget();
			$sfee_rate 		= div(max($service_fee, 0), $contractBudget);
			return $this->calc_actual_budget()*$sfee_rate;
		}
		
		/* Lấy ra danh sách tất cả các thanh toán trước post hiện tại */
		$receipts = $this->receipt_m
    	->select('posts.post_id,post_author,post_title,post_status,created_on,updated_on,post_type,end_date')
    	->select('term_posts.term_id')
    	->join('term_posts', "term_posts.post_id = posts.post_id AND term_posts.term_id = {$this->contract->term_id}")
    	->where('end_date <=', $this->end_date)
    	->where('post_status', 'paid')
    	->where('post_type', 'receipt_payment')
    	->set_post_type()
    	->as_array()
    	->order_by([ 'end_date' => 'asc', 'posts.post_id' => 'asc'])
    	->get_all();

    	if( 1 == count($receipts)) 
    	{
    		return min([$payment_amount, $service_fee]);
    	}

    	$_preReceipts = array_slice($receipts, 0, (int) array_search($this->post_id, array_column($receipts, 'post_id')));
    	if(empty($_preReceipts)) return $service_fee;

    	$_prePaymentAmount 	= array_sum(array_map(function($x) use ($vat){
    		return div((double) get_post_meta_value($x['post_id'], 'amount'), 1 + $vat);
    	}, $_preReceipts));

    	$result = max([ ($service_fee - $_prePaymentAmount), 0 ]);
    	return $result > $payment_amount ? $payment_amount : $result;
	}

	/**
	 * Calculates the service provider tax.
	 * 
	 * Exp. Google Tax Fee apply after 01.11.2022
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_provider_tax()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		/**
		 * Phí của nhà cung cấp dịch vụ yêu cầu bắt buộc
		 * Tạm gọi tắt là "SPT"
		 * Ví dụ như thuế Google thu 5% áp dụng từ ngày 01.11.2022
		 */
		$service_provider_tax_rate = (float) get_term_meta_value($this->contract->term_id, 'service_provider_tax_rate');
		return $this->calc_actual_budget() * $service_provider_tax_rate;
	}

	/* phí giảm thỏa thuận (bao gồm VAT, FCT - nếu có)*/
	public function calc_deal_reduction_fee()
	{
		$this->load->config('googleads/contract');

		$isNormalBudgetPaymentType 	= 'normal' == (get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type') ?: $this->config->item('default', 'contract_budget_payment_types'));
		$fct = div((int) get_term_meta_value($this->contract->term_id, 'fct'), 100);

		$service_provider_tax_rate = (float) get_term_meta_value($this->contract->term_id, 'service_provider_tax_rate');
		/**
		 * Quy trình bình thường
		 * 
		 * Bổ sung sau ngày 01.11.2022 đối với hợp đồng có Thuế Nhà cung cấp thu (vd. Google)
		 * thì không tính giá giảm từ thuế nhà thầu
		 */
		if($isNormalBudgetPaymentType && ! $service_provider_tax_rate) 
		{
			return $fct > 0 ? 0 : (new contract_m)->set_contract($this->contract)->get_behaviour_m()->calc_fct(0.05, $this->calc_actual_budget());
		}

		/* Khách tự thanh toán hoặc thu hộ chi hộ */

		$isVatCompanyPay = (bool) get_term_meta_value($this->contract->term_id, 'isVatCompanyPay');
		if( ! $isVatCompanyPay) return 0;

		return $this->calc_vat_amount();
	}
}
/* End of file Receipt_m.php */
/* Location: ./application/modules/contract/models/Receipt_m.php */
