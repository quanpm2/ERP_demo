<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_m extends Post_m {

	public $post_type = 'contract-invoices';
	public $before_delete = array('before_delete');
	public $before_update = array('before_update');
	public $meta;
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('contract/invoice_item_m');
		$this->load->model('term_posts_m');
	}

	public function generate_id($invoice = null)
	{
		if(isset($invoice->post_id))
		{
			return '#'.$invoice->post_id;
		} else if(is_numeric($invoice) || is_string($invoice))
		{
			return '#'.$invoice;
		}
		else
		{
			return 0;
		}
	}

	public function price_format($price = 0)
	{
		return numberformat($price).' VNĐ';
	}

	public function status_label($status = 0)
	{
		$this->config->load('invoice');
		$array_status = $this->config->item('status','invoice');
		return isset($array_status[$status]) ? $array_status[$status] : $status;
	}

	public function before_delete($id)
	{
		$this->invoice_item_m->delete_by('inv_id', $id);
	}

	public function before_update($data)
	{
		if( ! empty($data['post_status']))
		{
			$post_id = $data['post_id'];
			update_post_meta($post_id,'inv_status_updated_on',time());
		}

		// Xóa tất cả cache của các term chứa post
		if( ! empty($data['post_id']))
		{
			$term_ids = $this->term_posts_m->get_the_terms($data['post_id']);
			if( ! empty($term_ids)) array_map(function($x){ $this->scache->delete_group("term/{$x}_posts_"); return $x; }, $term_ids);
		}

		return $data;
	}

	public function list_all_invoices()
	{
		$invoices = $this
		->select('posts.post_id,post_title,post_type,start_date,end_date,post_status,term.term_id,term_name,term_type,term_parent')
		->join('term_posts','term_posts.post_id = posts.post_id')
		->join('term','term.term_id = term_posts.term_id')
		// ->where('start_date >=',$start_time)
		// ->where('end_date <=',$end_time)
		->where('post_type',$this->post_type)
		->order_by('start_date')
		->as_array()
		->get_many_by();

		if(!$invoices) return FALSE;

		$result = array();

		$this->load->model('term_users_m');

		$term_parent_ids = array_column($invoices,'term_parent');

		$customer_map_term = $this->term_users_m
		->select('term_id,user.user_id,user.user_type,user.display_name,user.user_email,user.user_status')
		->join('user','user.user_id = term_users.user_id','LEFT JOIN')
		->where_in('term_id',$term_parent_ids)
		->where_in('user_type',array('customer_person','customer_company'))
		->get_many_by();

		$customer_map_term = array_column($customer_map_term, NULL,'term_id');
		foreach ($invoices as $key => $inv) 
		{
			$invoices[$key]['customer'] = $customer_map_term[$inv['term_parent']];
		}

		foreach ($invoices as $invoice) 
		{
			$staff_business = get_term_meta_value($invoice['term_id'], 'staff_business');
			$staff_business = $staff_business ? $this->admin_m->get_field_by_id($staff_business,'display_name') : '--';

			$invoice_total = $this->invoice_item_m->get_total($invoice['post_id'], 'total');
			$vat = get_term_meta_value($invoice['term_id'], 'vat')?:0;
			$revenue = $invoice_total - ($invoice_total*div($vat,100));
			$service = $this->config->item($invoice['term_type'],'services');

			$inv_status_updated_on = get_post_meta_value($invoice['post_id'],'inv_status_updated_on');

			$row = array(
				my_date($inv_status_updated_on,'d/m/Y'),
				$invoice['customer']->display_name,
				get_term_meta_value($invoice['term_id'],'contract_code'),
				$invoice_total,
				$staff_business,
				$vat.'%',
				$revenue,
				$this->config->item($invoice['term_type'],'services')
				);
			$result[] = $row;
		}

		usort($result, function($a, $b){
               return strcasecmp($a[0], $b[0]);
           });

		return $result;
	}

	public function export_excel($start_time,$end_time)
	{
		$data = $this->list_all_invoices();

		if(!$data) return FALSE;
		
		$total = array_sum(array_column($data,3));
		$total_revenue = array_sum(array_column($data,6));
		$vat_money = $total - $total_revenue;

		$customer_count = count(array_unique(array_column($data, 1)));
		$contract_count = count(array_unique(array_column($data, 2)));

		$data = array_map(function($row){
			$row['3'] = currency_numberformat($row['3']);
			$row['6'] = currency_numberformat($row['6']);
			return $row;
		}, $data);

		$start_date = my_date($start_time,'d/m/Y');
		$end_date = my_date($end_time,'d/m/Y');
		$title = "Hóa đơn đã thu ( {$start_date} - {$end_date} )";

		$this->load->library('excel');
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
        ->getProperties()
        ->setCreator("WEBDOCTOR.VN")
        ->setLastModifiedBy("WEBDOCTOR.VN")
        ->setTitle($title);

        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/invoices/revenue_report.xlsx');
        $objPHPExcel->setActiveSheetIndex(0); 
        
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $objWorksheet->setCellValueByColumnAndRow(0, 1, $title); // set title report
        
        $objWorksheet->setCellValueByColumnAndRow(3, 5, $customer_count); // set customer count
        $objWorksheet->setCellValueByColumnAndRow(3, 6, $contract_count); // set contract count

        $objWorksheet->setCellValueByColumnAndRow(7, 5, $total); // set total money
        $objWorksheet->setCellValueByColumnAndRow(7, 6, $vat_money); // set vat money
        $objWorksheet->setCellValueByColumnAndRow(7, 7, $total_revenue); // set revenue

        $start_coordinate = array('x' => 1,'y' => 13);
        $objWorksheet->fromArray($data, NULL, 'A' . $start_coordinate['y'],TRUE);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->setIncludeCharts(TRUE);

        $folder_upload = "files/invoices/";
        if(!is_dir($folder_upload))
        {
            try 
            {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $ext = 'xlsx';
        $file_name = $folder_upload;
        $file_name.= 'thong-bao-thu-tien_'.my_date($start_time,'Y-m-d').'_to_'.my_date($end_time,'Y-m-d').'_created_on_'.my_date(0,'Y-m-d-H-i-s').'.'.$ext;

        try 
        {
            $objWriter->save($file_name);
        }
        catch (Exception $e) 
        {
            trigger_error($e->getMessage());
            return FALSE; 
        }

        return $file_name;
	}

	/**
	 * Calculates the estimate cost.
	 *
	 * @param      <type>   $term_id     The term identifier
	 * @param      <type>   $start_time  The start time
	 * @param      <type>   $end_time    The end time
	 *
	 * @return     integer  The estimate cost.
	 */
	public function calc_estimate_cost($term_id,$start_time = FALSE,$end_time = FALSE)
	{
		$ret_val = 0;
		return $ret_val;
	}
}
/* End of file Invoice_m.php */
/* Location: ./application/modules/googleads/models/Invoice_m.php */