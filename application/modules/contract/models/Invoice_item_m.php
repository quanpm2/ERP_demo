<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_item_m extends Base_Model {

	public $_table = 'invoice_items';
	public $primary_key = 'invi_id';


	public function get_total($inv_id = 0, $type = false)
	{
		if($inv_id <=0)
			return false;
		if(!$type)
		{
			$type = 'price,quantity,total';
		}
		$types = explode(',', $type);
		foreach($types as $t)
		{
			$this->invoice_item_m->select_sum($t);
		}
		$result = $this->invoice_item_m->get_by('inv_id',$inv_id);
		if(count($types) == 1)
		{
			return isset($result->{$type}) ? $result->{$type} : 0;
		}
		return $result;
	}

	public function calc_total_price($price = '', $quantity = 1, $rate = 100)
	{
		if(empty($price))
			$price = 0;
		return ($price * $quantity * $rate) / 100;
	}
}

/* End of file invoice_item_m.php */