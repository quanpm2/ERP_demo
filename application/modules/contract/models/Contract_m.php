<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Term_m.php');
require_once(APPPATH. 'modules/contract/models/Behaviour_m.php');

class Contract_m extends Term_m 
{
	protected 	$_copy = array();
	protected 	$contract = NULL;
	public 		$behaviour_model = NULL;
	public 		$term_type = NULL;

	/**
	 * Prepare data for clone contract
	 *
	 * @param      void  $primary_value  The primary value
	 */
	

	function __construct() 
	{
		parent::__construct();
		$this->load->config('contract/contract');
		$this->load->helper('array');
	}

	/**
	 * Get All Type configured of contracts
	 */
	public function getTypes()
	{
		$taxonomies 		= $this->config->item('taxonomy');
		return array_keys($taxonomies);
	}

	/**
     * Sets the term type.
     *
     * @return     this
     */	
	public function set_term_type()
	{
		if(empty($this->term_type))
		{
			/* Init term_type*/
			$taxonomies 		= $this->config->item('taxonomy');
			$this->term_type 	= array_keys($taxonomies);
			return $this->where_in('term.term_type', $this->term_type);
		}

		return is_array($this->term_type) ? $this->where_in('term.term_type', $this->term_type) : $this->where('term.term_type', $this->term_type);
	}

	/**
	 * Clone the specific contract
	 *
	 * @param      integer  $primary_value  The primary value
	 *
	 * @return     mixed   Insert ID if clone successful , FALSE otherwise
	 */
	public function copy($primary_value = 0)
	{

		if(empty($primary_value) && empty($this->_copy)) return FALSE;

		$this->prepare_copy($primary_value);

		if(empty($this->_copy)) return FALSE;

		$metadata = $this->_copy['metadata'] ?? [];
		$customer = $this->_copy['customer'] ?? [];

		unset($this->_copy['metadata'],$this->_copy['customer']);

		$insert_id = $this->insert($this->_copy);

		if(!empty($customer))
		{
			$this->term_users_m->set_term_users($insert_id, $customer, ['customer_person', 'customer_company'], ['command'=>'replace']);
		}

		if(empty($metadata)) return $insert_id;
		foreach ($metadata as $meta) 
		{
			if(!is_array($meta)) continue;
			foreach ($meta as $m) 
			{	
				update_term_meta($insert_id,$m['meta_key'], $m['meta_value']);
			}

			// DESTROY METADATAS SPECIFIED CANNOT CLONE
			$this->termmeta_m->delete_meta($insert_id, 'is_first_contract');
		}

		return $insert_id;
	}

	/**
	 * Gets the contract.
	 *
	 * @return     object  The copy.
	 */
	public function get_contract()
	{	
		return $this->contract;
	}

	/**
	 * Gets the copy.
	 *
	 * @return     array  The copy.
	 */
	public function get_copy()
	{	
		return $this->_copy;
	}

	/**
	 * Sets the copy.
	 *
	 * @param      array  $copy_data  The copy data
	 */
	public function set_copy($copy_data = array()){

		if(empty($copy_data)) return;

		if(!is_array($copy_data)) {

			$copy_data = (array) $copy_data;
		}

		$this->_copy = $copy_data;
	}

	/**
	 * Generate contract code by taxonomy
	 *
	 * @param      term_m  $term   The term
	 *
	 * @return     string  contract code
	 */
	public function gen_contract_code()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;
		return $this->behaviour_model->gen_contract_code();
	}

	/**
	 * Gets the contract model based on.
	 *
	 * @param      <type>  $term   The term
	 *
	 * @return     mixed  The contract model if EXISTS othersise return FALSE.
	 */
	public function get_contract_model($term)
	{
		$model = FALSE;

		switch ($term->term_type) 
		{
			case 'webgeneral' :
				$model = 'webgeneral/webgeneral_contract_m';
				break;

			case 'webdesign' :
				$model = 'webbuild/webbuild_contract_m';
				break;

			case 'weboptimize' :
				$model = 'weboptimize/weboptimize_contract_m' ;
				break;

			case 'hosting' :
				$model = 'hosting/hosting_contract_m';
				break;

			case 'domain' :
				$model = 'domain/domain_contract_m' ;
				break;

			case 'banner' :
				$model = 'banner/banner_contract_m';
				break;

			case 'courseads' :
				$model = 'courseads/courseads_contract_m';
				break;

			case 'google-ads' :
				$model = 'googleads/googleads_contract_m';
				break;

			case 'facebook-ads' :
				$model = 'facebookads/facebookads_contract_m' ;
				break;
            case 'zalo-ads' :
                $model = 'zaloads/zaloads_contract_m' ;
                break;

			case 'onead' :
				$model = 'onead/onead_contract_m' ;
				break;

            case 'oneweb' :
                $model = 'oneweb/oneweb_contract_m' ;
                break;

            case 'gws' :
                $model = 'gws/gws_contract_m' ;
                break;

			case 'pushdy': 
				$this->load->model('pushdy/pushdy_m');
				$contract = new pushdy_m();
				$contract->set_contract($this->get_contract());
				$model = $contract;
				break;
			
			default: break;
		}

		return $model;
	}

	/**
	 * Determines if service started.
	 *
	 * @param      <type>   $term   The term
	 *
	 * @return     boolean  True if service start, False otherwise.
	 */
	public function is_service_start($term)
	{
		if(empty($term)) return FALSE;
		$term_id = is_numeric($term) ? $term : $term->term_id;
		$started_service = get_term_meta_value($term_id,'started_service');
		return !empty($started_service);
	}

	/**
	 * Determines if service proc.
	 *
	 * @param      <type>   $term   The term
	 *
	 * @return     boolean  True if service proc, False otherwise.
	 */
	public function is_service_proc($term)
	{
		if(empty($term)) return FALSE;
		$term_id = is_numeric($term) ? $term : $term->term_id;
		$start_service_time = get_term_meta_value($term_id,'start_service_time');
		return !empty($start_service_time);
	}

	/**
	 * Determines if service end.
	 *
	 * @param      <type>   $term   The term
	 *
	 * @return     boolean  True if service end, False otherwise.
	 */
	public function is_service_end($term)
	{
		if(empty($term)) return FALSE;
		$term_id = is_numeric($term) ? $term : $term->term_id;
		is_numeric($term) AND $term = $this->get($term);
		return in_array($term->term_status, array('ending','liquidation'));
	}

	/**
	 * Determines if running.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     boolean    True if running, False otherwise.
	 */
	public function is_running()
	{
		if( ! $this->contract) throw new Exception("ID Hợp đồng không hợp lệ");

		$start_service_time = get_term_meta_value($this->contract->term_id, 'start_service_time');
		return !empty($start_service_time);
	}

	public function has_ended()
	{
		if( ! $this->contract) throw new Exception("ID Hợp đồng không hợp lệ");

		return in_array($this->contract->term_status, array('ending','liquidation'));
	}

	/**
	 * Stops a contract.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     bool   TRUE if successful, False otherwise
	 */
	public function stop_contract($term_id = 0)
	{
		$term = $this->get($term_id);
		if(empty($term)) return FALSE;

		$model = $this->get_contract_model($term);
		if(!empty($model)){
			$this->load->model($model,'tmp_contract_m');
			if(method_exists($this->tmp_contract_m,'stop_contract'))
				return $this->tmp_contract_m->stop_contract($term);
		}

		$this->update($term_id, array('term_status'=>'ending'));
		$time = time();
		update_term_meta($term_id,'end_contract_time',$time);
		$this->log_m->insert(array('log_type' =>'end_contract_time','user_id' => $this->admin_m->id,'term_id' => $term_id,'log_content' => my_date($time,'Y/m/d H:i:s')));
		
		return TRUE;
	}

	/**
	 * Check the domain is valid and exist in Database
	 *
	 * @param      string  $domain  The domain
	 *
	 * @return     mixed  Bolean if domain not exists or invalid , String if domain not exist and valid
	 */
	public function check_domain($url = '')
	{
		$url = preg_replace('/\s+/', '',trim(mb_strtolower($url)));
		
		if( ! $url) return FALSE;

		if( ! checkdomain($url)) return FALSE;

		$domain = parse_url(prep_url($url),PHP_URL_HOST);

		if(strpos($domain, 'www.') === 0)
		{
			$domain = str_replace('www.', '', strtolower($domain));
		}

		if($domain == 'facebook.com')
		{
			$path = parse_url($url,PHP_URL_PATH);
			$domain.= !empty($path) ? rtrim($path,'/') : '';
		}

		$term = $this->get_by(['term_name'=>$domain,'term_type'=>'website']);
		if(!empty($term)) return FALSE;
		
		return $domain;
	}


	/**
	 * Determines if it has verification permission.
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function has_verification_permission($term_id = 0)
	{
		if( ! has_permission('admin.contract.manage')) return FALSE;

		$term = $this->select('term_id')->set_term_type()->get_by(['term_id'=>$term_id,'term_status'=>'unverified']);
		if( ! $term) return FALSE;

		return TRUE;
	}


	/**
	 * Determines if it has start permission.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  True if has start permission, False otherwise.
	 */
	public function has_start_contract_permission($term_id = 0)
	{
		if( ! has_permission('contract.start_service')) return FALSE;
		
		$term = $this->select('term_id')->set_term_type()->get_by(['term_id'=>$term_id,'term_status'=>'waitingforapprove']);
		if( ! $term) return FALSE;

		// Kiểm tra tiến độ thanh toán để nhận biết hợp đồng đã được thanh toán chưa
		$payment_amount = get_term_meta_value($term_id,'payment_amount');
		if($payment_amount > 0) return TRUE;

		// Kiểm tra có bảo lãnh nào phát sinh không !?
		$this->load->model('contract/receipt_m');
		$receipt = $this->receipt_m->select('posts.post_id')->join('term_posts','term_posts.post_id = posts.post_id','LEFT')
		->order_by('posts.end_date','desc')
		->get_by(['term_posts.term_id' => $term_id,'posts.end_date >'=>time(),'posts.post_status !='=>'draft','posts.post_type'=>'receipt_caution']);

		if( ! $receipt) return FALSE;

		return TRUE;
	}


	/**
	 * Determines if it has verification permission.
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function has_stop_contract_permission($term_id = 0)
	{
		if( ! has_permission('contract.stop_contract')) return FALSE;

		$term = $this->select('term_id')->set_term_type()->where_in('term_status',['publish','pending'])->get($term_id);
		if( ! $term) return FALSE;

		return TRUE;
	}
	
	/**
	 * Determines ability to do.
	 *
	 * @param      <type>   $permission  The permission
	 * @param      integer  $user_id     The user identifier
	 */
	public function can($permission, $user_id = 0)
	{
		if( ! $this->contract) return false;
		return $this->has_permission($this->contract->term_id, $permission, ($user_id ?: $this->admin_m->id));
	}

	/**
	 * Determines if it has permission.
	 *
	 * @param      integer  $term_id     The term identifier
	 * @param      string   $permission  The permission
	 * @param      integer  $user_id     The user identifier
	 *
	 * @return     boolean  True if has permission, False otherwise.
	 */
	public function has_permission($term_id = 0, $permission = '', $user_id = 0)
	{
		if(empty($permission)) $permission = $this->router->fetch_class().'.'.$this->router->fetch_method().'.access';
		if(empty($user_id)) $user_id = $this->admin_m->id;

		$relate_users = $this->admin_m->get_all_by_permissions($permission, $user_id);
		if(is_bool($relate_users)) return $relate_users;

		$this->load->model('term_users_m');
		$users = $this->term_users_m->get_the_users($term_id, 'admin');
		if(empty($users)) return FALSE;

		$user_intersect = array_intersect($relate_users, array_column($users, 'user_id'));
		if(empty($user_intersect)) return FALSE;

		return TRUE;
	}

	/**
	 * Determines if it has lock permission.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  True if has lock permission, False otherwise.
	 */
	public function has_lock_permission($term_id = 0)
	{
		if( ! has_permission('contract.lock') 
			|| ! has_permission('admin.contract.manage')) return FALSE;

		return TRUE;
	}

	/**
	 * Determines if it has destroy permission.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  True if has destroy permission, False otherwise.
	 */
	public function has_destroy_permission($term_id = 0)
	{
		if( ! has_permission('contract.remove') 
			|| ! has_permission('admin.contract.manage')) return FALSE;

		$term = $this->select('term_id')->set_term_type()
		->where_not_in('term_status',['publish','pending','liquidation','remove','ending'])
		->get($term_id);

		if( ! $term) return FALSE;

		return TRUE;
	}

	/**
	 * Determines if it has renew contract permission.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  True if has renew contract permission, False otherwise.
	 */
	public function has_renew_contract_permission($term_id = 0)
	{
		$term = $this->select('term_id,term_type')->set_term_type()
		->where_in('term_type',['hosting','domain'])
		->where_in('term_status',['publish','pending','ending','liquidation'])
		->get($term_id);
		if( ! $term) return FALSE;

		if($term->term_type == 'domain' && !has_permission("domain.renew.update")) return FALSE;
		elseif($term->term_type == 'hosting' && !has_permission("hosting.renew.update")) return FALSE;

		$contract_end = (int) get_term_meta_value($term->term_id,'contract_end');
		if($contract_end > time()) return FALSE;

		return TRUE;
	}

    /**
	 * Determines if it has lock permission.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  True if has lock permission, False otherwise.
	 */
	public function has_manipulation_lock_permission()
	{
		if( !has_permission('contract.manipulation_lock.update') || 
            !has_permission('contract.manipulation_lock.manage')) return FALSE;

		return TRUE;
	}

    public function lock_manipulation($term_id, $time = ''){
        $status_manipulation_lock = $this->has_manipulation_lock_permission();
        if(!$status_manipulation_lock) return FALSE;

        $is_locked = (bool) get_term_meta_value($term_id, 'is_manipulation_locked');
        $_is_locked = force_var($is_locked, 1, 0);
        
        $_time = $time ?: time();
        if($_is_locked) update_term_meta($term_id, 'manipulation_locked_at', $_time);

        update_term_meta($term_id, 'is_manipulation_locked', $_is_locked);

        $this->log_m->insert([
            'log_type' =>'lock_manipulation', 
            'user_id' => $this->admin_m->id,
            'term_id' => $term_id,
            'log_content' => $_is_locked,
            'log_time_create' => my_date(time(),'Y/m/d H:i:s')
        ]);
    }

	/**
	 * Sets the contract.
	 *
	 * @param      Contract_m  $contract  The contract
	 *
	 * @return     self    ( description_of_the_return_value )
	 */
	public function set_contract($contract = NULL)
	{
		if(is_numeric($contract))
		{
			$contract = $this->set_term_type()->get((int) $contract);
		}

		if( ! $contract) return FALSE;

		$this->contract = $contract;
		$this->set_behaviour_m();

		return $this;
	}

	protected function set_behaviour_m()
	{
		if( ! $this->contract) return FALSE;

		if('tiktok-ads' == $this->contract->term_type)
		{
			$this->behaviour_model = \AdsService\Behaviours\Factory::build($this->contract->term_id);
			return $this;
		}

	 	$this->behaviour_model = (new behaviour_m())->set_contract($this->contract)->get_instance();
	 	return $this;
	}

	public function get_behaviour_m()
	{
		return $this->behaviour_model;
	}

	public function prepare_preview()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;
		
		return $this->behaviour_model->prepare_preview();
	}


	public function update_budget_metrics($budget = 0, $rate = null)
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		$rate OR $rate = $this->calc_service_fee_rate($budget);
		$service_fee = $this->calc_service_fee($budget, $rate);
		
		$budget AND update_term_meta($this->contract->term_id, 'contract_budget', $budget);

		update_term_meta($this->contract->term_id, 'original_service_fee_rate', $rate);
		update_term_meta($this->contract->term_id, 'service_fee', $service_fee);
		update_term_meta($this->contract->term_id, 'discount_amount', $this->calc_disacount_amount());
		update_term_meta($this->contract->term_id, 'service_provider_tax', $this->calc_service_provider_tax($budget, (float) get_term_meta_value($this->contract->term_id, 'service_provider_tax_rate')));

		return true;
	}

	/**
	 * Calculates the contract value.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  The contract value.
	 */
	public function calc_contract_value()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->calc_contract_value();
	}

	public function getTypeOfService()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->getTypeOfService();
	}	

	/**
	 * Calculates the service provider tax
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  The service provider tax
	 */
	public function calc_service_provider_tax($budget = 0, $rate = 0.05)
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->calc_service_provider_tax($budget, $rate);
	}
	
	/**
	 * Calculates the Actual service fee rate
	 *
	 * @return     boolean  The contract value.
	 */
	public function calc_service_fee_rate_actual()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;
		return $this->behaviour_model->calc_service_fee_rate_actual();
	}

	/**
	 * Calculates the service fee
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  The contract value.
	 */
	public function calc_service_fee($budget = 0, $rate = null)
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->calc_service_fee($budget, $rate);
	}

	/**
	 * Calculates the service fee
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  The contract value.
	 */
	public function get_service_rule($budget = 0)
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->get_service_rule($budget);
	}

	/**
	 * Calculates the service fee
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  The contract value.
	 */
	public function calc_service_fee_rate($budget = 0)
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->calc_service_fee_rate($budget);
	}

	/**
	 * Calculates the discount amount
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  The contract value.
	 */
	public function calc_disacount_amount()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->calc_disacount_amount();
	}

	/**
	 * Creates invoices.
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function clone()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->clone();
	}

	/**
	 * Creates invoices.
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function create_invoices()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->create_invoices();
	}

	/**
	 * Creates invoices.
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function delete_all_invoices()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->delete_all_invoices();
	}

	/**
	 * Sync All amount value of this contract
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function sync_all_amount()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->sync_all_amount();
	}

	/**
	 * Sync All Invoices Rate of this contract
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function sync_invoices_rate()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->sync_invoices_rate();
	}


	/**
	  * Tính tổng số ngày chạy dịch vụ dựa theo ngày hợp đồng
	  *
	  * @return     double  The real progress.
	  */
	public function get_contract_days()
	{
		if( ! $this->contract) return FALSE;

        $contract_begin = (int) get_term_meta_value($this->contract->term_id,'contract_begin');
        $contract_end 	= (int) get_term_meta_value($this->contract->term_id,'contract_end');
        return diffInDates(start_of_day($contract_begin), start_of_day($contract_end));
	}

	/**
	 * { function_description }
	 *
	 * @return     String  The contract code.
	 */
	public function get_contract_code()
	{
		if( ! $this->contract) return FALSE;

		$contract_code = get_term_meta_value($this->contract->term_id, 'contract_code');
		if($contract_code) return $contract_code;

		$this->load->config('contract/contract');

		$term_name 	= strtoupper($this->contract->term_name);
		$taxonomy 	= $this->config->item($this->contract->term_type, 'taxonomy');
		$date 		= get_term_meta_value($this->contract->term_id, 'contract_begin');
		$date 		= empty($date) ? my_date(time(),'my') : my_date($date,'my');
		$code 		= sprintf("%s/%s/%s/%s", $taxonomy, $this->contract->term_id, $date, $term_name);
		
		return strtoupper($code);
	}

	/**
	 * Determines if renewal.
	 *
	 * @return     bool  True if renewal, False otherwise.
	 */
	public function isRenewal()
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		$result = (bool) get_term_meta_value($this->contract->term_id, 'is_first_contract');
		return ! $result;
	}

	public function mutateIsRenewal()
	{
		if( ! $this->contract || ! $this->behaviour_model) return false;

		$term_id 	= $this->contract->term_id;

		$validStates = array(
			'waitingforapprove', /* Cấp số */
			'pending', /* Ngưng hoạt động */
			'publish', /* Thực hiện */
			'ending', /* Đã kết thúc */
			'liquidation', /* Thanh lý */
		);

		if( ! in_array( $this->contract->term_status, $validStates)) throw new Exception('Hợp đồng không đủ điều kiện để xác định trạng thái ký mới');

		$customer 	= $this->term_users_m->get_the_users($term_id, [ 'customer_person', 'customer_company' ]);
		if(empty($customer)) throw new Exception('Hợp đồng có cấu hình không hợp lệ. Liên hệ quản trị viên để hướng dẫn xử lý');

		$customer AND $customer = reset($customer);

		$contracts = $this->set_term_type()
		->join('term_users', 'term_users.term_id = term.term_id')
		->where('term_users.user_id', $customer->user_id)
		->where_in('term.term_status', $validStates)
		->get_all();
		
		# Filter & remove all not started
		foreach ($contracts as $k => $term)
        {
        	# CHECK TO FIND CONTRACT WAS FIRST CONTRACT
        	# If TRUE , stop process and return FALSE
        	$is_first_contract = get_term_meta_value($term->term_id, 'is_first_contract');
        	if( ! empty($is_first_contract))
        	{
        		return false;
        	}

            $start_service_time = get_term_meta_value($term->term_id, 'start_service_time');
            if(empty($start_service_time)) 
            {
            	unset($contracts[$k]);
            	continue;
            }

            $contracts[$k]->start_service_time = $start_service_time;
        }

        if(empty($contracts))
        {
        	update_term_meta($term_id, 'is_first_contract', 1);
        	return false;
        }

        # SORT BY START_SERVICE_TIME ASC
        usort($contracts, function($x,$y) { return $x->start_service_time > $y->start_service_time ? TRUE : FALSE; });

        $first_contract = reset($contracts);
        $first_contract AND update_term_meta($first_contract->term_id, 'is_first_contract', 1);

		return true;
	}
	
	/**
	 * Gets the field configuration for datatable.
	 *
	 * @return     Array  The field configuration.
	 */
	public function get_field_config()
	{
		$this->config->load('contract/fields');
		return $this->config->item('datasource');
	}

	public function get_field_callback()
	{
		return array(

			'contract_code' => array(
				'field' => 'contract_code',
				'func'	=> function($data, $row_name){
					
					$term_id = $data['term_id'];

					// Mã hợp đồng
					$contract_code = get_term_meta_value($term_id, 'contract_code');
					$data['contract_code_raw'] = $contract_code;

					$contract_code = $contract_code ?: '<code>HĐ chưa được cấp số</code>';

					if(has_permission('admin.contract.edit') || has_permission('admin.contract.add'))
					{
						$_lbl = mb_strlen($contract_code) > 50 ? (mb_substr($contract_code, 0,50).'...') : $contract_code;
						$contract_code = anchor(admin_url("contract/create_wizard/index/{$term_id}"), $_lbl, [
							'target' 		=> '_blank',
							'data-toggle' 	=> 'tooltip',
							'title' 		=> $contract_code, 
							'data-html' 	=> TRUE]);			
					}

					'RECEIVED' == get_term_meta_value($term_id, 'HardCoppyStatus') AND $contract_code.= "<i class='fa fa-fw fa-check-square-o'></i>";

					$contract_code = "<b>{$contract_code}</b><br/>";
					
					// Người tạo
					if($created_by = get_term_meta_value($term_id,'created_by'))
					{
						$created_display_name = $this->admin_m->get_field_by_id($created_by,'display_name'); 
						$contract_code.= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Người tạo'><i class='fa fa-fw fa-user'></i>Tạo bởi :{$created_display_name}</span>";

						$data['created_by'] = $created_display_name;
					}

					// Người tạo
					if($created_on = get_term_meta_value($term_id,'created_on'))
					{
						$data['created_on_raw'] = $created_on;
						$created_datetime = my_date($created_on,'d/m/Y');
						$contract_code.= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Ngày tạo'><i class='fa fa-fw  fa-clock-o'></i>Ngày tạo :{$created_datetime}</span>";

						$data['created_on'] = $created_datetime;
					}

					// Get customer display name
					if(!empty($data['term_name']))
					{
						$_label 	= mb_strlen($data['term_name']) > 50 ? (mb_substr($data['term_name'], 0,50).'...') : $data['term_name'];
						$website 	= anchor_popup(prep_url($data['term_name']),$_label);
						$contract_code.= "<span class='text-muted col-xs-12' data-toggle='tooltip' title='Trang thực hiện'><i class='fa fa-fw fa-globe'></i>Webpage : {$website}</span>";
					}

					// Get customer display name
					$customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company']);
					if( ! empty($customers))
					{
						$customer 		= end($customers);
						$display_name 	= mb_ucwords(mb_strtolower($customer->display_name));
						$_display_name 	= mb_strlen($display_name) > 60 ? (mb_substr($display_name, 0,60).'...') : $display_name;
						$contract_code.= br()."<span class='text-muted col-xs-12' data-toggle='tooltip' title='{$display_name}'><i class='fa fa-fw fa-user-secret'></i>KH - <b>".cid($customer->user_id, $customer->user_type)."</b> - {$_display_name}</span>";
					}

					$data['contract_code'] = $contract_code;

					return $data;
				},
				'row_data' => FALSE,
			),

			'customer_code' => array(
				'field' => 'customer_code',
				'func'	=> function($data, $row_name){

					$term_id = $data['term_id'];
					$customer = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company']);
					$customer AND $customer = reset($customer);
					$data['customer_code'] = $customer ? cid($customer->user_id, $customer->user_type) : '--';
					return $data;
				},
				'row_data' => FALSE,
			),

			'customer_display_name' => array(
				'field' => 'customer_display_name',
				'func'	=> function($data, $row_name){

					$term_id = $data['term_id'];
					$customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company']);
					if(empty($customers)) return $data;

					$customer = end($customers);
					$data['customer_display_name'] = $customer->display_name;
					return $data;
				},
				'row_data' => FALSE,
			),

			'customer_user_type' => array(
				'field' => 'customer_user_type',
				'func'	=> function($data, $row_name){

					$term_id = $data['term_id'];
					$customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company']);
					if(empty($customers)) return $data;

					$customer = end($customers);
					$data['customer_user_type'] = $this->config->item($customer->user_type, 'customer_type');
					return $data;
				},
				'row_data' => FALSE,
			),

			'customer_tax' => array(
				'field' => 'customer_tax',
				'func'	=> function($data, $row_name){
					$extra = @unserialize(get_term_meta_value($data['term_id'], 'extra'));
					$data['customer_tax'] = $extra['customer_tax'] ?? '';
					return $data;
				},
				'row_data' => FALSE,
			),

			'created_on' => array(
				'field' => 'created_on',	
				'func'	=> function($data, $row_name){

					if( ! empty($data['created_on'])) return $data;

					$created_on 			= get_term_meta_value($data['term_id'],'created_on');
					$data['created_on_raw'] = (int) $created_on;
					$data['created_on'] = $created_on ? my_date($created_on, 'd/m/Y') : '--';
					
					return $data;
				},
				'row_data' => FALSE,
			),

			'created_by' => array(
				'field' => 'created_by',
				'func'	=> function($data, $row_name){

					if( ! empty($data['created_by'])) return $data;

					$created_by 		= get_term_meta_value($data['term_id'], 'created_by');
					$created_by 		= $this->admin_m->get_field_by_id($created_by,'display_name');
					$data['created_by'] = $created_by ?: '--';
					return $data;
				},
				'row_data' => FALSE,
			),

			'contract_note' => array(
				'field' => 'contract_note',
				'func'	=> function($data, $row_name){

					if( ! empty($data['contract_note'])) return $data;
					
					$data['contract_note'] = get_term_meta_value($data['term_id'], 'contract_note');
					return $data;
				},
				'row_data' => FALSE,
			),

			'website' => array(
				'field' => 'website',
				'func'	=> function($data, $row_name){

					if( ! empty($data['website'])) return $data;

					$data['website'] = $data['term_name'] ?? '--';
					return $data;
				},
				'row_data' => FALSE,
			),

			'id' => array(
				'field' => 'id',
				'func'	=> function($data, $row_name){

					if( ! empty($data['id'])) return $data;

					$data['id'] = $data['term_id'];
					return $data;
				},
				'row_data' => FALSE,
			),

			'representative_name' => array(
				'field' => 'representative_name',
				'func'	=> function($data, $row_name){

					if( ! empty($data['representative_name'])) return $data;
					$data['representative_name'] = get_term_meta_value($data['term_id'], 'representative_name');
					return $data;
				},
				'row_data' => FALSE,
			),

			'representative_email' => array(
				'field' => 'representative_email',
				'func'	=> function($data, $row_name){

					if( ! empty($data['representative_email'])) return $data;
					$data['representative_email'] = get_term_meta_value($data['term_id'], 'representative_email');
					return $data;
				},
				'row_data' => FALSE,
			),

			'representative_phone' => array(
				'field' => 'representative_phone',
				'func'	=> function($data, $row_name){

					if( ! empty($data['representative_phone'])) return $data;
					$data['representative_phone'] = get_term_meta_value($data['term_id'], 'representative_phone');
					return $data;
				},
				'row_data' => FALSE,
			),

			'verified_on' => array(
				'field' => 'verified_on',
				'func'	=> function($data, $row_name){

					if( ! empty($data['verified_on'])) return $data;

					$verified_on 		= get_term_meta_value($data['term_id'],'verified_on');

					$data['verified_on_raw'] 	= $verified_on;
					$data['verified_on'] 		= $verified_on ? my_date($verified_on, 'd/m/Y') : '--';
					return $data;
				},
				'row_data' => FALSE,
			),

            'verfied_by' => array(
				'field' => 'verfied_by',
				'func'	=> function($data, $row_name){

					if( ! empty($data['verfied_by'])) return $data;

					$verfied_by              = get_term_meta_value($data['term_id'],'verfied_by');
                    $staff_business         = $this->admin_m->get_field_by_id($verfied_by, 'display_name');
					$data['verfied_by'] = $staff_business ?: '--';

					return $data;
				},
				'row_data' => FALSE,
			),

			'contract_begin' => array(
				'field' => 'contract_begin',
				'func'	=> function($data, $row_name){

					if( ! empty($data['contract_begin'])) return $data;

					$contract_begin 			= get_term_meta_value($data['term_id'], 'contract_begin');
					$data['contract_begin_raw'] = $contract_begin;
					$data['contract_begin'] 	= $contract_begin ? my_date($contract_begin, 'd/m/Y') : '--';
					return $data;
				},
				'row_data' => FALSE,
			),

			'contract_end' => array(
				'field' => 'contract_end',
				'func'	=> function($data, $row_name){

					if( ! empty($data['contract_end'])) return $data;

					$contract_end 				= get_term_meta_value($data['term_id'], 'contract_end');
					$data['contract_end_raw'] 	= $contract_end;
					$data['contract_end'] 		= $contract_end ? my_date($contract_end, 'd/m/Y') : '--';
					return $data;
				},
				'row_data' => FALSE,
			),

			'contract_daterange' => array(
				'field' => 'contract_daterange',
				'func'	=> function($data, $row_name){

					$term_id = $data['term_id'];
					$contract_begin = !empty($data['contract_begin'])?$data['contract_begin']:get_term_meta_value($term_id, 'contract_begin');
					if(is_numeric($contract_begin) || empty($contract_begin))
					{
						$data['contract_begin_raw'] = $contract_begin;

						$contract_begin 		= $contract_begin ? my_date($contract_begin,'d/m/Y') : '--';
						$data['contract_begin'] = $contract_begin;
					}

					$contract_end = !empty($data['contract_end'])?$data['contract_end']:get_term_meta_value($term_id, 'contract_end');
					if(is_numeric($contract_end) || empty($contract_end)) 
					{
						$data['contract_end_raw'] = $contract_end;

						$contract_end 			= $contract_end ? my_date($contract_end, 'd/m/Y') : '--';
						$data['contract_end'] 	= $contract_end;
					}

					$data['contract_daterange'] = "<i class='fa fa-fw fa-play' style='color:#72d072;font-size:0.8em'></i> {$contract_begin} <br/><i class='fa fa-fw fa-stop' style='color:#72d072;font-size:0.8em'></i> {$contract_end}";

					$data['contract_daterange'] = "{$contract_begin} - {$contract_end}";

					return $data;
				},
				'row_data' => FALSE,
			),

			'start_service_time' => array(
				'field' => 'start_service_time',
				'func'	=> function($data, $row_name){
					
					if( ! empty($data['start_service_time'])) return $data;

					$start_service_time = get_term_meta_value($data['term_id'],'start_service_time');
					$data['start_service_time_raw'] = $start_service_time;
					$data['start_service_time'] 	= $start_service_time ? my_date($start_service_time, 'd/m/Y') : '--';
					return $data;

				},
				'row_data' => FALSE,
			),

			'end_service_time' => array(
				'field' => 'end_service_time',
				'func'	=> function($data, $row_name){
					
					if( ! empty($data['end_service_time'])) return $data;

					$end_service_time = get_term_meta_value($data['term_id'],'end_service_time');
					$data['end_service_time_raw'] = $end_service_time;
					$data['end_service_time'] 	= $end_service_time ? my_date($end_service_time, 'd/m/Y') : '--';
					return $data;

				},
				'row_data' => FALSE,
			),

			'staff_business' => array(
				'field' => 'staff_business',
				'func'	=> function($data, $row_name){
					
					if( ! empty($data['staff_business'])) return $data;

					$staff_business 		= get_term_meta_value($data['term_id'], 'staff_business');
					$staff_business 		= $this->admin_m->get_field_by_id($staff_business,'display_name');
					$data['staff_business'] = $staff_business ?: '--';
					return $data;

				},
				'row_data' => FALSE,
			),

			'staff_advertise' => array(
				'field' => 'staff_advertise',
				'func'	=> function($data, $row_name){

					if( ! empty($data['staff_advertise'])) return $data;

					// CALLBACK : TECH USER
					$term_id 		= $data['term_id'];
					$service_type 	= get_term_meta_value($term_id,'service_type');
					if(empty($service_type)) return $data;
					
					$this->load->model('googleads/googleads_kpi_m');
					$kpis = $this->googleads_kpi_m->get_kpis($term_id,'users',$service_type);
					if(empty($kpis)) return $data;

					$staff_advertise = '';
					$user_ids = array();
					foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;

					$data['staff_advertise'] = implode(', ', array_values(array_map(function($user_id){
						$_user = $this->admin_m->get_field_by_id($user_id);
                        if(empty($_user))
                        {
                            return '--';
                        }

                        return $_user['display_name'] ?: $_user['user_email'];
					}, $user_ids)));


					return $data;

				},
				'row_data' => FALSE,
			),

			'fb_staff_advertise' => array(
				'field' => 'fb_staff_advertise',
				'func'	=> function($data, $row_name){

					if( ! empty($data['fb_staff_advertise'])) return $data;

					// CALLBACK : TECH USER
					$term_id 		= $data['term_id'];

					$this->load->model('facebookads/facebookads_kpi_m');
					$kpis = $this->facebookads_kpi_m->get_kpis($term_id, 'users', 'tech');
					if(empty($kpis)) return $data;
					
					$fb_staff_advertise = '';
					$user_ids = array();
					foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;

					$data['fb_staff_advertise'] = implode(', ', array_values(array_map(function($user_id){
						$_user = $this->admin_m->get_field_by_id($user_id);
                        if(empty($_user))
                        {
                            return '--';
                        }

						return $_user['display_name'] ?: $_user['user_email'];
					}, $user_ids)));


					return $data;

				},
				'row_data' => FALSE,
			),

            'tiktok_staff_advertise' => array(
				'field' => 'tiktok_staff_advertise',
				'func'	=> function($data, $row_name){

					if( ! empty($data['tiktok_staff_advertise'])) return $data;

					// CALLBACK : TECH USER
					$term_id 		= $data['term_id'];

					$this->load->model('tiktokads/tiktokads_kpi_m');
					$kpis = $this->tiktokads_kpi_m->get_kpis($term_id, 'users', 'tech');
					if(empty($kpis)) {
                        $data['tiktok_staff_advertise'] = '--';

                        return $data;
                    }
					
					$user_ids = array();
					foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;

					$data['tiktok_staff_advertise'] = implode(', ', array_values(array_map(function($user_id){
						$_user = $this->admin_m->get_field_by_id($user_id);
						return $_user['display_name'] ?: $_user['user_email'];
					}, $user_ids)));


					return $data;

				},
				'row_data' => FALSE,
			),

			'type' => array(
				'field' => 'type',
				'func'	=> function($data, $row_name){

					$data['type'] = $this->config->item($data['term_type'], 'taxonomy');
					return $data;
				},
				'row_data' => FALSE,
			),

			'status' => array(
				'field' => 'status',
				'func'	=> function($data, $row_name){

					$data['status'] = $this->config->item($data['term_status'],'contract_status');
					return $data;
				},
				'row_data' => FALSE,
			),

			'contract_value' => array(
				'field' => 'contract_value',
				'func'	=> function($data, $row_name){

					if( ! empty($data['contract_value'])) return $data;

					// Giá trị hợp đồng trước VAT
					$contract_value 		= get_term_meta_value($data['term_id'], 'contract_value');
					$data['contract_value_raw'] = $contract_value;
					$data['contract_value'] = currency_numberformat($contract_value, 'đ');
					return $data;
				},
				'row_data' => FALSE,
			),

			'contract_budget' => array(
				'field' => 'contract_budget',
				'func'	=> function($data, $row_name){

					if( ! empty($data['contract_budget'])) return $data;

					// Giá trị hợp đồng trước VAT
					$contract_budget 		= get_term_meta_value($data['term_id'], 'contract_budget');
					$data['contract_budget_raw'] 	= $contract_budget;
					$data['contract_budget'] 		= currency_numberformat($contract_budget, 'đ');
					return $data;
				},
				'row_data' => FALSE,
			),

			'actual_result' => array(
				'field' => 'actual_result',
				'func'	=> function($data, $row_name){

					if( ! empty($data['actual_result'])) return $data;

					$actual_result = 0;

					try
					{
						$contract_m = (new contract_m())->set_contract((object) $data);
						$actual_result = $contract_m->get_behaviour_m()->get_actual_result();
					}
					catch (Exception $e) {}
					
					$data['actual_result_raw'] 	= $actual_result;
					$data['actual_result'] 		= currency_numberformat($actual_result, 'đ');
					return $data;
				},
				'row_data' => FALSE,
			),

			'vat_amount' => array(
				'field' => 'vat_amount',
				'func'	=> function($data, $row_name){

					if( ! empty($data['vat_amount'])) return $data;

					try
					{
						$contract_m = (new contract_m())->set_contract((object) $data);
						$vat_amount = $contract_m->get_behaviour_m()->cacl_vat_amount();
					}
					catch (Exception $e)
					{
						$vat_amount = 0;
					}


					$data['vat_amount_raw'] 	= $vat_amount;
					$data['vat_amount'] 		= currency_numberformat($vat_amount, 'đ');
					return $data;
				},
				'row_data' => FALSE,
			),

			'fct_amount' => array(
				'field' => 'fct_amount',
				'func'	=> function($data, $row_name){

					if( ! empty($data['fct_amount'])) return $data;

					$contract_m = (new contract_m())->set_contract((object) $data);
					try
					{
						$fct        = (double) $contract_m->get_behaviour_m()->get_fct_tax();
						$budget     = $contract_m->get_behaviour_m()->calc_actual_budget();
						$fct_amount = $contract_m->get_behaviour_m()->calc_fct($fct, $budget);
					} 
					catch (Exception $e) {
						$fct_amount = 0;
					}

					$data['fct_amount_raw'] 	= $fct_amount;
					$data['fct_amount'] 		= currency_numberformat($fct_amount, 'đ');
					return $data;
				},
				'row_data' => FALSE,
			),

			'budget' => array(
				'field' => 'budget',
				'func'	=> function($data, $row_name){

					if( ! empty($data['budget'])) return $data;

					$budget = 0;
					try
					{	
						$contract_m 	= (new contract_m())->set_contract((object) $data);
						$behaviour_m 	= $contract_m->get_behaviour_m();
						method_exists($behaviour_m, 'calc_actual_budget') AND $budget = $behaviour_m->calc_actual_budget();
					}
					catch (Exception $e) {}

					$data['budget_raw'] 	= $budget;
					$data['budget'] 		= currency_numberformat($budget, 'đ');
					return $data;
				},
				'row_data' => FALSE,
			),

			'service_fee' => array(
				'field' => 'service_fee',
				'func'	=> function($data, $row_name){

					if( ! empty($data['service_fee'])) return $data;

					// Giá trị hợp đồng trước VAT
					$service_fee 		= get_term_meta_value($data['term_id'], 'service_fee');
					$data['service_fee_raw'] = $service_fee;
					$data['service_fee'] = currency_numberformat($service_fee, 'đ');
					return $data;
				},
				'row_data' => FALSE,
			),

			'service_fee_rate_actual' => array(
				'field' => 'service_fee_rate_actual',
				'func'	=> function($data, $row_name){

					if( ! empty($data['service_fee_rate_actual'])) return $data;
					
					$service_fee_rate_actual 			= (double) get_term_meta_value($data['term_id'], 'service_fee_rate_actual');
					$data['service_fee_rate_actual'] 	= currency_numberformat($service_fee_rate_actual*100, '%', 2);
					return $data;
				},
				'row_data' => FALSE,
			),

			'payment_service_fee' => array(
				'field' => 'payment_service_fee',
				'func'	=> function($data, $row_name){

					if( ! empty($data['payment_service_fee'])) return $data;

					$payment_service_fee = 0;

					try
					{
						$contract_m 			= (new contract_m())->set_contract((object) $data);
						$payment_service_fee 	= $contract_m->get_behaviour_m()->calc_payment_service_fee();
					}
					catch (Exception $e) {}

					$data['payment_service_fee_raw'] = $payment_service_fee;
					$data['payment_service_fee'] = currency_numberformat($payment_service_fee, 'đ');
					return $data;
				},
				'row_data' => FALSE,
			),

			'vat' => array(
				'field' => 'vat',
				'func'	=> function($data, $row_name){

					if( ! empty($data['vat'])) return $data;

					// Giá trị hợp đồng trước VAT
					$vat 			= get_term_meta_value($data['term_id'], 'vat');
					$data['vat'] 	= currency_numberformat($vat,'%');
					return $data;
				},
				'row_data' => FALSE,
			),

			'fct' => array(
				'field' => 'fct',
				'func'	=> function($data, $row_name){

					if( ! empty($data['fct'])) return $data;

					// Giá trị hợp đồng trước fct
					$fct 			= get_term_meta_value($data['term_id'], 'fct');
					$data['fct'] 	= currency_numberformat($fct,'%');
					return $data;
				},
				'row_data' => FALSE,
			),

			'is_first_contract' => array(
				'field' => 'is_first_contract',
				'func'	=> function($data, $row_name){

					if( ! empty($data['is_first_contract'])) return $data;

					$is_first_contract = get_term_meta_value($data['term_id'],'is_first_contract');
					$data['is_first_contract'] = $is_first_contract ? '<i class="fa fa-fw fa-star"></i>' : '<i class="fa fa-fw fa-star-o"></i>';
					$data['is_first_contract_raw'] = $is_first_contract ? 'Ký mới' : 'Tái ký';

					return $data;
				},
				'row_data' => FALSE,
			),

			'mcm_client_id' => array(
				'field' => 'mcm_client_id',
				'func'	=> function($data, $row_name){

					if( ! empty($data['mcm_client_id'])) return $data;
					$mcm_client_id = (int) get_term_meta_value($data['term_id'], 'mcm_client_id');
					if(empty($mcm_client_id)) return $data;

					$data['mcm_client_id'] = get_term_meta_value($mcm_client_id, 'customer_id');
					return $data;
				},
				'row_data' => FALSE,
			),

			'adaccount_id' => array(
				'field' => 'adaccount_id',
				'func'	=> function($data, $row_name){

					if( ! in_array($data['term_type'], [ 'google-ads', 'facebook-ads' ])) return $data;

					$_adaccounts = get_term_meta($data['term_id'], 'adaccounts', FALSE, TRUE);
					$_adaccounts AND $_adaccounts = array_filter(array_map('intval', $_adaccounts));
					if(empty($_adaccounts)) return $data;

					$_adaccounts = array_flip($_adaccounts);

					foreach($_adaccounts as $adaccount_id => &$value)
					{
						if('google-ads' == $data['term_type'])
						{
							$value = [
								'account_name'	=> get_term_meta_value($adaccount_id, 'account_name'),
								'customer_id'	=> get_term_meta_value($adaccount_id, 'customer_id'),
							];

							continue;
						}

						$key_cache 		= "term/facebookads-{$adaccount_id}";
						$_adaccount 	= $this->scache->get($key_cache);
						if($_adaccount)
						{
							$value = [
								'account_name'	=> $_adaccount->term_name,
                				'customer_id'	=> $_adaccount->term_slug,
							];
						}

						$this->load->model('facebookads/adaccount_m');
						$facebookads = $this->adaccount_m->set_term_type()->get($adaccount_id);
						if(empty($facebookads)) continue;
						$this->scache->write($facebookads, $key_cache, 3600);
					}

					$data['adaccount_id'] = $data['adaccount_id_raw'] = implode(', ', array_column($_adaccounts, 'customer_id'));
					return $data;
				},
				'row_data' => FALSE,
			),

			'adaccount_name' => array(
				'field' => 'adaccount_name',
				'func'	=> function($data, $row_name){
					
					if( ! in_array($data['term_type'], [ 'google-ads', 'facebook-ads' ])) return $data;

					$_adaccounts = get_term_meta($data['term_id'], 'adaccounts', FALSE, TRUE);
					$_adaccounts AND $_adaccounts = array_filter(array_map('intval', $_adaccounts));
					if(empty($_adaccounts)) return $data;

					$_adaccounts = array_flip($_adaccounts);

					foreach($_adaccounts as $adaccount_id => &$value)
					{
						if('google-ads' == $data['term_type'])
						{
							$value = [
								'account_name'	=> get_term_meta_value($adaccount_id, 'account_name'),
								'customer_id'	=> get_term_meta_value($adaccount_id, 'customer_id'),
							];

							continue;
						}

						$key_cache 		= "term/facebookads-{$adaccount_id}";
						$_adaccount 	= $this->scache->get($key_cache);
						if($_adaccount)
						{
							$value = [
								'account_name'	=> $_adaccount->term_name,
                				'customer_id'	=> $_adaccount->term_slug,
							];
						}

						$this->load->model('facebookads/adaccount_m');
						$facebookads = $this->adaccount_m->set_term_type()->get($adaccount_id);
						if(empty($facebookads)) continue;
						$this->scache->write($facebookads, $key_cache, 3600);
					}

					$data['adaccount_name'] = $data['adaccount_name_raw'] = implode(', ', array_column($_adaccounts, 'account_name'));
					return $data;
				},
				'row_data' => FALSE,
			),

			'adaccount_status' => array(
                'field' => 'adaccount_status',    
                'func'  => function($data, $row_name){

                    if( ! empty($data['adaccount_status'])) return $data;

                    $this->config->load('facebookads/facebookads');

                    $term_id            = $data['term_id'];
                    $states             = $this->config->item('states', 'adaccount_status');
                    $adaccount_status   = get_term_meta_value($term_id, 'adaccount_status') ?: $this->config->item('default', 'adaccount_status');

                    $data['adaccount_status']       = $states[$adaccount_status] ?? 'UNKNOWN';
                    $data['adaccount_status_raw']   = $states[$adaccount_status] ?? 'UNKNOWN';

                    return $data;
                },
                'row_data' => FALSE,
            ),

			'typeOfService' => array(
                'field' => 'typeOfService',    
                'func'  => function($data, $row_name){

                    if( ! empty($data['typeOfService'])) return $data;

                    $this->config->load('googleads/contract');

                    $term_id            = $data['term_id'];
                    $typeOfServices		= $this->config->item('items', 'typeOfServices');
                    $typeOfService   	= get_term_meta_value($term_id, 'typeOfService') ?: '';

                    $data['typeOfService']       = $typeOfServices[$typeOfService] ?? '';
                    $data['typeOfService_raw']   = $data['typeOfService'];

                    return $data;
                },
                'row_data' => FALSE,
            ),

			'contract_budget_payment_type' => array(
                'field' => 'contract_budget_payment_type',    
                'func'  => function($data, $row_name){

                    if( ! empty($data['contract_budget_payment_type'])) return $data;

                    $term_id = $data['term_id'];
                    $contract_budget_payment_type = get_term_meta_value($term_id, 'contract_budget_payment_type');

					$this->config->load('googleads/contract');
					$contract_budget_payment_types = $this->config->item('enums', 'contract_budget_payment_types');

					if(empty($contract_budget_payment_type) || empty($contract_budget_payment_types[$contract_budget_payment_type])) return $data;

                    $data['contract_budget_payment_type']       = $contract_budget_payment_types[$contract_budget_payment_type] ?? '';
                    $data['contract_budget_payment_type_raw']   = $data['contract_budget_payment_type'];
					
                    return $data;
                },
                'row_data' => FALSE,
            ),

            'service_fee_payment_type' => array(
                'field' => 'service_fee_payment_type',    
                'func'  => function($data, $row_name){

                    if( ! empty($data['service_fee_payment_type'])) return $data;

                    $term_id = $data['term_id'];
                    $service_fee_payment_type = get_term_meta_value($term_id, 'service_fee_payment_type');

					$this->config->load('contract/contract');
					$service_fee_payment_types = $this->config->item('service_fee_payment_type');

					if(empty($service_fee_payment_type) || empty($service_fee_payment_types[$service_fee_payment_type])) return $data;

                    $data['service_fee_payment_type']       = $service_fee_payment_types[$service_fee_payment_type] ?? '--';
                    $data['service_fee_payment_type_raw']   = $data['service_fee_payment_type'];
					
                    return $data;
                },
                'row_data' => FALSE,
            ),
		);
	}

	/**
	 * Get the customer associated with the contract.
	 *
	 * @return mixed Returns the customer object if found, or FALSE otherwise.
	 */
	public function getCustomer()
	{
		if (!$this->contract) {
			return false;
		}

		$customer = $this->term_users_m->get_the_users($this->contract->term_id, ['customer_person', 'customer_company']);
		$customer = reset($customer);

		$customer->cid = get_user_meta_value($customer->user_id, 'cid');

		return $customer;
	}
    
    /**
     * start_service
     *
     * @throws Exception Invalid data or cannot start_service
     * @return bool
     */
    public function start_service()
    {
        if(empty($this->contract->term_id))
        {
            throw new Exception("Không tìm thấy hợp đồng");
        }

        // Update contract status to publish
        $this->term_m->update($this->contract->term_id, array('term_status'=> 'publish'));
        update_term_meta($this->contract->term_id,'started_service', time());

        $this->log_m->insert(array(
            'log_type' =>'started_service',
            'user_id' => $this->admin_m->id,
            'term_id' => $this->contract->term_id,
            'log_content' => date('Y/m/d H:i:s')
        ));

        $model = $this->contract_m->get_contract_model($this->contract);
        if(! $model)
        {
            throw new Exception("Loại hợp đồng không khả dụng");
        }
        
        // Excute background process after service depend on contract type,
        // Such as send_started_email, ...
        if(is_string($model))
        {
            $this->load->model('contract/base_contract_m');
            $this->load->model($model,'tmp_contract_m');
            if( ! method_exists($this->tmp_contract_m, 'start_service'))
            {
                throw new Exception("Loại hợp đồng không khả dụng");
            }

            $this->tmp_contract_m->start_service($this->contract);
        }
        else
        {
            $model->start_service();
        }

        $this->load->model('contract/common_report_m');
        $this->common_report_m->init($this->contract)->send_new_contract_congratulation_email();

        /* truyền thông chúc mừng nhân viên kinh doanh đã ký được hợp đồng */
        $this->contract_m->set_contract($this->contract->term_id);
        $this->mutateIsRenewal();
        $this->common_report_m->init($this->contract)->postSlackNewContractMessage();

        return TRUE;
    }
}
//------------------------------------------------------------------------------
// Helper Functions
//------------------------------------------------------------------------------

if (! function_exists('is_service_end')) {
    function is_service_end($term){
    	return get_instance()->contract_m->is_service_end($term);
    }
}

if (! function_exists('is_service_proc')) {
    function is_service_proc($term){
    	return get_instance()->contract_m->is_service_proc($term);
    }
}

if (! function_exists('is_service_start')) {
    function is_service_start($term){
    	return get_instance()->contract_m->is_service_start($term);
    }
}

/* End of file Contract_m.php */
/* Location: ./application/modules/contract/models/Contract_m.php */