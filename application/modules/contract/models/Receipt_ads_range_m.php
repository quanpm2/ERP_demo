<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Receipt_m.php');

class Receipt_ads_range_m extends Receipt_m
{
	/**
	 * Calculates the VAT amount
	 *
	 * @return     integer  The vat amount.
	 */
	public function calc_vat_amount()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$service_fee = (double) get_post_meta_value($this->post_id, 'amount');
		$vat_rate 	= div((float) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((float) get_term_meta_value($this->contract->term_id, 'vat'), 100);

		return div($service_fee, 1 + $vat_rate) * $vat_rate;
	}

	/**
	 * Calculates the fct.
	 *
	 * @param      integer    $percent  The percent
	 *c
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The fct.
	 */
	public function calc_fct()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		if('receipt_payment_on_behaft' == $this->post_type) return 0;
		
		$fct = (double) div((int) get_term_meta_value($this->contract->term_id, 'fct'), 100);
		if(empty($fct)) return 0;
		return (new contract_m)->set_contract($this->contract)->get_behaviour_m()->calc_fct($fct, $this->calc_actual_budget());
	}


	/**
	 * Calculates the actual budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The actual budget.
	 */
	public function calc_actual_budget()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		if(!in_array($this->post_type, ['receipt_payment', 'receipt_payment_on_behaft'])) return FALSE;

		$this->amount 	= (double) get_post_meta_value($this->post_id, 'amount');
        $vat_rate 	= div((float) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((float) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		$payment_amount = div($this->amount, 1 + $vat_rate);

        $service_fee_rate = div((double) get_post_meta_value($this->post_id, 'service_fee_rate'), 100);

		return div($payment_amount, $service_fee_rate);
	}

	/**
	 * Calculates the service fee.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_fee()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		if('receipt_payment_on_behaft' == $this->post_type) return 0;

        $vat_rate 	= div((float) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((float) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		return div($this->amount, 1 + $vat_rate);
	}

	/* phí giảm thỏa thuận (bao gồm VAT, FCT - nếu có)*/
	public function calc_deal_reduction_fee()
	{
		// $this->load->config('googleads/contract');

		// $isNormalBudgetPaymentType 	= 'normal' == (get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type') ?: $this->config->item('default', 'contract_budget_payment_types'));

		// $fct = div((int) get_term_meta_value($this->contract->term_id, 'fct'), 100);

		// /* Quy trình bình thường */
		// if($isNormalBudgetPaymentType) 
		// {
		// 	return $fct > 0 ? 0 : (new contract_m)->set_contract($this->contract)->get_behaviour_m()->calc_fct(0.05, $this->calc_actual_budget());
		// }

		// /* Khách tự thanh toán hoặc thu hộ chi hộ */

		// $isVatCompanyPay = (bool) get_term_meta_value($this->contract->term_id, 'isVatCompanyPay');
		// if( ! $isVatCompanyPay) return 0;

		// return $this->calc_vat_amount();

        return 0;
	}
}
/* End of file Receipt_m.php */
/* Location: ./application/modules/contract/models/Receipt_m.php */
