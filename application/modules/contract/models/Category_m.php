<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_m extends Term_m 
{
	public $term_type = 'category';
	
	public function __construct() 
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('text');
	}


	/**
	 * Sets the term type 
	 *
	 * @return     this
	 */
	public function set_term_type()
	{
		return $this->where('term.term_type',$this->term_type);
	}


	public function add($data)
	{
		if(empty($data)) return FALSE ;

		$data['term_type']		= $this->term_type;
		$data['term_slug']  	= convert_accented_characters(url_title($data['term_name'], '-', TRUE)) ;
		//$data['term_status']	= $this->term_type;

		$term_id = $this->term_m->insert($data) ;
		if(! $term_id ) return FALSE;

		$meta = array('created' => time(), 'created_by' => $this->admin_m->id);
		foreach ($meta as $meta_key => $meta_value) {
			update_term_meta($term_id,$meta_key,$meta_value) ;
		}

		return $term_id;
	}


	public function get_categories() 
	{
		$terms = $this->select()->where('term_type', $this->term_type)->order_by('term_name', 'ASC')->get_many_by() ;
		if(empty($terms)) return FALSE;
		
		return $terms;
	}

	public function edit($term_id = 0, $data = null)
	{
		if(empty($term_id)) return FALSE ;
		if(empty($data)) return FALSE ;

		$data['term_type']	= $this->term_type;
		$data['term_slug']  = convert_accented_characters(url_title($data['term_name'], '-', TRUE)) ;

		$status  			= $this->term_m->update($term_id, $data);

		if(FALSE === $status) return FALSE;

		$meta = array('modified' => time(), 'modified_by' => $this->admin_m->id);
		foreach ($meta as $meta_key => $meta_value) {
			update_term_meta($term_id,$meta_key,$meta_value) ;
		}

		return TRUE;
	}
}