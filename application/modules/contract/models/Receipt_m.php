<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receipt_m extends Post_m
{
	public $before_create = array('init_default_data');
	public $before_update = array('timestamp');

	// public $after_create = array('calc_amount_callback');
	public $after_update = [
        'active_contract', 
        'recacl_amount_callback'
    ];

	function __construct()
	{
		parent::__construct();
	}

	public function calc_amount_callback($insert_id)
	{
		if(!$insert_id) return FALSE;

		$receipt = $this->get($insert_id);
		if(empty($receipt)) return FALSE;

		if('receipt_payment_on_behaft' == $receipt->post_type)
		{
			$this->addJobCalcMetadata($insert_id);
			return FALSE;
		}

		if($receipt != 'receipt_payment') return FALSE;

		$this->load->model('term_posts_m');
		$post_terms = $this->term_posts_m->get_the_terms($receipt->post_id);
		$term_id = reset($post_terms);
		if(empty($term_id)) return FALSE;

		$this->load->model('contract/base_contract_m');
		$this->base_contract_m->sync_all_amount($term_id);

		/* Add Job */
		$this->addJobCalcMetadata($insert_id);
	}

	protected function recacl_amount_callback($data,$status = TRUE)
	{
		if(empty($data)) return FALSE;

		if(empty($data[1])) return FALSE;

		$post_id = end($data);
		if(empty($post_id)) return FALSE;

		$update_data = reset($data);
		if(empty($update_data['post_type'])) return FALSE;


		$this->load->model('term_posts_m');
		$post_terms = $this->term_posts_m->get_the_terms($post_id);	
		$term_id = reset($post_terms);
		
		if(empty($term_id)) return FALSE;

		/* Đồng bộ dữ liệu bảo lãnh phí hợp đồng */
		if($update_data['post_type'] == 'receipt_caution')
		{
			$this->load->model('contract/base_contract_m');
			$this->base_contract_m->sync_bailment_amount($term_id);
			return TRUE;
		}

		/* Đồng bộ dữ liệu thanh toán phía hợp đồng */
		if(!in_array($update_data['post_type'], ['receipt_payment', 'receipt_payment_on_behaft'])) return FALSE;

		
		$this->load->model('contract/contract_m');
		$this->contract_m->set_contract($term_id);
		$this->contract_m->sync_all_amount();

		/* Add Job */
		$this->addJobCalcMetadata($post_id);

		return TRUE;
	}

    /**
     * active_contract
     *
     * @return array data of receipt, use for another hook
     */
    protected function active_contract($data)
    {
        // Validate data
        if(empty($data)) return $data;

        if(empty($data[1])) return $data;

        $post_id = end($data);
        if(empty($post_id)) return $data;

        $update_data = reset($data);
        if(empty($update_data['post_type'])) return $data;

        // Main process
        $receipt_type_can_active_contract = [
            'receipt_payment', 
            'receipt_caution', 
            'receipt_payment_on_behaft'
        ];
        if(!in_array($update_data['post_type'], $receipt_type_can_active_contract))
        {
            return $data;
        }

        if('paid' != $update_data['post_status']) return $data;

        $this->load->model('term_posts_m');
        $post_terms = $this->term_posts_m->get_the_terms($post_id);	
        $term_id = reset($post_terms);
        
        $this->load->model('contract_m');
        $contract = $this->contract_m->set_contract($term_id);
        if(empty($contract))
        {
            return $data;
        }

        if($contract->is_service_start($term_id))
        {
            return $data;
        }

        $is_first_paid_receipt_of_contract = $this->is_first_paid_receipt_of_contract($term_id, $receipt_type_can_active_contract);
        if(!$is_first_paid_receipt_of_contract)
        {   
            return $data;
        }

        try
        {
            $contract->start_service();
        }
        catch(Exception $e)
        {
            log_message('ERROR', 'Start contract has failed. ["message" => "'. $e->getMessage() .'"]');
        }

        return $data;
    }

	/**
	 * Adds a job calculate metadat.
	 *
	 * @param      <type>  $post_id  The post identifier
	 */
	private function addJobCalcMetadata($post_id)
	{
		/* Add Cron calculate metadata */
		$this->load->model('log_m');
		$insert_id = $this->log_m->insert([ 'log_type' => 'queue-receipt-sync-metadata', 'log_status' => 0, 'log_content' => $post_id ]);

		$this->load->config('amqps');
		$amqps_host 	= $this->config->item('host', 'amqps');
		$amqps_port 	= $this->config->item('port', 'amqps');
		$amqps_user 	= $this->config->item('user', 'amqps');
		$amqps_password = $this->config->item('password', 'amqps');
		
        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];

		$connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
		$channel 	= $connection->channel();
		$channel->queue_declare($queue, false, true, false, false);

		$payload = [
			'event' 	=> 'contract_payment.sync.metadata',
			'paymentId'	=> $post_id,
			'log_id' => $insert_id
		];

		$message = new \PhpAmqpLib\Message\AMQPMessage(
			json_encode($payload),
			array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
		);

		$channel->basic_publish($message, '', $queue);	
		$channel->close();
		$connection->close();
	}

	protected function timestamp($data)
	{
		$data['updated_on'] = time();
		return $data;
	}

	protected function init_default_data($data)
	{
		$data['post_status'] = $data['post_status'] ?? 'draft';
		$data['post_type'] = $data['post_type'] ?? 'receipt_payment';
		return $data;	
	}

	/**
	 * Calculates the gross income
	 *
	 * @param      <type>   $term_id     The term identifier
	 * @param      <type>   $start_time  The start time , default is start Of Month
	 * @param      <type>   $end_time    The end time , default is end Of Month
	 *
	 * @return     integer  The gross income.
	 */
	public function calc_gross_income($term_id,$start_time = FALSE,$end_time = FALSE)
	{
		$end_time = $end_time ?: $this->mdate->endOfMonth();
		if($start_time)
		{
			$this->where('posts.end_date >=',$start_time);
		}

		$posts = $this
		->select('posts.post_id')
		->join('term_posts','term_posts.post_id = posts.post_id')
		->get_many_by([
			'term_posts.term_id' => $term_id,
			'posts.post_status' => 'paid',
			'posts.post_type' => 'receipt_payment',
			'posts.end_date <=' => $end_time
			]);

		$ret_val = 0;

		if(empty($posts)) return $ret_val;

		foreach ($posts as $post)
		{
			$ret_val+= (int) get_post_meta_value($post->post_id,'amount');
		}

		return $ret_val;
	}


	/**
	 * Sets the post type.
	 *
	 * @return     self  set default post type query for this model
	 */
	public function set_post_type()
	{
		$this->config->load('contract/receipt');
		$post_types = $this->config->item('type', 'receipt');
		$post_types = $post_types ? array_keys($post_types) : [];

		$this->where_in('posts.post_type', $post_types);
		return $this;
	}

	/**
	 * { function_description }
	 *
	 * @param      array  $attributes  The attributes
	 *
	 * @return     self   ( description_of_the_return_value )
	 */
	public function forceFill(array $attributes)
	{
		$this->attributes = array_merge($this->attributes, $attributes);
		return $this;
	}

	/**
	 * Calculates the VAT amount
	 *
	 * @return     integer  The vat amount.
	 */
	public function calc_vat_amount()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$amount = (double) get_post_meta_value($this->post_id, 'amount');
		$vat 	= div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		return div($amount, 1+ $vat)*$vat;
	}

	/**
	 * Calculates the fct.
	 *
	 * @param      integer    $percent  The percent
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The fct.
	 */
	public function calc_fct()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		if('receipt_payment_on_behaft' == $this->post_type) return 0;
		
		$fct = div((int) get_term_meta_value($this->contract->term_id, 'fct'), 100);
		if(empty($fct)) return 0;
		return (new contract_m)->set_contract($this->contract)->get_behaviour_m()->calc_fct($fct, $this->calc_actual_budget());
	}


	/**
	 * Calculates the actual budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The actual budget.
	 */
	public function calc_actual_budget()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		$this->amount 	= (double) get_post_meta_value($this->post_id, 'amount');
		if('receipt_payment_on_behaft' == $this->post_type) return $this->amount;
		
		if($this->post_type != 'receipt_payment') return FALSE;

		/* contract_budget_payment_types */
		$contract_budget_payment_type = get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type');
		if('customer' == $contract_budget_payment_type) return 0;


		$fct 			= div((int) get_term_meta_value($this->contract->term_id, 'fct'), 100);
		$vat 			= div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		$payment_amount = div($this->amount, 1 + $vat); /* Giá trị sau khi đã trừ VAT */
		$service_fee 	= (int) get_term_meta_value($this->contract->term_id, 'service_fee');

		$service_fee_payment_type = get_term_meta_value($this->contract->term_id , 'service_fee_payment_type');
		if('devide' == $service_fee_payment_type)
		{
			$contractBudget = (new contract_m)->set_contract($this->contract)->get_behaviour_m()->calc_budget();
			$sfee_rate 		= div($service_fee, $contractBudget);
			return div($payment_amount*(1 - $fct), (1 + $sfee_rate) * (1 - $fct) + $fct);
		}

		return max([ ($payment_amount - $this->calc_service_fee()), 0 ]);
	}

	/**
	 * Calculates the service fee.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_fee()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$this->amount = (double) get_post_meta_value($this->post_id, 'amount');

		$fct = div((int) get_term_meta_value($this->contract->term_id, 'fct'), 100);
		$vat = div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);

		$payment_amount = div($this->amount, 1 + $vat); /* Giá trị sau khi đã trừ VAT */

		/* contract_budget_payment_types */
		$contract_budget_payment_type = get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type');
		if('customer' == $contract_budget_payment_type) return $payment_amount;

		/* NORMAL PAYMENT */

		$service_fee = (int) get_term_meta_value($this->contract->term_id, 'service_fee');
		$service_fee_payment_type = get_term_meta_value($this->contract->term_id , 'service_fee_payment_type');

		$contract = (new contract_m)->set_contract($this->contract);
		if('devide' == $service_fee_payment_type)
		{
			$contractBudget = $contract->get_behaviour_m()->calc_budget();
			$sfee_rate 		= div($service_fee, $contractBudget);
			$actual_budget 	= div($payment_amount*(1 - $fct), (1 + $sfee_rate) * (1 - $fct) + $fct);

			return $this->calc_actual_budget()*$sfee_rate;
		}
		
		/* Lấy ra danh sách tất cả các thanh toán trước post hiện tại */
		$receipts = $this->receipt_m
    	->select('posts.post_id,post_author,post_title,post_status,created_on,updated_on,post_type,end_date')
    	->select('term_posts.term_id')
    	->join('term_posts', "term_posts.post_id = posts.post_id AND term_posts.term_id = {$this->contract->term_id}")
    	->where('end_date <=', $this->end_date)
    	->where('post_status', 'paid')
    	->where('post_type', 'receipt_payment')
    	->set_post_type()
    	->as_array()
    	->order_by([ 'end_date' => 'asc', 'posts.post_id' => 'asc'])
    	->get_all();

    	if( 1 == count($receipts)) 
    	{
    		return min([$payment_amount, $service_fee]);
    	}

    	$_preReceipts = array_slice($receipts, 0, (int) array_search($this->post_id, array_column($receipts, 'post_id')));
    	if(empty($_preReceipts)) return $service_fee;

    	$_prePaymentAmount 	= array_sum(array_map(function($x) use ($vat){
    		return div((double) get_post_meta_value($x['post_id'], 'amount'), 1 + $vat);
    	}, $_preReceipts));

    	$result = max([ ($service_fee - $_prePaymentAmount), 0 ]);
    	return $result > $payment_amount ? $payment_amount : $result;
	}

	/* phí giảm thỏa thuận (bao gồm VAT, FCT - nếu có)*/
	public function calc_deal_reduction_fee()
	{
		return 0;
	}

	public function  __set($name, $value) {

		$this->attributes[$name] = $value;
		$this->{$name} = $value;
		return $this;
    }

	/**
	 * __get magic
	 *
	 * Allows models to access CI's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string	$key
	 */
	public function __get($key)
	{
		if( isset($this->attributes[$key])) return $this->attributes[$key];

		// Debugging note:
		//	If you're here because you're getting an error message
		//	saying 'Undefined Property: system/core/Model.php', it's
		//	most likely a typo in your model code.
		return get_instance()->$key;
	}
	
	/**
	 * Gets the instance.
	 *
	 * @return     self  The instance.
	 */
	public function get_instance()
	{
        if( ! $this->contract) return FALSE;
        
        $is_promotion = (bool)get_post_meta_value($this->post_id, 'is_promotion');
        if($is_promotion){
            $this->load->model('contract/receipt_promotion_m');
			$_instance = (new Receipt_promotion_m())->forceFill($this->attributes);
            return $_instance;
        }

		$this->load->config('contract/contract');

		if( in_array($this->contract->term_type, [ 'onead' ])) 
		{
			$this->load->model('contract/receipt_onead_m');
			$_instance = (new Receipt_onead_m())->forceFill($this->attributes);
            return $_instance;
		}

		if( in_array($this->contract->term_type, array_keys($this->config->item('CHANNEL-SALES', 'group-services')))) 
		{
            if('gws' == $this->contract->term_type){
                $this->load->model('contract/receipt_gws_m');
                $_instance = (new Receipt_gws_m())->forceFill($this->attributes);
                return $_instance;
            }

			$this->load->model('contract/receipt_courses_m');
			$_instance = (new Receipt_courses_m())->forceFill($this->attributes);
            return $_instance;
		}

		if( in_array($this->contract->term_type, array_keys($this->config->item('ADSPLUS.VN', 'group-services')))) 
		{
			if('manual' == get_post_meta_value($this->post_id, 'input_method')) 
			{
				$this->load->model('contract/receipt_ads_manual_m');
				$_instance = (new receipt_ads_manual_m())->forceFill($this->attributes);
            	return $_instance;
			}

            $service_fee_payment_type = get_term_meta_value($this->contract->term_id, 'service_fee_payment_type');
            if('range' == $service_fee_payment_type){
                $this->load->model('contract/receipt_ads_range_m');
			    $_instance = (new Receipt_ads_range_m())->forceFill($this->attributes);
                return $_instance;
            }
			
			if('zalo-ads' == $this->contract->term_type)
			{
				$this->load->model('contract/receipt_zaloads_m');
				$_instance = (new Receipt_zaloads_m())->forceFill($this->attributes);
				return $_instance;
			}

			$this->load->model('contract/receipt_ads_m');
			$_instance = (new Receipt_ads_m())->forceFill($this->attributes);
            return $_instance;
		}

		if( in_array($this->contract->term_type, array_keys($this->config->item('WEBDOCTOR.VN', 'group-services')))) 
		{
			$this->load->model('contract/receipt_web_m');
			$_instance = (new Receipt_web_m())->forceFill($this->attributes);
            return $_instance;
		}

		return $this;
	}

	/**
     * @param string $value
     * 
     * @return [type]
     */
    public function row_id_check($value)
    {
        $isExisted = $this->postmeta_m->select('post_id')->where([ 'meta_key' => 'row_id', 'meta_value' => trim($value) ])->count_by() > 0;
		if($isExisted)
        {
            $this->form_validation->set_message('row_id_check', 'Mã phiếu đã tồn tại');    
            return false;
        }

        return true;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function row_id_exist_check($value)
    {
        if(empty(trim($value))) return true;

        $isExisted = $this->postmeta_m->select('post_id')->where([ 'meta_key' => 'row_id', 'meta_value' => trim($value) ])->count_by() > 0;
		if(!$isExisted)
        {
            $this->form_validation->set_message('row_id_exist_check', 'Mã phiếu không tồn tại');    
            return false;
        }

        return true;
    }

	/**
     * @param int $value
     * 
     * @return boolean
     */
    public function contract_id_check($value)
    {
		if(empty($value)) return true;
        $isExisted = $this->contract_m->select('term_id')->where('term_id', (int) $value)->count_by() > 0;
        if( ! $isExisted)
        {
            $this->form_validation->set_message('contract_id_check', 'ID Hợp đồng không tồn tại');
            return false;
        }
        
        return true;
    }

	/**
     * @param string $value
     * 
     * @return boolean
     */
    public function contract_code_check($value)
    {
		if(empty($value)) return true;

        $isExisted = $this->termmeta_m->where([ 'meta_key' => 'contract_code', 'meta_value' => trim($value) ])->count_by() > 0;
        if( ! $isExisted)
        {
            $this->form_validation->set_message('contract_code_check', 'Mã hợp đồng không tồn tại');
            return false;
        }

        return true;
    }

	/**
     * @param string $value
     * 
     * @return boolean
     */
    public function existed_n_owned_check($value)
    {		
		has_permission('contract.receipt.manage') OR $this->where('posts.post_author', $this->admin_m->id);
        $isExisted = $this->where('posts.post_id', (int) $value)->count_by() > 0;
        if( ! $isExisted)
        {
            $this->form_validation->set_message('existed_n_owned_check', 'Mã không tồn tại hoặc không có quyền thao tác.');
            return false;
        }
		
        return true;
    }

	public function contract()
	{
		$post_id = $this->post_id;
		if(empty($post_id)) return null;

		$contract = $this->term_posts_m->get_post_terms($post_id, $this->contract_m->getTypes());
        $contract AND $contract = array_filter(array_values($contract));
        if( ! empty($contract))
        {
            $contract = array_merge(...$contract);
            $contract = reset($contract);
        }

		$this->contract = $contract;
		return $this->contract;
	}

	/**
	 * Calculates the service provider tax.
	 * 
	 * Exp. Google Tax Fee apply after 01.11.2022
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_provider_tax()
	{
		return 0;
	}
    
    /**
     * is_first_paid_receipt_of_contract
     *
     * @param  int|string $contract_id
     * @param  array|string $receipt_type
     * @return bool
     */
    public function is_first_paid_receipt_of_contract($contract_id, $receipt_type = [])
    {
        if(empty($receipt_type))
        {
            $this->config->load('contract/receipt');
            $receipt_type = $this->config->item('type', 'receipt');
            $receipt_type = $receipt_type ? array_keys($receipt_type) : [];

            if(empty($receipt_type))
            {
                return FALSE;
            }
        }

        $this->load->model('contract_m');
        $contract_receipts = $this->join('term_posts', 'term_posts.post_id = posts.post_id')
            ->join('term', 'term.term_id = term_posts.term_id')
            ->where('term.term_id', '=', $contract_id)
            ->where('posts.post_status', '=', 'paid')
            ->group_by('term.term_id')
            ->group_by('posts.post_id')
            ->select('posts.post_id')
            ->limit(1);

        if(is_array($receipt_type))
        {
            $contract_receipts->where_in('posts.post_type', $receipt_type);
        }

        if(is_string($receipt_type))
        {
            $contract_receipts->where('posts.post_type', $receipt_type);
        }

        $num_of_paid_receipts = $contract_receipts->count_by();
        $is_first_paid_receipt_of_contract = $num_of_paid_receipts <= 1;
        return $is_first_paid_receipt_of_contract;
    }
}
/* End of file Receipt_m.php */
/* Location: ./application/modules/contract/models/Receipt_m.php */