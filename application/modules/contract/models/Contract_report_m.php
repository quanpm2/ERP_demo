<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contract_report_m extends Base_Model {

	public function __construct() 
	{
		parent::__construct();

		$models = array('term_posts_m', 'term_users_m', 'staffs/admin_m', 'contract/contract_m', 'webgeneral/webgeneral_kpi_m');
		$this->load->model($models);

		$this->load->library('email');		
		$this->load->config('contract/receipt');
		$this->load->config('contract/contract') ;
		defined('SALE_MANAGER_ID') OR define('SALE_MANAGER_ID',18);
	}

    /**
     * Sends an email successful payment.
     *
     * @param      integer  $post_id  The post identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function send_email_successful_payment($post_id = 0)
    {
        $this->load->model('contract/receipt_m');

        $receipt = $this->receipt_m
        ->select('post_id,post_title,post_status,post_type')
        ->get_by([
            'post_type' => 'receipt_payment',
            'post_status' => 'paid',
            'post_id' => $post_id
        ]);

        if(empty($receipt)) return FALSE;

        $term_ids = $this->term_posts_m->get_the_terms($post_id);
        if(empty($term_ids)) return FALSE;

        $term_id        = reset($term_ids);
        $trading_type   = get_term_meta_value($term_id, 'trading_type');
        if($trading_type == 'agency') return TRUE;

        $term    = $this->contract_m->select('term_id,term_name,term_type,term_status')->set_term_type()->get($term_id);

        $transfer_type_def  = $this->config->item('transfer_type', 'receipt');
        $transfer_type      = get_post_meta_value($post_id, 'transfer_type');

        $transfer_account_def   = $this->config->item('transfer_account', 'receipt');
        $transfer_account       = get_post_meta_value($post_id, 'transfer_account');

        $contract_begin = get_term_meta_value($term_id,'contract_begin');
        $contract_end   = get_term_meta_value($term_id,'contract_end');

        $parent_service = '';
        $group_services = $this->config->item('group-services');
        foreach ($group_services as $group_name => $services)
        {
            if(empty($services[$term->term_type])) continue;

            $parent_service = $group_name;
            break;
        }

        $contract_code  = mb_strtoupper(get_term_meta_value($term_id,'contract_code'));
        $title          = "[{$parent_service}] xác nhận thanh toán của Quý Khách cho hợp đồng {$contract_code}";        

        $data = array(
            'receipt' => $receipt,
            'term_id' => $term_id,
            'term' => $term,
            'contract_code' => $contract_code,
            'amount' => get_post_meta_value($post_id,'amount'),
            'transfer_type' => $transfer_type_def[$transfer_type] ?? '',
            'transfer_account' => $transfer_account_def[$transfer_account] ?? '',
            'contract_begin' => $contract_begin,
            'contract_end' => $contract_end,
            'contract_daterange' => my_date($contract_begin,'d/m/Y').' - '.my_date($contract_end,'d/m/Y'),
            'parent_service' => $parent_service,
            'title' => $title,
            'sale_email' => $parent_service == 'ADSPLUS.VN' ? 'sales@adsplus.vn' : 'sales@adsplus.vn',
        );

        $content    = $this->load->view('report/send_email_successful_payment',$data,TRUE);
        
        $recipients = array('mailto' => [],'mail_cc' => [],'mail_bcc' => []);
        
        $recipients['mailto'][] = get_term_meta_value($term_id,'representative_email');
        $recipients['mail_cc'][] = $this->admin_m->get_field_by_id((int) get_term_meta_value($term_id, 'staff_business'),'user_email'); 

        /* Configuration Email-lib for particular service */
        switch ($parent_service)
        {
            case 'ADSPLUS.VN':

                # Load all TECHNICAL belóng to this CONTRACT
                $this->load->model('googleads/googleads_kpi_m');
                if($kpis = $this->googleads_kpi_m->select('user_id')->group_by('user_id')->get_many_by(['term_id'=>$term_id]))
                {
                    foreach ($kpis as $kpi)
                    {
                        array_push($recipients['mail_cc'], $this->admin_m->get_field_by_id($kpi->user_id,'user_email'));
                    }
                }

                $recipients['mail_cc'][]    = 'ketoan@adsplus.vn';
                $recipients['mail_bcc'][]   = 'trind@adsplus.vn';
                $recipients['mail_bcc'][]   = 'thonh@webdoctor.vn';
                $recipients['mail_bcc'][]   = 'tanhn@adsplus.vn';

                # Send from 'adsplus'
                $this->email->from('ketoan@adsplus.vn', 'ADSPLUS.VN');

                break;
            
            default:

                # Load all TECHNICAL belóng to this CONTRACT
                if($kpis = $this->webgeneral_kpi_m->select('user_id')->group_by('user_id')->get_many_by(['term_id'=>$term_id]))
                {
                    foreach ($kpis as $kpi)
                    {
                        array_push($recipients['mail_cc'], $this->admin_m->get_field_by_id($kpi->user_id,'user_email'));
                    }
                }

                $recipients['mail_cc'][]    = 'ketoan@webdoctor.vn';
                $recipients['mail_bcc'][]   = 'thonh@webdoctor.vn';
                $recipients['mail_bcc'][]   = 'tanhn@adsplus.vn';

                # Send from 'webdoctor'
                $this->email->from('ketoan@webdoctor.vn', 'ADSPLUS.VN');

                break;
        }

        $result = $this->email->subject($title)->message($content)->to($recipients['mailto'])->cc($recipients['mail_cc'])->bcc($recipients['mail_bcc'])->send();

        /* LOG FUNCTION PROCESS */
        $insert_id = $this->log_m->insert(array(
            'term_id'       => $term_id,
            'log_title'     => $title,
            'log_status'    => (int) $result,
            'log_type'      => 'send_email_successful_payment',
            'log_time_create'   => my_date('Y-m-d H:i:s')
        ));

        # Log content for report & review purpose
        $this->log_m->meta->update_meta($insert_id, 'email_content', $content); # Email contents
        $this->log_m->meta->update_meta($insert_id, 'recipients', serialize($recipients)); # Email recipients
        $this->log_m->meta->update_meta($insert_id, 'receipt_payment_id', $post_id); # receipt payment id

        return $result;
    }
    
    /**
     * Sends an activation SMS 2 customer.
     *
     * @param      integer  $term_id  The term identifier
     */
    public function send_sms_activation_2customer($term_id = 0, $fake_send = TRUE)
    {
        $term = $this->contract_m->select('term_id,term_type')->set_term_type()->get($term_id);
        if( ! $term) return FALSE;


        $customer = $this->term_users_m->get_the_users($term->term_id, ['customer_person','customer_company']);
        if(empty($customer)) return FALSE;

        $customer       = reset($customer);
        $customer_phone = trim(preg_replace('/\s+/', '', get_user_meta_value($customer->user_id, 'customer_phone')));
        if(empty($customer_phone)) return FALSE;

        $group_services = $this->config->item('group-services');
        $group_services_flip = array();
        foreach ($group_services as $brand => $services)
        {
            $group_services_flip = array_merge(array_map(function($x) use ($brand) { $x = $brand; return $x;},$services),$group_services_flip);
        }

        $brand      = $group_services_flip[$term->term_type] ?? '' ;
        $service_f  = ucwords(convert_utf82ascci($group_services[$brand][$term->term_type] ?? ''));
        $message    = "{$brand} da kich hoat hop dong {$service_f}. Cam on Quy Khach da tin tuong chung toi";
        

        $sms_config = array();
        if($brand == 'WEBDOCTOR.VN')
        {
            $sms_config['service_id']   = 'Webdoctor';
            $sms_config['command_code'] = 'Webdoctor';
        }

        if($fake_send == FALSE && ENVIRONMENT !== 'production')
        {
            $fake_send = TRUE;
        }

        $this->load->library('sms', $sms_config);
        $result     = ($fake_send === TRUE) ? $this->sms->fake_send($customer_phone,$message,0) : $this->sms->send($customer_phone,$message);
        if( ! empty($result))
        {
            $result = array_shift($result);
            unset($result['config']);
        }

        $this->load->model('log_m');
        /* LOG FUNCTION PROCESS */
        $insert_id = $this->log_m->insert(array(
            'term_id'           => $term_id,
            'log_type'          => 'send_sms_activation_2customer',
            'log_title'         => $message,
            'log_status'        => (int) (!empty($result)),
            'log_content'       => serialize($result),
            'log_time_create'   => my_date('Y-m-d H:i:s')
        ));
        
        return TRUE;
    }

    /**
     * Sends a sms successful payment.s
     *
     * @param      integer  $post_id  The post identifier
     */
    public function send_sms_successful_payment($post_id = 0, $fake_send = FALSE)
    {
        $this->load->model('contract/receipt_m');

        $receipt    = $this->receipt_m->select('post_id,post_title,post_status,post_type')->get_by(['post_type'=>'receipt_payment','post_status'=>'paid','post_id'=>$post_id]);

        if( ! $receipt) return FALSE;

        $term_ids   = $this->term_posts_m->get_the_terms($post_id);
        if(empty($term_ids)) return FALSE;

        $term_id    = reset($term_ids);
        $term       = $this->contract_m->select('term_id,term_type')->set_term_type()->get($term_id);
        if( ! $term) return FALSE;

        $customer = $this->term_users_m->get_the_users($term->term_id, ['customer_person','customer_company']);
        if(empty($customer)) return FALSE;

        $customer       = reset($customer);
        $customer_phone = trim(preg_replace('/\s+/', '', get_user_meta_value($customer->user_id, 'customer_phone')));
        if(empty($customer_phone)) return FALSE;

        $amount         = get_post_meta_value($post_id, 'amount');
        $amount_f       = currency_numberformat($amount, 'd');

        if(empty($amount_f)) return FALSE;

        $group_services = $this->config->item('group-services');
        $group_services_flip = array();
        foreach ($group_services as $brand => $services)
        {
            $group_services_flip = array_merge(array_map(function($x) use ($brand) { $x = $brand; return $x;},$services),$group_services_flip);
        }

        $brand = $group_services_flip[$term->term_type] ?? '' ;

        $service_f = ucwords(convert_utf82ascci($group_services[$brand][$term->term_type] ?? ''));

        $message    = "{$brand} da nhan {$amount_f} cua Quy khach thanh toan hop dong {$service_f}. Cam on Quy Khach da tin tuong chung toi.";
        $sms_config = array();
        if($brand == 'WEBDOCTOR.VN')
        {
            $sms_config['service_id']   = 'Webdoctor';
            $sms_config['command_code'] = 'Webdoctor';
        }

        $this->load->library('sms', $sms_config);
        $result     = ($fake_send === TRUE) ? $this->sms->fake_send($customer_phone,$message,0) : $this->sms->send($customer_phone,$message);
        if( ! empty($result))
        {
            $result = array_shift($result);
            unset($result['config']);
        }

        /* LOG FUNCTION PROCESS */
        $insert_id = $this->log_m->insert(array(
            'term_id'           => $term_id,
            'log_type'          => 'send_sms_successful_payment',
            'log_title'         => $message,
            'log_status'        => (int) (!empty($result)),
            'log_content'       => serialize($result),
            'log_time_create'   => my_date('Y-m-d H:i:s')
        ));

        # Log content for report & review purpose
        $this->log_m->meta->update_meta($insert_id, 'receipt_payment_id', $post_id); # receipt payment id
        
        return TRUE;
    }

    /**
     * Sends a mail contract composed 2 admin.
     *
     * @param      <type>  $term_id  The term identifier
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function send_mail_contract_composed_2admin($term_id)
    {
    	$data = array() ;

    	$term = $this->contract_m->set_term_type()->get_by(['term_id'=>$term_id]);
    	if(empty($term)) return FALSE;

    	$access_link = anchor(module_url("edit/{$term_id}"));

    	$customers = $this->term_users_m->get_the_users($term_id,'customer_person,customer_company');
    	$customer = $customers ? reset($customers) : NULL;

    	$contract_code = get_term_meta_value($term_id,'contract_code');

    	$title 		= "[ĐỢI CẤP SỐ] Hợp đồng website {$term->term_name} đã soạn hoàn tất và đợi cấp số";
    	$content 	= $title;

		$this->config->load('table_mail');
		$this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Thông tin hợp đồng chưa cấp số');
        $this->table
        ->add_row('Link xem nhanh:', $access_link)
        ->add_row('Dịch vụ:', $this->config->item($term->term_type,'services'))
        ->add_row('Ngày tạo:', my_date(get_term_meta_value($term->term_id,'created_on'),'d/m/Y'))
        ->add_row('Người tạo:', $this->admin_m->get_field_by_id(get_term_meta_value($term->term_id,'created_by'),'display_name'));

        $staff_business = get_term_meta_value($term->term_id,'staff_business');
        if(!empty($staff_business))
        {
            $display_name = $this->admin_m->get_field_by_id($staff_business,'display_name');
            $display_name = empty($display_name) ? $this->admin_m->get_field_by_id($staff_business,'display_name') : $display_name;
            $this->table->add_row('Kinh doanh phụ trách:', "#{$staff_business} - {$display_name}");
        }

        
        $this->table->add_row('Ghi chú (nguồn khách hàng) :',get_term_meta_value($term->term_id,'contract_note'));


        $contract_value = (int) get_term_meta_value($term->term_id,'contract_value');
        $this->table->add_row('Giá trị HĐ: ', currency_numberformat($contract_value));

        $vat = (int)get_term_meta_value($term->term_id,'vat');
        $this->table->add_row('VAT: ', currency_numberformat($vat,'%'));
        $this->table->add_row('Tổng cộng: ', currency_numberformat(($contract_value+($contract_value*div($vat,100)))));

        $content.= $this->table->generate();

        $this->table->set_caption('Thông tin khách hàng');
        $content.=
        $this->table
        ->add_row('Người đại diện', get_term_meta_value($term->term_id,'representative_name'))
        ->add_row('Địa chỉ', get_term_meta_value($term->term_id,'representative_address'))
        ->add_row('Website', $term->term_name)
        ->add_row('Mail liên hệ', mailto(get_term_meta_value($term->term_id,'representative_email')))
        ->add_row('Số điện thoại', get_user_meta_value($customer->user_id ?? 0,'customer_phone'))
        ->generate();
        $recipients = array(
            'mail_to' => array(),
            'mail_cc' => array()
        );

        if($sale_mail = $this->admin_m->get_field_by_id($staff_business,'user_email'))
        {
        	$recipients['mail_to'][] = $sale_mail;
        }

        if($created_by = $this->admin_m->get_field_by_id(get_term_meta_value($term->term_id,'created_by'),'user_email'))
        {
        	$recipients['mail_to'][] = $created_by;
        }

		$this->set_mail_to($term_id,$recipients);
		$this->email->subject($title.' SSID:'.time());
		$this->email->message($content);

		$this->email->to($recipients['mail_to']);
		$this->email->cc($recipients['mail_cc']);

		$send_status = $this->email->send();
		return $send_status;
    }

    
    /**
     * Sends a mail alert receipt deadline.
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function send_mail_alert_receipt_deadline($term_id = 0)
    {
        $data = array() ;

        $term = $this->contract_m
        ->set_term_type()
        ->select('term.*')
        ->select('posts.end_date')
        ->select('SUM(postmeta.meta_value)*COUNT(DISTINCT posts.post_id)/COUNT(postmeta.post_id) AS "amount"')
        ->join('term_posts','term.term_id = term_posts.term_id')
        ->join('posts','posts.post_id = term_posts.post_id')
        ->join('postmeta', 'postmeta.post_id = posts.post_id AND postmeta.meta_key = "amount"','LEFT')
        ->where('posts.post_type','receipt_caution')
        ->where('posts.post_status','publish')
        ->where('posts.end_date <=',$this->mdate->startOfDay())
        ->where_in('term.term_status',['waitingforapprove','pending','publish'])
        ->where('term.term_id',$term_id)
        ->group_by('term.term_id')
        ->get_by();

        if(empty($term)) return FALSE;

        $access_link = anchor(module_url("edit/{$term_id}"));

        $customers = $this->term_users_m->get_the_users($term_id,'customer_person,customer_company');
        $customer = $customers ? reset($customers) : NULL;

        $contract_code = get_term_meta_value($term_id,'contract_code');

        $title = "[CẢNH BÁO][BẢO LÃNH] Hợp đồng {$contract_code} đã đến hạn bảo lãnh";

        $content = $title;

        $this->config->load('table_mail');
        $this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Thông tin hợp đồng bảo lãnh');
        $this->table
        ->add_row('Mã hợp đồng:', $contract_code)
        ->add_row('Giá trị bảo lãnh:', $term->amount)
        ->add_row('Ngày hết hạn bảo lãnh:',my_date($term->end_date,'d/m/Y'))

        ->add_row('Dịch vụ:', $this->config->item($term->term_type,'services'))
        ->add_row('Người tạo:', $this->admin_m->get_field_by_id(get_term_meta_value($term->term_id,'created_by'),'display_name'))
        ->add_row('Người duyệt:', $this->admin_m->get_field_by_id(get_term_meta_value($term->term_id,'verfied_by'),'display_name'));

        $staff_business = get_term_meta_value($term->term_id,'staff_business');
        if(!empty($staff_business))
        {
            $display_name = $this->admin_m->get_field_by_id($staff_business,'display_name');
            $display_name = empty($display_name) ? $this->admin_m->get_field_by_id($staff_business,'display_name') : $display_name;
            $this->table->add_row('Kinh doanh phụ trách:', "#{$staff_business} - {$display_name}");
        }

        $contract_value = (int) get_term_meta_value($term->term_id,'contract_value');
        $this->table->add_row('Giá trị HĐ: ', currency_numberformat($contract_value));

        $vat = (int)get_term_meta_value($term->term_id,'vat');
        $this->table->add_row('VAT: ', currency_numberformat($vat,'%'));
        $this->table->add_row('Tổng cộng: ', currency_numberformat(($contract_value+$contract_value*div($vat,100))));

        $content.= $this->table->generate();

        $this->table->set_caption('Thông tin khách hàng');
        $content.=
        $this->table
        ->add_row('Người đại diện', get_term_meta_value($term->term_id,'representative_name'))
        ->add_row('Địa chỉ', get_term_meta_value($term->term_id,'representative_address'))
        ->add_row('Website', $term->term_name)
        ->add_row('Mail liên hệ', mailto(get_term_meta_value($term->term_id,'representative_email')))
        ->add_row('Số điện thoại', get_user_meta_value($customer->user_id ?? 0,'customer_phone'))
        ->generate();
        $recipients = array(
            'mail_to' => array(),
            'mail_cc' => array('ketoan@adsplus.vn')
        );

        if($sale_mail = $this->admin_m->get_field_by_id($staff_business,'user_email'))
        {
            $recipients['mail_to'][] = $sale_mail;
        }

        if($created_by = $this->admin_m->get_field_by_id(get_term_meta_value($term->term_id,'created_by'),'user_email'))
        {
            $recipients['mail_to'][] = $created_by;
        }

        $this->set_mail_to($term_id,$recipients);
        $this->email->subject("{$title} SSID:".time());
        $this->email->message($content);

        $this->email->to($recipients['mail_to']);
        $this->email->cc($recipients['mail_cc']);
        $this->email->bcc('thonh@webdoctor.vn');
        $send_status = $this->email->send();

        return $send_status;
    }

    /**
     * Sends a mail report collectmoney.
     *
     * @param      <type>  $start_time  The start time
     * @param      <type>  $end_time    The end time
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function send_mail_report_collectmoney($start_time, $end_time)
    {
        $start_time = $this->mdate->startOfDay($start_time);
        $end_time = $this->mdate->endOfDay($end_time);

        $this->load->model('staffs/department_m');
        $this->load->model('contract/receipt_m');
        $posts =  $this->receipt_m
        ->select('posts.*,term_posts.*')
        ->join('term_posts','posts.post_id = term_posts.post_id')
        ->where('posts.post_status','paid')
        ->where('posts.end_date >=', $start_time)
        ->where('posts.end_date <=', $end_time)
        ->get_many_by('posts.post_type',['receipt_payment']);
        
        if(empty($posts)) return FALSE;
       
        $contract = $this->contract_m->set_term_type()->where_in('term_id',array_column($posts,'term_id'))->get_many_by();

        $contract_term_key = array_column($contract,NULL,'term_id');// đưa term_id ra làm key của mảng

        $data =[];
        $sum_money_before_VAT = 0;
        $sum_money_after_VAT = 0;

        foreach ($posts as $value) 
        {
            $term_id = $value->term_id;
            //so hop dong
            $number_contract = get_term_meta_value($term_id,'contract_code');
            //so tien
            $money  = get_post_meta_value($value->post_id,'amount');

            if(empty($money)) continue;

            $sum_money_after_VAT += $money;
            //VAT
            $vat = get_term_meta_value($term_id,'vat');


            $money_after_vat = $money;
            if((int)$vat!=0){
                // $money_after_vat = $money - $money*(div($vat,100));
                $money_after_vat = div($money, (1 + div($vat,100)));
            }

            $sum_money_before_VAT += $money_after_vat; 
            //dich vu
            $contract_type = $this->config->item($contract_term_key[$term_id]->term_type, 'taxonomy');
            //nhan vien
            $staff_business_display_name = '';
            $staff_business_id = get_term_meta_value($term_id,'staff_business');

            $staff_business_display_name = $this->admin_m->get_field_by_id($staff_business_id, 'display_name');
            //nhan vien group

            //nhan vien phong

            //user_group
            $user_group_display_name = '';
            $department_display_name = 'Khác';
            $user_group = $this->term_users_m->get_user_terms($staff_business_id, 'user_group');
            
            if(!empty($user_group)) 
            {
                $user_group_array = array_values($user_group)[0];
                //user_group
                $user_group_display_name = $user_group_array->term_name;
                //Phong
                // $department_display_name = $this->department_m->set_term_type()->get($user_group_array->term_parent)->term_name;
                
                if($department = $this->department_m->set_term_type()->get($user_group_array->term_parent))
                {
                    $department_display_name = $department->term_name;
                }
            }

            // //khach hang
            // $customer = $this->term_users_m->get_the_users($contract_term_key[$term_id]->term_id, array('customer_person','customer_company'));
            // if(!empty($customer)) $customer = array_values($customer)[0]->display_name;
             
            
            $data [] = array(
                'number_contract' => $number_contract,
                'money' => $money,
                'money_after_vat' =>$money_after_vat,
                'contract_type' => $contract_type,
                'staff_business_display_name' => $staff_business_display_name,
                'user_group_display_name' => $user_group_display_name,
                'department_display_name' => $department_display_name,
                // 'customer' => $customer,
                'vat'   => $vat
            );
        }
        $_content = '';
        $_content1 = '';
        $content = ' Thông báo thu tiền ngày '.my_date($end_time,'d/m/Y');
        //Table_group_department
       
            $group_department =  array_group_by($data,'department_display_name');
            $sum_money_group = array_map(function($x){return array_sum(array_column($x,'money'));},$group_department);

            $this->config->load('table_mail');
            $this->table->set_template($this->config->item('mail_template'));
            $this->table->add_row(my_date($end_time,'d/m/Y'),'TỔNG CỘNG', array('data'=>currency_numberformat(array_sum($sum_money_group),''),'style'=>'color:red;font-weight: bold;'));
            $stt_group = 1;
            $_key = '';
            $_value = 0;
            foreach ($sum_money_group as $key => $value) {

                if($key=="Khác"){
                    $_key = $key;
                    $_value = $value;
                    continue;
                }
                $this->table->add_row($stt_group,array('data'=>$key,'style'=>'font-weight: bold;'),array('data'=>currency_numberformat($value,''),'style'=>'font-weight: bold;'));
                $stt_group ++;
              
            }

            $this->table->add_row($stt_group,array('data'=>$_key,'style'=>'font-weight: bold;'),array('data'=>currency_numberformat($_value,''),'style'=>'font-weight: bold;'));
            
            $_content .= $this->table->generate();
        //end Table_group_department

        //Table_group_contractype
            $group_contractype =  array_group_by($data,'contract_type');
            
            $sum_money_group = array_map(function($x){return array_sum(array_column($x,'money'));},$group_contractype);

            $this->table->add_row(my_date($end_time,'d/m/Y'),'TỔNG CỘNG', array('data'=>currency_numberformat(array_sum($sum_money_group),''),'style'=>'color:red;font-weight: bold;'));
            $stt_group = 1;
            $_key = '';
            $_value = 0;
            foreach ($sum_money_group as $key => $value) {

                $this->table->add_row($stt_group,array('data'=>$key,'style'=>'font-weight: bold;'),array('data'=>currency_numberformat($value,''),'style'=>'font-weight: bold;'));
                $stt_group ++;  
              
            }
            $_content1 .= $this->table->generate();
        //END Table_group_contractype
        //---------------------------
        $this->table->clear();
        $this->table->set_template(NULL);

        $table = new CI_Table();

        $table->add_row($_content,array('data'=>'','style'=>'width:10px'),$_content1);
        $content    .= $table->generate();
        //------------------
        $content    .= '<br>';

        $this->table->set_template($this->config->item('mail_template'));
        $this->table->add_row(array('data'=>'THÔNG BÁO THU TIỀN THÁNG '.my_date($end_time,'m/Y').'' ,'colspan'=>3,'style'=>'text-align: center;font-weight: bold;'),array('data'=>currency_numberformat($sum_money_after_VAT,''),'style'=>'color:red;font-weight: bold;'),array('data'=>'','colspan'=>2),array('data'=>currency_numberformat($sum_money_before_VAT,''),'style'=>'color:red;font-weight: bold;'),'');
        $this->table->add_row(array('data'=>'Ngày','style'=>'font-weight: bold;'),
                                // array('data'=>'Tên công ty','style'=>'font-weight: bold;'),
                                array('data'=>'Nội dung','style'=>'font-weight: bold;'),
                                array('data'=>'Số tiền','style'=>'font-weight: bold;'),
                                array('data'=>'Nhân viên kinh doanh','style'=>'font-weight: bold;'),
                                array('data'=>'Nhóm','style'=>'font-weight: bold;'),
                                array('data'=>'Phòng','style'=>'font-weight: bold;'),
                                array('data'=>'VAT','style'=>'font-weight: bold;'),
                                array('data'=>'Doanh thu','style'=>'font-weight: bold;'));
                               //array('data'=>'KH','style'=>'font-weight: bold;'),
                                
        //text-align: right;
        $stt = 1;
        foreach ($data as $item) {
            $this->table->add_row(my_date($end_time,'d/m/Y'),
                $item['number_contract'],array('data'=>currency_numberformat($item['money'],''),'style'=>'text-align: right;'),
                $item['staff_business_display_name'],
                $item['user_group_display_name'],
                $item['department_display_name'],
                $item['vat'].'%',array('data'=>currency_numberformat($item['money_after_vat'],''),'style'=>'text-align: right;')
                //$item['contract_type'] 
            );
          $stt ++;
        }
        $this->table->add_row('',array('data'=>'Tổng cộng' ,'colspan'=>2,'style'=>'text-align: center;font-weight: bold;'),array('data'=>currency_numberformat($sum_money_after_VAT,''),'style'=>'color:red;font-weight: bold;'),array('data'=>'','colspan'=>2),array('data'=>currency_numberformat($sum_money_before_VAT,''),'style'=>'color:red;font-weight: bold;'),'');

        $content    .= $this->table->generate();

        $this->email->from('support@adsplus.vn', 'adsplus.vn');
        $this->email->subject('THÔNG BÁO THU TIỀN NGÀY '.my_date($end_time,'d/m/Y'));
        $this->email->message($content);
        $this->email->to('bod@adsplus.vn');
        $this->email->bcc('thonh@webdoctor.vn');

        $send_status = $this->email->send();

        return $send_status;
    }

    /**
     * Sets the mail to.
     *
     * @param      integer  $term_id  The term identifier
     * @param      string   $type_to  The type to [admin|customer|mailreport|specified]
     *
     * @return     self     ( description_of_the_return_value )
     */
    public function set_mail_to($term_id = 0,$type_to = 'admin')
    {
        $this->email->clear(TRUE);

        $recipients = array(
            'mail_to' => array(),
            'mail_cc' => array(),
            'mail_bcc' => array('thonh@webdoctor.vn')
        );

        if(is_array($type_to) && !empty($type_to)) 
        {
            $recipients = array_merge_recursive($type_to,$recipients);
            $type_to = $type_to['type_to'] ?? 'specified';
        }

        extract($recipients);

        $this->email->from('support@adsplus.vn', 'Adsplus.vn');

        // Nếu email được cấu hình phải gửi cho một nhóm người được truyền vào
        if($type_to == 'specified')
        {
            $this->recipients = $mail_to;
            $this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
            return $this;
        }

        // Nếu email được cấu hình gửi cho nhóm quản lý
        if($type_to == 'mailreport')
        {
            $mail_to = array('thonh@webdoctor.vn');
            $this->recipients = $mail_to;
            $this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
            return $this;
        }

        // Nếu KD nghỉ, người nhận là người quản lý
        $sale_id = get_term_meta_value($term_id,'staff_business');
        $sale    = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID); 
        
        // Lấy thông tin kỹ thuật phụ trách
        $tech_kpis = $this->webgeneral_kpi_m->select('user_id')->group_by('user_id')->get_many_by(['term_id'=>$term_id]);
                                     
        // Nếu gửi mail cho 'admin'
        if($type_to == 'admin')
        {
            if(!empty($tech_kpis))
            {
                foreach ($tech_kpis as $i)
                {
                    $mail = $this->admin_m->get_field_by_id($i->user_id,'user_email');
                    if(empty($mail)) continue;

                    $mail_to[] = $mail;
                }
            }

            if($sale)
            {
                $mail_cc[] = $sale->user_email;
            }

            $this->recipients = $mail_to;
            $this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
            return $this;
        }

        // Nếu gửi mail cho khách hàng
        if($representative_email = get_term_meta_value($term_id,'representative_email'))
        {
            $mail_to[] = $representative_email;
        }

        // Email người nhận báo cáo được cấu hình theo dịch vụ
        if($mail_report = get_term_meta_value($term_id,'mail_report'))
        {
            $mails = explode(',',trim($mail_report));
            if(!empty($mails))
            {
                foreach ($mails as $mail) 
                {
                    $mail_to[] = $mail;
                }
            }
        }

        if(!empty($tech_kpis))
        {
            foreach ($tech_kpis as $i)
            {
                $mail = $this->admin_m->get_field_by_id($i->user_id,'user_email');
                if(empty($mail)) continue;

                $mail_cc[] = $mail;
            }
        }

        if($sale)
        {
            $mail_cc[] = $sale->user_email;
        }

        $this->recipients = $mail_to;

        $this->email->from('support@adsplus.vn', 'Adsplus.vn')->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);

        return $this;
    }
}
/* End of file Contract_report_m.php */
/* Location: ./application/modules/contract/models/Contract_report_m.php */