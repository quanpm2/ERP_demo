<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content_m extends Post_m
{
	public $post_type = 'contract_content';

	public function set_type()
	{
		$this->where('posts.post_type', $this->post_type);
		return $this;
	}
}
/* End of file Receipt_m.php */
/* Location: ./application/modules/contract/models/Receipt_m.php */