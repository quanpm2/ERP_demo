<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Receipt_m.php');

class Receipt_web_m extends Receipt_m
{
	/**
	 * Calculates the VAT amount
	 *
	 * @return     integer  The vat amount.
	 */
	public function calc_vat_amount()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		$amount = (double) get_post_meta_value($this->post_id, 'amount');
		$vat 	= div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		return div($amount, 1+ $vat)*$vat;
	}

	public function calc_fct()
	{
		return FALSE;
	}

	public function calc_actual_budget()
	{
		return FALSE;
	}

	/**
	 * Calculates the service fee.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_fee()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		$this->amount = (double) get_post_meta_value($this->post_id, 'amount');
		if( in_array($this->contract->term_type, [ 'webgeneral', 'webdesign', 'oneweb', 'weboptimize', 'banner', 'webcontent' ])) return $this->amount;

		/* if( in_array($this->contract->term_type, [ 'hosting', 'domain' ])) */
		$vat 			= div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		$payment_amount = div($this->amount, 1 + $vat); /* Giá trị sau khi đã trừ VAT */
		return $payment_amount;
	}
}
/* End of file Receipt_m.php */
/* Location: ./application/modules/contract/models/Receipt_m.php */