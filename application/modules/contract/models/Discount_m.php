<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Discount_m extends Base_Model {

	public $data = array();
	protected $contract = NULL;

	function __construct() 
	{
		parent::__construct();
		$this->load->config('contract/discount');
	}

	final public function get_instance()
	{
		if( ! $this->contract) return FALSE;

		$allowed_models = $this->config->item('models', 'discount');
		$model = $allowed_models[$this->contract->term_type] ?? '';
		if( ! $model) return $this;

		$this->load->model($model, 'instance');
		$this->instance->set_contract($this->contract);
		return $this->instance;
	}

	/**
	 * Sets the contract.
	 *
	 * @param      Contract_m  $contract  The contract
	 *
	 * @return     self    ( description_of_the_return_value )
	 */
	public function set_contract($contract = NULL)
	{
		if(is_numeric($contract))
		{
			$this->load->model('contract/contract_m');
			$contract = $this->contract_m->set_term_type()->get($contract);
		}

		if( ! $contract) return FALSE;

		$this->contract = $contract;
		return $this;
	}

	/**
	 * Gets the packages.
	 *
	 * @return    Mixed  The packages.
	 */
	public function get_packages() { return FALSE; }


	/**
	 * Validate all amount discount before update
	 *
	 * @param      string     $discount_amount  The discount amount
	 * @param      string     $amount           The amount
	 * @param      string     $package          The package
	 *
	 * @throws     Exception Errors messages
	 *
	 * @return     BOOLEAN the result
	 */
	public function validate($discount_amount = '', $amount = '', $package = '')
	{ 
		throw new Exception("Hiện không có chương trình giảm giá nào khả dụng.");
		return FALSE; 
	}


	/**
	 * Check discount package is valid
	 *
	 * @param      string  $package  The package
	 *
	 * @return     boolean the Result
	 */
	public function check_package($package = '')
	{
		return FALSE;
	}
}
/* End of file Discount_m.php */
/* Location: ./application/modules/contract/models/Discount_m.php */