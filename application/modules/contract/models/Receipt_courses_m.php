<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Receipt_m.php');

class Receipt_courses_m extends Receipt_m
{
	/**
	 * Calculates the VAT amount
	 *
	 * @return     integer  The vat amount.
	 */
	public function calc_vat_amount()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$amount = (double) get_post_meta_value($this->post_id, 'amount');
		$vat 	= div((int) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);

		return div($amount, 1+ $vat)*$vat;
	}

	/**
	 * Calculates the fct.
	 *
	 * @param      integer    $percent  The percent
	 *c
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The fct.
	 */
	public function calc_fct()
	{
		return 0;
	}


	/**
	 * Calculates the actual budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The actual budget.
	 */
	public function calc_actual_budget()
	{
		return 0;
	}

	/**
	 * Calculates the service fee.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_fee()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');

		if('receipt_payment_on_behaft' == $this->post_type) return 0;

		$this->amount = (double) get_post_meta_value($this->post_id, 'amount');

        $vat = div((int) get_post_meta_value($this->post_id, 'vat'), 100) ?: div((int) get_term_meta_value($this->contract->term_id, 'vat'), 100);
		$payment_amount = div($this->amount, 1 + $vat); /* Giá trị sau khi đã trừ VAT */
		return $payment_amount;
	}

	/* phí giảm thỏa thuận (bao gồm VAT, FCT - nếu có)*/
	public function calc_deal_reduction_fee()
	{
		return 0;
	}
}
/* End of file receipt_courses_m.php */
/* Location: ./application/modules/contract/models/receipt_courses_m.php */