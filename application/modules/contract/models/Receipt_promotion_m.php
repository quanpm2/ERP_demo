<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Receipt_m.php');

class Receipt_promotion_m extends Receipt_m
{
	/**
	 * Calculates the VAT amount
	 *
	 * @return     integer  The vat amount.
	 */
	public function calc_vat_amount()
	{
		return 0;
	}

	/**
	 * Calculates the fct.
	 *
	 * @param      integer    $percent  The percent
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The fct.
	 */
	public function calc_fct()
	{
		return 0;
	}


	/**
	 * Calculates the actual budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The actual budget.
	 */
	public function calc_actual_budget()
	{
        $actual_budget = (int) get_post_meta_value($this->post_id, 'actual_budget');

		return $actual_budget;
	}

	/**
	 * Calculates the service fee.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The service fee.
	 */
	public function calc_service_fee()
	{
		return 0;
	}

	/* phí giảm thỏa thuận (bao gồm VAT, FCT - nếu có)*/
	public function calc_deal_reduction_fee()
	{
		return 0;
	}
}
/* End of file receipt_courses_m.php */
/* Location: ./application/modules/contract/models/receipt_courses_m.php */