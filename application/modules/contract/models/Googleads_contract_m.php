<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Googleads_contract_m extends Contract_m {

	function __construct()
	{
		parent::__construct();
		$this->load->model('history_m');
	}

	public function wizard_service_step($term_id = 0,$post = array())
	{
		if(empty($post)) return FALSE;

		/*FIELDS*/
		if(empty($post['edit']['meta'])) return FALSE;

		if( ! empty($post['edit']['meta']['googleads-begin_time']))
		{
			$post['edit']['meta']['googleads-begin_time'] = strtotime($post['edit']['meta']['googleads-begin_time']);
		}

		if( ! empty($post['edit']['meta']['googleads-end_time']))
		{
			$post['edit']['meta']['googleads-end_time'] = strtotime($post['edit']['meta']['googleads-end_time']);
		}

		parent::wizard_service_step($term_id,$post);
	}
}
/* End of file Contract_m.php */
/* Location: ./application/modules/contract/models/Googleads_contract_m.php */