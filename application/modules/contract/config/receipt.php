<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['receipt'] = array(
	
	'transfer_type' => ['cash' => 'Tiền mặt', 'account' => 'Tài khoản', 'none' => 'None'],

	'transfer_account' => [
		'none' 		=> 'None',
		'mb-bank'	=> 'MBBANK-NGUYEN THI THANH HUYEN',
		'acb'	=> 'ACB-NGUYEN DAC TRI',
		'bidv-mainn' => 'BIDV-NGUYEN NHU MAI',
		'vcb-tuanna' => 'VCB-NGUYEN ANH TUAN',
		'vcb-tuanna' => 'VCB-NGUYEN ANH TUAN',
		'vcb-tuanna' => 'VCB-NGUYEN ANH TUAN',
		'vcb1'		=> 'Vietcombank-Thuy',
		'vib-2'		=> 'VIB-Suong',
		'tech-1'	=> 'Techombank-Thuy',
		'tech-2'	=> 'Techcombank-Cty',
		'tpb-1'		=> 'TPBank-Cty'
	],

	'status' => [
		'receipt_payment' => ['draft' => 'Đang soạn','paid' => 'Đã thanh toán'],
		'promotion' => ['draft' => 'Đang soạn','paid' => 'hoàn tất'],
		'receipt_caution' => ['draft' => 'Đang soạn','publish' => 'Đang được bảo lãnh','paid' => 'Hoàn tất bảo lãnh'],
		'receipt_payment_on_behaft' => ['draft' => 'Đang soạn','paid' => 'Đã thanh toán'],
		'all' => ['paid' => 'Hoàn tất','publish' => 'Chưa hoàn tất']
	],

	'type' => [
		'receipt_payment' 			=> 'Thanh toán',
		'receipt_caution' 			=> 'Bảo lãnh',
		'receipt_payment_on_behaft' => 'Nhận thay mặt thanh toán',
		'promotion'                 => 'Ngân sách khuyến mãi'
	],

    'formula_input' => [
        'service_fee' => 'Phí theo từng đợt',
        'budget' => 'Phí theo ngân sách',
        'none' => 'None',
    ],

	'originalPaymentTypes' => [ 'receipt_payment', 'receipt_caution' ]
);