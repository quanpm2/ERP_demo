<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['discount'] = array(

	/* CONTRACT'S MODEL CAN APPLIES DISCOUNT */
	'models' => array(
		'webdesign' => 'webbuild/webbuild_discount_m',
		'oneweb' 	=> 'oneweb/oneweb_discount_m'
	),
	
	'packages' => array(
		'normal' => 'Chương trình bình thường',
		'cross-sell' => 'Chương trình Cross-sell',
	)
);