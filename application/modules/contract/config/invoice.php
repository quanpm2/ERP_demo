<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$invoice = array();
$invoice['status'] = array(
	'publish'=>'Mới tạo',
	'paid'=>'Đã thanh toán',
	'unpaid'=>'Không thanh toán',
	'send' =>'Đang chuyển'
	);

$invoice['item_status'] = array(
	'publish'=>'Mới tạo',
	'paid'=>'Đã thanh toán',
	'unpaid'=>'Không thanh toán',
	);

$invoice['tax'] = 10; //thuế
$invoice['commission'] = 5; // hoa hồng
$config['invoice'] = $invoice;

unset($invoice);