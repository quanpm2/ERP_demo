<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['datasource'] = array(
	'columns' => array(

		'id' => array(
			'name' => 'id',
			'label' => '#ID',
			'set_select' => FALSE,
			'title' => '#ID',
			'type' => 'string',
		),

		'website' => array(
			'name' => 'website',
			'label' => 'Website',
			'set_select' => FALSE,
			'title' => 'Website',
			'type' => 'string',
		),

		'status' => array(
			'name' => 'status',
			'label' => 'Trạng thái dịch vụ',
			'set_select' => FALSE,
			'title' => 'T.Thái',
			'type' => 'string',
		),

		'contract_code' => array(
			'name' => 'contract_code',
			'label' => 'Mã Hợp đồng',
			'set_select' => FALSE,
			'title' => 'Mã Hợp đồng',
			'set_order' => TRUE,
			'type' => 'string',
		),

		'customer_display_name' => array(
			'name' 			=> 'customer_display_name',
			'label' 		=> 'Khách hàng',
			'set_select' 	=> FALSE,
			'title' 		=> 'Khách hàng',
			'set_order' 	=> FALSE,
			'type' 			=> 'string',
		),

		'customer_user_type' => array(
			'name' 			=> 'customer_user_type',
			'label' 		=> 'Loại Kh',
			'set_select' 	=> FALSE,
			'title' 		=> 'Loại Kh',
			'set_order' 	=> FALSE,
			'type' 			=> 'string',
		),

		'customer_tax' => array(
			'name' 			=> 'customer_tax',
			'label' 		=> 'MST',
			'set_select' 	=> FALSE,
			'title' 		=> 'MST',
			'set_order' 	=> FALSE,
			'type' 			=> 'string',
		),

		'representative_name' => array(
			'name' => 'representative_name',
			'label' => 'Tên người đại diện ký HĐ',
			'set_select' => FALSE,
			'title' => 'Người đại diện',
			'set_order' => TRUE,
			'type' => 'string',
		),

		'representative_email' => array(
			'name' => 'representative_email',
			'label' => 'Email người đại diện',
			'set_select' => FALSE,
			'title' => '(email) Người đại diện',
			'type' => 'string',
			'set_order' => TRUE
		),

		'representative_phone' => array(
			'name' => 'representative_phone',
			'label' => 'Số điện thoại người đại diện',
			'set_select' => FALSE,
			'title' => '(Phone) Người đại diện',
			'type' => 'string',
			'set_order' => TRUE
		),

		'contract_note' => array(
			'name' => 'contract_note',
			'label' => 'ghi chú nguồn khách hàng',
			'set_select' => FALSE,
			'title' => 'Ghi chú',
			'type' => 'string',
			'set_order' => TRUE
		),

		'created_on' => array(
			'name' => 'created_on',
			'label' => 'Ngày tạo hợp đồng',
			'set_select' => FALSE,
			'title' => 'Ngày tạo',
			'type' => 'timestamp',
			'set_order' => FALSE
		),	

		'created_by' => array(
			'name' => 'created_by',
			'label' => 'Người tạo hợp đồng',
			'set_select' => FALSE,
			'title' => 'Người tạo',
			'type' => 'string',
			'set_order' => FALSE
		),

		'verified_on' => array(
			'name' => 'verified_on',
			'label' => 'Ngày duyệt hợp đồng',
			'set_select' => FALSE,
			'title' => 'Duyệt lúc',
			'type' => 'timestamp',
			'set_order' => FALSE
		),

        'verfied_by' => array(
			'name' => 'verfied_by',
			'label' => 'Người duyệt hợp đồng',
			'set_select' => FALSE,
			'title' => 'Người duyệt',
			'type' => 'string',
			'set_order' => FALSE
		),

		'contract_begin' => array(
			'name' => 'contract_begin',
			'label' => 'Ngày bắt đầu hợp đồng',
			'set_select' => FALSE,
			'title' => 'Ngày bắt đầu HĐ',
			'type' => 'timestamp',
			'set_order' => FALSE
		),

		'contract_end' => array(
			'name' => 'contract_end',
			'label' => 'Ngày kết thúc hợp đồng',
			'set_select' => FALSE,
			'title' => 'Ngày kết thúc HĐ',
			'type' => 'timestamp',
			'set_order' => FALSE
		),

		'contract_daterange' => array(
			'name' => 'contract_daterange',
			'label' => 'Thời gian Hợp đồng',
			'set_select' => FALSE,
			'title' => 'T/G HĐ',
			'type' => 'string',
			'set_order' => TRUE
		),

		'start_service_time' => array(
			'name' => 'start_service_time',
			'label' => 'Ngày kích hoạt dịch vụ',
			'set_select' => FALSE,
			'title' => 'Ng.Kích hoạt',
			'type' => 'timestamp',
			'set_order' => TRUE
		),

		'end_service_time' => array(
			'name' => 'end_service_time',
			'label' => 'Ngày kết thúc dịch vụ',
			'set_select' => FALSE,
			'title' => 'Ng.K.Thúc dịch vụ',
			'type' => 'timestamp',
			'set_order' => TRUE
		),

		'staff_business' => array(
			'name' => 'staff_business',
			'label' => 'Kinh doanh phụ trách',
			'set_select' => FALSE,
			'title' => 'NVKD',
			'type' => 'string',
			'set_order' => TRUE
		),

		'staff_advertise' => array(
			'name' => 'staff_advertise',
			'label' => 'Kỹ thuật thực hiện',
			'set_select' => FALSE,
			'title' => 'Kỹ thuật',
			'type' => 'string',
			'set_order' => FALSE
		),

		'fb_staff_advertise' => array(
			'name' => 'fb_staff_advertise',
			'label' => 'Kỹ thuật thực hiện (FB)',
			'set_select' => FALSE,
			'title' => 'Kỹ thuật (FB)',
			'type' => 'string',
			'set_order' => FALSE
		),

		'type' => array(
			'name' => 'type',
			'label' => 'Loại dịch vụ',
			'set_select' => FALSE,
			'title' => 'Dịch vụ',
			'type' => 'string',
			'set_order' => TRUE
		),

		'contract_value' => array(
			'name' => 'contract_value',
			'label' => 'Giá trị hợp đồng (chưa thuế)',
			'set_select' => FALSE,
			'title' => 'GTHĐ (-VAT)',
			'type' => 'number',
			'set_order' => TRUE
		),

		'contract_budget' => array(
			'name' => 'contract_budget',
			'label' => 'Ngân sách QC',
			'set_select' => FALSE,
			'title' => 'Ngân sách QC',
			'type' => 'number',
			'set_order' => FALSE
		),

		'budget' => array(
			'name' => 'budget',
			'label' => 'Ngân sách đã thu',
			'set_select' => FALSE,
			'title' => 'Ngân sách đã thu',
			'type' => 'number',
			'set_order' => FALSE
		),

		'actual_result' => array(
			'name' => 'actual_result',
			'label' => 'NS đã chạy',
			'set_select' => FALSE,
			'title' => 'NS đã chạy',
			'type' => 'number',
			'set_order' => FALSE
		),

		'service_fee' => array(
			'name' => 'service_fee',
			'label' => 'Phí dịch vụ',
			'set_select' => FALSE,
			'title' => 'Phí dịch vụ',
			'type' => 'number',
			'set_order' => FALSE
		),

		'service_fee_rate_actual' => array(
			'name' => 'service_fee_rate_actual',
			'label' => '% Phí dịch vụ thực tế',
			'set_select' => FALSE,
			'title' => '% Phí dịch vụ thực tế',
			'type' => 'number',
			'set_order' => FALSE
		),

		'payment_service_fee' => array(
			'name' => 'payment_service_fee',
			'label' => 'Phí dịch vụ đã thanh toán',
			'set_select' => FALSE,
			'title' => 'Phí dịch vụ đã thanh toán',
			'type' => 'number',
			'set_order' => FALSE
		),

		'vat' => array(
			'name' => 'vat',
			'label' => 'VAT',
			'set_select' => FALSE,
			'title' => 'VAT',
			'type' => 'number',
			'set_order' => TRUE
		),

		'vat_amount' => array(
			'name' => 'vat_amount',
			'label' => 'VAT (số tiền)',
			'set_select' => FALSE,
			'title' => 'VAT (số tiền)',
			'type' => 'number',
			'set_order' => TRUE
		),

		'fct' => array(
			'name' => 'fct',
			'label' => 'Thuế nhà thầu',
			'set_select' => FALSE,
			'title' => 'Thuế nhà thầu',
			'type' => 'number',
			'set_order' => TRUE
		),

		'fct_amount' => array(
			'name' => 'fct_amount',
			'label' => 'Thuế nhà thầu (amount)',
			'set_select' => FALSE,
			'title' => 'Thuế nhà thầu (amount)',
			'type' => 'number',
			'set_order' => TRUE
		),

		'is_first_contract' => array(
			'name' => 'is_first_contract',
			'label' => 'Ký mới hoặc Tái ký',
			'set_select' => FALSE,
			'title' => 'Ký mới',
			'type' => 'string',
			'set_order' => FALSE
		),

		'adcampaign_ids' => array(
			'name' => 'adcampaign_ids',
			'label' => 'Campaign (FB)',
			'set_select' => FALSE,
			'title' => 'Campaign (FB)',
			'type' => 'string',
			'set_order' => FALSE
		),	

		'adaccount_id' => array(
			'name' => 'adaccount_id',
			'label' => 'AdAccount Id',
			'set_select' => FALSE,
			'title' => 'ID Tài khoản',
			'type' => 'string',
			'set_order' => FALSE
		),

		'adaccount_name' => array(
			'name' => 'AdAccount',
			'label' => 'Tên tài khoản',
			'set_select' => FALSE,
			'title' => 'Tên tài khoản',
			'type' => 'string',
			'set_order' => FALSE
		),
		
		'adaccount_status' => array(
			'name' => 'adaccount_status',
			'label' => 'TT AdAccounts (FB)',
			'set_select' => FALSE,
			'title' => 'TT AdAccounts (FB)',
			'type' => 'string',
			'set_order' => FALSE
		),

		'customer_code' => array(
			'name' => 'customer_code',
			'label' => 'MSKH',
			'set_select' => FALSE,
			'title' => 'MSKH',
			'type' => 'string',
			'set_order' => true
		),

		'typeOfService' => array(
			'name' => 'typeOfService',
			'label' => 'Loại hình',
			'set_select' => FALSE,
			'title' => 'Loại hình',
			'type' => 'string',
			'set_order' => FALSE
		),

		'contract_budget_payment_type' => array(
			'name' => 'contract_budget_payment_type',
			'label' => 'PTTT.NSQC',
			'set_select' => FALSE,
			'title' => 'PTTT.NSQC',
			'type' => 'string',
			'set_order' => FALSE
        ),

        'service_fee_payment_type' => array(
			'name' => 'service_fee_payment_type',
			'label' => 'PTTT.PDV',
			'set_select' => FALSE,
			'title' => 'PTTT.PDV',
			'type' => 'string',
			'set_order' => FALSE
		),
	),
	'default_columns' => array(
		'contract_code',
		'contract_daterange',
		'start_service_time',
		'contract_value',
		'staff_business',
		'type',
		'is_first_contract',
		'status',
	)
);