<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$columns = array(
    'fact_warehouse_exchange_id' => array(
        'name' => 'fact_warehouse_exchange_id',
        'label' => 'UUID',
        'set_select' => false,
        'title' => 'UUID',
        'type' => 'string',
        'align' => 'left'
    ),

	'fact_warehouse_exchange_begin_balance' => array(
        'name' => 'fact_warehouse_exchange_begin_balance',
        'label' => 'Đầu kỳ',
        'set_select' => false,
        'title' => 'Đầu kỳ',
        'type' => 'number',
        'align' => 'right'
    ),
	
	'fact_warehouse_exchange_import' => array(
        'name' => 'fact_warehouse_exchange_import',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
	'fact_warehouse_exchange_export_use' => array(
        'name' => 'fact_warehouse_exchange_export_use',
        'label' => 'Xuất dùng',
        'set_select' => false,
        'title' => 'Xuất dùng',
        'type' => 'number',
        'align' => 'right'
    ),
	'fact_warehouse_exchange_export_suspend' => array(
        'name' => 'fact_warehouse_exchange_export_suspend',
        'label' => 'Xuất TK chết',
        'set_select' => false,
        'title' => 'Xuất TK chết',
        'type' => 'number',
        'align' => 'right'
    ),
	'fact_warehouse_exchange_balance' => array(
        'name' => 'fact_warehouse_exchange_balance',
        'label' => 'Balance',
        'set_select' => false,
        'title' => 'Balance',
        'type' => 'number',
        'align' => 'right'
    ),
	'fact_warehouse_exchange_time_id' => array(
        'name' => 'fact_warehouse_exchange_time_id',
        'label' => 'ID_TIME',
        'set_select' => false,
        'title' => 'ID_TIME',
        'type' => 'string',
        'align' => 'left'
    ),
	'fact_warehouse_exchange_ad_warehouse_id' => array(
        'name' => 'fact_warehouse_exchange_ad_warehouse_id',
        'label' => 'ID_FWE',
        'set_select' => false,
        'title' => 'ID_FWE',
        'type' => 'string',
        'align' => 'left'
    ),
	'fact_warehouse_exchange_ad_bm_id' => array(
        'name' => 'fact_warehouse_exchange_ad_bm_id',
        'label' => 'IDBM',
        'set_select' => false,
        'title' => 'IDBM',
        'type' => 'string',
        'align' => 'left'
    ),
	'fact_warehouse_exchange_ad_account_id' => array(
        'name' => 'fact_warehouse_exchange_ad_account_id',
        'label' => 'ID TK(local)',
        'set_select' => false,
        'title' => 'ID TK(local)',
        'type' => 'string',
        'align' => 'left'
    ),
	'fact_warehouse_exchange_contract_id' => array(
        'name' => 'fact_warehouse_exchange_contract_id',
        'label' => 'ID HĐ',
        'set_select' => false,
        'title' => 'ID HĐ',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_warehouse_id' => array(
        'name' => 'ad_warehouse_id',
        'label' => 'ID Kho',
        'set_select' => false,
        'title' => 'ID Kho',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_warehouse_name' => array(
        'name' => 'ad_warehouse_name',
        'label' => 'Tên Kho',
        'set_select' => false,
        'title' => 'Tên Kho',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_warehouse_status' => array(
        'name' => 'ad_warehouse_status',
        'label' => 'TT Kho',
        'set_select' => false,
        'title' => 'TT Kho',
        'type' => 'string',
        'align' => 'center'
    ),
	'ad_warehouse_quantity' => array(
        'name' => 'ad_warehouse_quantity',
        'label' => 'Quantity',
        'set_select' => false,
        'title' => 'Quantity',
        'type' => 'number',
        'align' => 'right'
    ),
	'ad_warehouse_created_at' => array(
        'name' => 'ad_warehouse_created_at',
        'label' => 'Kho tạo lúc',
        'set_select' => false,
        'title' => 'Kho tạo lúc',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_warehouse_updated_at' => array(
        'name' => 'ad_warehouse_updated_at',
        'label' => 'Kho cập nhật lúc',
        'set_select' => false,
        'title' => 'Kho cập nhật lúc',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_warehouse_bm_id' => array(
        'name' => 'ad_warehouse_bm_id',
        'label' => 'IDBM',
        'set_select' => false,
        'title' => 'IDBM',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_warehouse_original_bm_term_id' => array(
        'name' => 'ad_warehouse_original_bm_term_id',
        'label' => 'IDBMT',
        'set_select' => false,
        'title' => 'IDBMT',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_id' => array(
        'name' => 'ad_business_manager_id',
        'label' => 'ID Business Manager',
        'set_select' => false,
        'title' => 'ID Business Manager',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_name' => array(
        'name' => 'ad_business_manager_name',
        'label' => 'Business Manager',
        'set_select' => false,
        'title' => 'Business Manager',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_created_by' => array(
        'name' => 'ad_business_manager_created_by',
        'label' => 'BM tạo bởi',
        'set_select' => false,
        'title' => 'BM tạo bởi',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_business_manager_created_time' => array(
        'name' => 'ad_business_manager_created_time',
        'label' => 'BM tạo lúc',
        'set_select' => false,
        'title' => 'BM tạo lúc',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_business_manager_extended_updated_time' => array(
        'name' => 'ad_business_manager_extended_updated_time',
        'label' => 'BM cập nhật lúc',
        'set_select' => false,
        'title' => 'BM cập nhật lúc',
        'type' => 'string',
        'align' => 'timestap'
    ),
	'ad_business_manager_is_hidden' => array(
        'name' => 'ad_business_manager_is_hidden',
        'label' => 'BM IS_HIDDEN',
        'set_select' => false,
        'title' => 'BM IS_HIDDEN',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_verification_status' => array(
        'name' => 'ad_business_manager_verification_status',
        'label' => 'verification_status',
        'set_select' => false,
        'title' => 'verification_status',
        'type' => 'string',
        'align' => 'center'
    ),
	'ad_business_manager_payment_account_id' => array(
        'name' => 'ad_business_manager_payment_account_id',
        'label' => 'payment_account_id',
        'set_select' => false,
        'title' => 'payment_account_id',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_timezone_id' => array(
        'name' => 'ad_business_manager_timezone_id',
        'label' => 'timezone_id',
        'set_select' => false,
        'title' => 'timezone_id',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_created_at' => array(
        'name' => 'ad_business_manager_created_at',
        'label' => 'created_at',
        'set_select' => false,
        'title' => 'created_at',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_business_manager_updated_at' => array(
        'name' => 'ad_business_manager_updated_at',
        'label' => 'updated_at',
        'set_select' => false,
        'title' => 'updated_at',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_business_manager_original_term_id' => array(
        'name' => 'ad_business_manager_original_term_id',
        'label' => 'BM original term_id',
        'set_select' => false,
        'title' => 'BM original term_id',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_business_id' => array(
        'name' => 'ad_business_manager_business_id',
        'label' => 'ad BM business_id',
        'set_select' => false,
        'title' => 'ad BM business_id',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_account_id' => array(
        'name' => 'ad_account_id',
        'label' => 'ID TK',
        'set_select' => false,
        'title' => 'ID TK',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_account_created_at' => array(
        'name' => 'ad_account_created_at',
        'label' => 'ad account created at',
        'set_select' => false,
        'title' => 'ad account created at',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_account_updated_at' => array(
        'name' => 'ad_account_updated_at',
        'label' => 'ad account updated_at',
        'set_select' => false,
        'title' => 'ad account updated_at',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_account_name' => array(
        'name' => 'ad_account_name',
        'label' => 'Tên TK',
        'set_select' => false,
        'title' => 'Tên TK',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_account_account_id' => array(
        'name' => 'ad_account_account_id',
        'label' => 'ID TK',
        'set_select' => false,
        'title' => 'ID TK',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_account_status' => array(
        'name' => 'ad_account_status',
        'label' => 'TT.TK',
        'set_select' => false,
        'title' => 'TT.TK',
        'type' => 'string',
        'align' => 'center'
    ),
	'ad_account_amount_spent' => array(
        'name' => 'ad_account_amount_spent',
        'label' => 'Amount-Spent',
        'set_select' => false,
        'title' => 'Amount-Spent',
        'type' => 'number',
        'align' => 'right'
    ),
	'ad_account_age' => array(
        'name' => 'ad_account_age',
        'label' => 'Age',
        'set_select' => false,
        'title' => 'Age',
        'type' => 'number',
        'align' => 'right'
    ),
	'ad_account_created_time' => array(
        'name' => 'ad_account_created_time',
        'label' => 'TK tạo lúc',
        'set_select' => false,
        'title' => 'TK tạo lúc',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_account_stock_status' => array(
        'name' => 'ad_account_stock_status',
        'label' => 'Stock Status',
        'set_select' => false,
        'title' => 'Stock Status',
        'type' => 'string',
        'align' => 'center'
    ),
	'ad_account_type_of_ownership' => array(
        'name' => 'ad_account_type_of_ownership',
        'label' => 'Ownership',
        'set_select' => false,
        'title' => 'Ownership',
        'type' => 'string',
        'align' => 'center'
    ),
	'ad_account_creation_type' => array(
        'name' => 'ad_account_creation_type',
        'label' => 'Creation type',
        'set_select' => false,
        'title' => 'Creation type',
        'type' => 'string',
        'align' => 'center'
    ),
	'ad_account_currency' => array(
        'name' => 'ad_account_currency',
        'label' => 'CURRENCY',
        'set_select' => false,
        'title' => 'CURRENCY',
        'type' => 'string',
        'align' => 'center'
    ),
	'ad_account_funding_source' => array(
        'name' => 'ad_account_funding_source',
        'label' => 'FUNDING SOURCE',
        'set_select' => false,
        'title' => 'FUNDING SOURCE',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_account_funding_source_details' => array(
        'name' => 'ad_account_funding_source_details',
        'label' => 'FUNDING SOURCE DETAIL',
        'set_select' => false,
        'title' => 'FUNDING SOURCE DETAIL',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_account_spend_cap' => array(
        'name' => 'ad_account_spend_cap',
        'label' => 'ADACCOUNT SPEND CAP',
        'set_select' => false,
        'title' => 'ADACCOUNT SPEND CAP',
        'type' => 'number',
        'align' => 'right'
    ),
	'ad_account_min_daily_budget' => array(
        'name' => 'ad_account_min_daily_budget',
        'label' => 'AD ACCOUNT MIN DAILY BUDGET',
        'set_select' => false,
        'title' => 'AD ACCOUNT MIN DAILY BUDGET',
        'type' => 'number',
        'align' => 'right'
    ),
	'ad_account_balance' => array(
        'name' => 'ad_account_balance',
        'label' => 'AD ACCOUNT BALANCE',
        'set_select' => false,
        'title' => 'AD ACCOUNT BALANCE',
        'type' => 'number',
        'align' => 'right'
    ),
	'ad_account_disable_reason' => array(
        'name' => 'ad_account_disable_reason',
        'label' => 'DISABLE REASON',
        'set_select' => false,
        'title' => 'DISABLE REASON',
        'type' => 'string',
        'align' => 'center'
    ),
	'ad_account_original_term_id' => array(
        'name' => 'ad_account_original_term_id',
        'label' => 'Ad account original id',
        'set_select' => false,
        'title' => 'Ad account original id',
        'type' => 'number',
        'align' => 'right'
    ),
	'ad_account_bank_id' => array(
        'name' => 'ad_account_bank_id',
        'label' => 'BankId',
        'set_select' => false,
        'title' => 'BankId',
        'type' => 'number',
        'align' => 'right'
    )
);

$config['datasource'] = array(
    'columns' => $columns,
    'default_columns' => array(

        'fact_warehouse_exchange_id',
		'ad_business_manager_business_id',
		'ad_business_manager_name',
		'ad_account_name',
		'ad_account_account_id',

		'fact_warehouse_exchange_begin_balance',
		'fact_warehouse_exchange_import',
		'fact_warehouse_exchange_export_use',
		'fact_warehouse_exchange_export_suspend',
		'fact_warehouse_exchange_balance',

        'ad_warehouse_id',
        'ad_warehouse_name',
        'ad_warehouse_status',
        'ad_warehouse_created_at',
        'ad_warehouse_updated_at',

        'ad_business_manager_business_id',
        'ad_business_manager_name',
        'ad_business_manager_created_by',
        'ad_business_manager_created_time',
        'ad_business_manager_extended_updated_time',
        'ad_business_manager_is_hidden',
        'ad_business_manager_verification_status',
        'ad_business_manager_payment_account_id',
        'ad_business_manager_timezone_id',
        'ad_business_manager_created_at',
        'ad_business_manager_updated_at',
        
        
        'ad_account_name',
        'ad_account_account_id',
        'ad_account_status',
        'ad_account_created_at',
        'ad_account_updated_at',
        'ad_account_amount_spent',
        'ad_account_age',
        'ad_account_created_time',
        'ad_account_stock_status',
        'ad_account_type_of_ownership',
        'ad_account_creation_type',
        'ad_account_currency',
        'ad_account_funding_source',
        'ad_account_funding_source_details',
        'ad_account_spend_cap',
        'ad_account_min_daily_budget',
        'ad_account_balance',
        'ad_account_disable_reason'
    )
);
unset($columns);