<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['receipt'] = array(
	
	'transfer_type' => ['cash' => 'Tiền mặt', 'account' => 'Tài khoản'],

	'type' => [
		'receipt_payment' 			=> 'Thanh toán',
		'receipt_caution' 			=> 'Bảo lãnh',
		'receipt_payment_on_behaft' => 'Nhận thay mặt thanh toán'
	],

	'originalPaymentTypes' => [ 'receipt_payment', 'receipt_caution' ]
);