<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$columns = array(

    'fact_warehouse_summary_id' => array(
        'name' => 'fact_warehouse_summary_id',
        'label' => 'UUID',
        'set_select' => false,
        'title' => 'UUID',
        'type' => 'string',
        'align' => 'left'
    ),

    'ad_warehouse_id' => array(
        'name' => 'ad_warehouse_id',
        'label' => 'ID Kho',
        'set_select' => false,
        'title' => 'ID Kho',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_warehouse_name' => array(
        'name' => 'ad_warehouse_name',
        'label' => 'Tên Kho',
        'set_select' => false,
        'title' => 'Tên Kho',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_warehouse_status' => array(
        'name' => 'ad_warehouse_status',
        'label' => 'TT Kho',
        'set_select' => false,
        'title' => 'TT Kho',
        'type' => 'string',
        'align' => 'center'
    ),
	'ad_warehouse_quantity' => array(
        'name' => 'ad_warehouse_quantity',
        'label' => 'Quantity',
        'set_select' => false,
        'title' => 'Quantity',
        'type' => 'number',
        'align' => 'right'
    ),
	'ad_warehouse_created_at' => array(
        'name' => 'ad_warehouse_created_at',
        'label' => 'Kho tạo lúc',
        'set_select' => false,
        'title' => 'Kho tạo lúc',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_warehouse_updated_at' => array(
        'name' => 'ad_warehouse_updated_at',
        'label' => 'Kho cập nhật lúc',
        'set_select' => false,
        'title' => 'Kho cập nhật lúc',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_warehouse_bm_id' => array(
        'name' => 'ad_warehouse_bm_id',
        'label' => 'IDBM',
        'set_select' => false,
        'title' => 'IDBM',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_warehouse_original_bm_term_id' => array(
        'name' => 'ad_warehouse_original_bm_term_id',
        'label' => 'IDBMT',
        'set_select' => false,
        'title' => 'IDBMT',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_id' => array(
        'name' => 'ad_business_manager_id',
        'label' => 'ID Business Manager',
        'set_select' => false,
        'title' => 'ID Business Manager',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_name' => array(
        'name' => 'ad_business_manager_name',
        'label' => 'Business Manager',
        'set_select' => false,
        'title' => 'Business Manager',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_created_by' => array(
        'name' => 'ad_business_manager_created_by',
        'label' => 'BM tạo bởi',
        'set_select' => false,
        'title' => 'BM tạo bởi',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_business_manager_created_time' => array(
        'name' => 'ad_business_manager_created_time',
        'label' => 'BM tạo lúc',
        'set_select' => false,
        'title' => 'BM tạo lúc',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_business_manager_extended_updated_time' => array(
        'name' => 'ad_business_manager_extended_updated_time',
        'label' => 'BM cập nhật lúc',
        'set_select' => false,
        'title' => 'BM cập nhật lúc',
        'type' => 'string',
        'align' => 'timestap'
    ),
	'ad_business_manager_is_hidden' => array(
        'name' => 'ad_business_manager_is_hidden',
        'label' => 'BM IS_HIDDEN',
        'set_select' => false,
        'title' => 'BM IS_HIDDEN',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_verification_status' => array(
        'name' => 'ad_business_manager_verification_status',
        'label' => 'verification_status',
        'set_select' => false,
        'title' => 'verification_status',
        'type' => 'string',
        'align' => 'center'
    ),
	'ad_business_manager_payment_account_id' => array(
        'name' => 'ad_business_manager_payment_account_id',
        'label' => 'payment_account_id',
        'set_select' => false,
        'title' => 'payment_account_id',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_timezone_id' => array(
        'name' => 'ad_business_manager_timezone_id',
        'label' => 'timezone_id',
        'set_select' => false,
        'title' => 'timezone_id',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_created_at' => array(
        'name' => 'ad_business_manager_created_at',
        'label' => 'created_at',
        'set_select' => false,
        'title' => 'created_at',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_business_manager_updated_at' => array(
        'name' => 'ad_business_manager_updated_at',
        'label' => 'updated_at',
        'set_select' => false,
        'title' => 'updated_at',
        'type' => 'timestamp',
        'align' => 'left'
    ),
	'ad_business_manager_original_term_id' => array(
        'name' => 'ad_business_manager_original_term_id',
        'label' => 'BM original term_id',
        'set_select' => false,
        'title' => 'BM original term_id',
        'type' => 'string',
        'align' => 'left'
    ),
	'ad_business_manager_business_id' => array(
        'name' => 'ad_business_manager_business_id',
        'label' => 'ad BM business_id',
        'set_select' => false,
        'title' => 'ad BM business_id',
        'type' => 'string',
        'align' => 'left'
    ),

    'fact_warehouse_summary_time_id' => array(
        'name' => 'fact_warehouse_summary_time_id',
        'label' => 'UUID Time',
        'set_select' => false,
        'title' => 'UUID Time',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_warehouse_id' => array(
        'name' => 'fact_warehouse_summary_ad_warehouse_id',
        'label' => 'UUID AWH',
        'set_select' => false,
        'title' => 'UUID AWH',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_bm_id' => array(
        'name' => 'fact_warehouse_summary_ad_bm_id',
        'label' => 'UUID BM',
        'set_select' => false,
        'title' => 'UUID BM',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_import' => array(
        'name' => 'fact_warehouse_summary_import',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_export_use' => array(
        'name' => 'fact_warehouse_summary_export_use',
        'label' => 'Xuất dùng',
        'set_select' => false,
        'title' => 'Xuất dùng',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_export_suspend' => array(
        'name' => 'fact_warehouse_summary_export_suspend',
        'label' => 'Suspended',
        'set_select' => false,
        'title' => 'Suspended',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_count',
        'label' => 'AdAccounts',
        'set_select' => false,
        'title' => 'AdAccounts',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_status_active_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_status_active_count',
        'label' => 'ACTIVE',
        'set_select' => false,
        'title' => 'ACTIVE',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_status_disabled_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_status_disabled_count',
        'label' => 'DISABLED',
        'set_select' => false,
        'title' => 'DISABLED',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_status_unsettled_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_status_unsettled_count',
        'label' => 'AdAccounts UNSETTLED',
        'set_select' => false,
        'title' => 'AdAccounts UNSETTLED',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_status_pending_risk_review_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_status_pending_risk_review_count',
        'label' => 'PENDING RISK REVIEW',
        'set_select' => false,
        'title' => 'PENDING RISK REVIEW',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_status_pending_settlement_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_status_pending_settlement_count',
        'label' => 'PENDING SETTLEMENT',
        'set_select' => false,
        'title' => 'PENDING SETTLEMENT',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_status_in_grace_period_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_status_in_grace_period_count',
        'label' => 'INGRACE PERIOD',
        'set_select' => false,
        'title' => 'INGRACE PERIOD',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_status_pending_closure_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_status_pending_closure_count',
        'label' => 'PENDING CLOSURE',
        'set_select' => false,
        'title' => 'PENDING CLOSURE',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_status_closed_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_status_closed_count',
        'label' => 'CLOSED',
        'set_select' => false,
        'title' => 'CLOSED',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_status_any_active_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_status_any_active_count',
        'label' => 'ANY ACTIVE',
        'set_select' => false,
        'title' => 'ANY ACTIVE',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_status_any_closed_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_status_any_closed_count',
        'label' => 'ANY CLOSED',
        'set_select' => false,
        'title' => 'ANY CLOSED',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_status_unspecified_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_status_unspecified_count',
        'label' => 'UNSPECIFIED',
        'set_select' => false,
        'title' => 'UNSPECIFIED',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_none_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_none_count',
        'label' => 'DISABLED NONE',
        'set_select' => false,
        'title' => 'DISABLED NONE',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_ads_integrity_policy_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_ads_integrity_policy_count',
        'label' => 'DISABLED ADS INTEGRITY POLICY',
        'set_select' => false,
        'title' => 'DISABLED ADS INTEGRITY POLICY',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_ads_ip_review_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_ads_ip_review_count',
        'label' => 'DISABLED IP REVIEW',
        'set_select' => false,
        'title' => 'DISABLED IP REVIEW',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_risk_payment_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_risk_payment_count',
        'label' => 'DISABLED RISK PAYMENT',
        'set_select' => false,
        'title' => 'DISABLED RISK PAYMENT',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_gray_account_shut_down_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_gray_account_shut_down_count',
        'label' => 'DISABLED GRAY ACCOUNT SHUTDOWN',
        'set_select' => false,
        'title' => 'DISABLED GRAY ACCOUNT SHUTDOWN',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_ads_afc_review_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_ads_afc_review_count',
        'label' => 'DISABLED AFC REVIEW',
        'set_select' => false,
        'title' => 'DISABLED AFC REVIEW',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_business_integrity_rar_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_business_integrity_rar_count',
        'label' => 'DISABLED BUSINESS INTEGRITY RAR',
        'set_select' => false,
        'title' => 'DISABLED BUSINESS INTEGRITY RAR',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_permanent_close_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_permanent_close_count',
        'label' => 'DISABLED PERMANENT CLOSE',
        'set_select' => false,
        'title' => 'DISABLED PERMANENT CLOSE',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_unused_reseller_account_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_unused_reseller_account_count',
        'label' => 'DISABLED UNUSED RESELLER ACCOUNT',
        'set_select' => false,
        'title' => 'DISABLED UNUSED RESELLER ACCOUNT',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_unused_account_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_unused_account_count',
        'label' => 'DISABLED UNUSED ACCOUNT',
        'set_select' => false,
        'title' => 'DISABLED UNUSED ACCOUNT',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_umbrella_ad_account_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_umbrella_ad_account_count',
        'label' => 'DISABLED UMBRELLA AD ACCOUNT',
        'set_select' => false,
        'title' => 'DISABLED UMBRELLA AD ACCOUNT',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_business_manager_integrity_policy_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_business_manager_integrity_policy_count',
        'label' => 'DISABLED BM INTEGIRY POLICY',
        'set_select' => false,
        'title' => 'DISABLED BM INTEGIRY POLICY',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_misrepresented_ad_account_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_misrepresented_ad_account_count',
        'label' => 'DISABLED MISREPRESENTED AD ACCOUNT',
        'set_select' => false,
        'title' => 'DISABLED MISREPRESENTED AD ACCOUNT',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_aoab_deshare_legal_entity_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_aoab_deshare_legal_entity_count',
        'label' => 'DISABLED AOAB DESHARE LEGAL ENTITY',
        'set_select' => false,
        'title' => 'DISABLED AOAB DESHARE LEGAL ENTITY',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_ctx_thread_review_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_ctx_thread_review_count',
        'label' => 'DISABLED CTX THREAD REVIEW',
        'set_select' => false,
        'title' => 'DISABLED CTX THREAD REVIEW',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_disable_compromised_ad_account_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_disable_compromised_ad_account_count',
        'label' => 'DISABLED COMPROMISED AD ACCOUNT',
        'set_select' => false,
        'title' => 'DISABLED COMPROMISED AD ACCOUNT',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_stock_status_inuse_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_stock_status_inuse_count',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_stock_status_instock_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_stock_status_instock_count',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_stock_status_pending_io_review_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_stock_status_pending_io_review_count',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_stock_status_closed_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_stock_status_closed_count',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_stock_status_unspecified_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_stock_status_unspecified_count',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_type_of_ownership_internal' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_type_of_ownership_internal',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_type_of_ownership_external' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_type_of_ownership_external',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_type_of_ownership_unspecified' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_type_of_ownership_unspecified',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_creation_type_purchase_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_creation_type_purchase_count',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_creation_type_create_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_creation_type_create_count',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_creation_type_default_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_creation_type_default_count',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_creation_type_unknown_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_creation_type_unknown_count',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_creation_type_unspecified_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_creation_type_unspecified_count',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_creation_type_partner_count' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_creation_type_partner_count',
        'label' => 'Nhập',
        'set_select' => false,
        'title' => 'Nhập',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_amount_spent' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_amount_spent',
        'label' => 'Tổng chi tiêu',
        'set_select' => false,
        'title' => 'Tổng chi tiêu',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_spend_cap_avg' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_spend_cap_avg',
        'label' => 'Spend Cap AVG',
        'set_select' => false,
        'title' => 'Spend Cap AVG',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_min_daily_budget_avg' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_min_daily_budget_avg',
        'label' => 'Min Daily Budget AVG',
        'set_select' => false,
        'title' => 'Min Daily Budget AVG',
        'type' => 'number',
        'align' => 'right'
    ),
    'fact_warehouse_summary_ad_accounts_min_balance_total' => array(
        'name' => 'fact_warehouse_summary_ad_accounts_min_balance_total',
        'label' => 'Min Balance',
        'set_select' => false,
        'title' => 'Min Balance',
        'type' => 'number',
        'align' => 'right'
    ),
);

$config['datasource'] = array(
    'columns' => $columns,
    'default_columns' => array(

        'fact_warehouse_summary_id',
        // 'ad_warehouse_id',
        'ad_warehouse_status',
        'ad_business_manager_business_id',
        'ad_business_manager_name',
        'ad_business_manager_verification_status',
        'fact_warehouse_summary_ad_accounts_amount_spent',
        'fact_warehouse_summary_ad_accounts_spend_cap_avg',
        'fact_warehouse_summary_ad_accounts_min_daily_budget_avg',
        'fact_warehouse_summary_ad_accounts_min_balance_total',
        'fact_warehouse_summary_import',
        'fact_warehouse_summary_export_use',
        'fact_warehouse_summary_export_suspend',
        'fact_warehouse_summary_ad_accounts_count',
        'fact_warehouse_summary_ad_accounts_status_active_count',
        'fact_warehouse_summary_ad_accounts_status_disabled_count',
        'fact_warehouse_summary_ad_accounts_status_unsettled_count',
        'fact_warehouse_summary_ad_accounts_status_pending_risk_review_count',
        'fact_warehouse_summary_ad_accounts_status_pending_settlement_count',
        'fact_warehouse_summary_ad_accounts_status_in_grace_period_count',
        'fact_warehouse_summary_ad_accounts_status_pending_closure_count',
        'fact_warehouse_summary_ad_accounts_status_closed_count',
        'fact_warehouse_summary_ad_accounts_status_any_active_count',
        'fact_warehouse_summary_ad_accounts_status_any_closed_count',
        'fact_warehouse_summary_ad_accounts_status_unspecified_count',
        'fact_warehouse_summary_ad_accounts_disable_none_count',
        'fact_warehouse_summary_ad_accounts_disable_ads_integrity_policy_count',
        'fact_warehouse_summary_ad_accounts_disable_ads_ip_review_count',
        'fact_warehouse_summary_ad_accounts_disable_risk_payment_count',
        'fact_warehouse_summary_ad_accounts_disable_gray_account_shut_down_count',
        'fact_warehouse_summary_ad_accounts_disable_ads_afc_review_count',
        'fact_warehouse_summary_ad_accounts_disable_business_integrity_rar_count',
        'fact_warehouse_summary_ad_accounts_disable_permanent_close_count',
        'fact_warehouse_summary_ad_accounts_disable_unused_reseller_account_count',
        'fact_warehouse_summary_ad_accounts_disable_unused_account_count',
        'fact_warehouse_summary_ad_accounts_disable_umbrella_ad_account_count',
        'fact_warehouse_summary_ad_accounts_disable_business_manager_integrity_policy_count',
        'fact_warehouse_summary_ad_accounts_disable_misrepresented_ad_account_count',
        'fact_warehouse_summary_ad_accounts_disable_aoab_deshare_legal_entity_count',
        'fact_warehouse_summary_ad_accounts_disable_ctx_thread_review_count',
        'fact_warehouse_summary_ad_accounts_disable_compromised_ad_account_count',
        'fact_warehouse_summary_ad_accounts_stock_status_inuse_count',
        'fact_warehouse_summary_ad_accounts_stock_status_instock_count',
        'fact_warehouse_summary_ad_accounts_stock_status_pending_io_review_count',
        'fact_warehouse_summary_ad_accounts_stock_status_closed_count',
        'fact_warehouse_summary_ad_accounts_stock_status_unspecified_count',
        'fact_warehouse_summary_ad_accounts_type_of_ownership_internal',
        'fact_warehouse_summary_ad_accounts_type_of_ownership_external',
        'fact_warehouse_summary_ad_accounts_type_of_ownership_unspecified',
        'fact_warehouse_summary_ad_accounts_creation_type_purchase_count',
        'fact_warehouse_summary_ad_accounts_creation_type_create_count',
        'fact_warehouse_summary_ad_accounts_creation_type_default_count',
        'fact_warehouse_summary_ad_accounts_creation_type_unknown_count',
        'fact_warehouse_summary_ad_accounts_creation_type_unspecified_count',
        'fact_warehouse_summary_ad_accounts_creation_type_partner_count',
    )
);
unset($columns);