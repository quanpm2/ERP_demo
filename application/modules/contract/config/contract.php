<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Enum : taxonomy of contract (term:term_type) */
$config['taxonomy'] = array( 
	'webdoctor'		=> 'WEBDOCTOR', 
	'seo-traffic'	=> 'SEO', 
	'seo-top'		=> 'SEOTOP', 
	'google-ads'	=> 'ADSPLUS',
	'webgeneral' 	=> 'WEBDOCTOR',
	'webdesign'	 	=> 'TKWEB',
	'oneweb' 		=> '1WEB',
	'pushdy' 		=> 'PUSHDY',
	'hosting'	 	=> 'HOSTING',
	'domain'	 	=> 'DOMAIN',
	'facebook-ads'	=> 'FACEBOOK-ADS',
	'zalo-ads'	    => 'ZALO-ADS',
	'tiktok-ads' 	=> 'TIKTOK-ADS',
	'weboptimize'	=> 'WEBOPTIMIZE',
	'banner'		=> 'TKBANNER',
	'fbcontent' 	=> 'BAIVIET',
	'courseads'		=> 'GURU',
	'onead' 		=> '1AD',
	'gws' 		=> 'GOOGLE WORKSPACE',
);

$config['contract_models'] = [
	'webdesign'		=> 'webbuild/webbuild_m',
	'oneweb'		=> 'oneweb/oneweb_m',
	'pushdy'		=> 'pushdy/pushdy_m',
	'weboptimize'	=> 'weboptimize/weboptimize_m',
	'banner'		=> 'banner/banner_m',
	'domain'		=> 'domain/domain_m',
	'hosting'		=> 'hosting/hosting_m',
	'courseads'		=> 'courseads/courseads_m',
	'google-ads'	=> 'googleads/googleads_m',
	'facebook-ads'	=> 'facebookads/facebookads_m',
	'zalo-ads'	    => 'zaloads/zaloads_m',
	'tiktok-ads'	=> 'tiktokads/tiktokads_m',
	'onead'			=> 'onead/onead_m',
	'fbcontent' 	=> 'fbcontent/fbcontent_m',
    'gws'           => 'gws/gws_m',
];

/* Enum : taxonomy - model name mapping */
$config['behaviour_models'] = array(
	'webdesign'		=> 'webbuild/webbuild_behaviour_m',
	'oneweb'		=> 'oneweb/oneweb_behaviour_m',
	'pushdy'		=> 'pushdy/pushdy_behaviour_m',
	'weboptimize'	=> 'weboptimize/weboptimize_behaviour_m',
	'banner'		=> 'banner/banner_behaviour_m',
	'domain'		=> 'domain/domain_behaviour_m',
	'hosting'		=> 'hosting/hosting_behaviour_m',
	'courseads'		=> 'courseads/courseads_behaviour_m',
	'google-ads'	=> 'googleads/googleads_behaviour_m',
	'facebook-ads'	=> 'facebookads/facebookads_behaviour_m',
	'zalo-ads'   	=> 'zaloads/zaloads_behaviour_m',
	'tiktok-ads'	=> 'tiktokads/tiktokads_behaviour_m',
	'onead'			=> 'onead/onead_behaviour_m',
	'fbcontent' 	=> 'fbcontent/fbcontent_behaviour_m',
	'gws' 	        => 'gws/gws_behaviour_m',
);

$config['remap_term_types'] = array(
	'webdesign'		=> 'webdesign',
	'oneweb'		=> 'oneweb',
	'pushdy'		=> 'pushdy',
	'weboptimize'	=> 'weboptimize',
	'banner'		=> 'banner',
	'domain'		=> 'domain',
	'hosting'		=> 'hosting',
	'courseads'		=> 'courseads',
	'google-ads'	=> 'googleads',
	'facebook-ads'	=> 'facebookads',
	'zalo-ads'  	=> 'zaloads',
	'tiktok-ads'	=> 'tiktokads',
	'onead'			=> 'onead',
	'fbcontent' 	=> 'fbcontent',
    'gws' 	        => 'gws',
);

/* Enum Webbuild's services packages */
$config['package'] = array(
	'landing_page'	=>'Landing page',
	'clone_web'		=>'Clone Website',
	'normal'		=>'Web yêu cầu'
);

/* Enum Contract's status (term:term_status) */
$config['contract_status'] = array(
	'draft' 			=> 'Đang soạn',
	'unverified'		=> 'Chưa cấp số',
	'waitingforapprove' => 'Cấp số',
	'pending' 			=> 'Ngưng hoạt động',
	'publish' 			=> 'Thực hiện',
	'ending'			=> 'Đã kết thúc',
	'liquidation'		=> 'Thanh lý',
	'remove' 			=> 'Hủy bỏ'
);

/* Enum Contract's Active Status (term:term_status) */
$config['contract_status_on'] = array('pending', 'publish');

/* Enum Service's name */
$config['services'] = array( 
	'webdoctor'   	=> 'webdoctor', 
	'seo-traffic' 	=> 'SEO Chặng', 
	'seo-top' 	  	=> 'SEO Top', 
	'google-ads'  	=> 'Adsplus',
	'webgeneral'  	=> 'Web Tổng hợp',
	'webdesign'	  	=> 'Thiết kế website',
	'oneweb' 	  	=> '1WEB',
	'pushdy' 	  	=> 'Phần mềm Pushdy',
	'hosting'	  	=> 'Hosting',
	'domain'	  	=> 'Domain',
	'facebook-ads' 	=> 'Facebook ads',
	'zalo-ads'   	=> 'Zalo ads',
	'tiktok-ads' 	=> 'Tik-tok ads',
	'weboptimize' 	=> 'Tối Ưu Web',
	'banner'	  	=> 'Thiết kế Banner',
	'fbcontent' 	=> 'Content Fanpage',
	'courseads' 	=> 'Khóa học GURU',
	'onead' 		=> '1AD',
    'gws' 	        => 'Google workspace',
);

/* Enum Service's name */
$config['contract_services'] = array(
	'seo-traffic'	=> 'SEO Chặng', 
	'seo-top' 	  	=> 'SEO Top', 
	'google-ads'  	=> 'Adsplus',
	'facebook-ads'  => 'Facebook Ads',
	'tiktok-ads'  	=> 'Tiktok Ads',
    'zalo-ads'   	=> 'Zalo ads',
	'webgeneral'  	=> 'Web Tổng Hợp',
	'webdesign'	  	=> 'Thiết kế website',
	'oneweb' 		=> '1WEB - LANDINGPAGE',
	'pushdy' 		=> 'Phần mềm Pushdy',
	'hosting'	  	=> 'Hosting',
	'domain'	  	=> 'Domain' ,
	'weboptimize' 	=> 'Tối Ưu Web',
	'banner'	  	=> 'Thiết kế Banner',
	'fbcontent' 	=> 'Content Fanpage',
	'onead' 		=> '1AD',
    'gws' 	        => 'Google workspace',
);

$config['group-services'] = array(

	'ADSPLUS.VN' => [
		'google-ads'	=> 'Quảng cáo Google Adwords',
		'facebook-ads' 	=> 'Quảng cáo Facebook Ads',
		'zalo-ads'  	=> 'Quảng cáo Zalo Ads',
		'tiktok-ads' 	=> 'Quảng cáo Tik-Tok Ads',
		'onead' 		=> '1AD - Setup QC'
	],
	'WEBDOCTOR.VN' => [
		'webgeneral'	=> 'Chăm sóc Website',
		'webdesign'		=> 'Thiết kế Website',
		'oneweb' 		=> '1WEB - LANDINGPAGE',
		'pushdy' 		=> 'Phần mềm Pushdy',
		'hosting'		=> 'Hosting',
		'domain'		=> 'Domain',
		'weboptimize'	=> 'Tối ưu Website',
		'banner'		=> 'Thiết kế Banner',
	],

	'CHANNEL-SALES' => [
		'courseads' 	=> 'Khóa học',
		'fbcontent' 	=> 'Content Fanpage',
        'gws' 	        => 'Google workspace',
	]
);

$config['regions'] = array(
	'north' => 'Miền Bắc',
	'south' => 'Miền Nam'
);

$config['contract_type'] = array(
	'public'	=> 'Hợp đồng',
	'private'	=> 'Phiếu đăng ký'
);

$config['upload_config'] = array(
	'upload_path'	=> './files/contract/',
	'allowed_types'	=> 'gif|jpg|png',
	'max_size'		=> '4096'
);

$config['read_customers_role_id'] = array(1, 5);

$config['currency'] = array('VND', 'USD', 'AUD');

$config['banks'] = array(
	'TPB-66852888203-CH' => [
		'account' 	=> 'Công ty Cổ Phần Quảng cáo Cổng Việt Nam',
		'id'		=> 66852888203,
		'name'	 	=> 'TPBANK - Chi nhánh Hoàn Kiếm',
		'type' 		=> 'company',
		'zone'		=> 'hn'
	],
	'TCB-19129832121014-CH' => [
		'account' 	=> 'Công ty Cổ Phần Quảng cáo Cổng Việt Nam',
		'id'		=> 19129832121014,
		'name'	 	=> 'TECHCOMBANK - Chi nhánh HCM - Phòng giao dịch Văn Thánhf',
		'type' 		=> 'company',
		'zone'		=> 'hcm'
	],
	'ACB-719677-CH' => [
		'account'	=> 'NGUYEN DAC TRI',
		'id'		=> 719677,
		'name'		=> 'Ngân hàng Á Châu',
		'type' 		=> 'person',
		'zone'		=> 'hcm'
	],
	'MBB-9500123999989-CH' => [
		'account'	=> 'NGUYEN THI THANH HUYEN',
		'id'		=> 9500123999989,
		'name'		=> 'Ngân hàng Quân Đội Hội Sở',
		'type' 		=> 'person',
		'type' 		=> 'hn'
	],
);

$config['offices'] = array(
	'hcm' => [
		'key' => 'hcm',
		'name' => 'CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM',
		'address' => 'tầng 12A, số 1 Thái Hà, phường Trung Liệt, Đống Đa, Hà Nội, Việt Nam',
	],
	'hn' => [
		'key' => 'hn',
		'name' => 'CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM',
		'address' => 'tầng 8, số 402 Nguyễn Thị Minh Khai, phường 5, quận 3, TP.Hồ Chí Minh, Việt Nam',
	],
	'default' => 'hcm'
);

$config['bank_infos'] = array(	
	'company' => array(
		'Chủ tài khoản'	=> 'CONG TY CO PHAN QUANG CAO CONG VN',
		'Số tài khoản'	=> '19129832121014',
		'Ngân hàng'		=> 'TECHCOMBANK - Chi nhánh HCM - Phòng giao dịch Văn Thánh'
	),
	'person'=> array(
		array(
			'Chủ tài khoản'	=> 'NGUYEN DAC TRI',
			'Số tài khoản'	=> '719677',
			'Ngân hàng'		=> 'Ngân hàng Á Châu',
			'representative_zone' => 'hcm'
		),
		array(
			'Chủ tài khoản'	=> 'NGUYEN THI THANH HUYEN',
			'Số tài khoản'	=> '9500123999989 ',
			'Ngân hàng'		=> 'Ngân hàng Quân Đội Hội Sở',
			'representative_zone' => 'hn'
		),
	)
);

$config['printable_title'] = array(
	'default'    	=> 'HỢP ĐỒNG',
	'hosting'    	=> 'HỢP ĐỒNG <br/> <b>Mua dịch vụ hosting</b>',
	'domain'    	=> '<b>HỢP ĐỒNG <br/>Mua tên miền</b>',
	'banner'		=> '<strong>PHIẾU ĐĂNG KÝ DỊCH VỤ THIẾT KẾ BANNER</strong>',
	'fbcontent' 	=> 'HỢP ĐỒNG CUNG CẤP DỊCH VỤ CONTENT FANPAGE',
	'courseads' 	=> 'HỢP ĐỒNG KHOÁ HỌC QUẢNG CÁO TẠI HỌC VIỆN GURU',
	'facebook-ads'  => 'HỢP ĐỒNG CUNG CẤP DỊCH VỤ QUẢNG CÁO TRỰC TUYẾN',
	'tiktok-ads'	=> 'HỢP ĐỒNG CUNG CẤP DỊCH VỤ QUẢNG CÁO TRỰC TUYẾN',
	'googleads'  	=> 'HỢP ĐỒNG CUNG CẤP DỊCH VỤ QUẢNG CÁO TRỰC TUYẾN',
	'webgeneral' 	=> 'HỢP ĐỒNG CUNG CẤP GIẢI PHÁP PHẦN MỀM ỨNG DỤNG <br> <strong>CHĂM SÓC WEBSITE</strong>',
	'weboptimize'	=> 'PHIẾU ĐĂNG KÝ <br> <strong>Dịch vụ "Tối ưu website"</strong>',
	'oneweb'    	=> 'HỢP ĐỒNG MUA BÁN PHẦN MỀM 1WEB.VN',
	'pushdy'    	=> 'HỢP ĐỒNG CUNG CẤP PHẦN MỀM',
	'webdesign'  	=> array(
		'landing_page'	=> 'HỢP ĐỒNG CUNG CẤP GIẢI PHÁP PHẦN MỀM ỨNG DỤNG <br> <strong>THIẾT KẾ WEBSITE (LANDINGPAGE)</strong>',
		'clone_web'		=> 'HỢP ĐỒNG CUNG CẤP GIẢI PHÁP PHẦN MỀM ỨNG DỤNG <br> <strong>THIẾT KẾ WEBSITE THEO MẪU</strong>',
		'normal'		=> 'HỢP ĐỒNG CUNG CẤP GIẢI PHÁP PHẦN MỀM ỨNG DỤNG <br> <strong>THIẾT KẾ WEBSITE</strong>'
	),
	'onead' 		=> 'HỢP ĐỒNG CUNG CẤP DỊCH VỤ SETUP CHIẾN DỊCH QUẢNG CÁO',
	'gws'           => 'HỢP ĐỒNG DỊCH VỤ CUNG CẤP GOOGLE WORKSPACE'
);

$config['con_signature_config'] = array(
	'normal' 	=> array(
		'key' 		=> 'normal',
		'company' 	=> 'CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM',
		'address' 	=> 'Tầng 12A, Tòa nhà Việt Tower, Số 1 Thái Hà, P. Trung Liệt, Đống Đa, HN',
		'phone' 	=> '1800.0098',
		'tax' 		=> '0313547231',
		'name' 		=> 'Bà Nguyễn Như Mai',
		'title' 	=> 'Tổng Giám Đốc'
	),
	'default' 	=> 'normal'
);

$config['contract_represent'] = array(
	'company' 	=> 'CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM',
	'address' 	=> 'Tầng 12A, Tòa nhà Việt Tower, Số 1 Thái Hà, P. Trung Liệt, Đống Đa, HN',
	'phone' 	=> '1800.0098',
	'tax' 		=> '0313547231',
	'name' 		=> 'Bà Nguyễn Như Mai',
	'position' 	=> 'Giám Đốc'
);

$config['payment_methods'] = array(
	'cash' => [
		'name' => 'cash',
		'text' => 'Tiền mặt'
	],
	'bank' => [
		'name' => 'bank',
		'text' => 'Chuyển khoản'
	],
);

$config['metadata_contract'] = array(
	'created_on',
	'created_by',
	'contract_code',
	'representative_gender',
	'representative_name',
	'representative_email',
	'representative_address',
	'representative_zone',
	'representative_phone',
	'representative_position',
	'staff_business',
	'contract_note',
	'vat',
	'contract_value',
	'extra',
	'number_of_payments',
	'contract_begin',
	'contract_end',
);

/* Cấu hình phí dịch vụ thanh toán 100% lần đầu hoặc thanh toán theo đợt */
$config['service_fee_payment_type'] = array(
	'full' => 'Thanh toán 100% phí dịch trong đợt đầu',
	'devide' => 'Thu phí dịch vụ theo mỗi đợt thanh toán',
    'range' => 'Thu phí dịch vụ theo mức ngân sách',
);

$config['user_id_allow_change_balance_spend'] = explode(',', getenv('USER_ID_ALLOW_CHANGE_BALANCE_SPEND') ?: '');