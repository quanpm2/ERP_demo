<?php
class Contract_Package extends Package
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('contract/base_contract_m');
	}

	public function name(){
		
		return 'Contract';
	}

	public function init()
	{	
		$this->_load_menu();
	}

	private function _load_menu()
	{
		$itemId = 'contract';

		if( ! is_module_active('contract')) return FALSE;

		if(has_permission('Googleads.index.access') || has_permission('Facebookads.index.access'))
		{
			$this->menu->add_item(array(
				'id' => 'contract-ad-storage-done',
				'name' => 'Kho TK Ads',
				'parent' => null,
				'slug' => admin_url('contract/adStorage/done'),
				'order' => 0,
				'icon' => 'fa fa-cube',
			), 'left-ads-service');
		}

		if(has_permission('Admin.adwarehouse'))
		{
			$this->menu->add_item(array(
				'id' => 'admin-adwarehouse-index',
				'name' => 'Kho TK FB',
				'parent' => null,
				'slug' => admin_url('contract/adStorage/datawarehouse'),
				'order' => 0,
				'icon' => 'fa fa-cubes',
			), 'left-ads-service');
		}

		if( ! has_permission('Admin.Contract')) return FALSE;

		has_permission('Reports.nonrenewal') AND $this->menu->add_item(array(
			'id' 	=> 'admin-reports-norenewal-10',
			'name' 	=> 'Gooogle-Ads | HĐ kết thúc 10 ngày',
			'parent'=> 'reports',
			'slug' 	=> admin_url('contract/Reports/NonRenewal/googleads'),
			'icon' 	=> 'fa fa-fw fa-barcode',
			'order' => 1,
		),'navbar');

		has_permission('Reports.nonrenewal') AND $this->menu->add_item(array(
			'id' 	=> 'admin-reports-norenewal-30',
			'name' 	=> 'Gooogle-Ads | HĐ kết thúc trên 30 ngày',
			'parent'=> 'reports',
			'slug' 	=> admin_url('contract/Reports/NonRenewal/googleads/30/120'),
			'icon' 	=> 'fa fa-fw fa-barcode',
			'order' => 1,
		),'navbar');

		has_permission('Reports.adspending') AND $this->menu->add_item(array(
			'id' 	=> 'admin-reports-adspending-3',
			'name' 	=> 'Gooogle-Ads | HĐ tạm ngưng 3 ngày',
			'parent'=> 'reports',
			'slug' 	=> admin_url('contract/Reports/AdsPending/googleads/3'),
			'icon' 	=> 'fa fa-fw fa-barcode',
			'order' => 1,
		),'navbar');

		has_permission('Reports.adspending') AND $this->menu->add_item(array(
			'id' 	=> 'admin-reports-adspending-15',
			'name' 	=> 'Gooogle-Ads | HĐ tạm ngưng 15 ngày',
			'parent'=> 'reports',
			'slug' 	=> admin_url('contract/Reports/AdsPending/googleads/15'),
			'icon' 	=> 'fa fa-fw fa-barcode',
			'order' => 1,
		),'navbar');

		has_permission('Reports.adspending') AND $this->menu->add_item(array(
			'id' 	=> 'admin-reports-adspending-30',
			'name' 	=> 'Gooogle-Ads | HĐ tạm ngưng trên 30 ngày',
			'parent'=> 'reports',
			'slug' 	=> admin_url('contract/Reports/AdsPending/googleads/30'),
			'icon' 	=> 'fa fa-fw fa-barcode',
			'order' => 1,
		),'navbar');

		has_permission('Reports.nonrenewal') AND $this->menu->add_item(array(
			'id' 	=> 'admin-reports-facebookads-norenewal-10',
			'name' 	=> 'Facebook | HĐ kết thúc 10 ngày',
			'parent'=> 'reports',
			'slug' 	=> admin_url('contract/Reports/NonRenewal/facebookads'),
			'icon' 	=> 'fa fa-fw fa-barcode',
			'order' => 1,
		),'navbar');

		has_permission('Reports.nonrenewal') AND $this->menu->add_item(array(
			'id' 	=> 'admin-reports-facebookads-norenewal-30',
			'name' 	=> 'Facebook | HĐ kết thúc trên 30 ngày',
			'parent'=> 'reports',
			'slug' 	=> admin_url('contract/Reports/NonRenewal/facebookads/30/120'),
			'icon' 	=> 'fa fa-fw fa-barcode',
			'order' => 1,
		),'navbar');

		has_permission('Reports.adspending') AND $this->menu->add_item(array(
			'id' 	=> 'admin-reports-facebookads-adspending-3',
			'name' 	=> 'Facebook | HĐ tạm ngưng 3 ngày',
			'parent'=> 'reports',
			'slug' 	=> admin_url('contract/Reports/AdsPending/facebookads/3'),
			'icon' 	=> 'fa fa-fw fa-barcode',
			'order' => 1,
		),'navbar');

		has_permission('Reports.adspending') AND $this->menu->add_item(array(
			'id' 	=> 'admin-reports-facebookads-adspending-15',
			'name' 	=> 'Facebook | HĐ tạm ngưng 15 ngày',
			'parent'=> 'reports',
			'slug' 	=> admin_url('contract/Reports/AdsPending/facebookads/15'),
			'icon' 	=> 'fa fa-fw fa-barcode',
			'order' => 1,
		),'navbar');

		has_permission('Reports.adspending') AND $this->menu->add_item(array(
			'id' 	=> 'admin-reports-facebookads-adspending-30',
			'name' 	=> 'Facebook | HĐ tạm ngưng trên 30 ngày',
			'parent'=> 'reports',
			'slug' 	=> admin_url('contract/Reports/AdsPending/facebookads/30'),
			'icon' 	=> 'fa fa-fw fa-barcode',
			'order' => 1,
		),'navbar');

		$this->menu->add_item(array(
			'id' 	=> $itemId,
			'name' 	=> 'Hợp đồng',
			'parent'=> null,
			'slug' 	=> admin_url('contract'),
			'icon' 	=> 'fa fa-barcode',
			'order' => 0,
		),'left');

		if(!is_module('contract')) return FALSE;

		$order = 0;

        $this->menu->add_item(array(
            'id' => 'contract-audit-index',
            'name' => 'Data log',
            'parent' => $itemId,
            'slug' => module_url('audit'),
            'icon' => 'fa fa-fw fa-table',
            'order' => $order++
            ),'left');

		if(has_permission('contract.revenue.manage'))
		{
			$this->menu->add_item(array(
			'id' => 'contract-dashboard',
			'name' => 'Tổng quan',
			'parent' => $itemId,
			'slug' => module_url('dashboard'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-dashboard'
			), 'left');
		}

		if(has_permission('Admin.Contract'))
		{
			$this->menu->add_item(array(
				'id' => 'contract-index',
				'name' => 'Hợp đồng',
				'parent' => $itemId,
				'slug' => module_url(),
				'order' => $order++,
				'icon' => 'fa fa-fw fa-database'
			), 'left');
		}

        if(has_permission('admin.report.access'))
		{
			$this->menu->add_item(array(
				'id' => 'contract-report',
				'name' => 'Báo cáo',
				'parent' => $itemId,
				'slug' => module_url('report'),
				'order' => $order++,
				'icon' => 'fa fw fa-bookmark'
			), 'left');
		}

		if(has_permission('contract.category.access'))
		{
			$this->menu->add_item(array(
				'id' => 'contract-category',
				'name' => 'Ngành nghề',
				'parent' => $itemId,
				'slug' => module_url('category'),
				'icon' => 'fa fa-fw fa-tags',
				'order' => $order++
			),'left');
		}

		if(has_permission('Contract.Revenue'))
		{
			$this->menu->add_item(array(
				'id' => 'contract-revenue-index',
				'name' => 'Dự thu',
				'parent' => $itemId,
				'slug' => module_url('revenue'),
				'icon' => 'fa fa-fw fa-circle-o-notch',
				'order' => $order++
			),'left');
		}

		if(has_permission('contract.receipt.access'))
		{
			$this->menu->add_item(array(
				'id' => 'contract-receipt-index',
				'name' => 'Thanh toán',
				'parent' => $itemId,
				'slug' => module_url('receipt'),
				'icon' => 'fa fa-fw fa-money',
				'order' => $order++
				),'left');
		}

		if(has_permission('contract.setting.access'))
		{
			$this->menu->add_item(array(
				'id' => 'contract-setting-index',
				'name' => 'Cấu hình',
				'parent' => $itemId,
				'slug' => module_url('setting'),
				'icon' => 'fa fa-fw fa-wrench',
				'order' => $order++
				),'left');
		}
	}

	private function _update_permissions()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	private function init_permissions()
	{
		return array(

			'Admin.Contract' 		=> array(
				'description' 		=> 'Quản lý hợp đồng',
				'actions' 			=> array('view','add','edit','delete','update','manage','mdeparment','mgroup')),

			'Contract.Revenue'		=> array(
				'description'		=> 'Quản lý doanh thu',
				'actions' 			=> array('access','add','delete','update','manage','mdeparment','mgroup')),

			'Contract.Receipt' 		=> array(
				'description' 		=> 'Quản lý thanh toán',
				'actions' 			=> array('access','add','delete','update','manage','mdeparment','mgroup')),

			'Contract.Approval' 	=> array(
				'description' 		=> 'Danh sách hợp đồng chờ duyệt',
				'actions' 			=> array('access','mdeparment','mgroup')),

			'Invoices.Queue_waiting'=> array(
				'description' 		=> 'Danh sách hóa đơn chờ thu',
				'actions' 			=> array('access')),

			'Contract.Start_service'=> array(
				'description' 		=> 'Chạy dịch vụ',
				'actions' 			=> array('manage')),

			'Contract.Stop_contract'=> array(
				'description' 		=> 'Kết thúc hợp đồng',
				'actions' 			=> array('manage')),

			'Contract.Remove' 		=> array(
				'description' 		=> 'Hủy hợp đồng',
				'actions' 			=> array('manage')),

			'Contract.Lock' 		=> array(
				'description' 		=> 'Khóa chỉnh sửa hợp đồng',
				'actions' 			=> array('manage')),
			
            'Contract.manipulation_lock' 		=> array(
                'description' 		=> 'Khóa thao tác hợp đồng',
                'actions' 			=> array('access','create','delete','update','manage','mdeparment','mgroup')),

			'Contract.notification_payment' => array(
					'description' 	=> 'Gửi mail cảm ơn cho khách hàng và nội bộ công ty',
					'actions'		=> array('manage')),

			'Contract.setting' => array(
					'description' 	=> 'Cấu hình',
					'actions'		=> array('access','add','delete','update','manage','mdeparment','mgroup')),

			'Contract.webdesign' => array(
				'description' 	=> 'Quyền quản trị webdesign',
				'actions' 		=> [ 'access' ]
			),

			'Contract.oneweb' => array(
				'description' 	=> 'Quyền quản trị oneweb',
				'actions' 		=> [ 'access' ]
			),

			'Contract.weboptimize' => array(
				'description' 	=> 'Quyền quản trị weboptimize',
				'actions' 		=> [ 'access' ]
			),

			'Contract.banner' => array(
				'description' 	=> 'Quyền quản trị banner',
				'actions' 		=> [ 'access' ]
			),

			'Contract.domain' => array(
				'description' 	=> 'Quyền quản trị domain',
				'actions' 		=> [ 'access' ]
			),

			'Contract.hosting' => array(
				'description' 	=> 'Quyền quản trị hosting',
				'actions' 		=> [ 'access' ]
			),
			
			'Contract.courseads' => array(
				'description' 	=> 'Quyền quản trị courseads',
				'actions' 		=> [ 'access' ]
			),
			
			'Contract.googleads' => array(
				'description' 	=> 'Quyền quản trị googleads',
				'actions' 		=> [ 'access' ]
			),
			
			'Contract.facebookads' => array(
				'description' 	=> 'Quyền quản trị facebookads',
				'actions' 		=> [ 'access' ]
			),
			
			'Contract.tiktokads' => array(
				'description' 	=> 'Quyền quản trị tiktokads',
				'actions' 		=> [ 'access' ]
			),

            'Contract.zaloads' => array(
				'description' 	=> 'Quyền quản trị zaloads',
				'actions' 		=> [ 'access' ]
			),
			
			'Contract.onead' => array(
				'description' 	=> 'Quyền quản trị onead',
				'actions' 		=> [ 'access' ]
			),
			
			'Contract.fbcontent' => array(
				'description' 	=> 'Quyền quản trị fbcontent',
				'actions' 		=> [ 'access' ]
			),

			'Admin.Report' => array(
				'description' 	=> 'Quyền truy cập báo cáo',
				'actions' 		=> [ 'access']
			),

			'Reports.nonrenewal' => array(
				'description' 	=> 'Quyền truy cập báo cáo kết thúc không gia hạn',
				'actions' 		=> [ 'access', 'manage', 'mdeparment', 'mgroup' ]
			),

			'Reports.adspending' => array(
				'description' 	=> 'Quyền truy cập báo cáo hợp đồng tạm ngưng chạy ads',
				'actions' 		=> [ 'access', 'manage', 'mdeparment', 'mgroup' ]
			),
            'Reports.total'   => array(
				'description' => 'Quyền truy cập báo cáo tổng hợp hợp đồng',
				'actions'     => [ 'access', 'manage', 'mdeparment', 'mgroup' ]
			),
            'Reports.spend'   => array(
				'description' => 'Quyền truy cập báo cáo spend',
				'actions'     => [ 'access', 'manage', 'mdeparment', 'mgroup' ]
			),
            'Reports.revenue' => array(
				'description' => 'Quyền truy cập báo cáo doanh thu',
				'actions'     => [ 'access', 'manage', 'mdeparment', 'mgroup' ]
			),
            'Reports.over_budget' => array(
				'description'     => 'Quyền truy cập báo cáo cảnh báo hợp đồng chạy lố',
				'actions'         => [ 'access', 'manage', 'mdeparment', 'mgroup' ]
			),
            'Reports.receipt_caution' => array(
				'description'         => 'Quyền truy cập báo cáo cảnh báo hợp đồng có bảo lãnh',
				'actions'             => [ 'access', 'manage', 'mdeparment', 'mgroup' ]
			),

			'Admin.adwarehouse' => array(
				'description'         => 'Quyền truy cập tài khoản trong kho',
				'actions'             => [ 'access', 'manage', 'mdeparment', 'mgroup' ]
			),
		);
	}

	public function title()
	{
		return 'Contract';
	}

	public function author()
	{
		return 'Thonh';
	}

	public function version()
	{
		return '0.1';
	}

	public function description()
	{
		return 'Contract';
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}