<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Example extends Admin_Controller {

	public $model = '';

	function __construct()
	{
		parent::__construct();
		restrict('Admin.Example');
		$this->load->model('example_m');
	}


	public function index()
	{
		$data = array();
		parent::render($data);
	}
}