<?php
if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Filtering_m extends Base_Model {

	function __construct() 
	{
		parent::__construct();

		$this->load->model('tracking/visitor_sessions_m');
		// $this->config->load('tracking/api_constants');
	}


	/**
	 * Check IP by restrict creativeID
	 *
	 * @param      string   $ip         The ip address
	 * @param      bigint   $creative   The creative
	 * @param      int  	$max        The maximum
	 * @param      int   	$durations  The durations
	 *
	 * @return     boolean  return TRUE if IP is valid , otherwise return FALSE
	 */
	public function adCheck($ip,$creative,$max,$durations)
	{
		$result = $this->visitor_sessions_m
		->select('id')
		->where('ip',$ip)
		->where('created_on <=',time())
		->where('created_on >=',strtotime("-{$durations} second"))
		->where('creative',trim($creative))
		->count_by();
		
		return $result <= $max;
	}


	/**
	 * Check IP by restrict Adgroup ID
	 *
	 * @param      string   $ip         The ip address
	 * @param      <type>   $adgroupid  The adgroupid
	 * @param      integer  $max        The maximum
	 * @param      <type>   $durations  The durations
	 *
	 * @return     boolean  ( description_of_the_return_value )
	 */
	public function adgroupCheck($ip,$adgroupid,$max,$durations)
	{
		$result = $this->visitor_sessions_m
		->select('id')
		->where('ip',$ip)
		->where('created_on <=',time())
		->where('created_on >=',strtotime("-{$durations} second"))
		->where('adgroupid',trim($adgroupid))
		->count_by();

		return $result <= $max;
	}


	/**
	 * Check IP by restrict Campaign ID
	 *
	 * @param      string   $ip             The ip address
	 * @param      string   $campaignid  The campaig cl snid
	 * @param      integer  $max            The maximum
	 * @param      <type>   $durations      The durations
	 *
	 * @return     boolean  ( description_of_the_return_value )
	 */
	public function adcampaignCheck($ip,$campaignid,$max,$durations)
	{
		$result = $this->visitor_sessions_m
		->select('id')
		->where('ip',$ip)
		->where('created_on <=',time())
		->where('created_on >=',strtotime("-{$durations} second"))
		->where('campaignid',trim($campaignid))
		->count_by();

		return $result <= $max;
	}


	/**
	 * Check IP by restrict Octet
	 *
	 * @param      string   $tracking_code  The tracking code
	 * @param      string   $ip             The IP address
	 * @param      integer  $octet          The number group
	 * @param      integer  $max            The maximum
	 * @param      integer  $durations      The durations
	 *
	 * @return     boolean  ( description_of_the_return_value )
	 */
	public function ipOctetsCheck($tracking_code = '',$ip = '',$octet = 2,$max,$durations)
	{
		$ip_prefix = implode('.', array_slice(explode('.', $ip),0, $octet));
		if(empty($ip_prefix)) return TRUE;

		$result = $this->visitor_sessions_m
		->select("substring_index(ip,'.',{$octet}) as ip_series")
		->select('count(ip) as num')
		->like('ip',"{$ip_prefix}.",'after')
		->where('created_on <=',time())
		->where('created_on >=',strtotime("-{$durations} seconds"))
		->where('tracking_code',$tracking_code)
		->group_by("substring_index(ip,'.',{$octet})")
		->having('count(ip) >=',$max)
		->get_by();

		if( ! $result) return TRUE;

		return $result->num <= $max;
	}


	/**
	 * { function_description }
	 *
	 * @param      string  $browser  The browser
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function oldBrowserCheck($browser = '')
	{
		$this->load->config('blacklist_user_agents');
		$browsers = $this->config->item('browsers','blacklist_agents');
		return !in_array(trim($browser), array_keys($browsers));
	}


	/**
	 * Check if Client browser in Blacklist
	 *
	 * @param      string  $browser  The browser
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function riskBrowserCheck($browser = '')
	{
		$this->load->config('blacklist_user_agents');
		$browsers = $this->config->item('browsers','blacklist_agents');
		return !in_array(trim($browser), array_keys($browsers));
	}

	/**
	 * Check OS risk
	 *
	 * @param      string  $platform  The platform
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function riskPlatformCheck($platform = '')
	{
		$this->load->config('blacklist_user_agents');
		$platforms = $this->config->item('platforms','blacklist_agents');
		return !in_array(trim($platform), array_keys($platforms));
	}

	public function getRuleDefined($campaign_id = 0)
	{
		$cache_key = "modules/filtering/rule-{$campaign_id}";
		$rule = $this->scache->get($cache_key);
		if($rule) return $rule;

		$ruleResponse = Requests::get('http://localhost:9090/erp.webdoctor.vn/api/googleads/getAdcampaignRule/'.trim($campaign_id));
		$ruleResponse = json_decode($ruleResponse->body);
		$rule = $ruleResponse->data;

		$this->scache->write($rule,$cache_key);
		return $rule;
	}
}
/* End of file Filtering.php */
/* Location: ./application/modules/googleads/models/Filtering.php */