<?php
class Filtering_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name(){
		
		return 'Filtering';
	}

	public function init(){	

		if(has_permission('Admin.Filtering') && is_module_active('filtering'))
		{

		}
	}

	public function title()
	{
		return 'Filtering';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '0.1';
	}

	public function description()
	{
		return 'Filtering';
	}
	private function init_permissions()
	{
		$permissions = array();
		$permissions['Admin.Filtering'] = array(
			'description' => 'Quản lý Filtering',
			'actions' => array('view','add','edit','delete','update'));

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}