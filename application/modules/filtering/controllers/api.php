<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends API_Controller {

	function __construct()
	{
		parent::__construct();

		defined('IPV4_NUM_OCTETS') OR define('IPV4_NUM_OCTETS',4);
	}

	/**
	 * Check restrict of vistior sessions and detect risk
	 *
	 * @param      integer  $visitor_sess  The visitor sess
	 *
	 * @return     json The result
	 */
	public function detect($visitor_sess = 0)
	{
		$response = array('status' => FALSE, 'msg' => 'Failure', 'data' => array());

		$this->load->model('tracking/visitor_sessions_m');
		$visitor = $this->visitor_sessions_m->get($visitor_sess);
		if( ! $visitor)
		{
			$response['msg'] = 'The session is undefined or unloaded';
			return parent::renderJson($response);
		}


		$this->load->model('filtering_m');
		$rule = $this->filtering_m->getRuleDefined($visitor->campaignid);
		if(empty($rule))
		{
			$response['msg'] = 'The rules is undefined or unloaded';
			return parent::renderJson($response);
		}

		/**
		 * restrict IP by ads's rule
		 */
		if($rule->ad->active 
			&& !$this->filtering_m->adCheck($visitor->ip,$visitor->creative,$rule->ad->max_clicks,$rule->ad->durations))
		{
			$response['msg'] 	= 'blocked successfuly';
			$response['status'] = TRUE;
			$response['data'] 	= array(
				'risk' 	 		=> 10,
				'notify' 		=> $this->pushRisk($visitor->ip,$visitor->tracking_code),
				'ip' 	 		=> $visitor->ip,
				'description' 	=> 'AD restricted !!!'
			);

			return parent::renderJson($response);
		}


		/**
		 * restrict IP by adgroup's rule
		 */
		if($rule->adgroup->active 
			&& !$this->filtering_m->adgroupCheck($visitor->ip,$visitor->creative,$rule->adgroup->max_clicks,$rule->adgroup->durations))
		{
			$response['msg'] 	= 'blocked successfuly';
			$response['status'] = TRUE;
			$response['data'] 	= array(
				'risk' 	 		=> 10,
				'notify' 		=> $this->pushRisk($visitor->ip,$visitor->tracking_code),
				'ip' 	 		=> $visitor->ip,
				'description' 	=> 'ADGROUP restricted !!!'
			);

			return parent::renderJson($response);
		}


		/**
		 * restrict IP by campaign's rule
		 */
		if($rule->adcampaign->active 
			&& !$this->filtering_m->adcampaignCheck($visitor->ip,$visitor->creative,$rule->adcampaign->max_clicks,$rule->adcampaign->durations))
		{
			$response['msg'] 	= 'blocked successfuly';
				$response['status'] = TRUE;
				$response['data'] 	= array(
					'risk' 	 		=> 10,
					'notify' 		=> $this->pushRisk($visitor->ip,$visitor->tracking_code),
					'ip' 	 		=> $visitor->ip,
					'description' 	=> 'IP by campaign restricted !!!'
				);

				return parent::renderJson($response);
		}


		/**
		 * restrict IP by ranges 2 groups 
		 */
		if($rule->ip_series_p2->active 
			&& $this->filtering_m->ipOctetsCheck($visitor->tracking_code,2,$rule->ip_series_p2->max_clicks,$rule->ip_series_p2->durations))
		{
			$ip_octets = explode('.', $visitor->ip);
			if(count($ip_octets) == IPV4_NUM_OCTETS)
			{
				$visitor->ip = implode('.', array_slice($ip_octets,0,2)).'.0.0/16';
			}

			$response['msg'] 	= 'blocked successfuly';
			$response['status'] = TRUE;
			$response['data'] 	= array(
				'risk' 	 		=> 10,
				'notify' 		=> $this->pushRisk($visitor->ip,$visitor->tracking_code),
				'ip' 	 		=> $visitor->ip,
				'description' 	=> 'IP by ranges 2 restricted !!!'
			);

			return parent::renderJson($response);
		}


		/**
		 * restrict IP by ranges 3 groups 
		 */
		if($rule->ip_series_p3->active 
			&& $this->filtering_m->ipOctetsCheck($visitor->tracking_code,3,$rule->ip_series_p3->max_clicks,$rule->ip_series_p3->durations))
		{
			$ip_octets = explode('.', $visitor->ip);
			if(count($ip_octets) == IPV4_NUM_OCTETS)
			{
				$visitor->ip = implode('.', array_slice($ip_octets,0,3)).'.0/24';
			}

			$response['msg'] 	= 'blocked successfuly';
			$response['status'] = TRUE;
			$response['data'] 	= array(
				'risk' 	 		=> 10,
				'notify' 		=> $this->pushRisk($visitor->ip,$visitor->tracking_code),
				'ip' 	 		=> $visitor->ip,
				'description' 	=> 'IP by ranges 3 restricted !!!'
			);

			return parent::renderJson($response);
		}


		/**
		 * restrict IP by BEHAVIOR
		 */
		if($rule->adcampaign->active 
			&& $this->filtering_m->ipOctetsCheck('',3,$rule->adcampaign->max_clicks,$rule->adcampaign->durations))
		{
			$response['msg'] 	= 'blocked successfuly';
			$response['status'] = TRUE;
			$response['data'] 	= array(
				'risk' 	 		=> 10,
				'notify' 		=> $this->pushRisk($visitor->ip,$visitor->tracking_code),
				'ip' 	 		=> $visitor->ip,
				'description' 	=> 'IP by BEHAVIOR restricted !!!'
			);

			return parent::renderJson($response);
		}


		if($rule->devices->active)
		{
			/**
			 * restrict IP by devices
			 */
			if($rule->devices->browser_olds->active 
				&& $this->filtering_m->oldBrowserCheck($visitor->browser))
			{
				$response['msg'] 	= 'blocked successfuly';
				$response['status'] = TRUE;
				$response['data'] 	= array(
					'risk' 	 		=> 10,
					'notify' 		=> $this->pushRisk($visitor->ip,$visitor->tracking_code),
					'ip' 	 		=> $visitor->ip,
					'description' 	=> 'Browser invalid restricted !!!'
				);

				return parent::renderJson($response);
			}


			/**
			 * restrict IP by devices
			 */
			if($rule->devices->browser_undetected->active 
				&& $this->filtering_m->riskBrowserCheck($visitor->browser))
			{
				$response['msg'] 	= 'blocked successfuly';
				$response['status'] = TRUE;
				$response['data'] 	= array(
					'risk' 	 		=> 10,
					'notify' 		=> $this->pushRisk($visitor->ip,$visitor->tracking_code),
					'ip' 	 		=> $visitor->ip,
					'description' 	=> 'Browser undetected restricted !!!'
				);

				return parent::renderJson($response);
			}


			/**
			 * restrict IP by devices
			 */
			if($rule->devices->os_undetected->active 
				&& $this->filtering_m->riskPlatformCheck($visitor->platform))
			{
				$response['msg'] 	= 'blocked successfuly';
				$response['status'] = TRUE;
				$response['data'] 	= array(
					'risk' 	 		=> 10,
					'notify' 		=> $this->pushRisk($visitor->ip,$visitor->tracking_code),
					'ip' 	 		=> $visitor->ip,
					'description' 	=> 'Platform restricted !!!'
				);

				return parent::renderJson($response);
			}
		}

		$response['msg'] 	= 'blocked successfuly';
		$response['status'] = TRUE;
		$response['data'] 	= array(
			'risk' 	 		=> 0,
			'notify' 		=> FALSE,
			'ip' 	 		=> $visitor->ip,
			'description' 	=> 'Low Risk'
		);

		return parent::renderJson($response);
	}
	

	/**
	 * Pushes a risk to ERP back-end.
	 *
	 * @param      <type>  $ip             { parameter_description }
	 * @param      string  $tracking_code  The tracking code
	 *
	 * @return     boolean  IF TRUE return TRUE , othewise return FALSE;
	 */
	protected function pushRisk($ip,$tracking_code = '')
	{
		return TRUE;

		$ruleApiUrl = base_url("googleads/api/ipBlockListener/{$ip}/{$tracking_code}");
		$ruleResponse = Requests::post($ruleApiUrl);
		$ruleResponse = json_decode($ruleResponse->body);
		$rule = $ruleResponse->data;

		/* googleads/api/ipBlockListener */
	}
}
/* End of file Api.php */
/* Location: ./application/modules/filtering/controllers/Api.php */