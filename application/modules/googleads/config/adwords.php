<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

$config['date_type'] = array('CUSTOM_DATE','LAST_7_DAYS','LAST_MONTH','ALL_TIME','LAST_14_DAYS','LAST_30_DAYS','YESTERDAY');

$config['report_type'] = array(
	'CAMPAIGN_PERFORMANCE_REPORT',
	'ADGROUP_PERFORMANCE_REPORT',
	'CRITERIA_PERFORMANCE_REPORT',
	'GEO_PERFORMANCE_REPORT',
	'ACCOUNT_PERFORMANCE_REPORT'
	);

$config['selector'] = array(
	
	'ACCOUNT_PERFORMANCE_REPORT' => array(
		'AccountCurrencyCode',
		'AccountDescriptiveName',
		'Date',
		'Impressions',
		'Clicks',
		'InvalidClicks',
		'AverageCpc',
		'Cost',
		'Conversions',
		'AllConversions',
		'AdNetworkType1'
	),

	'CAMPAIGN_PERFORMANCE_REPORT' => array(
		'Date',
		'CampaignId',
		'CampaignStatus',
		'CampaignName',
		'Impressions',
		'Clicks',
		'InvalidClicks',
		'Ctr',
		'AverageCpc',
		'AverageCpm',
		'Cost',
		
		'AveragePosition', // removed
		'AbsoluteTopImpressionPercentage', //The percent of your ad impressions that are shown as the very first ad above the organic search results (imprAbsTop)
		'TopImpressionPercentage', //The percent of your ad impressions that are shown anywhere above the organic search results (imprTop)

		'AccountCurrencyCode',
		'AdvertisingChannelType',
		'Conversions',
		'AllConversions',
		'AdNetworkType1',
		// 'Device'
	),
	
	'ADGROUP_PERFORMANCE_REPORT' => array(
		'Date',
		'CampaignId',
		'CampaignName',
		'AdGroupId',
		'AdGroupName',
		'AdGroupStatus',
		'Impressions',
		'Clicks',
		'Ctr',
		'AverageCpc',
		'AverageCpm',
		'Cost',
		
		'AveragePosition',
		'AbsoluteTopImpressionPercentage', //The percent of your ad impressions that are shown as the very first ad above the organic search results (imprAbsTop)
		'TopImpressionPercentage', //The percent of your ad impressions that are shown anywhere above the organic search results (imprTop)

		'AccountCurrencyCode',
		'Conversions',
		'AllConversions',
		'AdNetworkType1',
		// 'Device'
	),

	'KEYWORDS_PERFORMANCE_REPORT' => array(
		'AccountCurrencyCode',
		'AccountDescriptiveName',
		'AdGroupId',
		'AdGroupName',
		'AdGroupStatus',
		'AdNetworkType1',
		'ApprovalStatus',
		'AverageCost',
		'AverageCpc',
		
		'AveragePosition',
		'AbsoluteTopImpressionPercentage', //The percent of your ad impressions that are shown as the very first ad above the organic search results (imprAbsTop)
		'TopImpressionPercentage', //The percent of your ad impressions that are shown anywhere above the organic search results (imprTop)

		'AverageTimeOnSite',
		'BounceRate',
		'CampaignId',
		'CampaignName',
		'CampaignStatus',
		'Clicks',
		'Cost',
		'Criteria',
		'Ctr',
		'Date',
		'HasQualityScore',
		'Id',
		'Impressions',
		'IsNegative',
		'KeywordMatchType',
		'QualityScore',
		'Status',
		'FirstPageCpc',
		'TopOfPageCpc',
		'Conversions'
	),

	'GEO_PERFORMANCE_REPORT' => array(
		'AccountCurrencyCode',
		'AccountDescriptiveName',
		'AdFormat',
		'AdGroupId',
		'AdGroupName',
		'AdGroupStatus',
		'AdNetworkType1',
		'AllConversionRate',
		'AllConversions',
		'AllConversionValue',
		'AverageCost',
		'AverageCpc',
		'AverageCpm',
		'AverageCpv',
		'CampaignId',
		'CampaignName',
		'CampaignStatus',
		'CityCriteriaId',
		'Clicks',
		'ConversionRate',
		'Conversions',
		'ConversionValue',
		'Cost',
		'CostPerAllConversion',
		'CostPerConversion',
		'CountryCriteriaId',
		'CrossDeviceConversions',
		'Ctr',
		'CustomerDescriptiveName',
		'Date',
		// 'Device',
		'ExternalCustomerId',
		'Impressions',
		'InteractionRate',
		'Interactions',
		'IsTargetingLocation',
		'LocationType',
		'MetroCriteriaId',
		'MostSpecificCriteriaId',
		'Quarter',
		'RegionCriteriaId',
		'ValuePerAllConversion',
		'ValuePerConversion',
		'VideoViewRate',
		'VideoViews',
		'ViewThroughConversions'
	),

	'CRITERIA_PERFORMANCE_REPORT' => array(
		'Date',
		'CampaignId',
		'CampaignName',
		'AdGroupId',
		'AdGroupName',
		'AdGroupStatus',
		'Impressions',
		'Clicks',
		'Ctr',
		'AverageCpc',
		'AverageCpm',
		'Cost',
		'Id',
		'Criteria',
		
		'AveragePosition',
		'AbsoluteTopImpressionPercentage', //The percent of your ad impressions that are shown as the very first ad above the organic search results (imprAbsTop)
		'TopImpressionPercentage', //The percent of your ad impressions that are shown anywhere above the organic search results (imprTop)

		'AccountCurrencyCode',
		'AdNetworkType1')
);