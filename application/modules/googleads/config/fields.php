<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['datasource'] = array(
	'columns' => array(

		'term.term_id' => array(
			'name' => 'term.term_id',
			'label' => '#ID',
			'set_select' => TRUE,
			'title' => '#ID',
		),

		'term.term_name' => array(
			'name' => 'term.term_name',
			'label' => 'Website',
			'set_select' => TRUE,
			'title' => 'Website',
		),

		'term.term_status' => array(
			'name' => 'term.term_status',
			'label' => 'Trạng thái dịch vụ',
			'set_select' => TRUE,
			'title' => 'T.Thái',
		),

		'contract_code' => array(
			'name' => 'contract_code',
			'label' => 'Mã Hợp đồng',
			'set_select' => FALSE,
			'title' => 'Mã Hợp đồng',
			'set_order' => FALSE
		),

		'created_on' => array(
			'name' => 'created_on',
			'label' => 'Ngày tạo hợp đồng',
			'set_select' => FALSE,
			'title' => 'Ngày tạo',
			'set_order' => FALSE
		),	

		'created_by' => array(
			'name' => 'created_by',
			'label' => 'Người tạo hợp đồng',
			'set_select' => FALSE,
			'title' => 'Người tạo',
			'set_order' => FALSE
		),

		'verified_on' => array(
			'name' => 'verified_on',
			'label' => 'Người duyệt hợp đồng',
			'set_select' => FALSE,
			'title' => 'Duyệt bởi',
			'set_order' => FALSE
		),

		'contract_begin' => array(
			'name' => 'contract_begin',
			'label' => 'Ngày bắt đầu hợp đồng',
			'set_select' => FALSE,
			'title' => 'Ngày bắt đầu HĐ',
			'set_order' => FALSE
		),

		'contract_end' => array(
			'name' => 'contract_end',
			'label' => 'Ngày kết thúc hợp đồng',
			'set_select' => FALSE,
			'title' => 'Ngày kết thúc HĐ',
			'set_order' => FALSE
		),

		'contract_daterange' => array(
			'name' => 'contract_daterange',
			'label' => 'Thời gian Hợp đồng',
			'set_select' => FALSE,
			'title' => 'T/G HĐ',
			'set_order' => FALSE
		),

		'start_service_time' => array(
			'name' => 'start_service_time',
			'label' => 'Ngày kích hoạt dịch vụ',
			'set_select' => FALSE,
			'title' => 'Ng.Kích hoạt',
			'set_order' => FALSE
		),

		'staff_business' => array(
			'name' => 'staff_business',
			'label' => 'Kinh doanh phụ trách',
			'set_select' => FALSE,
			'title' => 'NVKD',
			'set_order' => TRUE
		),

		'term.term_type' => array(
			'name' => 'term.term_type',
			'label' => 'Loại dịch vụ',
			'set_select' => FALSE,
			'title' => 'Dịch vụ',
			'set_order' => TRUE
		),

		'contract_value' => array(
			'name' => 'contract_value',
			'label' => 'Giá trị hợp đồng (chưa thuế)',
			'set_select' => FALSE,
			'title' => 'GTHĐ (-VAT)',
			'set_order' => TRUE
		),

		'is_first_contract' => array(
			'name' => 'is_first_contract',
			'label' => 'Ký mới hoặc Tái ký',
			'set_select' => FALSE,
			'title' => 'Ký mới',
			'set_order' => TRUE
		),	

        'latest_spend_date' => array(
            'name' => 'latest_spend_date',
            'label' => 'Ngày ghi nhận spend mới nhất',
            'set_select' => FALSE,
            'title' => 'Ngày ghi nhận spend mới nhất',
            'type' => 'timestamp',
            'set_order' => FALSE
        ),

        'latest_spend_days' => array(
            'name' => 'latest_spend_days',
            'label' => 'Số ngày không ghi nhận spend',
            'set_select' => FALSE,
            'title' => 'Số ngày không ghi nhận spend',
            'type' => 'number',
            'set_order' => FALSE
        ),
	),
	'default_columns' => array(
		'term.term_id',
		'term.term_name',
		'customer_code',
		'term.term_status',
		'contract_code',
		'created_on',
		'created_by',
		'verified_on',
		'contract_begin',
		'contract_end',
		'contract_daterange',
		'start_service_time',
		'staff_business',
		'term.term_type',
		'contract_value',
		'is_first_contract'
	)
);