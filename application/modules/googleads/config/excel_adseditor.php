<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config['adseditor'] = array(
	'EXTRACT_RATE' 	=> 1,
    'PHRASE_RATE' 	=> 0.9,
    'BROAD_RATE' 	=> 0.8,
    'ADGROUP_DEFAULT_CPC' => 1,
    'ADGROUP_DEFAULT_CPM' => 1
);