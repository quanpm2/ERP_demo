<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['googleadsDeveloperToken'] = getenv('GOOGLEADS_DEVELOPER_TOKEN');

$config['mcm'] = array(
    'authorizationUri' 	=> 'https://accounts.google.com/o/oauth2/v2/auth',
    'clientId' 			=> getenv('GOOGLEADS_CLIENT_ID') ?: '464291558600-q7f4mble6to95socccl6bglo03i5lqge.apps.googleusercontent.com',
    'clientSecret' 		=> getenv('GOOGLEADS_CLIENT_SECRET') ?: 'lqkUHf2sruzQ2Cba9Y0Y6eWw',
    'scope'				=> ['https://www.googleapis.com/auth/adwords','https://www.googleapis.com/auth/userinfo.email'],
    'userAgent'			=> 'example.com:ReportDownloader:V3.2'
);