<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$config['frauds_rules'] = array(
	'ad' => array(
		'active' => TRUE,
		'max_clicks' => 5,
		'durations' => 1*60, // 1 minutes
	),
	'adgroup' => array(
		'active' => TRUE,
		'max_clicks' => 20,
		'durations' => 1*60, // 1 minutes
	),
	'adcampaign' => array(
		'active' => TRUE,
		'max_clicks' => 100,
		'durations' => 1*60, // 1 minutes
	),
	'ip_series_p3' => array(
		'active' => TRUE,
		'max_clicks' => 100,
		'durations' => 1*60, // 1 minutes
	),
	'ip_series_p2' => array(
		'active' => TRUE,
		'max_clicks' => 100,
		'durations' => 1*60, // 1 minutes
	),
	'behavior' => array(
		'active' => TRUE,
		'min_time_on_site' => 1*60, // 1 minutes
		'max_sessions' => 10,
		'durations' => 60*60 // 1 minutes
	),
	'devices' => array(
		'active' => TRUE,
		'browser_olds' => TRUE,
		'browser_undetected' => TRUE,
		'mouse_undetected' => TRUE,
		'screen_undetected' => TRUE,
		'os_undetected' => TRUE
	),
	'recovery' => 'noexpired'
);