<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['ggapps'] = array(

	'production' => array(
		'label' 		=> 'API Adsplus',
		'name'			=> 'production',
		'credential' 	=> array(
			'app_id' 		=> getenv('GG_APP_ID') ?: '495292314010324',
			'app_secret' 	=> getenv('GG_APP_SECRET') ?: 'b27239966b8d54c4b2e6aecd1a288cf4',
			'access_token' 	=> getenv('GG_ACCESS_TOKEN') ?: 'EAAHCdzpvwtQBAOEJ8x8y1cxElzg2u4POfLWPZAbqIZCZC3GLxnf8dUR9G8FwkdFTuclHScpKcBFB432nM5Na7Aaj5lELVs1zLoS938b5qkg8Lb3mTt6FJyMLm68KNY3XnURJ5yWz2ZAVkc5cswjTv95tdYepJFcdzUDj502WTQZDZD',
			'graph_version' => getenv('GG_GRAPH_VERSION') ?: 'v3.2'
		)
	),

	'development' => array(
		'label' 		=> 'API Agency Adsplus',
		'name'			=> 'development',
		'credential' 	=> array(
			'app_id' 		=> getenv('GG_DEVELOPMENT_APP_ID') ?: null,
			'app_secret' 	=> getenv('GG_DEVELOPMENT_APP_SECRET') ?: null,
			'access_token' 	=> getenv('GG_DEVELOPMENT_ACCESS_TOKEN') ?: null,
			'graph_version' => getenv('GG_DEVELOPMENT_GRAPH_VERSION') ?: NULL
		)
	),

	'default' => getenv('GG_APP_ENV') ?: 'production'
);