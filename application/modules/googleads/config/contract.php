<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

$config['typeOfServices'] = [
	'items' => [
		'cpc_type' 			=> 'CPC',
		'behaft_rent'  		=> 'Thu chi hộ - Thuê TK',
		'behaft_default' 	=> 'Thu chi hộ - QLTK',
		'fullvat_rent'		=> 'Full VAT - Thuê TK',
		'fullvat_default' 	=> 'Full VAT',
		'unspecified'		=> 'Chưa xác định'
	],
	'default' => 'unspecified'
];

/* Cấu hình phí dịch vụ thanh toán 100% lần đầu hoặc thanh toán theo đợt */
$config['service_fee_payment_type'] = array(
	'full' => 'Thanh toán 100% phí dịch trong đợt đầu',
	'devide' => 'Thu phí dịch vụ theo mỗi đợt thanh toán',
    'range' => 'Thu phí dịch vụ theo mức ngân sách',
);

$config['contract_budget_payment_types'] = array(
	'enums' => [
		'normal' 	=> 'ADSPLUS thanh toán theo hợp dồng',
		'customer' 	=> 'Khách hàng tự thanh toán',
	],
	'default' => 'normal'
);

$config['contract_budget_customer_payment_types'] = array(
	'enums' => [
		'normal' 	=> 'Khách tự thanh toán',
		'behalf' 	=> 'Thu & chi hộ',
	],
	'default'	=> 'normal'
);

$config['service_fee_rule'] = array(

	'minimal' 	=> [
		'min' => 0, // 12.000.000
		'max' => (12000000 - 1), // 50.000.000	
		'amount' => 2400000, 		
		'rate' => FALSE
	],
	'basic' 	=> [
		'min' => 12000000, // 12.000.000
		'max' => (50000000 - 1), // 50.000.000	
		'amount' => FALSE, 		
		'rate' => 0.2
	],
	'normal' 	=> [
		'min' => 50000000,
		'max' => (100000000 - 1),	
		'amount' => FALSE, 		
		'rate' => 0.17
	],
	'plus' 		=> [
		'min' => 100000000,
		'max' => (300000000 - 1),	
		'amount' => FALSE, 		
		'rate' => 0.15
	],
	'extra' 	=> [
		'min' => 300000000,
		'max' => PHP_INT_MAX, 		
		'amount' => FALSE, 		
		'rate' => 0.12
	],
	'default' 	=> 'normal'
);

$config['serviceFeeRuleEnums'] = array(
	'normal' => [
		'name' => 'normal',
		'unit' => 'normal',
		'rule' => $config['service_fee_rule']
	],
	'rent' => [
		'name' => 'rent',
		'unit' => 'month', 
		'rule' => [
			'minimal' 	=> [
				'min' => 0, // 12.000.000
				'max' => (50000000 - 1), // 50.000.000	
				'amount' => 5000000, 		
				'rate' => FALSE
			],
			'basic' 	=> [
				'min' => 50000000, // 12.000.000
				'max' => (100000000 - 1), // 50.000.000	
				'amount' => FALSE, 		
				'rate' => 0.1
			],
			'normal' 	=> [
				'min' => 100000000,
				'max' => (300000000 - 1),	
				'amount' => FALSE, 		
				'rate' => 0.09
			],
			'plus' 		=> [
				'min' => 300000000,
				'max' => (500000000 - 1),
				'amount' => FALSE, 		
				'rate' => 0.08
			],
			'extra' 	=> [
				'min' => 500000000,
				'max' => (1000000000 - 1),
				'amount' => FALSE, 		
				'rate' => 0.07
			],
			'extra_plus' 	=> [
				'min' => 1000000000,
				'max' => (2000000000 - 1),
				'amount' => FALSE, 		
				'rate' => 0.06
			],
			'max' 	=> [
				'min' => 2000000000,
				'max' => PHP_INT_MAX, 		
				'amount' => FALSE, 		
				'rate' => 0.05
			],
			'default' 	=> 'normal'
		]
	]
);

$config['printable-network-rules'] = array(

    'search' 		=> array(
        'title'		=> 'Quảng cáo trên trang tìm kiếm của Google',
        'content'	=> '<ul>
		<li> Quảng cáo của Bên A sẽ xuất hiện tại các vị trí cho phép đặt quảng cáo của Google. Trang web mà quảng cáo sẽ xuất hiện là www.google.com.vn/google.com, cho phép toàn bộ người tìm kiếm trên trang web đó tìm thấy quảng cáo khi dùng từ khóa tương ứng.</li>
		<li> Bên B sẽ tư vấn và phối hợp với Bên A xây dựng các chiến dịch quảng cáo, danh mục từ khóa, câu quảng cáo (adtext) để chiến dịch quảng cáo đạt hiệu quả cao nhất.</li>
		</ul>'
    ),

    'display' 		=> array(
        'title'		=> 'Quảng cáo trên mạng hiển thị của Google',
        'content'	=> '<ul>
	        <li> Quảng cáo của Bên A sẽ xuất hiện tại các vị trí cho phép đặt quảng cáo trên các website thuộc mạng hiển thị của Google.</li>
	        <li> Bên B sẽ tư vấn và phối hợp với Bên A xây dựng các chiến dịch quảng cáo, hình ảnh và thông điệp để chiến dịch quảng cáo đạt hiệu quả cao nhất nhưng không chịu trách nhiệm thực hiện thiết kế hình ảnh quảng cáo. Trường hợp Bên A có nhu cầu cần thiết kế hình ảnh/ thông điệp quảng cáo thì Bên B sẽ tính phí thiết kế theo bảng giá dịch vụ quy định tại thời điểm yêu cầu. </li>
	        <li> Thông tin của quảng cáo sẽ do Bên A cung cấp và nội dung quảng cáo phải đạt được sự thống nhất của hai bên trước khi thực hiện.</li>
	    </ul>'
    ),

    'facebook' 		=> array(
        'title'		=> 'Quảng Cáo Facebook',
        'content'	=> '<ul>
	        <li> Quảng cáo của Bên A sẽ xuất hiện tại các vị trí cho phép đặt quảng của Facebook.</li>
	        <li> Bên B sẽ tư vấn và phối hợp với Bên A xây dựng các chiến dịch quảng cáo, hình ảnh và thông điệp để chiến dịch quảng cáo đạt hiệu quả cao nhất nhưng không chịu trách nhiệm thực hiện thiết kế hình ảnh quảng cáo. Trường hợp Bên A có nhu cầu cần thiết kế hình ảnh/ thông điệp quảng cáo thì Bên B sẽ tính phí thiết kế theo bảng giá dịch vụ quy định tại thời điểm yêu cầu. </li>
	        <li> Thông tin của quảng cáo sẽ do Bên A cung cấp và nội dung quảng cáo phải đạt được sự thống nhất của hai bên trước khi thực hiện.</li>
	        <li> Bên B sẽ đề xuất với Bên A những giải pháp để tăng cường hiệu quả của chiến dịch quảng cáo.</li>
	    </ul>'
    ),

    'default' => ['search', 'display']
);

$config['metadata_googleads'] = array(
	'service_type',
	'network_type',
	'gdn_contract_cpc',
	'gdn_contract_clicks',
	'service_fee_payment_type',
	'contract_budget',
	'service_fee',
	'exchange_rate_usd_to_vnd',
	'exchange_rate_aud_to_vnd',
	'mcm_client_id',
	'contract_curators',
	'contract_cpc',
	'contract_clicks',
);