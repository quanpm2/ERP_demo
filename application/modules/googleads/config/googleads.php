<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$config['adaccount_status'] = [
	'states' => [
		'UNSPECIFIED' 		=> 'Chưa cấu hình',
		'PENDING_APPROVAL'  => 'Đã cấu hình',
		'APPROVED' 			=> 'Đã kích hoạt',
		'REMOVED' 			=> 'Đã xóa cấu hình',
		'UNKNOWN' 			=> 'Chưa xác định',
	],
	'default' => 'UNSPECIFIED'
];


/* Giờ khả thi thực hiện các tác vụ */
$config['runtime'] = array(5, 9, 11);

/* Khoảng thời gian gửi email báo cáo định kỳ */
$config['email_range_day'] = 7;

/* Khoảng thời gian gửi sms định kỳ */
$config['sms_range_day'] = 1;

/* Loại dịch vụ */
$config['stypes'] = array(

	/* Dịch vụ CPC - Cost Per Click */
	'cpc_type' => array( 'name' => 'cpc_type', 'label' => 'CPC (Cost per Click)' ),

	/* Dịch vụ quản lý tài khoản quảng cáo - thu phí dịch vụ */
	'account_type' => array( 'name' => 'account_type', 'label' => 'Quản lý tài khoản' ),

	/* Dịch vụ mặc định */
	'default' => 'account_type'
);

/* Các Hình thức quảng cáo Google khả dụng */
$config['googleads_service_type'] = array(
	'cpc_type' => 'CPC',
	'account_type' => 'Quản lý tài khoản'
);


$config['googleads'] = array(

	/* Quy định trạng thái dịch vụ đang thực hiện */
	'status' => array('pending' => 'Ngưng hoạt động','publish' => 'Đang hoạt động'),

	/* Quy định số người giám sát tối đa cho một hợp đồng */
	'max_input_curators' => 2,

	/* Quy định xóa log */ 
	'clear_log' => TRUE,

	/* Các Hình thức quảng cáo Google khả dụng */
	'report_type' => $config['googleads_service_type'],

	'default_service_type' => $config['stypes']['default']
);

/* Enums các kênh quảng cáo khả dụng */
$config['network_types'] = array('search' => 'Search','display' => 'GDN');

$config['packages'] = array(
	'service' => array(

		'minimum' 	=> array(
			'label'	=>'Không có dịch vụ tặng kèm',
			'name'	=> 'Gói bình thường',
			'gift'	=> FALSE
		),
		'basic'	=> array(

			'label'	=>'Gói ngân sách QC từ 2 - 6 triệu/tháng',
			'name'	=> 'Gói tiêu chuẩn',
			'gift'	=> array(

				'webdesign' => array(
					'label' => 'Thiết kế website theo mẫu / lần (tối đa 5 trang)',
					'value' => '2500000'
				),

				'content' 	=> array(
					'label' => 'Webdoctor khởi tạo nội dung / lần',
					'value' => '1200000'
				)
			)
		),
		'normal' => array(
			'label'			=> 'Gói ngân sách QC trên 6 triệu/tháng',
			'name'			=> 'Gói chuyên nghiệp',
			'seotraffic'	=> 3000,
			'content' 		=> 30,
			'price' 		=> 6000000,
			'banner' 		=> 6,
			'gift' => array(
				'webdesign' => array(
					'label' => 'Thiết kế website theo mẫu / lần (tối đa 5 trang)',
					'value' => '5500000'
				),
				'content' => array(
					'label' => 'Webdoctor khởi tạo nội dung / lần',
					'value' => '1200000'
				)
			)
		)
	),
	'default' =>'minimum'
);


$columns = array(
	
	'term_name' => array(
		'name' => 'term_name',
		'label' => 'Website',
		'filter' => array(
			'type' => 'string',
		),
		'type' => 'field',
	),

	'term_status' => array(
		'name' => 'term_status',
		'label' => 'Trạng thái',
		'filter' => array(
			'type' => 'select',
		),
		'type' => 'field',
	),

	'staff_advertise' => array(
		'name' => 'staff_advertise',
		'label' => 'Kỹ thuật',
		'filter' => array(
			'type' => 'string',
		),
		'type' => 'meta',
	),

	'staff_business' => array(
		'name' => 'staff_business',
		'label' => 'Nhân viên kinh doanh',
		'filter' => array(
			'type' => 'string',
		),
		'type' => 'meta',
	),

	'expected_end_time' => array(
		'name' => 'expected_end_time',
		'label' => 'Dự kiến(theo ngân sách)',
		'filter' => array(
			'type' => 'daterange',
			'multiple' => TRUE,
		),
		'type' => 'field',
	),

	'payment_percentage' => array(
		'name' => 'payment_percentage',
		'label' => 'Đã thu',
		'filter' => FALSE,
		'type' => 'meta',
	),

	'status_goal' => array(
		'name' => 'status_goal',
		'label' => '% Hoàn thành',
		'filter' => array(
			'type' => 'string',
		),
		'type' => 'meta',
	),

	'real_progress' => array(
		'name' => 'real_progress',
		'label' => 'Tiến độ (theo N.S)',
		'filter' => array(
			'type' => 'string',
		),
		'type' => 'meta',
	),

	'cost_per_day_left' => array(
		'name' => 'cost_per_day_left',
		'label' => 'est.NS/Ngày',
		'filter' => array(
			'type' => 'string',
		),
		'type' => 'meta',
	),

	'service_type' => array(
		'name' => 'service_type',
		'label' => 'Loại',
		'filter' => array(
			'type' => 'select',
			'multiple' => TRUE,
		),
		'type' => 'meta',
	),

	'cid' => array(
		'name' => 'cid',
		'label' => 'Email',
		'filter' => array( 'type' => 'email'),
		'type' => 'field',
	),
);

$config['datasource'] = array(
	'columns' => $columns,
	'default_columns' => ['term_name,term_status,staff_advertise,staff_business,expected_end_time,payment_percentage,status_goal,real_progress,cost_per_day_left,service_type,cid']
);
unset($columns);