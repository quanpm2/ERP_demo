<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td align="center">
      <p style="font-family:Roboto;color:#ffffff;font-size:30px; font-weight:300; margin:5px 0;  text-transform:uppercase;"><?php echo @$title;?></p>
    </td>
  </tr>
  <tr>
    <td align="center">
      <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng: 
        <strong>
          <?php echo get_term_meta_value($term->term_id,'contract_code');?>
        </strong>
      </p>
      <?php
      $start_time_contract = get_term_meta_value($term->term_id,'contract_begin');
      $end_time_contract = get_term_meta_value($term->term_id,'contract_end');
      $time_contract = 0;
      if(!empty($start_time_contract) && !empty($end_time_contract))
      {
        $time_contract = $end_time_contract - $start_time_contract;
      }

      $start_time = get_term_meta_value($term->term_id,'start_service_time');
      $end_time = $start_time + $time_contract;

      $contract_date = my_date($start_time,'d/m/Y').' - '.my_date($end_time,'d/m/Y');
      ?>
      <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Thời gian thực hiện từ: <span style="font-weight:bold"><?php echo $contract_date;?></span> .</p> &nbsp;
    </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">Kính chào Quý khách</p>
      <p>Adsplus.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>
      <p>Adsplus.vn gửi email thông báo chiến dịch của quý khách DỰ KIẾN KẾT THÚC hợp đồng ADSPLUS cho <?php echo anchor(prep_url($term->term_name),$term->term_name);?> bao gồm các thông tin chi tiết sau</p>
    </td>
  </tr>

  <tr>
    <td align="center" style="border:20px solid #e6e6e6;"><table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <tbody>
        <tr>
          <td bgcolor="#fff">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
              <tbody>
                <tr>
                  <td bgcolor="#fff">
                    <?php
                    $template = array(
                      'table_open' => '<table border="0" cellspacing="0" cellpadding="5" width="100%" style="font-size:13px">',
                      'row_alt_start' => '<tr bgcolor="#f4f8fb">');

                    $this->table->set_template($template);
                    $this->table->add_row(array('data'=>'PHẦN CAM KẾT','style'=>'color:#fff;font-weight:bold','width'=>'80%','colspan'=>2,'bgcolor'=>'#f58220'));

                    $this->table->add_row('Thời gian bắt đầu :',my_date($start_time,'d/m/Y'));

                    $this->load->config('googleads/googleads');
                    $service_types = $this->config->item('report_type','googleads');
                    $keys_service_type = array_keys($service_types);
                    $service_type = force_var(get_term_meta_value($term->term_id,'service_type'), reset($keys_service_type));

                    if($service_type == 'cpc_type')
                    {
                      $total_value = 0;
                      $total_clicks = 0;

                      $contract_clicks = get_term_meta_value($term->term_id,'contract_clicks');
                      if(!empty($contract_clicks))
                      {
                        $cpc = get_term_meta_value($term->term_id,'contract_cpc');
                        $search_value = $cpc * $contract_clicks;
                        $this->table->add_row('[Search] Ngân sách',currency_numberformat($search_value));
                        $this->table->add_row('[Search] Clicks cam kết',$contract_clicks);

                        $total_value+= $search_value;
                        $total_clicks+= $contract_clicks;
                      }

                      $gdn_contract_clicks = get_term_meta_value($term->term_id,'gdn_contract_clicks');
                      if(!empty($gdn_contract_clicks))
                      {
                        $gdn_contract_cpc = get_term_meta_value($term->term_id,'gdn_contract_cpc');
                        $gdn_value = $gdn_contract_cpc * $gdn_contract_clicks;
                        $this->table->add_row('[GDN] Ngân sách ',currency_numberformat($gdn_value));
                        $this->table->add_row('[GDN] Clicks cam kết ',$gdn_contract_clicks);

                        $total_value+= $gdn_value;
                        $total_clicks+= $gdn_contract_clicks;
                      }

                      $this->table->add_row(
                        array('data'=> 'Tổng ngân sách','style'=>'color:#0072bc; font-weight:bold;'),
                        array('data'=> currency_numberformat($total_value),'style'=>'color:#0072bc; font-weight:bold;')
                        );

                      $this->table->add_row(
                        array('data'=> 'Tổng clicks cam kết','style'=>'color:#0072bc; font-weight:bold;'),
                        array('data'=> $total_clicks,'style'=>'color:#0072bc; font-weight:bold;')
                        ); 
                    }
                    else
                    {
                      $bugget = get_term_meta_value($term->term_id,'contract_budget');
                      $bugget = currency_numberformat($bugget);

                      $this->table->add_row(array('data'=>'Ngân sách cam kết','style'=>'color:#0072bc; font-weight:bold;'),$bugget);
                    }
                    echo $this->table->generate();

                    $this->table->set_template($template);
                    $this->table->add_row(array('data'=>'PHẦN ĐANG THỰC HIỆN','style'=>'color:#fff;font-weight:bold','colspan'=>2,'bgcolor'=>'#f58220'));

                    $start_time = get_term_meta_value($term->term_id,'googleads-begin_time');
                    $end_time = $this->mdate->convert_time(get_term_meta_value($term->term_id,'googleads-end_time'));

                    $this->table->add_row(array('data'=>'Thời gian bắt đầu :','width'=>'60%'),my_date($start_time,'d/m/Y'));
                    $this->table->add_row('Thời gian dự kiến kết thúc :',my_date($expected_end_time,'d/m/Y'));

                    if($service_type == 'cpc_type')
                    {

                      $contract_clicks = get_term_meta_value($term->term_id,'contract_clicks');
                      if(!empty($contract_clicks) && is_numeric($contract_clicks))
                      {
                        // progress of search networking
                        $progress = $this->googleads_m->get_the_progress($term->term_id,'search');
                        $actual = get_term_meta_value($term->term_id, 'search_actual_result');

                        $this->table->add_row('[Search] Click thực tế :',$actual);
                        $this->table->add_row(
                          array('data'=> '[Search] % tiến độ hoàn thành','style'=>'color:#0072bc; font-weight:bold;'),
                          array('data'=> currency_numberformat($progress,'%',2),'style'=>'color:#0072bc; font-weight:bold;'));
                      }

                      $gdn_contract_clicks = get_term_meta_value($term->term_id,'gdn_contract_clicks');
                      if(!empty($gdn_contract_clicks) && is_numeric($gdn_contract_clicks))
                      {
                        // progress of search networking
                        $progress = $this->googleads_m->get_the_progress($term->term_id,'display');
                        $actual = get_term_meta_value($term->term_id, 'display_actual_result');

                        $this->table->add_row('[GDN] Click thực tế :',$actual);
                        $this->table->add_row(
                          array('data'=> '[GDN] % tiến độ hoàn thành','style'=>'color:#0072bc; font-weight:bold;'),
                          array('data'=> currency_numberformat($progress,'%',2),'style'=>'color:#0072bc; font-weight:bold;'));
                      }

                      $contract_clicks = (int) get_term_meta_value($term->term_id,'contract_clicks');
                      $gdn_contract_clicks = (int) get_term_meta_value($term->term_id,'gdn_contract_clicks');
                      $clicks = $contract_clicks + $gdn_contract_clicks;

                      $progress = $this->googleads_m->get_the_progress($term->term_id);
                      $actual = get_term_meta_value($term->term_id,'actual_result');

                      // $this->table->add_row('Tổng click thực tế :',$actual);
                      // $row_dat = array(
                      //   array('data'=>'Tổng % tiến độ thực hiện','style'=>'color:#0072bc; font-weight:bold;'),
                      //   array('data'=> currency_numberformat($progress,'%',2),'style'=>'color:#0072bc; font-weight:bold;')
                      //   );
                      // $this->table->add_row($row_dat);
                    }
                    else
                    {
                      $progress = $this->googleads_m->get_the_progress($term->term_id);
                      $actual = get_term_meta_value($term->term_id,'actual_result');
                      $money = currency_numberformat($actual,' đồng');

                      $this->table->add_row(
                        array('data'=>'Ngân sách thực tế','style'=>'color:#0072bc; font-weight:bold;'),
                        array('data'=>$money,'style'=>'color:#0072bc; font-weight:bold;'));
                    }
                    echo $this->table->generate();
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>


  <tr>
    <td align="center" style="border:20px solid #e6e6e6;">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
        <tbody>
          <tr>
            <td bgcolor="#fff">
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                <tbody>
                  <tr>
                    <td bgcolor="#fff">
                      <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Thông tin khách hàng</p>
                      <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                        <tbody>
                          <tr bgcolor="#f4f8fb">
                            <td width="25%" height="25">Tên khách hàng</td>
                            <td>: <?php echo 'Anh/Chị '.get_term_meta_value($term->term_id,'representative_name');?></td>
                          </tr>
                          <tr>
                            <td height="25">Email</td>
                            <td>: <?php echo mailto(get_term_meta_value($term->term_id,'representative_email'));?></td>
                          </tr>
                          <tr bgcolor="#f4f8fb">
                            <td  height="25">Điện thoại di động</td>
                            <td>: <?php echo get_term_meta_value($term->term_id,'representative_phone');?></td>
                          </tr>
                          <tr>
                            <td height="25">Địa chỉ</td>
                            <td>: <?php echo get_term_meta_value($term->term_id,'representative_address');?></td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>    
              <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">&nbsp;</p>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>

<tr>
  <td align="center" style="border:20px solid #e6e6e6;">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <tbody>
        <tr>
          <td bgcolor="#fff">
          <b><u>Quý khách lưu ý:</u></b> Đây là thông báo tiến độ chạy quảng cáo, có thể ngày dự kiến kết thúc sớm hơn hoặc chậm hơn so với cam kết do xu hướng tìm kiếm người dùng tăng hoặc giảm:
          <ul style="line-height: 1.2 em">
            <li style="margin-bottom: 10px"><b>Đối với trường hợp chạy chậm hơn so với tiến độ</b>, chúng tôi sẽ cố gắng kéo dài thời gian chạy so với cam kết để đạt được tiến độ cam kết..
            </li>
            <li style="margin-bottom: 10px"><b>Đối với trường hợp chạy nhanh hơn tiến độ</b>, chúng tôi có thể chạy nhanh hơn tiến độ cam kết  nhưng không vượt quá 30% thời gian cam kết để tránh bỏ lỡ cơ hội bán hàng khi xu hướng tìm kiếm tăng. Trong trường hợp Quý khách vẫn muốn chạy đúng tiến độ, vui lòng phản hồi để chúng tôi điều chỉnh để chạy đúng tiến độ như thời gian cam kết, điều này có thể làm giảm cơ hội bán hàng của Quý khách.
            </li>
          </ul>
          <b><u>Thanh Toán:</u></b>
          <ul style="line-height: 1.2 em">
            <li style="margin-bottom: 10px">
            <b>Đối với Quý khách hàng thanh toán theo từng đợt</b> xin vui lòng thanh toán trước ngày <b><?php echo my_date($expected_end_time,'d/m/Y');?></b>  để Quảng cáo không bị gián đoạn.
            </li>
            <li style="margin-bottom: 10px">
            <b>Đối với Quý khách hàng sắp hết hợp đồng</b> vui lòng liên hệ Nhân viên kinh doanh trực tiếp chăm sóc Quý khách để tiếp tục ký hợp đồng mới để Quảng cáo không bị gián đoạn.
            </li>
          </ul>

Cảm ơn Quý khách đã tin tưởng và đồng hành sử dụng dịch vụ của Adsplus.vn trong thời gian qua.
Xin chân thành cảm ơn !       
          </td>
        </tr>
      </tbody>
    </table>
  </td>
</tr>

</table>