<tr>
  <td align="center">
    <p style="font-family:Roboto;color:#ffffff;font-size:30px; font-weight:300; margin:5px 0;  text-transform:uppercase;">
    <?php echo @$title;?>
    </p>
  </td>
</tr>
<tr>
  <td align="center">
    <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">
    Mã hợp đồng: <span style="font-weight:bold"><?php echo get_term_meta_value($term->term_id,'contract_code');?></span> 
    </p>
    <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">
    <?php
    $start_time = get_term_meta_value($term->term_id,'contract_begin');
    $end_time = get_term_meta_value($term->term_id,'contract_end');
    $contract_date = my_date($start_time,'d/m/Y').' - '.my_date($end_time,'d/m/Y');
    ?>
    Thời gian thực hiện từ: <span style="font-weight:bold"><?php echo $contract_date;?></span> 
    </p>
    &nbsp;</td>
</tr>
<tr>
  <td bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6;border-right: 20px solid #e6e6e6;  "><table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td bgcolor="#f58220" ><table width="640" cellspacing="0" cellpadding="0" border="0" align="center"   >
              <tbody>
                <tr>
                  <td style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px ; font-weight:bold;line-height:35px;"> &nbsp; Để phục khách hàng tốt hơn trong quá trình thực hiện hợp đồng, Adsplus có những lưu ý :</td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td bgcolor="#e6e6e6"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
                <tr>
                  <td valign="top"><img alt="arrow down" src="file:///D|/E-mail/img/arrowdown.png"></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td><table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border-bottom:20px #ffffff solid;border-top:20px #ffffff solid;border-left:20px #ffffff solid;border-right:20px #ffffff solid;background:#ffffff; margin-bottom: 20px">
              <tbody>
                <tr>
                  <td bgcolor="#ffffff" style="    font-size: 13px;
color: #363636;
font-family: Arial, Helvetica, sans-serif;" >
<p>Đây là thông báo tiến độ chạy quảng cáo, có thể ngày dự kiến kết thúc sớm hơn hoặc chậm hơn so với cam kết do xu hướng tìm kiếm người dùng tăng hoặc giảm:</p>
<p><strong>1 . Đối với trường hợp chạy chậm hơn so với tiến độ:  </strong>Anh/Chị share giúp Wedoctor.vn vào tài khoản mail google-analytics@erp-webdoctor.iam.gserviceaccount.com => việc này giúp bên em theo dõi những lỗi và những cập nhật mới từ google cho site chúng ta. </p>
                    <p ><strong>2. Tài khoản Goolge analytics : </strong>chúng tôi có thể chạy nhanh hơn tiến độ cam kết  nhưng không vượt quá 30% thời gian cam kết để tránh bỏ lỡ cơ hội bán hàng khi xu hướng tìm kiếm tăng. Trong trường hợp Quý khách vẫn muốn chạy đúng tiến độ, vui lòng phản hồi để chúng tôi điều chỉnh để chạy đúng tiến độ như thời gian cam kết, điều này có thể làm giảm cơ hội bán hàng của Quý khách.</p>
                    <p ><strong>3 . Thanh Toán: </strong> <br />
                    &nbsp; &nbsp;   Đối với Quý khách hàng thanh toán theo từng đợt xin vui lòng thanh toán trước ngày 13/05/2016 để Quảng cáo không bị gián đoạn. <br />
                    &nbsp; &nbsp;   Đối với Quý khách hàng sắp hết hợp đồng: vui lòng liên hệ Nhân viên kinh doanh trực tiếp chăm sóc Quý khách để tiếp tục ký hợp đồng mới để Quảng cáo không bị gián đoạn.</p>
                    <p >Cảm ơn Quý khách đã tin tưởng và đồng hành sử dụng dịch vụ của Adsplus.vn trong thời gian qua. </p>
                    </td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table></td>
</tr>