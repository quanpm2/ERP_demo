
<div class="clear10"></div>
<div class="col-sm-8 col-xs-12">
	<div class="statistic" >
		<h1 class="text-center text-primary">Bảng thống kê KPIs hiện tại</h1>
		<div class="row">
			<div class="col-sm-3 col-xs-3">
				<div class="btn-success text-center text-white"><strong>S/ngày đã t/hiện</strong></div>
				<h3 class="col-sm-6 col-xs-6 text-center"><?php echo date('d', $end_date);?></h3>
				<h3 class="col-sm-6 col-xs-6 text-center text-danger">
				<?php echo date('d',$dayEndOfMonth) - date('d', $end_date);?>
				<?php //echo $numDateOfMonth;?></h3>
				<div class="clear"></div>
				<div class="bg-warning label-info text-center "><strong>KPIs</strong></div>
				<h1 class="text-center text-primary "><strong><?php echo $kpi;?></strong></h1>
			</div>
			<div class="col-sm-6 col-sm-6 col-xs-6">
				<table cellspacing="0" cellpadding="0" width="100%">
					<tr>
						<td width="50%" class="text-center bg-primary">&nbsp;</td>
						<td width="" class="text-center bg-primary"><strong>Chỉ số đã đạt</strong></td>
					</tr>
					<tr>
						<td class="text-organic">Organic Search</td>
						<td class="text-center text-success"><span class="line text-center"><strong><?php echo $ga_total['total']['Organic Search'];?></strong></span></td>
					</tr>

					<tr>
						<td class="text-direct">Direct</td>
						<td class="text-center"><?php echo $ga_total['total']['Direct'];?></td>
					</tr>

					<tr>
						<td class="text-referral">Referral </td>
						<td class="text-center"><?php echo $ga_total['total']['Referral'];?></td>
					</tr>
					<tr>
						<td class="text-social">Social</td>
						<td class="text-center"><?php echo $ga_total['total']['Social'];?></td>
					</tr>
					<tr>
						<td class="">Tổng</td>
						<td class="text-center"><strong><?php echo $ga_total['total']['ga:sessions'];?></strong></td>
					</tr>
				</table>
			</div>
			<div class="col-sm-3 col-xs-3">
				<div class="btn-success text-center text-white"><strong>Tiến độ</strong></div>
				<h1 class="text-center text-primary "><strong><?php echo $this->seotraffic_m->predict_traffic(@$ga_total['total'][$seotraffic_type], 1, 1, $kpi);?>%</strong></h1>
			</div>
			<div class="clear"></div>
			<h3 class="col-sm-3 bg-warning" style="margin:0; padding:18px; height:96px" >Dự đoán KPIs cuối tháng</h3>
			<div class="btn-success col-sm-6 text-center" >
				<?php echo @$ga_label[$seotraffic_type];?>
				<h1><?php echo @$this->seotraffic_m->predict_traffic($ga_total['total'][$seotraffic_type], date('d', $end_date), date('d',$dayEndOfMonth), $kpi, 'traffic');?></h1>
			</div>
			<div class="btn-primary col-sm-3 text-center" >
				Tiến độ
				<h1><?php echo $this->seotraffic_m->predict_traffic(@$ga_total['total'][$seotraffic_type], date('d', $end_date), date('d',$dayEndOfMonth), $kpi);?>%</h1>
			</div>

		</div>
	</div>
	<div class="clear10"></div>
</div>
<div class="col-sm- col-xs-1"></div>
<div class="col-sm-3 dotnut col-xs-12">
	<div id="canvas-holder" class="col-sm-8 col-xs-12 col-centered">
		<canvas id="chart-area" width="100" height="100"/>
	</div>
	<div class="col-sm-12 col-xs-12 ">
		<div class="organic">Search Engine</div>
		<div class="direct">Direct</div>
		<div class="referral" >Referral</div>
		<div class="social" >Social</div>
	</div>
	
	<script async="async" type="text/javascript">

		var doughnutData = [
		{
			value: <?php echo $ga_total['total']['Organic Search'];?>,
			color:"#F7464A",
			highlight: "#FF5A5E",
			label: "Organic Search"
		},
		{
			value: <?php echo $ga_total['total']['Direct'];?>,
			color: "#46BFBD",
			highlight: "#5AD3D1",
			label: "Direct"
		},
		{
			value: <?php echo $ga_total['total']['Referral'];?>,
			color: "#FDB45C",
			highlight: "#FFC870",
			label: "Referral"
		},
		{
			value: <?php echo $ga_total['total']['Social'];?>,
			color: "#949FB1",
			highlight: "#A8B3C5",
			label: "Social"
		}

		];
	</script> 
	<div class="clear"></div>
</div>