Kính chào quý khách! <br>
<b>WEBDOCTOR.VN</b> kính gửi quý khách báo cáo chỉ số Google Analytics tuần <b><?php echo $week; ?> <i>(từ ngày <?php echo date('d/m/Y',$start_date); ?> đến ngày <?php echo date('d/m/Y',$end_date); ?>)</i>.</b> <br>
<?php echo anchor( $report_link, 'Quý khách vui lòng bấm vào đây để xem chi tiết báo cáo này', 'title="" target="_blank"'); ?>
<p></p>
Cảm ơn quý khách đã sử dụng dịch vụ của <b><a href="//webdoctor.vn" target="_blank" style="color:#00F">WWW.WEBDOCTOR.VN</a></b>. <br>

<em style="color:#666; font-size:10px">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không phải reply lại email này.</em> <br>