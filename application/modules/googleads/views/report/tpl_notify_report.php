<div style="font-family:Arial; font-size:13px; line-height:18px; color:#333"><br>
<b><a href="//adsplus.vn" target="_blank" style="color:#00F">ADSPLUS.VN</a></b> 
Thống kê báo cáo email - sms tự động ngày <?php echo date('d-m-Y');?><br><br>

<?php

$this->load->library('table');

if(!empty($email)){

	$this->table->set_caption('BÁO CÁO GỬI MAIL TỰ ĐỘNG');

	$this->table
	->set_template(array('table_open'=>'<table border="0" cellpadding="5" cellspacing="0" class="table table-bordered table-hover">'));

	$this->table->set_heading(array('STT','Khách hàng','Người nhận','Tình trạng','File báo cáo'));

	$index = 0;

	foreach ($email as $i) {

		$this->table->add_row(array(
			++$index,
			@$i['term_name'],
			$this->googleads_log_m->meta->get_meta_value(@$i['log_id'], 'email_to'),
			@$i['log_status'] ? '<b style="color:green;">Gửi thành công</b>' : '<b style="color:red;">Thất bại</b>',
			anchor(base_url() . $this->googleads_log_m->meta->get_meta_value(@$i['log_id'], 'attachment'), 'File báo cáo')
		));
	}	

	echo $this->table->generate();
}

echo '<br>';

if(!empty($sms)){

	$this->table->clear()->set_caption('BÁO CÁO GỬI SMS TỰ ĐỘNG');

	$this->table
	->set_template(array('table_open'=>'<table border="0" cellpadding="5" cellspacing="0" class="table table-bordered table-hover">'));

	$this->table->set_heading(array('STT','Khách hàng','Người nhận','Tình trạng','Nội dung'));

	$index = 0;

	foreach ($sms as $i) {

		$this->table->add_row(array(
			++$index,
			@$i['vars']['website'],
			@$i['vars']['customer_name'],
			@$i['config']['sms_status'][$i['response']->SendSMSResult],
			@$i['message'],
		));
	}	

	echo $this->table->generate();
}
	
echo '
	Cảm ơn quý khách đã sử dụng dịch vụ của <b>' . anchor('http://adsplus.vn','ADSPLUS.VN','target="_blank" style="color:#00F"') . '</b>. <br>
	<em style="color:#666; font-size:10px">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không phải reply lại email này.</em> <br>
</div>';