<?php
$this->template->javascript->add(base_url("dist/vGoogleadsConfigurationContractBox.js"));
?>

<div class="col-md-12" id="service_tab">
<?php

$term_id = $edit->term_id;

echo $this->admin_form->form_open();
echo form_hidden('edit[term_id]', $term_id);

?>
<div id="vGoogleadsConfigurationContractBox">
    <v-googleads-configuration-contract-box term_id="<?php echo $edit->term_id;?>"></v-googleads-configuration-contract-box>
</div>

<?php

$begin_time = get_term_meta_value($term_id, 'googleads-begin_time');
$begin_time = empty($begin_time) ? $begin_time : my_date($begin_time, 'd-m-Y');

$end_time = get_term_meta_value($term_id, 'googleads-end_time');
$end_time = empty($end_time) ? $end_time : my_date($end_time, 'd-m-Y');

/* LOAD THÔNG TIN LOẠI HÌNH CHẠY QUẢNG CÁO [QUẢN LÝ TÀI KHOẢN | CPC] */
$this->load->config('googleads/googleads');

$service_types  = $this->config->item('report_type', 'googleads');
$service_type   = get_term_meta_value($term_id, 'service_type');

// Nếu meta value không chứa giá trị bất kỳ, lấy giá trị mặc định đầu tiên trong file cấu hình "loại hình dịch vụ"
if( empty($service_type))
{
    $service_type = $this->config->item('default_service_type', 'googleads');
    $keys_service_type      = array_keys($service_types);
    in_array($service_type, $keys_service_type) OR $service_type = end($keys_service_type);
}

// BEGIN::Curators
echo $this->admin_form->box_open('Thông tin người nhận báo cáo');
$curators = @unserialize(get_term_meta_value($edit->term_id,'contract_curators'));

if(empty($curators))
{
    $curators = array();
    $curators[] = [
        'name'=>get_term_meta_value($edit->term_id,'representative_name'),
        'phone'=>get_term_meta_value($edit->term_id,'representative_phone'),
        'email'=>get_term_meta_value($edit->term_id,'representative_email')
    ];

    /* Load dữ liệu báo cáo tự hợp đồng cũ */
    $this->load->model('googleads/googleads_m');
    $old_terms = $this->googleads_m
    ->select('term_id')
    ->set_term_type()
    ->order_by('term_id','desc')
    ->where('term_parent', $edit->term_parent)
    ->limit(5)
    ->get_all();

    if( ! empty($old_terms))
    {
        foreach ($old_terms as $_term)
        {
            $_curators = get_term_meta_value($_term->term_id, 'googleads-curators');
            $_curators = is_serialized($_curators) ? unserialize($_curators) : [];
            if(empty($_curators)) continue;

            $_names = array_column($curators, 'name');
            $_email = array_column($curators, 'email');

            $_curators = array_filter($_curators, function($x) use($_names, $_email) {
                if( empty($x['name'])) return FALSE;
                if( in_array($x['name'], $_names) || in_array($x['email'], $_email)) return FALSE;
                return TRUE;
            });

            if(empty($_curators)) continue;

            $curators = wp_parse_args( $_curators, $curators);
        }
    }
}

$max_curator = $this->option_m->get_value('max_number_curators');
$max_curator = $max_curator ?: $this->config->item('max_input_curators','googleads');
for ($i=0; $i < $max_curator; $i++)
{ 
    $name = '';
    $phone = '';
    $email = '';
    if(!empty($curators[$i]))
    {
        $name = $curators[$i]['name'];
        $phone = $curators[$i]['phone'];
        $email = $curators[$i]['email'];
    }

    echo $this->admin_form->formGroup_begin();
    ?>
    <div class="col-xs-4" style="padding-left: 0;">
    <?php echo form_input(['name'=>'edit[meta][contract_curators][curator_name][]','class'=>'form-control','placeholder'=>'Người nhận báo cáo'],$name);?>
    </div>
    <div class="col-xs-4">
    <?php echo form_input(['name'=>'edit[meta][contract_curators][curator_email][]','class'=>'form-control','placeholder'=>'Email'],$email); ?>
    </div>
    <div class="col-xs-4">
    <?php echo form_input(['name'=>'edit[meta][contract_curators][curator_phone][]','class'=>'form-control','placeholder'=>'Số điện thoại'],$phone);?>
    </div>
    <?php
    echo $this->admin_form->formGroup_end();
}

echo $this->admin_form->box_close();
// End::Curators

/***************************************/
echo $this->admin_form->submit('', 'confirm_step_service', 'confirm_step_service', '', ['style'=>'display:none;', 'id'=>'confirm_step_service']);
echo $this->admin_form->form_close();
?>
</div>
<script type="text/javascript">

var service_fee_rule = <?php echo json_encode(array_values($this->config->item('service_fee_rule') ?? []));?>;

/**
 * Calculates the rate.
 */
function calc_rate(amount, number_of_payment)
{
    var service_fee = 0;
    var i = 0;

    for (i; i < service_fee_rule.length; i++)
    {
        if( amount < service_fee_rule[i].min || amount > service_fee_rule[i].max) continue;

        if(service_fee_rule[i].amount !== false) 
        {
            service_fee = service_fee_rule[i].amount;
            break;
        }

        if(service_fee_rule[i].rate !== false)
        { 
            service_fee = amount * service_fee_rule[i].rate;
            break;
        }
    }

    return service_fee;
}


function get_rate(amount)
{
    var i = 0;

    for (i; i < service_fee_rule.length; i++)
    {
        if( amount < service_fee_rule[i].min || amount > service_fee_rule[i].max) continue;
        
        if(service_fee_rule[i].rate !== false) return service_fee_rule[i].rate;
    }

    return false;
}


function calc_service_fee(budget, number_of_payment, payment_type)
{
    var amount = budget;
    if(payment_type == 'devide' && number_of_payment > 1)
    {
        amount = budget / number_of_payment;
    }

    var service_fee = 1000000;
    var rate        = get_rate(amount);

    if(isNaN(rate)) return 1000000 * number_of_payment;

    if( ! isNaN(rate) && rate != false) service_fee = budget * rate;

    return _.round(service_fee);
}

$(function(){

    $('.contract_budget_input, .number_of_payment_input').keyup(function(){
        
        var budget              = $('.contract_budget_input').val();
        var number_of_payment   = $('.number_of_payment_input').val();
        var payment_type        = $('.service_fee_payment_type_input').val();

        $('.service_fee_input').val(calc_service_fee(budget, number_of_payment, payment_type));
    });

    $('.service_fee_payment_type_input').on('select2:select', function (e) {

        var budget              = $('.contract_budget_input').val();
        var number_of_payment   = $('.number_of_payment_input').val();
        var payment_type        = $('.service_fee_payment_type_input').val();

        $('.service_fee_input').val(calc_service_fee(budget, number_of_payment, payment_type));
    });
    
    $('.input-mask').inputmask();
    $('.set-datepicker').datepicker({ format: 'dd-mm-yyyy', todayHighlight: true, autoclose: true });

    /* INIT RADIO COMPONENT FOR SERVICE TYPES */
    $('#service_tab input[type="radio"][name="edit[meta][service_type]"]')
    .iCheck({checkboxClass: 'icheckbox_flat-blue', radioClass: 'iradio_flat-blue' })
    .on('ifChecked', function(event){ $(".tab-pane-service-type").toggleClass('active'); });

    /* INIT RADIO COMPONENT FOR BUDGET PAYMENT TYPE */
    $('#service_tab input[type="radio"][name="edit[meta][contract_budget_payment_type]"]')
    .iCheck({checkboxClass: 'icheckbox_flat-blue', radioClass: 'iradio_flat-blue' })
    .on('ifChecked', function(event){

        if(_.isEqual('normal', $(this).val()))
        {
            $('#service_tab div.contractBudgetCustomerPaymentTypeEl').hide();
            return;
        }
        
        $('#service_tab div.contractBudgetCustomerPaymentTypeEl').show();
    });

    /* INIT RADIO COMPONENT FOR BUDGET PAYMENT TYPE VIA WHICH OBJECT */
    $('#service_tab input[type="radio"][name="edit[meta][contract_budget_customer_payment_type]"]')
    .iCheck({checkboxClass: 'icheckbox_flat-blue', radioClass: 'iradio_flat-blue' });

    $('.select2').select2();
    $('.select2').css('width','100%'); 
})
</script>