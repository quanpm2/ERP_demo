<style type="text/css">
@media print {
    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
}
</style>
<div class="pagebreak"> </div>
<!-- PHỤ LỤC THU & CHI HỘ  -->
<p align="center">
    CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br/>
    <span>Độc lập - Tự do - Hạnh phúc</span><br/>
    <span>--------------oOo--------------</span>
</p>

<p>&nbsp;</p>
<p align="center">
    <span style="font-size:1.3em; line-height:1.3em">BIÊN BẢN THỎA THUẬN</span><br />
    <span style="font-size:1.1em; line-height:1.3em"><b>(Về việc thu hộ - chi hộ)</b></span><br/>
    <span style="font-size:1.1em; line-height:1.3em">
        <em>Đính kèm hợp đồng số <?php echo get_term_meta_value($term_id,'contract_code');?> 
        ký ngày <?php echo my_date($verified_on, 'd');?> tháng <?php echo my_date($verified_on, 'm');?> năm <?php echo my_date($verified_on, 'Y');?></em>
    </span><br/>
    
</p>
<p align="center">&nbsp;</p>

<ul>
    <li>
        <p>
            <em>                
            Căn cứ vào hợp đồng Số <?php echo get_term_meta_value($term_id,'contract_code');?> ký ngày <?php echo my_date($verified_on, 'd/m/Y');?> giữa CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM và <?php echo mb_strtoupper($customer->display_name);?>
            </em>
        </p>
    </li>
    <li>
        <p>
            <i>Luật Thương mại số 36/2005/QH11 của nước CHXHCN Việt Nam và các văn bản hướng dẫn thi hành;</i>
        </p>
    </li>
</ul>

<p align="center">&nbsp;</p>
<p>
    Hôm nay, ngày <?php echo my_date($verified_on, 'd');?> tháng <?php echo my_date($verified_on, 'm');?> năm <?php echo my_date($verified_on, 'Y');?> chúng tôi gồm:
</p>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
    <tr>
        <td width="35%"><strong>BÊN THUÊ DỊCH VỤ (BÊN A)</strong></td>
        <td>: <strong><?php echo mb_strtoupper($customer->display_name);?></strong></td>
    </tr>
    <?php foreach ($data_customer as $label => $val) :?>
    <?php if (empty($val)) continue;?>
    <tr>
        <td width="130px" valign="top"><?php echo $label;?></td>
        <td>: <?php echo $val;?></td>
    </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td><strong>BÊN CUNG ỨNG DỊCH VỤ (BÊN B)</strong></td>
        <td>: <strong><?php echo $company_name; ?></strong></td>
    </tr>

    <?php foreach ($data_represent as $label => $val) :?>
    <?php if (empty($val)) continue;?>
    <tr>
        <td width="130px" valign="top"><?php echo $label;?></td>
        <td>: <?php echo $val;?></td>
    </tr>
    <?php endforeach; ?>

</table>

<p>Hai bên cùng nhau thỏa thuận với các điều khoản như sau:<br /><br /></p>
<p>
    <strong>Điều 1: Nội Dung Thỏa Thuận</strong><br />  
    <ul>
        <li>Bên B thực hiện quản lý tài khoản quảng cáo cho Bên A theo hợp đồng số <?php echo get_term_meta_value($term_id,'contract_code');?>, ký ngày <?php echo my_date($verified_on, 'd/m/Y');?></li>
        <li>Bên B đồng ý làm trung gian thanh toán cho Google phần ngân sách của Bên A chưa bao gồm thuế giá trị gia tăng và thuế nhà thầu</li>
        <li>Gía trị thanh toán như sau:</li>
    </ul>
</p>

<table border="1" cellspacing="0" cellpadding="3" width="100%">
    <tr>
        <th>Hạng mục</th>
        <th>Thành tiền (vnđ)</th>
    </tr>
    <tr>        
        <td>Ngân sách quảng cáo</td>
        <td style="text-align:right"><?php echo currency_numberformat($data_service['budget'],'');?></td>
    </tr>
    <tr>        
        <td>Số tiền phải thanh toán</td>
        <td style="text-align:right"><?php echo currency_numberformat($data_service['budget'],'');?></td>
    </tr>
</table>
<p>Bằng chữ: <?php echo currency_number_to_words($data_service['budget']); ?></em></p>

<?php

$_personalBankInfo = $this->config->item('person', 'bank_infos');
$_personalBankInfo = array_map(function($bankInfo){
    if(isset($bankInfo['representative_zone'])) unset($bankInfo['representative_zone']);
    return $bankInfo;
}, array_filter($_personalBankInfo, function($x) use ($representative_zone){
    return $x['representative_zone'] == $representative_zone;
}));
?>

<?php if( ! empty($_personalBankInfo)) :?>
<p>Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin tài khoản do Bên B cung cấp, cụ thể: </p>
<ul style="padding-left:0;list-style:none">
<?php foreach ($_personalBankInfo as $label => $text) :?>
    <?php if (is_array($text)) : ?>
        <?php foreach ($text as $key => $value) :?>
            <li>- <?php echo $key;?>: <?php echo $value;?></li>
        <?php endforeach;?>
    <?php continue; endif;?>
    <li>- <?php echo $label;?>: <?php echo $text;?></li>
<?php endforeach;?>
</ul>
<?php endif; ?>

<p><strong>Điều 2: Trách nhiệm và quyền hạn của mỗi bên</strong><br /></p>
<p>
    <u>2.1 Trách nhiệm và quyền hạn của Bên A:</u>
    <ul>
        <li>Bên A chịu trách nhiệm kê khai, nộp đầy đủ thuế và các nghĩa vụ khác với nhà nước (nếu có).</li>
        <li>Bên A có trách nhiệm thanh toán phần ngân sách cho Bên B trước khi thực hiện dịch vụ.</li>
        <li>Bên A chịu các khoản phí ngân hàng (nếu có) trong quá trình Bên B thanh toán cho Google.</li>
    </ul>
</p>

<p>
    <u>2.2 Trách nhiệm và quyền hạn của Bên B:</u>
    <ul>
        <li>Bên B sẽ không lập hóa đơn cho phần thu hộ và chi hộ này.</li>
        <li>Bên B sẽ hoàn trả cho Bên A số tiền còn lại chưa sử dụng trong ngân sách quảng cáo mà Bên A đã tạm ứng cho Bên B để thực hiện thanh toán cho Google trong trường hợp Bên A chấm dứt hợp đồng trước hạn.</li>
    </ul>
</p>
<p><strong>Điều 3: Hiệu lực thỏa thuận</strong><br /></p>
<p>
    Biên bản thỏa thuận này có hiệu lực kể từ ngày ký đến khi hợp đồng số <?php echo get_term_meta_value($term_id,'contract_code');?>, ký ngày <?php echo my_date($verified_on, 'd/m/Y');?> được xác nhận thanh lý từ hai bên.
</p>
<p>
    Biên bản thỏa thuận này được làm thành 02 bản có giá trị ngang nhau, Bên A giữ 01 bản, Bên B giữ 01 bản.
</p>



<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
    <tr>
        <td width="44%" valign="top" style="text-align:center">
            <p>
                <strong>BÊN A</strong><br />
                <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                <strong><?php echo mb_strtoupper($customer->display_name);?>  </strong>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
            <td rowspan="2"></td>

            <td width="46%" valign="top" style="text-align:center">
                <p><strong>BÊN B</strong><br />
                    <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                    <strong>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</strong>
                </p>
                <p><strong>&nbsp;</strong></p>
                <p><strong>&nbsp;</strong></p>
                <p>&nbsp;</p>
            </td>
    </tr>
    <tr>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_customer['Đại diện'] ;?></strong><br/>
                <?php echo $data_customer['Chức vụ'] ;?>
            </p>
        </td>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_represent['Đại diện'] ?? ''; ?></strong><br />
                <?php echo $data_represent['Chức vụ']; ?>
            </p>
        </td>
    </tr>
</table>