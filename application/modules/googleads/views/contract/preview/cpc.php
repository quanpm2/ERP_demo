<?php $this->load->view('googleads/contract/preview/header');?>

<p>Dựa trên:Nhu cầu của Bên A và khả năng cung cấp của Bên B về dịch vụ. Hai bên thống nhất ký kết hợp đồng kinh tế với các điều khoản cụ thể như sau: <br /><br /></p>

<strong>Điều 1: Điều khoản chung</strong><br />
Bên B nhận cung cấp cho Bên A dịch vụ quảng cáo trên trang tìm kiếm của Google theo các nội dung sau:<br />
<?php
$network_rules = array(
  'search' => array(
    'title'=> 'Quảng cáo trên trang tìm kiếm của Google',
    'content'=> '<ul>
  <li>Quảng cáo của Bên A sẽ xuất hiện tại các vị trí cho phép đặt quảng cáo của Google. Trang web mà quảng cáo sẽ xuất hiện là www.google.com.vn/google.com, cho phép toàn bộ người tìm kiếm trên trang web đó tìm thấy quảng cáo khi dùng từ khóa tương ứng.</li>
  <li> Bên B sẽ tư vấn và phối hợp với Bên A xây dựng các chiến dịch quảng cáo, danh mục từ khóa, câu quảng cáo (adtext) để chiến dịch quảng cáo đạt hiệu quả cao nhất.</li>
  <li> Bên B sẽ theo dõi, giám sát hoạt động của chiến dịch quảng cáo hàng ngày và gửi báo cáo định kỳ đến Bên A hàng tuần. Báo cáo bao gồm các thống kê về số lần xuất hiện quảng cáo, số lần website được truy cập và các nguồn dẫn đến truy cập, các hoạt động của người tìm kiếm khi truy cập website của Bên A và các thông tin khác liên quan mà công cụ phân tích của Google cho phép thực hiện. Bên B sẽ đề xuất với Bên A những giải pháp để tăng cường hiệu quả của chiến dịch quảng cáo. </li>
  </li>
</ul>'
    ),
  'display' => array(
    'title'=> 'Quảng cáo trên mạng hiển thị của Google',
    'content'=> '<ul>
  <li> Quảng cáo của Bên A sẽ xuất hiện tại các vị trí cho phép đặt quảng cáo trên các website thuộc mạng hiển thị của Google.</li>
  <li> Bên B sẽ tư vấn và phối hợp với Bên A xây dựng các chiến dịch quảng cáo, hình ảnh và thông điệp để chiến dịch quảng cáo đạt hiệu quả cao nhất nhưng không chịu trách nhiệm thực hiện thiết kế hình ảnh quảng cáo. Trường hợp Bên A có nhu cầu cần thiết kế hình ảnh/ thông điệp quảng cáo thì Bên B sẽ tính phí thiết kế theo bảng giá dịch vụ quy định tại thời điểm yêu cầu. </li>
  <li> Thông tin của quảng cáo sẽ do Bên A cung cấp và nội dung quảng cáo phải đạt được sự thống nhất của hai bên trước khi thực hiện.</li>
</ul>')
  );

$network_type = get_term_meta_value($term->term_id,'network_type') ?: 'search,display';
$network_type = explode(',', $network_type);
if(!empty($network_type))
{
  $i = 1;
  foreach ($network_type as $network) 
  {
    if(empty($network_rules[$network])) continue;
    $title = $network_rules[$network]['title'];
    $content = $network_rules[$network]['content'];
    echo "<p><strong>{$i}. {$title}:</strong>{$content}</p>";
    $i++;
  }
}

?>
<p> Toàn bộ chiến dịch được thực hiện cho website của Bên A tại địa chỉ sau: <?php echo $websites_text;?></p>

<?php if ( ! empty($contract_curators)):?>
    <p>Toàn bộ báo cáo gửi cho Bên A theo thông tin sau:</p>
    <ul>
    <?php foreach ($contract_curators as $curator): ?>
        <?php if (empty($curator['name']) || empty($curator['email'])): continue;?>
            
        <?php endif ?>
        <li><u>Ông/bà</u>: <?php echo $curator['name']??''; ?></li>
        <li><u>Email</u>: <?php echo $curator['email']??''; ?></li>
        <br/>
    <?php endforeach ?>
    </ul>
<?php endif ?>

<p>
<strong>Điều 2: Giá trị hợp đồng</strong>
<ul>
  <li>Đơn vị tính giá: Việt Nam Đồng (VND).</li>
  <li>Thanh toán bằng tiền đồng Việt Nam.  </li>
</ul>
</p>
<?php
$total = 0;

$template = array('table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">');
$heading = array('Dịch vụ','CPC(vnđ)','Số click','Thành tiền(vnđ)');
$this->table->set_template($template);
$this->table->set_heading($heading);

if(!empty($campaign_types['google_search']))
{
  $clicks = $campaign_types['google_search']['clicks'];
  $cost_per_click = $campaign_types['google_search']['cpc'];
  $search_total = $clicks*$cost_per_click;

  $this->table->add_row(
    'Quảng cáo Google Search',
    array('data' => currency_numberformat($cost_per_click),'align'=>'center'),
    array('data' => $clicks,'align'=>'center'),
    array('data' => currency_numberformat($search_total,''),'align'=>'right'));

  $total+= $search_total;
}

if(!empty($campaign_types['google_display_network']))
{
  $clicks = $campaign_types['google_display_network']['clicks'];
  $cost_per_click = $campaign_types['google_display_network']['cpc'];
  $gdn_total = $clicks*$cost_per_click;

  $this->table->add_row(
    'Quảng cáo Google Display Network',
    array('data' => currency_numberformat($cost_per_click),'align'=>'center'),
    array('data' => $clicks,'align'=>'center'),
    array('data' => currency_numberformat($gdn_total,''),'align'=>'right'));

  $total+= $gdn_total;
}

$discount_amound = 0;
$services_pricetag = get_term_meta_value($term->term_id, 'services_pricetag');
$services_pricetag = @unserialize($services_pricetag);
if(!empty($services_pricetag['discount']['is_discount']))
{
  $discount_amound = ($total * ((float) @$services_pricetag['discount']['percent'] / 100));
  if($discount_amound != 0)
  {
    $total-=$discount_amound;
    $discount_rate = (float) @$services_pricetag['discount']['percent'];

    $this->table->add_row(
      array('data' => '<strong>Giảm giá '.$discount_rate.'&#37</strong>','colspan' => 3),
      array('data' => currency_numberformat($discount_amound,''),'align' => 'right'));
  }
}

$this->table->add_row(
      array('data' => '<strong>Tổng cộng</strong>','colspan' => 3),
      array('data' => currency_numberformat($total,''),'align' => 'right'));

if(!empty($vat))
{
  $tax = $total*($vat/100);
  $total = cal_vat($total,$vat);

  $this->table->add_row(
        array('data' => "VAT ({$vat}&#37;)", 'colspan' => 3,'align'=>'center'),
        array('data' => currency_numberformat($tax,''), 'align' => 'right'));

  $this->table->add_row(
        array('data' => '<strong>Số tiền phải thanh toán</strong>', 'colspan' => 3,'align'=>'center'),
        array('data' => currency_numberformat($total,''), 'align' => 'right'));
}

echo $this->table->generate();
?>
<p><em>Bằng chữ: 
<?php 
$total = numberformat($total,0,'.','');
echo currency_number_to_words($total);
?></em></p>
<?php

$this->load->model('googleads/googleads_config_m');
$this->load->config('googleads/googleads');
$bonus_package = get_term_meta_value($term->term_id,'bonus_package') ?: $this->config->item('default','packages');

$bonus_package = $this->googleads_config_m->get_gift($bonus_package);
if(!empty($bonus_package))
{
  echo '<p>Dịch vụ tặng kèm</p>';

  $bonus_total = 0;

  $template = array('table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">');
  $heading = array('Dịch vụ','Giá trị tặng','Thời gian','Thành tiền(vnđ)');
  $this->table->set_template($template);
  $this->table->set_heading($heading);

  $rowspan = count($bonus_package)+1;
  $first_row = true;
  foreach ($bonus_package as $row) 
  {
    $bonus_total += is_numeric($row['value']) ? $row['value'] : 0;
    if($first_row) 
    {
      $this->table->add_row($row['label'],array('data'=>currency_numberformat($row['value']),'align'=>'right'),array('data'=>'Trong thời gian quảng cáo','style'=>'text-align:center','rowspan'=>$rowspan),array('data'=>'Miễn phí','style'=>'text-align:center','rowspan'=>$rowspan));
      $first_row = FALSE;
      continue;
    }

    $this->table->add_row($row['label'],array('data'=>currency_numberformat($row['value']),'align'=>'right'));
  }

  $this->table->add_row('<strong>Tổng cộng</strong>',array('data'=>currency_numberformat($bonus_total),'align'=>'right'));
  echo $this->table->generate();
}

if(!empty($bank_info))
{
  echo '<p><strong>Điều 3: Phương thức thanh toán</strong><br />Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin tài khoản do Bên B cung cấp, cụ thể:</p>';

  $template = array('table_open' => '<table width="80%" border="0" cellspacing="0" cellpadding="3" align="center">');
  $this->table->set_template($template);

  foreach ($bank_info as $label => $text) 
  {
    if(is_array($text))
    {
      foreach ($text as $key => $value)
      {
        $this->table->add_row(array('data'=>'+ '.$key,'width'=>'30%'),': '.$value);
      }
      echo $this->table->generate().br();
      $this->table->set_template($template);
      continue;
    }

    $this->table->add_row(array('data'=>'+ '.$label,'width'=>'30%'),': '.$text);
  }

  if(!empty($this->table->rows))
  {
    echo $this->table->generate();
  }

  echo "<p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p><br/>";
}
?>

<?php echo $payments; ?>

<?php if ($vat > 0): ?>
<p>Bên B xuất hóa đơn giá trị gia tăng cho Bên A theo thông tin sau:</p>
<ul>
    <li><u>Tên công ty</u>: <?php echo $customer->display_name ?? '';?></li>
    <li><u>Địa chỉ</u>: <?php echo $data_customer['Địa chỉ'] ?? '';?></li>
    <li><u>Mã số thuế</u>: <?php echo $data_customer['Mã số thuế'] ?? '';?></li>
</ul>
<p>Và gửi về địa chỉ email: <?php echo $customer->representative['email'] ?? '';?></p>
<?php endif ?>

<p><strong>Điều 4: Thời gian xuất hiện quảng cáo và dừng chiến dịch</strong></p>
<ul>
  <li> Quảng cáo sẽ xuất hiện trên hệ thống quảng cáo Google dự kiến từ ngày <?php echo my_date($contract_begin,'d/m/Y');?> đến ngày <?php echo my_date($contract_end,'d/m/Y');?> hoặc đến khi hết ngân sách. </li>
  <li> Kể từ khi hợp đồng được ký kết, Bên A thanh toán tiền cho Bên B theo quy định của hợp đồng này và Bên A duyệt đề xuất triển khai của Bên B. Bên B có trách nhiệm gửi đề xuất đến Bên A trong vòng 03 ngày làm việc kể từ ngày hợp đồng được ký kết. </li>
  <li> Trong thời gian thực hiện chiến dịch quảng cáo, nếu Bên A muốn dừng quảng cáo cho một số ngày nhất định, Bên A cần thông báo trước cho Bên B ít nhất 03 ngày bằng văn bản hoặc bằng email. Trong trường hợp đó, số ngày hiện quảng cáo trên Google của Bên A sẽ được tăng thêm đúng bằng số ngày tạm dừng quảng cáo nhưng thời gian dừng không quá 1 tháng (30 ngày).</li>
  <li> Trong trường hợp Bên A chậm thanh toán cho Bên B theo quy định tại điều 3 của hợp đồng thì Bên B sẽ tạm dừng chiến dịch quảng cáo tương ứng với số ngày chậm thanh toán của Bên A. Bên B có trách nhiệm đưa quảng cáo xuất hiện trở lại 01 (một) ngày sau khi nhận được đầy đủ phần thanh toán bị chậm của Bên A.<br />
  </li>
  <li>Chiến dịch quảng cáo sẽ hoàn tất khi Bên B gửi Thông báo kết thúc dịch vụ qua email cho Bên A. Sau 2 ngày kể từ khi nhận được email từ Bên B, nếu Bên A không có phản hồi khác, chiến dịch quảng cáo được xem là kết thúc.</li>
</ul>
<p><strong>Điều 5: Chấm dứt hợp đồng</strong><br />
  Trong trường hợp Bên A có thay đổi chiến dịch quảng cáo và muốn chấm dứt hợp đồng trước thời hạn thì Bên A phải gửi thông báo trước bằng văn bản cho Bên B trước 07 (bảy) ngày khi chính thức dừng quảng cáo và phải được Bên B chấp nhận về thời điểm dừng hợp đồng. <br />
Kể từ ngày Bên B chấp nhận về thời điểm dừng hợp đồng.</p>
<ul>
  <li>  Trong vòng 30 ngày: Bên A được bảo lưu chiến dịch quảng cáo</li>
  <li> Trong vòng 60 ngày: Bên A được quyền qui đổi sang dịch vụ khác của Bên B đang cung cấp </li>
  <li> Quá 60 ngày: Giá trị dịch vụ còn lại chưa sử dụng của Bên A sẽ không còn giá trị sử dụng và Bên B sẽ không thực hiện hoàn trả lại phần giá trị đó cho Bên A. <br />
  </li>
</ul>
<p> <strong>Điều 6: Sự kiện bất khả kháng</strong><br />
Trong trường hợp có những sự kiện bất khả kháng không lường trước được, trách nhiệm và thời hạn thực hiện hợp đồng của cả hai bên sẽ được xem xét, đàm phán và quyết định lại. Các sự kiện bất khả kháng bao gồm nhưng không giới hạn các rủi ro sau:</p>
<ul>
  <li> Rủi ro do ngừng hoặc lỗi kĩ thuật từ dịch vụ Google cung cấp</li>
  <li> Rủi ro về đường truyền internet, cơ sở hạ tầng mạng quốc gia</li>
  <li> Thiên tai, chiến tranh, khủng bố, hoả hoạn, dịch bệnh...<br />
  </li>
</ul>
<p> <strong>Điều 7: Tranh chấp và phân xử</strong><br />
  Hai bên cam kết thực hiện những điều khoản trong hợp đồng. Nếu có vướng mắc, mỗi bên thông báo cho nhau để cùng bàn bạc giải quyết trên tinh thần hợp tác, thiện chí, vì lợi ích cả hai bên. Trong trường hợp không thể giải quyết được bất đồng, tranh chấp sẽ được giải quyết tại Tòa án có thẩm quyền tại TP Hồ Chí Minh.<br />
</p>
<p><strong>Điều 8: Điều khoản khác</strong>
<br/>Hợp đồng có hiệu lực kể từ ngày ký và được xem như tự động thanh lý khi Hai bên thực hiện đầy đủ tất cả các điều khoản nêu trong hợp đồng này.<br />
Mọi sửa đổi hoặc bổ sung cho Hợp đồng này sẽ chỉ có hiệu lực sau khi các đại diện của cả hai bên kí kết bằng văn bản tạo thành bộ phận hợp nhất của hợp đồng.</p>
<p>  Hợp đồng này được làm thành 02 bản có giá trị ngang nhau, Bên A giữ 01 bản, Bên B giữ 01 bản.<br />
  <br />
</p>
<?php $this->load->view('googleads/contract/preview/footer'); ?>