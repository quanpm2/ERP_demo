<?php $this->load->view('googleads/contract/preview/header');?>


<p>Dựa trên:Nhu cầu của Bên A và khả năng cung cấp của Bên B về dịch vụ. Hai bên thống nhất ký kết hợp đồng kinh tế với các điều khoản cụ thể như sau: <br /><br /></p>

<div contenteditable="true" id="9c5502ab-7f92-478e-a3cd-b9eb084c6c13">
    <strong>Điều 1: Điều khoản chung</strong><br />
    Bên B nhận cung cấp cho Bên A dịch vụ quảng cáo trên các hệ thống quảng cáo theo các nội dung sau:<br />
    <?php echo $network_rules; /* Rules theo từng dịch vụ */ ?>
    <p>Bên B sẽ theo dõi, giám sát hoạt động của chiến dịch quảng cáo hàng ngày và gửi báo cáo định kỳ đến Bên A hàng tuần. Báo cáo bao gồm các thống kê về số lần xuất hiện quảng cáo, số lần website được truy cập và các nguồn dẫn đến truy cập, các hoạt động của người tìm kiếm khi truy cập website của Bên A và các thông tin khác liên quan mà công cụ phân tích của hệ thống quảng cáo cho phép thực hiện. Bên B sẽ đề xuất với Bên A những giải pháp để tăng cường hiệu quả của chiến dịch quảng cáo.</p>
    <p>Toàn bộ chiến dịch được thực hiện cho website của Bên A tại địa chỉ sau: <?php echo $websites_text;?></p>
</div>

<p>
<strong>Điều 2: Giá trị hợp đồng</strong>
<ul>
    <li>Đơn vị tính giá: Việt Nam Đồng (VND).</li>
    <li>Thanh toán bằng tiền đồng Việt Nam.</li>
</ul></p>
<table border="1" cellspacing="0" cellpadding="3" width="100%">
    <tr><th>Dịch vụ</th><th>Ngân sách</th><th>Thời gian</th><th>Thành tiền(vnđ)</th></tr>

    <?php $total = $data_service['budget']; ?>
    <tr>
        <td><?php echo $data_service['service_name'];?></td>
        <td style="text-align:center"><?php echo currency_numberformat($data_service['budget'],'');?></td>
        <td style="text-align:center">
            <?php echo $contract_days;?> ngày<br />
            <?php printf('(từ %s đến %s)',my_date($contract_begin,'d/m/Y'),my_date($contract_end,'d/m/Y'));?>
        </td>
        <td style="text-align:right"><?php echo currency_numberformat($data_service['budget'],'');?></td>
    </tr>

    <?php if ($fct): $fct_amount = div($data_service['budget'], 0.95)*div($fct, 100); $total+=$fct_amount;
    ?>
    <tr>
        <td colspan="3" style="text-align:center">Thuế nhà thầu</td>
        <td style="text-align:right"><?php echo currency_numberformat($fct_amount, '');?></td>
    </tr>
    <?php endif ?>


    <?php if ($data_service['service_fee']): $total+=$data_service['service_fee']; ?>
    <tr>
        <td colspan="3" style="text-align:center">Phí dịch vụ</td>
        <td style="text-align:right"><?php echo currency_numberformat($data_service['service_fee'],'');?></td>
    </tr>
    <?php endif ?>

    <?php

    $services_pricetag  = get_term_meta_value($term->term_id, 'services_pricetag');
    $services_pricetag  = is_serialized($services_pricetag) ? unserialize($services_pricetag) : [];
    $discount_amound    = 0;

    if( ! empty($services_pricetag['discount']['is_discount']))
    {
        $discount_amound = ($total * ((float) @$services_pricetag['discount']['percent'] / 100));
        if($discount_amound != 0)
        {
            $total-=$discount_amound;
            $discount_rate = (float) @$services_pricetag['discount']['percent'];

            $this->table->add_row(array('data'=>'Giảm giá '.$discount_rate.'&#37','colspan'=>3),
                array('data'=>currency_numberformat(-1*$discount_amound),'align'=>'right'));

            echo 
            '<tr>
                <td  colspan="3" style="text-align:center"><strong>Giảm giá '.$discount_rate.' %</strong></td>
                <td  style="text-align:right">'.currency_numberformat($discount_amound).'</td>
            </tr>';
        }
    }
    ?>

    <?php if( ! empty($data_service['discount_amount'])) :
        $total-= $data_service['discount_amount'];
    ?>
        <?php foreach ($data_service['promotions'] as $promotion): ?>
        <tr>
            <td colspan="3" style="text-align:center"><?php echo $promotion['text'] ;?></td>
            <td style="text-align:right">-<?php echo currency_numberformat($promotion['amount'], '');?></td>
        </tr> 
        <?php endforeach;?>
    <?php endif;?>

    <tr>
        <td colspan="3" style="text-align:center"><strong>Tổng cộng</strong></td>
        <td style="text-align:right"><?php echo currency_numberformat($total,'');?></td>
    </tr>

    <?php
    if(!empty($vat))
    {
        $tax    = $total*($vat/100);
        $total  = cal_vat($total,$vat);

        printf('<tr>
            <td  colspan="3" style="text-align:center">VAT (%s&#37;)</td>
            <td  style="text-align:right">%s</td>
        </tr>
        <tr>
            <td  colspan="3" style="text-align:center"><strong>Số tiền phải thanh toán</strong></td>
            <td  style="text-align:right">%s</td>
        </tr>',$vat,currency_numberformat($tax,''),currency_numberformat($total,''));
    }
    ?>
</table>
<p>Bằng chữ: <?php $total = numberformat($total,0,'.',''); echo currency_number_to_words($total); ?></em></p>

<?php if (!empty($fct)): ?>    
<p><em><b><u>Ghi chú : </u></b>Thuế nhà thầu nước ngoài = Doanh thu tính thuế Thu nhập doanh nghiệp (Ngân sách quảng cáo/95%)*Thuế suất thuế nhà thầu(5%))</em></p>
<?php endif ?>

<?php if (!empty($service_provider_tax_rate)): ?>    
    <p>
        <em>
            <b><u>Ghi chú : </u></b>
            Thuế Google thu (5%) = Ngân sách quảng cáo * 5%
        </em>
    </p>
<?php endif ?>

<?php
$this->load->model('googleads/googleads_config_m');
$this->load->config('googleads/googleads');
$bonus_package = get_term_meta_value($term->term_id,'bonus_package') ?: $this->config->item('default','packages');

$bonus_package = $this->googleads_config_m->get_gift($bonus_package);
if(!empty($bonus_package))
{
    echo '<p>Dịch vụ tặng kèm</p>';

    $bonus_total = 0;

    $template = array('table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">');
    $heading = array('Dịch vụ','Giá trị tặng','Thời gian','Thành tiền(vnđ)');
    $this->table->set_template($template);
    $this->table->set_heading($heading);

    $rowspan = count($bonus_package)+1;
    $first_row = true;
    foreach ($bonus_package as $row) 
    {
        $bonus_total += is_numeric($row['value']) ? $row['value'] : 0;
        if($first_row) 
        {
            $this->table->add_row($row['label'],array('data'=>currency_numberformat($row['value']),'align'=>'right'),array('data'=>'Trong thời gian quảng cáo','style'=>'text-align:center','rowspan'=>$rowspan),array('data'=>'Miễn phí','style'=>'text-align:center','rowspan'=>$rowspan));
            $first_row = FALSE;
            continue;
        }

        $this->table->add_row($row['label'],array('data'=>currency_numberformat($row['value']),'align'=>'right'));
    }

    $this->table->add_row('<strong>Tổng cộng</strong>',array('data'=>currency_numberformat($bonus_total),'align'=>'right'));
    echo $this->table->generate();
}
?>

<?php if( ! empty($bank_info)) :?>
<p><strong>Điều 3: Phương thức thanh toán</strong>
    <br />Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin tài khoản do Bên B cung cấp, cụ thể:</p>
<ul style="padding-left:0;list-style:none">
<?php foreach ($bank_info as $label => $text) :?>
    <?php if (is_array($text)) : ?>
        <?php foreach ($text as $key => $value) :?>
            <li>- <?php echo $key;?>: <?php echo $value;?></li>
        <?php endforeach;?>
    <?php continue; endif;?>
    <li>- <?php echo $label;?>: <?php echo $text;?></li>
<?php endforeach;?>
</ul>
<p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
<?php endif; ?>

<?php echo $payments; ?>

<?php if ($vat > 0): ?>
<p>Bên B xuất hóa đơn giá trị gia tăng cho Bên A theo thông tin sau:</p>
<ul>
    <li><u>Tên công ty</u>: <?php echo $customer->display_name ?? '';?></li>
    <li><u>Địa chỉ</u>: <?php echo $data_customer['Địa chỉ'] ?? '';?></li>
    <li><u>Mã số thuế</u>: <?php echo $data_customer['Mã số thuế'] ?? '';?></li>
</ul>
<p>Và gửi về địa chỉ email: <?php echo $customer->representative['email'] ?? '';?></p>
<?php endif ?>

<p><strong>Điều 4: Thời gian xuất hiện quảng cáo và dừng chiến dịch</strong></p>
<ul>
    <li> Quảng cáo sẽ xuất hiện trên hệ thống quảng cáo dự kiến từ ngày <?php echo my_date($contract_begin,'d/m/Y');?> đến ngày <?php echo my_date($contract_end,'d/m/Y');?> hoặc đến khi hết ngân sách. </li>
    <li> Kể từ khi hợp đồng được ký kết, Bên A thanh toán tiền cho Bên B theo quy định của hợp đồng này và Bên A duyệt đề xuất triển khai của Bên B. Bên B có trách nhiệm gửi đề xuất đến Bên A trong vòng 03 ngày làm việc kể từ ngày hợp đồng được ký kết. </li>
    <li> Trong thời gian thực hiện chiến dịch quảng cáo, nếu Bên A muốn dừng quảng cáo cho một số ngày nhất định, Bên A cần thông báo trước cho Bên B ít nhất 03 ngày. Trong trường hợp đó, số ngày hiện quảng cáo trên hệ thống quảng cáo của Bên A sẽ được tăng thêm đúng bằng số ngày tạm dừng quảng cáo nhưng thời gian dừng không quá 1 tháng (30 ngày).</li>
    <li> Trong trường hợp Bên A chậm thanh toán cho Bên B theo quy định tại điều 3 của hợp đồng thì Bên B sẽ tạm dừng chiến dịch quảng cáo tương ứng với số ngày chậm thanh toán của Bên A. Bên B có trách nhiệm đưa quảng cáo xuất hiện trở lại 01 (một) ngày sau khi nhận được đầy đủ phần thanh toán bị chậm của Bên A.<br />
    </li>
    <li>Chiến dịch quảng cáo sẽ hoàn tất khi Bên B gửi Thông báo kết thúc dịch vụ qua email cho Bên A. Sau 2 ngày kể từ khi nhận được email từ Bên B, nếu Bên A không có phản hồi khác, chiến dịch quảng cáo được xem là kết thúc.</li>
</ul>
<br>
<strong>Điều 5: Chấm dứt hợp đồng</strong><br />
Trong trường hợp Bên A có thay đổi chiến dịch quảng cáo và muốn chấm dứt hợp đồng trước thời hạn thì Bên A phải gửi thông báo trước bằng văn bản cho Bên B trước 07 (bảy) ngày khi chính thức dừng quảng cáo và phải được Bên B chấp nhận về thời điểm dừng hợp đồng.</p>

<p>Bên B sẽ hoàn trả cho Bên A số tiền còn lại chưa sử dụng trong ngân sách quảng cáo trừ đi 50% giá trị gói chương trình khuyến mãi (nếu có) mà Bên A đã tạm ứng cho Bên B để thực hiện dịch vụ. Bên B sẽ không thực hiện hoàn trả cho Bên A giá trị phí dịch vụ quản lý tài khoản trong trường hợp Bên A chấm dứt hợp đồng trước hạn.</p>

<strong>Điều 6: Sự kiện bất khả kháng</strong><br />
Trong trường hợp có những sự kiện bất khả kháng không lường trước được, trách nhiệm và thời hạn thực hiện hợp đồng của cả hai bên sẽ được xem xét, đàm phán và quyết định lại. Các sự kiện bất khả kháng bao gồm nhưng không giới hạn các rủi ro sau:</p>
<ul>
    <li> Rủi ro do ngừng hoặc lỗi kĩ thuật từ dịch vụ hệ thống quảng cáo cung cấp</li>
    <li> Rủi ro về đường truyền internet, cơ sở hạ tầng mạng quốc gia</li>
    <li> Thiên tai, chiến tranh, khủng bố, hoả hoạn, dịch bệnh...<br /></li>
</ul>
<p>
    <strong>Điều 7: Tranh chấp và phân xử</strong><br />
    Hai bên cam kết thực hiện những điều khoản trong hợp đồng. Nếu có vướng mắc, mỗi bên thông báo cho nhau để cùng bàn bạc giải quyết trên tinh thần hợp tác, thiện chí, vì lợi ích cả hai bên. Trong trường hợp không thể giải quyết được bất đồng, tranh chấp sẽ được giải quyết tại Tòa án có thẩm quyền tại TP Hồ Chí Minh.<br />
</p>
<p>
    <strong>Điều 8: Điều khoản khác</strong><br/>Hợp đồng có hiệu lực kể từ ngày ký và được xem như tự động thanh lý khi Hai bên thực hiện đầy đủ tất cả các điều khoản nêu trong hợp đồng này.<br />
    Mọi sửa đổi hoặc bổ sung cho Hợp đồng này sẽ chỉ có hiệu lực sau khi các đại diện của cả hai bên kí kết bằng văn bản tạo thành bộ phận hợp nhất của hợp đồng.
</p>
<p>Hợp đồng này được làm thành 02 bản có giá trị ngang nhau, Bên A giữ 01 bản, Bên B giữ 01 bản.<br /></p>
<?php $this->load->view('googleads/contract/preview/footer'); ?>