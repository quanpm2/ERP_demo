<style>
.progress-group .progress-number {
    font-size: 0.8em;
}
</style>
<?php 
$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

echo $content['table']??'';
echo $content['pagination']??'';
?>
<script type="text/javascript">
$(function(){
    $(".input_daterange").daterangepicker({
        format: 'DD-MM-YYYY',
    });
})
</script>