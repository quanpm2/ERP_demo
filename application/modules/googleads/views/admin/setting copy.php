<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');

$version = 201811260931;

$has_start_permission 	= has_permission('Googleads.start_service');
$has_stop_permission	= has_permission('Googleads.stop_service');
$is_service_proc 		= is_service_proc($term);
$is_service_end 		= is_service_end($term);

?>

<style type="text/css">
    .multiselect--active {
    z-index: 1000;
}
</style>

<?php

$this->admin_form->set_col(12,6);
echo $this->admin_form->box_open();
if($has_start_permission && !$is_service_proc)
{
	$this->template->javascript->add(base_url("dist/vGoogleadsConfigurationBox.js?v={$version}"));
	echo "<div id='app-process-service-container'> <v-googleads-configuration-box term_id='{$term_id}' begin_date='{$begin_date}' /> </div>";
}

if($has_stop_permission && $is_service_proc && !$is_service_end)
{
	$this->template->javascript->add(base_url("dist/vGoogleadsConfigurationStopBox.js?v={$version}"));

	$end_date_suggest = $end_date ?: my_date(time(),'d-m-Y');
	
	echo "<div id='app-stop-process-service-container'> <v-googleads-configuration-stop-box term_id='{$term_id}' begin_date='{$begin_date}' /> </div>";

	// echo "<stop-service-component term_id='{$term_id}' end_date='{$end_date_suggest}'></stop-service-component>";
	echo "<suspend-service-component term_id='{$term_id}' end_date='{$end_date_suggest}'></suspend-service-component>";
	echo '<p class="help-block">Vui lòng cấu hình để hệ thống E-mail hoạt động</p>';
	
	$this->template->javascript->add(admin_theme_url("modules/googleads/stop-service-component.js?v={$version}"));
	$this->template->javascript->add(admin_theme_url("modules/googleads/suspend-service-component.js?v={$version}"));
}


echo $this->admin_form->box_close();

# CURATORS SETTING BOX
$this->admin_form->set_col(6,6);
echo $this->admin_form->form_open();

echo $this->admin_form->box_open('Thông tin nhận báo cáo Email');

$curators = @unserialize(get_term_meta_value($term->term_id,'contract_curators'));
$max_curator = $this->option_m->get_value('max_number_curators');
$max_curator = $max_curator ?: $this->config->item('max_input_curators','googleads');
for ($i=0; $i < $max_curator; $i++)
{ 
    echo $this->admin_form->formGroup_begin();
    echo '<div class="col-xs-4" style="padding-left: 0;">';
    echo form_input(['name'=>'edit[meta][curator_name][]','class'=>'form-control','placeholder'=>'Người nhận báo cáo '.($i+1)],$curators[$i]['name']??'');
    echo '</div>';
    echo '<div class="col-xs-4">';
    echo form_input(['name'=>'edit[meta][curator_email][]','class'=>'form-control','placeholder'=>'Email '.($i+1)],$curators[$i]['email']??'');
    echo '</div>';

    echo '<div class="col-xs-4">';
    echo form_input(['name'=>'edit[meta][curator_phone][]','class'=>'form-control','placeholder'=>'Phone '.($i+1)],$curators[$i]['phone']??'');
    echo '</div><br/>';
    echo $this->admin_form->formGroup_end();
}

echo $this->admin_form->box_close(['curator_submit','Lưu lại'],['button','Hủy bỏ']);
echo $this->admin_form->form_close();
# END CURATORS SETTING BOX

$this->template->javascript->add(base_url("dist/vGoogleadsConfigurationServiceBox.js?v={$version}"));
echo "<div id='app-configuration-service-box-container'><v-googleads-configuration-service-box term_id='{$term_id}'/> </div>";

/* End of file setting.php */
/* Location: ./application/modules/googleads/views/admin/setting.php */