<?php


if(!empty($term->term_id))
{
  $this->template->stylesheet->add('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
  $this->template->javascript->add('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');
  
  $this->table->set_heading(['Lịch gửi','Ngày gửi tiếp theo','Người nhận']);

  $date_start = $time_duration = $report_date = '';

  if($meta_start_date = get_term_meta_value($term->term_id,'googleads-begin_time'))
  {
    $date_start = '<b>' . my_week_name($meta_start_date) . '</b> hàng tuần';
    $time_duration = human_timing($meta_start_date);

    $_duration_day = (time() - $meta_start_date) / (24 * 60 * 60);
    $_week_count = ceil($_duration_day / 7);
    $report_time = $meta_start_date + ($_week_count * 7 * 24 * 60 * 60);
    $report_date = date('d-m-Y', $report_time);
  }

  $term->curators = array();

  if($term->curators = get_term_meta_value($term->term_id,'contract_curators'))
  {
    $term->curators = @unserialize($term->curators);
  }

  $sms_ouput['phone_curators'] = 'Chưa cập nhật';

  if(!empty($term->curators))
  {
    
    $sms_ouput['phone_curators'] = implode('<br/>', array_map(function($x){
      if(!empty($x['email'])) return $x['email'] . ' (' . $x['name'] . ')';
      return '';
    }, $term->curators));
  }

  $this->table->add_row("{$date_start}<br/>Hoạt động: {$time_duration}",$report_date,$sms_ouput['phone_curators']);
  echo $this->table->generate();
}

if(!empty($content))
{
  echo $content['table'] . $content['pagination'];
}
?>