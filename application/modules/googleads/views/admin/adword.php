sss<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
?>
<style scoped> .progress-group .progress-number { font-size: 0.8em !important; } </style>

<div class="row">
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-list-ul"></i> Tất cả dịch vụ</a></li>
                <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-pause"></i> Đang tạm ngưng <small>(không có dữ liệu từ Adword API)</small></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                   <?php echo $this->admin_form->datatable(admin_url('googleads/ajax/dataset'));?>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                   <?php echo $this->admin_form->datatable(admin_url('googleads/ajax/dataset?&where%5Bterm_status%5D=pending'));?>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/component/ui.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/googleads/app.js'));
/* End of file index.php */
/* Location: ./application/modules/googleads/views/admin/index.php */