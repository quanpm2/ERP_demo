<?php
$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
?>
<div class="row">
	<div class="col-md-12">
		<?php 

		echo $this->admin_form->form_open();

		# echo $this->admin_form->input('Số hợp đồng','',$this->termmeta_m->get_meta_value(@$edit->term_id,'contract_code'),'',array('disabled'=>'disabled'));

		echo $this->admin_form->dropdown('Website', 'edit[term_parent]', $avail_websites, @$edit->term_parent,'', 
			array('id'=>'website_select2'));

		echo form_hidden('edit[term_name]', @$edit->term_name);

		echo '<div id="customer_profile">'. $this->template->customer_profile .'</div>';

		echo $this->admin_form->input('Thời gian thực hiện','edit[meta][contract_daterange]',$this->termmeta_m->get_meta_value(@$edit->term_id,'contract_daterange'),'',array('class'=>'form-control','id'=>'input_daterange'));


		echo $this->admin_form->formGroup_begin(0,'Thông tin ngân hàng');

		echo '<div class="col-xs-4" style="padding-left: 0;">';

		echo form_input(array('name'=>'edit[meta][bank_id]','class'=>'form-control','placeholder'=>'Số tài khoản'),
			$this->termmeta_m->get_meta_value(@$edit->term_id,'bank_id'));

		echo '</div>';

		echo '<div class="col-xs-4">';

		echo form_input(array('name'=>'edit[meta][bank_owner]','class'=>'form-control','placeholder'=>'Chủ ngân hàng'),
			$this->termmeta_m->get_meta_value(@$edit->term_id,'bank_owner'));

		echo '</div>';

		echo '<div class="col-xs-4">';

		echo form_input(array('name'=>'edit[meta][bank_name]','class'=>'form-control','placeholder'=>'Ngân hàng'), $this->termmeta_m->get_meta_value(@$edit->term_id,'bank_name'));

		echo '</div>';

		echo $this->admin_form->formGroup_end();

		echo $this->admin_form->dropdown('Trạng thái', 'edit[term_status]', array('draft'=>'Bản nháp','publish'=>'Bản chính thức'), @$edit->term_status,'', array('id'=>'website_select2'));

		echo $this->admin_form->submit('submit','Save change');

		echo $this->admin_form->form_close();
		?>
	</div>
</div>

<script type="text/javascript">
$(function(){
	$("#input_daterange").daterangepicker();

	$('#website_select2')
	.on("change", function(e) {

		var _web_id = $(this).val();

		$.notify('Đang xử lý ...','info');

		$("#customer_profile").empty();

		if(_web_id == 0) {
			$("input[name='edit[term_name]']").val('');
			return;
		}

		$("input[name='edit[term_name]']").val($('#website_select2 option:selected').text());

		$.post('<?php echo module_url();?>render_customer_info/' + _web_id, {}, 
			function( resp ) { $("#customer_profile").append(resp.content);}, "json");
    })	
});
</script>