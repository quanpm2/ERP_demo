<?php
if(!is_service_proc($term->term_id))
{
  echo $this->admin_form->box_alert('Chức năng tạo báo cáo hiện tại đã đóng vì dịch vụ chưa được kích hoạt','warning','Cảnh báo');
  return;
}

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

echo '<div class="col-md-12">';
echo $this->admin_form->form_open();

$report_types = $this->config->item('report_type','googleads');
$label = 'Báo cáo '.$report_types[$service_type];
echo $this->admin_form->formGroup_begin(0,$label);
echo form_button('daterange','<i class="fa fa-calendar"></i> Chọn thời gian <i class="fa fa-caret-down"></i>',array('class'=>'btn btn-default', 'id'=>'daterange-btn'));
echo $this->admin_form->formGroup_end();

// if($service_type === 'cpc_type')
// {
//   $target_networks = array();
//   $target_networks['search'] = 'Google Search';
//   $target_networks['display'] = 'Google Display Network';
//   // $target_networks['all'] = 'Cả hai';

//   echo $this->admin_form->formGroup_begin();

//   foreach($target_networks as $k => $v)
//   {
//     echo $this->admin_form->radio('target_networks',$k,TRUE,"data-type='{$k}' class='radio_service_type'");
//     echo nbs(5).form_label($v).br();
//   }

//   echo $this->admin_form->formGroup_end();
// }

echo $this->admin_form->formGroup_begin();
echo $this->admin_form->submit('submit','Tạo file báo cáo');
echo form_hidden('date_range', '');
echo $this->admin_form->formGroup_end();
echo $this->admin_form->form_close();

if(!empty($files)){
  
  $this->table->set_heading('Tên', 'Kích cỡ', 'Ngày tạo');
  $this->table->set_template(array('table_open'=>'<table border="1" cellpadding="2" cellspacing="1" class="table table-bordered table-striped table-hover data-table">'));

  foreach ($files as $file) 
  {
    $path = base_url(str_replace('./','',$file['server_path']));
    $text = $file['name'];
    $anchor = '<i class="fa fa-fw fa-file-excel-o" style="color:green;"></i>'.anchor($path,$text);
    $created_on = $this->mdate->date('d/m/Y H:i:s',$file['date']);

    $this->table->add_row($anchor,$file['size'].' bytes',$created_on);
  }
  echo $this->table->generate();
}

echo '</div>';

?>
<script type="text/javascript">
$(function(){

	$('#daterange-btn').daterangepicker({
          ranges: {
            'Today': [moment(), moment()],  
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
            'Last 30 Days': [moment().subtract(30, 'days'), moment().subtract(1, 'days')],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $("input[name=date_range]").val(start.format('D-M-YYYY') + ' - ' + end.format('D-M-YYYY'));
        }
  );

  $("input[name=date_range]").val(moment().subtract(29, 'days').format('D-M-YYYY') + ' - ' + moment().format('D-M-YYYY'));
})
</script>