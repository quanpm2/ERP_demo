<?php
$term_id = $term->term_id;

if( ! is_service_proc($term_id))
{
    echo $this->admin_form->box_alert('Không có số liệu thống kê vì dịch vụ chưa được kích hoạt','warning','Cảnh báo');
    return;
}

echo $this->template->trigger_javascript('https://code.highcharts.com/highcharts.js');

$start_service_time = get_term_meta_value($term_id,'start_service_time');
$end_service_time = get_term_meta_value($term_id,'end_service_time');

$this->admin_form->set_col(6);
echo $this->admin_form->box_open(false);
$this->table->set_caption('Thông tin hợp đồng');
$staff_id = get_term_meta_value($term_id, 'staff_business');
$staff_name = $this->admin_m->get_field_by_id($staff_id, 'display_name');

$this->table->add_row('Mã hợp đồng', get_term_meta_value($term_id, 'contract_code'));
$this->table->add_row('Thời gian kích hoạt', my_date($start_service_time, 'd/m/Y'));

$start_time = get_term_meta_value($term_id,'googleads-begin_time');
$start_date = $start_time ? my_date($start_time,'d/m/Y') : 'N/a';
$this->table->add_row('Thời gian bắt đầu',$start_date);

$end_date = 'N/a';
$contract_days = 0;
if($start_time)
{
    $contract_days = $this->base_adwords_m->calc_contract_days($term_id);
    $end_time = strtotime("+{$contract_days} days -1 day",$this->mdate->startOfDay($start_time));
    $end_date = my_date($end_time,'d/m/Y');
}

$this->table->add_row('Thời gian kết thúc',$end_date);
$this->table->add_row('Thời gian',"$contract_days ngày");

$this->table->add_row('Kinh doanh phụ trách', $staff_name);

$technical_staff = '<code>chưa xác định KPI</code>';
if($kpis)
{
    $technical_staff = implode(', ', array_map(function($x){ 
        return $this->admin_m->get_field_by_id($x->user_id, 'display_name') ?: $this->admin_m->get_field_by_id($x->user_id, 'user_email');
    }, $kpis));
}
$this->table->add_row('Kỹ thuật phụ trách',$technical_staff);

$this->table->add_row('Loại', $this->config->item(get_term_meta_value($term_id,'service_type'),'googleads_service_type'));
echo $this->table->generate();
echo $this->admin_form->box_close();

$this->admin_form->set_col(6);
echo $this->admin_form->box_open(false);
$this->table->set_caption('Thông tin khách hàng');
$this->table->add_row('Người đại diện', get_term_meta_value($term_id,'representative_name'));

if($this->admin_m->role_id != 21)
{
    $this->table->add_row('Mail liên hệ', mailto(get_term_meta_value($term_id,'representative_email')));
    $this->table->add_row('Địa chỉ', get_term_meta_value($term_id,'representative_address'));
}

if(!empty($performance_report))
{
    foreach ($performance_report as $key => $item) 
    {
        $this->table->add_row("({$key}) Clicks", $item['clicks'] .'<i class="fa fa-fw fa-hand-o-up"></i>');

        $cost = $item['cost']/1000000;
        $currency = $item['currency'];
        $used_budget = currency_numberformat($cost,$currency);
        if($currency !== 'VND')
        {
            $meta_exchange_rate_key = strtolower("exchange_rate_{$currency}_to_vnd");
            $exchange_rate = get_term_meta_value($term_id, $meta_exchange_rate_key);
            if(!empty($exchange_rate))
                $used_budget.= ' ~('.currency_numberformat($cost*$exchange_rate).')';
            else $used_budget.= ' ~('.currency_numberformat(money_convert2vnd($cost, $currency)).')';
        }
        
        $this->table->add_row("({$key}) Đã dùng", $used_budget);        
    }
}

echo $this->table->generate();
echo $this->admin_form->box_close();

if($efficient_data)
{
    echo $this->admin_form->set_col(12)->box_open('Báo cáo hiệu quả','box-success');
    echo $efficient_data;
    echo $this->admin_form->box_close();
}

echo $this->admin_form->set_col(12)->box_open();
echo $charts?:'';
if($this->admin_m->role_id == 1)
{
    echo $this->admin_form->form_open();
    echo $this->admin_form->submit('','download_data','Bổ sung dữ liệu');
    echo $this->admin_form->form_close();
}
echo $this->admin_form->box_close();
?>
<style type="text/css">.tooltip-inner {text-align: left !important;}</style>
<div class="clear10"></div>