<?php

// CONFLICT
foreach($conflict_contract_chains as $conflict_at => $conflict_chains){
    $this->table->add_row('', '<b>Bắt đầu chuỗi hợp đồng trùng</b>');

    foreach($conflict_chains as $chain)
    {
        $index = array_search($chain['conflict_at'], $chain['chain']);

        $chain_left = array_slice($chain['chain'], 0, $index);
        $chain_right = array_slice($chain['chain'], $index + 1);

        $contract_str = '<p style="color: red">' . implode('_', $chain_left) . (empty($chain_left) ? '<b>' : '_<b>') . $chain['chain'][$index] . (empty($chain_right) ? '</b>' : '</b>_') . implode('_', $chain_right) . '</p>';

        $destroy_chain = array_slice($chain['chain'], $index);

        $btn_remove = '';
        if(!empty($chain['error_point']))
        {
            $directive = $chain['directive'];
            $directive = ('left' == $directive) ? ' bên trái' : (('right' == $directive) ? ' bên phải' : '');

            $btn_remove = $this->admin_form->form_open();
            $btn_remove .= $this->admin_form->_submit(['name' => 'submit_remove_contract_join'], 'Ngắt tại ' . $chain['error_point'] . $directive);
            $btn_remove .= '<input type="hidden" name="error_point" value="'. $chain['error_point'] . '">';
            $btn_remove .= '<input type="hidden" name="directive" value="'. $chain['directive'] . '">';
            $btn_remove .= $this->admin_form->form_close();
        }

        $advertise_time = [];
        array_walk($chain['detail'], function($item, $contract_id) use (&$chain, &$advertise_time){
            $advertise_start_time = my_date($item['advertise_start_time'], 'd/m/Y');
            $advertise_end_time = $item['advertise_end_time'] ? my_date($item['advertise_end_time'], 'd/m/Y') : 'hiện tại';
            $advertise_time[] = $contract_id .': ' . $advertise_start_time . ' -> ' . $advertise_end_time;
        });
        $advertise_time = implode('<br>', $advertise_time);
        $desc = implode('<br>', $chain['desc']);

        $this->table->add_row('', $contract_str . '---<br>' . $advertise_time, $desc, $btn_remove);
    }

    $this->table->add_row('', '<b>Kết thúc chuỗi hợp đồng trùng</b>');
}
echo $this->table->generate();

// WARNING
echo $this->admin_form->form_open();
foreach($warning_contract_chains as $chain){
    $index = array_search($chain['warning_at'], $chain['chain']);
    $chain_left = array_slice($chain['chain'], 0, $index);
    $chain_right = array_slice($chain['chain'], $index + 1);
    
    $desc = implode('<br>', $chain['desc']);
    $contract_str = '
    <p style="color: dodgerblue; text-decoration: underline">' 
    . '<a target="blank" href="' . admin_url('googleads/fix/fix_join_contract/') . '?chains=' . implode('_', $chain['chain']) . '&desc=' . $desc . '">'
    . implode('_', $chain_left) . (empty($chain_left) ? '<b>' : '_<b>') . $chain['chain'][$index] . (empty($chain_right) ? '</b>' : '</b>_') . implode('_', $chain_right) 
    . '</a></p>';

    $advertise_time = [];
    array_walk($chain['detail'], function($item, $contract_id) use (&$chain, &$advertise_time){
        $advertise_start_time = my_date($item['advertise_start_time'], 'd/m/Y');
        $advertise_end_time = $item['advertise_end_time'] ? my_date($item['advertise_end_time'], 'd/m/Y') : 'hiện tại';
        $advertise_time[] = $contract_id .': ' . $advertise_start_time . ' -> ' . $advertise_end_time;
    });
    $advertise_time = implode('<br>', $advertise_time);

    $checkbox = '<input type="checkbox" value="' . implode('_', $chain['chain']) . '" name="spend_join_all_contract[]">';

    $this->table->add_row($checkbox, $contract_str . '---<br>' . $advertise_time, $desc);
}
echo $this->table->generate();
echo $this->admin_form->submit('submit_spend_join_all_contract', 'Nối chuỗi hàng loạt');
echo $this->admin_form->form_close();

// NORMAL
foreach($contract_chains as $chain){
    $contract_str = implode('_', $chain['chain']);

    $advertise_time = [];
    array_walk($chain['detail'], function($item, $contract_id) use (&$chain, &$advertise_time){
        $advertise_start_time = my_date($item['advertise_start_time'], 'd/m/Y');
        $advertise_end_time = $item['advertise_end_time'] ? my_date($item['advertise_end_time'], 'd/m/Y') : 'hiện tại';
        $advertise_time[] = $contract_id .': ' . $advertise_start_time . ' -> ' . $advertise_end_time;
    });
    $advertise_time = implode('<br>', $advertise_time);
    $desc = implode('<br>', $chain['desc']);

    $btn_connect = '<a target="blank" href="' . admin_url('googleads/fix/fix_join_contract/') . '?chains=' . implode('_', $chain['chain']) . '&desc=' . $desc . '">Kết nối</a>';

    $this->table->add_row('', $contract_str . '<br>---<br>' . $advertise_time, $desc, $chain['log'], $btn_connect);
}

echo $this->table->generate();