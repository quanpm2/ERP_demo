<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#profile" data-toggle="tab" aria-expanded="false">Thông tin</a></li>
				<li class=""><a href="#website" data-toggle="tab" aria-expanded="false">Website</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="profile">
				<?php 
					$this->load->view('admin/edit_' . $user_type, $this);
				?>
				</div>
				<!-- /.tab-pane -->
				<div class="tab-pane" id="website">

				<?php
				echo $this->admin_form->form_open();
				?>

				<div class="row">
					<div class="col-xs-3">
					<?php 
					echo form_input(array('name'=>'edit[term_name]','class'=>'form-control','placeholder'=>'website domain'));
					?>
					</div>
					<div class="col-xs-3">
					<?php
					echo form_input(array('name'=>'edit[term_description]','class'=>'form-control','placeholder'=>'Mô tả'));
					?>
					</div>

					<?php
					echo $this->admin_form->submit( array('type'=>'submit','name'=>'add_website','class'=>'btn btn-info setting_options'),'Thêm mới website');
					?>

				</div>
				
				<?php
				echo $this->admin_form->form_close();
				?>

				<?php

				if(!empty($list_website)){

	    			echo $list_website['table'];

					echo $list_website['pagination'];
				}

				?>
				</div>
			</div>
			<!-- /.tab-content -->
		</div>
	</div>
</div>

<script type="text/javascript">
  $(function(){
  	$('.domain_field').editable('option','validate',function(value){
  		if(!/^(http(s)?\/\/:)?(www\.)?[a-zA-Z\-]{3,}(\.[a-z]{2,4})?$/.test(value)){
  			if(!value) return 'Tên miền không hợp lệ !';		
  		}
  		if(!value) return 'Tên miền không được để trống';
  	});
  });
</script>