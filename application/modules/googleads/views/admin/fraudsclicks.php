<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.date.extensions.js');
$this->template->javascript->add('plugins/input-mask/jquery.inputmask.extensions.js');

$this->template->javascript->add('vendors/highchart-6.0.3/highcharts.js');
$this->template->javascript->add('vendors/highchart-6.0.3/modules/exporting.js');
?>

<!-- Inegrate frauds clicks command buttonr  -->
<frauds-clicks-report-component></frauds-clicks-report-component>

<script type="text/javascript">
var baseURL = "<?php echo admin_url('');?>";
var jsobjectdata = <?php echo $jsobjectdata;?>;
</script>

<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/googleads/form-components.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/googleads/adword-tracking-integrated-component.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/googleads/frauds-clicks-report-component.js'));
echo $this->template->trigger_javascript(admin_theme_url('modules/googleads/app.js'));