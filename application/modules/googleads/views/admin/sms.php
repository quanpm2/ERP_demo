<?php

if(!empty($term->term_id))
{

  $this->template->stylesheet->add('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
  $this->template->javascript->add('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');

  $this->load->library('table');

  $this->table
  ->set_template(array('table_open'=>'<table border="1" cellpadding="2" cellspacing="1" class="table table-bordered table-hover">'));

  $this->table->set_heading(array('Lịch gửi','Ngày gửi tiếp theo','Người nhận'));

  $sms_ouput = array('date_start'=>'Ngày bắt đầu chưa cập nhật',
                'time_duration'=>'Ngưng hoạt động',
                'time_next'=>'Chưa xác định');

  if($meta_start_date = $this->termmeta_m->get_meta_value($term->term_id,'googleads-begin_time')){

    $sms_ouput['date_start'] = my_week_name($meta_start_date) . ' hàng tuần';

    $sms_ouput['time_duration'] = human_timing($meta_start_date);

    $_duration_week = (time() - $meta_start_date) / (24*60*60);

    $report_day = $meta_start_date + (( round($_duration_week / $this->config->item('sms_range_day'))) * 24 * 60 * 60);

    $sms_ouput['report_date'] = date('d-m-Y',$report_day + (24*60*60));
  }

  $term->curators = $this->termmeta_m->get_meta_value($term->term_id,'contract_curators');
  $term->curators = $term->curators ? @unserialize($term->curators) : $term->curators;

  $sms_ouput['phone_curators'] = 'Chưa cập nhật';

  if(!empty($term->curators)){
    
    $sms_ouput['phone_curators'] = implode('<br/>', array_map(function($x){
      if(!empty($x['phone'])) return $x['phone'] . ' (' . $x['name'] . ')';
      return '';
    }, $term->curators));
  }

  $this->table->add_row("{$sms_ouput['date_start']}<br/>Hoạt động: {$sms_ouput['time_duration']}",@$sms_ouput['report_date'],$sms_ouput['phone_curators']);

  echo $this->table->generate();
}

echo ($content??'');
?>