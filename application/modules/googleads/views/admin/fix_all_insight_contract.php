<h1>* Spend tính từ ngày <?php echo $start_date; ?></h1>

<p>Tổng số hợp đồng: <?php echo count($contracts); ?></p>

<?php

$this->table->set_heading('Id hợp đồng', 'Mã hợp đồng', 'Tổng spend ban đầu', 'Tổng spend trùng', 'Tổng spend sau tính toán', 'Mô tả ngắn');
foreach($contracts as $contract){
    $desc = '';
    foreach($contract['duplicated_insights'] as $ad_acocunt_id => $duplicated_insight){
        $sum_by_account = array_sum($duplicated_insight);
        $sum_by_account = currency_numberformat($sum_by_account, '');
        $desc .= "<p><b>{$ad_acocunt_id} ({$sum_by_account})</b>";
        
        foreach($duplicated_insight as $date => $insight){
            $_insight = currency_numberformat((float)$insight, '');
            $desc .= "<br/>{$date}: {$_insight}";
        }

        $desc .= '</p>';
    }

    $url = '<a target="_blank" href="' . admin_url('googleads/fix/fix_insight_contract/' . $contract['term_id']) . '?start_date=' . $start_date . '">'. $contract['term_id'] . '</a>';
    $this->table->add_row(
        $url,
        $contract['contract_code'],
        currency_numberformat((float)$contract['sum_spend'], ''),
        currency_numberformat((float)$contract['sum_spend_of_duplicated_insight'], ''),
        currency_numberformat(((float)$contract['sum_spend'] - (float)$contract['sum_spend_of_duplicated_insight']), ''),
        $desc
    );
}

echo $this->table->generate();
?>