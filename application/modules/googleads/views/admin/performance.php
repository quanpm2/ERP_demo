<?php
if(!is_service_proc($term->term_id))
{
  echo $this->admin_form->box_alert('Chức năng tạo báo cáo hiện tại đã đóng vì dịch vụ chưa được kích hoạt','warning','Cảnh báo');
  return;
}

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

echo '<div class="col-md-12">';
echo $this->admin_form->form_open();

$report_types = $this->config->item('report_type','googleads');
echo $this->admin_form->formGroup_begin();
echo form_button('daterange','<i class="fa fa-calendar"></i> Chọn thời gian <i class="fa fa-caret-down"></i>',array('class'=>'btn btn-default', 'id'=>'daterange-btn'));

echo $this->admin_form->submit('btnFilter','Filter');
echo $this->admin_form->submit(['name'=>'btnRefresh','class'=>'btn btn-primary'] ,'Load dữ liệu');
echo $this->admin_form->submit(['name'=>'btnExport','class'=>'btn btn-default'] ,'Export báo cáo');
echo form_hidden('date_range', '');
echo form_hidden('start_time', $start_time);
echo form_hidden('end_time', $end_time);
echo $this->admin_form->formGroup_end();
echo $this->admin_form->form_close();

if(!empty($files)){
  
  $this->table->set_heading('Tên', 'Kích cỡ', 'Ngày tạo');
  $this->table->set_template(array('table_open'=>'<table border="1" cellpadding="2" cellspacing="1" class="table table-bordered table-striped table-hover data-table">'));

  foreach ($files as $file) 
  {
    $path = base_url(str_replace('./','',$file['server_path']));
    $text = $file['name'];
    $anchor = '<i class="fa fa-fw fa-file-excel-o" style="color:green;"></i>'.anchor($path,$text);
    $created_on = $this->mdate->date('d/m/Y H:i:s',$file['date']);

    $this->table->add_row($anchor,$file['size'].' bytes',$created_on);
  }
  echo $this->table->generate();
}

echo '</div>';
?>


<div class="col-md-12">
	<!-- Custom Tabs -->
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
		<li class="active"><a href="#tab_campaigns" data-toggle="tab">Campaigns</a></li>
			<li><a href="#tab_adgroups" data-toggle="tab">Ad Groups</a></li>
			<li><a href="#tab_keywords" data-toggle="tab">Keywords</a></li>
			<li><a href="#tab_geo" data-toggle="tab">GEO</a></li>
		</ul>
		<div class="tab-content">

			<div class="tab-pane active" id="tab_campaigns">
			<?php echo $cam_datatable ?? ''; ?>
			</div>
			<!-- /.tab-pane -->

			<div class="tab-pane" id="tab_adgroups">	
			<?php echo $ad_group_datatable ?? ''; ?>
			</div>
			<!-- /.tab-pane -->

			<div class="tab-pane" id="tab_keywords">	
			<?php echo $keyword_datatable ?? ''; ?>
			</div>
			<!-- /.tab-pane -->

			<div class="tab-pane" id="tab_geo">	
			<?php echo $geo_datatable ?? ''; ?>
			</div>
			<!-- /.tab-pane -->
			
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- nav-tabs-custom -->
</div>

<script type="text/javascript">
var start_time = $("input[name=start_time]").val();
var end_time = $("input[name=end_time]").val();

$(function(){

	$('#daterange-btn').daterangepicker({
			ranges: 
			{
				'Today': [moment(), moment()],  
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
				'Last 30 Days': [moment().subtract(30, 'days'), moment().subtract(1, 'days')],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			startDate : moment.unix(start_time).format("MM/DD/YYYY"),
			endDate : moment.unix(end_time).format("MM/DD/YYYY"),
        },
        function (start, end) 
        {
			$("input[name=start_time]").val(start.unix());
			$("input[name=end_time]").val(end.unix());
			$("input[name=date_range]").val(start.format('D-M-YYYY') + ' - ' + end.format('D-M-YYYY'));	
        }
  	);

  	$("input[name=date_range]").val(moment().subtract(29, 'days').format('D-M-YYYY') + ' - ' + moment().format('D-M-YYYY'));
})
</script>