<?php
$rows_id 		= array();
$data 			= array();
$start_month 	= $this->mdate->startOfMonth();
$technicians 	= array_column($technicians, NULL, 'user_id');

$this->table->set_heading(array('STT','Kỹ thuật thực hiện','Loại','Hành động'));
$this->table->set_caption('Kỹ thuật thực hiện');

if(!empty($kpis))
{
	$i = 0;
	foreach ($kpis as $kpi) 
	{
		$btn_delete = anchor(module_url('kpi/'.$term_id.'/'.$kpi['kpi_id'].'/delete'), 'Xóa', 'class="btn btn-danger btn-flat btn-xs"');

		$text = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');
		if( ! empty($technicians[$kpi['user_id']]))
		{
			$_user = $technicians[$kpi['user_id']];
			$text = implode((' - '), [ $_user->role_name, $_user->display_name, $_user->user_email ]);
		}

		$type = $this->config->item($kpi['kpi_type'],'googleads_service_type');
		$this->table->add_row(++$i, $text,$type,$btn_delete);
	}
}

echo $this->table->generate();
echo $this->admin_form->form_open();
echo $this->admin_form->dropdown('Nhân viên thực hiện', 'user_id', array_map(function($_user){ return implode(' - ', [ $_user->role_name, $_user->display_name, $_user->user_email ]); }, $technicians));
echo $this->admin_form->hidden('','kpi_type', get_term_meta_value($term_id,'service_type'));
echo $this->admin_form->submit('submit_kpi','Lưu lại');
echo $this->admin_form->form_close();