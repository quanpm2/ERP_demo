<?php
$this->template->javascript->add('plugins/validate/jquery.validate.js');
$this->template->stylesheet->add('plugins/bootstrap-fileinput/css/fileinput.css');
$this->template->javascript->add('plugins/bootstrap-fileinput/js/fileinput.min.js');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');
$this->template->javascript->add('modules/googleads/adword-editor-convert-tool-component-v2.js');
?>
<div id="app_root">
	<adword-editor-convert-tool-component-v2/>
</div>

<script type="text/javascript"> var app_root = new Vue({ 
	el: '#app_root',
}); </script>