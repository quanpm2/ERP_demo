<?php 
    $this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
?>

<?php echo '<p>' . $desc . '</p>'; ?>

<?php 
    $version = time();
    
    $this->template->javascript->add(base_url("dist/vGoogleadsFixJoinContract.js?v={$version}"));
    
    $_contract = json_encode($contracts);
    $_contract_ids = json_encode($contract_ids);
    
    echo $this->admin_form->form_open();
    echo "<div id='app-googleads-fix-join-contract'><v-googleads-fix-join-contract :contract_ids='" . $_contract_ids . "' :contracts='" . $_contract . "' base_url='" . admin_url('googleads') . "'/></div>";
    echo $this->admin_form->submit('submit_spend_join', 'Xác nhận lưu');
    echo $this->admin_form->form_close();
?>