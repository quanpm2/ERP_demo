<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="60;url=<?php echo $next_url;?>" />
<title>TOP Hợp đồng chưa hiệu quả ngày <?php echo my_date(strtotime('yesterday'),'d/m/Y');?></title>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,700,500,500italic,700italic,900,900italic&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
<style type="text/css">

body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background: #39a8dd url(<?php echo theme_url('tv_overview/images/1.jpg');?>) no-repeat top center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
	
}
.gold {
	color: #fb0;
	float: left;
	font-size: 260px;
	font-weight: 700;
	line-height: 217px;
	text-shadow: 2px 2px 5px #000;
}
.hd {
	float: left;
	font-family: roboto slab;
	font-size: 50px;
	font-weight: 600;
}
.new {
	color: #fff;
	font-family: roboto slab;
	font-size: 129px;
	font-weight: 700;
	text-shadow: 2px 2px 5px #000;
}
.do{font-size:50px; text-align:center}
.title {
    color: #fff;
    font-size: 87px;
    font-weight: 100;
    line-height: 125px;
    text-align: center;
    text-shadow: 1px 1px 2px #000;
        margin: 60px 100px;
    border-bottom: 1px solid #fff;
}

.title strong {
    font-weight: 700;
}

.son {
    bottom: 100px;
    position: absolute;
    right: 0;
}
</style>
<link rel="stylesheet" href="<?php echo theme_url('tv_overview/css/animate.css');?>">
<script src="<?php echo theme_url('tv_overview/js/wow.min.js');?>"></script>

<script>
new WOW().init();
wow = new WOW({boxClass:'wow',animateClass:'animated',offset:1});
wow.init();

</script>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<div class="title" ><strong>TOP HĐ chưa hiệu quả ngày <?php echo my_date(strtotime('yesterday'),'d/m');?></div>
<div id="container" style="min-width: 250px; height: 600px; margin: 0 10px 0 10px">
<style type="text/css">
table
{
    background-color: #373a3c;
    width: 100%;
    max-width: 100%;
    margin-bottom: 1rem;
    border-collapse: collapse;
    padding: auto;
    font-size: 1.4em;
    font-family: Roboto,"Helvetica Neue",Arial,sans-serif;
    line-height: 1.5;
}
.table-inverse {
    color: #eceeef;
    background-color: #373a3c;
}
.table-inverse > td{
    color:white !important;
}
th,td{
    padding: .75em !important;
}
tbody tr:nth-child(n+1)
{
    background-color: red;
}
tbody tr:nth-child(n+4)
{
    background-color: #af2727;
}
tbody tr:nth-child(n+9)
{
    
    background-color: #6f1f1f;
}

td:nth-child(n+3)
{
    text-align: right;
}
</style>
<?php
echo $content;
?>
<!-- <img src="<?php echo theme_url('tv_overview/images/The_death.png');?>" class="son"/> -->
</div>
</body>
</html>
