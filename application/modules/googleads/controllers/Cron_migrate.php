<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Cron_migrate extends Public_Controller 
{
	public function __construct() 
	{
        $this->autoload['models'][] = 'googleads/googleads_m';

		parent::__construct();
	}

	public function index() 
	{
        $googleads = $this->googleads_m->set_term_type()->select('term_id')->as_array()->get_all();
        $googlead_ids = array_reduce($googleads, function($result, $item){
            $term_id = $item['term_id'];
            
            $is_migrated = (bool)get_term_meta_value($term_id, 'is_migrated');
            if(!$is_migrated){
                $result[] = $term_id;
            }

            return $result;
        }, []);
        

        $this->load->config('amqps');
		$amqps_host 	= $this->config->item('host', 'amqps');
		$amqps_port 	= $this->config->item('port', 'amqps');
		$amqps_user 	= $this->config->item('user', 'amqps');
		$amqps_password = $this->config->item('password', 'amqps');
		$queue          = 'googleads.event.default';

		$connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
		$channel 	= $connection->channel();
		$channel->queue_declare($queue, false, true, false, false);

        foreach ($googlead_ids as $contract_id)
        {
            $payload = [
				'event' => 'googleads.migrate.spend',
				'contract_id'	=> (int) $contract_id
			];

            $message = new \PhpAmqpLib\Message\AMQPMessage(
                json_encode($payload),
                array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
            );
            $channel->basic_publish($message, '', $queue);	
        }

		$channel->close();
		$connection->close();

        return true;
	}
}
/* End of file Cron_receipt.php */
/* Location: ./application/modules/Cron/controllers/Cron_receipt.php */
