<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetSync extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'googleads/googleads_m';

        parent::__construct();

        $this->load->config('googleads/googleads');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        // if (!has_permission('contract.receipt.access')) {
        //     $response['msg'] = 'Quyền truy cập không hợp lệ.';
        //     return parent::response($response);
        // }

        $args = wp_parse_args(parent::get(), [
            'offset' => 0,
            'per_page' => 20,
            'cur_page' => 1,
            'is_filtering' => true,
            'is_ordering' => true
        ]);

        $this->filtering();

        $data['content'] = $this
            ->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')

            ->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])
            ->add_search('result_updated_on', ['placeholder' => 'Ngày đồng bộ', 'class' => 'form-control set-datepicker'])
            ->add_search('time_next_api_check', ['placeholder' => 'Ngày đồng bộ tiếp theo', 'class' => 'form-control set-datepicker'])

            ->add_column('contract_code', array('title' => 'Mã hợp đồng', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('result_updated_on', array('title' => 'Ngày đồng bộ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('time_next_api_check', array('title' => 'Ngày đồng bộ tiếp theo', 'set_select' => FALSE, 'set_order' => TRUE))

            ->add_column_callback('term_id', function ($data, $row_name) {
                $result_updated_on = $data['result_updated_on'];
                $data['result_updated_on'] = $result_updated_on ? date('h:i:s d/m/Y', $result_updated_on) : '';

                $time_next_api_check = $data['time_next_api_check'];
                $data['time_next_api_check'] = $time_next_api_check ? date('h:i:s d/m/Y', $time_next_api_check) : '';

                return $data;
            }, FALSE)

            ->select("term.term_id as term_id")
            ->select("max(if(contract_code.meta_key = 'contract_code', contract_code.meta_value, null)) as contract_code")
            ->select("max(if(result_updated_on.meta_key = 'result_updated_on', result_updated_on.meta_value, null)) as result_updated_on")
            ->select("max(if(time_next_api_check.meta_key = 'time_next_api_check', time_next_api_check.meta_value, null)) as time_next_api_check")

            ->from('term')

            ->join('termmeta as contract_code', 'contract_code.term_id = term.term_id and contract_code.meta_key = "contract_code"', 'LEFT')
            ->join('termmeta as result_updated_on', 'result_updated_on.term_id = term.term_id and result_updated_on.meta_key = "result_updated_on"', 'LEFT')
            ->join('termmeta as time_next_api_check', 'time_next_api_check.term_id = term.term_id and time_next_api_check.meta_key = "time_next_api_check"', 'LEFT')
            ->join('termmeta started_service', 'started_service.term_id = term.term_id', 'INNER')

            ->where('term.term_type', $this->googleads_m->term_type)
            ->where_in('term.term_status', 'publish')
            ->where('started_service.meta_key ', 'started_service')

            ->group_by('term.term_id')

            ->order_by('[custom-select].result_updated_on', 'DESC');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            // $this->export_receipt($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering()
    {
        restrict('contract.revenue.access');

        $args = parent::get(NULL, TRUE);

        // Flter and Sort contract_code 
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        if ($filter_contract_code) {
            $this->datatable_builder->having("contract_code like '%". $this->db->escape_like_str(trim($filter_contract_code)) ."%'");
        }

        $order_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($order_contract_code) {
            $this->datatable_builder->order_by("[custom-select].contract_code", $order_contract_code);
        }

        // Flter and Sort result_updated_on 
        $filter_result_updated_on = $args['where']['result_updated_on'] ?? FALSE;
        if ($filter_result_updated_on) {
            $dates = explode(' - ', $filter_result_updated_on);

            $this->datatable_builder->having("result_updated_on >=", start_of_day(reset($dates)));
            $this->datatable_builder->having("result_updated_on <=", end_of_day(end($dates)));
        }

        $order_result_updated_on = $args['order_by']['result_updated_on'] ?? FALSE;
        if ($order_result_updated_on) {
            $this->datatable_builder->order_by("[custom-select].result_updated_on", $order_result_updated_on);
        }

        // Flter and Sort time_next_api_check 
        $filter_time_next_api_check = $args['where']['time_next_api_check'] ?? FALSE;
        if ($filter_time_next_api_check) {
            $dates = explode(' - ', $filter_time_next_api_check);

            $this->datatable_builder->having("time_next_api_check >=", start_of_day(reset($dates)));
            $this->datatable_builder->having("time_next_api_check <=", end_of_day(end($dates)));
        }

        $order_time_next_api_check = $args['order_by']['time_next_api_check'] ?? FALSE;
        if ($order_time_next_api_check) {
            $this->datatable_builder->order_by("[custom-select].time_next_api_check", $order_time_next_api_check);
        }
    }
}
/* End of file DatasetReceipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetReceipts.php */