<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'vendor/autoload.php';

class Test extends MREST_Controller
{
    function __construct()
    {
        $this->autoload['models'][] = 'term_posts_m';
        $this->autoload['models'][] = 'ads_segment_m';
        $this->autoload['models'][] = 'googleads/mcm_account_m';
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'googleads/base_adwords_m';

        parent::__construct();
    }

    public function migrate_get()
    {
        $contract_id = '47097';

        $contract     = (new googleads_m())->set_contract($contract_id);
        if (!$contract) {
            return parent::responseHandler([], 'Không tìm thấy dữ liệu của hợp đồng', 'warning', 400);
        }

        $adsSegments    = $this->term_posts_m->get_term_posts($contract_id, $this->ads_segment_m->post_type);
        if (empty($adsSegments)) {
            return parent::responseHandler([], 'Không tìm thấy dữ liệu spend của hợp đồng', 'warning', 400);
        }

        $insights = [];
        foreach ($adsSegments as $adsSegment) {
            $adaccount = $this->term_posts_m->get_post_terms($adsSegment->post_id, $this->mcm_account_m->term_type);
            if (empty($adaccount)) continue;

            $adaccount  = reset($adaccount);

            $_segment_start = $adsSegment->start_date;
            $_segment_end = $adsSegment->end_date ?: time();

            $_start = max(strtotime('2021-01-01'), $_segment_start);
            $_end   = min(time(), $_segment_end);

            if ($_start > $_end) continue;

            $this->base_adwords_m->set_mcm_account($adaccount->term_id);

            $account_data = $this->base_adwords_m->get_cache('ACCOUNT_PERFORMANCE_REPORT', $_start, $_end);
            $account_data and $account_data = array_filter($account_data);

            if (empty($account_data)) continue;

            foreach ($account_data as $timestamp => $accounts) {
                if (empty($accounts)) continue;

                $insights = array_merge($insights, $accounts);
            }

            $insights = array_group_by($insights, 'day');
    
            $insights = array_map(function ($_insights) {
                $_insight = reset($_insights);
    
                return [
                    'currency' => $_insight['currency'],
                    'account' => $_insight['account'],
                    'day' => $_insight['day'],
    
                    'impressions' => array_sum(array_column($_insights, 'impressions')),
                    'clicks' => array_sum(array_column($_insights, 'clicks')),
                    'invalidClicks' => array_sum(array_column($_insights, 'invalidClicks')),
                    'cost' => array_sum(array_column($_insights, 'cost')),
                    'avgCPC' => div(array_sum(array_column($_insights, 'cost')), array_sum(array_column($_insights, 'clicks'))),
                    'conversions' => array_sum(array_column($_insights, 'conversions')),
                    'allConv' => array_sum(array_column($_insights, 'allConv')),
                    'network' => 'all',
                ];
            }, $insights);
    
            $first_row = reset($insights);
            $has_date = isset($first_row['day']);
            if (!$has_date) return true;
    
            $days = array_unique(array_column($insights, 'day'));
    
            $_isegments = $this->term_posts_m->get_term_posts($adaccount->term_id, $this->insight_segment_m->post_type, [
                'fields' => 'posts.post_id, posts.end_date',
                'where' => [
                    'end_date >=' => start_of_day(min($days)),
                    'end_date <=' => end_of_day(max($days)),
                    'post_name' => 'day'
                ]
            ]);
    
            $_isegments and $_isegments = array_column($_isegments, null, 'end_date');
    
            foreach ($insights as $data) {
                $datetime = start_of_day($data['day']);
                $_isegment = $_isegments[$datetime] ?? null;
    
                if (empty($_isegment)) {
                    $_isegmentInsert     = [
                        'post_name'     => 'day',
                        'post_type'     => 'insight_segment',
                        'start_date'     => $datetime,
                        'end_date'         => $datetime
                    ];
    
                    $_isegment_id = $this->insight_segment_m->insert($_isegmentInsert);
                    $this->term_posts_m->set_term_posts($adaccount->term_id, [$_isegment_id], $this->insight_segment_m->post_type);
    
                    $_isegment = (object) $_isegmentInsert;
                    $_isegment->post_id = $_isegment_id;
                }
    
                $data['spend'] = div($data['cost'], 1000000);
                $data['cpc'] = div($data['avgCPC'], 1000000);
    
                $data['account_currency'] = $data['currency'];
                $meta_exchange_rate_key = '';
                if ('VND' != $data['account_currency']) {
                    $meta_exchange_rate_key = 'exchange_rate_' . strtolower($data['account_currency']) . '_to_vnd';
                    $exchange_rate = (int)get_post_meta_value($_isegment->post_id, $meta_exchange_rate_key);
                    empty($exchange_rate) and $data['account_currency'] == 'USD' and $exchange_rate = (int)get_term_meta_value($contract_id, 'dollar_exchange_rate');
                    empty($exchange_rate) and $exchange_rate = (int)get_term_meta_value($contract_id, $meta_exchange_rate_key);
                    empty($exchange_rate) and $exchange_rate = get_exchange_rate($data['account_currency']);
    
                    $data[$meta_exchange_rate_key] = $exchange_rate;
                    $data['spend'] = (float)$data['spend'] * $exchange_rate;
                }
    
                $item_array = ['account_currency', 'spend', 'cost', 'cpc', 'impressions', 'clicks', 'invalidClicks', 'conversions', 'network'];
                !empty($meta_exchange_rate_key) and $item_array[] = $meta_exchange_rate_key;
                $data = elements($item_array, $data);
    
                foreach ($data as $key => $value) {
                    update_post_meta($_isegment->post_id, $key, (string) $value);
                }
            }
        }

        return parent::responseHandler([], 'Done');
    }
}
/* End of file MCCReport.php */
/* Location: ./application/modules/googleads/controllers/api_v2/MCCReport.php */