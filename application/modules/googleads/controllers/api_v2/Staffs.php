<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'vendor/autoload.php';

class Staffs extends MREST_Controller
{
    /**
     * /GET List All Googleads Customers
     */
    public function customers_get()
    {
        $this->load->model([ 'googleads/googleads_m', 'googleads/googleads_kpi_m', 'googleads/mcm_account_m']);

        $saleIds = [];
        if( $saleEmails = parent::get('sale_email', TRUE) )
        {
            $saleEmails = array_map(function($x){ return trim($x); }, explode(',', $saleEmails));
            $saleIds = array_column($this->admin_m->select('user_id')->set_get_admin()->where_in('user_email', $saleEmails)->as_array()->get_all(), 'user_id');
            if(empty($saleIds)) parent::response();
            $saleIds = array_map(function($x){ return (int) $x; }, $saleIds);
        }

        $adsTechIds = [];
        if( $adsTechEmails = parent::get('gat_email', TRUE) )
        {
            $adsTechEmails = array_map(function($x){ return trim($x); }, explode(',', $adsTechEmails));

            $adsTechIds = array_column($this->admin_m->select('user_id')->set_get_admin()->where_in('user_email',$adsTechEmails)->as_array()->get_all(),'user_id');
            if(empty($adsTechIds)) parent::response();
            $adsTechIds = array_map(function($x){ return (int) $x; }, $adsTechIds);
        }
        
        empty($saleIds) OR $this->googleads_m->m_find(['key' => 'staff_business', 'compare' => 'where_in', 'value' => $saleIds]);
        empty($adsTechIds) OR $this->googleads_m->where_in('googleads_kpi.user_id', $adsTechIds);

        $terms = $this->googleads_m
        ->select('term.term_id, term.term_name, googleads_kpi.user_id')
        ->join('googleads_kpi', "googleads_kpi.term_id = term.term_id")
        ->m_find(['key' => 'mcm_client_id', 'compare' => '!=', 'value' => ''])
        ->set_default_query()
        ->get_many_by();

        if(empty($terms)) parent::response();

        $customers = $this->mcm_account_m->select('term_id')->set_term_type()
        ->get_many(array_map(function($x){ return (int) $x; }, array_column($terms, 'mcm_client_id')));

        $terms = array_group_by($terms, 'mcm_client_id');
        unset($terms[0]);

        if(empty($customers)) parent::response();

        $customers = array_map(function($x) use ($terms){
            $x->account_name    = get_term_meta_value($x->term_id, 'account_name');
            $x->currency_code   = get_term_meta_value($x->term_id, 'currency_code');
            $x->customer_id     = get_term_meta_value($x->term_id, 'customer_id');

            $x->gats = array_map(function($i){
                return [
                    'user_id'       => $i,
                    'user_email'    => $this->admin_m->get_field_by_id($i, 'user_email'),
                    'display_name'  => $this->admin_m->get_field_by_id($i, 'display_name')
                ];
            }, array_unique(array_column($terms[$x->term_id], 'user_id')));

            $x->sales = array_map(function($i){
                return [
                    'user_id'       => $i,
                    'user_email'    => $this->admin_m->get_field_by_id($i, 'user_email'),
                    'display_name'  => $this->admin_m->get_field_by_id($i, 'display_name')
                ];
            }, array_unique(array_map(function($i){ 
                return get_term_meta_value($i->term_id, 'staff_business');
            }, $terms[$x->term_id])));

            unset($x->term_id);
            return $x;
        }, $customers);

        parent::response([ 'status' => true,'data' => $customers ]);
    }
}
/* End of file MCCReport.php */
/* Location: ./application/modules/googleads/controllers/api_v2/MCCReport.php */