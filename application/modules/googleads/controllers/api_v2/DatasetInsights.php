<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetInsights extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';

        parent::__construct();

        $this->load->config('contract/receipt');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('googleads.overview.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $args = wp_parse_args(parent::get(null, true), [
            'offset' => 0,
            'per_page' => 20,
            'cur_page' => 1,
            'is_filtering' => true,
            'is_ordering' => true
        ]);

        $extra = [];
        $contractId = $args['where']['contractId'] ?? FALSE;
        if(!empty($contractId))
        {
            $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
            $manipulation_locked['is_manipulation_locked'] = (bool) $manipulation_locked['is_manipulation_locked']
                                    && TRUE == (bool)get_term_meta_value($contractId, 'is_manipulation_locked');
            $extra['manipulation_locked'] = $manipulation_locked;
        }

        $this->filtering();

        $data['content'] = $this
            ->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')

            ->select("term.term_id as contractId")
            ->select("term.term_name as contractUrl")
            ->select("term.term_type as contractType")
            ->select("term.term_status as contractStatus")
            ->select("COALESCE(ads_segment.post_id, balance_spend.post_id) as segmentId")
            ->select("COALESCE(ads_segment.post_type, balance_spend.post_type) as segmentType")
            ->select("COALESCE(ads_segment.post_status, balance_spend.post_status) as segmentStatus")
            ->select("COALESCE(ads_segment.start_date, balance_spend.start_date) as segmentStartDate")
            ->select("COALESCE(ads_segment.end_date, balance_spend.end_date) as segmentEndDate")
            ->select("mcm_account.term_id as mcmAccountTermId")
            ->select("mcm_account.term_name as mcmAccountId")
            ->select("max(if(mcm_account_metadata.meta_key = 'account_name', mcm_account_metadata.meta_value, null)) as mcmAccountName")
            ->select("mcm_account.term_status as mcmAccountStatus")
            ->select("max(if(mcm_account_metadata.meta_key = 'source', mcm_account_metadata.meta_value, null)) as mcmAccountSource")
            ->select("insights.post_id as insightId")
            ->select("COALESCE(insights.post_name, balance_spend.post_name) as insightSegmentType")
            ->select("insights.post_type as insightType")
            ->select("insights.post_status as insightStatus")
            ->select("insights.created_on as insightCreatedOn")
            ->select("insights.updated_on as insightUpdatedOn")
            ->select("COALESCE(insights.start_date, balance_spend.start_date) as insightStartDate")
            ->select("COALESCE(max(if(mcm_account_metadata.meta_key = 'source', mcm_account_metadata.meta_value, null)), balance_spend.comment_status) as insightSource")
            ->select("max(if(insight_metadata.meta_key = 'account_currency', insight_metadata.meta_value, null)) as account_currency")
            ->select("max(if(insight_metadata.meta_key = 'clicks', insight_metadata.meta_value, null)) as clicks")
            ->select("max(if(insight_metadata.meta_key = 'impressions', insight_metadata.meta_value, null)) as impressions")
            ->select("COALESCE(max(if(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, null)), balance_spend.post_content) as spend")
            ->select("MAX(IF(insight_metadata.meta_key = 'cost', insight_metadata.meta_value, NULL)) AS cost")
            ->select("MAX(IF(insight_metadata.meta_key = 'exchange_rate', insight_metadata.meta_value, NULL)) AS exchange_rate")
            ->select("tp_contract_ads_segment.post_id")
            ->select("balance_spend.post_excerpt as note")

            ->add_search('insightStartDate', ['placeholder' => 'Ngày', 'class' => 'form-control set-datepicker'])
            ->add_search('mcmAccountId', ['placeholder' => 'ID Tài khoản'])
            ->add_search('mcmAccountName', ['placeholder' => 'Tên tài khoản'])

            ->add_column('insightStartDate', array('title' => 'Ngày', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('mcmAccountId', array('title' => 'Account ID', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('mcmAccountName', array('title' => 'Tên account', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('mcmAccountStatus', array('title' => 'Trạng thái', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('mcmAccountSource', array('title' => 'Nguồn tài khoản', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('segmentStatus', array('title' => 'TT Phân đoạn', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('segmentStartDate', array('title' => 'Ng. bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('segmentEndDate', array('title' => 'Ng. kết thúc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('insightSource', array('title' => 'Nguồn số liệu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('account_currency', array('title' => 'Đơn vị tiền', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('exchange_rate', array('title' => 'Tỉ giá (VNĐ)', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('cost', array('title' => 'Chi tiêu gốc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('spend', array('title' => 'Chi tiêu (VNĐ)', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('clicks', array('title' => 'Clicks', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('impressions', array('title' => 'Hiển thị', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('insightCreatedOn', array('title' => 'Ngày tạo', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('insightUpdatedOn', array('title' => 'Ngày cập nhật', 'set_select' => FALSE, 'set_order' => TRUE))

            ->add_column_callback('segmentType', function ($data, $row_name) use ($extra){
                $data['extra'] = $extra;

                if('balance_spend' != $data['segmentType'])
                {
                    return $data;
                }

                $data['mcmAccountId'] = 'Theo hợp đồng';
                $data['mcmAccountName'] = 'Cân bằng thủ công';

                if('auto' == $data['insightSource'])
                {
                    $data['mcmAccountName'] = 'Chưa xác minh hợp đồng nối';

                    if(empty($data['note']))
                    {
                        return $data;
                    }

                    $data['mcmAccountName'] = $data['note'];
                }

                return $data;
            }, FALSE)

            ->from('term')

            ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
            ->join('posts AS balance_spend', '
                balance_spend.post_id = tp_contract_ads_segment.post_id 
                AND balance_spend.post_type = "balance_spend"', 
                'LEFT')
            ->join('posts AS ads_segment', '
                ads_segment.post_id = tp_contract_ads_segment.post_id 
                AND ads_segment.post_type = "ads_segment"', 
                'LEFT')
            ->join('term_posts AS tp_segment_mcm_account', 'tp_segment_mcm_account.post_id = ads_segment.post_id', 'LEFT')
            ->join('term AS mcm_account', '
                tp_segment_mcm_account.term_id = mcm_account.term_id 
                AND mcm_account.term_type = "mcm_account"', 
                'LEFT'
            )
            ->join('termmeta AS mcm_account_metadata', '
                mcm_account_metadata.term_id = mcm_account.term_id 
                AND meta_key IN ("source", "account_name")', 
                'LEFT'
            )
            ->join('term_posts AS tp_mcm_account_insights', 'tp_mcm_account_insights.term_id = mcm_account.term_id', 'LEFT')
            ->join('posts AS insights', '
                tp_mcm_account_insights.post_id = insights.post_id 
                AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) 
                AND insights.start_date <= IF(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) 
                AND insights.post_type = "insight_segment" 
                AND insights.post_name = "day"', 
                'LEFT'
            )
            ->join('postmeta AS insight_metadata', '
                insight_metadata.post_id = insights.post_id 
                AND insight_metadata.meta_key IN ("account_currency", "clicks", "impressions", "spend", "cost", "exchange_rate")', 
                'LEFT'
            )

            ->where('term.term_type', 'google-ads')
            ->where('( insights.post_id > 0 or balance_spend.post_id > 0 )')
            ->group_by('balance_spend.post_id, insights.post_id')

            ->order_by('balance_spend.end_date', 'DESC')
            ->order_by('insights.start_date', 'DESC');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);
        
        // If account_currency is empty OR have not any foreign currency
        // Hide exchange rate fields
        $account_currency_rows = array_column($data['rows'], 'account_currency');
        $account_currency_rows = array_filter($account_currency_rows, function($account_currency){
            return !empty($account_currency) && 'VND' != $account_currency;
        });
        if(empty($account_currency_rows))
        {
            unset($data['headings']['cost']);
            unset($data['headings']['exchange_rate']);

            foreach($data['rows'] as &$item)
            {
                unset($item['cost']);
                unset($item['exchange_rate']);
            }
        }

        $subheadings_data = $this->datatable_builder->generate(['per_page' => PHP_INT_MAX, 'cur_page' => 1]);
        $subheadings_data = $subheadings_data['rows'];
        $subheadings = array_reduce($subheadings_data, function ($result, $item) {
            $result['spend'] += (double)$item['spend'];
            $result['clicks'] += (int)$item['clicks'];
            $result['impressions'] += (int)$item['impressions'];
            $result['cost'] += div((int)$item['cost'], 1000000);

            return $result;
        }, [
            'spend' => 0,
            'clicks' => 0,
            'impressions' => 0,
            'cost' => 0,
        ]);
        $subheadings = array_map(function ($item) {
            $item .= '';
            return $item;
        }, $subheadings);
        $data['subheadings'] = $subheadings;



        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export_xlsx($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering()
    {
        restrict('googleads.overview.access');

        $args = parent::get(NULL, TRUE);
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        $contractId = $args['where']['contractId'] ?? FALSE;
        if ($contractId) {
            $this->datatable_builder->where("term.term_id", (int) $contractId);
            unset($args['where']['contractId']);
        }

        $insightStartDate = $args['where']['insightStartDate'] ?? FALSE;
        if ($insightStartDate) {
            $dates = explode(' - ', $insightStartDate);
            $dates = explode(' - ', $insightStartDate);
            if('Invalid date' != $dates[0] && 'Invalid date' != $dates[1]) {
                $start_date = start_of_day(reset($dates));
                $end_date = end_of_day(end($dates));
                
                $this->datatable_builder->where("
                    ((insights.start_date >= {$start_date} AND insights.end_date <= {$end_date})
                    OR
                    (balance_spend.start_date >= {$start_date} AND balance_spend.start_date <= {$end_date}))
                ");
            }
            unset($args['where']['insightStartDate']);
        }

        $mcmAccountId = $args['where']['mcmAccountId'] ?? FALSE;
        if ($mcmAccountId) {
            $dates = explode(' - ', $mcmAccountId);
            $this->datatable_builder->where("mcm_account.term_name like '%" . $this->db->escape_like_str(trim($mcmAccountId)) . "%'");
            unset($args['where']['mcmAccountId']);
        }

        $mcmAccountName = $args['where']['mcmAccountName'] ?? FALSE;
        if ($mcmAccountName) {
            $dates = explode(' - ', $mcmAccountName);
            $this->datatable_builder->where("mcm_account_metadata.meta_value like '%" . $this->db->escape_like_str(trim($mcmAccountName)) . "%'");
            unset($args['where']['mcmAccountName']);
        }

        if (!empty($args['where'])) {
            foreach ($args['where'] as $key => $value) {
                if (empty($value)) continue;
                if (!empty($key)) $this->datatable_builder->where($key, $value);
            }
        }
        if (!empty($args['order_by'])) {
            foreach ($args['order_by'] as $key => $value) {
                $this->datatable_builder->order_by("[custom-select].{$key}", $value);
            }
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function export_xlsx($query = '')
    {
        restrict('googleads.overview.access');

        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $data = $this->term_m->query($query)->result();
        if (!$data) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $title          = my_date(time(), 'Ymd') . '_export_google_insight_spend';
        $spreadsheet    = IOFactory::load('./files/excel_tpl/insight/google_insight_export.xlsx');
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));

        $sheet = $spreadsheet->getActiveSheet();
        $rowIndex = 4;

        foreach ($data as $data_item) {
            $col = 1;

            $insightStartDate = isset($data_item->insightStartDate) ? my_date($data_item->insightStartDate, 'd/m/Y') : '--';
            $segmentStartDate = isset($data_item->segmentStartDate) ? my_date($data_item->segmentStartDate, 'd/m/Y') : '--';
            $segmentEndDate = isset($data_item->segmentEndDate) ? my_date($data_item->segmentEndDate, 'd/m/Y') : '--';

            $mcmAccountId = $data_item->mcmAccountId;
            if('balance_spend' == $data_item->segmentType) $mcmAccountId = 'Theo hợp đồng';
            
            $mcmAccountName = $data_item->mcmAccountName;
            if('balance_spend' == $data_item->segmentType) $mcmAccountName = 'Cân bằng thủ công';

            $mcmAccountStatusEnums = [
                'unlinked' => 'Mất kết nối',
                'active' => 'Đang hoạt động',
                'unauthorized' => 'Đã xác thực (I)',
            ];
            $mcmAccountStatus = $data_item->mcmAccountStatus;
            $mcmAccountStatus = $mcmAccountStatusEnums[$mcmAccountStatus] ?? $mcmAccountStatus;

            $mcmAccountSourceEnums = [
                'linked' => 'Ủy Quyền',
                'direct' => 'Tạo Trực Tiếp',
            ];
            $mcmAccountSource = $data_item->mcmAccountSource;
            $mcmAccountSource = $mcmAccountSourceEnums[$mcmAccountSource] ?? $mcmAccountSource;

            // Set Cell
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $insightStartDate);
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $mcmAccountId ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $mcmAccountName ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $mcmAccountStatus ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $mcmAccountSource ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item->segmentStatus ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $segmentStartDate);
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $segmentEndDate);
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item->insightSource ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item->account_currency ?? '--');
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, div($data_item->cost ?? 0, 1000000));
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item->exchange_rate ?? 0);
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item->spend ?? 0);
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item->clicks ?? 0);
            $sheet->setCellValueByColumnAndRow($col++, $rowIndex, $data_item->impressions ?? 0);

            $rowIndex++;
        }

        $numbers_of_data = count($data);
        // Set Cells style for Date
        $sheet->getStyle('A' . $rowIndex . ':A' . ($numbers_of_data + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
        $sheet->getStyle('G' . $rowIndex . ':H' . ($numbers_of_data + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);

        // Set Cells Style Currency
        $sheet->getStyle('K' . $rowIndex . ':N' . ($numbers_of_data + $rowIndex))
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        $folder_upload  = 'files/insight/google/';
        if (!is_dir($folder_upload)) {
            try {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}/{$title}.xlsx";
        try {
            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save($fileName);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetReceipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetReceipts.php */