<?php

use Google\Service\ShoppingContent\ReturnShipment;

defined('BASEPATH') OR exit('No direct script access allowed');

class Contracts extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'googleads/mcm_account_m';
        $this->autoload['models'][] = 'googleads/adwords_report_m';
        $this->autoload['models'][] = 'log_m';
        $this->autoload['models'][] = 'option_m';

        parent::__construct($config);
    }


    /**
     * Load Data Of Contract
     *
     * @param      int   $id     The identifier
     */
    public function index_get($id = 0)
    {
        $contract = $this->googleads_m->set_term_type()->get($id);
        if(empty($contract)) parent::response(['status' => false, 'data' => null]);

        $networkTypes = get_term_meta_value($id, 'network_type') ?: [];
        $networkTypes AND $networkTypes = explode(',', $networkTypes);
        $contract->network_type = $networkTypes;

        $contract->term_id              = (int) $contract->term_id;
        $contract->term_parent          = (int) $contract->term_parent;
        $contract->term_count           = (int) $contract->term_count;
        $contract->mcm_client_id        = (int) get_term_meta_value($id, 'mcm_client_id');
        $contract->service_type         = get_term_meta_value($id, 'service_type');
        $contract->number_of_payments   = (int)(get_term_meta_value($id, 'number_of_payments') ?: 1);

        $contract->adaccounts = get_term_meta($id, 'adaccounts', FALSE, TRUE);
        $contract->adaccounts AND $contract->adaccounts = array_filter(array_map('intval', $contract->adaccounts));

        $contract->contract_budget_payment_type = get_term_meta_value($id, 'contract_budget_payment_type');
        $contract->contract_budget_customer_payment_type = get_term_meta_value($id, 'contract_budget_customer_payment_type');

        $contract->service_fee_payment_type = get_term_meta_value($id, 'service_fee_payment_type');

        $contract->mcm_client_id        = (int) get_term_meta_value($id, 'mcm_client_id');
        
        $contract->advertise_start_time = (int) get_term_meta_value($id, 'advertise_start_time');
        $contract->advertise_end_time   = (int) get_term_meta_value($id, 'advertise_end_time');
        $contract->googleads_begin_time = (int) get_term_meta_value($id, 'googleads-begin_time');
        $contract->googleads_end_time   = (int) get_term_meta_value($id, 'googleads-end_time');

        $contract->contract_code        = get_term_meta_value($id, 'contract_code');
        $contract->actual_budget        = (int) get_term_meta_value($id, 'actual_budget');

        $contract->service_fee          = (int) get_term_meta_value($id, 'service_fee');
        $contract->contract_budget      = (int) get_term_meta_value($id, 'contract_budget');

        $contract->exchange_rates = [];
        $this->load->model('option_m');
        $exchange_rates = $this->option_m->get_value('exchange_rate', TRUE);
        if( ! empty($exchange_rates))
        {
            foreach($exchange_rates as $currency => $exchange_rate){
                $meta_key = 'exchange_rate_' . strtolower($currency);
    
                $meta_value = (double) get_term_meta_value($id, $meta_key) ?? 0;
                if(!empty($meta_value)) $contract->exchange_rates[$meta_key] = $meta_value;
            }
        }

        $contract->contract_cpc         = 0;
        $contract->contract_clicks      = 0;
        $contract->gdn_contract_cpc     = 0;
        $contract->gdn_contract_clicks  = 0;
        
        if('cpc' == $contract->service_type)
        {
            $contract->contract_cpc           = (int) get_term_meta_value($id, 'contract_cpc');
            $contract->contract_clicks        = (int) get_term_meta_value($id, 'contract_clicks');
            $contract->gdn_contract_cpc       = (int) get_term_meta_value($id, 'gdn_contract_cpc');
            $contract->gdn_contract_clicks    = (int) get_term_meta_value($id, 'gdn_contract_clicks');
        }

        /**
         * Danh sách chương trình khuyến mãi hợp đồng đang áp dụng
         */
        $_promotions = get_term_meta_value($id, 'promotions');
        if($_promotions)
        {
            $_promotions = unserialize($_promotions);
            $_promotions = array_map(function($x){
                $x['value'] = (double) $x['value'];
                return $x;
            }, $_promotions);
        }
        $contract->promotions = $_promotions;
        $contract->apply_discount_type = get_term_meta_value($id, 'apply_discount_type') ?: 'last_invoice';

        $contract->is_service_proc    = (bool) is_service_proc($contract);
        $contract->is_service_end     = (bool) is_service_end($contract);

        $contract_m = (new googleads_m())->set_contract($contract);
        $contract->actual_result = (double) $contract_m->get_behaviour_m()->get_actual_result();

        $contract_curators = get_term_meta_value($id, 'contract_curators');
        $contract_curators = is_serialized($contract_curators) ? unserialize($contract_curators) : [];
        $contract->curators = $contract_curators;

        $contract->is_display_promotions_discount =  (int) get_term_meta_value($id, 'is_display_promotions_discount');

        $result_updated_on      = get_term_meta_value($id, 'result_updated_on');
        $contract->result_updated_on = $result_updated_on ? date('h:i:s d/m/Y', $result_updated_on) : '--';
        
		$time_next_api_check  = get_term_meta_value($id, 'time_next_api_check');
        $contract->time_next_api_check = $time_next_api_check ? date('h:i:s d/m/Y', $time_next_api_check) : '--';

        $this->googleads_m->set_contract($contract);

        $contract->balance_spend = $this->googleads_m->getBalanceSpend();

        $contract->balanceBudgetReceived = 0;
        $join_balance_spend = $this->term_posts_m->get_term_posts($id, $this->balance_spend_m->post_type, [
            'where' => [
                'post_title' => 'join_command', 
                'comment_status' => 'auto'
            ]
        ]);
        if(!empty($join_balance_spend))
        {
            foreach($join_balance_spend as $_join_balance_spend)
            {
                $_join_balance_spend = (array) $_join_balance_spend;
                
                $join_direction = get_post_meta_value($_join_balance_spend['post_id'], 'join_direction');
                if('from' != $join_direction)
                {
                    continue;
                }

                $contract->balanceBudgetReceived = (int) $_join_balance_spend['post_content'];
            }
        }

        $contract->segments = $contract_m->getSegments();
        $contract->segments = array_map(function($segment){
            $segment['assigned_technician_id'] = $segment['post_author'];
            $segment['assigned_technician_rate'] = $segment['post_content'];

            unset($segment['post_author']);
            unset($segment['post_content']);
            
            return $segment;
        }, $contract->segments);

        $service_fee_plan = get_term_meta_value($id, 'service_fee_plan');
        $service_fee_plan = is_serialized($service_fee_plan) ? unserialize($service_fee_plan) : [];
        $service_fee_plan = array_values($service_fee_plan);
        $service_fee_plan = array_map(function($item){
            $item['guid'] = uniqid('service_fee_plan_');

            return $item;
        }, $service_fee_plan);
        $contract->service_fee_plan = $service_fee_plan;

        $this->load->model('staffs/admin_m');
        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        if(!empty($manipulation_locked['manipulation_locked_by']))
        {
            $manipulation_locked['manipulation_locked_by_name'] = $this->admin_m->get_field_by_id($manipulation_locked['manipulation_locked_by'], 'display_name');
        }

        if(!empty($manipulation_locked['manipulation_unlocked_by']))
        {
            $manipulation_locked['manipulation_unlocked_by_name'] = $this->admin_m->get_field_by_id($manipulation_locked['manipulation_unlocked_by'], 'display_name');
        }
        $is_manipulation_unlocked = (bool) $manipulation_locked['is_manipulation_locked']
                                  && TRUE == (bool)get_term_meta_value($id, 'is_manipulation_locked');
        $manipulation_locked['is_manipulation_locked'] = $is_manipulation_unlocked;
        $contract->manipulation_locked = $manipulation_locked;

        parent::response([ 'status' => true, 'data' => $contract ]);
    }

    /**
     * Update Googleads Contract
     *
     * @param      integer  $id     The identifier
     */
    public function index_put($id)
    {
        $contract = $this->googleads_m->set_term_type()->get($id);
        if(empty($contract)) parent::response(['status' => false, 'data' => null]);

        if(FALSE === $this->contract_m->has_permission($id, 'admin.contract.update'))
        {
            parent::response('Không thể thực hiện tác vụ, quyền truy cập bị hạn chế', parent::HTTP_NOT_ACCEPTABLE);
        }

        $this->load->library('form_validation');

        if($promotions = parent::put('promotions', TRUE))
        {
            $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
            $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                              && TRUE == (bool)get_term_meta_value($id, 'is_manipulation_locked');
            if($is_manipulation_locked)
            {
                $started_service = (int) get_term_meta_value($id, 'started_service') ?: time();
                $started_service = end_of_day($started_service);
                $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
                if($manipulation_locked_at > $started_service)
                {
                    $manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                    parent::response([ 'code' => parent::HTTP_NOT_ACCEPTABLE, 'error' => "Hợp đồng đã khoá thao tác lúc {$manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá."]);
                }
            }
            
            $args = parent::put(null, TRUE);
            $this->form_validation->set_data($args);
            $this->form_validation->set_rules('apply_discount_type', 'apply_discount_type', 'required|in_list[average,last_invoice]');
            if(FALSE == $this->form_validation->run())
            {
                parent::response([ 'code' => 400, 'error' => $this->form_validation->error_array() ]);
            }

            foreach ($promotions as $promotion)
            {
                $this->form_validation->set_data($promotion);
                $this->form_validation->set_rules('name', 'name', 'required');
                $this->form_validation->set_rules('value', 'value', 'required|numeric');
                $this->form_validation->set_rules('unit', 'unit', 'required|in_list[percentage,number]');

                if($this->form_validation->run() != FALSE) continue;

                parent::response([
                    'code' => 400,
                    'error' => $this->form_validation->error_array()
                ]);
            }

            $contract = (new contract_m)->set_contract($id);

            update_term_meta($id, 'apply_discount_type', $args['apply_discount_type']);
            update_term_meta($id, 'promotions', serialize($promotions));
            update_term_meta($id, 'discount_amount', (double) $contract->get_behaviour_m()->calc_disacount_amount());
            update_term_meta($id, 'contract_value', (double) $contract->get_behaviour_m()->calc_contract_value());
            update_term_meta($id, 'service_fee_rate_actual', (double) $contract->get_behaviour_m()->calc_service_fee_rate_actual());
            
            $contract->delete_all_invoices(); // Delete all invoices available
            $contract->create_invoices(); // Create new contract's invoices
            $contract->sync_all_amount(); // Update all invoices's amount & receipt's amount

            $messages[] = 'Đợt thanh toán đã đồng bộ thành công !';
            $messages[] = 'Số liệu thu chi đã được đồng bộ thành công !';
        }


        if($curators = parent::put('curators', TRUE))
        {
            foreach ($curators as $curator)
            {
                $this->form_validation->set_data($curator);
                $this->form_validation->set_rules('name', 'name', 'required');
                $this->form_validation->set_rules('email', 'value', 'valid_email');
                $this->form_validation->set_rules('phone', 'unit', 'min_length[9]|max_length[12]|numeric');

                if($this->form_validation->run() != FALSE) continue;

                parent::response([ 'code' => 400, 'error' => $this->form_validation->error_array() ]);
            }

            update_term_meta($id, 'contract_curators', serialize($curators));
        }

        if($service_fee_plan = parent::put('service_fee_plan', TRUE))
        {
            $args = parent::put(null, TRUE);
            $this->form_validation->set_data($args);

            foreach ($service_fee_plan as $index => $service_fee)
            {
                $this->form_validation->set_data($service_fee);

                // Validate from
                $this->form_validation->set_rules('from', 'from', 
                    [
                        'required', 
                        'numeric', 
                        [
                            'handle_from_value', function($value) use ($service_fee_plan, $index){
                                if(0 == $index) return TRUE;

                                $prev_to = $service_fee_plan[$index - 1]['to'];
                                return (int)$value == (int)$prev_to + 1;
                            },
                        ]
                    ]
                );
                $this->form_validation->set_message('handle_from_value', '"Mức ngân sách từ" phải liên tục.');

                // Validate to
                $this->form_validation->set_rules('to', 'to', 
                    [
                        'required', 
                        'numeric', 
                        [
                            'handle_to_value', function($value) use ($service_fee_plan, $index){
                                if (($index == count($service_fee_plan) - 1) && 0 == $value) return TRUE;

                                return ((int)$service_fee_plan[$index]['from']) < (int)$value;
                            },
                        ]
                    ]
                );
                $this->form_validation->set_message('handle_to_value', '"Mức ngân sách đến" phải lớn hơn "Mức ngân sách từ".');

                // Validate service_fee_rate
                $this->form_validation->set_rules('service_fee_rate', 'service_fee_rate', 
                    [
                        'required', 
                        'numeric', 
                        [
                            'handle_service_fee_rate_value', function($value) use ($service_fee_plan, $index){
                                if(0 == $index) return TRUE;

                                $prev_service_fee_rate = $service_fee_plan[$index - 1]['service_fee_rate'];
                                return (int)$prev_service_fee_rate > (int)$value;
                            },
                        ]
                    ]
                );
                $this->form_validation->set_message('handle_service_fee_rate_value', '"% Phí dịch vụ" phải lớn hơn giá trị trước đó.');

                if($this->form_validation->run() != FALSE) continue;

                parent::response([
                    'code' => 400,
                    'error' => $this->form_validation->error_array()
                ]);
            }

            update_term_meta($id, 'service_fee_plan', serialize($service_fee_plan));

            $service_fee_rate = array_column($service_fee_plan, 'service_fee_rate');
            $service_fee_rate = array_map(function($item){ return (int)$item; }, $service_fee_rate);
            $original_service_fee_rate = div(max($service_fee_rate), 100);
            update_term_meta($id, 'original_service_fee_rate', $original_service_fee_rate);

            $contract_budget = array_column($service_fee_plan, 'to');
            $contract_budget = reset($contract_budget);
            update_term_meta($id, 'contract_budget', $contract_budget);

            $service_fee = $original_service_fee_rate * $contract_budget;
            update_term_meta($id, 'service_fee', $service_fee);

            $service_fee_rate_actual = (int)get_term_meta_value($id, 'service_fee_rate_actual');
            if($service_fee_rate_actual <= 0){
                update_term_meta($id, 'service_fee_rate_actual', $original_service_fee_rate);
            }

            $contract_value = $service_fee + $contract_budget;
            update_term_meta($id, 'contract_value', $contract_value);

            (new contract_m())->set_contract($id)->get_behaviour_m()->recalc_receipt();

            $messages[] = 'Mức % phí dịch vụ cập nhật thành công !';
        }

        parent::response([ 'code' => 200 , 'messages' => $messages]);
    }

    /**
     * Get all activated by Cid
     *
     * @param      int   $cid    The cid
     */
    public function activated_by_cid_get($cid = 0)
    {
        $args = parent::get();

        $m_args = array(
            'key' => 'mcm_client_id', 'value' => (int) $cid, 'compare' => '=' 
        );

        $contracts = $this->googleads_m->select('term.term_id, term_name, term_status, term_status, term_type')->m_find($m_args)->set_term_type()
        ->where_in('term_status', ['pending', 'publish'])
        ->get_all();

        $contracts AND $contracts = array_map(function($contract){

            $contract->term_id = (int) $contract->term_id;
            $contract->mcm_client_id = (int) get_term_meta_value($contract->term_id, 'mcm_client_id');
            $contract->googleads_begin_time = (int) get_term_meta_value($contract->term_id, 'googleads-begin_time');
            $contract->googleads_end_time = (int) get_term_meta_value($contract->term_id, 'googleads-end_time');
            $contract->contract_code = get_term_meta_value($contract->term_id, 'contract_code');
            $contract->actual_budget = (int) get_term_meta_value($contract->term_id, 'actual_budget');
            
            $contract_m = (new contract_m())->set_contract($contract);
            $contract->actual_result = (double) $contract_m->get_behaviour_m()->get_actual_result();

            return $contract;

        }, $contracts);
        
        parent::response([ 'status' => true,
            'data' => [
                'isActive' => ! empty($contracts),
                'contracts' => $contracts
            ]
        ]);
    }

    /**
     * Determines if valid start date.
     *
     * @param      int   $term_id  The term identifier
     * @param      int   $cid      The cid
     */
    public function is_valid_start_date_get($term_id = 0, $cid = 0, $start_time = 0)
    {
        $contract   = new googleads_m();
        $contract->set_contract($term_id);

        parent::response([
            'status' => true,
            'data' => [
               'isValid' => empty($contracts),
               'contracts' => $contract->get_conflict_by_start_date($cid, $start_time)
            ] 
        ]);
    }

    /**
     * { function_description }
     *
     * @param      int   $cid    The cid
     */
    public function related_by_cid_get($cid = 0)
    {
        $args = wp_parse_args(parent::get(null, true), array( 'cid' => $cid,  'term_id' => null ));

        ! empty($args['term_id']) AND $this->googleads_m->where('term.term_id !=', (int) $args['term_id']);

        $contracts = $this->googleads_m->get_all_by_cid($cid);

        parent::response([ 'status' => true, 'data' => $contracts ]);
    }

    /**
     * /GET List All Googleads Customers
     */
    public function  customers_get()
    {
        $this->load->model([ 'googleads/googleads_m', 'googleads/googleads_kpi_m', 'googleads/mcm_account_m']);

        $saleIds = [];
        if( $saleEmails = parent::get('sale_email', TRUE) )
        {
            $saleEmails = array_map(function($x){ return trim($x); }, explode(',', $saleEmails));
            $saleIds = array_column($this->admin_m->select('user_id')->set_get_admin()->where_in('user_email', $saleEmails)->as_array()->get_all(), 'user_id');
            if(empty($saleIds)) parent::response();
            $saleIds = array_map(function($x){ return (int) $x; }, $saleIds);
        }

        $adsTechIds = [];
        if( $adsTechEmails = parent::get('gat_email', TRUE) )
        {
            $adsTechEmails = array_map(function($x){ return trim($x); }, explode(',', $adsTechEmails));

            $adsTechIds = array_column($this->admin_m->select('user_id')->set_get_admin()->where_in('user_email',$adsTechEmails)->as_array()->get_all(),'user_id');
            if(empty($adsTechIds)) parent::response();
            $adsTechIds = array_map(function($x){ return (int) $x; }, $adsTechIds);
        }
        
        empty($saleIds) OR $this->googleads_m->m_find(['key' => 'staff_business', 'compare' => 'where_in', 'value' => $saleIds]);
        empty($adsTechIds) OR $this->googleads_m->where_in('googleads_kpi.user_id', $adsTechIds);

        $terms = $this->googleads_m
        ->select('term.term_id, term.term_name, googleads_kpi.user_id')
        ->join('googleads_kpi', "googleads_kpi.term_id = term.term_id")
        ->m_find(['key' => 'mcm_client_id', 'compare' => '!=', 'value' => ''])
        ->set_default_query()
        ->get_many_by();

        if(empty($terms)) parent::response();

        $customers = $this->mcm_account_m->select('term_id')->set_term_type()
        ->get_many(array_map(function($x){ return (int) $x; }, array_column($terms, 'mcm_client_id')));

        $terms = array_group_by($terms, 'mcm_client_id');
        unset($terms[0]);

        if(empty($customers)) parent::response();

        $customers = array_map(function($x) use ($terms){
            $x->account_name    = get_term_meta_value($x->term_id, 'account_name');
            $x->currency_code   = get_term_meta_value($x->term_id, 'currency_code');
            $x->customer_id     = get_term_meta_value($x->term_id, 'customer_id');

            $x->gats = array_map(function($i){
                return [
                    'user_id'       => $i,
                    'user_email'    => $this->admin_m->get_field_by_id($i, 'user_email'),
                    'display_name'  => $this->admin_m->get_field_by_id($i, 'display_name')
                ];
            }, array_unique(array_column($terms[$x->term_id], 'user_id')));

            $x->sales = array_map(function($i){
                return [
                    'user_id'       => $i,
                    'user_email'    => $this->admin_m->get_field_by_id($i, 'user_email'),
                    'display_name'  => $this->admin_m->get_field_by_id($i, 'display_name')
                ];
            }, array_unique(array_map(function($i){ 
                return get_term_meta_value($i->term_id, 'staff_business');
            }, $terms[$x->term_id])));

            unset($x->term_id);
            return $x;
        }, $customers);

        parent::response([ 'status' => true,'data' => $customers ]);
    }

     /**
     * Determines if valid start date.
     *
     * @param      int   $term_id  The term identifier
     * @param      int   $cid      The cid
     */
    public function join_put($sourceId = 0, $destinationId = 0)
    {
        $args = wp_parse_args(parent::put(null, true));

        $this->load->library('form_validation');
        $this->form_validation->set_data(parent::put(null, TRUE));
        
        $this->form_validation->set_rules('origination[contractId]', null, 'required|integer');
        $this->form_validation->set_rules('origination[segments][0][post_id]', null, 'required|integer');
        $this->form_validation->set_rules('origination[actual_result]', null, 'required|integer');
        $this->form_validation->set_rules('origination[budget_balance]', null, 'required|integer');

        array_walk($args['origination']['segments'], function($value, $key){
            $this->form_validation->set_rules("origination[segments][{$key}]", null, 'required');
            $this->form_validation->set_rules("origination[segments][{$key}][post_id]", null, 'required|integer');
            $this->form_validation->set_rules("origination[segments][{$key}][adaccount_id]", null, 'required|integer');
            $this->form_validation->set_rules("origination[segments][{$key}][contract_id]", null, 'required|integer');
            $this->form_validation->set_rules("origination[segments][{$key}][cost]", null, 'required|integer');
            $this->form_validation->set_rules("origination[segments][{$key}][start_date]", null, 'required|integer');
            $this->form_validation->set_rules("origination[segments][{$key}][end_date]", null, 'integer');
        });

        $this->form_validation->set_rules('destination[contractId]', null, 'required|integer');
        $this->form_validation->set_rules('destination[segments][0][post_id]', null, 'required|integer');
        $this->form_validation->set_rules('destination[actual_result]', null, 'required|integer');
        $this->form_validation->set_rules('destination[budget_balance]', null, 'required|integer');

        array_walk($args['destination']['segments'], function($value, $key){
            $this->form_validation->set_rules("destination[segments][{$key}]", null, 'required');
            $this->form_validation->set_rules("destination[segments][{$key}][post_id]", null, 'required|integer');
            $this->form_validation->set_rules("destination[segments][{$key}][adaccount_id]", null, 'required|integer');
            $this->form_validation->set_rules("destination[segments][{$key}][contract_id]", null, 'required|integer');
            $this->form_validation->set_rules("destination[segments][{$key}][cost]", null, 'required|integer');
            $this->form_validation->set_rules("destination[segments][{$key}][start_date]", null, 'required|integer');
            $this->form_validation->set_rules("destination[segments][{$key}][end_date]", null, 'integer');
        });


        if( FALSE === $this->form_validation->run())
        {
            parent::response([
                'code' => 400,
                'error' => $this->form_validation->error_array()
            ]);            
        }

        $origination = (new googleads_m())->set_contract($args['origination']['contractId']);
        $originationContract = $origination->get_contract();
        if( ! $origination->has_ended())
        {
            if( ! $origination->can('Googleads.stop_service.update'))
            {
                parent::response([
                    'code' => parent::HTTP_BAD_REQUEST,
                    'error' => 'Không có quyền thực hiện "Kết thúc hợp đồng" này.'
                ]);
            }
        }

        $destination = (new googleads_m())->set_contract($args['destination']['contractId']);
        $destinationContract = $destination->get_contract();
        if( ! $destination->can('Googleads.start_service.update'))
        {
            parent::response([
                'code' => 400,
                'error' => 'Không có quyền thực hiện tác vụ này.'
            ]);
        }

        // Check if the service is running then stop excute and return
        if($destination->is_running())
        {
            parent::response([
                'code' => 400,
                'error' => 'Dịch vụ đã được thực hiện.'
            ]);
        }

        foreach ([ $args['origination'], $args['destination'] ] as $item)
        {
            $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
            $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                      && TRUE == (bool)get_term_meta_value($item['contractId'], 'is_manipulation_locked');
            if($is_manipulation_locked)
            {
                $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
                
                $date_manipulation_locked_at = my_date($manipulation_locked_at, 'd/m/Y');
                $contract_code = get_term_meta_value($item['contractId'], 'contract_code');

                $this->contract_m->set_contract($item['contractId']);
                $_segments = $this->contract_m->get_behaviour_m()->get_segments();

                $segments_group_by_adaccount_id = array_group_by($item['segments'], 'adaccount_id');
                $_segments_group_by_adaccount_id = array_group_by($_segments, 'adaccount_id');

                foreach($segments_group_by_adaccount_id as $adaccount_id => $segment_data)
                {
                    $_segment_data = $_segments_group_by_adaccount_id[$adaccount_id] ?? [];
                    $segment_data_start_date = array_column($segment_data, 'start_date');
                    $segment_data_start_date = array_filter($segment_data_start_date, function($item) use ($manipulation_locked_at){ return start_of_day($item) < end_of_day($manipulation_locked_at); });
                    $_segment_data_start_date = array_column($_segment_data, 'start_date');
                    $_segment_data_start_date = array_filter($_segment_data_start_date, function($item) use ($manipulation_locked_at){ return start_of_day($item) < end_of_day($manipulation_locked_at); });
                    $segment_data_start_date_diff = array_diff($segment_data_start_date, $_segment_data_start_date);
                    if(!empty($segment_data_start_date_diff))
                    {
                        parent::response([
                            'code' => 400,
                            'error' => 'Hợp đồng ' . $contract_code . ' đã khoá thao tác vào ' . $date_manipulation_locked_at . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                        ]);
                    }

                    $segment_data_end_date = array_column($segment_data, 'end_date');
                    $segment_data_end_date = array_map(function($item) { return $item > 0 ? end_of_day($item) : 0; }, $segment_data_end_date);
                    $segment_data_end_date = array_filter($segment_data_end_date, function($item) use ($manipulation_locked_at){ return $item > 0 ? (end_of_day($item) < end_of_day($manipulation_locked_at)) : FALSE; });
                    $_segment_data_end_date = array_column($_segment_data, 'end_date');
                    $_segment_data_end_date = array_filter($_segment_data_end_date, function($item) use ($manipulation_locked_at){ return $item > 0 ? (end_of_day($item) < end_of_day($manipulation_locked_at)) : FALSE; });
                    $segment_data_end_date_diff = array_diff($segment_data_end_date, $_segment_data_end_date);
                    if(!empty($segment_data_end_date_diff))
                    {
                        parent::response([
                            'code' => 400,
                            'error' => 'Hợp đồng ' . $contract_code . ' đã khoá thao tác vào ' . $date_manipulation_locked_at . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                        ]);
                    }
                }
            }

            array_walk($item['segments'], function($segment){
                $this->ads_segment_m->set_post_type()->update($segment['post_id'], array(
                    'start_date' => start_of_day($segment['start_date'] ?? ''),
                    'end_date' => !empty($segment['end_date']) ? end_of_day($segment['end_date']) : null,
                ));
            });

            /* Tiến hành đồng bộ thời gian hoạt động của hợp đồng dịch vụ quảng cáo này */
            $startMilestones        = array_column($item['segments'], 'start_date');
            $advertise_start_time   = min($startMilestones);
    
            $endMilestones          = array_column($item['segments'], 'end_date');
            $advertise_end_time     = empty(array_filter($endMilestones, function($x){ return empty($x); })) ? max($endMilestones) : null;
    
            update_term_meta($item['contractId'], 'advertise_start_time', (int) $advertise_start_time);
            update_term_meta($item['contractId'], 'advertise_end_time', (int) $advertise_end_time);

            update_term_meta($item['contractId'], 'googleads-begin_time', (int) $advertise_start_time);
            update_term_meta($item['contractId'], 'googleads-end_time', (int) $advertise_end_time);
        }

        $this->load->model('balance_spend_m');

        // Give (join to)
        $old_given_balance_spend_items = $this->balance_spend_m
        ->set_post_type()
        ->select('posts.post_id')
        ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$args['origination']['contractId']})")
        ->where('posts.comment_status', 'auto')
        ->where('posts.post_title', 'join_command')
        ->as_array()
        ->get_all();
        $old_given_balance_spend_items = array_filter($old_given_balance_spend_items, function($item){
            $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

            return 'to' == $join_direction;
        });
        if( ! empty($old_given_balance_spend_items))
        {
            $this->term_posts_m->delete_term_posts($args['origination']['contractId'], array_column($old_given_balance_spend_items, 'post_id'));
            $this->balance_spend_m->delete_many(array_column($old_given_balance_spend_items, 'post_id'));
        }

        update_term_meta($args['origination']['contractId'], 'nextContractId', $args['destination']['contractId']);
        update_term_meta($args['origination']['contractId'], 'balanceBudgetAddTo', $args['origination']['budget_balance']);
        $post_excerpt = 'Nối đến HĐ ' . get_term_meta_value($args['destination']['contractId'], 'contract_code');
        $balance_spend_join_to_date = get_term_meta_value($args['origination']['contractId'], 'advertise_end_time');
        $balance_spend_join_to_id = $this->balance_spend_m->insert([
            'post_type' => $this->balance_spend_m->post_type,
            'comment_status' => 'auto',
            'post_title' => 'join_command',
            'post_status' => 'publish',
            'post_content' => $args['origination']['budget_balance'],
            'post_excerpt' => $post_excerpt,
            'start_date' => $balance_spend_join_to_date,
            'end_date' => $balance_spend_join_to_date,
        ]);
        update_post_meta($balance_spend_join_to_id, 'join_direction', 'to');
        $this->term_posts_m->set_term_posts( $args['origination']['contractId'], [ $balance_spend_join_to_id ], $this->balance_spend_m->post_type);
        $this->log_m->insert([
            'log_type'        => 'joinContracts',
            'log_status'      => 1,
            'term_id'         => $args['origination']['contractId'],
            'log_content'     => json_encode(
                [
                    'destination_id' => $args['destination']['contractId'],
                    'given' => $args['origination']['budget_balance']
                ]
            ),
            'user_id'         => $this->admin_m->id
        ]);

        try
        {
            $contract = (new googleads_m())->set_contract($args['origination']['contractId']);
            $behaviour_m = $contract->get_behaviour_m();
            $behaviour_m->get_the_progress();
            $behaviour_m->sync_all_amount();
        }
        catch (Exception $e)
        {
            log_message('error', "[GoogleadsContracts::join] Sync amount #{$args['origination']['contractId']} error. " . json_encode(['error' => $e->getMessage()]));
        }

        // Receive (join from)
        $old_received_balance_spend_items = $this->balance_spend_m
        ->set_post_type()
        ->select('posts.post_id')
        ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$args['destination']['contractId']})")
        ->where('posts.comment_status', 'auto')
        ->where('posts.post_title', 'join_command')
        ->as_array()
        ->get_all();
        $old_received_balance_spend_items = array_filter($old_received_balance_spend_items, function($item){
            $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

            return 'from' == $join_direction;
        });
        if( ! empty($old_received_balance_spend_items))
        {
            $this->term_posts_m->delete_term_posts($args['destination']['contractId'], array_column($old_received_balance_spend_items, 'post_id'));
            $this->balance_spend_m->delete_many(array_column($old_received_balance_spend_items, 'post_id'));
        }

        update_term_meta($args['destination']['contractId'], 'previousContractId', $args['origination']['contractId']);
        update_term_meta($args['destination']['contractId'], 'balanceBudgetReceived', (-1) * $args['destination']['budget_balance']);
        $post_excerpt = 'Nối từ HĐ ' . get_term_meta_value($args['origination']['contractId'], 'contract_code');
        $balance_spend_join_from_date = get_term_meta_value($args['destination']['contractId'], 'advertise_start_time');
        $balance_spend_join_from_id = $this->balance_spend_m->insert([
            'post_type' => $this->balance_spend_m->post_type,
            'comment_status' => 'auto',
            'post_title' => 'join_command',
            'post_status' => 'publish',
            'post_content' => (-1) * $args['destination']['budget_balance'],
            'post_excerpt' => $post_excerpt,
            'start_date' => $balance_spend_join_from_date,
            'end_date' => $balance_spend_join_from_date,
        ]);
        update_post_meta($balance_spend_join_from_id, 'join_direction', 'from');
        $this->term_posts_m->set_term_posts( $args['destination']['contractId'], [ $balance_spend_join_from_id ], $this->balance_spend_m->post_type);
        $this->log_m->insert([
            'log_type'        => 'joinContracts',
            'log_status'      => 1,
            'term_id'         => $args['destination']['contractId'],
            'log_content'     => json_encode(
                [
                    'origination_id' => $args['origination']['contractId'],
                    'received' => (-1) * $args['destination']['budget_balance'],
                ]
            ),
            'user_id'         => $this->admin_m->id
        ]);

        try
        {
            $contract = (new googleads_m())->set_contract($args['destination']['contractId']);
            $behaviour_m = $contract->get_behaviour_m();
            $behaviour_m->get_the_progress();
            $behaviour_m->sync_all_amount();
        }
        catch (Exception $e)
        {
            log_message('error', "[GoogleadsContracts::join] Sync amount #{$args['destination']['contractId']} error. " . json_encode(['error' => $e->getMessage()]));
        }

        if( ! $origination->has_ended())
        {
            try
            {
                $origination->get_behaviour_m()->stop_service();
            }
            catch(Exception $e)
            {
                parent::response([ 'code' => parent::HTTP_BAD_REQUEST, 'error' => $e->getMessage() ]);
            }
        }

        // Start contract join to
        if( ! $destination->is_running())
        {
            try
            {
                $destination->get_behaviour_m()->proc_service();
            }
            catch(Exception $e)
            {
                parent::response([ 'code' => parent::HTTP_BAD_REQUEST, 'error' => $e->getMessage() ]);
            }

            // Detect if contract is the first signature (tái ký | ký mới)
            $this->load->model('contract/base_contract_m');
            $this->base_contract_m->detect_first_contract($destinationContract->term_id);
        }

        parent::response([ 'code' => parent::HTTP_OK, 'message' => "Hệ thống xử lý thành công" ]); 
    }

    
     /**
     * Determines if valid start date.
     *
     * @param      int   $term_id  The term identifier
     * @param      int   $cid      The cid
     */
    public function rejoin_put($sourceId = 0, $destinationId = 0)
    {
        $has_permission = has_permission('googleads.rejoin_contract.update') 
                          && has_permission('googleads.rejoin_contract.manage');
        if(!$has_permission)
        {
            return parent::response([
                'code' => 400,
                'error' => 'Quyền truy cập không hợp lệ.'
            ]);            
        }

        $args = wp_parse_args(parent::put(null, true));

        $this->load->library('form_validation');
        $this->form_validation->set_data(parent::put(null, TRUE));
        
        $this->form_validation->set_rules('origination[contractId]', null, 'required|integer');
        $this->form_validation->set_rules('origination[segments][0][post_id]', null, 'required|integer');
        $this->form_validation->set_rules('origination[actual_result]', null, 'required|integer');
        $this->form_validation->set_rules('origination[budget_balance]', null, 'required|integer');

        array_walk($args['origination']['segments'], function($value, $key){
            $this->form_validation->set_rules("origination[segments][{$key}]", null, 'required');
            $this->form_validation->set_rules("origination[segments][{$key}][post_id]", null, 'required|integer');
            $this->form_validation->set_rules("origination[segments][{$key}][adaccount_id]", null, 'required|integer');
            $this->form_validation->set_rules("origination[segments][{$key}][contract_id]", null, 'required|integer');
            $this->form_validation->set_rules("origination[segments][{$key}][cost]", null, 'required|integer');
            $this->form_validation->set_rules("origination[segments][{$key}][start_date]", null, 'required|integer');
            $this->form_validation->set_rules("origination[segments][{$key}][end_date]", null, 'integer');
        });

        $this->form_validation->set_rules('destination[contractId]', null, 'required|integer');
        $this->form_validation->set_rules('destination[segments][0][post_id]', null, 'required|integer');
        $this->form_validation->set_rules('destination[actual_result]', null, 'required|integer');
        $this->form_validation->set_rules('destination[budget_balance]', null, 'required|integer');

        array_walk($args['destination']['segments'], function($value, $key){
            $this->form_validation->set_rules("destination[segments][{$key}]", null, 'required');
            $this->form_validation->set_rules("destination[segments][{$key}][post_id]", null, 'required|integer');
            $this->form_validation->set_rules("destination[segments][{$key}][adaccount_id]", null, 'required|integer');
            $this->form_validation->set_rules("destination[segments][{$key}][contract_id]", null, 'required|integer');
            $this->form_validation->set_rules("destination[segments][{$key}][cost]", null, 'required|integer');
            $this->form_validation->set_rules("destination[segments][{$key}][start_date]", null, 'required|integer');
            $this->form_validation->set_rules("destination[segments][{$key}][end_date]", null, 'integer');
        });


        if( FALSE === $this->form_validation->run())
        {
            parent::response([
                'code' => 400,
                'error' => $this->form_validation->error_array()
            ]);            
        }

        $origination = (new googleads_m())->set_contract($args['origination']['contractId']);
        $originationContract = $origination->get_contract();

        $destination = (new googleads_m())->set_contract($args['destination']['contractId']);
        $destinationContract = $destination->get_contract();

        foreach ([ $args['origination'], $args['destination'] ] as $item)
        {
            $is_manipulation_locked = (bool) get_term_meta_value($item['contractId'], 'is_manipulation_locked');
            if($is_manipulation_locked)
            {
                $manipulation_locked_at = get_term_meta_value($item['contractId'], 'manipulation_locked_at') ?: end_of_day(time());
                $manipulation_locked_at = end_of_day($manipulation_locked_at);
                
                $date_manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                $contract_code = get_term_meta_value($item['contractId'], 'contract_code');
    
                $_segments = $item['segments'];
                $_segment_ids = array_column($_segments, 'post_id');
                $ads_segments = $this->ads_segment_m->set_post_type()
                    ->select('post_id, start_date, end_date')
                    ->where_in('post_id', $_segment_ids)
                    ->as_array()
                    ->get_all() ?: [];
                $ads_segments = array_combine(array_column($ads_segments, 'post_id'), $ads_segments);
    
                foreach($_segments as $_segment)
                {
                    $_segment_id = $_segment['post_id'];
                    
                    $_start_date = $_segment['start_date'] ?: end_of_day(time());
                    $_start_date = start_of_day($_start_date);
                    $start_date = $ads_segments[$_segment_id]['start_date'] ?: end_of_day(time());
                    $start_date = start_of_day($start_date);
                    if($_start_date != $start_date)
                    {
                        parent::response([
                            'code' => 400,
                            'error' => "Hợp đồng {$contract_code} đã khoá thao tác lúc {$date_manipulation_locked_at}. Vui lòng liên hệ bộ phận Kế toán để mở khoá."
                        ]);
                    }

                    $_end_date = $_segment['end_date'] ?: end_of_day(time());
                    $_end_date = end_of_day($_end_date);
                    $end_date = $ads_segments[$_segment_id]['end_date'] ?: end_of_day(time());
                    $end_date = end_of_day($end_date);
                    if( $_end_date != $end_date
                        && $_end_date < $manipulation_locked_at)
                    {
                        parent::response([
                            'code' => 400,
                            'error' => "Hợp đồng {$contract_code} đã khoá thao tác lúc {$date_manipulation_locked_at}. Vui lòng liên hệ bộ phận Kế toán để mở khoá."
                        ]);
                    }
                }
            }

            array_walk($item['segments'], function($segment){
                $this->ads_segment_m->set_post_type()->update($segment['post_id'], array(
                    'start_date' => start_of_day($segment['start_date'] ?? ''),
                    'end_date' => !empty($segment['end_date']) ? end_of_day($segment['end_date']) : null,
                ));
            });

            /* Tiến hành đồng bộ thời gian hoạt động của hợp đồng dịch vụ quảng cáo này */
            $startMilestones        = array_column($item['segments'], 'start_date');
            $advertise_start_time   = min($startMilestones);
    
            $endMilestones          = array_column($item['segments'], 'end_date');
            $advertise_end_time     = empty(array_filter($endMilestones, function($x){ return empty($x); })) ? max($endMilestones) : null;
    
            update_term_meta($item['contractId'], 'advertise_start_time', (int) $advertise_start_time);
            update_term_meta($item['contractId'], 'advertise_end_time', (int) $advertise_end_time);

            update_term_meta($item['contractId'], 'googleads-begin_time', (int) $advertise_start_time);
            update_term_meta($item['contractId'], 'googleads-end_time', (int) $advertise_end_time);
        }

        $this->load->model('balance_spend_m');

        // Give (join to)
        $old_given_balance_spend_items = $this->balance_spend_m
        ->set_post_type()
        ->select('posts.post_id')
        ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$args['origination']['contractId']})")
        ->where('posts.comment_status', 'auto')
        ->where('posts.post_title', 'join_command')
        ->as_array()
        ->get_all();
        $old_given_balance_spend_items = array_filter($old_given_balance_spend_items, function($item){
            $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

            return 'to' == $join_direction;
        });
        if( ! empty($old_given_balance_spend_items))
        {
            $this->term_posts_m->delete_term_posts($args['origination']['contractId'], array_column($old_given_balance_spend_items, 'post_id'));
            $this->balance_spend_m->delete_many(array_column($old_given_balance_spend_items, 'post_id'));
        }

        update_term_meta($args['origination']['contractId'], 'nextContractId', $args['destination']['contractId']);
        update_term_meta($args['origination']['contractId'], 'balanceBudgetAddTo', $args['origination']['budget_balance']);
        $post_excerpt = 'Nối đến HĐ ' . get_term_meta_value($args['destination']['contractId'], 'contract_code');
        $balance_spend_join_to_date = get_term_meta_value($args['origination']['contractId'], 'advertise_end_time');
        $balance_spend_join_to_id = $this->balance_spend_m->insert([
            'post_type' => $this->balance_spend_m->post_type,
            'comment_status' => 'auto',
            'post_title' => 'join_command',
            'post_status' => 'publish',
            'post_content' => $args['origination']['budget_balance'],
            'post_excerpt' => $post_excerpt,
            'start_date' => $balance_spend_join_to_date,
            'end_date' => $balance_spend_join_to_date,
        ]);
        update_post_meta($balance_spend_join_to_id, 'join_direction', 'to');
        $this->term_posts_m->set_term_posts( $args['origination']['contractId'], [ $balance_spend_join_to_id ], $this->balance_spend_m->post_type);
        $this->log_m->insert([
            'log_type'        => 'rejoinContracts',
            'log_status'      => 1,
            'term_id'         => $args['origination']['contractId'],
            'log_content'     => json_encode(
                [
                    'destination_id' => $args['destination']['contractId'],
                    'given' => $args['origination']['budget_balance']
                ]
            ),
            'user_id'         => $this->admin_m->id
        ]);

        try
        {
            $contract = (new googleads_m())->set_contract($args['origination']['contractId']);
            $behaviour_m = $contract->get_behaviour_m();
            $behaviour_m->get_the_progress();
            $behaviour_m->sync_all_amount();
        }
        catch (Exception $e)
        {
            log_message('error', "[GoogleadsContracts::rejoin] Sync amount #{$args['origination']['contractId']} error. " . json_encode(['error' => $e->getMessage()]));
        }

        // Receive (join from)
        $old_received_balance_spend_items = $this->balance_spend_m
        ->set_post_type()
        ->select('posts.post_id')
        ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$args['destination']['contractId']})")
        ->where('posts.comment_status', 'auto')
        ->where('posts.post_title', 'join_command')
        ->as_array()
        ->get_all();
        $old_received_balance_spend_items = array_filter($old_received_balance_spend_items, function($item){
            $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

            return 'from' == $join_direction;
        });
        if( ! empty($old_received_balance_spend_items))
        {
            $this->term_posts_m->delete_term_posts($args['destination']['contractId'], array_column($old_received_balance_spend_items, 'post_id'));
            $this->balance_spend_m->delete_many(array_column($old_received_balance_spend_items, 'post_id'));
        }

        update_term_meta($args['destination']['contractId'], 'previousContractId', $args['origination']['contractId']);
        update_term_meta($args['destination']['contractId'], 'balanceBudgetReceived', (-1) * $args['destination']['budget_balance']);
        $post_excerpt = 'Nối từ HĐ ' . get_term_meta_value($args['origination']['contractId'], 'contract_code');
        $balance_spend_join_from_date = get_term_meta_value($args['destination']['contractId'], 'advertise_start_time');
        $balance_spend_join_from_id = $this->balance_spend_m->insert([
            'post_type' => $this->balance_spend_m->post_type,
            'comment_status' => 'auto',
            'post_title' => 'join_command',
            'post_status' => 'publish',
            'post_content' => (-1) * $args['destination']['budget_balance'],
            'post_excerpt' => $post_excerpt,
            'start_date' => $balance_spend_join_from_date,
            'end_date' => $balance_spend_join_from_date,
        ]);
        update_post_meta($balance_spend_join_from_id, 'join_direction', 'from');
        $this->term_posts_m->set_term_posts( $args['destination']['contractId'], [ $balance_spend_join_from_id ], $this->balance_spend_m->post_type);
        $this->log_m->insert([
            'log_type'        => 'rejoinContracts',
            'log_status'      => 1,
            'term_id'         => $args['destination']['contractId'],
            'log_content'     => json_encode(
                [
                    'origination_id' => $args['origination']['contractId'],
                    'received' => (-1) * $args['destination']['budget_balance'],
                ]
            ),
            'user_id'         => $this->admin_m->id
        ]);

        try
        {
            $contract = (new googleads_m())->set_contract($args['destination']['contractId']);
            $behaviour_m = $contract->get_behaviour_m();
            $behaviour_m->get_the_progress();
            $behaviour_m->sync_all_amount();
        }
        catch (Exception $e)
        {
            log_message('error', "[GoogleadsContracts::rejoin] Sync amount #{$args['destination']['contractId']} error. " . json_encode(['error' => $e->getMessage()]));
        }

        // Recompute contract chain
        $this->_recompute_contract_chains($args['origination']['contractId']);

        parent::response([ 'code' => parent::HTTP_OK, 'message' => "Nối hợp đồng đã được cập nhật mới ."]); 
    }
    
    /**
     * Unjoin contract
     *
     * @param      int   $contractId          Contract compare
     * @param      int   $removeContractId    Contract has removed
     */
    public function unjoin_put()
    {
        $has_permission = has_permission('googleads.unjoin_contract.update') 
                          && has_permission('googleads.unjoin_contract.manage');
        if(!$has_permission)
        {
            return parent::responseHandler([
                'permisision_deny' => 'Quyền truy cập không hợp lệ.'
            ], "permisision_deny", 'error', parent::HTTP_BAD_REQUEST); 
        }

        $args = wp_parse_args(parent::put(null, true));

        $this->load->library('form_validation');
        $this->form_validation->set_data(parent::put(null, TRUE));
        
        $this->form_validation->set_rules('original_contract_id', null, 'required|integer');
        $this->form_validation->set_rules('unjoin_contract_id', null, 'required|integer');

        if( FALSE === $this->form_validation->run())
        {
            return parent::responseHandler($this->form_validation->error_array(), "Hợp đồng nối không khả dụng", 'error', parent::HTTP_BAD_REQUEST); 
        }

        $original_contract_id = $args['original_contract_id'];
        $original_contract_prev = get_term_meta_value($original_contract_id, 'previousContractId');
        $original_contract_next = get_term_meta_value($original_contract_id, 'nextContractId');

        $unjoin_contract_id = $args['unjoin_contract_id'];
        $unjoin_contract_prev = get_term_meta_value($unjoin_contract_id, 'previousContractId');
        $unjoin_contract_next = get_term_meta_value($unjoin_contract_id, 'nextContractId');

        $is_unjoin_manipulation_locked = (bool)get_term_meta_value($unjoin_contract_id, 'is_manipulation_locked');
        if($is_unjoin_manipulation_locked)
        {
            $manipulation_locked_at = get_term_meta_value($unjoin_contract_id, 'manipulation_locked_at') ?: end_of_day(time());
            $manipulation_locked_at = end_of_day($manipulation_locked_at);
            $date_manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
            $contract_code = get_term_meta_value($unjoin_contract_id, 'contract_code');

            parent::response([
                'code' => 400,
                'error' => "Hợp đồng {$contract_code} đã khoá thao tác lúc {$date_manipulation_locked_at}. Vui lòng liên hệ bộ phận Kế toán để mở khoá."
            ]);
        }

        $is_original_manipulation_locked = (bool)get_term_meta_value($original_contract_id, 'is_manipulation_locked');
        if($is_original_manipulation_locked)
        {
            $manipulation_locked_at = get_term_meta_value($original_contract_id, 'manipulation_locked_at') ?: end_of_day(time());
            $manipulation_locked_at = end_of_day($manipulation_locked_at);
            $date_manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
            $contract_code = get_term_meta_value($original_contract_id, 'contract_code');

            parent::response([
                'code' => 400,
                'error' => "Hợp đồng {$contract_code} đã khoá thao tác lúc {$date_manipulation_locked_at}. Vui lòng liên hệ bộ phận Kế toán để mở khoá."
            ]);
        }

        $directive  = '';
        if( $original_contract_next == $unjoin_contract_id 
            && $unjoin_contract_prev == $original_contract_id) 
        {
            $directive = 'right';
        }
        if( $original_contract_prev == $unjoin_contract_id 
            && $unjoin_contract_next == $original_contract_id) 
        {
            $directive = 'left';
        }
        
        if(empty($directive))
        {
            return parent::responseHandler([], "Hợp đồng nối không khả dụng", 'error', parent::HTTP_EXPECTATION_FAILED); 
        }

        $this->load->model('balance_spend_m');

        switch($directive)
        {
            // Unjoin from unjoin_contract with original_contract
            // unjoin_contract is joined to original_contract
            // In UI, original_contract is right or below unjoin_contract
            case 'right':
                // Give (join to)
                $old_given_balance_spend_items = $this->balance_spend_m
                ->set_post_type()
                ->select('posts.post_id')
                ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$original_contract_id})")
                ->where('posts.comment_status', 'auto')
                ->where('posts.post_title', 'join_command')
                ->as_array()
                ->get_all();
                $old_given_balance_spend_items = array_filter($old_given_balance_spend_items, function($item){
                    $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                    return 'to' == $join_direction;
                });
                if( ! empty($old_given_balance_spend_items))
                {
                    $this->term_posts_m->delete_term_posts($original_contract_id, array_column($old_given_balance_spend_items, 'post_id'));
                    $this->balance_spend_m->delete_many(array_column($old_given_balance_spend_items, 'post_id'));
                }
                update_term_meta($original_contract_id, 'balanceBudgetAddTo', 0);
                update_term_meta($original_contract_id, 'nextContractId', '');

                $this->log_m->insert([
                    'log_type'        => 'unjoinContracts',
                    'log_status'      => 1,
                    'term_id'         => $original_contract_id,
                    'log_content'     => serialize([
                        'unjoin_contract_id' => $unjoin_contract_id, 
                        'directive' => 'right'
                    ]),
                    'user_id'         => $this->admin_m->id
                ]);

                try
                {
                    $contract = (new googleads_m())->set_contract($original_contract_id);
                    $behaviour_m = $contract->get_behaviour_m();
                    $behaviour_m->get_the_progress();
                    $behaviour_m->sync_all_amount();
                }
                catch (Exception $e)
                {
                    log_message('error', "[GoogleadsContracts::unjoin] Sync amount #{$original_contract_id} error. " . json_encode(['error' => $e->getMessage()]));
                }

                $this->_recompute_contract_chains($original_contract_id);

                // Receive (join from)
                $old_received_balance_spend_items = $this->balance_spend_m
                ->set_post_type()
                ->select('posts.post_id')
                ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$unjoin_contract_id})")
                ->where('posts.comment_status', 'auto')
                ->where('posts.post_title', 'join_command')
                ->as_array()
                ->get_all();
                $old_received_balance_spend_items = array_filter($old_received_balance_spend_items, function($item){
                    $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                    return 'from' == $join_direction;
                });
                if( ! empty($old_received_balance_spend_items))
                {
                    $this->term_posts_m->delete_term_posts($unjoin_contract_id, array_column($old_received_balance_spend_items, 'post_id'));
                    $this->balance_spend_m->delete_many(array_column($old_received_balance_spend_items, 'post_id'));
                }
                update_term_meta($unjoin_contract_id, 'balanceBudgetReceived', 0);
                update_term_meta($unjoin_contract_id, 'previousContractId', '');

                $this->log_m->insert([
                    'log_type'        => 'unjoinContracts',
                    'log_status'      => 1,
                    'term_id'         => $unjoin_contract_id,
                    'log_content'     => serialize([
                        'unjoin_contract_id' => $original_contract_id, 
                        'directive' => 'left'
                    ]),
                    'user_id'         => $this->admin_m->id
                ]);

                try
                {
                    $contract = (new googleads_m())->set_contract($unjoin_contract_id);
                    $behaviour_m = $contract->get_behaviour_m();
                    $behaviour_m->get_the_progress();
                    $behaviour_m->sync_all_amount();
                }
                catch (Exception $e)
                {
                    log_message('error', "[GoogleadsContracts::unjoin] Sync amount #{$unjoin_contract_id} error. " . json_encode(['error' => $e->getMessage()]));
                }
                
                $this->_recompute_contract_chains($unjoin_contract_id);

                break;

            // Unjoin from original_contract with unjoin_contract
            // original_contract is joined to unjoin_contract
            // In UI, original_contract is left or above unjoin_contract
            case 'left':
                // Give (join to)
                // original_contract_id = unjoin_contract_id
                $old_given_balance_spend_items = $this->balance_spend_m
                ->set_post_type()
                ->select('posts.post_id')
                ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$original_contract_id})")
                ->where('posts.comment_status', 'auto')
                ->where('posts.post_title', 'join_command')
                ->as_array()
                ->get_all();
                $old_given_balance_spend_items = array_filter($old_given_balance_spend_items, function($item){
                    $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                    return 'from' == $join_direction;
                });
                if( ! empty($old_given_balance_spend_items))
                {
                    $this->term_posts_m->delete_term_posts($original_contract_id, array_column($old_given_balance_spend_items, 'post_id'));
                    $this->balance_spend_m->delete_many(array_column($old_given_balance_spend_items, 'post_id'));
                }
                update_term_meta($original_contract_id, 'balanceBudgetReceived', 0);
                update_term_meta($original_contract_id, 'previousContractId', '');

                $this->log_m->insert([
                    'log_type'        => 'unjoinContracts',
                    'log_status'      => 1,
                    'term_id'         => $original_contract_id,
                    'log_content'     => serialize([
                        'original_contract_id' => $original_contract_id,
                        'unjoin_contract_id' => $unjoin_contract_id, 
                        'directive' => 'left'
                    ]),
                    'user_id'         => $this->admin_m->id
                ]);

                try
                {
                    $contract = (new googleads_m())->set_contract($original_contract_id);
                    $behaviour_m = $contract->get_behaviour_m();
                    $behaviour_m->get_the_progress();
                    $behaviour_m->sync_all_amount();
                }
                catch (Exception $e)
                {
                    log_message('error', "[GoogleadsContracts::unjoin] Sync amount #{$original_contract_id} error. " . json_encode(['error' => $e->getMessage()]));
                }

                $this->_recompute_contract_chains($original_contract_id);

                // Receive (join from)
                // unjoin_contract_id = original_contract_id
                $old_received_balance_spend_items = $this->balance_spend_m
                ->set_post_type()
                ->select('posts.post_id')
                ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$unjoin_contract_id})")
                ->where('posts.comment_status', 'auto')
                ->where('posts.post_title', 'join_command')
                ->as_array()
                ->get_all();
                $old_received_balance_spend_items = array_filter($old_received_balance_spend_items, function($item){
                    $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                    return 'to' == $join_direction;
                });
                if( ! empty($old_received_balance_spend_items))
                {
                    $this->term_posts_m->delete_term_posts($unjoin_contract_id, array_column($old_received_balance_spend_items, 'post_id'));
                    $this->balance_spend_m->delete_many(array_column($old_received_balance_spend_items, 'post_id'));
                }
                update_term_meta($unjoin_contract_id, 'balanceBudgetAddTo', 0);
                update_term_meta($unjoin_contract_id, 'nextContractId', '');

                $this->log_m->insert([
                    'log_type'        => 'unjoinContracts',
                    'log_status'      => 1,
                    'term_id'         => $unjoin_contract_id,
                    'log_content'     => serialize([
                        'original_contract_id' => $unjoin_contract_id,
                        'unjoin_contract_id' => $original_contract_id, 
                        'directive' => 'right'
                    ]),
                    'user_id'         => $this->admin_m->id
                ]);

                try
                {
                    $contract = (new googleads_m())->set_contract($unjoin_contract_id);
                    $behaviour_m = $contract->get_behaviour_m();
                    $behaviour_m->get_the_progress();
                    $behaviour_m->sync_all_amount();
                }
                catch (Exception $e)
                {
                    log_message('error', "[GoogleadsContracts::unjoin] Sync amount #{$unjoin_contract_id} error. " . json_encode(['error' => $e->getMessage()]));
                }
                
                $this->_recompute_contract_chains($unjoin_contract_id);

                break;
            default:
                return parent::responseHandler([], 'Không nhận diện được hợp đồng nối', 'error', parent::HTTP_EXPECTATION_FAILED); 

                break;
        }

        parent::responseHandler([], 'Hệ thống xử lý thành công');
    }


    /**
     * Determines if valid start date.
     *
     * @param      int   $term_id  The term identifier
     * @param      int   $adaccount_id      The adaccount_id
     */
    public function configurations_put($contractId = 0)
    {
        $this->load->model('ads_segment_m');
        $this->load->model('googleads/mcm_account_m');
        $this->load->model('googleads/googleads_kpi_m');

        $args = wp_parse_args(parent::put(null, true));

        $this->load->library('form_validation');
        $this->form_validation->set_data($args);
        $this->form_validation->set_rules('adaccounts[]', 'adaccounts[]', 'integer');
        $this->form_validation->set_rules('segments[]', 'segments', 'required');
        $this->form_validation->set_rules('exchange_rates', 'exchange_rates', 'array');

        $technician_ids = $this->googleads_kpi_m->order_by('kpi_type')
            ->select('user_id')
            ->as_array()
            ->get_many_by([ 'term_id' => $contractId]);
        $technician_ids = array_column($technician_ids, 'user_id');

        $segments = $args['segments'] ?? [];
        array_walk($segments, function($segment, $key) use ($segments, $args, $technician_ids){
            $this->form_validation->set_rules("segments[{$key}]", null, 'required');
            $this->form_validation->set_rules("segments[{$key}][adaccount_id]", null, ['required', 'integer', array('check_adaccount_exist', function ($value) {
                $is_exist = $this->mcm_account_m->set_term_type()->where('term_id', $value)->count_by() > 0;

                if (!$is_exist) {
                    $this->form_validation->set_message('check_adaccount_exist', 'Tài khoản không có trên hệ thống');
                    return FALSE;
                }

                return TRUE;
            })]);
            $this->form_validation->set_rules("segments[{$key}][contract_id]", null, ['required', 'integer', array('check_contract_exist', function ($value) {
                $is_exist = $this->googleads_m->set_term_type()->where('term_id', $value)->count_by() > 0;

                if (!$is_exist) {
                    $this->form_validation->set_message('check_contract_exist', 'Hợp đồng không có trên hệ thống');
                    return FALSE;
                }

                return TRUE;
            })]);
            $this->form_validation->set_rules("segments[{$key}][start_date]", null, 'required|numeric');
            $this->form_validation->set_rules("segments[{$key}][end_date]", null, ['numeric', array('check_end_date', function ($value) use ($segment){
                if(empty($value)) return TRUE;
                if(empty($segment['start_date'])) return TRUE;

                if ($segment['start_date'] > $value) {
                    $this->form_validation->set_message('check_end_date', 'Ngày kết thúc phải lớn hơn ngày bắt đầu');
                    return FALSE;
                }

                return TRUE;
            })]);
            $this->form_validation->set_rules("segments[{$key}][assigned_technician_id]", null, [
                [
                    'is_assigned_technician',
                    function($value) use ($segment, $args, $technician_ids) {
                        $is_empty_adaccount = empty($args['adaccounts']);
                        if($is_empty_adaccount)
                        {
                            return TRUE;
                        }

                        if(!in_array($value, $technician_ids))
                        {
                            $this->form_validation->set_message('is_assigned_technician', 'Kỹ thuật chưa được phân công');
                            return FALSE;
                        }

                        return TRUE;
                    }
                ]
            ]);
            $this->form_validation->set_rules("segments[{$key}][assigned_technician_rate]", null, [
                [
                    'total_percent',
                    function($value) use ($segments, $segment, $args) {
                        $is_empty_adaccount = empty($args['adaccounts']);
                        if($is_empty_adaccount)
                        {
                            return TRUE;
                        }

                        $value = (int)$value;

                        // Check unique segment
                        $duplicated_segments = array_filter($segments, function($item) use ($segment){
                            if($segment['post_id'] == $item['post_id'])
                            {
                                return FALSE;
                            }

                            $is_duplicated_adaccount_id = $segment['adaccount_id'] == $item['adaccount_id'];
                            $is_duplicated_assigned_technician_id = (int) ($segment['assigned_technician_id']) == (int) ($item['assigned_technician_id']);
                            $is_duplicated_assigned_technician_rate = $segment['assigned_technician_rate'] == $item['assigned_technician_rate'];

                            $_maxDateMilstone = end_of_day(time());

                            $start_date = start_of_day($segment['start_date'] ?: $_maxDateMilstone);
                            $_start_date = start_of_day($item['start_date'] ?: $_maxDateMilstone);
                            $is_duplicated_start_date = $start_date == $_start_date;

                            $end_date = end_of_day($segment['end_date'] ?: $_maxDateMilstone);
                            $_end_date = end_of_day($item['end_date'] ?: $_maxDateMilstone);
                            $is_duplicated_end_date = $end_date == $_end_date;

                            if( $is_duplicated_adaccount_id
                                && $is_duplicated_start_date
                                && $is_duplicated_end_date
                                && $is_duplicated_assigned_technician_id
                                && $is_duplicated_assigned_technician_rate
                            )
                            {
                                return TRUE;
                            }

                            return FALSE;
                        });
                        if(!empty($duplicated_segments))
                        {
                            $this->form_validation->set_message('total_percent', 'Phân đoạn đã cấu hình');
                            return FALSE;
                        }

                        // Get overlap ad accounts
                        $ad_accounts_overlap = array_filter($segments, function($item) use ($segment){
                            if($segment['adaccount_id'] != $item['adaccount_id'] ) 
                            {
                                return FALSE;
                            }
                            
                            return TRUE;
                        });

                        // IF ad accounts is not overlap
                        // THEN check assigned_technician_rate must be 100%
                        if(empty($ad_accounts_overlap)) 
                        {
                            $assigned_technician_rate = (int) $segment['assigned_technician_rate'] ?: 0;
                            if(100 != $assigned_technician_rate)
                            {
                                $this->form_validation->set_message('total_percent', 'Tỉ lệ phân công không đúng');
                                return FALSE;
                            }

                            return TRUE;
                        }

                        // Get overlap segment
                        $segment_overlap = array_filter($ad_accounts_overlap, function($item) use ($segment){
                            $_maxDateMilstone = end_of_day(time());
                            $x_1 = start_of_day($segment['start_date'] ?: $_maxDateMilstone);
                            $y_1 = end_of_day($segment['end_date'] ?: $_maxDateMilstone);
                            $x_2 = start_of_day($item['start_date'] ?: $_maxDateMilstone);
                            $y_2 = end_of_day($item['end_date'] ?: $_maxDateMilstone);

                            if(($x_2 <= $y_1) && ($y_2 >= $x_1))
                            {
                                return TRUE;
                            }

                            return FALSE;
                        });

                        // IF segment is not overlap
                        // THEN check assigned_technician_rate must be 100%
                        if(empty($segment_overlap)) 
                        {
                            $assigned_technician_rate = (int) $segment['assigned_technician_rate'] ?: 0;
                            if(100 != $assigned_technician_rate)
                            {
                                $this->form_validation->set_message('total_percent', 'Tỉ lệ phân công không đúng');
                                return FALSE;
                            }

                            return TRUE;
                        }

                        // Validate start date, end date must be equal this segment
                        $invalid_technician_overlap = array_filter($segment_overlap, function($item) use ($segment){
                            $_maxDateMilstone = end_of_day(time());
                            $x_1 = start_of_day($segment['start_date'] ?: $_maxDateMilstone);
                            $y_1 = end_of_day($segment['end_date'] ?: $_maxDateMilstone);
                            $x_2 = start_of_day($item['start_date'] ?: $_maxDateMilstone);
                            $y_2 = end_of_day($item['end_date'] ?: $_maxDateMilstone);

                            if(($x_1 != $x_2) || ($y_1 != $y_2))
                            {
                                return TRUE;
                            }

                            return FALSE;
                        });
                        if(!empty($invalid_technician_overlap))
                        {
                            $this->form_validation->set_message('total_percent', 'Ngày bắt đầu phải giống nhau hoặc ngày kết thúc phải giống nhau');
                            return FALSE;
                        }

                        // Get technician overlap
                        $technician_overlap = array_filter($segment_overlap, function($item) use ($segment){
                            $_maxDateMilstone = end_of_day(time());
                            $x_1 = start_of_day($segment['start_date'] ?: $_maxDateMilstone);
                            $y_1 = end_of_day($segment['end_date'] ?: $_maxDateMilstone);
                            $x_2 = start_of_day($item['start_date'] ?: $_maxDateMilstone);
                            $y_2 = end_of_day($item['end_date'] ?: $_maxDateMilstone);

                            if(($x_1 == $x_2) && ($y_1 == $y_2))
                            {
                                $technician_id = $segment['assigned_technician_id'];
                                $_technician_id = $item['assigned_technician_id'];

                                return $technician_id != $_technician_id;
                            }

                            return FALSE;
                        });

                        // IF technician is not overlap
                        // THEN check assigned_technician_rate must be 100%
                        if(empty($technician_overlap)) 
                        {
                            $assigned_technician_rate = (int) $segment['assigned_technician_rate'] ?: 0;
                            if(100 != $assigned_technician_rate)
                            {
                                $this->form_validation->set_message('total_percent', 'Tỉ lệ phân công không đúng');
                                return FALSE;
                            }

                            return TRUE;
                        }
                        
                        // Calc total rate
                        $sum_technician_rate = array_sum(array_column($technician_overlap, 'assigned_technician_rate'));
                        $sum_technician_rate += $value;
                        if(0 == 100 - $sum_technician_rate)
                        {
                            return TRUE;
                        }

                        $this->form_validation->set_message('total_percent', 'Tỉ lệ phân công không đúng');
                        return FALSE;
                    }
                ]
            ]);
        });

        if( FALSE === $this->form_validation->run())
        {
            parent::response([
                'code' => 400,
                'error' => $this->form_validation->error_array()
            ]);            
        }

        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                  && TRUE == (bool)get_term_meta_value($contractId, 'is_manipulation_locked');
        if($is_manipulation_locked)
        {
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
            
            $this->contract_m->set_contract($contractId);

            // Check adaccounts change
            $adaccounts = parent::put('adaccounts', TRUE) ?: [];
            $_adaccounts = $this->contract_m->get_behaviour_m()->get_adaccount();
            $_adaccounts = array_filter($_adaccounts, function($item) use ($manipulation_locked_at){ return start_of_day($item['start_date']) < end_of_day($manipulation_locked_at); });
            $_adaccounts = array_unique(array_column($_adaccounts, 'term_id'));
            foreach($_adaccounts as $_adaccount_id)
            {
                if(!in_array($_adaccount_id, $adaccounts))
                {
                    parent::response([
                        'code' => 400,
                        'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                    ]);
                }
            }

            // Check exchange rate change
            $exchange_rates = parent::put('exchange_rates', TRUE) ?: [];
            foreach($exchange_rates as $exchange_rate => $value){
                $_exchange_rate = get_term_meta_value($contractId, $exchange_rate);
                if($_exchange_rate != $value) {
                    parent::response([
                        'code' => 400,
                        'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                    ]);
                }
            }

            // Check segment change
            $segments = parent::put('segments', TRUE) ?: [];
            $segments AND $segments = array_filter($segments, function($x){ return !empty($x['adaccount_id']); });
            $_segments = $this->contract_m->get_behaviour_m()->get_segments(['assigned_technician_id', 'assigned_technician_rate']);

            $segments_group_by_adaccount_id = array_group_by($segments, 'adaccount_id');
            $_segments_group_by_adaccount_id = array_group_by($_segments, 'adaccount_id');

            foreach($segments_group_by_adaccount_id as $adaccount_id => $segment_data)
            {
                $_segment_data = $_segments_group_by_adaccount_id[$adaccount_id] ?? [];
                $new_segment_data = array_filter($segment_data, function($item) { return empty($item['post_id']);});
                $old_segment_data = array_filter($segment_data, function($item) { return !empty($item['post_id']);});

                // Check segment start date
                $segment_data_start_date = array_column($old_segment_data, 'start_date');
                $segment_data_start_date = array_filter($segment_data_start_date, function($item) use ($manipulation_locked_at){ return start_of_day($item) < end_of_day($manipulation_locked_at); });
                $_segment_data_start_date = array_column($_segment_data, 'start_date');
                $_segment_data_start_date = array_filter($_segment_data_start_date, function($item) use ($manipulation_locked_at){ return start_of_day($item) < end_of_day($manipulation_locked_at); });
                $segment_data_start_date_diff = array_diff($segment_data_start_date, $_segment_data_start_date);
                if(!empty($segment_data_start_date_diff))
                {
                    parent::response([
                        'code' => 400,
                        'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                    ]);
                }

                // Check segment end date
                $segment_data_end_date = array_column($old_segment_data, 'end_date');
                $segment_data_end_date = array_filter($segment_data_end_date, function($item) use ($manipulation_locked_at){ return $item > 0 ? (end_of_day($item) < end_of_day($manipulation_locked_at)) : FALSE; });
                $_segment_data_end_date = array_column($_segment_data, 'end_date');
                $_segment_data_end_date = array_filter($_segment_data_end_date, function($item) use ($manipulation_locked_at){ return $item > 0 ? (end_of_day($item) < end_of_day($manipulation_locked_at)) : FALSE; });
                $segment_data_end_date_diff = array_diff($segment_data_end_date, $_segment_data_end_date);
                if(!empty($segment_data_end_date_diff))
                {
                    parent::response([
                        'code' => 400,
                        'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                    ]);
                }
                
                // Check assigned technician change
                $segment_data_assigned_technician_id = array_column($old_segment_data, 'assigned_technician_id');
                $_segment_data_assigned_technician_id = array_column($_segment_data, 'assigned_technician_id');
                $segment_data_assigned_technician_id_diff = array_diff($segment_data_assigned_technician_id, $_segment_data_assigned_technician_id);
                if(!empty($segment_data_assigned_technician_id_diff))
                {
                    parent::response([
                        'code' => 400,
                        'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                    ]);
                }

                // Check assigned technician rate change
                $segment_data_assigned_technician_rate = array_column($old_segment_data, 'assigned_technician_rate');
                $_segment_data_assigned_technician_rate = array_column($_segment_data, 'assigned_technician_rate');
                $segment_data_assigned_technician_rate_diff = array_diff($segment_data_assigned_technician_rate, $_segment_data_assigned_technician_rate);
                if(!empty($segment_data_assigned_technician_rate_diff))
                {
                    parent::response([
                        'code' => 400,
                        'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                    ]);
                }
                
                // Check new segment
                if(!empty($new_segment_data))
                {
                    $segment_data_start_date = array_column($new_segment_data, 'start_date');
                    $segment_data_start_date = array_filter($segment_data_start_date, function($item) use ($manipulation_locked_at){ return start_of_day($item) < end_of_day($manipulation_locked_at); });
                    if(!empty($segment_data_start_date))
                    {
                        parent::response([
                            'code' => 400,
                            'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                        ]);
                    }
    
                    $segment_data_end_date = array_column($new_segment_data, 'end_date');
                    $segment_data_end_date = array_filter($segment_data_end_date, function($item) use ($manipulation_locked_at){ return $item > 0 ? (end_of_day($item) < end_of_day($manipulation_locked_at)) : FALSE; });
                    if(!empty($segment_data_end_date))
                    {
                        parent::response([
                            'code' => 400,
                            'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                        ]);
                    }
                }
            }
        }

        $exchange_rates = parent::put('exchange_rates', TRUE);
        if(isset($exchange_rates)){
            foreach($exchange_rates as $exchange_rate => $value){
                $regex = '/^exchange_rate_[a-z]{3}_to_[a-z]{3}$/';
                if(preg_match($regex, $exchange_rate)){
                    update_term_meta($contractId, $exchange_rate, (double) $value);
                }
            }
        }

        $adaccount_source = parent::put('adaccount_source', TRUE);
        update_term_meta($contractId, 'adaccount_source', $adaccount_source ?? 'internal');

        $adaccounts = parent::put('adaccounts', TRUE);

        $has_manage_ads_segment = (int) (
            has_permission('googleads.kpi.update') 
            && (
                has_permission('googleads.kpi.manage')
                || has_permission('googleads.kpi.mdeparment')
                || has_permission('googleads.kpi.mgroup')
            )
        );

        /**
         * Trường hợp hợp đồng huỷ bỏ cấu hình (không có tài khoản quảng cáo), sẽ thực hiện các việc sau:
         * 
         * 1. Xoá các phân đoạn và relationship của nó
         * 2. Xoá meta "adaccounts" của hợp đồng
         * 3. Gỡ meta nối hợp đồng (nextContractId, previousContractId)
         * 4. Chạy lại chuỗi nối cho "nextContractId" và "previousContractId" (nếu có)
         * 5. Trả kết quả cho frontend
         * 
         * */ 
        if(empty($adaccounts))
        {
            $this->load->model('ads_segment_m');
            $this->load->model('term_users_m');
            $this->load->model('googleads/mcm_account_m');

            // 1. Xoá các phân đoạn và relationship của nó
            $adssegments 	= $this->term_posts_m->get_term_posts($contractId, $this->ads_segment_m->post_type);
            if(empty($adssegments)) 
            {
                $response = array('code' => 200, 'message' => 'Xử lý thành công.');
                parent::response($response);
            }

            $ads_segments_authors = array_filter(array_column($adssegments, 'post_author'));
            $is_diff = ! empty(array_diff($ads_segments_authors, [$this->admin_m->id]));
            if($is_diff && !$has_manage_ads_segment)
            {
                parent::response([
                    'code' => 400,
                    'message' => 'Bạn không có quyền thao tác với phân đoạn của người khác.'
                ]);
            }

            $adsSegmentsIds = array_filter(array_column($adssegments, 'post_id'));
            array_walk($adsSegmentsIds, function($x) {
                $adaccounts = $this->term_posts_m->get_post_terms($x, $this->mcm_account_m->term_type);
                if( ! empty($adaccounts))
                {
                    $adaccountIds = array_column($adaccounts, 'term_id');
                    $this->ads_segment_m->detach_ad_account($x, $adaccountIds, $this->mcm_account_m->term_type);
                }
            });

            $this->ads_segment_m->delete_many($adsSegmentsIds);
            $this->term_posts_m->delete_term_posts($contractId, $adsSegmentsIds);

            // 2. Xoá meta "adaccounts" của hợp đồng
            $this->termmeta_m->delete_meta($contractId, 'adaccounts');

            // 3. Gỡ meta nối hợp đồng (nextContractId, previousContractId)
            $nextContractId = get_term_meta_value($contractId, 'nextContractId');
            $previousContractId = get_term_meta_value($contractId, 'previousContractId');

            update_term_meta($contractId, 'nextContractId', '');
            update_term_meta($nextContractId, 'previousContractId', '');

            update_term_meta($contractId, 'previousContractId', '');
            update_term_meta($previousContractId, 'nextContractId', '');

            // 4. Chạy lại chuỗi nối cho "nextContractId" và "previousContractId" (nếu có)
            if(!empty($nextContractId))
            {
                $this->load->config('amqps');
                $amqps_host 	= $this->config->item('host', 'amqps');
                $amqps_port 	= $this->config->item('port', 'amqps');
                $amqps_user 	= $this->config->item('user', 'amqps');
                $amqps_password = $this->config->item('password', 'amqps');
                
                $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
                $queue = $amqps_queues['contract_events'];
            
                $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
                $channel 	= $connection->channel();
                $channel->queue_declare($queue, false, true, false, false);
            
                $payload = [
                    'event' => 'contract_chain.googleads.recalc',
                    'contract_id' => $nextContractId,
                ];
            
                $message = new \PhpAmqpLib\Message\AMQPMessage(
                    json_encode($payload),
                    array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                );
            
                $channel->basic_publish($message, '', $queue);	
                $channel->close();
                $connection->close();
            }

            if(!empty($previousContractId))
            {
                $this->load->config('amqps');
                $amqps_host 	= $this->config->item('host', 'amqps');
                $amqps_port 	= $this->config->item('port', 'amqps');
                $amqps_user 	= $this->config->item('user', 'amqps');
                $amqps_password = $this->config->item('password', 'amqps');
                
                $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
                $queue = $amqps_queues['contract_events'];
            
                $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
                $channel 	= $connection->channel();
                $channel->queue_declare($queue, false, true, false, false);
            
                $payload = [
                    'event' => 'contract_chain.googleads.recalc',
                    'contract_id' => $previousContractId,
                ];
            
                $message = new \PhpAmqpLib\Message\AMQPMessage(
                    json_encode($payload),
                    array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
                );
            
                $channel->basic_publish($message, '', $queue);	
                $channel->close();
                $connection->close();
            }

            // 5. Trả kết quả cho frontend
            $response = array('code' => 200, 'message' => 'Xử lý thành công.');
            parent::response($response);
        }

        /**
         * Thao tác ghi nhận cấu hình sẽ thực hiện các việc sau:
         * 
         * 1. Ghi nhận thay đổi của phân đoạn
         * 2. Ghi nhận thay đổi của tài khoản
         * 
         * */ 

        //  1. Ghi nhận thay đổi của phân đoạn
        $adaccounts = array_unique($adaccounts);
        $segments = parent::put('segments', TRUE);
        $segments AND $segments = array_filter($segments, function($x){ return !empty($x['adaccount_id']); });
        if( ! empty($segments))
        {
            // Check account is available
            $contract = new googleads_m();
            $contract->set_contract($contractId);
            $conflict_contracts = $contract->get_conflict_contract(['segments' => $segments]);
            if(!empty($conflict_contracts)){
                $response['code'] = parent::HTTP_CONFLICT;
                $response['error'] = 'Tài khoản chưa cấu hình kết thúc.';
                $response['data'] = $conflict_contracts;

                parent::response($response);
            }

            /* Load danh sách tất cả phân đoạn của hợp đồng */
            $adsSegmentsIds = array();
            $adssegments = $this->term_posts_m->get_term_posts($contractId, $this->ads_segment_m->post_type); 
            $adssegments AND $adsSegmentsIds = array_filter(array_column($adssegments, 'post_id'));

            $removeSegmentsIds = array_filter($adsSegmentsIds, function($x) use ($segments){
                if(empty($segments)) return true;
                return ! in_array($x, array_column($segments, 'post_id'));
            });

            /** 
             * Nếu phân đoạn nào không được thấy trong giá trị này thì cần phải xóa nó ra khỏi data 
             * 
             * Trường hợp có ít nhất 1 phân đoạn không được gán cho user đăng đăng nhập
             * Và user đăng nhập không có quyền quản lý phân đoạn thì trả về thông báo permission
             */
            if( ! empty($removeSegmentsIds))
            {
                foreach($removeSegmentsIds as $segment_id)
                {
                    $ads_segment = (array) $adssegments[$segment_id] ?? [];
                    $ads_segments_author = $ads_segment['post_author'] ?: null;
                    if( ($ads_segments_author != null && $ads_segments_author != $this->admin_m->id)
                        && !$has_manage_ads_segment)
                    {
                        parent::response([
                            'code' => 400,
                            'message' => 'Bạn không có quyền thao tác với phân đoạn của người khác.'
                        ]);
                    }
                }

                $this->ads_segment_m->delete_many($removeSegmentsIds);
                $this->term_posts_m->where_in('post_id', $removeSegmentsIds)->delete_by();
            }

            /** 
             * Tạo mới hoặc cập nhật lại phân đoạn đã có
             * 
             * 1. Nếu user không có quyền quản lý thì chia tập dữ liệu thành 2 phần.
             * 1.1. Phần của chính họ => Sẽ xử lý
             * 1.2. Phần của người khác => Không xử lý
             * 2. Ngược lại, xử lý tất cả
             */
            $process_segment = $segments;
            if(!$has_manage_ads_segment)
            {
                $process_segment = array_filter($segments, function($segment){
                    return $segment['assigned_technician_id'] == $this->admin_m->id;
                });
            }

            $new_segment_ids = [];
            foreach ($process_segment as &$segment)
            {
                $start_date = start_of_day($segment['start_date'] ?: '');
                $end_date   = "";
                $segment['end_date'] AND $end_date = end_of_day($segment['end_date']);

                $segment_data = [
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'post_author' => $segment['assigned_technician_id'],
                    'post_content' => $segment['assigned_technician_rate'],
                ];

                // Nếu $segment không tìm thấy trong DB thì tiến hành tạo mới.
                if(empty($segment['post_id']))
                {
                    if( $segment['assigned_technician_id'] != $this->admin_m->id
                    && !$has_manage_ads_segment)
                    {
                        parent::response([
                            'code' => 400,
                            'message' => 'Bạn không có quyền thao tác với phân đoạn của người khác.'
                        ]);
                    }

                    $insert_id          = $this->ads_segment_m->insert($segment_data);
                    $segment['post_id'] = $insert_id;
                    $new_segment_ids[] = $insert_id;

                    $this->ads_segment_m->attach_ad_account($insert_id, $segment['adaccount_id'], $this->mcm_account_m->term_type);
                    continue;
                }

                // Nếu $segment có tồn tại trong DB thì tiến hành cập nhật
                $ads_segment = (array) $adssegments[$segment['post_id']] ?? [];
                $ads_segments_author = $ads_segment['post_author'] ?: null;
                if( ($ads_segments_author != null && $ads_segments_author != $this->admin_m->id)
                    && !$has_manage_ads_segment)
                {
                    parent::response([
                        'code' => 400,
                        'message' => 'Bạn không có quyền thao tác với phân đoạn của người khác.'
                    ]);
                }

                $this->ads_segment_m->update($segment['post_id'], $segment_data);                
                $this->ads_segment_m->attach_ad_account($segment['post_id'], $segment['adaccount_id'], $this->mcm_account_m->term_type);
            }

            $segment_ids = array_merge(array_filter(array_unique(array_column($segments, 'post_id'))), $new_segment_ids);
            $this->term_posts_m->set_term_posts($contractId, $segment_ids, $this->ads_segment_m->post_type, FALSE);


            /* Tiến hành đồng bộ thời gian hoạt động của hợp đồng dịch vụ quảng cáo này */
            $startMilestones        = array_column($segments, 'start_date');
            $advertise_start_time   = min($startMilestones);

            $endMilestones          = array_column($segments, 'end_date');
            $advertise_end_time     = empty(array_filter($endMilestones, function($x){ return empty($x); })) ? max($endMilestones) : null;

            update_term_meta($contractId, 'advertise_start_time', (int) $advertise_start_time);
            update_term_meta($contractId, 'advertise_end_time', (int) $advertise_end_time);

            update_term_meta($contractId, 'googleads-begin_time', (int) $advertise_start_time);
            update_term_meta($contractId, 'googleads-end_time', (int) $advertise_end_time);


            $is_service_proc    = is_service_proc($contractId);
            $_adaccount_status  = $is_service_proc ? 'REMOVED' : 'UNSPECIFIED';
            ! empty($adaccounts) AND $_adaccount_status = $is_service_proc ? 'APPROVED' : 'PENDING_APPROVAL';

            $_adaccount_status != get_term_meta_value($contractId, 'adaccount_status') AND update_term_meta($contractId, 'adaccount_status', $_adaccount_status);

            // Recompute contract chain
            $this->load->config('amqps');
            $amqps_host 	= $this->config->item('host', 'amqps');
            $amqps_port 	= $this->config->item('port', 'amqps');
            $amqps_user 	= $this->config->item('user', 'amqps');
            $amqps_password = $this->config->item('password', 'amqps');
            
            $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
            $queue = $amqps_queues['contract_events'];
        
            $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
            $channel 	= $connection->channel();
            $channel->queue_declare($queue, false, true, false, false);
        
            $payload = [
                'event' => 'contract_chain.googleads.recalc',
                'contract_id' => $contractId,
            ];
        
            $message = new \PhpAmqpLib\Message\AMQPMessage(
                json_encode($payload),
                array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
            );
        
            $channel->basic_publish($message, '', $queue);	
            $channel->close();
            $connection->close();
        }

        // 2. Ghi nhận thay đổi của tài khoản
        $this->termmeta_m->delete_meta($contractId, 'adaccounts');
        array_walk($adaccounts, function($_adAccountId) use($contractId){
            $this->termmeta_m->add_meta($contractId, 'adaccounts', $_adAccountId);
        });

        $response = array('code' => 200, 'message' => 'Xử lý thành công.');

        parent::response($response);
    }

    /**
     * Excuse the set of stop services process
     *
     * 1. Update googleads-end_time metadata
     * 1. Update end_service_time metadata with now()
     * 2. Change term_status to "ending"
     * 3. Log trace
     * 4. Send Overall E-mail with Adwords Data to someone
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     JSON
     */
    public function stop_service_put($term_id = 0)
    {
        $response   = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
        $args       = wp_parse_args( parent::put(null, true), ['end_date' => my_date(time(), 'd-m-Y')]);

        $contract   = new googleads_m();
        $contract->set_contract($term_id);

        // Restrict permission of current user
        if( ! $contract->can('Googleads.stop_service.update'))
        {
            $response['msg'] = 'Không có quyền thực hiện tác vụ này.';
            parent::response($response);
        }
        
        $end_time = end_of_day($args['end_date']);

        // Check if the service is running then stop excute and return
        if($contract->is_service_end($term_id))
        {
            $response['msg'] = 'Hợp đồng đã được đóng.';
            parent::response($response);
        }

        /* Load danh sách tất cả phân đoạn của hợp đồng */
        $adssegments = $this->term_posts_m->get_term_posts($term_id, $this->ads_segment_m->post_type); 
        if(empty($adssegments))
        {
            $response['msg'] = 'Có lỗi xảy ra vì phân đoạn không được tìm thấy.';
            parent::response($response);
        }

        foreach ($adssegments as $adssegment)
        {
            $_end_date = end_of_day($adssegment->end_date ?: $end_time);
            $this->ads_segment_m->set_post_type()->update($adssegment->post_id, [ 'end_date' => $_end_date ]);
        }
        
        try
        {
            $contract->get_behaviour_m()->stop_service();

            $service_fee_payment_type = get_term_meta_value($term_id, 'service_fee_payment_type');
            if('range' == $service_fee_payment_type){
                $contract->get_behaviour_m()->recalc_receipt();
            }
        }
        catch(Exception $e)
        {
            parent::response([ 'code' => parent::HTTP_BAD_REQUEST, 'error' => $e->getMessage() ]);
        }

        $response['msg']    = 'Hợp đồng đã được kết thúc.';
        $response['status'] = TRUE;
        $response['code'] = parent::HTTP_OK;
        parent::response($response);
    }

    /**
     * Excuse the set of start services process
     * 
     * 1. Update Money Exchange Rate from VIETCOMBANK API
     * 2. Update Advertise start time if is not set
     * 3. Log trace action 
     * 4. Send activation email to all responsibilty users
     * 5. Detect if first contract
     * 6. Send SMS notify to contract's customer
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     JSON
     */
    public function proc_service_put($term_id = 0)
    {
        $response       = [ 'code' => parent::HTTP_BAD_REQUEST, 'error' => 'Quá trình xử lý không thành công.' ];
        $contract       = new googleads_m();
        if( ! $contract->set_contract($term_id))
        {
            $response['error'] = 'Hợp đồng hết hiệu lực hoặc không tồn tại.';
            parent::response($response);    
        }

        // Restrict permission of current user
        if( ! $contract->can('Googleads.start_service.update'))
        {
            $response['error'] = 'Không có quyền thực hiện tác vụ này.';
            parent::response($response);
        }

        // Check if the service is running then stop excute and return
        if($contract->is_running())
        {
            $response['error'] = 'Dịch vụ đã được thực hiện.';
            parent::response($response);
        }

        // Check account is available
        $conflict_contract = $contract->get_conflict_contract();
        if(!empty($conflict_contract)){
            $response['code'] = parent::HTTP_CONFLICT;
            $response['error'] = 'Tài khoản chưa cấu hình kết thúc.';
            $response['data'] = $conflict_contract;

            parent::response($response);
        }

        // Proc service
        $result = $contract->get_behaviour_m()->proc_service();
        if( ! $result)
        {
            $response['error'] = 'Quá trình xử lý không thành công.';
            parent::response($response);
        }

        // Detect if contract is the first signature (tái ký | ký mới)
        $this->base_contract_m->detect_first_contract($term_id);

        // Send SMS to customer
        $this->load->model('contract/contract_report_m');
        // $this->contract_report_m->send_sms_activation_2customer($term_id);

        $response['message']    = 'Dịch vụ đã được kích hoạt thực hiện thành công.';
        $response['code']       = parent::HTTP_OK;
        parent::response($response);
    }

    public function progress_sync_get($term_id = 0)
    {
        $_contract = new googleads_m();
        $_contract->set_contract($term_id);
        
        $target_model = $this->googleads_m->get_adword_model($term_id);
        $target_model->get_the_progress($term_id, FALSE, FALSE, TRUE);
        
        try
        {
            $_contract->get_behaviour_m()->sync_all_progress();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    /**
     * { function_description }
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function cids_get()
    {
        $response       = array('status' => FALSE, 'msg' => 'Quá trình xử lý thất bại', 'data' => []);
        $defaults       = array();
        $args           = wp_parse_args( $this->input->get(), $defaults );
        $args_encypt    = md5(json_encode($args));
        $data           = array();
        $key_cache      = Mcm_account_m::CACHE_ALL_KEY . $args_encypt;

        $mcm_accounts = $this->scache->get($key_cache);
        if( ! $mcm_accounts)
        {
            $mcm_accounts = $this->mcm_account_m->select('term.term_id,term.term_name')
            ->get_many_by(['term_status'=>'active','term_type'=>$this->mcm_account_m->term_type]);

            if( ! $mcm_accounts)
            {
                $response['msg'] = 'Dữ liệu không tồn tại';
                return parent::renderJson($response);
            }

            $this->scache->write($mcm_accounts, $key_cache, 300);
        }

        foreach ($mcm_accounts as $mcm_account)
        {
            $mcm_account->term_id = (int) $mcm_account->term_id;
            $mcm_account->term_name = (int) $mcm_account->term_name;
            $mcm_account->account_name = get_term_meta_value($mcm_account->term_id,'account_name');
            $data[] = $mcm_account;
        }

        $response['data']   = $data;
        $response['msg']    = 'Xử lý dữ liệu thành công !';
        $response['status'] = TRUE;

        parent::response($response);
    }

    /**
     * /PUT API-V2/FACEBOOKADS/CONTRACT/ADACCOUNT_INSIGHT/:ID
     *
     * @param      int    $term_id  The term identifier
     *
     * @return     Json  Result
     */
    public function adaccount_insight_put(int $term_id = 0)
    {
        $result     = [ 'success' => 'error', 'msg' => 'Xử lý không thành công', 'data' => ''];
        $contract   = (new googleads_m())->set_contract($term_id);
        try
        {
            // $contract->update_insights();
            $behaviour_m = $contract->get_behaviour_m();
            $a = $behaviour_m->get_the_progress();
            $behaviour_m->sync_all_amount();
        }
        catch (Exception $e)
        {
            $result['msg'] = $e->getMessage();
            return parent::response($result);
        }

        $result['success']  = 'success';
        $result['msg']      = 'Dữ liệu đã được đồng bộ thành công. Vui lòng (F5) để xem sự thay đổi';
        return parent::response($result);
    }


    /**
     * Send Alert Notifications
     *
     * @param      int   $term_id  The term identifier
     */
    public function alert_service_put(int $term_id = 0)
    {
        $contract = (new googleads_m())->set_contract($term_id);
        if( ! $contract) parent::response([ 'code' => 400, 'data' => true ]);

        try
        {
            $behaviour_m = $contract->get_behaviour_m();
            $behaviour_m->get_the_progress();
            $behaviour_m->sync_all_amount();
        }
        catch (Exception $e)
        {
            parent::response([ 'code' => 400, 'error' => $e->getMessage() ]);
        }

        $this->load->model('googleads/googleads_report_m');

        $time_next_api_check = $now = time();
        $actual_progress_percent_net = ((float) get_term_meta_value($contract->term_id, 'actual_progress_percent_net'))*100;

        /* Xử lý theo Ngân sách hợp đồng */
        if($actual_progress_percent_net >= 80)
        {    
            /* Gửi mail thông báo đến admin và các bên liên quan */
            $this->send_progress_email($contract, 'actual', TRUE); 
            $time_next_api_check = strtotime('+1 hour', $now);
        }
        else $time_next_api_check = strtotime('+1 hour', $now);

        /* Xử lý theo Ngân sách hợp đồng */
        $progress = (float) get_term_meta_value($contract->term_id, 'actual_progress_percent');
        if($progress >= 100) 
        {
            /* Gửi kèm mail [demo] báo cáo kết thúc gửi cho khách hàng đến admin và các bên liên quan để xem trước */
            $this->googleads_report_m->send_finished_service($contract->term_id, 'admin');
        }
        else if($progress >= 80)
        {
            $notice_finish_mail_has_sent = get_term_meta_value($contract->term_id, 'notice_finish_mail_has_sent');
            if(empty($notice_finish_mail_has_sent))
            {
                $this->googleads_report_m->send_noticefinish_mail($contract->term_id, 'customer');
                update_term_meta($contract->term_id, 'notice_finish_mail_has_sent', 1);
            }
        }

        update_term_meta($contract->term_id, 'time_next_api_check', $time_next_api_check);

        // AUTO-UPDATE STATUS BY PAUSED CAMPAIGN DURATIONS
        $result_updated_on = (int) get_term_meta_value($contract->term_id, 'result_updated_on');
        if($result_updated_on >= 0)
        {
            $is_valid_pending_durations = strtotime('+1 day', $result_updated_on) >= start_of_day();

            # Case : Adwords data has not updated greater than 1 day then status is "PENDING"           
            if( ! $is_valid_pending_durations && $contract->term_status == 'publish')
            {
                $this->googleads_m->set_default_query()->update($contract->term_id, ['term_status' => 'pending']);
            }

            # Case : Adwords data has not updated smaller than 1 day then status is "PENDING"
            if($is_valid_pending_durations && $contract->term_status == 'pending')
            {
                $this->googleads_m->set_default_query()->update($contract->term_id, ['term_status' => 'publish']);
            }
        }

        parent::response([
            'code' => 200,
            'data' => [
                'progress' => $progress,
                'time_next_api_check' => $time_next_api_check,
                'result_updated_on' => $result_updated_on,
            ]
        ]);
    }

    /**
     * Sends a progress email.
     *
     * @param      <type>  $term         The term
     * @param      string  $budget_type  The budget type
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    protected function send_progress_email($term = NULL, $budget_type = 'commit', $force_send = FALSE)
    {
        if( ! $term) return FALSE;

        $cache_key = "modules/googleads/old-progress/term_{$term->term_id}"; // Cache progress theo hợp đồng
        if($budget_type == 'actual') $cache_key = "modules/googleads/old-actual-progress/term_{$term->term_id}"; // Cache progress theo thực thu

        $past_progress  = (float) $this->scache->get($cache_key);
        $progress       = $past_progress;

        switch ($budget_type)
        {
            case 'actual': $progress = (float) get_term_meta_value($term->term_id, 'actual_progress_percent_net'); break;
            default: $progress = ((float) get_term_meta_value($term->term_id, 'actual_progress_percent'))*100; break;
        }

        /* Nếu tiến độ không thay đổi so với lần load dữ liệu trước */
        if( ! $force_send && $progress == $past_progress) return TRUE;

        $adwords_report_m = new adwords_report_m();
        $adwords_report_m = $adwords_report_m->init($term);
        $adwords_report_m->send_progress_email($budget_type);

        $this->scache->write($progress, $cache_key);
        return TRUE;
    }

    /**
     * Calculates the metrics put.
     *
     * @param      int   $term_id  The term identifier
     */
    public function compute_metrics_put(int $term_id = 0)
    {
        $contract = (new googleads_m())->set_contract($term_id);
        if( ! $contract) parent::response([ 'code' => 400, 'data' => true ]);

        try
        {
            $behaviour_m = $contract->get_behaviour_m();
            $behaviour_m->get_the_progress();
            $behaviour_m->sync_all_amount();
        }
        catch (Exception $e)
        {
            parent::response([ 'code' => 400, 'error' => $e->getMessage() ]);
        }

        parent::response([ 'code' => 200]);
    }
    
    /**
     * contract_chains_get
     * 
     * Get all contrtact has joined from the previos contract to last contract
     *
     * @param  int $term_id
     * @return void
     */
    public function contract_chains_get(int $term_id = 0){
        if(!has_permission('googleads.unjoin_contract.access'))
        {
            return parent::responseHandler([
                'permisision_deny' => 'Quyền truy cập không hợp lệ.'
            ], "permisision_deny", 'error', parent::HTTP_BAD_REQUEST); 
        }

        $contract = $this->googleads_m->set_contract($term_id);
        if(! $contract) 
        {
            return parent::responseHandler([
                'term_id' => 'Contract is not exist'
            ], 'Hợp đồng không tồn tại', 'error', parent::HTTP_BAD_REQUEST);
        }

        // Setup chains
        $contract_chains = [$term_id];

        $is_conflict = FALSE;
        $is_stop = FALSE;
        $_term_id = $term_id;
        while(!$is_stop)
        {
            $prev_contract_id = get_term_meta_value($_term_id, 'previousContractId');
            if(empty($prev_contract_id)) 
            {
                $is_stop = TRUE;
                
                break;
            }

            if(in_array($prev_contract_id, $contract_chains))
            {
                $is_conflict = TRUE;
                $is_stop = TRUE;
                
                break;
            }

            array_unshift($contract_chains, $prev_contract_id);
            $_term_id = $prev_contract_id;
        }
        if($is_conflict) 
        {
            return parent::responseHandler([], 'Phát hiện chuỗi hợp trùng. Liên hệ bộ phận Công nghệ để được xử lý!', 'error', parent::HTTP_EXPECTATION_FAILED);
        }
        
        $is_conflict = FALSE;
        $is_stop = FALSE;
        $_term_id = $term_id;
        while(!$is_stop)
        {
            $next_contract_id = get_term_meta_value($_term_id, 'nextContractId');
            if(empty($next_contract_id)) 
            {
                $is_stop = TRUE;
                
                break;
            }

            if(in_array($next_contract_id, $contract_chains))
            {
                $is_conflict = TRUE;
                $is_stop = TRUE;
                
                break;
            }

            array_push($contract_chains, $next_contract_id);
            $_term_id = $next_contract_id;
        }

        if($is_conflict) 
        {
            return parent::responseHandler([], 'Phát hiện chuỗi hợp trùng. Liên hệ bộ phận Công nghệ để được xử lý!', 'error', parent::HTTP_EXPECTATION_FAILED);
        }
        
        if(1 >= count($contract_chains)) 
        {
            return parent::responseHandler([], 'Không tìm thấy chuỗi hợp đồng!', 'error', parent::HTTP_EXPECTATION_FAILED);
        }
        
        // Get contract spend
        $contract_spends = $this->googleads_m
            ->select("term.term_id AS term_id")
            ->select("COALESCE(SUM(IF(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, NULL)), balance_spend.post_content) AS spend")
            ->select("COALESCE(ads_segment.post_type, balance_spend.post_type) AS segment_type")
            ->select("COALESCE(ads_segment.post_title, balance_spend.post_title) AS segment_title")
            ->select("mcm_account.term_id AS adaccount_id")
            ->select("COALESCE(ads_segment.post_id, balance_spend.post_id) AS ads_segment_id")
            ->select("ads_segment.start_date AS ads_segment_start")
            ->select("ads_segment.end_date AS ads_segment_end")

            ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
            ->join('posts AS balance_spend', 'balance_spend.post_id = tp_contract_ads_segment.post_id AND balance_spend.post_type = "balance_spend"', 'LEFT')
            ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'LEFT')
            ->join('term_posts AS tp_segment_mcm_account', 'tp_segment_mcm_account.post_id = ads_segment.post_id', 'LEFT')
            ->join('term AS mcm_account', 'tp_segment_mcm_account.term_id = mcm_account.term_id AND mcm_account.term_type = "mcm_account"', 'LEFT')
            ->join('termmeta AS mcm_account_metadata', 'mcm_account_metadata.term_id = mcm_account.term_id AND meta_key = "account_name"', 'LEFT')
            ->join('term_posts AS tp_mcm_account_insights', 'tp_mcm_account_insights.term_id = mcm_account.term_id', 'LEFT')
            ->join('posts AS insights', 'tp_mcm_account_insights.post_id = insights.post_id AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) AND insights.start_date <= IF(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) AND insights.post_type = "insight_segment" AND insights.post_name = "day"', 'LEFT')
            ->join('postmeta AS insight_metadata', 'insight_metadata.post_id = insights.post_id AND insight_metadata.meta_key = "spend"', 'LEFT')

            ->where_in('term.term_id', $contract_chains)

            ->where('term.term_type', 'google-ads')
            ->where('( mcm_account.term_id > 0 OR balance_spend.post_id > 0)')
            ->group_by('mcm_account.term_id, ads_segment.post_id, balance_spend.post_id')

            ->as_array()

            ->get_all();

        $contract_spends = array_group_by($contract_spends, 'term_id');

        $contracts = array_map(function($contract_spend)
        {
            $instance = [
                'term_id' => reset($contract_spend)['term_id'],
                'actual_result' => 0,
                'balance_spend' => 0,
                'actual_budget' => 0,
                'previous_contract_id' => NULL,
                'next_contract_id' => NULL,
                'contract_code' => NULL,
                'advertise_start_time' => NULL,
                'advertise_end_time' => NULL,
                'is_manipulation_locked' => FALSE,
                'manipulation_locked_at' => NULL,
            ];

            $instance['previous_contract_id'] = get_term_meta_value($instance['term_id'], 'previousContractId');
            $instance['next_contract_id'] = get_term_meta_value($instance['term_id'], 'nextContractId');
            $instance['contract_code'] = get_term_meta_value($instance['term_id'], 'contract_code');
            $instance['advertise_start_time'] = get_term_meta_value($instance['term_id'], 'advertise_start_time') ?: get_term_meta_value($instance['term_id'], 'googleads-begin_time');
            $instance['advertise_end_time'] = get_term_meta_value($instance['term_id'], 'advertise_end_time') ?: get_term_meta_value($instance['term_id'], 'googleads-end_time');
            $instance['actual_budget'] = (double)get_term_meta_value($instance['term_id'], 'actual_budget');
            $instance['is_manipulation_locked'] = (bool)get_term_meta_value($instance['term_id'], 'is_manipulation_locked');
            $instance['manipulation_locked_at'] = (double)(get_term_meta_value($instance['term_id'], 'manipulation_locked_at') ?: end_of_day(time()));

            $contract_spend_group_by_segment_type = array_group_by($contract_spend, 'segment_type');

            $insights = array_filter($contract_spend_group_by_segment_type['ads_segment'] ?? [], function($item){
                return 0 != $item['adaccount_id'];
            });

            // Reduce unique $insights
            $insights = array_reduce($insights, function($result, $insight) {
                $segment_id = $insight['ads_segment_id'];
                $ad_account_id = $insight['adaccount_id'];

                // Filter overlap segment
                $overlap_segment = array_filter($result, function($_segment) use ($ad_account_id, $segment_id){
                    // Ignore current segment
                    if($_segment['ads_segment_id'] == $segment_id)
                    {
                        return FALSE;
                    }

                    return $_segment['adaccount_id'] == $ad_account_id;
                }); 
                if(empty($overlap_segment))
                {
                    $result[] = $insight;
                    return $result;
                }

                return $result;
            }, []);
            $actual_result = round(array_sum(array_column($insights, 'spend')));
            $instance['actual_result'] = $actual_result;
            
            $balance_spend_array = $contract_spend_group_by_segment_type['balance_spend'] ?? [];
            
            $join_spend = array_filter($balance_spend_array, function($item){
                return 'join_command' == $item['segment_title'];
            });
            $join_spend = array_map(function($_join_spend){
                $_join_spend['join_direction'] = get_post_meta_value($_join_spend['ads_segment_id'], 'join_direction');

                return $_join_spend;
            }, $join_spend);
            $join_spend = array_group_by($join_spend, 'join_direction');
            $instance['balance_budget_add_to'] = array_sum(array_column($join_spend['to'] ?? [], 'spend'));
            $instance['balance_budget_received'] = array_sum(array_column($join_spend['from'] ?? [], 'spend'));
            
            $balance_spend = array_filter($balance_spend_array, function($item){
                return 'join_command' != $item['segment_title'];
            });
            $balance_spend = array_sum(array_column($balance_spend, 'spend'));
            $instance['balance_spend'] = $balance_spend;

            return $instance;
        }, $contract_spends);

        parent::responseHandler(['contract_chains' => $contracts], 'Lấy dữ liệu thành công');
    }
    
    /**
     * recompute_contract_chains_put
     *
     * @param  mixed $contract_id
     * @return void
     */
    public function recompute_contract_chains_put($contract_id)
    {
        if(!has_permission('googleads.setting.manage'))
        {
            return parent::responseHandler([
                'permisision_deny' => 'Quyền truy cập không hợp lệ.'
            ], "permisision_deny", 'error', parent::HTTP_BAD_REQUEST); 
        }

        $contract = $this->googleads_m->set_contract($contract_id);
        if(! $contract) 
        {
            return parent::responseHandler([], 'Contract is not exist', 'error', parent::HTTP_BAD_REQUEST);
        }

        // Recompute contract chain
        $this->_recompute_contract_chains($contract_id);

        parent::responseHandler([], 'Tiến trình đã đưa vào hàng chờ, sẽ xử lý trong vài phút tới');
    }

    /**
     * _recompute_contract_chains
     *
     * @param  mixed $contract_id
     * @return void
     */
    private function _recompute_contract_chains($contract_id)
    {
        $this->load->config('amqps');
        $amqps_host 	= $this->config->item('host', 'amqps');
        $amqps_port 	= $this->config->item('port', 'amqps');
        $amqps_user 	= $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        
        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];
    
        $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $channel 	= $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);
    
        $payload = [
            'event' => 'contract_chain.googleads.recalc',
            'contract_id' => $contract_id,
        ];
    
        $message = new \PhpAmqpLib\Message\AMQPMessage(
            json_encode($payload),
            array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
    
        $channel->basic_publish($message, '', $queue);	
        $channel->close();
        $connection->close();

        return TRUE;
    }

    /** 
     * toggle_lock_manipulation_put
     *
     * @param      int   $term_id  The term identifier
     */
    public function toggle_lock_manipulation_put(int $term_id = 0)
    {
        if(!$this->googleads_m->has_manipulation_lock_permission()) 
        {
            parent::response([ 'code' => 403, 'data' => false, 'message' => 'Quyền truy cập bị hạn chế.' ]);
        }

        $contract = (new googleads_m())->set_contract($term_id);
        if( ! $contract) parent::response([ 'code' => 400, 'data' => false, 'message' => 'Không tìm thấy hợp đồng.' ]);

        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                  && TRUE == (bool)get_term_meta_value($term_id, 'is_manipulation_locked');
        update_term_meta($term_id, 'is_manipulation_locked', !$is_manipulation_locked);

        parent::response([
            'code' => 200,
            'data' => []
        ]);
    }
}
/* End of file MCCReport.php */
/* Location: ./application/modules/googleads/controllers/api_v2/MCCReport.php */