<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class DataReport_v2 extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['models'][] = 'customer/customer_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'googleads/mcm_account_m';
        $this->autoload['models'][] = 'googleads/base_adwords_m';
        $this->autoload['models'][] = 'googleads/insight_segment_m';
        $this->autoload['models'][] = 'ads_segment_m';
        $this->autoload['helpers'][] = 'array';
        
        parent::__construct();

        $this->load->config('contract/contract');
        $this->load->config('googleads/contract');
    }

    /**
     * Googleads Ads Monthly Spend
     *
     * @throws     Exception  (description)
     */
    public function monthly_get()
    {
        if( ! in_array($this->admin_m->role_id, array(1, 5)))
        {
            parent::response([], 400);
        }

        $args = wp_parse_args(parent::get(null, TRUE), [
            'month' => my_date(time(), 'm'),
            'year' => my_date(time(), 'Y')
        ]);

        $this->load->library('form_validation');
        $this->form_validation->set_data($args);
        $this->form_validation->set_rules('month', 'Month', 'required|integer|greater_than_equal_to[1]|less_than_equal_to[12]');
        $this->form_validation->set_rules('year', 'Year', 'required|integer|greater_than_equal_to[2015]|less_than_equal_to['.my_date(strtotime("+1 year"), 'Y').']');
        if( FALSE == $this->form_validation->run()) parent::response([ 'code' => 400, 'error' => $this->form_validation->error_array() ]);

        $start_time = start_of_month(date("{$args['year']}/{$args['month']}/d"));
        $end_time   = end_of_month(date("{$args['year']}/{$args['month']}/d"));

        $args = [ 'start_time' => $start_time, 'end_time' => $end_time ];
        $data = $this->build($args);
        $isDownloadEvt = ! empty(parent::get('download', TRUE) ?: 0);

        $headings = $this->getColumns($isDownloadEvt ? 'export' : 'default');
        $headings AND $headings = array_map(function($x){ return element('label', $x); }, $headings);

        $columns    = array_keys($headings);
        $data       = array_map(function($x) use ($columns){ return elements($columns, $x); }, $data);

        if( ! $isDownloadEvt) 
        {
            parent::response([ 'rows' => $data, 'headings' => $headings ]);
        }
        
        $this->export([
            'title' => '[ Google Ads - Tháng ' . date('m', $end_time) . ' ] - Báo cáo chi tiêu theo Hợp đồng và khách hàng - [#'. time() .']',
            'data' => $data
        ]);
    }

    /**
     * Googleads Ads Monthly Spend
     *
     * @throws     Exception  (description)
     */
    public function ranges_get($start = '', $end = '')
    {
        if(empty($start) || empty($end)) parent::response([], 400);
        if( ! in_array($this->admin_m->role_id, array(1, 5))) parent::response([], 400);

        $start_time = start_of_day($start);
        $end_time = end_of_day($end);

        $args           = [ 'start_time' => $start_time, 'end_time' => $end_time ];
        $data           = $this->build($args);
        $isDownloadEvt  = ! empty(parent::get('download', TRUE) ?: 0);
        
        $headings = $this->getColumns($isDownloadEvt ? 'export' : 'default');
        $headings AND $headings = array_map(function($x){ return element('label', $x); }, $headings);

        $columns    = array_keys($headings);
        $data       = array_map(function($x) use ($columns){ return elements($columns, $x); }, $data);

        empty(parent::get('download', TRUE) ?: 0) AND parent::response([ 'rows' => $data, 'headings' => $headings ]);

        $this->export([
            'title' => '[ Google Ads - Trong khoảng ' . date('dmY', $start_time) . '-' . date('dmY', $end_time) . ' ] - Báo cáo chi tiêu theo Hợp đồng và khách hàng - [#'. time() .']',
            'data' => $data
        ]);
    }

    /**
     * Buidl Raw Data
     */
    protected function build( $args = [])
    {
        $args = wp_parse_args($args, [
            'start_time'    => $args['start_time'],
            'end_time'      => $args['end_time']
        ]);

        $start_time = start_of_day( $args['start_time']);
        $end_time   = end_of_day( $args['end_time']);

        $payment_type_enums = array(
            'normal' => 'Full VAT',
            'customer' => 'Khách hàng tự thanh toán',
            'behalf' => 'ADSPLUS thu & chi hộ'
        );
        $typeOfServices		= $this->config->item('items', 'typeOfServices');

        $insights = $this->googleads_m
        ->set_term_type()
        ->join('term_users', 'term_users.term_id = term.term_id')
        ->join('user', 'user.user_id = term_users.user_id')
        ->where_in('user.user_type', [ 'customer_person', 'customer_company' ])

        ->join('termmeta as m_adaccounts_status', 'm_adaccounts_status.term_id = term.term_id and meta_key = "adaccount_status" and meta_value = "APPROVED"')
        ->where_in('term.term_status', [ 'pending', 'publish', 'ending', 'liquidation', 'remove' ])

        ->join('term_posts as tp_segments', 'tp_segments.term_id = term.term_id')
        ->join('posts as segments', 'segments.post_id = tp_segments.post_id')
        ->where('segments.post_type', 'ads_segment')

        ->join('term_posts as tp_adaccounts', 'tp_adaccounts.post_id = segments.post_id')
        ->join('term as adaccounts', 'tp_adaccounts.term_id = adaccounts.term_id')
        ->where('adaccounts.term_type', $this->mcm_account_m->term_type)

        ->join('term_posts as tp_insights', 'tp_insights.term_id = adaccounts.term_id')
        ->join('posts as insights', 'tp_insights.post_id = insights.post_id')
        ->where('insights.post_type', 'insight_segment')
        
        ->where('insights.start_date >= segments.start_date')
        ->where('insights.start_date <= if(segments.end_date = 0 or segments.end_date is null, UNIX_TIMESTAMP (), segments.end_date)')

        ->where('insights.start_date >=', $start_time)
        ->where('insights.start_date <=', $end_time)

        ->group_by('term.term_id, segments.post_id, adaccounts.term_id, insights.post_id')

        ->select('
            term.term_id,
            term.term_name,
            term.term_status,
            term.term_type,

            user.user_id as customerId,
            user.user_type as customerType,
            user.display_name as customerName,
            segments.post_id as segmentId,
            segments.start_date as segmentStartDate,
            segments.end_date as segmentEndDate,
            adaccounts.term_id as adaccountId,
            adaccounts.term_name as adaccountName,
            adaccounts.term_slug adaccountSlug,
            insights.post_id as insightId,
            insights.start_date as insightStartDate 
        ')
        
        ->get_all();

        if(empty($insights)) return [];

        $output = [];
        $groupedByContractIdInsights = array_group_by($insights, 'term_id');
        $contractIds = array_unique(array_keys($groupedByContractIdInsights));

        $this->load->model('googleads/googleads_kpi_m');
        $kpis = $this->googleads_kpi_m
            ->select('term_id, user_id')
            ->where_in('term_id', $contractIds)
            ->setTechnicalKpiTypes()
            ->group_by('term_id')
            ->as_array()
            ->get_all();

        $users = array();
        if($kpis)
        {
            $userIds = array_unique(array_column($kpis, 'user_id'));
            foreach ($userIds as $userId)
            {
                $users[$userId] = $this->admin_m->get_field_by_id($userId);
            } 
            $kpis AND $kpis = array_group_by($kpis, 'term_id');
        }

        foreach($groupedByContractIdInsights as $contractId => $contractInsights)
        {
            $contract = reset($contractInsights);
            $contract_code = get_term_meta_value($contractId, 'contract_code');

            $contract_begin        = get_term_meta_value($contractId, 'contract_begin');
            $contract_end          = get_term_meta_value($contractId, 'contract_end');
            $contract_daterange    = implode(' - ', array_map(function($x){ return my_date($x, 'd/m/Y'); },[$contract_begin, $contract_end]));
            $contract_value = (int) get_term_meta_value($contractId, 'contract_value');

            /* PARSE TECHNICAL STAFFS */
            $techStaffs = "";
            empty($kpis[$contractId]) OR $techStaffs = implode(', ', array_filter(array_map(function($y) use ($users){
                if(empty($users[$y['user_id']])) return '';
                return $users[$y['user_id']]['display_name'] ?: $users[$y['user_id']]['user_email'];
            }, $kpis[$contractId])));

            $saleId = (int) get_term_meta_value($contractId, 'staff_business');
            $sale   = $user[$saleId] ?? null;
            if( ! empty($saleId) && empty($user[$saleId]) )
            {
                $sale           = $this->admin_m->get_field_by_id($saleId);
                $users[$saleId] = $sale;
            }

            $actual_result  = (int) get_term_meta_value($contractId, 'actual_result');
            $actual_budget  = (int) get_term_meta_value($contractId, 'actual_budget');
            $remain_budget  = $actual_budget - $actual_result;

            $contract_m = (new googleads_m())->set_contract((object) $contract);
            $budget         = $contract_m->get_behaviour_m()->calc_budget();
            $service_fee    = (int) get_term_meta_value($contractId, 'service_fee');
            $service_rate   = div($service_fee, $budget);

            $discount_amount = $contract_m->get_behaviour_m()->calc_disacount_amount();
            $service_fee_rate_actual = div(max([$service_fee - $discount_amount, 0]), $budget);

            $payment_type = 'normal';
            if('customer' == get_term_meta_value($contractId, 'contract_budget_payment_type'))
            {
                $payment_type = 'customer';
                'behalf' == get_term_meta_value($contractId, 'contract_budget_customer_payment_type') AND $payment_type = 'behalf';
            }

            $typeOfService = get_term_meta_value($contractId, 'typeOfService') ?: '';
            $typeOfService AND $typeOfService = $typeOfServices[$typeOfService] ?? '';

            $groupedBySegmentIdInsights = array_group_by($contractInsights, 'segmentId');
            
            foreach($groupedBySegmentIdInsights as $segmentId => $segmentInsights)
            {
                $segment = reset($segmentInsights);
                $account_currency = get_post_meta_value($segment->insightId, 'account_currency');
                $exchange_rate = get_post_meta_value($segment->insightId, 'exchange_rate');
                $cost = array_sum(array_map(function($x){ return (int) get_post_meta_value($x->insightId, 'spend'); }, $segmentInsights));
            
                $output[] = [
                    'contract_code_link'        => admin_url("facebookads/overview/{$contractId}"),
                    'contract_code_link_tag'    => $contract_code, // Mã hơp đồng
                    'contract_code'              => $contract_code,
                    
                    'cid'         => cid($contract->customerId, $contract->customerType),
                    'customer_code'         => cid($contract->customerId, $contract->customerType),
                    'customer'              => $contract->customerName ?? '---',
                    'Service'               => 'GOOGLE ADS',
                    'payment_type'          => $payment_type_enums[$payment_type] ?? '',
                    'typeOfService'         => $typeOfService,

                    'sale_name'             => $sale['display_name'] ?? null,
                    'sale_email'            => $sale['user_email'] ?? null,
                    'adaccount_id'          => $segment->adaccountName,
                    'adaccount_name'        => $segment->adaccountName,
                    'tech'                  => $techStaffs,
                    'contract_daterange'    => $contract_daterange,
                    'contract_value'        => $contract_value,
                    'start_date'            => my_date($segment->segmentStartDate, 'd/m/Y'),
                    'end_date'              => $segment->segmentEndDate ? my_date($segment->segmentEndDate, 'd/m/Y') : '--',
                    'term_status'           => $this->config->item($contract->term_status, 'contract_status'),
                    'remain_budget'         => (double) $remain_budget,
                    'currency'              => $account_currency,
                    'exchange'              => $exchange_rate,
                    'cost'                  => $cost,
                    'service_fee'           => (int) ($cost*$service_rate),
                    'discount_amount'       => (int) $discount_amount,
                    'service_fee_rate'          => (double) ($service_rate*100),
                    'service_fee_rate_actual'   => (double) ($service_fee_rate_actual*100),
                ];
            }
        }

        return $output;
    }

    public function export($args)
    {
        $title  = $args['title'];
        $data   = $args['data'];

        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));
        $sheet = $spreadsheet->getActiveSheet();
        
        $columns = $this->getColumns('export');
        $sheet->fromArray(array_column(array_values($columns), 'label'), NULL, 'A1');

        $rowIndex = 2;

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column)
            {
                switch ($column['type'])
                {
                    case 'number': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'decimal': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("0.00");
                        break;

                    case 'timestamp': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }    

                @$sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item[$key]);
                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }

    protected function getColumns($type = 'default')
    {
        $enums = array(

            'contract_code'             => [ 'type' => 'string', 'label' => 'Mã hợp đồng' ],
            'contract_code_link'        => [ 'type' => 'string', 'label' => 'Mã hợp đồng' ],
            'contract_code_link_tag'    => [ 'type' => 'string', 'label' => 'Mã hợp đồng' ],
            'customer_code'             => [ 'type' => 'string', 'label' => 'Customer ID' ],
            'customer'                  => [ 'type' => 'string', 'label' => 'Khách hàng' ],
            'Service'                   => [ 'type' => 'string', 'label' => 'Dịch vụ' ],
            'payment_type'              => [ 'type' => 'string', 'label' => 'Loại' ],
            'typeOfService'              => [ 'type' => 'string', 'label' => 'Loại hình dịch vụ' ],
            'cid'                       => [ 'type' => 'string', 'label' => 'Mã Khách Hàng' ],
            'adaccount_id'              => [ 'type' => 'string', 'label' => 'AdAccount ID' ],
            'adaccount_name'            => [ 'type' => 'string', 'label' => 'AdAccount Name' ],
            'tech'                      => [ 'type' => 'string', 'label' => 'Kỹ thuật' ],
            'contract_daterange'        => [ 'type' => 'string', 'label' => 'T/G HĐ' ],
            'contract_value'            => [ 'type' => 'number', 'label' => 'Giá tị hợp đồng' ],
            'start_date'                => [ 'type' => 'string', 'label' => 'Ngày bắt đầu' ],
            'end_date'                  => [ 'type' => 'string', 'label' => 'Ngày kết thúc' ],
            'term_status'               => [ 'type' => 'string', 'label' => 'Trạng thái' ],
            'department'                => [ 'type' => 'string', 'label' => 'Phòng' ],
            'sale_name'                 => [ 'type' => 'string', 'label' => 'Tên nhân viên' ],
            'sale_email'                => [ 'type' => 'string', 'label' => 'Email nhân viên' ],
            'currency'                  => [ 'type' => 'string', 'label' => 'Currecy' ],
            'remain_budget'             => [ 'type' => 'number', 'label' => 'Ngân sách còn lại' ],
            'exchange'                  => [ 'type' => 'number', 'label' => 'Tỉ giá' ],
            'cost'                      => [ 'type' => 'number', 'label' => 'Ngân sách chạy trong tháng' ],
            'service_fee'               => [ 'type' => 'number', 'label' => 'Phí quản lý' ],
            'discount_amount'           => [ 'type' => 'number', 'label' => 'Giảm giá' ],
            'service_fee_rate'          => [ 'type' => 'decimal', 'label' => '% Phí quản lý' ],
            'service_fee_rate_actual'   => [ 'type' => 'decimal', 'label' => '% Phí quản lý (thực tế)' ],
        );

        $config = array(

            'default' => array(
                'contract_code_link_tag',
                'cid',
                'customer',
                'Service',
                'payment_type',
                'typeOfService',
                'adaccount_id',
                'tech',
                'contract_daterange',
                'contract_value',
                'start_date',
                'end_date',
                'term_status',
                'department',
                'sale_name',
                'sale_email',
                'currency',
                'remain_budget',
                'exchange',
                'cost',
                'service_fee',
                'discount_amount',
                'service_fee_rate',
                'service_fee_rate_actual'
            ),

            'export' => array(
                'contract_code',
                'cid',
                'customer',
                'Service',
                'payment_type',
                'typeOfService',
                'customer_code',
                'adaccount_id',
                'adaccount_name',
                'tech',
                'contract_daterange',
                'contract_value',
                'start_date',
                'end_date',
                'term_status',
                'department',
                'sale_name',
                'sale_email',
                'currency',
                'remain_budget',
                'exchange',
                'cost',
                'service_fee',
                'discount_amount',
                'service_fee_rate',
                'service_fee_rate_actual'
            )
        );

        return elements($config[$type], $enums);
    }
}
/* End of file DataReport.php */
/* Location: ./application/modules/googleads/controllers/api_v2/DataReport.php */