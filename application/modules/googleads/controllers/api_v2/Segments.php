<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Segments extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'googleads/mcm_account_m';
        $this->autoload['models'][] = 'ads_segment_m';
        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'log_m';
        $this->autoload['models'][] = 'googleads/insight_segment_m';
        
        parent::__construct($config);
    }

    /**
     * Create new Insight Record
     *
     * @param      int   $id     The identifier
     */
    public function insight_post(int $id = 0)
    {
        $segment = $this->insight_segment_m->select('post_id,start_date,end_date')->get($id);
        if(empty($segment)) parent::responseHandler([], ['Phân đoạn không tồn tại'], 'error', 400);

        $adAccount = $this->term_posts_m->get_post_terms($segment->post_id, $this->mcm_account_m->term_type);
        $adAccount AND $adAccount = reset($adAccount);

        $payload = parent::post(null, TRUE);

        $this->load->library('form_validation');
        $this->form_validation->set_data($payload);
        $this->form_validation->set_rules('time', 'time', 'required|numeric');
        $this->form_validation->set_rules('spend', 'spend', 'required|numeric');

        if( FALSE === $this->form_validation->run())
        {
            parent::responseHandler([], $this->form_validation->error_array(), 'error', 400);
        }

        ! empty($payload['time']) AND $payload['time'] = start_of_day($payload['time']);
                
        $related_contract_id = null;
        $related_contract = $this->insight_segment_m->contracts($id);
        $related_contract = reset($related_contract);
        if(!empty($related_contract))
        {
            $related_contract_id = $related_contract->term_id;
        }
        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                  && TRUE == (bool)get_term_meta_value($related_contract_id, 'is_manipulation_locked');
        if($is_manipulation_locked)
        {
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
            $time = end_of_day($payload['time']);
            if($time < $manipulation_locked_at)
            {
                parent::response([
                    'code' => 400,
                    'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                ]);
            }
        }

        $iSegments = $this->term_posts_m->get_term_posts($adAccount->term_id, $this->insight_segment_m->post_type, [
            'where' => [
                'start_date' => $payload['time'],
                'end_date' => $payload['time'],
                'post_type' => $this->insight_segment_m->post_type,
                'post_name' => 'day'
            ]
        ]);

        if( ! empty($iSegments)) parent::responseHandler([], ['Dữ liệu đã tồn tại, không thể thêm mới.'], 'error', 400);

        $_isegmentInsert = [
            'post_name'     => 'day',
            'post_type'     => $this->insight_segment_m->post_type,
            'start_date'    => $payload['time'],
            'end_date'      => $payload['time']
        ];

        $_isegment_id = $this->insight_segment_m->insert($_isegmentInsert);
        if(empty($_isegment_id))
        {
            parent::responseHandler([], ['Quá trình xử lý không thành công, Vui lòng thử lại !'], 'error', 400);
        }

        $metadata = array(
            'account_currency'  => 'VND',
            'account_id'        => $adAccount->term_slug,
            'account_name'      => "{$adAccount->term_name} - {$adAccount->term_slug}",
            'date_start'        => my_date($payload['time'], 'Y-m-d'),
            'date_stop'         => my_date($payload['time'], 'Y-m-d'),
            'spend'             => $payload['spend'] ?: 0,
        );

        array_walk($metadata, function($value, $key) use($_isegment_id){
            update_post_meta($_isegment_id, $key, $value);
        });

        $this->term_posts_m->set_term_posts($adAccount->term_id, [$_isegment_id], $this->insight_segment_m->post_type);

        $relatedContracts = $this->insight_segment_m->contracts($_isegment_id);
        $relatedContracts AND $this->sync_amount_contracts($relatedContracts);

        parent::responseHandler([], ['Chi tiêu đã được thêm vào tài khoản'], 'success', 200);
    }

    /**
     * Update the Insight Record
     *
     * @param      int   $id     The identifier
     */
    public function insight_put(int $id = 0, int $insight_segment_id = 0)
    {
        $iSegment = $this->insight_segment_m->get($insight_segment_id);
        if(empty($iSegment)) parent::responseHandler([], ['Chi tiêu không tồn tại'], 'success', 400);

        $adAccount = $this->term_posts_m->get_post_terms($iSegment->post_id, $this->mcm_account_m->term_type);
        $adAccount AND $adAccount = reset($adAccount);

        $segments = $this->term_posts_m->get_term_posts($adAccount->term_id, $this->ads_segment_m->post_type, [
            'where' => [ 'term_posts.post_id' => $id ]
        ]);

        if(empty($segments)) parent::responseHandler([], ['Phân đoạn không tồn tại'], 'error', 400);

        $payload = parent::put(null, TRUE);

        $this->load->library('form_validation');
        $this->form_validation->set_data($payload);
        $this->form_validation->set_rules('time', 'time', 'required|numeric');
        $this->form_validation->set_rules('spend', 'spend', 'required|numeric');
        $this->form_validation->set_rules('adaccount_id', 'adaccount_id', "less_than_equal_to[{$adAccount->term_id}]|greater_than_equal_to[{$adAccount->term_id}]");

        if( FALSE === $this->form_validation->run())
        {
            parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $related_contract_id = null;
        $related_contract = $this->insight_segment_m->contracts($insight_segment_id);
        $related_contract = reset($related_contract);
        if(!empty($related_contract))
        {
            $related_contract_id = $related_contract->term_id;
        }
        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                  && TRUE == (bool)get_term_meta_value($related_contract_id, 'is_manipulation_locked');
        if($is_manipulation_locked)
        {
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);

            $insight = $this->insight_segment_m->get_by(['post_id' => $insight_segment_id]);
            $insight_time = end_of_day($insight->start_date);
            $insight_spend = (int) get_post_meta_value($insight_segment_id, 'spend');
            
            $time = end_of_day($payload['time']);
            $spend = $payload['spend'] ?: 0;
            if($insight_time < $manipulation_locked_at
                && ($insight_time != $time
                    || $insight_spend != $spend)
            )
            {
                parent::response([
                    'code' => 400,
                    'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                ]);
            }
        }

        ! empty($payload['time']) AND $payload['time'] = start_of_day($payload['time']);

        $iSegments = $this->term_posts_m->get_term_posts($adAccount->term_id, $this->insight_segment_m->post_type, [
            'where' => [
                'start_date' => $payload['time'],
                'end_date' => $payload['time'],
                'post_type' => $this->insight_segment_m->post_type,
                'post_name' => 'day',
                'posts.post_id !=' => $insight_segment_id
            ]
        ]);

        if( ! empty($iSegments)) parent::responseHandler([], ['Chi tiêu không tồn tại, không thể cập nhật.'], 'error', 400);

        $this->insight_segment_m->update($iSegment->post_id, [
            'start_date'    => $payload['time'],
            'end_date'      => $payload['time']
        ]);

        update_post_meta($iSegment->post_id, 'date_start', my_date($payload['time'], 'Y-m-d'));
        update_post_meta($iSegment->post_id, 'date_stop', my_date($payload['time'], 'Y-m-d'));
        update_post_meta($iSegment->post_id, 'spend', $payload['spend'] ?: 0);

        $relatedContracts = $this->insight_segment_m->contracts($iSegment->post_id);
        $relatedContracts AND $this->sync_amount_contracts($relatedContracts);
        
        parent::responseHandler([], ['Chi tiêu đã được cập nhật'], 'success', 200);
    }

    /**
     * Delete the Insight Record
     *
     * @param      int   $id     The identifier
     */
    public function insight_delete(int $id = 0, int $insight_segment_id = 0)
    {
        $iSegment = $this->insight_segment_m->get($insight_segment_id);
        if(empty($iSegment)) parent::responseHandler([], ['Chi tiêu không tồn tại'], 'error', 400);

        $adAccount = $this->term_posts_m->get_post_terms($iSegment->post_id, $this->mcm_account_m->term_type);
        $adAccount AND $adAccount = reset($adAccount);

        $segments = $this->term_posts_m->get_term_posts($adAccount->term_id, $this->ads_segment_m->post_type, [
            'where' => [ 'term_posts.post_id' => $id ]
        ]);

        if(empty($segments)) parent::responseHandler([], ['Phân đoạn không tồn tại'], 'error', 400);

        $related_contract_id = null;
        $relatedContracts = $this->insight_segment_m->contracts($insight_segment_id);
        $relatedContracts = reset($relatedContracts);
        if(!empty($relatedContracts))
        {
            $related_contract_id = $relatedContracts->term_id;
        }
        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                  && TRUE == (bool)get_term_meta_value($related_contract_id, 'is_manipulation_locked');
        if($is_manipulation_locked)
        {
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);

            $insight = $this->insight_segment_m->get_by(['post_id' => $insight_segment_id]);
            $insight_time = end_of_day($insight->start_date ?? 0);
            if($insight_time < $manipulation_locked_at)
            {
                parent::response([
                    'code' => 400,
                    'message' => 'Hợp đồng đã khoá thao tác vào ' . my_date($manipulation_locked_at, 'd/m/Y') . '. Vui lòng liên hệ bộ phận kế toán để mở khoá.'
                ]);
            }
        }

        $this->insight_segment_m->delete($insight_segment_id);

        $relatedContracts AND $this->sync_amount_contracts($relatedContracts);

        parent::responseHandler([], ['Chi tiêu đã được xoá'], 'success', 200);
    }

    public function sync_amount_contracts($contracts)
    {
        array_walk($contracts, function($x){

            /* Init the instance of contract behaviour_m */
            $contract = (new googleads_m())->set_contract($x);

            $behaviour_m    = $contract->get_behaviour_m();
            try
            {   
                $behaviour_m->get_the_progress(); /* Get progress info */
                $behaviour_m->sync_all_amount(); /* Update all metadata in relations */
            }
            catch (Exception $e)
            {

            }
        });
    }
}
/* End of file MCCReport.php */
/* Location: ./application/modules/googleads/controllers/api_v2/MCCReport.php */