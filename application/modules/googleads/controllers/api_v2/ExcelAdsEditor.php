<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'vendor/autoload.php';

class ExcelAdsEditor extends MREST_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->config('googleads/excel_adseditor');

        define('EXTRACT_RATE' , (float) $this->config->item('EXTRACT_RATE', 'adseditor'));
        define('PHRASE_RATE' , (float) $this->config->item('PHRASE_RATE', 'adseditor'));
        define('BROAD_RATE' , (float) $this->config->item('BROAD_RATE', 'adseditor'));
        define('ADGROUP_DEFAULT_CPC' , (float) $this->config->item('ADGROUP_DEFAULT_CPC', 'adseditor'));
        define('ADGROUP_DEFAULT_CPM' , (float) $this->config->item('ADGROUP_DEFAULT_CPM', 'adseditor'));
    }

    /**
     * Retrieve File Data and Create file token for this Files
     */
    public function upload_post()
    {
        $upload_cfg = ['upload_path' => './tmp/', 'allowed_types' => 'xlsx', 'max_size' => '4096','file_name' => md5(time())];

        if(!is_dir($upload_cfg['upload_path'])) mkdir($upload_cfg['upload_path'], '0777', TRUE);

        $this->load->library('upload');
        $this->upload->initialize($upload_cfg);

        if( ! $this->upload->do_upload('inputFile')) 
            parent::response(['message'=>'Tệp không hợp lệ , vui lòng thử lại !','status' => FALSE], parent::HTTP_NOT_ACCEPTABLE);

        $data       = $this->upload->data();
        $file_name  = "{$upload_cfg['upload_path']}{$data['file_name']}";

        try
        {
        	$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file_name);
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($file_name);
        }
        catch (Exception $e) { parent::response($e->getMessage(), parent::HTTP_INTERNAL_SERVER_ERROR); }

        $sheetData      = $spreadsheet->getActiveSheet()->toArray(null, TRUE,FALSE,TRUE);
        $sheetHeading   = array_shift($sheetData);

        if(empty($sheetData)) parent::response("Không có dữ liệu trong file Xlxs được Upload", parent::HTTP_NOT_ACCEPTABLE);


        $this->load->helper('text');

        $adGroups = array();

        foreach ($sheetData as $row)
        {
            $campaign   = $row['A'];

            if(empty($campaign)) continue;

            $adGroup    = $row['B'];
            $keyword    = $row['C'];
            $maxCPC     = (int) $row['D'];
            $finalURL   = $row['E'];
            $finalMobileURL = $row['F'];

            $path_1 = $row['G'];
            $path_2 = $row['H'];

            // Get headline
            $headLine = [];            
            $headLine_col = ['I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W'];
            for ($i = 0; $i < count($headLine_col); $i++) { 
                if(!empty($row[$headLine_col[$i]])){
                    $headLine[] = $row[$headLine_col[$i]];
                }
            }

            // Get description
            $description = [];
            $description_col = ['X', 'Y', 'Z', 'AA'];
            for ($i = 0; $i < count($description_col); $i++) { 
                if(!empty($row[$description_col[$i]])){
                    $description[] = $row[$description_col[$i]];
                }
            }

            $keywords   = array();
            $unaccentedkeyword      = convert_accented_characters($keyword);
            $isUnaccentedkeyword    = ($keyword == convert_accented_characters($keyword));

            //Extractly keyword
            $keywords[] = array(
                'keyword'       => $keyword,
                'criterionType' => 'Exact',
                'maxCPC'        => $maxCPC * EXTRACT_RATE,
            );

            if( ! $isUnaccentedkeyword)
            {
                $keywords[] = array(
                    'keyword'       => $unaccentedkeyword,
                    'criterionType' => 'Exact',
                    'maxCPC'        => $maxCPC * EXTRACT_RATE,
                );
            }

            //Phrase keyword
            $keywords[] = array(
                'keyword'       => $keyword,
                'criterionType' => 'Phrase',
                'maxCPC'        => $maxCPC * PHRASE_RATE,
            );

            if( ! $isUnaccentedkeyword)
            {
                $keywords[] = array(
                    'keyword'       => $unaccentedkeyword,
                    'criterionType' => 'Phrase',
                    'maxCPC'        => $maxCPC * PHRASE_RATE,
                );  
            }

            $adGroups[] = array(
                'campaign'      => $campaign,
                'adGroup'       => $adGroup,
                'maxCPC'        => ADGROUP_DEFAULT_CPC,
                'maxCPM'        => ADGROUP_DEFAULT_CPM,
                'keywords'      => $keywords,
                'headLine'      => $headLine,
                'description'   => $description,
                'path_1'        => $path_1,
                'path_2'        => $path_2,
                'finalURL'       => $finalURL,
                'finalMobileURL' => $finalMobileURL
            );
        }

        $file_token = uniqid('excelAdsEditor-upload-'. time().'-');
        $this->scache->write($adGroups, "modules/googleads/actions/{$file_token}", 60 * 60);

        $response = array(
            'status' => TRUE,
            'message' => "Import dữ liệu thành công .",
            'data' => array(
                'fileToken' => $file_token,
                'adGroups' => $adGroups
            )
        );

        parent::response($response, parent::HTTP_OK);
    }


    /**
     * Parse & Export CVS Format For Adword Editor
     */
    public function export_post()
    {
        $fileToken = parent::post('fileToken');

        if(empty($fileToken)) parent::response("Invalid File Import Token [0]", parent::HTTP_FORBIDDEN);

        $cache_key  = "modules/googleads/actions/{$fileToken}";
        $adGroups   = $this->scache->get($cache_key);
        if(empty($adGroups)) parent::response("Data Not Found [1]", parent::HTTP_NOT_FOUND);

        try
        {
            $export_template    = FCPATH . 'files/excel_tpl/tool/google-adwords-editors-export-template-v2.csv';
            $reader             = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($export_template);
            $spreadsheet        = $reader->load($export_template);
        }
        catch (Exception $e) { parent::response($e->getMessage(), parent::HTTP_INTERNAL_SERVER_ERROR); }

        $spreadsheet
        ->getProperties()
        ->setCreator('ADSPLUS.VN')
        ->setLastModifiedBy('ADSPLUS.VN')
        ->setTitle('Export Adword Editor XLSX File')
        ->setSubject('Export Adword Editor XLSX File')
        ->setDescription('The File Converted From Adsplus Template XLSX');

        $spreadsheet->setActiveSheetIndex(0);

        $iRow = 2;
        foreach ($adGroups as $adGroup) 
        {
            $adGroup = (array) $adGroup;
            // Set cells value for CAMPAIGN
            $spreadsheet->getActiveSheet()
            ->setCellValue("A{$iRow}", $adGroup['campaign']) // campaign name
            ->setCellValue("G{$iRow}", 'en;vi') // campaign languages
            ->setCellValue("O{$iRow}", 'Location of presence'); // campaign targeting method
            $iRow++;

            // Set cells value for AD GROUP
            $spreadsheet->getActiveSheet()
            ->setCellValue("A{$iRow}", $adGroup['campaign']) // campaign name
            ->setCellValue("R{$iRow}", $adGroup['adGroup']) // ad group
            ->setCellValue("T{$iRow}", $adGroup['maxCPM']); // Max CPM
            $iRow++;

            // Set keywords of each AD GROUP
            foreach ($adGroup['keywords'] as $keyword)
            {
                $keyword = (array) $keyword;
                // Set cells value for KEYWORD
                $spreadsheet->getActiveSheet()
                ->setCellValue("A{$iRow}", $adGroup['campaign']) // campaign name
                ->setCellValue("R{$iRow}", $adGroup['adGroup']) // ad group
                ->setCellValue("S{$iRow}", $keyword['maxCPC']) // Max CPC
                ->setCellValue("AU{$iRow}", $keyword['criterionType']) // Criterion Type
                ->setCellValue("AT{$iRow}", $keyword['keyword']); // Keyword

                $iRow++;
            }

            $spreadsheet->getActiveSheet()
                ->setCellValue("A{$iRow}", $adGroup['campaign']) // campaign name
                ->setCellValue("R{$iRow}", $adGroup['adGroup']) // ad group
                ->setCellValue("BC{$iRow}", $adGroup['finalURL']) // Final URL
                ->setCellValue("BD{$iRow}", $adGroup['finalMobileURL']) // Final mobile URL
                ->setCellValue("CR{$iRow}", $adGroup['path_1']) // Path 1
                ->setCellValue("CS{$iRow}", $adGroup['path_2']); // Path 2
            
            // Set headline of each AD GROUP
            $headLine_col = ['BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI'];
            $iCol = 0;
            foreach ($adGroup['headLine'] as $headline) 
            {
                // Set cells value for headline
                $spreadsheet->getActiveSheet()
                ->setCellValue("{$headLine_col[$iCol]}{$iRow}", '"' . $headline . '"');
                $iCol += 2;
            }

            // Set description of each AD GROUP
            $description_col = ['CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ'];
            $iCol = 0;
            foreach ($adGroup['description'] as $description) 
            {
                // Set cells value for description
                $spreadsheet->getActiveSheet()
                ->setCellValue("{$description_col[$iCol]}{$iRow}", '"' . $description . '"');
                $iCol += 2;
            }
        } 

        $file_name  = 'tmp/' . uniqid('export-converted-csv-adword-editor-') . '.csv';
        $writer     = new \PhpOffice\PhpSpreadsheet\Writer\Csv($spreadsheet);

        $writer->setUseBOM(true);
        $writer->setEnclosure('');
        $writer->save(FCPATH . $file_name);

    	parent::response(['status' => TRUE, 'message' => 'File đã được export thành công.','fileName' => base_url($file_name)]);
    }

    /**
     * Retrieve File Data and Create file token for this Files
     */
    public function upload_v2_1_post()
    {
        if(!isset($_FILES['inputFile'])) parent::responseHandler([], 'Không tìm thấy file', 'failed', 400);
        
        $upload_cfg = ['upload_path' => './tmp/', 'allowed_types' => 'xlsx', 'max_size' => '20480', 'file_name' => md5(time())];
        if(!is_dir($upload_cfg['upload_path'])) mkdir($upload_cfg['upload_path'], '0777', TRUE);

        $this->load->library('upload');
        $this->upload->initialize($upload_cfg);

        if( ! $this->upload->do_upload('inputFile')) {
            parent::response(['message'=>'Tệp không hợp lệ , vui lòng thử lại !','status' => FALSE], parent::HTTP_NOT_ACCEPTABLE);
        }

        $data       = $this->upload->data();
        $file_name  = "{$upload_cfg['upload_path']}{$data['file_name']}";

        $ttl = 60 * 60 * 2; // 2 hours
        $time_expire = time() + $ttl;
        
        $data = [
            'file_name' => $file_name,
            'time_expire' => $time_expire
        ];
        
        $file_token = md5(json_encode($data));
        $cache_key = 'modules/googleads/convert/' . $file_token;
        $this->scache->write($data, $cache_key);

        // Dispatch job
        $this->load->config('amqps');
		$amqps_host 	= $this->config->item('host', 'amqps');
		$amqps_port 	= $this->config->item('port', 'amqps');
		$amqps_user 	= $this->config->item('user', 'amqps');
		$amqps_password = $this->config->item('password', 'amqps');
		$queue          = $this->config->item('utility', 'amqps_queues');

		$connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
		$channel 	= $connection->channel();
		$channel->queue_declare($queue, false, true, false, false);

        $payload = [
            'event' => 'utility.convert.adword',
            'payload' => [
                'file_token' => $file_token
            ]
        ];

        $message = new \PhpAmqpLib\Message\AMQPMessage(
            json_encode($payload),
            array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $channel->basic_publish($message, '', $queue);	

		$channel->close();
		$connection->close();

        // Rreturn response
        $response = ['fileToken' => $file_token];
        parent::responseHandler($response, 'success');
    }

    /**
     * Parse & Export CVS Format For Adword Editor
     */
    public function export_v2_1_get()
    {
        $file_token = parent::get('file_token');
        if(empty($file_token)) parent::responseHandler([], 'Invalid File Import Token [0]', 'error', parent::HTTP_FORBIDDEN);

        $cache_key = 'modules/googleads/convert/' . $file_token;
        $data      = $this->scache->get($cache_key);
        if(empty($data)) parent::responseHandler([], 'Invalid File Import Token [1]', 'error', parent::HTTP_NOT_ACCEPTABLE);

        // Handle error
        $time_expire = $data['time_expire'] ?? 0;
        if(time() > $time_expire) parent::responseHandler([], 'Invalid File Import Token [2]', 'error', parent::HTTP_NOT_ACCEPTABLE);
        if(!empty($data['error'])) parent::responseHandler([], $data['error'], 'error', parent::HTTP_NOT_ACCEPTABLE);

        // Download file
        $is_download = (bool) parent::get('is_download');
        if($is_download){
            $export_file = $data['export_file'];
            if(empty($export_file)) parent::responseHandler([], 'Không tìm thấy file dữ liệu', 'error', parent::HTTP_BAD_REQUEST);

            parent::responseHandler([
                'file_url' => base_url($export_file)
            ], 'File đã được convert');
        }

        // Handle result
        if(!isset($data['result'])){
            $progress_percent = $data['progress_percent'] ?? 0;

            parent::responseHandler(['progress_percent' => $progress_percent], 'File đang convert', 'success', parent::HTTP_ACCEPTED);
        }
        
        $response = [
            'result' => $data['result'],
            'progress_percent' => 100,
        ];
        parent::responseHandler($response, 'File đã được convert');
    }
}
/* End of file ExcelAdsEditor.php */
/* Location: ./application/modules/googleads/controllers/api_v2/ExcelAdsEditor.php */