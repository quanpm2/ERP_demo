<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class MCCReport extends MREST_Controller
{

    public function __construct()
    {
        $this->autoload['models'][] = 'customer/customer_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'googleads/mcm_account_m';
        $this->autoload['models'][] = 'googleads/base_adwords_m';
        $this->autoload['models'][] = 'ads_segment_m';
        parent::__construct();
        $this->load->config('contract/contract');
    }

    public function quarter_get( int $quarter = null, int $year = null)
    {
        $args = wp_parse_args([parent::get(null, TRUE)], [
            'q' => my_date(time(), 'm'),
            'year' => my_date(time(), 'Y')
        ]);

        $start_time = start_of_day(parent::get('start_time', TRUE));
        $end_time   = start_of_day(parent::get('end_time', TRUE));

        $m_args = array(
            ['key' => 'result_updated_on', 'value' => $start_time, 'compare' => '>='],
            ['key' => 'mcm_client_id', 'value' => "", 'compare' => '!='],
            ['key' => 'contract_begin', 'value' => "", 'compare' => '!='],
            ['key' => 'contract_end', 'value' => "", 'compare' => '!='],
            ['key' => 'googleads-begin_time', 'value' => "", 'compare' => '!=']
        );

        $cache_key  = 'modules/googleads/mccreport-quarter-'.md5(json_encode([$start_time, $end_time]));
        $terms      = $this->scache->get($cache_key);

        $payment_type_enums = [
            'normal' => 'Full VAT',
            'customer' => 'Khách hàng tự thanh toán',
            'behalf' => 'ADSPLUS thu & chi hộ'
        ];

        if(empty($terms))
        {
            // $terms = $this->googleads_m->select('term.term_id')->m_find($m_args)->set_term_type()->as_array()->get_all();
            $_field = 'googleads-end_time';
            $_alias = uniqid('googleads_end_time');
            $terms = $this->googleads_m
            ->set_term_type()
            ->select("term.term_id, {$_alias}.meta_value as '{$_field}'")
            ->m_find($m_args)
            ->join("termmeta {$_alias}", "{$_alias}.term_id = term.term_id AND {$_alias}.meta_key = '{$_field}'", 'left')
            ->group_start()
                ->or_group_start() 
                    ->where("{$_alias}.meta_value <=", $end_time)
                ->group_end()
                ->or_where("{$_alias}.meta_value is null")
                ->or_where("{$_alias}.meta_value", 0)
            ->group_end()
            ->as_array()
            ->get_all();

            $terms = array_map(function($x) use($end_time){ 
                $x['googleads-end_time'] = get_term_meta_value($x['term_id'], 'googleads-end_time') ?: $end_time;
                return $x;
            }, $terms);
        }

        if(empty($terms)) throw new Exception('Không tìm thấy bất kỳ hợp đồng  nào');
        $this->scache->write($terms, $cache_key, 60);

        $customers = [];
        $contract_ids = array_unique(array_column($terms, 'term_id'));
        $customers = $this->customer_m
        ->select('user.user_id, display_name, term_users.term_id')
        ->set_user_type()
        ->join('term_users','term_users.user_id = user.user_id')
        ->where_in('term_users.term_id', $contract_ids)
        ->as_array()
        ->group_by('term_users.term_id')
        ->get_all();

        $customers = array_column($customers, NULL, 'term_id');

        $this->load->model('googleads/adword_calculation_m');
        $this->load->model('googleads/base_adwords_m');

        $data = array();
        $mccs = array_group_by($terms, 'mcm_client_id');

        foreach ($mccs as $mcc_id => $contracts)
        {
            $mcc = get_term_meta_value($mcc_id, 'customer_id');
            $currency_code = get_term_meta_value($mcc_id, 'currency_code');

            foreach ($contracts as $contract)
            {
                $mccs[$mcc_id] = array(
                    'contracts' => $contracts,
                    'account_name' => get_term_meta_value($mcc_id, 'account_name'),
                    'contract_type' => count($contracts) > 1 ? 'renewal' : 'new'
                );

                $is_renewal = ! ((bool) get_term_meta_value($contract['term_id'], 'is_first_contract'));
                $interval   = (new DateTime())->setTimestamp($contract['googleads-begin_time'])->diff((new DateTime())->setTimestamp($contract['googleads-end_time']));

                $month_durations = number_format($interval->m + ($interval->d/30), 1); // Thời gian hợp đồng

                $this->base_adwords_m->set_mcm_account($mcc_id, FALSE);
                $this->adword_calculation_m->set_base_adword_model($this->base_adwords_m)
                ->set_report_type('ACCOUNT_PERFORMANCE_REPORT');

                // $cost = $this->adword_calculation_m->calc_cost( max([$contract['googleads-begin_time'], $start_time]), $contract['googleads-end_time']);
                $cost = $this->adword_calculation_m->calc_cost( max([$contract['googleads-begin_time'], $start_time]), min($contract['googleads-end_time'], end_of_month($end_time)));

                $account_currency_code = get_term_meta_value($contract['term_id'], 'account_currency_code');
                // $meta_dollar_exchange_rate_key = 'dollar_exchange_rate_' . strtolower($account_currency_code) . '_to_vnd';
                // $exchange_rate = (float) (get_term_meta_value($contract['term_id'], $meta_dollar_exchange_rate_key) ?: get_exchange_rate());
                $exchange_rate = get_exchange_rate($account_currency_code, $contract['term_id']);
                
                $budget         = $this->googleads_m->set_contract($contract['term_id'])->get_behaviour_m()->calc_budget();
                $service_fee    = (int) get_term_meta_value($contract['term_id'], 'service_fee');
                $service_rate   = div($service_fee, $budget);
                $contract_code  = get_term_meta_value($contract['term_id'], 'contract_code');

                $payment_type = 'normal';
                if('customer' == get_term_meta_value($contract['term_id'], 'contract_budget_payment_type'))
                {
                    $payment_type = 'customer';
                    'behalf' == get_term_meta_value($contract['term_id'], 'contract_budget_customer_payment_type') AND $payment_type = 'behalf';
                }

                $data[] = array(
                    $mcc,        
                    $customers[$contract['term_id']]['display_name'] ?? '---',
                    $contract_code,
                    ($is_renewal ? 'renewal' : 'new'),
                    $payment_type_enums[$payment_type] ?? '',
                    $month_durations,
                    my_date($contract['contract_begin']),
                    my_date($contract['googleads-begin_time']),
                    my_date($contract['contract_end']),
                    my_date($contract['googleads-end_time']),
                    $cost,
                    $account_currency_code,
                    $exchange_rate,
                    $cost*$service_rate,
                    currency_numberformat($service_rate*100, '%', 2),
                    $account_currency_code == 'VND' ? div($cost, $exchange_rate) : $cost,
                    ($account_currency_code == 'VND' ? div($cost, $exchange_rate) : $cost)*$service_rate,
                );
            }
        }

        $this->load->library('table');
        $this->table->set_heading(['Customer ID', 'Customer Name', 'Contract Code', 'Contract type', 'Payment Type', 'Total contract durations (months)', 'Contract Start date (contract)', 'Contract Start date (kick of campaign)', 'Contract End date (contract)', 'Contract End date (Off Campaign)', 'Media spend this Quarter','currency', 'Exchange', 'Management fee on Media Spend this quarter', 'Management fee %','Media spend this Quarter (USD)', 'Management fee on Media Spend this quarter (USD)']);
        echo $this->table->generate($data);
        die();
    }

    /**
     * Googleads Ads Monthly Spend
     *
     * @throws     Exception  (description)
     */
    public function monthly_get()
    {
        $args = wp_parse_args(parent::get(null, TRUE), [
            'month' => my_date(time(), 'm'),
            'year' => my_date(time(), 'Y')
        ]);

        $this->load->library('form_validation');
        $this->form_validation->set_data($args);
        $this->form_validation->set_rules('month', 'Month', 'required|integer|greater_than_equal_to[1]|less_than_equal_to[12]');
        $this->form_validation->set_rules('year', 'Year', 'required|integer|greater_than_equal_to[2015]|less_than_equal_to['.my_date(strtotime("+1 year"), 'Y').']');
        if( FALSE == $this->form_validation->run()) parent::response([ 'code' => 400, 'error' => $this->form_validation->error_array() ]);

        $start_time = start_of_month(date("{$args['year']}/{$args['month']}/d"));
        $end_time   = end_of_month(date("{$args['year']}/{$args['month']}/d"));

        $cacheStacks = [ 'functions', CI::$APP->router->fetch_module(), get_called_class(), __FUNCTION__, md5(json_encode($args)) ];

        $cache_key = implode('/', array_merge($cacheStacks, ['raw']));
        $raw = $this->scache->get($cache_key);
        if( ! $raw)
        {
            $raw = $this->ads_segment_m
            ->set_post_type()
            ->select('posts.post_id, start_date, end_date, term_posts.term_id, term.*')
            ->join('term_posts', 'term_posts.post_id = posts.post_id')
            ->join('term', 'term.term_id = term_posts.term_id')
            ->where_in('term.term_type', [ $this->googleads_m->term_type, $this->mcm_account_m->term_type ])
            ->where_not_in('term.term_status', [ 'draft','unverified','waitingforapprove','remove'])
            ->where('start_date <=', $end_time)
            ->where('if(end_date = 0 or end_date is null, UNIX_TIMESTAMP (), end_date) >=', $start_time)
            
            ->order_by('term.term_id', 'desc')
            ->get_all();

            $this->scache->write($raw, $cache_key, 5*300);
        }

        $data           = array();
        $contracts      = array();
        $raw            = array_group_by($raw, 'term_type');
        $adAccounts     = array_column(($raw[$this->mcm_account_m->term_type] ?: []), null, 'post_id');
        $contractsRaw   = array_group_by(($raw[$this->googleads_m->term_type] ?: []), 'term_id');

        /* LOAD ALL STAFFS OF ADS CONTRACTS */
        $this->load->model('googleads/googleads_kpi_m');
        $cache_key = implode('/', array_merge($cacheStacks, ['kpis']));
        $kpis = $this->scache->get($cache_key);
        if( ! $kpis)
        {
            $kpis = $this->googleads_kpi_m
            ->select('term_id, user_id')
            ->where_in('term_id', array_unique(array_keys($contractsRaw)))
            ->setTechnicalKpiTypes()
            ->group_by('term_id')
            ->as_array()
            ->get_all();

            $this->scache->write($kpis, $cache_key, 5*300);
        }

        $users = array();
        if($kpis)
        {
            $userIds = array_unique(array_column($kpis, 'user_id'));
            foreach ($userIds as $userId)
            {
                $users[$userId] = $this->admin_m->get_field_by_id($userId);
            } 
            $kpis AND $kpis = array_group_by($kpis, 'term_id');
        }
        
        /* LOAD ALL STAFFS OF ADS CONTRACTS - END */
        $cache_key = implode('/', array_merge($cacheStacks, ['customers']));
        $customers = $this->scache->get($cache_key);
        if( ! $customers)
        {
            $customers = $this->customer_m
            ->select('user.user_id, display_name, user.user_type, term_users.term_id')
            ->set_user_type()
            ->join('term_users','term_users.user_id = user.user_id')
            ->where_in('term_users.term_id', array_unique(array_keys($contractsRaw)))
            ->as_array()
            ->group_by('term_users.term_id')
            ->get_all() ?: [];
            $customers AND $customers = array_column($customers, NULL, 'term_id');

            $this->scache->write($customers, $cache_key, 5*300);
        }

        $payment_type_enums = [
            'normal' => 'Full VAT',
            'customer' => 'Khách hàng tự thanh toán',
            'behalf' => 'ADSPLUS thu & chi hộ'
        ];

        foreach ($contractsRaw as $segments)
        {
            $_contract = reset($segments);

            /* PARSE TECHNICAL STAFFS */
            $techStaffs = "";
            empty($kpis[$_contract->term_id]) OR $techStaffs = implode(', ', array_filter(array_map(function($x) use ($users){
                if(empty($users[$x['user_id']])) return '';
                return $users[$x['user_id']]['display_name'] ?: $users[$x['user_id']]['user_email'];
            }, $kpis[$_contract->term_id])));
            /* PARSE TECHNICAL STAFFS - END */

            if( ! is_service_proc($_contract)) continue; 

            if('UNSPECIFIED' == get_term_meta_value($_contract->term_id, 'adaccount_status')) continue;

            if(empty($segments)) continue;

            $contract_id = $_contract->term_id;

            $cache_key = implode('/', array_merge($cacheStacks, [ "contract_{$_contract->term_id}", md5(json_encode($segments))]));
            $_segments = $this->scache->get($cache_key);
            if( ! $_segments)
            {
                $_segments = array_map(function($segment) use($adAccounts, $start_time, $end_time, $contract_id){

                    $segment->cost = 0;
                    $segment->adaccount_id = $adAccounts[$segment->post_id]->term_id;

                    $_start     = max($start_time, $segment->start_date);
                    $_end       = min($end_time, ($segment->end_date ?: time()));
                    if($_start > $_end) return $segment;

                    $this->base_adwords_m->set_mcm_account($segment->adaccount_id);
                    $account_data = $this->base_adwords_m->get_cache('ACCOUNT_PERFORMANCE_REPORT', $_start, $_end);
                    $account_data AND $account_data = array_filter($account_data);
                    if(empty($account_data)) return $segment;

                    $segment->cost = array_sum(array_map(function($x) use($contract_id){
                        $exchange_rate = get_exchange_rate($x['currency'], $contract_id);

                        ('VND' != $x['currency']) AND $x['cost']*= $exchange_rate;
                        return convertMicroCost( (double) $x['cost']);
                    }, array_merge(...$account_data)));

                    return (object) array(
                        'post_id'       => (int) $segment->post_id,
                        'start_date'    => (int) $segment->start_date,
                        'end_date'      => (int) $segment->end_date,
                        'cost'          => (double) $segment->cost,
                        'adaccount_id'  => (int) $segment->adaccount_id,
                    );

                }, $segments);

                $this->scache->write($_segments, $cache_key, 5*300);
            }

            $segments = $_segments;

            $contract_code  = get_term_meta_value($_contract->term_id, 'contract_code');
            $customer_code  = cid($customers[$_contract->term_id]['user_id'], $customers[$_contract->term_id]['user_type']);
            $customer       = $customers[$_contract->term_id]['display_name'] ?? '';

            $_contract_daterange    = array();
            $_contract_begin        = get_term_meta_value($_contract->term_id, 'contract_begin') AND $_contract_daterange[] = $_contract_begin;
            $_contract_end          = get_term_meta_value($_contract->term_id, 'contract_end') AND $_contract_daterange[] = $_contract_end;
            $_contract_daterange    = implode(' - ', array_map(function($x){ return my_date($x, 'd/m/Y'); }, $_contract_daterange));

            $budget = $this->googleads_m->set_contract($_contract->term_id)->get_behaviour_m()->calc_budget();
            $service_fee        = (int) get_term_meta_value($_contract->term_id, 'service_fee');
            $discount_amount    = $this->googleads_m->get_behaviour_m()->calc_disacount_amount();

            $service_rate               = div($service_fee, $budget);
            $service_fee_rate_actual    = div(max([$service_fee - $discount_amount, 0]), $budget);

            $saleId     = (int) get_term_meta_value($_contract->term_id, 'staff_business');
            $sale       = $this->admin_m->get_field_by_id($saleId);
            $department = $this->term_users_m->get_user_terms($saleId, 'department');
            $department AND $department = reset($department);

            $payment_type = 'normal';
            if('customer' == get_term_meta_value($_contract->term_id, 'contract_budget_payment_type'))
            {
                $payment_type = 'customer';
                'behalf' == get_term_meta_value($_contract->term_id, 'contract_budget_customer_payment_type') AND $payment_type = 'behalf';
            }

            $contract_m     = (new googleads_m())->set_contract($_contract);
            $actual_result  = (int) $contract_m->get_behaviour_m()->get_actual_result();
            $actual_budget  = (int) get_term_meta_value($_contract->term_id, 'actual_budget');
            $remain_budget  = $actual_budget - $actual_result;

            foreach ($segments as $segment)
            {   
                $mcc = get_term_meta_value($segment->adaccount_id, 'customer_id');
                $currency_code  = get_term_meta_value($segment->adaccount_id, 'currency_code');

                $_start     = max($start_time, $segment->start_date);
                $_end       = min($end_time, ($segment->end_date ?: time()));

                $item = array(
                    'contract_code'         => $contract_code, // Mã hơp đồng
                    'customer_code'         => $customer_code, // Mã khách hàng
                    'customer'              => $customer, // Khách hàng */
                    'Service'               => 'ADSPLUS', // Loại dịch vụ */
                    'payment_type'          => $payment_type_enums[$payment_type] ?? '',
                    'cid'                   => $mcc,
                    /*Kỹ thuật*/
                    'tech'                  => $techStaffs,
                    /*T/G HĐ*/
                    'contract_daterange'    => $_contract_daterange,
                    'contract_value'        => (int) get_term_meta_value($_contract->term_id, 'contract_value'),
                    'start_date'            => my_date($_start, 'd/m/Y'), // Ng.Kích hoạt
                    'end_date'              => my_date($_end, 'd/m/Y'), // Ng.Kết thúc
                    'term_status'           => $this->config->item($_contract->term_status, 'contract_status'),
                    'remain_budget'         => currency_numberformat($remain_budget),
                    'department'            => $department->term_name ?? null,
                    'sale_name'             => $sale['display_name'],
                    'sale_email'            => $sale['user_email'],
                    'currency'              => $currency_code,
                    /*N/S còn lại (hiện tại)*/
                    'remain_budget'         => $remain_budget,
                    'exchange'              => number_format($exchange_rate, 0, ',', ''),
                    'cost'                  => (int) number_format($segment->cost, 2, ',', ''),
                    'service_fee'           => (int) number_format($segment->cost*$service_rate, 2, ',', ''),
                    'discount_amount'       => (int) number_format($discount_amount, 2, ',', ''),
                    'service_fee_rate'          => currency_numberformat($service_rate*100, '%'),
                    'service_fee_rate_actual'   => currency_numberformat($service_fee_rate_actual*100, '%', 2),
                );

                $data[] = $item;
            }
        }

        $title          = 'Export dữ liệu Hợp Đồng & Khách hàng đang thực hiện trong tháng ' . date('m', $end_time);
        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));
        
        $sheet = $spreadsheet->getActiveSheet();

        $columns = array(
            'contract_code'     => [ 'type' => 'string', 'label' => 'Mã hợp đồng' ],
            'customer_code'     => [ 'type' => 'string', 'label' => 'Mã Khách Hàng' ],
            'customer'          => [ 'type' => 'string', 'label' => 'Khách hàng' ],
            'Service'           => [ 'type' => 'string', 'label' => 'Dịch vụ' ],
            'payment_type'      => [ 'type' => 'string', 'label' => 'Loại' ],
            'cid'               => [ 'type' => 'string', 'label' => 'Customer ID' ],
            'tech'              => [ 'type' => 'string', 'label' => 'Kỹ thuật' ],
            'contract_daterange' => [ 'type' => 'string', 'label' => 'T/G HĐ' ],
            'contract_value'    => [ 'type' => 'number', 'label' => 'Giá tị hợp đồng' ],
            'start_date'        => [ 'type' => 'string', 'label' => 'Ngày bắt đầu' ],
            'end_date'          => [ 'type' => 'string', 'label' => 'Ngày kết thúc' ],
            'term_status'       => [ 'type' => 'string', 'label' => 'Trạng thái' ],
            'department'        => [ 'type' => 'string', 'label' => 'Phòng' ],
            'sale_name'         => [ 'type' => 'string', 'label' => 'Tên nhân viên' ],
            'sale_email'        => [ 'type' => 'string', 'label' => 'Email nhân viên' ],
            'currency'          => [ 'type' => 'string', 'label' => 'Currecy' ],
            'remain_budget'     => [ 'type' => 'number', 'label' => 'Ngân sách còn lại' ],
            'exchange'          => [ 'type' => 'number', 'label' => '1 USD = ? VND' ],
            'cost'              => [ 'type' => 'number', 'label' => 'Ngân sách chạy trong tháng' ],
            'service_fee'       => [ 'type' => 'number', 'label' => 'Phí quản lý' ],
            'discount_amount'  => [ 'type' => 'number', 'label' => 'Giảm giá' ],
            'service_fee_rate'  => [ 'type' => 'number', 'label' => '% Phí quản lý' ],
            'service_fee_rate_actual'  => [ 'type' => 'number', 'label' => '% Phí quản lý (thực tế)' ],
        );

        $sheet->fromArray(array_column(array_values($columns), 'label'), NULL, 'A1');

        $rowIndex = 2;

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column)
            {
                switch ($column['type'])
                {
                    case 'number': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }    

                $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item[$key]);
                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }

    /**
     * Googleads Ads Dateragen Spend
     *
     * @throws     Exception  (description)
     */
    public function date_range_get($start = '', $end = '')
    {
        $start_date = DateTime::createFromFormat('Y-m-d', $start);
        $end_date   = DateTime::createFromFormat('Y-m-d', $end);

        if(empty($start_date) || empty($start_date))
            die('Tham số ngày bắt đầu & ngày kết thúc chưa được truyền vào đầy đủ.');

        $start_time = start_of_day($start_date->getTimestamp());
        $end_time = end_of_day($end_date->getTimestamp());

        $m_args = array(
            ['key' => 'mcm_client_id', 'value' => '', 'compare' => '!='],
            ['key' => 'contract_begin', 'value' => '', 'compare' => '!='],
            ['key' => 'contract_end', 'value' => '', 'compare' => '!='],
            ['key' => 'googleads-begin_time', 'value' => $end_time, 'compare' => '<=']
        );

        $_field = 'googleads-end_time';
        $_alias = uniqid('meta_');

        $this->googleads_m
        ->select("{$_alias}.meta_value as '{$_field}'")
        ->join("termmeta {$_alias}", "{$_alias}.term_id = term.term_id AND {$_alias}.meta_key = '{$_field}'", 'left')
        ->group_start()
            ->or_group_start() 
                ->where("{$_alias}.meta_value >=", $start_time)
                // ->where("{$_alias}.meta_value <=", $end_time)
            ->group_end()
            ->or_where("{$_alias}.meta_value is null")
            ->or_where("{$_alias}.meta_value", 0)
        ->group_end();

        $cache_key  = 'modules/googleads/mccreport-daterange-'.md5(json_encode([$start_time, $end_time]));
        $terms      = $this->scache->get($cache_key);
        if(empty($terms))
        {
            $terms = $this->googleads_m->select('term.term_id')->m_find($m_args)->set_term_type()->as_array()->get_all() AND 
            $terms = array_map(function($x) use($end_time){ 
                $x['googleads-end_time'] = get_term_meta_value($x['term_id'], 'googleads-end_time') ?: $end_time;
                return $x;
            }, $terms);
        }

        $this->db->reset_query();

        if(empty($terms)) throw new Exception('Không tìm thấy bất kỳ hợp đồng  nào');
        $this->scache->write($terms, $cache_key, 60);

        $customers = $this->customer_m
        ->select('user.user_id, display_name, term_users.term_id')
        ->set_user_type()
        ->join('term_users','term_users.user_id = user.user_id')
        ->where_in('term_users.term_id', array_unique(array_column($terms, 'term_id')))
        ->as_array()
        ->group_by('term_users.term_id')
        ->get_all() ?: [];

        $customers AND $customers = array_column($customers, NULL, 'term_id');

        $this->load->model('googleads/adword_calculation_m');
        $this->load->model('googleads/base_adwords_m');

        $data = array();
        $mccs = array_group_by($terms, 'mcm_client_id');

        $payment_type_enums = [
            'normal' => 'Full VAT',
            'customer' => 'Khách hàng tự thanh toán',
            'behalf' => 'ADSPLUS thu & chi hộ'
        ];

        foreach ($mccs as $mcc_id => $contracts)
        {
            $mcc = get_term_meta_value($mcc_id, 'customer_id');
            $currency_code = get_term_meta_value($mcc_id, 'currency_code');

            foreach ($contracts as $contract)
            {
                $mccs[$mcc_id] = array(
                    'contracts' => $contracts,
                    'account_name' => get_term_meta_value($mcc_id, 'account_name'),
                    'contract_type' => count($contracts) > 1 ? 'renewal' : 'new'
                );

                $is_renewal = ! ((bool) get_term_meta_value($contract['term_id'], 'is_first_contract'));
                $interval   = (new DateTime())->setTimestamp($contract['googleads-begin_time'])->diff((new DateTime())->setTimestamp($contract['googleads-end_time']));

                $month_durations = number_format($interval->m + ($interval->d/30), 1); // Thời gian hợp đồng

                $this->base_adwords_m->set_mcm_account($mcc_id, FALSE);
                $this->adword_calculation_m->set_base_adword_model($this->base_adwords_m)->set_report_type('ACCOUNT_PERFORMANCE_REPORT');

                $cost = $this->adword_calculation_m->calc_cost( max([$contract['googleads-begin_time'], $start_time]), min($contract['googleads-end_time'], end_of_day($end_time) ));
                if(empty($cost)) continue;

                $account_currency_code = get_term_meta_value($contract['term_id'],'account_currency_code');
                
                $meta_key = strtolower("exchange_rate_{$account_currency_code}_to_vnd");
                $exchange_rate = (float) (get_term_meta_value($contract['term_id'], $meta_key) ?: get_exchange_rate());

                $budget         = $this->googleads_m->set_contract($contract['term_id'])->get_behaviour_m()->calc_budget();
                $service_fee    = (int) get_term_meta_value($contract['term_id'], 'service_fee');
                $service_rate   = div($service_fee, $budget);

                $saleId     = (int) get_term_meta_value($contract['term_id'], 'staff_business');
                $sale       = $this->admin_m->get_field_by_id($saleId);
                $department = $this->term_users_m->get_user_terms($saleId, 'department');
                $department AND $department = reset($department);

                $cost = ($account_currency_code == 'USD' || $account_currency_code == 'AUD') ? $cost*$exchange_rate : $cost;

                $payment_type = 'normal';
                if('customer' == get_term_meta_value($contract['term_id'], 'contract_budget_payment_type'))
                {
                    $payment_type = 'customer';
                    'behalf' == get_term_meta_value($contract['term_id'], 'contract_budget_customer_payment_type') AND $payment_type = 'behalf';
                }

                $item = array(
                    'contract_code'     => get_term_meta_value($contract['term_id'], 'contract_code'),
                    'customer'          => $customers[$contract['term_id']]['display_name'] ?? '',
                    'Service'           => 'ADSPLUS',
                    'payment_type'      => $payment_type_enums[$payment_type] ?? '',
                    'cid'               => $mcc,
                    'contract_value'    => (int) get_term_meta_value($contract['term_id'], 'contract_value'),
                    'department'        => $department->term_name ?? null,
                    'sale_name'         => $sale['display_name'],
                    'sale_email'        => $sale['user_email'],
                    'currency'          => $account_currency_code,
                    'exchange'          => number_format($exchange_rate, 0, ',', ''),
                    'cost'              => (int) number_format($cost, 2, ',', ''),
                    'service_fee'       => (int) number_format($cost*$service_rate, 2, ',', ''),
                    'service_fee_rate'  => currency_numberformat($service_rate*100, '%'),
                );

                $data[] = $item;
            }
        }

        $title          = 'Export dữ liệu Hợp Đồng & Khách hàng thực hiện từ ngày ' . $start_date->format('d/m/Y') . ' đến ngày ' .$end_date->format('d/m/Y');

        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));
        
        $sheet = $spreadsheet->getActiveSheet();

        $columns = array(
            'contract_code'     => [ 'type' => 'string', 'label' => 'Mã hợp đồng' ],
            'customer'          => [ 'type' => 'string', 'label' => 'Khách hàng' ],
            'Service'           => [ 'type' => 'string', 'label' => 'Dịch vụ' ],
            'payment_type'      => [ 'type' => 'string', 'label' => 'Loại' ],
            'cid'               => [ 'type' => 'string', 'label' => 'Customer ID' ],
            'contract_value'    => [ 'type' => 'number', 'label' => 'Giá tị hợp đồng' ],
            'department'        => [ 'type' => 'string', 'label' => 'Phòng' ],
            'sale_name'         => [ 'type' => 'string', 'label' => 'Tên nhân viên' ],
            'sale_email'        => [ 'type' => 'string', 'label' => 'Email nhân viên' ],
            'currency'          => [ 'type' => 'string', 'label' => 'Currecy' ],
            'exchange'          => [ 'type' => 'number', 'label' => '1 USD = ? VND' ],
            'cost'              => [ 'type' => 'number', 'label' => 'Ngân sách đã sử dụng' ],
            'service_fee'       => [ 'type' => 'number', 'label' => 'Phí quản lý' ],
            'service_fee_rate'  => [ 'type' => 'number', 'label' => '% Phí quản lý' ],
        );

        $sheet->fromArray(array_column(array_values($columns), 'label'), NULL, 'A1');

        $rowIndex = 2;

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column)
            {
                switch ($column['type'])
                {
                    case 'number': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 

                        if(empty($value)) break;

                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        
                        break;

                    default: break;
                }    

                $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item[$key]);
                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $this->load->helper('text');
        $title = url_title(convert_accented_characters($title));
        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file MCCReport.php */
/* Location: ./application/modules/googleads/controllers/api_v2/MCCReport.php */