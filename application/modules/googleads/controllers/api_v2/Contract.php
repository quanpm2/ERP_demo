<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use AdsService\AdsInsight\GoogleadsInsight;

class Contract extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'googleads/mcm_account_m';
        $this->autoload['models'][] = 'googleads/adwords_report_m';
        $this->autoload['models'][] = 'log_m';

        parent::__construct($config);
    }


    /**
	 * @param int $id Id hợp đồng FB
	 * 
	 * @return [type]
	 */
	public function overview_get(int $id)
	{
		$data = $this->data;

		if( false == $this->googleads_m->set_contract($id)) return parent::responseHandler([], 'Hợp đồng không tồn tại hoặc đã bị xóa.', 'error', 404);
		if( ! $this->googleads_m->can('googleads.overview.access')) return parent::responseHandler([], 'Không có quyền truy cập.', 'error', 401);

		$contract = $this->googleads_m->get_contract();
		$segments = $this->googleads_m->getSegments();

		$saleId = (int) get_term_meta_value($id, 'staff_business');
		$sale 	= $this->admin_m->get_field_by_id($saleId);


		$kpis = $this->googleads_kpi_m->order_by('kpi_type')->get_many_by([ 'term_id' => $id]);
		$kpis AND $kpis = array_map(function($kpi) {
			$kpi->display_name = $this->admin_m->get_field_by_id($kpi->user_id, 'display_name');
			return $kpi;
		}, $kpis);

		$data = array(
			'term_id'				=> $id,
			'term'					=> $contract,
			'kpis'					=> $kpis,
			'sale'					=> $sale,
            'adaccount_status'		=> get_term_meta_value($id, 'adaccount_status'),
			'contract_code'			=> get_term_meta_value($id, 'contract_code'),
			'representative_name'	=> get_term_meta_value($id, 'representative_name'),
			'representative_email'		=> get_term_meta_value($id, 'representative_email'),
			'representative_address'	=> get_term_meta_value($id, 'representative_address'),
		);

        $exchange_rates = [];
        $this->load->model('option_m');
        $exchange_rate_option = $this->option_m->get_value('exchange_rate', TRUE);
        foreach($exchange_rate_option as $currency => $exchange_rate){
            $meta_key = 'exchange_rate_' . strtolower($currency);

            $meta_value = (double) get_term_meta_value($id, $meta_key) ?? 0;
            if(!empty($meta_value)) $exchange_rates[$meta_key] = $meta_value;
        }
        $data['exchange_rates'] = $exchange_rates;

		$start_service_time 	= (int) get_term_meta_value($id,'start_service_time');
		$end_service_time 		= (int) get_term_meta_value($id,'end_service_time');
		$advertise_start_time	= (int) get_term_meta_value($id,'advertise_start_time');
		$advertise_end_time		= (int) get_term_meta_value($id,'advertise_end_time');
		$vat 					= div((double) get_term_meta_value($id,'vat'),100);
		$contract_budget 		= (double) get_term_meta_value($id,'contract_budget');
		$service_fee 			= (double) get_term_meta_value($id,'service_fee');
		$payment_amount 		= (double) get_term_meta_value($id,'payment_amount');
        $result_updated_on      = get_term_meta_value($id, 'result_updated_on');
		$time_next_api_check  = get_term_meta_value($id, 'time_next_api_check');

		if(!empty($vat)) $service_fee+= $service_fee*$vat;
		
		$actual_budget 				= (int) get_term_meta_value($id, 'actual_budget');
		$balanceBudgetReceived 		= (int) get_term_meta_value($id, 'balanceBudgetReceived');
        $balanceBudgetAddTo         = (int) get_term_meta_value($id, 'balanceBudgetAddTo');

		$payment_percentage 		= 100 * (double) get_term_meta_value($id,'payment_percentage');

		$balance_spend 				= (double) get_term_meta_value($id, 'balance_spend');

		$data['actual_budget']				= $actual_budget;
		$data['total_actual_budget']		= $actual_budget;
		$data['balanceBudgetReceived']		= $balanceBudgetReceived;
        $data['balanceBudgetAddTo']		    = $balanceBudgetAddTo;
		$data['balance_spend'] 				= $balance_spend;
		$data['contract_budget']			= $contract_budget;
		$data['payment_amount']				= $payment_amount;
		$data['payment_percentage']			= $payment_percentage;
		$data['start_service_time']			= $start_service_time;
		$data['end_service_time']			= $end_service_time;
		$data['advertise_start_time']		= $advertise_start_time;
		$data['advertise_end_time']			= $advertise_end_time;
        $data['result_updated_on']          = $result_updated_on ? date('H:i:s d/m/Y', $result_updated_on) : '--';
        $data['time_next_api_check']      	= $time_next_api_check ? date('H:i:s d/m/Y', $time_next_api_check) : '--';

		$start_service_time = start_of_day($start_service_time);
		$end_service_time 	= end_of_day(($end_service_time ?: time()));

		$insight 		= array();
		$adsSegments 	= $this->term_posts_m->get_term_posts($id, $this->ads_segment_m->post_type);
		if( ! empty($adsSegments))
		{
			foreach ($adsSegments as &$adsSegment)
			{
				$adaccount = $this->term_posts_m->get_post_terms($adsSegment->post_id, $this->mcm_account_m->term_type);
				if(empty($adaccount)) continue;
				
				$adaccount 	= reset($adaccount);

				$_iSegments = $this->mcm_account_m->get_data($adaccount->term_id, $adsSegment->start_date, end_of_day( (int) $adsSegment->end_date));
                $_iSegments = array_map(function($item){
                    $item['day'] = date('d-m-Y', $item['start_date']);

                    return $item;
                }, $_iSegments);
                $insight 	= array_merge($_iSegments, $insight);
				
				$adsSegment->adaccount 		= $adaccount;
				$adsSegment->adaccount_id 	= $adaccount->term_id;
				$adsSegment->insights 		= $_iSegments;
			}
		}

		if($insight)
		{
            $insight = array_group_by($insight, 'start_date');
			$insight = array_map(function($x) use ($id){
                $_instance = reset($x);

				return [
					'result'		=> array_sum(array_column($x, 'result')),
					'cost'			=> array_sum(array_column($x, 'cost')),
					'spend'			=> array_sum(array_column($x, 'spend')),
                    'clicks'        => array_sum(array_column($x, 'clicks')),
                    'impressions'   => array_sum(array_column($x, 'impressions')),
					'date_start' 	=> $_instance['day']
				];
			}, $insight);
		}

		ksort($insight, SORT_NUMERIC);

		$data['chart_data'] = [
			'axis_categories' 	=> array_column($insight, 'date_start'),
			'result' 			=> array_column($insight, 'result'),
			'spend' 			=> array_column($insight, 'spend'),
            'impressions'       => array_column($insight, 'impressions'),
			'cost_per_result' 	=> div(array_sum(array_column($insight, 'spend')), array_sum(array_column($insight, 'result')))
		];

		$data['result']                  = array_sum(array_column($insight, 'result'));
		$data['clicks']             = array_sum(array_column($insight, 'clicks'));
		$data['impressions']             = array_sum(array_column($insight, 'impressions'));
		
		$actual_result                      = (double) get_term_meta_value($id, 'actual_result') ;
		$data['amount_spend']               = $actual_result + $data['balance_spend'];
		$data['amount_spend_percentage']	= div($data['amount_spend'], $actual_budget) * 100;
		

		$data['adAccountsInfomation'] = array();
		if( ! empty($adsSegments))
		{
			$rows = [];
			foreach ($adsSegments as $i => $segment)
			{
				if(empty($segment->insights)) continue;

                $adaccount = $segment->adaccount;

                $spend = array_sum(array_column($segment->insights, 'spend'));
				$row = [
					'id' => $adaccount->term_name ?? null,
					'name' => get_term_meta_value($adaccount->term_id, 'account_name') ?? null,
					'start' => my_date($segment->start_date, 'd/m/Y'),
					'end' => empty($segment->end_date) ? 'Hiện tại' : my_date($segment->end_date, 'd/m/Y'),
					'cost' => (int)$spend
				];

				$data['adAccountsInfomation'][] = $row;
			}
		}

		parent::responseHandler($data, 'ok');
	}

    /**
	 * /PUT API-V2/FACEBOOKADS/CONTRACT/ADACCOUNT_INSIGHT/:ID
	 *
	 * @param      int    $term_id  The term identifier
	 *
	 * @return     Json  Result
	 */
	public function adaccount_insight_put_old(int $term_id = 0)
	{
		$contract 	= (new googleads_m())->set_contract($term_id);
		if( ! $contract)
		{
            return parent::responseHandler([], 'Không tìm thấy dữ liệu của hợp đồng', 'success', 400);
		}

		try
		{
			$contract->update_insights();
			$behaviour_m 	= $contract->get_behaviour_m();
			$progress 		= $behaviour_m->get_the_progress();
			$behaviour_m->sync_all_amount();

			update_term_meta($term_id, 'time_next_api_check', strtotime('+30 minutes'));
		}
		catch (Exception $e)
		{
			$result = $e->getMessage();
			log_message('debug', $e->getMessage());
            return parent::responseHandler([], $result, 'error', 400);
		}

        $description = 'Đã đồng bộ lúc ' . date('h:i:s d/m/Y') . '. Kỳ đồng bộ tiếp theo lúc ' . date('h:i:s d/m/Y', get_term_meta_value($term_id, 'time_next_api_check'));
        audit('sync_spend', 'term', $term_id, '', $description, '', $this->admin_m->id, 'erp');

		return parent::responseHandler([], 'Dữ liệu đã được đồng bộ thành công. Vui lòng (F5) để xem sự thay đổi', 'success', 200);
	}

    public function adaccount_insight_put(int $term_id = 0)
	{
		$contract 	= (new googleads_m())->set_contract($term_id);
		if( ! $contract)
		{
            return parent::responseHandler([], 'Không tìm thấy dữ liệu của hợp đồng', 'success', 400);
		}

        $status = (new GoogleadsInsight())->syncInsight($term_id);
        if(!$status){
            return parent::responseHandler([], 'Tiến trình đồng bộ gặp sự cố. Vui lòng liên hệ quản trị viên để được hỗ trợ', 'failed', 400);
        }

		return parent::responseHandler([], 'Dữ liệu đã được đồng bộ thành công. Vui lòng chờ trong giây lát.', 'success', 200);
	}

	/**
	 * /PUT API-V2/GOOGLEADS/CONTRACT/SYNC_METRICS/:ID
	 *
	 * @param      int    $term_id  The term identifier
	 *
	 * @return     Json  Result
	 */
	public function sync_metrics_put(int $term_id = 0)
	{
		$contract 	= (new googleads_m())->set_contract($term_id);
		if( ! $contract) return parent::responseHandler([], 'Không tìm thấy dữ liệu của hợp đồng', 'success', 400);

		try
		{
			$behaviour_m = $contract->get_behaviour_m();
			$behaviour_m->sync_all_amount();
		}
		catch (Exception $e)
		{
			$result = $e->getMessage();
			log_message('debug', $e->getMessage());
            return parent::responseHandler([], $result, 'error', 400);
		}

        $description = 'Đã đồng bộ lúc ' . date('h:i:s d/m/Y') . '. Kỳ đồng bộ tiếp theo lúc ' . date('h:i:s d/m/Y', get_term_meta_value($term_id, 'time_next_api_check'));
        audit('sync_spend', 'term', $term_id, '', $description, '', $this->admin_m->id, 'erp');

		return parent::responseHandler(null, 200, 'success', 200);
	}
}
/* End of file MCCReport.php */
/* Location: ./application/modules/googleads/controllers/api_v2/MCCReport.php */