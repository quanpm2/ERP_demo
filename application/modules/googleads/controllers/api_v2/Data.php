<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'googleads/mcm_account_m';
        parent::__construct($config);
    }

    public function accounts_get($contractId = null)
    {
        $contract   = (new googleads_m())->set_contract($contractId);
        $data       = $contract->get_accounts();
        parent::response([ 'status' => true, 'data' => $data ]);
    }

    public function account_cost_get($contractId = null)
    {
        // dd(parent::get(), my_date(parent::get('start_time')), my_date(parent::get('end_time')));
        $contract   = (new googleads_m())->set_contract($contractId);
        $accounts = $contract->get_accounts(parent::get(NULL, TRUE));

        if(empty($accounts)) parent::response(['status' => true, 'data' => 0]);

        $ins    = reset($accounts);
        $cost   = div(array_sum(array_column($accounts, 'cost')), 1000000);

        $exchange_rate = get_exchange_rate($ins['account_currency'], $contractId);
        ('VND' != $ins['account_currency']) AND $cost *= $exchange_rate;

        parent::response([ 'status' => true, 'data' => $cost ]);
    }

    /**
     * GET All Contracts Realted by Segments
     *
     * @param      int   $id     The identifier
     */
    public function contracts_related_get(int $id)
    {
        if(FALSE == $this->googleads_m->set_contract($id)) parent::response(['code' =>  parent::HTTP_BAD_REQUEST, 'error' => parent::HTTP_BAD_REQUEST ]);

        $contract = $this->googleads_m->get_contract();

        $segments       = $this->googleads_m->getSegments();
        $adaccountIds   = array_column($segments, 'adaccount_id');

        $allSegments = $this->ads_segment_m->set_post_type()
        ->select('posts.post_id, start_date, end_date, term_posts.term_id as adaccount_id')
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->where_in('term_posts.term_id', array_unique($adaccountIds))
        ->group_by('posts.post_id')
        ->get_all();

        if(empty($allSegments)) parent::response(['code' => parent::HTTP_OK, 'total' => 0, 'data' => []]);

        $relatedContracts = $this->googleads_m->set_term_type()
        ->select('term.term_id, term_name, term_type, term_parent, term_status, object_id, post_id as segment_id')
        ->join($this->term_posts_m->_table, "{$this->term_posts_m->_table}.term_id = term.term_id")
        ->where_in("{$this->term_posts_m->_table}.post_id", array_map('intval', array_unique(array_filter(array_column($allSegments, 'post_id')))))
        ->where('term.term_id <>', $id)
        ->get_all();

        if(empty($relatedContracts)) parent::response(['code' => parent::HTTP_OK, 'total' => 0, 'data' => []]);

        $relatedContracts AND $relatedContracts = array_group_by($relatedContracts, 'term_id');
        $allSegments AND $allSegments = array_column($allSegments, null, 'post_id');

        $contracts = array_map(function($items) use($allSegments){

            $_contract = reset($items);

            $this->load->model('staffs/admin_m');
            $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
            if(!empty($manipulation_locked['manipulation_locked_by']))
            {
                $manipulation_locked['manipulation_locked_by_name'] = $this->admin_m->get_field_by_id($manipulation_locked['manipulation_locked_by'], 'display_name');
            }

            if(!empty($manipulation_locked['manipulation_unlocked_by']))
            {
                $manipulation_locked['manipulation_unlocked_by_name'] = $this->admin_m->get_field_by_id($manipulation_locked['manipulation_unlocked_by'], 'display_name');
            }
            $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                    && TRUE == (bool)get_term_meta_value($_contract->term_id, 'is_manipulation_locked');
            $manipulation_locked['is_manipulation_locked'] = $is_manipulation_locked;

            return array(
                'term_id' => (int) $_contract->term_id,
                'term_name' => $_contract->term_name,
                'contract_code' => get_term_meta_value($_contract->term_id, 'contract_code') ?: $_contract->term_id,
                'term_type' => $_contract->term_type,
                'term_parent' => (int) $_contract->term_parent,
                'term_status' => $_contract->term_status,
                'advertise_start_time' => (int) get_term_meta_value($_contract->term_id, 'advertise_start_time'),
                'advertise_end_time' => (int) get_term_meta_value($_contract->term_id, 'advertise_end_time'),
                'segments' => array_map(function($item) use($allSegments){
                    return array_map('intval', (array) $allSegments[$item->segment_id]);
                }, $items),
                'manipulation_locked' => $manipulation_locked,
            );
        }, $relatedContracts);

        parent::response([
            'code' => parent::HTTP_OK,
            'total' => count($contracts),
            'data' => array_values($contracts)
        ]);
    }

    public function segments_get(int $id)
    {
        $segment = $this->ads_segment_m
        ->select('post_id, start_date, end_date, post_status, post_type')
        ->set_post_type()
        ->get($id);

        if( ! $segment)
        {
            parent::response(['code' => parent::HTTP_BAD_REQUEST, 'error' => parent::HTTP_BAD_REQUEST]);
        }

        $this->load->model('googleads/mcm_account_m');
        $this->load->model('googleads/base_adwords_m');
        
        $adaccount = $this->term_posts_m->get_post_terms($segment->post_id, $this->mcm_account_m->term_type);
        $adaccount AND $adaccount = reset($adaccount);


        $this->base_adwords_m->set_mcm_account($adaccount->term_id);
        $account_data = $this->base_adwords_m->get_cache('ACCOUNT_PERFORMANCE_REPORT', $segment->start_date, $segment->end_date ?: time());
        $account_data AND $account_data = array_filter($account_data);
        $account_data AND $account_data = array_merge(...$account_data);

        parent::response([ 'code' => 200, 'data' => array(
            'post_id'       => (int) $segment->post_id,
            'start_date'    => (int) $segment->start_date,
            'end_date'      => (int) $segment->end_date,
            'adaccount_id'  => (int) $adaccount->term_id,
            'adaccount' => array(
                'id'    => (int) $adaccount->term_id,
                'account_name'  => get_term_meta_value($adaccount->term_id, 'account_name'),
                'customer_id'   => get_term_meta_value($adaccount->term_id, 'customer_id'),
                'currency_code' => get_term_meta_value($adaccount->term_id, 'currency_code'),
            ),
            'insight' => $account_data
        )]);
    }

    public function adaccount_get(int $id)
    {
        $segment = $this->ads_segment_m
        ->select('post_id, start_date, end_date, post_status, post_type')
        ->set_post_type()
        ->get($id);

        if( ! $segment)
        {
            parent::response(['code' => parent::HTTP_BAD_REQUEST, 'error' => parent::HTTP_BAD_REQUEST]);
        }

        $this->load->model('googleads/mcm_account_m');
        $this->load->model('googleads/base_adwords_m');
        
        $adaccount = $this->term_posts_m->get_post_terms($segment->post_id, $this->mcm_account_m->term_type);
        $adaccount AND $adaccount = reset($adaccount);


        $this->base_adwords_m->set_mcm_account($adaccount->term_id);
        $account_data = $this->base_adwords_m->get_cache('ACCOUNT_PERFORMANCE_REPORT', $segment->start_date, $segment->end_date ?: time());
        $account_data AND $account_data = array_filter($account_data);
        $account_data AND $account_data = array_merge(...$account_data);

        parent::response([ 'code' => 200, 'data' => array(
            'post_id'       => (int) $segment->post_id,
            'start_date'    => (int) $segment->start_date,
            'end_date'      => (int) $segment->end_date,
            'adaccount_id'  => (int) $adaccount->term_id,
            'adaccount' => array(
                'id'    => (int) $adaccount->term_id,
                'account_name'  => get_term_meta_value($adaccount->term_id, 'account_name'),
                'customer_id'   => get_term_meta_value($adaccount->term_id, 'customer_id'),
                'currency_code' => get_term_meta_value($adaccount->term_id, 'currency_code'),
            ),
            'insight' => $account_data
        )]);
    }

    
    /**
     * 
     * Compute total cost of Adaccount with daterange
     * 
     * @param int $adaccount_id
     * 
     * @return [type]
     */
    public function adaccount_cost_get( int $adaccount_id)
    {
        $args = wp_parse_args( array_filter(parent::get(NULL, TRUE)), [
            'end_time' => time(),
            'ignore_cache' => false
        ]);
        
        $this->load->model('googleads/mcm_account_m');
        $account_data = $this->mcm_account_m->get_data($adaccount_id, $args['start_time'], $args['end_time']);
        $account_data = array_map(function($x){
            return $x['spend'];
        }, $account_data);

        parent::response([
            'code' => 200,
            'data' => array_sum($account_data)
        ]);
    }
}
/* End of file MCCReport.php */
/* Location: ./application/modules/googleads/controllers/api_v2/MCCReport.php */