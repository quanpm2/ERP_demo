<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class DatasetUnconfigured extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'googleads/googleads_kpi_m';
        $this->autoload['models'][] = 'ads_segment_m';
        parent::__construct($config);

        $this->load->config('googleads/googleads');
        $this->load->config('googleads/contract');
    }


    /**
     * Render dataset of FB's services
     */
    public function index_get()
    {
        $response   = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
        $permission = 'googleads.index.access';

        if( ! has_permission($permission))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        $relate_users       = $this->admin_m->get_all_by_permissions($permission);
        if($relate_users === FALSE)
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        $this->load->library('datatable_builder');

        if(is_array($relate_users))
        {
            $this->datatable_builder->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
        }

        $default = array(
            'offset'        => 0,
            'per_page'      => 10,
            'cur_page'      => 1,
            'is_filtering'  => TRUE,
            'is_ordering'   => TRUE,
            'filter_position'   => FILTER_TOP_OUTTER,
        );

        $args               = wp_parse_args( $this->input->get(), $default);

        /* Applies get query params for filter */
        $this->search_filter();

        $columns = $this->getColumns();
        foreach($columns AS $key => $value) $this->datatable_builder->add_column($key, $value);

        $callbacks = $this->getCallbacks(array_keys($columns));
        foreach($callbacks AS $callback) $this->datatable_builder->add_column_callback($callback['field'], $callback['func'], $callback['row_data']);

        $data =  $this->datatable_builder
        ->setOutputFormat('JSON')

        ->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])
        ->add_search('customer_code', ['placeholder' => 'Mã KH'])
        ->add_search('contract_begin', ['placeholder' => 'Ngày bắt đầu','class' => 'form-control input_daterange'])
        ->add_search('contract_end', ['placeholder' => 'Ngày kết thúc','class' => 'form-control input_daterange'])
        ->add_search('actual_budget', ['placeholder' => 'Ngân sách đã thu'])
        ->add_search('staff_business', ['placeholder' => 'NVKD'])
        ->add_search('gg_staff_advertise', ['placeholder' => 'Kỹ thuật'])
        ->add_search('status', ['content' => form_dropdown(['name'=>'where[status]','class'=>'form-control select2'], prepare_dropdown([TRUE => 'Đã kích hoạt', FALSE => 'Chưa kích hoạt'],'Trạng thái : All'),$this->input->get('where[status]'))])

        ->select('term.term_id, term.term_status, term.term_name, term.term_type')
        ->select('MAX(IF(m_term.meta_key = "started_service", m_term.meta_value, NULL)) AS started_service')
        ->select('MAX(IF(m_term.meta_key = "adaccount_status", m_term.meta_value, NULL)) AS adaccount_status')
        ->from('term')
        ->join('termmeta AS m_term', 'm_term.term_id = term.term_id AND m_term.meta_key IN ("started_service", "adaccount_status")', 'LEFT')
        ->where('term.term_type', $this->googleads_m->term_type)
        ->where_in('term.term_status', array_keys($this->config->item('status','googleads')))
        ->group_by('term.term_id')
        ->having('adaccount_status = "UNSPECIFIED" OR adaccount_status = "REMOVED"')
        ->generate(array('per_page'=>$args['per_page'],'cur_page'=>$args['cur_page']));

        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->export($last_query);
            return TRUE;
        }
        
        parent::response($data);
    }

    protected function search_filter($search_args = array())
    {   
        $args = $this->input->get();

        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

        // Contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if($filter_contract_code || $sort_contract_code)
        {
            $alias = uniqid('contract_code_');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

            if($filter_contract_code)
            {   
                $this->datatable_builder->like("{$alias}.meta_value",$filter_contract_code);
            }

            if($sort_contract_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_code);
            }

            unset($args['where']['contract_code'],$args['order_by']['contract_code']);
        }

        // customer_code FILTERING & SORTING
        $filter_customer_code = $args['where']['customer_code'] ?? FALSE;
        $sort_customer_code   = $args['order_by']['customer_code'] ?? FALSE;
        if($filter_customer_code || $sort_customer_code)
        {
            $alias = uniqid('customer_code_');
            $this->datatable_builder
            ->join("term_users tu_customer","tu_customer.term_id = term.term_id")
            ->join("user customer","customer.user_id = tu_customer.user_id AND customer.user_type IN ('customer_person', 'customer_company')")
            ->join("usermeta {$alias}","{$alias}.user_id = customer.user_id and {$alias}.meta_key = 'cid'", 'LEFT');

            if($filter_customer_code)
            {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_customer_code);
                unset($args['where']['customer_code']);
            }

            if($sort_customer_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_customer_code);
                unset($args['order_by']['customer_code']);
            }
        }

        // Contract_begin FILTERING & SORTING
        $filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
        $sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
        if($filter_contract_begin || $sort_contract_begin)
        {
            $alias = uniqid('contract_begin_');
			$this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

			if($filter_contract_begin)
			{	
				$dates = explode(' - ', $filter_contract_begin);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['contract_begin']);
			}

			if($sort_contract_begin)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value", $sort_contract_begin);
				unset($args['order_by']['contract_begin']);
			}
        }

        // Contract_end FILTERING & SORTING
        $filter_contract_end = $args['where']['contract_end'] ?? FALSE;
        $sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
        if($filter_contract_end || $sort_contract_end)
        {
            $alias = uniqid('contract_end_');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_end'", 'LEFT');

			if($filter_contract_end)
			{	
				$dates = explode(' - ', $filter_contract_end);
				$this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)));
				$this->datatable_builder->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));

				unset($args['where']['contract_end']);
			}

			if($sort_contract_end)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_end);
				unset($args['order_by']['contract_end']);
			}
        }

        // Actual_budget FILTERING & SORTING
        $filter_actual_budget = $args['where']['actual_budget'] ?? FALSE;
        $sort_actual_budget = $args['order_by']['actual_budget'] ?? FALSE;
        if($filter_actual_budget || $sort_actual_budget)
        {
            $alias = uniqid('actual_budget');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_budget'", 'LEFT');

            if($filter_actual_budget)
			{	
				$this->datatable_builder->where("CAST({$alias}.meta_value AS UNSIGNED) >=", $filter_actual_budget);
				unset($args['where']['actual_budget']);
			}

			if($sort_actual_budget)
			{
				$this->datatable_builder->order_by("CAST({$alias}.meta_value AS UNSIGNED)", $sort_actual_budget);
				unset($args['order_by']['actual_budget']);
			}
        }

        // Staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if($filter_staff_business || $sort_staff_business)
        {
            $alias = uniqid('staff_business_');
            $this->datatable_builder
            ->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT')
            ->join("user tblcustomer","{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

            if($filter_staff_business)
            {   
                $this->datatable_builder->like("tblcustomer.display_name",$filter_staff_business);
                unset($args['where']['staff_business']);
            }

            if($sort_staff_business)
            {
                $this->datatable_builder->order_by('tblcustomer.display_name',$sort_staff_business);
                unset($args['order_by']['staff_business']);
            }
        }

        // FIND AND SORT STAFF_ADVERTISE WITH Google_KPI
        $filter_gg_staff_advertise = $args['where']['gg_staff_advertise'] ?? FALSE;
        $sort_gg_staff_advertise = $args['order_by']['fb_gg_staff_advertise'] ?? FALSE;
        if($filter_gg_staff_advertise || $sort_gg_staff_advertise)
        {
            $this->datatable_builder->join('googleads_kpi','term.term_id = googleads_kpi.term_id', 'LEFT OUTER');
            $this->datatable_builder->join('user','user.user_id = googleads_kpi.user_id', 'LEFT OUTER');

            if($filter_gg_staff_advertise)
            {   
                $this->datatable_builder->like('user.user_email',strtolower($filter_gg_staff_advertise),'both');
            }

            if($sort_gg_staff_advertise)
            {
                $this->datatable_builder->order_by('user.user_email',$sort_gg_staff_advertise);
            }

            unset($args['where']['gg_staff_advertise'],$args['order_by']['gg_staff_advertise']);
        }

        // status FILTERING & SORTING
        $filter_status = $args['where']['status'] ?? FALSE;
        $sort_status = $args['order_by']['status'] ?? FALSE;
        if(is_numeric($filter_status) || $sort_status)
        {
            $alias = uniqid('status_');
            $this->datatable_builder
            ->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'start_service_time'", 'LEFT');

            if($filter_status){
                $this->datatable_builder->where("{$alias}.meta_value > 0");
            }
            else{
                $this->datatable_builder->where("({$alias}.meta_value <= 0 OR {$alias}.meta_value IS NULL)");
            }

            if($sort_status)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_status);
            }

            unset($args['where']['status'],$args['order_by']['status']);
        }
    }

    protected function getColumns($type = 'default')
    {
        $enums = [
            'contract_code' => ['type' => 'string', 'title' => 'Mã hợp đồng', 'set_order' => TRUE, 'set_select' => FALSE],
            'customer_code' => ['type' => 'string', 'title' => 'Mã KH', 'set_order' => TRUE, 'set_select' => FALSE],
            'contract_begin' => ['type' => 'timestamp', 'title' => 'Ngày bắt đầu', 'set_order' => TRUE, 'set_select' => FALSE],
            'contract_end' => ['type' => 'timestamp', 'title' => 'Ngày kết thúc', 'set_order' => TRUE, 'set_select' => FALSE],
            'actual_budget' => ['type' => 'number', 'title' => 'Ngân sách đã thu', 'set_order' => TRUE, 'set_select' => FALSE],
            'staff_business' => ['type' => 'number', 'title' => 'NVKD', 'set_order' => TRUE, 'set_select' => FALSE],
            'gg_staff_advertise' => ['type' => 'string', 'title' => 'Kỹ thuật', 'set_order' => TRUE, 'set_select' => FALSE],
            'status' => ['type' => 'string', 'title' => 'Trạng thái hợp đồng', 'set_order' => TRUE, 'set_select' => FALSE],
        ];

        $config = [
            'default' => [
                'contract_code',
                'customer_code',
                'contract_begin',
                'contract_end',
                'actual_budget',
                'staff_business',
                'gg_staff_advertise',
                'status'
            ],

            'export' => [
                'contract_code',
                'customer_code',
                'contract_begin',
                'contract_end',
                'actual_budget',
                'staff_business',
                'gg_staff_advertise',
                'status'
            ],
        ];

        return elements($config[$type], $enums);
    }

    protected function getCallbacks($args = [])
    {
        $callbacks = [];

        $callbacks['contract_code'] = [
            'field' => 'contract_code',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['contract_code'] = get_term_meta_value($term_id, 'contract_code') ?: '--';

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['customer_code'] = [
            'field' => 'customer_code',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['customer_code'] = '--';

                $customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company']);
                if(empty($customers)) return $data;

                $customer = end($customers);
                
                $data['customer_id'] = $customer->user_id;
                $data['customer_code'] = cid($customer->user_id, $customer->user_type);

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['contract_begin'] = [
            'field' => 'contract_begin',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['contract_begin'] = '--';

                $contract_begin = get_term_meta_value($term_id, 'contract_begin');
                if(empty($contract_begin)) return $data;

                $data['contract_begin_raw'] = $contract_begin;
                $data['contract_begin'] = my_date($contract_begin, 'd/m/Y');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['contract_end'] = [
            'field' => 'contract_end',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['contract_end'] = '--';

                $contract_end = get_term_meta_value($term_id, 'contract_end');
                if(empty($contract_end)) return $data;

                $data['contract_end_raw'] = $contract_end;
                $data['contract_end'] = my_date($contract_end, 'd/m/Y');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['actual_budget'] = [
            'field' => 'actual_budget',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $actual_budget              = get_term_meta_value($term_id ,'actual_budget');
                $data['actual_budget_raw']  = (int) $actual_budget;
                $data['actual_budget']      = (int) $actual_budget;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['staff_business'] = [
            'field' => 'staff_business',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['staff_business'] = [];
                
                $staff_business 		= get_term_meta_value($term_id, 'staff_business');
                if(empty($staff_business)) return $data;
                
                $staff_business = $this->admin_m->get_field_by_id($staff_business);
                
                $staff_business = [
                    'user_id' => $staff_business['user_id'],
                    'display_name' => $staff_business['display_name'],
                    'user_avatar' => $staff_business['user_avatar'],
                    'user_email' => $staff_business['user_email'],
                ];
            
                $data['staff_business'] = $staff_business;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $this->load->model('googleads/googleads_kpi_m');
        $callbacks['gg_staff_advertise'] = [
            'field' => 'gg_staff_advertise',    
            'func'  => function($data, $row_name){
                $term_id 		= $data['term_id'];

                $data['gg_staff_advertise'] = [];

			    $kpis = $this->googleads_kpi_m->get_kpis($term_id, 'users', 'tech');
                $kpis = reset($kpis);
			    if(empty($kpis)) return $data;
                
			    $user_ids = array();
			    foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;

			    $data['gg_staff_advertise'] = array_reduce($user_ids, function($result, $user_id){
                    $_user = $this->admin_m
                    ->select('user_id, user_email, display_name, user_avatar, user.role_id, role_name')
                    ->join('role', 'role.role_id = user.role_id')
                    ->as_array()
                    ->get($user_id);
                    if(empty($_user)) return $result;

                    $user = [
                        'user_id' => $_user['user_id'],
                        'user_email' => $_user['user_email'],
                        'display_name' => $_user['display_name'],
                        'user_avatar' => $_user['user_avatar'],
                        'role_name' => $_user['role_name'] ?? NULL,
                    ];
                    $result[] = $user;
			    	
                    return $result;
			    }, []);

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['status'] = [
            'field' => 'status',
            'func'  => function($data, $row_name) {
                $term_id = $data['term_id'];

                $is_service_proc = is_service_proc($term_id);
                $data['status'] = $is_service_proc ? 'Đã kích hoạt' : 'Chưa kích hoạt';

                return $data;
            },
            'row_data' => FALSE,
        ];

        $selected_callbacks = [];
        if(empty($args)) $selected_callbacks = $callbacks;

        foreach($args AS $callback_name){
            if(!is_string($callback_name)) continue;
            if(!isset($callbacks[$callback_name])) continue;

            $selected_callbacks[$callback_name] = $callbacks[$callback_name];
        }

        return $selected_callbacks;
    }


    public function export($query = '')
    {
        if(empty($query)) return FALSE;

        // remove limit in query string
        $query = explode('LIMIT', $query);
        $query = reset($query);
        
        $terms = $this->contract_m->query($query)->result();

        if( ! $terms) return FALSE;

        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("Báo cáo hợp đồng Google chưa cấu hình"));
        $sheet = $spreadsheet->getActiveSheet();
        
        $columns = $this->getColumns('export');
        $sheet->fromArray(array_column(array_values($columns), 'title'), NULL, 'A1');

        $rowIndex = 2;

        $this->load->model('googleads/googleads_kpi_m');

        foreach ($terms as $item)
        {
            $i = 1;
            $colIndex = $i;

            $term_id = $item->term_id;

            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, get_term_meta_value($term_id, 'contract_code') ?: '--');
            $colIndex++;

            $customer_code = '--';
            $customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company']);
            if(!empty($customers)){
                $customer = end($customers);
                $customer_code = cid($customer->user_id, $customer->user_type);
            }
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $customer_code);
            $colIndex++;

            $contract_begin_date = '--';
            $contract_begin = get_term_meta_value($term_id, 'contract_begin');
            $contract_begin AND $contract_begin_date = my_date($contract_begin, 'd/m/Y');
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $contract_begin_date)
            ->getStyleByColumnAndRow($colIndex, $rowIndex)
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);;
            $colIndex++;

            $contract_end_date = '--';
            $contract_end = get_term_meta_value($term_id, 'contract_end');
            $contract_end AND $contract_end_date = my_date($contract_end, 'd/m/Y');
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $contract_end_date)
            ->getStyleByColumnAndRow($colIndex, $rowIndex)
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);;
            $colIndex++;

            $actual_budget = get_term_meta_value($term_id, 'actual_budget') ?: 0;
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $actual_budget)
            ->getStyleByColumnAndRow($colIndex, $rowIndex)
            ->getNumberFormat()
            ->setFormatCode("#,##0");
            $colIndex++;

            $staff_business 		= '--';
            $staff_business_id 		= get_term_meta_value($term_id, 'staff_business');
            if(!empty($staff_business_id)){
                $staff_business = $this->admin_m->get_field_by_id($staff_business_id, 'display_name');
            }
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $staff_business);
            $colIndex++;

            $gg_staff_advertise = '--';
			$kpis = $this->googleads_kpi_m->get_kpis($term_id, 'users', 'tech');
            $kpis = reset($kpis);
			if(!empty($kpis)) {
                $user_ids = array();
                foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;
    
                $gg_staff_advertise = implode(', ', array_values(array_map(function($user_id){
                    $_user = $this->admin_m->get_field_by_id($user_id);
                    return $_user['display_name'] ?: $_user['user_email'];
                }, $user_ids)));
            }            
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $gg_staff_advertise);
            $colIndex++;

            $is_service_proc = is_service_proc($term_id);
            $status = $is_service_proc ? 'Đã kích hoạt' : 'Chưa kích hoạt';
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $status);
            $colIndex++;

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-bao-cao-hop-dong-google-chua-cau-hinh.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetUnconfigured.php */
/* Location: ./application/modules/googleads/controllers/api_v2/DatasetUnconfigured.php */
