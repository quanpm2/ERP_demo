<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class McmAccounts extends MREST_Controller
{
    function __construct()
    {
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'googleads/mcm_account_m';

        $this->autoload['libraries'][]  = 'form_validation';
        $this->autoload['helpers'][]    = 'date';

        parent::__construct();
    }

    public function index_get()
    {
        $args           = wp_parse_args(parent::get(NULL, TRUE), []);
        $args_encypt    = md5(json_encode($args));

        $key_cache      = Mcm_account_m::CACHE_ALL_KEY . $args_encypt;
        $mcm_accounts   = $this->scache->get($key_cache);
        if( ! $mcm_accounts)
        {
            $mcm_accounts = $this->mcm_account_m
            ->set_term_type()
            ->join('termmeta AS m_term', 'm_term.term_id = term.term_id AND m_term.meta_key in ("account_name", "customer_id")', 'LEFT')
            ->select('term.term_id')
            ->select('MAX(IF(m_term.meta_key = "account_name", m_term.meta_value, NULL)) AS account_name')
            ->select('MAX(IF(m_term.meta_key = "customer_id", m_term.meta_value, NULL)) AS customer_id')
            ->group_by('term.term_id')
            ->order_by('term.term_id', 'DESC')
            ->limit(500);

            if(!empty($args['account_id'])){
                $mcm_accounts->having("account_name LIKE '%{$this->db->escape_like_str(trim($args['account_id']))}%' OR customer_id LIKE '%{$this->db->escape_like_str(($args['account_id']))}%'");
            }

            $mcm_accounts = $mcm_accounts->get_all();

            if( ! $mcm_accounts)
            {
                return parent::responseHandler([], 'Empty account', 'success', 204);
            }

            $this->scache->write($mcm_accounts, $key_cache, 300);
        }
        
        $mcm_accounts = array_map(function($x){
            $x->term_id = (int) $x->term_id;
            $x->term_name = (int) get_term_meta_value($x->term_id,'customer_id');
            $x->account_name = get_term_meta_value($x->term_id,'account_name');
            $x->isExternal = (bool) get_term_meta_value($x->term_id,'isExternal');
            
            return $x;
        }, $mcm_accounts);

        parent::responseHandler($mcm_accounts, 'OK');
    }

    /**
     * { function_description }
     *
     * @param      int   $contractId  The contract identifier
     */
    public function authentization_uri_get($contractId = 0)
    {
        if(FALSE == $this->googleads_m->set_contract($contractId)) parent::response(['code' => 400, 'error' => 'Truy vấn không hợp lệ.']);

        parent::response(['code' => 200, 'data' => module_url('mcm_account/grant_oauth2?redirect_url='.urlencode(admin_url("googleads/setting/{$contractId}")))]);
    }    

    /**
	 * Create new Adaccount & Business Account
	 */
	public function index_post()
	{
		$this->load->library('form_validation');

		$post = parent::post(null, TRUE);

		$this->form_validation->set_data($post);
        $this->form_validation->set_rules('account_name', 'adaccount name', 'required');
        $this->form_validation->set_rules('customer_id', 'customer id', 'required|numeric');

        if(FALSE == $this->form_validation->run())
    	{
            parent::responseHandler([], $this->form_validation->error_array(), 'error', 400);
    	}
            
    	$isExists = $this->mcm_account_m->set_term_type()
        ->select('MAX(IF(m_term.meta_key = "account_name", m_term.meta_value, NULL)) AS account_name')
        ->select('MAX(IF(m_term.meta_key = "customer_id", m_term.meta_value, NULL)) AS customer_id')
        ->join('termmeta AS m_term', 'm_term.term_id = term.term_id AND m_term.meta_key in ("account_name", "customer_id")', 'LEFT')
        ->group_by('term.term_id')
        ->having('customer_id', $post['customer_id'])
        ->count_by() > 0;

    	if($isExists)
    	{
            parent::responseHandler([], ['Tài khoản đã tồn tại. không thể thêm mới'], 'error', 400);
    	}

    	$insert_data = array(
            'term_name' => $post['customer_id'],
            'term_status' => 'active',
            'term_type' => $this->mcm_account_m->term_type
        );

    	$insert_id                   = $this->mcm_account_m->insert($insert_data);
    	$insert_data['term_id']      = (int) $insert_id;
    	$insert_data['term_name']    = $post['customer_id'];
    	$insert_data['account_name'] = $post['account_name'];
    	$insert_data['isExternal']   = true;

		$meta = array(
            'customer_id'   => $post['customer_id'],
            'account_name'  => $post['account_name'],
            'currency_code' => 'VND',
            'owner'         => $this->admin_m->id,
            'isExternal'    => 1,
			'source'        => 'direct'
        );

        foreach ($meta as $meta_key => $meta_value)
        {
            update_term_meta($insert_id, $meta_key, $meta_value);
        }


        parent::responseHandler($insert_data, 'Tạo mới tài khoản thành công.');
	}
}
/* End of file Adaccount.php */
/* Location: ./application/modules/facebookads/controllers/api_v2/Adaccount.php */
