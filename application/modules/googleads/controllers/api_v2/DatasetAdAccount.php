<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class DatasetAdAccount extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'term_users_m';
        $this->autoload['models'][] = 'contract/receipt_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'googleads/mcm_account_m';

        parent::__construct();

        $this->load->config('contract/receipt');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('googleads.overview.access')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $args = wp_parse_args(parent::get(), [
            'offset' => 0,
            'per_page' => 20,
            'cur_page' => 1,
            'is_filtering' => true,
            'is_ordering' => true
        ]);

        $this->filtering();

        $join_insight_conditon = '';
        $join_balance_spend_conditon = '';
        $dateRange = $args['where']['dateRange'] ?? FALSE;
        if ($dateRange) {
            $dates = explode(' - ', $dateRange);
            if('Invalid date' != $dates[0] && 'Invalid date' != $dates[1]) {
                $start_date = start_of_day(reset($dates));
                $end_date = end_of_day(end($dates));
                $join_insight_conditon .= " AND insights.start_date >= {$start_date} AND insights.end_date <= {$end_date}";
                $join_balance_spend_conditon .= " AND balance_spend.start_date >= {$start_date} AND balance_spend.end_date <= {$end_date}";
            }
        }

        $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')

            ->from('term')
            ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
            ->join('posts AS balance_spend', '
                balance_spend.post_id = tp_contract_ads_segment.post_id 
                AND balance_spend.post_type = "balance_spend"
                ' . ($join_balance_spend_conditon ?: ''), 
                'LEFT'
            )
            ->join('posts AS ads_segment', '
                ads_segment.post_id = tp_contract_ads_segment.post_id 
                AND ads_segment.post_type = "ads_segment"', 
                'LEFT'
            )
            ->join('term_posts AS tp_segment_mcm_account', 'tp_segment_mcm_account.post_id = ads_segment.post_id', 'LEFT')
            ->join('term AS mcm_account', '
                tp_segment_mcm_account.term_id = mcm_account.term_id 
                AND mcm_account.term_type = "mcm_account"', 
                'LEFT'
            )
            ->join('termmeta AS mcm_account_metadata', '
                mcm_account_metadata.term_id = mcm_account.term_id 
                AND meta_key = "account_name"', 
                'LEFT'
            )
            ->join('term_posts AS tp_mcm_account_insights', 'tp_mcm_account_insights.term_id = mcm_account.term_id', 'LEFT')
            ->join('posts AS insights', '
                tp_mcm_account_insights.post_id = insights.post_id 
                AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) 
                AND insights.start_date <= IF(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) 
                AND insights.post_type = "insight_segment" 
                AND insights.post_name = "day"
                ' . ($join_insight_conditon ?: ''), 
                'LEFT'
            )
            ->join('postmeta AS insight_metadata', '
                insight_metadata.post_id = insights.post_id 
                AND insight_metadata.meta_key IN ("account_currency", "clicks", "impressions", "spend", "cost", "exchange_rate")', 
                'LEFT'
            )
            ->where('term.term_type', 'google-ads')
            ->where('( mcm_account.term_id > 0 OR balance_spend.post_id > 0)')
            ->group_by('mcm_account.term_id, ads_segment.post_id, balance_spend.post_id')
            ->order_by('mcm_account_metadata.meta_value', 'ASC')

            ->select("term.term_id AS contractId")
            ->select("mcm_account.term_id AS mcmAccountTermId")
            ->select("mcm_account.term_name AS mcmAccountId")
            ->select("MAX(IF(mcm_account_metadata.meta_key = 'account_name', mcm_account_metadata.meta_value, NULL)) AS mcmAccountName")
            ->select("mcm_account.term_status AS mcmAccountStatus")
            ->select("COALESCE(insights.post_id, balance_spend.post_id) AS insightId")
            ->select("COALESCE(ads_segment.start_date, balance_spend.start_date) AS segmentStartDate")
            ->select("COALESCE(ads_segment.end_date, balance_spend.end_date) AS segmentEndDate")
            ->select("MAX(IF(insight_metadata.meta_key = 'account_currency', insight_metadata.meta_value, NULL)) AS account_currency")
            ->select("COALESCE(SUM(IF(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, NULL)), balance_spend.post_content) AS spend")
            ->select("SUM(IF(insight_metadata.meta_key = 'cost', insight_metadata.meta_value, NULL)) AS cost")
            ->select("AVG(IF(insight_metadata.meta_key = 'exchange_rate', insight_metadata.meta_value, NULL)) AS exchange_rate")
            ->select("SUM(IF(insight_metadata.meta_key = 'clicks', insight_metadata.meta_value, NULL)) AS clicks")
            ->select("SUM(IF(insight_metadata.meta_key = 'impressions', insight_metadata.meta_value, NULL)) AS impressions")
            ->select("COALESCE(ads_segment.post_type, balance_spend.post_type) as segmentType")
            ->select("COALESCE(insights.start_date, balance_spend.start_date) as insightStartDate")
            ->select("COALESCE(max(if(mcm_account_metadata.meta_key = 'source', mcm_account_metadata.meta_value, null)), balance_spend.comment_status) as insightSource")
            ->select("balance_spend.post_excerpt as note")
            ->select("ads_segment.post_author AS segment_technician_id")
            ->select("ads_segment.post_content AS segment_technician_rate")

            ->add_search('mcmAccountId', ['placeholder' => 'ID Tài khoản'])
            ->add_search('mcmAccountName', ['placeholder' => 'Tên tài khoản'])

            ->add_column('mcmAccountId', array('title' => 'Account ID', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('mcmAccountName', array('title' => 'Tên account', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('mcmAccountStatus', array('title' => 'Trạng thái', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('mcmAccountSource', array('title' => 'Nguồn tài khoản', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('segmentStartDate', array('title' => 'Ng. bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('segmentEndDate', array('title' => 'Ng. kết thúc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('segment_technician', array( 'set_select' => FALSE, 'title' => 'Kỹ thuật phụ trách'))
            ->add_column('segment_technician_rate', array( 'set_select' => FALSE, 'title' => 'Tỉ lệ phụ trách'))
            ->add_column('account_currency', array('title' => 'Đơn vị tiền', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('exchange_rate', array('title' => 'Tỉ giá Trung bình (VNĐ)', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('cost', array('title' => 'Chi tiêu gốc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('spend', array('title' => 'Chi tiêu (VNĐ)', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('clicks', array('title' => 'Clicks', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('impressions', array('title' => 'Hiển thị', 'set_select' => FALSE, 'set_order' => TRUE))

            ->add_column_callback('mcmAccountTermId', function ($data, $row_name) {
                $mcmAccountTermId = $data['mcmAccountTermId'];
                if(empty($mcmAccountTermId)) return $data;

                $data['mcmAccountSource'] = get_term_meta_value($mcmAccountTermId, 'source');

                return $data;
            }, FALSE)

            ->add_column_callback('segmentType', function ($data, $row_name) {
                if('balance_spend' != $data['segmentType'])
                {
                    return $data;
                }

                $data['mcmAccountId'] = 'Theo hợp đồng';
                $data['mcmAccountName'] = 'Cân bằng thủ công';

                if('auto' == $data['insightSource'])
                {
                    $data['mcmAccountName'] = 'Chưa xác minh hợp đồng nối';

                    if(empty($data['note']))
                    {
                        return $data;
                    }

                    $data['mcmAccountName'] = $data['note'];
                }

                return $data;
            }, FALSE)

            ->add_column_callback('segment_technician_id', function ($data, $row_name) {
                if(empty($data['segment_technician_id']))
                {
                    $data['segment_technician'] = null;
                    return $data;
                }
    
                $technician_id = $data['segment_technician_id'];
                $staff_display_name = $this->admin_m->get_field_by_id($technician_id, 'display_name');
                $data['segment_technician'] = $staff_display_name ?: '--';
                return $data;
            }, FALSE);
    
        $pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);
        $subheadings_data = $this->datatable_builder->generate(['per_page' => PHP_INT_MAX, 'cur_page' => 1]);
    
        // Calc spend depends on segment_technician_rate
        $data_items = [$data, $subheadings_data];
        foreach($data_items as &$data_item)
        {
            $data_rows = $data_item['rows'];
            $_data_rows = array_group_by($data_rows, 'mcmAccountTermId');

            $_data_rows = array_map(function($rows) {
                $instance = reset($rows);
                if(empty($instance))
                {
                    return $rows;
                }
    
                if('ads_segment' != $instance['segmentType'])
                {
                    return $rows;
                }
    
                $num_of_overlap = count($rows);
                if($num_of_overlap < 2)
                {
                    return $rows;
                }

                foreach($rows as &$row)
                {
                    $segment_technician_rate = (int) $row['segment_technician_rate'];
                    $segment_technician_rate = div($segment_technician_rate, 100);
    
                    $cost = (int) $row['cost'];
                    $cost = round($cost * $segment_technician_rate);
                    $row['cost'] = $cost;

                    $spend = (double) $row['spend'];
                    $spend = round($spend * $segment_technician_rate);
                    $row['spend'] = $spend;
    
                    $clicks = (int) $row['clicks'];
                    $clicks = round($clicks * $segment_technician_rate);
                    $row['clicks'] = $clicks;
    
                    $impressions = (int) $row['impressions'];
                    $impressions = round($impressions * $segment_technician_rate);
                    $row['impressions'] = $impressions;
                }
    
                return $rows;
            },
            $_data_rows);
            $data_item['rows'] = array_merge(...array_values($_data_rows));
        }

        $data = reset($data_items);
        $subheadings_data = end($data_items);

        // If account_currency is empty OR have not any foreign currency
        // Hide exchange rate fields
        $account_currency_rows = array_column($data['rows'], 'account_currency');
        $account_currency_rows = array_filter($account_currency_rows, function($account_currency){
            return !empty($account_currency) && 'VND' != $account_currency;
        });
        if(empty($account_currency_rows))
        {
            unset($data['headings']['cost']);
            unset($data['headings']['exchange_rate']);

            foreach($data['rows'] as &$item)
            {
                unset($item['cost']);
                unset($item['exchange_rate']);
            }
        }

        $subheadings_data = $subheadings_data['rows'];
        $subheadings = array_reduce($subheadings_data, function ($result, $item) {
            $result['spend'] += (double)$item['spend'];
            $result['clicks'] += (int)$item['clicks'];
            $result['impressions'] += (int)$item['impressions'];
            $result['cost'] += div((int)$item['cost'], 1000000);

            return $result;
        }, [
            'spend' => 0,
            'clicks' => 0,
            'impressions' => 0,
            'cost' => 0,
        ]);
        $subheadings = array_map(function ($item) {
            $item .= '';
            return $item;
        }, $subheadings);
        $data['subheadings'] = $subheadings;

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function filtering()
    {
        restrict('googleads.overview.access');

        $args = parent::get(NULL, TRUE);
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        $contractId = $args['where']['contractId'] ?? FALSE;
        if ($contractId) {
            $this->datatable_builder->where("term.term_id", (int) $contractId);
            unset($args['where']['contractId']);
        }

        $mcmAccountId = $args['where']['mcmAccountId'] ?? FALSE;
        if ($mcmAccountId) {
            $this->datatable_builder->where("mcm_account.term_name like '%" . $this->db->escape_like_str(trim($mcmAccountId)) . "%'");
            unset($args['where']['mcmAccountId']);
        }

        $mcmAccountName = $args['where']['mcmAccountName'] ?? FALSE;
        if ($mcmAccountName) {
            $this->datatable_builder->where("mcm_account_metadata.meta_value like '%" . $this->db->escape_like_str(trim($mcmAccountName)) . "%'");
            unset($args['where']['mcmAccountName']);
        }
    }
}
/* End of file DatasetReceipts.php */
/* Location: ./application/modules/contract/controllers/api_v2/DatasetReceipts.php */