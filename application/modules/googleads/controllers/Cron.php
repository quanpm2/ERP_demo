<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Notifications\Erp\Reports\Ads\Googleads\CustomerDailyClicks\Sms as CustomerDailyClicks;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Cron extends Public_Controller 
{
	function __construct()
	{
		parent::__construct();
		$models = array(
			'googleads_m',
			'googleads_log_m',
			'googleads_report_m',
			'googleads/mcm_account_m',
			'googleads/adwords_report_m'
		);

		$this->load->model($models);
		$this->load->config('googleads');
		$this->load->config('amqps');
	}

	public function index()
	{
		$hour = date('G');

		$day_of_week = date('w');
		if($hour == 1 && $day_of_week != 0)
		{
			$this->send_contract_mail2admin();
		}

		/*Mail thống kê tài khoản hiệu quả từ khóa quảng cáo tháng trước */
		if($hour == 6 && date('d') == 1)
		{
			$this->googleads_report_m->send_mail_inefficient_by(strtotime('last month'));	
		}

		if(in_array($hour, array(2,3,4)))
        {
			$this->prepare_adword_data();
		}
		
		if(in_array($hour, array(9,10)))
		{
			// $this->batch_daily_sms(); // Auto SMS daily
			$this->initializeDailySMS();
			// $this->googleads_report_m->send_statistical_cronmail2admin(); // Email thống kê các báo cáo đã thực hiện trong ngày

			if($hour == 9)
			{
				$this->googleads_report_m->send_mail_inefficient();
			}
		}

		if($hour >= 10)
        {
        	$this->proc_alert_services_2();
            $this->calc_real_process_daily();
        }
	}


	public function initializeDailySMS()
	{
		$terms = $this->googleads_m->get_active_terms();
        if(empty($terms)) return FALSE;

        $logs = $this->googleads_log_m
        ->select('log_id, term_id')
	    ->where('log_time_create >= curdate()')
	    ->where('log_time_create < curdate() + interval 1 day')
	    ->get_many_by(['log_type' => 'googleads-report-sms','log_status >' => 0]);

    	$sent_term_ids = array_column($logs, 'term_id');

    	$contracts = array_filter($terms, function($x) use($sent_term_ids) {
    		return ! in_array($x->term_id, $sent_term_ids);
    	});

    	if(empty($contracts)) return true;

    	$now = time();
    	$amqps_host 	= $this->config->item('host', 'amqps');
		$amqps_port 	= $this->config->item('port', 'amqps');
		$amqps_user 	= $this->config->item('user', 'amqps');
		$amqps_password = $this->config->item('password', 'amqps');
		$queue 			= $this->config->item('googleads', 'amqps_queues');

		$connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
		$channel 	= $connection->channel();

		$channel->queue_declare($queue, false, true, false, false);

		foreach ($contracts as $term)
		{
			$payload = [ 'contractId' => $term->term_id, 'event' => 'googleads.daily_sms'];
			$message = new AMQPMessage(
				json_encode($payload),
				array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
			); 

			$channel->basic_publish($message, '', $queue);
		}

		$channel->close();
		$connection->close();
	}

	public function fireBatchSMS()
	{
		$terms = $this->googleads_m->get_active_terms();
        if(empty($terms)) return FALSE;

        $logs = $this->googleads_log_m
        ->select('log_id, term_id')
	    ->where('log_time_create >= curdate()')
	    ->where('log_time_create < curdate() + interval 1 day')
	    ->get_many_by(['log_type' => 'googleads-report-sms','log_status >' => 0]);

    	$sent_term_ids = array_column($logs, 'term_id');
    	foreach ($terms as $term)
    	{
    		if( in_array($term->term_id, $sent_term_ids) ) continue; // Contract send successfully SMS in past

    		$result = (new CustomerDailyClicks(['contract' => $term->term_id]))->send();    		
    		
			$this->googleads_log_m->insert(array(
	    		'term_id' => $term->term_id,
				'log_type' => 'googleads-report-sms',
	    		'log_title' => 'Send SMS '.my_date(0, 'Y-m-d H:i:s'),
	    		'log_status' => ( empty($result) ? 0 : 1 ),
	    		'log_content' => serialize($result),
	    		'log_time_create' => my_date(0, 'Y-m-d H:i:s'),
			));
    	}
	}

	public function frequency()
	{
		$hour = date('G');

		if(in_array($hour, array(2,3,4))) $this->prepare_adword_data();
		if(in_array($hour, array(9, 10))) $this->send_daily_sms();

		if($hour >= 8)
        {
            $this->proc_alert_services();
            $this->calc_real_process_daily();
        }
	}

	protected function batch_daily_sms()
	{
		$terms = $this->googleads_m->get_active_terms();
        if(empty($terms)) return FALSE;

        $logs = $this->googleads_log_m
        ->select('log_id, term_id')
	    ->where('log_time_create >= curdate()')
	    ->where('log_time_create < curdate() + interval 1 day')
	    ->get_many_by(['log_type' => 'googleads-report-sms','log_status >' => 0]);

    	$sent_term_ids = array_column($logs, 'term_id');
    	foreach ($terms as $term)
    	{
    		if( in_array($term->term_id, $sent_term_ids) ) continue; // Contract send successfully SMS in past

    		$result = (new CustomerDailyClicks(['contract' => $term->term_id]))->send();    		
    		
			$this->googleads_log_m->insert(array(
	    		'term_id' => $term->term_id,
				'log_type' => 'googleads-report-sms',
	    		'log_title' => 'Send SMS '.my_date(0, 'Y-m-d H:i:s'),
	    		'log_status' => ( empty($result) ? 0 : 1 ),
	    		'log_content' => serialize($result),
	    		'log_time_create' => my_date(0, 'Y-m-d H:i:s'),
			));
    	}
	}

	/**
	 * Sends a daily week report.
	 */
	protected function send_daily_sms()
	{
		$terms = $this->googleads_m->get_active_terms();
        if(empty($terms)) return FALSE;

        $logs = $this->googleads_log_m
        ->select('log_id, term_id')
	    ->where('log_time_create >= curdate()')
	    ->where('log_time_create < curdate() + interval 1 day')
	    ->get_many_by(['log_type' => 'googleads-report-sms','log_status >' => 0]);

    	$sent_term_ids = array_column($logs, 'term_id');
    	foreach ($terms as $term)
    	{
    		if( in_array($term->term_id, $sent_term_ids) ) continue; // Contract send successfully SMS in past
    		$result = $this->googleads_report_m->send_daily_sms($term->term_id);
    		
			$this->googleads_log_m->insert(array(
	    		'term_id' => $term->term_id,
				'log_type' => 'googleads-report-sms',
	    		'log_title' => 'Send SMS '.my_date(0, 'Y-m-d H:i:s'),
	    		'log_status' => ( empty($result) ? 0 : 1 ),
	    		'log_content' => serialize($result),
	    		'log_time_create' => my_date(0, 'Y-m-d H:i:s'),
			));
    	}
	}


	public function scheduled_reporting()
	{
		$this->_write_log_cron();

		$terms = $this->googleads_m->get_active_terms();
        if(empty($terms)) return FALSE;

		$default_log = array($this->googleads_log_m->prefix_log_type.'sms'=>array());
		$log_records = $notify_list = $default_log;

		$logs = $this->googleads_log_m
		->where_in('log_type', array_keys($log_records))
		->where('log_status >', 0)
	    ->where('log_time_create >= curdate()')
	    ->where('log_time_create < curdate() + interval 1 day')
	    ->get_many_by();

		if(!empty($logs))
		{
			foreach ($logs as $log)
			{
				if(in_array($log->term_id, $log_records[$log->log_type])) 
					continue;
				$log_records[$log->log_type][] = $log->term_id;
			}
		}

    	$log_sms_key = $this->googleads_log_m->prefix_log_type.'sms';

        foreach ($terms as $term) 
        {
        	$allowed_send_sms = !in_array($term->term_id, $log_records[$log_sms_key]);
        	if($allowed_send_sms) $this->_proc_sms_report($term->term_id);
        }

        $this->googleads_report_m->send_statistical_cronmail2admin();
	}

	private function _write_log_cron(){

        $this->load->helper('file');

        write_file(FCPATH.'log/log_cron.txt', $this->input->ip_address() .' [' . date("D M j G:i:s Y") . ']'  . "\n" ,'a');
    }

	private function _proc_sms_report($term_id)
	{
		$result = $this->googleads_report_m->send_daily_sms($term_id);
		$is_send = empty($result) ? 0 : 1;
		$log = array(
			'log_type' => $this->googleads_log_m->prefix_log_type . 'sms',
    		'log_status' => $is_send,
    		'term_id' => $term_id,
    		'log_content' => serialize($result),
    		'log_time_create' => my_date(0, 'Y-m-d H:i:s'),
    		'log_title' => 'Send SMS '.my_date(0, 'Y-m-d H:i:s')
			);

		$log_id = $this->googleads_log_m->insert($log);
	}


	/**
	 * Downloads a performance daily.
	 *
	 * @param      <type>  $term_id      The term identifier
	 * @param      string  $report_type  The report type
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	function download_performance_daily($term_id, $report_type = 'ACCOUNT_PERFORMANCE_REPORT')
	{
		$mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
		if(empty($mcm_account_id)) 
		{
			trigger_error('MCM_ACCOUNT IS NULL');
			return FALSE;
		}

		$now = time();
		$meta_key = strtolower($report_type).'_download_next_time';
		$next_time = (int)get_term_meta_value($term_id,$meta_key);
		if($next_time > $now)
		{
			# echo '_download_next_time > now';
			return FALSE;
		}

		$locked_meta_key = strtolower($report_type).'_locked_time_to';
		$locked_time_to = (int)get_term_meta_value($term_id,$locked_meta_key);
		if($locked_time_to > $now)
		{
			# echo '_locked_time_to > now';
			return FALSE;
		}

		$lock_time_to = strtotime('+15 minutes',$now);
		update_term_meta($term_id, $locked_meta_key, $lock_time_to);

		$adsSegments    = $this->term_posts_m->get_term_posts($this->contract->term_id, $this->ads_segment_m->post_type);

	 	$anchorTime = strtotime('-3 days');

	 	$this->load->model('googleads/base_adwords_m');

        if( ! empty($adsSegments))
        {
            foreach ($adsSegments as $adsSegment)
            {
            	$adaccount = $this->term_posts_m->get_post_terms($adsSegment->post_id, $this->mcm_account_m->term_type);
                if(empty($adaccount)) continue;

                $adaccount  = reset($adaccount);

                $_segment_end = $adsSegment->end_date ?: time();
                if($anchorTime > $_segment_end) continue;

                $this->base_adwords_m->set_mcm_account($adaccount->term_id);
				$this->base_adwords_m->download($report_type,$anchorTime, $now);
            }
        }

		$next_time = $this->mdate->endOfDay($now);
		update_term_meta($term_id, $meta_key, $next_time);
		update_term_meta($term_id, $locked_meta_key, 0);
		return TRUE;
	}

	public function prepare_adword_data()
	{
		$terms = $this->googleads_m->get_active_terms();
		if(empty($terms)) return FALSE;

		foreach ($terms as $term)
		{
			$this->download_performance_daily($term->term_id, 'ACCOUNT_PERFORMANCE_REPORT');
			$this->download_performance_daily($term->term_id, 'KEYWORDS_PERFORMANCE_REPORT');
		}
	}

	public function download_data($term_id=0,$start_time=0,$end_time=0,$refresh = FALSE)
	{
		$start_time = $this->mdate->startOfDay($start_time);
		$end_time = empty($end_time) ? time() : $end_time;
		$end_time = $this->mdate->endOfDay($end_time);
		
		$mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
		if(empty($mcm_account_id)) return FALSE;

		if( ! $refresh) return FALSE;

		$this->load->model('base_adwords_m');
		$this->base_adwords_m->set_mcm_account($mcm_account_id);
		$this->base_adwords_m->download('ACCOUNT_PERFORMANCE_REPORT',$start_time,$end_time);
		$this->base_adwords_m->download('CAMPAIGN_PERFORMANCE_REPORT',$start_time,$end_time);
		$this->base_adwords_m->download('GEO_PERFORMANCE_REPORT',$start_time,$end_time);
		
		$this->base_adwords_m->download('ADGROUP_PERFORMANCE_REPORT',$start_time,$end_time);
		$this->base_adwords_m->download('KEYWORDS_PERFORMANCE_REPORT',$start_time,$end_time);

		$selectorFields = $this->base_adwords_m->init_selector_fields('ADGROUP_PERFORMANCE_REPORT')->get_selector_fields();
		if (($key = array_search('Date', $selectorFields)) !== false) unset($selectorFields[$key]);
		$this->base_adwords_m->set_selector_fields($selectorFields)->download('ADGROUP_PERFORMANCE_REPORT', $start_time, $end_time);

		$selectorFields = $this->base_adwords_m->init_selector_fields('KEYWORDS_PERFORMANCE_REPORT')->get_selector_fields();
		if (($key = array_search('Date', $selectorFields)) !== false) unset($selectorFields[$key]);
		$this->base_adwords_m->set_selector_fields($selectorFields)->download('KEYWORDS_PERFORMANCE_REPORT', $start_time, $end_time);

		return TRUE;
	}
	
	public function send_contract_mail2admin()
	{
		$this->load->model('log_m');
		$this->load->library('table');
		$this->config->load('table_mail');
		$this->data = array();
		
		$descriptions = array('Hợp đồng được thực hiện'=>0,'Hợp đồng được cấp số'=>0,'Dịch vụ đã kết thúc'=>0,'Hợp đồng sắp hêt hạn trong 7 ngày'=>0);

		$this->data = array(
			'google-ads'=>$descriptions,
			'webgeneral'=>$descriptions);	

		$content = 
		$this->get_proc_service().
		$this->get_started_contract().
		$this->get_stopped_contract().
		$this->get_willexpire_contract();

		$content = $this->overview_contract().$content;

		if(empty($content)) return FALSE;

		$title = 'Báo cáo trạng thái hợp đồng ngày '. my_date(strtotime('yesterday'),'d/m/Y H:i:s');

		$this->load->library('email');
		$this->email
		->from('support@webdoctor.vn', 'Webdoctor.vn')
		->to('hopdong@adsplus.vn')
		->subject($title)
		->message($content)
		->send();
	}

	private function fetch_webgeneral()
	{
		$result = array();

		$this->load->model('webgeneral/webgeneral_m');
		$terms = $this->term_m
		->where('term.term_type', $this->webgeneral_m->term_type)
		->where_in('term.term_status', 'publish')
		->get_many_by();

		$terms_lates = 0;
		$start_time = $this->mdate->startOfDay(date('1-m-Y'));
		foreach ($terms as $key => $term){
			$service_package = get_term_meta_value($term->term_id,'service_package');
			if($service_package == 'startup') {
				unset($terms[$key]);
				continue;
			}

			$term->start_service_time = get_term_meta_value($term->term_id,'start_service_time');
			if($term->start_service_time < $start_time) continue;

			++$terms_lates;
		}

		$result['webgeneral']['Hợp đồng mới'] = $terms_lates;
		$result['webgeneral']['Hợp đồng đang thực hiện'] = count($terms);

		return $result;
	}

	private function fetch_adsplus()
	{
		$result = array();
		$terms = $this->term_m
		->where('term.term_type', 'google-ads')
		->where_in('term.term_status', array_keys($this->config->item('status','googleads')))
		->get_many_by();

		$start_time = $this->mdate->startOfDay(date('1-m-Y'));
		$terms_lates = 0;
		foreach ($terms as $term){
			$term->started_service = get_term_meta_value($term->term_id,'start_service_time');
			if($term->started_service < $start_time) continue;
			++$terms_lates;
		}

		$result['google-ads']['Hợp đồng mới'] = $terms_lates;
		$result['google-ads']['Hợp đồng đang thực hiện'] = count($terms);

		return $result;
	}

	public function overview_contract()
	{
		$template = $this->config->item('mail_template');
		$template['table_open'] = '<table  border="0" cellpadding="4" cellspacing="0" style="font-size:12px; font-family:Arial" width="100%"><tr><th colspan="5" style="background: #138474 none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">';

		$data = array_merge($this->fetch_adsplus(),$this->fetch_webgeneral());
		$this->data = array_merge_recursive($data,$this->data);

		$this->table->set_caption('Thống kê tổng quan hợp đồng');	

		$headings = array('');
		$types = array();
		if(!empty($this->data)){
			foreach ($this->data as $type => $var){
				
				if($type == 'webgeneral'){
					$headings[] = 'WEB TỔNG HỢP';	
				} 
				else if($type == 'google-ads')
				{
					$headings[] = 'ADSPLUS';
				}
				else $headings[] = $type;
				$types[] = $type;
			}

			$data = array();
			reset($this->data);
			$tmp = key($this->data);
			foreach ($this->data[$tmp] as $key => $value) {
				$row = array($key);
				foreach ($this->data as $type => $arr)
					$row[] = (int)@$this->data[$type][$key];
				$this->table->add_row($row);
			}	
		}

		$this->table->set_template($template);
		$this->table->set_heading($headings);

		return $this->table->generate();
	}

	private function get_logs($time,$log_type='')
	{
		$begin_time = $this->mdate->startOfDay($time);
		$end_time = $this->mdate->endOfDay($begin_time);
		$logs = $this->log_m
		->where('log_type',$log_type)
		->where('log_time_create >',my_date($begin_time,'Y/m/d H:i:s'))
		->where('log_time_create <',my_date($end_time,'Y/m/d H:i:s'))
		->order_by('log_time_create')
		->group_by('term_id')
		->get_many_by();

		return $logs;
	}

	private function get_started_contract()
	{
		$yesterday = strtotime('yesterday');
		$data = $this->get_logs($yesterday,'started_service');
		if(empty($data)) return '';

		$headings = array(
			array('data'=>'STT','width'=>'10%'),
			array('data'=>'Mã HĐ','width'=>'20%'),
			array('data'=>'Thời gian cấp số','width'=>'20%'),
			array('data'=>'Nhân viên kinh doanh')
		);

		$template = $this->config->item('mail_template');
		$template['table_open'] = '<table  border="0" cellpadding="4" cellspacing="0" style="font-size:12px; font-family:Arial" width="100%"><tr><th colspan="5" style="background: #439A43 none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">';
		$this->table->set_heading($headings);
		$this->table->set_template($template);

		$this->table->set_caption('Thống kê hợp đồng được cấp số');
		$this->table->set_heading($headings);

		foreach ($data as $key => $item){

			$term = $this->term_m->get($item->term_id);
			if(!isset($this->data[$term->term_type]))
				$this->data[$term->term_type] = array('Hợp đồng được cấp số'=>0);
			if(empty($this->data[$term->term_type]['Hợp đồng được cấp số']))
				$this->data[$term->term_type]['Hợp đồng được cấp số'] = 1;
			else $this->data[$term->term_type]['Hợp đồng được cấp số']++;

			$no = $key + 1;
			$contract_code = get_term_meta_value($item->term_id,'contract_code');
			$time = my_date(strtotime($item->log_time_create),'H:i:s');
			$this->load->model('staffs/admin_m');
			$staff = get_term_meta_value($item->term_id,'staff_business');
			if(!empty($staff)) $staff_name = $this->admin_m->get_field_by_id($staff, 'display_name');

			$this->table->add_row($no,$contract_code,$time,$staff_name);
		}

		return $this->table->generate();
	}

	private function get_proc_service()
	{
		$yesterday = strtotime('yesterday');
		$data = $this->get_logs($yesterday,'start_service_time');
		if(empty($data)) return '';
		$template = $this->config->item('mail_template');
		$template['table_open'] = '<table  border="0" cellpadding="4" cellspacing="0" style="font-size:12px; font-family:Arial" width="100%"><tr><th colspan="6" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">';
		$headings = array(
			array('data'=>'STT','width'=>'10%'),
			array('data'=>'Mã HĐ','width'=>'20%'),
			array('data'=>'Thời gian thực hiện','width'=>'20%'),
			array('data'=>'Ngày thực hiện','width'=>'20%'),
			array('data'=>'Thời hạn','width'=>'10%'),
			array('data'=>'Nhân viên kinh doanh'),
			);
		$this->table->set_template($template);
		$this->table->set_caption('Dịch vụ được thực hiện '.my_date($yesterday));
		$this->table->set_heading($headings);

		foreach ($data as $key => $item){

			$term = $this->term_m->get($item->term_id);
			if(!isset($this->data[$term->term_type])) 
				$this->data[$term->term_type] = array('Hợp đồng được thực hiện'=>0);
			if(empty($this->data[$term->term_type]['Hợp đồng được thực hiện']))
				$this->data[$term->term_type]['Hợp đồng được thực hiện'] = 1;
			else $this->data[$term->term_type]['Hợp đồng được thực hiện']++;


			$no = $key + 1;
			$contract_code = get_term_meta_value($item->term_id,'contract_code');
			$time = my_date(strtotime($item->log_time_create),'H:i:s');

			$this->load->model('staffs/admin_m');
			$staff = get_term_meta_value($item->term_id,'staff_business');
			if(!empty($staff)) $staff = $this->admin_m->get_field_by_id($staff, 'display_name');

			$start_service_time = get_term_meta_value($item->term_id,'start_service_time');
			$end_service_time = get_term_meta_value($item->term_id,'end_service_time');
			$date_service = my_date($start_service_time).' - '.(empty($end_service_time)?'Chưa xác định':my_date($end_service_time));
			$days = 0;
			if(!empty($end_service_time))
				$days = round(abs($start_service_time-$end_service_time)/86400);
			$days = empty($days) ? '---' : $days.' ngày';

			$this->table->add_row($no,$contract_code,$time,$date_service,$days,$staff);
		}

		return $this->table->generate();
	}

	private function get_stopped_contract()
	{
		$yesterday = strtotime('yesterday');
		$data = $this->get_logs($yesterday,'end_service_time');
		if(empty($data)) return '';

		$headings = array(
			array('data'=>'STT','width'=>'10%'),
			array('data'=>'Mã HĐ','width'=>'20%'),
			array('data'=>'Thời gian kết thúc','width'=>'20%'),
			array('data'=>'Thời gian thực hiện','width'=>'20%'),
			array('data'=>'Nhân viên kinh doanh')
			);
		$template = $this->config->item('mail_template');
		$template['table_open'] = '<table  border="0" cellpadding="4" cellspacing="0" style="font-size:12px; font-family:Arial" width="100%"><tr><th colspan="5" style="background: #BB3E3E none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">';

		$this->table->set_heading($headings);
		$this->table->set_template($template);
		$this->table->set_caption('Thống kê dịch vụ đã kết thúc '.my_date($yesterday));
		$this->table->set_heading($headings);

		foreach ($data as $key => $item){

			$term = $this->term_m->get($item->term_id);
			if(!isset($this->data[$term->term_type]))
				$this->data[$term->term_type]['Dịch vụ đã kết thúc'] = array('Dịch vụ đã kết thúc'=>0);
			if(empty($this->data[$term->term_type]['Dịch vụ đã kết thúc']))
				$this->data[$term->term_type]['Dịch vụ đã kết thúc'] = 1;
			else $this->data[$term->term_type]['Dịch vụ đã kết thúc']++;

			$no = $key + 1;
			$contract_code = get_term_meta_value($item->term_id,'contract_code');
			$time = my_date(strtotime($item->log_time_create),'H:i:s');

			$this->load->model('staffs/admin_m');
			$staff = get_term_meta_value($item->term_id,'staff_business');
			if(!empty($staff)) $staff = $this->admin_m->get_field_by_id($staff, 'display_name');

			$start_service_time = get_term_meta_value($item->term_id,'start_service_time');
			$end_service_time = get_term_meta_value($item->term_id,'end_service_time');
			$date_service = my_date($start_service_time).' - '.(empty($end_service_time)?'Chưa xác định':my_date($end_service_time));
			
			$this->table->add_row($no,$contract_code,$time,$date_service,$staff);
		}

		return $this->table->generate();
	}

	private function get_willexpire_contract()
	{
		$terms = $this->term_m
		->where_in('term.term_status',array('publish','pending'))
		->join('termmeta', 'termmeta.term_id = term.term_id', 'INNER')
		->where('termmeta.meta_key','end_service_time')
		->where('termmeta.meta_value >=', time())
		->where('termmeta.meta_value <=', strtotime("+7 day"))
		->order_by('termmeta.meta_value')
		->get_many_by();

		if(empty($terms)) return '';
		$this->start_service_adsplus = count($terms);

		$headings = array(
			array('data'=>'STT','width'=>'10%'),
			array('data'=>'Mã HĐ','width'=>'20%'),
			array('data'=>'Ngày kết thúc dự kiến','width'=>'20%'),
			array('data'=>'Còn lại','width'=>'20%'),
			array('data'=>'Nhân viên kinh doanh'),
			);

		$template = $this->config->item('mail_template');
		$template['table_open'] = '<table  border="0" cellpadding="4" cellspacing="0" style="font-size:12px; font-family:Arial" width="100%"><tr><th colspan="5" style="background: #D8D83E none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">';

		$this->table->set_heading($headings);
		$this->table->set_template($template);
		$this->table->set_caption('Hợp đồng sắp hết hạn trong 7 ngày tới');

		foreach ($terms as $key => $term){

			if(!isset($this->data[$term->term_type]))
				$this->data[$term->term_type] = array('Hợp đồng sắp hêt hạn trong 7 ngày'=>0);

			if(empty($this->data[$term->term_type]['Hợp đồng sắp hêt hạn trong 7 ngày']))
				$this->data[$term->term_type]['Hợp đồng sắp hêt hạn trong 7 ngày'] = 1;
			else $this->data[$term->term_type]['Hợp đồng sắp hêt hạn trong 7 ngày']++;

			$no = $key + 1;
			$contract_code = get_term_meta_value($term->term_id,'contract_code');
			
			$this->load->model('staffs/admin_m');
			$staff = get_term_meta_value($term->term_id,'staff_business');

			if(!empty($staff)) $staff = $this->admin_m->get_field_by_id($staff, 'display_name');

			$date = my_date($term->meta_value);
			$timeleft = round(abs($term->meta_value-time())/86400);

			$this->table->add_row($no,$contract_code,$date,$timeleft.' ngày',$staff);
		}

		return $this->table->generate();
	}

	/**
	 * CRON actions that get new ADWORD DATA and Sending mail alert report
	 *
	 * @param      array   $terms  The terms
	 *
	 * @return     bool  Result of function
	 */
	public function proc_alert_services_2($terms = array())
	{
		$terms = empty($terms) ? $this->googleads_m->get_active_terms() : $terms;
    	if(!$terms) return FALSE;

    	$amqps_host 	= $this->config->item('host', 'amqps');
		$amqps_port 	= $this->config->item('port', 'amqps');
		$amqps_user 	= $this->config->item('user', 'amqps');
		$amqps_password = $this->config->item('password', 'amqps');
		$queue 			= $this->config->item('googleads', 'amqps_queues');

		$connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
		$channel 	= $connection->channel();
		$channel->queue_declare($queue, false, true, false, false);

		foreach ($terms as $term)
		{
			$time_next_api_check = get_term_meta_value($term->term_id, 'time_next_api_check');
    		if(!empty($time_next_api_check) && $time_next_api_check > time()) continue;
    		$payload = [
				'event'			=> 'googleads.cron.alert',
				'contractId' 	=> $term->term_id,
				'created' 		=> my_date()
			];

			$message = new AMQPMessage(
				json_encode($payload),
				array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
			);
			$channel->basic_publish($message, '', $queue);		
		}

		$channel->close();
		$connection->close();
	}

	/**
	 * CRON actions that get new ADWORD DATA and Sending mail alert report
	 *
	 * @param      array   $terms  The terms
	 *
	 * @return     bool  Result of function
	 */
	public function proc_alert_services($terms = array())
	{
		$terms = empty($terms) ? $this->googleads_m->get_active_terms() : $terms;
    	if(!$terms) return FALSE;

    	foreach ($terms as $term) 
    	{
    		$_contract = new googleads_m();
    		$_contract->set_contract($term);

    		$now = time();
    		$time_next_api_check = get_term_meta_value($term->term_id,'time_next_api_check');
    		if(!empty($time_next_api_check) && $time_next_api_check > $now) continue;

    		$target_model = $this->googleads_m->get_adword_model($term->term_id);
    		$target_model->get_the_progress($term->term_id,FALSE,FALSE,TRUE);

    		/* Đồng bộ tất cả số liệu có liên quan của dịch vụ quảng cáo */
    		$this->sync_all_progress($term);
    		
    		$actual_progress_percent_net = ((float) get_term_meta_value($term->term_id, 'actual_progress_percent_net'))*100;
			if($actual_progress_percent_net >= 80) /* Xử lý theo Ngân sách hợp đồng */
			{	
				$this->send_progress_email($term, 'actual', TRUE); //gửi mail thông báo đến admin và các bên liên quan
				$time_next_api_check = strtotime('+1 hour',$now);
			}
			else $time_next_api_check = strtotime('+1 hour',$now);

			$progress = (float) get_term_meta_value($term->term_id, 'actual_progress_percent');
			if($progress >= 100) /* Xử lý theo Ngân sách hợp đồng */
			{
	    		# gửi kèm mail [demo] báo cáo kết thúc gửi cho khách hàng đến admin và các bên liên quan để xem trước
	    		$this->googleads_report_m->send_finished_service($term->term_id,'admin');
			}
			else if($progress >= 80)
			{
				$notice_finish_mail_has_sent = get_term_meta_value($term->term_id,'notice_finish_mail_has_sent');
				if(empty($notice_finish_mail_has_sent))
				{
	    			$this->googleads_report_m->send_noticefinish_mail($term->term_id, 'customer');
					update_term_meta($term->term_id,'notice_finish_mail_has_sent',1);
				}
			}

	  		update_term_meta($term->term_id, 'time_next_api_check', $time_next_api_check);
	  		
    		// AUTO-UPDATE STATUS BY PAUSED CAMPAIGN DURATIONS
    		$result_updated_on = (int) get_term_meta_value($term->term_id, 'result_updated_on');
    		if(empty($result_updated_on)) continue; // remove after

    		$is_valid_pending_durations = strtotime('+1 day',$result_updated_on) >= $this->mdate->startOfDay();

    		# Case : Adwords data has not updated greater than 1 day then status is "PENDING"    		
    		if( ! $is_valid_pending_durations && $term->term_status == 'publish')
    		{
    			$this->googleads_m->set_default_query()->update($term->term_id, ['term_status' => 'pending']);
    			continue;
    		}

    		# Case : Adwords data has not updated smaller than 1 day then status is "PENDING"
    		if($is_valid_pending_durations && $term->term_status == 'pending')
    		{
    			$this->googleads_m->set_default_query()->update($term->term_id, ['term_status' => 'publish']);
    			continue;
    		}
    	}
	}


	/**
	 * Đồng bộ toàn bộ số liệu quảng cáo nếu chiến dịch có phát sinh thay đổi về ngân sách sử dụng
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function sync_all_progress($term_id = 0)
	{
		$contract = new googleads_m();
		try
		{
			$contract->set_contract($term_id);
			$contract->get_behaviour_m()->sync_all_progress();
		} 
		catch (Exception $e) { var_dump($e->getMessage()); }
	}


	/**
	 * Calculates the real process daily.
	 *
	 * @param      array   $terms  The terms
	 *
	 * @return     bool  result of function
	 */
	protected function calc_real_process_daily($terms = array())
	{
		$terms = empty($terms) ? $this->googleads_m->get_active_terms() : $terms;
    	if(!$terms) return FALSE;

		foreach ($terms as $term) 
		{
			$target_model = $this->googleads_m->get_adword_model($term->term_id);
    		$real_progress = $target_model->calc_real_progress($term->term_id);
		}

		return TRUE;
	}
}
/* End of file Cron.php 
/* Location: ./application/modules/googleads/controllers/Cron.php */
