<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use AdsService\AdsReport\GoogleadsReport;
class Report extends Public_Controller {

	public function download($term_id = 0, $file_name ='')
	{	
		$error_msg = 'Tài liệu hết hiệu lực. Xin quý khách vui lòng liên hệ người phụ trách hợp đồng hoặc support@adsplus.vn để được hỗ trợ chi tiết.'; 
		if(empty($term_id) || empty($file_name)) 
			die($error_msg);

        $file_path = (new GoogleadsReport())->download_report_file($term_id, $file_name);
        if(FALSE === $file_path) die($error_msg);

		$this->load->helper('download');
		force_download($file_path, NULL);
	}
}
/* End of file Report.php */
/* Location: ./application/modules/googleads/controllers/Report.php */
