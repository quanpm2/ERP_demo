<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistical extends Public_Controller {

	function __construct()
	{
		parent::__construct();
		$models = array(
			'googleads_m'
			);
		$this->load->model($models);
	}

	public function index()
	{
		$data = array();
		$data = array_merge($data,$this->fetch_adsplus(),$this->fetch_webgeneral());
		$this->load->view('frontend/tv_overview/index',$data);
	}

	public function all_staff_rank()
	{
		$data = array();
		$results = $this->googleads_m
		->select('user_id,COUNT(term.term_id) as term_count')
		->join('googleads_kpi','googleads_kpi.term_id = term.term_id','LEFT OUTER')
		->set_term_type()
		->where('term_status','publish')
		->group_by('user_id')
		->order_by('term_count','desc')
		->get_many_by();

		if(empty($results)) die('Không có dữ liệu');

		$nodes = array();

		foreach ($results as $item) 
		{
			$user = $this->admin_m->get($item->user_id);
			if(empty($user))
			{
				$nodes[] = (object) array('day_labels' => 'uncategorized','count' => $item->term_count);
				continue;
			}

			$display_name = $user->display_name ?: $user->user_email;
			$nodes[] = (object) array('day_labels' => $display_name,'count' => $item->term_count,);
		}

		$chart_result = array(
			'x_labels' => 'day_labels',
			'series' => array('count'),
			'data' => $nodes
			);

		$datalabels = array(
			'enabled' => true,
			// rotation: -90,
			'colorByPoint' => true,
			'color' => '#FFFFFF',
			'align' => 'center',
			'format' => '{point.y:.0f}',
			'y' => 20,
			'style' => array(
			    'fontSize' => '30px',
			    'fontFamily' => 'Verdana, sans-serif'
				)
			);

		$this->load->library('highcharts');
		$this->highcharts
		->set_title('THỐNG KÊ SỐ LƯỢNG HỢP ĐỒNG ADSPLUS ĐANG CHẠY')
		->set_type('column')
		->set_dimensions(null, 600)
		->from_result($chart_result)
		->set_serie_options(array('dataLabels'=>$datalabels,'colorByPoint'=>true),'count')
		->add();
		
		$data['chart'] = $this->highcharts->render();

		$this->load->view('statistical/top_staffs',$data);
	}

	public function month_staff_rank()
	{
		$data = array();
		$results = $this->googleads_m
		->select('user_id,COUNT(term.term_id) as term_count')
		->join('googleads_kpi','googleads_kpi.term_id = term.term_id','LEFT OUTER')
		->set_term_type()
		->where('term_status','publish')
		->group_by('user_id')
		->order_by('term_count','desc')
		->get_many_by();

		if(empty($results)) die('Không có dữ liệu');

		$nodes = array();

		foreach ($results as $item) 
		{
			$user = $this->admin_m->get($item->user_id);
			if(empty($user))
			{
				$nodes[] = (object) array('day_labels' => 'uncategorized','count' => $item->term_count);
				continue;
			}

			$display_name = $user->display_name ?: $user->user_email;
			$nodes[] = (object) array('day_labels' => $display_name,'count' => $item->term_count,);
		}

		$chart_result = array(
			'x_labels' => 'day_labels',
			'series' => array('count'),
			'data' => $nodes
			);

		$datalabels = array(
			'enabled' => true,
			// rotation: -90,
			'colorByPoint' => true,
			'color' => '#FFFFFF',
			'align' => 'center',
			'format' => '{point.y:.0f}',
			'y' => 20,
			'style' => array(
			    'fontSize' => '30px',
			    'fontFamily' => 'Verdana, sans-serif'
				)
			);

		$this->load->library('highcharts');
		$this->highcharts
		->set_title('THỐNG KÊ SỐ LƯỢNG HỢP ĐỒNG ADSPLUS ĐANG CHẠY')
		->set_type('column')
		->set_dimensions(null, 600)
		->from_result($chart_result)
		->set_serie_options(array('dataLabels'=>$datalabels,'colorByPoint'=>true),'count')
		->add();
		
		$data['chart'] = $this->highcharts->render();

		$this->load->view('statistical/rank_staffs',$data);
	}

	public function top_staffs()
	{
		$results = $this->googleads_m
		->select('user_id,COUNT(term.term_id) as term_count')
		->join('googleads_kpi','googleads_kpi.term_id = term.term_id','LEFT OUTER')
		->set_term_type()
		->where('term_status','publish')
		->group_by('user_id')
		->order_by('term_count','desc')
		->get_many_by();
		
		$data = array();
		$data['series_data'] = array();
		$data['images'] = array();

		foreach ($results as $item) 
		{
			if(empty($item->user_id))
			{
				$data['images'][] = base_url('/template/frontend/tv_overview/images/avatar/unassigned.png');
				$data['series_data'][] = array( 'name'=>'Một ai đó', 'y'=> (int)$item->term_count);
				continue;
			}

			$email = $this->admin_m->get_field_by_id($item->user_id,'user_email');

			$data['series_data'][] = array( 'name'=>$email, 'y'=> (int)$item->term_count);
			$email = substr($email,0,strpos($email, '@'));

			$img_src = '/template/frontend/tv_overview/images/avatar/'.$email.'.png';
			if(!file_exists('.'.$img_src))
			{
				$img_src = '/template/frontend/tv_overview/images/avatar/unassigned.png';
			}

			$data['images'][] = base_url($img_src);

		}

		$data['series_data'] = json_encode($data['series_data']);
		$data['images'] = json_encode($data['images']);
		$data['next_url'] = base_url('tv_overview/top_staffs_active');

		$this->load->view('statistical/top_staffs',$data);
	}

	public function top_staffs_active()
	{
		$data = array();
		$staff_map = array();
		$term_users = $this->googleads_m
		->select('user_id,term.term_id')
		->join('googleads_kpi','googleads_kpi.term_id = term.term_id')
		->set_term_type()
		->where_in('term.term_status',array('pending','publish','ending','liquidation'))
		->as_array()
		->get_many_by();

		if(!empty($term_users))
		{
			$startOfMonth = $this->mdate->startOfMonth();
			foreach ($term_users as $row) 
			{

				$term_id = $row['term_id'];
				$activated_until_time = (int)get_term_meta_value($term_id,'activated_until_time');
				if($activated_until_time < $startOfMonth)
					continue;

				
				$mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
				if(empty($mcm_account_id)) 
					continue;

				$user_id = $row['user_id'];
				$staff_map[$user_id] = $staff_map[$user_id]??array();
				if(empty($staff_map[$user_id]))
				{
					$staff_map[$user_id] = array($mcm_account_id);
					continue;
				}

				if(in_array($mcm_account_id, $staff_map[$user_id]))
					continue;

				$staff_map[$user_id][] = $mcm_account_id;
			}
		}

		if(!empty($staff_map))
		{
			foreach ($staff_map as $key => $row) 
			{
				$staff_map[$key] = count($row);
			}
			arsort($staff_map);
		}

		$data['images'] = array();
		$data['series_data'] = array();
		foreach ($staff_map as $user_id => $value) 
		{	
			$email = $this->admin_m->get_field_by_id($user_id,'user_email');
			$data['series_data'][] = array( 'name'=>$email, 'y'=> (int)$value);

			$email = substr($email,0,strpos($email, '@'));
			$img_src = '/template/frontend/tv_overview/images/avatar/'.$email.'.png';
			if(!file_exists('.'.$img_src))
			{
				$img_src = '/template/frontend/tv_overview/images/avatar/unassigned.png';
			}

			$data['images'][] = base_url($img_src);
		}

		$data['series_data'] = json_encode($data['series_data']);
		$data['images'] = json_encode($data['images']);
		$data['next_url'] = base_url('tv_overview.html');

		$this->load->view('statistical/top_staffs_active',$data);
	}

	public function top_inefficient($refresh = FALSE)
	{
		$terms = $this->term_m
		->join('googleads_kpi','googleads_kpi.term_id = term.term_id','LEFT OUTER')
		->group_by('term.term_id')
		->where(array('term_status'=>'publish','term_type'=>'google-ads'))
		->get_many_by();

		if(empty($terms)) return FALSE;

		$this->load->model('contract/contract_m');
		$now = time();
		foreach ($terms as $key => $term)
		{
			$mcm_account_id = get_term_meta_value($term->term_id,'mcm_client_id');
			if(empty($mcm_account_id))
			{
				unset($terms[$key]);
				continue;
			}

			$begin_time = get_term_meta_value($term->term_id,'googleads-begin_time');
			if(empty($begin_time))
			{
				unset($terms[$key]);
				continue;
			}

			$start_service_time = (int) get_term_meta_value($term->term_id,'start_service_time');
			if(empty($start_service_time))
			{
				unset($terms[$key]);
				continue;
			}

			$start_service_time_valid = strtotime('+3 day',$start_service_time);
			$start_service_time_valid = $this->mdate->startOfDay($start_service_time_valid);
			if($start_service_time_valid > $now)
			{
				unset($terms[$key]);
				continue;
			}

			$end_time = get_term_meta_value($term->term_id,'googleads-end_time');
	    	$last_end_time = strtotime('+1 day',$end_time);
	    	
	    	if(empty($end_time) || $last_end_time > $now)
	    	{
	    		continue;
	    	}

	    	unset($terms[$key]);
		}

		if(empty($terms)) return FALSE;

		$this->load->model('base_adwords_m');
		$report_type = 'KEYWORDS_PERFORMANCE_REPORT';

		if($refresh)
		{	
			foreach ($terms as $term) 
			{
				$term_id = $term->term_id;
				$mcm_account_id = get_term_meta_value($term_id, 'mcm_client_id');
				$this->base_adwords_m->set_mcm_account($mcm_account_id);
				$this->base_adwords_m->download($report_type,strtotime('yesterday'));
			}
			redirect(base_url('tv_overview/top_inefficient'),'refresh');
		}

		$data = array();
		foreach ($terms as $term) 
		{
			$term_id = $term->term_id;
			$tech_name = $this->admin_m->get_field_by_id($term->user_id,'display_name');
			$tech_name = $tech_name?:$this->admin_m->get_field_by_id($term->user_id,'user_email');

			$mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
			if(empty($mcm_account_id)) continue;

			$this->base_adwords_m->set_mcm_account($mcm_account_id);
			$keywords = $this->base_adwords_m->get_cache($report_type,strtotime('yesterday'));

			$keywords = array_filter($keywords);
			if(empty($keywords)) continue;

			$keywords = reset($keywords);
			$count_keywords = count($keywords);
			$ctr_invalid = array_filter($keywords,function($x){ return ((double)$x['ctr'] <= 6);});
			$position_invalid = array_filter($keywords,function($x){ return ((double)$x['avgPosition'] > 4);});

			$score_invalid = array_filter($keywords,function($x){

				if(is_numeric($x['qualityScore']) || @$x['hasQualityScore'] == 'true')
				{
					return ((double)$x['qualityScore'] < 7);	
				}
				return FALSE;
			});
			$percent_score_invalid = (count($score_invalid)/$count_keywords)* 100;
			$count_score_invalid = count($score_invalid);
			if(empty($count_score_invalid)) 
				continue;
			
			$timeonsite_invalid = FALSE;
			$bounceRate_invalid = FALSE;
			$sum_pagesSession = array_sum(array_column($keywords, 'pagesSession'));
			$keywords_has_clicks = array_filter($keywords,function($x){ return (int) $x['clicks'] > 0;});
			if(!empty($sum_pagesSession) && !empty($keywords_has_clicks))
			{	
				$timeonsite_invalid = array_filter($keywords_has_clicks,function($x){return ((double)$x['avgSessionDurationSeconds'] < 30);});
				$bounceRate_invalid = array_filter($keywords_has_clicks,function($x){ return ((double)$x['bounceRate'] >= 90);});
			}
			$timeonsite_invalid_label = !$timeonsite_invalid ? '---' : count($timeonsite_invalid).'/'.count($keywords_has_clicks);
			$bounceRate_invalid_label = !$bounceRate_invalid ? '---' : count($bounceRate_invalid).'/'.count($keywords_has_clicks);

			$data[$term_id] = array(
				'term_id' => $term_id,
				'tech_name' => $tech_name,
				'term_name' => $term->term_name,
				'keywords' => $count_keywords,
				'ctr_invalid' => count($ctr_invalid),
				'position_invalid' => count($position_invalid),
				'score_invalid' => $count_score_invalid,
				'percent_score_invalid' => numberformat($percent_score_invalid, 2),
				'timeonsite_invalid' => $timeonsite_invalid_label,
				'bounceRate_invalid' => $bounceRate_invalid_label
				);

		}

		usort($data, function($a,$b){
			if($a['percent_score_invalid'] == $b['percent_score_invalid'])
				return ($a['keywords'] < $b['keywords'] ? 1 : -1); 
			return ($a['percent_score_invalid'] < $b['percent_score_invalid'] ? 1 : -1); 
		});

		$rank_data = array();

		$template = array('table_open' => '<table class="table table-inverse wow" border="0" cellpadding="4" cellspacing="0">');
		$this->table->set_template($template);
		$headings = array('# Rank','Phụ trách','Website','Total Keyword','CTR(<=6%)','Avg. Pos(>4)','Qual. score(<7)','% Qual. score(<7)','T.o.s < 30s','Bounce >= 90%');
		$this->table->set_heading($headings);

		$i = 0;
		foreach ($data as $item) 
		{
			$dat = array(
				++$i,
				$item['tech_name'],
				$item['term_name'],
				$item['keywords'],
				$item['ctr_invalid'],
				$item['position_invalid'],
				$item['score_invalid'],
				$item['percent_score_invalid'].' %',
				$item['timeonsite_invalid'] == $item['keywords'] ? '-' : $item['timeonsite_invalid'],
				$item['bounceRate_invalid']?:'-'
			);

			$this->table->add_row($dat);

			if($i == 10) break;
		}
		$data['content'] = $this->table->generate();
		$data['next_url'] = base_url('tv_overview/top_staffs');
		$this->load->view('statistical/top_inefficient',$data);
	}
}