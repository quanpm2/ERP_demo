<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Google\AdsApi\AdWords\v201809\cm\ReportDefinitionReportType;

class Dataset extends Admin_Controller {

	public $model = 'googleads_m';

	public $term_type = 'google-ads';

	public $website_id = 0;

	public $term = FALSE;

	function __construct()
	{
		parent::__construct();

		$models = array(
			'googleads_m',
			'googleads_kpi_m',
			'contract/contract_m',
			'googleads_log_m',
			'googleads_report_m',
			'history_m',
			'base_adwords_m',
			'googleads/mcm_account_m');

		$this->load->model($models);
		$this->load->config('googleads');
		$this->load->config('contract');
		$this->load->config('group');
	}

	public function index()
	{	
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
		$permission = 'googleads.index.access';

		if( ! has_permission($permission))
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		$defaults	= [
			'offset' 			=> 0,
			'per_page' 			=> 10,
			'cur_page' 			=> 1,
			'is_filtering' 		=> TRUE,
			'is_ordering' 		=> TRUE,
			'filter_position' 	=> FILTER_TOP_OUTTER
		];

		$args 		= wp_parse_args( $this->input->get(), $defaults);

		$this->load->library('datatable_builder');

		$relate_users 		= $this->admin_m->get_all_by_permissions($permission);
		// LOGGED USER NOT HAS PERMISSION TO ACCESS
		if($relate_users === FALSE)
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		if(is_array($relate_users))
		{
			$this->datatable_builder->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
		}

		$mcm_accounts = array();
		if( $_mcm_accounts = $this->mcm_account_m->select('term.term_id,term.term_name')->get_many_by(['term_status'=>'active','term_type'=>$this->mcm_account_m->term_type])) {
			$mcm_accounts = array_column($_mcm_accounts, 'term_name', 'term_id');
		}

        $service_fee_payment_types = $this->config->item('service_fee_payment_type');

		/* Applies get query params for filter */
		$this->search_filter();
		$this->template->title->append('Tổng quan dịch vụ đang thực hiện ADSPLUS');
		$this->datatable_builder
		->set_filter_position($args['filter_position'])
		->add_search('term.term_id',['placeholder'=>'#ID'])
		->add_search('customer_code')
		->add_search('contract_code')
		->add_search('term_status',array('content'=> form_dropdown(array('name'=>'where[term_status]','class'=>'form-control select2'),prepare_dropdown($this->config->item('status','googleads'),'Tất cả'),$this->input->get('where[term_status]'))))
		->add_search('service_fee_payment_type',array('content'=> form_dropdown(array('name'=>'where[service_fee_payment_type]','class'=>'form-control select2'),prepare_dropdown($service_fee_payment_types,'Tất cả'),$this->input->get('where[service_fee_payment_type]'))))
		->add_search('staff_advertise')
		->add_search('staff_business')
        
        ->add_search('adaccount_status', array('content' => form_dropdown([ 'name'  =>'where[adaccount_status]', 'class' =>'form-control select2'],
            prepare_dropdown($this->config->item('states', 'adaccount_status'), 'Trạng thái : All'),
            $this->input->get('where[adaccount_status]'))))

		->add_column('contract_code', array('set_select'=>FALSE, 'title'=>'Mã HĐ'))
		->add_column('customer_code', array('set_select'=>FALSE,'title'=>'MSKH'))

		->add_column('service_fee_rate_actual', array('set_select' => FALSE, 'title'=>'% Phí DV thực tế', 'set_order'=> FALSE))
		->add_column('actual_result', array('set_select'=>FALSE,'title'=>'Đã dùng'))

		->add_column('payment_percentage', array('set_select'=>FALSE,'title'=>'Ngân sách<br><small><u><i>Đã thu</i></u></small>'))
		->add_column('payment_expected_end_time', array('set_select'=> FALSE, 'title'=> 'Dự kiến<br><small><u><i>(NS thực)</i></u></small>'))
		->add_column('payment_real_progress', array('set_select'=> FALSE, 'title'=> 'Tiến độ<br/><small><u><i>(NS thực)</i></u></small>'))
		->add_column('payment_cost_per_day_left', array('set_select'=> FALSE, 'title'=> 'est.NS/Ngày <br><small><u><i>(NS thực)</i></u></small>', 'set_order'=> FALSE))
		->add_column('actual_progress_percent_net', array('set_select' => FALSE,'title' => 'H.Thành (%)<br><small><u><i>(NS thực)</i></u></small>'))

		->add_column('contract_budget', array('set_select'=>FALSE,'title'=>'Ngân sách<br><small><u><i>(NS HĐ)</i></u></small>'))
		->add_column('expected_end_time', array('set_select'=> FALSE, 'title'=> 'Dự kiến<br><small><u><i>(NS HĐ)</i></u></small>'))
		->add_column('real_progress', array('set_select'=> FALSE, 'title'=> 'Tiến độ<br/><small><u><i>(NS HĐ)</i></u></small>'))
		->add_column('cost_per_day_left', array('set_select'=> FALSE, 'title'=> 'est.NS/ngày<br><small><u><i>(NS HĐ)</i></u></small>', 'set_order'=> FALSE))
		->add_column('actual_progress_percent', array('set_select' => FALSE,'title' => 'H.Thành (%)<br><small><u><i>(NS HĐ)</i></u></small>'))

		->add_column('result_updated_on', array('set_select'=>FALSE,'title'=>'Cập nhật lúc'))
		->add_column('time_next_api_check', array('set_select'=>FALSE,'title'=>'Đồng bộ kế tiếp'))

		->add_column('service_type', array('set_select'=> FALSE,'title'=>'Loại'))
		->add_column('staff_advertise', array('set_select' => FALSE,'title' => 'Kỹ thuật'))
		->add_column('staff_business', array('set_select' => FALSE,'title' => 'NVKD'))
        
        ->add_column('service_fee_payment_type', array('set_select'=>FALSE,'title'=>'PTTT.PDV'))
        ->add_column('term.term_status','T.Thái')
		
		->add_column('adaccount_status', array('set_select' => FALSE, 'title'=> 'TT AdAccounts (GG)', 'set_order'=> TRUE))

		// ->add_column('cid', array('set_select'=> FALSE, 'title'=> 'CID', 'set_order'=>FALSE))

		->add_column_callback('term_id',function($data,$row_name) use ($service_fee_payment_types){ 

			$term_id = $data['term_id'];

			$contract_m = (new contract_m())->set_contract((object) $data);
			$contract_budget = $contract_m->get_behaviour_m()->calc_budget();
			$data['contract_budget'] = currency_numberformat($contract_budget, '');

			$vat = div((double) get_term_meta_value($term_id,'vat'),100);
			if($vat > 0) $data['contract_budget'] = "<b>{$data['contract_budget']}</b>";
			$fct = (float) get_term_meta_value($term_id, 'fct');
			if($fct > 0) $data['contract_budget'] = "<u>{$data['contract_budget']}</u>";

			// CALLBACK SERVICE TYPES
			$default_services = $this->config->item('report_type','googleads');
			$service_type = get_term_meta_value($data['term_id'],'service_type') ?: 'cpc_type';
			$data['service_type'] = $default_services[$service_type];
			if($service_type == 'cpc_type')
			{
				// If the term belongs CPC , then indicates that CPC type [Search|GDN|Search+GDN]
				$data['service_type'].= br();

				// If the term belongs CPC with Search Network
				$contract_clicks = (int) get_term_meta_value($data['term_id'],'contract_clicks');
				if(!empty($contract_clicks))
				{
					$data['service_type'].= '<span class="label label-primary">Search</span> ';
				}

				// If the term belongs CPC with GDN
				$gdn_contract_clicks = (int) get_term_meta_value($data['term_id'],'gdn_contract_clicks');	
				if(!empty($gdn_contract_clicks))
				{
					$data['service_type'].= '<span class="label label-success">GDN</span> ';
				}
			}

			// CALLBACK : WEBSITE TERM_NAME
			// $term_name = $data['term_name'];
			$contract_code = get_term_meta_value($data['term_id'], 'contract_code');
			$short_name = mb_strlen($contract_code) > 60 ? (mb_substr($contract_code, 0,60).'...') : $contract_code;
			$data['contract_code'] = anchor(module_url("overview/{$data['term_id']}"), '<i class="fa fw fa-external-link-square"></i>'.nbs(2).($short_name?:'<code>Không tồn tại</emtpy>'));

            $data['service_fee_payment_type'] = $service_fee_payment_types[get_term_meta_value($data['term_id'], 'service_fee_payment_type')] ?? '--';

			/* Icon Trạng thái */
			switch ($data['term_status'])
			{
				case 'publish':

					if( ! is_service_proc($term_id) || !get_term_meta_value($term_id,'googleads-begin_time'))
					{
						$data['contract_code'].= '<i class="fa fa-circle-o text-red pull-right" data-toggle="tooltip" data-placement="top" title="Trạng thái : Chưa cấu hình"></i>';
						break;
					}

					$data['contract_code'].= '<i class="fa fa-circle-o text-green pull-right" data-toggle="tooltip" data-placement="top" title="Trạng thái : Đang hoạt động"></i>';

					break;

				case 'pending':
					$data['contract_code'].= '<i class="fa fa-circle-o pull-right" data-toggle="tooltip" data-placement="top" title="Trạng thái : Ngưng hoạt động"></i>';
					break;
				
				default:
					# code...
					break;
			}
			/* end Icon Trạng thái*/

			$start_service_time = get_term_meta_value($term_id,'start_service_time');
			if(!empty($start_service_time))
			{
				$start_time = start_of_day(get_term_meta_value($term_id,'googleads-begin_time'));
				$start_date = my_date($start_time,'d/m/Y');
				$contract_days = $this->base_adwords_m->calc_contract_days($term_id);

				$end_time = strtotime("+{$contract_days} days -1 day",$start_time);
				$end_date = my_date($end_time,'d/m/Y');

				$data['contract_code'].= "<br/><span style='font-size:0.8em'><b>{$start_date}</b> - <b>{$end_date}</b></span>";		
			}

			$result_updated_on = (int) get_term_meta_value($term_id, 'result_updated_on');
			$data['result_updated_on'] = $result_updated_on ? '<i>'.my_date($result_updated_on, 'd/m H:i:s').'</i>' : '--';

			$time_next_api_check = (int) get_term_meta_value($term_id, 'time_next_api_check');
			$data['time_next_api_check'] = $time_next_api_check ? '<i>'.my_date($time_next_api_check, 'd/m H:i:s').'</i>' : '--';

			// CALLBACK : TERM_STATUS
			$_status = $this->config->item('status','googleads');
			switch ($data['term_status'])
			{	
				case 'publish':
					$data['term_status'] = '<i class="fa fa-toggle-on text-green" data-toggle="tooltip" data-placement="top" title="Trạng thái : Đang hoạt động"></i>';
					break;
				
				default:
					$data['term_status'] = '<i class="fa fa-toggle-off" data-toggle="tooltip" data-placement="top" title="Trạng thái : Ngưng hoạt động"></i>';
					break;
			}

			

			// CALLBACK : PAYMENT PERCENTAGE

			$contract_budget 	= (double) get_term_meta_value($term_id,'contract_budget');

			$service_fee 		= (double) get_term_meta_value($term_id,'service_fee');
			$payment_amount 	= (double) get_term_meta_value($term_id,'payment_amount');
				$actual_budget 		= (int) get_term_meta_value($term_id, 'actual_budget');

			$payment_percentage = 100 * (double) get_term_meta_value($term_id,'payment_percentage');

			$payment_percentage_text = currency_numberformat($payment_percentage,'%');
			if('customer' == get_term_meta_value($term_id, 'contract_budget_payment_type'))
			{
				$payment_percentage_text = "Khách tự thanh toán ({$payment_percentage_text})";
			}
			

			if($payment_percentage >= 50 && $payment_percentage < 80) $text_color = 'text-yellow';
			else if($payment_percentage >= 80 && $payment_percentage < 90) $text_color = 'text-light-blue';
			else if($payment_percentage >= 90) $text_color = 'text-green';
			else $text_color = 'text-red';

			$payment_percentage = currency_numberformat($payment_percentage,' %');
			$data['payment_percentage'] = '--';
			if($actual_budget > 0)
			{
				$data['payment_percentage'] = 
				"<b data-toggle='tooltip' data-original-title='{$payment_percentage_text}' class='{$text_color}'>". currency_numberformat($actual_budget,'').'</b>';
			}

			if('customer' == get_term_meta_value($term_id, 'contract_budget_payment_type'))
			{
				$data['payment_percentage'] = "<strike>{$data['payment_percentage']}</strike>";
				if('behalf' == get_term_meta_value($term_id, 'contract_budget_customer_payment_type'))
					$data['payment_percentage'] = '<span class="fa fa-credit-card"></span> &nbsp'.$data['payment_percentage'];
			}

			$vat = (int) get_term_meta_value($data['term_id'], 'vat');
			switch (get_term_meta_value($data['term_id'], 'contract_budget_payment_type'))
			{
				case 'customer':
					$data['payment_percentage'].= (empty($vat) ? '<small class="pull-right">(đ)</small>' : '');
					break;
				
				default:
					$data['payment_percentage'].= (empty($vat) ? '<small class="pull-right">(đ)</small>' : '&nbsp<small class="pull-right">($)</small>');
					break;
			}

			/* Tình trạng bảo lãnh hiện tại của hợp đồng */
			$bailment_amount = (int) get_term_meta_value($term_id,'bailment_amount');
			$bailment_end_date = get_term_meta_value($term_id,'bailment_end_date');
			if($bailment_amount > 0)
			{
				$bailment_class = $bailment_end_date < time() ? 'text-red' : 'text-yellow';
				$bailment_decr = $bailment_end_date < time() ? 'Nợ ' : 'Bảo lãnh';

				$data['payment_percentage'].= '<p class="'.$bailment_class.'"><small>'.$bailment_decr.' '.currency_numberformat($bailment_amount,'').'</small></p>';
			}

			/* Ngân sách đã sử dụng */
			$actual_result = (double) $contract_m->get_behaviour_m()->get_actual_result();
			$balance_spend = (double) get_term_meta_value($data['term_id'], 'balance_spend');
			empty($balance_spend) OR $actual_result+= $balance_spend;

			$data['actual_result'] = $actual_result > 0 ? currency_numberformat($actual_result, '') : '--';

			// CALLBACK : TECH USER
			$term_id = $data['term_id'];
			$service_type = get_term_meta_value($term_id,'service_type');

			$kpis = $this->googleads_kpi_m->get_kpis($term_id,'users',$service_type);
			if(empty($kpis)) return $data;

			$staff_advertise = '';
			$user_ids = array();

			foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;

			$data['staff_advertise'] = implode(br(), array_map(function($user_id){

				$email = $this->admin_m->get_field_by_id($user_id,'user_email');
				$email_parts = explode('@', $email);
				return reset($email_parts);

			}, $user_ids));


			// CALLBACK : SALE USER
			$sale_id = get_term_meta_value($term_id, 'staff_business');
			$email = $this->admin_m->get_field_by_id($sale_id,'user_email');
			$email_parts = explode('@', $email);
			$data['staff_business'] = reset($email_parts) ?: '--';

			return $data;
		},FALSE)

		->add_column_callback('customer_code', function($data, $row_name){

			$term_id = $data['term_id'];
			$customer = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company']);
			$customer AND $customer = reset($customer);
			$data['customer_code'] = $customer ? cid($customer->user_id, $customer->user_type) : '--';
			return $data;

		},FALSE)

        ->add_column_callback('original_service_fee_rate', function($data, $row_name){

			$term_id = $data['term_id'];
			$_original_service_fee_rate = ((double) get_term_meta_value($term_id, 'original_service_fee_rate'))*100;
			$data['original_service_fee_rate_raw'] = $_original_service_fee_rate;
			$data['original_service_fee_rate'] = ((int) $_original_service_fee_rate ?: '--') . '%';
			return $data;

		},FALSE)

		->add_column_callback('service_fee_rate_actual', function($data, $row_name){

            $term_id = $data['term_id'];
                
            $contract_m = (new googleads_m())->set_contract($term_id);

            $budget         = $contract_m->get_behaviour_m()->calc_budget();
            $service_fee    = (int) get_term_meta_value($term_id, 'service_fee');
            $discount_amount = $contract_m->get_behaviour_m()->calc_disacount_amount();

            $service_fee_rate_actual = div(max($service_fee - $discount_amount, 0), $budget);
            $data['service_fee_rate_actual_raw'] = $service_fee_rate_actual * 100;
            $data['service_fee_rate_actual'] = round($data['service_fee_rate_actual_raw'], 2) . '%';

			return $data;

		},FALSE)	

		->add_column_callback('actual_progress_percent_net', function($data, $row_name){

			$term_id = $data['term_id'];
			$data['actual_progress_percent_net'] = '--';

			$value = (float) get_term_meta_value($term_id, 'actual_progress_percent_net')*100;
			$class = $value < 50 ? 'text-aqua' : ($value < 80 ? 'text-yellow' : ($value < 90 ? 'text-green' : 'text-red'));
			$data['actual_progress_percent_net'] = "<p class='{$class}'><b>".currency_numberformat($value,'%',2).'</b></p>';
			return $data;

		},FALSE)

		->add_column_callback('actual_progress_percent', function($data, $row_name){

			$term_id = $data['term_id'];
			$data['actual_progress_percent'] = '--';

			$value = (float) get_term_meta_value($term_id, 'actual_progress_percent');
			$class = $value < 50 ? 'text-red' : ($value < 80 ? 'text-yellow' : ($value < 90 ? 'text-green' : 'text-aqua'));
			$data['actual_progress_percent'] = "<p class='{$class}'><b>".currency_numberformat($value,'%',2).'</b></p>';

			return $data;
		},FALSE)

		->add_column_callback('real_progress',function($data,$row_name){

			$data['real_progress'] = '--';

			$term_id 		= $data['term_id'];
			$real_progress 	= get_term_meta_value($term_id,'real_progress');
			if(empty($real_progress)) return $data;

			$data['real_progress'] = "{$real_progress}%";			

			try
			{
				$_contract = (new contract_m())->set_contract((object) $data);

				$expected_end_time 	= $_contract->get_behaviour_m()->calc_expected_end_time();
		        $contract_days 		= $_contract->get_behaviour_m()->calc_contract_days();
		        
		        $start_time 		= start_of_day(get_term_meta_value($term_id, 'googleads-begin_time'));
		        $commit_end_time 	= strtotime("+{$contract_days} days -1 day",$start_time);
		        $balance_days 		= round(div($expected_end_time-$commit_end_time,86400));

		        $class = ($real_progress < 100) ? 'text-red' : 'text-green';
		        $title = $balance_days > 0 ? "Chậm tiến độ {$balance_days} ngày" : "Vượt tiến độ {$balance_days} ngày";

		        $data['real_progress'] = "<span class='progress-text {$class}' data-toggle='tooltip' data-original-title='{$title}'><b>{$real_progress}%</b></span>";

			} 
			catch (Exception $e) { return $data; }

			return $data;

		},FALSE)

		->add_column_callback('payment_real_progress',function($data,$row_name){

			$data['payment_real_progress'] 	= '--';
			$term_id 				= $data['term_id'];
			$payment_real_progress 	= get_term_meta_value($term_id, 'payment_real_progress');
			if(empty($payment_real_progress)) return $data;

			$data['payment_real_progress'] = "{$payment_real_progress}%";

			try
			{
				$_contract = (new contract_m())->set_contract((object) $data);

				$expected_end_time 	= $_contract->get_behaviour_m()->calc_payment_expected_end_time();
		        $contract_days 		= $_contract->get_behaviour_m()->calc_contract_days();
		        
		        $start_time 		= start_of_day(get_term_meta_value($term_id, 'googleads-begin_time'));
		        $commit_end_time 	= strtotime("+{$contract_days} days -1 day",$start_time);
		        $balance_days 		= round(div($expected_end_time-$commit_end_time,86400));

		        $class = ($payment_real_progress < 100) ? 'text-red' : 'text-green';
		        $title = $balance_days > 0 ? "Chậm tiến độ {$balance_days} ngày" : "Vượt tiến độ {$balance_days} ngày";

		        $data['payment_real_progress'] = "<span class='progress-text {$class}' data-toggle='tooltip' data-original-title='{$title}'><b>{$payment_real_progress}%</b></span>";

			} 
			catch (Exception $e) { return $data; }

			return $data;

		},FALSE)

		->add_column_callback('cost_per_day_left',function($data,$row_name){
				try
				{
					$_contract = (new contract_m())->set_contract((object) $data);

					$cost_per_day_left 			= $_contract->get_behaviour_m()->calc_cost_per_day_left('commit');
					$data['cost_per_day_left']	= currency_numberformat($cost_per_day_left, '');
				} 
				catch (Exception $e) { $data['cost_per_day_left'] = '--'; }

				return $data;

		},FALSE)

		->add_column_callback('payment_expected_end_time',function($data,$row_name){
				try
				{
					$_contract = (new contract_m())->set_contract((object) $data);
					$payment_expected_end_time = $_contract->get_behaviour_m()->calc_payment_expected_end_time();
					$data['payment_expected_end_time'] = my_date($payment_expected_end_time, 'd/m/Y');
				} 
				catch (Exception $e) { $data['payment_expected_end_time'] = '--'; }

				return $data;

		},FALSE)

		->add_column_callback('expected_end_time',function($data,$row_name){
				try
				{
					$_contract = (new contract_m())->set_contract((object) $data);
					$expected_end_time = $_contract->get_behaviour_m()->calc_expected_end_time();
					$data['expected_end_time'] = my_date($expected_end_time, 'd/m/Y');
				} 
				catch (Exception $e) { $data['expected_end_time'] = '--'; }

				return $data;

		},FALSE)

		->add_column_callback('payment_cost_per_day_left',function($data,$row_name){
				try
				{
					$_contract = (new contract_m())->set_contract((object) $data);
					$payment_cost_per_day_left 	= $_contract->get_behaviour_m()->calc_cost_per_day_left('payment');
					$data['payment_cost_per_day_left']	= currency_numberformat($payment_cost_per_day_left, '');
				} 
				catch (Exception $e) { $data['cost_per_day_left'] = '--'; }
				return $data;

		},FALSE)

		->add_column_callback('cid',function($data,$row_name) use ($mcm_accounts) {

				$term_id 		= $data['term_id'];

				$mcm_client_id 	= get_term_meta_value($term_id,'mcm_client_id');
				if(empty($mcm_accounts[$mcm_client_id])) 
				{
					$data['cid'] = anchor(base_url("admin/googleads/setting/{$term_id}"), '<span class="btn btn-danger btn-xs"><i class="fa fw fa-cog"></i>&nbsp;&nbsp;Cấu hình CID</span>', ['target' => "_blank"]);
					return $data;
				}

				$data['cid'] = anchor('https://ads.google.com/um/signin', "<span class='btn btn-primary btn-xs'><i class='fa fw fa-external-link-square'></i>&nbsp;&nbsp;{$mcm_accounts[$mcm_client_id]}</span>", ['target' => "_blank"]);

				return $data;
		},FALSE)


		->add_column_callback('adaccount_status',function($data,$row_name) {

			if( ! empty($data['adaccount_status'])) return $data;

            $term_id            = $data['term_id'];
            $states             = $this->config->item('states', 'adaccount_status');
            $adaccount_status   = get_term_meta_value($term_id, 'adaccount_status') ?: $this->config->item('default', 'adaccount_status');
            $text               = $states[$adaccount_status] ?? 'UNKNOWN';

            $data['adaccount_status']       = $adaccount_status;
            $data['adaccount_status_raw']   = $text;

            if( 'UNSPECIFIED' == $adaccount_status)
            {
                $data['adaccount_status'] = anchor(base_url("admin/googleads/setting/{$term_id}"), '<span class="btn btn-danger btn-xs"><i class="fa fw fa-times-circle-o"></i>&nbsp;&nbsp;'.$text.'</span>', ['target' => "_blank"]);
                return $data;
            }

            if( 'PENDING_APPROVAL' == $adaccount_status)
            {
                $data['adaccount_status'] = anchor(base_url("admin/googleads/setting/{$term_id}"), '<span class="btn btn-primary btn-xs"><i class="fa fw fa-cog"></i>&nbsp;&nbsp;'.$text.'</span>', ['target' => "_blank"]);
                return $data;
            }

            if( 'APPROVED' == $adaccount_status)
            {
                $data['adaccount_status'] = anchor(base_url("admin/googleads/setting/{$term_id}"), '<span class="btn btn-success btn-xs"><i class="fa fw  fa-check-circle-o"></i>&nbsp;&nbsp;'.$text.'</span>', ['target' => "_blank"]);
                return $data;
            }

            if( 'REMOVED' == $adaccount_status)
            {
                $data['adaccount_status'] = anchor(base_url("admin/googleads/setting/{$term_id}"), '<span class="btn btn-warning btn-xs"><i class="fa fw fa-cog"></i>&nbsp;&nbsp;'.$text.'</span>', ['target' => "_blank"]);
                return $data;
            }

            $data['adaccount_status'] = anchor(base_url("admin/googleads/setting/{$term_id}"), '<span class="btn btn-default btn-xs"><i class="fa fw fa-times-circle-o"></i>&nbsp;&nbsp;Unknown</span>', ['target' => "_blank"]);

            return $data;
		},FALSE)

        ->select('term.term_id, term.term_type, term.term_name')
		->from('term')
		->join('termmeta started_service', 'started_service.term_id = term.term_id', 'INNER')
		->where('term.term_type', $this->term_type)
		->where_in('term.term_status', array_keys($this->config->item('status','googleads')))
		->where('started_service.meta_key ','started_service')
		->group_by('term.term_id');

		$data = $this->datatable_builder->generate(array('per_page'=>$args['per_page'],'cur_page'=>$args['cur_page']));
		
		// OUTPUT : DOWNLOAD XLSX
		if( ! empty($args['download']) 
			&& $last_query = $this->datatable_builder->last_query())
		{
			$this->export($last_query);
			return TRUE;
		}

		return parent::renderJson($data);
	}

	/**
	 * Export Excel Dataset
	 *
	 * @param      string  $query  The query
	 */
	public function export($query = '')
	{
		if(empty($query)) return FALSE;

		// remove limit in query string
		$query = explode('LIMIT', $query);
		$query = reset($query);
		
		$terms = $this->contract_m->query($query)->result();

		if( ! $terms) return FALSE;

		$this->config->load('contract/fields');
		$default 	= array('columns' => implode(',', $this->config->item('default_columns', 'datasource')));
		$args 		= wp_parse_args( $this->input->get(), $default);
		$args['columns'] = explode(',', $args['columns']);

		$this->load->library('excel');
		$cacheMethod 	= PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings 	= array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("WEBDOCTOR.VN")->setLastModifiedBy("WEBDOCTOR.VN")->setTitle(uniqid('Danh sách hợp đồng __'));
        $objPHPExcel->setActiveSheetIndex(0);

        $objWorksheet 	= $objPHPExcel->getActiveSheet();
        $callbacks 		= $this->googleads_m->get_field_callback();
		$field_config 	= $this->googleads_m->get_field_config();

		$args['columns'] = array(
            'contract_code',
            'customer_code',
			'website',
			'contract_budget_payment_type',
			'contract_budget_customer_payment_type',
            'service_fee_payment_type',
			'actual_result',

			'actual_budget',
			'payment_expected_end_time',
			'payment_real_progress',
			'payment_cost_per_day_left',
			'actual_progress_percent_net',

			'contract_budget',
			'expected_end_time',
			'real_progress',
			'cost_per_day_left',
			'actual_progress_percent',

			'staff_business',
			'staff_advertise',

			'start_service_time',
			'end_service_time',
			'status',

			'adaccount_status',
		);

        $headings = array_map(function($x) use($field_config){
        	return $field_config['columns'][$x]['title']??$x;
        }, $args['columns']);

        $objWorksheet->fromArray($headings, NULL, 'A1');
        $row_index = 2;
        foreach ($terms as $key => &$term)
		{
			$term = (array) $term;
			foreach ($args['columns'] as $column)
			{
				if(empty($field_config['columns'][$column])) continue;
				if(empty($callbacks[$column])) continue;

				$term = call_user_func_array($callbacks[$column]['func'], array($term, $column, []));
			}

			$i = 0;
			$row_number = $row_index + $key;

			foreach ($args['columns'] as $column)
			{
				if(empty($field_config['columns'][$column])) continue;

				$value = $term["{$column}_raw"] ?? ($term["{$column}"] ?? '');
				$col_number = $i;
				$i++;

				switch ($field_config['columns'][$column]['type'])
				{
					case 'string' : 
						$objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(str_repeat('0', strlen($value)));
						break;

					case 'number': 
						$objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode("#,##0");
						break;

					case 'timestamp': 
						$value = PHPExcel_Shared_Date::PHPToExcel($value);
						$objWorksheet->getStyleByColumnAndRow($col_number, $row_number)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
						break;

					default: break;
				}

				$objWorksheet->setCellValueByColumnAndRow($col_number, $row_number, $value);
			}
		}

		$num_rows = count($terms);
		
        // We'll be outputting an excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $folder_upload = "files/contracts/";
        if(!is_dir($folder_upload))
        {
            try 
            {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $created_datetime 	= my_date(time(),'Y-m-d-H-i-s');
        $file_name 			= "{$folder_upload}danh-sach-hop-dong-google-ads-{$created_datetime}.xlsx";

       	try { $objWriter->save($file_name); }
        catch (Exception $e) { trigger_error($e->getMessage()); return FALSE;  }

		$this->load->helper('download');
		force_download($file_name, NULL);
		return TRUE;
	}

	public function staffs()
	{
		return $this->index();
	}

	protected function search_filter($search_args = array())
	{	
		$args = $this->input->get();

		if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

		// Contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if($filter_contract_code || $sort_contract_code)
        {
            $alias = uniqid('contract_code_');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

            if($filter_contract_code)
            {   
                $this->datatable_builder->like("{$alias}.meta_value",$filter_contract_code);
            }

            if($sort_contract_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value",$sort_contract_code);
            }

            unset($args['where']['contract_code'],$args['order_by']['contract_code']);
        }

		if( ! empty($args['where']['started_service']))
		{
			$explode = explode(' - ', $args['where']['started_service']);
			if( ! empty($explode))
			{
				$this->datatable_builder
				->join('termmeta AS tm_stated_service', 'tm_stated_service.term_id = term.term_id', 'LEFT')
				->where('tm_stated_service.meta_key','started_service')
				->where('tm_stated_service.meta_value >=', start_of_day(reset($explode)))
				->where('tm_stated_service.meta_value <=', end_of_day(end($explode)));
			}
			unset($args['where']['started_service']);
		}

		if( ! empty($args['order_by']['started_service']))
		{
			$this->datatable_builder->join('termmeta AS tm_stated_service', 'tm_stated_service.term_id = term.term_id', 'LEFT');
			$args['where']['tm_stated_service.meta_key'] = 'started_service';
			$args['order_by']['tm_stated_service.meta_value'] = $args['order_by']['started_service'];
			unset($args['order_by']['started_service']);
		}

		// service_type FILTERING & SORTING
		$filter_service_type 	= $args['where']['service_type'] ?? FALSE;
		$sort_service_type 		= $args['order_by']['service_type'] ?? FALSE;
		if($filter_service_type || $sort_service_type)
		{
			$alias = uniqid('service_type_');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'service_type'", 'LEFT')
			->join("user tblcustomer","{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

			if($filter_service_type)
			{	
				$this->datatable_builder->like("{$alias}.meta_value", $filter_service_type);
				unset($args['where']['service_type']);
			}

			if($sort_service_type)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value", $sort_service_type);
				unset($args['order_by']['service_type']);
			}
		}

		// customer_code FILTERING & SORTING
        $filter_customer_code = $args['where']['customer_code'] ?? FALSE;
        $sort_customer_code   = $args['order_by']['customer_code'] ?? FALSE;
        if($filter_customer_code || $sort_customer_code)
        {
            $alias = uniqid('customer_code_');
            $this->datatable_builder
            ->join("term_users tu_customer","tu_customer.term_id = term.term_id")
            ->join("user customer","customer.user_id = tu_customer.user_id AND customer.user_type IN ('customer_person', 'customer_company')")
            ->join("usermeta {$alias}","{$alias}.user_id = customer.user_id and {$alias}.meta_key = 'cid'", 'LEFT');

            if($filter_customer_code)
            {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_customer_code);
                unset($args['where']['customer_code']);
            }

            if($sort_customer_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_customer_code);
                unset($args['order_by']['customer_code']);
            }
        }
	
		// Staff_business FILTERING & SORTING
		$filter_staff_business = $args['where']['staff_business'] ?? FALSE;
		$sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
		if($filter_staff_business || $sort_staff_business)
		{
			$alias = uniqid('staff_business_');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT')
			->join("user tblcustomer","{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

			if($filter_staff_business)
			{	
				$this->datatable_builder->like("tblcustomer.user_email",$filter_staff_business);
				unset($args['where']['staff_business']);
			}

			if($sort_staff_business)
			{
				$this->datatable_builder->order_by('tblcustomer.user_email',$sort_staff_business);
				unset($args['order_by']['staff_business']);
			}
		}

		if(!empty($args['order_by']['actual_progress_percent_net']))
		{
			$alias = uniqid('actual_progress_percent_net');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_progress_percent'", 'LEFT OUTER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['actual_progress_percent_net'];
			unset($args['order_by']['actual_progress_percent_net']);
		}

		if(!empty($args['order_by']['actual_progress_percent']))
		{
			$alias = uniqid('actual_progress_percent');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_progress_percent'", 'LEFT OUTER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['actual_progress_percent'];
			unset($args['order_by']['actual_progress_percent']);
		}

		if(!empty($args['order_by']['service_fee_rate_actual']))
		{
			$alias = uniqid('service_fee_rate_actual');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'service_fee_rate_actual'", 'LEFT OUTER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['service_fee_rate_actual'];
			unset($args['order_by']['service_fee_rate_actual']);
		}


		if(!empty($args['order_by']['real_progress']))
		{
			$alias = uniqid('real_progress');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'real_progress'", 'LEFT OUTER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['real_progress'];
			unset($args['order_by']['real_progress']);
		}

		if(!empty($args['order_by']['payment_real_progress']))
		{
			$alias = uniqid('payment_real_progress');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_real_progress'", 'LEFT OUTER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_real_progress'];
			unset($args['order_by']['payment_real_progress']);
		}

		if(!empty($args['order_by']['result_updated_on']))
		{
			$alias = uniqid('result_updated_on');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'result_updated_on'", 'LEFT OUTER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['result_updated_on'];
			unset($args['order_by']['result_updated_on']);
		}

		if(!empty($args['order_by']['time_next_api_check']))
		{
			$alias = uniqid('time_next_api_check');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'time_next_api_check'", 'LEFT OUTER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['time_next_api_check'];
			unset($args['order_by']['time_next_api_check']);
		}

		if(!empty($args['order_by']['actual_result']))
		{
			$alias = uniqid('actual_result');
			$this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_result'", 'LEFT OUTER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['actual_result'];
			unset($args['order_by']['actual_result']);
		}

		// FIND AND SORT STAFF_ADVERTISE WITH FACEBOOK_KPI
		$filter_staff_advertise = $args['where']['staff_advertise'] ?? FALSE;
		$sort_staff_advertise = $args['order_by']['staff_advertise'] ?? FALSE;
		if($filter_staff_advertise || $sort_staff_advertise)
		{
			$this->datatable_builder->join('googleads_kpi','term.term_id = googleads_kpi.term_id', 'LEFT OUTER');
			$this->datatable_builder->join('user','user.user_id = googleads_kpi.user_id', 'LEFT OUTER');

			if($filter_staff_advertise)
			{	
				$this->datatable_builder->like('user.user_email',strtolower($filter_staff_advertise),'both');
			}

			if($sort_staff_advertise)
			{
				$this->datatable_builder->order_by('user.user_email',$sort_staff_advertise);
			}

			unset($args['where']['staff_advertise'],$args['order_by']['staff_advertise']);
		}

		if( ! empty($args['order_by']['payment_percentage']))
		{
			$alias = uniqid('payment_percentage');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_percentage'", 'INNER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_percentage'];
			unset($args['order_by']['payment_percentage']);
		}

		if( ! empty($args['order_by']['payment_percentage']))
		{
			$alias = uniqid('payment_percentage');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_percentage'", 'INNER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_percentage'];
			unset($args['order_by']['payment_percentage']);
		}

		if( ! empty($args['order_by']['expected_end_time']))
		{
			$alias = uniqid('expected_end_time');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'expected_end_time'", 'INNER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['expected_end_time'];
			unset($args['order_by']['expected_end_time']);
		}

		if( ! empty($args['order_by']['payment_expected_end_time']))
		{
			$alias = uniqid('payment_expected_end_time');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'payment_expected_end_time'", 'INNER');
			$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['payment_expected_end_time'];
			unset($args['order_by']['payment_expected_end_time']);
		}

        // FIND AND SORT service_fee_payment_type
		$filter_service_fee_payment_type = $args['where']['service_fee_payment_type'] ?? FALSE;
		$sort_service_fee_payment_type = $args['order_by']['service_fee_payment_type'] ?? FALSE;
		if($filter_service_fee_payment_type || $sort_service_fee_payment_type)
		{
            $alias = uniqid('service_fee_payment_type_');
			$this->datatable_builder
			->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'service_fee_payment_type'", 'LEFT');

			if($filter_service_fee_payment_type)
			{	
				$this->datatable_builder->where("{$alias}.meta_value", $filter_service_fee_payment_type);
				unset($args['where']['service_fee_payment_type']);
			}

			if($sort_service_fee_payment_type)
			{
				$this->datatable_builder->order_by("{$alias}.meta_value", $sort_service_fee_payment_type);
				unset($args['order_by']['service_fee_payment_type']);
			}
		}

        // adaccount_status FILTERING & SORTING
        $filter_adaccount_status = $args['where']['adaccount_status'] ?? FALSE;
        $sort_adaccount_status = $args['order_by']['adaccount_status'] ?? FALSE;
        if($filter_adaccount_status || $sort_adaccount_status)
        {
            $alias = uniqid('adaccount_status_');
            $this->datatable_builder
            ->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'adaccount_status'", 'LEFT');

            if($filter_adaccount_status)
            {   
                $this->datatable_builder->like("{$alias}.meta_value", $filter_adaccount_status);
                unset($args['where']['adaccount_status']);
            }

            if($sort_adaccount_status)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_adaccount_status);
                unset($args['order_by']['adaccount_status']);
            }
        }
		
		// APPLY DEFAULT FILTER BY MUTATE FIELD		
		$args = $this->datatable_builder->parse_relations_searches($args);
		if( ! empty($args['where']))
		{
			foreach ($args['where'] as $key => $value)
			{
				if(empty($value)) continue;

				if(empty($key))
				{
					$this->datatable_builder->add_filter($value, '');
					continue;
				}

				$this->datatable_builder->add_filter($key, $value);
			}
		}

		if(!empty($args['order_by'])) 
		{
			foreach ($args['order_by'] as $key => $value)
			{
				$this->datatable_builder->order_by($key, $value);
			}
		}
	}
}
/* End of file Googleads.php */
/* Location: ./application/modules/googleads/controllers/Googleads.php */