<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tracking extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct() ;
		$models = array(
		);

		$this->load->model($models);

		// if(ENVIRONMENT != 'production') $this->output->enable_profiler(TRUE);
	}

	/**
	 * Get all Mcm Accounts
	 *
	 * @return     json  mcm-accounts
	 */
	public function list()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		$defaults 	= array();
		$args 		= wp_parse_args( $this->input->get(), $defaults );
		$args_encypt= md5(json_encode($args));

		$key_cache = "modules/googleads/mcm_accounts-{$args_encypt}";
		$mcm_accounts = $this->scache->get($key_cache);
		if( ! $mcm_accounts)
		{
			$mcm_accounts = $this->mcm_account_m->select('term.term_id,term.term_name')
			->get_many_by(['term_status'=>'active','term_type'=>$this->mcm_account_m->term_type]);

			if( ! $mcm_accounts)
			{
				$response['msg'] = 'Dữ liệu không tồn tại';
				return parent::renderJson($response);
			}

			$this->scache->write($mcm_accounts,$key_cache,300);
		}

		foreach ($mcm_accounts as $mcm_account)
		{
			$mcm_account->account_name = get_term_meta_value($mcm_account->term_id,'account_name');
			$data[] = $mcm_account;
		}

		$response['data'] 		= $data;
		$response['msg']		= 'Xử lý dữ liệu thành công !';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}


	/**
	 * Gets the campaigns.
	 *
	 * @param      integer  $term_id  Contract identity primary key
	 *
	 * @return     JSON 	The campaigns.
	 */
	public function getCampaigns()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		$defaults	= array('term_id' => 0);
		$args 		= wp_parse_args( $this->input->get(), $defaults );

		/* Check permission for ADSPLUS Department */
		if( ! has_permission('googleads.index.manage'))
		{
			$this->load->model('googleads/googleads_kpi_m');
			$kpis = $this->googleads_kpi_m
			->select('kpi_id')
			->where_in('kpi_type', ['account_type','cpc_type'])
			->get_many_by(['user_id'=>$this->admin_m->id,'term_id'=>$args['term_id']]);
			if(empty($kpis))
			{
				$response['msg'] = 'Quyền truy cập bị hạn chế !';
				return parent::renderJson($response);
			}
		}

		/**
		 * Check if mcm_account_id is exists
		 * If TRUE continue get all campaigns else then return No Content
		 */
		$mcm_account_id	= get_term_meta_value($args['term_id'],'mcm_client_id');
		if( ! $mcm_account_id)
		{
			$response['msg'] = 'Dịch vụ chưa được cấu hình kết nối tài khoản Google Adword !';
			return parent::renderJson($response);
		}

		/**
		 * Request campaigns from cache system
		 * if TRUE return Campaigns
		 * else continue to get fresh data
		 */
		$key_cache	= "modules/googleads/adsobjects/campaigns-{$mcm_account_id}";
		$campaigns 	= $this->scache->get($key_cache);
		if($campaigns && 1==2)
		{
			$response['data']	= $campaigns;
			$response['msg']	= 'Xử lý dữ liệu thành công !';
			$response['status']	= TRUE;

			return parent::renderJson($response);
		}
		
		/**
		 * Check if mcm_account_id is exists
		 * IF TRUE continue get all campaigns ELSE then return No Content
		 */
		$mcm_account = $this->mcm_account_m->select('term.term_id')->set_term_type()->get($mcm_account_id);
		if( ! $mcm_account)
		{
			$response['msg'] = 'Tài khoản quảng cáo không tồn tại hoặc đã bị xóa !';
			return parent::renderJson($response);
		}

		/**
		 * Check if campaigns is exists
		 * If TRUE continue get all campaigns detail else then return No Content
		 */
		$account_campaigns = $this->term_posts_m->get_term_posts($mcm_account_id,$this->adcampaign_m->post_type);
		if( ! $account_campaigns)
		{
			$response['msg'] = 'Chiến dịch quảng cáo không được tìm thấy !';
			return parent::renderJson($response);	
		}


		$campaigns = $this->adcampaign_m
		->select('posts.post_id,posts.post_name,posts.post_slug,posts.post_status')
		->select('postmeta.meta_value as trackingUrlTemplate')
		->join('postmeta','postmeta.post_id = posts.post_id AND postmeta.meta_key = "trackingUrlTemplate"', 'LEFT')
		->where_in('posts.post_id',array_column($account_campaigns, 'post_id'))->get_all();
		if( ! $campaigns)
		{
			$response['msg'] = 'Chiến dịch quảng cáo không tồn tại hoặc đã bị xóa !';
			return parent::renderJson($response);	
		}


		$this->scache->write($campaigns,$key_cache); // Cache campaigns for later call
		
		$response['data']	= $campaigns;
		$response['msg']	= 'Xử lý dữ liệu thành công !';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}


	/**
	 * mudate Tracking Url Template for Campaign
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function mutateTrackingUrlTemplate()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Kết nối không thành công','data'=>[]);

		$defaults	= array('term_id' => 0, 'adcampaign_id' => 0);
		$args 		= wp_parse_args( $this->input->get(), $defaults );

		/* Check permission for ADSPLUS Department */
		if( ! has_permission('googleads.index.manage'))
		{
			$this->load->model('googleads/googleads_kpi_m');
			$kpis = $this->googleads_kpi_m
			->select('kpi_id')
			->where_in('kpi_type', ['account_type','cpc_type'])
			->get_many_by(['user_id'=>$this->admin_m->id,'term_id'=>$args['term_id']]);
			if(empty($kpis))
			{
				$response['msg'] = 'Quyền truy cập bị hạn chế !';
				return parent::renderJson($response);
			}
		}


		/**
		 * Check if mcm_account_id is exists
		 * If TRUE continue get all campaigns else then return No Content
		 */
		$mcm_account_id	= get_term_meta_value($args['term_id'],'mcm_client_id');
		if( ! $mcm_account_id)
		{
			$response['msg'] = 'Dịch vụ chưa được cấu hình kết nối tài khoản Google Adword !';
			return parent::renderJson($response);
		}

		if(empty($args['adcampaign_id']))
		{
			$response['msg'] = 'Chiến dịch được yêu cầu không thể xử lý !';
			return parent::renderJson($response);
		}

		/*
		 * Check adcampaign_id is belongs to mcm_account
		 */
		$account_campaigns	= $this->term_posts_m->get_term_posts($mcm_account_id,$this->adcampaign_m->post_type);
		$adcampaign_ids 	= array_column($account_campaigns, 'post_id');
		if(empty($adcampaign_ids) || !in_array($args['adcampaign_id'], $adcampaign_ids))
		{
			$response['msg'] = 'Chiến dịch không thuộc tài khoản chạy quảng cáo .';
			return parent::renderJson($response);
		}

		
		$trackingUrlTemplate = 'http://erp.webdoctor.vn/tracking?keycode=C6ffzORwYMYaQ&lpurl={lpurl}&matchtype={matchtype}&device={device}&campaignid={campaignid}&keyword={keyword}&adposition={adposition}&loc_interest_ms={loc_interest_ms}&loc_physical_ms={loc_physical_ms}&placement={placement}&network={network}&devicemodel={devicemodel}&creative={creative=}&aceid={aceid}&adgroupid={adgroupid}&feeditemid={feeditemid}';

		$trackingUrlTemplate = $this->adcampaign_m->mutateTrackingUrlTemplate($mcm_account_id, $args['adcampaign_id'], $trackingUrlTemplate);

		$response['status'] = TRUE;
		$response['msg'] 	= 'Cấu hình Tracking hoàn tất';
		$response['data']	= array( 'trackingUrlTemplate' => $trackingUrlTemplate ?: null );
		return parent::renderJson($response);
	}


	/**
	 * mudate Tracking Url Template for Campaign
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function mutateIpBlock()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Kết nối không thành công','data'=>[]);

		$defaults	= array('term_id' => 0, 'adcampaign_id' => 0);
		$defaults 	= array('term_id' => 2, '');
		$args 		= wp_parse_args( $this->input->get(), $defaults );

		/* Check permission for ADSPLUS Department */
		if( ! has_permission('googleads.index.manage'))
		{
			$this->load->model('googleads/googleads_kpi_m');
			$kpis = $this->googleads_kpi_m
			->select('kpi_id')
			->where_in('kpi_type', ['account_type','cpc_type'])
			->get_many_by(['user_id'=>$this->admin_m->id,'term_id'=>$args['term_id']]);
			if(empty($kpis))
			{
				$response['msg'] = 'Quyền truy cập bị hạn chế !';
				return parent::renderJson($response);
			}
		}

		/**
		 * Check if mcm_account_id is exists
		 * If TRUE continue get all campaigns else then return No Content
		 */
		$mcm_account_id	= get_term_meta_value($args['term_id'],'mcm_client_id');
		if( ! $mcm_account_id)
		{
			$response['msg'] = 'Dịch vụ chưa được cấu hình kết nối tài khoản Google Adword !';
			return parent::renderJson($response);
		}

		if(empty($args['adcampaign_id']))
		{
			$response['msg'] = 'Chiến dịch được yêu cầu không thể xử lý !';
			return parent::renderJson($response);
		}

		/*
		 * Check adcampaign_id is belongs to mcm_account
		 */
		$account_campaigns	= $this->term_posts_m->get_term_posts($mcm_account_id,$this->adcampaign_m->post_type);
		$adcampaign_ids 	= array_column($account_campaigns, 'post_id');
		if(empty($adcampaign_ids) || !in_array($args['adcampaign_id'], $adcampaign_ids))
		{
			$response['msg'] = 'Chiến dịch không thuộc tài khoản chạy quảng cáo .';
			return parent::renderJson($response);
		}

		
		$trackingUrlTemplate = 'http://erp.webdoctor.vn/tracking?keycode=C6ffzORwYMYaQ&lpurl={lpurl}&matchtype={matchtype}&device={device}&campaignid={campaignid}&keyword={keyword}&adposition={adposition}&loc_interest_ms={loc_interest_ms}&loc_physical_ms={loc_physical_ms}&placement={placement}&network={network}&devicemodel={devicemodel}&creative={creative=}&aceid={aceid}&adgroupid={adgroupid}&feeditemid={feeditemid}';

		$trackingUrlTemplate = $this->adcampaign_m->mutateTrackingUrlTemplate($mcm_account_id, $args['adcampaign_id'], $trackingUrlTemplate);

		$response['status'] = TRUE;
		$response['msg'] 	= 'Cấu hình Tracking hoàn tất';
		$response['data']	= array( 'trackingUrlTemplate' => $trackingUrlTemplate ?: null );
		return parent::renderJson($response);
	}

	/**
	 * Resquest Campaings via Adword Api and update campaign has changed
	 */
	public function updateCampaigns()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Kết nối không thành công','data'=>[]);

		$defaults	= array('term_id' => 0);
		$args 		= wp_parse_args( $this->input->get(), $defaults );

		/* Check permission for ADSPLUS Department */
		if( ! has_permission('googleads.index.manage'))
		{
			$this->load->model('googleads/googleads_kpi_m');
			$kpis = $this->googleads_kpi_m
			->select('kpi_id')
			->where_in('kpi_type', ['account_type','cpc_type'])
			->get_many_by(['user_id'=>$this->admin_m->id,'term_id'=>$args['term_id']]);
			if(empty($kpis))
			{
				$response['msg'] = 'Quyền truy cập bị hạn chế !';
				return parent::renderJson($response);
			}
		}

		/**
		 * Check if mcm_account_id is exists
		 * If TRUE continue get all campaigns else then return No Content
		 */
		$mcm_account_id	= get_term_meta_value($args['term_id'],'mcm_client_id');
		if( ! $mcm_account_id)
		{
			$response['msg'] = 'Dịch vụ chưa được cấu hình kết nối tài khoản Google Adword !';
			return parent::renderJson($response);
		}

		/**
		 * Request all campaign in mcm_account,
		 * then update campaign changed in DB
		 */
		$campaigns = $this->adcampaign_m->request_all($mcm_account_id);
		if( ! $campaigns)
		{
			$response['msg'] = 'Kết nối dữ liệu Google Adword không thành công , vui lòng thử lại !';
			return parent::renderJson($response);
		}

		/**
		 * has Campaigns from Api response
		 */
		$campaign_ids 		= array_column($campaigns, 'id');
		$campaigns_exists 	= $this->adcampaign_m->select('post_id,post_slug')->set_post_type()->where_in('post_slug',$campaign_ids)->get_all();
		$campaigns_exists 	= $campaigns_exists ? array_column($campaigns_exists, NULL,'post_slug') : array();

		$insert_ids = array();

		foreach ($campaigns as $campaign)
		{
			$campaign_data = array(
				'post_slug' => $campaign['id'],
				'post_name' => $campaign['name'],
				'post_status' => $campaign['status'],
				'post_type' => $this->adcampaign_m->post_type
			);

			// Case : campaignId not found then CREATE NEW
			if(empty($campaigns_exists[$campaign['id']]))
			{
				$insert_id = $this->adcampaign_m->insert($campaign_data);
				update_post_meta($insert_id, 'advertisingChannelType', $campaign['advertisingChannelType']);
				update_post_meta($insert_id, 'trackingUrlTemplate', $campaign['trackingUrlTemplate']);

				$insert_ids[] = $insert_id;
				continue;
			}

			// Case : campaignId exists in DB then update
			$post_id = $campaigns_exists[$campaign['id']]->post_id;
			$this->adcampaign_m->set_post_type()->update($post_id, $campaign_data);

			update_post_meta($post_id, 'advertisingChannelType', $campaign['advertisingChannelType']);
			update_post_meta($post_id, 'trackingUrlTemplate', $campaign['trackingUrlTemplate']);
		}

		if(!empty($insert_ids))
		{
			$this->term_posts_m->set_term_posts($mcm_account_id, $insert_ids, $this->adcampaign_m->post_type);
		}

		$this->scache->delete("modules/googleads/adsobjects/campaigns-{$mcm_account_id}");

		$response['status'] = TRUE;
		$response['msg'] = 'Đồng bộ dữ liệu chiến dịch thành công ';
		return parent::renderJson($response);
	}


	/**
	 * Gets the rule conf.
	 *
	 * @return     <type>  The rule conf.
	 */
	public function getRuleConf()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		$defaults	= array('term_id' => 0);
		$args 		= wp_parse_args( $this->input->get(), $defaults );

		if(empty($args['term_id']))
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ !';
			return parent::renderJson($response);
		}

		/* Check permission for ADSPLUS Department */
		if( ! has_permission('googleads.index.manage'))
		{
			$this->load->model('googleads/googleads_kpi_m');
			$kpis = $this->googleads_kpi_m
			->select('kpi_id')
			->where_in('kpi_type', ['account_type','cpc_type'])
			->get_many_by(['user_id'=>$this->admin_m->id,'term_id'=>$args['term_id']]);
			if(empty($kpis))
			{
				$response['msg'] = 'Quyền truy cập bị hạn chế !';
				return parent::renderJson($response);
			}
		}

		$this->load->config('googleads/frauds_rules');

		$rules_default = $this->config->item('frauds_rules');
		$rules = unserialize(get_term_meta_value($args['term_id'],'frauds_rules')) ?: array();
		$rules = wp_parse_args($rules,$rules_default);

		$response['data']	= $rules;
		$response['msg']	= 'Xử lý dữ liệu thành công !';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}


	/**
	 * Update rule config 
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function updateRuleConf($term_id = 0)
	{
		$response = array('status'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		$post = $this->input->post();
		if(empty($term_id) || empty($post))
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ !';
			return parent::renderJson($response);
		}

		$this->load->model('googleads/googleads_m');
		$term = $this->googleads_m->set_default_query()->select('term.term_id')->get($term_id);
		if( ! $term)
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ !';
			return parent::renderJson($response);
		}

		/* Check permission for ADSPLUS Department */
		if( ! has_permission('googleads.index.manage'))
		{
			$this->load->model('googleads/googleads_kpi_m');
			$kpis = $this->googleads_kpi_m
			->select('kpi_id')
			->where_in('kpi_type', ['account_type','cpc_type'])
			->get_many_by(['user_id'=>$this->admin_m->id,'term_id'=>$term_id]);
			if(empty($kpis))
			{
				$response['msg'] = 'Quyền truy cập bị hạn chế !';
				return parent::renderJson($response);
			}
		}

		$this->load->config('googleads/frauds_rules');

		$rules_default = $this->config->item('frauds_rules');
		$rules = wp_parse_args($this->input->post('rules'),$rules_default);
		$rules = array(
			'ad' => array(
				'active' => !empty($rules['ad']['active']),
				'max_clicks' => (int) $rules['ad']['max_clicks'],
				'durations' => ( (int) $rules['ad']['durations'])*60
			),

			'adgroup' => array(
				'active' => !empty($rules['adgroup']['active']),
				'max_clicks' => (int) $rules['adgroup']['max_clicks'],
				'durations' => ( (int) $rules['adgroup']['durations'])*60
			),

			'adcampaign' => array(
				'active' => !empty($rules['adcampaign']['active']),
				'max_clicks' => (int) $rules['adcampaign']['max_clicks'],
				'durations' => ( (int) $rules['adcampaign']['durations'])*60
			),

			'ip_series_p3' => array(
				'active' => !empty($rules['ip_series_p3']['active']),
				'max_clicks' => (int) $rules['ip_series_p3']['max_clicks'],
				'durations' => ( (int) $rules['ip_series_p3']['durations'])*60
			),

			'ip_series_p2' => array(
				'active' => !empty($rules['ip_series_p2']['active']),
				'max_clicks' => (int) $rules['ip_series_p2']['max_clicks'],
				'durations' => ( (int) $rules['ip_series_p2']['durations'])*60
			),

			'behavior' => array(
				'active' => !empty($rules['behavior']['active']),
				'min_time_on_site' => (int) $rules['behavior']['min_time_on_site'] * 60 ,
				'max_sessions' => (int) $rules['behavior']['max_sessions'],
				'durations' => (int) $rules['behavior']['durations'] * 60
			),

			'devices' => array(
				'active' => !empty($rules['devices']['active']),
				'browser_olds' => !empty($rules['devices']['browser_olds']),
				'browser_undetected' => !empty($rules['devices']['browser_undetected']),
				'mouse_undetected' => !empty($rules['devices']['mouse_undetected']),
				'screen_undetected' => !empty($rules['devices']['screen_undetected']),
				'os_undetected' => !empty($rules['devices']['os_undetected'])
			),

			'recovery' => $rules['recovery']
		);

		update_term_meta($term_id,'frauds_rules',serialize($rules));

		$response['msg']	= 'Cấu hình thành công !';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}


	/**
	 * Gets the visitors.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   The visitors.
	 */
	public function getVisitors($term_id = 0)
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);
		$defaults	= array(
			'term_id' => 0,
			'start_time' => $this->mdate->startOfDay(strtotime('-1 week')),
			'end_time' => $this->mdate->endOfDay(),
			'fields' => 'ip,datetime,day,status,created_on,browser,platform,device'
		);
		
		$args = wp_parse_args( $this->input->get(), $defaults );
		$args['fields'] = explode(',', $args['fields']);
		if(empty($args['term_id']) || empty($args['fields']))
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ !';
			return parent::renderJson($response);
		}

		$tracking_code = get_term_meta_value($args['term_id'],'tracking_code');
		$response_data = base_url("api/tracking/visitors?tracking_code={$tracking_code}");
        $response_data = Requests::get($response_data,[],['data_format'=>'body']);
        if($response_data->status_code != 200)
        {
			$response['msg'] = 'Kết nối API Tracking System bị gián đoạn !';
			return parent::renderJson($response);
        }

        $visitors = (json_decode($response_data->body))->data;
        if(empty($visitors))
        {
			$response['msg'] = 'Kết nối API Tracking System bị gián đoạn !';
			return parent::renderJson($response);
        }

        /**
         * MAPPING AND ANALYTICS VISITOR STAT
         */
        $logs = $this->log_m->get_many_by(['term_id'=>$args['term_id'],'log_type'=>'frauds_ip_blocked']);
        if( ! empty($logs))
        {
        	$logs = array_map(function($x){ $x->datetime = $this->mdate->startOfDay($x->log_time_create); return $x; }, $logs);
        	$logs = array_group_by($logs,'datetime');
        }

		$keys_diff = array_diff($this->getVisitorsFields(), $args['fields']);

        foreach ($visitors as &$visitor)
        {
        	$datetime 			= $this->mdate->startOfDay($visitor->created_on);
        	$visitor->datetime 	= $datetime;
        	$visitor->day 		= my_date($datetime,'d/m/Y');
        	$visitor->status 	= empty($logs[$datetime]) ? 'enable' : 'blocked';
			$visitor 			= array_diff_key((array)$visitor, array_flip($keys_diff));
        }

		$response['data']	= $visitors;
		$response['msg']	= 'Xử lý dữ liệu thành công !';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}


	/**
	 * Gets the visitors fields.
	 *
	 * @return     array  The visitors fields.
	 */
	private function getVisitorsFields()
	{
		return [
			'id',
			'ip',
			'network',
			'device',
			'loc_interest_ms',
			'loc_physical_ms',
			'campaignid',
			'adgroupid',
			'creative',
			'targetid',
			'keyword',
			'user_agent',
			'resolution',
			'browser',
			'is_browser',
			'is_mobile',
			'is_desktop',
			'is_robot',
			'created_on',
			'tracking_code',
			'platform',
			'version',
			'datetime',
			'status',
			'day',
		];
	}


	/**
	 * Gets the ip blocked logs.
	 *
	 * @return     <type>  The ip blocked logs.
	 */
	public function getIPBlockedLogs()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);
		$defaults	= array(
			'term_id' => 0,
			'start_time' => $this->mdate->startOfDay(strtotime('-1 week')),
			'end_time' => $this->mdate->endOfDay()
		);
		$args = wp_parse_args( $this->input->get(), $defaults );
		if(empty($args['term_id']))
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ !';
			return parent::renderJson($response);
		}

		$logs = $this->log_m->select('log_id, log_status,log_title,log_content,log_time_create')->get_many_by(['term_id'=>$args['term_id'],'log_type'=>'frauds_ip_blocked']);
		if( ! $logs)
		{
			$response['msg'] = 'Dữ liệu không được tìm thấy hoặc đã bị xóa.';
			return parent::renderJson($response);
		}

		$this->load->helper('date');
		$response['status']	= TRUE;
		$response['msg']	= 'Xử lý dữ liệu thành công !';
		$response['data'] 	= array_map(function($x){
			return [
				'id' => $x->log_id,
				'datetime' => $x->log_time_create,
				'ip' => $x->log_title,
				'status' => $x->log_status,
				'start_time' => mysql_to_unix($x->log_time_create),
				'end_time' => (int) $x->log_content
			];
		}, $logs);

		return parent::renderJson($response);	
	}
}
/* End of file Mcm_account.php */
/* Location: ./application/modules/googleads/controllers/ajax/Tracking.php */