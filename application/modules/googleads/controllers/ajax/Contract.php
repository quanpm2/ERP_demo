<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct() ;
		$models = array(
			'contract/category_m',
			'contract/term_categories_m',
			'contract/receipt_m',
			'googleads/mcm_account_m',
			'googleads/adcampaign_m',
			'googleads/googleads_m',
			'googleads/googleads_kpi_m',
		);

		$this->load->model($models);
	}


	/**
	 * Excuse the set of start services process
	 * 
	 * 1. Update Money Exchange Rate from VIETCOMBANK API
	 * 2. Update Advertise start time if is not set
	 * 3. Log trace action 
	 * 4. Send activation email to all responsibilty users
	 * 5. Detect if first contract
	 * 6. Send SMS notify to contract's customer
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     JSON
	 */
	public function proc_service($term_id = 0)
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
		$args 		= wp_parse_args( $this->input->post(), ['begin_date' => my_date(time(), 'd-m-Y')]);

		// Restrict permission of current user
		if( ! $this->googleads_m->has_permission($term_id, 'Googleads.start_service.update'))
		{
			$response['msg'] = 'Không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}

		// Check if the service is running then stop excute and return
		if($this->googleads_m->is_service_proc($term_id))
		{
			$response['msg'] = 'Dịch vụ đã được thực hiện.';
			return parent::renderJson($response);
		}

		$term = $this->googleads_m->set_default_query()->get($term_id);
		if( ! $term)
		{
			$response['msg'] = 'Không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}

		// Update Adwords begin time
		update_term_meta($term_id, 'googleads-begin_time', $this->mdate->startOfDay($args['begin_date']));

		# code
		$this->load->model('googleads/googleads_contract_m');
		$result = $this->googleads_contract_m->proc_service($term);
		if( ! $result)
		{
			$response['msg'] = 'Quá trình xử lý không thành công.';
			return parent::renderJson($response);
		}

		// Detect if contract is the first signature (tái ký | ký mới)
		$this->load->model('contract/base_contract_m');
		$this->base_contract_m->detect_first_contract($term_id);

		// Send SMS to customer
		$this->load->model('contract/contract_report_m');
		$this->contract_report_m->send_sms_activation_2customer($term_id);

		$response['msg']	= 'Dịch vụ đã được kích hoạt thực hiện thành công.';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}


	/**
	 * Excuse the set of stop services process
	 *
	 * 1. Update googleads-end_time metadata
	 * 1. Update end_service_time metadata with now()
	 * 2. Change term_status to "ending"
	 * 3. Log trace
	 * 4. Send Overall E-mail with Adwords Data to someone
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     JSON
	 */
	public function stop_service($term_id = 0)
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
		$args 		= wp_parse_args( $this->input->post(), ['end_date' => my_date(time(), 'd-m-Y')]);

		// Restrict permission of current user
		if( ! $this->googleads_m->has_permission($term_id, 'Googleads.stop_service.update'))
		{	
			$response['msg'] = 'Không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}

		// Check if the service is running then stop excute and return
		if($this->googleads_m->is_service_end($term_id))
		{
			$response['msg'] = 'Hợp đồng đã được đóng.';
			return parent::renderJson($response);
		}

		// Check this contract is bind to current logged user
		/*$term = $this->googleads_m->select('term.*')->set_default_query()->join('googleads_kpi','googleads_kpi.term_id = term.term_id')->get_by(['term.term_id'=>$term_id, 'googleads_kpi.user_id' => $this->admin_m->id]);*/
		$term = $this->googleads_m->select('term.*')->set_default_query()->join('googleads_kpi','googleads_kpi.term_id = term.term_id')->get_by(['term.term_id'=>$term_id]);
		if( ! $term && $this->admin_m->role_id != 1)
		{
			$response['msg'] = 'Không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}

		// Update Adwords begin time
		$end_time = $this->mdate->endOfDay($args['end_date']);
		update_term_meta($term_id, 'googleads-end_time', $end_time);

		// Main process
		$this->load->model('googleads/googleads_contract_m');
		$result = $this->googleads_contract_m->stop_service($term, $end_time);
		if( ! $result)
		{
			$response['msg'] = 'Quá trình xử lý không thành công.';
			return parent::renderJson($response);
		}

		$response['msg']	= 'Hợp đồng đã được kết thúc.';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}


	/**
	 * Excuse the set of suspend services process
	 *
	 * 1. Update googleads-end_time metadata
	 * 1. Update end_service_time metadata with now()
	 * 2. Change term_status to "ending"
	 * 3. Log trace
	 * 4. Send Overall E-mail with Adwords Data to someone
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     JSON
	 */
	public function suspend_service($term_id = 0)
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
		$args 		= wp_parse_args( $this->input->post(), ['end_date' => my_date(time(), 'd-m-Y')]);

		// Restrict permission of current user
		if( ! $this->googleads_m->has_permission($term_id, 'Googleads.stop_service.update'))
		{	
			$response['msg'] = 'Không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}

		// Check if the service is running then stop excute and return
		if($this->googleads_m->is_service_end($term_id))
		{
			$response['msg'] = 'Hợp đồng đã được đóng.';
			return parent::renderJson($response);
		}

		// Check this contract is bind to current logged user
		$term = $this->googleads_m->set_default_query()->get($term_id);
		if( ! $term)
		{
			$response['msg'] = 'Không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}

		// Update Adwords begin time
		$end_time = $this->mdate->endOfDay($args['end_date']);
		update_term_meta($term_id, 'googleads-end_time', $end_time);

		// Main process
		$this->load->model('googleads/googleads_contract_m');
		$result = $this->googleads_contract_m->suspend_service($term, $end_time);
		if( ! $result)
		{
			$response['msg'] = 'Quá trình xử lý không thành công.';
			return parent::renderJson($response);
		}

		$response['msg']	= 'Hợp đồng đã được thanh lý.';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}
}
/* End of file Contract.php */
/* Location: ./application/modules/googleads/controllers/ajax/Contract.php */