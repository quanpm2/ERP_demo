<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcm_account extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct() ;
		$models = array(
			'contract/category_m',
			'contract/term_categories_m',
			'contract/receipt_m',
			'googleads/mcm_account_m',
			'googleads/adcampaign_m'
		);

		$this->load->model($models);
	}


	/**
	 * Get all Mcm Accounts
	 *
	 * @return     json  mcm-accounts
	 */
	public function list()
	{
		$response 		= array('status' => FALSE, 'msg' => 'Quá trình xử lý thất bại', 'data' => []);
		$defaults 		= array();
		$args 			= wp_parse_args( $this->input->get(), $defaults );
		$args_encypt	= md5(json_encode($args));
		$data 			= array();
		$key_cache 		= Mcm_account_m::CACHE_ALL_KEY . $args_encypt;

		$mcm_accounts = $this->scache->get($key_cache);
		if( ! $mcm_accounts)
		{
			$mcm_accounts = $this->mcm_account_m->select('term.term_id,term.term_name')
			->get_many_by(['term_status'=>'active','term_type'=>$this->mcm_account_m->term_type]);

			if( ! $mcm_accounts)
			{
				$response['msg'] = 'Dữ liệu không tồn tại';
				return parent::renderJson($response);
			}

			$this->scache->write($mcm_accounts, $key_cache, 300);
		}

		foreach ($mcm_accounts as $mcm_account)
		{
			$mcm_account->account_name = get_term_meta_value($mcm_account->term_id,'account_name');
			$data[] = $mcm_account;
		}

		$response['data'] 		= $data;
		$response['msg']		= 'Xử lý dữ liệu thành công !';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}


	/**
	 * Gets the campaigns.
	 *
	 * @param      integer  $term_id  Contract identity primary key
	 *
	 * @return     JSON 	The campaigns.
	 */
	public function getCampaigns()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Quá trình xử lý thất bại','data'=>[]);

		$defaults	= array('term_id' => 0);
		$args 		= wp_parse_args( $this->input->get(), $defaults );

		/* Check permission for ADSPLUS Department */
		if( ! has_permission('googleads.index.manage'))
		{
			$this->load->model('googleads/googleads_kpi_m');
			$kpis = $this->googleads_kpi_m
			->select('kpi_id')
			->where_in('kpi_type', ['account_type','cpc_type'])
			->get_many_by(['user_id'=>$this->admin_m->id,'term_id'=>$args['term_id']]);
			if(empty($kpis))
			{
				$response['msg'] = 'Quyền truy cập bị hạn chế !';
				return parent::renderJson($response);
			}
		}

		/**
		 * Check if mcm_account_id is exists
		 * If TRUE continue get all campaigns else then return No Content
		 */
		$mcm_account_id	= get_term_meta_value($args['term_id'],'mcm_client_id');
		if( ! $mcm_account_id)
		{
			$response['msg'] = 'Dịch vụ chưa được cấu hình kết nối tài khoản Google Adword !';
			return parent::renderJson($response);
		}

		/**
		 * Request campaigns from cache system
		 * if TRUE return Campaigns
		 * else continue to get fresh data
		 */
		$key_cache	= "modules/googleads/adsobjects/campaigns-{$mcm_account_id}";
		$campaigns 	= $this->scache->get($key_cache);
		if($campaigns && 1==2)
		{
			$response['data']	= $campaigns;
			$response['msg']	= 'Xử lý dữ liệu thành công !';
			$response['status']	= TRUE;

			return parent::renderJson($response);
		}
		
		/**
		 * Check if mcm_account_id is exists
		 * IF TRUE continue get all campaigns ELSE then return No Content
		 */
		$mcm_account = $this->mcm_account_m->select('term.term_id')->set_term_type()->get($mcm_account_id);
		if( ! $mcm_account)
		{
			$response['msg'] = 'Tài khoản quảng cáo không tồn tại hoặc đã bị xóa !';
			return parent::renderJson($response);
		}

		/**
		 * Check if campaigns is exists
		 * If TRUE continue get all campaigns detail else then return No Content
		 */
		$account_campaigns = $this->term_posts_m->get_term_posts($mcm_account_id,$this->adcampaign_m->post_type);
		if( ! $account_campaigns)
		{
			$response['msg'] = 'Chiến dịch quảng cáo không được tìm thấy !';
			return parent::renderJson($response);	
		}


		$campaigns = $this->adcampaign_m
		->select('posts.post_id,posts.post_name,posts.post_slug,posts.post_status')
		->select('postmeta.meta_value as trackingUrlTemplate')
		->join('postmeta','postmeta.post_id = posts.post_id AND postmeta.meta_key = "trackingUrlTemplate"', 'LEFT')
		->where_in('posts.post_id',array_column($account_campaigns, 'post_id'))->get_all();
		if( ! $campaigns)
		{
			$response['msg'] = 'Chiến dịch quảng cáo không tồn tại hoặc đã bị xóa !';
			return parent::renderJson($response);	
		}


		$this->scache->write($campaigns,$key_cache); // Cache campaigns for later call
		
		$response['data']	= $campaigns;
		$response['msg']	= 'Xử lý dữ liệu thành công !';
		$response['status']	= TRUE;

		return parent::renderJson($response);
	}


	/**
	 * mudate Tracking Url Template for Campaign
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function mutateTrackingUrlTemplate()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Kết nối không thành công','data'=>[]);

		$defaults	= array('term_id' => 0, 'adcampaign_id' => 0);
		$args 		= wp_parse_args( $this->input->get(), $defaults );

		/* Check permission for ADSPLUS Department */
		if( ! has_permission('googleads.index.manage'))
		{
			$this->load->model('googleads/googleads_kpi_m');
			$kpis = $this->googleads_kpi_m
			->select('kpi_id')
			->where_in('kpi_type', ['account_type','cpc_type'])
			->get_many_by(['user_id'=>$this->admin_m->id,'term_id'=>$args['term_id']]);
			if(empty($kpis))
			{
				$response['msg'] = 'Quyền truy cập bị hạn chế !';
				return parent::renderJson($response);
			}
		}


		/**
		 * Check if mcm_account_id is exists
		 * If TRUE continue get all campaigns else then return No Content
		 */
		$mcm_account_id	= get_term_meta_value($args['term_id'],'mcm_client_id');
		if( ! $mcm_account_id)
		{
			$response['msg'] = 'Dịch vụ chưa được cấu hình kết nối tài khoản Google Adword !';
			return parent::renderJson($response);
		}


		if(empty($args['adcampaign_id']))
		{
			$response['msg'] = 'Chiến dịch được yêu cầu không thể xử lý !';
			return parent::renderJson($response);
		}


		/*
		 * Check adcampaign_id is belongs to mcm_account
		 */
		$account_campaigns	= $this->term_posts_m->get_term_posts($mcm_account_id,$this->adcampaign_m->post_type);
		$adcampaign_ids 	= array_column($account_campaigns, 'post_id');
		if(empty($adcampaign_ids) || !in_array($args['adcampaign_id'], $adcampaign_ids))
		{
			$response['msg'] = 'Chiến dịch không thuộc tài khoản chạy quảng cáo .';
			return parent::renderJson($response);
		}

		$mcm_account_tracking_code = md5($args['term_id']);
		update_term_meta($args['term_id'],'tracking_code');

		$trackingUrlTemplate = 'http://erp.webdoctor.vn/tracking?tracking_code={$mcm_account_tracking_code}&lpurl={lpurl}&matchtype={matchtype}&device={device}&campaignid={campaignid}&keyword={keyword}&adposition={adposition}&loc_interest_ms={loc_interest_ms}&loc_physical_ms={loc_physical_ms}&placement={placement}&network={network}&devicemodel={devicemodel}&creative={creative=}&aceid={aceid}&adgroupid={adgroupid}&feeditemid={feeditemid}';

		$trackingUrlTemplate = $this->adcampaign_m->mutateTrackingUrlTemplate($mcm_account_id, $args['adcampaign_id'], $trackingUrlTemplate);

		$response['status'] = TRUE;
		$response['msg'] 	= 'Cấu hình Tracking hoàn tất';
		$response['data']	= array( 'trackingUrlTemplate' => $trackingUrlTemplate ?: null );
		return parent::renderJson($response);
	}


	/**
	 * Resquest Campaings via Adword Api and update campaign has changed
	 */
	public function updateCampaigns()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Kết nối không thành công','data'=>[]);

		$defaults	= array('term_id' => 0);
		$args 		= wp_parse_args( $this->input->get(), $defaults );

		/* Check permission for ADSPLUS Department */
		if( ! has_permission('googleads.index.manage'))
		{
			$this->load->model('googleads/googleads_kpi_m');
			$kpis = $this->googleads_kpi_m
			->select('kpi_id')
			->where_in('kpi_type', ['account_type','cpc_type'])
			->get_many_by(['user_id'=>$this->admin_m->id,'term_id'=>$args['term_id']]);
			if(empty($kpis))
			{
				$response['msg'] = 'Quyền truy cập bị hạn chế !';
				return parent::renderJson($response);
			}
		}

		/**
		 * Check if mcm_account_id is exists
		 * If TRUE continue get all campaigns else then return No Content
		 */
		$mcm_account_id	= get_term_meta_value($args['term_id'],'mcm_client_id');
		if( ! $mcm_account_id)
		{
			$response['msg'] = 'Dịch vụ chưa được cấu hình kết nối tài khoản Google Adword !';
			return parent::renderJson($response);
		}

		/**
		 * Request all campaign in mcm_account,
		 * then update campaign changed in DB
		 */
		$campaigns = $this->adcampaign_m->request_all($mcm_account_id);
		if( ! $campaigns)
		{
			$response['msg'] = 'Kết nối dữ liệu Google Adword không thành công , vui lòng thử lại !';
			return parent::renderJson($response);
		}

		/**
		 * has Campaigns from Api response
		 */
		$campaign_ids 		= array_column($campaigns, 'id');
		$campaigns_exists 	= $this->adcampaign_m->select('post_id,post_slug')->set_post_type()->where_in('post_slug',$campaign_ids)->get_all();
		$campaigns_exists 	= $campaigns_exists ? array_column($campaigns_exists, NULL,'post_slug') : array();

		$insert_ids = array();

		foreach ($campaigns as $campaign)
		{
			$campaign_data = array(
				'post_slug' => $campaign['id'],
				'post_name' => $campaign['name'],
				'post_status' => $campaign['status'],
				'post_type' => $this->adcampaign_m->post_type
			);

			// Case : campaignId not found then CREATE NEW
			if(empty($campaigns_exists[$campaign['id']]))
			{
				$insert_id = $this->adcampaign_m->insert($campaign_data);
				update_post_meta($insert_id, 'advertisingChannelType', $campaign['advertisingChannelType']);
				update_post_meta($insert_id, 'trackingUrlTemplate', $campaign['trackingUrlTemplate']);

				$insert_ids[] = $insert_id;
				continue;
			}

			// Case : campaignId exists in DB then update
			$post_id = $campaigns_exists[$campaign['id']]->post_id;
			$this->adcampaign_m->set_post_type()->update($post_id, $campaign_data);

			update_post_meta($post_id, 'advertisingChannelType', $campaign['advertisingChannelType']);
			update_post_meta($post_id, 'trackingUrlTemplate', $campaign['trackingUrlTemplate']);
		}

		if(!empty($insert_ids))
		{
			$this->term_posts_m->set_term_posts($mcm_account_id, $insert_ids, $this->adcampaign_m->post_type);
		}

		$this->scache->delete("modules/googleads/adsobjects/campaigns-{$mcm_account_id}");

		$response['status'] = TRUE;
		$response['msg'] = 'Đồng bộ dữ liệu chiến dịch thành công ';
		return parent::renderJson($response);
	}
}
/* End of file Mcm_account.php */
/* Location: ./application/modules/googleads/controllers/ajax/Mcm_account.php */