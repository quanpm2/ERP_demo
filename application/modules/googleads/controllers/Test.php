<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

		$models = array(
			'googleads/googleads_m',
			'googleads/googleads_report_m',
			'googleads/adwords_report_m'
		);

		$this->load->model($models);	
		$this->load->library('unit_test');
	}

	public function compute_service_fee($term_id, $budget = 0)
	{
		$this->contract_m->set_contract($term_id);

		$rate 			= $this->contract_m->calc_service_fee_rate($budget);
		$service_fee 	= $this->contract_m->calc_service_fee($budget);

		update_term_meta($term_id, 'contract_budget', $budget);
		update_term_meta($term_id, 'service_fee_rate', $rate);
		update_term_meta($term_id, 'service_fee', $service_fee);
	}

	public function postSlack()
	{
		$this->load->model('contract/common_report_m');

		$term_id = 13128;

		$this->common_report_m->init($term_id)->postSlackNewContractMessage();
		$this->common_report_m->clear();

		$post_id = 90923;

		$this->common_report_m->init($term_id);
        $this->common_report_m->setReceipt($post_id);
        $this->common_report_m->postSlackNewCustomerPayment();
        $this->common_report_m->clear();
        
		die();
		$this->config->load('slack');
		$slack = new wrapi\slack\slack($this->config->item('token', 'slack'));

		$blocks = array(
			[
				'type' => 'section',
				'text' => [
					'type' => 'mrkdwn',
					'text' => "Chúc mừng cụ *Nguyễn A* (nhóm kinh doanh Hà Nội)\nKhách hàng *đã thanh toán* số tiền *6.000.001 đ*"
				]
			],
			[
				'type' => 'section',
				'text' => [
					'type' => 'mrkdwn',
					'text' => "*Khách hàng:* ADSPLUS\n*Hợp đồng:* Ký mới\n*Giá trị hợp đồng: 12.000.000 đ*"
				],
				'accessory' => [
					'type' => 'image',
					'image_url' => "https://lh5.googleusercontent.com/-vqhwIEnDZ-c/AAAAAAAAAAI/AAAAAAAABM8/SkKord3xnhQ/photo.jpg",
					'alt_text' => "employee thumbnail"
				]
			],
			[
				'type' => "divider"
			]
		);

		$slack->chat->postMessage([
			"channel" => "@thonh",
			"text" => "sample text !",
			'blocks' => json_encode($blocks),
			'username' => 'Tiền về :3'
		]);
	}
}
/* End of file Test.php */
/* Location: ./application/modules/googleads/controllers/Test.php */