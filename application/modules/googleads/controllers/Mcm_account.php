<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Google\Auth\CredentialsLoader;
use Google\Auth\OAuth2;

use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\v201809\cm\OrderBy;
use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\SortOrder;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomerService;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\AdWords\v201809\mcm\CustomerService;

class Mcm_account extends Admin_Controller 
{
	const PAGE_LIMIT = 500;
	protected $oauth2_credential = NULL;
	protected $oauth2_token_builder = NULL;
	protected $mcm_client_id = 0;

	function __construct()
	{
		parent::__construct();
		$this->load->model('mcm_client_m');
		$this->load->model('mcm_account_m');
		$this->load->model('term_users_m');
		$this->load->model('mcm_account_clients_m');
	}

	/**
	 * Rquest credentials via oauth 2.0
	 */
	public function grant_oauth2()
	{
		$this->load->config('credentials');
		$auth_config = [
			'authorizationUri' 		=> $this->config->item('authorizationUri','mcm'),
			'tokenCredentialUri' 	=> CredentialsLoader::TOKEN_CREDENTIAL_URI,
			'redirectUri' 			=> admin_url("googleads/mcm_account/grant_oauth2"),
			'clientId' 				=> $this->config->item('clientId','mcm'),
			'clientSecret' 			=> $this->config->item('clientSecret','mcm'),
			'scope' 				=> $this->config->item('scope','mcm'),
		];

		$oauth2 = new OAuth2($auth_config);

		if (!isset($_GET['code'])) 
		{
			// Create a 'state' token to prevent request forgery.
			// Store it in the session for later validation.
			$oauth2->setState(sha1(openssl_random_pseudo_bytes(1024)));
			$_SESSION['oauth2state'] = $oauth2->getState();

			$redirect_url = $this->input->get('redirect_url');
			if(!empty($redirect_url))
			{
				$this->session->set_userdata('redirect_url',$redirect_url);
			}

			// Redirect the user to the authorization URL.
			$config = ['access_type' => 'offline'];

			header('Location: ' . $oauth2->buildFullAuthorizationUri($config));
			exit;
		}

		$redirect_url = $this->session->userdata('redirect_url') ?: module_url();

		$code = $_GET['code'];
		$oauth2->setCode($code);
		$authToken = $oauth2->fetchAuthToken();
		
		$mcm_client_id = $this->get_set_mcm_client_id($authToken);
		if(empty($mcm_client_id)) 
		{
			$this->messages->error('Kết nối/truy xuất dữ liệu không thành công !!!');
			redirect($redirect_url,'refresh');
		}

		$authToken['refresh_token'] = get_user_meta_value($mcm_client_id,'rtoken');
		$result = $this->refresh_all_accounts($authToken);

		if(!$result) $this->messages->error('Truy xuất dữ liệu không tồn tại.');
		else $this->messages->success('Danh sách đã được làm nạp mới.');
		
		redirect($redirect_url,'refresh');
	}


	/**
	 * Check Exists mcm_client
	 * 
	 * If exists return mcm_client
	 * 
	 * otherwise then insert into mcm_client_m and return mcm_client
	 *
	 * @param      array   $auth_token  The auth token
	 *
	 * @return     int mcm_client_id
	 */
	private function get_set_mcm_client_id($auth_token = array())
	{
		if(empty($auth_token)) return FALSE;

		// request userinfo via HTTP GET
		$request_userinfo = Requests::get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token={$auth_token['access_token']}");
		$userinfo = json_decode($request_userinfo->body);
		if(empty($userinfo)) return FALSE;

		$mcm_client = $this->mcm_client_m->set_get_client()->get_by(['user_email'=>$userinfo->email,'user_login'=>$userinfo->id]);
		if($mcm_client) 
		{
			if(!empty($auth_token['refresh_token']))
			{
				update_user_meta($mcm_client->user_id,'rtoken',$auth_token['refresh_token']);
			}

			$this->mcm_client_id = $mcm_client->user_id;
			return $this->mcm_client_id;
		}

		$insert_data = ['user_email'=>$userinfo->email,'user_login'=>$userinfo->id,'user_type'=>$this->mcm_client_m->user_type];
		$insert_id = $this->mcm_client_m->insert($insert_data);
		update_user_meta($insert_id,'rtoken',$auth_token['refresh_token']);
		
		$this->mcm_client_id = $insert_id;

		return $this->mcm_client_id;
	}


	/**
	 * Gets the hierachy mcm account recursion.
	 *
	 * @param      <type>   $account                   The account
	 * @param      <type>   $customerIdsToAccounts     The customer identifiers to accounts
	 * @param      <type>   $customerIdsToParentLinks  The customer identifiers to parent links
	 * @param      <type>   $customerIdsToChildLinks   The customer identifiers to child links
	 * @param      integer  $depth                     The depth
	 */
	function get_hierachy_account_recursion($account,$customerIdsToAccounts,$customerIdsToParentLinks,$customerIdsToChildLinks,$depth = null) 
	{
		if ($depth === null) 
		{
			$this->get_hierachy_account_recursion($account, $customerIdsToAccounts,$customerIdsToParentLinks, $customerIdsToChildLinks, 0);
			return;
		}
		
		$customerId = $account->getCustomerId();
		$this->update($account,($customerIdsToParentLinks[$customerId] ?? FALSE));

		if (!array_key_exists($customerId, $customerIdsToChildLinks)) return;

		foreach ($customerIdsToChildLinks[$customerId] as $childLink) 
		{
			$childAccount = $customerIdsToAccounts[$childLink->getClientCustomerId()];
			$this->get_hierachy_account_recursion($childAccount, $customerIdsToAccounts,$customerIdsToParentLinks,$customerIdsToChildLinks, $depth + 1);
		}
	}
	
	/**
	 * Load all account and insert/update
	 *
	 * @param      array   $auth_token  The auth token
	 *
	 * @return     boolean  progress's sucess
	 */
	public function refresh_all_accounts($auth_token = array())
	{
		if(empty($auth_token)) return FALSE;
		$token = $auth_token['refresh_token'] ?? ($auth_token['access_token'] ?? FALSE);
		if(!$token) return FALSE;

		$this->load->config('credentials');
		$this->oauth2_token_builder = (new OAuth2TokenBuilder())
		->withClientId($this->config->item('clientId','mcm'))
		->withClientSecret($this->config->item('clientSecret','mcm'))
		->withRefreshToken($token);

		$oAuth2Credential = $this->oauth2_token_builder->build();
		$this->oauth2_credential = $oAuth2Credential;

		$session = (new AdWordsSessionBuilder())
		->withDeveloperToken($this->config->item('googleadsDeveloperToken'))
		->withUserAgent("example.com:ReportDownloader:V3.2")
		->withOAuth2Credential($oAuth2Credential)
		->build();
		
		$adWordsServices = new AdWordsServices();
		$customerService = $adWordsServices->get($session, CustomerService::class);

		$cache_key = "adWordsServices_call_get_call_getCustomers.{$token}";
		$customers = $this->scache->get($cache_key);
		if(!$customers || 1==1)
		{
			$customers = $customerService->getCustomers();
			if(!empty($customers))
			{
				$this->scache->write($customers,$cache_key,864000);
			}
		}

		if(empty($customers)) return FALSE;
		foreach ($customers as $customer)
		{
			$oAuth2Credential = $this->oauth2_credential;
			if(!$oAuth2Credential) continue;

			$adWordsServices = new AdWordsServices();
			$session = (new AdWordsSessionBuilder())
			->withDeveloperToken($this->config->item('googleadsDeveloperToken'))
			->withUserAgent("example.com:ReportDownloader:V3.2")
			->withClientCustomerId($customer->getCustomerId())
			->withOAuth2Credential($oAuth2Credential)
			->build();

			$managedCustomerService = $adWordsServices->get($session, ManagedCustomerService::class);

			// Create selector.
			$selector = new Selector();
			$selector->setFields(['CustomerId', 'Name','CanManageClients','CurrencyCode','DateTimeZone']);
			$selector->setOrdering([new OrderBy('CustomerId', SortOrder::ASCENDING)]);
			$selector->setPaging(new Paging(0, self::PAGE_LIMIT));

	    	// Maps from customer IDs to accounts and links.
			$customerIdsToAccounts = [];
			$customerIdsToChildLinks = [];
			$customerIdsToParentLinks = [];

			$totalNumEntries = 0;
			do 
			{
	      		// Make the get request.
				$page = $managedCustomerService->get($selector);

	      		// Create links between manager and clients.
				if ($page->getEntries() !== null) 
				{
					$totalNumEntries = $page->getTotalNumEntries();
					if ($page->getLinks() !== null) 
					{
						foreach ($page->getLinks() as $link) 
						{
							$customerIdsToChildLinks[$link->getManagerCustomerId()][] = $link;
							$customerIdsToParentLinks[$link->getClientCustomerId()] = $link;
						}
					}

					foreach ($page->getEntries() as $account) 
					{
						$customerIdsToAccounts[$account->getCustomerId()] = $account;
					}
				}

	      		// Advance the paging index.
				$selector->getPaging()->setStartIndex($selector->getPaging()->getStartIndex() + self::PAGE_LIMIT);
			} 
			while ($selector->getPaging()->getStartIndex() < $totalNumEntries);

	    	// Find the root account.
			$rootAccount = null;
			foreach ($customerIdsToAccounts as $account) 
			{
				if (!array_key_exists($account->getCustomerId(),$customerIdsToParentLinks)) 
				{
					$rootAccount = $account;
					break;
				}
			}

			if(!$rootAccount) continue;

			$this->get_hierachy_account_recursion($rootAccount, $customerIdsToAccounts,$customerIdsToParentLinks, $customerIdsToChildLinks);
		}

		$this->scache->delete_group(Mcm_account_m::CACHE_ALL_KEY);
		
		return TRUE;
	}

	/**
	 * update mcm_account data
	 * If not exists then insert
	 * otherwise check manager_id then update
	 *
	 * @param      <type>  $customer    The customer
	 * @param      <type>  $parentLink  The parent link
	 *
	 * @return     bool  progress's success
	 */
	public function update($customer,$parentLink = FALSE)
	{		
		$refresh_token = $this->oauth2_token_builder->getRefreshToken();
		if(empty($refresh_token)) return FALSE;

		$manager_id = !empty($parentLink) ? $parentLink->getManagerCustomerId() : 0;
		$customer_id = $customer->getCustomerId();

		// If mcm_account not exist then create new and mapping
		$account = $this->mcm_account_m->get_by(['term_name'=>$customer_id,'term_type'=>$this->mcm_account_m->term_type]);
		if(empty($account))
		{
			$data = array(
				'term_name' => $customer_id,
				'term_parent' => $manager_id,
				'term_status' => 'active',
				'term_type' => $this->mcm_account_m->term_type,
				);

			$insert_id = $this->mcm_account_m->insert($data);
			
			update_term_meta($insert_id, 'customer_id', $customer_id);
			update_term_meta($insert_id, 'account_name', $customer->getName());
			update_term_meta($insert_id, 'currency_code', $customer->getCurrencyCode());
			update_term_meta($insert_id, 'can_manage_clients', (int) $customer->getCanManageClients());

			// Mapping mcm_account to mcm_client
			$this->mcm_account_clients_m->set_mcm_account_clients($insert_id,$this->mcm_client_id);
			return TRUE;
		}

		// REMAPPING THEN CHECK FOR UPDATE
		$this->mcm_account_clients_m->set_mcm_account_clients($account->term_id,$this->mcm_client_id);

		// If account is not map to any manager && the new has manager then update
		if(!$account->term_parent && $manager_id)
		{
			$this->mcm_account_m->update($account->term_id,['term_parent'=>$manager_id]);
		}
	}

	/**
	 * Gets the AdCampaign's data insight
	 * If time input not defined , default is get LAST_7_DAYS
	 *
	 * @param      integer  $term_id     The term identifier
	 * @param      integer  $start_time  The start time
	 * @param      integer  $end_time    The end time
	 *
	 * @return     array    The data.
	 */
	public function get_data($term_id = 0,$start_time = FALSE, $end_time = FALSE)
	{
		$end_time		= $end_time ?: time();
		$end_time		= $this->mdate->endOfDay($end_time);
		$start_time 	= $start_time ?: strtotime('-7 days',$end_time);
		$start_time 	= start_of_day($start_time);

		$iSegments = $this->term_posts_m->get_term_posts($term_id, $this->insight_segment_m->post_type, [
			'fields' => 'posts.post_id, posts.start_date',
			'where' => [
				'start_date >='	=> $start_time,
				'start_date <='	=> $end_time,
				'post_name' 	=> 'day'
			]
		]);

		if(empty($iSegments)) return [];

		$data = array_map(function($x){
			
			$metadata 		= get_post_meta_value($x->post_id);
			if(empty($metadata)) return [];

			$x = array_merge( (array) $x, array_column($metadata, 'meta_value', 'meta_key'));
			$x['post_id'] 	= (int) $x['post_id'];
			$x['time'] 		= (int) $x['start_date'];
			$x['start_date']= (int) $x['start_date'];
			$x['spend'] 	= (int) ($x['spend']??0);
			return $x;
		}, $iSegments);

		return array_values(array_filter($data));
	}
}
/* End of file Mcm_account.php */
/* Location: ./application/modules/googleads/controllers/Mcm_account.php */