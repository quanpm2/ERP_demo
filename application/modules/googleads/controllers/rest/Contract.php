<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends REST_Controller
{
	function __construct()
	{
		parent::__construct('contract/rest');
		$this->load->model('usermeta_m');
		$this->load->model('termmeta_m');
		$this->load->model('permission_m');
		$this->load->model('googleads/googleads_m');
	}

	public function config_get()
	{
		$default 	= array('name' => '');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);
		if( ! has_permission('googleads.index.access')) parent::response(['msg'=>'Quyền hạn không hợp lệ.'], parent::HTTP_UNAUTHORIZED);

		$data = array();
		$data = wp_parse_args($this->googleads_m->get_field_config(), $data);

		parent::response(array('msg' => 'Dữ liệu tải thành công','data' => $data));
	}
}
/* End of file Contract.php */
/* Location: ./application/modules/wservice/controllers/api/Contract.php */