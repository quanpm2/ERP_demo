<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Contact extends API_Controller
{
	function __construct($config = 'contact/rest')
	{
		$this->autoload['models'][] 	= 'contact/contact_m';
		$this->autoload['configs'][] 	= 'contact/contact';

		parent::__construct($config);
	}

	/**
	 * Retrieve a specific resource (by id) or a collection of resources
	 */
	public function index_get()
	{
		$default 	= array('id' => 0);
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);

		/* Truy xuất 1 contact */
		if(!empty($args['id'])) $this->get_by($args['id']);

		/* Truy xuất nhiều contact */
		$this->get_many_by();

		parent::response();
	}

	/**
	 * Create a new resource
	 */
	public function index_post()
	{
		parent::response();
	}

	/**
	 * Update a specific resource (by id)
	 */
	public function index_put()
	{
		parent::response();
	}


	/**
	 * Remove a specific resource by id
	 */
	public function index_delete()
	{
		parent::response();
	}


	/**
	 * Gets the one contact by.
	 *
	 * @param      integer  $user_id  The user identifier
	 */
	protected function get_by($user_id = 0)
	{
		if( ! $user_id) parent::response('ID Không tồn tại', parent::HTTP_NOT_FOUND);

        $this->load->config('contact/contact');
        
        $default 	= array('columns' => $this->config->item('default_columns', 'datasource'));
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);

		$columns_def 	= $this->config->item('columns', 'datasource');
		$field_cols 	= array_filter($args['columns'], function($x) use($columns_def) { return 'field' == ($columns_def[$x]['type'] ?? '0');});

		$contact = $this->contact_m->select('user.user_id')->select($field_cols)->as_array()->get((int) $user_id);
        if( ! $contact) parent::response('ID Không tồn tại', parent::HTTP_NOT_FOUND);

        $metadata 	= get_user_meta_value($user_id);
        $meta_cols 	= array_filter($args['columns'], function($x) use($columns_def) { return 'meta' == ($columns_def[$x]['type'] ?? '0');});
        if( ! empty($meta_cols))
        {
        	$metadata = get_user_meta_value($user_id);
        	foreach ($meta_cols as $_meta_col)
        	{
        		if(!empty($metadata[$_meta_col]) && is_serialized($metadata[$_meta_col]))
        		{
        			$contact[$_meta_col] = unserialize($metadata[$_meta_col]);
        			continue;
        		}

				$contact[$_meta_col] = $metadata[$_meta_col]['meta_value'] ?? '';
        	}
        }

        /* BEGIN : Xử lý các field liên kết đến table khác tại đây */
        if( ! empty($args['columns']['company_id'])) // Truy xuất Company sở hữu contact
        {
        	$company = $this->company_m->select('user.user_id,display_name,user_email')->as_array()->get($args['columns']['company_id']);
        	if($company && $c_metadata = get_user_meta_value($id))
        	{
        		$company = wp_parse_args(key_value($c_metadata, 'meta_key', 'meta_value'), $company);
        	}
        	$contact['company'] = $company;
        }

        $user->admin = $this->user_m->select('user_id,display_name')->where('user_type','admin')->get_many_by();
        /* END */

		parent::response($contact);
	}

	/**
	 * Gets the contacts by.
	 */
	protected function get_many_by()
	{
		$default = array(
			'pagination' => ['page' => 1, 'pages' => 35, 'perpage' => 10, 'total' => 350],
			'sort' => ['field' => 'user.user_id', 'sort' => 'DESC'],
			'query' => '',
			'columns' => $this->config->item('default_columns', 'datasource')
		);

		$args 	= wp_parse_args(parent::get(NULL, TRUE), $default);

		$this->load->model('contact/contact_m');
		$this->contact_m->select('user_id')->order_by($args['sort']['field'], $args['sort']['sort']);

		$istart 	= ($args['pagination']['page'] - 1) * $args['pagination']['perpage'];
        $ilength 	= $args['pagination']['perpage'];
        $this->contact_m->limit($ilength, $istart);

		$def_columns 	= $this->config->item('columns', 'datasource');
		$field_columns 	= array_group_by($def_columns, 'type');
		if( ! empty($field_columns['field']) && $field_columns = array_column($field_columns['field'], 'name'))
		{
			$this->contact_m->select($field_columns);
		}

		$contacts = $this->contact_m->as_array()->get_all();
		$contacts = array_map(function($x) use($args) { return $this->contact_m->callback($x, $args['columns']); }, $contacts);

		$total_rows = count($contacts);

		$response = array(
			'meta' => array(
				'page' =>  $args['pagination']['page'],
				'pages' =>  ceil(div($total_rows, $args['pagination']['perpage'])),
				'perpage' =>  $args['pagination']['perpage'],
				'total' =>  $total_rows,
				'sort' =>  'asc',
				'field' =>  'user_id'
			),
			'data' => $contacts
		);

		parent::response($response);
	}

	public function config_get($name = 'columns')
	{
		$config = $this->config->item('datasource');
		$data 	= $config[$name] ?? $config;
		parent::response($data);
	}
}
/* End of file Googleads.php */
/* Location: ./application/modules/googleads/controllers/rest/Googleads.php */