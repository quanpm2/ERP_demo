<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Google\AdsApi\AdWords\v201809\cm\ReportDefinitionReportType;
use AdsService\AdsReport\GoogleadsReport;

class Googleads extends Admin_Controller {

	public $model 		= 'googleads_m';
	public $term_type 	= 'google-ads';
	public $website_id 	= 0;
	public $term 		= FALSE;

	function __construct()
	{
		parent::__construct();

		$model_names = array(
			'term_users_m',
			'googleads_m',
			'googleads_kpi_m',
			'contract/contract_m',
			'googleads_log_m',
			'googleads_report_m',
			'history_m',
			'base_adwords_m',
			'googleads/mcm_account_m',
			'staffs/staffs_m'
		);

		$this->load->model($model_names);

		$this->load->config('googleads');
		$this->load->config('group');
		$this->init_website();
	}

	public function edit($edit_id = 0)
	{
		parent::render404();
	}
	
	private function init_website()
	{
		$term_id = $this->uri->segment(4);
		$method  = $this->uri->segment(3);

		if(in_array($method, [ 'index', '', 'ajax', 'convert', 'convert_v2', 'staffs', 'utilities'])) return true;

		if(empty($term_id))
		{
			$this->messages->error("Truy cập không hợp lệ !!!");
			redirect(module_url(), 'refresh');
		}
		
		$term = $this->googleads_m->set_term_type()->get($term_id);
		if(empty($term))
		{
			$this->messages->error("Truy cập #{$term_id} không hợp lệ !!!");
			redirect(module_url(),'refresh');
		}

        $contract_code = get_term_meta_value($term_id, 'contract_code');
		$this->template->title->set(strtoupper(' '.($contract_code ?: $term->term_name)));
		$this->website_id = $term_id;
		$this->data['term'] = $this->term = $term;
	}

	/**
	 * DATATABLE LIST ALL SERVICES
	 */
	public function index()
	{	
		restrict('Googleads.Index.Access');

		$this->template->is_box_open->set(1);
		$this->template->title->set('Tổng quan dịch vụ Google Adword đang thực hiện');
		$this->template->javascript->add(base_url("dist/vGoogleadsAdminIndexPage.js"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		$this->template->publish();
	}

	/**
	 * googleads email admin page 
	 * func : setting and view logs reporting
	 *
	 * @param      interger  $term_id  The term identifier
	 */
	public function email($term_id)
	{
		restrict('Googleads.Email.Access');

		if(FALSE === $this->googleads_m->has_permission($term_id))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

		if($post = $this->input->post())
		{
			restrict('Googleads.Email.Update',current_url());

			if(!empty($post['edit']['meta']))
			{
				$metadatas = $post['edit']['meta'];
				foreach ($metadatas as $meta_key => $meta_value)
					update_term_meta($term_id,$meta_key,$meta_value);

				$this->messages->success('Cập nhật thành công !');
			}
			redirect(module_url("email/{$term_id}"), 'refresh');
		}

		$this->admin_ui
		->select('log_content,log_status')
		->add_column('log_time_create','Ngày gửi')
		->add_column('log_title','Tiêu đề')
		->add_column('attachment',array('title'=>'Attachment','set_select'=>FALSE))
		->add_column_callback('attachment', function($data,$row_name){

			$log_content = unserialize($data['log_content']);
			$data['attachment'] = '<code>Không tồn tại</code>';
			if(!empty($log_content['attachment']))
			{
				$attachments = unserialize($log_content['attachment']);
				if(!empty($attachments))
				{
					$attachment_links = '';
					foreach ($attachments as $key => $path)
					{
						$attachment_links.= anchor(base_url($path),$key);
					}
					$data['attachment'] = $attachment_links;
				}
			}
			return $data;

			},FALSE)
		->add_column('status',array('title'=>'Trạng thái','set_select'=>FALSE))
		->add_column_callback('status', function($data, $row_name){
			$data['status'] = $data['log_status'] ? '<mark> Gửi thành công</mark>' : '<code> Không gửi được</code>';
			return $data;
			},FALSE)
		->from('log')
		->where('term_id',$term_id)
		->where('log_type', "{$this->googleads_log_m->prefix_log_type}email")
		->order_by('log_time_create desc')
		->set_ordering(FALSE);

		$data = $this->data;
		$data['content'] = $this->admin_ui->generate();

		$this->template->title->prepend('Lịch sử EMAIL báo cáo ');
		parent::render($data);
	}


	/**
	 * googleads SMS admin page 
	 * func : setting and view logs reporting
	 *
	 * @param      interger  $term_id  The term identifier
	 */
	public function sms($term_id)
	{
		restrict('Googleads.Sms');

		if(FALSE === $this->googleads_m->has_permission($term_id))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

		$this->load->config('sms/sms');
		$data = $this->data;

		if($post = $this->input->post())
		{
			restrict('Googleads.Sms.Update',current_url());

			if(!empty($post['edit']['meta']))
			{
				foreach ($post['edit']['meta'] as $key => $value)
				{
					update_term_meta($term_id, $key, $value);
				}

				$this->messages->success('Cập nhật thành công !');
			}

			redirect(module_url("sms/{$term_id}"), 'refresh');
		}

		
		$headings = array('Ngày gửi','Người nhận','Nội dung','Trạng thái');
		$this->table->set_heading($headings);

		$logs = $this->googleads_log_m
		->where('term_id', $term_id)
		->where('log_type', $this->googleads_log_m->prefix_log_type . 'sms')
		->where('log_time_create >=',$this->mdate->startOfDay(strtotime('-2 week'), FALSE))
		->where('log_time_create <=',$this->mdate->startOfDay(strtotime('+1 day'), FALSE))
		->order_by('log_time_create','desc')
		->get_many_by();

		if( ! $logs)
		{
			$data['content'] = $this->table->generate();
			return parent::render($data);
		}


		$this->load->config('sms');

		foreach ($logs as $log) 
		{
			$status = $log->log_status;
			$log_content = @unserialize($log->log_content);
			if(empty($log_content))
			{
				// $this->table->add_row($log->log_time_create, '<code>--</code>', '<code>--</code>',$status);
				continue;
			}

			if(is_bool($log_content))
			{
				$this->table->add_row($log->log_time_create, "<code>Số mặc định</code>", "<code>Nội dung mặc định</code>", ($log_content ? 'Thành công' : 'Không thành công'));	
				continue;
			}

			$recipient = implode(" ", [$log_content['recipient'] ?? 'default', '(' . ($log_content['customer_name'] ?? 'default') . ')' ]);
			$status = $this->config->item($log_content['response']->SendSMSResult ?? -1,'sms_status');

			$this->table->add_row($log->log_time_create, $recipient, $log_content['message'] ?? '--', $status);
		}

		$data['content'] = $this->table->generate();

		$this->template->title->prepend('Lịch sử báo cáo SMS ');
		parent::render($data);
	}

    /**
	 * Render "Publish" contract datatable
	 */
	public function overview($term_id)
	{
        if(FALSE === $this->googleads_m->set_contract($term_id))
		{
			$this->messages->error('Hợp đồng không tồn tại');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === $this->googleads_m->can('googleads.Overview.access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === is_service_start($term_id))
		{
			$this->messages->error('Hợp đồng chưa được kích hoạt');
			redirect(admin_url(), 'refresh');
		}

		$this->template->is_box_open->set(1);
		$this->template->title->set( get_term_meta_value($term_id, 'contract_code') . ' | Dịch vụ Google');
		$this->template->javascript->add(base_url("dist/vGoogleadsAdminViewPage.js"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));

		$this->template->javascript->add('https://code.highcharts.com/highcharts.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/variable-pie.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/exporting.js');
		$this->template->javascript->add('https://www.gstatic.com/charts/loader.js');
		$this->template->footer->prepend('<script type="text/javascript">var contractId = '.$term_id.'</script>');
		$this->template->publish();
	}

	/**
	 * Adwords Setting Page
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function setting($term_id = 0)
	{
		if(FALSE === $this->googleads_m->set_contract($term_id))
		{
			$this->messages->error('Hợp đồng không tồn tại');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === $this->googleads_m->can('Googleads.Setting.access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === is_service_start($term_id))
		{
			$this->messages->error('Hợp đồng chưa được kích hoạt');
			redirect(admin_url(), 'refresh');
		}

		if(empty($this->term)) return $this->render404('Dịch vụ này không tồn tại.');

		$this->submit($term_id);

        $this->template->title->prepend('Cấu hình');

		$data = $this->data;
		$data['term_id'] 	= $term_id;
		$data['ajaxUrls'] 	= array('mcm_accounts' => admin_url('googleads/ajax/mcm_account/list'));

		$begin_time = get_term_meta_value($term_id, 'googleads-begin_time');
		$begin_date = $begin_time ? my_date($begin_time, 'd-m-Y') : '';
		$data['begin_time'] = $begin_time;
		$data['begin_date'] = $begin_date;

		$end_time 	= get_term_meta_value($term_id, 'googleads-end_time');
		$end_date	= $end_time ? my_date($end_time, 'd-m-Y') : '';
		$data['end_time']	= $end_time;
		$data['end_date'] 	= $end_date;

		$data['jsobjectdata'] 	= json_encode($data);

		$this->template->is_box_open->set(1);
        $this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		parent::render($data);
	}

	public function manual_report($term_id = 0)
	{
		restrict('Googleads.Manual_report');

		if( FALSE == $this->googleads_m->set_contract($term_id))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

		if( ! $this->googleads_m->can('googleads.manual_report.access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}
		
		$segments = $this->googleads_m->get_segments();
		if( empty($segments)) return $this->render404('Dịch vụ này không tồn tại hoặc chưa được cấu hình để thực hiện chức năng này.');

		$term = $this->googleads_m->get_contract();
		$service_type = get_term_meta_value($term_id,'service_type');

		if($post = $this->input->post())
		{
			if(empty($post['date_range']))
			{
				$this->messages->error('Vui lòng truyền vào tham số thời gian !');
				redirect(module_url("manual_report/{$term_id}"));
			}

			$date_range = explode(' - ', $post['date_range']);
			$start_date = my_date(convert_time(reset($date_range)), 'Y-m-d');
			$end_date 	= my_date(convert_time(end($date_range)), 'Y-m-d');

            $data = (new GoogleadsReport())->generate_report_file($term_id, ['start_date' => $start_date, 'end_date' => $end_date, 'staff_id' => $this->admin_m->id]);
            if(empty($data)){
                $this->messages->error('Tiến trình gặp lỗi không xác định!');

                redirect(module_url("manual_report/{$term_id}"));
            }

			$this->messages->success('Báo cáo đang được khởi tạo. Vui lòng kiểm tra email để lấy file báo cáo!');
			redirect(module_url("manual_report/{$term_id}"));
		}

		$data = array('service_type' => $service_type,'term' => $term,'files'=>array());

		$this->load->helper('directory');
		$this->load->helper('file');

		$path 		= implode('/', array_filter([ './files/google_adword', ($service_type == 'cpc_type' ? '' : 'account_type'), $term_id ]));
		$files_map 	= (new GoogleadsReport())->get_report_files($term_id) ?? [];
        if(empty($files_map)){
            $this->messages->warn('Không tìm thấy danh sách file báo cáo!');

            $this->template->title->prepend('Export báo cáo ');
		    return parent::render($data);
        }
		
        foreach ($files_map as $file)
        {
            $file['server_path'] = $path . '/' . $file['name'];
            $data['files'][] = $file;
        }

        $this->template->title->prepend('Export báo cáo ');
		parent::render($data);
	}

	protected function search_filter($search_args = array())
	{	
		if(empty($search_args) && $this->input->get('search')) 
			$search_args = $this->input->get();

		if(empty($search_args)) return FALSE;

		$this->hook->add_filter('search_filter', function($args){

			if(!empty($args['where']['started_service']))
			{
				$explode = explode(' - ', $args['where']['started_service']);
				if(!empty($explode))
				{
					$startTime = $this->mdate->startOfDay(reset($explode));
					$endTime = $this->mdate->endOfDay(end($explode));

					$this->admin_ui->join('termmeta AS tm_stated_service', 'tm_stated_service.term_id = term.term_id', 'LEFT');
					$this->admin_ui->where('tm_stated_service.meta_key','started_service');
					$this->admin_ui->where('tm_stated_service.meta_value >=', $startTime);
					$this->admin_ui->where('tm_stated_service.meta_value <=', $endTime);
				}
				unset($args['where']['started_service']);
			}

			if(!empty($args['order_by']['started_service']))
			{
				$this->admin_ui->join('termmeta AS tm_stated_service', 'tm_stated_service.term_id = term.term_id', 'LEFT');
				$args['where']['tm_stated_service.meta_key'] = 'started_service';
				$args['order_by']['tm_stated_service.meta_value'] = $args['order_by']['started_service'];
				unset($args['order_by']['started_service']);
			}

			if(!empty($args['where']['googleads_rangedate'])){
				$explode = explode(' - ', $args['where']['googleads_rangedate']);
				if(!empty($explode)){
					$this->admin_ui->join('termmeta', 'termmeta.term_id = term.term_id', 'LEFT');
					$this->admin_ui->where('termmeta.meta_key','googleads-begin_time');
					$this->admin_ui->where('termmeta.meta_value >=', strtotime(reset($explode)));
					$this->admin_ui->where('termmeta.meta_value <=', strtotime(end($explode)));
				}
				unset($args['where']['googleads_rangedate']);
			}

			if(!empty($args['order_by']['googleads_rangedate'])){
				$this->admin_ui->join('termmeta', 'termmeta.term_id = term.term_id', 'LEFT');
				$args['where']['termmeta.meta_key'] = 'googleads-begin_time';
				$args['order_by']['termmeta.meta_value'] = $args['order_by']['googleads_rangedate'];
				unset($args['order_by']['googleads_rangedate']);
			}

			if(!empty($args['where']['service_type'])){
				$this->admin_ui->join('termmeta AS tm_svct', 'tm_svct.term_id = term.term_id', 'LEFT');
				$this->admin_ui->where('tm_svct.meta_key', 'service_type');
				$this->admin_ui->like('tm_svct.meta_value', $args['where']['service_type']);
				unset($args['where']['service_type']);
			}

			if(!empty($args['where']['staff_business']))
			{
				$this->admin_ui
				->join('termmeta AS tm_sb', 'tm_sb.term_id = term.term_id', 'LEFT')
				->where('tm_sb.meta_key', 'staff_business')
				->where('tm_sb.meta_value', $args['where']['staff_business']);
				unset($args['where']['staff_business']);
			}

			if(!empty($args['order_by']['service_type'])){
				$this->admin_ui->join('termmeta AS tm_svct', 'tm_svct.term_id = term.term_id', 'LEFT');
				$this->admin_ui->like('tm_svct.meta_key', 'service_type');
				$args['order_by']['tm_svct.meta_value'] = $args['order_by']['service_type'];
				unset($args['order_by']['service_type']);
			}

			/* Sort for actual progress goal */
			if(!empty($args['order_by']['status_goal']))
			{
				$alias = uniqid('status_goal');
				$this->admin_ui->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'actual_progress_percent'", 'LEFT OUTER');
				$args['order_by']["({$alias}.meta_value)*1"] = $args['order_by']['status_goal'];
				unset($args['order_by']['status_goal']);
			}

			if(!empty($args['order_by']['staff_business']))
			{
				$this->admin_ui->join('termmeta AS tm_sb', 'tm_sb.term_id = term.term_id', 'LEFT');
				$args['order_by']['tm_sb.meta_value'] = $args['order_by']['staff_business'];
				unset($args['order_by']['staff_business']);
			};

			//applies sort event handler
			if(!empty($args['order_by']['real_progress'])){
				$this->admin_ui->join('termmeta AS tm_real_progress', 'tm_real_progress.term_id = term.term_id', 'LEFT OUTER');
				$this->admin_ui->where('tm_real_progress.meta_key', 'real_progress');
				$args['order_by']['CAST(`tm_real_progress`.`meta_value` AS DECIMAL(5))'] = $args['order_by']['real_progress'];
				unset($args['order_by']['real_progress']);
			}

			// FIND AND SORT STAFF_ADVERTISE WITH GOOGLEADS_KPI_M
			if(!empty($args['where']['staff_advertise']))
			{
				$this->admin_ui->join('googleads_kpi','term.term_id = googleads_kpi.term_id', 'LEFT OUTER');
				$this->admin_ui->join('user','user.user_id = googleads_kpi.user_id', 'LEFT OUTER');
				$this->admin_ui->like('user.user_email',strtolower($args['where']['staff_advertise']),'both');

				unset($args['where']['staff_advertise']);
			}

			if(!empty($args['order_by']['staff_advertise']))
			{
				$this->admin_ui->join('googleads_kpi','term.term_id = googleads_kpi.term_id', 'LEFT OUTER');
				$this->admin_ui->join('user','user.user_id = googleads_kpi.user_id', 'LEFT OUTER');
				$args['order_by']['user.user_email'] = $args['order_by']['staff_advertise'];
				unset($args['order_by']['staff_advertise']);
			}

			if(!empty($args['order_by']['payment_percentage']))
			{
				$this->admin_ui->join('termmeta AS payment_percentage_termmeta', 'payment_percentage_termmeta.term_id = term.term_id', 'INNER');
				$args['where']['payment_percentage_termmeta.meta_key'] = 'payment_percentage';

				$args['order_by']['(payment_percentage_termmeta.meta_value)*1'] = $args['order_by']['payment_percentage'];

				unset($args['order_by']['payment_percentage']);
			}

			return $args;			
		});
		parent::search_filter($search_args);
	}

	/**
	 * KPI ADMIN PAGE
	 * @param      integer  $term_id
	 * @param      integer  $delete_id
	 */
	public function kpi($term_id = 0, $delete_id= 0)
	{
		if(FALSE === $this->googleads_m->set_contract($term_id))
		{
			$this->messages->error('Hợp đồng không tồn tại');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === $this->googleads_m->can('Googleads.Kpi.access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === is_service_start($term_id))
		{
			$this->messages->error('Hợp đồng chưa được kích hoạt');
			redirect(admin_url(), 'refresh');
		}

		$data = $this->data;
		$data['term_id'] = $term_id;
		$this->submit_kpi($term_id,$delete_id);

		$roleIds 				= $this->option_m->get_value('group_ads_role_ids', true);
		$data['technicians'] 	= $this->admin_m->select('user_id,display_name,user_email, role.role_name')->join('role', 'role.role_id = user.role_id')->where_in('user.role_id', array_map('intval', $roleIds))->set_get_active()->order_by('role_name')->get_all();
		// if( ! empty($technicians)) foreach ($technicians as $user) $data['users'][$user->user_id] = implode(' - ', [ $user->role_name, $user->display_name, $user->user_email ]);

		$data['kpis'] = $this->googleads_kpi_m
		->order_by('kpi_type')
		->where('term_id',$term_id)
		->as_array()
		->get_many_by();

		$this->template->title->prepend('KPI');

		parent::render($data);
	}

	/**
	 * Submit handler of function KPI()
	 * @param  integer	$term_id
	 * @param  integer	$delete_id
	 */
	public function submit_kpi($term_id = 0,$delete_id = 0)
	{
		if($delete_id > 0)
		{
			$this->_delete_kpi($term_id,$delete_id);
			$this->googleads_kpi_m->delete_cache($delete_id);
			redirect(module_url("kpi/{$term_id}"),'refresh');
		}

		$post = $this->input->post();
		if(empty($post)) return FALSE;

		// restrict('Googleads.Kpi.add');
		if(FALSE === $this->googleads_m->has_permission($term_id, 'Googleads.Kpi.Add'))
		{
			$this->messages->error('Không có quyền thực hiện tác vụ này');
			redirect(current_url(), 'refresh');
		}

		$insert_data = array(
			'kpi_type' => $post['kpi_type'],
			'kpi_datetime' => $this->mdate->startOfMonth(time(), FALSE),
			'user_id' => $post['user_id'],
			'term_id' => $term_id
		);

		# Insert relation of term's kpi to user in kpi table
		$this->googleads_kpi_m->insert($insert_data);

		# Update relation of term's user in term_users table
		$this->term_users_m->set_relations_by_term($term_id,[$post['user_id']], 'admin');

		/* Update Technician to Contract */
		$this->googleads_m->set_contract($term_id)->get_behaviour_m()->setTechnicianId();

		$this->messages->success('Cập nhật thành công');
		redirect(current_url(), 'refresh');
	}

	/**
	 * Delete the KPI record in googleads_kpi_m
	 * @param  integer	$term_id	
	 * @param  integer	$delete_id		
	 * @return boolean
	 */
	private function _delete_kpi($term_id = 0, $delete_id = 0)
	{
		if(FALSE === $this->googleads_m->has_permission($term_id, 'Googleads.Kpi.Delete'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			return FALSE;
		}

		$kpi = $this->googleads_kpi_m->get($delete_id);

		if( ! $kpi)
		{
			$this->messages->error('Xóa không thành công, dữ liệu không tồn tại!');
			return FALSE;
		}

		// Xóa kpi thành công
		$this->googleads_kpi_m->delete_cache($delete_id);
		$this->googleads_kpi_m->delete($delete_id);
		$this->googleads_m->set_contract($term_id)->get_behaviour_m()->setTechnicianId();

		/* Update Technician to Contract */

		$this->messages->success('Xóa thành công');

		// Nếu KPI đang dành cho bộ phận khác kỹ thuật thì kết thúc quá trình xử lý
		$service_type_config = array_keys($this->config->item('googleads_service_type'));
		if( empty($service_type_config) || ! in_array($kpi->kpi_type, $service_type_config)) return TRUE;


		$this->load->config('staffs/group');

		$sale_role_ids 	= $this->config->item('salesexecutive', 'groups');
		$role_id 		= $this->admin_m->get_field_by_id($kpi->user_id, 'role_id');

		// NẾU KPI CÓ VAI TRÒ NVKD THÌ KHÔNG CẦN XỬ LÝ TIẾP
		if(empty($sale_role_ids) || in_array($role_id, $sale_role_ids)) return TRUE;

		// TRƯỜNG HỢP KPI KHÁC CỦA USER VẪN CÒN TỒN TẠI
		$kpis = $this->googleads_kpi_m->get_many_by(['term_id'=>$term_id,'user_id'=>$kpi->user_id]);
		if( ! empty($kpis)) return TRUE;

		// NẾU USER KHÔNG CÒN BẤT KỲ LIÊN HỆ NÀO VỚI HỢP ĐỒNG => XÓA TERM_USERS
		$this->term_users_m->delete_term_users($term_id, [$kpi->user_id]);
		return TRUE;
	}

	/**
	 * The page mapping contract with staffs
	 * @return [void]
	 */
	public function staffs()
	{
		if( ! has_permission('Googleads.staffs.access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

		$this->template->is_box_open->set(1);
		$this->template->title->set('Bảng phân công phụ trách thực hiện');
		$this->template->javascript->add(base_url("dist/vGoogleConfigurationAssignedKpi.js"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
        $this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
        $this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));

		$this->template->publish();
	}

    public function convert()
	{
		restrict('Googleads.Convert');
		$data = $this->data;

		$post = $this->input->post(NULL, TRUE);
		if( ! empty($post))
		{
			switch (TRUE) 
			{
				case !empty($post['btnImportFile']):

					$upload_cfg = array( 'upload_path' => './tmp/', 'allowed_types' => 'xlsx', 'max_size'	=> '4096', 'file_name' => md5(time()) );

					if(!is_dir($upload_cfg['upload_path']))
						mkdir($upload_cfg['upload_path'], '0777', TRUE);

					$this->load->library('upload');
					$this->upload->initialize($upload_cfg);

					$response = ['msg'=>'Tệp không hợp lệ , vui lòng thử lại!','success' => FALSE];

					if(!$this->upload->do_upload('inputFile'))
						return $this->output->set_content_type('application/json')->set_output(json_encode($response));

					$data = $this->upload->data();
					$fileName = $upload_cfg['upload_path'].$data['file_name'];

					$response = ['msg' => 'Upload file hợp đồng thành công !','success' => TRUE,'notification'=>'Dữ liệu không hợp lệ !'];

					/* PROCESS EXCEL FILE TO DATA */
					$this->load->library('excel');
					$objReader = PHPExcel_IOFactory::createReaderForFile($fileName);
					$objReader->setReadDataOnly(TRUE); //set this, to not read all excel properties, just data
					$objPHPExcel = $objReader->load($fileName);

					$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,TRUE,FALSE,TRUE);
					$sheetHeading = array_shift($sheetData);
					
					$objPHPExcel->disconnectWorksheets();
					unset($objPHPExcel);
					@unlink($fileName);
					
					if(empty($sheetData))
						return $this->output->set_content_type('application/json')->set_output(json_encode($response));

					define('EXTRAC_TRATE',1);
					define('PHRASE_RATE',0.9);
					define('BROAD_RATE',0.8);
					define('ADGROUP_DEFAULT_CPC',1);
					define('ADGROUP_DEFAULT_CPM',1);

					$this->load->helper('text');

					$adGroups = array();
					foreach ($sheetData as $row)
					{
						// map vars to row cell data
						$campaign = $row['A'];
					    $adGroup = $row['B'];
					    $maxCPC = $row['C'];
					    $finalURL = $row['D'];
					    $finalMobileURL = $row['E'];
					    $headLine_1_2 = $row['F'];
					    $description_1 = $row['G'];
					    $path_1 = $row['H'];
					    $path_2 = $row['I'];
					    $headLine_2_2 = $row['J'];
					    $description_2 = $row['K'];
					    $path_2_1 = $row['L'];
					    $path_2_2 = $row['M'];

					    $insKeyword = $adGroup;
					    $unaccentedkeyword = convert_accented_characters($insKeyword);
					    $isUnaccentedkeyword = ($insKeyword == convert_accented_characters($insKeyword));

					    $keywords = array();

					    //Extractly keyword
					    $keywords[] = array(
					    	'keyword' => $insKeyword,
					    	'criterionType' => 'Exact',
					    	'maxCPC' => $maxCPC * EXTRAC_TRATE,
					    	);

					    if(!$isUnaccentedkeyword)
					    {
					    	$keywords[] = array(
					    	'keyword' => $unaccentedkeyword,
					    	'criterionType' => 'Exact',
					    	'maxCPC' => $maxCPC * EXTRAC_TRATE,
					    	);	
					    }

					    //Phrase keyword
					    $keywords[] = array(
					    	'keyword' => $insKeyword,
					    	'criterionType' => 'Phrase',
					    	'maxCPC' => $maxCPC * PHRASE_RATE,
					    	);

					    if(!$isUnaccentedkeyword)
					    {
					    	$keywords[] = array(
					    	'keyword' => $unaccentedkeyword,
					    	'criterionType' => 'Phrase',
					    	'maxCPC' => $maxCPC * PHRASE_RATE,
					    	);	
					    }

					    //Phrase keyword
					    $keywords[] = array(
					    	'keyword' => '+'.str_replace(' ', ' +', $insKeyword),
					    	'criterionType' => 'Broad',
					    	'maxCPC' => $maxCPC * BROAD_RATE,
					    	);

					    if(!$isUnaccentedkeyword)
					    {
					    	$keywords[] = array(
					    	'keyword' => '+'.str_replace(' ', ' +', $unaccentedkeyword),
					    	'criterionType' => 'Broad',
					    	'maxCPC' => $maxCPC * BROAD_RATE,
					    	);	
					    }

					    $banner_key = mb_convert_case($insKeyword, MB_CASE_TITLE);

					    $ads = array(
					    	array(
					    		'headline_1' => $banner_key,
					    		'headline_2' => $headLine_1_2,
					    		'description' => $description_1,
					    		'path_1' => $path_1,
					    		'path_2' => $path_2,
					    		'finalURL' => $finalURL,
					    		'finalMobileURL' => $finalMobileURL
					    	),
					    	array(
					    		'headline_1' => $banner_key,
					    		'headline_2' => $headLine_2_2,
					    		'description' => $description_2,
					    		'path_1' => $path_2_1,
					    		'path_2' => $path_2_2,
					    		'finalURL' => $finalURL,
					    		'finalMobileURL' => $finalMobileURL
					    	)
					    );

					    $adGroups[] = array(
					    	'campaign' => $campaign,
					    	'adGroup' => $adGroup,
					    	'maxCPC' => ADGROUP_DEFAULT_CPC,
					    	'maxCPM' => ADGROUP_DEFAULT_CPM,
					    	'keywords' => $keywords,
					    	'ads' => $ads
					    	);
					}

					$tmpCacheKey = md5(time());
					$this->scache->write($adGroups,$tmpCacheKey);
					$response['tmpKey'] = $tmpCacheKey;

					$campaigns = $adgroups = $keywords = $ads = array();
					foreach ($adGroups as $row)
					{
						if(!isset($campaigns[$row['campaign']])) 
							$campaigns[$row['campaign']] = $row['campaign'];

						$adgroups[] = $row['adGroup'];
						if(!empty($row['keywords']))
							$keywords = array_merge($keywords,array_column($row['keywords'], 'keyword'));

						if(!empty($row['ads']))
							$ads = array_merge($ads,array_values($row['ads']));
					}

					$messages = '';
			        $messages.= count($campaigns).' Campaign được tìm thấy.'.br();
			        $messages.= count($adgroups).' Ad Group được tìm thấy.'.br();
			        $messages.= count($keywords).' Keywords được tạo.'.br();
			        $messages.= count($ads).' mẫu quảng cáo được tạo.'.br();
			        $response['notification'] = $messages;

			        $this->scache->write('','modules/googleads/actions/user-'.$this->admin_m->id.'-convert'.time());

					return  $this->output->set_content_type('application/json')->set_output(json_encode($response));
					
					break;

				case !empty($post['btnExportFile']):

					$tmpCacheKey = $post['tmpKey'];
					if(empty($tmpCacheKey))
					{
						$this->messages->error('Đầu vào không hợp lệ . Vui lòng thử lại.');
						break;
					}
					
					$adGroups = $this->scache->get($tmpCacheKey);

					if(empty($adGroups))
					{
						$this->messages->error('Dữ liệu không hợp lệ . Vui lòng thử lại.');
						break;
					}

					// WRITE EXPECTED EXCEL FILE
					$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
			        $cacheSettings = array( 'memoryCacheSize' => '512MB');
			        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

			        $export_template = './files/excel_tpl/tool/google-adwords-editors-export-template.csv';
			        $objPHPExcel = new PHPExcel();

			        $inputFileType = PHPExcel_IOFactory::identify($export_template);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType, false, 'UTF-8');
					$objPHPExcel = $objReader->load($export_template);
			        $objPHPExcel->getProperties()->setCreator("ADSPLUS.VN")->setLastModifiedBy("ADSPLUS.VN")->setTitle("Converted File");

			        $objSheet = $objPHPExcel->getActiveSheet();

			        $iRow = 2;
			    	foreach ($adGroups as $adGroup) 
			    	{
			    		// Set cells value for CAMPAIGN
			    		$objSheet->setCellValue("A{$iRow}",$adGroup['campaign']); // campaign name
			    		$iRow++;

			    		// Set cells value for AD GROUP
			    		$objSheet->setCellValue("A{$iRow}",$adGroup['campaign']);	// campaign name
			    		$objSheet->setCellValue("W{$iRow}",$adGroup['adGroup']);	// ad group
			    		$objSheet->setCellValue("X{$iRow}",$adGroup['maxCPC']);	// Max CPC
			    		$objSheet->setCellValue("Y{$iRow}",$adGroup['maxCPM']);	// Max CPM
			    		$iRow++;

			    		// Set keywords of each AD GROUP
			    		foreach ($adGroup['keywords'] as $keyword)
			    		{
							// Set cells value for KEYWORD
				    		$objSheet->setCellValue("A{$iRow}",$adGroup['campaign']);	// campaign name
				    		$objSheet->setCellValue("W{$iRow}",$adGroup['adGroup']);	// ad group
				    		$objSheet->setCellValue("X{$iRow}",$keyword['maxCPC']);	// Max CPC
				    		$objSheet->setCellValue("AL{$iRow}",$keyword['criterionType']);	// Criterion Type
				    		// $objSheet->setCellValue("BJ{$iRow}", ($keyword['criterionType'] == 'Broad' ? '\'':'') . $keyword['keyword']);	// Keyword
				    		$objSheet->setCellValue("BJ{$iRow}", $keyword['keyword']);	// Keyword

				    		$iRow++;    			
			    		}

			    		// Set banners of each AD GROUP
			    		foreach ($adGroup['ads'] as $ad) 
			    		{
			    			// Set cells value for KEYWORD
				    		$objSheet->setCellValue("A{$iRow}",$adGroup['campaign']);	// campaign name
				    		$objSheet->setCellValue("W{$iRow}",$adGroup['adGroup']);	// ad group
				    		
				    		$objSheet->setCellValue("AO{$iRow}",$ad['finalURL']);	// Final URL
				    		$objSheet->setCellValue("AP{$iRow}",$ad['finalMobileURL']);	// Final mobile URL
				    		$objSheet->setCellValue("BX{$iRow}",'"'.$ad['description'].'"');	// Description
				    		$objSheet->setCellValue("CG{$iRow}",'"'.$ad['headline_1'].'"');	// Headline 1
				    		$objSheet->setCellValue("CH{$iRow}",'"'.$ad['headline_2'].'"');	// Headline 2
				    		$objSheet->setCellValue("CI{$iRow}",$ad['path_1']);	// Path 1
				    		$objSheet->setCellValue("CJ{$iRow}",$ad['path_2']);	// Path 2

				    		$iRow++;
			    		}
			    	}

			        $folder_upload = "files/google_adword/account_type/{$client_customer_id}";
			        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
			        $objWriter->setUseBOM(TRUE);
			        $objWriter->setEnclosure('');
			        $fileName =  './result-export-file.csv';
			        $objWriter->save($fileName);

			        $this->load->helper('download');
			        force_download($fileName,NULL);
			        $this->messages->success('Xử lý thành công . File sẽ tự động được tải về máy.');
					break;
			}
		}

		$this->template->stylesheet->add('plugins/bootstrap-fileinput/css/fileinput.css');
		$this->template->javascript
		->add('plugins/validate/jquery.validate.js')
		->add('plugins/bootstrap-fileinput/js/fileinput.min.js')
		->add('vendors/vuejs/vue.js')
		->add('https://unpkg.com/axios/dist/axios.min.js')
		->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js')
		->add('modules/googleads/adword-editor-convert-tool-component-v2.js');

		$content = '<div id="app_root"><adword-editor-convert-tool-component-v2/></div>';
		$content.= '<script type="text/javascript"> var app_root = new Vue({ el: "#app_root" }); </script>';

		$this->template->title->set("Convert file Import cho trình Adwords Editor ver 2");
		$this->template->content->set($content);
		$this->template->publish();
	}
    
    /**
     * utilities
     *
     * @return void
     */
    public function utilities()
	{
        if( ! has_permission('Googleads.utilities.access'))
        {
            $this->messages->error('Quyền truy cập không hợp lệ');
            redirect(admin_url(), 'refresh');
        }

        $this->template->is_box_open->set(1);
        $this->template->title->set('Công cụ hỗ trợ');
        $this->template->javascript->add('plugins/bootstrap-filestyle/bootstrap-filestyle.min.js');
        $this->template->javascript->add(base_url("dist/vGoogleUtilities.js"));

        $this->template->publish();
    }

	/**
	 * Submit handler for function Overview|Setting
	 * Process and redirect to parent page
	 * @return     <type>  Process
	 */
	private function submit($term_id = 0)
	{
		$post = $this->input->post();
		if(empty($post)) return FALSE;

		$term = $this->googleads_m->set_term_type()->get($term_id);
		if( ! $term)
		{
			$this->messages->error('Dịch vụ không tồn tại hoặc không có quyền truy cập');
			redirect(current_url("overview/{$term->term_id}"), 'refresh');
		}

		if($this->input->post('authentization') != NULL)
		{
			$authentization_url = 'mcm_account/grant_oauth2?redirect_url='.urlencode(admin_url("googleads/setting/{$term_id}"));
			redirect(module_url($authentization_url),'refresh');
		}

		if( ! empty($post['curator_submit'])) // Update curator infomation reciepents for system reporting
		{
			restrict('Googleads.Setting.update',current_url());
			$count = count($post['edit']['meta']['curator_name']);
			if($count)
			{
				$i = 0;
				$curators = array();
				for ($i; $i < $count; $i++)
				{
					$curators[] = array(
						'name'=>$post['edit']['meta']['curator_name'][$i],
						'phone'=>$post['edit']['meta']['curator_phone'][$i],
						'email'=>$post['edit']['meta']['curator_email'][$i]);
				}

				$this->history_m->table = $this->term_m->_table;
				$this->history_m->object_id = $term->term_id;
				$this->history_m
				->set_original_data(array('contract_curators'=>get_term_meta_value($term->term_id,'contract_curators')))
				->get_dirty(array('contract_curators'=>serialize($curators)))
				->save();

				$meta_value = serialize($curators);
				update_term_meta($term->term_id,'contract_curators',$meta_value);
				$this->messages->success('Cập nhật thành công !');
			}

			redirect(current_url(), 'refresh');
		}

		if( empty($post['adword_submit']) || empty($post['edit'])) redirect(current_url(), 'refresh');


		/**
		 * Submit Setting box with metrics affect to ADS Data
		 */
		restrict('Googleads.Setting.update',current_url());
		$oldDatas = (array) $this->data['term'];

		if( ! empty($post['edit']['meta']))
		{
			$canUpdateCoreMetricsUpdate =  $this->contract_m->has_permission($term_id, 'Googleads.metrics_core.update');

			if(isset($post['edit']['meta']['googleads-begin_time']))
			{
				if(is_service_proc($term) && ! $canUpdateCoreMetricsUpdate)
				{
					$this->messages->error('Cập nhật ngày bắt đầu chạy QC sẽ ảnh hưởng số liệu QC . Quyền hạn bị hạn chế !');
					redirect(current_url(), 'refresh');	
				}
			}

			if(isset($post['edit']['meta']['googleads-end_time']))
			{
				if(is_service_end($term) && ! $canUpdateCoreMetricsUpdate)
				{
					$this->messages->error('Cập nhật ngày kết thúc QC sẽ ảnh hưởng số liệu QC . Quyền hạn bị hạn chế !');
					redirect(current_url(), 'refresh');	
				}
			}

			if( ! empty($post['edit']['meta']['mcm_client_id']))
			{
				$currency_code = get_term_meta_value($post['edit']['meta']['mcm_client_id'], 'currency_code');
				$vat = (int) get_term_meta_value($term->term_id, 'vat');

				if($vat > 0 && ($currency_code != 'USD' || $currency_code != 'AUD') && 'customer' != get_term_meta_value($term_id, 'contract_budget_payment_type'))
				{
					$this->messages->error('Không thể cập nhật , Hợp đồng VAT yêu cầu tài khoản quảng cáo phải sử dụng USD hoặc AUD!');
					redirect(current_url("setting/{$term->term_id}"), 'refresh');	
				}
			}

			if(empty($post['edit']['meta']['exchange_rate_usd_to_vnd']))
			{
				$post['edit']['meta']['exchange_rate_usd_to_vnd'] = get_exchange_rate('USD', $term->term_id);
			}

            if(empty($post['edit']['meta']['exchange_rate_aud_to_vnd']))
			{
				$post['edit']['meta']['exchange_rate_aud_to_vnd'] = get_exchange_rate('AUD', $term->term_id);
			}

			foreach ($post['edit']['meta'] as $key => $value) 
			{	
				$oldDatas[$key] = get_term_meta_value($term->term_id,$key);
				$this->termmeta_m->update_meta( $term->term_id, $key, $value);
			}

			$this->history_m->get_dirty($post['edit']['meta']);

			unset($post['edit']['meta']);
		}

		if(!empty($post['edit']))
		{
			$this->history_m->table = $this->term_m->_table;
			$this->history_m->object_id = $term->term_id;
			$this->history_m
			->set_original_data($oldDatas)
			->get_dirty($post['edit'])
			->save();

			$this->term_m->update($term->term_id, $post['edit']);
			$this->messages->success('Cập nhật thành công !');
		}
		
		
		redirect(current_url(), 'refresh');
	}

	/**
	 * Admin page quản lý chặn click không hợp lệ
	 *
	 * @param      int  $term_id  The term identifier
	 */
	public function fraudsClicks($term_id)
	{
		$this->config->load('googleads/frauds_rules');

		$data = $this->data;
		$tracking_code	 = get_term_meta_value($term_id,'tracking_code');
		$data['term_id'] = $term_id;

		$data['url_get_visitors']		= admin_url('googleads/ajax/tracking/getVisitors');
		$data['url_get_campaigns'] 		= admin_url('googleads/ajax/mcm_account/getCampaigns');
		$data['url_request_campaigns'] 	= admin_url('googleads/ajax/mcm_account/updateCampaigns');
		$data['url_get_rule_conf']		= admin_url('googleads/ajax/tracking/getRuleConf');
		$data['url_update_rule_conf'] 	= admin_url('googleads/ajax/tracking/updateRuleConf');
		$data['url_mutate_trackingtemplateurl'] = admin_url('googleads/ajax/mcm_account/mutateTrackingUrlTemplate');
		$data['url_get_blocked_logs'] = admin_url('googleads/ajax/tracking/getIPBlockedLogs');
		
		$data['jsobjectdata'] = json_encode($data);

		$this->template->is_box_open->set(1);
		parent::render($data);
	}
	
	public function setTrackingUrlTemplate($adcampaignId = '734919008'){}
}
/* End of file Googleads.php */
/* Location: ./application/modules/googleads/controllers/Googleads.php */
