<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends API_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('term_m');
		$this->load->model('googleads/api_googleads_m');
		$this->load->model('googleads/googleads_m');
		$this->load->model('googleads/mcm_account_m');
		$this->load->model('googleads/base_adwords_m');
		$this->load->model('ads_segment_m');

		ini_set('display_errors', 0);
	}
	

	/**
	 * GET ALL CONTRACT LIST
	 */
	public function list()
	{
		if( ! $this->authenticate) return parent::render();

		$response = array('status' => 1, 'lists' => array());

		$terms = $this->api_googleads_m->get_contract_list_by($this->token);
		if( ! $terms)
		{
			return parent::render($response);
		}

		$response['lists'] = $terms;

		$this->default_response['auth'] = 1;
		return parent::render($response);
	}


	/**
	 * GET contract data
	 *
	 * @return     JSON
	 */
	function contract_info()
	{
		if(!$this->authenticate)
			return parent::render();

		$data = array('status'=>0);
		$id = (int) $this->post('id');
		$token = $this->get('token');

		$data['data'] = $this->api_googleads_m->get_contract_info($id,$token);
		$data['status'] = 1;

		$this->default_response['auth'] = 1;
		return parent::render($data);
	}


	/**
	 * GET all messages
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	function list_msg()
	{
		if(!$this->authenticate) return parent::render();
		$id = (int) $this->post('id');
		if(empty($id)){
			$id = $this->get('id');
		}
		$token = $this->get('token');
		
		$data = array('status'=>0);	

		$data['data'] = $this->api_googleads_m->get_message_by($id,$token);
		$data['status'] = 1;

		$this->default_response['auth'] = 1;
		return parent::render($data);
	}


	/**
	 * Gets the adword data.
	 *
	 * @return     <type>  The adword data.
	 */
	function get_adword_data()
	{
		if(!$this->authenticate) return parent::render();

		$data = array();
		$data['status'] = 0;
		$data['data'] = array();

		$term_id = (int) $this->post('id');
		$token = $this->get('token');
		// $end_time = $this->post('end_time') ?? time();
		$end_time = $this->post('end_time');
		if(empty($end_time))
		{
			$end_time = get_term_meta_value($term_id,'googleads-end_time') ?: time();
		}

		// $start_time = $this->post('start_time') ?? $this->mdate->startOfMonth();
		$start_time = $this->post('start_time');
		if(empty($start_time))
		{
			$start_time = get_term_meta_value($term_id,'googleads-begin_time');
			if($start_time > $end_time)
			{
				$start_time = strtotime('-14 day',$end_time);
			}
		}

		$term = $this->term_m->get($term_id);

		$this->load->model('base_adwords_m');

		$service_type = get_term_meta_value($term_id,'service_type');
		
		$mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
		$this->base_adwords_m->set_mcm_account($mcm_account_id,FALSE);
		$account_data = $this->base_adwords_m->get_cache('ACCOUNT_PERFORMANCE_REPORT',$start_time,$end_time);
		$account_data = array_filter($account_data);

		if(empty($account_data)) 
			return parent::render($data);

		$adword_data = array();

		$contract_clicks = get_term_meta_value($term_id,'contract_clicks');
		$gdn_contract_clicks = get_term_meta_value($term_id,'gdn_contract_clicks');
		$is_full_cpc = !empty($contract_clicks) && !empty($gdn_contract_clicks);

		if($service_type == 'account_type' || $is_full_cpc)
		{
			$adword_data = array();
			foreach ($account_data as $timestamp => $accounts) 
			{	
				if(empty($accounts))
				{	
					$adword_data[] = array('datetime'=>$timestamp,'clicks'=>0,'invalidClicks'=>0);
					continue;
				}

				$_clicks = array_sum(array_column($accounts, 'clicks'));
				$_invalidClicks = array_sum(array_column($accounts, 'invalidClicks'));

				$adword_data[] = array('datetime'=>$timestamp,'clicks'=>$_clicks,'invalidClicks'=>$_invalidClicks);
			}
		}
		else if (!empty($contract_clicks)) 
		{
			$adword_data = array();
			foreach ($account_data as $timestamp => $accounts) 
			{							
				$accounts = array_filter($accounts,function($c){ 
                    if(empty($c['network']) || !stristr($c['network'], 'search')) 
                        return FALSE;
                    return TRUE;
                });

				if(empty($accounts))
				{	
					$adword_data[] = array('datetime'=>$timestamp,'clicks'=>0,'invalidClicks'=>0);
					continue;
				}

				$_clicks = array_sum(array_column($accounts, 'clicks'));
				$_invalidClicks = array_sum(array_column($accounts, 'invalidClicks'));
				$adword_data[] = array('datetime'=>$timestamp,'clicks'=>$_clicks,'invalidClicks'=>$_invalidClicks);
			}
		}
		else if(!empty($gdn_contract_clicks))
		{
			$adword_data = array();
			foreach ($account_data as $timestamp => $accounts) 
			{				
				$accounts = array_filter($accounts,function($c){ 
                    if(empty($c['network']) || !stristr($c['network'], 'display')) 
                        return FALSE;
                    return TRUE;
                });

				if(empty($accounts))
				{	
					$adword_data[] = (object) array('datetime'=>$timestamp,'clicks'=>0,'invalidClicks'=>0);
					continue;
				}

				$_clicks = array_sum(array_column($accounts, 'clicks'));
				$_invalidClicks = array_sum(array_column($accounts, 'invalidClicks'));
				$adword_data[] = array('datetime'=>$timestamp,'clicks'=>$_clicks,'invalidClicks'=>$_invalidClicks);
			}
		}

		$data['data'] = $adword_data;
		$data['status'] = 1;
		$this->default_response['auth'] = 1;
		return parent::render($data);
	}


	/**
	 * Gets the statistic.
	 *
	 * @param      integer  $term_id      The term identifier
	 * @param      <type>   $report_type  The report type
	 * @param      integer  $start_time   The start time
	 * @param      integer  $end_time     The end time
	 *
	 * @return     <type>   The statistic.
	 */
	function getStatistic($term_id = 0,$report_type = 'ACCOUNT_PERFORMANCE_REPORT', $start_time = FALSE, $end_time = FALSE)
	{
		if(!$this->authenticate) return parent::render();

		$this->default_response['auth'] = 1;

		$token		= parent::get('token');
		$hasDate 	= ! empty(parent::get('group_by_date'));

		$data = array('status' => 0, 'data' => array());

		// CODE CHECK TERM'OWNER
		$term = $this->googleads_m->select('term_id')->set_term_type()->get($term_id);
		if( ! $term) return parent::render($data);

		// Tiến hành đọc dữ liệu dựa trên các phân đoạn của hợp đồng
		$insight        = [];
        $adsSegments    = $this->term_posts_m->get_term_posts($term_id, $this->ads_segment_m->post_type);
        if(empty($adsSegments)) return parent::render($data);


		// PARSE THỜI GIAN LÁY DỮ LIỆU DỰA TRÊN CẤU HÌNH , HOẶC DỰA TRÊN THỜI GIAN HỢP ĐỒNG.
		$advertise_start_time 	= get_term_meta_value($term_id,'advertise_start_time') ?: get_term_meta_value($term_id, 'contract_begin');
		$advertise_start_time 	= start_of_day($advertise_start_time);

		$advertise_end_time 		= get_term_meta_value($term_id, 'advertise_end_time') ?: time();
		$advertise_end_time 		= end_of_day($advertise_end_time);

		$endOfYesterday = end_of_day(strtotime('yesterday'));
		$start_time = empty($start_time) ? $advertise_start_time : (($start_time < $advertise_start_time) ? $advertise_start_time : $start_time);

		$end_time = (empty($end_time) || $end_time > $advertise_end_time) ? $advertise_end_time : $end_time;
		$end_time = $end_time > $endOfYesterday ? $endOfYesterday : $end_time;
		$end_time = start_of_day($end_time);

		$start_time > $end_time AND $start_time = $end_time;
		// KẾT THÚC QUÁ TRÌNH PARSE THỜI GIAN HỢP ĐỒNG

        foreach ($adsSegments as $adsSegment)
        {
            $adaccount = $this->term_posts_m->get_post_terms($adsSegment->post_id, $this->mcm_account_m->term_type);
            if(empty($adaccount)) continue;

            $adaccount  = reset($adaccount);

            $_segment_start = $adsSegment->start_date;
            $_segment_end = $adsSegment->end_date ?: time();

            $_start = max($start_time, $_segment_start);
            $_end   = min($end_time, $_segment_end);

            if($_start > $_end) continue;

	        $this->base_adwords_m->set_mcm_account($adaccount->term_id);


	        // XỬ LÝ PHÂN LOẠI THỜI GIAN | KHÔNG THỜI GIAN TẠI ĐÂY
	        if($hasDate)
	        {
	        	$account_data = $this->base_adwords_m->get_cache($report_type, $_start, $_end);
	        	if(empty($account_data))
	        	{
	        		$this->base_adwords_m->download($report_type, $_start, $_end);
	        		$account_data = $this->base_adwords_m->get_cache($report_type, $_start, $_end);
	        	}

	        	$account_data AND $account_data = array_filter($account_data);
				if(empty($account_data)) continue;

				$insight = array_merge(array_merge(...$account_data), $insight);
				continue;
	        }

	        $_insight = $this->base_adwords_m->get_range_cache($report_type, $_start, $_end);
            if(empty($_insight))
            {
                $selectorFields = $this->base_adwords_m->init_selector_fields($report_type)->get_selector_fields();

                // REMOVE 'DATE' ATTRIBUTE
                if (($key = array_search('Date', $selectorFields)) !== false)
                	unset($selectorFields[$key]);

                // REMOVE 'AdNetworkType2' ATTRIBUTE
				if(($key = array_search('AdNetworkType2', $selectorFields)) !== FALSE)
					unset($selectorFields[$key]);

				// REMOVE 'AdNetworkType2' ATTRIBUTE
				if(($key = array_search('network', $selectorFields)) !== FALSE)
					unset($selectorFields[$key]);

                $this->base_adwords_m->set_selector_fields($selectorFields)->download($report_type, $_start, $_end);
                $_insight = $this->base_adwords_m->get_range_cache($report_type, $_start, $_end);
            }

            $_insight AND $insight = array_merge($_insight, $insight);
	    }

        if(empty($insight)) return parent::render($data);

        $insight = array_map(function($x) use($term_id){

			

        	$x['impressions']	= (int) $x['impressions'];
			$x['clicks'] 		= (int) $x['clicks'];
			$x['invalidClicks'] = (int) ($x['invalidClicks'] ?? 0);
			$x['avgCPC'] 		= (int) $x['avgCPC'];
			$x['cost'] 			= (int) $x['cost'];
			$x['conversions'] 	= (double) $x['conversions'];
			$x['allConv'] 		= (double) ($x['allConv'] ?? 0);
			
			if(empty($x['currency']) || $x['currency'] == 'VND') return $x;

            if('VND' == $x['currency']) return $x;

            $exchange_rate = get_term_meta_value($term_id, strtolower("exchange_rate_{$x['currency']}_to_vnd"));
			
            empty($exchange_rate) && $exchange_rate = get_exchange_rate($x['currency']);

			$x['cost'] 	= !empty($x['cost']) ? $x['cost'] * $exchange_rate : 0;
			$x['avgCPC'] = !empty($x['avgCPC']) ? $x['avgCPC'] * $exchange_rate : 0;
			return $x;

        }, $insight);

		$this->default_response['auth'] = 1;
		$data['data'] = $insight;
		return parent::render($data);
	}

    /**
	 * Gets the statistic v2
	 *
	 * @param      integer  $term_id      The term identifier
	 * @param      <type>   $report_type  The report type
	 * @param      integer  $start_time   The start time
	 * @param      integer  $end_time     The end time
	 *
	 * @return     <type>   The statistic.
	 */
	function getStatisticNew($term_id = 0, $start_time = FALSE, $end_time = FALSE)
	{
		if(!$this->authenticate) return parent::render();

		$this->default_response['auth'] = 1;

		$token		= parent::get('token');
		$hasDate 	= ! empty(parent::get('group_by_date'));

		$data = array('status' => 0, 'data' => array());

		// CODE CHECK TERM'OWNER
		$term = $this->googleads_m->select('term_id')->set_term_type()->get($term_id);
		if( ! $term) return parent::render($data);


        $insight = $this->term_m
            ->select("term.term_id as contractId")
            ->select("term.term_name as contractUrl")
            ->select("term.term_type as contractType")
            ->select("term.term_status as contractStatus")
            ->select("COALESCE(ads_segment.post_id, balance_spend.post_id) as segmentId")
            ->select("COALESCE(ads_segment.post_type, balance_spend.post_type) as segmentType")
            ->select("COALESCE(ads_segment.post_status, balance_spend.post_status) as segmentStatus")
            ->select("COALESCE(ads_segment.start_date, balance_spend.start_date) as segmentStartDate")
            ->select("COALESCE(ads_segment.end_date, balance_spend.end_date) as segmentEndDate")
            ->select("mcm_account.term_id as mcmAccountTermId")
            ->select("mcm_account.term_name as mcmAccountId")
            ->select("max(if(mcm_account_metadata.meta_key = 'account_name', mcm_account_metadata.meta_value, null)) as mcmAccountName")
            ->select("mcm_account.term_status as mcmAccountStatus")
            ->select("max(if(mcm_account_metadata.meta_key = 'source', mcm_account_metadata.meta_value, null)) as mcmAccountSource")
            ->select("insights.post_id as insightId")
            ->select("COALESCE(insights.post_name, balance_spend.post_name) as insightSegmentType")
            ->select("insights.post_type as insightType")
            ->select("insights.post_status as insightStatus")
            ->select("COALESCE(insights.start_date, balance_spend.start_date) as insightStartDate")
            ->select("COALESCE(max(if(mcm_account_metadata.meta_key = 'source', mcm_account_metadata.meta_value, null)), balance_spend.comment_status) as insightSource")
            ->select("max(if(insight_metadata.meta_key = 'account_currency', insight_metadata.meta_value, null)) as account_currency")
            ->select("max(if(insight_metadata.meta_key = 'clicks', insight_metadata.meta_value, null)) as clicks")
            ->select("max(if(insight_metadata.meta_key = 'impressions', insight_metadata.meta_value, null)) as impressions")
            ->select("max(if(insight_metadata.meta_key = 'conversions', insight_metadata.meta_value, null)) as conversions")
            ->select("COALESCE(max(if(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, null)), balance_spend.post_content) as spend")
            ->select("tp_contract_ads_segment.post_id")
            ->select("balance_spend.post_excerpt as note")

            ->join('term_posts as tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
            ->join('posts as balance_spend', 'balance_spend.post_id = tp_contract_ads_segment.post_id and balance_spend.post_type = "balance_spend"', 'left')
            ->join('posts as ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id and ads_segment.post_type = "ads_segment"', 'left')
            ->join('term_posts as tp_segment_mcm_account', 'tp_segment_mcm_account.post_id = ads_segment.post_id', 'left')
            ->join('term as mcm_account', 'tp_segment_mcm_account.term_id = mcm_account.term_id and mcm_account.term_type = "mcm_account"', 'left')
            ->join('termmeta as mcm_account_metadata', 'mcm_account_metadata.term_id = mcm_account.term_id and meta_key in ("source", "account_name")', 'left')
            ->join('term_posts as tp_mcm_account_insights', 'tp_mcm_account_insights.term_id = mcm_account.term_id', 'left')
            ->join('posts as insights', 'tp_mcm_account_insights.post_id = insights.post_id AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) AND insights.start_date <= if(ads_segment.end_date = 0 or ads_segment.end_date is null, UNIX_TIMESTAMP (), ads_segment.end_date) AND insights.post_type = "insight_segment" AND insights.post_name = "day"', 'left')
            ->join('postmeta insight_metadata', 'insight_metadata.post_id = insights.post_id and insight_metadata.meta_key in ("account_currency", "clicks", "impressions", "spend", "conversions")', 'left')

            ->where('term.term_id', $term_id)
            ->where('( insights.post_id > 0 or balance_spend.post_id > 0 )')
            ->group_by('term.term_id, tp_contract_ads_segment.post_id, mcm_account.term_id, insights.post_id')
            ->order_by('insightStartDate', 'DESC')
            ->as_array()
            ->get_all();
        
        if(empty($insight)) return parent::render($data);

        $insight = array_map(function($x) use($term_id){
        	$x['impressions']	= (int) $x['impressions'];
			$x['clicks'] 		= (int) $x['clicks'];
			$x['invalidClicks'] = (int) ($x['invalidClicks'] ?? 0);
			$x['avgCPC'] 		= (int) $x['avgCPC'];
			$x['cost'] 			= (int) $x['cost'];
			$x['conversions'] 	= (double) $x['conversions'];
			$x['allConv'] 		= (double) ($x['allConv'] ?? 0);

			if(empty($x['account_currency']) || $x['account_currency'] == 'VND' || empty($dollar_exchange_rate)) return $x;

			if('USD' != $x['account_currency'] || 'AUD' != $x['account_currency']) return $x;

            $dollar_exchange_rate = get_exchange_rate($x['account_currency'], $term_id);

			$x['cost'] 	= !empty($x['cost']) ? $x['cost']*$dollar_exchange_rate : 0;
			$x['avgCPC'] = !empty($x['avgCPC']) ? $x['avgCPC']*$dollar_exchange_rate : 0;
			return $x;

        }, $insight);

		$this->default_response['auth'] = 1;
		$data['data'] = $insight;
		return parent::render($data);
	}

    /**
	 * Gets the statistic sumary
	 *
	 * @param      integer  $term_id      The term identifier
	 * @param      <type>   $report_type  The report type
	 * @param      integer  $start_time   The start time
	 * @param      integer  $end_time     The end time
	 *
	 * @return     <type>   The statistic.
	 */
	function getStatisticSumary($term_id = 0)
	{
		if(!$this->authenticate) return parent::render();

		$this->default_response['auth'] = 1;

		$data = array('status' => 0, 'data' => array());

		// CODE CHECK TERM'OWNER
		$term = $this->googleads_m->select('term_id')->set_term_type()->get($term_id);
		if( ! $term) return parent::render($data);

        $insight = $this->term_m
            ->select("max(if(insight_metadata.meta_key = 'clicks', insight_metadata.meta_value, null)) as clicks")
            ->select("max(if(insight_metadata.meta_key = 'impressions', insight_metadata.meta_value, null)) as impressions")
            ->select("COALESCE(max(if(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, null)), balance_spend.post_content) as spend")

            ->join('term_posts as tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
            ->join('posts as balance_spend', 'balance_spend.post_id = tp_contract_ads_segment.post_id and balance_spend.post_type = "balance_spend"', 'left')
            ->join('posts as ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id and ads_segment.post_type = "ads_segment"', 'left')
            ->join('term_posts as tp_segment_mcm_account', 'tp_segment_mcm_account.post_id = ads_segment.post_id', 'left')
            ->join('term as mcm_account', 'tp_segment_mcm_account.term_id = mcm_account.term_id and mcm_account.term_type = "mcm_account"', 'left')
            ->join('termmeta as mcm_account_metadata', 'mcm_account_metadata.term_id = mcm_account.term_id and meta_key in ("source", "account_name")', 'left')
            ->join('term_posts as tp_mcm_account_insights', 'tp_mcm_account_insights.term_id = mcm_account.term_id', 'left')
            ->join('posts as insights', 'tp_mcm_account_insights.post_id = insights.post_id AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) AND insights.start_date <= if(ads_segment.end_date = 0 or ads_segment.end_date is null, UNIX_TIMESTAMP (), ads_segment.end_date) AND insights.post_type = "insight_segment" AND insights.post_name = "day"', 'left')
            ->join('postmeta insight_metadata', 'insight_metadata.post_id = insights.post_id and insight_metadata.meta_key in ("account_currency", "clicks", "impressions", "spend")', 'left')

            ->where('term.term_id', $term_id)
            ->where('( insights.post_id > 0 or balance_spend.post_id > 0 )')
            ->group_by('term.term_id, tp_contract_ads_segment.post_id, mcm_account.term_id, insights.post_id')
            ->get_all();
    
        if(empty($insight)) return parent::render($data);

        $clicks = array_sum(array_column($insight, 'clicks'));
		$impressions = array_sum(array_column($insight, 'impressions'));
		$spend = array_sum(array_column($insight, 'spend'));
        
		$avgCPC = div($spend,$clicks);

        $contract_budget = (int)get_term_meta_value($term_id, 'actual_budget');

        $sumary = [
            'term_id' => $term->term_id,
			'clicks' => $clicks,
			'impressions' => $impressions,
			'avgCPC' => $avgCPC,
			'usedBudget' => $spend,
			'balanceBudget' => $contract_budget - $spend,
			'totalBudget' => $contract_budget
        ];

		$this->default_response['auth'] = 1;
		$data['data'] = $sumary;
		return parent::render($data);
	}


	/**
	 * Gets the adcampaign rule.
	 *
	 * @param      integer  $adcampaign_id  The adcampaign identifier
	 *
	 * @return     <type>   The adcampaign rule.
	 */
	public function getAdcampaignRule($adcampaign_id = 0)
	{
		$response = array('status'=>FALSE,'msg'=>'Fail','data'=>array());
		$this->load->model('googleads/adcampaign_m');
		$adcampaign_cache_key = "modules/googleads/adsobjects/adcampaignid-{$adcampaign_id}";
		$adcampaign = $this->adcampaign_m->cache($adcampaign_cache_key)->get_by('post_slug',$adcampaign_id);

		$mcmaccounts = $this->term_posts_m->get_post_terms($adcampaign->post_id,'mcm_account');
		$term = $this->term_m
		->cache("modules/googleads/mcm_account/".md5(json_encode($mcmaccounts)).'_terms',300)
		->select('term.term_id')
		->join('termmeta','termmeta.term_id = term.term_id')
		->where('term_status','publish')
		->where('meta_key','mcm_client_id')
		->where('term_type','google-ads')
		->where_in('meta_value',array_column($mcmaccounts, 'term_id'))
		->get_by();

		$this->load->config('googleads/frauds_rules');
		$rules_default = $this->config->item('frauds_rules');
		$rules = unserialize(get_term_meta_value($term->term_id,'frauds_rules')) ?: array();
		$rules = wp_parse_args($rules,$rules_default);

		$response['status'] = TRUE;
		$response['msg'] = 'Xử lý thành công .';
		$response['data'] = $rules;
		return parent::renderJson($response);
	}

	/**
	 * Receive IP Risk from filtering Modules
	 *
	 * @return     JSON
	 */
	public function ipBlockListener()
	{
		$response 	= array('status'=>FALSE,'msg'=>'Failure','data'=>array());
		$default 	= array('ip'=>'','tracking_code'=>'','date'=>$this->mdate->startOfDay());
		$args 		= wp_parse_args($this->input->post(),$default);
		if(empty($args['ip']) || empty($args['tracking_code']))
		{
			$response['msg'] = 'Input parameter Invalid !';
			return parent::renderJson($response, 406);
		}


		$term = $this->googleads_m->select('term.term_id')->set_default_query()->join('termmeta',"termmeta.term_id = term.term_id AND meta_key='tracking_code' AND meta_value='{$args['tracking_code']}'")->get_by();
		if( ! $term)
		{
			$response['msg'] = 'Contract not found or deleted!';
			return parent::renderJson($response, 406);
		}

		$where = array(
			'log_type' => 'frauds_ip_blocked',
			'term_id' => $term->term_id,
			'log_title' => $args['ip'],
			'log_content >' => time(),
			'log_time_create >=' => my_date($this->mdate->startOfDay(), 'Y-m-d H:i:s'),
			'log_time_create <=' => my_date($this->mdate->endOfDay(), 'Y-m-d H:i:s'),
		);

		if($logs = $this->log_m->get_many_by($where))
		{
			$response['status'] = TRUE;
			$response['msg'] = 'IP has been blocked !';
			return parent::renderJson($response, 200);
		}

		# Check if mcm_account_id is exists .If TRUE continue get all campaigns else then return No Content
		$mcm_account_id	= get_term_meta_value($term->term_id, 'mcm_client_id');
		if( ! $mcm_account_id)
		{
			$response['msg'] = 'Google Adwords setting incorrect !';
			return parent::renderJson($response);
		}

		# Check adcampaign_id is belongs to mcm_account
		$account_campaigns	= $this->term_posts_m->get_term_posts($mcm_account_id, 'adcampaign');
		if(empty($account_campaigns))
		{
			$response['msg'] = 'Adwords Campaigns not found or deleted !';
			return parent::renderJson($response);
		}

		# ADD IP MUST BLOCKED TO ALL CAMPAIGNS VIA ADWORD API IPBLOCKED
		$this->load->model('googleads/ipblock_m');
		foreach ($account_campaigns as $adcampaign)
		{
			$this->ipblock_m->addIpBlock($mcm_account_id, $adcampaign->post_slug, $args['ip']);
		}

		
		# LOAD RECOVERY DAY FROM FRAUDS CLICKS RULES METADATA
		$frauds_rules = unserialize(get_term_meta_value($term->term_id, 'frauds_rules'));
		if(empty($frauds_rules))
		{
			$this->load->config('googleads/frauds_rules');
			$frauds_rules = $this->config->item('frauds_rules');
		}
		
		# WRITES LOG WITH TIME EXPIRED FOR EACH IP ADDRESS
		$recovery_days = $frauds_rules['recovery'] ?? NULL;
		$next_time_clear_ipblocked = is_numeric($recovery_days) ? strtotime("+{$recovery_days} days") : strtotime("+1 year");
		$this->log_m->insert(array(
			'log_type' => 'frauds_ip_blocked',
			'term_id' => $term->term_id,
			'log_time_create' => my_date(time(),'Y-m-d H:i:s'),
			'log_title' => $args['ip'],
			'log_content' => $next_time_clear_ipblocked,
			'log_status' => 'publish'
		));

		$response['status'] = TRUE;
		$response['msg']	= 'Successfully.';
		return parent::renderJson($response, 200);
	}


	/**
	 * Clear Ip blocked of terms
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function clearIpBlock()
	{
		$response 	= array('status' => FALSE, 'msg' => 'Failure', 'data' => array());

		$default 	= array('term_id' => 0, 'token' => parent::build_token());
		$args 		= wp_parse_args($this->input->post(),$default);

		/* Check token is a live */
		if( empty($args['term_id']) || parent::build_token() != $args['token'] )
		{
			$response['msg'] = 'Input parameter Invalid !';
			return parent::renderJson($response);
		}


		$term = $this->googleads_m->select('term_id')->set_term_type()->get($args['term_id']);
		if( ! $term)
		{
			$response['msg'] = 'Contract not found or deleted!';
			return parent::renderJson($response);
		}

		$frauds_rules = unserialize(get_term_meta_value($args['term_id'], 'frauds_rules'));
		if(empty($frauds_rules))
		{
			$this->load->config('googleads/frauds_rules');
			$frauds_rules = $this->config->item('frauds_rules');
		}

		$recovery_days = $frauds_rules['recovery'] ?? NULL;
		/* Nếu $recovery không phải là kiểu số => nghĩa là "không hết hạn" hoặc "xóa sau khi kết thúc dịch vụ" */
		if( ! is_numeric($recovery_days)) 
		{
			$response['msg'] = 'Không có quyền remove IP ra khỏi tài khoản quảng cáo do cấu hình.';
			return parent::renderJson($response);
		};

		
		/**
		 * Check if mcm_account_id is exists
		 * If TRUE continue get all campaigns else then return No Content
		 */
		$mcm_account_id	= get_term_meta_value($args['term_id'],'mcm_client_id');
		if( ! $mcm_account_id)
		{
			$response['msg'] = 'Google Adwords setting incorrect !';
			return parent::renderJson($response);
		}

		// Check adcampaign_id is belongs to mcm_account
		$account_campaigns	= $this->term_posts_m->get_term_posts($mcm_account_id, 'adcampaign');
		if(empty($account_campaigns))
		{
			$response['msg'] = 'Adwords Campaigns not found or deleted !';
			return parent::renderJson($response);
		}

		$adcampaign = reset($account_campaigns);

		$this->load->model('googleads/ipblock_m');
		$this->ipblock_m->init_credential($mcm_account_id);
		$ipBlocks = $this->ipblock_m->get_all_by($adcampaign->post_id);
		if(empty($ipBlocks))
		{
			$response['msg'] = 'Chiến dịch không tồn tại bất kỳ IP Exclude nào !';
			return parent::renderJson($response);
		}

		$result = $this->ipblock_m->remove_by($adcampaign->post_id, $ipBlocks);

		$this->load->model('log_m');
		$this->log_m->insert(array(
			'term_id'       => $args['term_id'],
            'log_title'     => "Remove All IP blocked out of campaign [{$adcampaign->post_name}]",
            'log_status'    => (int) $result,
            'log_content'	=> serialize($ipBlocks),
            'log_type'      => 'api_googleads_clearIpBlock',
            'log_time_create'   => my_date('Y-m-d H:i:s')
			)
		);

		if(empty($result))
		{
			$response['msg'] = 'Qúa trình xử lý không thành công !';
			return parent::renderJson($response);
		}
		
		$response['status'] = TRUE;
		$response['msg'] 	= 'Successfully.';
		return parent::renderJson($response);
	}

    /**
	 * Load ad account
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
    public function getAdAccount($contract_id)
    {
        if(!$this->authenticate) return parent::render();

		$this->default_response['auth'] = 1;

		$token		= parent::get('token');
		$hasDate 	= ! empty(parent::get('group_by_date'));

		$data = array('status' => 0, 'data' => array());

        // CODE CHECK TERM'OWNER
		$term = $this->googleads_m->select('term_id')->set_term_type()->get($contract_id);
		if( ! $term) return parent::render($data);

		// Get Ad account
		$ad_accounts = $this->googleads_m->set_term_type()
        ->join('term_posts AS tp_ads_segment', 'tp_ads_segment.term_id = term.term_id')
        ->join('posts AS ads_segment', 'ads_segment.post_id = tp_ads_segment.post_id AND ads_segment.post_type = "ads_segment"')
        ->join('term_posts AS tp_ad_accounts', 'tp_ad_accounts.post_id = ads_segment.post_id')
        ->join('term AS ad_accounts', 'ad_accounts.term_id = tp_ad_accounts.term_id AND ad_accounts.term_type = "mcm_account"')

        ->where('term.term_id', $contract_id)

        ->group_by('term.term_id')
        ->group_by('ads_segment.post_id')
        ->group_by('ad_accounts.term_id')

        ->select('ad_accounts.term_id AS ad_account_id')
        
        ->get_all();
        
		if( ! $ad_accounts) return parent::render($data);

        $ad_accounts = array_column($ad_accounts, 'ad_account_id');

		$this->default_response['auth'] = 1;
		$data['data'] = $ad_accounts;
		return parent::render($data);
    }
}
/* End of file Api.php */
/* Location: ./application/modules/googleads/controllers/Api.php */