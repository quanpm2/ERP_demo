<?php
class Googleads_Package extends Package
{
	const ROOT_ROLE_ID = 1;
	
	function __construct()
	{
		parent::__construct();
		
		$this->website_id  = $this->uri->segment(4);
	}

	public function name()
	{
		return 'Google Ads';
	}

	public function init()
	{
		$this->_load_menu();
		// $this->_update_permissions();
	}

	private function _load_menu()
	{
		if(!is_module_active('googleads')) return FALSE;

		$order 	= 0;
		$itemId = 'googleads';


		if(in_array($this->admin_m->role_id, [1, 5]))
		{
			$this->menu->add_item(array(
				'id' => 'googleads-report',
				'name' => 'B/c Spend GoogleAds',
				'parent' => null,
				'slug' => '#',
				'order' => $order++,
				'icon' => 'fa fa-buysellads  fa-lg',
			), 'navbar');

			$reportItems = array(
				[
					'id' => uniqid('googleads-report-'),
					'name' => 'Export B/c Spend tháng hiện tại',
					'url' => base_url('api-v2/googleads/DataReport/monthly?'. http_build_query([ 'month' => date('m'), 'year' => date('Y'), 'download' => true ]))
				],
				[
					'id' => uniqid('googleads-report-'),
					'name' => 'Export B/c Spend tháng ' . date('m', strtotime('-1 months')),
					'url' => base_url('api-v2/googleads/DataReport/monthly?'. http_build_query([ 'month' => date('m', strtotime('-1 month')), 'year' => date('Y', strtotime('-1 month')), 'download' => true ]))
				],
				[
					'id' => uniqid('googleads-report-'),
					'name' => 'Export B/c Spend tháng ' . date('m', strtotime('-2 months')),
					'url' => base_url('api-v2/googleads/DataReport/monthly?'. http_build_query([ 'month' => date('m', strtotime('-2 month')), 'year' => date('Y', strtotime('-2 month')), 'download' => true ]))
				]
			);

			foreach($reportItems as $reportItem)
			{
				$this->menu->add_item(array(
					'id' => $reportItem['id'],
					'name' => $reportItem['name'],
					'parent' => 'googleads-report',
					'slug' => $reportItem['url'],
					'order' => 101+$order++,
					'icon' => 'fa fa-buysellads fa-lg',
				), 'navbar');
			}

			$this->menu->add_item(array(
				'id' => 'googleads-report-1',
				'name' => 'Export B/c Spend tháng hiện tại',
				'parent' => 'googleads-report',
				'slug' => base_url('api-v2/googleads/MCCReport/monthly'),
				'order' => 101+$order++,
				'icon' => 'fa fa-buysellads  fa-lg',
			), 'navbar');

			$this->menu->add_item(array(
				'id' => 'googleads-report-' . date('m', strtotime('-1 months')),
				'name' => 'Export B/c Spend tháng ' . date('m', strtotime('-1 months')),
				'parent' => 'googleads-report',
				'slug' => base_url('api-v2/googleads/MCCReport/monthly?month='.date('m', strtotime('-1 months'))),
				'order' => 101+$order++,
				'icon' => 'fa fa-buysellads  fa-lg',
			), 'navbar');

			$this->menu->add_item(array(
				'id' => 'googleads-report-' . date('m', strtotime('-2 months')),
				'name' => 'Export B/c Spend tháng ' . date('m', strtotime('-2 months')),
				'parent' => 'googleads-report',
				'slug' => base_url('api-v2/googleads/MCCReport/monthly?month='.date('m', strtotime('-2 months'))),
				'order' => 101+$order++,
				'icon' => 'fa fa-buysellads  fa-lg',
			), 'navbar');
		}

		

		if(has_permission('Googleads.index.access'))
		{
			$this->menu->add_item(array(
				'id' => $itemId,
				'name' => 'Google Ads',
				'parent' => null,
				'slug' => admin_url('googleads'),
				'order' => $order++,
				'is_active' => is_module('googleads'),	
				'icon' => 'fa fa-buysellads fa-lg',
			), 'left-ads-service');
		}

		if(!is_module('googleads')) return FALSE;

		if(has_permission('Googleads.staffs.manage'))
		{
			$this->menu->add_item(array(
			'id' => 'googleads-index',
			'name' => 'Đang thực hiện',
			'parent' => $itemId,
			'slug' => admin_url('googleads'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-database'
			), 'left-ads-service');
		}

		if(has_permission('Googleads.staffs.manage'))
		{
			$this->menu->add_item(array(
			'id' => 'googleads-staffs',
			'name' => 'Phân công/Phụ trách',
			'parent' => $itemId,
			'slug' => admin_url('googleads/staffs'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-group'
			), 'left-ads-service');
		}

		if(has_permission('Googleads.convert.manage'))
		{
            $this->menu->add_item(array(
            'id' => 'googleads-convert-v2',
            'name' => 'Adword Editor Convert v2',
            'parent' => $itemId,
            'slug' => admin_url('googleads/convert'),
            'order' => 999,
            'icon' => 'fa fa-fw fa-xs fa-buysellads'
            ), 'left-ads-service');
		}

        if(has_permission('Googleads.utilities.access'))
        {
            $this->menu->add_item(array(
                'id' => 'googleads-utilities',
                'name' => 'Công cụ hỗ trợ',
                'parent' => $itemId,
                'slug' => admin_url('googleads/utilities'),
                'order' => 999,
                'icon' => 'fa fa-fw fa-xs fa-wrench'
                ), 'left-ads-service');
        }

		if(empty($this->website_id)) return FALSE;

		// $order = 0;
		$left_navs = array(

			'overview' => array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-xs fa-tachometer'),
			
			// 'fraudsclicks' => array(
			// 	'name' => 'Chặn click ảo',
			// 	'icon' => 'fa fa-fw fa-xs fa-shield'),

			'sms' => array(
				'name' => 'SMS',
				'icon' => 'fa fa-fw fa-xs fa-envelope-square'),
			'email' => array(
				'name' => 'EMAIL',
				'icon' => 'fa fa-fw fa-xs fa-envelope-o'),
			'manual_report' => array(
				'name' => 'Export báo cáo',
				'icon' => 'fa fa-fw fa-xs fa-cloud-download'),
			'kpi' => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-xs fa-heartbeat'),
			'setting' => array(
				'name' => 'Cấu hình dịch vụ',
				'icon' => 'fa fa-fw fa-xs fa-cogs'),
		);

		if(in_array($this->admin_m->role_id, array(1,7,15)))
		{
			$this->menu->add_item(array(
				'id' => 'googleads-performance',
				'name' => 'Dữ liệu báo cáo',
				'parent' => $itemId,
				'slug' => module_url("performance/{$this->website_id}"),
				'order' => $order++,
				'icon' => 'fa fa-fw fa-xs fa-file-excel-o'
			), 'left-ads-service');
		}

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission('Googleads.'.$method.'.access')) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

            $this->menu->add_item(array(
				'id' => 'googleads-'.$method,
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url($method.'/'.$this->website_id),
				'order' => $order++,
				'icon' => $icon
				), 'left-ads-service');
		}
	}

	private function _update_permissions()
	{
		$permissions = $this->init_permissions();

		if(empty($permissions)) return FALSE;

		foreach($permissions as $name => $value)
		{
			$description 	= $value['description'];
			$actions 		= $value['actions'];

			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function title(){

		return 'Google Ads';
	}

	public function author(){

		return 'Thonh';
	}

	public function version(){

		return '0.1';
	}

	public function description(){

		return 'Google Ads';
	}

	private function init_permissions(){
		
		$permissions = array(
			'Module.Googleads' => array(
				'description' => '',
				'actions' => array('manage')
			),
			'Googleads.Index' => array(
				'description' => '',
				'actions' => array('access','manage','mdeparment','mgroup')
			),
			'Googleads.FraudsClicks' => array(
				'description' => 'Chặn click ảo',
				'actions' => array('access','add','delete','update','manage','mdeparment','mgroup')
			),
			'Googleads.Overview' => array(
				'description' => '',
				'actions' => array('access','manage','mdeparment','mgroup')
			),
			'Googleads.Setting' => array(
				'description' => '',
				'actions' => array('access','update','manage','mdeparment','mgroup')
			),
			'Googleads.Email' => array(
				'description' => '',
				'actions' => array('access','update','manage','mdeparment','mgroup')
			),
			'Googleads.Sms' => array(
				'description' => '',
				'actions' => array('access','update','manage','mdeparment','mgroup')
			),
			'Googleads.Manual_report' => array(
				'description' => '',
				'actions' => array('access','manage','mdeparment','mgroup')
			),
			'Googleads.staffs' => array(
				'description' => 'Bảng phân công phụ trách thực hiện',
				'actions' => array('access','manage','mdeparment','mgroup')
			),

			'Googleads.start_service' => array(
				'description' => 'Kích hoạt/thực hiện dịch vụ',
				'actions' => array('update','manage','mdeparment','mgroup')
			),

			'Googleads.stop_service' => array(
				'description' => 'Kết thúc dịch vụ',
				'actions' => array('update','manage','mdeparment','mgroup')
			),

			'Googleads.Contract' => array(
				'description' => '',
				'actions' => array('access','add','delete','update')
			),

			'Googleads.Kpi' => array(
				'description' => 'Quản lý KPI',
				'actions' => array('manage','access','add','delete','update','mdeparment','mgroup')
			),

			'Googleads.Convert' => array(
				'description' => 'Adword Editor Convert tool',
				'actions' => array('access','update','manage')
			),

            'Googleads.Utilities' => array(
				'description' => 'Công cụ hỗ trợ',
				'actions' => array('manage','access','add','delete','update','mdeparment','mgroup'),
			),

			'Googleads.metrics_core' => array(
				'description' => 'Quyền update các dữ liệu nâng cao',
				'actions' => array('update','manage')
			),

            'Googleads.unjoin_contract' => array(
				'description' => 'Quyền gỡ nối hợp đồng',
				'actions' => array('access', 'update', 'manage')
			),

            'Googleads.rejoin_contract' => array(
				'description' => 'Quyền fix nối hợp đồng',
				'actions' => array('access', 'update', 'manage')
			),

            'Googleads.fix' => array(
                'description' => 'Quyền fix dữ liệu (Chỉ dành cho admin)',
                'actions' => array('access', 'update', 'manage')
            ),
		);

		return $permissions;
	}

	/**
	 * Install module
	 *
	 * @return     bool  status of command
	 */
	public function install()
	{
		$permissions = $this->init_permissions();
		if( ! $permissions) return FALSE;

		$rootPermissions = $this->role_permission_m->get_by_role_id(self::ROOT_ROLE_ID);
		$rootPermissions AND $rootPermissions = array_column($rootPermissions, 'role_id', 'permission_id');

		foreach($permissions as $name => $value)
		{
			$description   = $value['description'];
			$actions 	   = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);

			if( ! empty($rootPermissions[$permission_id])) continue;

			$this->role_permission_m->insert([
				'permission_id'	=> $permission_id,
				'role_id'		=> self::ROOT_ROLE_ID,
				'action'		=> serialize($actions)
			]);
		}

		return TRUE;
	}

	public function uninstall(){

		$permissions = $this->init_permissions();

		if(!$permissions) return false;

		foreach($permissions as $name => $value){

			$this->permission_m->delete_by_name($name);
		}
	}
}