<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Google\AdsApi\AdWords\v201809\cm\Campaign;
use Google\AdsApi\AdWords\v201809\cm\CampaignOperation;
use Google\AdsApi\AdWords\v201809\cm\CampaignCriterionService;

use Google\AdsApi\AdWords\v201809\cm\IpBlock;
use Google\AdsApi\AdWords\v201809\cm\NegativeCampaignCriterion;
use Google\AdsApi\AdWords\v201809\cm\CampaignCriterionOperation;

use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\Common\OAuth2TokenBuilder;

class IpBlock_m extends Base_Model {

	const PAGE_LIMIT = 500;

	public $adWordsSession = NULL;

	function __construct()
	{
		parent::__construct();

		$this->load->model('googleads/mcm_account_m');
		$this->load->config('googleads/credentials');
	}

	/**
	 * Init Session from Credentital
	 *
	 * @param      string  $refreshToken      The refresh token
	 * @param      string  $clientCustomerId  The client customer identifier
	 *
	 * @return     self    ( description_of_the_return_value )
	 */
	protected function init_credential($mcm_account_id = 0)
	{
		$mcmAccount = $this->mcm_account_m->set_term_type()->get($mcm_account_id); // Get mcm_account in DB
  		if(empty($mcmAccount)) return FALSE;

        $mcmClientId	= 1402; // systemapi@adsplus.vn
        $refreshToken	= get_user_meta_value($mcmClientId,'rtoken'); // API token of mcmClientId
        if(empty($refreshToken)) return FALSE; // IF empty , no reason to continue , break

	    $oAuth2Credential = (new OAuth2TokenBuilder())
        ->withClientId($this->config->item('clientId','mcm'))
        ->withClientSecret($this->config->item('clientSecret','mcm'))
        ->withRefreshToken($refreshToken)
        ->build();

        $this->adWordsSession = (new AdWordsSessionBuilder())
        ->withDeveloperToken(getenv('GOOGLEADS_DEVELOPER_TOKEN'))
        ->withClientCustomerId($mcmAccount->term_name)
        ->withOAuth2Credential($oAuth2Credential)
        ->build();

        return $this;
	}


	/**
	 * Gets all by.
	 *
	 * @param      integer  $adCampaignId  The ad campaign identifier
	 *
	 * @return     <type>   All by.
	 */
	public function get_all_by($adCampaignId = 0)
	{
		if(empty($this->adWordsSession)) return FALSE;

		$ipBlocks = array();
		$campaignCriterionService = (new AdWordsServices())->get($this->adWordsSession, CampaignCriterionService::class);

		try
        {
		    $query = 'SELECT Id, IpAddress,CriteriaType,CampaignCriterionStatus WHERE CriteriaType = IP_BLOCK'; // Create AWQL query.

			// Create paging controls.
			$offset = 0;
			$totalNumEntries = 0;

			do 
			{
				$page_query = sprintf('%s LIMIT %d,%d', $query, $offset, self::PAGE_LIMIT);
				
				$page = $campaignCriterionService->query($page_query); // Make the query request.

				
				if ($page->getEntries() !== NULL) // Display results from the query.
				{
					$totalNumEntries = $page->getTotalNumEntries();
					foreach ($page->getEntries() as $object)
					{
						$criterion 	= $object->getCriterion();
						$ipBlock 	= ['id' => $criterion->getId(), 'type' => $criterion->getType(), 'ipAddress' => $criterion->getIpAddress()];
						array_push($ipBlocks, $ipBlock);
					}
				}

				$offset += self::PAGE_LIMIT;

			} while ($offset < $totalNumEntries);

        }
        catch (Exception $e) { prd($e->getMessage()); return FALSE; }

        if(empty($ipBlocks)) return FALSE;

        return  array_column($ipBlocks, NULL,'id');
	}


	/**
	 * Removes all ip block.
	 *
	 * @param      integer  $mcm_account_id  The mcm account identifier
	 * @param      integer  $adCampaignId    The ad campaign identifier
	 */
	public function remove_by($adCampaignId = 0, $ipBlocks = array())
	{
		if(empty($this->adWordsSession) || empty($ipBlocks) || empty($adCampaignId)) return FALSE;

		$this->load->model('googleads/adcampaign_m');
        $adcampaign = $this->adcampaign_m->select('post_id,post_slug')->set_post_type()->get($adCampaignId);
        if( ! $adcampaign) return FALSE;

        $campaignCriterionService = (new AdWordsServices())->get($this->adWordsSession, CampaignCriterionService::class);

		foreach($ipBlocks as $item)
		{
			$ipBlock = new IpBlock();
			$ipBlock->setId($item['id']);
			$ipBlock->setIpAddress($item['ipAddress']);

			// Add a negative campaign criterion.
			$negativeCriterion = new NegativeCampaignCriterion();
			$negativeCriterion->setCampaignId($adcampaign->post_slug); // Set campaign ID
			$negativeCriterion->setCriterion($ipBlock);

			$operation = new CampaignCriterionOperation();
			$operation->setOperator(Operator::REMOVE);
			$operation->setOperand($negativeCriterion);

			try
			{	
				$result = $campaignCriterionService->mutate([$operation]);
			} 
			catch (Exception $e)
			{
				log_message('error', "({$item['id']}) ".$e->getMessage());
				continue;
			}
		}

		return TRUE;
	}


		/**
	 * Adds an ip block.
	 *
	 * @param      integer  $mcm_account_id  The mcm account identifier
	 * @param      integer  $adCampaignId    The ad campaign identifier
	 * @param      array    $ip_addresses    The ip addresses
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function addIpBlock($mcm_account_id = 0, $adCampaignId = 0,$ip_addresses = array())
	{
		is_array($ip_addresses) OR $ip_addresses = explode(',', $ip_addresses);
		if(empty($ip_addresses)) return FALSE;

		$this->load->model('googleads/mcm_account_m');
		$mcmAccount = $this->mcm_account_m->set_term_type()->get($mcm_account_id); // Get mcm_account in DB
  		if(empty($mcmAccount)) return FALSE;
        
        $mcmClientId	= 1402; // systemapi@adsplus.vn
        $refreshToken	= get_user_meta_value($mcmClientId,'rtoken'); // API token of mcmClientId
        if(empty($refreshToken)) return FALSE; // IF empty , no reason to continue , break

        $this->load->model('googleads/adcampaign_m');
        $adcampaign = $this->adcampaign_m->set_post_type()->get($adCampaignId);
        if( ! $adcampaign) return FALSE;

		$operations = array();
        $campaign 	= NULL;
        $session 	= $this->init_credential($refreshToken, $mcmAccount->term_name);
        $campaignCriterionService = (new AdWordsServices())->get($session, CampaignCriterionService::class);

		foreach($ip_addresses as $ip)
		{
			$ipBlock = new IpBlock();
			$ipBlock->setIpAddress(trim($ip));

			// Add a negative campaign criterion.
			$negativeCriterion = new NegativeCampaignCriterion();
			$negativeCriterion->setCampaignId($adcampaign->post_slug); // Set campaign ID
			$negativeCriterion->setCriterion($ipBlock);

			$operation = new CampaignCriterionOperation();
			$operation->setOperator(Operator::ADD);
			$operation->setOperand($negativeCriterion);

			try
			{	
				$result = $campaignCriterionService->mutate([$operation]);
			} 
			catch (Exception $e)
			{
				log_message('error', "({$ip}) ".$e->getMessage());
				continue;
			}
		}

		return TRUE;
	}

	/**
	 * { function_description }
	 *
	 * @param      integer  $mcm_account_id  The mcm account identifier
	 * @param      integer  $adCampaignId    The ad campaign identifier
	 * @param      array    $ip_addresses    The ip addresses
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function mutateIpBlock($mcm_account_id = 0, $adCampaignId = 0,$ip_addresses = array())
	{
		$this->load->model('googleads/mcm_account_m');
		$mcmAccount = $this->mcm_account_m->set_term_type()->get($mcm_account_id); // Get mcm_account in DB
  		if(empty($mcmAccount)) return FALSE;
        
        $mcmClientId	= 1402; // systemapi@adsplus.vn
        $refreshToken	= get_user_meta_value($mcmClientId,'rtoken'); // API token of mcmClientId
        if(empty($refreshToken)) return FALSE; // IF empty , no reason to continue , break
        $adcampaign = $this->adcampaign_m->set_post_type()->get($adCampaignId);
        if( ! $adcampaign) return FALSE;

        $campaign 	= NULL;
        $session 	= $this->init_credential($refreshToken, $mcmAccount->term_name);
        $campaignCriterionService = (new AdWordsServices())->get($session, CampaignCriterionService::class);

        // $IpBlock = new IpBlock($adcampaign->post_slug);
        
        $ip = '115.79.42.123';
        $ipBlock = new IpBlock(1,'IP_BLOCK','IpBlock');
		prd($ipBlock);

        prd($campaignCriterionService);
        prd(func_get_args());
        die('paused !!!');
		
		# SOLUTION 2 : construct campaign object
		$campaign = new Campaign($adcampaign->post_slug);

		$campaign->setTrackingUrlTemplate($trackingUrlTemplate);
		// Create an ad group operation.
	    $operation = new CampaignOperation();
	    $operation->setOperand($campaign);
	    $operation->setOperator(Operator::SET);

	    try
	    {
		    $result = $campaignService->mutate([$operation]);
	    	update_post_meta($adCampaignId,'trackingUrlTemplate',$trackingUrlTemplate);
	    	return $trackingUrlTemplate;
	    }
	    catch (Exception $e)
	    {
	    	return FALSE;	
	    }

	    return TRUE;
	}
}
/* End of file IpBlock.php */
/* Location: ./application/modules/googleads/models/IpBlock.php */