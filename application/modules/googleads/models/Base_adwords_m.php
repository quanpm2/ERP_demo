<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\Common\SoapSettingsBuilder;
use Google\AdsApi\AdWords\ReportSettingsBuilder;

use Google\AdsApi\AdWords\Reporting\v201809\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinitionDateRangeType;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloader;

use Google\AdsApi\AdWords\v201809\cm\LocationCriterionService;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201809\cm\ReportDefinitionReportType;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\DateRange;
use Google\AdsApi\Common\OAuth2TokenBuilder;

class Base_adwords_m extends Base_model 
{
    public      $user, $ci;
    public      $selector_fields    = array();
    private     $refresh_token      = NULL;
    private     $client_customer_id = NULL;
    private     $mcm_account_id     = 0;

    protected   $client_id = '';

    function __construct()
    {
        parent::__construct();

        $this->load->library('googleads/adwords');
        $this->load->library('excel');
        $this->load->library('email');
        $this->load->config('googleads/adwords');
        $this->load->model('googleads/mcm_client_m');
        $this->load->model('googleads/mcm_account_m');
        $this->load->model('googleads/mcm_account_clients_m');
        $this->load->model('googleads/insight_segment_m');

        $this->user = new Adwords();
    }

    public function get_client_customer_id()
    {
        return $this->client_customer_id;
    }

    public function set_client_customer_id($client_customer_id)
    {
        $this->client_customer_id = $client_customer_id;
        return $this;
    }


    /**
     * Sets the mcm account to Get Refresh Tokens of mcm_account
     *
     * @param      integer  $mcm_account_id  The mcm account identifier
     * @param      bool     $invoke_token    The invoke token, IF TRUE then then
     *                                       retrieve all relates Refresh TOken,
     *                                       otherwise return $this
     *
     * @return     self
     */
    public function set_mcm_account($mcm_account_id = 0, $invoke_token = TRUE)
    {
        /* Clean all property last used */
        $this->set_client_customer_id('');
        $this->refresh_token = '';

        # Load mcm_account_id to check 
        $mcm_account = $this->mcm_account_m->select('term_id,term_name')->set_term_type()->get($mcm_account_id);
        if( ! $mcm_account) return FALSE;

        log_message('debug', 'AdAccount Loaded : ' . json_encode([
            'contract' => $mcm_account
        ]));

        $this->mcm_account_id = $mcm_account_id;
        $this->set_client_customer_id($mcm_account->term_name);

        if( ! $invoke_token) return $this;

        $mcm_account_clients = $this->mcm_account_clients_m->get_mcm_account_clients($mcm_account->term_id);
        if(empty($mcm_account_clients)) return FALSE;

        $refresh_tokens = array();
        foreach ($mcm_account_clients as $mcm_account_client)
        {
            $mcm_refresh_token = get_user_meta_value($mcm_account_client['user_id'],'rtoken');
            if(empty($mcm_refresh_token)) continue;

            $refresh_tokens[$mcm_account_client['user_id']] = $mcm_refresh_token;

            // log_message('debug', 'Client Token Loaded : ' . json_encode([
            //     'token' => $mcm_refresh_token,
            //     'client' => $mcm_account_client
            // ]));
        }
        if(empty($refresh_tokens)) 
        {
            $this->termmeta_m->delete_meta($mcm_account_id, 'last_active_mcm_account_id');
            return FALSE;
        }
        
        # Check Priority For Active Refresh Token Last Used
        $last_active_mcm_account_id = get_term_meta_value($mcm_account_id, 'last_active_mcm_account_id');
        if( ! empty($last_active_mcm_account_id) && ! empty($refresh_tokens[$last_active_mcm_account_id]))
        {
            $last_active_item = array($last_active_mcm_account_id => $refresh_tokens[$last_active_mcm_account_id]);
            unset($refresh_tokens[$last_active_mcm_account_id]);

            $refresh_tokens = $last_active_item + $refresh_tokens;
        }

        $this->refresh_token = $refresh_tokens;
    }

    public function set_user_adword($adword_id = 0)
    {
        $this->user->SetClientCustomerId($adword_id);
        $this->client_id = $adword_id;
        return $this;
    }

    public function init_selector_fields($report_type = 'report_type')
    {
        $selector_fields = $this->config->item('selector');
        if(empty($selector_fields[$report_type])) return FALSE;

        $this->selector_fields = $selector_fields[$report_type];
        return $this;
    }

    public function get_selector_fields()
    {
        return $this->selector_fields;
    }

    function set_selector_fields($fields)
    {
        $this->selector_fields = $fields;
        return $this;
    }

    function clear_selector_fields()
    {
        $this->selector_fields = array();
        return $this;
    }


    function download($report_type, $start_time, $end_time = FALSE, $contract_id = 0)
    {
        if(empty($this->refresh_token)) return FALSE;

        $client_customer_id = $this->get_client_customer_id();
        if(empty($client_customer_id)) return FALSE;

        $this->load->config('googleads/credentials');

        $refresh_tokens = $this->refresh_token;

        foreach ($refresh_tokens as $key => $token) 
        {
            try 
            {
                log_message('debug', json_encode([
                    'client_customer_id' => $client_customer_id,
                    'refresh_token' => $token
                ]));

                $oAuth2Credential = (new OAuth2TokenBuilder())
                ->withClientId($this->config->item('clientId', 'mcm'))
                ->withClientSecret($this->config->item('clientSecret', 'mcm'))
                ->withRefreshToken($token)
                ->build();

                $reportSettings = (new ReportSettingsBuilder())->includeZeroImpressions(false)->build();

                // $soapSettings = (new SoapSettingsBuilder())
                // ->disableSslVerify()
                // ->build();

                $session = (new AdWordsSessionBuilder())
                ->withDeveloperToken($this->config->item('googleadsDeveloperToken'))
                ->withClientCustomerId($client_customer_id)
                ->withOAuth2Credential($oAuth2Credential)
                // ->withSoapSettings($soapSettings)
                ->withReportSettings($reportSettings)
                ->build();

                $selector = new Selector();
                $selector_fields = $this->get_selector_fields();
                if(empty($selector_fields)) $this->init_selector_fields($report_type);
                $selector->setFields($this->get_selector_fields());

                // Use a predicate to filter out paused criteria (this is optional).
                $end_time = $end_time ?: $start_time;
                $selector->setDateRange(new DateRange(my_date($start_time, 'Ymd'), my_date($end_time, 'Ymd')));

                $reportDefinition = new ReportDefinition();
                $reportDefinition->setSelector($selector);
                $reportDefinition->setReportName(uniqid());
                $reportDefinition->setDateRangeType(ReportDefinitionDateRangeType::CUSTOM_DATE);

                $reportDefinition->setReportType($report_type);
                $reportDefinition->setDownloadFormat(DownloadFormat::XML);

                $reportDownloader = new ReportDownloader($session);
                $reportDownloadResult = $reportDownloader->downloadReport($reportDefinition);
                $xml_data = $reportDownloadResult->getAsString();
                
                $this->write_cache($xml_data,$report_type,$start_time,$end_time);
                $this->write_db($xml_data, $report_type, $start_time, $end_time, $contract_id);
                
                $this->clear_selector_fields();

                /* Log refresh token for next call */
                if( ! empty($this->mcm_account_id))
                {   
                    update_term_meta($this->mcm_account_id, 'last_active_mcm_account_id', $key);
                }
            } 
            catch (Exception $e) 
            {
                log_message('error', $e->getMessage());
                continue;
            }

            return $xml_data;       
        }

        $this->clear_selector_fields();
    }

    public function getReportAsString($filePath){
        $content = simplexml_load_file($filePath);

        return $content;
    }

    public function write_db($xml_data, $report_type, $start_time, $end_time = FALSE, $contract_id = 0)
    {
        if($report_type != 'ACCOUNT_PERFORMANCE_REPORT') return TRUE;

        $insights = $this->convert_xml2array($xml_data);
        if(empty($insights)) 
        {
            throw new Exception('xml data has null data or parse fail !', 1);
            log_message('error', 'xml data has null data or parse fail !');
            return FALSE;
        }

        $client_customer_id = $this->get_client_customer_id();
        empty($client_customer_id) AND trigger_error('client_customer_id is NULL !',E_USER_ERROR);

        $insights = array_group_by($insights, 'day');
        
        $insights = array_map(function($_insights){

            $_insight = reset($_insights);

            return [
                'currency' => $_insight['currency'],
                'account' => $_insight['account'],
                'day' => $_insight['day'],
                
                'impressions' => array_sum(array_column($_insights, 'impressions')),
                'clicks' => array_sum(array_column($_insights, 'clicks')),
                'invalidClicks' => array_sum(array_column($_insights, 'invalidClicks')),
                'cost' => array_sum(array_column($_insights, 'cost')),
                'avgCPC' => div(array_sum(array_column($_insights, 'cost')), array_sum(array_column($_insights, 'clicks'))),
                'conversions' => array_sum(array_column($_insights, 'conversions')),
                'allConv' => array_sum(array_column($_insights, 'allConv')),
                'network' => 'all',
            ];

        }, $insights);

        $first_row = reset($insights);
        $has_date = isset($first_row['day']);
        if( ! $has_date) return true;

        $days = array_unique(array_column($insights, 'day'));

        $_isegments = $this->term_posts_m->get_term_posts($this->mcm_account_id, $this->insight_segment_m->post_type, [
            'fields' => 'posts.post_id, posts.end_date',
            'where' => [
                'end_date >=' => start_of_day(min($days)),
                'end_date <=' => end_of_day(max($days)),
                'post_name' => 'day'
            ]
        ]);

        $_isegments AND $_isegments = array_column($_isegments, null, 'end_date');

        $this->load->model('option_m');
        $exchange_rate_option = $this->option_m->get_value('exchange_rate', TRUE);
        foreach($insights as $data)
        {
            $datetime = start_of_day($data['day']);
            $_isegment = $_isegments[$datetime] ?? null;
            
            if(empty($_isegment))
			{
				$_isegmentInsert 	= [
					'post_name' 	=> 'day',
					'post_type' 	=> 'insight_segment',
					'start_date' 	=> $datetime,
					'end_date' 		=> $datetime
				];

				$_isegment_id = $this->insight_segment_m->insert($_isegmentInsert);
				$this->term_posts_m->set_term_posts($this->mcm_account_id, [$_isegment_id], $this->insight_segment_m->post_type);

				$_isegment = (object) $_isegmentInsert;
				$_isegment->post_id = $_isegment_id;
			}

            $data['spend'] = div($data['cost'], 1000000);
            $data['cpc'] = div($data['avgCPC'], 1000000);

            $data['account_currency'] = $data['currency'];
            if('VND' != $data['account_currency']){
                $exchange_rate = (int)get_post_meta_value($_isegment->post_id, 'exchange_rate');
                empty($exchange_rate) AND $exchange_rate = (double) get_term_meta_value($contract_id, strtolower("exchange_rate_{$data['account_currency']}_to_vnd"));
                empty($exchange_rate) AND $exchange_rate = (double) $exchange_rate_option[strtolower("{$data['account_currency']}_to_vnd")] ?? 0;

                $data['exchange_rate'] = $exchange_rate;
                $data['spend'] = (double)$data['spend'] * $exchange_rate;
            }
            else
            {
                $data['exchange_rate'] = 1;
            }

            $item_array = ['account_currency', 'spend', 'cost', 'cpc', 'impressions', 'clicks', 'invalidClicks', 'conversions', 'network', 'exchange_rate'];
            $data = elements($item_array, $data);
            
            foreach($data as $key => $value)
            {
                update_post_meta($_isegment->post_id, $key, (string) $value);
            }
        }

        return true;
    }

    public function write_cache($xml_data,$report_type,$start_time,$end_time = FALSE)
    {
        $data = $this->convert_xml2array($xml_data);
        if(empty($data)) 
        {
            throw new Exception('xml data has null data or parse fail !', 1);
            log_message('error', 'xml data has null data or parse fail !');
            return FALSE;
        }

        $client_customer_id = $this->get_client_customer_id();
        if(empty($client_customer_id)) 
        {
            trigger_error('client_customer_id is NULL !',E_USER_ERROR);
        }
        $first_row = reset($data);
        $has_date = isset($first_row['day']);
        if($has_date)
        {
            $data_with_key_date = array();
            foreach ($data as $row) 
            {
                $time = $this->mdate->startOfDay($row['day']);

                if(!isset($data_with_key_date[$time]))
                    $data_with_key_date[$time] = array();

                $data_with_key_date[$time][] = $row;
            }

            foreach ($data_with_key_date as $timestamp => $dat) 
            {
                $year = my_date($timestamp,'Y');
                $month = my_date($timestamp,'m');
                $day = my_date($timestamp,'d');

                $cache_name = "google-adword/{$client_customer_id}/{$year}/{$month}/{$day}/{$report_type}";
                $this->scache->write($dat,$cache_name);
            }
            return TRUE;
        }

        /* Cache data with no 'day' field */
        $start_time = $this->mdate->startOfDay($start_time);
        $end_time = $end_time ?: $start_time;
        $end_time = $this->mdate->startOfDay($end_time);

        $is_one_date = ($start_time == $end_time);
        if($is_one_date)
        {
            $year = my_date($start_time,'Y');
            $month = my_date($start_time,'m');
            $day = my_date($start_time,'d');

            $cache_name = "google-adword/{$client_customer_id}/{$year}/{$month}/{$day}/{$report_type}";
            $this->scache->write($data,$cache_name);
            return TRUE;
        }

        $cache_name = "google-adword/{$client_customer_id}/date_range/{$start_time}_{$end_time}/{$report_type}";
        $this->scache->write($data,$cache_name);

        return TRUE;
    }

    public function get_cache($report_type,$start_time,$end_time = FALSE)
    {
        $data = array();
        $start_time = $this->mdate->startOfDay($start_time);
        $end_time = $end_time?:$start_time;
        $end_time = $this->mdate->startOfDay($end_time);
        $is_one_date = ($start_time == $end_time);

        if($is_one_date)
        {
            $client_customer_id = $this->get_client_customer_id();
            if(empty($client_customer_id)) return $data;

            $year = my_date($start_time,'Y');
            $month = my_date($start_time,'m');
            $day = my_date($start_time,'d');
            $cache_name = "google-adword/{$client_customer_id}/{$year}/{$month}/{$day}/{$report_type}";
            $data[$start_time] = $this->scache->get($cache_name);

            return $data;
        }

        $end_time = $this->mdate->endOfDay($end_time);

        while ($start_time < $end_time) 
        {
            $data+= $this->get_cache($report_type,$start_time);
            $start_time = strtotime('+1 day',$start_time);
        }

        return $data;        
    }

    public function get_db($term_id, $start_time, $end_time = FALSE){
        $start_time = $this->mdate->startOfDay($start_time);
        $end_time = $end_time?:$start_time;
        $end_time = $this->mdate->startOfDay($end_time);

        $data = $this->mcm_account_m
            ->set_term_type()
            ->where('term.term_id', $term_id)

            ->select('posts.post_id, posts.end_date')
            ->select('term.term_id')
            ->select('term.term_name AS account_id')
            ->select("MAX(IF(termmeta.meta_key = 'account_name',`termmeta`.`meta_value`, NULL)) AS account_name")
            ->select("DATE_FORMAT(FROM_UNIXTIME(posts.end_date), '%Y-%m-%d') AS day")
            ->select("MAX(IF(postmeta.meta_key = 'account_currency',`postmeta`.`meta_value`, NULL)) AS currency")
            ->select("MAX(IF(postmeta.meta_key = 'spend',`postmeta`.`meta_value`, NULL)) AS spend")
            ->select("MAX(IF(postmeta.meta_key = 'cost',`postmeta`.`meta_value`, NULL)) AS cost")
            ->select("MAX(IF(postmeta.meta_key = 'cpc',`postmeta`.`meta_value`, NULL)) AS cpc")
            ->select("MAX(IF(postmeta.meta_key = 'impressions',`postmeta`.`meta_value`, NULL)) AS impressions")
            ->select("MAX(IF(postmeta.meta_key = 'clicks',`postmeta`.`meta_value`, NULL)) AS clicks")
            ->select("MAX(IF(postmeta.meta_key = 'invalidClicks',`postmeta`.`meta_value`, NULL)) AS invalidClicks")
            ->select("MAX(IF(postmeta.meta_key = 'conversions',`postmeta`.`meta_value`, NULL)) AS conversions")
            ->select("MAX(IF(postmeta.meta_key = 'network',`postmeta`.`meta_value`, NULL)) AS network")
            
            ->join('term_posts', 'term_posts.term_id = term.term_id')
            ->join('posts', 'posts.post_id = term_posts.post_id')
            ->join('termmeta', "termmeta.term_id = term.term_id AND termmeta.meta_key IN ('account_name')", 'LEFT')
            ->join('postmeta', "postmeta.post_id = posts.post_id AND postmeta.meta_key IN ('account_currency' , 'spend', 'cost', 'cpc', 'impressions', 'clicks', 'invalidClicks', 'conversions', 'network')", 'LEFT')

            ->where('posts.end_date >=', $start_time)
            ->where('posts.end_date <=', $end_time)

            ->group_by('posts.post_id, day, posts.end_date')

            ->as_array()
            ->get_all();
        // dd($mcm_account);

        // while ($start_time < $end_time) 
        // {
        //     $data+= $this->get_cache($report_type,$start_time);
        //     $start_time = strtotime('+1 day',$start_time);
        // }

        return $data;
    }

    public function get_range_cache($report_type, $start_time, $end_time)
    {
        $client_customer_id = $this->get_client_customer_id();
        if(empty($client_customer_id)) return false;

        $start_time = start_of_day($start_time);
        $end_time = start_of_day($end_time);

        return $this->scache->get("google-adword/{$client_customer_id}/date_range/{$start_time}_{$end_time}/{$report_type}");
    }

    public function write_adword_data($xml_data,$report_type,$start_time,$end_time)
    {    
        $adw_logs = simplexml_load_string($xml_data);
        $count = count($adw_logs->table->row);
        if(!count($adw_logs->table->row))
        {
            echo 'Report rỗng , không chứa bất cứ dữ liệu nào';
            $this->clear_selector_fields();
            return FALSE;
        }

        $start_date = $this->mdate->startOfDay($start_time);
        $end_date = $this->mdate->startOfDay($end_time);
        $client_customer_id = $this->user->GetClientCustomerId();
        $selector_fields = $this->get_selector_fields();
        $has_day = isset($selector_fields['Date']);
        $result = array();

        $is_date_range = ($start_date != $end_date);
        if($is_date_range)
        {
            if($has_day)
            {
                foreach ($adw_logs->table->row as $item) 
                {
                    $day = (string) $row->attributes()['day'];
                    $target_time = $this->mdate->startOfDay(strtotime($day));
                    if(!isset($result[$target_time]))
                        $result[$target_time] = array();

                    $attributes = array();
                    foreach($item->attributes() as $attribute)
                        $attributes[$attribute->getName()] = (string)$attribute;

                    $result[$target_time][] = $attributes;
                }
                
                if(!empty($result))
                {
                    foreach ($result as $day => $data) 
                    {
                        $year = my_date($day,'Y');
                        $month = my_date($day,'m');
                        $day = my_date($day,'d');

                        $cache_name = "google-adword/{$client_customer_id}/{$year}/{$month}/{$day}/{$report_type}";
                        $cache_name = strtolower($cache_name);
                        $this->scache->write($data,$cache_name);
                    }
                }
            }
            else
            {
                foreach ($adw_logs->table->row as $item) 
                {
                    $attributes = array();
                    foreach($item->attributes() as $attribute)
                        $attributes[$attribute->getName()] = (string)$attribute;

                    $result[] = $attributes;
                }

                $end_date = $this->mdate->endOfDay($end_time);
                $cache_name = "google-adword/{$client_customer_id}/{$report_type}/{$start_date}.{$end_date}";
                $cache_name = strtolower($cache_name);
                $this->scache->write($result,$cache_name);
            }

            echo 'cached data type date-range';
            return TRUE;
        } 

        foreach ($adw_logs->table->row as $item) 
        {
            $day = (string) $item->attributes()['day'];
            if(!empty($day))
            {
                $start_time = $this->mdate->startOfDay(strtotime($day));
            }
            if(!isset($result[$start_time]))
                $result[$start_time] = array();

            $attributes = array();
            foreach($item->attributes() as $attribute)
                $attributes[$attribute->getName()] = (string)$attribute;

            $result[$start_time][] = $attributes;
        }

        if(!empty($result))
        {
            foreach ($result as $day => $data) 
            {
                $year = my_date($day,'Y');
                $month = my_date($day,'m');
                $day = my_date($day,'d');

                $cache_name = "google-adword/{$client_customer_id}/{$year}/{$month}/{$day}/{$report_type}";
                $cache_name = strtolower($cache_name);
                $this->scache->write($result,$cache_name);
            }
            
            echo 'cached data same day';
            return TRUE;
        }

        echo 'cached none data';
        return FALSE;
    }

    public function download_report($report_type = false,$date_range = false,$args = false, $selector_fields = false,$ignore_cache = FALSE)
    {
        if(!$this->user->GetClientCustomerId()) return FALSE;

        $data = Null;
        $path = 'adwords/'.$this->user->GetClientCustomerId().'/';
        $cache_name = "{$path}{$report_type}.{$date_range}"  ;
        $cache_name.= empty($args) ? '' : '.'.convert_sth_to_string($args);
        $cache_name.= empty($selector_fields) ? '' : implode('.', array_map(function($x){return $x;}, $selector_fields));
        $cache_name = strtolower($cache_name);
        
        if(!$ignore_cache)
        {
            $data = $this->scache->get($cache_name);
            if(!empty($data)) 
                return $data;
        }

        $this->user->LoadService('ReportDefinitionService', ADWORDS_VERSION);

        $selector = new Selector();

        if($selector_fields == false)
        {
            $selector_fields = $this->config->item('selector');
            if(empty($selector_fields[$report_type])) return FALSE;
            $selector_fields = $selector_fields[$report_type];
        }

        $selector->fields = $selector_fields;

        if($date_range === 'CUSTOM_DATE'){
            $invalid_date = empty($args['custom_date_from']) || empty($args['custom_date_to']);
            if($invalid_date) return FALSE;
            $selector->dateRange = new DateRange($args['custom_date_from'], $args['custom_date_to']);
        }

        if(!empty($args))
        {
            if(!empty($args['AdvertisingChannelType']))
                $selector->predicates[] = new Predicate('AdvertisingChannelType', 'IN', $args['AdvertisingChannelType']);

            if(!empty($args['CampaignId']))
                $selector->predicates[] = new Predicate('CampaignId', 'IN', $args['CampaignId']);
        }

        $reportDefinition = new ReportDefinition();
        $reportDefinition->selector = $selector;
        $reportDefinition->reportName = uniqid();
        $reportDefinition->dateRangeType = $date_range;
        $reportDefinition->reportType = $report_type;
        $reportDefinition->downloadFormat = 'XML';
        // $reportDefinition->includeZeroImpressions = false;

        $options = array('version' => ADWORDS_VERSION);

        try 
        {
            $data = ReportUtils::DownloadReport($reportDefinition, Null, $this->user, $options);
            $this->scache->write($data, $cache_name, 24 * 3600);
        } 
        catch (Exception $e) 
        {
            return FALSE;
        }

        return $data;   
    }

    public function convert_xml2array($xml_data,$identity = 'campaigns')
    {
        $data = array();
        $xml_data = simplexml_load_string($xml_data);   
        if(empty($xml_data) || count($xml_data->table->row) <= 0) 
            return $data;

        foreach ($xml_data->table->row as $item) 
        {
            $attributes = array();
            foreach($item->attributes() as $attribute)
                $attributes[$attribute->getName()] = (string)$attribute;

            $data[] = $attributes;
        }

        return $data;
    }

    public function get_column($field,$report_type = false,$date_range = false,$args = false,$ignore_cache = false)
    {
        $raw_data = $this->download_report($report_type,$date_range,$args,FALSE,$ignore_cache);
        $data = simplexml_load_string($raw_data);
        
        if(empty($data) || count($data->table->row) <= 0) return FALSE;

        $result = array();
        foreach ($data->table->row as $row) {
            if(empty($row->attributes()[$field])) continue;
            $result[] = (string)$row->attributes()[$field];
        }
        return $result;
    }

    public function format_money($microAmount, $decimals = 2 , $dec_point = "," ,  $thousands_sep = "."){
        return number_format($microAmount/1000000, $decimals, $dec_point ,  $thousands_sep);
    }

    function widget_complete_percent_bar($real =0, $total = 0, $colors = array(), $titles = array())
    {

        $percent = ($total == 0) ? 0 : div($real,$total);
        $percent = $percent *100;
        $progress_color = reset($colors);

        $default_titles = array('real'=>'', 'percent'=>'', 'total'=>'');
        $titles = array_merge($default_titles, $titles);
        $real_decimal = is_float($real) ? 0 : 0;
        $total_decimal = is_float($total) ? 2 : 0;
        $percent_decimals = is_float($percent) ? 2 : 0;;

        foreach($colors as $color_pecent => $class)
        {
            if($percent >= $color_pecent)
                $progress_color = $class;
        }
        return
            '<div class="progress-group">
            <span class="progress-text" data-toggle="tooltip" title="" data-original-title="'.$titles['percent'].'"><small>' . numberformat($percent, $percent_decimals) . '%</small></span>
            <span class="progress-number"><b><span data-toggle="tooltip" title="'.$titles['real'].'"><small>' .
                numberformat($real, $real_decimal) . '</small></span></b>/
                <span data-toggle="tooltip" title="'.$titles['total'].'"><small>' . 
                    numberformat((int) $total, (int) $total_decimal).
                    '</small></span></span>
                    <div class="progress sm" style="width:100%;">
                        <div class="progress-bar '.$progress_color.' progress-bar-striped" style="width: ' . $percent . '%"></div>
                    </div>
                </div>';
    }

    public function get_location_criterions($atts = array('critetiaIds'=>array(),'locationName' => array())) 
    {
        $refresh_tokens = $this->refresh_token;
        if(empty($refresh_tokens)) return FALSE;

        extract($atts);
        if(empty($atts) || (empty($critetiaIds) && empty($locationName))) return FALSE;

        $this->load->config('credentials');

        foreach ($refresh_tokens as $refresh_token) 
        {
            $oAuth2Credential = (new OAuth2TokenBuilder())
            ->withClientId($this->config->item('clientId','mcm'))
            ->withClientSecret($this->config->item('clientSecret','mcm'))
            ->withRefreshToken($refresh_token)
            ->build();

            // $soapSettings = (new SoapSettingsBuilder())
            // ->disableSslVerify()
            // ->build();

            $session = (new AdWordsSessionBuilder())
            ->withDeveloperToken($this->config->item('googleadsDeveloperToken'))
            ->withClientCustomerId($this->get_client_customer_id())
            // ->withSoapSettings($soapSettings)
            ->withOAuth2Credential($oAuth2Credential)
            ->build();

            $locationCriterionService = (new AdWordsServices())->get($session, LocationCriterionService::class);

            // Create a selector to select all locations.
            $selector = new Selector();
            $selector->setFields(['Id','LocationName','CanonicalName','DisplayType','ParentLocations','Reach','TargetingStatus']);

            $predicates = array();
            $predicates = [(new Predicate('Locale', PredicateOperator::EQUALS, ['vi']))];

            if(!empty($critetiaIds))
            {
                $predicates[] = (new Predicate('Id',PredicateOperator::IN,$critetiaIds));
            }

            if(!empty($LocationName))
            {
                $predicates[] = (new Predicate('LocationName',PredicateOperator::IN,$locationName));
            }

            $selector->setPredicates($predicates);

            try 
            {
                
                $location_criterions = $locationCriterionService->get($selector);

                $result = array();

                if(empty($location_criterions)) continue;
                
                foreach ($location_criterions as $location_criterion)
                {
                    $location = $location_criterion->getLocation();
                    $result[$location->getId()] = $location->getLocationName();
                }
            } 
            catch (Exception $e) 
            {    
                continue;
            }

            return $result;
        }

        return FALSE;
    }

    function get_the_progress($term_id){}

    /**
     * Calculates the budget progress.
     *
     * @param      <type>  $term_id  The term identifier
     *
     * @return     double  The real progress.
     */
        
    function calc_budget_progress($term_id){}

    /**
     * Tính tổng số ngày chạy dịch vụ dựa theo ngày hợp đồng
     *
     * @param      <type>  $term_id  The term identifier
     *
     * @return     double  The real progress.
     */
        
    function calc_contract_days($term_id)
    {
        $retval = 0;

        $contract_begin = get_term_meta_value($term_id,'contract_begin');
        $contract_end = get_term_meta_value($term_id,'contract_end');
        $retval = diffInDates($this->mdate->startOfDay($contract_begin),$this->mdate->startOfDay($contract_end));

        return $retval;
    }

    /**
     * Tính tổng số ngày đã chạy dịch vụ dựa theo ngày kích hoạt thực hiện dịch vụ
     *
     * @param      <type>  $term_id  The term identifier
     *
     * @return     double  The real progress.
     */
        
    function calc_real_days($term_id)
    {
        $start_service_time = get_term_meta_value($term_id,'start_service_time');
        if(empty($start_service_time)) return 0;

        $retval = 0;

        $start_time = $this->mdate->startOfDay(get_term_meta_value($term_id,'googleads-begin_time'));
        $end_service_time = get_term_meta_value($term_id,'end_service_time') ?: $this->mdate->startOfDay();
        $retval = diffInDates($start_time,$end_service_time);
        
        return $retval;
    }

    /**
     * Calculates the real progress.
     *
     * @param      <type>  $term_id  The term identifier
     *
     * @return     <type>  The real progress.
     */
        
    function calc_real_progress($term_id){}

   
    function set_network($type = 'all')
    {
        $type = strtolower($type);
        $this->network = $type;
        return $this;
    }

    function get_campaigns()
    {
        $clientCustomerId = $this->user->GetClientCustomerId();
        if(!isset($clientCustomerId))
            throw new ReportDownloadException('The client customer ID must be specified for report downloads.');

        $cache_name = 'adwords/'.$clientCustomerId.'/get_campaigns';
        $campaigns = $this->scache->get($cache_name);
        if(empty($campaigns))
        {
            $campaigns = array();

            $campaignService = $this->user->GetService('CampaignService', ADWORDS_VERSION);
            $selector = new Selector();
            $selector->setFields(array('Id', 'Name','Status','AdvertisingChannelType'));
            $selector->setOrdering(array('Name', 'ASCENDING'));
            $selector->setPaging(new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE));

            do 
            {
                $page = $campaignService->get($selector);

                if (isset($page->entries)) 
                {
                    foreach ($page->entries as $campaign)
                    {
                        $campaign_dat = array(
                            'id'=>$campaign->id,
                            'name'=>$campaign->name,
                            'status'=>$campaign->status,
                            'advertisingChannelType'=>$campaign->advertisingChannelType
                            );
                        $campaigns[$campaign->id] = $campaign_dat;
                    }
                }

                $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;

            } while ($page->totalNumEntries > $selector->paging->startIndex);

            $this->scache->write($campaigns, $cache_name, 24 * 3600);
        }

        return $campaigns;
    }

    function update_campaign($campaignId)
    {
        $campaignService = $this->user->GetService('CampaignService', ADWORDS_VERSION);
        $campaign = new Campaign();
        $campaign->id = $campaignId;
        $campaign->status = 'PAUSED';

        $operation = new CampaignOperation();
        $operation->operand = $campaign;
        $operation->operator = 'SET';
        $operations = array($operation);

        try
        {
            $result = $campaignService->mutate($operations);
            $campaign = $result->value[0];
        }
        catch (Exception $e) 
        {
            echo $e->getMessage();
            return FALSE;
        }

        return TRUE;
    }
}
/* End of file Base_adwords_m.php */
/* Location: ./application/modules/googleads/models/Base_adwords_m.php */