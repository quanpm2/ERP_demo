<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/googleads/models/Googleads_behaviour_m.php');

class Account_behaviour_m extends Googleads_behaviour_m {

	public $stype = 'account_type';

	/**
	 * Calculates the fct.
	 *
	 * @param      float|integer  $percent  The percent
	 *
	 * @return     <type>         The fct.
	 */
	public function calc_fct($percent = 0.05, $amount = false)
	{
		parent::is_exist_contract(); // Determines if exist contract.
		false === $amount AND $amount = (double) get_term_meta_value($this->contract->term_id, 'contract_budget');
		
		return div($amount, 1-$percent)*$percent;
	}


	/**
	 * Calculates the contract value.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The contract value.
	 */
	public function calc_contract_value()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$contract_budget		= (double) get_term_meta_value($this->contract->term_id, 'contract_budget');
		$service_fee 			= (double) get_term_meta_value($this->contract->term_id, 'service_fee');
		$service_provider_tax 	= (double) $this->calc_service_provider_tax($contract_budget);

		return max(0, $contract_budget + $service_provider_tax + $service_fee - parent::calc_disacount_amount());
	}


	/**
	 * Calculates the budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The budget.
	 */
	public function calc_budget()
	{
		parent::is_exist_contract(); // Determines if exist contract.
		return (int) get_term_meta_value($this->contract->term_id, 'contract_budget');
	}
	/**
	 * Render Pritable contract
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     Array   Data result for Preview Action
	 */
	public function prepare_preview($term_id = 0)
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$data = $this->data;

		$data['data_service'] = array(
			'service_name' 	=> 'Quản lý tài khoản',
			'budget'		=> (int) get_term_meta_value($this->contract->term_id, 'contract_budget'),
			'currency' 		=> get_term_meta_value($this->contract->term_id, 'currency'),
			'service_fee' 	=> (int) get_term_meta_value($this->contract->term_id, 'service_fee'),
			'service_provider_tax' => (int) get_term_meta_value($this->contract->term_id, 'service_provider_tax'), // Thuế Google thu áp dụng từ 01.11.2022
		);


		$discount_amount = (double) get_term_meta_value($this->contract->term_id, 'discount_amount');
		$promotions = get_term_meta_value($this->contract->term_id, 'promotions') ?: [];

		if( !empty($discount_amount) && ! empty($promotions))
		{

			$promotions = unserialize($promotions);

			$data['data_service']['discount_amount'] = $discount_amount;
			$data['data_service']['promotions'] = array_map(function($promotion) use ($data){

				$promotion['text'] 	= "CTKM - {$promotion['name']}";
				$_value 	= $promotion['value'];

				if( 'percentage' == $promotion['unit'])
				{
					$_value = $data['data_service']['service_fee'] * div($promotion['value'], 100);
					$promotion['text'].= "({$promotion['value']}% phí dịch vụ)";
				}

				$promotion['amount'] = $_value;
				return $promotion;
			}, $promotions);
		}

		$data['view_file'] 	= 'googleads/contract/preview/account';

		$verified_on 	= get_term_meta_value($this->contract->term_id, 'verified_on') ?: time();
		
		$verified_on > strtotime('2021/01/20') AND $data['view_file'] = "googleads/contract/preview/account_after_2021_jan";

		return $data;
	}


	/**
	  * Tính thời gian dự kiến kết thúc
	  *
	  * Dự kiến kết thúc = [Tổng NS tiến độ]/[Tổng NS đã chạy]*[Số ngày HD] - 1 + Ngày kích hoạt thực hiện dịch vụ
	  *
	  * @throws     Exception  Error , stop and return
	  *
	  * @return     int        Thời gian dự kiến kết thúc dịch vụ dựa theo tiến độ thực tế
	  */
    public function calc_expected_end_time()
    {
    	parent::is_exist_contract(); // Determines if exist contract.
    	if( ! is_service_proc($this->contract->term_id)) throw new Exception('Dịch vụ chưa được thực hiện');

        $budget_progress 	= parent::calc_budget_progress(); //Tính tổng ngân sách tiến độ (phải chạy) cho đến thời điểm kết thúc dịch vụ
        $actual_result 		= parent::get_actual_result(); // Tính tổng ngân sách thực tế đã chi
        $contract_days 		= parent::calc_contract_days(); // Tính tổng số ngày thực hiện trên hợp đồng
        
        $expected_day 	= round(div($budget_progress, $actual_result)*$contract_days);
        $start_time 	= start_of_day(get_term_meta_value($this->contract->term_id,'googleads-begin_time'));

        return strtotime("+{$expected_day} days -1 day",$start_time);
    }


    /**
	  * Tính thời gian dự kiến kết thúc
	  *
	  * Dự kiến kết thúc = [Tổng NS tiến độ]/[Tổng NS đã chạy]*[Số ngày HD] - 1 + Ngày kích hoạt thực hiện dịch vụ
	  *
	  * @throws     Exception  Error , stop and return
	  *
	  * @return     int        Thời gian dự kiến kết thúc dịch vụ dựa theo tiến độ thực tế
	  */
    public function calc_payment_expected_end_time()
    {
    	parent::is_exist_contract(); // Determines if exist contract.
    	if( ! is_service_proc($this->contract->term_id)) throw new Exception('Dịch vụ chưa được thực hiện');

        $budget_progress 	= parent::calc_actual_budget_progress(); //Tính tổng ngân sách tiến độ (phải chạy) cho đến thời điểm kết thúc dịch vụ
        $actual_result 		= parent::get_actual_result(); // Tính tổng ngân sách thực tế đã chi
        $actual_budget_days = parent::calc_actual_budget_days(); // Số ngày thực hiện hợp đồng dựa trên ngân sách thực thu

        $expected_day 	= round(div($budget_progress, $actual_result)*$actual_budget_days);
        $start_time 	= start_of_day(get_term_meta_value($this->contract->term_id,'googleads-begin_time'));

        return strtotime("+{$expected_day} days -1 day",$start_time);
    }
}
/* End of file Account_behaviour_m.php */
/* Location: ./application/modules/googleads/models/Account_behaviour_m.php */