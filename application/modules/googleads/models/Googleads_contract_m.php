<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Googleads_contract_m extends Base_contract_m
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('term_posts_m');
		$this->load->model('contract/invoice_m');
		$this->load->model('contract_m');
		$this->load->model('googleads/googleads_m');
	}

	/**
	 * Creates a default invoice for adword cpc|account contract.
	 * Required numbers_of_payment and contract_value
	 *
	 * @param      term_m  $term   The term
	 *
	 * @return     boolean  status of process
	 */
	public function create_default_invoice($term)
	{
		if(empty($term)) return FALSE;

		$number_of_payments = get_term_meta_value($term->term_id,'number_of_payments') ?: 1;
		
		$contract_begin 	= get_term_meta_value($term->term_id,'contract_begin');
		$contract_end 		= get_term_meta_value($term->term_id,'contract_end');
		$num_dates 			= diffInDates($contract_begin,$contract_end);

		$service_type 	= get_term_meta_value($term->term_id,'service_type') ?: 'cpc_type';
		$start_date 	= $contract_begin;
		$total_amount 	= 0;
		$service_fee 	= 0;

		$amount_per_payments 			= 0;
		$addition_amount_first_payment 	= 0;

		switch ($service_type)
		{
			case 'cpc_type':
				$contract_cpc 		= get_term_meta_value($term->term_id,'contract_cpc');
				$contract_clicks 	= get_term_meta_value($term->term_id,'contract_clicks');
				$total_amount 		= $contract_cpc * $contract_clicks;
				$amount_per_payments = $total_amount / $number_of_payments;
				break;

			case 'account_type':

				$contract_budget 	= get_term_meta_value($term->term_id,'contract_budget');
				$service_fee 		= get_term_meta_value($term->term_id,'service_fee');
				$total_amount 		= $contract_budget + $service_fee;

				$amount_per_payments = div(($contract_budget + $service_fee), $number_of_payments);

    			$service_fee_payment_type 		= get_term_meta_value($term->term_id, 'service_fee_payment_type');

				$this->config->load('googleads/contract');
				$service_fee_payment_type_def 	= $this->config->item('service_fee_payment_type');

    			// Khởi tạo giá trị mặc định cho loại thanh toán phí dịch vụ dựa trên file config
			    if( empty($service_fee_payment_type))
			    {
			        $_service_fee_payment_type  = array_keys($service_fee_payment_type_def);
			        $service_fee_payment_type   = reset($_service_fee_payment_type);
			    }

			    if($service_fee_payment_type == 'full')
			    {
			    	$amount_per_payments = div($contract_budget, $number_of_payments);
			    	$addition_amount_first_payment = $service_fee;
			    }

				break;
		}

		$num_days4inv 			= ceil(div($num_dates, $number_of_payments));

		$i = 0;
		for($i ; $i < $number_of_payments; $i++)
		{	
			if($num_days4inv == 0) break;

			$end_date = strtotime('+'.$num_days4inv.' day -1 second', $start_date);
			$end_date = $this->mdate->endOfDay($end_date);

			$insert_data = array(
				'post_title' 	=> 'Thu tiền đợt '. ($i + 1),
				'post_content' 	=> '',
				'start_date' 	=> $start_date,
				'end_date' 		=> $end_date,
				'post_type' 	=> $this->invoice_m->post_type );

			$inv_id = $this->invoice_m->insert($insert_data);

			if(empty($inv_id)) continue;

			$this->term_posts_m->set_post_terms($inv_id, $term->term_id, $term->term_type);

			$quantity = $num_days4inv;

			$amount = $amount_per_payments;
			if($i == 0 && !empty($addition_amount_first_payment)) // đợt thanh toán đầu tiên
			{
				$amount+= $addition_amount_first_payment;
			}

			$invi_insert_data = array(
				'invi_title' => 'Quảng cáo Google',
				'inv_id' => $inv_id,
				'invi_description' => '',
				'invi_status' => 'publish',
				'price' => $amount,
				'quantity' => 1,
				'invi_rate' => 100,
				'total' => $this->invoice_item_m->calc_total_price($amount, 1, 100)
			);
			$this->invoice_item_m->insert($invi_insert_data);

			$start_date = strtotime('+1 second', $end_date);

			$day_end = $num_dates - $num_days4inv;
			if($day_end < $num_days4inv) 
				$num_days4inv = $day_end;
		}

		update_term_meta($term->term_id, 'contract_value', $total_amount);

		return TRUE;
	}

	public function stop_contract($term)
	{
		$time = time();
		update_term_meta($term->term_id,'end_contract_time',$time);
		$this->log_m->insert(array(
			'log_type' =>'end_contract_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => my_date($time,'Y/m/d H:i:s')
		));
		
		$this->load->model('googleads/googleads_report_m');
		return $this->googleads_report_m->send_finish_mail2admin($term->term_id);
	}
	
	public function start_service($term)
	{
		$this->contract_m->update($term->term_id,['term_status'=>'publish']);
		$this->messages->success('Trạng thái hợp đồng đã được chuyển sang "đang chạy".');

		$this->load->model('googleads/adwords_report_m');
		$this->adwords_report_m->init($term->term_id);
		$is_sent = $this->adwords_report_m->send_started_email();

		if($is_sent) $this->messages->success('Email thông báo bắt đầu thực hiện hợp đồng đã được gửi đến team Adsplus');
		else $this->messages->error('Email gửi không thành công.');

        return $is_sent;
	}

	/**
	 * Stops a service.
	 *
	 * @param      <type>  $term   The term
	 * @param      string  $time   The time
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function stop_service($term, $time = '')
	{
		$time = empty($time) ? time() : $time;

		update_term_meta($term->term_id,'googleads-end_time',$time);
		update_term_meta($term->term_id,'end_service_time',time());
		
		$this->log_m->insert(array(
			'log_type' =>'end_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => date('Y/m/d H:i:s')
			));
		
		$this->contract_m->update($term->term_id, array('term_status'=>'ending'));
		
		$this->load->model('googleads/googleads_report_m');
		$result = $this->googleads_report_m->send_finished_service($term->term_id, 'admin');

		return $result;
	}


	/**
	 * Suspend a service.
	 *
	 * @param      <type>  $term   The term
	 * @param      string  $time   The time
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function suspend_service($term, $time = '')
	{
		$time = empty($time) ? time() : $time;

		update_term_meta($term->term_id, 'googleads-end_time', $time);
		update_term_meta($term->term_id, 'end_service_time', time());
		
		$this->log_m->insert(array(
			'log_type' 	=>'end_service_time',
			'user_id' 	=> $this->admin_m->id,
			'term_id' 	=> $term->term_id,
			'log_content'	=> date('Y/m/d H:i:s')
		));
		
		$this->contract_m->update($term->term_id, array('term_status'=>'liquidation'));
		$this->load->model('googleads/googleads_report_m');
		$result = $this->googleads_report_m->send_finished_service($term->term_id,'admin');

		# code thêm phần thanh lý hợp đồng ở đây
		# code thêm phần email với nội dung thanh lý hợp đồng ở đây

		return $result;
	}

	function proc_service($term = FALSE)
	{
		if(!$term || $term->term_type !='google-ads') return FALSE;

		$now = time();
		update_term_meta($term->term_id, 'start_service_time', $now);

		/* Luôn luôn lấy tỉ giá ngay tại thời điểm function được gọi*/
        $currency = get_term_meta_value($term->term_id, 'account_currency_code');
        $meta_exchange_rate_key = strtolower("exchange_rate_{$currency}_to_vnd");
		update_term_meta($term->term_id, $meta_exchange_rate_key, get_exchange_rate(1, $currency));

		$this->log_m->insert(array(
			'log_type' =>'start_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => my_date($now,'Y/m/d H:i:s')
			));

		$this->load->model('googleads/googleads_report_m');
		$stat = $this->googleads_report_m->send_activation_mail2customer($term->term_id);
		return $stat;
	}

	public function send_mail_to_adsplus_team($term)
	{
		$this->contract_m->update($term->term_id, array('term_status'=>'publish'));

		$contract_code = get_term_meta_value($term->term_id, 'contract_code');
		$contract_begin = get_term_meta_value($term->term_id, 'contract_begin');
		$contract_end = get_term_meta_value($term->term_id, 'contract_end');
		$googleads_begin_time = get_term_meta_value($term->term_id, 'googleads-begin_time');

		$service_type = force_var(get_term_meta_value($term->term_id,'service_type'));
		$is_cpc_type = ($service_type == 'cpc_type');
		$service_type_label = $is_cpc_type ? 'Quản lý theo clicks & CPC' : 'Quản lý theo tài khoản';

		$from = 'support@adsplus.vn';
		$to = 'support@adsplus.vn';
		$cc = array();
		$title = 'Hợp đồng '.$contract_code.' đã được khởi chạy.';

		$content = 'Dear nhóm Adsplus <br>';
		$content.= 'Thông tin hợp đồng adsplus vừa được khởi chạy như sau: <br>';
		$content.= anchor(admin_url("googleads/setting/{$term->term_id}"),'Xin vui lòng vào link sau để cấu hình thông tin Adword và thông tin người giám sát để nhận thống kê số liệu hằng ngày <br>', 'target="_blank"');

		$this->config->load('table_mail');
		$this->table->set_template($this->config->item('mail_template'));
		$this->table->set_caption('Thông tin hợp đồng');
		$this->table
		->add_row('Mã hợp đồng:', $contract_code)
		->add_row('Tên dịch vụ:', 'Dịch vụ adsplus')
		->add_row('Gói dịch vụ:', $service_type_label);

		$vat = get_term_meta_value($term->term_id,'vat');
		$has_vat = ($vat > 0);
		
		$this->table
		->add_row('Loại hình:', ($has_vat ? 'V' : 'NV'))
		->add_row('Thời gian bắt đầu chạy adword:', my_date($googleads_begin_time,'d/m/Y'));

		$sale_id = get_term_meta_value($term->term_id,'staff_business');
		if(!empty($sale_id)){
			$display_name = $this->admin_m->get_field_by_id($sale_id,'display_name');
			$display_name = empty($display_name) ? $this->admin_m->get_field_by_id($sale_id,'display_name') : $display_name;
			$this->table->add_row('Kinh doanh phụ trách:', "#{$sale_id} - {$display_name}");
			
			$sale_email = $this->admin_m->get_field_by_id($sale_id,'user_email');
			if(!empty($sale_email))
				$cc[] = $sale_email;
		}

		$content.= $this->table->generate();

		$this->table
			->clear()
			->set_caption('Thông tin khách hàng');
		$this->table
			->add_row('Người đại diện', get_term_meta_value($term->term_id,'representative_name'))
			->add_row('Địa chỉ', get_term_meta_value($term->term_id,'representative_address'))
			->add_row('Website', $term->term_name)
			->add_row('Mail liên hệ', mailto(get_term_meta_value($term->term_id,'representative_email')));

		$content.= $this->table->generate();

		$this->load->library('email');
		$this->email
			->from($from)
			->to($to)
			->cc($cc)
			->subject($title)
			->message($content);
			
		$has_send = $this->email->send();
		if($has_send)
			$this->messages->success('Email thông báo bắt đầu thực hiện hợp đồng đã được gửi đến team Adsplus');
		else 
			$this->messages->error('Email gửi không thành công.');

		return $content;
	}
}