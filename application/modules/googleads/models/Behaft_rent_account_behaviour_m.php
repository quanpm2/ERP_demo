<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/googleads/models/Behaft_account_behaviour_m.php');

class Behaft_rent_account_behaviour_m extends Behaft_account_behaviour_m {

	public $stype = 'account_type';

	/**
	 * Render Pritable contract
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     Array   Data result for Preview Action
	 */
	public function prepare_preview($term_id = 0)
	{
		parent::is_exist_contract(); // Determines if exist contract.
		
		$data = $this->data;
		$data['data_service'] = array(
			'service_name' 	=> 'Cung cấp tài khoản',
			'budget'		=> (int) get_term_meta_value($this->contract->term_id, 'contract_budget'),
			'currency' 		=> get_term_meta_value($this->contract->term_id, 'currency'),
			'service_fee' 	=> (int) get_term_meta_value($this->contract->term_id, 'service_fee'),
			'isAccountForRent' => (bool) get_term_meta_value($this->contract->term_id, 'isAccountForRent')
		);

		$discount_amount = (double) get_term_meta_value($this->contract->term_id, 'discount_amount');
		$promotions = get_term_meta_value($this->contract->term_id, 'promotions') ?: [];

		if( !empty($discount_amount) && ! empty($promotions))
		{

			$promotions = unserialize($promotions);

			$data['data_service']['discount_amount'] = $discount_amount;
			$data['data_service']['promotions'] = array_map(function($promotion) use ($data){

				$promotion['text'] 	= "CTKM - {$promotion['name']}";
				$_value 	= $promotion['value'];

				if( 'percentage' == $promotion['unit'])
				{
					$_value = $data['data_service']['service_fee'] * div($promotion['value'], 100);
					$promotion['text'].= "({$promotion['value']}% phí dịch vụ)";
				}

				$promotion['amount'] = $_value;
				return $promotion;
			}, $promotions);
		}
		
		$data['contract_budget_customer_payment_type'] = (string) get_term_meta_value($this->contract->term_id, 'contract_budget_customer_payment_type');

		$data['view_file'] 	= 'googleads/contract/preview/behaft_rent_account';

        $verified_on 	= get_term_meta_value($this->contract->term_id, 'verified_on') ?: time();
        $verified_on > strtotime('2022/02/20') AND $data['view_file'] = "googleads/contract/preview/behaft_rent_account_after_2022_feb";

		return $data;
	}
}
/* End of file Rent_account_behaviour_m.php */
/* Location: ./application/modules/googleads/models/Rent_account_behaviour_m.php */