<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Adsplus_report_m.php');

class Adwords_report_m extends Adsplus_report_m {

    /* Allow Include departments emails to any Email */
    protected   $inc_departments    = TRUE;

    function __construct()
    {
        $this->autoload['models'][] = 'googleads/googleads_m';
        $this->autoload['models'][] = 'googleads/googleads_kpi_m';
        parent::__construct();
    }

    /**
     * Sends a started mail to local staffs - department
     */
    public function send_started_email()
    {
        $this->config->load('googleads/googleads');
        return parent::send_started_email();
    }

    /**
     * Sends an activation email.
     *
     * @param      string  $type_to  The type to
     */
    public function send_activation_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;

        $this->data['subject'] = "[ADSPLUS.VN] Thông báo kích hoạt dịch vụ Adsplus của website {$this->term->term_name}";
        $this->data['content_tpl'] = 'googleads/report/activation_email';
        return parent::send_activation_email($type_to);
    }

    public function init($term_id = 0)
    {
        parent::init($term_id);

        if( ! $this->term || $this->term->term_type != $this->googleads_m->term_type) return FALSE;

        $this->init_people_belongs();

        $this->data['googleads_begin_time'] = get_term_meta_value($this->term->term_id, 'googleads-begin_time');
        $this->data['service_type']         = get_term_meta_value($this->term->term_id, 'service_type');
        return $this;
    }


    /**
     * Loads workers.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_workers()
    {
        parent::load_workers();

        $kpis = $this->googleads_kpi_m->select('user_id')->where('term_id',$this->term_id)->group_by('user_id')->get_many_by();
        if( ! $kpis) return $this;

        $_workers       = array_map(function($x){ return $this->admin_m->get_field_by_id($x->user_id); }, $kpis);
        $this->workers  = wp_parse_args(array_column($_workers, NULL, 'user_email'), $this->workers);

        return $this;
    }
}
/* End of file Adwords_report_m.php */
/* Location: ./application/modules/googleads/models/Adwords_report_m.php */