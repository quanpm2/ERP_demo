<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adwords_account_m extends Base_adwords_m {

    protected $total_avgposition = 0;
    private $_custom_date = array();

    /**
     * Tính tổng ngân sách tiến độ (phải chạy) cho đến thời điểm kết thúc dịch vụ
     *
     * Tổng NS phải chạy = Ngân sách HĐ / Số ngày HĐ * số ngày chạy thực tế
     *
     * @param      int  $term_id  The term identifier
     *
     * @return     double  The real progress.
     */
    function calc_budget_progress($term_id)
    {
        $start_time = get_term_meta_value($term_id,'start_service_time');
        if(empty($start_time)) return 0;

        $retval = 0;
        $contract_budget = get_term_meta_value($term_id,'contract_budget');
        $contract_days = parent::calc_contract_days($term_id);
        $real_days = parent::calc_real_days($term_id);

        $retval = div($contract_budget,$contract_days)*$real_days; 
        return $retval;
    }

    /**
     * Tính tiến độ thực hiện thực tế đến thời điểm kết thúc dịch vụ
     * hoặc thời gian tính nếu hợp đồng vẫn đang thực hiện
     *
     * Tiến độ = Tổng NS đã chạy / Tổng NS phải chạy
     *
     * @param      int  $term_id  The term identifier
     *
     * @return     double  The real progress.
     */
    function calc_real_progress($term_id)
    {
        // Nếu HĐ chưa được kích hoạt thực hiện thì return 0
        $start_time = get_term_meta_value($term_id,'start_service_time');
        if(empty($start_time)) return 0;

        $retval = 0;
        $actual_result = get_term_meta_value($term_id,'actual_result') ?? 0;

        $currency_code = (int) get_term_meta_value($term_id,'account_currency_code');
        $exchange_rate = (int) get_exchange_rate($currency_code, $term_id);
        ('VND' != $currency_code) AND $actual_result *= $exchange_rate;

        $total_budget_progress = $this->calc_budget_progress($term_id);

        $retval = numberformat(div($actual_result,$total_budget_progress)*100, 2);

        // Cập nhật metadata real_progress phục vụ cho get nhanh
        update_term_meta($term_id,'real_progress',$retval);

        return $retval;
    }

    /**
     * Tính thời gian dự kiến kết thúc
     *
     * Dự kiến kết thúc = [Tổng NS tiến độ]/[Tổng NS đã chạy]*[Số ngày HD] - 1 + Ngày kích hoạt thực hiện dịch vụ
     *
     * @param      int  $term_id  The term identifier
     *
     * @return     int  Thời gian dự kiến kết thúc dịch vụ dựa theo tiến độ thực tế
     */
    public function calc_expected_end_time($term_id)
    {
        $start_service_time = get_term_meta_value($term_id,'start_service_time');
        if(empty($start_service_time)) return 0;

        $retval = 0; 

        $total_budget_progress = $this->calc_budget_progress($term_id);

        // Tính tổng ngân sách thực tế đã chi
        $total_budget_actual = get_term_meta_value($term_id,'actual_result') ?? 0;
        $account_currency_code = get_term_meta_value($term_id,'account_currency_code');
        $exchange_rate = get_exchange_rate($account_currency_code, $term_id);
        ('VND' != $account_currency_code) AND $total_budget_actual *= $exchange_rate;

        $contract_days = parent::calc_contract_days($term_id);

        $start_time = $this->mdate->startOfDay(get_term_meta_value($term_id,'googleads-begin_time'));
        $expected_day = round(div($total_budget_progress,$total_budget_actual)*$contract_days);
        $retval = strtotime("+{$expected_day} days -1 day",$start_time);

        return $retval;
    }

    

    /**
     * Gets the service progress.
     * 
     * Calculate the progress rate from date-range input
     * value = real_index / target_index (with the same date-range)
     *
     * @param      integer   $term_id       The term identifier
     * @param      integer   $start_time    The start time
     * @param      integer   $end_time      The end time
     * @param      boolean   $ignore_cache  The ignore cache
     *
     * @return     float  The progress.
     */
    public function get_the_progress($term_id, $start_time = FALSE, $end_time = FALSE, $ignore_cache = FALSE)
    {
        $actual     = $progress = 0;
        $target     = get_term_meta_value($term_id,'contract_budget');
        
        $mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
        $this->set_mcm_account($mcm_account_id);

        $all_time   = empty($start_time) && empty($end_time);
        $start_time = empty($start_time) ? get_term_meta_value($term_id,'googleads-begin_time') : $start_time;
        if(empty($end_time)) $end_time = get_term_meta_value($term_id,'googleads-end_time') ?: time();

        # IF request is force to download new performance-report , then download report from API first and cache
        if($ignore_cache) $this->download('ACCOUNT_PERFORMANCE_REPORT', $start_time, $end_time);

        $account_data = $this->get_cache('ACCOUNT_PERFORMANCE_REPORT', $start_time, $end_time);
        $account_data = array_filter($account_data);

        $accounts_filtered = array();
        if( ! empty($account_data))
        {
            foreach ($account_data as $timestamp => $accounts) 
            {
                if(empty($accounts)) continue;

                $accounts_filtered = array_merge_recursive($accounts_filtered,$accounts);
            }

            $actual = array_sum(array_column($accounts_filtered, 'cost'))/1000000;
            
            if($all_time)
            {
                // Store last adword account data updated time
                $actual_result_past = get_term_meta_value($term_id, 'actual_result');
                if($actual_result_past != $actual)
                {
                    // update result updated on by current time excuted
                    update_term_meta($term_id, 'result_updated_on', time());
                }
                
                // update actual_result
                update_term_meta($term_id, 'actual_result', $actual);
            }
        }

        $currency_code = get_term_meta_value($term_id,'account_currency_code');
        if(!empty($accounts_filtered))
        {
            $default_account = reset($accounts_filtered);
            $currency_code = $default_account['currency'];
            update_term_meta($term_id,'account_currency_code',$currency_code);
        }

        $exchange_rate = get_exchange_rate($currency_code, $term_id);
        ('VND' != $currency_code) AND $actual *= $exchange_rate;

        $progress = number_format(div($actual, $target)*100, 3);
        if($all_time)
        {
            $actual_progress_percent = number_format((double)get_term_meta_value($term_id, 'actual_progress_percent'), 3);
            if($actual_progress_percent != $progress) update_term_meta($term_id,'actual_progress_percent',$progress);

            /* Cập nhật ngân sách */
            $actual_budget                  = (int) get_term_meta_value($term_id, 'actual_budget');
            $actual_progress_percent_net    = number_format(div($actual, $actual_budget), 3);
            if($actual_progress_percent_net != number_format( (double) get_term_meta_value($term_id,'actual_progress_percent_net'),3))  
            update_term_meta($term_id, 'actual_progress_percent_net', $actual_progress_percent_net);
        }

        return $progress;
    }

    function widget_complete_percent_bar($real =0, $total = 0, $colors = array(), $titles = array())
    {
        $colors_default = array(
            '50' =>'progress-bar-red',
            '80' =>'progress-bar-yellow',
            '90' =>'progress-bar-green',
            '100' =>'progress-bar-aqua'
            );
        $titles_default = array(
            'percent' => '% thực hiện hiện tại',
            'total' => 'tổng',
            'real' => 'thực tế'
            );
        $colors = $colors+$colors_default ;
        $titles = array_merge($titles_default, $titles);

        return parent::widget_complete_percent_bar($real, $total, $colors, $titles);
    }
}
/* End of file Adwords_account_m.php */
/* Location: ./application/modules/googleads/models/Adwords_account_m.php */