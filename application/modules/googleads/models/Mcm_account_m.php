<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcm_account_m extends Term_m {

	const CACHE_ALL_KEY = "modules/googleads/mcm_accounts-";

    public $term_type 	= 'mcm_account';

    function __construct()
    {
        parent::__construct();

        $this->load->model('googleads/insight_segment_m');
    }

    public function set_term_type()
    {
        return $this->where('term.term_type',$this->term_type);
    }

    /**
     * Gets the AdCampaign's data insight
     * If time input not defined , default is get LAST_7_DAYS
     *
     * @param      integer  $term_id     The term identifier
     * @param      integer  $start_time  The start time
     * @param      integer  $end_time    The end time
     *
     * @return     array    The data.
     */
    public function get_data($adAccount_id = 0, $start_time = FALSE, $end_time = FALSE)
    {
        $mcm_account = (new Mcm_account_m())->set_term_type()->as_array()->get($adAccount_id);

        if(empty($mcm_account)) return false;

        $account_id = $mcm_account['term_name'] ?? '';
        $account_name = get_term_meta_value($adAccount_id, 'account_name');

        $end_time		= $end_time ?: time();
        $end_time       = end_of_day($end_time);
		$start_time 	= $start_time ?: strtotime('-7 days',$end_time);
		$start_time 	= start_of_day($start_time);

		$iSegments = $this->term_posts_m->get_term_posts($adAccount_id, $this->insight_segment_m->post_type, [
			'fields' => 'posts.post_id, posts.start_date',
			'where' => [
				'start_date >='	=> $start_time,
				'start_date <='	=> $end_time,
				'post_name' 	=> 'day'
			]
		]);

		if(empty($iSegments)) return [];

		$data = array_map(function($x) use ($account_id, $account_name){
			
			$metadata 		= get_post_meta_value($x->post_id);
			if(empty($metadata)) return [];

			$x = array_merge( (array) $x, array_column($metadata, 'meta_value', 'meta_key'));

            $x['account_id'] = $account_id;
            $x['account_name'] = $account_name;
			$x['post_id'] 	= (int) $x['post_id'];
			$x['time'] 		= (int) $x['start_date'];
			$x['start_date']= (int) $x['start_date'];
			$x['spend'] 	= (double) ($x['spend'] ?? 0);
			$x['cost']    	= (double) ($x['cost'] ?? 0);
			return $x;
		}, $iSegments);

		return array_values(array_filter($data));
    }

    /**
     * Gets the insight.
     *
     * @param      integer   $term_id     The term identifier
     * @param      <type>    $start_time  The start time
     * @param      <type>    $end_time    The end time
     *
     * @return     Campaign  The insight.
     */
    public function get_insight($term_id = 0, $start_time = FALSE, $end_time = FALSE)
    {
        $term = $this->set_term_type()->get($term_id);
        if (!$term) return FALSE;

        $result     = array();
        $insights     = $this->requestInsightAPI($term_id, $start_time, $end_time);
        if (empty($insights)) return FALSE;

        foreach ($insights as $key => $insight) {
            // Real data
            // $data = $insight->getData();

            // Fake data
            $data = $insight;

            if (empty($data)) continue;

            $data             = array_filter($data);
            $datetime        = start_of_day($data['date_start']);

            $_isegment = $this->term_posts_m->get_term_posts($term_id, $this->insight_segment_m->post_type, [
                'fields' => 'posts.post_id',
                'where' => [
                    'start_date'     => $datetime,
                    'end_date'         => $datetime,
                    'post_name'     => 'day'
                ]
            ]);

            /* Create new if empty */
            if (empty($_isegment)) {
                $_isegmentInsert     = [
                    'post_name'     => 'day',
                    'post_type'     => 'insight_segment',
                    'start_date'     => $datetime,
                    'end_date'         => $datetime
                ];

                $_isegment_id = $this->insight_segment_m->insert($_isegmentInsert);

                $this->term_posts_m->set_term_posts($term_id, [$_isegment_id], $this->insight_segment_m->post_type);

                $_isegment = (object) $_isegmentInsert;
                $_isegment->post_id = $_isegment_id;
            }

            is_array($_isegment) and $_isegment = reset($_isegment);

            $account_currency = $data['account_currency'];
            if ('VND' != $account_currency) {
                $meta_exchange_rate_key = strtolower("exchange_rate_{$account_currency}_to_vnd");
                $exchange_rate = (int)(get_term_meta_value($term_id, $meta_exchange_rate_key) ?: get_exchange_rate($account_currency));

                $spend = (int)$data['spend'];
                $data['spend'] = $spend * $exchange_rate;
            }

            array_walk($data, function ($value, $key) use ($_isegment) {
                update_post_meta($_isegment->post_id, $key, $value);
            });

            $result[$datetime] = $data;
        }

        return $result;
    }

    /**
     * Request Insight API Data
     *
     * @param      int              $adAccountId  The ad account identifier
     * @param      <type>           $start_time   The start time
     * @param      bool             $end_time     The end time
     *
     * @throws     Exception        (description)
     *
     * @return     AdAccount|array  ( description_of_the_return_value )
     */
    protected function requestInsightAPI($adAccountId = 0, $start_time = FALSE, $end_time = FALSE)
    {
        // $term = $this->set_term_type()->get($adAccountId);
        // if( ! $term) return FALSE;

        // $ggAppEnv		= $this->config->item('default', 'ggapps');
        // $ggApp 			= $this->config->item($ggAppEnv, 'ggapps');
        // $app_id 		= $ggApp['credential']['app_id'] ?? null;
        // $app_secret 	= $ggApp['credential']['app_secret'] ?? null;

        // $ggAccounts 	= array();
        // $ggAccounts[] 	= array(
        // 	'user_id' => null,
        // 	'access_token' => ( (array) $this->config->item('production', 'ggapps'))['credential']['access_token'] ?? null
        // );		

        // $_ggAccounts = $this->term_users_m->get_the_users($adAccountId, $this->term_type);
        // $_ggAccounts AND $ggAccounts = array_merge($ggAccounts, array_map(function($x){
        // 	return [
        // 		'user_id' => $x->user_id,
        // 		'access_token' => get_user_meta_value($x->user_id, 'access_token')
        // 	];
        // }, $_ggAccounts));

        // foreach ($ggAccounts as $ggAccount)
        // {
        // 	$ggAccount = (array) $ggAccount;

        // 	empty($ggAccount['access_token']) AND $ggAccount['access_token'] = get_user_meta_value($ggAccount['user_id'], 'access_token');
        // 	Api::init($app_id, $app_secret, $ggAccount['access_token']);

        // 	$params = array( 'time_increment' => 1, 'date_preset' => AdsInsightsDatePresetValues::LAST_7D );

        // 	if( ! empty($start_time))
        // 	{
        // 		unset($params['date_preset']);

        // 		$start_date = my_date($start_time,'Y-m-d');
        // 		$end_date 	= $end_time ? my_date($end_time,'Y-m-d') : my_date(0,'Y-m-d');

        // 		$params['time_range'] = (object) array(
        // 			'since'	=> $start_date,
        // 			'until'	=> $end_date
        // 		);
        // 	}

        // 	$adAccount = new AdAccount($term->term_slug);	
		// 	$adAccount = new AdAccount($term->term_slug);	
        // 	$adAccount = new AdAccount($term->term_slug);	
		// 	$adAccount = new AdAccount($term->term_slug);	
        // 	$adAccount = new AdAccount($term->term_slug);	
		// 	$adAccount = new AdAccount($term->term_slug);	
        // 	$adAccount = new AdAccount($term->term_slug);	

        // 	try
        // 	{
        // 		$insights 	= $adAccount->getInsights($this->get_fields(), $params);
        // 		$content 	= $insights->getLastResponse()->getContent();
        // 		while ( ! empty($content['paging']['next']))
        // 		{
        // 			try
        // 			{
        // 				$insights->fetchAfter();	
		// 				$insights->fetchAfter();	
        // 				$insights->fetchAfter();	
		// 				$insights->fetchAfter();	
        // 				$insights->fetchAfter();	
		// 				$insights->fetchAfter();	
        // 				$insights->fetchAfter();	
        // 			} 
		// 			} 
        // 			} 
		// 			} 
        // 			} 
		// 			} 
        // 			} 
        // 			catch (Exception $e)
        // 			{
        // 				log_message('error', $e->getMessage());
        // 				throw new Exception($e->getMessage());
        // 				break;
        // 			}

        // 			$content = $insights->getLastResponse()->getContent();
        // 		}

        // 		update_term_meta($term->term_id, 'fbaccount_status_with_user_id_' . ($ggAccount['user_id'] ?: 0), 'established');

        // 		$result = $insights->getObjects();
        // 	} 
		// 	} 
        // 	} 
		// 	} 
        // 	} 
		// 	} 
        // 	} 
        // 	catch (\FacebookAds\Http\Exception\AuthorizationException $e)
        // 	{
        // 		update_term_meta($term->term_id, 'fbaccount_status_with_user_id_' . ($ggAccount['user_id'] ?: 0), 'unauthorized');
        // 		log_message('error', $e->getMessage());
        // 		continue;
        // 	}
        // 	catch (Exception $e)
        // 	{
        // 		log_message('error', $e->getMessage());
        // 		continue;
        // 	}
        // }

        // $numOfconnectionEstablished = array_filter($ggAccounts, function($x) use ($term){
        // 	$api_connected = get_term_meta_value($term->term_id, 'fbaccount_status_with_user_id_' . ($x['user_id'] ?: 0));
        // 	return $api_connected != 'unauthorized';
        // });

        // if( ! $numOfconnectionEstablished) $this->set_term_type()->update($term->term_id, 'term_status', 'unlinked');

        $fake_data = [
            [
                'account_currency' =>  'VND',
                'account_id' =>  '3310322085649952',
                'account_name' =>  'Adsplus 5 - Kymdan (Va)',
                'action_values' =>  '',
                'actions' => '',
                'ad_bid_type' =>  '',
                'ad_bid_value' =>  '',
                'ad_click_actions' =>  '',
                'ad_delivery' =>  '',
                'ad_id' => '',
                'ad_impression_actions' =>  '',
                'ad_name' => '',
                'adset_bid_type' =>  '',
                'adset_bid_value' =>  '',
                'adset_budget_type' =>  '',
                'adset_budget_value' =>  '',
                'adset_delivery' =>  '',
                'adset_end' =>  '',
                'adset_id' =>  '',
                'adset_name' =>  '',
                'adset_start' =>  '',
                'age_targeting' =>  '',
                'attribution_setting' =>  '',
                'auction_bid' =>  '',
                'auction_competitiveness' =>  '',
                'auction_max_competitor_bid' =>  '',
                'buying_type' =>  '',
                'campaign_id' =>  '',
                'campaign_name' =>  '',
                'canvas_avg_view_percent' =>  '',
                'canvas_avg_view_time' =>  '',
                'catalog_segment_actions' =>  '',
                'catalog_segment_value' =>  '',
                'catalog_segment_value_mobile_purchase_roas' =>  '',
                'catalog_segment_value_omni_purchase_roas' =>  '',
                'catalog_segment_value_website_purchase_roas' =>  '',
                'clicks' =>  '94',
                'conversion_rate_ranking' =>  '',
                'conversion_values' =>  '',
                'conversions' =>  '',
                'converted_product_quantity' =>  '',
                'converted_product_value' =>  '',
                'cost_per_15_sec_video_view' =>  '',
                'cost_per_2_sec_continuous_video_view' =>  '',
                'cost_per_action_type' =>  '',
                'cost_per_ad_click' =>  '',
                'cost_per_conversion' =>  '',
                'cost_per_dda_countby_convs' =>  '',
                'cost_per_estimated_ad_recallers' =>  '',
                'cost_per_inline_link_click' =>  '',
                'cost_per_inline_post_engagement' =>  '',
                'cost_per_one_thousand_ad_impression' =>  '',
                'cost_per_outbound_click' =>  '',
                'cost_per_thruplay' =>  '',
                'cost_per_unique_action_type' =>  '',
                'cost_per_unique_click' =>  '',
                'cost_per_unique_conversion' =>  '',
                'cost_per_unique_inline_link_click' =>  '',
                'cost_per_unique_outbound_click' =>  '',
                'cpc' =>  '5744.5',
                'cpm' =>  '91912',
                'cpp' =>  '97505.055977',
                'created_time' =>  '',
                'ctr' => '1.6',
                'date_start' =>  '2020-01-01',
                'date_stop' =>  '2020-01-01',
                'dda_countby_convs' =>  '',
                'dda_results' =>  '',
                'engagement_rate_ranking' =>  '',
                'estimated_ad_recall_rate' =>  '',
                'estimated_ad_recall_rate_lower_bound' =>  '',
                'estimated_ad_recall_rate_upper_bound' =>  '',
                'estimated_ad_recallers' =>  '',
                'estimated_ad_recallers_lower_bound' =>  '',
                'estimated_ad_recallers_upper_bound' =>  '',
                'frequency' =>  '',
                'full_view_impressions' =>  '',
                'full_view_reach' =>  '',
                'gender_targeting' =>  '',
                'impressions' =>  '5875',
                'inline_link_click_ctr' =>  '',
                'inline_link_clicks' =>  '',
                'inline_post_engagement' =>  '',
                'instant_experience_clicks_to_open' =>  '',
                'instant_experience_clicks_to_start' =>  '',
                'instant_experience_outbound_clicks' =>  '',
                'interactive_component_tap' =>  '',
                'labels' =>  '',
                'location' =>  '',
                'mobile_app_purchase_roas' =>  '',
                'objective' =>  'PAGE_LIKES',
                'optimization_goal' =>  '',
                'outbound_clicks' =>  '',
                'outbound_clicks_ctr' =>  '',
                'place_page_name' =>  '',
                'purchase_roas' =>  '',
                'qualifying_question_qualify_answer_rate' =>  '',
                'quality_ranking' =>  '',
                'quality_score_ectr' =>  '',
                'quality_score_ecvr' =>  '',
                'quality_score_organic' =>  '',
                'reach' =>  '5538',
                'social_spend' =>  '',
                'spend' =>  '539983',
                'unique_actions' =>  '',
                'unique_clicks' =>  '',
                'unique_conversions' =>  '',
                'unique_ctr' =>  '',
                'unique_inline_link_click_ctr' =>  '',
                'unique_inline_link_clicks' =>  '',
                'unique_link_clicks_ctr' =>  '',
                'unique_outbound_clicks' =>  '',
                'unique_outbound_clicks_ctr' =>  '',
                'unique_video_continuous_2_sec_watched_actions' =>  '',
                'unique_video_view_15_sec' =>  '',
                'updated_time' =>  '',
                'video_15_sec_watched_actions' =>  '',
                'video_30_sec_watched_actions' =>  '',
                'video_avg_time_watched_actions' =>  '',
                'video_continuous_2_sec_watched_actions' =>  '',
                'video_p100_watched_actions' =>  '',
                'video_p25_watched_actions' =>  '',
                'video_p50_watched_actions' =>  '',
                'video_p75_watched_actions' =>  '',
                'video_p95_watched_actions' =>  '',
                'video_play_actions' =>  '',
                'video_play_curve_actions' =>  '',
                'video_play_retention_0_to_15s_actions' =>  '',
                'video_play_retention_20_to_60s_actions' =>  '',
                'video_play_retention_graph_actions' =>  '',
                'video_thruplay_watched_actions' =>  '',
                'video_time_watched_actions' =>  '',
                'website_ctr' =>  '',
                'website_purchase_roas' =>  '',
                'wish_bid' =>  '',
            ]
        ];

        return $fake_data;
    }

    public function get_insight_db($term_id, $start_time, $end_time = FALSE){
        $start_time = $this->mdate->startOfDay($start_time);
        $end_time = $end_time?:$start_time;
        $end_time = $this->mdate->startOfDay($end_time);

        $data = (new mcm_account_m())
            ->select("term.term_id AS contractId")
            ->select("term.term_name AS contractUrl")
            ->select("term.term_type AS contractType")
            ->select("term.term_status AS contractStatus")
            ->select("COALESCE(ads_segment.post_id, balance_spend.post_id) AS segmentId")
            ->select("COALESCE(ads_segment.post_type, balance_spend.post_type) AS segmentType")
            ->select("COALESCE(ads_segment.post_status, balance_spend.post_status) AS segmentStatus")
            ->select("COALESCE(ads_segment.start_date, balance_spend.start_date) AS segmentStartDate")
            ->select("COALESCE(ads_segment.end_date, balance_spend.end_date) AS segmentEndDate")
            ->select("mcm_account.term_id AS mcmAccountTermId")
            ->select("mcm_account.term_name AS mcmAccountId")
            ->select("MAX(IF(mcm_account_metadata.meta_key = 'account_name', mcm_account_metadata.meta_value, NULL)) AS mcmAccountName")
            ->select("mcm_account.term_status AS mcmAccountStatus")
            ->select("MAX(IF(mcm_account_metadata.meta_key = 'source', mcm_account_metadata.meta_value, NULL)) AS mcmAccountSource")
            ->select("insights.post_id AS insightId")
            ->select("COALESCE(insights.post_name, balance_spend.post_name) AS insightSegmentType")
            ->select("insights.post_type AS insightType")
            ->select("insights.post_status AS insightStatus")
            ->select("COALESCE(insights.start_date, balance_spend.start_date) AS insightStartDate")
            ->select("COALESCE(MAX(IF(mcm_account_metadata.meta_key = 'source', mcm_account_metadata.meta_value, NULL)), balance_spend.comment_status) AS insightSource")
            ->select("MAX(IF(insight_metadata.meta_key = 'account_currency', insight_metadata.meta_value, NULL)) AS account_currency")
            ->select("MAX(IF(insight_metadata.meta_key = 'clicks', insight_metadata.meta_value, NULL)) AS clicks")
            ->select("MAX(IF(insight_metadata.meta_key = 'invalidClicks', insight_metadata.meta_value, NULL)) AS invalidClicks")
            ->select("MAX(IF(insight_metadata.meta_key = 'impressions', insight_metadata.meta_value, NULL)) AS impressions")
            ->select("MAX(IF(insight_metadata.meta_key = 'cost', insight_metadata.meta_value, NULL)) AS cost")
            ->select("MAX(IF(insight_metadata.meta_key = 'cpc', insight_metadata.meta_value, NULL)) AS cpc")
            ->select("MAX(IF(insight_metadata.meta_key = 'conversions', insight_metadata.meta_value, NULL)) AS conversions")
            ->select("MAX(IF(insight_metadata.meta_key = 'network', insight_metadata.meta_value, NULL)) AS network")
            ->select("COALESCE(MAX(IF(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, NULL)), balance_spend.post_content) AS spend")
            ->select("tp_contract_ads_segment.post_id")
            ->select("balance_spend.post_excerpt AS note")
            ->select("DATE_FORMAT(FROM_UNIXTIME(insights.end_date), '%Y-%m-%d') AS day")
            
            ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
            ->join('posts AS balance_spend', 'balance_spend.post_id = tp_contract_ads_segment.post_id AND balance_spend.post_type = "balance_spend"', 'left')
            ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'left')
            ->join('term_posts AS tp_segment_mcm_account', 'tp_segment_mcm_account.post_id = ads_segment.post_id', 'left')
            ->join('term AS mcm_account', 'tp_segment_mcm_account.term_id = mcm_account.term_id AND mcm_account.term_type = "mcm_account"', 'left')
            ->join('termmeta AS mcm_account_metadata', 'mcm_account_metadata.term_id = mcm_account.term_id AND meta_key in ("source", "account_name")', 'left')
            ->join('term_posts AS tp_mcm_account_insights', 'tp_mcm_account_insights.term_id = mcm_account.term_id', 'left')
            ->join('posts AS insights', 'tp_mcm_account_insights.post_id = insights.post_id AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) AND insights.start_date <= if(ads_segment.end_date = 0 or ads_segment.end_date is null, UNIX_TIMESTAMP (), ads_segment.end_date) AND insights.post_type = "insight_segment" AND insights.post_name = "day"', 'left')
            ->join('postmeta insight_metadata', 'insight_metadata.post_id = insights.post_id AND insight_metadata.meta_key in ("account_currency", "clicks", "impressions", "spend", "invalidClicks", "cost", "cpc", "conversions", "network")', 'left')

            ->where('term.term_id', $term_id)
            ->where('(insights.post_id > 0 or balance_spend.post_id > 0 )')
            ->where('insights.end_date >=', $start_time)
            ->where('insights.end_date <=', $end_time)

            ->group_by('term.term_id, tp_contract_ads_segment.post_id, mcm_account.term_id, insights.post_id')
            ->order_by('insightStartDate', 'DESC')

            ->as_array()
            ->get_all();

        return $data;
    }
}
/* End of file Mcm_account_m.php */
/* Location: ./application/modules/googleads/models/Mcm_account_m.php */