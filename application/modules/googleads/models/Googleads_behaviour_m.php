<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Behaviour_m.php');

use AdsService\MessageQueue\RabbitMQ;

class Googleads_behaviour_m extends Behaviour_m {

	public $term_type 	= 'google-ads';
	public $stype 		= 'account_type';

	const FCT_TAX 				= 0.05;
	const FCT_PERCENTAGE_MIN 	= 0;
	const FCT_PERCENTAGE_MAX 	= 0.05;

	function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
		$this->load->config('googleads/googleads');
	}

	public function get_service_rule($budget = 0)
	{
		$budget OR $budget = (double) $this->calc_budget();
		$this->config->load('googleads/contract');

		$isAccountForRent	= (int) get_term_meta_value($this->contract->term_id, 'isAccountForRent');
		$service_fee_rules 	= $this->config->item(($isAccountForRent ? 'rent' : 'normal'), 'serviceFeeRuleEnums');
		$service_fee_rules 	= $service_fee_rules['rule'];

		$rule = array_filter($service_fee_rules, function($x) use ($budget){
			if( ! is_array($x)) return false;
			return $x['min'] <= $budget && $budget <= $x['max'];
		});

		$rule AND $rule = reset($rule);

		return $rule;		
	}

	/**
     * Calculates the actual service fee rate.
     *
     * @param      int   $budget  The budget
     *
     * @return     bool  The service fee rate.
     */
    public function calc_service_fee_rate_actual()
	{
		$discount_amount 	= (double) $this->calc_disacount_amount();
		$service_fee 		= (double) get_term_meta_value($this->contract->term_id, 'service_fee');
		return div(max([$service_fee - $discount_amount, 0]), $this->calc_budget());
	}

	public function calc_service_provider_tax($budget = 0, $rate = 0.05)
	{
		parent::is_exist_contract(); // Determines if exist contract.
		$budget OR $budget = (double) $this->calc_budget();

		return (double) $budget * (float) $rate;
	}

	public function calc_service_fee_rate($budget = 0)
	{
		$rule = $this->get_service_rule($budget);
		if(empty($rule)) return false;

		return $rule['rate'];
	}

	public function calc_service_fee($budget = 0, $rate = null)
	{
		parent::is_exist_contract(); // Determines if exist contract.
		$budget OR $budget = (double) $this->calc_budget();

		if( ! empty($rate)) return $budget * $rate;

		$rate 	= $this->calc_service_fee_rate($budget);

		if(empty($rate))
		{
			$rule = $this->get_service_rule($budget);
			return (double) $rule['amount'] ?: 999999999;
		}

		return $budget * $rate;
	}

	/**
	 * Compute disacount amount from promotions metadata
	 */
	public function calc_disacount_amount()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$term_id = $this->contract->term_id;

		$promotions = get_term_meta_value($term_id, 'promotions') ?: [];
		$promotions AND $promotions = unserialize($promotions);

		$service_fee = (double) get_term_meta_value($term_id, 'service_fee');

		$values = array_map(function($x) use ($service_fee){

			$value 	= (double) $x['value'];

			if('percentage' === $x['unit'])
			{
				$_rate = div($x['value'], 100);
				$value = $service_fee*$_rate;
			}

			return $value;

		}, $promotions);

		return array_sum($values);
	}

	/**
	 * Calc the fct tax.
	 */
	public function get_fct_tax()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$term_id 			= $this->contract->term_id;
		return $fct = div((int) get_term_meta_value($term_id, 'fct'), 100);
	}

	/**
	 * Get the VAT amount
	 *
	 * @return     integer  ( description_of_the_return_value )
	 */
	public function cacl_vat_amount()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$term_id 			= $this->contract->term_id;
		$payment_amount 	= (int) get_term_meta_value($term_id, 'payment_amount');
		$vat 				= div((int) get_term_meta_value($term_id, 'vat'), 100);
		return div($payment_amount, 1+ $vat)*$vat;
	}

	/**
	 * Sets the contract.
	 *
	 * @param      Contract_m  $contract  The contract
	 *
	 * @return     self    ( description_of_the_return_value )
	 */
	public function set_contract($contract = NULL)
	{
		parent::set_contract($contract);
		if( ! $this->contract || $this->contract->term_type != $this->term_type) return FALSE;

		return $this;
	}


	/**
	 * Gets the instance.
	 *
	 * @return     self  The instance.
	 */
	public function get_instance()
	{
		if( ! $this->contract) return FALSE;
		if( ! empty($this->_instance_m) && $this->_instance_m->contract->term_id == $this->contract->term_id)
		{
			$this->_instance_m->set_contract($this->contract);
			return $this->_instance_m;
		}

		$service_type = get_term_meta_value($this->contract->term_id, 'service_type') ?: $this->config->item('default', 'stypes');

		/**
		 * Section for deprecated service type CPC_TYPE
		 */
		if('cpc_type' == $service_type)
		{
			$this->load->model('googleads/cpc_behaviour_m');
			$this->_instance_m = new cpc_behaviour_m();
			$this->_instance_m->set_contract($this->contract);
			return $this->_instance_m;
		}


		// BELOW SECTION FOR ACCOUNT MANAGEMENT TYPE

		$modelEnums = array(
			'fullvat' => [
				'default'	=> 'googleads/account_behaviour_m',
				'rent' 		=> 'googleads/rent_account_behaviour_m',	
			],
			'behaft' => [
				'default'	=> 'googleads/behaft_account_behaviour_m',
				'rent' 		=> 'googleads/behaft_rent_account_behaviour_m'	
			]
		);

		$isBehaft 			= 'customer' == get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type');
		$isAccountForRent 	= (bool) get_term_meta_value($this->contract->term_id, 'isAccountForRent');

		$parent_type	= $isBehaft ? 'behaft' : 'fullvat';
		$child_type		= $isAccountForRent ? 'rent' : 'default';
		$model_path 	= $modelEnums[$parent_type][$child_type];

		$model_path_arr = explode('/', $model_path);
		$model_name 	= end($model_path_arr);
		$this->load->model($model_path);

		$this->_instance_m = new $model_name();
		$this->_instance_m->set_contract($this->contract);
		return $this->_instance_m;
	}

	public function getTypeOfService()
	{
		/**
		 * Section for deprecated service type CPC_TYPE
		 */
		if('cpc_type' == get_term_meta_value($this->contract->term_id, 'service_type'))
		{
			return 'cpc_type';
		}

		$isBehaft = 'customer' == get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type');
		$isAccountForRent = (bool) get_term_meta_value($this->contract->term_id, 'isAccountForRent');

		$parent_type	= $isBehaft ? 'behaft' : 'fullvat';
		$child_type		= $isAccountForRent ? 'rent' : 'default';
		return "{$parent_type}_{$child_type}";
	}

	/**
	 * Calculates the fct.
	 *
	 * @param      integer    $percent  The percent
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The fct.
	 */
	public function calc_fct($percent = self::FCT_TAX, $amount = 0)
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		if( ! is_float($percent)) throw new Exception('% phí nhà thầu phải là kiểu số.');

		if(self::FCT_PERCENTAGE_MIN > $percent) 
			throw new Exception('% phí nhà thầu tối thiểu là '.currency_numberformat(self::FCT_PERCENTAGE_MIN*100,' %'));

		if(self::FCT_PERCENTAGE_MAX < $percent) 
			throw new Exception('% phí nhà thầu tối đa là '.currency_numberformat(self::FCT_PERCENTAGE_MAX*100,' %'));

		return $this->get_instance()->calc_fct($percent, $amount);
	}


	/**
	 * Sync All amount value of this contract
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function sync_all_amount()
	{
		parent::sync_all_amount();
		$this->sync_all_progress();
		return TRUE;
	}


	/**
	 * Đồng bộ các chỉ số bị ảnh thưởng khi có phát sinh chi tiêu
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function sync_all_progress()
	{
		parent::is_exist_contract(); // Determines if exist contract.
		$metadata = array();
		
		if('customer' == get_term_meta_value($this->contract->term_id, 'contract_budget_payment_type')) return $this->get_instance()->sync_all_progress();

		try
		{
			$actual_budget = max($this->calc_actual_budget(), 0);
			update_term_meta($this->contract->term_id, 'actual_budget', $actual_budget);
		}
		catch (Exception $e) {}

		try
		{
			$expected_end_time = $this->calc_expected_end_time();
			update_term_meta($this->contract->term_id, 'expected_end_time', $expected_end_time);
		}
		catch (Exception $e) {}

		try 
		{
			$real_progress = $this->calc_real_progress();
		}
		catch (Exception $e) {}

		try
		{
			$payment_expected_end_time = $this->calc_payment_expected_end_time();
			update_term_meta($this->contract->term_id, 'payment_expected_end_time', $payment_expected_end_time);
		}
		catch (Exception $e) {}

		try 
		{
			$real_progress = $this->calc_real_progress_by_payment();
		}
		catch (Exception $e) {}

		return TRUE;
	}

	/**
	 * Calculates the contract value.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The contract value.
	 */
	public function calc_contract_value()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");
		
		return $this->get_instance()->calc_contract_value();
	}

	/**
     * Tính tổng số ngày đã chạy dịch vụ dựa theo ngày kích hoạt thực hiện dịch vụ
     *
     * @param      <type>  $term_id  The term identifier
     *
     * @return     double  The real progress.
     */
        
    function calc_real_days()
    {
    	parent::is_exist_contract(); // Determines if exist contract.

		$term_id 			= $this->contract->term_id;
        $start_service_time = get_term_meta_value($term_id,'start_service_time');
        if(empty($start_service_time)) throw new Exception('Dịch vụ chưa được kích hoạt thực hiên !');
        
        $start_time 		= start_of_day(get_term_meta_value($term_id, 'googleads-begin_time'));
        $end_service_time 	= get_term_meta_value($term_id, 'end_service_time') ?: start_of_day();

        return diffInDates($start_time,$end_service_time);
    }

	/**
     * Tính tổng ngân sách tiến độ (phải chạy) cho đến thời điểm kết thúc dịch vụ
     *
     * Tổng NS phải chạy = Ngân sách HĐ / Số ngày HĐ * số ngày chạy thực tế
     *
     * @param      int  $term_id  The term identifier
     *
     * @return     double  The real progress.
     */
    function calc_budget_progress()
    {
    	parent::is_exist_contract(); // Determines if exist contract.

		$term_id 	= $this->contract->term_id;
        $start_time = get_term_meta_value($term_id,'start_service_time');
        if(empty($start_time)) throw new Exception('Dịch vụ chưa được kích hoạt thực hiên !');

        $retval = 0;
        $contract_budget 	= $this->calc_budget();
        $contract_days 		= parent::calc_contract_days(); // Tính tổng số ngày chạy dịch vụ dựa theo ngày hợp đồng
        $real_days 			= $this->calc_real_days();

        return div($contract_budget, $contract_days)*$real_days;
    }


    /**
     * Tính tổng ngân sách tiến độ (phải chạy) cho đến thời điểm kết thúc dịch vụ
     *
     * Tổng NS thực thu phải chạy = Ngân sách HĐ / Số ngày HĐ * số ngày chạy thực tế
     *
     * @param      int  $term_id  The term identifier
     *
     * @return     double  The real progress.
     */
    function calc_actual_budget_progress()
    {
    	parent::is_exist_contract(); // Determines if exist contract.
    	if( ! is_service_proc($this->contract->term_id)) throw new Exception('Dịch vụ chưa được kích hoạt thực hiên !');

    	$actual_budget 		= $this->calc_actual_budget(); // Ngân sách thực thu
    	$actual_budget_days = $this->calc_actual_budget_days(); // Số ngày thực hiện hợp đồng dựa trên ngân sách thực thu
    	$real_days 			= $this->calc_real_days(); // Số ngày đã thực hiện dịch vụ

    	return div($actual_budget, $actual_budget_days)*$real_days;
    }


	/**
	 * Calculates the budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The budget.
	 */
	public function calc_budget()
	{
		parent::is_exist_contract(); // Determines if exist contract.
		return $this->get_instance()->calc_budget();
	}


	/**
	 * Creates invoices.
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function create_invoices()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$term_id = $this->contract->term_id;

		if('customer' == get_term_meta_value($term_id, 'contract_budget_payment_type')) return $this->get_instance()->create_invoices();

		$contract_begin 	= get_term_meta_value($term_id,'contract_begin');
		$contract_end 		= get_term_meta_value($term_id,'contract_end');
		$num_dates 			= diffInDates($contract_begin,$contract_end);

		$service_type 	= get_term_meta_value($term_id,'service_type') ?: 'cpc_type';
		$start_date 	= $contract_begin;
		$total_amount 	= 0;

		$number_of_payments = get_term_meta_value($term_id,'number_of_payments') ?: 1;

		$budget 			= $this->calc_budget(); // Ngân sách QC
		$budget_per_payment = div($budget, $number_of_payments); // Ngân sách QC mỗi đợt thanh toán

		$service_provider_tax_rate 			= (float) get_term_meta_value($term_id, 'service_provider_tax_rate');
		$service_provider_tax_per_payment 	= $budget_per_payment * $service_provider_tax_rate;

		$service_fee 		= (int) get_term_meta_value($term_id, 'service_fee'); // Phí dịch vụ
		$sfee_per_payment	= div($service_fee, $number_of_payments); // Phí dịch vụ mỗi đợt thanh toán

		$service_fee_payment_type = get_term_meta_value($term_id, 'service_fee_payment_type');
	    if( empty($service_fee_payment_type)) // Khởi tạo giá trị mặc định cho loại thanh toán phí dịch vụ dựa trên file config
	    {
			$this->config->load('googleads/contract');
			$service_fee_payment_type_def 	= $this->config->item('service_fee_payment_type');
	        $service_fee_payment_type_def 	= array_keys($service_fee_payment_type_def);
	        $service_fee_payment_type 		= reset($service_fee_payment_type_def);
	    }

	    $fct 				= (int) get_term_meta_value($term_id, 'fct');
	    $fct_per_payment 	= !empty($fct) ? div(div($budget, 0.95)*div($fct, 100), $number_of_payments) : 0;

	    $apply_discount_type 			= get_term_meta_value($term_id, 'apply_discount_type') ?: 'last_invoice';
	    $discount_amount 				= $this->calc_disacount_amount();
	    $discount_amount_per_payment 	= div($discount_amount, $number_of_payments);

	    $num_days4inv 	= ceil(div($num_dates, $number_of_payments));
		$i 				= 0;
		for($i ; $i < $number_of_payments; $i++)
		{	
			if($num_days4inv == 0) break;

			/* Khởi tạo đợt thanh toán */
			$end_date = strtotime('+'.$num_days4inv.' day -1 second', $start_date);
			$end_date = $this->mdate->endOfDay($end_date);

			$insert_data = array(
				'post_title' 	=> 'Thu tiền đợt '. ($i + 1),
				'post_content' 	=> '',
				'start_date' 	=> $start_date,
				'end_date' 		=> $end_date,
				'post_type' 	=> $this->invoice_m->post_type );

			$inv_id = $this->invoice_m->insert($insert_data);
			if(empty($inv_id)) continue;

			$this->term_posts_m->set_post_terms($inv_id, $term_id, $this->contract->term_type);

			$invi_insert_data = array();

			// Khởi tạo hạng mục "Ngân sách QC"
			$invi_insert_data[] = array(
				'invi_title' 	=> 'Ngân sách quảng cáo Google',
				'invi_status' 	=> 'publish',
				'invi_description' => '',
				'quantity' 	=> 1,
				'invi_rate' => 100,
				'inv_id' 	=> $inv_id,
				'price' 	=> $budget_per_payment,
				'total' 	=> $this->invoice_item_m->calc_total_price($budget_per_payment, 1, 100)
			);

			// Khởi tạo hạng mục "Phí nhà thầu"
			if( ! empty($fct_per_payment))
			{
				$invi_insert_data[] = array(
					'invi_title' 	=> 'Thuế nhà thầu ('.currency_numberformat($fct, '%').')', 
					'invi_status' 	=> 'publish',
					'invi_description' => '',
					'quantity' 	=> 1,
					'invi_rate' => 100,
					'inv_id' 	=> $inv_id,
					'price' 	=> $fct_per_payment,
					'total' 	=> $this->invoice_item_m->calc_total_price($fct_per_payment, 1, 100)
				);
			}

			if( ! empty($service_provider_tax_per_payment))
			{
				$invi_insert_data[] = array(
					'invi_title' 	=> 'Thuế Google thu ('.currency_numberformat($service_provider_tax_rate*100, '%').')', 
					'invi_status' 	=> 'publish',
					'invi_description' => '',
					'quantity' 	=> 1,
					'invi_rate' => 100,
					'inv_id' 	=> $inv_id,
					'price' 	=> $service_provider_tax_per_payment,
					'total' 	=> $this->invoice_item_m->calc_total_price($service_provider_tax_per_payment, 1, 100)
				);
			}

			/* Khởi tạo với hạng mục trong đợt thanh toán */
			if($i == 0 && 'full' == $service_fee_payment_type)
			{
				$invi_insert_data[] = array(
					'invi_title' 		=> 'Phí dịch vụ quảng cáo (thanh toán 100% đợt đầu)',
					'invi_status' 		=> 'publish',
					'invi_description' 	=> '',
					'quantity' 			=> 1,
					'invi_rate' 		=> 100,
					'inv_id' 			=> $inv_id,
					'price' 			=> $service_fee,
					'total' 			=> $this->invoice_item_m->calc_total_price($service_fee, 1, 100)
				);

				$sfee_per_payment = 0;
			}

			if( ! empty($sfee_per_payment))
			{
				$invi_insert_data[] = array(
					'invi_title' 	=> 'Phí dịch vụ quảng cáo',
					'invi_status' 	=> 'publish',
					'invi_description' => '',
					'quantity' 	=> 1,
					'invi_rate' => 100,
					'inv_id' 	=> $inv_id,
					'price' 	=> $sfee_per_payment,
					'total' 	=> $this->invoice_item_m->calc_total_price($sfee_per_payment, 1, 100)
				);
			}

			if( ! empty($discount_amount))
			{
				switch ($apply_discount_type)
				{
					case 'last_invoice':
						if($i != $number_of_payments - 1) break;
						$invi_insert_data[] = array(
							'invi_title' 	=> 'Chương trình giảm giá',
							'invi_status' 	=> 'publish',
							'invi_description' => '',
							'quantity' 	=> 1,
							'invi_rate' => 100,
							'inv_id' 	=> $inv_id,
							'price' 	=> $discount_amount,
							'total' 	=> $this->invoice_item_m->calc_total_price(-$discount_amount, 1, 100)
						);
						break;
					
					default:
						$invi_insert_data[] = array(
							'invi_title' 	=> 'Chương trình giảm giá',
							'invi_status' 	=> 'publish',
							'invi_description' => '',
							'quantity' 	=> 1,
							'invi_rate' => 100,
							'inv_id' 	=> $inv_id,
							'price' 	=> $discount_amount_per_payment,
							'total' 	=> $this->invoice_item_m->calc_total_price(-$discount_amount_per_payment, 1, 100)
						);
						break;
				}
			}

			foreach ($invi_insert_data as $_invi)
			{
				$this->invoice_item_m->insert($_invi);
			}

			$start_date = strtotime('+1 second', $end_date);
			$day_end = $num_dates - $num_days4inv;
			if($day_end < $num_days4inv) $num_days4inv = $day_end;
		}

		update_term_meta($term_id, 'contract_value', $this->calc_contract_value());
		return TRUE;
	}


	/**
	 * Render Pritable contract
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     Array   Data result for Preview Action
	 */
	public function prepare_preview()
	{
		if( ! $this->contract) throw new Exception('Hợp đồng chưa được khởi tạo.');
		
		parent::prepare_preview();

		$fct 				= (int) get_term_meta_value($this->contract->term_id, 'fct'); // % thuế nhà thầu (đánh trên ngân sách quảng cáo)
		$this->data['fct'] 	= $fct;
		
		$service_provider_tax_rate = (float) get_term_meta_value($this->contract->term_id, 'service_provider_tax_rate'); // % thuế nhà thầu (đánh trên ngân sách quảng cáo)
		$this->data['service_provider_tax_rate'] 	= $service_provider_tax_rate;

		/* Chi tiết các đợt thanh toán */
		$invoices = $this->term_posts_m->get_term_posts($this->contract->term_id, $this->invoice_m->post_type, ['orderby'=>'posts.end_date']) ?: [];
		$number_of_payments = count($invoices);
		$payments_stack 	= [];

		switch (TRUE)
		{
			case $number_of_payments == 1:
				$payments_stack[] = "<p>Bên A thanh toán cho Bên B 100% giá trị vào ngày hai bên ký kết hợp đồng này.</p>";
				break;
			
			default:
				$payments_stack[] = "<p>Bên A thanh toán cho Bên B làm {$number_of_payments} đợt:</p>";
				$payments_stack[] = '<ul>';

				$i 		= 1;
				$_vat 	= $this->data['vat'];
				foreach ($invoices as $inv)
				{
					if( ! empty($fct))
					{
						
					}

				    $_total 	= numberformat(cal_vat($this->invoice_item_m->get_total($inv->post_id, 'total'), $_vat), 0, '.', '');
				    $_total_f 	= currency_numberformat($_total,'đ');
				    $_total_w 	= currency_number_to_words($_total);
				    $_end_date 	= my_date($inv->end_date,'d/m/Y');

				    $payments_stack[] = '<li> Đợt '.($i++).": thanh toán {$_total_f} ($_total_w), chậm nhất là ngày {$_end_date}.</li>";
				}

				$payments_stack[] = '</ul>';
				break;
		}

		$this->data['payments'] = implode('', $payments_stack);

		/* Rules theo từng dịch vụ */
		$this->load->config('googleads/contract');
		$network_rules  = $this->config->item('printable-network-rules');
		$network_type   = get_term_meta_value($this->contract->term_id,'network_type');
		$network_type   = $network_type ? explode(',', $network_type) : $this->config->item('default', 'printable-network-rules');

		$network_rules_stack = [];
		if( ! empty($network_type))
		{
		    $i = 1;

		    foreach ($network_type as $network) 
		    {
		        if(empty($network_rules[$network])) continue;

		        $network_rules_stack[] = '<p><strong>'. (count($network_type) > 1 ? ($i++).'.' : '')." {$network_rules[$network]['title']}:</strong>{$network_rules[$network]['content']}</p>";
		    }
		}
		$this->data['network_rules'] = implode('', $network_rules_stack);

		/* BEGIN::CONTRACT CURATORS */
		$contract_curators = get_term_meta_value($this->contract->term_id, 'contract_curators');
		$contract_curators = is_serialized($contract_curators) ? unserialize($contract_curators) : [];
		if( ! empty($contract_curators)) $this->data['contract_curators'] = $contract_curators;
		/* END::CONTRACT CURATORS */

		return wp_parse_args($this->get_instance()->prepare_preview(), $this->data);
	}


	/**
	 * Tính tiến độ thực hiện thực tế đến thời điểm kết thúc dịch vụ
	 * hoặc thời gian tính nếu hợp đồng vẫn đang thực hiện
	 *
	 * Tiến độ = Tổng NS đã chạy / Tổng NS phải chạy
	 *
	 * @param      int  $term_id  The term identifier
	 *
	 * @return     double  The real progress.
	 */
	function calc_real_progress()
	{
		parent::is_exist_contract(); // Determines if exist contract.
		if( ! is_service_proc($this->contract->term_id)) return 0; // Nếu HĐ chưa được kích hoạt thực hiện thì return 0

	    $result = numberformat(div($this->get_actual_result(),$this->calc_budget_progress())*100, 2);

	    update_term_meta($this->contract->term_id, 'real_progress', $result);

	    return $result;
	}

	/**
	 * Tính tiến độ thực hiện thực tế đến thời điểm kết thúc dịch vụ
	 * hoặc thời gian tính nếu hợp đồng vẫn đang thực hiện
	 *
	 * Tiến độ = Tổng NS đã chạy / Tổng NS phải chạy
	 *
	 * @param      int  $term_id  The term identifier
	 *
	 * @return     double  The real progress.
	 */
	function calc_real_progress_by_payment()
	{
		parent::is_exist_contract(); // Determines if exist contract.
		if( ! is_service_proc($this->contract->term_id)) return 0; // Nếu HĐ chưa được kích hoạt thực hiện thì return 0

	    $result = numberformat(div($this->get_actual_result(),$this->calc_actual_budget_progress())*100, 2);

	    update_term_meta($this->contract->term_id, 'payment_real_progress', $result);

	    return $result;
	}

	/**
	  * Tính thời gian dự kiến kết thúc
	  *
	  * Dự kiến kết thúc = [Tổng NS tiến độ]/[Tổng NS đã chạy]*[Số ngày HD] - 1 + Ngày kích hoạt thực hiện dịch vụ
	  *
	  * @throws     Exception  Error , stop and return
	  *
	  * @return     int        Thời gian dự kiến kết thúc dịch vụ dựa theo tiến độ thực tế
	  */
    public function calc_expected_end_time()
    {
    	parent::is_exist_contract(); // Determines if exist contract.
    	if( ! is_service_proc($this->contract->term_id)) throw new Exception('Dịch vụ chưa được thực hiện');

    	return $this->get_instance()->calc_expected_end_time();
    }

    /**
     * Tính thời gian dự kiến kết thúc
     *
     * Dự kiến kết thúc = [Tổng NS tiến độ]/[Tổng NS đã chạy]*[Số ngày HD] - 1 +
     * Ngày kích hoạt thực hiện dịch vụ
     *
     * @throws     Exception  Error , stop and return
     *
     * @return     int        Thời gian dự kiến kết thúc dịch vụ dựa theo tiến độ thực tế
     */
    public function calc_payment_expected_end_time()
    {
    	parent::is_exist_contract(); // Determines if exist contract.
    	if( ! is_service_proc($this->contract->term_id)) throw new Exception('Dịch vụ chưa được thực hiện');

    	return $this->get_instance()->calc_payment_expected_end_time();
    }


    /**
     * Gets the actual result.
     */
    public function get_actual_result()
    {
    	if( ! $this->contract) return 0;

    	$term_id 		= $this->contract->term_id;
    	$actual_result 	= (float) get_term_meta_value($term_id,'actual_result') ?: 0;
    	return $actual_result;
    }

    /**
     * Calculates the cost per day left.
     *
     * @param      string     $budget_type  The budget type
     *
     * @throws     Exception  (description)
     *
     * @return     <type>     The cost per day left.
     */
    public function calc_cost_per_day_left($budget_type = 'commit')
    {
    	parent::is_exist_contract(); // Determines if exist contract.
    	if( ! is_service_proc($this->contract->term_id)) throw new Exception('Dịch vụ chưa được thực hiện');

    	$left_days 		= parent::calc_contract_days() - $this->calc_real_days();
		$left_days 		= $left_days > 0 ? $left_days : 1;
		$actual_result 	= $this->get_actual_result();

		$budget 		= 0;
		switch ($budget_type)
		{
			case 'commit': 	$budget = $this->calc_budget(); break;
			case 'payment': $budget = $this->calc_actual_budget(); break;
			default: break;
		}

		return div(($budget - $actual_result), $left_days);
    }

    /**
     * Tính số ngày thực hiện hợp đồng dựa trên ngân sách thực thu
     *
     * @throws     Exception  (description)
     *
     * @return     integer    The actual budget days.
     */
    public function calc_actual_budget_days()
    {
    	parent::is_exist_contract(); // Determines if exist contract.
    	if( ! is_service_proc($this->contract->term_id)) throw new Exception('Dịch vụ chưa được thực hiện');

    	$actual_budget 		= $this->calc_actual_budget(); // Ngân sách thực thu
    	$contract_budget 	= $this->calc_budget(); // Ngân sách cam kết trên hợp đồng
    	$rate 				= div($actual_budget, $contract_budget); // Tỉ lệ ngân sách thực thu so với cam kết
    	$contract_days 		= parent::calc_contract_days(); // Số ngày thực hiện hợp đồng

    	return $contract_days*$rate; 
    }

    /* Clone new contract */
	public function clone()
	{
		$_contract = $this->contract;
		$insert_id = parent::clone();
		if( ! $insert_id) throw new Exception('Quá trình thêm mới không thành công');

		$term = $this->contract;
		$this->load->model('term_users_m');
		$this->load->model('customer/customer_m');
		
		$insert_data = (array) $term;
		$insert_data['term_status'] = 'unverified';
		unset($insert_data['term_id']);

		/* Clone các trường mặc định của hợp đồng */
		$metadata = get_term_meta_value($term->term_id);
		$metadata = key_value($metadata, 'meta_key', 'meta_value');

        $account_currency_code = $metadata['account_currency_code'];
        $meta_key = strtolower("exchange_rate_{$account_currency_code}_to_vnd");
		$metadata[$meta_key] = get_exchange_rate(1, $account_currency_code);

		$this->config->load('googleads/contract');
		$fields = array_intersect(array_keys($metadata), $this->config->item('metadata_googleads'));
		if( ! empty($fields))
		{
			foreach ($fields as $f)
			{
				if(empty($metadata[$f])) continue;
				update_term_meta($insert_id, $f, $metadata[$f]);
			}
		}
		
		return $insert_id;
	}

	/**
	 * Sets the technician identifier.
	 */
	public function setTechnicianId()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$term_id = $this->contract->term_id;

		$this->load->model('googleads/googleads_kpi_m');

		$kpis = $this->googleads_kpi_m->select('user_id')->distinct('user_id')->where('term_id', $this->contract->term_id)->get_all();
		if( ! $kpis)
		{
			update_term_meta($this->contract->term_id, 'technicianId', 0);
			return true;
		}

		$roleNameEnums 	= [ $this->option_m->get_value('ads_role_id') => 'gat', $this->option_m->get_value('leader_ads_role_id') => 'am', $this->option_m->get_value('manager_ads_role_id') => 'manager' ];
		$staffs 		= array_map(function($x) use ($roleNameEnums){
			$_staff = $this->admin_m->get_field_by_id($x->user_id);
			$_staff['roleName'] = $roleNameEnums[$_staff['role_id']] ?? 'unknown';
			return $_staff;
		}, $kpis);
		
		if( 1 == count($staffs))
		{
			$_staff = reset($staffs);
			update_term_meta($this->contract->term_id, 'technicianId', $_staff['user_id']);
			return true;
		}
		$staffsGrouped = array_group_by($staffs, 'roleName');

		foreach (array_values($roleNameEnums) as $roleName)
		{
			if(empty($staffsGrouped[$roleName])) continue;

			$_staff = reset($staffsGrouped[$roleName]);
			update_term_meta($this->contract->term_id, 'technicianId', $_staff['user_id']);
			return true;
		}

		$_staff = reset($staffs);
		update_term_meta($this->contract->term_id, 'technicianId', $_staff['user_id']);
		return true;
	}

	/**
	 * Gets the progress.
	 *
	 * @param      <type>   $start_time    The start time
	 * @param      <type>   $end_time      The end time
	 *
	 * @return     integer  The progress.
	 */
	public function get_the_progress($start_time = 0, $end_time = 0)
    {
    	$all_time 	= empty($start_time) && empty($end_time);
    	$actual 	= 0;
    	$progress 	= 0;
    	$target 	= get_term_meta_value($this->contract->term_id, 'contract_budget');

    	if(!empty($start_time))
        {
            $start_time = start_of_day($start_time);
        }

        if(!empty($end_time))
        {
            $end_time = end_of_day($end_time);
        }

        $this->googleads_m->set_contract($this->contract);

		$insight = $this->googleads_m->get_insights(['start_time' => $start_time, 'end_time' => $end_time, 'summary' => TRUE]);
		$insight AND $actual = $insight['spend']; 

    	$progress = number_format(div($actual, $target)*100, 3);
    	if( ! $all_time) return $progress;

    	/* Store last adword account data updated time then update result updated on by current time excuted */
    	$actual != get_term_meta_value($this->contract->term_id, 'actual_result') 
    	AND update_term_meta($this->contract->term_id, 'result_updated_on', time());

        /* update actual_result */
        update_term_meta($this->contract->term_id, 'actual_result', $actual);

        $actual_progress_percent = number_format((double)get_term_meta_value($this->contract->term_id, 'actual_progress_percent'), 3);
        if($actual_progress_percent != $progress) update_term_meta($this->contract->term_id,'actual_progress_percent',$progress);

        /* Cập nhật ngân sách */
        $actual_budget                  = (int) get_term_meta_value($this->contract->term_id, 'actual_budget');
        $actual_progress_percent_net    = number_format(div($actual, $actual_budget), 3);
        if($actual_progress_percent_net != number_format( (double) get_term_meta_value($this->contract->term_id,'actual_progress_percent_net'),3))  
        update_term_meta($this->contract->term_id, 'actual_progress_percent_net', $actual_progress_percent_net);

        return $progress;
    }

    /**
     * Delete All Ads Segments
     *
     * @return     boolean  ( description_of_the_return_value )
     */
    public function delete_segments()
	{
		parent::is_exist_contract(); // Determines if exist contract.
		$contractId = $this->contract->term_id;

		$this->load->model('ads_segment_m');

		$adssegments 	= $this->term_posts_m->get_term_posts($contractId, $this->ads_segment_m->post_type);
		if(empty($adssegments)) return true;

		$this->load->model('googleads/mcm_account_m');
		$adsSegmentsIds = array_filter(array_column($adssegments, 'post_id'));
		array_walk($adsSegmentsIds, function($x){
			$adaccounts = $this->term_posts_m->get_post_terms($x, $this->mcm_account_m->term_type);
			if( ! empty($adaccounts))
			{
				$adaccountIds = array_column($adaccounts, 'term_id');
                $this->ads_segment_m->detach_ad_account($x, $adaccountIds, $this->mcm_account_m->term_type);
			}
		});

		$this->ads_segment_m->delete_many($adsSegmentsIds);
		$this->term_posts_m->delete_term_posts($contractId, $adsSegmentsIds);
		return true;
	}

	/**
	 * Proc Service
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	function proc_service()
	{
		parent::is_exist_contract(); // Determines if exist contract.
		$contractId = $this->contract->term_id;

		$now = time();
		update_term_meta($contractId, 'start_service_time', $now);
        update_term_meta($contractId, 'adaccount_status' , 'APPROVED');

		/* Luôn luôn lấy tỉ giá ngay tại thời điểm function được gọi*/
        $currency = get_term_meta_value($contractId, 'account_currency_code');
        $meta_exchange_rate_key = strtolower("exchange_rate_{$currency}_to_vnd");
		update_term_meta($contractId, $meta_exchange_rate_key, get_exchange_rate());

		$this->log_m->insert(array(
			'log_type' =>'start_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $contractId,
			'log_content' => my_date($now, 'Y/m/d H:i:s')
			));

		$this->load->model('googleads/googleads_report_m');
		$stat = $this->googleads_report_m->send_activation_mail2customer($contractId);

		try
        {
        	$this->get_the_progress();
        	$this->sync_all_amount();
        }
        catch (Exception $e) {}

		return $stat;
	}

	/**
	 * Stops a service.
	 */
	function stop_service()
	{
		parent::is_exist_contract(); // Determines if exist contract.
		$contractId = $this->contract->term_id;

		$contract_m = (new googleads_m())->set_contract($this->contract);
		$segments 	= $contract_m->getSegments();

		$start_date = array_map(function($x){
			return empty($x) ? start_of_day() : start_of_day($x);
		}, array_column($segments, 'start_date')); 

		$end_date = array_map(function($x){
			return empty($x) ? end_of_day() : end_of_day($x);
		}, array_column($segments, 'end_date'));

		update_term_meta($contractId, 'googleads-begin_time', min($start_date));
		update_term_meta($contractId, 'googleads-end_time', max($end_date));
		
		update_term_meta($contractId, 'advertise_start_time', min($start_date));
		update_term_meta($contractId, 'advertise_end_time', max($end_date));

		update_term_meta($contractId, 'end_service_time', time());

        $service_fee_payment_type = get_term_meta_value($contractId, 'service_fee_payment_type');
        if('range' == $service_fee_payment_type){
            $this->calc_range_service_fee();
        }

		$this->log_m->insert(array(
			'log_type' =>'end_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $contractId,
			'log_content' => date('Y/m/d H:i:s')
		));

		$this->googleads_m->set_term_type()->update($contractId, array('term_status'=>'ending'));

        // Dispatch message queue to send finished email
        $payload = [
            'contract_id' => $this->contract->term_id,
            'start_date' => my_date(min($start_date), 'Y-m-d'),
            'end_date' => my_date(max($end_date), 'Y-m-d'),
        ];
        (new RabbitMQ())->publish('notification', $payload);

		return true;
	}

    public function recalc_receipt(){
        $contract_id = $this->contract->term_id;

		$insert_id = $this->log_m->insert([ 'log_type' => 'queue-receipt-recalc-metadata', 'log_status' => 0, 'log_content' => $contract_id ]);

		$this->load->config('amqps');
		$amqps_host 	= $this->config->item('host', 'amqps');
		$amqps_port 	= $this->config->item('port', 'amqps');
		$amqps_user 	= $this->config->item('user', 'amqps');
		$amqps_password = $this->config->item('password', 'amqps');
		
        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];

		$connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
		$channel 	= $connection->channel();
		$channel->queue_declare($queue, false, true, false, false);

		$payload = [
			'event' => 'contract_payment.recalc.metadata',
			'contract_id' => $contract_id,
			'log_id' => $insert_id
		];

		$message = new \PhpAmqpLib\Message\AMQPMessage(
			json_encode($payload),
			array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
		);

		$channel->basic_publish($message, '', $queue);	
		$channel->close();
		$connection->close();
    }

    public function get_adaccount(){
        $contract_id = $this->contract->term_id;

        $adaccounts = $this->term_posts_m
        ->join('posts AS ads_segment', 'ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = "ads_segment"')
        ->join('term_posts AS tp_adaccount', 'tp_adaccount.post_id = ads_segment.post_id')
        ->join('term AS adaccount', 'adaccount.term_id = tp_adaccount.term_id AND adaccount.term_type = "' . $this->mcm_account_m->term_type . '"')

        ->where('term_posts.term_id', $contract_id)

        ->select('
            adaccount.term_id AS term_id, 
            adaccount.term_name AS account_id,
            ads_segment.start_date AS start_date,
            ads_segment.end_date AS end_date
        ')
        ->as_array()
        ->get_all();
        if(empty($adaccounts)) return [];

        $adaccounts = array_map(function($item){
            $adaccount_id = $item['term_id'];

            $adaccount_meta = get_term_meta_value($adaccount_id);
            $adaccount_meta = key_value($adaccount_meta, 'meta_key', 'meta_value');
            
            $item = array_merge($item, $adaccount_meta);
            
            return $item;
        }, $adaccounts);

        return $adaccounts;
    }

    /**
     * Retrieves segments based on the given extend selects and contract ID.
     *
     * @param array $extend_selects An array of extended select fields.
     *
     * @throws Some_Exception_Class Description of exception
     *
     * @return array An array of segments.
     */
    public function get_segments(array $extend_selects = []){
        $contract_id = $this->contract->term_id;

        $segments = $this->term_posts_m
        ->join('posts AS ads_segment', 'ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = "ads_segment"')
        ->join('term_posts AS tp_adaccount', 'tp_adaccount.post_id = ads_segment.post_id')
        ->join('term AS adaccount', 'adaccount.term_id = tp_adaccount.term_id AND adaccount.term_type = "' . $this->mcm_account_m->term_type . '"')

        ->where('term_posts.term_id', $contract_id)

        ->select('
            ads_segment.post_id AS post_id,
            adaccount.term_id AS adaccount_id,
            term_posts.term_id AS contract_id, 
            ads_segment.start_date AS start_date,
            ads_segment.end_date AS end_date
        ');

        $allow_select = [
            'assigned_technician_id',
            'assigned_technician_rate'
        ];
        array_walk($extend_selects, function($extend_select) use ($allow_select, $segments){
            if(!in_array($extend_select, $allow_select))
            {
                return;
            }

            switch($extend_select)
            {
                case 'assigned_technician_id':
                    $segments->select('ads_segment.post_author AS assigned_technician_id');
                    return;
                case 'assigned_technician_rate':
                    $segments->select('ads_segment.post_content AS assigned_technician_rate');
                    return;
                default:
                    return;
            }
        });

        $segments = $segments->as_array()->get_all();
        if(empty($segments)) return [];

        return $segments;
    }

    /**
     * lock_manipulation
     *
     * @param  mixed $time
     * @return void
     */
    public function lock_manipulation($time){
        parent::is_exist_contract(); // Determines if exist contract.

        $is_locked = (bool) (get_term_meta_value($this->contract->term_id, 'is_manipulation_locked') ?: FALSE);
        if($is_locked)
        {
            return FALSE;
        }

        update_term_meta($this->contract->term_id, 'is_manipulation_locked', !$is_locked);
        update_term_meta($this->contract->term_id, 'manipulation_locked_at', $time);

        $this->log_m->insert([
            'log_type' =>'lock_manipulation', 
            'user_id' => $this->admin_m->id,
            'term_id' => $this->contract->term_id,
            'log_content' => !$is_locked,
            'log_time_create' => my_date(time(),'Y/m/d H:i:s')
        ]);
    }

    /**
     * unlock_manipulation
     *
     * @param  mixed $time
     * @return void
     */
    public function unlock_manipulation(){
        parent::is_exist_contract(); // Determines if exist contract.

        $is_locked = (bool) (get_term_meta_value($this->contract->term_id, 'is_manipulation_locked') ?: FALSE);
        if(!$is_locked)
        {
            return FALSE;
        }

        update_term_meta($this->contract->term_id, 'is_manipulation_locked', !$is_locked);
        update_term_meta($this->contract->term_id, 'manipulation_locked_at', '');

        $this->log_m->insert([
            'log_type' =>'lock_manipulation', 
            'user_id' => $this->admin_m->id,
            'term_id' => $this->contract->term_id,
            'log_content' => !$is_locked,
            'log_time_create' => my_date(time(),'Y/m/d H:i:s')
        ]);
    }
}
/* End of file Googleads_behaviour_m.php */
/* Location: ./application/modules/googleads/models/Googleads_behaviour_m.php */