<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/googleads/models/Googleads_behaviour_m.php');

class Cpc_behaviour_m extends Googleads_behaviour_m{

	public $stype = 'cpc_type';

	/**
	 * Calculates the fct.
	 *
	 * @param      float|integer  $percent  The percent
	 * @param      integer        $amount   The amount
	 *
	 * @return     <type>         The fct.
	 */
	public function calc_fct($percent = 0.05, $amount = 0)
	{
		$contract_cpc 		= (int) get_term_meta_value($this->contract->term_id,'contract_cpc');
		$contract_clicks 	= (int) get_term_meta_value($this->contract->term_id,'contract_clicks');
		$bugdet 			= $contract_cpc * $contract_clicks;

		return div($bugdet, 1-parent::FCT_TAX)*$percent;
	}

	/**
	 * Calculates the contract value.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The contract value.
	 */
	public function calc_contract_value()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$contract_cpc 		= (int) get_term_meta_value($this->contract->term_id, 'contract_cpc');
		$contract_clicks 	= (int) get_term_meta_value($this->contract->term_id, 'contract_clicks');

		return $contract_cpc * $contract_clicks;
	}

	/**
	 * Calculates the budget.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     The budget.
	 */
	public function calc_budget()
	{
		parent::is_exist_contract(); // Determines if exist contract.
		$contract_cpc 		= (int) get_term_meta_value($this->contract->term_id, 'contract_cpc');
		$contract_clicks 	= (float) get_term_meta_value($this->contract->term_id, 'contract_clicks');
		return $contract_cpc * $contract_clicks;
	}

	/**
	 * Render Pritable contract
	 *
	 * @return     Array   Data result for Preview Action
	 */
	public function prepare_preview()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$data = $this->data;
		$data['view_file'] 		= 'googleads/contract/preview/cpc';
		$data['data_service'] 	= array('service_name' => 'Quảng cáo Google Search');
		$data['campaign_types'] = array();

		$cpc 	= (int) get_term_meta_value($this->contract->term_id, 'contract_cpc');
		$clicks = (int) get_term_meta_value($this->contract->term_id, 'contract_clicks');
		if(!empty($cpc) && !empty($clicks))
		{
			$data['campaign_types']['google_search'] = array('cpc' => $cpc, 'clicks' => $clicks);
		}

		$gdn_cpc 	= (int) get_term_meta_value($this->contract->term_id, 'gdn_contract_cpc');
		$gdn_clicks = (int) get_term_meta_value($this->contract->term_id, 'gdn_contract_clicks');
		if(!empty($gdn_cpc) && !empty($gdn_clicks))
		{
			$data['campaign_types']['google_display_network'] = array('cpc' => $gdn_cpc, 'clicks' => $gdn_clicks);
		}

		return $data;
	}

	/**
     * Calculates the expected end time.
     *
     * @param      <type>   $term_id  The term identifier
     *
     * @return     integer  The expected end time.
     */
    public function calc_expected_end_time()
    {
    	parent::is_exist_contract(); // Determines if exist contract.
    	if( ! is_service_proc($this->contract->term_id)) throw new Exception('Dịch vụ chưa được thực hiện');

    	$term_id = $this->contract->term_id;

        $expected_end_time 		= 0; // Thời gian dự kiến kết thúc

        $commit_start_time 	= (int) get_term_meta_value($term_id,'googleads-begin_time');
        $commit_end_time 	= (int) get_term_meta_value($term_id,'googleads-end_time');
        $commit_day 		= round(($commit_end_time - $commit_start_time)/86400) + 1;

        $commit_click = ((int) get_term_meta_value($term_id, 'contract_clicks')) + ((int) get_term_meta_value($term_id, 'gdn_contract_clicks'));

        $actual_start_time 		= get_term_meta_value($term_id,'start_service_time');
        $actual_end_time 		= strtotime('-1 second', start_of_day());  
        $actual_day 			= round(($actual_end_time - $actual_start_time)/86400) + 1;
        $total_clicks_progress 	= div($commit_click, $commit_day)*$actual_day;

        /* Tính tổng ngân sách thực tế */
        $progress 				= $this->get_the_progress($term_id);
        $total_clicks_actual 	= div($progress, 100) * $commit_click;

        if($total_clicks_actual > 0) return $actual_start_time;

        $expected_day = (int) round(div($total_clicks_progress,$total_clicks_actual) * $commit_day) - 1;
        if($expected_day <= 0) return $actual_start_time;

        return strtotime("+{$expected_day} days", $actual_start_time);
    }

    /**
     * Calculates the expected end time.
     *
     * @param      <type>   $term_id  The term identifier
     *
     * @return     integer  The expected end time.
     */
    public function calc_payment_expected_end_time()
    {
    	parent::is_exist_contract(); // Determines if exist contract.
    	if( ! is_service_proc($this->contract->term_id)) throw new Exception('Dịch vụ chưa được thực hiện');

    	$term_id = $this->contract->term_id;

        $expected_end_time 		= 0; // Thời gian dự kiến kết thúc

        $commit_start_time 	= (int) get_term_meta_value($term_id,'googleads-begin_time');
        $commit_end_time 	= (int) get_term_meta_value($term_id,'googleads-end_time');
        $commit_day 		= round(($commit_end_time - $commit_start_time)/86400) + 1;

        $commit_click = ((int) get_term_meta_value($term_id, 'contract_clicks')) + ((int) get_term_meta_value($term_id, 'gdn_contract_clicks'));

        $actual_start_time 		= get_term_meta_value($term_id,'start_service_time');
        $actual_end_time 		= strtotime('-1 second', start_of_day());  
        $actual_day 			= round(($actual_end_time - $actual_start_time)/86400) + 1;
        $total_clicks_progress 	= div($commit_click, $commit_day)*$actual_day;

        /* Tính tổng ngân sách thực tế */
        $progress = parent::calc_real_progress_by_payment();
        $total_clicks_actual 	= div($progress, 100) * $commit_click;

        if($total_clicks_actual > 0) return $actual_start_time;

        $expected_day = (int) round(div($total_clicks_progress,$total_clicks_actual) * $commit_day) - 1;
        if($expected_day <= 0) return $actual_start_time;

        return strtotime("+{$expected_day} days", $actual_start_time);
    }
}
/* End of file Cpc_behaviour_m.php */
/* Location: ./application/modules/googleads/models/Cpc_behaviour_m.php */