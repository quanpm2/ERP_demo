<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use AdsService\AdWords\Reports\XlsWeeklyReport;

class Googleads_report_m extends Base_model {

    public $recipients = NULL;

    function __construct()
    {
        parent::__construct();

        $models = array(
            'log_m',
            'googleads/googleads_m',
            'googleads/googleads_kpi_m',
            'googleads/googleads_log_m',
            'googleads/mcm_account_m'
            );

        $this->load->model($models);
        $this->load->library('googleads/adwords');
        $this->load->library('excel'); 
        $this->load->library('email');
    }


    /**
     * Sends daily sms.
     *
     * @param      integer  $term_id    The term identifier
     * @param      <type>   $fake_send  The fake send
     *
     * @return     array    ( description_of_the_return_value )
     */
    public function send_daily_sms($term_id = 0,$fake_send = FALSE)
    {
        $term = $this->googleads_m->select('term_id, term_name')->set_term_type()->get($term_id);

        if(empty($term))
        {
            trigger_error("Hợp đồng #{$term_id} không tồn tại");
            return FALSE;
        }

        $mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
        if(empty($mcm_account_id)) 
        {
            trigger_error('Client-customer-id không tồn tại');
            return FALSE;
        }

        $curators = get_term_meta_value($term_id, 'contract_curators');
        $curators = @unserialize($curators);
        if(empty($curators))
        {
            trigger_error('Thông tin người nhận SMS không tồn tại');
            return FALSE;
        }

        $updated_on = (int) get_term_meta_value($term_id,'account_performance_report_download_next_time');
        if($updated_on < time())
        {
            trigger_error('download_performance_daily FAILURE');
            return FALSE;
        }

        $this->load->model('base_adwords_m');
        $this->base_adwords_m->set_mcm_account($mcm_account_id,FALSE);

        $this->load->model('adword_calculation_m');
        $this->adword_calculation_m
        ->set_base_adword_model($this->base_adwords_m)
        ->set_report_type('ACCOUNT_PERFORMANCE_REPORT');

        $yesterday      = $this->mdate->startOfDay(strtotime('yesterday'));
        $clicks         = $this->adword_calculation_m->calc_clicks($yesterday);
        $invalidClicks  = $this->adword_calculation_m->calc_invalidclicks($yesterday);

        if(empty($clicks))
        {
            trigger_error("Không có dữ liệu clicks cho #{$term_id} ngày ".my_date($yesterday));
            return FALSE;
        }

        /* adding meta for check term has clicks in current month */
        $activated_until_time   = get_term_meta_value($term_id,'activated_until_time');
        $start_of_month         = $this->mdate->startOfMonth();
        $end_of_month           = $this->mdate->endOfMonth();
        if(empty($activated_until_time) || ((int) $activated_until_time < $start_of_month))
        {
            update_term_meta($term_id,'activated_until_time',$end_of_month);
        }

        $sms_template   = 'KQ:{date} - {website}: click: {clicks}, click ko hop le: {invalidclicks}.{extra}';
        $smstpl         = get_term_meta_value($term_id,'sms_template');

        if(!empty($smstpl))
        {
            $smstpl         = html_entity_decode($smstpl);
            $smstpl         = strip_tags($smstpl);
            $sms_template   = $smstpl;
        }

        $extra_data = 'Thong tin chi tiet xin lh: 0909109394 (Mr.Tri)';

        $kpi        = $this->googleads_kpi_m->order_by('kpi_type')->where('term_id',$term_id)->get_by();

        if(!empty($kpi))
        {
            $kpi_info   = array();
            $admin      = $this->admin_m->set_get_admin()->limit(1)->get($kpi->user_id);

            if(!empty($admin))
            {
                $this->load->helper('text');
                if(!empty($admin->display_name))
                {
                    $display_name       = explode(' ', $admin->display_name);
                    $last_name          = ucfirst(convert_accented_characters(end($display_name)));

                    $gender     = get_user_meta_value($kpi->user_id, 'gender');
                    $gender_f   = !empty($gender) ? 'Mr.' : 'Ms.';

                    $kpi_info['name']   = "{$gender_f}{$last_name}";
                }

                if(!empty($admin->user_email))
                {
                    $kpi_info['mail'] = $admin->user_email;
                }

                if($tech_phone = $this->usermeta_m->get_meta_value($kpi->user_id, 'user_phone'))
                {
                    $kpi_info['phone'] = $tech_phone;
                }

                $kpi_info = implode(', ', $kpi_info);
                if(!empty($kpi_info))
                {
                    $extra_data = 'Thong tin vui long LH-Ky Thuat: '.$kpi_info;
                }
            }
        }

        $sms_log = array();

        $this->load->library('sms');
        
        /* Thay đổi từ khóa nhạy cảm - không cho phép hiển thị */
        if($blacklist_keywords = $this->option_m->get_value('blacklist_keywords', TRUE))
        {
            $replaces = array_map(function($k){ 
                return substr($k, 0, 2).str_repeat('*', strlen(substr($k, 2, strlen($k))));
            }, $blacklist_keywords);

            $term->term_name = str_replace($blacklist_keywords, $replaces, $term->term_name);
        }

        foreach ($curators as $curator)
        {
            $result = array();
            $phone = preg_replace('/\s+/', '', $curator['phone']);
            if(empty($phone)) 
                continue;

            $search_replace = array(
                '{clicks}' => $clicks,
                '{invalidclicks}' => $invalidClicks,
                '{website}' => $term->term_name,
                '{ten_khach_hang}' => $curator['name'],
                '{ten_nguoi_nhan_sms}' => $curator['name'],
                '{date}' => my_date($yesterday,'d/m'),
                '{extra}' => $extra_data
                );

            $message = str_replace(array_keys($search_replace), array_values($search_replace), $sms_template);

            /* Push số liệu lên Money24h App */
            try
            {
                (new \Money24h\Notifications())->send("Adsplus báo cáo số liệu quảng cáo hàng ngày", $message, $phone);
            }
            catch (Exception $e){}

            $result = false;
            if($fake_send)
            {
                $result = $this->sms->fake_send($phone,$message,0);
                trigger_error('Fake message has sent');
                continue;
            }

            $result = $this->sms->send($phone,$message);

            if(!empty($result))
            {
                $result = array_shift($result);
                unset($result['config']);
            }

            if(is_array($result))
            {
                $result['customer_name'] = $curator['name'];
                $result['day'] = my_date($yesterday,'d/m');
            }
            
            $sms_log[] = $result;
        }

        if(!empty($sms_log))
        {
            $content = '';
            $day = '';
            $recipients = array();
            foreach ($sms_log as $row) 
            {
                $content = $content ?: $row['message'];
                $day = $day ?: $row['day'];
                $recipients[] = $row['recipient'];
            }

            if(!empty($recipients))
            {
                $title = 'Thống kê lưu lượng click ngày '.$day;
                $data_message = array(    
                    'term_id' => $term_id,
                    'msg_title' => $title,
                    'msg_content' => $content,
                    'msg_type' => 'default',
                    'msg_status' => 'sent',
                    'metadata' => '',
                    'created_on' => time(),
                    'hed_type' => 'sms'
                    );

                $user_id = 0;
                $this->load->model('term_users_m');
                if($term_user = $this->term_users_m->select('term_id,user_id')->get_by(array('term_id'=>$term->term_id)))
                {
                    $user_id = $term_user->user_id;
                }
                $data_message['user_id'] = $user_id;

                $this->create_message($data_message,$recipients);
            }
        }

        return $sms_log;
    }


    /**
     * Sends an activation mail 2 customer.
     *
     * @param      integer  $term_id  The term identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function send_activation_mail2customer($term_id = 0)
    {
        $term = $this->googleads_m->get($term_id);
        if(empty($term)) return FALSE;

        $this->set_send_from('reporting');
        $this->set_mail_to($term_id, 'customer');

        $data = array();
        $data['term'] = $term;
        $data['title'] = 'Thông tin kích hoạt dịch vụ';
        $data['kpis'] = $this->googleads_kpi_m->order_by('kpi_type')->where('term_id',$term_id)->as_array()->get_many_by();

        $title = '[ADSPLUS.VN] Thông báo kích hoạt dịch vụ Adsplus của website '.$term->term_name;
        $data['email_source'] = $title;

        $data['isJoinedContract']   = false;
        $data['balanceBudgetReceived'] = (int) get_term_meta_value($term_id, 'balanceBudgetReceived');
        if( ! empty($data['balanceBudgetReceived']))
        {
            $data['isJoinedContract']       = true;
            $data['previousContractId']     = (int) get_term_meta_value($term_id, 'previousContractId');
            $data['previousContractCode']   = get_term_meta_value($data['previousContractId'], 'contract_code');
        }

        $content = $this->render_content($data,'googleads/report/activation_email');

        $this->email->subject($title);
        $this->email->message($content);

        $result = $this->email->send();
        if(!$result) return $result;

        $data_message = array(    
            'term_id' => $term_id,
            'msg_title' => $title,
            'msg_content' => $content,
            'msg_type' => 'default',
            'msg_status' => 'sent',
            'metadata' => '',
            'created_on' => time(),
            'hed_type' => 'email'
        );

        $mail_headers = $this->email->headers;

        $recipients = array();
        if(!empty($mail_headers['To']))
        {
            $_mails = array_map(function($m){ return trim($m);},explode(',', $mail_headers['To']));
            $recipients = array_merge($recipients,$_mails);
        }

        $user_id = 0;
        $this->load->model('term_users_m');
        if($term_user = $this->term_users_m->select('term_id,user_id')->get_by(array('term_id'=>$term->term_id)))
        {
            $user_id = $term_user->user_id;
        }
        $data_message['user_id'] = $user_id;
        
        $this->create_message($data_message,$recipients);
        $this->email->headers = array();

        return $result;
    }

    public function create_message($data = array(),$recipients = array())
    {
        if(empty($data)) return FALSE;

        $default = array(    
            'term_id' => 0,
            'msg_title' => '',
            'msg_content' => '',
            'msg_type' => 'default',
            'msg_status' => 'sent',
            'metadata' => '',
            'created_on' => time()
            );

        $this->load->model('message/message_m');
        $message = array_merge($default,$data);
        $headers = array();

        $term_id = $message['term_id'] ?? 0;

        $hed_type = 'email';
        if(isset($message['hed_type']))
        {
            $hed_type = $message['hed_type'];
            unset($message['hed_type']);
        }

        $user_id = 0;
        if(isset($message['user_id']))
        {
            $user_id = $message['user_id'];
            unset($message['user_id']);
        }

        foreach ($recipients as $recipient)
        {
            $headers[] = array(
                'hed_type' => $hed_type,
                'hed_subject' => $message['msg_title'] ?? '',
                'hed_status' => 0,
                'hed_to' => $recipient,
                'user_id' => $user_id,
                'term_id' => $term_id,
                'msg_id' => 0
            );
        }

        return $this->message_m->create($message,$headers);
    }

    public function send_statistical_cronmail2admin($start_time = false,$end_time = false)
    {
        $start_time = empty($start_time) ? time() : $start_time;
        $end_time = empty($end_time) ? $start_time : $end_time;
        $start_date = $this->mdate->startOfDay($start_time, FALSE);
        $end_date = $this->mdate->endOfDay($end_time, FALSE);

        $logs = $this->googleads_log_m
        ->like('log_type',$this->googleads_log_m->prefix_log_type,'left')
        ->where('log_time_create >=',$start_date)
        ->where('log_time_create <=',$end_date)
        ->get_many_by();
        if(empty($logs)) 
            return FALSE;

        $this->set_send_from('reporting');
        $this->set_mail_to(0,'mailreport');

        $data = array();
        foreach ($logs as $log) 
        {
            $k = str_replace($this->googleads_log_m->prefix_log_type, '', $log->log_type);
            if($k == 'fetch') 
                continue;
            $data[$k][] = $log;
        }

        if(empty($data)) 
            return FALSE;

        $content = '';
        $template = $this->config->item('mail_template');
        $template['table_open'] = '<table  border="0" cellpadding="4" cellspacing="0" style="font-size:12px; font-family:Arial" width="100%"><tr><th colspan="5" style="background: #138474 none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">';

        if(!empty($data['email']))
        {
            $this->table->set_caption('Thống kê gửi EMAIL tự động');
            $this->table->set_template($template);
            $this->table->set_heading('STT','Website','Người nhận','Tình trạng','File báo cáo');

            $i =0;
            foreach ($data['email'] as $k => $log) 
            {
                $term = $this->googleads_m->get($log->term_id);
                if(empty($term)) continue;

                $website = $term->term_name;
                $status = force_var($log->log_status,'Không gửi được','Gửi thành công');

                $log_content =  @unserialize($log->log_content);
                if(empty($log_content)) continue;

                $attachments = @unserialize($log_content['attachment']);
                if(empty($attachments)) continue;

                $attachment_links = '';
                foreach ($attachments as $key => $path)
                {
                    $attachment_links.= anchor(base_url($path),$key);
                }

                $mail_to = '';
                if(!empty($log_content['recipients']))
                {
                    $recipients = $log_content['recipients'];
                    $mail_to = is_array($recipients) ? implode(',', $recipients) : $recipients;
                }

                $anchor_link = base_url($log_content['attachment']);
                $file_download_link = anchor($anchor_link,'Tải xuống');
                $this->table->add_row(++$i,$website,$mail_to,$status,$attachment_links);
            }

            $content.= $this->table->generate();    
        }
        if(!empty($data['sms']))
        {
            $this->load->library('sms');
            $this->table->set_caption('Thống kê gửi SMS tự động');
            $template['table_open'] = '<table  border="0" cellpadding="4" cellspacing="0" style="font-size:12px; font-family:Arial" width="100%"><tr><th colspan="6" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">';
            $this->table->set_template($template);
            $this->table->set_heading('STT','Khách hàng','Người nhận','Tình trạng','Nội dung');

            $i = 0;
            foreach ($data['sms'] as $k => $log) 
            {
                $term = $this->googleads_m->get($log->term_id);
                if(empty($term)) 
                    continue;

                $result_rows = @unserialize($log->log_content);
                if(empty($result_rows) || !is_array($result_rows)) 
                    continue;

                foreach ($result_rows as $row) 
                {
                    $website = $term->term_name;
                    $status = $this->sms->get_status_label($row['response']->SendSMSResult);    
                    $message = $row['message'];
                    $this->table->add_row(++$i,$term->term_name,$row['customer_name'],$row['recipient'],$status,$row['message']);
                }
            }

            $content.= $this->table->generate();
        }

        $title = '[Adsplus.vn] thống kê báo cáo tự động ngày ' . my_date(time(), 'd-m-Y') .' [ID:'.time().']';

        $this->email->subject($title);
        $this->email->message($content);
        $result = $this->email->send();
        return $result;        
    }

    public function send_mail_alert_2admin($term_id = 0)
    {
        $term = $this->googleads_m->get($term_id);
        if(empty($term)) return FALSE;

        $this->set_send_from('warning');
        $this->set_mail_to($term_id,'admin');

        $data = array();
        $data['term'] = $term;
        $data['progress'] = $this->googleads_m->get_the_progress($term_id);
        $data['kpis'] = $this->googleads_kpi_m->order_by('kpi_type')->where('term_id',$term_id)->as_array()->get_many_by();

        $complete_progress = currency_numberformat($data['progress'],'%',2);
        $mail_id = 'ID:'.time();
        $data['title'] = "{$term->term_name} [{$complete_progress}] tiến độ cam kết {$mail_id}";

        $content = $this->render_content($data,'googleads/report/alert_process');

        $this->email->subject('[CẢNH BÁO] '.$data['title']);
        $this->email->message($content);

        $result = $this->email->send();
        return $result;
    }

    public function send_notify_finish_mail($term_id = 0, $type_to = 'admin')
    {
        $term = $this->googleads_m->get($term_id);
        if(empty($term)) return FALSE;

        $mail_data = array(
            'mail_cc' => array('ketoan@adsplus.vn'),
            'type_to' => $type_to
            );

        $this->set_send_from('warning');
        $this->set_mail_to($term_id,$mail_data);

        $data = array();
        $data['term'] = $term;
        $data['progress'] = $this->googleads_m->get_the_progress($term_id);
        $data['kpis'] = $this->googleads_kpi_m->order_by('kpi_type')->where('term_id',$term_id)->as_array()->get_many_by();

        $complete_progress = currency_numberformat($data['progress'],'%',2);
        $data['title'] = $term->term_name.' Đã đạt ['.$complete_progress.'] tiến độ cam kết';
        
        $data['email_source'] = $data['title'];

        $content = $this->render_content($data,'googleads/report/alert_process');

        $this->email->subject('[THÔNG BÁO ĐẠT ĐỦ CAM KẾT] '.$data['title']);
        $this->email->message($content);

        return $this->email->send();
    }

    public function send_noticefinish_mail($term_id = 0,$type_to = 'admin')
    {
        $term = $this->googleads_m->get($term_id);
        if(empty($term)) return FALSE;

        $this->set_send_from('warning');
        $this->set_mail_to($term_id, $type_to);

        $data = array();
        $data['term'] = $term;
        $data['title'] = 'Thông báo dự kiến kết thúc dịch vụ ADSPLUS';

        $data['kpis'] = $this->googleads_kpi_m
        ->order_by('kpi_type')
        ->where('term_id',$term_id)
        ->as_array()
        ->get_many_by();

        $data['expected_end_time'] = $this->googleads_m->calc_expected_end_time($term_id);

        $title = '[ADSPLUS.VN] Thông báo dự kiến kết thúc dịch vụ Adsplus của website '.$term->term_name;
        $data['email_source'] = $title;
        $this->email->subject($title);
        
        $content = $this->render_content($data,'googleads/report/notice_finish');
        $this->email->message($content);


        $is_send = $this->email->send(TRUE);

        if($is_send)
        {
            $data_message = array(    
                'term_id' => $term_id,
                'msg_title' => $title,
                'msg_content' => $content,
                'msg_type' => 'default',
                'msg_status' => 'sent',
                'metadata' => '',
                'created_on' => time(),
                'hed_type' => 'email'
                );

            $mail_headers = $this->email->headers;

            $recipients = array();
            if(!empty($mail_headers['To']))
            {
                $_mails = array_map(function($m){ return trim($m);},explode(',', $mail_headers['To']));
                $recipients = array_merge($recipients,$_mails);
            }

            // if(!empty($mail_headers['Cc']))
            // {
            //     $_mails = array_map(function($m){ return trim($m);},explode(',', $mail_headers['Cc']));
            //     $recipients = array_merge($recipients,$_mails);
            // }

            // if(!empty($mail_headers['Bcc']))
            // {
            //     $_mails = array_map(function($m){ return trim($m);},explode(',', $mail_headers['Bcc']));
            //     $recipients = array_merge($recipients,$_mails);
            // }

            $user_id = 0;
            $this->load->model('term_users_m');
            if($term_user = $this->term_users_m->select('term_id,user_id')->get_by(array('term_id'=>$term->term_id)))
            {
                $user_id = $term_user->user_id;
            }
            $data_message['user_id'] = $user_id;
            
            $this->create_message($data_message,$recipients);
            $this->email->headers = array();
        }

        return $is_send;
    }  

    public function send_finish_mail2admin($term_id = 0)
    {
        $term = $this->googleads_m->get($term_id);
        if(empty($term)) return FALSE;

        $this->set_send_from('reporting');
        $this->set_mail_to($term_id, 'admin');

        $contract_code = get_term_meta_value($term_id, 'contract_code');
        $service_type = force_var(get_term_meta_value($term->term_id,'service_type'));
        $is_cpc_type = ($service_type == 'cpc_type');
        $service_type_label = $is_cpc_type ? 'Quản lý theo clicks & CPC' : 'Quản lý theo tài khoản';

        $title = 'Hợp đồng '.$contract_code.' đã được kết thúc.';
        $content = 'Dear nhóm Adsplus <br>';

        $end_contract_time = get_term_meta_value($term_id,'end_contract_time');
        $content.= 'Hợp đồng Adsplus vừa được kết thúc vào lúc <b>'.my_date($end_contract_time,'d-m-Y H:i:s').'</b><br>';
        
        $content.= anchor(admin_url("googleads/setting/{$term->term_id}"),'Xin vui lòng vào link sau để cấu hình dịch vụ Adword , kết thúc dịch vụ và gửi mail thông báo đến khách hàng<br>', 'target="_blank"');

        $this->config->load('table_mail');
        $this->table->set_template($this->config->item('mail_template'));
        $this->table->set_caption('Thông tin hợp đồng');
        $this->table
        ->add_row('Mã hợp đồng:', $contract_code)
        ->add_row('Tên dịch vụ:', 'Dịch vụ adsplus')
        ->add_row('Loại hình:', $service_type_label);

        $vat = get_term_meta_value($term->term_id,'vat');
        $has_vat = ($vat > 0);
        $googleads_begin_time = get_term_meta_value($term_id, 'googleads-begin_time');

        $this->table
        ->add_row('Loại hình:', ($has_vat ? 'V' : 'NV'))
        ->add_row('Thời gian bắt đầu chạy adword:', my_date($googleads_begin_time,'d/m/Y'));

        $sale_id = get_term_meta_value($term->term_id,'staff_business');
        if(!empty($sale_id)){
            $display_name = $this->admin_m->get_field_by_id($sale_id,'display_name');
            $display_name = empty($display_name) ? $this->admin_m->get_field_by_id($sale_id,'display_name') : $display_name;
            $this->table->add_row('Kinh doanh phụ trách:', "#{$sale_id} - {$display_name}");
        }
        $content.= $this->table->generate();

        $this->table->set_caption('Thông tin khách hàng');

        $this->table
        ->add_row('Người đại diện', get_term_meta_value($term->term_id,'representative_name'))
        ->add_row('Địa chỉ', get_term_meta_value($term->term_id,'representative_address'))
        ->add_row('Website', $term->term_name)
        ->add_row('Mail liên hệ', mailto(get_term_meta_value($term->term_id,'representative_email')));

        $content.= $this->table->generate();
        
        $this->email->subject($title);
        $this->email->message($content);

        $result = $this->email->send();
        return $result;
    }    

    public function send_finished_service($term_id = 0,$type_to = 'admin')
    {
        $term = $this->googleads_m->get($term_id);
        if(empty($term))
            return FALSE;
        
        $this->set_send_from('reporting');
        $this->set_mail_to($term_id, $type_to);

        $data = array();
        $data['term'] = $term;
        $data['title'] = 'Thông báo kết thúc dịch vụ';
        $data['kpis'] = $this->googleads_kpi_m
        ->order_by('kpi_type')
        ->where('term_id',$term_id)
        ->as_array()
        ->get_many_by();
        
        $title = ($type_to != 'customer') ? '[XEM TRƯỚC]' : '';
        $title.= '[ADSPLUS.VN] Thông báo kết thúc dịch vụ Adsplus của website '.$term->term_name;
        $this->email->subject($title);

        $data['email_source'] = $title;

        $data['isJoinedContract']   = false;
        $data['balanceBudgetAddTo'] = (int) get_term_meta_value($term_id, 'balanceBudgetAddTo');
        if( ! empty($data['balanceBudgetAddTo']))
        {
            $data['isJoinedContract']   = true;
            $data['nextContractId']     = (int) get_term_meta_value($term_id, 'nextContractId');
            $data['nextContractCode']   = get_term_meta_value($data['nextContractId'], 'contract_code');
        }

        $start_time = get_term_meta_value($term_id, 'googleads-begin_time');
        $start_time OR $start_time = get_term_meta_value($term_id, 'advertise_start_time');

        $end_time = get_term_meta_value($term_id, 'googleads-end_time');
        $end_time OR $end_time = get_term_meta_value($term_id, 'advertise_start_time');
        $end_time OR $end_time = get_term_meta_value($term_id, 'end_service_time');
        $end_time OR $end_time = time();

        $xslFilePath = (new XlsWeeklyReport($term_id, $start_time, $end_time))->build();
        $this->email->attach($xslFilePath ,'attachment', "quan-ly-tai-khoan-bao-cao-tuan.xlsx");

        $content = $this->render_content($data, 'googleads/report/finish_email');
        $this->email->message($content);

        $result = $this->email->send();

        if($result && $type_to == 'customer')
        {
            $data_message = array(    
                'term_id' => $term->term_id,
                'msg_title' => $title,
                'msg_content' => $content,
                'msg_type' => 'default',
                'msg_status' => 'sent',
                'metadata' => '',
                'created_on' => time(),
                'hed_type' => 'email'
                );

            $mail_headers = $this->email->headers;

            $recipients = array();
            if(!empty($mail_headers['To']))
            {
                $_mails = array_map(function($m){ return trim($m);},explode(',', $mail_headers['To']));
                $recipients = array_merge($recipients,$_mails);
            }

            $user_id = 0;
            $this->load->model('term_users_m');
            if($term_user = $this->term_users_m->select('term_id,user_id')->get_by(array('term_id'=>$term->term_id)))
            {
                $user_id = $term_user->user_id;
            }
            $data_message['user_id'] = $user_id;
            
            $this->create_message($data_message,$recipients);
            $this->email->headers = array();
        }

        $this->email->clear(TRUE);

        return $result;
    }

    public function set_mail_to($term_id = 0,$type_to = 'admin')
    {
        $this->email->clear(TRUE);

        $recipients = array(
            'mail_to' => array(),
            'mail_cc' => array(),
            'mail_bcc' => array('thonh@webdoctor.vn')
        );

        if(is_array($type_to) && !empty($type_to)) 
        {
            $recipients = array_merge_recursive($type_to,$recipients);
            $type_to = $type_to['type_to'] ?? 'specified';
        }

        extract($recipients);

        $staff_mail = '';
        $tech_mail = array();

        if(!empty($term_id))
        {
            $this->load->model('staffs/admin_m');
            $staff_id = get_term_meta_value($term_id,'staff_business');
            $staff_mail = $this->admin_m->get_field_by_id($staff_id,'user_email');    

            $kpis = $this->googleads_kpi_m
            ->select('user_id')
            ->where('term_id',$term_id)
            ->group_by('user_id')
            ->get_many_by();
            if(!empty($kpis))
            {
                foreach ($kpis as $kpi)
                { 
                    $mail = $this->admin_m->get_field_by_id($kpi->user_id,'user_email');
                    if(empty($mail)) continue;
                    $tech_mail[] = $mail;
                }
            }
        }

        if($type_to == 'admin')
        {
            if(!empty($tech_mail))
            {
                foreach ($tech_mail as $mail)
                {
                    $mail_to[] = $mail;
                }
            }

            if(!empty($staff_mail)) 
            {
                $mail_cc[] = $staff_mail;
            }
            
            $mail_cc[] = 'trind@adsplus.vn';
        }
        else if($type_to == 'customer')
        {
            $curators = get_term_meta_value($term_id,'contract_curators');
            $curators = @unserialize($curators);
            if(!empty($curators))
            {
                foreach ($curators as $item)
                {
                    $email = trim($item['email']);
                    if(empty($email)) continue;
                    $mail_to[] = $email;
                }
            }

            $mail_cc[] = 'trind@adsplus.vn';
            if(!empty($staff_mail)) $mail_cc[] = $staff_mail;
            if(!empty($tech_mail))
                foreach ($tech_mail as $mail)
                    $mail_cc[] = $mail;
        }
        else if($type_to == 'mailreport')
        {
            $mail_to = array('thonh@webdoctor.vn');
        }

        $this->recipients = $mail_to;

        $this->email
        ->from('support@adsplus.vn', 'Adsplus.vn')
        ->to($mail_to)
        ->cc($mail_cc)
        ->bcc($mail_bcc);

        return $this;
    }

    public function set_send_from($group_name = FALSE)
    {
        if($group_name == FALSE) return FALSE;


        $this->config->load('googleads/email');
        $group_name     = strtolower(trim($group_name));
        $conf_emails    = $this->config->item($group_name,'email-groups');
        if(empty($conf_emails)) return FALSE;

        $rand_key = array_rand($conf_emails);
        $this->email->initialize($conf_emails[$rand_key]);

        return TRUE;
    }

    public function render_content($data = array(),$content_view,$layout='')
    {
        if(!is_array($data))
            $data = array($data);

        if(empty($content_view)) return FALSE;
        if(empty($layout)) $layout = 'email/yellow_tpl';

        $data['content'] = $this->load->view($content_view,$data,TRUE);
        $content = $this->load->view($layout,$data,TRUE);
        if(empty($content)) return FALSE;
        return $content;
    }
  
    public function send_mail_inefficient($refresh = FALSE)
    {
        $allowed_stats = array('publish','ending','liquidation');
        $user_terms = $this->googleads_m
        ->select('term.term_id,term_name,term_status,user_id,kpi_type')
        ->join('googleads_kpi','googleads_kpi.term_id = term.term_id','INNER')
        ->group_by('term.term_id')
        ->where_in('term.term_status',$allowed_stats)
        ->set_term_type()
        ->as_array()
        ->get_many_by();

        if(empty($user_terms)) return FALSE;

        $term_user = array();
        foreach ($user_terms as $key => $term)
        {
            $term_id = $term['term_id'];

            if($term['term_status'] == 'liquidation')
            {
                $end_service_time = get_term_meta_value('end_service_time');
                if($end_service_time < strtotime('yesterday'))
                    continue;
            }

            $mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
            if(empty($mcm_account_id)) continue;

            $begin_time = get_term_meta_value($term_id,'googleads-begin_time');
            if(empty($begin_time)) continue;

            $start_service_time = (int) get_term_meta_value($term_id,'start_service_time');
            if(empty($start_service_time)) continue;

            $start_service_time_valid = strtotime('+3 day',$start_service_time);
            $start_service_time_valid = $this->mdate->startOfDay($start_service_time_valid);
            if($start_service_time_valid > time()) continue;

            $end_time = get_term_meta_value($term_id,'googleads-end_time');
            $last_end_time = strtotime('+1 day',$end_time);
            if(empty($end_time) || $last_end_time > time())
                $term_user[] = $term;
        }

        if(empty($term_user)) return FALSE;

        $this->load->model('base_adwords_m');
        $report_type = 'KEYWORDS_PERFORMANCE_REPORT';

        if($refresh)
        {
            foreach ($term_user as $term) 
            {
                $mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
                $this->base_adwords_m->set_mcm_account($mcm_account_id);
                $result = $this->base_adwords_m->download($report_type,strtotime('yesterday'));
            }
        }

        $data = array();
        foreach ($term_user as $term) 
        {
            $term_id = $term['term_id'];

            $mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
            $this->base_adwords_m->set_mcm_account($mcm_account_id,FALSE);

            $keywords = $this->base_adwords_m->get_cache($report_type,strtotime('yesterday'));
            $keywords = array_filter($keywords);
            if(empty($keywords)) continue;

            $keywords = reset($keywords);
            $count_keywords = count($keywords);
            $ctr_invalid = array_filter($keywords,function($x){ return ((double)$x['ctr'] <= 6);});
            $position_invalid = array_filter($keywords,function($x){ return ((double)$x['avgPosition'] > 4);});

            $score_invalid = array_filter($keywords,function($x) {
                if(is_numeric($x['qualityScore']) || @$x['hasQualityScore'] == 'true')
                    return ((double)$x['qualityScore'] < 7);    
                return FALSE;
            });
            $count_score_invalid = count($score_invalid);
            $percent_score_invalid = div($count_score_invalid,$count_keywords)*100;

            $timeonsite_invalid = FALSE;
            $bounceRate_invalid = FALSE;

            $sum_pagesSession = array_sum(array_column($keywords, 'pagesSession'));
            $keywords_has_clicks = array_filter($keywords,function($x){ return (int) $x['clicks'] > 0;});
            if(!empty($sum_pagesSession) && !empty($keywords_has_clicks))
            {   
                $timeonsite_invalid = array_filter($keywords_has_clicks,function($x){return ((double)$x['avgSessionDurationSeconds'] < 30);});
                $bounceRate_invalid = array_filter($keywords_has_clicks,function($x){ return ((double)$x['bounceRate'] >= 90);});
            }

            $timeonsite_invalid_label = !$timeonsite_invalid ? '---' : count($timeonsite_invalid).'/'.count($keywords_has_clicks);
            $bounceRate_invalid_label = !$bounceRate_invalid ? '---' : count($bounceRate_invalid).'/'.count($keywords_has_clicks);

            $user_id = $term['user_id'];
            $tech_name = $this->admin_m->get_field_by_id($user_id,'display_name');
            $tech_name = $tech_name?:$this->admin_m->get_field_by_id($user_id,'user_email');

            $data[$term_id] = array(
                'term_id' => $term_id,
                'tech_name' => $tech_name,
                'term_name' => $term['term_name'],
                'keywords' => $count_keywords,
                'user_id' => $user_id,
                'ctr_invalid' => count($ctr_invalid),
                'position_invalid' => count($position_invalid),
                'score_invalid' => $count_score_invalid,
                'percent_score_invalid' => numberformat($percent_score_invalid, 2),
                'timeonsite_invalid' => $timeonsite_invalid_label,
                'bounceRate_invalid' => $bounceRate_invalid_label
                );

        }

        usort($data, function($a,$b){
            if($a['percent_score_invalid'] == $b['percent_score_invalid'])
                return ($a['keywords'] < $b['keywords'] ? 1 : -1); 
            return ($a['percent_score_invalid'] < $b['percent_score_invalid'] ? 1 : -1); 
        });

        $this->config->load('table_mail');
        $this->table->set_template($this->config->item('mail_template'));
        $headings = array('# Rank','Phụ trách','Website','Total Keyword','CTR(<=6%)','Avg. Pos(>4)','Qual. score(<7)','% Qual. score(<7)','T.o.s < 30s','Bounce >= 90%');
        $this->table->set_heading($headings);
        $this->table->set_caption('<h2>XẾP HẠNG HĐ KHÔNG HIỆU QUẢ NGÀY '.my_date(strtotime('yesterday')));

        foreach ($data as $key => $item) 
        {
            $dat = array(
                ($key+1),
                $item['tech_name'],
                $item['term_name'],
                $item['keywords'],
                $item['ctr_invalid'],
                $item['position_invalid'],
                $item['score_invalid'],
                $item['percent_score_invalid'].' %',
                $item['timeonsite_invalid'] == $item['keywords'] ? '-' : $item['timeonsite_invalid'],
                $item['bounceRate_invalid']?:'-'
            );

            $this->table->add_row($dat);
        }

        $term_detail_index = $this->table->generate();

        $group_data = array();
        foreach($data as $key => $item)
        {
            $group_data[$item['user_id']][$key] = $item;
        }

        $this->table->set_caption('<h2>THỐNG KÊ PHỤ TRÁCH HĐ NGÀY '.my_date(strtotime('yesterday')));
        $this->table->set_heading(array('#Rank','NV Phụ trách','Tổng tài khoản','SL không HQ<br/>(Qual.score > 30%)*','% không HQ'));

        $summary_data = array();
        $i = 0;
        foreach ($group_data as $user_id => $terms) 
        {
            if(empty($terms)) continue;

            $terms_count = count($terms);
            $ineffecient_count = count(array_filter($terms,function($term){ return $term['percent_score_invalid'] > 30;}));
            $ineffecient_percent = number_format(div($ineffecient_count,$terms_count) * 100, 2);

            $base_term = reset($terms);

            $display_name = $base_term['tech_name'].' - #'.$base_term['user_id'];

            $summary_data[] = array(
                'display_name' => $display_name,
                'terms_count' => $terms_count,
                'ineffecient_count' => $ineffecient_count,
                'ineffecient_percent' => $ineffecient_percent
                );
        }

        usort($summary_data, function($a,$b){
            return ($a['ineffecient_percent'] < $b['ineffecient_percent'] ? 1 : -1); 
        });

        foreach ($summary_data as $key => $row) 
        {
            array_unshift($row, $key+1);
            $row['ineffecient_percent'].= '%';
            $this->table->add_row($row);
        }

        $total = array_sum(array_column($summary_data,'terms_count'));
        $total_ineffecient = array_sum(array_column($summary_data,'ineffecient_count'));
        $total_percent_ineffecient = ($total > 0 ? number_format(div($total_ineffecient,$total) * 100, 2) : '0.00').'%';
        $this->table->add_row(array('colspan'=>2,'data'=>'<b>Tổng</b>'),$total,$total_ineffecient,$total_percent_ineffecient);

        $summary_index = $this->table->generate();
        $summary_index.= '<p>*SL không HQ (Qual.score > 30%) = tổng số lượng các tài khoản có % điểm chất lượng(Qual.score < 7)  chiếm hơn 30% tổng từ khóa của tài khoản ngày '.my_date(strtotime('yesterday'),'d/m/Y').'</p>';

        $content = $summary_index.$term_detail_index;

        $title = '[ADSPLUS.VN] Báo cáo thống kê HĐ không hiệu quả ngày '.my_date(strtotime('yesterday'));

        $mail_data = array(
            'mail_cc' => array( 'trind@adsplus.vn' ),
            'type_to' => 'mailreport'
            );    

        $this->set_send_from('reporting');
        $this->set_mail_to(0,$mail_data);

        $this->email->subject($title);
        $this->email->message($content);
        
        $result = $this->email->send();
        $this->email->clear(TRUE);

        return $result;
    }

    public function send_mail_quality_score($user_id = 0,$start_time = FALSE,$end_time = FALSE)
    { 
        if($end_time && my_date($end_time) == my_date())
        {
            $end_time = strtotime('-1 day');
        }

        $terms = array();
        $adword_client_ids = array();

        $term_map_users = $this->term_m
        ->join('googleads_kpi','googleads_kpi.term_id = term.term_id','LEFT OUTER')
        ->group_by('term.term_id')
        ->where_in('term_status',array('publish','pending','ending','liquidation'))
        ->where('term_type','google-ads')
        ->as_array()
        ->get_many_by();

        if(!$term_map_users) return FALSE;
        foreach ($term_map_users as $key => $tmu) 
        {
            if(empty($tmu['user_id'])) continue;

            $user = $this->admin_m->get($tmu['user_id']);
            if($user->user_status != 1)
                continue;

            $term_id = $tmu['term_id'];

            $mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
            if(!$mcm_account_id)
                continue;

            $mcm_account = $this->mcm_account->get($mcm_client_id);
            if(empty($mcm_account))
                continue;

            $adword_client_id = $mcm_account->term_name;

            $start_service_time = get_term_meta_value($term_id,'start_service_time');
            if(!$start_service_time)
                continue;
            if($start_service_time > $end_time)
                continue;
            if(strtotime('+3 day',$start_service_time) > time())
                continue;

            $end_service_time = get_term_meta_value($term_id,'end_service_time');
            if($end_service_time && $end_service_time < $start_time) 
                continue;

            $tmu['adword_client_id'] = preg_replace('/\s+/', '', $adword_client_id);
            $tmu['start_service_time'] = $start_service_time;
            $tmu['end_service_time'] = $end_service_time;
            $terms[$tmu['user_id']][] = $tmu;
        }

        if(empty($terms)) return FALSE;

        $this->load->model('base_adwords_m');
        $report_type = 'KEYWORDS_PERFORMANCE_REPORT';

        $start_date = my_date($start_time,'Y-m-d');
        $end_date = my_date($end_time,'Y-m-d');

        foreach ($terms as $user_id => $value) 
        {
            $statistic_adwords = array();
            $adword_client_ids = array_unique(array_column($value, 'adword_client_id'));
            if(empty($adword_client_ids))
                continue;

            foreach ($adword_client_ids as $adword_client_id) 
            {
                $adword_client_ids[$adword_client_id] = array();

                $cache_key = "google-adword/{$adword_client_id}/keywords_quality_cache_{$start_date}_{$end_date}";
                $keyword_quality_cache = $this->scache->get($cache_key);
                if(empty($keyword_quality_cache))
                {
                    $mcm_account = $this->mcm_account_m->get_by(['term_name'=>$adword_client_id]);
                    if(empty($mcm_account)) continue;

                    $this->base_adwords_m->set_mcm_account($mcm_account->term_id,FALSE);

                    $keywords_report = $this->base_adwords_m->set_user_adword($adword_client_id)->get_cache($report_type,$start_time,$end_time);
                    $keywords_report = array_filter($keywords_report);

                    if(empty($keywords_report)) 
                    {
                        $this->scache->write(NULL,$cache_key);
                        continue;
                    }

                    # filter content group by [day] => group by [keyword]
                    $keywords = array();
                    foreach ($keywords_report as $timestamp => $data) 
                    {
                        if(empty($data)) continue;

                        foreach ($data as $row) 
                        {
                            $keywordID = $row['keywordID'];

                            if(!isset($keywords[$keywordID]))
                            {
                                $keywords[$keywordID] = array();
                            }

                            $keywords[$keywordID][] = $row;
                        }
                    }

                    foreach ($keywords as $keywordID => $data) 
                    {
                        $campaign = @$data[0]['campaign'];
                        $adGroup = @$data[0]['adGroup'];
                        $keyword = @$data[0]['keyword'];
                        
                        $clicks = array_sum(array_column($data,'clicks'));
                        $impressions = array_sum(array_column($data,'impressions'));
                        $ctr = $impressions ? number_format(div($clicks,$impressions) * 100, 2) : 0;

                        $avg_pos_index = array_sum(array_map(function($x){return $x['impressions']*$x['avgPosition'];}, $data));
                        $avgPosition = $impressions ? number_format(div($avg_pos_index,$impressions),1) : 0;

                        $cost = array_sum(array_column($data,'cost'));
                        $avgCPC = $clicks ? div($cost,$clicks) : 0;

                        $last_keyword = end($data);

                        $keywords[$keywordID] = array(
                            'campaign' => $campaign,
                            'adGroup' => $adGroup,
                            'keyword' => $keyword,
                            'impressions' => $impressions,
                            'clicks' => $clicks,
                            'ctr' => $ctr,
                            'avgPosition' => $avgPosition,
                            'avgCPC' => $avgCPC,
                            'cost' => $cost,
                            'qualityScore' => $last_keyword['qualityScore']
                            );
                    }

                    $count_keywords = count($keywords);

                    $ctr_invalid = array_filter($keywords,function($x){ return ((double)$x['ctr'] <= 6);});
                    $position_invalid = array_filter($keywords,function($x){ return ((double)$x['avgPosition'] > 4);});

                    $score_invalid = array_filter($keywords,function($x){ 

                        if(is_numeric($x['qualityScore']))
                        {
                            return ((double)$x['qualityScore'] < 7);
                        }

                        return FALSE;
                    });
                    $percent_score_invalid = (count($score_invalid)/$count_keywords)* 100;
                    $count_score_invalid = count($score_invalid);
                    if(empty($count_score_invalid)) 
                    {
                        $this->scache->write(NULL,$cache_key);
                        continue;
                    }
                    
                    $timeonsite_invalid = FALSE;
                    $bounceRate_invalid = FALSE;
                    $sum_pagesSession = array_sum(array_column($keywords, 'pagesSession'));
                    $keywords_has_clicks = array_filter($keywords,function($x){ return (int) $x['clicks'] > 0;});
                    if(!empty($sum_pagesSession) && !empty($keywords_has_clicks))
                    {   
                        $timeonsite_invalid = array_filter($keywords_has_clicks,function($x){return ((double)$x['avgSessionDurationSeconds'] < 30);});
                        $bounceRate_invalid = array_filter($keywords_has_clicks,function($x){ return ((double)$x['bounceRate'] >= 90);});
                    }

                    $timeonsite_invalid_label = !$timeonsite_invalid ? '---' : count($timeonsite_invalid).'/'.count($keywords_has_clicks);
                    $bounceRate_invalid_label = !$bounceRate_invalid ? '---' : count($bounceRate_invalid).'/'.count($keywords_has_clicks);

                    $keyword_quality_cache = array(
                        'adword_client_id' => $adword_client_id,
                        'keywords' => $count_keywords,
                        'ctr_invalid' => count($ctr_invalid),
                        'position_invalid' => count($position_invalid),
                        'score_invalid' => $count_score_invalid,
                        'percent_score_invalid' => numberformat($percent_score_invalid, 2),
                        'timeonsite_invalid' => $timeonsite_invalid_label,
                        'bounceRate_invalid' => $bounceRate_invalid_label
                        );

                    $this->scache->write($keyword_quality_cache,$cache_key);
                }

                $statistic_adwords[$adword_client_id] = $keyword_quality_cache;
            }

            $terms[$user_id] = $statistic_adwords;
        }

        $this->load->config('table');
        $this->table->set_caption('Thống kê KPI hiệu quả quảng cáo');

        $data_table = array();

        $dtable_overview = array();

        foreach ($terms as $user_id => $quality_indexes) 
        {
            $accounts_count = count($quality_indexes);
            if(!$accounts_count) continue;
            
            $display_name = $this->admin_m->get_field_by_id($user_id,'display_name')?:$this->admin_m->get_field_by_id($user_id,'user_email');
            $kpi_max = 0.4;
            $account_score_invalid = array_filter($quality_indexes,function($x){ return ((double)$x['percent_score_invalid'] > 40);});
            $account_score_invalid_count = count($account_score_invalid);
            $account_score_invalid_percent = div($account_score_invalid_count,$accounts_count);


            $dtable_overview[] = array(
                $display_name,
                $accounts_count,
                $account_score_invalid_count,
                $account_score_invalid_percent
                );

            $rowspan = ($accounts_count + 1);

            $headings = array(
                );

            $this->table->add_row(
                array('rowspan'=>$rowspan,'data'=>$display_name),
                'ADWORD ID',
                'Total Keyword',
                'CTR(<=6%)',
                'Avg. Pos(>4)',
                'Qual. score(<7)',
                '% Qual. score(<7)'
                );

            usort($quality_indexes, function($a,$b){
                return ($a['percent_score_invalid'] < $b['percent_score_invalid'] ? 1 : -1); 
            });

            foreach ($quality_indexes as $adword_client_id => $item) 
            {
                $this->table->add_row(
                    $item['adword_client_id'],
                    $item['keywords'],
                    $item['ctr_invalid'],
                    $item['position_invalid'],
                    $item['score_invalid'],
                    $item['percent_score_invalid']
                    );
            }

            switch (TRUE) 
            {
                case ($account_score_invalid_percent >= $kpi_max):
                    $this->table->add_row('Không đạt KPI',NULL,NULL,NULL,NULL,NULL);
                    break;
                
                default:
                    $this->table->add_row('Đạt KPI',NULL,NULL,NULL,NULL,NULL,NULL);
                    break;
            }
        }

        $detail_content = $this->table->generate();

        usort($dtable_overview, function($a,$b){
            return ($a[3] < $b[3] ? 1 : -1); 
        });

        $this->table->set_caption('Thống kê theo dõi TK không hiệu quả '."($start_date - $end_date)");
        $this->table->set_heading(array('#Rank','NV Phụ trách','Tổng tài khoản','SL không HQ','% không HQ'));
        foreach ($dtable_overview as $key => $row) 
        {
            $row[3] = number_format($row[3]*100, 2, '.', '').' %';
            array_unshift($row,$key+1);
            $this->table->add_row($row);
        }

        $overview_content = $this->table->generate();

        $title = '[ADSPLUS.VN] Thống kê theo dõi hiệu quả tài khoản';
        $content = $detail_content;
        $this->email->subject($title);
        $this->email->from('support@adsplus.vn', 'Adsplus.vn');
        $this->email->to('thonh@webdoctor.vn');
        $this->email->cc('');
        $this->email->bcc('');
        $this->email->message($content);

        $result = $this->email->send();
        return $result;
    }

    /**
     * Gửi email thống kê hợp đồng hiệu quả trong tháng
     *
     * @param      string  $time   The time
     */
    public function send_mail_inefficient_by($time = '')
    {
        $time = $time ?: time();
        $start_time = $this->mdate->startOfMonth($time);
        $end_time = $this->mdate->endOfMonth($time);

        # Lấy tất cả term có type:googleads va status[pending|publish|liquidation]
        $terms = $this->term_m
        ->where('term_type','google-ads')
        ->where_in('term_status',array('publish','pending','liquidation'))
        ->as_array()
        ->get_many_by();

        define("STARTUP_DAYS",3);

        foreach ($terms as $key => $term) 
        {
            $mcm_account_id = get_term_meta_value($term['term_id'],'mcm_client_id');
            if(empty($mcm_account_id)) 
            {
                unset($terms[$key]);
                continue;
            }

            $mcm_account = $this->mcm_account_m->get_by(['term_type'=>$this->mcm_account_m->term_type,'term_id'=>$mcm_account_id]);
            if(empty($mcm_account))
            {
                unset($terms[$key]);
                continue;
            }

            $start_service_time = get_term_meta_value($term['term_id'],'start_service_time');
            if(empty($start_service_time))
            {
                unset($terms[$key]);
                continue;
            }

            # case liquidation : check end service time
            if($term['term_status'] == 'liquidation')
            {   
                $end_service_time = get_term_meta_value($term['term_id'],'end_service_time');
                if($end_service_time < $start_time)
                {
                    unset($terms[$key]);
                    continue;
                }
            }

            $start_service_time_valid = strtotime('+'.STARTUP_DAYS.' day',$start_service_time);
            if($start_service_time_valid < $start_time && $start_service_time_valid > $end_time)
            {
                unset($terms[$key]);
                continue;
            }

            $terms[$key]['client_customer_id'] = $mcm_account->term_name;
        }

        if(empty($terms)) prd('Không tồn tại HĐ trong khoảng thời gian đã định');

        $start_time = $this->mdate->startOfDay($start_time);
        $end_time = $this->mdate->startOfDay($end_time);

        $report_type = 'KEYWORDS_PERFORMANCE_REPORT';
        $this->load->model('base_adwords_m');
        foreach ($terms as $key => $term) 
        {
            $mcm_account_id = get_term_meta_value($term['term_id'],'mcm_client_id');
            $client_customer_id = $term['client_customer_id'];
            $cache_name = "google-adword/{$client_customer_id}/date_range/{$start_time}_{$end_time}/{$report_type}";
            $data = $this->scache->get($cache_name);
            if(empty($data))
            {
                $baseAdwordsModel = new Base_adwords_m();
                $baseAdwordsModel->set_mcm_account($mcm_account_id);

                $keyword_performance_fields = array(
                    'AccountCurrencyCode','ExternalCustomerId','AccountDescriptiveName','AdGroupId','AdGroupName','AdGroupStatus','AdNetworkType1','ApprovalStatus','AverageCost','AverageCpc','AveragePosition','AverageTimeOnSite','BounceRate','CampaignId','CampaignName','CampaignStatus','Clicks','Cost','Criteria','Ctr','HasQualityScore','Id','Impressions','IsNegative','KeywordMatchType','QualityScore','Status','FirstPageCpc','TopOfPageCpc','Conversions'
                    );

                $baseAdwordsModel->set_selector_fields($keyword_performance_fields);
                $baseAdwordsModel->download($report_type,$start_time,$end_time);
            }
        }

        $adword_client_id = array_column($terms,NULL, 'client_customer_id');
        $termIds = array_column($terms, 'term_id');
        $kpis = $this->googleads_kpi_m->where_in('term_id',$termIds)->as_array()->get_many_by();
        $term_kpis = array_group_by($kpis,'term_id');

        define("MIN_CTR", 6);
        define("MIN_AVGPOSITION", 4);
        define("MIN_QUALITYSCORE", 7);

        $data = array();
        $clientCustomerIds = array_group_by($terms,'client_customer_id');
        foreach ($clientCustomerIds as $clientCustomerId => $termChilds) 
        {
            $keywords = $this->scache->get("google-adword/{$clientCustomerId}/date_range/{$start_time}_{$end_time}/{$report_type}");
            if(empty($keywords)) continue;

            $count_keywords = count($keywords);
            $ctr_invalid = array_filter($keywords,function($x){ return ((double)$x['ctr'] <= MIN_CTR);});
            $position_invalid = array_filter($keywords,function($x){ return ((double)$x['avgPosition'] > MIN_AVGPOSITION);});

            $score_invalid = array_filter($keywords,function($x) use ($adword_client_id) {
                if(is_numeric($x['qualityScore']) || @$x['hasQualityScore'] == 'true')
                    return ((double)$x['qualityScore'] < MIN_QUALITYSCORE);
                return FALSE;
            });
            $count_score_invalid = count($score_invalid);
            $percent_score_invalid = div($count_score_invalid,$count_keywords)*100;

            $timeonsite_invalid = FALSE;
            $bounceRate_invalid = FALSE;

            $keywords_has_clicks = array_filter($keywords,function($x){ return (int) $x['clicks'] > 0;});
            if(!empty($keywords_has_clicks))
            {   
                $timeonsite_invalid = array_filter($keywords_has_clicks,function($x){return ((double)$x['avgSessionDurationSeconds'] < 30);});
                $bounceRate_invalid = array_filter($keywords_has_clicks,function($x){ return ((double)$x['bounceRate'] >= 90);});
            }

            $timeonsite_invalid_label = !$timeonsite_invalid ? '---' : count($timeonsite_invalid).'/'.count($keywords_has_clicks);
            $bounceRate_invalid_label = !$bounceRate_invalid ? '---' : count($bounceRate_invalid).'/'.count($keywords_has_clicks);

            $clientCustomerKpis = array();
            foreach ($termChilds as $termChild) 
            {
                if(empty($term_kpis[$termChild['term_id']])) continue;

                $termChildKpis = $term_kpis[$termChild['term_id']];
                foreach ($termChildKpis as $termChildKpi) 
                {
                    if($termChildKpi['user_id'] == 1) continue;
                    $clientCustomerKpis[] = $termChildKpi['user_id'];
                }
            }

            $clientCustomerKpis = array_unique($clientCustomerKpis);
            if(empty($clientCustomerKpis)) continue;
            
            foreach ($clientCustomerKpis as $clientCustomerKpi) 
            {
                $tech_name = $this->admin_m->get_field_by_id($clientCustomerKpi,'display_name');
                $tech_name = $tech_name?:$this->admin_m->get_field_by_id($clientCustomerKpi,'user_email');
                $tech_name = '#'.$clientCustomerKpi.' '.$tech_name;

                $data[] = array(
                    'clientCustomerId' => $clientCustomerId,
                    'Account' => $keywords[0]['account'],
                    'tech_name' => $tech_name,
                    'keywords' => $count_keywords,
                    'user_id' => $clientCustomerKpi,
                    'ctr_invalid' => count($ctr_invalid),
                    'position_invalid' => count($position_invalid),
                    'score_invalid' => $count_score_invalid,
                    'percent_score_invalid' => numberformat($percent_score_invalid, 2),
                    'timeonsite_invalid' => $timeonsite_invalid_label,
                    'bounceRate_invalid' => $bounceRate_invalid_label
                    );
            }
        }

        usort($data, function($a,$b){
            if($a['percent_score_invalid'] == $b['percent_score_invalid'])
                return ($a['keywords'] < $b['keywords'] ? 1 : -1); 
            return ($a['percent_score_invalid'] < $b['percent_score_invalid'] ? 1 : -1); 
        });

        $this->config->load('table_mail');
        $this->table->set_template($this->config->item('mail_template'));
        
        $headings = array(
            '# Rank',
            'Phụ trách',
            'Account',
            'AdwordID',
            'Total Keyword',
            'CTR(<=6%)',
            'Avg. Pos(>4)',
            'Qual. score(<7)',
            '% Qual. score(<7)',
            'T.o.s < 30s',
            'Bounce >= 90%');
        $this->table->set_heading($headings);
        $this->table->set_caption('<h2>XẾP HẠNG HĐ KHÔNG HIỆU QUẢ THÁNG '.my_date($start_time,'m'));

        foreach ($data as $key => $item) 
        {
            $dat = array(
                ($key+1),
                $item['tech_name'],
                $item['Account'],
                $item['clientCustomerId'],
                $item['keywords'],
                $item['ctr_invalid'],
                $item['position_invalid'],
                $item['score_invalid'],
                $item['percent_score_invalid'].' %',
                $item['timeonsite_invalid'] == $item['keywords'] ? '-' : $item['timeonsite_invalid'],
                $item['bounceRate_invalid']?:'-'
            );

            $this->table->add_row($dat);
        }

        $term_detail_index = $this->table->generate();
        
        $this->table->set_caption('<h2>THỐNG KÊ PHỤ TRÁCH HĐ THÁNG '.my_date($start_time,'m'));
        $this->table->set_heading(array('#Rank','NV Phụ trách','Tổng tài khoản','SL không HQ<br/>(Qual.score > 30%)*','% không HQ'));
        $user_clientCustomerIds = array_group_by($data,'user_id');
        $summaryData = array();
        $i = 0;

        foreach ($user_clientCustomerIds as $user_id => $clientCustomerIds) 
        {
            if(empty($clientCustomerIds)) continue;

            $clientCustomerIdCount = count($clientCustomerIds);
            $ineffecient_count = count(array_filter($clientCustomerIds,function($x){ return $x['percent_score_invalid'] > 30;}));
            $ineffecient_percent = number_format(div($ineffecient_count,$clientCustomerIdCount) * 100, 2);

            $baseClientCustomer = reset($clientCustomerIds);

            $display_name = $baseClientCustomer['tech_name'].' - #'.$baseClientCustomer['user_id'];

            $sumaryData[] = array(
                'display_name' => $display_name,
                'terms_count' => $clientCustomerIdCount,
                'ineffecient_count' => $ineffecient_count,
                'ineffecient_percent' => $ineffecient_percent.'%'
                );
        }

        usort($sumaryData, function($a,$b){
            return ($a['ineffecient_percent'] < $b['ineffecient_percent'] ? 1 : -1); 
        });

        foreach ($sumaryData as $key => $row) 
        {
            array_unshift($row, $key+1);
            $row['ineffecient_percent'].= '%';
            $this->table->add_row($row);
        }

        $total = array_sum(array_column($sumaryData,'terms_count'));
        $total_ineffecient = array_sum(array_column($sumaryData,'ineffecient_count'));
        $total_percent_ineffecient = ($total > 0 ? number_format(div($total_ineffecient,$total) * 100, 2) : '0.00').'%';
        $this->table->add_row(array('colspan'=>2,'data'=>'<b>Tổng</b>'),$total,$total_ineffecient,$total_percent_ineffecient);

        $summary_index = $this->table->generate();
        $summary_index.= '<p>*SL không HQ (Qual.score > 30%) = tổng số lượng các tài khoản có % điểm chất lượng(Qual.score < 7)  chiếm hơn 30% tổng từ khóa của tài khoản ngày '.my_date($start_time,'d/m/Y').' đến ngày '.my_date($end_time,'d/m/Y').'</p>';

        $content = $summary_index.$term_detail_index;

        $title = '[ADSPLUS.VN] Thống kê hiệu quả HĐ tháng '.my_date($start_time,'m');

        $mail_data = array(
            'mail_cc' => array(
                'trind@adsplus.vn',
                'hopdong@adsplus.vn'
            ),
            'type_to' => 'mailreport'
        );    

        $this->set_send_from('reporting');
        $this->set_mail_to(0,$mail_data);

        $this->email->subject($title);
        $this->email->message($content);
        
        $result = $this->email->send();
        $this->email->clear(TRUE);
    }
}
/* End of file Googleads_report_m.php */
/* Location: ./application/modules/googleads/models/Googleads_report_m.php */