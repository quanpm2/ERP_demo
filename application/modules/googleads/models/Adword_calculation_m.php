<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adword_calculation_m extends Base_model 
{
    public $client_customer_id = NULL;
    protected $base_adword = NULL;

    function __construct($base_adword = NULL)
    {
        parent::__construct();

        if(empty($base_adword))
        {
            $this->load->model('base_adwords_m');
            $this->base_adword = $this->base_adwords_m;
        }
    }

    public function set_base_adword_model($base_adwords_m = NULL)
    {
        $this->base_adword = $base_adwords_m;
        return $this;
    }

    function set_report_type($report_type)
    {
        $this->report_type = $report_type;
        return $this;
    }

    function set_client_customer_id($client_customer_id)
    {
        $this->base_adword->set_client_customer_id($client_customer_id);
        return $this;
    }

    function calc_clicks($start_time,$end_time = FALSE)
    {
        return array_sum($this->get_column('clicks',$start_time,$end_time));
    }

    function calc_invalidclicks($start_time,$end_time = FALSE)
    {
        return array_sum($this->get_column('invalidClicks',$start_time,$end_time));
    }

    function calc_impressions($start_time,$end_time = FALSE)
    {
        return array_sum($this->get_column('impressions',$start_time,$end_time));
    }

    function calc_ctr($start_time,$end_time)
    {
        $clicks = array_sum($this->get_column('clicks',$start_time,$end_time));
        $impressions = array_sum($this->get_column('impressions',$start_time,$end_time));
        $result = 0;
        if($impressions) 
        {
            $result = round(div($clicks,$impressions)*100,2);
        }
        return $result;
    }

    function calc_cost($start_time,$end_time)
    {
        $cost = array_sum($this->get_column('cost',$start_time,$end_time));
        return $cost/1000000;
    }

    function count($field = '',$start_time,$end_time = FALSE)
    {
        $data = $this->get_column($field,$start_time,$end_time);
        $data = array_unique($data);
        return count($data);
    }

    function get_column($field = '',$start_time,$end_time = FALSE)
    {
        $data = $this->base_adword->get_cache($this->report_type,$start_time,$end_time);

        if(empty($data)) return [];

        $result = array();
        foreach ($data as $day => $dat) 
        {
            if(empty($dat)) continue;
            $result = array_merge($result,array_column($dat,$field));
        }

        return $result;
    }
}
/* End of file Adword_calculation.php */
/* Location: ./application/modules/googleads/models/Adword_calculation.php */