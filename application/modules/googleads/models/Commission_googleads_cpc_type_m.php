<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'modules/googleads/models/Commission_googleads_m.php');

class Commission_googleads_cpc_type_m extends Commission_m {

    protected $commission_package = 'cpc_type';

	function __construct()
	{
		parent::__construct();
		$this->config->load('googleads/contract');
	}

    /**
     * Gets the actually collected.
     *
     * @param      integer  $term_id     The term identifier
     * @param      integer  $user_id     The user identifier
     * @param      string   $start_time  The start time
     * @param      string   $end_time    The end time
     *
     * @return     integer  The actually collected.
     */
    public function get_actually_collected($term_id = 0, $user_id = 0, $start_time = '', $end_time = '')
    {
        $amount_not_vat = parent::get_amount_not_vat($term_id, $user_id, $start_time, $end_time);
        if(empty($amount_not_vat)) return 0;

        $vat = (int) get_term_meta_value($term_id, 'vat');
        if(empty($vat)) return $amount_not_vat;

        return div($amount_not_vat, 1 + FOREIGN_CONTRACTOR_TAX);
    }
}
/* End of file Commission_googleads_cpc_type_m.php */
/* Location: ./application/modules/googleads/models/Commission_googleads_cpc_type_m.php */