<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'modules/googleads/models/Commission_googleads_m.php');

class Commission_googleads_account_type_m extends Commission_m {

    protected $commission_package = 'account_type';

	function __construct()
	{
		parent::__construct();
		$this->load->model('googleads/googleads_m');
		$this->config->load('googleads/contract');
	}

    /**
     * Gets the actually collected.
     *
     * @param      integer         $term_id     The term identifier
     * @param      integer         $user_id     The user identifier
     * @param      integer|string  $start_time  The start time
     * @param      string          $end_time    The end time
     *
     * @return     integer         The actually collected.
     */
    public function get_actually_collected($term_id = 0, $user_id = 0, $start_time = '', $end_time = '')
    {
        $amount_not_vat = parent::get_amount_not_vat($term_id, $user_id, $start_time, $end_time);
        if(empty($amount_not_vat)) return 0;

        $vat = (int) get_term_meta_value($term_id, 'vat');

        $service_fee_payment_type = get_term_meta_value($term_id, 'service_fee_payment_type');

        if($service_fee_payment_type == 'devide')
        {
            $rate = $this->get_rate($amount_not_vat);

            $service_fee_rule = $this->config->item('service_fee_rule');
            $rate = 0;
            foreach ($service_fee_rule as $level => $rule)
            {
                if($amount_not_vat < $rule['min'] || $amount_not_vat > $rule['max']) continue;

                if($rule['amount'] !== FALSE)
                {
                    $rate = $rule['amount'];
                    break;
                }

                if($rule['rate'] !== FALSE) 
                {
                    $rate = $rule['rate'];
                    break;
                }
            }

            $amount_foreign_contractor_tax = 0;

            $actually_collected = 0;

            if(is_int($rate)) $actually_collected = $rate;
            else if(is_float($rate)) $actually_collected = $amount_not_vat * $rate;

            // Nếu hợp đồng có VAT thì phải trừ đi phí nhà thầu
            if($vat > 0) 
            {
                $amount_foreign_contractor_tax = $amount_not_vat * FOREIGN_CONTRACTOR_TAX;
            }

            $actually_collected -= $amount_foreign_contractor_tax;

            return $actually_collected;
        }

        // if($service_fee_payment_type == 'full')
        $contract_begin = get_term_meta_value($term_id, 'contract_begin');
        $contract_time  = $this->mdate->startOfDay($contract_begin);
        $tartget_time   = $this->mdate->endOfDay(($start_time - 1));

        $past_amount_paid = 0;

        $past_receipts = parent::get_receipts_paid($term_id, $user_id, $contract_time, $tartget_time);
        if( ! empty($past_receipts))
        {
            $past_amount_paid = array_sum(array_column($past_receipts, 'amount_not_vat'));
        }

        $service_fee        = get_term_meta_value($term_id, 'service_fee');

        $service_fee_remain = $service_fee - $past_amount_paid;
        if($service_fee_remain < 0) return 0;

        $service_fee_remain = abs($service_fee_remain);

        $actually_collected = ($service_fee_remain >= $amount_not_vat) ? $amount_not_vat : $service_fee_remain;
        
        // Nếu hợp đồng có VAT thì phải trừ đi phí nhà thầu
        if($vat > 0) 
        {
            $amount_foreign_contractor_tax = 0;
            $contract_budget = get_term_meta_value($term_id, 'contract_budget');
            $amount_foreign_contractor_tax = $contract_budget * FOREIGN_CONTRACTOR_TAX;

            $actually_collected-= $amount_foreign_contractor_tax;
        }

        if($actually_collected <= 0) return 0;
        
        return $actually_collected;
    }
}
/* End of file Commisision_googleads_account_type_m.php */
/* Location: ./application/modules/googleads/models/Commisision_googleads_account_type_m.php */