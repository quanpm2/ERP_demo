<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Google\AdsApi\AdWords\v201809\cm\Campaign;
use Google\AdsApi\AdWords\v201809\cm\CampaignOperation;
use Google\AdsApi\AdWords\v201809\cm\CampaignCriterionService;

use Google\AdsApi\AdWords\v201809\cm\IpBlock;
use Google\AdsApi\AdWords\v201809\cm\NegativeCampaignCriterion;
use Google\AdsApi\AdWords\v201809\cm\CampaignCriterionOperation;

use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\Common\OAuth2TokenBuilder;

class Adcampaign_m extends Post_m {

    public $post_type = 'adcampaign';
	const PAGE_LIMIT = 500;

	function __construct()
	{
		parent::__construct();
	}


	/**
	 * Sets the post type.
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function set_post_type()
	{
		return $this->where('posts.post_type',$this->post_type);
	}

	public function mutateTrackingUrlTemplate($mcm_account_id = 0, $adCampaignId = 0,$trackingUrlTemplate = '')
	{
		$mcmAccount = $this->mcm_account_m->set_term_type()->get($mcm_account_id); // Get mcm_account in DB
        if(empty($mcmAccount)) return FALSE;
        
        $mcmClientId	= 1402; // systemapi@adsplus.vn
        $refreshToken	= get_user_meta_value($mcmClientId,'rtoken'); // API token of mcmClientId
        if(empty($refreshToken)) return FALSE; // IF empty , no reason to continue , break

        $adcampaign = $this->adcampaign_m->set_post_type()->get($adCampaignId);
        if( ! $adcampaign) return FALSE;

		$campaign = new Campaign($adcampaign->post_slug);

		$campaign->setTrackingUrlTemplate($trackingUrlTemplate);
		// Create an ad group operation.
	    $operation = new CampaignOperation();
	    $operation->setOperand($campaign);
	    $operation->setOperator(Operator::SET);

	    try
	    {
		    // $result = $campaignService->mutate([$operation]);
		    $result = TRUE; // FAKE RETURN
	    	update_post_meta($adCampaignId,'trackingUrlTemplate',$trackingUrlTemplate);
	    	return $trackingUrlTemplate;
	    }
	    catch (Exception $e)
	    {
	    	return FALSE;	
	    }

	    return TRUE;
	}


	/**
	 * Gets all ip block.
	 *
	 * @param      integer  $mcm_account_id  The mcm accousnt identifier
	 * @param      integer  $adCampaignId    The ad campaign identifier
	 *
	 * @return     <type>   All ip block.
	 */
	public function getAllIPBlock($mcm_account_id = 0, $adCampaignId = 0)
	{
		$this->load->model('googleads/mcm_account_m');
		$mcmAccount = $this->mcm_account_m->set_term_type()->get($mcm_account_id); // Get mcm_account in DB
  		if(empty($mcmAccount)) return FALSE;

        $mcmClientId	= 1402; // systemapi@adsplus.vn
        $refreshToken	= get_user_meta_value($mcmClientId,'rtoken'); // API token of mcmClientId
        if(empty($refreshToken)) return FALSE; // IF empty , no reason to continue , break

        $adcampaign = $this->adcampaign_m->set_post_type()->get($adCampaignId);
        if( ! $adcampaign) return FALSE;
        
        $ipBlocks = array();
        $campaign 	= NULL;
        $session 	= $this->init_credential($refreshToken, $mcmAccount->term_name);
        $campaignCriterionService = (new AdWordsServices())->get($session, CampaignCriterionService::class);

        try
        {
        	// Create AWQL query.
		    $query = 'SELECT Id, IpAddress,CriteriaType WHERE CriteriaType = IP_BLOCK';

			// Create paging controls.
			$offset = 0;
			$totalNumEntries = 0;
			do 
			{
				$page_query = sprintf('%s LIMIT %d,%d', $query, $offset, self::PAGE_LIMIT);
				// Make the query request.
				$page = $campaignCriterionService->query($page_query);

				// Display results from the query.
				if ($page->getEntries() !== NULL)
				{
					$totalNumEntries = $page->getTotalNumEntries();
					var_dump(count($page->getEntries()));
					foreach ($page->getEntries() as $object)
					{
						$criterion 	= $object->getCriterion();
						$ipBlock 	= ['id' => $criterion->getId(), 'type' => $criterion->getType(), 'ipAddress' => $criterion->getIpAddress()];
						array_push($ipBlocks, $ipBlock);
					}
				}

				$offset += self::PAGE_LIMIT;

			} while ($offset < $totalNumEntries);

        }
        catch (Exception $e)
        {
        	return FALSE;
        }

        if(empty($ipBlocks)) return FALSE;

        return  array_column($ipBlocks, NULL,'id');
	}


		/**
	 * Adds an ip block.
	 *
	 * @param      integer  $mcm_account_id  The mcm account identifier
	 * @param      integer  $adCampaignId    The ad campaign identifier
	 * @param      array    $ip_addresses    The ip addresses
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function addIpBlock($mcm_account_id = 0, $adCampaignId = 0,$ip_addresses = array())
	{
		is_array($ip_addresses) OR $ip_addresses = explode(',', $ip_addresses);
		if(empty($ip_addresses)) return FALSE;

		$this->load->model('googleads/mcm_account_m');
		$mcmAccount = $this->mcm_account_m->set_term_type()->get($mcm_account_id); // Get mcm_account in DB
  		if(empty($mcmAccount)) return FALSE;
        
        $mcmClientId	= 1402; // systemapi@adsplus.vn
        $refreshToken	= get_user_meta_value($mcmClientId,'rtoken'); // API token of mcmClientId
        if(empty($refreshToken)) return FALSE; // IF empty , no reason to continue , break
        $adcampaign = $this->adcampaign_m->set_post_type()->get($adCampaignId);
        if( ! $adcampaign) return FALSE;

		$operations = array();
        $campaign 	= NULL;
        $session 	= $this->init_credential($refreshToken, $mcmAccount->term_name);
        $campaignCriterionService = (new AdWordsServices())->get($session, CampaignCriterionService::class);

		foreach($ip_addresses as $ip)
		{
			$ipBlock = new IpBlock();
			$ipBlock->setIpAddress(trim($ip));

			// Add a negative campaign criterion.
			$negativeCriterion = new NegativeCampaignCriterion();
			$negativeCriterion->setCampaignId($adcampaign->post_slug); // Set campaign ID
			$negativeCriterion->setCriterion($ipBlock);

			$operation = new CampaignCriterionOperation();
			$operation->setOperator(Operator::ADD);
			$operation->setOperand($negativeCriterion);

			try
			{	
				$result = $campaignCriterionService->mutate([$operation]);
			} 
			catch (Exception $e) {
				log_message('error', "({$ip}) ".$e->getMessage());
			}
		}
	}


	/**
	 * Removes all ip block.
	 *
	 * @param      integer  $mcm_account_id  The mcm account identifier
	 * @param      integer  $adCampaignId    The ad campaign identifier
	 */
	public function removeIpBlock($mcm_account_id = 0, $adCampaignId = 0, $ipBlocks = array())
	{
		if(empty($ipBlocks)) return FALSE;

		$this->load->model('googleads/mcm_account_m');
		$mcmAccount = $this->mcm_account_m->set_term_type()->get($mcm_account_id); // Get mcm_account in DB
  		if(empty($mcmAccount)) return FALSE;
        
        $mcmClientId	= 1402; // systemapi@adsplus.vn
        $refreshToken	= get_user_meta_value($mcmClientId,'rtoken'); // API token of mcmClientId
        if(empty($refreshToken)) return FALSE; // IF empty , no reason to continue , break
        $adcampaign = $this->adcampaign_m->set_post_type()->get($adCampaignId);
        if( ! $adcampaign) return FALSE;

		$operations = array();
        $campaign 	= NULL;
        $session 	= $this->init_credential($refreshToken, $mcmAccount->term_name);
        $campaignCriterionService = (new AdWordsServices())->get($session, CampaignCriterionService::class);

		foreach($ip_addresses as $ip)
		{
			$ipBlock = new IpBlock();
			$ipBlock->setIpAddress(trim($ip));

			// Add a negative campaign criterion.
			$negativeCriterion = new NegativeCampaignCriterion();
			$negativeCriterion->setCampaignId($adcampaign->post_slug); // Set campaign ID
			$negativeCriterion->setCriterion($ipBlock);

			$operation = new CampaignCriterionOperation();
			$operation->setOperator(Operator::ADD);
			$operation->setOperand($negativeCriterion);

			try
			{	
				$result = $campaignCriterionService->mutate([$operation]);
			} 
			catch (Exception $e) {
				log_message('error', "({$ip}) ".$e->getMessage());
			}
		}
	}

	/**
	 * { function_description }
	 *
	 * @param      integer  $mcm_account_id  The mcm account identifier
	 * @param      integer  $adCampaignId    The ad campaign identifier
	 * @param      array    $ip_addresses    The ip addresses
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function mutateIpBlock($mcm_account_id = 0, $adCampaignId = 0,$ip_addresses = array())
	{
		$this->load->model('googleads/mcm_account_m');
		$mcmAccount = $this->mcm_account_m->set_term_type()->get($mcm_account_id); // Get mcm_account in DB
  		if(empty($mcmAccount)) return FALSE;
        
        $mcmClientId	= 1402; // systemapi@adsplus.vn
        $refreshToken	= get_user_meta_value($mcmClientId,'rtoken'); // API token of mcmClientId
        if(empty($refreshToken)) return FALSE; // IF empty , no reason to continue , break
        $adcampaign = $this->adcampaign_m->set_post_type()->get($adCampaignId);
        if( ! $adcampaign) return FALSE;

        $campaign 	= NULL;
        $session 	= $this->init_credential($refreshToken, $mcmAccount->term_name);
        $campaignCriterionService = (new AdWordsServices())->get($session, CampaignCriterionService::class);

        // $IpBlock = new IpBlock($adcampaign->post_slug);
        
        $ip = '115.79.42.123';
        $ipBlock = new IpBlock(1,'IP_BLOCK','IpBlock');
		prd($ipBlock);

        prd($campaignCriterionService);
        prd(func_get_args());
        die('paused !!!');
		
		# SOLUTION 2 : construct campaign object
		$campaign = new Campaign($adcampaign->post_slug);

		$campaign->setTrackingUrlTemplate($trackingUrlTemplate);
		// Create an ad group operation.
	    $operation = new CampaignOperation();
	    $operation->setOperand($campaign);
	    $operation->setOperator(Operator::SET);

	    try
	    {
		    // $result = $campaignService->mutate([$operation]);
		    $result = TRUE; // FAKE RETURN
	    	update_post_meta($adCampaignId,'trackingUrlTemplate',$trackingUrlTemplate);
	    	return $trackingUrlTemplate;
	    }
	    catch (Exception $e)
	    {
	    	return FALSE;	
	    }

	    return TRUE;
	}


	/**
	 * Retrieve all campaigns belongs to mcm_account via Adwords API
	 *
	 * @param      integer  $mcm_account_id  The mcm account identifier
	 *
	 * @return     array    campaigns
	 */
	public function request_all($mcm_account_id = 0)
	{
		$mcm_account = $this->mcm_account_m->get($mcm_account_id);
        if(empty($mcm_account)) return FALSE;

        /**
         * List all mcm_account_clients belongs to this mcm_account
         * to retrieve : live dynamic refresh_token
         */
        $this->load->model('googleads/mcm_account_clients_m');
        $mcm_account_clients = $this->mcm_account_clients_m->get_mcm_account_clients($mcm_account->term_id);
        if(empty($mcm_account_clients)) return FALSE;

        foreach ($mcm_account_clients as $mcm_account_client)
        {
        	/* Get refresh token belongs to mcm_account_client */
            $mcm_refresh_token = get_user_meta_value($mcm_account_client['user_id'],'rtoken');
            if(empty($mcm_refresh_token)) continue;

            /* Create session by refresh token retrieved */
            $campaigns 	= array();
            $session 	= $this->init_credential($mcm_refresh_token, $mcm_account->term_name);
            $campaign_service = (new AdWordsServices())->get($session, CampaignService::class);

            try
            {
            	// Create AWQL query.
			    $query = 'SELECT Id, Name, Status, StartDate, AdvertisingChannelType, TrackingUrlTemplate, SelectiveOptimization  ORDER BY Name';

				// Create paging controls.
				$offset = 0;
				$totalNumEntries = 0;
				do 
				{
					$page_query = sprintf('%s LIMIT %d,%d', $query, $offset, self::PAGE_LIMIT);
					// Make the query request.
					$page = $campaign_service->query($page_query);

					// Display results from the query.
					if ($page->getEntries() !== null)
					{
						$totalNumEntries = $page->getTotalNumEntries();
						foreach ($page->getEntries() as $campaign)
						{
							$campaigns[] = array(
								'id' 		=> $campaign->getId(),
								'name' 		=> $campaign->getName(),
								'startDate' => $campaign->getStartDate(),
								'status' => strtolower($campaign->getStatus()),

								'advertisingChannelType' => strtolower($campaign->getAdvertisingChannelType()),
								'trackingUrlTemplate'	 => $campaign->getTrackingUrlTemplate(),
								'selectiveOptimization'	 => $campaign->getSelectiveOptimization(),
							);
						}
					}

					// Advance the paging offset.
					$offset += self::PAGE_LIMIT;
				} while ($offset < $totalNumEntries);

            } catch (Exception $e) {
            	continue;	
            }

            /**
             * If API return empty , try next mcm_account
             * else return campaigns result and stop loop
             */
            if(empty($campaigns)) continue;

            return $campaigns;
        }
	}


	/**
	 * Init Session from Credentital
	 *
	 * @param      string  $refresh_token       The refresh token
	 * @param      string  $client_customer_id  The client customer identifier
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	protected function init_credential($refresh_token = '',$client_customer_id = '')
	{
		$this->load->config('credentials');

	    $oAuth2Credential = (new OAuth2TokenBuilder())
        ->withClientId($this->config->item('clientId','mcm'))
        ->withClientSecret($this->config->item('clientSecret','mcm'))
        ->withRefreshToken($refresh_token)
        ->build();

        $session = (new AdWordsSessionBuilder())
        ->withDeveloperToken( $this->config->item('googleadsDeveloperToken'))
        ->withClientCustomerId($client_customer_id)
        ->withOAuth2Credential($oAuth2Credential)
        ->build();

        return $session;
	}
}
/* End of file Adcampaign_m.php */
/* Location: ./application/modules/googleads/models/Adcampaign_m.php */