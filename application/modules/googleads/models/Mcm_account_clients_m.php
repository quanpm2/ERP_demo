<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

require_once(APPPATH. 'models/Term_users_m.php');

class Mcm_account_clients_m extends Term_users_m {

	function __construct()
	{
		parent::__construct();
	}

	public function set_mcm_account_clients($mcm_account_id = 0,$mcm_clients = array())
	{
		// clean cache mapping data
		$this->scache->delete("mcm_account/{$mcm_account_id}_clients_mcm_client");

		if(empty($mcm_account_id) || empty($mcm_clients)) return FALSE;

		if(!is_array($mcm_clients))
		{
			$mcm_clients = [$mcm_clients];
		}

		foreach ($mcm_clients as $mcm_client_id)
		{
			$account_client = $this->get_by(['term_id'=>$mcm_account_id,'user_id'=>$mcm_client_id]);
			if($account_client) continue;

			$this->insert(['term_id'=>$mcm_account_id,'user_id'=>$mcm_client_id]);
		}
	}

	public function get_mcm_account_clients($mcm_account_id = 0)
	{
		$cache_key = "mcm_account/{$mcm_account_id}_clients_mcm_client";
		$account_clients = $this->scache->get($cache_key);
		if(!empty($account_clients)) return $account_clients;

		$this->load->model('googleads/mcm_client_m');
		$account_clients = $this
		->join('user','user.user_id = term_users.user_id')
		->as_array()
		->get_many_by(['term_users.term_id'=>$mcm_account_id,'user.user_type'=>$this->mcm_client_m->user_type]);
		
		if(empty($account_clients)) return FALSE;

		$this->scache->write($account_clients,$cache_key);

		return $account_clients;
	}
}
/* End of file Mcm_account_clients_m.php */
/* Location: ./application/modules/googleads/models/Mcm_account_clients_m.php */