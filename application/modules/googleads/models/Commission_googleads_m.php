<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'models/Commission_m.php');

class Commission_googleads_m extends Commission_m {

    protected $service_type = NULL;
    protected $commission_package = 'account_type';

	function __construct()
	{
		parent::__construct();

        defined('FOREIGN_CONTRACTOR_TAX')  OR define('FOREIGN_CONTRACTOR_TAX', 0.05);
	}

    public function set_instance($term_id = 0)
    {
		$service_type = get_term_meta_value($term_id, 'service_type');
		$this->service_type = $service_type;

        switch ($service_type)
        {
            case 'account_type' : 
                $this->load->model('googleads/commission_googleads_account_type_m');
                $this->instance = $this->commission_googleads_account_type_m;
                $this->commission_package = 'account_type';
                break;

            case 'cpc_type' : 
                $this->load->model('googleads/commission_googleads_cpc_type_m');
                $this->instance = $this->commission_googleads_cpc_type_m;
                $this->commission_package = 'cpc_type';
                break;

            default:
                $this->instance = NULL;
                break;
        }

        return TRUE;
    }

    public function get_commission_package()
    {
        return $this->commission_package;
    }

    /**
     * Gets the commision rate.
     *
     * @param      integer  $amount  The amount
     *
     * @return     integer  The rate.
     */
    public function get_rate($amount = 0)
    {
        $package    = $this->commission_package;
        $commission = $this->config->item($package, 'commission');

        if(empty($commission)) return 0;

        $rate = 0;
        foreach ($commission as $rule)
        {   
            if($amount < $rule['min'] || $amount > $rule['max']) continue;

            $rate = $rule['rate'];
            break;
        }

        return $rate;
    }
    
    public function get_actually_collected($term_id = 0, $user_id = 0, $start_time = '', $end_time = '')
    {
        $this->set_instance($term_id);
        $instance = $this->get_instance();
        if( ! $instance) return FALSE;
        
    	return $instance->get_actually_collected($term_id, $user_id, $start_time, $end_time);
    }

    /**
     * Gets the amount kpi.
     *
     * @param      integer  $term_id     The term identifier
     * @param      integer  $user_id     The user identifier
     * @param      string   $start_time  The start time
     * @param      string   $end_time    The end time
     *
     * @return     <type>   The amount kpi.
     */
    public function get_amount_kpi($term_id = 0, $user_id = 0, $start_time = '', $end_time = '')
    {
        $actually_collected = (float) $this->get_actually_collected($term_id, $user_id, $start_time, $end_time);
        if(empty($actually_collected)) return $actually_collected;

        $vat = (int) get_term_meta_value($term_id , 'vat');
        if(empty($vat)) return $actually_collected;

        return $actually_collected * (1 + FOREIGN_CONTRACTOR_TAX);
    }
}
/* End of file Commisision_googleads_m.php */
/* Location: ./application/modules/googleads/models/Commisision_googleads_m.php */