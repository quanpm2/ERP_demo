<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Googleads_log_m extends Log_m {

	public $prefix_log_type = 'googleads-report-';

    public function __construct(){

        parent::__construct();
    }
}
/* End of file googleads_log_m.php */
/* Location: ./application/modules/seotraffic/models/googleads_log_m.php */