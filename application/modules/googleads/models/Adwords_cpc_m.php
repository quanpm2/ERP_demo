<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adwords_cpc_m extends Base_adwords_m {

    protected $total_avgposition = 0;
    private $_custom_date = array();
    public $report_type = array();
    public $network = 'ALL';

    function __construct()
    {
        parent::__construct();

        $this->report_type = array(
            'campaigns' => 'CAMPAIGN_PERFORMANCE_REPORT',
            'adgroups' => 'ADGROUP_PERFORMANCE_REPORT',
            'keywords' => 'CRITERIA_PERFORMANCE_REPORT',
            'geolocations' => 'GEO_PERFORMANCE_REPORT');
    }


    /**
     * Calculates the expected end time.
     *
     * @param      <type>   $term_id  The term identifier
     *
     * @return     integer  The expected end time.
     */
    public function calc_expected_end_time($term_id)
    {
        $expected_end_time = 0; // Thời gian dự kiến kết thúc
        $total_budget_progress = 0; // Tổng click tiến độ


        /* Tính tổng click tiến độ */
        $commit_click = 0;
        switch ($this->network) 
        {
            case 'search':
                $commit_click = (int) get_term_meta_value($term_id,'contract_clicks');
                break;

            case 'display':
                $commit_click = (int) get_term_meta_value($term_id,'gdn_contract_clicks');
                break;
            
            default:
                $search_contract_clicks = (int) get_term_meta_value($term_id,'contract_clicks');
                $gdn_contract_clicks = (int) get_term_meta_value($term_id,'gdn_contract_clicks');
                $commit_click = $search_contract_clicks + $gdn_contract_clicks;
                $this->network = 'ALL';
                break;
        }
        $commit_start_time = get_term_meta_value($term_id,'googleads-begin_time');
        $commit_end_time = get_term_meta_value($term_id,'googleads-end_time');
        $commit_day = round(($commit_end_time - $commit_start_time)/86400) + 1;

        $actual_start_time = get_term_meta_value($term_id,'start_service_time');
        $actual_end_time =  strtotime('-1 second',$this->mdate->startOfDay());  
        $actual_day = round(($actual_end_time - $actual_start_time)/86400) + 1;
        $total_clicks_progress = ($commit_click / $commit_day * $actual_day);

        /* Tính tổng ngân sách thực tế */
        $progress = $this->get_the_progress($term_id);
        $total_clicks_actual = ($progress/100) * $commit_click;

        $expected_day = 0;
        if($total_clicks_actual > 0)
            $expected_day = (int)round(div($total_clicks_progress,$total_clicks_actual)* $commit_day) - 1;

        $expected_end_time = @strtotime("+{$expected_day} day",$actual_start_time);

        return $expected_end_time;
    }


    /**
     * Gets the progress.
     *
     * @param      <type>   $term_id       The term identifier
     * @param      <type>   $start_time    The start time
     * @param      <type>   $end_time      The end time
     * @param      <type>   $ignore_cache  The ignore cache
     *
     * @return     integer  The progress.
     */
    function get_the_progress($term_id,$start_time = FALSE,$end_time = FALSE,$ignore_cache = FALSE)
    {
        $actual = $progress = $target = 0;

        switch ($this->network) 
        {
            case 'search':
                $target = (int) get_term_meta_value($term_id,'contract_clicks');
                break;

            case 'display':
                $target = (int) get_term_meta_value($term_id,'gdn_contract_clicks');
                break;
            
            default:
                $search_contract_clicks = (int) get_term_meta_value($term_id,'contract_clicks');
                $gdn_contract_clicks = (int) get_term_meta_value($term_id,'gdn_contract_clicks');

                if(!empty($search_contract_clicks) && !empty($gdn_contract_clicks))
                {
                    $this->network = 'ALL';    
                    $target = $search_contract_clicks + $gdn_contract_clicks;    
                }
                else 
                {
                    if(!empty($search_contract_clicks)) 
                        $this->network = 'search';
                    else $this->network = 'display';
                    return $this->get_the_progress($term_id,$start_time,$end_time,$ignore_cache);
                }
                
                break;
        }
        
        $mcm_account_id = get_term_meta_value($term_id,'mcm_client_id');
        $this->set_mcm_account($mcm_account_id);

        $all_time = empty($start_time) && empty($end_time);
        $start_time = empty($start_time) ? get_term_meta_value($term_id,'googleads-begin_time') : $start_time;
        if(empty($end_time)) 
            $end_time = $this->mdate->convert_time(get_term_meta_value($term_id,'googleads-end_time')) ?: time();

        if($ignore_cache)
        {
            $this->download('ACCOUNT_PERFORMANCE_REPORT',$start_time,$end_time);
        }

        $account_data = $this->get_cache('ACCOUNT_PERFORMANCE_REPORT',$start_time,$end_time);
        $account_data = array_filter($account_data);

        if(!empty($account_data))
        {
            $accounts_filtered = array();
            foreach ($account_data as $timestamp => $accounts) 
            {
                if(empty($accounts)) continue;

                $accounts = array_filter($accounts,function($c){ 
                    if($this->network != 'ALL' && (empty($c['network']) || !stristr($c['network'], $this->network))) 
                        return FALSE;
                    return TRUE;
                });

                $accounts_filtered = array_merge_recursive($accounts_filtered,$accounts);
            }
            $actual = array_sum(array_column($accounts_filtered, 'clicks'));

            if($all_time)
            {
                $meta_key = strtolower("{$this->network}_actual_result");
                
                // Store last adword account data updated time
                $actual_result_past = get_term_meta_value($term_id, $meta_key);
                if($actual_result_past != $actual)
                {
                    // update result updated on by current time excuted
                    update_term_meta($term_id, 'result_updated_on', time());
                }

                update_term_meta($term_id, $meta_key, $actual);
            }
        }

        $progress = ($target > 0) ? $actual/$target*100 : $progress;
        $progress = number_format($progress,3);

        if($all_time)
        {
            $actual_progress_percent = number_format( (double) get_term_meta_value($term_id,'actual_progress_percent'),3);
            if($actual_progress_percent != $progress) update_term_meta($term_id,'actual_progress_percent',$progress);

            /* Cập nhật ngân sách */
            $actual_budget                  = (int) get_term_meta_value($term_id, 'actual_budget');
            $actual_progress_percent_net    = number_format(div($actual, $actual_budget), 3);
            if($actual_progress_percent_net != number_format( (double) get_term_meta_value($term_id,'actual_progress_percent_net'),3))  
                update_term_meta($term_id, 'actual_progress_percent_net', $actual_progress_percent_net);
        }

        return $progress;
    }


    /**
     * widget_complete_percent_bar
     *
     * @param      integer        $real    The real
     * @param      integer        $total   The total
     * @param      array|integer  $colors  The colors
     * @param      array          $titles  The titles
     *
     * @return     string HTML_result
     */
    function widget_complete_percent_bar($real =0, $total = 0, $colors = array(), $titles = array())
    {
        $colors_default = array(
            '50' =>'progress-bar-red',
            '80' =>'progress-bar-yellow',
            '90' =>'progress-bar-green',
            '100' =>'progress-bar-aqua'
            );
        $titles_default = array(
            'percent' => '% thực hiện hiện tại',
            'total' => 'tổng',
            'real' => 'thực tế'
            );
        $colors = $colors+$colors_default ;
        $titles = array_merge($titles_default, $titles);

        return parent::widget_complete_percent_bar($real, $total, $colors, $titles);
    }
}
/* End of file Adwords_cpc_m.php */
/* Location: ./application/modules/googleads/models/Adwords_cpc_m.php */