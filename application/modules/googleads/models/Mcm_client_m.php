<?php

if (!defined('BASEPATH')) exit('No direct script access allowed'); 

require_once(APPPATH. 'models/User_m.php');

class Mcm_client_m extends User_m {

    public $user_type = 'mcm_client';
    public $before_create = array('create_timestamp');

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Callback generate timestamp
     *
     * @param      <type>  $row    The row
     *
     * @return     array data updated
     */
    public function create_timestamp($row)
    {
        $row['user_time_create'] = time();
        return $row;
    }

    /**
     * Set default query for mcm_client_m.
     *
     * @return     Mcm_client_m
     */
    public function set_get_client()
    {
        $this->where('user_type',$this->user_type);
        return $this;
    }
}
/* End of file Mcm_client_m.php */
/* Location: ./application/modules/googleads/models/Mcm_client_m.php */