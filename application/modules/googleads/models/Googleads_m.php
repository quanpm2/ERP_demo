<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Contract_m.php');

class Googleads_m extends Contract_m {

	public $term_type = 'google-ads';
	public $meta;

	function __construct()
	{
		parent::__construct();
		$this->meta = $this->termmeta_m;

		$this->load->model('ads_segment_m');
		$this->load->model('googleads/mcm_account_m');
	}
	

	/**
	 * Sets the default query.
	 *
	 * @return     this
	 */
	public function set_default_query()
	{
		$this->load->config('googleads/googleads');
		$status = $this->config->item('status','googleads');
		$status = array_keys($status);
		return $this->set_term_type()->where_in('term.term_status',$status);
	}


	/**
	 * { function_description }
	 *
	 * @param      integer  $status  The status
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function status_label($status = 0)
	{
		$this->config->load('invoice');
		$array_status = $this->config->item('status','invoice');
		return isset($array_status[$status]) ? $array_status[$status] : $status;
	}


	/**
	 * Gets the adword model.
	 *
	 * @param      mixed   $termId_or_serviceType  The term identifier or service type
	 *
	 * @return     mixed  The adword model.
	 */
	function get_adword_model($termId_or_serviceType)
	{
		$this->load->model('googleads/base_adwords_m');
		$service_type = (int)$termId_or_serviceType;
		if(!empty($service_type))
		{
			$service_type = get_term_meta_value($termId_or_serviceType,'service_type');
		}

		if(!$service_type)
			return false;

		$model = '';
		switch ($service_type) {

			case 'account_type':
				$model = 'adwords_account_m';
				break;

			case 'cpc_type':
				$model = 'adwords_cpc_m';
				break;
		}
		if(!$model)
			return false;

		$this->load->model("googleads/{$model}");
		return $this->{$model};
	}


	/**
	 * Gets the progress.
	 *
	 * @param      integer  $term_id  The term identifier
	 * @param      string  $type     The type
	 *
	 * @return     float  The progress.
	 */
	function get_the_progress($term_id, $type = 'All')
	{
		$target_model = $this->get_adword_model($term_id);
		$target_model->network = $type;
		$progress = $target_model->get_the_progress($term_id);
		return $progress;
	}

	function get_segments($args = [])
	{
		if( ! $this->contract) return FALSE;

		$defaultArgs = [
			'ignore_cache' 	=> false,
			'start_time' 	=> get_term_meta_value($this->contract->term_id,'googleads-begin_time') ?: start_of_day(),
			'end_time'		=> get_term_meta_value($this->contract->term_id,'googleads-end_time') ?: end_of_day(),
		];

		$args = wp_parse_args($args, $defaultArgs);

		$result 		= array();
        $adsSegments    = $this->term_posts_m->get_term_posts($this->contract->term_id, $this->ads_segment_m->post_type);

        if( ! empty($adsSegments))
        {
        	$target_model = $this->get_adword_model($this->contract->term_id);

            foreach ($adsSegments as &$adsSegment)
            {
                $adaccount = $this->term_posts_m->get_post_terms($adsSegment->post_id, $this->mcm_account_m->term_type);
                if(empty($adaccount)) continue;

                $adaccount  = reset($adaccount);

                $_segment_start = $adsSegment->start_date;
                $_segment_end = $adsSegment->end_date ?: time();

                $_start = max($args['start_time'], $_segment_start);
                $_end   = min($args['end_time'], $_segment_end);

                if($_start > $_end) continue;

		        $target_model->set_mcm_account($adaccount->term_id);

				# IF request is force to download new performance-report , then download report from API first and cache
				if( ! empty($args['ignore_cache']))
				{
					$target_model->download('ACCOUNT_PERFORMANCE_REPORT', $_start, $_end);
				}

				$adsSegment->adaccount = $adaccount;

				$account_data = $target_model->get_cache('ACCOUNT_PERFORMANCE_REPORT', $_start, $_end);
				// $account_data = $target_model->get_db($this->contract->term_id, $_start, $_end);
				$account_data AND $account_data = array_filter($account_data);

				$adsSegment->insight = $account_data ? array_merge(...$account_data) : [];
            }
        }

        return $adsSegments;
	}

	function get_accounts($args = [])
	{	
		if( ! $this->contract) return FALSE;

		$defaultArgs = [
			'ignore_cache' 	=> false,
			'start_time' 	=> get_term_meta_value($this->contract->term_id, 'googleads-begin_time') ?: start_of_day(),
			'end_time'		=> get_term_meta_value($this->contract->term_id, 'googleads-end_time') ?: end_of_day(),
		];

		$args = wp_parse_args($args, $defaultArgs);

		$insight        = [];
        $adsSegments    = $this->term_posts_m->get_term_posts($this->contract->term_id, $this->ads_segment_m->post_type);

        $this->load->model('googleads/base_adwords_m');

        if( ! empty($adsSegments))
        {
            foreach ($adsSegments as $adsSegment)
            {
                $adaccount = $this->term_posts_m->get_post_terms($adsSegment->post_id, $this->mcm_account_m->term_type);
                if(empty($adaccount)) continue;

                $adaccount  = reset($adaccount);

                $_segment_start = $adsSegment->start_date;
                $_segment_end = $adsSegment->end_date ?: time();

                $_start = max($args['start_time'], $_segment_start);
                $_end   = min($args['end_time'], $_segment_end);

                if($_start > $_end) continue;

				# IF request is force to download new performance-report , then download report from API first and cache
				if( ! empty($args['ignore_cache']))
				{
					$this->base_adwords_m->set_mcm_account($adaccount->term_id);
					$this->base_adwords_m->download('ACCOUNT_PERFORMANCE_REPORT', $_start, $_end, $this->contract->term_id);
				}

				$_iSegments = $this->mcm_account_m->get_data($adaccount->term_id, $_start, $_end);
                $insight    = array_merge($_iSegments, $insight);
            }
        }

        
		return $insight;
	}


	function get_accounts2($args = [])
	{	
		if( ! $this->contract) return FALSE;

		$defaultArgs = [
			'ignore_cache' => false,
			'start_time' => get_term_meta_value($this->contract->term_id,'googleads-begin_time'),
			'end_time' => get_term_meta_value($this->contract->term_id,'googleads-end_time') ?: time(),
		];

		$args = wp_parse_args($args, $defaultArgs);

		$target_model = $this->get_adword_model($this->contract->term_id);

		$mcm_account_id = get_term_meta_value($this->contract->term_id,'mcm_client_id');
        $target_model->set_mcm_account($mcm_account_id);

		# IF request is force to download new performance-report , then download report from API first and cache
		$args['ignore_cache'] AND $target_model->download('ACCOUNT_PERFORMANCE_REPORT', $args['start_time'], $args['end_time']);

		$account_data = $target_model->get_cache('ACCOUNT_PERFORMANCE_REPORT', $args['start_time'], $args['end_time']);

		$result = array();

		if( empty($account_data)) return $result;

		foreach ($account_data as $timestamp => $accounts) 
        {
            if(empty($accounts)) continue;

            $result = array_merge_recursive($result,$accounts);
        }

		return $result;
	}

    function get_conflict_contract($args = []){
        if( ! $this->contract) return FALSE;

        $defaultArgs = [
			'segments' => [],
		];

		$args = wp_parse_args($args, $defaultArgs);
        
        $contract_id = $this->contract->term_id;

        $segments = $args['segments'];
        if(empty($segments)){
            $conflictContracts = $this->ads_segment_m->set_post_type()
            ->join('term_posts AS tp_contracts', 'tp_contracts.post_id = posts.post_id')
            ->join('term_posts AS tp_adaccounts', 'tp_adaccounts.post_id = posts.post_id')
            ->join('term AS adaccounts', "adaccounts.term_id = tp_adaccounts.term_id AND adaccounts.term_type = '{$this->mcm_account_m->term_type}'")
            ->join('term_posts AS another_tp_segments', 'another_tp_segments.term_id = adaccounts.term_id')
            ->join('posts AS another_segments', "another_segments.post_id = another_tp_segments.post_id AND another_segments.post_type = '{$this->ads_segment_m->post_type}'")
            ->join('term_posts AS tp_another_contract', 'tp_another_contract.post_id = another_segments.post_id')
            ->join('term AS another_contracts', "another_contracts.term_id = tp_another_contract.term_id AND another_contracts.term_type = '{$this->term_type}'")
            ->where('tp_contracts.term_id', $this->contract->term_id)
            ->where('another_segments.post_id != posts.post_id')
            ->where('another_segments.post_author = posts.post_author')
            ->where('posts.start_date < IF(another_segments.end_date = 0 OR another_segments.end_date IS NULL, UNIX_TIMESTAMP(), another_segments.end_date)')
            ->where('another_segments.start_date < IF(posts.end_date = 0 OR posts.end_date IS NULL, UNIX_TIMESTAMP(), posts.end_date)')
            ->select('
                another_contracts.term_id AS contract_id,
                another_contracts.term_name AS contract_name,
                another_contracts.term_status AS contract_status,
                another_contracts.term_type AS contract_type,
                another_contracts.term_description AS contract_description,
                adaccounts.term_id AS account_term_id,
                adaccounts.term_slug AS account_id,
                adaccounts.term_name AS account_name,
                adaccounts.term_parent AS account_parent,
                another_segments.start_date AS segments_start_date,
	            another_segments.end_date AS segments_end_date
            ')
            ->as_array()
            ->get_all();

            if(empty($conflictContracts)) return [];

            $conflictContracts = array_map(function($conflictContract){
                $term_id = $conflictContract['contract_id'];
                $conflictContract['contract_code'] = get_term_meta_value($term_id, 'contract_code');

                $account_term_id = $conflictContract['account_term_id'];
                $conflictContract['account_id'] = get_term_meta_value($account_term_id, 'customer_id');
                $conflictContract['account_name'] = get_term_meta_value($account_term_id, 'account_name');

                $conflictContract['segments_start_date'] = my_date($conflictContract['segments_start_date'], 'd/m/Y');
                $conflictContract['segments_end_date'] = empty($conflictContract['segments_end_date']) ? NULL : my_date($conflictContract['segments_end_date'], 'd/m/Y');
            
                return $conflictContract;
            }, $conflictContracts);
            
            return $conflictContracts;
        }

        $segments_group_by_adaccount_id = array_group_by($segments, 'adaccount_id');

        $ad_account_ids = array_keys($segments_group_by_adaccount_id);
        $db_ads_segments = $this->mcm_account_m->set_term_type()
            ->join('term_posts AS tp_ads_segment', 'tp_ads_segment.term_id = term.term_id')
            ->join('posts AS ads_segment', 'ads_segment.post_id = tp_ads_segment.post_id')
            ->join('term_posts AS tp_contract', 'tp_contract.post_id = ads_segment.post_id')
            ->join('term AS contract', 'contract.term_id = tp_contract.term_id')
            
            ->where_in('term.term_id', $ad_account_ids)
            ->where('contract.term_id !=', $contract_id)
            ->where('ads_segment.post_type', $this->ads_segment_m->post_type)
            ->where('contract.term_type = ', $this->term_type)

            ->select('
                contract.term_id AS contract_id,
                contract.term_name AS contract_name,
                contract.term_status AS contract_status,
                contract.term_type AS contract_type,
                term.term_id AS account_term_id,
                term.term_slug AS account_id,
                term.term_name AS account_name,
                term.term_parent AS account_parent,
                ads_segment.start_date AS segments_start_date,
                ads_segment.end_date AS segments_end_date
            ')
            ->as_array()
            ->get_all();
        if(empty($db_ads_segments))
        {
            return [];
        }

        $conflictContracts = [];
        foreach($db_ads_segments as $db_ads_segment)
        {
            $ad_account_id = $db_ads_segment['account_term_id'];
            $ads_segments = $segments_group_by_adaccount_id[$ad_account_id] ?? [];

            $db_ads_segment_start = start_of_day($db_ads_segment['segments_start_date']);
            $db_ads_segment_end = end_of_day($db_ads_segment['segments_end_date'] ?: time());

            $conflict_segments = array_filter($ads_segments, function($item) use ($db_ads_segment_start, $db_ads_segment_end){
                $start_date = start_of_day($item['start_date']);
                $end_date = end_of_day($item['end_date'] ?: time());

                if($start_date < $db_ads_segment_start && $end_date < $db_ads_segment_start)
                {
                    return FALSE;
                }

                if($start_date > $db_ads_segment_end && $end_date > $db_ads_segment_end)
                {
                    return FALSE;
                }

                return TRUE;
            });
            if(empty($conflict_segments))
            {
                continue;
            }

            $conflictContracts[] = $db_ads_segment;
        }
        
        if(empty($conflictContracts)) return [];

        $conflictContracts = array_filter($conflictContracts, function($item) use ($contract_id){
            return $item['contract_id'] != $contract_id;
        });

        $conflictContracts = array_map(function($conflictContract){
            $term_id = $conflictContract['contract_id'];
            $conflictContract['contract_code'] = get_term_meta_value($term_id, 'contract_code');

            $account_term_id = $conflictContract['account_term_id'];
            $conflictContract['account_id'] = get_term_meta_value($account_term_id, 'customer_id');
            $conflictContract['account_name'] = get_term_meta_value($account_term_id, 'account_name');

            $conflictContract['segments_start_date'] = my_date($conflictContract['segments_start_date'], 'd/m/Y');
            $conflictContract['segments_end_date'] = empty($conflictContract['segments_end_date']) ? NULL : my_date($conflictContract['segments_end_date'], 'd/m/Y');

            return $conflictContract;
        }, $conflictContracts);
        
        return $conflictContracts;
    }


	/**
	 * Gets all active terms.
	 *
	 * @return     array  All active.
	 */
	public function get_active_terms()
	{
		$terms = $this
		->set_default_query()
		->select('term.term_id,term_name,term_parent,term_status,term_type')
		->m_find([
            [ 'key' => 'adaccounts', 'value' => "", 'compare' => '!='],
            [ 'key' => 'advertise_start_time', 'value' => "", 'compare' => '!=']

        ])->get_all();

		if(empty($terms)) return FALSE;

		foreach ($terms as $key => $term)
		{
			# Ignore term if it's not proc
			if( ! parent::is_service_proc($term->term_id))
			{
				unset($terms[$key]);
				continue;
			}

			# Ignore term if it's not set begin time
			$advertise_start_time 	= get_term_meta_value($term->term_id, 'advertise_start_time');
			if(empty($advertise_start_time))
			{
				unset($terms[$key]);
				continue;
			}

			$adaccount_status = get_term_meta_value($term->term_id, 'adaccount_status');
			if('APPROVED' != $adaccount_status)
			{
				unset($terms[$key]);
				continue;
			}

			$adaccounts = get_term_meta($term->term_id, 'adaccounts', FALSE, TRUE);
        	$adaccounts AND $adaccounts = array_filter(array_map('intval', $adaccounts));
        	if(empty($adaccounts)) 
        	{
        		unset($terms[$key]);
				continue;
        	}
        	
			$advertise_end_time = (int) get_term_meta_value($term->term_id,'advertise_end_time');
	    	$last_end_time 		= strtotime('+1 day',$advertise_end_time);
	    	if(empty($advertise_end_time) || $last_end_time > time())
	    	{
	    		$term->begin_time 	= $advertise_start_time;
	    		$term->end_time 	= $advertise_end_time;
	    		continue;
	    	}

	    	unset($terms[$key]);
		}
		
		return $terms;
	}

	/**
	 * Calculates the expected end time.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     integer  The expected end time.
	 */
	public function calc_expected_end_time($term_id = FALSE)
	{
		if(empty($term_id)) return FALSE;
		$expected_end_time = $this->get_adword_model($term_id)->set_network('all')->calc_expected_end_time($term_id);
		return $expected_end_time;
	}
	
	/**
	 * Gets the real progress.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     float  The real progress.
	 */
	public function get_real_progress($term_id = FALSE)
	{
		$target_model = $this->get_adword_model($term_id);
		$retval = $target_model->calc_real_progress($term_id);
		return $retval;
	}


	/**
	 * Determines if it has permission.
	 *
	 * @param      integer  $term_id     The term identifier
	 * @param      string   $permission  The permission
	 * @param      integer  $user_id     The user identifier
	 *
	 * @return     boolean  True if has permission, False otherwise.
	 */
	public function has_permission($term_id = 0, $permission = '', $user_id = 0)
	{
		if(empty($permission)) $permission = $this->router->fetch_class().'.'.$this->router->fetch_method().'.access';
		if(empty($user_id)) $user_id = $this->admin_m->id;

		$relate_users = $this->admin_m->get_all_by_permissions($permission, $user_id);
		if(is_bool($relate_users)) return $relate_users;

		$this->load->model('term_users_m');
		$users = $this->term_users_m->get_the_users($term_id, 'admin');
		if(empty($users)) return FALSE;

		$user_intersect = array_intersect($relate_users, array_column($users, 'user_id'));
		if(empty($user_intersect)) return FALSE;

		return TRUE;
	}

	protected function set_behaviour_m()
	{
		if( ! $this->contract) return FALSE;
		$this->load->model('googleads/googleads_behaviour_m', 'behaviour_tmp');
		$this->behaviour_tmp->set_contract($this->contract);
	 	$this->behaviour_model = $this->behaviour_tmp->get_instance();
	 	return $this;
	}

	/**
	 * Calculates the fct.
	 *
	 * @return     <type>  The fct.
	 */
	public function calc_fct($percent = 0.05)
	{
		if( ! $this->contract || ! $this->behaviour_model) return FALSE;

		return $this->behaviour_model->calc_fct($percent);
	}

	/**
	 * Gets all by cid.
	 *
	 * @param      int     $cid    The cid
	 *
	 * @return     <type>  All by cid.
	 */
	public function get_all_by_cid($cid = 0)
	{
		if( ! empty($this->contract)) $cid = get_term_meta_value($this->contract->term_id, 'mcm_client_id');

		if(empty($cid)) return false;

		$m_args = array(
            'key' => 'mcm_client_id', 'value' => (int) $cid, 'compare' => '=' 
        );

        $contracts = $this->select('term.term_id, term_name, term_status, term_type')->m_find($m_args)->set_term_type()->get_all();

        foreach ($contracts as &$contract)
        {
            $contract->mcm_client_id = (int) get_term_meta_value($contract->term_id, 'mcm_client_id');
            $contract->googleads_begin_time = (int) get_term_meta_value($contract->term_id, 'googleads-begin_time');
            $contract->googleads_end_time = (int) get_term_meta_value($contract->term_id, 'googleads-end_time');
            $contract->contract_code = get_term_meta_value($contract->term_id, 'contract_code');

            $contract->actual_budget = (int) get_term_meta_value($contract->term_id, 'actual_budget');
            
            $contract_m = (new contract_m())->set_contract($contract);
            $contract->actual_result = (double) $contract_m->get_behaviour_m()->get_actual_result();
        }

        return $contracts;
	}

	/**
	 * Determines if valid start date.
	 *
	 * @param      int   $cid         The cid
	 * @param      int   $start_date  The start date
	 *
	 * @return     bool  True if valid start date, False otherwise.
	 */
	public function isValidStartDate($cid = 0, $start_date = 0)
	{
		if( ! $this->contract) return FALSE;

		return empty($this->get_conflict_by_start_date());
	}

	/**
	 * Gets the conflict by start date.
	 *
	 * @param      int     $cid         The cid
	 * @param      int     $start_date  The start date
	 *
	 * @return     <type>  The conflict by start date.
	 */
	public function get_conflict_by_start_date($cid = 0, $start_date = 0)
	{
		if( ! $this->contract) return FALSE;

		
        ! empty($this->contract) AND $this->where('term.term_id !=', (int) $this->contract->term_id);

		$this->contract->googleads_begin_time = (int) get_term_meta_value($this->contract->term_id, 'googleads-begin_time');

        $m_args = [ 'key' => 'mcm_client_id', 'compare' => '=' , 'value' => (int) $cid ];

        $contracts = $this->select('term.term_id, term_name, term_status, term_type')->m_find($m_args)->set_term_type()->where('term.term_id !=', (int) $this->contract->term_id)->get_all();

        if(empty($contracts)) return [];

        $contracts = array_map(function($x){

        	$x->mcm_client_id = (int) get_term_meta_value($x->term_id, 'mcm_client_id');;
            $x->googleads_begin_time = (int) get_term_meta_value($x->term_id, 'googleads-begin_time');
            $x->googleads_end_time = (int) get_term_meta_value($x->term_id, 'googleads-end_time') ?: end_of_day();

            $x->googleads_begin_date = my_date($x->googleads_begin_time);
            $x->googleads_end_date = my_date($x->googleads_end_time);

            $x->contract_code = get_term_meta_value($x->term_id, 'contract_code');
            return $x;
        }, $contracts);

        $contracts = array_filter($contracts, function($x) use ($start_date){
        	return ( $x->googleads_begin_time <= $start_date && $start_date <= $x->googleads_end_time ) || ( $start_date <= $x->googleads_begin_time && $start_date <= $x->googleads_end_time);
        });

        return $contracts;
	}


	/**
	 * Gets the segments.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     array      The segments.
	 */
	public function getSegments()
	{
		if( ! $this->contract) throw new Exception('CONTRACT UNSPECIFIED');
		
		$this->load->model('ads_segment_m');

		$segments = $this->term_posts_m->get_term_posts($this->contract->term_id, $this->ads_segment_m->post_type);
		if(empty($segments)) return [];

		$this->load->model('googleads/mcm_account_m');

        $segments = array_map(function($x){

            $adaccount = $this->term_posts_m->get_post_terms( $x->post_id, $this->mcm_account_m->term_type);
            $adaccount AND $adaccount = reset($adaccount);

            return array(
                'post_id' 		=> (int) $x->post_id,
                'start_date' 	=> (int) $x->start_date,
                'end_date'		=> (int) $x->end_date,
                'contract_id'	=> (int) $x->term_id,
                'post_author'	=> (int) $x->post_author,
                'post_content'	=> (int) $x->post_content,
                'adaccount_id'	=> (int) $adaccount->term_id,
                'adaccount' => array(
                	'id'	=> (int) $adaccount->term_id,
                	'account_name'	=> get_term_meta_value($adaccount->term_id, 'account_name'),
                	'customer_id'	=> $adaccount->term_name,
                	'currency_code'	=> get_term_meta_value($adaccount->term_id, 'currency_code'),
                    'source' => get_term_meta_value($adaccount->term_id, 'source'),
                ),
                'technician' => [
                    'display_name' => $this->admin_m->get_field_by_id($x->post_author, 'display_name'),
                    'user_email' => $this->admin_m->get_field_by_id($x->post_author, 'user_email'),
                ]
            );
        }, $segments);

        $segments = array_values($segments);

        $this->contract->segments = $segments;
        return $segments;
	}

    /**
	 * Gets the balance spend.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     array      The balance spend.
	 */
	public function getBalanceSpend()
	{
		if( ! $this->contract) throw new Exception('CONTRACT UNSPECIFIED');
		
		$this->load->model('balance_spend_m');

        $balance_spend = $this->term_posts_m->get_term_posts($this->contract->term_id, $this->balance_spend_m->post_type, [
            'where' => [
                'post_status' => 'publish', 
                'comment_status' => 'direct'
            ]
        ]);
		if(empty($balance_spend)) return [];

        $balance_spend = array_map(function($x){
            return array(
                'post_id' 		=> (int) $x->post_id,
                'start_date' 	=> (int) $x->start_date,
                'end_date'		=> (int) $x->end_date,
                'contract_id'	=> (int) $x->term_id,
                'spend'         => (int) $x->post_content
            );
        }, $balance_spend);

        $balance_spend = array_values($balance_spend);

        $this->contract->balance_spend = $balance_spend;
        return $balance_spend;
	}


	/**
	 * Gets the field configuration for datatable.
	 *
	 * @return     Array  The field configuration.
	 */
	public function get_field_config()
	{
		$config = parent::get_field_config();
		$config['columns'] = array_merge($config['columns'], array(

			'actual_result' => [
				'name' => 'actual_result',
				'title' => 'Đã sử dụng',
				'type' => 'number'
			],
			'actual_budget' => [
				'name' => 'actual_budget',
				'title' => 'NS thực thu',
				'type' => 'number'
			],
			'contract_budget' => [
				'name' => 'contract_budget',
				'title' => 'NS HĐ',
				'type' => 'number'
			],
			'contract_budget_payment_type' => [
				'name' => 'contract_budget_payment_type',
				'title' => 'Phương thức thanh toán',
				'type' => 'number'
			],
			'contract_budget_customer_payment_type' => [
				'name' => 'contract_budget_customer_payment_type',
				'title' => 'Tùy chọn thu & chi',
				'type' => 'number'
			],
			'actual_progress_percent_net' => [
				'name' => 'actual_progress_percent_net',
				'title' => '% H.Thành (NS thực)',
				'type' => 'number'
			],
			'actual_progress_percent' => [
				'name' => 'actual_progress_percent',
				'title' => '% H.Thành (NS HĐ)',
				'type' => 'number'
			],
			'real_progress' => [
				'name' => 'real_progress',
				'title' => 'Tiến độ (NS HĐ)',
				'type' => 'number'
			],
			'payment_real_progress' => [
				'name' => 'payment_real_progress',
				'title' => 'Tiến độ (NS thực)',
				'type' => 'number'
			],
			'cost_per_day_left' => [
				'name' => 'cost_per_day_left',
				'title' => 'est.NS/Ngày (NS HĐ)',
				'type' => 'number'
			],
			'payment_cost_per_day_left' => [
				'name' => 'payment_cost_per_day_left',
				'title' => 'est.NS/Ngày (NS thực)',
				'type' => 'number'
			],
			'expected_end_time' => [
				'name' => 'expected_end_time',
				'title' => 'Dự kiến (NS HĐ)',
				'type' => 'timestamp'
			],
			'payment_expected_end_time' => [
				'name' => 'payment_expected_end_time',
				'title' => 'Dự kiến (NS thực)',
				'type' => 'timestamp'
			],
			'cid' => [
				'name' => 'cid',
				'title' => 'CID',
				'type' => 'string'
			],

			'result_updated_on' => [
				'name' => 'result_updated_on',
				'title' => 'Cập nhật lúc',
				'type' => 'timestamp'
			],

			'result_pending_days' => [
				'name' => 'result_pending_days',
				'title' => 'Số ngày không phát sinh chi phí',
				'type' => 'number'
            ],

            'adaccount_status' => [
				'name' => 'adaccount_status',
				'title' => 'TT Adaccounts (GG)',
				'type' => 'string'
			],

            'contract_code' => [
				'name' => 'contract_code',
				'title' => 'Mã hợp đồng',
				'type' => 'string'
			],

            'customer_code' => [
				'name' => 'customer_code',
				'title' => 'Mã khách hàng',
				'type' => 'string'
			],

            'latest_spend_date' => array(
                'name' => 'latest_spend_date',
                'label' => 'Ngày ghi nhận spend mới nhất',
                'set_select' => FALSE,
                'title' => 'Ngày ghi nhận spend mới nhất',
                'type' => 'timestamp',
                'set_order' => FALSE
            ),
    
            'latest_spend_days' => array(
                'name' => 'latest_spend_days',
                'label' => 'Số ngày không ghi nhận spend',
                'set_select' => FALSE,
                'title' => 'Số ngày không ghi nhận spend',
                'type' => 'number',
                'set_order' => FALSE
            ),
		));

		return $config;
	}

	/**
	 * Gets the fields callback.
	 *
	 * @return     Array  The field callback.
	 */
	public function get_field_callback()
	{
		return array_merge(parent::get_field_callback(), array(

			/* Ngân sách đã sử dụng */
			'actual_result' => array(
				'field' => 'actual_result',
				'func'	=> function($data, $row_name){

					if( ! empty($data['actual_result'])) return $data;
					$_contract = (new contract_m())->set_contract((object) $data);
					$data['actual_result_raw'] = $_contract->get_behaviour_m()->get_actual_result();
					return $data;
				},
				'row_data' => FALSE
			),

			/* Ngân sách thực tế */
			'actual_budget' => array(
				'field' => 'actual_budget',
				'func'	=> function($data, $row_name){

					if( ! empty($data['actual_budget'])) return $data;

					$data['actual_budget_raw'] = (int) get_term_meta_value($data['term_id'], 'actual_budget');
					
					return $data;
				},
				'row_data' => FALSE
			),

			/* Ngân sách hợp đồng */
			'contract_budget' => array(
				'field' => 'contract_budget',
				'func'	=> function($data, $row_name){

					if( ! empty($data['contract_budget'])) return $data;
					
					$_contract = (new contract_m())->set_contract((object) $data);
					$data['contract_budget_raw'] = $_contract->get_behaviour_m()->calc_budget();
					return $data;
					
					return $data;
				},
				'row_data' => FALSE
			),

			/* Phương thức thanh toán ngân sách quảng cáo */
			'contract_budget_payment_type' => array(
				'field' => 'contract_budget_payment_type',
				'func'	=> function($data, $row_name){

					if( ! empty($data['contract_budget_payment_type'])) return $data;

					$this->load->config('googleads/contract');

					$type = get_term_meta_value($data['term_id'], 'contract_budget_payment_type');
					empty($type) AND $type = $this->config->item('default', 'contract_budget_payment_types');

					$contract_budget_payment_types = $this->config->item('enums', 'contract_budget_payment_types');
					$data['contract_budget_payment_type_raw'] = $contract_budget_payment_types[$type];

					return $data;
				},
				'row_data' => FALSE
			),

			/* Tùy chọn thu & chi */
			'contract_budget_customer_payment_type' => array(
				'field' => 'contract_budget_customer_payment_type',
				'func'	=> function($data, $row_name){

					if( ! empty($data['contract_budget_customer_payment_type'])) return $data;

					$this->load->config('googleads/contract');

					$type = get_term_meta_value($data['term_id'], 'contract_budget_customer_payment_type');
					empty($type) AND $type = $this->config->item('default', 'contract_budget_customer_payment_types');

					$contract_budget_customer_payment_types = $this->config->item('enums', 'contract_budget_customer_payment_types');
					$data['contract_budget_customer_payment_type_raw'] = $contract_budget_customer_payment_types[$type] ?? '';

					return $data;
				},
				'row_data' => FALSE
			),

			/* Ngân sách đã sử dụng */
			'payment_percentage' => array(
				'field' => 'payment_percentage',
				'func'	=> function($data, $row_name){

					if( ! empty($data['payment_percentage'])) return $data;
					$_contract = (new contract_m())->set_contract((object) $data);
					$data['actual_result_raw'] = $_contract->get_behaviour_m()->get_actual_result();
					return $data;
				},
				'row_data' => FALSE
			),

			/* Tiến độ công việc theo Ngân sách thực thu */
			'actual_progress_percent_net' => array(
				'field' => 'actual_progress_percent_net',
				'func'	=> function($data, $row_name){
					if( ! empty($data['actual_progress_percent_net'])) return $data;
					$data['actual_progress_percent_net_raw'] = (float) get_term_meta_value($data['term_id'], 'actual_progress_percent_net')*100;
					return $data;
				},
				'row_data' => FALSE
			),

			'actual_progress_percent' => array(
				'field' => 'actual_progress_percent',
				'func'	=> function($data, $row_name){
					if( ! empty($data['actual_progress_percent'])) return $data;
					$data['actual_progress_percent_raw'] = (float) get_term_meta_value($data['term_id'], 'actual_progress_percent');
					return $data;
				},
				'row_data' => FALSE
			),

			'real_progress' => array(
				'field' => 'real_progress',
				'func'	=> function($data, $row_name){
					if( ! empty($data['real_progress'])) return $data;
					$data['real_progress_raw'] 	= get_term_meta_value($data['term_id'],'real_progress');
					return $data;
				},
				'row_data' => FALSE
			),

			'payment_real_progress' => array(
				'field' => 'payment_real_progress',
				'func'	=> function($data, $row_name){
					if( ! empty($data['payment_real_progress'])) return $data;
					$data['payment_real_progress_raw'] = get_term_meta_value($data['term_id'], 'payment_real_progress');
					return $data;
				},
				'row_data' => FALSE
			),

			'cost_per_day_left' => array(
				'field' => 'cost_per_day_left',
				'func'	=> function($data, $row_name){
					if( ! empty($data['cost_per_day_left'])) return $data;
					try
					{
						$_contract = (new contract_m())->set_contract((object) $data);
						$data['cost_per_day_left_raw'] = $_contract->get_behaviour_m()->calc_cost_per_day_left('commit');
					} 
					catch (Exception $e) {
						$data['cost_per_day_left_raw'] = 0;
					}

					return $data;

				},
				'row_data' => FALSE
			),

			'payment_expected_end_time' => array(
				'field' => 'payment_expected_end_time',
				'func'	=> function($data, $row_name){
					if( ! empty($data['payment_expected_end_time'])) return $data;
					try
					{
						$_contract = (new contract_m())->set_contract((object) $data);
						$data['payment_expected_end_time_raw'] = $_contract->get_behaviour_m()->calc_payment_expected_end_time();
					} 
					catch (Exception $e) { $data['payment_expected_end_time_raw'] = null; }

					return $data;
				},
				'row_data' => FALSE
			),

			'expected_end_time' => array(
				'field' => 'expected_end_time',
				'func'	=> function($data, $row_name){
					if( ! empty($data['expected_end_time'])) return $data;
					try
					{
						$_contract = (new contract_m())->set_contract((object) $data);
						$data['expected_end_time_raw'] = $_contract->get_behaviour_m()->calc_expected_end_time();
					} 
					catch (Exception $e) { $data['expected_end_time_raw'] = null; }

					return $data;
				},
				'row_data' => FALSE
			),

			'result_updated_on' => array(
				'field' => 'result_updated_on',
				'func'	=> function($data, $row_name){

					if( ! empty($data['result_updated_on'])) return $data;

					$result_updated_on 			= get_term_meta_value($data['term_id'], 'result_updated_on');
					$data['result_updated_on_raw'] = $result_updated_on;
					$data['result_updated_on'] 	= $result_updated_on ? my_date($result_updated_on, 'd/m/Y') : '--';
					return $data;
				},
				'row_data' => FALSE,
			),

			'result_pending_days' => array(
				'field' => 'result_pending_days',
				'func'	=> function($data, $row_name){

					$result_updated_on = $data['result_updated_on_raw'] ?? get_term_meta_value($data['term_id'], 'result_updated_on');
					if(empty($result_updated_on)) {
                        $data['result_pending_days_raw'] 	= 0;
					    $data['result_pending_days'] 		= 0;

                        return $data;
                    }

					$days = round((time() - (int)$result_updated_on) / (60 * 60 * 24), 0);

					$data['result_pending_days_raw'] 	= $days;
					$data['result_pending_days'] 		= $days;
					return $data;
				},
				'row_data' => FALSE,
			),

			'payment_cost_per_day_left' => array(
				'field' => 'payment_cost_per_day_left',
				'func'	=> function($data, $row_name){
					if( ! empty($data['payment_cost_per_day_left'])) return $data;
					try
					{
						$_contract = (new contract_m())->set_contract((object) $data);
						$data['payment_cost_per_day_left_raw'] 	= $_contract->get_behaviour_m()->calc_cost_per_day_left('payment');
					} 
					catch (Exception $e) { $data['payment_cost_per_day_left_raw'] = 0; }
					return $data;
				},
				'row_data' => FALSE
			),

			'cid' => array(
				'field' => 'cid',
				'func'	=> function($data, $row_name){
					if( ! empty($data['cid'])) return $data;
					$mcm_client_id 	= get_term_meta_value($data['term_id'], 'mcm_client_id');
					if(empty($mcm_client_id)) 
					{
						$data['cid_raw'] = null;
						return $data;
					}

					$data['cid_raw'] = get_term_meta_value($mcm_client_id, 'customer_id');
					return $data;
				},
				'row_data' => FALSE
			),

            'service_fee_payment_type' => array(
				'field' => 'service_fee_payment_type',
				'func'	=> function($data, $row_name){
					if( ! empty($data['service_fee_payment_type'])) return $data;
					$service_fee_payment_type = get_term_meta_value($data['term_id'], 'service_fee_payment_type');
					if(empty($service_fee_payment_type)) 
					{
						$data['service_fee_payment_type_raw'] = '--';
						return $data;
					}

                    $service_fee_payment_types = $this->config->item('service_fee_payment_type');
					$data['service_fee_payment_type_raw'] = $service_fee_payment_types[$service_fee_payment_type] ?? '--';

					return $data;
				},
				'row_data' => FALSE
			),


            'latest_spend_date' => array(
                'field' => 'latest_spend_date',    
                'func'  => function($data, $row_name){

                    $data['latest_spend_date_raw'] = $data['latest_spend_date'];
                    $data['latest_spend_date'] = my_date($data['latest_spend_date'], 'd-m-Y');

                    return $data;
                },
                'row_data' => FALSE,
            ),

            'latest_spend_days' => array(
                'field' => 'latest_spend_days',    
                'func'  => function($data, $row_name){

                    $latest_spend_date = (int) ($data['latest_spend_date_raw'] ?: $data['latest_spend_date']);
                    $diff_days = round(div((time() - $latest_spend_date), 60 * 60 * 24));

                    $data['latest_spend_days'] = $diff_days;

                    return $data;
                },
                'row_data' => FALSE,
            ),
		));
	}

    /**
     * Update Insights Data
     *
     * @param      array  $args   The arguments
     *
     * @return     bolean Result
     */
    public function update_insights($args = [])
    {
        if( ! $this->contract) return FALSE;

		$this->get_accounts([
			'ignore_cache' => true,
			'start_time' => get_term_meta_value($this->contract->term_id, 'advertise_start_time') ?: start_of_day(),
			'end_time' => get_term_meta_value($this->contract->term_id, 'advertise_end_time') ?: end_of_day()
		]);

        return true;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function existed_check($value)
    {
        if (empty($value)) {
            return true;
        }
        
        $isExisted = $this->set_term_type()->where('term.term_id', (int) $value)->count_by() > 0;
        if (!$isExisted) {
            $this->form_validation->set_message('existed_check', 'Hợp đồng không tồn tại.');
            return false;
        }

        return true;
    }

    /**
     * Get insights of contract
     * 
     * @param arrray $args
     * 
     * @return array
     */
    public function get_insights($args = [])
    {
        if( ! $this->contract) return FALSE;

        $defaultArgs = [
            'summary' => false,
            'start_time' => 0,
            'end_time' => 0,
            'fields' => ['spend']
		];

        $args = array_merge($defaultArgs, $args);

        $contract_id = $this->contract->term_id;

        $insights = $this->term_posts_m
            ->join('posts AS ads_segment', "ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = '{$this->ads_segment_m->post_type}'")
            ->join('term_posts AS tp_adaccounts', 'tp_adaccounts.post_id = ads_segment.post_id')
            ->join('term AS adaccounts', "adaccounts.term_id = tp_adaccounts.term_id AND adaccounts.term_type = '{$this->mcm_account_m->term_type}'")
            ->join('term_posts AS tp_adaccount_insights', 'tp_adaccount_insights.term_id = adaccounts.term_id')
            ->join('posts AS insights', "
                tp_adaccount_insights.post_id = insights.post_id 
                AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, '%Y-%m-%d 00:00:00')) 
                AND insights.start_date <= if(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) 
                AND insights.post_type = '{$this->insight_segment_m->post_type}' 
                AND insights.post_name = 'day'
            ")
            ->join('postmeta AS insight_metadata', 'insight_metadata.post_id = insights.post_id', 'LEFT')
            
            ->where('term_posts.term_id', $contract_id)
            ->select('term_posts.term_id AS contract_id');

        if(!empty($args['start_time']))
        {
            $start_time = $this->mdate->startOfDay($args['start_time']);
            $insights->where('insights.start_date >=', $start_time);
        }

        if(!empty($args['end_time']))
        {
            $end_time = $this->mdate->endOfDay($args['end_time']);
            $insights->where('insights.start_date <=', $end_time);
        }
        
        // if($args['summary'])
        // {
        //     $args['fields'] = array_unique($args['fields']);
        //     $insights->where_in('insight_metadata.meta_key', $args['fields']);
        //     foreach($args['fields'] as $field){
        //         $insights->select('SUM(IF(insight_metadata.meta_key = "' . $field . '", insight_metadata.meta_value, NULL)) AS ' . $field);
        //     }

        //     $insights = $insights->as_array()->get_by();
        // }
        if($args['summary'])
        {
            $insights->select('`ads_segment`.`post_id` AS `segment_id`')
                ->select('`adaccounts`.`term_id` AS `adaccount_id`')
                ->select('`ads_segment`.`start_date` AS `segment_start_date`')
                ->select('`ads_segment`.`end_date` AS `segment_end_date`')
                ->group_by('`ads_segment`.`post_id`');

            $args['fields'] = array_unique($args['fields']);
            $insights->where_in('insight_metadata.meta_key', $args['fields']);
            foreach($args['fields'] as $field){
                $insights->select('SUM(IF(insight_metadata.meta_key = "' . $field . '", insight_metadata.meta_value, 0)) AS ' . $field);
            }

            $insights = $insights->as_array()->get_all();
            if(empty($insights))
            {
                $result = [];
                foreach($args['fields'] as $field)
                {
                    $result[$field] = 0;
                }

                return $result;
            }

            // Reduce unique $insights
            $insights = array_reduce($insights, function($result, $insight) {
                $segment_id = $insight['segment_id'];
                $ad_account_id = $insight['adaccount_id'];

                // Filter overlap segment
                $overlap_segment = array_filter($result, function($_segment) use ($ad_account_id, $segment_id){
                    // Ignore current segment
                    if($_segment['segment_id'] == $segment_id)
                    {
                        return FALSE;
                    }

                    return $_segment['adaccount_id'] == $ad_account_id;
                }); 
                if(empty($overlap_segment))
                {
                    $result[] = $insight;
                    return $result;
                }

                return $result;
            }, []);
            
            $result = [];
            foreach($args['fields'] as $field)
            {
                $result[$field] = round(array_sum(array_column($insights, $field)));
            }

            return $result;
        }
        else 
        {
            $args['fields'] = array_unique($args['fields']);
            $insights->where_in('insight_metadata.meta_key', $args['fields']);
            foreach($args['fields'] as $field){
                $insights->select('MAX(IF(insight_metadata.meta_key = "' . $field . '", insight_metadata.meta_value, NULL)) AS ' . $field);
            }

            $insights->select('
                insights.post_id AS insight_id,
                adaccounts.term_name AS ad_account_id,
                insights.start_date AS date_time
            ')
            ->group_by('ads_segment.post_id, adaccounts.term_id, insights.post_id');
            $insights = $insights->as_array()->get_all();
        }

        return $insights;
    }
}
/* End of file Googleads_m.php */
/* Location: ./application/modules/googleads/models/Googleads_m.php */