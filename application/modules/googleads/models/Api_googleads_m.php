<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_googleads_m extends Base_model 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('api_user_m');
        $this->load->model('googleads/Base_adwords_m');
        $this->load->model('googleads/googleads_m');
    }

    function get_message_by($term_id = FALSE,$token = '')
    {
        $this->load->model('message/message_m'); 

        $user_id = $this->api_user_m->user_id;
        $auser_phone = $this->api_user_m->auser_phone;

        $data = $this->message_m->get_all_by($term_id,$user_id,$auser_phone);
        return $data;
    }

    /**
     * Gets the contract list by.
     *
     * @param      string  $token  The token
     *
     * @return     array   The contract list by.
     */
    function get_contract_list_by($token = '')
    {
        $data   = array();
        $token  = $token ?: $this->api_user_m->auser_token ?? '';

        $services = array();
        $term_ids = array();

        # CHECK CUSTOMER_ID AND QUERY ALL CONTRACTS
        $customer_ids   = !empty($this->api_user_m->user_id) ? unserialize($this->api_user_m->user_id) : NULL;
        if(!empty($customer_ids) && is_array($customer_ids))
        {
            $this->load->model('term_users_m');
            $this->config->load('contract/contract');
            $taxonomies_def = array_keys( $this->config->item('taxonomy') );

            foreach ($customer_ids as $customer_id)
            {
                $customer_id = abs($customer_id);
                $_terms      = $this->term_users_m->get_user_terms($customer_id, $taxonomies_def, ['where_in'=>['term.term_status'=>['pending','publish','ending','liquidation']]]);

                if(empty($_terms)) continue;

                $term_ids = array_merge($term_ids, array_column($_terms, 'term_id'));
            }
        }

        # QUERY ALL CONTRACT WITH PHONE SETTING WITHIN
        if( ! empty($this->api_user_m->auser_phone)
            && $termmetas = $this->termmeta_m->select('term_id')
            ->where('meta_key', 'contract_curators')->like('meta_value', $this->api_user_m->auser_phone)->get_all())
        {
            $term_ids = array_unique(array_merge($term_ids, array_column($termmetas, 'term_id')));
        }

        # NO RESULT
        if(empty($term_ids)) return FALSE;

        # SECTION FILTER ALL ACTIVE CONTRACT
        $this->load->model('googleads/googleads_m');
        $terms = $this->googleads_m->select('term_id,term_name,term_type,term_status')->set_term_type()
        ->where_in('term_status', ['pending','publish','ending','liquidation'])
        ->where_in('term_id', $term_ids)
        ->order_by('term_id', 'desc')
        ->get_all();

        # NO RESULT FILTERED
        if(empty($terms)) return FALSE;
        
        foreach ($terms as $term)
        {
            $term_id    = $term->term_id;
            $status     = 0;

            $description    = 'HĐ đã hết hạn';
            $contract_code  = get_term_meta_value($term_id, 'contract_code');

            $service_type = get_term_meta_value($term_id, 'service_type');
            if($service_type != 'account_type') continue;

            $network_type = get_term_meta_value($term_id, 'network_type');
            if($network_type == 'facebook') continue;

            switch ($term->term_status) 
            {
                case 'ending':
                    $status      = 2;
                    $description = 'HĐ bị khóa';
                    break;

                case 'liquidation':
                    $status      = 0;
                    $description = 'HĐ đã hết hạn';
                    break;
                
                default:
                    $status      = 1;
                    $description = 'HĐ còn hạn';
                    break;
            }

            
            $currency_code = get_term_meta_value($term_id,'account_currency_code');
            $meta_key = strtolower("exchange_rate_{$currency_code}_to_vnd");
            $exchange_rate   = get_term_meta_value($term_id, $meta_key);
            
            $contract_budget        = get_term_meta_value($term_id, 'actual_budget');
            $started_service        = get_term_meta_value($term_id, 'started_service');
            
            $start_service_time     = get_term_meta_value($term_id, 'start_service_time');
            $expected_end_time      = $this->googleads_m->calc_expected_end_time($term_id);
            $contract_begin         = get_term_meta_value($term_id, 'contract_begin');
            $contract_end           = get_term_meta_value($term_id, 'contract_end');

            $time_contract = 0;
            if(!empty($contract_begin) && !empty($contract_end))
            {
                $time_contract = $contract_end - $contract_begin;
            }

            $committed_end_time = $start_service_time + $time_contract;
            $domain             = mb_strtolower(trim($term->term_name));

            $data[$term_id] = array(

                'id'     => $term_id,
                'name'   => "{$domain}-HĐ{$term_id}",
                'status' => $status,
                'domain' => $domain,

                'description'       => $description,
                'contract_code'     => $contract_code,
                'contract_budget'   => $contract_budget,

                $meta_key               => $exchange_rate,
                'started_service'       => $started_service,
                'expected_end_time'     => $expected_end_time,
                'start_service_time'    => $start_service_time,
                'committed_end_time'    => $committed_end_time
            );
        }

        return $data;
    }

    function get_tasks_by($term_id)
    {
    	$data = array();

    	switch ($term_id) 
    	{
    		case 1:
    			# code...
    			break;
    		
    		case 2:
				# code...
    			break;
    	}

    	return $data;
    }

    function get_contract_info($term_id=0,$token='')
    {
        $data = array('sale'=>array(),'tech'=>array());

        $term = $this->term_m->get($term_id);
        if(!$term) return $data;

        $sale = array(
            'name' => 'Triệu Minh Hải',
            'phone' => '0972899723',
            'email' => 'haitm@webdoctor.vn'
        );

        if($staff_business = get_term_meta_value($term_id,'staff_business'))
        {
            if($phone = $this->usermeta_m->get_meta_value($staff_business, 'user_phone'))
            {
                $sale['phone'] = $this->usermeta_m->get_meta_value($staff_business, 'user_phone');
            }
            
            if($display_name = $this->admin_m->get_field_by_id($staff_business, 'display_name'))
            {
                $sale['name'] = $display_name ?: 'NVKD';
            }

            if($email = $this->admin_m->get_field_by_id($staff_business, 'user_email'))
            {
                $sale['email'] = $email;
            }
        }

        $data['sale'] = $sale;


        $tech = array(
            'name' => 'Triệu Minh Hải',
            'phone' => '0972899723',
            'email' => 'haitm@webdoctor.vn'
        );

        $this->load->model('googleads/googleads_kpi_m');
        if($kpi = $this->googleads_kpi_m->order_by('kpi_type')->where('term_id',$term_id)->get_by())
        {
            if($admin = $this->admin_m->set_get_admin()->limit(1)->get($kpi->user_id))
            {
                if($phone = $this->usermeta_m->get_meta_value($admin->user_id, 'user_phone'))
                {
                    $tech['phone'] = $this->usermeta_m->get_meta_value($admin->user_id, 'user_phone');
                }
                
                if($display_name = $this->admin_m->get_field_by_id($admin->user_id, 'display_name'))
                {
                    $tech['name'] = $display_name ?: 'Kỹ Thuật';
                }

                if($email = $this->admin_m->get_field_by_id($admin->user_id, 'user_email'))
                {
                    $tech['email'] = $email;
                }
            }
        }

        $data['tech'] = $tech;

        return $data;
    }
}