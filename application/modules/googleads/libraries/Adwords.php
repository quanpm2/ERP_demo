<?php  
if (!defined('BASEPATH')) exit('No direct script access allowed');

define('SRC_PATH', APPPATH.'/third_party/google-adwords/libraries/');

define('LIB_PATH', 'AdWords/Lib');

define('UTIL_PATH', 'Common/Util');

define('AW_UTIL_PATH', 'AdWords/Util');

define('ADWORDS_VERSION', 'v201809');

// Configure include path
ini_set('include_path', implode(array(ini_get('include_path'), PATH_SEPARATOR, SRC_PATH)));

// Include the AdWordsUser file
require_once SRC_PATH.LIB_PATH. '/AdWordsUser.php';

require_once AW_UTIL_PATH . '/ReportUtils.php';

class Adwords extends AdWordsUser {

  public function __construct() { 
    
    parent::__construct();
  }   

  function GetCampaigns(){

  // Get the service, which loads the required classes.
  $campaignService = $this->GetService('CampaignService', ADWORDS_VERSION);

  // Create selector.
  $selector = new Selector();

  $selector->fields = array('Id', 'Name','EndDate','StartDate');

  $selector->ordering[] = new OrderBy('Name', 'ASCENDING');

  // Create paging controls.
  $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

  return $campaignService->get($selector);
  }  

  function GetAdGroups($campaignId) {
  // Get the service, which loads the required classes.
  $adGroupService =  $this->GetService('AdGroupService', ADWORDS_VERSION);

  // Create selector.
  $selector = new Selector();
  $selector->fields = array('Id', 'Name');
  $selector->ordering[] = new OrderBy('Name', 'ASCENDING');

  // Create predicates.
  $selector->predicates[] =
  new Predicate('CampaignId', 'IN', array($campaignId));

  // Create paging controls.
  $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

  do {
  // Make the get request.
  $page = $adGroupService->get($selector);

  // Display results.
  if (isset($page->entries)) { 
  return $page->entries;
  } else {
  return ;
  }

  // Advance the paging index.
  $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
  } while ($page->totalNumEntries > $selector->paging->startIndex);

  }

  function GetKeywords($adGroupId) {
  // Get the service, which loads the required classes.
  $adGroupCriterionService =
  $this->GetService('AdGroupCriterionService', ADWORDS_VERSION);

  // Create selector.
  $selector = new Selector();
  $selector->fields = array('KeywordText', 'KeywordMatchType', 'Id');
  $selector->ordering[] = new OrderBy('KeywordText', 'ASCENDING');

  // Create predicates.
  $selector->predicates[] = new Predicate('AdGroupId', 'IN', array($adGroupId));
  $selector->predicates[] =
  new Predicate('CriteriaType', 'IN', array('KEYWORD'));

  // Create paging controls.
  $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

  do {
  // Make the get request.
  $page = $adGroupCriterionService->get($selector);

  // Display results.
  if (isset($page->entries)) {
  return $page->entries;
  // foreach ($page->entries as $adGroupCriterion) {
  // printf("Keyword with text '%s', match type '%s', and ID '%s' was "
  //     . "found.\n", $adGroupCriterion->criterion->text,
  //     $adGroupCriterion->criterion->matchType,
  //     $adGroupCriterion->criterion->id);
  // }
  } else {
  return;
  }

  // Advance the paging index.
  $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
  } while ($page->totalNumEntries > $selector->paging->startIndex);
  }

  function GetKeywordIdeas($keyword) {

  // Get the service, which loads the required classes.
  $targetingIdeaService = $this->GetService('TargetingIdeaService', ADWORDS_VERSION);


  // Create selector.
  $selector = new TargetingIdeaSelector();

  $selector->requestType = 'STATS';

  $selector->ideaType = 'KEYWORD';

  $selector->requestedAttributeTypes = array('KEYWORD_TEXT', 'SEARCH_VOLUME',
  'CATEGORY_PRODUCTS_AND_SERVICES','COMPETITION','AVERAGE_CPC');


  // Create language search parameter (optional).
  // The ID can be found in the documentation:
  //   https://developers.google.com/adwords/api/docs/appendix/languagecodes
  // Note: As of v201302, only a single language parameter is allowed.
  $languageParameter = new LanguageSearchParameter();

  $vietnamese = new Language();

  $vietnamese->id = 1040;

  $languageParameter->languages = array($vietnamese);


  // Create related to query search parameter.
  $relatedToQuerySearchParameter = new RelatedToQuerySearchParameter();

  $relatedToQuerySearchParameter->queries = $keyword;

  $selector->searchParameters[] = $relatedToQuerySearchParameter;

  $selector->searchParameters[] = $languageParameter;


  // Set selector paging (required by this service).
  $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

  do {

  // Make the get request.
  $page = $targetingIdeaService->get($selector);

  // Display results.
  if (isset($page->entries)) {

  $result = array();

  foreach ($page->entries as $targetingIdea) {

  $data = MapUtils::GetMap($targetingIdea->data);

  $keyword = $data['KEYWORD_TEXT']->value;

  $search_volume = isset($data['SEARCH_VOLUME']->value) ? $data['SEARCH_VOLUME']->value : 0;

  // $categoryIds =
  //     implode(', ', $data['CATEGORY_PRODUCTS_AND_SERVICES']->value);

  $compete = isset($data['COMPETITION']->value) ? $data['COMPETITION']->value : 0;

  $average_cpc = isset($data['AVERAGE_CPC']->value) ? $data['AVERAGE_CPC']->value->microAmount  : 0;

  $result[] = (object) array('name'=>$keyword,'search_volume'=>$search_volume,'compete'=>$compete,'average_cpc'=>($average_cpc/ 1000000));  
  }

  return $result;
  } 
  else {

  return ;
  }

  // Advance the paging index.
  $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;

  } 
  while ($page->totalNumEntries > $selector->paging->startIndex);
  }

  function full_map_keywords($adGroupId){

  $cache_name = 'myadwords.full_map_keywords.'.$adGroupId;

  $CI = & get_instance();

  $CI->load->model('Scache','scache');

  $result = $CI->scache->get($cache_name);

  if(!$result){

  $result = array();

  $adgroups = $this->GetAdGroups($adGroupId);

  $seo_keywords = array();

  if($adgroups){

  foreach ($adgroups as $grp) {

  $key_w = $this->GetKeywords($grp->id);

  $keywords = array();

  if($key_w){

  foreach ($key_w as $k) {

  $keywords[] = $k->criterion->text;
  }

  $keywords = $this->GetKeywordIdeas($keywords);

  $row_keywords = array();

  foreach ($keywords as $k) {

  $k->groupname = $grp->name;

  $row_keywords[] = array($k->name,$k->search_volume,$k->compete,$k->average_cpc,$grp->name);
  }

  $result[] = array('group_name'=>$grp->name,'keywords' => $row_keywords);
  } 

  }

  $CI->scache->write($result, $cache_name, 24*60*60);
  }
  }

  return $result;
  } 

  public function lookup_location($atts = array('critetiaIds'=>array(),'locationName' => array())) {

  extract($atts);

  if(empty($atts) || (empty($critetiaIds) && empty($locationName))) return;

  $locationCriterionService = $this->GetService('LocationCriterionService', ADWORDS_VERSION);

  $selector = new Selector();

  $selector->fields = array('Id', 'LocationName', 'CanonicalName', 'DisplayType','ParentLocations','Reach','TargetingStatus');

  if(!empty($critetiaIds)){

  $selector->predicates[] = new Predicate('Id', 'IN', $critetiaIds);
  }

  if(!empty($LocationName)){

  $selector->predicates[] = new Predicate('LocationName', 'IN', $locationName);
  }

  $selector->predicates[] = new Predicate('Locale', 'EQUALS', 'vi');

  $locationCriteria = $locationCriterionService->get($selector);

  $out = array();

  if (isset($locationCriteria)) {

  foreach ($locationCriteria as $locationCriterion) {

  $out[$locationCriterion->location->id] = $locationCriterion->location->locationName;
  }
  }

  return $out;
  }
}