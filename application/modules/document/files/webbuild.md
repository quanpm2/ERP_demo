WEBBUILD
===============

*webbuild*

URL API:

	GET /webbuild/


####Link: 
[http://erp.webdoctor.vn/api/webbuild/?token=ab123456789012345678901234567890](http://erp.webdoctor.vn/api/webbuild/?token=ab123456789012345678901234567890)

#list danh danh sách hợp đồng

URL API:

  GET /webbuild/list?token=ab123456789012345678901234567890

#### Parameters
No

#### Response
Trả về danh sách các hợp đồng mà KH này đang sử dụng

``` json
{
  "status": 1,
  "lists": {
    "11": {
      "id": 11,
      "name": "webdoctor.vn",
      "status": 1,
      "description": "HĐ sắp hết hạn"
    }
  }
}
```
Chú thích về `lists` được trả về

- `id`: Mã hợp đồng
- `name`: Tên, tiêu đề HĐ
- `status`: Trạng thái, `0`: hết hạn (sẽ mờ đi trong app), `1`: còn hạn, `2`: Lock không cho xem

# Lấy thông tin liên hệ của kỹ thuật và kinh doanh

URL API:

  POST /webbuild/contract_info?token=token_key

#### Parameters
`id` : Mã hợp đồng

#### Response
Trả về danh sách các hợp đồng ADSPLUS mà KH này đang sử dụng

``` json
{
  "status": 1,
  "data": {
    "sale": {
      "name": "hotline",
      "phone": "0873004488",
      "email": "haitm@webdoctor.vn"
    },
    "tech": {
      "name": "Triệu Minh Hải",
      "phone": "0873004488",
      "email": "haitm@webdoctor.vn"
    }
  }
}
```
- `status`: Trạng thái xử lý của server, `1`: xử lý thành công , `0`: chưa thành công

Chú thích về `data` được trả về

- `sale`: Thông tin liên hệ của kinh doanh
  - `name`: Họ và tên
  - `phone`: Số điện thoại liên hệ
  - `email` : Email liên hệ

- `tech`: Thông tin liên hệ của kỹ thuật
  - `name`: Họ và tên
  - `phone`: Số điện thoại liên hệ
  - `email` : Email liên hệ