ADSPLUS
===============

*ADSPLUS*

URL API:

	GET /googleads/


####Link: 
[http://erp.webdoctor.vn/api/googleads/?token=ab123456789012345678901234567890](http://erp.webdoctor.vn/api/googleads/?token=ab123456789012345678901234567890)

#list danh sách hợp đồng

URL API:

	GET /googleads/list?token=token_key

#### Response
Trả về danh sách các hợp đồng ADSPLUS mà KH này đang sử dụng

``` json
{
"status": "1",
"lists" {
		"1": {
			"id" : "1",
      "name" : "example1.com",
			"domain" : "example1.com",
			"status" : "1",
			"description" : "HĐ sắp hết hạn",
			"contract_code": "ADSPLUS/495/0516/DIENHOA38DO.COM",
			"contract_budget": "3000000",
			"dollar_exchange_rate": "",
			"started_service": "1465007650",
			"expected_end_time": 1464825600,
			"start_service_time": "1465263829",
			"committed_end_time": 1467769429
		},
		... 
	}
}
```
Chú thích về `lists` được trả về

- `id`: Mã hợp đồng
- `name`: Tên, tiêu đề HĐ
- `status`: Trạng thái, `0`: hết hạn (sẽ mờ đi trong app), `1`: còn hạn, `2`: Lock không cho xem
- `description` : Mô tả hợp đồng
- `contract_code` : Mã hợp đồng
- `contract_budget` : Ngân sách,
- `dollar_exchange_rate` : Tỉ giá quy đổi ngoại tệ thành việt nam đồng,
- `started_service` : Ngày thực hiện chiến dịch,
- `expected_end_time` : Ngày dự kiến kết thúc,
- `start_service_time` : Ngày chạy chiến dịch,
- `committed_end_time` : Ngày kết thúc


# Lấy thông tin liên hệ của kỹ thuật và kinh doanh

URL API:

	POST /googleads/contract_info?token=token_key

#### Parameters
`id` : Mã hợp đồng

#### Response
Trả về danh sách các hợp đồng ADSPLUS mà KH này đang sử dụng

``` json
{
  "status": 1,
  "data": {
    "sale": {
      "name": "hotline",
      "phone": "0873004488",
      "email": "haitm@webdoctor.vn"
    },
    "tech": {
      "name": "Triệu Minh Hải",
      "phone": "0873004488",
      "email": "haitm@webdoctor.vn"
    }
  }
}
```
- `status`: Trạng thái xử lý của server, `1`: xử lý thành công , `0`: chưa thành công

Chú thích về `data` được trả về

- `sale`: Thông tin liên hệ của kinh doanh
	- `name`: Họ và tên
	- `phone`: Số điện thoại liên hệ
	- `email` : Email liên hệ

- `tech`: Thông tin liên hệ của kỹ thuật
	- `name`: Họ và tên
	- `phone`: Số điện thoại liên hệ
	- `email` : Email liên hệ

=======================================================================
# Số liệu tổng của tài khoản chạy quảng cáo

URL API:

	POST /googleads/getStatistic/{$MA_HOP_DONG}/ACCOUNT_PERFORMANCE_REPORT/?token=token_key

#### Parameters
`{$MA_HOP_DONG}` : Mã hợp đồng

#### Response
Trả về mảng dữ liệu của chiến dịch quảng cáo

``` json
{
  "status": 0,
  "data": [
    {
      "currency": "VND",
      "account": "dienhoa38do.com",
      "impressions": "1369",
      "clicks": "192",
      "invalidClicks": "8",
      "avgCPC": "8520317708",
      "avgPosition": "2.6",
      "cost": "1635901000000",
      "conversions": "3.0",
      "allConv": "3.0",
      "network": "Search Network",
      "networkWithSearchPartners": "Google search"
    },
    ...
  ]
}
```
- `status`: Trạng thái xử lý của server, `1`: xử lý thành công , `0`: chưa thành công

Chú thích về `data` được trả về

- `currency` : Đơn vị tiền tệ , nếu là ngoại tệ bắt buộc phải tính ra tiền Việt (tỉ giá quy đổi lấy từ thông tin hợp đồng (dollar_exchange_rate)),
	- `VND`: Việt Nam đồng
	- `USD`: đô-la

- `account` : tên chiến dịch,
- `impressions` : số lượt hiển thị,
- `clicks` : số lượng clicks,
- `invalidClicks` : Số lượng từ khóa không hợp lệ,
- `avgCPC` : Số tiền trung bình mỗi click,
- `avgPosition` : Vị trí xuất hiện trung bình,
- `cost` : Số tiền ( Bắt buộc phải chia cho 1000000 để lấy giá trị thật) ,
- `conversions` : Chuyển đổi,
- `allConv` : Tất cả chuyển đổi,
- `network` : Search Network,
- `networkWithSearchPartners` : networkWithSearchPartners


=======================================================================
# Số liệu của tài khoản chạy quảng cáo từ ngày đến ngày

URL API:

	POST /googleads/getStatistic/{$MA_HOP_DONG}/ACCOUNT_PERFORMANCE_REPORT/{$start_time}/{$end_time}/?token={$token_key}&group_by_date=1

#### Parameters
`{$MA_HOP_DONG}` : Mã hợp đồng,
`{$start_time}` : thời gian bắt đầu (Int),
`{$end_time}` : thời gian kết thúc (Int),
`{token_key}` : mã token

`Note` : set {$start_time} và {$end_time} = 0 nếu lấy tất cả dữ liệu

#### Response
Trả về mảng dữ liệu của chiến dịch quảng cáo

``` json
{
  "auth": 1,
  "status": 0,
  "data": {
    "1489276800": false,
    "1489449600": [
      {
        "currency": "VND",
        "account": "dienhoa38do.com",
        "day": "2017-03-14",
        "impressions": "254",
        "clicks": "23",
        "invalidClicks": "0",
        "avgCPC": "8599739130",
        "avgPosition": "3.0",
        "cost": "197794000000",
        "conversions": "0.0",
        "allConv": "0.0",
        "network": "Search Network",
        "networkWithSearchPartners": "Google search"
      },
      {
        "currency": "VND",
        "account": "dienhoa38do.com",
        "day": "2017-03-14",
        "impressions": "1",
        "clicks": "0",
        "invalidClicks": "0",
        "avgCPC": "0",
        "avgPosition": "3.0",
        "cost": "0",
        "conversions": "0.0",
        "allConv": "0.0",
        "network": "Search Network",
        "networkWithSearchPartners": "Search partners"
      },
      ...
    ],
    ...
  }
}
```
- `status`: Trạng thái xử lý của server, `1`: xử lý thành công , `0`: chưa thành công

Chú thích về `data` được trả về

- `key` : thời gian (int) , là một ngày cụ thể
- `value` : dữ liệu trong ngày
	- `currency` : Đơn vị tiền tệ , nếu là ngoại tệ bắt buộc phải tính ra tiền Việt (tỉ giá quy đổi lấy từ thông tin hợp đồng (dollar_exchange_rate)),
		- `VND`: Việt Nam đồng
		- `USD`: đô-la

	- `account` : tên chiến dịch,
	- `impressions` : số lượt hiển thị,
	- `clicks` : số lượng clicks,
	- `invalidClicks` : Số lượng từ khóa không hợp lệ,
	- `avgCPC` : Số tiền trung bình mỗi click,
	- `avgPosition` : Vị trí xuất hiện trung bình,
	- `cost` : Số tiền ( Bắt buộc phải chia cho 1000000 để lấy giá trị thật) ,
	- `conversions` : Chuyển đổi,
	- `allConv` : Tất cả chuyển đổi,
	- `network` : Search Network,
	- `networkWithSearchPartners` : networkWithSearchPartners


=======================================================================
# Số liệu của các từ khóa chạy quảng cáo từ ngày đến ngày

URL API:

	POST /googleads/getStatistic/{$MA_HOP_DONG}/KEYWORDS_PERFORMANCE_REPORT/{$start_time}/{$end_time}/?token={$token_key}

#### Parameters
`{$MA_HOP_DONG}` : Mã hợp đồng,
`{$start_time}` : thời gian bắt đầu (Int),
`{$end_time}` : thời gian kết thúc (Int),
`{token_key}` : mã token

`Note` : set {$start_time} và {$end_time} = 0 nếu lấy tất cả dữ liệu

#### Response
Trả về mảng dữ liệu của chiến dịch quảng cáo

``` json
{
  "auth": 1,
  "status": 0,
  "data": {
    "1489276800": [
      {
        "currency": "VND",
        "account": "dienhoa38do.com",
        "timeZone": "(GMT+07:00) Hanoi",
        "adGroupID": "34966343998",
        "adGroup": "hoa đám tang",
        "adGroupState": "enabled",
        "network": "Search Network",
        "networkWithSearchPartners": "Google search",
        "approvalStatus": "approved",
        "avgCost": "0",
        "avgCPC": "0",
        "avgPosition": "0.0",
        "avgSessionDurationSeconds": "0",
        "bounceRate": "100.00%",
        "campaignID": "669259916",
        "campaign": "Hoa Viếng",
        "campaignState": "enabled",
        "clicks": "0",
        "cost": "0",
        "keyword": "hoa đám tang",
        "ctr": "0.00%",
        "day": "2017-03-12",
        "hasQualityScore": "true",
        "keywordID": "39481577347",
        "impressions": "0",
        "isNegative": "false",
        "matchType": "Phrase",
        "qualityScore": "8",
        "keywordState": "enabled",
        "firstPageCPC": "980000000",
        "topOfPageCPC": "3480000000",
        "conversions": "0.0"
      },
      ...
    ],
    ...
  }
}
```
- `status`: Trạng thái xử lý của server, `1`: xử lý thành công , `0`: chưa thành công

Chú thích về `data` được trả về

- `key` : thời gian (int) , là một ngày cụ thể
- `value` : dữ liệu trong ngày
	
	- `currency` : Đơn vị tiền tệ , nếu là ngoại tệ bắt buộc phải tính ra tiền Việt (tỉ giá quy đổi lấy từ thông tin hợp đồng (dollar_exchange_rate)),
		- `VND`: Việt Nam đồng
		- `USD`: đô-la

	- `account` : tên chiến dịch,
	- `timeZone` : "(GMT+07:00) Hanoi",
	- `adGroupID` : "34966343998",
	- `adGroup` : "hoa đám tang",
	- `adGroupState` : "enabled",
	- `network` : "Search Network",
	- `networkWithSearchPartners` : "Google search",
	- `approvalStatus` : "approved",
	- `avgCost` : Giá trị trung bình ( Bắt buộc phải chia cho 1000000 để lấy giá trị thật),
	- `avgCPC` : Trung bình số tiền mỗi click ( Bắt buộc phải chia cho 1000000 để lấy giá trị thật),
	- `avgPosition` : vị trí xuất ,
	- `avgSessionDurationSeconds` : "0",
	- `bounceRate` : "100.00%",
	- `campaignID` : "669259916",
	- `campaign` : "Hoa Viếng",
	- `campaignState` : "enabled",
	- `clicks` : "0",
	- `cost` : "0",
	- `keyword` : "hoa đám tang",
	- `ctr` : "0.00%",
	- `day` : "2017-03-12",
	- `hasQualityScore` : "true",
	- `keywordID` : "39481577347",
	- `impressions` : "0",
	- `isNegative` : "false",
	- `matchType` : "Phrase",
	- `qualityScore` : "8",
	- `keywordState` : "enabled",
	- `firstPageCPC` : Số tiền ( Bắt buộc phải chia cho 1000000 để lấy giá trị thật),
	- `topOfPageCPC` : Số tiền ( Bắt buộc phải chia cho 1000000 để lấy giá trị thật),
	- `conversions` : "0.0"