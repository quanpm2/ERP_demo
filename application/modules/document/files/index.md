Giới thiệu tổng quan
===============

*Tổng quan về API*

Mọi hoạt động về thao tác với ERP đều được thông qua API
Mặc định URL của API ERP là: 

	http://erp.webdoctor.vn/api/

Tham số mặc định khi gọi API đều phải gửi kèm pragma

	GET /?token = ABC

`token`: có độ dài 32 ký tự

##Response
	
	Status: 200 OK

``` json
{
"auth": "1",
.... data
}
```
`auth`:

- `0`: Bắt đăng nhập lại
- `1`: Token còn hạn sử dụng
