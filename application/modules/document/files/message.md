
# GET nội dung chi tiết thông báo

URL API:

  POST /customer/message_view?token=token_key

####Link: 
[http://erp.webdoctor.vn/api/customer/message_view?token=ab123456789012345678901234567890](http://erp.webdoctor.vn/api/customer/message_view/?token=ab123456789012345678901234567890)

#### Parameters
`id` : Mã tin

#### Response
Trả về dữ liệu của tin 

``` json
{
"status": "1",
"data" : {
    "id" : "1",
    "type" : "adsplus",
    "msg" : "message",
    "msg_type" : "message",
    "task_status" : "1",
    "title" : "title",
    "content" : "Nội dung tin",
    "icon" : "link",
    "status" : "1",
    "day" : "1454042799",
    "contract_id" : "1",
    "metadata" : {
          "sheets" : {
            "campaign": {
              "headings" : {"Date","Campaign","Impressions","Clicks","Invalid Clicks","CTR","Avg.Position","Campaign Status","Avg.CPC","Cost"} ,
              "rows" : {
                  {"18/10/2016","xxx.COM","197","11","0","5.58%","3.2","enabled","10.718,09","117.899,00"},
                  ...
                }
              },
            "adgroup": {
              "headings" : {"Date","Campaign","Impressions","Clicks","Invalid Clicks","CTR","Avg.Position","Campaign Status","Avg.CPC","Cost"} ,
              "rows" : {
                {"18/10/2016","xxx.COM","197","11","0","5.58%","3.2","enabled","10.718,09","117.899,00"},
                ...
              }
              },
            "keyword": {
              "headings" : {"Date","Campaign","Impressions","Clicks","Invalid Clicks","CTR","Avg.Position","Campaign Status","Avg.CPC","Cost"} ,
              "rows" : {
                  {"18/10/2016","xxx.COM","197","11","0","5.58%","3.2","enabled","10.718,09","117.899,00"},
                  ...
                }
              },
            "geo": {
              "headings" : {"Date","Campaign","Impressions","Clicks","Invalid Clicks","CTR","Avg.Position","Campaign Status","Avg.CPC","Cost"} ,
              "rows" : {
                  {"18/10/2016","xxx.COM","197","11","0","5.58%","3.2","enabled","10.718,09","117.899,00"},
                  ...
              }
            }
          }
  }
}
```
- `status`: Trạng thái xử lý của server, `1`: xử lý thành công , `0`: chưa thành công

Chú thích về `data` được trả về

- `id`: mã tin thông báo
- `type`: Loại hợp đồng, `adsplus`: dịch vụ ADSPLUS, `webgeneral`: dịch vụ ADSPLUS, `hosting`: dịch vụ chăm sóc web, `webbuild`: dịch vụ thiết kế web
- `msg`: tin thông báo
- `msg_type`: loại tin thông báo `message`: tin thông báo, `task`: đầu việc thực hiện
- `task_status`: Trạng thái của công việc (áp dụng cho msg_type 'task' của dịch vụ thiết kế web)  `0`: Chờ, `1`: đã thực hiện ,`2`: đang thực hiện
- `title`: Tiêu đề
- `content`: Nội dung text của tin thông báo
- `icon`: icon-link
- `status`: Trạng thái, `0`: Chưa xem, `1`: Đã xem
- `day`: Timestamp (thời gian gửi tin)
- `contract_id`: Id hợp đồng
- `metadata` : chứa dữ liệu cần hiển thị thêm dành cho tin thông báo

Chú thích về `metadata` được trả về

- `sheets` : dữ liệu quảng cáo adsplus ( dành cho báo cáo tuần cần hiển thị dữ liệu keyword,campaign,adgroup và geo)
  - `campaign` : dữ liệu tất cả các campaign
    - `headings` : dữ liệu tiều đề các column trong sheet campaign
    - `rows` : chứa các dòng dữ liệu tương ứng với headings

  - `adgroup` : dữ liệu tất cả các adgroup
    - `headings` : dữ liệu tiều đề các column trong sheet adgroup
    - `rows` : chứa các dòng dữ liệu tương ứng với headings

  - `keyword` : dữ liệu tất cả các keyword
    - `headings` : dữ liệu tiều đề các column trong sheet keyword
    - `rows` : chứa các dòng dữ liệu tương ứng với headings

  - `geo` : dữ liệu tất cả các campaign
    - `headings` : dữ liệu tiều đề các column trong sheet geo
    - `rows` : chứa các dòng dữ liệu tương ứng với headings