MESSAGES
===============

URL API:

  POST /customer/messages?token=tokey-key


####Link: 
[http://erp.webdoctor.vn/api/googleads/messages?token=token_key](http://erp.webdoctor.vn/api/customer/messages/?token=ab123456789012345678901234567890)

#### Parameters
No

#### Response
Trả về danh sách tất cả các tin thông báo dịch vụ

``` json
{
"status": "1",
"data" : {
  "lists" {
      "1": {
        "id" : "1",
        "type" : "adsplus",
        "msg" : "message",
        "msg_type" : "message",
        "task_status" : "1",
        "title" : "title",
        "icon" : "link",
        "status" : "1",
        "day" : "1454042799",
        "contract_id" : "1",
      },
      ...
    },
  "next_time" : '11111111'
  }
}
```
Chú thích về `data` được trả về

- `lists`: Chứa tất cả thông báo của user
- `next_time`: Thời gian tiếp theo có thể lấy dữ liệu

Chú thích về `lists` được trả về

- `id`: mã tin thông báo
- `type`: Loại hợp đồng, `adsplus`: dịch vụ ADSPLUS, `webgeneral`: dịch vụ chăm sóc web, `hosting`: dịch vụ hosting,`domain`: dịch vụ domain, `webbuild`: dịch vụ thiết kế web
- `msg`: tin thông báo
- `msg_type`: loại tin thông báo `message`: tin thông báo, `task`: đầu việc thực hiện
- `task_status`: Trạng thái của công việc (áp dụng cho msg_type 'task' của dịch vụ thiết kế web)  `0`: Chờ, `1`: đã thực hiện ,`2`: đang thực hiện
- `title`: Tiêu đề
- `icon`: <empty>
- `status`: Trạng thái, `0`: Chưa xem, `1`: Đã xem
- `day`: Timestamp (thời gian gửi tin)
- `contract_id`: Id hợp đồng,

MESSAGES (Sample data)
===============

URL API:

  POST /customer/messages?token=ab123456789012345678901234567890

#### Parameters
No

#### Response
``` json
{
  "status": 1,
  "data": {
    "lists": {
      "1": {
        "id": "1",
        "type": "adsplus",
        "msg": "Thống kê lưu lượng click clicks ngày 1/10/2016",
        "msg_type": "message",
        "task_status": "1",
        "title": "Thống kê lưu lượng click clicks ngày 1/10/2016",
        "icon": "",
        "status": "1",
        "day": 1454042799,
        "contract_id": 1081
      },
      "2": {
        "id": "2",
        "type": "adsplus",
        "msg": "Báo cáo tuần (18/10/2016 đến 24/10/2016)",
        "msg_type": "message",
        "task_status": "1",
        "title": "Báo cáo tuần (18/10/2016 đến 24/10/2016)",
        "icon": "",
        "status": "0",
        "day": 1454032799,
        "contract_id": 1081
      },
      "3": {
        "id": "3",
        "type": "adsplus",
        "msg": "Hợp đồng hết hạn",
        "msg_type": "message",
        "task_status": "1",
        "title": "Hợp đồng hết hạn",
        "icon": "",
        "status": "1",
        "day": 1454042799,
        "metadata": 0,
        "contract_id": 1086
      },
      "4": {
        "id": "4",
        "type": "webgeneral",
        "msg": "báo cáo Web tổng hợp ngày 26/10/2016 09:00:03",
        "msg_type": "message",
        "task_status": "1",
        "title": "báo cáo Web tổng hợp ngày 26/10/2016 09:00:03",
        "icon": "",
        "link": "",
        "status": "1",
        "day": 1454042799,
        "metadata": 0,
        "contract_id": 1
      },
      "5": {
        "id": "5",
        "type": "webgeneral",
        "msg": "báo cáo Web tổng hợp ngày 26/10/2016 09:00:03",
        "msg_type": "message",
        "task_status": "1",
        "title": "báo cáo Web tổng hợp ngày 26/10/2016 09:00:03",
        "icon": "",
        "link": "",
        "status": "1",
        "day": 1454042799,
        "metadata": 0,
        "contract_id": 1
      },
      "111": {
        "id": "111",
        "type": "webbuild",
        "msg": "Đang thực hiện thiết kế giao diện",
        "msg_type": "message",
        "task_status": "1",
        "title": "Đang thực hiện thiết kế giao diện",
        "icon": "",
        "link": "",
        "status": "0",
        "day": 1454042799,
        "metadata": 0,
        "contract_id": 11
      },
      "112": {
        "id": "112",
        "type": "webbuild",
        "msg": "Hoàn tất thiết kế giao diện",
        "msg_type": "message",
        "task_status": "1",
        "title": "Hoàn tất thiết kế giao diện",
        "icon": "",
        "link": "",
        "status": "1",
        "day": 1454042798,
        "metadata": 0,
        "contract_id": 11
      },
      "113": {
        "id": "113",
        "type": "webbuild",
        "msg": "Thiết kế giao diện ban đầu",
        "msg_type": "task",
        "task_status": "0",
        "title": "Thiết kế giao diện ban đầu",
        "icon": "",
        "link": "",
        "status": "0",
        "day": 1454042797,
        "metadata": 0,
        "contract_id": 11
      },
      "114": {
        "id": "114",
        "type": "webbuild",
        "msg": "Chỉnh sửa giao diện 1 lần",
        "msg_type": "task",
        "task_status": "0",
        "title": "Chỉnh sửa giao diện 1 lần",
        "icon": "",
        "link": "",
        "status": "0",
        "day": 1454042796,
        "metadata": 0,
        "contract_id": 11
      },
      "115": {
        "id": "115",
        "type": "webbuild",
        "msg": "Chỉnh sửa giao diện trang module",
        "msg_type": "task",
        "task_status": "1",
        "title": "Chỉnh sửa giao diện trang module",
        "icon": "",
        "link": "",
        "status": "0",
        "day": 1454042799,
        "metadata": 0,
        "contract_id": 11
      },
      "116": {
        "id": "116",
        "type": "webbuild",
        "msg": "Thiết kế banner top",
        "msg_type": "task",
        "task_status": "2",
        "title": "Thiết kế banner top",
        "icon": "",
        "link": "",
        "status": "0",
        "day": 1454042799,
        "metadata": 0,
        "contract_id": 11
      },
      "211": {
        "id": "211",
        "type": "hosting",
        "msg": "Xác nhận đã thanh toán phí đăng ký hosting",
        "msg_type": "message",
        "task_status": "1",
        "title": "Xác nhận đã thanh toán phí đăng ký hosting",
        "icon": "",
        "link": "",
        "status": "0",
        "day": 1454042799,
        "metadata": 0,
        "contract_id": 21
      },
      "212": {
        "id": "212",
        "type": "hosting",
        "msg": "Sắp hết hạn sử dụng Hosting",
        "msg_type": "message",
        "task_status": "1",
        "title": "Sắp hết hạn sử dụng Hosting",
        "icon": "",
        "link": "",
        "status": "0",
        "day": 1454042799,
        "metadata": 0,
        "contract_id": 21
      },
      "311": {
        "id": "311",
        "type": "domain",
        "msg": "Thông báo gia hạn domain thành công",
        "msg_type": "message",
        "task_status": "1",
        "title": "Thông báo gia hạn domain thành công",
        "icon": "",
        "link": "",
        "status": "0",
        "day": 1454042799,
        "metadata": 0,
        "contract_id": 31
      },
      "312": {
        "id": "312",
        "type": "domain",
        "msg": "Thông tin truy cập đã được gửi , Check Mail để xem chi tiết",
        "msg_type": "message",
        "task_status": "2",
        "title": "Thông tin truy cập đã được gửi , Check Mail để xem chi tiết",
        "icon": "",
        "link": "",
        "status": "0",
        "day": 1454042799,
        "metadata": 0,
        "contract_id": 31
      }
    }
  },
  "next_time": 1477973502
}
```