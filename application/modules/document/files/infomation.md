# GET thông tin user đăng nhập

URL API:

  GET /customer/profile?token=token_key

####Link: 
[http://erp.webdoctor.vn/api/customer/profile?token=ab123456789012345678901234567890](http://erp.webdoctor.vn/api/customer/profile?token=ab123456789012345678901234567890)

#### Parameters
No

#### Response
Trả về thông tin user đăng nhập

``` json
{
  "status": 1,
  "data": {
    "display_name": "0908070601",
    "id": "",
    "name": "",
    "phone": "0908070601"
  }
}
```
- `status`: Trạng thái xử lý của server, `1`: xử lý thành công , `0`: chưa thành công

Chú thích về `data` được trả về

- `display_name`: Tên user hiển thị trên app
- `id`: Id user
- `name`: Tên user
- `phone` : Số điện thoại user


# GET thông tin chung của app

URL API:

	GET /customer/general_info/

####Link: 
[http://erp.webdoctor.vn/api/customer/general_info](http://erp.webdoctor.vn/api/customer/general_info)

#### Parameters
No

#### Response
Trả về thông tin công ty hoặc các dữ liệu liên quan

``` json
{
  "status": 1,
  "info": {
    "name": "CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM",
    "address": "Văn phòng: Tầng 8, 402 Nguyễn Thị Minh Khai , Phường 5, Quận 3, TP.HCM, Việt Nam",
    "map": {
      "coordinate": "10.777314,106.691334"
    },
    "tax_code": "0313547231",
    "website": [
      {
        "url": "https://webdoctor.vn",
        "text": "webdoctor.vn"
      },
      ...
    ],
    "hotline": [
      {
        "label": "Hotline trung tâm Kinh doanh",
        "value": "(08) 7300 448"
      },
      ...
    ],
    "email": [
      {
        "label": "Email Trung tâm Kinh doanh",
        "value": "sales@adsplus.vn"
      },
      ...
    ],
    "bank": [
      {
        "label": "Tài khoản Techcombank",
        "value": "19129831121014"
      },
      ...
    ],
    "version": "1.0.0"
  }
}
```
- `status`: Trạng thái xử lý của server, `1`: xử lý thành công , `0`: chưa thành công

Chú thích về `info` được trả về

- `name`: Tên công ty
- `address`: Địa chỉ công ty
- `map`: Thông tin về bản đồ
	- `coordinate`: Tọa độ trên google map
- `tax_code`: Mã số thuế
- `website`: Mảng chứa Website của công ty 
	- `url`: Liên kết đến website
	- `text`: Tên website
- `hotline`: Mảng chứa Hotline của công ty 
	- `label`: label hiển thị trước hotline
	- `value`: Số hotline
- `email`: Mảng chứa Email của công ty 
	- `label`: label hiển thị trước email
	- `value`: địa chỉ email
- `bank`: Mảng chứa Thông tin ngân hàng 
	- `label`: label hiển thị trước số tài khoản ngân hàng 
	- `value`: Số tài khoản ngân hàng tương ứng
- `version`: Phiên bản api hiện tại
