Đăng nhập
===============



*Đăng nhập*

URL API:

	/customer/


#Get OTP

	POST /customer/login/

#### Parameters

`phone` : Số điện thoại khách hàng

#### Example

``` json
{
	"phone": "0974681379"
}
```

#### Response

``` json
{
"status": "0",
"msg": "Số điện thoại không tồn tại",
"token": "abc123456789"
}
```

`status`:

- `0`: Số điện thoại không tồn tại
- `1`: Số điện thoại có tồn tại trong hệ thống
- `2`: Lock điện thoại không cho Get Token

`msg`: Hiển thị `msg` nếu `msg` có dữ liệu

`token`: Nếu thành công sẽ trả về token

#Login

	POST	/customer/login/?token=abc123456789

#### Input
`token` : Token của server vừa gửi

`otp`: Mã OTP của Server gửi


#### Response

``` json
{
"status": "1",
"msg": "Đăng nhập thành công",
"token": "abc123456789",
"service" {
		"adsplus": 1,
		"webgeneral": 1,
		"hosting": 0,
		"webbuild": 0
	}
}
```
`status`: Trạng thái đăng nhập

- `0`: OTP hoặc Token không chính xác
- `1`: Đăng nhập thành công

`service`: Danh sách các service của KH này đang sử dụng, 

- `1`: có sử dụng
- `0`: không sử dụng

Chú thích danh sách service:

	adsplus: Adsplus
	webgeneral: Webdoctor
	hosting: Hosting
	webbuild: Thiết kế Web

============================================
#Login bằng mật khẩu

	POST	/customer/login/pwd/?token=abc123456789

#### Input
`token` : Token của server vừa gửi

`password`: Mật khẩu của user

#### Response

``` json
{
"status": "1",
"msg": "Đăng nhập thành công",
"token": "abc123456789",
"service" {
		"adsplus": 1,
		"webgeneral": 1,
		"hosting": 0,
		"webbuild": 0
	}
}
```
`status`: Trạng thái đăng nhập

- `0`: OTP hoặc Token không chính xác
- `1`: Đăng nhập thành công

`service`: Danh sách các service của KH này đang sử dụng, 

- `1`: có sử dụng
- `0`: không sử dụng

Chú thích danh sách service:

	adsplus: Adsplus
	webgeneral: Webdoctor
	hosting: Hosting
	webbuild: Thiết kế Web
==========================================

#Get OTP (sample data)
===============

	POST /customer/login/

#### Input

`phone` : 0908070601

#### Response

``` json
{
"status": "1",
"msg": "Số điện thoại có tồn tại trong hệ thống",
"token": "ab123456789012345678901234567890",
"auth" : "0"
}
```

#### Input

`phone` : 0908070602

#### Response

``` json
{
"status": "0",
"msg": "Số điện thoại không tồn tại",
"token": "",
"auth" : "0"
}
```

#### Input

`phone` : 0908070603

#### Response

``` json
{
"status": "0",
"msg": "Lock điện thoại không cho Get Token",
"token": "abc123456789",
"auth" : "0"
}
```	

#Login (sample data)

	POST /customer/login?token=token_key

#### Input

`token` : ab123456789012345678901234567890

`otp` : 123

#### Response

``` json
{
  "status": "1",
  "msg": "Đăng nhập thành công",
  "token": "abc123456789",
  "service": {
    "adsplus": 1,
    "webgeneral": 1,
    "hosting": 1,
    "webbuild": 1
  }
}
```

#### Input

`token` : ab123456789012345678901234567891

`otp` : 456

#### Response

``` json
{
"status": "0",
"msg": "OTP hoặc Token không chính xác",
"token": "ab123456789012345678901234567891",
"service" : {
		"adsplus": 0,
		"webgeneral": 0,
		"hosting": 0,
		"webbuild": 0
	},
"auth": "0"
}
```