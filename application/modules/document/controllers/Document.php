<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document extends Admin_Controller {

	protected $is_authencation = false;

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		redirect(admin_url().'document/api/','refresh');
	}

}