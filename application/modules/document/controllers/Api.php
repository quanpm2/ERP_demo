<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends Admin_Controller {

	protected $is_authencation = false;

	function __construct()
	{
		parent::__construct();
		$this->init_nav();
	}
	
	protected function init_nav()
	{
		$module_url = admin_url().'document/api/';

		$slug = $this->uri->segment(5);
		if(!$slug)
			$slug = 'index';

		$documents = [
		'index' => 'Tổng quan',
		'auth' => 'Đăng nhập',
		'notify' => 'Notify',
		'infomation' => 'Thông tin chung',
		'message' => 'Thông báo',
		'adsplus' => 'AdsPlus',
		'webgeneral' => 'Webdoctor',
		'webbuild' => 'Thiết kế Website',
		'hosting' => 'Hosting',
		'domain' => 'Tên miền',
		];

		$order = 0;
		$this->template->title->set('API ');

		foreach($documents as $key => $val)
		{
			$is_active = ($slug == $key);

			if($is_active)
			{
				$this->template->title->append($val);
			}

			$this->menu->add_item(array(
				'id' =>  $key,
				'name' => $val,
				'parent' => null,
				'slug' => $module_url.'view/'.$key,
				'order' => ($order++),
				'is_active' => $is_active
				),'left');
		}

		// $this->menu->add_item(array(
		// 		'id' =>  'debug',
		// 		'name' => 'Debug',
		// 		'parent' => null,
		// 		'slug' => $module_url.'debug',
		// 		'order' => ($order++),
		// 		'is_active' => $is_active
		// 		),'left');
	}

	function index($file = 'index')
	{
		$this->load->library('markdown');
		
		$this->template->stylesheet->add('plugins/highlight/default.css');
		$this->template->stylesheet->add('plugins/highlight/github.css');
		$this->template->javascript->add('plugins/highlight/highlight.min.js');

		$path = APPPATH.'modules/document/files/';
		$file_path = $path.$file.'.md';

		if(!$file)
		{
			$file_path = $path.'index';
		}

		$content =	'';
		$content.= file_exists($file_path) ? 
		$this->markdown->parse_file($file_path) :
		'Tài liệu đang được xây dựng';

		return parent::render(['content'=>$content], 'api');
	}

	function view($file = 'index')
	{
		return $this->index($file);
	}

	function debug($file = 'index')
	{
		$data = [];
		return parent::render($data);
	}

	function list($file = 'index')
	{
		echo '123';
	}
}