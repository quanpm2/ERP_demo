<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config['network'] = array(
	'g' => 'Google search', 
	's' => 'Search partner', 
	'd' => 'Display Network'
);

$config['device'] = array(
	'm' => 'Mobile', // (including WAP)
	't' => 'tablet', 
	'c' => 'Computer'
);