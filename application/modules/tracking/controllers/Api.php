<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends API_Controller {

	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Request IP visitors API
	 *
	 * @return     JSON
	 */
	function visitors()
	{
		$response = array('status'=>FALSE,'msg'=>'Failure','data'=>array());

		$default = array('start_time'=>$this->mdate->startOfDay('2017/01/01'),'end_time'=>$this->mdate->endOfDay(),'tracking_code'=>'');
		$args = wp_parse_args($this->input->get(), $default);

		if(empty($args['tracking_code']))
		{
			$response['msg'] = 'Tham số không hợp lệ .';
			return parent::renderJson($response);
		}

		$this->load->model('tracking/visitor_sessions_m');
		$data = $this->visitor_sessions_m->get_many_by(['tracking_code'=>$args['tracking_code'],'created_on >=' => $args['start_time'],'created_on <=' => $args['end_time']]);
		if( ! $data)
		{
			$response['msg'] = 'Dữ liệu không tìm thấy hoặc đã bị xóa .';
			return parent::renderJson($response);
		}

		$response['status'] = TRUE;
		$response['msg'] = 'Xử lý thành công !';
		$response['data'] = $data;

		return parent::renderJson($response);
	}
}
/* End of file Api.php */
/* Location: ./application/modules/tracking/controllers/Api.php */