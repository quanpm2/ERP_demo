<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tracking extends Public_Controller {

	function __construct()
	{
		parent::__construct();
		// $this->config->load('tracking/value_track');
	}

	public function index()
	{
		$this->update();
		redirect($this->input->get('lpurl'), 'refresh');
	}

	public function update($args = array())
	{
		$args = $this->input->get();

		$this->load->library('user_agent');

		$ip = $this->input->ip_address();

		$agent_string = $this->agent->agent_string();
		$platform = $this->agent->platform();
		$browser = $this->agent->browser();
		$version = $this->agent->version();

		$is_browser = $this->agent->is_browser();
		$is_robot = $this->agent->is_robot();
		$is_mobile = $this->agent->is_mobile();

		$device = FALSE;
		switch ($args['device'])
		{
			case 'm':
				$device = 'mobile';
				break;

			case 't':
				$device = 'tablet';
				break;
			
			default:
				$device = 'computer';
				break;
		}

		$data = array(
			'ip' 				=> $ip,
			'campaignid' 		=> $args['campaignid'] ?? '',
			'adgroupid' 		=> $args['adgroupid'] ?? '',
			'keyword' 			=> $args['keyword'] ?? '',
			'creative' 			=> $args['creative'] ?? '',
			'targetid' 			=> $args['targetid'] ?? '',
			'loc_interest_ms' 	=> $args['loc_interest_ms'] ?? '',
			'loc_physical_ms' 	=> $args['loc_physical_ms'] ?? '',
			'network' 			=> $args['network'] ?? '',
			'device' 			=> $device,
			
			'user_agent' 		=> $agent_string,
			'browser' 			=> $browser,
			'is_mobile' 		=> $is_mobile,
			'is_browser' 		=> $is_browser,
			'is_robot' 			=> $is_robot,

			'platform' 			=> $platform,
			'browser'			=> $browser,
			'version' 			=> $version,

			'created_on' 		=> time(),
			'tracking_code' 	=> $args['tracking_code'] ?? '',
			'resolution' 		=> $args['resolution'] ?? '',
		);

		$this->load->model('tracking/visitor_sessions_m');
		$this->visitor_sessions_m->insert($data);

		/* Pre-Analytic IP */

		/* PING HAS CHANGED TO ERP SYSTEM */
		
		// single IP - many session

		// many IP session
		
		// prd($this->input->get(),$data);
	}
}
/* End of file Tracking.php */
/* Location: ./application/tests/modules/tracking/controllers/Tracking.php */