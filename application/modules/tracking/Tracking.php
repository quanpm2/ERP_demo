<?php
class Tracking_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name(){
		
		return 'Tracking';
	}

	public function init(){	

		if(has_permission('Admin.Tracking') && is_module_active('tracking'))
		{

		}
	}

	public function title()
	{
		return 'Tracking';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '0.1';
	}

	public function description()
	{
		return 'Tracking';
	}
	private function init_permissions()
	{
		$permissions = array();
		$permissions['Admin.Tracking'] = array(
			'description' => 'Quản lý Tracking',
			'actions' => array('view','add','edit','delete','update'));

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}