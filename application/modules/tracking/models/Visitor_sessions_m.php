<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Visitor_sessions_m extends Base_model {

	public $primary_key = 'id';
	public $_table = 'visitor_sessions';

	function __construct() 
	{
		parent::__construct();
	}
}
/* End of file Visitor_sessions_m.php */
/* Location: ./application/modules/tracking/models/Visitor_sessions_m.php */