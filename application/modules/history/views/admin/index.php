<div class="col-md-12">
  <!-- The time line -->
  <ul class="timeline">
    <!-- timeline time label -->
    <li class="time-label"><span class="bg-red">Today</span></li>
    <?php 
    if(!empty($history)):
      foreach ($history as $node) {

        $display_name = $this->admin_m->get_field_by_id($node->user_id, 'display_name');
        
        echo 
        '<li>
          <i class="fa fa-user bg-aqua"></i>
          <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> '.human_timing($node->created_time).'</span>
            <h3 class="timeline-header no-border">'.
            anchor('#', $display_name)." <b>updated </b> <u>{$node->key}</u> <br/><b>from</b> <u>{$node->old_value}</u> <br/><b>to</b> <u>{$node->new_value}</u>".'
            </h3>
          </div>
        </li>';
      }
    endif;
    ?>
    <li>
      <i class="fa fa-clock-o bg-gray"></i>
    </li>
  </ul>
</div>