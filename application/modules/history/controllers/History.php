<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends Admin_Controller {

	public $model = 'history_m';

	function __construct(){

		parent::__construct();

		$this->load->model($this->model);
	}

	public function index(){

		$this->data['history'] = $this->history_m->order_by('created_time','desc')->get_many_by();

		parent::render($this->data);
	}
}