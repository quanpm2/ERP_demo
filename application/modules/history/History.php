<?php
Class History_Package extends Package
{
	public function name()
	{
		return 'History';
	}

	public function init(){

		if(gethostname() == 'DESKTOP-U6TJDVM'){
			
			$this->menu->add_item(['id'=>'staffs','name'=>'Lịch sử','parent'=>NULL,'slug'=>admin_url('history'),'order'=>1,'icon'=>''],'navbar');
		}
	}

	public function title()
	{
		return 'Lịch sử';
	}

	public function author()
	{
		return 'thonh';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Lịch sử sửa đổi dữ liệu';
	}

	private function init_permissions()
	{
		$permissions = array();
		// $permissions['Admin.Staffs'] = array(
		// 	'description' => 'Quản lý nhân lực',
		// 	'actions' => array('view','edit','delete','update'));

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}