<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends REST_Controller
{
	function __construct()
	{
		parent::__construct('wservice/rest');
		$this->load->helper('meta_helper');
		$this->load->model('usermeta_m');
	}

	public function subscribe_get()
	{
		$default 	= array('event' => '', 'email' => '', 'name' => '', 'redirect_url' => '//event.adsplus.vn?registed=1');
		$args 		= wp_parse_args(parent::get(NULL, TRUE), $default);

		if('july-june-event' !=  $args['event']) parent::response(NULL, parent::HTTP_BAD_GATEWAY);

		try // Call curl for build data and no need to wait
        {
			/* send email with no wait response */	
			Requests::post(base_url('wservice/v1/guru/email'), [], $args, ['timeout' => 1,'connect_timeout' => 10]);
        }
        catch(Exception $e) { /* do nothing */ }; 

		/* redirect to redirect url */
		redirect($args['redirect_url'], 'refresh');
	}


	/**
	 * Gửi thông tin LEAD quang tâm hội thảo Guru tháng 6-7/2018
	 */
	public function email_post()
	{
		$default 	= array('email' => '','name' => '', 'phone' => '', 'source' => '');
		$args 		= wp_parse_args(parent::post(NULL, TRUE), $default);

		if(empty($args['email']) || empty($args['name'])) 
			parent::response(NULL, parent::HTTP_BAD_GATEWAY);

		$date = my_date(time(), 'd.m.Y H:i:s');
		$title 		= "[GURU-SUBSCRIBED] Thông tin khách hàng đăng ký quan tâm hội thảo vào lúc {$date}";
		$content 	= "
		Tên gọi : {$args['name']} <br/>
		Email : {$args['email']} <br/>
		Phone : {$args['phone']} <br/>
		Nguồn email : {$args['source']}";

		$this->load->library('email');
		$this->email->to('event@adsplus.vn');
		$this->email->cc('thonh@webdoctor.vn');
		$this->email->subject($title);
		$this->email->from('support@adsplus.vn','ADSPLUS.VN');
		$this->email->message($content);
		$result = $this->email->send();
		parent::response();
	}
}
/* End of file Guru.php */
/* Location: ./application/modules/wservice/controllers/Guru.php */