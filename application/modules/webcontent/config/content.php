	<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['packages'] = array(
    'service' => array(),
    'default' =>''
);


$config['tasks_type'] = array(
	'task-default'		 	=>'thiết kế Bannner',
	'custom-task' 		 	=>'Yêu cầu khách hàng'
	);

$config['post_type'] = array(
	'post_facebook'		 	=>'post_facebook',
	'fanpage_facebook'	=>'fanpage_facebook',
	);

$config['description_content']='Post Quảng Cáo: 500k/bài viết, dưới 400 từ. hỗ trợ hình ảnh khách gửi (10 hình), gắn logo.
          Chăm sóc Fanpage: 2.000.000/Gói, 1 gói 10 bài viết, dưới 200 từ. Không cập nhật chức năng fanpage, không trả lời comment.'; 

$config['packages_content']=array(
	'facebook_content'=>array(
		'post_facebook'=>array(
			'label'		=>'Bài viết quảng cáo Facebook <br/> đơn vị tính là "bài viết" dưới 400 từ. hỗ trợ hình ảnh khách gửi (10 hình), gắn logo.',
			'number'	=>'number_post_facebook',
			'name' 		=>'price_post_facebook',
			'price_content'	=>500000,
			'kpi_point'		=> 	0,
			'min'			=>  1,
			'sale_off'		=>array('min'=>1,'value'=>0)
			),
		'fanpage_facebook'=>array(
			'label'=>'Bài Viết Fanpage Facebook <br/> đơn vị tính là "Gói" 1 gói 10 bài viết, dưới 200 từ. Không cập nhật chức năng fanpage, không trả lời comment.',
			'number'=>'number_fanpage_facebook',
			'name' =>'price_fanpage_facebook',
			'price_content'=>200000,
			'kpi_point'		=> 	0,
			'min'			=> 10,
			'sale_off'		=>array('min'=>1,'value'=>0)
			),
		)
	);

	
//set kpi mac dinh của thanh cho thiet ke content
$config['kpi']=array(
		 'user_id'		=>	'727',
		 'kpi_value'	=>	'1',
		 'kpi_type'		=>'tech'
);

$config['total_kpi_month']	= 500;
$config['bonus_kpi']		= 10000;

