<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics extends Admin_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('webcontent_m');
		$this->load->model('webcontent_kpi_m');
		$this->load->config('webcontent/content');
	}
	
	function kpi($month = '', $year = '')
	{
		
		restrict('Webcontent.Content.Manage,Webcontent.Content.Access');


		if($time = $this->input->post('time'))
		{
			$month = date('m',$time);
			$year = date('Y',$time);
			redirect(module_url("statistics/kpi/{$month}/{$year}"),'refresh');
		}

		$data = $this->data;
		$this->template->title->prepend('Thống kê bài viết theo nhân viên');

		$year = $year ?: date('Y');
		$month = $month ?: date('m');
		$data['time'] = strtotime("{$year}/{$month}/01");
		$kpi_datetime = date("{$year}/{$month}/01");

		$kpi_content = $this->webcontent_kpi_m
		->order_by(['user_id'=>'asc','term_id'=>'desc'])
		->get_many_by(['kpi_type'=>$this->webcontent_m->get_post_type(),'kpi_datetime'=>$kpi_datetime]);



		if(empty($kpi_content)) return parent::render($data,'statistics/kpi');

		$i = 1;
		$user_kpis = array_group_by($kpi_content,'user_id');
		$this->table->set_heading('#','Nhân viên','Dự án','KPI', 'Hoàn thành','Còn lại');

		foreach ($user_kpis as $user_id => $kpis) 
		{
			$rowspan = count($kpis) + 1;
			$display_name = $this->admin_m->get_field_by_id($user_id, 'display_name');

			$kpi_total = array_sum(array_column($kpis,'kpi_value'));
			$kpi_total_result = array_sum(array_column($kpis,'result_value'));
			$kpi_total_progress = $this->admin_form->progress_bar($kpi_total_result,$kpi_total);

			$this->table->add_row(['data'=>$i++,'rowspan'=>$rowspan],['data'=>$display_name.br().$kpi_total_progress,'rowspan'=>$rowspan]);

			foreach ($kpis as $kpi) 
			{
				$kpi_term = $this->term_m->get($kpi->term_id);
				$display_name = $this->admin_m->get_field_by_id($kpi->user_id, 'display_name');
				$remain = $kpi->kpi_value - $kpi->result_value;
				$remain = $remain > 0 ? $remain : 0;

				$kpi_progress = $this->admin_form->progress_bar($kpi->result_value,$kpi->kpi_value);

				$this->table->add_row($kpi_term->term_name.br().$kpi_progress,$kpi->kpi_value,$kpi->result_value,$remain);
			}
		}

		$data['content'] = $this->table->generate();
		parent::render($data,'statistics/kpi');
	}

}
/* End of file Statistics.php */
/* Location: ./application/modules/webcontent/controllers/Statistics.php */