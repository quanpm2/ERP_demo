<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webcontent extends Admin_Controller {

	public $model = '';
	private $website_id;
	private $term;
	private $term_type;

	function __construct()
	{
		parent::__construct();
		
		$models = array(
			'webcontent_m',
			'webcontent_kpi_m',
			'webcontent_seotraffic_m',
			'webcontent_callback_m',
			'webcontent_seotop_m',
			'webcontent_config_m',
			'webcontent_overview_m',
			'contract/contract_m'
			);

		$this->load->model($models);
		$this->load->config('content');
		$this->load->add_package_path(APPPATH.'third_party/google-analytics/');

		$this->term_type = $this->webcontent_m->term_type;
		$this->init_website();
		
	
	}
	private function init_website()
	{
		$term_id = $this->uri->segment(4);
		$method  = $this->uri->segment(3);
		
		$is_allowed_method = (!in_array($method, array('index','done','ajax','statistic','statistics')));
		if(!$is_allowed_method || empty($term_id)) return FALSE;

		$term = $this->term_m
		->where('term_status','publish')
		->where('term_type',$this->term_type)
		->where('term_id',$term_id)
		->get_by();

		if(empty($term) OR !$this->is_assigned($term_id)) 
		{
			$this->messages->error('Truy cập #'.$term_id.' không hợp lệ !!!');
			redirect(module_url(),'refresh');
		}

		$this->template->title->set(strtoupper(' '.$term->term_name));
		$this->website_id = $this->data['term_id'] = $term_id;
		$this->data['term'] = $this->term = $term;
	}

	public function index()
	{
		restrict('Webcontent.Index.Access');

		$this->search_filter();
		$data = array();
		$time = time();
		$day = date('d',$time) - 1;
		if($day == 0){
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}
		$time = strtotime(date('Y-m-'.$day, $time));
		$this->time = $time;
		$day = str_pad($day, 2, "0", STR_PAD_LEFT);
		$start_date = strtotime(date('Y/m/01',$time));
		$end_date = strtotime(date('Y/m/d',$time));

		$this->template->title->append('Tổng quan dịch vụ từ ngày '.date('d/m/Y',$start_date).' đến ngày '.date('d/m/Y',$end_date));
		$this->config->load('webcontent/content');
		$serv_packages = array_map(function($x){ return $x; }, $this->config->item('packages_content'));

		$this->admin_ui
			->set_filter_position(FILTER_TOP_OUTTER)
			->select('term.term_id')

			->add_column('stt', array('set_select'=> FALSE, 'title'=> 'STT','set_order'=> FALSE))
			->add_column('term.term_name','Website')
			->add_column('content', array('set_select'=>FALSE,'title'=>'Bài viết','set_order'=>FALSE))
			->add_column('payment_percentage', array('set_select' => FALSE,'title'=> 'T/đ thanh toán','set_order'=>FALSE))
			->add_column('action', array('set_select'=>FALSE, 'title'=>'Actions', 'set_order'=>FALSE))

			->add_column_callback('stt',function($data,$row_name){
				
				global $stt;
				$data['stt'] = (++$stt);

				$term_id = $data['term_id'];
				
				// Check permission for sale role

				if(has_permission('webcontent.Sale.access'))
				{
					$sale_id = get_term_meta_value($term_id,'staff_business');
					if($sale_id != $this->admin_m->id) 
					{
						return FALSE;
					}
				}

				// CALLBACK FOR WEBSITE COLUMN
				$domain = $data['term_name'];

				$data['term_name'] = anchor(module_url("overview/{$term_id}"),$domain,'data-toggle="tooltip" title="Chi tiết '.$domain.'"');

				$package_name = $this->webcontent_config_m->get_package_label($data['term_id']);
				$data['term_name'].= empty($package_name) ? '' : '<br/>'.$package_name;

				$has_start_service = get_term_meta_value($data['term_id'],'start_service_time');
				$data['term_name'].= empty($has_start_service)?'<br/><span class="label label-danger">Chưa gửi mail kết nối</span>': '' ;

				if(!empty($has_start_service))
				{

					$contract_begin = get_term_meta_value($data['term_id'],'start_service_time');
					$contract_end = get_term_meta_value($data['term_id'],'end_service_time');
					$diff = diffInMonths(time(),$contract_end);

					$label_color = 'label-default';
					($diff == 1 || $diff == 0 ) AND $label_color ='label-warning';
					(time() > $contract_end) AND $label_color ='label-danger';
					
					$data['term_name'].= (($label_color) ? '<br/><span class="label '.$label_color.'">Kết thúc HĐ: '.date('d/m/Y',$contract_end).'</span>' :'');
				}

				return $data;

			},FALSE)

			->add_column_callback('content',function($data,$row_name) {

					$term_id = $data['term_id'];

					$contract_begin = my_date(get_term_meta_value($term_id,'contract_begin'),'d/m/Y');
					$contract_end = my_date(get_term_meta_value($term_id,'$contract_end'),'d/m/Y');

					$str =  get_term_meta_value($term_id, 'contents_selected');
          			$services = unserialize($str);
					
					$this->load->config('webcontent/content');
          			$packages_content_config = $this->config->item('packages_content');
					$content = '';
      				foreach ($services as $key_packages => $packages) {
			            foreach ($packages as $key_package => $package) {
			                $content .= str_replace('_',' ',$key_package);
			                $sum_content_kpi_result = $this->webcontent_kpi_m->select_sum('result_value')->where('kpi_type',$key_package)->where('term_id',$term_id)->get_by();
			                $total_content_kpi = 0;
			                $total_content_kpi = $package['number_'.$key_package];
			                if($key_package=='fanpage_facebook')
			                {
								
			                	$content .= $this->admin_form->progress_bar($sum_content_kpi_result->result_value,$total_content_kpi*10);
			                	continue;
			                }
			               	$content .= $this->admin_form->progress_bar($sum_content_kpi_result->result_value,$total_content_kpi);
			            }
		            }
	            	$data['content'] = $content;
					return $data;
				},FALSE)

			->add_column_callback('payment_percentage',function($data,$row_name){

				$term_id = $data['term_id'];
				$contract_value = (double)get_term_meta_value($term_id,'contract_value');
				$payment_amount = (double)get_term_meta_value($term_id,'payment_amount');

				$payment_percentage = div($payment_amount,$contract_value)*100;

				if($payment_percentage >= 50 && $payment_percentage < 80)
					$text_color = 'text-yellow';
				else if($payment_percentage >= 80 && $payment_percentage < 90)
					$text_color = 'text-light-blue';
				else if($payment_percentage >= 90)
					$text_color = 'text-green';
				else $text_color = 'text-red';

				$payment_percentage = currency_numberformat($payment_percentage,' %');
				$data['payment_percentage'] = "<b class='{$text_color}'>{$payment_percentage}</b>";
				
				return $data;

			},FALSE)

			->add_column_callback('action', function($data, $row_name){
				
				$data['action'] = anchor(module_url('overview/'.$data['term_id']),'<i class="fa fa-fw fa-line-chart"></i>','data-toggle="tooltip" title="Chi tiết"');
				
				return $data;	

			}, FALSE)

			->where('term_status','publish')
			->where('term.term_type', $this->term_type)
			//->where('term.term_type', 'webcontent')
			->from('term')
			->group_by('term.term_id');

		$data['content'] = $this->admin_ui->generate(array('per_page' => 100,'uri_segment' => 4,'base_url'=> module_url('index/')));

		parent::render($data);
	}
	// OVERVIEW
	public function overview($term_id = 0)
	{
		restrict('Webcontent.Overview.Access');

		$term = $this->term_m->get($term_id);
		if(empty($term)){
			$this->messages->error('Dịch vụ không tồn tại');
			redirect(module_url(),'refresh');
		}

		$time = time();	
		$day = date('d',$time) - 1;
		if($day == 0){
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}
		$date_start = date('01-m-Y',$time);
		$day = str_pad($day, 2, "0", STR_PAD_LEFT);
		$date_end = date($day.'-m-Y', $time);


		$description = 'Từ '.$date_start.' đến '.$date_end;

		$time_start  = $this->mdate->startOfDay(strtotime($date_start));
		$time_end    = $this->mdate->endOfDay(strtotime($date_end));

		$this->template->description->append($description);
		$this->template->title->prepend('Tổng quan');
		$this->template->javascript->add('plugins/chartjs/Chart.js');

		$statistic_index_data = $this->webcontent_overview_m->get_statistic_index_data($term,$time_start,$time_end);

		$char_data = $this->webcontent_overview_m->build_seotraffic_chart($term_id,my_date($time_start,'y-m-d'),my_date($time_end,'y-m-d'));

		$content_data = $this->webcontent_overview_m->get_content_table($term_id,$time_start,$time_end);

		$customer_table = $this->webcontent_overview_m->get_customer_info($term_id);

		$data = array_merge_recursive($statistic_index_data,$char_data,$content_data,$customer_table);

		parent::render($data);
	}

	protected function is_assigned($term_id = 0,$kpi_type = '')
	{
		// check user has manager role permission
		$class = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		$result = $this->webcontent_m->has_permission($term_id,"{$class}.{$method}",'access');
		return $result;
	}
	// END OVERVIEW

	// KPI
	public function kpi($term_id = 0, $delete_id= 0)
	{
		restrict('Webcontent.Kpi.Access');
		
		$this->template->title->prepend('KPI');
		$this->load->config('group');
		$data = $this->data;
		$this->submit_kpi($term_id,$delete_id);

		$data['kpi_type'] = $this->config->item('services');

		$data['kpi_desc'] = array('seotraffic'=>'Điền KPI cần đạt', 'content'=>'Số lượng bài viết');
		$data['term_id']  = $term_id;

		$data['time'] 	  = $this->mdate->startOfMonth(time());
		$time_start 	  = get_term_meta_value($term_id, 'contract_begin');
		$time_end 		  = get_term_meta_value($term_id, 'contract_end');

		//+2 month
		$time_start = strtotime('-1 month');
		$time_end = strtotime('+2 month', $time_end);

		$data['time_start'] = $time_start;
		$data['time_end'] = $time_end;
		$data['time_start'] = $this->mdate->startOfDay($data['time_start']);
		$data['time_end'] = $this->mdate->endOfDay($data['time_end']);

		$targets = $this->webcontent_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(array(
			'term_id'=>$term_id
			));
		
		$data['targets'] 					= array();
		$data['targets']['post_facebook'] 		= array();
		$data['targets']['fanpage_facebook'] = array();
		$data['post_facebook']['number'] = 0;
		$data['fanpage_facebook']['number'] = 0;
		
		foreach($targets as $target)
		{
			$data['targets'][$target['kpi_type']][] = $target;
		}
		$data['date_contract'] = array();
		for($i=$data['time_start'];$i< $data['time_end']; $i++)
		{
			$data['date_contract'][$time_start]  = date('Y-m', $time_start);
			$date = $data['date_contract'][$time_start];

			$time_start = strtotime('+1 month', strtotime($date));
			if($time_start > $time_end)
				break;
		}

		$data['users'] = $this->admin_m->select('user_id,display_name')->where_in('role_id', $this->config->item('group_memberId'))->set_get_active()->order_by('display_name')->get_many_by();
		$data['users'] = key_value($data['users'],'user_id','display_name');

		// lay so luong bai viet gan vao data['number']
		$str =  get_term_meta_value($term_id, 'contents_selected');
      	$services = unserialize($str);
      	$this->load->config('webcontent/content');
      	$packages_content_config = $this->config->item('packages_content');

      	foreach ($services as $key_packages => $packages) {
	         foreach ($packages as $key_package => $package) {
	         	$data[$key_package]['number'] = $package['number_'.$key_package];
	         }
	       }
		$this->data = $data;

		parent::render($this->data);
	}
	public function submit_kpi($term_id = 0,$delete_id=0)
	{
		
		if($delete_id > 0)
		{
			$this->_delete_kpi($term_id,$delete_id);
			redirect(module_url("kpi/{$term_id}"),'refresh');
		}

		$post = $this->input->post();



		if(empty($post)) return FALSE;

		if( ! $this->webcontent_m->has_permission($term_id, 'Webcontent.kpi','add'))
		{	
			$this->messages->error('Không có quyền thực hiện tác vụ này.');
			redirect(module_url("kpi/{$term_id}"),'refresh');
		}

		$insert = array();
		$insert['kpi_datetime'] = $this->input->post('target_date');
		$insert['user_id'] = $this->input->post('user_id');
		$insert['kpi_value'] = (int)$this->input->post('target_post');

		$kpi_type = $this->input->post('target_type');
		
		if(empty($kpi_type))
		{
			$is_submit_post_facebook 	 = $this->input->post('submit_kpi_post_facebook');

			$is_submit_fanpage_facebook  = $this->input->post('submit_kpi_fanpage_facebook');

			if(!empty($is_submit_fanpage_facebook)) { 
				$kpi_type               = 'fanpage_facebook' ;
			}
			
			if(!empty($is_submit_post_facebook)) { 
				$kpi_type = 'post_facebook';
			}			
		}

		if($insert['kpi_datetime'] >= $this->mdate->startOfMonth())
		{
			if (empty($insert['kpi_value'])) {
				$this->messages->error('Cập nhật không thành công. Vui lòng nhập số lượngbài viết');
			}else{
				$this->webcontent_kpi_m->update_kpi_value($term_id, $kpi_type, $insert['kpi_value'], $insert['kpi_datetime'], $insert['user_id']);

				$this->webcontent_kpi_m->delete_cache($delete_id);
				$this->messages->success('Cập nhật thành công');
			}
			
		}
		else
		{
			$this->messages->error('Cập nhật không thành công do tháng cập nhật nhỏ hơn tháng hiện tại');
		}

		redirect(current_url(),'refresh');
	}
	private function _delete_kpi($term_id = 0,$delete_id=0)
	{
		if( ! $this->webcontent_m->has_permission($term_id, 'Webcontent.kpi','delete',$kpi_type = ''))
		{	
			$this->messages->error('Không có quyền thực hiện tác vụ này.');
			return FALSE;
		}

		$_tmp_model = 'webcontent_kpi_m';
		

		$check = $this->{$_tmp_model}->get($delete_id);
		$is_deleted = false;
		if($check)
		{
			if(strtotime($check->kpi_datetime) >= $this->mdate->startOfMonth())
			{
				$this->{$_tmp_model}->delete_cache($delete_id);
				$this->{$_tmp_model}->delete($delete_id);
				$is_deleted = true;
				$this->messages->success('Xóa thành công');
			}
		}
		($is_deleted) OR $this->messages->error('Xóa không thành công');
	}
	//END KPI

	//CONTENT
	function content($term_id = 0)
	{
		restrict('Webcontent.Content');
		
		if($this->input->post())
		{
			Modules::run('webcontent/tasks/insert_task',$term_id, $this->webcontent_m->get_post_type());
		}

		$this->template->title->prepend('Bài viết');
		$data = $this->data;

		$this->admin_ui
		->add_column('posts.post_id','#')
		->add_column('post_title','Tiêu đề')
		->add_column('post_content', 'Nội dung')
		->add_column_callback(array('post_content','post_author','type'),array($this->webcontent_callback_m,'post_get_meta'), FALSE)
		->add_column('post_type', array('title'=>'Loại','set_order'=>FALSE))
		->add_column('post_author', 'Người tạo')
		->add_column('start_date','Ngày thêm','$1','date("d/m/Y",start_date)')
		->add_column('view', array('set_select'=>FALSE, 'title' =>'', 'set_order'=>FALSE))
		->add_column_callback('view',function($data,$row_name){

				$post_id = $data['post_id'];
				$post_type = $data['post_type'];
				$view = '';

				if(has_permission('Webcontent.Content.Update'))
				{
					
					$view.= anchor(module_option_url('webdoctor',"tasks/edit/{$post_id}"),'<i class="fa fa-fw fa-edit"></i>',"title='Cập nhật' class='btn btn-default btn-xs ajax_edit' data-taskid='{$post_id}'");
				}

				if(has_permission('Webcontent.Content.Delete'))
				{
					$view.= form_button('','<i class="fa fa-fw fa-trash"></i>',"title='Xóa' class='btn btn-default btn-xs ajax_delete' data-taskid='{$post_id}' data-posttype = '{$post_type}' data-toggle='confirmation'");
				}

				$data['view'] = $view;
				return $data;
			},FALSE)
		->where_in('post_type', $this->webcontent_m->get_post_type())
		->where('term_id',$term_id)
		->order_by("posts.post_id desc")
		->from('posts')
		->join('term_posts', 'term_posts.post_id = posts.post_id');

		//	prd($this->admin_ui->get_query()); 
		// Add filter for user with no manage permission
		if( ! has_permission('Webcontent.Content.Manage')) 
		{
			$this->admin_ui->where('posts.post_author',$this->admin_m->id);
		}


		$data['content'] = $this->admin_ui->generate();


		$this->data = $data;
		parent::render($this->data);
	}
	//END CONTENT
	//WEBCONTENT DONE
		public function done()
	{
		restrict('Webcontent.Done.Access');

		$this->search_filter();
		$this->template->title->append('Dịch vụ đã hoàn thành');
		$data = $this->data;

		$this->admin_ui
			->select('term.term_id')
			->add_search('term_name')
			->add_search('service_package',array(
				'content'=> form_dropdown(array('name'=>'where[service_package]',
					'class'=>'form-control select2'), 
					prepare_dropdown(array_map(function($x){ return $x['name']; }, $this->config->item('service','packages'))),
					$this->input->get('where[service_package]'))))		
			->add_search('action',array('content'=> $this->admin_form->submit('search','Tìm kiếm')))
			->add_column('stt', array('set_select'=> FALSE, 'title'=> 'STT','set_order'=> FALSE))
			->add_column_callback('stt',function($data,$row_name){
				global $stt;
				$data['stt'] = (++$stt);
				return $data;
			},FALSE)
			->add_column('term_name','Website')
			->add_column_callback('term_id',function($data,$row_name){

				if(!has_permission('Webcontent.Sale.access')) return $data;
				$sale_id = get_term_meta_value($data['term_id'],'staff_business');
				if($sale_id != $this->admin_m->id) return FALSE;
				
				return $data;
			},FALSE)
			->add_column('content', array('set_select'=>FALSE,'title'=>'Bài viết','set_order'=>FALSE))
			->add_column('banner', array('set_select'=>FALSE,'title'=>'Thiết kế Banner','set_order'=>FALSE))
			->add_column('service_package', array('set_select'=>FALSE,'title'=>'Gói dịch vụ'))
			->add_column('payment_percentage', array('set_select' => FALSE,'title'=> 'T/đ thanh toán'))
			// ->add_column('action', array('set_select'=>FALSE, 'title'=>'Actions', 'set_order'=>FALSE))
			
			->add_column_callback('term_name',function($data,$row_name){
				
				$term_id = $data['term_id'];
				$text = anchor(module_url("overview/{$term_id}"),$data['term_name'],"data-toggle='tooltip' title='Chi tiết'");
				$data['term_name'] = $text;

				return $data;
			},FALSE)
			->add_column_callback('content',function($data,$row_name){
					
					$term_id = $data['term_id'];
					$start_service_time = get_term_meta_value($term_id,'start_service_time');
					$end_service_time = get_term_meta_value($term_id,'end_service_time');

					$start_service_month = $this->mdate->startOfMonth($start_service_time);
					$end_service_month = $this->mdate->startOfMonth($end_service_time);

					$kpi_content = 0;
					$kpi_content_result = 0;


					while($start_service_month < $end_service_month)
					{
						$kpi_month = (int) $this->webcontent_kpi_m->get_kpi_value($term_id,'content', $start_service_month);
						$kpi_content+= $kpi_month;

						$kpi_month_result = (int) $this->webcontent_kpi_m->get_kpi_result($term_id,'content', $start_service_month);
						$kpi_content_result+= $kpi_month_result;

						$start_service_month = strtotime('+1 month',$start_service_month);
					}

					if(empty($kpi_content)) return $data;

					$content = '';
					$percent = div($kpi_content_result,$kpi_content)*100;
					$percent_predict =  $this->webcontent_seotraffic_m->predict_traffic($kpi_content_result,$start_service_time,$end_service_time,$kpi_content);

					if($percent_predict >= 50 && $percent_predict < 80)
						$progress_color = 'progress-bar-yellow';
					else if($percent_predict >= 80 && $percent_predict < 90)
						$progress_color = 'progress-bar-aqua';
					else if($percent_predict >= 90)
						$progress_color = 'progress-bar-green';
					else $progress_color = 'progress-bar-red';

					$kpi_content_result = numberformat($kpi_content_result);
					$kpi_content = numberformat($kpi_content);
					$data['content'] = '<div class="progress-group">
						<span class="progress-text" data-toggle="tooltip" title="Dự đoán đạt '.numberformat($percent_predict).'%">'.numberformat($percent).'%</span>
						<span class="progress-number"><b><span data-toggle="tooltip" title="Bài viết đã thực hiện">'.$kpi_content_result.'</span></b>/<span data-toggle="tooltip" title="KPI">'.$kpi_content.'</span></span>
						<div class="progress sm">
							<div class="progress-bar '.$progress_color.' progress-bar-striped" style="width: '.$percent.'%"></div>
						</div>
					</div>';

					return $data;
				},FALSE)
			->add_column_callback('banner',function($data,$row_name){

					$term_id = $data['term_id'];
					$start_time = get_term_meta_value($term_id,'start_service_time');
					$end_time = get_term_meta_value($term_id,'end_service_time');
					
					$service_package = get_term_meta_value($term_id, 'service_package');
					
					$author_banners = $this->webcontent_m
						->select('term_id,post_author,posts.post_title,count(posts.post_id) as number_of_banners')
					->where('posts.post_status','complete')
					->as_array()
					->group_by('post_author')
					->get_posts_from_date($start_time, $end_time,$term_id,'webcontent-task',array('meta_key'=>'task_type','meta_value'=>'banner'));

					$content_data_result = $author_banners ? array_sum(array_column($author_banners,'number_of_banners')) : 0;

					$kpi_banner = $this->webcontent_config_m->get_service_value($service_package, 'banner');
					$percent = ($kpi_banner == 0) ? 0 : (($content_data_result / $kpi_banner) * 100);
					$percent_predict =  $this->webcontent_seotraffic_m->predict_traffic($content_data_result,$start_time,$end_time,$kpi_banner);

					if($percent_predict >= 50 && $percent_predict < 80)
						$progress_color = 'progress-bar-yellow';
					else if($percent_predict >= 80 && $percent_predict < 90)
						$progress_color = 'progress-bar-aqua';
					else if($percent_predict >= 90)
						$progress_color = 'progress-bar-green';
					else $progress_color = 'progress-bar-red';

					$content_data_result = numberformat($content_data_result);
					$kpi_banner = numberformat($kpi_banner);
					$banner_content = '<div class="progress-group">
						<span class="progress-text" data-toggle="tooltip" title="Dự đoán đạt '.numberformat($percent_predict).'%">'.numberformat($percent).'%</span>
						<span class="progress-number"><b><span data-toggle="tooltip" title="Banner đã thực hiện">'.$content_data_result.'</span></b>/<span data-toggle="tooltip" title="KPI">'.$kpi_banner.'</span></span>
						<div class="progress sm">
							<div class="progress-bar '.$progress_color.' progress-bar-striped" style="width: '.$percent.'%"></div>
						</div>
					</div>';

					if(!empty($author_banners))
					{
						foreach ($author_banners as $author) 
						{
							$user_name = $this->admin_m->get_field_by_id($author['post_author'],"display_name");
							$banner_content.= ''.$user_name.': <span class="pull-right">'.$author['number_of_banners'].'</span><br>';
						}
					}
					
					$data['banner'] = $banner_content;

					return $data;
				},FALSE)
			->add_column_callback('service_package',function($data,$row_name){
				$data['service_package'] = $this->webcontent_config_m->get_package_name($data['term_id']);
				$kpis = $this->webcontent_kpi_m->get_kpis($data['term_id']);

				if(isset($kpis['users']['tech']))
				{
					$data['service_package'].= '<br>';
					foreach($kpis['users']['tech'] as $users)
					{
						foreach($users as $user_id => $vl)
						{
							$data['service_package'].= '<span class="label label-default">'.$this->admin_m->get_field_by_id($user_id,"display_name").'</span><br>';
						}
					}
				}
				return $data;
				},FALSE)
			->add_column_callback('payment_percentage',function($data,$row_name){
				$term_id = $data['term_id'];
				$invs_amount = (double)get_term_meta_value($term_id,'invs_amount');
				$payment_amount = (double)get_term_meta_value($term_id,'payment_amount');

				$payment_percentage = 100 * (double) get_term_meta_value($term_id,'payment_percentage');

				if($payment_percentage >= 50 && $payment_percentage < 80)
					$text_color = 'text-yellow';
				else if($payment_percentage >= 80 && $payment_percentage < 90)
					$text_color = 'text-light-blue';
				else if($payment_percentage >= 90)
					$text_color = 'text-green';
				else $text_color = 'text-red';

				$payment_percentage = currency_numberformat($payment_percentage,' %');
				$data['payment_percentage'] = "<b class='{$text_color}'>{$payment_percentage}</b>";
				return $data;
			},FALSE)
			->where('term_status','ending')
			->where('term.term_type', $this->term_type)
			->from('term')
			->group_by('term.term_id');

		$data['content'] = $this->admin_ui->generate(['per_page' => 100,'uri_segment' => 4,'base_url'=> module_url('all_done')]);

		parent::render($data,'blank');
	}
	//END WEBCONTENT DONE

	//STAFFS
		public function staffs()
	{
		restrict('Webcontent.staffs');
		$this->template->title->prepend('Bảng phân công phụ trách thực hiện');
		$data = $this->data;

		$data['content'] =
		$this->admin_ui	
		->select('term.term_id')
		->add_column('stt', array('set_select'=> FALSE, 'title'=> 'STT', 'set_order'=> FALSE))
		->add_column_callback('stt',function($data,$row_name){
			global $stt;
			$data['stt'] = (++$stt);
			return $data;
		},FALSE)
		->add_column('term.term_name',array('title'=>'Website','set_order'=>FALSE))
		->add_column_callback('term_name',function($data,$row_name){

			$term_id = $data['term_id'];
			$domain = $data['term_name'] ?: '<code>Chưa có website</code>';
			$data['term_name'] = anchor(module_url("kpi/{$term_id}"),$domain);

			return $data;
		},FALSE)
		->add_column('start_service_time', array('set_select'=>FALSE,'title'=>'T/G kích hoạt','set_order'=>FALSE))
		->add_column_callback('start_service_time',function($data,$row_name){
			$start_service_time = get_term_meta_value($data['term_id'],'start_service_time');
			$data['start_service_time'] = my_date($start_service_time);
			return $data;
		},FALSE)
		->add_column('tech_staff', array('set_select'=>FALSE,'title'=>'Phụ trách','set_order'=>FALSE))
		->add_column_callback('tech_staff',function($data,$row_name){

			$term_id = $data['term_id'];
			$kpis = $this->webcontent_kpi_m->get_kpis($term_id,'users',['post_facebook','fanpage_facebook']);
			if(empty($kpis)) return $data;

			$tech_staff = '';
			$user_ids = array();

			foreach ($kpis as $uids) 
			{
				foreach ($uids as $i => $val) 
				{
					$user_ids[$i] = $i;
				}
			}

			$data['tech_staff'] = implode(br(), array_map(function($user_id){
				$display_name = $this->admin_m->get_field_by_id($user_id,'display_name');
				return $display_name;

				$email_parts = explode('@', $email);
				return reset($email_parts);

			}, $user_ids));

			return $data;
		},FALSE)
		->add_column('kpi_content', array('set_select'=>FALSE,'title'=>'Nội dung','set_order'=>FALSE))
		->add_column_callback('kpi_content',function($data,$row_name){

			$term_id = $data['term_id'];
			$kpis = $this->webcontent_kpi_m->get_kpis($term_id,'users','content');
			if(empty($kpis)) return $data;
			$kpi_content = '';
			$user_ids = array();

			foreach ($kpis as $uids) 
			{
				foreach ($uids as $i => $val) 
				{
					$user_ids[$i] = $i;
				}
			}

			$data['kpi_content'] = implode(br(), array_map(function($user_id){
				$display_name = $this->admin_m->get_field_by_id($user_id,'display_name');
				return $display_name;	

			}, $user_ids));

			return $data;
		},FALSE)
		->add_column('sale_staff', array('set_select'=>FALSE,'title'=>'Kinh doanh','set_order'=>FALSE))
		->add_column_callback('sale_staff',function($data,$row_name){
			$staff_id = get_term_meta_value($data['term_id'],'staff_business');
            $sale_staff = $this->admin_m->get_field_by_id($staff_id,'display_name') ?: $this->admin_m->get_field_by_id($staff_id,'user_email');
            $data['sale_staff'] = $sale_staff ?: '<code>---</code>';
			return $data;
		},FALSE)
		->where('term.term_type', 'webcontent')
		->where_in('term.term_status',array('publish'))
		->from('term')

		//prd($this->admin_ui->get_query());
		->generate(array('per_page' => 10000,'uri_segment' => 4,'base_url'=> module_url('statistic')));

		$this->data = $data;
		parent::render($this->data, 'blank');
	}
	//END STAFFS


}