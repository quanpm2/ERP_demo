<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webcontent_mail_m extends Base_model {

	public $_table = 'mail_log';
	public $primary_key = 'mail_id';
	public $mail_type = 'webdoctor-email';

	function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->model('postmeta_m');
		$this->load->model('webdoctor_m');
		$this->load->model('term_posts_m');
		$this->load->model('term_m');
		$this->load->library('mdate');
	}
}