<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webcontent_keyword_rank_m extends Base_Model {

	public $_table = 'Webcontent_keyword_rank';
	public $primary_key = 'keyword_id';
	
}

/* End of file webgeneral_keyword_rank.php */
