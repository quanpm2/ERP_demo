<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webcontent_seotraffic_m extends Base_Model {

	public $ga_label = array(
		'ga:sessions' =>'Tổng visit',
		'Organic Search' => 'Organic Search',
		'Direct' => 'Direct',
		'Referral' => 'Referral',
		'Social'=> 'Social',
		'ga:bounceRate'=> 'Bounce Rate',
		'ga:pageviewsPerSession'=> 'Pages / Session',
		'ga:avgSessionDuration'=> 'Time On Site',
		'(Other)'=> 'Nguồn khác');

	function get_ga_label($label = '')
	{
		return isset($this->ga_label[$label]) ? $this->ga_label[$label] 
		: 'Không xác định';
	}

	function predict_traffic($curent_total = 0, $curent_day= 0, $total_day = 0, $total = 0, $type = 'percent')
		{
			if(!$total || $total == 0)
				return 0;
			if(!$curent_day || $curent_day == 0)
				return 0;

			$avg = $curent_total/$curent_day;
			$result = $avg * $total_day;
			if($type == 'percent')
			{
				$result = $result / $total;
				$result = round($result, 4) * 100;
			}
			return round($result, 2);
			
		}
		
	function reload_account_ga($type = false)
	{
		// $this->load->library('ga');
		$results = array();
		$results[] = '----Chọn-----';

		//get all account
		if($type || !($accounts = $this->scache->get('modules/seotraffic/ga-listAccount')))
		{
			$items = $this->ga->getListAccount();
			$accounts = array();
			if($items)
				foreach($items as $item)
				{
					$id = $item->getId();
					$accounts[$id] = array();
					$accounts[$id]['id'] = $id;
					$accounts[$id]['name'] = $item->getName();
				}
				$this->scache->write($accounts,'modules/seotraffic/ga-listAccount');
			}
			if(!$accounts)
				return null;
			//get list webproperties
			$webproperties = array();
			foreach($accounts as $account)
			{
				$accountId = $account['id'];
				$key_cache = 'modules/seotraffic/ga-property-'.$accountId;
				if(!$webproperties[$accountId] = $this->scache->get($key_cache))
				{
					$items = $this->ga->getListWebproperties($accountId);
					$webproperties[$accountId] = array();
					if($items)
					{
						foreach($items as $item)
						{
							$id = $item->getId();
							$webproperties[$accountId][$id] = array();
							$webproperties[$accountId][$id]['id'] = $id;
							$webproperties[$accountId][$id]['name'] = $item->getName();
						}
						$this->scache->write($webproperties[$accountId], $key_cache);
					}
					
				}
			}
			if(!$webproperties)
			{
				return null;
			}
			$profiles = array();

			foreach($webproperties as $accountId =>$webproperty)
			{
				foreach($webproperty as $web)
				{
					$id = $web['id'];
					$name = $web['name'];
					$key_cache = 'modules/seotraffic/ga-profile-'.$accountId.'-'.$id;

					if(!$profiles = $this->scache->get($key_cache))
					{
							// $profiles[$accountId][$id] = array();
						$listProfiles = $this->ga->getListProfiles($accountId, $id);

						if($listProfiles)
						{
							foreach($listProfiles as $i =>$item)
							{
								$tmp = array();
								$tmp['id'] = $item->getId();
								$tmp['name'] = $item->getName();
								$tmp['websiteUrl'] = $item->getWebsiteUrl();
								$profiles[] = $tmp;
							}
							$this->scache->write($profiles, $key_cache);
						}
					}


					if($profiles)
					{
						foreach($profiles as $profile)
						{
							$results[$profile['id']] = $profile['name'].' - '.$profile['websiteUrl'];
						}
					}

				}
			}
			return $results;
		}

		public function get_ga($start_date, $end_date, $ga_profile_id)
		{
			$ga = $this->get_ga_value($start_date, $end_date, $ga_profile_id);
			if(!$ga)
				return FALSE;

			$data = array();
			$data['ga'] = array();
			$data['ga']['results'] = array();
			$data['ga']['total'] = array();
			$data['ga']['headers'] = array_keys($ga['results']);

			foreach($ga['results'] as $date => $results)
			{
				foreach($results as $key=>$result)
				{
					$tmp_key = $key;
					switch ($key) {
						case 'ga:avgSessionDuration':
						$result = gmdate('H:i:s',$result);
					// $key = 'Time on site';
						break;
						case 'ga:pageviewsPerSession':
						$result = round($result, 2);
						break;

						case 'ga:bounceRate':
						$result = (int)$result.'%';
					// $key = 'Bounce Rate';
						break;
						case 'ga:sessions':
					// $key = 'Tổng visit';
						break;
						case 'Organic Search':
						case 'Referral':
						case 'Social':
						case '(Other)':
						break;

						default:

						break;
					}

					if(isset($ga['totalsForAllResults'][$tmp_key]))
					{
						$val = $ga['totalsForAllResults'][$tmp_key];
						unset($ga['totalsForAllResults'][$tmp_key]);
						switch ($tmp_key) {
							case 'ga:avgSessionDuration':
							$val = gmdate('H:i:s',$val);
							break;
							case 'ga:bounceRate':
							$val = (int)$val.'%';
							break;
							case 'ga:pageviewsPerSession':
							$val = round($val, 2);
							break;
							default:

							break;
						}
						$data['ga']['total'][$key] = $val;
					}
					$data['ga']['results'][$date][$key] = $result;
				}
			}
			return $data['ga'];
		}
		function get_ga_value($start_date, $end_date, $profileId)
		{
		/*
		Organic Search
		Direct
		Referral
		Social
		-----------
		Acquisition Overview


		Tổng visit
		Page/ Visit
		Bounce Rate
		Time on site
		----------
		Audience Overview
		*/

		if(!$profileId)
			return FALSE;
		$key_cache = 'ga/'.$profileId.'/'.$start_date.'-'.$end_date;
		if($ga = $this->scache->get($key_cache))
		{
			return $ga; //return cache
		}

		$this->load->add_package_path(APPPATH.'third_party/google-analytics', FALSE);
		// $this->load->library('ga');
		$data = array();
		if(strpos($profileId, 'ga:') === FALSE)
			$profileId = 'ga:'.$profileId;

		//Audience Overview
		$metrics = "ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession";
		$optParams = array("dimensions" => "ga:date");

		try{
			$ga_aud = $this->ga->get($profileId, $start_date,$end_date,$metrics,$optParams)->results();
		}
		catch(Google_Service_Exception $e)
		{
			// echo 'There was an Analytics API service error '. $e->getCode() . ':' . $e->getMessage();
			
			$this->load->remove_package_path(APPPATH.'third_party/google-analytics');
			return FALSE;
		}
		//Acquisition Overview
		$metrics = "ga:sessions";
		$optParams = array("dimensions" => "ga:date,ga:channelGrouping");
		$ga_acq = $this->ga->get($profileId, $start_date,$end_date,$metrics,$optParams)->results();

		$data['ga'] = array();
		$data['ga']['results'] = array();
		$data['ga']['totalsForAllResults'] = array();
		if($ga_aud->rows)
		{
			foreach($ga_aud->rows as $row)
			{
				$date = $row[0];
				unset($row[0]);
				$data['ga']['results'][$date] = array();
				$data['ga']['results'][$date]['ga:sessions'] = $row[1];
				$data['ga']['results'][$date]['ga:bounceRate'] = $row[2];
				$data['ga']['results'][$date]['ga:avgSessionDuration'] = $row[3];
				$data['ga']['results'][$date]['ga:pageviewsPerSession'] = $row[4];
			}
		}
		foreach($ga_aud->totalsForAllResults as $key=>$row)
		{
			$data['ga']['totalsForAllResults'][$key] = $row;
		}
		if($ga_acq->rows)
		{
			foreach($ga_acq->rows as $i=>$row)
			{
				$date = $row[0];
				$name = $row[1];
				$val = $row[2];
				$data['ga']['results'][$date][$name] = $val;
				if(!isset($data['ga']['totalsForAllResults'][$name]))
					$data['ga']['totalsForAllResults'][$name] = 0;
				$data['ga']['totalsForAllResults'][$name]+= $val;
			}
		}
		$this->load->remove_package_path(APPPATH.'third_party/google-analytics');

		$time_cache = 0;
		if($this->mdate->startOfDay($end_date) >= $this->mdate->startOfDay())
				$time_cache = 1800; //cache 30 phút
			$this->scache->write($data['ga'],$key_cache, $time_cache);
			
			return $data['ga'];
		}
		function encrypt($string, $term, $type='encode')
		{
			
			$this->load->library('encrypt');
			if(!$term)
				return FALSE;
			if(is_object($term))
			{
				$term_id = $term->term_id;
			}
			else if(is_array($term))
			{
				$term_id = $term['term_id'];
			}
			else
			{
				$term_id = $term;
			}
			$key = 'webdoctor-'.$term_id;

			if($type == 'encode')
				return $this->encrypt->encode($term_id,$key);

			return $this->encrypt->decode($string,$key);
		}
	}

	/* End of file seotraffic_report_m.php */
/* Location: ./application/modules/seotraffic/models/seotraffic_report_m.php */