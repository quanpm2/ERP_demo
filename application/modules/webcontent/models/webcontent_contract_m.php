<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webcontent_contract_m extends Base_contract_m {

	function __construct() 
	{
		parent::__construct();
	}

	public function preview($term_id = 0)
	{
		$term 								 = $this->term_m->get($term_id);
		$data 								 = array();
		$data['term'] 						 = $term;
		$extra 								 = @unserialize(get_term_meta_value($term_id,'extra'));
		$term->extra 						 = $extra;

		$gender 							 = get_term_meta_value($term_id,'representative_gender');
		$data['data_customer'] 				 = array();
		$data['data_customer']['Địa chỉ']    = get_term_meta_value($term_id, 'representative_address');
		$data['data_customer']['Điện thoại'] = get_term_meta_value($term_id, 'representative_phone');
		$data['data_customer']['Mã số thuế'] = $extra['customer_tax'] ?? '';
		$data['data_customer']['Đại diện']   = ($gender == 1 ? 'Ông' : 'Bà').' '.get_term_meta_value($term->term_id,'representative_name');
		$data['data_customer']['Chức vụ'] 	 = get_term_meta_value($term_id, 'representative_position');

		$customer 							 = $this->customer_m->get_customers($term_id);
		$data['customer'] 					 = isset($customer[0]) ? $customer[0] : null;
		$data['term_id'] 					 = $term_id;
		$data['time'] 						 = time();		
		$data['contract_value'] 			 = (int)get_term_meta_value($term->term_id,'contract_value');
		$data['vat'] 						 = get_term_meta_value($term->term_id,'vat');	
		$is_tranfer_company 				 = ($data['vat'] > 0);
		$data['bank_info'] 					 = $this->config->item(($is_tranfer_company?'company':'person'),'bank_infos');

		$printable_title = $this->config->item($term->term_type,'printable_title');
		$data['printable_title'] = $printable_title ?: $this->config->item($term->term_type,'printable_title');

		$data['company_name'] 				  = $this->config->item('company','contract_represent');
		
		$data['term_type'] = $term->term_type ;
		$data['data_represent'] = array();
		$data['data_represent']['Địa chỉ'] = $this->config->item('address','contract_represent');
		$data['data_represent']['Điện thoại'] = $this->config->item('phone','contract_represent');
		$data['data_represent']['Mã số thuế'] = $this->config->item('tax','contract_represent');
		$data['data_represent']['Đại diện'] = $this->config->item('name','contract_represent');
		$data['data_represent']['Chức vụ'] = $this->config->item('position','contract_represent');


		$service_type = force_var(get_term_meta_value($term_id,'service_type'),'cpc_type');
		$html = $this->load->view('webcontent/contract/preview', $data ,TRUE);
		return $html;
	}

}

/* End of file banner_report_m.php */
/* Location: ./application/modules/banner/models/banner_report_m.php */