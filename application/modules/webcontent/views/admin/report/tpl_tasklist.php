<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>[WEBDOCTOR.VN] Danh sách công việc dự kiến thực hiện</title>
</head>

<body>
<div style="padding:0px;margin:0px;background:#e6e6e6">
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
      <tr>
        <td bgcolor="#fff" height="50"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
              <tr>
                <td width="30%" height="50"><img alt="logo Webdoctor" src="http://webdoctor.vn/images/webdoctor-logo.png" style="margin-bottom: 4px;"><br></td>
                <td width="50%"><div>
                    <p style="font-family:tahoma;font-size:12px;color:#b8b8b8;text-align:right">HOTLINE:<br>
                      Email:<br>
                    </p>
                  </div></td>
                <td width="20%" style="border-left:5px #fff solid"><div>
                    <p style="font-family:tahoma;font-size:12px;color:#333;text-align:left">028.7300 4488<br>
                      sales@webdoctor.vn<br>
                    </p>
                  </div></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
      <tr>
        <td bgcolor="#0072bc" style="border-top:#0072bc 2px solid"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="border-top:#0072bc 10px solid;border-bottom:#0072bc 10px solid">
            <tbody>
              <tr>
                <td>
                <h1 style="font-family:tahoma;color:#ffffff;font-size:30px;margin:5px 0">Danh sách công việc dự kiến thực hiện
                </h1>
                <p style="font-family:tahoma;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng: <span style="font-weight:bold">
                  <?php echo get_term_meta_value($term->term_id,'contract_code');?>
                </span></p></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
      <tr>
        <td bgcolor="#e6e6e6"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
              <tr>
                <td valign="top" height="18"><img alt="arrow down" src="http://webdoctor.vn/images/arrowdown.png"></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-bottom:20px #e6e6e6 solid">
    <tbody>
      <tr>
        <td bgcolor="#e6e6e6"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:10px #e6e6e6 solid">
            <tbody>
              <tr>
                <td><p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Kính chào Quý khách</p>
                  <p style="font-family:tahoma;font-size:12px;color:#363636;line-height:18px">WebDoctor.vn xin thông báo các công việc dự kiến thực hiện Webdoctor cho <?php echo $term->term_name;?></p></td>
              </tr>
            </tbody>
          </table>
          <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border-bottom:20px #ffffff solid;border-top:20px #ffffff solid;border-left:20px #ffffff solid;border-right:20px #ffffff solid;background:#ffffff">
            <tbody>
              <tr>
                <td bgcolor="#ffffff"><p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Chi tiết công việc</p>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                    <?php
                    $odd = true;
                    foreach($tasks as $task){
                      $odd_row = $odd ? 'bgcolor="#f2f2f8"' : '';
                      $odd = !$odd;
                    ?>  
                    <tr <?php echo $odd_row;?> >
                      <td height="25">
                        <div align="left" style="min-width:149px;font-family:tahoma;font-size:12px;color:#363636">
                          <?php echo $task->post_title;?>
                        </div>
                      </td>
                      <td>
                        <div align="center" style="font-weight:bold; font-family:tahoma;font-size:12px;color:#363636">
                        <?php echo my_date($task->start_date,'d/m/Y').' - '.my_date($task->end_date,'d/m/Y');?>
                        </div>
                      </td>
                    </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                  </table>
                  </td>
              </tr>
            </tbody>
          </table>
<!--           <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:10px #e6e6e6 solid">
            <tbody>
              <tr>
                <td><span style="font-family:tahoma;font-size:12px;color:#363636;line-height:18px">* WebDoctor.vn sẽ gửi báo cáo SMS đến Quý khách sau khi hoàn tất mỗi đầu việc.<br />
* Báo cáo tổng hợp (đầu việc thực hiện, bài viết mới, lượt truy cập, ....) sẽ gửi Quý khách vào thứ 2 hàng tuần. 
* Lưu ý: đây chỉ là bản dự kiến ban đầu, chúng tôi có thể thay đổi theo tình hình và nhu cầu cụ thể. <br />
</span></td>
              </tr>
            </tbody>
          </table> -->
          </td>
      </tr>

      <tr>
        <td bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6;border-right: 20px solid #e6e6e6;  ">
          <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
              <tr>
                <td bgcolor="#0072bc" ><table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px"   >
                  <tbody>
                    <tr>
                      <td><p style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:16px ; font-weight:bold;margin:10px 20px">Webdoctor.vn cần 1 số thông tin để phục vụ cho việc chăm sóc website </p></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td bgcolor="#e6e6e6"><table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px">
                <tbody>
                  <tr>
                    <td valign="top"><img alt="arrow down" src="http://webdoctor.vn/images/arrowdown.png"></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                <tbody>
                  <tr>
                    <td bgcolor="#ffffff" style="font-size: 13px;
                    color: #363636;
                    font-family: Arial, Helvetica, sans-serif;" ><p><strong>1. Tài khoản webmaster tools : </strong>Anh/Chị share giúp Wedoctor.vn vào tài khoản mail <font style="color:#03F">
                    <?php echo mailto('google-analytics@erp-webdoctor.iam.gserviceaccount.com');?>
                    </font> => việc này giúp bên em theo dõi những lỗi và những cập nhật mới từ google cho site chúng ta. </p>
                    <p ><strong>2. Tài khoản Goolge analytics : </strong>Anh/chị  share giúp Webdoctor.vn vào tk mail <font style="color:#03F">
                    <?php echo mailto('google-analytics@erp-webdoctor.iam.gserviceaccount.com');?>
                    </font> => Việc này giúp bên Webdoctor.vn theo dõi tính trang visit của site và biết được khách hàng đang tập trung quan tâm nội dung nào trên trang. </p>
                    <p ><strong>3. Tài khoản admin đăng nhập. </strong></p>
                    <p >Cảm ơn anh/chị ! </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
    </tr>

      
      
    </tbody>
  </table>
  
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
      <tr>
        <td bgcolor="#2d2b2b"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:#2d2b2b 10px solid">
            <tbody>
              <tr>
                <td height="30" colspan="5"><p style="font-family:tahoma;font-size:14px;font-weight:bold;color:#b8b8b8">WebDoctor.vn</p></td>
              </tr>
              <tr>
                <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td width="53"><p style="font-family:tahoma;font-size:12px;color:#b8b8b8;line-height:18px">Add<br>
                            Tell<br>
                            Email</p></td>
                        <td width="587"><p style="font-family:tahoma;font-size:12px;color:#b8b8b8;line-height:18px">Tầng 8, 402 Nguyễn Thị Minh Khai , Phường 5, Quận 3, TP.HCM, Việt Nam<br>
                            (028) 7300.4488 <br>
                            <a target="_blank" href="mailto:sales@webdoctor.vn" style="color:#b8b8b8;text-decoration:none">sales@webdoctor.vn</a></p></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  <div class="yj6qo"></div>
  <div class="adL"> </div>
</div>
</body>
</html>
