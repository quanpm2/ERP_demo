  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>[WEBDOCTOR.VN] Thông báo dịch vụ đã được kích hoạt - webste <?php echo $term->term_name;?></title>
  </head>
  <body>
  <div style="padding:0px;margin:0px;background:#e6e6e6">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td bgcolor="#fff" height="50"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
                <tr>
                  <td width="30%" height="50">
                    <a href="http://webdoctor.vn" target="_blank"> 
                      <img alt="logo Webdoctor" src="http://webdoctor.vn/images/webdoctor-logo.png">
                    </a>
                  </td>
                  <td width="50%">
                    <div>
                      <p style="font-family:tahoma;font-size:12px;color:#b8b8b8;text-align:right">HOTLINE:<br>
                        Email:<br>
                      </p>
                    </div>
                  </td>
                  <td width="20%" style="border-left:5px #fff solid"><div>
                      <p style="font-family:tahoma;font-size:12px;color:#333;text-align:left">028.7300 4488<br>
                        sales@webdoctor.vn<br>
                      </p>
                    </div></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td bgcolor="#0072bc" style="border-top:#0072bc 2px solid"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="border-top:#0072bc 10px solid;border-bottom:#0072bc 10px solid">
              <tbody>
                <tr>
                  <td><h1 style="font-family:tahoma;color:#ffffff;font-size:30px;margin:5px 0">Thông báo kích hoạt dịch vụ Webdoctor</h1>
                    <p style="font-family:tahoma;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng: 
                      <span style="font-weight:bold">
                      <?php echo get_term_meta_value($term->term_id,'contract_code');?>
                      </span> 
                    </p>
                    <p style="font-family:tahoma;color:#ffffff;margin:5px 0;font-size:14px">
                      Thời gian thực hiện từ: <span style="font-weight:bold"><?php echo $duration_time;?></span> 
                    </p>
                  </td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td bgcolor="#e6e6e6"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
                <tr>
                  <td valign="top" height="18"><img alt="arrow down" src="http://webdoctor.vn/images/arrowdown.png"></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-bottom:20px #e6e6e6 solid">
      <tbody>
        <tr>
          <td bgcolor="#e6e6e6"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:10px #e6e6e6 solid">
              <tbody>
                <tr>
                  <td><p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Kính chào Quý khách</p>
                    <p style="font-family:tahoma;font-size:12px;color:#363636;line-height:18px">WebDoctor.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>
                    <p style="font-family:tahoma;font-size:12px;color:#363636;line-height:18px">
                      WebDoctor.vn gửi email thông báo kích hoạt & thực hiện hợp đồng webdoctor cho
                      <?php echo $term->term_name;?>
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
            <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border-bottom:20px #ffffff solid;border-top:20px #ffffff solid;border-left:20px #ffffff solid;border-right:20px #ffffff solid;background:#ffffff">
              <tbody>
                <tr>
                  <td bgcolor="#ffffff"><p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Thông tin khách hàng</p>
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                        <tr bgcolor="#f2f2f8">
                          <td width="150" height="25"><div align="left" style="min-width:149px;font-family:tahoma;font-size:12px;color:#363636">Tên khách hàng</div></td>
                          <td width="449" style="word-wrap:break-word">
                            <div align="left" style="max-width:445px;word-wrap:break-word;text-align:left;font-family:tahoma;font-size:12px;color:#363636;font-weight:bold">: 
                            <?php echo $representative_name;?>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td height="25"><div align="left" style="min-width:149px;font-family:tahoma;font-size:12px;color:#363636">Email</div></td>
                          <td>
                            <?php
                            $representative_email = get_term_meta_value($term->term_id, 'representative_email');
                            ?>
                            <div align="left" style="font-family:tahoma;font-size:12px;color:#363636;font-weight:bold">: 
                              <a target="_blank" href="mailto:<?php echo $representative_email;?>">
                                <?php echo $representative_email;?>
                              </a>
                            </div>
                          </td>
                        </tr>
                        <tr bgcolor="#f2f2f8">
                          <td height="25"><div align="left" style="min-width:149px;font-family:tahoma;font-size:12px;color:#363636">Điện thoại di động</div></td>
                          <td>
                            <div align="left" style="font-family:tahoma;font-size:12px;color:#363636;font-weight:bold">: 
                              <?php echo get_term_meta_value($term->term_id, 'representative_phone');?>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td height="25"><div align="left" style="min-width:149px;font-family:tahoma;font-size:12px;color:#363636">Địa chỉ</div></td>
                          <td style="word-wrap:break-word"><div align="left" style="max-width:445px;word-wrap:break-word;text-align:left;font-family:tahoma;font-size:12px;color:#363636;font-weight:bold">: 
                          <?php echo get_term_meta_value($term->term_id, 'representative_address');?>
                          </div></td>
                        </tr>
                      </tbody>
                    </table>
                    <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Nhân sự thực hiện (HOTLINE: 028.6273.6363)</p>
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                        <tr bgcolor="#f2f2f8">
                          <td width="150" height="25"><div align="left" style="min-width:149px;font-family:tahoma;font-size:12px;color:#363636">Phụ trách tối ưu website</div></td>
                          <td width="449" style="word-wrap:break-word"><div align="left" style="max-width:445px;word-wrap:break-word;text-align:left;font-family:tahoma;font-size:12px;color:#363636;font-weight:bold">: Mr.Hiệu | Điện thoại: <strong>0938.116.155</strong> | Email: <a href="mailto:hieuth@webdoctor.vn">hieuth@webdoctor.vn</a></div></td>
                        </tr>
                        <tr>
                          <td width="150" height="25"><div align="left" style="min-width:149px;font-family:tahoma;font-size:12px;color:#363636">Phụ trách nội dung</div></td>
                          <td width="449" style="word-wrap:break-word"><div align="left" style="max-width:445px;word-wrap:break-word;text-align:left;font-family:tahoma;font-size:12px;color:#363636;font-weight:bold">: Ms.Hồng | Điện thoại: <strong>0986.202.059</strong> | Email: <a href="mailto:hongvtk@webdoctor.vn">hongvtk@webdoctor.vn</a></div></td>
                        </tr>
                        <tr bgcolor="#f2f2f8">
                          <td width="150" height="25"><div align="left" style="min-width:149px;font-family:tahoma;font-size:12px;color:#363636">Phụ trách kỹ thuật</div></td>
                          <td width="449" style="word-wrap:break-word"><div align="left" style="max-width:445px;word-wrap:break-word;text-align:left;font-family:tahoma;font-size:12px;color:#363636;font-weight:bold">: Mr.Long | Điện thoại: <strong>093.759.7030</strong> | Email: <a href="mailto:longnq@webdoctor.vn">longnq@webdoctor.vn</a></div></td>
                        </tr>
                      </tbody>
                    </table>

                    <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">
                      Thông tin báo cáo công việc website <?php echo $term->term_name;?>
                    </p>

                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                        <tr bgcolor="#f2f2f8">
                          <td width="150" height="25"><div align="left" style="min-width:149px;font-family:tahoma;font-size:12px;color:#363636">Nhận báo cáo</div></td>
                          <td width="449" style="word-wrap:break-word">
                            <div align="left" style="max-width:445px;word-wrap:break-word;text-align:left;font-family:tahoma;font-size:12px;color:#363636;font-weight:bold">: 
                            <?php echo $representative_name;?>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td width="150" height="25"><div align="left" style="min-width:149px;font-family:tahoma;font-size:12px;color:#363636">SMS</div></td>
                          <td width="449" style="word-wrap:break-word">
                            <div align="left" style="max-width:445px;word-wrap:break-word;text-align:left;font-family:tahoma;font-size:12px;color:#363636;font-weight:bold">: 
                              <span style="font-family:tahoma;font-size:12px;color:#363636;font-weight:bold">
                                <?php echo (($phone = get_term_meta_value($term->term_id,'phone_report')) ? $phone : 'Chưa có');?>
                              </span>
                            </div>
                          </td>
                        </tr>
                        <tr bgcolor="#f2f2f8">
                          <td width="150" height="25"><div align="left" style="min-width:149px;font-family:tahoma;font-size:12px;color:#363636">Email</div></td>
                          <td width="449" style="word-wrap:break-word"><div align="left" style="font-family:tahoma;font-size:12px;color:#363636;font-weight:bold">: <?php echo (($mail_report = get_term_meta_value($term->term_id,'mail_report')) ? $mail_report : 'Chưa có');?>
                          </div></td>
                        </tr>
                      </tbody>
                    </table></td>
                </tr>
              </tbody>
            </table>
            <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:10px #e6e6e6 solid">
              <tbody>
                <tr>
                  <td><span style="font-family:tahoma;font-size:12px;color:#363636;line-height:18px">* WebDoctor.vn sẽ gửi báo cáo SMS đến Quý khách sau khi hoàn tất mỗi đầu việc vào ngày hôm sau.<br />
                    * Báo cáo tổng hợp (đầu việc thực hiện, bài viết mới, lượt truy cập, ....) sẽ gửi Quý khách vào thứ 2 hàng tuần. </span></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td bgcolor="#0072bc" style="border-top:#0072bc 2px solid"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="border-top:#0072bc 10px solid;border-bottom:#0072bc 10px solid">
              <tbody>
                <tr>
                  <td><h1 style="font-family:tahoma;color:#ffffff;font-size:16px;margin:5px 0">Webdoctor.vn cần 1 số thông tin để phục vụ cho việc chăm sóc website : </h1></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td bgcolor="#e6e6e6"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
                <tr>
                  <td valign="top" height="18"><img alt="arrow down" src="http://webdoctor.vn/images/arrowdown.png"></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td><table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border-bottom:20px #ffffff solid;border-top:20px #ffffff solid;border-left:20px #ffffff solid;border-right:20px #ffffff solid;background:#ffffff">
              <tbody>
                <tr>
                  <td bgcolor="#ffffff"><p style="font-family:tahoma;font-size:12px;color:#363636"><strong>1. Tài khoản Webmaster tools : </strong>Anh/Chị share giúp Webmaster tools vào tài khoản mail <b>seo@webdoctor.vn</b>, việc này giúp bên em theo dõi những lỗi và những cập nhật mới từ Google cho site chúng ta.
                    </p>
                    <p style="font-family:tahoma;font-size:12px;color:#363636"><strong>2. Tài khoản Goolge analytics : </strong>Anh/chị  share giúp Webdoctor.vn vào tài khoản mail <b>google-analytics@erp-webdoctor.iam.gserviceaccount.com</b> và <b>seo@webdoctor.vn</b>, Việc này giúp bên Webdoctor.vn theo dõi tính trang visit của site và biết được kh đang tập trung quan tâm nội dung nào trên trang.
                    </p>
                    <p style="font-family:tahoma;font-size:12px;color:#363636"><strong>3. Tài khoản admin đăng nhập.
                    </strong></p>
                    <p style="font-family:tahoma;font-size:12px;color:#363636">Cảm ơn anh/chị !                </p>
                  </td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td bgcolor="#2d2b2b"><table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:#2d2b2b 10px solid">
              <tbody>
                <tr>
                  <td height="30" colspan="5"><p style="font-family:tahoma;font-size:14px;font-weight:bold;color:#b8b8b8">WebDoctor.vn - Chăm sóc website chuyên nghiệp</p></td>
                </tr>
                <tr>
                  <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                        <tr>
                          <td width="53"><p style="font-family:tahoma;font-size:12px;color:#b8b8b8;line-height:18px">Addr:<br>
                              Tell:<br>
                              Email:</p></td>
                          <td width="587"><p style="font-family:tahoma;font-size:12px;color:#b8b8b8;line-height:18px">Tầng 8, 402 Nguyễn Thị Minh Khai , Phường 5, Quận 3, TP.HCM, Việt Nam<br>
                              (08) 7300.4488 <br>
                              <a target="_blank" href="mailto:sales@webdoctor.vn" style="color:#b8b8b8;text-decoration:none">sales@webdoctor.vn</a></p></td>
                        </tr>
                      </tbody>
                    </table></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table>
    <div class="yj6qo"></div>
    <div class="adL"> </div>
  </div>
  </body>
  </html>
