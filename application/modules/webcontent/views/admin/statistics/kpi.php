<?php echo $this->admin_form->form_open(); ?>
<div class="form-group col-md-3">
  <label>Tháng / Năm:</label>
  <div class="input-group date">
    <div class="input-group-addon">
      <i class="fa fa-calendar"></i>
    </div>
    <input type="text" class="form-control pull-right" id="datepicker" value="<?php echo my_date($time,'m/Y'); ?>"/>
    <input type="hidden" name="time"/>
  </div>
  <!-- /.input group -->
</div>
<?php echo $this->admin_form->form_close(); ?>
<?php echo $content ?? ''; ?>
<script type="text/javascript">
$(function(){

  $('#datepicker').datepicker({
    format: "mm/yyyy",
  })
  .on('changeDate',function(ev){
    $("input[name='time']").val(new Date(ev.date).getTime()/1000);
    $(this).closest('form').submit();
  });
})
</script>