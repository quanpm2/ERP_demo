<?php
$this->template->javascript->add('plugins/validate/jquery.validate.js');
$this->template->stylesheet->add('plugins/bootstrap-fileinput/css/fileinput.css');
$this->template->javascript->add('plugins/bootstrap-fileinput/js/fileinput.min.js');
?>
<button type="button" id="add_task" name="add_new" class="btn btn-info pull-right btn-add-new" data-toggle="modal" data-target="#importModal"><i class="glyphicon glyphicon-plus"></i>Import backlink</button> 

<!-- Import Modal -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="importModalLabel">Import Backlink<span id="span-id"></span></h4>
			</div>
			<?php echo $this->admin_form->form_open();?>		
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
					<?php
					$this->table->set_caption('Upload Backlink');
					$this->table->set_heading('Ngày','Upload');
					$day = 3;
					$dropdown_day = array();
					for ($day; $day >= 0; $day--)
					{
						$time = strtotime('-'.$day.' day');
						$date_drp[$time] = my_date($time,'d/m/Y');
					}

					// $upload_attrs['data-time'] = $time;
					$upload_attrs = array('class'=>'import_file','multiple'=>'multiple','none_label'=>TRUE);

					$upload_html = $this->admin_form->upload('','fileinput','','',$upload_attrs);

					$dropdown = $this->admin_form->dropdown('', 'created_on', $date_drp,my_date(time(),'d/m/Y'));
					$row = array(
							array('data'=>$dropdown,'width'=>'10%'),
							array('data'=>$upload_html)
							);

					$this->table->add_row($row);
					echo $this->table->generate();

					echo '<div id="input_keywords" style="display:none">';
					echo $this->admin_form->input('keywords','keywords','','Mỗi keyword cách nhau bởi dấu [,]');
					echo '</div';

					echo $this->admin_form->hidden('','import_file','','',array('id'=>'import_file'));
					?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" name="import" id="import_submit" class="btn btn-primary disabled" value="Import Dữ liệu">Import dữ liệu</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
			</div>
			<?php echo $this->admin_form->form_close();?>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){

	$("#import_submit").click(function(){
		if($(this).hasClass('disabled')) return false;
	});
	
	$(".import_file").fileinput({
	    uploadUrl: admin_url + 'backlink/ajax/upload_backlinks',
	    uploadAsync: true,
	    maxFileCount: 1,
	    showPreview: false,
	    showRemove: false,
    	uploadExtraData: {
	        term_id: '<?php echo $term_id;?>',
	        term_name : '<?php echo $term->term_name;?>'
	    },
	    allowedFileExtensions: ['xls','xlsx','txt'],
	})
	.on("fileuploaded", function(event, data, previewId, index){

		var resdata = data.response;
	    var btn = $("#import_submit");

	    if(resdata.success == false || resdata.file_name == ''){
	    	if(!btn.hasClass("disabled")) btn.addClass("disabled");
	    	return false;	
	    }

	    
		var fileExtension = resdata.file_name.substr((resdata.file_name.lastIndexOf('.') + 1));
		if(fileExtension == 'txt')
		{
			$("#input_keywords").show();
		}
		else
		{
			$("#input_keywords").hide();
		}

	    $("#import_file").val(resdata.file_name);
	    if(btn.hasClass("disabled")) btn.removeClass("disabled");
	});

});
</script>