<?php
$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

$this->load->view('upload');

echo anchor(admin_url('backlink/cron'),'Click vào đây để Hệ thống duyệt backlink','target="_blank"');

echo '<div style="clear: both;"></div>';
echo $content['table'];
echo $content['pagination'];
?>
<script type="text/javascript">
$(document).ready(function()
{
	$('.pingomatic').click(function(e){
		var id = $(this).data('blink_id');
		$.ajax({
			url: '<?php echo module_url();?>ajax/pingomatic/'+ id,
			type : 'POST',
			dataType: 'JSON', 
			success: function(response){
				if(response.success)
					$.notify(response.msg, "info");
				else $.notify(response.msg, "error");
			}
		});
		e.preventDefault();
		return false;
	});

	$(".input_daterange").daterangepicker({format: 'DD-MM-YYYY'});
});
</script>