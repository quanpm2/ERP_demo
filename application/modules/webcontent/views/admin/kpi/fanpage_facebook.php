<?php

$this->table->set_heading(array(
    'Tháng',
    'Tên',
    'Số lượng sản phẩm',
    'Thực hiện',
    'Hành động'
));

$this->table->set_caption('KPI fanpage facebook');
$rows_id     = array();
$data        = array();
$start_month = $this->mdate->startOfMonth();
if(!empty($targets['fanpage_facebook'])){
    foreach ($targets['fanpage_facebook'] as $i => $value) {
       
        $row_id = $value['kpi_datetime'];
        if (!isset($rows_id[$row_id]))
            $rows_id[$row_id] = 1;
        else
            $rows_id[$row_id]++;
        
        
        $btn_delete = (strtotime($value['kpi_datetime']) >= $start_month) ? anchor(module_url('kpi/' . $term_id . '/' . $value['kpi_id'] . '/delete'), 'Xóa', 'class="btn btn-danger btn-flat btn-xs"') : '';
        $real       = $this->webcontent_kpi_m->get_kpi_result($term_id, 'fanpage_facebook', strtotime($value['kpi_datetime']), $value['user_id']);
        $real       = ($real < $value['kpi_value']) ? '<span class="label label-danger">' . $real . '</span>' : '<span class="label label-success">' . $real . '</span>';
        
        $username = $this->admin_m->get_field_by_id($value['user_id'], 'display_name');
        $data[]   = array(
            $row_id,
            $username,
            $value['kpi_value'],
            $real,
            $btn_delete
        );
    }
    //btn btn-danger btn-flat btn-xs /$value['utarget_id']
    $is_rowspan = FALSE;
    if ($data)
        foreach ($data as $i => $value) {
            $row_id   = $value[0];
            // $row_id = $this->mdate->convert_time($row_id);
            $rowspan  = $rows_id[$row_id];
            $value[0] = $this->mdate->date('Y/m', $row_id);
            
            if ($rowspan > 1 && $is_rowspan === FALSE) {
                $cell = array(
                    'data' => $value[0],
                    'rowspan' => $rowspan + 1
                );
                $rows_id[$row_id]--;
                unset($value[0]);
                $is_rowspan = TRUE;
                $this->table->add_row($cell);
                $this->table->add_row($value);
            } else {
                if ($is_rowspan)
                    unset($value[0]);
                
                $this->table->add_row($value);
                
                if ($rows_id[$row_id] == 1)
                    $is_rowspan = FALSE;
                $rows_id[$row_id]--;
            }
        }
}
    // $this->table->set_caption('KPI bài viết');
    echo $this->table->generate();

?>

<?php

echo $this->admin_form->form_open();

echo $this->admin_form->dropdown('Tháng', 'target_date', $date_contract, '');
echo $this->admin_form->dropdown('Nhân viên thực hiện', 'user_id', $users, '');

echo $this->admin_form->input('Số lượng bài', 'target_post',$fanpage_facebook['number']);
// echo $this->admin_form->input('Số lượng sản phẩm','target_product'); 
echo $this->admin_form->submit('submit_kpi_fanpage_facebook', 'Lưu lại');
echo $this->admin_form->form_close();

?>