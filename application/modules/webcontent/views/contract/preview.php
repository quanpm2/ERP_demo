<?php 
   $this->load->config('webcontent/content');
   $this->config->item('packages');
   $packages_content_config = $this->config->item('packages_content');
   ?> 
<style type="text/css">
   #facebook-ads thead tr{
   text-align: center;
   }
   .row-containter {
   text-align: justify; 
   }
</style>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>ADSPLUS-<?php echo date('mY');?>-<?php echo $term->term_id;?>-<?php echo $term->term_name;?>.pdf</title>
  <style>
  body{line-height:1.2em; font-size: 14px; padding: 0 20mm;}
    p {margin:8px 0}
    .title{background:#eee; font-weight:600; border-top:1px solid #000}
    ul{margin: 0}
  </style>
</head>

<body style="font-family: 'Times New Roman', serif;">
  <p align="center">
      CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM <br/>
      <span>Độc lập - Tự do - Hạnh phúc</span><br/>
      <span>--------------oOo--------------</span>
  </p>
  <p>&nbsp;</p>

  <p align="center">  
    <span style="font-size:1.3em; line-height:1.3em"><?php echo ($printable_title?:'HỢP ĐỒNG'); ?></span>
    <?php 
        echo '<br>(' . get_term_meta_value($term_id,'contract_code') .')' ;
    ?>      
  </p>

  <table width="100%" id="table" border="1" cellspacing="0" cellpadding="3" class="table" style="margin: auto;">
    <tbody>
      <tr >
        <td colspan="2" style="text-align: left;background-color: #c0c0c0;"><strong> BÊN MUA DỊCH VỤ (BÊN A)</strong></td>
      </tr>
       <?php if(!empty($data_customer)): ?>
          <?php foreach ($data_customer as $label => $val): ?>
            <?php if (empty($val)) continue;?>
            <tr>
              <td style="background-color: #dddddd; width: 20%"><?php echo $label;?></td>
              <td>  <?php echo $val;?></td>
            </tr>
          <?php endforeach ?>
      <?php endif ?>
      <tr >
        <td colspan="2" style="text-align: left;background-color: c0c0c0;"><strong>BÊN CUNG CẤP DỊCH VỤ (BÊN B)</strong></td>
      </tr>
       <tr>
        <td style="background-color: #dddddd;">Công ty</td>
        <td><strong><?php echo $company_name; ?></strong></td>
      </tr>
      <?php foreach ($data_represent as $label => $val): ?>
        <?php if (empty($val)) continue;?>
        <tr>
          <td style="background-color: #dddddd;"><?php echo $label;?></td>
          <td>  <?php echo $val;?></td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
  <br/>
  <table width="100%"  border="1" cellspacing="0" cellpadding="3" style="margin: auto;">
    <thead>
      <tr style="background-color: #c0c0c0;">
        <th>Dịch vụ</th>
        <th>Số lượng (Bài viết)</th>
        <th>Đơn giá (VNĐ/Bài viết)</th>
        <th>Thành tiền (VNĐ)</th>
      </tr>
    </thead>
    <tbody >
        <?php
            $str =  get_term_meta_value($term_id, 'contents_selected');
            $services = unserialize($str);
            $sum_total = 0;
           
            foreach ($services as $key_packages => $packages) {
              foreach ($packages as $key_package => $package) {
                  
                  $quanties = $package['number_'.$key_package];
                  $price = $package['price_'.$key_package];
                  $number_post = $package['number_'.$key_package];
                  $price_post = number_format($package['price_'.$key_package]);
                  $total = 0;
                  if($package['sale_off'] !=0){
                    $total = ($price * $quanties) - ((($price * $quanties)*$package['sale_off'])/100);
                    // (price * quanties)-(((price * quanties)*parseInt(sale['value']))/100);
                  }else{
                    $total = $price * $quanties;
                  }
                  $total = $total/1000;
                  $total = round($total)*1000;
                  $sum_total += $total;
                 
                echo '
                    <tr style="text-align: center;">
                     <td>'.$package['checkbox_'.$key_package].'</td>

                     <td>'.$number_post.'</td>
                     <td>'.$price_post.'</td>
                     <td>'.number_format($total).'</td>
                    </tr>
                ';
              }
            }
        ?>
                    <tr >
                      <td colspan="3">Tổng cộng</td>
                      <td style="text-align: center;"><?php echo number_format($sum_total) ;?></td>
                    </tr>
                    <tr style="text-align: left;">
                      <td colspan="4">Bằng chữ: <?php echo convert_number_to_words($sum_total);?> ./.</td>
                    </tr>
                    <tr>
                      <td colspan="4"><strong>(*) Chi tiết các hạng mục dịch vụ, khách hàng xem ở Phụ lục đính kèm</strong></td>
                    </tr>
                    
    </tbody>
  </table>
  <br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="3" style="margin: auto;">
    <tr>
      <td><strong>HÌNH THỨC THANH TOÁN</strong></td>
      <td> Tiền mặt <input type="checkbox"></input></td>
      <td colspan="1">Chuyển khoản <input type="checkbox"></input></td>
    </tr>
    <tr>
      <td colspan="4">
        Trường hợp khách hàng thanh toán bằng chuyển khoản:<br/>
        THÔNG TIN TÀI KHOẢN:<br/>
        Số tài khoản: 19023393899019<br/>
        Chủ tài khoản: Nguyễn Thị Diễm Thúy<br/>
        Ngân hàng Techcombank – PGD Văn Thánh HCM<br/>
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <p style="font-style: italic;"><strong>Phương thức thanh toán:</strong></p> Bên A thanh toán cho Bên B 100% giá trị phiếu đăng ký sau khi ký và trước khi thực hiện.
      </td>
    </tr>
  </table><br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="3" style="margin: auto;">
    <tbody>
      <tr>
        <td colspan="2" >
          <strong>QUY ĐỊNH CHUNG</strong>
        </td>
        
      </tr>
      <tr>
        <td>
          - Nội dung bài viết: không có các từ ngữ vi phạm chính sách Facebook.
        </td>
      </tr>
      <tr>
        <td>
           - Số từ trong bài viết dưới 400 từ
        </td>
      </tr>
      <tr>
        <td>
          - Thời gian viết bài từ 24 giờ – 48 giờ kể từ khi Bên A gởi lại cho Bên B mẩu thông tin viết bài do Bên B đã cung cấp cho Bên A. Sau khi gửi Demo, Khách hàng không được chỉnh sửa quá 2 lần so với yêu cầu ban đầu như:  không thay đổi nội dung bố cục, hình ảnh, thông điệp so với yêu cầu ban đầu của Bên A.
        </td>
      </tr>
      <tr>
        <td>
          - Bài viết chính thức: là bài viết thuộc một trong hai trường hợp sau:
        </td>
      </tr>
      <tr>
        <td>
          <p style="margin-left: 5%;">+ Bài viết demo trở thành bài viết chính thức khi Bên A không yêu cầu Bên B chỉnh sửa trong thời hạn hai (02) ngày  kể từ ngày nhận được bài viết demo</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="margin-left: 5%;">+ Bài viết sau khi Bên B chỉnh sửa lần cuối cùng (không quá 3 lần)</p>
        </td>
      </tr>
    </tbody>  
  </table>


<?php echo $this->load->view('admin/preview/footer','',TRUE);?>