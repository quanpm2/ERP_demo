<?php
	$this->config->load('webcontent/content') ; 
	$contents = $this->config->item('packages_content') ;	
?>	

 <div class="col-md-12" id="service_tab">
	<?php 
	 $this->config->load('webcontent/content');

	 $content    		   = $this->config->item('packages_content');

	 echo $this->admin_form->form_open(), form_hidden('edit[term_id]', $edit->term_id), $this->admin_form->submit('', 'confirm_step_service','confirm_step_service','',array('style'=>'display:none;','id'=>'confirm_step_service')) ;

	$number_content=get_term_meta_value($edit->term_id,'number_content') ?: 1;

	if ($content) 
	 {
	 	foreach ($content as $key => $packages_content)
	 	 {
	 	 	// content TĨNH
	 	 	if ($key=='facebook_content') 
	 	 	{
		 	 	echo '<div id="load-package-info">' ;
				$this->table->set_heading('<b>TÊN GÓI</b>','<b>ĐƠN GIÁ (VNĐ)</b>','<b>SỐ LƯỢNG</b>','<b>GIẢM GIÁ (%)</b>', '<b>TỔNG TIỀN:</b> <span class="sum-price">0 VNĐ</span>');

				$this->table->add_row(array('data' => '<b>CONTENT FACEBOOK</b>', 'colspan' => 5 , 'class' => 'bg-info'));
				foreach ($packages_content as $nomal => $value) 
				{

		 	 		$this->table->add_row(nbs(10).$this->admin_form->checkbox('edit[meta]['.$key.']['.$nomal.'][checkbox_'.$nomal.']',$value['label'],FALSE,
		 	 			 array(
		 	 		 	'id'=>$value['name']
		 	 		 	))
		 	 			.form_label($value['label'], $value['name']),

		 	 			form_input(['type'=>'number', 'name'=>'edit[meta]['.$key.']['.$nomal.']['. $value['name'] . ']','class'=>'form-control', 'id' => ''.$value['name'].'', 'placeholder' => 'Đơn giá','readonly' => 'true'],$value['price_content']),

		 	 			form_input(['type'=>'number', 'name'=>'edit[meta]['.$key.']['.$nomal.']['. $value['number'] . ']','class'=>'form-control', 'id' => ''.$value['number'].'', 'value'=> $value['min'], 'placeholder' => 'Nhập số lượng','disabled'=>'true'],$number_content),

		 	 			array('data'=>'0%','class'=>'sale-off-'.$value['name']),
		 	 			array('data' => '0 VNĐ' , 'class' => 'td-' . $value['name'])
		 	 			);
		 	 	}
		 	}
		 	 	
		 	
	 	 }

		echo $this->table->generate(); 
		echo '</div>' ;	 	
	 }

	 echo $this->admin_form->form_close();
	?>

</div>
 <script type="text/javascript">

	function calc_price(price, quanties,sale='') {
		if(price == null) return false;
		if(quanties == null) return false;

		if(sale!='' && quanties>= parseInt(sale['min'])){
			var total = (price * quanties)-(((price * quanties)*parseInt(sale['value']))/100);
			total = total/1000;
			total = Math.round(total);
			return total*1000;
		}
		return (price * quanties);
	}

	function calc_sum_price(array) {
		if(array.length == 0) return false;
		var total_price = 0;

		$.each(array, function(i, val) {
			total_price = total_price + val;
		}); 
		
		return total_price;
	}


	function formatNumber(nStr, decSeperate = '.', groupSeperate = ',', type = ' VNĐ') {
        nStr += '';
        x = nStr.split(decSeperate);
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
        }
        return (x1 + x2) + type;
    }

	var sum_total 		= {};

 	$(document).ready(function() {

 		$('input[type="checkbox"]').iCheck({
		    checkboxClass: 'icheckbox_flat-blue',
		    radioClass: 'iradio_flat-blue'
  		});

 		var contents = <?php echo json_encode($contents) ;?>;

 		if(contents.length == 0) return false;
 		$('#load-package-info table thead tr span.sum-price').text(formatNumber(0)) ; 
		$.each(contents, function(key, content) {
			$.each(content, function(i, val) {
				$('#' + val.name).on('ifChanged', function() {

					// XÁC ĐỊNH THẺ CHA CHỨA
					var tag_parent  = $(this).closest('tr');

					// REMOVE CLASS KHI CHƯA CHỌN GÓI
					tag_parent.removeClass(val.name) ;
					
					// CÓ CHECK VÀO BUTTON RADIO HAY CHƯA: TRUE/FALSE
					var has_checked = $(this).prop( "checked");

					// REMOVE HTML KHI CHƯA CHỌN GÓI
					$('.td-' + val.name).text(formatNumber(0));

					delete sum_total[val.name];

					// TÍNH TỔNG TIỀN CỦA HỢP ĐỒNG
					$('#load-package-info table thead tr span.sum-price').text(formatNumber(0)) ;
					$('#load-package-info table thead tr span.sum-price').text(formatNumber(calc_sum_price(sum_total))) ;

					// KHI CHỌN GÓI => TÍNH LẠI TIỀN
			        if(has_checked == true ) {
			        	tag_parent.addClass(val.name) ;
						$(':input[name|="edit[meta]['+key+']['+i+'][' + val.name + ']"]').attr('readonly',false);
			        	var price  		= $(':input[name|="edit[meta]['+key+']['+i+'][' + val.name + ']"]').val();
			        	var quanties 	= $(':input[name|="edit[meta]['+key+']['+i+'][' + val.number + ']"]').val();

			        	if(quanties>=3)
			        		$('.sale-off-'+ val.name).empty().text(val['sale_off']['value']+'%');
		        		else 
		        			$('.sale-off-'+ val.name).empty().text('0%');

			        	var total  		= calc_price(price, quanties,val['sale_off']) ;

			        	// GÁN MỖI GÓI VỚI TỔNG TIỀN LÀ BAO NHIÊU
			        	sum_total[val.name] = total;
			        	
			        	// TÍNH TỔNG TIỀN CỦA HỢP ĐỒNG
						$('#load-package-info table thead tr span.sum-price').text(formatNumber(0)) ;
						$('#load-package-info table thead tr span.sum-price').text(formatNumber(calc_sum_price(sum_total))) ;
			        	
			        	$('.td-' + val.name).text(formatNumber(total));

			        	// KHI THAY ĐỔI SỐ LƯỢNG => TÍNH TIỀN
			        	$('#' + val.number).on('change keyup', function() {
			        		if($(':input[name|="edit[meta]['+key+']['+i+'][' + val.number + ']"]').val().length <= 0) 
			        		{
			        			$(':input[name|="edit[meta]['+key+']['+i+'][' + val.number + ']"]').val(1)
			        		}

			        		if($('#number_fanpage_facebook').val() < 10)
			        		{
			        			$('#number_fanpage_facebook').val(10);
			        		}
			        		var price  		= $(':input[name|="edit[meta]['+key+']['+i+'][' + val.name + ']"]').val();
			        		var quanties 	= $(':input[name|="edit[meta]['+key+']['+i+'][' + val.number + ']"]').val();

			        		if(quanties>=3)
			        			$('.sale-off-'+ val.name).empty().text(val['sale_off']['value']+'%');
			        		else 
			        			$('.sale-off-'+ val.name).empty().text('0%');

			        		var total  		= calc_price(price, quanties,val['sale_off']);

			        		$('.td-' + val.name).empty();
							$('.td-' + val.name).text(formatNumber(total));	

							// GÁN MỖI GÓI VỚI TỔNG TIỀN LÀ BAO NHIÊU
			        		sum_total[val.name] = total;
			        		
			        		// TÍNH TỔNG TIỀN CỦA HỢP ĐỒNG
							$('#load-package-info table thead tr span.sum-price').text(formatNumber(0)) ;
							$('#load-package-info table thead tr span.sum-price').text(formatNumber(calc_sum_price(sum_total))) ;
				        	});
			        }else{
			        	$(':input[name|="edit[meta]['+key+']['+i+'][' + val.name + ']"]').attr('readonly',true);
			        	$('.sale-off-'+ val.name).text('0%');
			        }
					document.getElementById(val.number).disabled = !this.checked;
				});	
			});
		});	
		 	});
	$(function(){
		$('.set-datepicker').datepicker({
			format: 'yyyy/mm/dd',
			todayHighlight: true,
			autoclose: true,	
		});
	});

	$(document).ready(function() {
		var load_package_info = $("#load-package-info").closest('form');
		load_package_info.validate({  
			rules: {
			  'edit[meta][description_content]': {
			    required: true,
			  },
			  'edit[meta][number_content]': {
			    required: true,
			  },
			  'edit[meta][price_content]': {
			    required: true,
			  },			  			  		  
			},
			messages: {
			  'edit[meta][description_content]': {
			    required	: 'Mô tả thiết kế content không được để trống',
			  },
			  'edit[meta][number_content]': {
			    required	: 'Số lượng content không được để trống',
			    digits		: 'Kiểu dữ liệu không hợp lệ, Số lượng content phải là kiểu số',
			  }	,
			  'edit[meta][price_content]': {
			    required	: 'Đơn giá thiết kế content không để trống',
			    digits		: 'Kiểu dữ liệu không hợp lệ, Đơn giá thiết kế content phải là kiểu số',
			  }			  		
			}
		});
	}) ;
 </script>