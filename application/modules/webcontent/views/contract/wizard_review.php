<?php $this->config->load('webcontent/content'); ?>
<div class="col-md-12" id="review-partial">

  <h2 class="page-header">
    <i class="fa fa-globe"></i> Mã hợp đồng : <?php echo get_term_meta_value($edit->term_id, 'contract_code');?> 
    <small class="pull-right">Ngày tạo : <?php echo my_date(time(), 'd-m-Y');?></small>
  </h2>
  
  <div class="row">
  <?php
  $representative_gender  = force_var(get_term_meta_value($edit->term_id,'representative_gender'),'Bà','Ông');
  $representative_name    = get_term_meta_value($edit->term_id,'representative_name') ?: '';
  $display_name           = "{$representative_gender} {$representative_name}";
  $representative_email   = get_term_meta_value($edit->term_id,'representative_email');
  $representative_address = get_term_meta_value($edit->term_id,'representative_address');
  $representative_phone   = get_term_meta_value($edit->term_id,'representative_phone');
  $contract_daterange     = get_term_meta_value($edit->term_id,'contract_daterange');

  echo $this->admin_form->set_col(6)->box_open('Thông tin khách hàng');
  echo $this->table->clear()
      ->add_row('Người đại diện',$display_name?:'Chưa cập nhật')
      ->add_row('Email',$representative_email?:'Chưa cập nhật')
      ->add_row('Địa chỉ',$representative_address?:'Chưa cập nhật')
      ->add_row('Số điện thoại',$representative_phone?:'Chưa cập nhật')
      ->add_row('Chức vụ',$edit->extra['representative_position']??'Chưa cập nhật')
      ->add_row('Mã Số thuế',$edit->extra['customer_tax']??'Chưa cập nhật')
      ->add_row('Thời gian thực hiện',$contract_daterange?:'Chưa cập nhật')
      ->generate();

  echo $this->admin_form->box_close();

  echo $this->admin_form->set_col(6)->box_open('Thông tin dịch vụ');

          $str =  get_term_meta_value($edit->term_id, 'contents_selected');
          $services = unserialize($str);
          
          $this->load->config('webcontent/content');
          $packages_content_config = $this->config->item('packages_content');

         
          $this->table->clear()->set_heading('Gói Content','Số lượng Content','Đơn Giá(VNĐ)','Giảm giá (%)','Tổng tiền');
          foreach ($services as $key_packages => $packages) {
             foreach ($packages as $key_package => $package) {
                  $quanties = $package[$packages_content_config[$key_packages][$key_package]['number']];
                  $price = $package[$packages_content_config[$key_packages][$key_package]['name']];
                  $total = 0;
                if($package['sale_off'] !=0){
                  $total = ($price * $quanties) - ((($price * $quanties)*$package['sale_off'])/100);
                  // (price * quanties)-(((price * quanties)*parseInt(sale['value']))/100);
                }else{
                  $total = $price * $quanties;
                }
                $total = $total/1000;
                
               $this->table->add_row($package['checkbox_'.$key_package],$quanties,number_format($price),$package['sale_off'],number_format(round($total)*1000));
             }
           } 
        
        echo $this->table->generate();
  echo $this->admin_form->box_close();
  ?>
  </div>
</div>
<div class="clearfix"></div>

<?php
$hidden_values = ['edit[term_status]'=>'waitingforapprove','edit[term_id]'=>$edit->term_id,'edit[term_type]'=>$edit->term_type];
echo $this->admin_form->form_open('',[],$hidden_values);
echo $this->admin_form->submit('','confirm_step_finish','confirm_step_finish','', array('style'=>'display:none;','id'=>'confirm_step_finish'));
echo $this->admin_form->form_close();
?>