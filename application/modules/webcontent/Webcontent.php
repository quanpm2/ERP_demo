<?php
class Webcontent_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name(){
		
		return 'Webcontent';
	}

	public function init(){	

		$this->_load_menu();
		//$this->_update_permissions();
	}

	public function title()
	{
		return 'Webcontent';
	}

	public function author()
	{
		return 'HL';
	}

	public function version()
	{
		return '0.1';
	}

	public function description()
	{
		return 'Webcontent';
	}
	private function init_permissions()
	{
		$permissions = array();
		$permissions['Admin.Webcontent'] = array(
			'description' => 'Quản lý Webcontent',
			'actions' => array('view','add','edit','delete','update'));

		$permissions['Webcontent.Staffs'] = array(
			'description' => 'Bảng phân công KPI',
			'actions' => array('manage','access'));

		$permissions['Webcontent.Done'] = array(
			'description' => 'Dịch vụ đã hoàn thành',
			'actions' => array('manage','access'));

		$permissions['Webcontent.Index'] = array(
			'description' => 'Trang chính Web Content',
			'actions' => array('manage','access'));

		$permissions['Webcontent.Overview'] = array(
			'description' => 'Trang tổng quan',
			'actions' => array('manage','access'));

		$permissions['Webcontent.Content'] = array(
			'description' => 'Quản lý nội dung',
			'actions' => array('manage','access','add','delete','update'));

		$permissions['Webcontent.Kpi'] = array(
			'description' => 'Quản lý KPI',
			'actions' => array('manage','access','add','delete','update'));

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}

	private function _load_menu()
	{

		if(!is_module_active('webcontent')) return FALSE;	

		$order = 1;
		
		if(has_permission('webcontent.index.access'))
		{
			$this->menu->add_item(array(
				'id' => 'webcontent',
				'name' => 'Nội dung',
				'parent' => null,
				'slug' => admin_url('webcontent'),
				'order' => $order++,
				'icon' => 'fa fa-pencil-square-o',
				'is_active' => is_module('webcontent')
				),'navbar');
		}
		
		if(!is_module('webcontent')) return FALSE;

		$this->menu->add_item(array(
		'id' => 'webcontent-index',
		'name' => 'Dịch vụ',
		'parent' => null,
		'slug' => admin_url('webcontent'),
		'order' => $order++,
		'icon' => 'fa fa-fw fa-xs fa-circle-o'
		), 'left');

		if(has_permission('Webcontent.Index.Access'))
		{
			$this->menu->add_item(array(
			'id' => 'webcontent-index-on',
			'name' => 'Đang thực hiện',
			'parent' => 'webcontent-index',
			'slug' => admin_url('webcontent'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-toggle-on'
			), 'left');
		}


		$has_content_manage = has_permission('webcontent.Content.Manage');
		$has_content_product_manage = has_permission('webcontent.Content_product.Manage');
		$is_administrator = ($this->admin_m->role_id == 1);
		$has_task_manage = has_permission('webcontent.Task.Manage');
		if($has_content_manage OR $is_administrator OR $has_task_manage OR $has_content_product_manage)
		{
			$this->menu->add_item(array(
			'id' => 'webcontent-statistics',
			'name' => 'Thống kê',
			'parent' => null,
			'slug' => admin_url('#'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-bar-chart-o'
			), 'left');

			if($has_content_manage)
			{
				$this->menu->add_item(array(
					'id' => 'webcontent-statistics-kpi',
					'name' => 'Nội dung',
					'parent' => 'webcontent-statistics',
					'slug' => admin_url('webcontent/statistics/kpi'),
					'order' => $order++,
					'icon' => 'fa fa-fw fa-pencil-square-o'
					), 'left');
			}
		}

		if(has_permission('Webcontent.staffs.manage'))
		{

			$this->menu->add_item(array(
			'id' => 'webcontent-staffs',
			'name' => 'Phân công/Phụ trách',
			'parent' => null,
			'slug' => admin_url('webcontent/staffs'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-group'
			), 'left');
		}

		$term_id = $this->uri->segment(4);
		$check_approve = $this->uri->segment(3);
		if($check_approve == 'wordpress')
		{
			$term_id = $this->uri->segment(5);
		}

		$this->website_id = $term_id;

		$left_navs = array(
			'overview' => array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-tachometer',
				),
			'content'  => array(
				'name' => 'Bài viết',
				'icon' => 'fa fa-fw fa-pencil-square-o',
				),
			'kpi'  => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-heartbeat',
				),
		);

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("Webcontent.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "webcontent-{$method}",
				'name' => $name,
				'parent' => NULL,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
				), 'left');
		}

		if(has_permission('Webcontent.backlink'))
			$this->menu->add_item(array(
				'id' => 'webcontent-backlink-index',
				'name' => 'Backlink',
				'parent' => null,
				'slug' => module_url("backlink/{$term_id}"),
				'order' => $order++,
				'icon' => ''
				), 'left');

		if(has_permission('Webcontent.ApprovePost'))
		{
			$service_url = get_term_meta_value($term_id, 'wp_service_url');
			$service_user_id = get_term_meta_value($term_id, 'wp_user_id');
			if($service_url && $service_user_id)
			{
				$this->menu->add_item(array(
				'id' => 'webcontent-sync_posts-index',
				'name' => 'Duyệt bài',
				'parent' => null,
				'slug' => module_url("wordpress/sync_posts/{$term_id}"),
				'order' => $order++,
				'icon' => ''
				), 'left');
			}
			
		}

		if(has_permission('Webcontent.backlink'))
			$this->menu->add_item(array(
				'id' => 'webcontent-backlink',
				'name' => 'Backlink',
				'parent' => 'webcontent-backlink-index',
				'slug' => module_url("backlink/{$term_id}"),
				'order' => $order++,
				'icon' => ''
				), 'left');

		if(has_permission('Webcontent.backlink'))
			$this->menu->add_item(array(
				'id' => 'webcontent-keywords',
				'name' => 'Keywords',
				'parent' => 'webcontent-backlink-index',
				'slug' => module_url("keywords/{$term_id}"),
				'order' => $order++,
				'icon' => ''
				), 'left');
	}

}