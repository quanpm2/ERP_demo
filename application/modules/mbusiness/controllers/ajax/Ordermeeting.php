<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordermeeting extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('mbusiness/ordermeeting_m');
		$this->load->config('mbusiness/ordermeeting');
		if(!$this->input->is_ajax_request()) return FALSE;
	}


	/**
	 * Delete ordermeeting
	 *
	 * @param      <type>  $id     The ordermeeting ID
	 *
	 * @return     json
	 */
	public function deleteOrder($id)
	{
		$result = array('status'=>FALSE,'msg'=>'Xóa không thành công hoặc không có quyền xóa.');

		if( ! $this->ordermeeting_m->has_permission($id, 'mbusiness.ordermeeting.delete')) 
		{
			$result['msg'] = 'Quyền hạn không hợp lệ ! không thể xóa lịch hẹn này';
			return parent::renderJson($result);
		}

		$updatedStatus = $this->ordermeeting_m
		->where_in('status', array('draft','sent'))->where('user_id',$this->admin_m->id)->update($id, ['status'=>'deleted']);

		if($updatedStatus === FALSE) return parent::renderJson($result);

		$result['status'] = TRUE;
		$result['msg'] = 'Lịch hẹn đã được xóa thành công.';

		return parent::renderJson($result);
	}


	/**
	 * COnfirm Order meeting
	 *
	 * @param      int  $id     The ordermeeting ID
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function confirmOrder($id = 0)
	{
		$result = array('msg'=>'Xử lý không thành công !','status'=>FALSE);

		if( ! $this->ordermeeting_m->has_permission($id, 'mbusiness.ordermeeting.confirm')) 
		{
			$result['msg'] = 'Quyền hạn không hợp lệ ! không thể xác nhận lịch hẹn này';
			return parent::renderJson($result);
		}

		$isUpdated = $this->ordermeeting_m->update($id,['status'=>'verified','user_confirm_id'=>$this->admin_m->id]);
		if( ! $isUpdated)
		{
			$result['msg'] = 'Hệ thống tạm thời chưa xử lý được , xin thử lại sau .';
			return parent::renderJson($result);
		}

		$this->sendMailFeedBack($id); // Send Email to user create order meeting form.
		
		$result['msg'] = 'Biểu mẫu order lịch gặp khách hàng đã được xác nhận';
		$result['status'] = TRUE;
		return parent::renderJson($result);
	}


	protected function sendMailFeedBack($orderMeetingId = 0)
	{
		$result = array('msg'=>'Không thành công , xin vui lòng thử lại sau!','status'=>FALSE);
		$this->output->set_content_type('application/json');

		$orderMeeting = $this->ordermeeting_m->get($orderMeetingId);
		if(!$orderMeeting) return FALSE;

		if($orderMeeting->status !== 'verified') 
		{
			$result['msg'] = 'Order đang trong trạng thái chờ , vui lòng thử lại sau.';
			return $this->output->set_output(json_encode($result));
		}

		$this->load->library('email');

		$mail_to = array();
		$mail_cc = array('trind@adsplus.vn');
		$mail_bcc = array('mailreport@webdoctor.vn');

		$subject = '[ERP] Lịch hẹn KH vào lúc '.my_date($orderMeeting->start_time,'d/m/Y H:i A').' đã được xác nhận.';
		$this->email->subject($subject);

        $mail_to[] = $this->admin_m->get_field_by_id($orderMeeting->user_id,'user_email');
		if($orderMeeting->support_id)
		{
			$mail_to[] = $this->admin_m->get_field_by_id($orderMeeting->support_id, 'user_email');
		}

		$happened_on = my_date($orderMeeting->start_time,'d-m-Y H:i:s').' đến '.my_date($orderMeeting->end_time,'d-m-Y H:i:s');

		$userSale = $this->admin_m->get_field_by_id($orderMeeting->user_id, 'display_name');

		$userSupport = '--';
		if(!empty($orderMeeting->support_id))
		{
			$userSupport = $this->admin_m->get_field_by_id($orderMeeting->support_id,'display_name');
		}

		$this->table->set_heading(['NVKD','HỖ TRỢ','KHÁCH HÀNG','THỜI GIAN HẸN','ĐỊA ĐIỂM','NGƯỜI LIÊN HỆ','ĐIỆN THOẠI','NỘI DUNG']);

		$this->table->add_row($userSale,$userSupport,$orderMeeting->display_name,$happened_on,$orderMeeting->location,$orderMeeting->cus_name,$orderMeeting->cus_phone,$orderMeeting->content);

		$mailContent = $this->table->generate();
		$mailContent.= '<small><i>Lịch hẹn đã được xác nhận và hệ thống ERP đã ghi nhận KPI.</i></small>';

		$this->email->message($mailContent);
		$this->email->from('support@adsplus.vn', 'ERP')->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);

		$isSent = $this->email->send();

		$result['status'] = TRUE;
		$result['msg'] = 'Mail phản hồi đã được gửi đến bộ phận KD.';

		return $this->output->set_output(json_encode($result));
	}

	public function sendMailOrder($id = 0)
	{
		$orderMeeting = $this->ordermeeting_m->get($id);
		if(!$orderMeeting) return FALSE;

		if($orderMeeting->status == 'draft')
		{
			# send mail
			$edit = $this->input->post('edit');
			$result = $this->ordermeeting_m->update($id,$edit);
			$orderMeeting = $this->ordermeeting_m->get($id);

			$this->config->load('staffs/group');
			$roleSaleIds = $this->config->item('salesexecutive','groups');
			$saleUser = $this->admin_m->set_role($roleSaleIds)->set_get_admin()->set_get_active()->get_by();

			$this->load->library('email');

			$mail_to = array('thonh@webdoctor.vn');
			$mail_cc = array();
			$mail_bcc = array();

	        $saleMail = $this->admin_m->get_field_by_id($orderMeeting->user_id,'user_email');
	        $mail_cc[] = $saleMail;

	        $saleName = $this->admin_m->get_field_by_id($orderMeeting->user_id, 'display_name') ?: $saleMail;
	        $saleName = "#{$orderMeeting->user_id} {$saleName}";

	        $saleSupportName = '---';
			if($orderMeeting->support_id)
			{
				$saleSupportMail = $this->admin_m->get_field_by_id($orderMeeting->support_id, 'user_email');
				$mail_cc[] = $saleSupportMail;

				$saleSupportName = $this->admin_m->get_field_by_id($orderMeeting->support_id,'display_name') ?: $saleSupportMail;
				$saleSupportName = "#{$orderMeeting->support_id} {$saleSupportName}";
			}

			$this->email->from('support@adsplus.vn', 'Hệ thống ADSPLUS')->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);

			$title = '[NVKD] Order lịch gặp khách hàng';
			$this->email->subject($title);

			$happened_on = my_date($orderMeeting->start_time,'d-m-Y H:i:s').' đến '.my_date($orderMeeting->end_time,'d-m-Y H:i:s');
			$this->table->set_heading(['KHÁCH HÀNG','THỜI GIAN','ĐỊA ĐIỂM','NGƯỜI LIÊN HỆ','ĐIỆN THOẠI','NỘI DUNG','NHÂN VIÊN','HỖ TRỢ']);
			$this->table->add_row($orderMeeting->display_name,$happened_on,$orderMeeting->location,$orderMeeting->cus_name,$orderMeeting->cus_phone,$orderMeeting->content,$saleName,$saleSupportName);
			$mailContent = $this->table->generate();
			$this->email->message($mailContent);
			
			$this->messages->success('Yêu cầu đã được gửi, xin vui lòng đợi phản hồi');
			return;
		}

		if($orderMeeting->status == 'sent')
		{
			# send mail
			$this->messages->info('Yêu cầu đang được chờ duyệt, check mail để xem chi tiết');
			return;
		}

		if($orderMeeting->status == 'verified')
		{
			# send mail
			$this->messages->success('Yêu cầu đã được duyệt, check mail để xem chi tiết');
			return;
		}
	}
}
/* End of file Ordermeeting.php */
/* Location: ./application/modules/mbusiness/controllers/ajax/Ordermeeting.php */