<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dataset extends Admin_Controller {

	public $term_type = 'banner';

	function __construct()
	{
		parent::__construct();

		$this->load->model('mbusiness/call_kpi_m');
		$this->load->library('datatable_builder');
		$this->load->helper('text');

		$this->load->config('staffs/staffs');
		defined('CALL_PER_ORDERMETTING') OR define('CALL_PER_ORDERMETTING',10);		

		// $this->scache->delete_all();
	}

	/**
	 * Ajax Dataset of Order meetings
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function ordermeeting()
	{
		$response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
		if( ! has_permission('mbusiness.ordermeeting.access'))
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		$defaults 	= ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
		$args 		= wp_parse_args( $this->input->get(), $defaults);

		$relate_users 	= $this->admin_m->get_all_by_permissions('mbusiness.ordermeeting.access');

		// LOGGED USER NOT HAS PERMISSION TO ACCESS
		if($relate_users === FALSE)
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		if(is_array($relate_users))
		{
			$this->datatable_builder->where_in('ordermeeting.user_id', $relate_users);
		}

		$this->load->model('mbusiness/ordermeeting_m');
		$this->load->config('mbusiness/ordermeeting');

		// $this->submit();
		$this->search_filter();

		$isManager = has_permission('mbusiness.ordermeeting.manage');
		$data = $this->data;

		$this->datatable_builder
		->set_filter_position(FILTER_TOP_OUTTER)
		->select('ordermeeting.id,ordermeeting.status,ordermeeting.user_id,support_id')
		->add_search('created_on',['placeholder'=>'Ngày tạo','class'=>'form-control input_daterange'])
		->add_search('start_time',['placeholder'=>'Ngày làm việc','class'=>'form-control input_daterange'])
		->add_search('content',['placeholder'=>'Nội dung làm việc'])

		->add_search('user_email',['placeholder'=>'Nhân viên kinh doanh'])
		->add_search('support_email',['placeholder'=>'Nhân viên Hỗ trợ'])
		
		->add_search('display_name',['placeholder'=>'Khách hàng'])
		->add_search('cus_name',['placeholder'=>'Người liên hệ'])
		->add_search('cus_phone',['placeholder'=>'Số điện thoại'])
		->add_search('status',array('content'=> form_dropdown(array('name'=>'where[status]','class'=>'form-control select2'),prepare_dropdown($this->config->item('processStates'),'Tất cả'),$this->input->get('where[status]'))))

		->add_column('created_on', array('title'=> 'Ngày tạo'))
		->add_column('start_time', array('title'=> 'Ngày hẹn'))
		->add_column('content', array('title'=> 'Nội dung'))
		->add_column('ordermeeting.display_name', array('title'=> 'Khách hàng'))
		->add_column('cus_name', array('title'=> 'Người LH'))
		->add_column('cus_phone', array('title'=> 'Số ĐT'))
		->add_column('ordermeeting.user_id', array('set_select' => FALSE ,'title'=> 'Kinh doanh'))
		->add_column('support_id', array('set_select' => FALSE,'title'=> 'KD Hỗ trợ'))
		->add_column('status', array('set_select' => FALSE,'title'=> 'Trạng thái'))
		->add_column('action', array('title'=> 'Action', 'set_order'=> FALSE,'set_select' => FALSE))

		->add_column_callback('id',function($data,$row_name) use ($relate_users){

			// Xem nhanh nội dung order
			$data['content'] = '<span data-container="body" role="button" data-toggle="popover" data-placement="right" title="Nội dung làm việc" data-content="'.$data['content'].'">'.word_limiter($data['content'],20).'</span>';

			// Nhân viên kinh doanh chính
			$data['user_id'] = $this->admin_m->get_field_by_id($data['user_id'], 'display_name');

			// Trưởng nhóm / Nhân viên kinh doanh hỗ trợ
			$data['support_id'] = !empty($data['support_id']) ? $this->admin_m->get_field_by_id($data['support_id'], 'display_name') : '--';

			// Tên gọi khách hàng
			if(mb_strlen($data['display_name']) > 20)
			{
				$data['display_name'] = '<span data-toggle="tooltip" title="'.$data['display_name'].'">'.mb_substr($data['display_name'],0,20).'..</span>';
			}
			
			$data['cus_name'] = '<span data-toggle="tooltip" title="'.$data['cus_name'].'">'.character_limiter($data['cus_name'],5).'</span>';

			// Thời gian tạo order
			$data['created_on'] = my_date($data['created_on'],'d/m/Y H:i A');

			// Thời gian làm việc
			$data['start_time'] = my_date($data['start_time'],'d/m/Y H:i A');

			$order_status = $data['status'];

			// Trạng thái của order form
			$data['status'] = $this->config->item($order_status,'processStates');

			// Command Actions
			$actions = '';	

			if(	$order_status == 'sent' 
				&& ($relate_users === TRUE || in_array($this->admin_m->id, $relate_users))
				&& has_permission('mbusiness.ordermeeting.confirm'))
			{
				$anchor_href 	= module_url("ajax/ordermeeting/confirmOrder/{$data['id']}");
				$anchor_title 	= '<i class="fa fa-fw fa-check-circle"></i>';
				$anchor_attrs 	= 'title="Xác nhận Order" class="btn btn-success btn-xs confirm-order-metting" data-toggle="confirmation"';

				$actions.= anchor($anchor_href, $anchor_title, $anchor_attrs);
			}

			if(	in_array($order_status, ['draft','sent'])
				&& ($relate_users === TRUE || in_array($this->admin_m->id, $relate_users))
				&& has_permission('mbusiness.ordermeeting.update'))
			{
				$anchor_href 	= module_url("ordermeeting/edit/{$data['id']}");
				$anchor_title 	= '<i class="fa fa-pencil"></i>';
				$anchor_attrs 	= 'title="Cập nhật Order" class="btn btn-info btn-xs"';

				$actions.= anchor($anchor_href, $anchor_title, $anchor_attrs);
			}

			if(	$order_status != 'deleted'
				&& ($relate_users === TRUE || in_array($this->admin_m->id, $relate_users))
				&& has_permission('mbusiness.ordermeeting.delete'))
			{
				$actions.= anchor(module_url("ajax/ordermeeting/deleteOrder/{$data['id']}"), '<i class="fa fa-fw fa-trash"></i>', 'title="Xóa order lịch hẹn" class="btn btn-danger btn-xs delete-order-metting" data-toggle="confirmation"');
			}

			$data['action'] = $actions;

			return $data;
		},FALSE)

		->from('ordermeeting')
		->where('status !=','draft')
		->order_by('created_on','desc');

		$config = array('per_page' => $args['per_page'],'cur_page' => $args['cur_page']);
		$data 	= $this->datatable_builder->generate($config);

		// OUTPUT : DOWNLOAD XLSX
		if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
		{
			$this->export_ordermeeting($last_query);
			return TRUE;
		}

		$this->template->title->append('Danh sách lịch hẹn');
		$this->template->description->set('Trang danh sách tất cả các hợp đồng');

		return parent::renderJson($data);
	}

	/**
	 * Trang tổng quan liệt kê danh sách tất cả các hợp đồng
	 */
	public function activity()
	{
		$response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

		if( ! has_permission('mbusiness.activity.access'))
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		$defaults 	= ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
		$args 		= wp_parse_args( $this->input->get(), $defaults);

		$relate_users 	= $this->admin_m->get_all_by_permissions('mbusiness.activity.access');

		// LOGGED USER NOT HAS PERMISSION TO ACCESS
		if($relate_users === FALSE)
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		if(is_array($relate_users))
		{
			$this->datatable_builder->where_in('user.user_id', $relate_users);
		}

		$this->search_filter();

		$start_time = $this->mdate->startOfMonth();
		$end_time 	= $this->mdate->endOfMonth();
		$daterange 	= $this->input->get('where[daterange]');
		if(!empty($daterange) && $dates = explode(' - ', $daterange))
		{
			$start_time = $this->mdate->startOfDay(reset($dates));
			$end_time = $this->mdate->endOfDay(end($dates));
		}

		$data['start_time'] = $start_time;
		$data['end_time'] = $end_time;
		
		$all_roles = $this->role_m->cache('role/role-get_all')->get_all();
		$all_roles = key_value($all_roles, 'role_id', 'role_name');
		$sale_role_ids = array(9,13);

		$data = $this->datatable_builder
		->set_filter_position(FILTER_TOP_OUTTER)

		->select('user.user_id,user.role_id,user.user_status')		

		->add_search('user_status',array(
			'content'=> form_dropdown(array(
				'name'=> 'where[user_status]',
				'class'=>'form-control select2'), 
			prepare_dropdown($this->config->item('user_status'),'Trạng thái : All'),
			$this->input->get('where[user_status]')),
			))
		->add_search('daterange',['placeholder'=>'Thời gian cần lấy','class'=>'form-control input_daterange'])

		->add_column('user.user_id','ID')
		->add_column('user.display_name',['title'=>'Tên hiển thị','set_order'=>FALSE])
		->add_column('SUM(callkpi.result_value)*COUNT(DISTINCT callkpi.kpi_id)/COUNT(`user`.user_id) AS call_kpi_result',['title'=>'Cuộc gọi','set_order'=>TRUE])
		->add_column('SUM(callkpi.kpi_value)*COUNT(DISTINCT callkpi.kpi_id)/COUNT(`user`.user_id) AS call_kpi_value',['title'=>'[gọi] KPI','set_order'=>TRUE])
		->add_column('COUNT(DISTINCT ordermeeting.id) AS meeting_num',['title'=>'lịch hẹn','set_order'=>TRUE])
		->add_column('COUNT(DISTINCT oms.id) AS support_meeting_num',['title'=>'Hỗ trợ <br/>cuộc hẹn','set_order'=>TRUE])

		->add_column('COUNT(DISTINCT ordermeeting.id)+COUNT(DISTINCT oms.id) AS total_meeting', array('set_select'=> TRUE, 'title'=> 'Tổng <br>cuộc hẹn','set_order'=> FALSE))

		->add_column('ordermeeting_convert_kpi_result', array('set_select'=> FALSE, 'title'=> 'Quy đổi <br>1 hẹn = '.CALL_PER_ORDERMETTING.' [gọi]','set_order'=> FALSE))
		->add_column('(SUM(callkpi.result_value)*COUNT(DISTINCT callkpi.kpi_id)/COUNT(`user`.user_id))+((COUNT(DISTINCT ordermeeting.id)+COUNT(DISTINCT oms.id))*'.CALL_PER_ORDERMETTING.') AS total_result',['title'=>'Tổng Đạt','set_order'=>TRUE])
		->add_column('month_kpi_rate', array('set_select'=> FALSE, 'title'=> 'Tỷ lệ đạt KPI','set_order'=> FALSE))

		->add_column_callback('user_id',function($data, $row_name, $all_roles){
			$user_id = $data['user_id'];
			$role_id = $data['role_id'];

			$role_name = $all_roles[$role_id] ?? '';

			$data['display_name'].= !empty($role_name) ? '<br><span class="label label-default">'.$role_name.'</span>' : '';

			$data['role_id'] = $all_roles[$role_id] ?? '';

			$call_kpi_result = (int)$data['call_kpi_result'];
			$total_result = (int)$data['total_result'];

			$ordermeeting = (int) $data['meeting_num'] + (int) $data['support_meeting_num'];

			$data['call_kpi_result'] = $call_kpi_result ?: '--';
			$data['total_result'] = $total_result ?: '--';
			$data['meeting_num'] = $data['meeting_num'] ?: '--';
			$data['support_meeting_num'] = $data['support_meeting_num'	] ?: '--';
			$data['total_meeting'] = $data['total_meeting'] ?: '--';
			$data['ordermeeting_convert_kpi_result'] = $ordermeeting * CALL_PER_ORDERMETTING;
			$data['month_kpi_rate'] = currency_numberformat(div($total_result,$data['call_kpi_value'])*100,' %',2);

			return $data;
		},FALSE, $all_roles)

		->join('kpi callkpi','callkpi.user_id = user.user_id AND callkpi.kpi_type = "phonecall" AND callkpi.kpi_datetime BETWEEN \''.my_date($start_time,'Y-m-d H:i:s').'\' AND \''.my_date($end_time,'Y-m-d H:i:s').'\'','LEFT')
		->join('ordermeeting',"ordermeeting.user_id = user.user_id AND ordermeeting.status = \"verified\" AND ordermeeting.start_time BETWEEN {$start_time} AND {$end_time}",'LEFT')
		->join('ordermeeting oms',"oms.support_id = user.user_id AND oms.status = \"verified\" AND oms.start_time BETWEEN {$start_time} AND {$end_time}",'LEFT')

		->where('user_type','admin')
		->where_in('role_id',$sale_role_ids)
		->from($this->admin_m->_table)
		->order_by('[custom-select].total_result','desc')
		->group_by('user.user_id')
		->generate(['per_page' => $args['per_page'],'cur_page' => $args['cur_page']]);

		$last_query = $this->datatable_builder->last_query;
		if( !empty($last_query) && $query_parse = explode(' LIMIT ', $last_query)) 
		{
			$last_query = reset($query_parse);
			$this->scache->write($last_query,"modules/staffs/business/index-last-query-{$this->admin_m->id}");
		}

		// OUTPUT : DOWNLOAD XLSX
		if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
		{
			$this->export_kpi_tracking($last_query);
			return TRUE;
		}


		return parent::renderJson($data);
	}

	/**
	 * Export KPI Tracking Excel file
	 *
	 * @param      string  $query  The query
	 *
	 * @return     string  ( description_of_the_return_value )
	 */
	protected function export_kpi_tracking($query = '')
	{	
		if(empty($query)) return FALSE;

		// remove limit in query string
		$pos = strpos($query, 'LIMIT');
		if(FALSE !== $pos) $query = mb_substr($query, 0, $pos);

		$result = $this->call_kpi_m->query($query)->result();
		if( ! $result) return FALSE;

		$this->load->library('excel');
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
        ->getProperties()
        ->setCreator("WEBDOCTOR.VN")
        ->setLastModifiedBy("WEBDOCTOR.VN")
        ->setTitle(uniqid('Thống kê KPI cuộc gọi và lịch hẹn phòng kinh doanh __'));

        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $headings = array('#ID','Tên Hiển thị','EXT','SL Cuộc gọi','KPI cuộc gọi','SL lịch hẹn','SL hỗ trợ lịch hẹn','Tổng lịch hẹn','Quy đổi (lịch hẹn => Gọi','Tổng Đạt','Tỷ lệ đạt KPI');
        $objWorksheet->fromArray($headings, NULL, 'A1');

        $row_index = 2;
        foreach ($result as $key => $row)
		{
			$ext = get_user_meta_value($row->user_id,'phone-ext');
			$ordermeeting_convert_kpi_result = $row->total_meeting * CALL_PER_ORDERMETTING;
			$month_kpi_rate = currency_numberformat(div($row->total_result,$row->call_kpi_value)*100,' %',2);
			$row_number = $row_index + $key;

			// Set Cell
    		$objWorksheet->setCellValueByColumnAndRow(0, $row_number, $row->user_id);
    		$objWorksheet->setCellValueByColumnAndRow(1, $row_number, $row->display_name);
    		$objWorksheet->setCellValueByColumnAndRow(2, $row_number, $ext);
    		$objWorksheet->setCellValueByColumnAndRow(3, $row_number, $row->call_kpi_result ?: 0);
    		$objWorksheet->setCellValueByColumnAndRow(4, $row_number, $row->call_kpi_value ?: 0);
    		$objWorksheet->setCellValueByColumnAndRow(5, $row_number, $row->meeting_num);
    		$objWorksheet->setCellValueByColumnAndRow(6, $row_number, $row->support_meeting_num);
    		$objWorksheet->setCellValueByColumnAndRow(7, $row_number, $row->total_meeting);
    		$objWorksheet->setCellValueByColumnAndRow(8, $row_number, $ordermeeting_convert_kpi_result);
    		$objWorksheet->setCellValueByColumnAndRow(9, $row_number, $row->total_result);
    		$objWorksheet->setCellValueByColumnAndRow(10, $row_number, $month_kpi_rate);
		}

		$total_call_kpi_result = array_sum(array_column($result,'call_kpi_result'));
		$total_call_kpi_value = array_sum(array_column($result,'call_kpi_value'));
		$total_meeting_num = array_sum(array_column($result,'meeting_num'));
		$total_support_meeting_num = array_sum(array_column($result,'support_meeting_num'));
		$total_total_meeting = array_sum(array_column($result,'total_meeting'));
		$total_total_result = array_sum(array_column($result,'total_result'));
		$total_month_kpi_rate = currency_numberformat(div($total_total_result,$total_call_kpi_value)*100,' %',2);

		// Sumary row
		$row_number+=1;
		$objWorksheet->setCellValueByColumnAndRow(0, $row_number, 'Tổng : ');
		$objWorksheet->setCellValueByColumnAndRow(3, $row_number, $total_call_kpi_result ?: 0);
		$objWorksheet->setCellValueByColumnAndRow(4, $row_number, $total_call_kpi_value ?: 0);
		$objWorksheet->setCellValueByColumnAndRow(5, $row_number, $total_meeting_num);
		$objWorksheet->setCellValueByColumnAndRow(6, $row_number, $total_support_meeting_num);
		$objWorksheet->setCellValueByColumnAndRow(7, $row_number, $total_total_meeting);
		$objWorksheet->setCellValueByColumnAndRow(9, $row_number, $total_total_result);
		$objWorksheet->setCellValueByColumnAndRow(10, $row_number, $total_month_kpi_rate);

		$num_rows = count($result);

        // We'll be outputting an excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $folder_upload = 'files/bussiness/kpi_tracking/';
        if(!is_dir($folder_upload))
        {
            try 
            {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $file_name = $folder_upload.'report-tracking-'.my_date(time(),'Y-m-d-H-i-s').'.xlsx';
       	try 
        {
            $objWriter->save($file_name);
        }
        catch (Exception $e) 
        {
            trigger_error($e->getMessage());
            return FALSE; 
        }

        $this->load->helper('download');
		force_download($file_name, NULL);
		return TRUE;
	}


	/**
	 * Export KPI Tracking Excel file
	 *
	 * @param      string  $query  The query
	 *
	 * @return     string  ( description_of_the_return_value )
	 */
	protected function export_ordermeeting($query = '')
	{	
		if(empty($query)) return FALSE;

		// remove limit in query string
		$pos = strpos($query, 'LIMIT');
		if(FALSE !== $pos) $query = mb_substr($query, 0, $pos);

		$ordermeetings = $this->ordermeeting_m->query($query)->result();
		if( ! $ordermeetings) return FALSE;

		$this->load->helper('file');
		$this->load->library('excel');
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '512MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("WEBDOCTOR.VN")->setLastModifiedBy("WEBDOCTOR.VN")->setTitle(uniqid('Danh sách lịch hẹn khách hàng __'));

		$objPHPExcel->setActiveSheetIndex(0);
		$objWorksheet = $objPHPExcel->getActiveSheet();

		$headings = array('STT','NGÀY TẠO','NGÀY HẸN','NỘI DUNG','KHÁCH HÀNG','NGƯỜI LIÊN HỆ','SỐ ĐIỆN THOẠI','KINH DOANH','KINH DOANH HỖ TRỢ','TRẠNG THÁI');
		$objWorksheet->fromArray($headings, NULL, 'A1');

		$row_index = 2;
		foreach ($ordermeetings as $key => $ordermeeting)
		{	
			$row_number = $row_index + $key;

			$objWorksheet->setCellValueByColumnAndRow(0, $row_number, ($key+1));

			// NGÀY TẠO
			$created_on		= my_date($ordermeeting->created_on, 'd/m/Y H:i A') ;
			$objWorksheet->setCellValueByColumnAndRow(1, $row_number, $created_on);
			
			// NGÀY HẸN
			$start_time     = my_date($ordermeeting->start_time, 'd/m/Y H:i A') ;	
			$objWorksheet->setCellValueByColumnAndRow(2, $row_number, $start_time);

			// NỘI DUNG
			$content     = $ordermeeting->content ?: '';
			$objWorksheet->setCellValueByColumnAndRow(3, $row_number, $content);

			// KHÁCH HÀNG
			$display_name = $ordermeeting->display_name ?: '';
			$objWorksheet->setCellValueByColumnAndRow(4, $row_number, $display_name);

			// NGƯỜI LIÊN HỆ
			$cus_name = $ordermeeting->cus_name ?: '';
			$objWorksheet->setCellValueByColumnAndRow(5, $row_number, $cus_name);

			// SỐ ĐIỆN THOẠI
			$cus_phone = $ordermeeting->cus_phone ?: '';
			$objWorksheet->setCellValueExplicit("G{$row_number}", $cus_phone,PHPExcel_Cell_DataType::TYPE_STRING);

			// KINH DOANH
			$user_id = $ordermeeting->user_id;
			$objWorksheet->setCellValueByColumnAndRow(7, $row_number, $this->admin_m->get_field_by_id($user_id, 'display_name') ?: '');
			
			// KINH DOANH HỖ TRỢ 
			$support_id = $ordermeeting->support_id;  
			$objWorksheet->setCellValueByColumnAndRow(8, $row_number, $this->admin_m->get_field_by_id($support_id, 'display_name') ?: '');
			
			// TRẠNG THÁI
			$status = $ordermeeting->status;
			$objWorksheet->setCellValueByColumnAndRow(9, $row_number, $this->config->item($status,'processStates') ?: '');
		}

		$num_rows = count($ordermeetings);

		// Set Cells style for Date
		$objPHPExcel->getActiveSheet()
			->getStyle('B'.$row_index.':B'.($num_rows+$row_index))
			->getNumberFormat()
			->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

		$objPHPExcel->getActiveSheet()
			   	->getStyle('C'.$row_index.':C'.($num_rows+$row_index))
			   	->getNumberFormat()
			->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);


		// We'll be outputting an excel file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		$folder_upload = "files/ordermeeting/";

        if(!is_dir($folder_upload))
        {
            try 
            {
                $oldmask = umask(0);
                mkdir($folder_upload, 0777, TRUE);
                umask($oldmask);
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $file_name = $folder_upload . 'export-ordermeeting-list-' . my_date(time(), 'd-m-Y') . '.xlsx';

       	try  { $objWriter->save($file_name); }
        catch (Exception $e) { trigger_error($e->getMessage()); return FALSE;  }

        $this->load->helper('download');
		force_download($file_name, NULL);
		return TRUE;
	}

	protected function search_filter($search_args = array())
	{
		$args = $this->input->get(NULL, TRUE);

		if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

		$filter_user_email 	= $args['where']['user_email'] ?? FALSE;
		$sort_user_email 	= $args['order_by']['user_email'] ?? FALSE;
		if($filter_user_email || $sort_user_email)
		{
			$this->datatable_builder->join('user', 'user.user_id = ordermeeting.user_id');

			if($filter_user_email)
			{
				$this->datatable_builder->like('user.user_email', trim($filter_user_email));
			}

			if($sort_user_email)
			{
				$this->datatable_builder->order_by('user.user_email', $sort_user_email);
			}

			unset($args['where']['user_email'],$args['order_by']['user_email']);
		}

		$filter_support_email 	= $args['where']['support_email'] ?? FALSE;
		$sort_support_email 	= $args['order_by']['support_email'] ?? FALSE;
		if($filter_support_email || $sort_support_email)
		{
			$alias = uniqid('support_name_');
			$this->datatable_builder->join("user {$alias}", "{$alias}.user_id = ordermeeting.support_id");

			if($filter_support_email)
			{
				$this->datatable_builder->like("{$alias}.user_email", trim($filter_support_email));
			}

			if($sort_support_email)
			{
				$this->datatable_builder->order_by("{$alias}.user_email", $sort_support_email);
			}

			unset($args['where']['support_email'],$args['order_by']['support_email']);
		}

		$filter_user_status = $args['where']['user_status'] ?? FALSE;
		$sort_user_status 	= $args['order_by']['user_status'] ?? FALSE;
		if($filter_user_status || $sort_user_status)
		{
			if(FALSE !== $filter_user_status && is_numeric($filter_user_status))
			{	
				$this->datatable_builder->where("user_status",$filter_user_status);
			}

			if($sort_user_status)
			{
				$this->datatable_builder->order_by("user.user_status",$sort_user_status);
			}

			unset($args['where']['user_status'],$args['order_by']['user_status']);
		}

		if(!empty($args['where']['daterange']))
		{
			unset($args['where']['daterange']);
		}

		if(!empty($args['order_by']['call_kpi_result']))
		{
			$this->datatable_builder->order_by('[custom-select].call_kpi_result',$args['order_by']['call_kpi_result']);
			unset($args['order_by']['call_kpi_result']);
		}

		if(!empty($args['order_by']['call_kpi_value']))
		{
			$this->datatable_builder->order_by('[custom-select].call_kpi_value',$args['order_by']['call_kpi_value']);
			unset($args['order_by']['call_kpi_value']);
		}

		if(!empty($args['order_by']['meeting_num']))
		{
			$this->datatable_builder->order_by('[custom-select].meeting_num',$args['order_by']['meeting_num']);
			unset($args['order_by']['meeting_num']);
		}

		if(!empty($args['order_by']['support_meeting_num']))
		{
			$this->datatable_builder->order_by('[custom-select].support_meeting_num',$args['order_by']['support_meeting_num']);
			unset($args['order_by']['support_meeting_num']);
		}

		if(!empty($args['order_by']['total_meeting']))
		{
			$this->datatable_builder->order_by('[custom-select].total_meeting',$args['order_by']['total_meeting']);
			unset($args['order_by']['total_meeting']);
		}

		if(!empty($args['order_by']['total_result']))
		{
			$this->datatable_builder->order_by('[custom-select].total_result',$args['order_by']['total_result']);
			unset($args['order_by']['total_result']);
		}

		/* APPLY ORDERMEETING DATASET FILTERS */

		// Created_on FILTERING & SORTING
		$filter_created_on = $args['where']['created_on'] ?? FALSE;
		if($filter_created_on)
		{
			if($filter_created_on)
			{	
				$dates 		= explode(' - ', $filter_created_on);
				$start_time = $this->mdate->startOfDay(reset($dates));
				$end_time 	= $this->mdate->endOfDay(end($dates));

				$this->datatable_builder->where('created_on >=', $start_time);
				$this->datatable_builder->where('created_on <=', $end_time);
				unset($args['where']['created_on']);
			}
		}

		// start_time FILTERING & SORTING
		$filter_start_time = $args['where']['start_time'] ?? FALSE;
		if($filter_start_time)
		{
			if($filter_start_time)
			{	
				$dates 		= explode(' - ', $filter_start_time);
				$start_time = $this->mdate->startOfDay(reset($dates));
				$end_time 	= $this->mdate->endOfDay(end($dates));

				$this->datatable_builder->where('start_time >=', $start_time);
				$this->datatable_builder->where('start_time <=', $end_time);
				unset($args['where']['start_time']);
			}
		}

		// APPLY DEFAULT FILTER BY MUTATE FIELD		
		$args = $this->datatable_builder->parse_relations_searches($args);
		if( ! empty($args['where']))
		{
			foreach ($args['where'] as $key => $value)
			{
				if(empty($value)) continue;

				if(empty($key))
				{
					$this->datatable_builder->add_filter($value, '');
					continue;
				}

				$this->datatable_builder->add_filter($key, $value);
			}
		}

		if(!empty($args['order_by'])) 
		{
			foreach ($args['order_by'] as $key => $value)
			{
				$this->datatable_builder->order_by($key, $value);
			}
		}
	}
}
/* End of file banner.php */
/* Location: ./application/modules/banner/controllers/ajax/dataset.php */