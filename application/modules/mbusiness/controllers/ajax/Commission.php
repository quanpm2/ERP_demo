<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Commission extends Admin_Controller
{
	function __construct()
	{
		parent::__construct();

        $model_names = array('staffs/commission_ticket_direct_m', 'staffs/commission_ticket_indirect_m');
        $this->load->model($model_names);
	}

    /**
     * Get commission info from user_id
     *
     * @param      integer  $user_id  The user identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function user($user_id = 0)
    {
        $response   = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

        if( ! has_permission('mbusiness.commission.access'))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $this->load->model('staffs/sale_m'); 
        $sale = $this->sale_m->select('user_id')->set_user_type()->set_role()->get($user_id);
        if( ! $sale)
        {
            $response['msg'] = 'Người dùng không phải nhân viên kinh doanh.';
            return parent::renderJson($response);
        }

        $relate_users   = $this->admin_m->get_all_by_permissions('mbusiness.commission.access');

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        if($relate_users === FALSE || ( is_array($relate_users) && ! in_array($user_id, $relate_users) ))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $default    = ['month' => date('m'), 'year' => date('Y')];
        $args       = wp_parse_args($this->input->get(), $default);

        if( ! is_numeric($args['month']) || ! is_numeric($args['year']) || ! checkdate($args['month'], 01, $args['year'])) 
        {
            $response['msg'] = 'Thời gian yêu cầu không hợp lệ';
            return parent::renderJson($response);
        }

        $start_time = start_of_month("{$args['year']}/{$args['month']}/01");
        $end_time   = end_of_month($start_time);

        $this->load->model('staffs/commission_ticket_m');
        $this->load->model('staffs/commission_ticket_direct_m');

        $commission_direct_ticket = $this->commission_ticket_direct_m
        ->select('posts.post_id, posts.post_author, posts.post_type, posts.post_content')
        ->get_by(['start_date >=' => $start_time, 'start_date <=' => $end_time, 'posts.post_author =' => $user_id]);
        if($commission_direct_ticket)
        {
            $_id = $commission_direct_ticket->post_id;
            $response['data']['direct_ticket'] = array(

                'amount_not_vat' => (int) get_post_meta_value($_id, 'amount_not_vat'),
                'contract_num' => (int) get_post_meta_value($_id, 'contract_num'),
                'amount_commisssion' => (int) get_post_meta_value($_id, 'amount_commisssion'),

                'adsplus_amount_commisssion' => (int) get_post_meta_value($_id, 'adsplus_amount_commisssion'),
                'adsplus_actually_collected' => (int) get_post_meta_value($_id, 'adsplus_actually_collected'),
                'adsplus_contract_num' => (int) get_post_meta_value($_id, 'adsplus_contract_num'),

                'webdoctor_amount_commisssion' => (int) get_post_meta_value($_id, 'webdoctor_amount_commisssion'),
                'webdoctor_actually_collected' => (int) get_post_meta_value($_id, 'webdoctor_actually_collected'),
                'webdoctor_contract_num' => (int) get_post_meta_value($_id, 'webdoctor_contract_num'),
            );
        }

        $this->load->model('staffs/commission_ticket_indirect_m');
        $commission_indirect_ticket = $this->commission_ticket_indirect_m
        ->select('posts.post_id, posts.post_author, posts.post_type, posts.post_content')
        ->get_by(['start_date >=' => $start_time, 'start_date <=' => $end_time, 'posts.post_author =' => $user_id]);

        if($commission_indirect_ticket)
        {
            $_id = $commission_indirect_ticket->post_id;
            $response['data']['indirect_ticket'] = array(

                'amount_commisssion' => (int) get_post_meta_value($_id, 'amount_commisssion'),

                'account_type_amount_kpi' => (int) get_post_meta_value($_id, 'account_type_amount_kpi'),
                'account_type_rate' => (float) get_post_meta_value($_id, 'account_type_rate'),
                'account_type_amount_commisssion' => (int) get_post_meta_value($_id, 'account_type_amount_commisssion'),

                'cpc_type_amount_kpi' => (int) get_post_meta_value($_id, 'cpc_type_amount_kpi'),
                'cpc_type_rate' => (float) get_post_meta_value($_id, 'cpc_type_rate'),
                'cpc_type_amount_commisssion' => (int) get_post_meta_value($_id, 'cpc_type_amount_commisssion'),

                'webdoctor_amount_kpi' => (int) get_post_meta_value($_id, 'webdoctor_amount_kpi'),
                'webdoctor_rate' => (float) get_post_meta_value($_id, 'webdoctor_rate'),
                'webdoctor_amount_commisssion' => (int) get_post_meta_value($_id, 'webdoctor_amount_commisssion'),
            );
        }

        $response['status'] = TRUE;
        $response['msg'] = 'Quá trình xử lý không thành công.';

        return parent::renderJson($response);
    }

    public function user_rank($user_id = 0)
    {
        $response = array('msg' => 'Quá trình xử lý không thành công', 'status' => TRUE, 'data' => '');
        if( ! has_permission('mbusiness.commission.access'))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $relate_users   = $this->admin_m->get_all_by_permissions('mbusiness.commission.access');
        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        if($relate_users === FALSE || ( is_array($relate_users) && ! in_array($user_id, $relate_users)))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }


        $year   = date('Y');
        $month  = $this->input->get('month') ?: date('m');

        $start_of_month = $this->mdate->startOfMonth("{$year}/{$month}/01");
        $end_of_month   = $this->mdate->endOfMonth($start_of_month);

        $this->load->model('staffs/commission_ticket_direct_m');
        $c_ticket = $this->commission_ticket_direct_m
        ->where('start_date >=', $start_of_month)
        ->where('start_date <=', $end_of_month)
        ->where('posts.post_author', $user_id)
        ->get_by();

        if( ! $c_ticket) 
        {
            $response['msg'] = 'Dữ liệu chưa được khởi tạo';
            return parent::renderJson($response);
        }

        $rank               = get_post_meta_value($c_ticket->post_author, 'sale_rank') ?: '??';
        $ranked_sale_num    = $this->scache->get("modules/mbusiness/ranked_sale_num_{$start_of_month}_{$end_of_month}") ?: '??';
        $data = array('index' => $rank, 'total' => $ranked_sale_num);

        $response['status'] = TRUE;
        $response['msg']    = 'Xử lý thành công';
        $response['data']   = $data;
        
        return parent::renderJson($response);
    }

    public function contract()
    {

    }

    /**
     * { function_description }
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function terms($user_id = 0)
    {
        $response   = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
        $default   = ['data_type' => '','user_id' => $user_id];
        $args       = wp_parse_args($this->input->get(), $default);

        $this->load->model('mbusiness/sale_commission_m');

        $data = $this->sale_commission_m->get_dataset(['data_type'=>'terms','user_id'=>$args['user_id']]);
        if(empty($data))
        {
            $response['msg'] = 'Dữ liệu không tìm thấy.';
            return parent::renderJson($response);
        }

        $data = array_group_by($data, function($x){

            if( ! in_array($x['commission_type'], array('account_type','cpc_type'))) return $x['commission_type'];

            return 'adsplus';
        });

        $data_type = $this->input->get('data_type') ?: 'webdoctor';
        if(empty($data[$data_type]))
        {
            $response['msg'] = 'Dữ liệu không tìm thấy.';
            return parent::renderJson($response);
        }

        $response['msg']    = 'Xử lý dữ liệu thành công.';
        $response['data']   = $data[$data_type];
        $response['status'] = TRUE;

        return parent::renderJson($response);   
    }

    /**
     * Trang tổng quan liệt kê danh sách tất cả các hợp đồng
     */
    public function sales()
    {
        $response   = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);
        $permission = 'mbusiness.commission';

        /* Kiểm tra quyền truy cập */
        if( ! has_permission("{$permission}.access"))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $default        = ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args           = wp_parse_args( $this->input->get(), $default);

        /* Kiểm tra quyền truy cập và truy xuất tất cả các nhân viên thuộc phòng ban|nhóm quản lý */
        $relate_users   = $this->admin_m->get_all_by_permissions("{$permission}.access");

        /* Không có quyền truy cập dựa vào phòng ban và nhóm */
        if($relate_users === FALSE)
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $this->load->model('staffs/sale_m');
        $sale_role_ids = $this->sale_m->get_roles();

        $roles = $this->role_m->select('role_id,role_name')->as_array()->get_many_by('role_id', $this->sale_m->get_roles());
        $roles = key_value($roles, 'role_id', 'role_name');

        $this->load->model('staffs/department_m');

        $departments = $this->department_m->select('term_id,term_name,term_parent')->set_term_type()->get_all();
        $user_groups = $this->user_group_m->select('term_id,term_name,term_parent')->set_term_type()->get_all();

        $departments = key_value($departments, 'term_id', 'term_name');
        $user_groups = key_value($user_groups, 'term_id', 'term_name');

        $this->load->model('staffs/commission_ticket_m');
        $this->load->library('datatable_builder');
        if(is_array($relate_users)) $this->datatable_builder->where_in('user.user_id', $relate_users);

        $this->load->config('staffs/staffs');

        /* Enable Department|user-group if logged user has members */
        if(TRUE == is_array($relate_users))
        {
            /* Filter available user roles */
            $u_roles    = array_map(function($u_id){ return $this->admin_m->get_field_by_id($u_id, 'role_id'); }, $relate_users);
            $roles      = array_filter($roles, function($k) use($u_roles) { return in_array($k, $u_roles); }, ARRAY_FILTER_USE_KEY);
            arsort($roles);


            /* Filter available departments */
            $u_departments  = $this->term_users_m->get_user_terms($this->admin_m->id, $this->department_m->term_type);
            $u_departments  = array_column($u_departments, 'term_id');
            $departments    = array_filter($departments, function($k) use($u_departments) { 
                return in_array($k, $u_departments); 
            }, ARRAY_FILTER_USE_KEY);
            arsort($departments);


            /* Filter available user-groups */
            $u_groups       = $this->term_users_m->get_user_terms($this->admin_m->id, $this->user_group_m->term_type);
            if( empty($u_groups))
            {
                $u_groups = array();
                foreach ($relate_users as $u_id)
                {
                    $u_id_groups = $this->term_users_m->get_user_terms($u_id, $this->user_group_m->term_type);
                    if(empty($u_id_groups)) continue;
                    $u_groups = array_merge($u_groups,$u_id_groups);
                }
            }

            if( ! empty($u_groups))
            {
                $u_groups       = array_unique(array_column($u_groups, 'term_id'));
                $user_groups    = array_filter($user_groups, function($k) use($u_groups) { 
                    return in_array($k, $u_groups); 
                }, ARRAY_FILTER_USE_KEY);
                arsort($user_groups);
            }
        }

        if(count($departments) > 1) 
        {
            $this->datatable_builder->add_search('department',array('content'=> form_dropdown(array('name'=>'where[department]','class'=>'form-control select2'),prepare_dropdown($departments,'Nhóm KD : all'), $this->input->get('where[department]'))));
        }

        if(count($user_groups) > 1)
        {
            $this->datatable_builder->add_search('group',array('content'=> form_dropdown(array('name'=>'where[group]','class'=>'form-control select2'),prepare_dropdown($user_groups,'Nhóm KD : all'), $this->input->get('where[group]'))));
        }

        if(count($roles) > 1)
        {
            $this->datatable_builder->add_search('role_id',array('content'=> form_dropdown(array('name'=>'where[role_id]','class'=>'form-control select2'),prepare_dropdown($roles,'Vai trò : all'), $this->input->get('where[role_id]'))));
        }

        if( TRUE === $relate_users || (is_array($relate_users) && count($relate_users) > 1))
        {
            $this->datatable_builder
            ->add_search('user_email',['placeholder'=>'Nhân viên kinh doanh'])
            ->add_search('user_status',array('content'=> form_dropdown(array('name'=> 'where[user_status]','class'=>'form-control select2'),prepare_dropdown($this->config->item('user_status'),'Trạng thái NVKD : All'),$this->input->get('where[user_status]'))));
        }

        $this->search_filter();

        $data = $this->datatable_builder

        ->set_filter_position(FILTER_TOP_OUTTER)
        ->select('user.user_id, user.user_avatar, user.role_id, posts.post_id')
        ->select('indirect_tickets.post_id as indirect_post_id')

        ->add_search('month',['placeholder'=>'Tháng','class'=>'form-control input_monthpicker'])
        ->add_column('rank', ['set_select' => FALSE,'title' => 'Hạng'])
        ->add_column('user.display_name', ['set_select' => TRUE,'title' => 'NVKD'])
        ->add_column('contract_num', ['set_select' => FALSE,'title' => 'HĐ'])
        ->add_column('amount_not_vat', ['set_select' => FALSE,'title' => 'Doanh số'])
        ->add_column('amount_kpi', ['set_select' => FALSE,'title' => 'DT KPIS'])
        ->add_column('actually_collected', ['set_select' => FALSE,'title' => 'DT Thưởng'])
        ->add_column('adsplus_amount_commisssion', ['set_select' => FALSE,'title' => 'Thưởng Adsplus'])
        ->add_column('webdoctor_amount_commisssion', ['set_select' => FALSE,'title' => 'Thưởng Webdoctor'])
        ->add_column('amount_commisssion', ['set_select' => FALSE,'title' => '+ hoa hồng'])
        ->add_column('indirect_amount_commisssion', ['set_select' => FALSE,'title' => '+ Quản lý'])
        ->add_column('total_amount_commisssion', ['set_select' => FALSE, 'set_order' => FALSE,'title' => 'Tổng thưởng'])

        ->add_column_callback('user_id',function($data, $row_name, $args){

            $post_id    = $data['post_id'];
            $user_id    = $data['user_id'];

            /* Retrieve sale rank by commission amount kpi point */

            $sale_rank = (int) get_post_meta_value($post_id, 'sale_rank');
            switch (TRUE)
            {
                case $sale_rank > 0 && $sale_rank <= 2 : 
                    $sale_rank = "<small class='label pull-right bg-blue'>{$sale_rank}</small>";
                    break;

                case $sale_rank > 2 && $sale_rank <= 10 : 
                    $sale_rank = "<small class='label pull-right bg-green'>{$sale_rank}</small>";
                    break;

                case $sale_rank > 10 && $sale_rank <= 20 : 
                    $sale_rank = "<small class='label pull-right bg-yellow'>{$sale_rank}</small>";
                    break;

                default : 
                    $sale_rank = "<small class='label pull-right bg-red'>??</small>";
                    break;        
            }

            $sale_rank.= img($data['user_avatar'], FALSE, ['class'=>'img-circle','alt'=>'User Image','width'=>50,'height'=>50]);
            $sale_rank = anchor(admin_url("mbusiness/commission/{$user_id}"), $sale_rank, ['target'=>'_blank']);


            /* Retrieve "user group label name" */
            $group = '';
            $this->load->model('staffs/user_group_m');
            $user_groups = $this->term_users_m->get_user_terms($user_id, $this->user_group_m->term_type);
            if( ! empty($user_groups)) $group = implode(', ', array_column($user_groups, 'term_name'));

            /* Retrieve "deparment label name" */
            $department = '';
            $this->load->model('staffs/department_m');
            $departments = $this->term_users_m->get_user_terms($user_id, $this->department_m->term_type);
            if( ! empty($departments)) $department = implode(', ', array_column($departments, 'term_name'));


            $data['display_name'].= '<br/>';

            $role_name = $args['roles'][$data['role_id']] ?? 'Chưa xác định';

            $data['display_name'].= "<span class='text-muted col-xs-12' data-toggle='tooltip' title='Vai trò'><i class='text-yellow fa fa-fw fa-star'></i>{$role_name}</span>";

            $data['display_name'].= "<span class='text-muted col-xs-12' data-toggle='tooltip' title='Nhóm'><i class='text-success fa fa-fw fa-users'></i>{$group} - {$department}</span>";


            $data['rank'] = $sale_rank;

            $data['department'] = $department;
            $data['group']      = $group;

            $data['contract_num']   = '--';
            $data['amount_not_vat'] = '--';
            $data['amount_kpi']     = '--';

            $data['actually_collected']             = '--';
            $data['adsplus_amount_commisssion']     = '--';
            $data['webdoctor_amount_commisssion']   = '--';
            $data['amount_commisssion']             = '--';
            $data['indirect_amount_commisssion']    = '--';

            $c_types = array('cpc_type' => 'CPC', 'account_type' => 'QLTK', 'webdoctor' => 'Webdoctor');

            $indirect_amount_commisssion = 0;
            if( ! empty($data['indirect_post_id']))
            {
                $indirect_post_id = $data['indirect_post_id'];
                $indirect_amount_commisssion = get_post_meta_value($indirect_post_id, 'amount_commisssion');
                $data['indirect_amount_commisssion'] = currency_numberformat($indirect_amount_commisssion, 'đ');

                foreach (array_keys($c_types) as $c_t)
                {
                    $_amount_kpi = (float) get_post_meta_value($indirect_post_id, "{$c_t}_amount_kpi");
                    if($_amount_kpi <= 0) continue;

                    $_rate          = ((float) get_post_meta_value($indirect_post_id, "{$c_t}_rate")) * 100;
                    $_rate_f        = currency_numberformat($_rate, '%');
                    $_amount_kpi_f  = currency_numberformat($_amount_kpi, 'đ');

                    $data['indirect_amount_commisssion'].= "<br/><small><i><span class='text-blue'>{$_rate_f}_of_</span><span class='text-red'>{$_amount_kpi_f}_{$c_types[$c_t]}</i></small></span>";
                }
            }

            $total_amount_commisssion = $indirect_amount_commisssion;
            $data['total_amount_commisssion'] = currency_numberformat($total_amount_commisssion, 'đ');


            if(empty($post_id)) return $data;

            $contract_num           = get_post_meta_value($post_id, 'contract_num') ?: 0;
            $data['contract_num']   = $contract_num;

            $amount_not_vat         = get_post_meta_value($post_id, 'amount_not_vat') ?: 0;
            $data['amount_not_vat'] = currency_numberformat($amount_not_vat, 'đ');

            $amount_kpi             = get_post_meta_value($post_id, 'amount_kpi') ?: 0;
            $data['amount_kpi']     = currency_numberformat($amount_kpi, 'đ');

            $actually_collected         = get_post_meta_value($post_id, 'actually_collected') ?: 0;
            $data['actually_collected'] = currency_numberformat($actually_collected, 'đ');

            $adsplus_amount_commisssion         = get_post_meta_value($post_id, 'adsplus_amount_commisssion') ?: 0;
            $data['adsplus_amount_commisssion'] = currency_numberformat($adsplus_amount_commisssion, 'đ');

            $webdoctor_amount_commisssion           = get_post_meta_value($post_id, 'webdoctor_amount_commisssion') ?: 0;
            $data['webdoctor_amount_commisssion']   = currency_numberformat($webdoctor_amount_commisssion, 'đ');

            foreach (array_keys($c_types) as $c_t)
            {
                $_amount_kpi = (float) get_post_meta_value($post_id, "{$c_t}_amount_kpi");
                if($_amount_kpi <= 0) continue;

                $_rate          = ((float) get_post_meta_value($post_id, "{$c_t}_rate")) * 100;
                $_rate_f        = currency_numberformat($_rate, '%');
                $_amount_kpi_f  = currency_numberformat($_amount_kpi, 'đ');

                $amount_commisssion_prefix = $c_t == 'webdoctor' ? 'webdoctor' : 'adsplus';
                $data["{$amount_commisssion_prefix}_amount_commisssion"].= "<br/><small><i><span class='text-blue'>{$_rate_f}_of_</span><span class='text-red'>{$_amount_kpi_f}_{$c_types[$c_t]}</i></small></span>";
            }

            $amount_commisssion                 = get_post_meta_value($post_id, 'amount_commisssion') ?: 0;
            $data['amount_commisssion']         = currency_numberformat($amount_commisssion, 'đ');

            $total_amount_commisssion           += $amount_commisssion;
            $data['total_amount_commisssion']   = currency_numberformat($total_amount_commisssion, 'đ');

            return $data;
        },FALSE, array('departments'=>$departments, 'user_groups'=>$user_groups, 'roles'=>$roles))

        ->from($this->sale_m->_table)
        ->where('user_type', 'admin')
        ->order_by('(posts.post_content)*1','desc')
        ->where_in('role_id',$sale_role_ids)
        ->group_by('user.user_id')
        ->generate(['per_page' => $args['per_page'],'cur_page' => $args['cur_page']]);

        $last_query = $this->datatable_builder->last_query();
        if( !empty($last_query) && $query_parse = explode(' LIMIT ', $last_query)) 
        {
            $last_query = reset($query_parse);
            $this->scache->write($last_query,"modules/staffs/business/index-last-query-{$this->admin_m->id}");
        }

        // OUTPUT : DOWNLOAD XLSX
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->export_sale_commissions($last_query);
            return TRUE;
        }

        return parent::renderJson($data);
    }


    /**
     * Trang tổng quan liệt kê danh sách tất cả các hợp đồng
     */
    public function contracts()
    {
        $response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

        if( ! has_permission('mbusiness.commission.access'))
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $default    = ['month' => date('m'), 'year' => date('Y'), 'user_id' => $this->admin_m->id];
        $args       = wp_parse_args( $this->input->get(), $default);

        if( ! is_numeric($args['month']) || ! is_numeric($args['year']) || ! checkdate($args['month'], 01, $args['year']))
        {
            $response['msg'] = 'Tham số truyền vào không hợp lệ';
            return parent::renderJson($response);
        }

        $relate_users   = $this->admin_m->get_all_by_permissions('mbusiness.commission.access');
        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        if($relate_users === FALSE)
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::renderJson($response);
        }

        $this->load->model('staffs/commission_ticket_m');
        $this->load->model('staffs/commission_ticket_direct_m');
        $this->load->model('staffs/commission_ticket_indirect_m');

        if(is_array($relate_users) && !in_array($args['user_id'], $relate_users))
        {
            $response['msg'] = 'Quyền truy cập thông tin hoa hồng nhân viên không hợp lệ.';
            return parent::renderJson($response);
        }

        $this->commission_ticket_m->where_in('posts.post_author', $args['user_id']);

        $start_time = start_of_month("{$args['year']}/{$args['month']}/01");
        $end_time   = end_of_month($start_time);

        $commission_tickets = $this->commission_ticket_direct_m->get_many_by(['start_date >=' => $start_time, 'start_date <=' => $end_time]);
        if( ! $commission_tickets) 
        {
            $response['msg'] = 'Dữ liệu không được tìm thấy';
            parent::renderJson($response);
        }
        
        $this->load->model('term_users_m');
        $this->load->model('staffs/sale_m');
        $this->load->model('staffs/department_m');
        $this->load->model('staffs/user_group_m');


        $roles = $this->role_m->select('role_id,role_name')->as_array()->get_many_by('role_id', $this->sale_m->get_roles());
        $roles = key_value($roles, 'role_id','role_name');

        $result = array();
        foreach ($commission_tickets as $c_ticket)
        {
            $term_commission_types = get_post_meta_value($c_ticket->post_id, 'term_commission_types');
            $term_commission_types = unserialize($term_commission_types);
            $commission_rate = array(
                'cpc_type'      => (float) get_post_meta_value($c_ticket->post_id, 'cpc_type_rate'),
                'account_type'  => (float) get_post_meta_value($c_ticket->post_id, 'account_type_rate'),
                'webdoctor'     => (float) get_post_meta_value($c_ticket->post_id, 'webdoctor_rate'),
                'none'          => 0,
            );

            $role_id = $this->admin_m->get_field_by_id($c_ticket->post_author, 'role_id');
            $departments = $this->term_users_m->get_user_terms($c_ticket->post_author, $this->department_m->term_type);
            $user_groups = $this->term_users_m->get_user_terms($c_ticket->post_author, $this->user_group_m->term_type);

            $sale = array(
                'user_id' => $c_ticket->post_author,
                'display_name' => $this->admin_m->get_field_by_id($c_ticket->post_author, 'display_name'),
                'role_id' => $role_id,
                'role_name' => $roles[$role_id] ?? '',
                'user_group' => (!empty($user_groups) ? implode(', ', array_column($user_groups, 'term_name')) : ''),
                'department' => (!empty($departments) ? implode(', ', array_column($departments, 'term_name')) : ''),
            );

            $term_commission_types = unserialize(get_post_meta_value($c_ticket->post_id, 'term_commission_types'));
            if(empty($term_commission_types)) continue;

            if(empty($args['commission_type']))
            {
                foreach ($term_commission_types as $commission_type => $_terms)
                {
                    foreach ($_terms as $t)
                    {
                        $t['commission_rate'] = $commission_rate[$commission_type];
                        $t['user'] = $sale;
                        $result[$t['term_id']] = $t;
                    }
                }
                continue;
            }

            if(empty($term_commission_types[$args['commission_type']])) continue;

            foreach ($term_commission_types[$args['commission_type']] as $t)
            {
                $t['commission_rate'] = $commission_rate[$args['commission_type']];
                $t['user'] = $sale;
                $result[$t['term_id']] = $t;
            }
        }

        $response['msg']    = 'Xử lý thành công . ';
        $response['status'] = TRUE;
        $response['data']   = array_values($result);

        return parent::renderJson($response);
    }

    protected function search_filter($search_args = array())
    {
        $args = $this->input->get(NULL, TRUE);

        /* SET DEFAULT DB QUERY CONSTRAINT */
        $filter_month = $this->input->get('where[month]', TRUE) ?: date('m-Y');
        $filter_month = date("01-{$filter_month}");

        $start_of_month = $this->mdate->startOfMonth($filter_month);
        $end_of_month   = $this->mdate->endOfMonth($start_of_month);

        $ticket_direct_join_queries = array(
            'posts.post_author  = user.user_id',
            "posts.post_type    = '{$this->commission_ticket_m->post_type}'",
            "posts.post_slug    = '{$this->commission_ticket_direct_m->commission_level}'",
            "posts.start_date >= {$start_of_month}",
            "posts.start_date <= {$end_of_month}");
        $this->datatable_builder->join('posts', implode(' AND ', $ticket_direct_join_queries));

        $ticket_direct_join_queries = array(
            'indirect_tickets.post_author  = user.user_id',
            "indirect_tickets.post_type    = '{$this->commission_ticket_m->post_type}'",
            "indirect_tickets.post_slug    = '{$this->commission_ticket_indirect_m->commission_level}'",
            "indirect_tickets.start_date >= {$start_of_month}",
            "indirect_tickets.start_date <= {$end_of_month}");
        $this->datatable_builder->join('posts indirect_tickets', implode(' AND ', $ticket_direct_join_queries), 'LEFT');

        unset($args['where']['month']);

        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

        $filter_group  = $args['where']['group'] ?? FALSE;
        if($filter_group)
        {
            $this->load->model('term_users_m');

            $alias = uniqid();
            $group_alias = "grouptlb_{$alias}_";

            $this->datatable_builder
            ->join("term_users {$alias}", "{$alias}.user_id = user.user_id")
            ->join("term {$group_alias}", "{$group_alias}.term_id = {$alias}.term_id AND {$group_alias}.term_type = 'user_group'")
            ->where("{$group_alias}.term_id", $filter_group);

            unset($args['where']['group']);
        }

        $filter_department = $args['where']['department'] ?? FALSE;
        if($filter_department)
        {
            $this->load->model('term_users_m');

            $alias = uniqid();
            $department_alias = "departmenttlb_{$alias}_";

            $this->datatable_builder
            ->join("term_users {$alias}", "{$alias}.user_id = user.user_id")
            ->join("term {$department_alias}", "{$department_alias}.term_id = {$alias}.term_id AND {$department_alias}.term_type = 'department'")
            ->where("{$department_alias}.term_id", $filter_department);
            
            unset($args['where']['department']);
        }


        $filter_user_email   = $args['where']['user_email'] ?? FALSE;
        $sort_user_email     = $args['order_by']['user_email'] ?? FALSE;
        if($filter_user_email || $sort_user_email)
        {
            if($filter_user_email)
            {
                $this->datatable_builder->like('user.user_email', $filter_user_email);
            }

            if($sort_user_email)
            {
                $this->datatable_builder->order_by('user.user_email', $sort_user_email);
            }

            unset($args['where']['user_email'],$args['order_by']['user_email']);
        }

        $filter_user_status = $args['where']['user_status'] ?? FALSE;
        $sort_user_status   = $args['order_by']['user_status'] ?? FALSE;
        if($filter_user_status || $sort_user_status)
        {
            if(FALSE !== $filter_user_status && is_numeric($filter_user_status))
            {   
                $this->datatable_builder->where("user_status",$filter_user_status);
            }

            if($sort_user_status)
            {
                $this->datatable_builder->order_by("user.user_status",$sort_user_status);
            }

            unset($args['where']['user_status'],$args['order_by']['user_status']);
        }


        $sort_rank = $args['order_by']['rank'] ?? FALSE;
        if($sort_rank)
        {
            $alias = uniqid('rank_');
            $this->datatable_builder
            ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'sale_rank'", 'LEFT')
            ->select("IFNULL({$alias}.meta_value, 9999) as sale_rank")
            // ->order_by("SIGN({$alias}.meta_value)", 'DESC')
            // ->order_by("ABS({$alias}.meta_value)", $sort_rank);
            ->order_by("([custom-select].sale_rank)*1", $sort_rank);
            unset($args['where']['rank'],$args['order_by']['rank']);
        }


        $sort_contract_num   = $args['order_by']['contract_num'] ?? FALSE;
        if($sort_contract_num)
        {
            $alias = uniqid('contract_num_');
            $this->datatable_builder
            ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'contract_num'")
            ->order_by("({$alias}.meta_value)*1",$sort_contract_num);

            unset($args['where']['contract_num'],$args['order_by']['contract_num']);
        }


        $sort_amount_not_vat   = $args['order_by']['amount_not_vat'] ?? FALSE;
        if($sort_amount_not_vat)
        {
            $alias = uniqid('amount_not_vat_');
            $this->datatable_builder
            ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'amount_not_vat'")
            ->order_by("({$alias}.meta_value)*1",$sort_amount_not_vat);

            unset($args['where']['amount_not_vat'],$args['order_by']['amount_not_vat']);
        }

        $sort_amount_kpi   = $args['order_by']['amount_kpi'] ?? FALSE;
        if($sort_amount_kpi)
        {
            $alias = uniqid('amount_kpi_');
            $this->datatable_builder
            ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'amount_kpi'")
            ->order_by("({$alias}.meta_value)*1",$sort_amount_kpi);

            unset($args['where']['amount_kpi'],$args['order_by']['amount_kpi']);
        }
        
        $sort_actually_collected   = $args['order_by']['actually_collected'] ?? FALSE;
        if($sort_actually_collected)
        {
            $alias = uniqid('actually_collected_');
            $this->datatable_builder
            ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'actually_collected'")
            ->order_by("({$alias}.meta_value)*1",$sort_actually_collected);

            unset($args['where']['actually_collected'],$args['order_by']['actually_collected']);
        }

        $sort_webdoctor_amount_commisssion   = $args['order_by']['webdoctor_amount_commisssion'] ?? FALSE;
        if($sort_webdoctor_amount_commisssion)
        {
            $alias = uniqid('webdoctor_amount_commisssion_');
            $this->datatable_builder
            ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'webdoctor_amount_commisssion'")
            ->order_by("({$alias}.meta_value)*1",$sort_webdoctor_amount_commisssion);

            unset($args['where']['webdoctor_amount_commisssion'],$args['order_by']['webdoctor_amount_commisssion']);
        }

        $sort_adsplus_amount_commisssion   = $args['order_by']['adsplus_amount_commisssion'] ?? FALSE;
        if($sort_adsplus_amount_commisssion)
        {
            $alias = uniqid('adsplus_amount_commisssion_');
            $this->datatable_builder
            ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'adsplus_amount_commisssion'")
            ->order_by("({$alias}.meta_value)*1",$sort_adsplus_amount_commisssion);

            unset($args['where']['adsplus_amount_commisssion'],$args['order_by']['adsplus_amount_commisssion']);
        }

        $sort_amount_commisssion   = $args['order_by']['amount_commisssion'] ?? FALSE;
        if($sort_amount_commisssion)
        {
            $alias = uniqid('amount_commisssion_');
            $this->datatable_builder
            ->join("postmeta {$alias}", "{$alias}.post_id = posts.post_id AND {$alias}.meta_key = 'amount_commisssion'")
            ->order_by("({$alias}.meta_value)*1",$sort_amount_commisssion);

            unset($args['where']['amount_commisssion'],$args['order_by']['amount_commisssion']);
        }
        
        $sort_indirect_amount_commisssion   = $args['order_by']['indirect_amount_commisssion'] ?? FALSE;
        if($sort_indirect_amount_commisssion)
        {
            $this->datatable_builder->order_by("(indirect_tickets.post_content)*1",$sort_indirect_amount_commisssion);

            unset($args['where']['indirect_amount_commisssion'],$args['order_by']['indirect_amount_commisssion']);
        }


        // APPLY DEFAULT FILTER BY MUTATE FIELD     
        $args = $this->datatable_builder->parse_relations_searches($args);
        if( ! empty($args['where']))
        {
            foreach ($args['where'] as $key => $value)
            {
                if(empty($value)) continue;

                if(empty($key))
                {
                    $this->datatable_builder->add_filter($value, '');
                    continue;
                }

                $this->datatable_builder->add_filter($key, $value);
            }
        }

        if(!empty($args['order_by'])) 
        {
            foreach ($args['order_by'] as $key => $value)
            {
                $this->datatable_builder->order_by($key, $value);
            }
        }
    }

    protected function export_sale_commissions($query = '')
    {
        if(empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if(FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $commission_tickets = $this->commission_ticket_m->query($query)->result();
        if( ! $commission_tickets)
        {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(),'refresh');
        }

        $this->load->library('excel');
        $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
        ->getProperties()
        ->setCreator("WEBDOCTOR.VN")
        ->setLastModifiedBy("WEBDOCTOR.VN")
        ->setTitle(uniqid('Danh sách hoa hồng trực tiếp tháng thanh toán __'));

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $this->load->model('staffs/department_m');
        $this->load->model('staffs/user_group_m');
        $this->load->model('staffs/sale_m');

        $roles  = $this->role_m->select('role_id,role_name')->where_in('role_id', $this->sale_m->get_roles())->get_all();
        $roles  = key_value($roles, 'role_id', 'role_name');

        $departments = $this->department_m->select('term_id,term_name,term_parent')->set_term_type()->get_all();
        $departments = key_value($departments, 'term_id', 'term_name');

        $user_groups = $this->user_group_m->select('term_id,term_name,term_parent')->set_term_type()->get_all();
        $user_groups = key_value($user_groups, 'term_id', 'term_name'); 

        /* SECTION : TITLE + HEADINGS ROWS FOR THIS REPORT */
        $objWorksheet->setCellValueByColumnAndRow(1, 1, 'HOA HỒNG TRỰC TIẾP');
        $headings = array('Hạng', 'NVKD', 'Phòng ban', 'Nhóm kinh doanh', 'Vai trò', 'SL HĐ', 'Doanh số', 'Doanh thu KPIs', 'Doanh thu thưởng', 'Thưởng ADSPLUS', 'Thưởng WEBDOCTOR','Hoa hồng bán hàng','Thưởng trưởng nhóm','Tổng thưởng');
        foreach ($headings as $key => $heading)
        {
            $objWorksheet->setCellValueByColumnAndRow($key, 2, $heading);
        }
        /* END SECTION */

        $row_index = 3;

        foreach ($commission_tickets as $key => &$item)
        {
            $item->rank           = get_post_meta_value($item->post_id, 'sale_rank') ?: '??';
            $item->display_name   = $item->display_name;
            $item->department     = '';
            $item->user_group     = '';

            $item->role_name      = $roles[$item->role_id] ?? '';

            $item->contract_num   = get_post_meta_value($item->post_id, 'contract_num') ?: 0;
            $item->amount_not_vat = get_post_meta_value($item->post_id, 'amount_not_vat') ?: 0;
            $item->amount_kpi     = get_post_meta_value($item->post_id, 'amount_kpi') ?: 0;

            $item->actually_collected               = get_post_meta_value($item->post_id, 'actually_collected') ?: 0;
            $item->adsplus_amount_commisssion       = get_post_meta_value($item->post_id, 'adsplus_amount_commisssion') ?: 0;
            $item->webdoctor_amount_commisssion     = get_post_meta_value($item->post_id, 'webdoctor_amount_commisssion') ?: 0;
            $item->amount_commisssion               = get_post_meta_value($item->post_id, 'amount_commisssion') ?: 0;
            $item->indirect_amount_commisssion      = get_post_meta_value($item->indirect_post_id, 'amount_commisssion') ?: 0;
            $item->total_amount_commisssion         = $item->amount_commisssion + $item->indirect_amount_commisssion;

            /* Retrieve "user group label name" */
            $this->load->model('staffs/user_group_m');

            $item->user_group   = '';
            $user_groups        = $this->term_users_m->get_user_terms($item->user_id, $this->user_group_m->term_type);
            if( ! empty($user_groups))  $item->user_group = implode(', ', array_map(function($x){ return $x->term_name; }, $user_groups));

            /* Retrieve "Department label name" */
            $this->load->model('staffs/department_m');

            $item->department   = '';
            $departments        = $this->term_users_m->get_user_terms($item->user_id, $this->department_m->term_type);
            if( ! empty($departments))  $item->department = implode(', ', array_map(function($x){ return $x->term_name; }, $departments));

            $row_number = $row_index + $key;

            // Set Cell
            $objWorksheet->setCellValueByColumnAndRow(0, $row_number, $item->rank);
            $objWorksheet->setCellValueByColumnAndRow(1, $row_number, $item->display_name);
            $objWorksheet->setCellValueByColumnAndRow(2, $row_number, $item->department);
            $objWorksheet->setCellValueByColumnAndRow(3, $row_number, $item->user_group);
            $objWorksheet->setCellValueByColumnAndRow(4, $row_number, $item->role_name);
            $objWorksheet->setCellValueByColumnAndRow(5, $row_number, $item->contract_num);
            $objWorksheet->setCellValueByColumnAndRow(6, $row_number, $item->amount_not_vat);
            $objWorksheet->setCellValueByColumnAndRow(7, $row_number, $item->amount_kpi);
            $objWorksheet->setCellValueByColumnAndRow(8, $row_number, $item->actually_collected);
            $objWorksheet->setCellValueByColumnAndRow(9, $row_number, $item->adsplus_amount_commisssion);
            $objWorksheet->setCellValueByColumnAndRow(10, $row_number, $item->webdoctor_amount_commisssion);
            $objWorksheet->setCellValueByColumnAndRow(11, $row_number, $item->amount_commisssion);
            $objWorksheet->setCellValueByColumnAndRow(12, $row_number, $item->indirect_amount_commisssion);
            $objWorksheet->setCellValueByColumnAndRow(13, $row_number, $item->total_amount_commisssion);
        }

        $numbers_of_post = count($commission_tickets);

        // Set Cells Style For Amount
        $objPHPExcel->getActiveSheet()
            ->getStyle('E1:N'.(2 + $numbers_of_post + 4)) // 2:start position ; $number_of_post:total data rows; 2:append 2 row footer
            ->getNumberFormat()
            ->setFormatCode("#,##0");

        // Set Cells Style For Amount
        $objPHPExcel->getActiveSheet()
            ->getStyle('A2:N2') // 2:start position ; $number_of_post:total data rows; 2:append 2 row footer
            ->getFont()
            ->setBold(TRUE);

        // Set cell label summary
        $objWorksheet->setCellValueByColumnAndRow(1, ($row_index + $numbers_of_post + 2), 'Tổng cộng');

        $total_contract_num                 = array_sum(array_column($commission_tickets, 'contract_num'));
        $total_amount_not_vat               = array_sum(array_column($commission_tickets, 'amount_not_vat'));
        $total_amount_kpi                   = array_sum(array_column($commission_tickets, 'amount_kpi'));
        $total_actually_collected           = array_sum(array_column($commission_tickets, 'actually_collected'));
        $total_adsplus_amount_commisssion   = array_sum(array_column($commission_tickets, 'adsplus_amount_commisssion'));
        $total_webdoctor_amount_commisssion = array_sum(array_column($commission_tickets, 'webdoctor_amount_commisssion'));
        $total_amount_commisssion           = array_sum(array_column($commission_tickets, 'amount_commisssion'));
        $total_indirect_amount_commisssion  = array_sum(array_column($commission_tickets, 'indirect_amount_commisssion'));
        $total_all_amount_commisssion       = array_sum(array_column($commission_tickets, 'total_amount_commisssion'));

        // Set cell sumary amount and amount not vat on TOP
        $objWorksheet->setCellValueByColumnAndRow(5, 1, $total_contract_num);
        $objWorksheet->setCellValueByColumnAndRow(6, 1, $total_amount_not_vat);
        $objWorksheet->setCellValueByColumnAndRow(7, 1, $total_amount_kpi);
        $objWorksheet->setCellValueByColumnAndRow(8, 1, $total_actually_collected);
        $objWorksheet->setCellValueByColumnAndRow(9, 1, $total_adsplus_amount_commisssion);
        $objWorksheet->setCellValueByColumnAndRow(10, 1, $total_webdoctor_amount_commisssion);
        $objWorksheet->setCellValueByColumnAndRow(11, 1, $total_amount_commisssion);
        $objWorksheet->setCellValueByColumnAndRow(12, 1, $total_indirect_amount_commisssion);
        $objWorksheet->setCellValueByColumnAndRow(13, 1, $total_all_amount_commisssion);

        // Set cell sumary amount and amount not vat on BOTTOM
        $column_index = $row_index + $numbers_of_post + 2;
        $objWorksheet->setCellValueByColumnAndRow(5, $column_index, $total_contract_num);
        $objWorksheet->setCellValueByColumnAndRow(6, $column_index, $total_amount_not_vat);
        $objWorksheet->setCellValueByColumnAndRow(7, $column_index, $total_amount_kpi);
        $objWorksheet->setCellValueByColumnAndRow(8, $column_index, $total_actually_collected);
        $objWorksheet->setCellValueByColumnAndRow(9, $column_index, $total_adsplus_amount_commisssion);
        $objWorksheet->setCellValueByColumnAndRow(10, $column_index, $total_webdoctor_amount_commisssion);
        $objWorksheet->setCellValueByColumnAndRow(11, $column_index, $total_amount_commisssion);
        $objWorksheet->setCellValueByColumnAndRow(12, $column_index, $total_indirect_amount_commisssion);
        $objWorksheet->setCellValueByColumnAndRow(13, $column_index, $total_all_amount_commisssion);

        // AUTO FIT WIDTH COLUMNS
        foreach(range('A','N') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(TRUE);
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $file_name = "tmp/export-sales-commissions.xlsx";
        $objWriter->save($file_name);

        try { $objWriter->save($file_name); }
        catch (Exception $e) { trigger_error($e->getMessage()); return FALSE;  }

        if(file_exists($file_name))
        {   
            $this->load->helper('download');
            force_download($file_name,NULL);
        }

        return FALSE;
    }
}
/* End of file banner.php */
/* Location: ./application/modules/banner/controllers/ajax/dataset.php */