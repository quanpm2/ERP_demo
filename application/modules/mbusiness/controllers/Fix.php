<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fix extends Admin_Controller
{
    public function calc_sale_rank()
    {
        $year = date('Y');
        $month = $this->input->get('month') ?: date('m');
        
        $time = start_of_day("{$year}/{$month}/01");
        $this->load->model('mbusiness/sale_commission_m');
        $this->sale_commission_m->calc_sale_rank($time);
    }

    public function test($user_id = 157)
    {
        $this->load->model('staffs/commission_ticket_m');
        $this->load->model('mbusiness/sale_commission_m');
        $this->load->model('term_users_m');

        $this->load->model('term_users_m');
        $this->load->model('staffs/sale_m');
        $this->load->model('staffs/department_m');
        $this->load->model('staffs/user_group_m');

        $start_time = start_of_month('2018/04/01');
        $end_time   = end_of_month('2018/04/01');

        $args                   = ['user_id'=>$user_id,'start_time'=>$start_time,'end_time'=>$end_time];
        $commission_overall     = $this->commission_ticket_m->get_commission_result(array_merge(['data_type'=>'all'], $args));
        prd($commission_overall['overall']);
        $account_type = $commission_overall['overall']['account_type'];
        $term_commission_types = $commission_overall['term_commission_types']['account_type'];

        var_dump($account_type['actually_collected'] == array_sum(array_column($term_commission_types, 'actually_collected')));
        var_dump($account_type['contract_value'] == array_sum(array_column($term_commission_types, 'contract_value')));
        var_dump($account_type['amount_not_vat'] == array_sum(array_column($term_commission_types, 'amount_not_vat')));
        var_dump($account_type['amount_kpi'] == array_sum(array_column($term_commission_types, 'amount_kpi')));
        var_dump($account_type['amount_kpi'],array_sum(array_column($term_commission_types, 'amount_kpi')));
        prd($account_type, $term_commission_types);
        die('paused');
        prd($account_type, $term_commission_types);
        prd($account_type);

        prd($commission_overall);
    }

    public function index()
    {
        $this->load->model('staffs/commission_ticket_m');
        $this->load->model('mbusiness/sale_commission_m');
        $this->load->model('term_users_m');

        $this->load->model('term_users_m');
        $this->load->model('staffs/sale_m');
        $this->load->model('staffs/department_m');
        $this->load->model('staffs/user_group_m');

        $sales = $this->sale_m->set_role()->get_all();

        foreach ($sales as $sale)
        {
            $user_groups = $this->term_users_m->get_user_terms($sale->user_id, $this->user_group_m->term_type);
            if(empty($user_groups)) continue;

            $departments = $this->term_users_m->get_user_terms($sale->user_id, $this->department_m->term_type);
            if( ! empty($departments)) continue;

            $department_ids = array_unique(array_column($user_groups, 'term_parent'));
            $this->term_users_m->set_user_terms($sale->user_id, $department_ids, $this->department_m->term_type);
            var_dump("{$sale->display_name} update department");
        }
    }

    /**
     * PROCESS : update all sale's monthly commissions ticket (direct & indirect)
     */
    public function update_monthly_commissions($month = NULL, $year = NULL)
    {
        set_time_limit (600);
        $month  = $month ?: date('m');
        $year   = $year ?: date('Y');

        $this->load->model('staffs/commission_ticket_m');
        $this->load->model('mbusiness/sale_commission_m');

        $sales = $this->sale_m->set_user_type()->set_role()->get_many_by('user_status',1);
        $this->config->load('staffs/group');

        $manager_role_ids = array();

        $manager_role_ids[] = $this->option_m->get_value('manager_role_id') ?: $this->config->item('manager_role_id', 'groups');
        $manager_role_ids[] = $this->option_m->get_value('leader_role_id') ?: $this->config->item('leader_role_id', 'groups');
        // Phrase 1 : update all commission tickets direct
        $manage_sales = [];
        foreach ($sales as $sale)
        {
            if(in_array($sale->role_id, $manager_role_ids)) array_push($manage_sales, $sale);

            $commission_direct_updated_on = (int) get_user_meta_value($sale->user_id, 'commission_direct_updated_on');
            // if($commission_direct_updated_on > time()) continue;

            $this->sale_commission_m->update_commission_ticket_direct($sale->user_id, $month, $year);
            update_user_meta($sale->user_id, 'commission_direct_updated_on', strtotime('+1 hour'));

            var_dump("{$sale->display_name} commission direct updated !!!.");
        }

        if(empty($manage_sales)) return FALSE;
        
        foreach ($manage_sales as $sale)
        {
            $commission_indirect_updated_on = (int) get_user_meta_value($sale->user_id, 'commission_indirect_updated_on');
            // if($commission_indirect_updated_on > time()) continue;

            $this->sale_commission_m->update_commission_ticket_indirect($sale->user_id, $month, $year);
            update_user_meta($sale->user_id, 'commission_indirect_updated_on', strtotime('+1 hour'));
            var_dump("{$sale->display_name} commission in-direct updated !!!.");
        }

        $this->sale_commission_m->calc_sale_rank(date("{$year}/{$month}/01"));
        var_dump("All Sale rank was calculated !!!.");
    }
}
/* End of file Crond.php */
/* Location: ./application/modules/facebookads/controllers/Crond.php */