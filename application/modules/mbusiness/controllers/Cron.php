<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'third_party/vendor/autoload.php';

class Cron extends Public_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$models = array('log_m','mbusiness/mbusiness_report_m','mbusiness/call_kpi_m');

		$this->load->model($models);
	}


	/**
	 * Function điều phối tác vụ cron
	 */
	public function index() 
	{
		if( ! parent::check_token())
        {
            log_message('error',"#mbusiness/cron/index has been illegal access to");
            return FALSE;
        }

		if(ENVIRONMENT != 'production') return FALSE;

		$hour = date('G');

		switch ($hour)
		{
			case 8:
				// Gửi mail thống kê cuộc gọi ngày hôm qua
				$this->mbusiness_report_m->send_monthly_callkpi_mail(FALSE);
				break;

			case 12:
				// Cập nhật dữ liệu cuộc gọi từ tổng đài
				$this->recalc_call_kpi($this->mdate->startOfDay(),TRUE);
				// Gửi mail thống kê cuộc gọi ngày hôm nay
				$this->mbusiness_report_m->send_monthly_callkpi_mail(TRUE);
				break;

			case 20:
				// Cập nhật dữ liệu cuộc gọi từ tổng đài
				$this->recalc_call_kpi($this->mdate->startOfDay(),FALSE);
				break;
			
			default:
				# code...
				break;
		}
	}


	/**
	 * Cập nhật dữ liệu cuộc gọi từ tổng đài
	 *
	 * @param      integer  $kpi_datetime  The kpi datetime
	 * @param      <type>   $rewrite       The rewrite
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	private function recalc_call_kpi($kpi_datetime = 0,$rewrite = TRUE)
	{
		$kpi_datetime = $kpi_datetime ?: $this->mdate->startOfDay();
		$this->load->model('phonebook/report_phonebook_m');
		$users = $this->report_phonebook_m->get_users();
		$exts = $users['usersExtIDArray'] ?? array();

		$roleLeader = 13;
		$roleMember = 9;
		$KPILeader = $this->option_m->get_value('leader_kpi_per_day') ?: $this->config->item('leader_kpi_per_day');
		$KPIMember = $this->option_m->get_value('member_kpi_per_day') ?: $this->config->item('member_kpi_per_day');

		if(empty($exts)) return FALSE;

		foreach ($exts as $ext => $user_id)
		{
			if(empty($ext)) continue;

			$role_id = $this->admin_m->get_field_by_id($user_id,'role_id');
			if( ! in_array($role_id, [$roleLeader,$roleMember])) continue;

			$user_status = $this->admin_m->get_field_by_id($user_id,'user_status');
			if($user_status != '1') continue;

			$kpi_value = $KPIMember;
			if($role_id == $roleLeader) $kpi_value = $KPILeader;

			if($rewrite) $this->call_kpi_m->update_kpi_value($ext, 'phonecall', $kpi_value, $kpi_datetime, $user_id);
			else $this->call_kpi_m->update_kpi_result($ext, 'phonecall', 0, $kpi_datetime, $user_id);
		}
	}
}
/* End of file Crond.php */
/* Location: ./application/modules/facebookads/controllers/Crond.php */