<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ordermeeting extends Admin_Controller {

	public $model = 'ordermeeting_m';

	function __construct()
	{
		parent::__construct();
		$this->load->model('mbusiness/ordermeeting_m');
		$this->load->config('mbusiness/ordermeeting');
	}

	/**
	 * Ordermeeting List All
	 */
	public function index()
	{
		restrict('mbusiness.ordermeeting.access');

		$this->template->is_box_open->set(1);
		$this->template->title->set('Danh sách lịch hẹn');
		$this->template->description->set('Trang quản lý lịch hẹn gặp khách hàng');

		parent::render($this->data, 'ordermeeting/index');
	}


	/**
	 * Handler form actions
	 * 
	 * 1. Export Excel2007 - GET
	 */
	protected function submit()
	{
		if($this->input->get('export_list_order_meeting') !== NULL)
		{
			$this->messages->clear() ;
			$last_query = $this->scache->get("modules/ordermeeting/list_order_meeting_{$this->admin_m->id}");

			if( ! $last_query )
			{
				$this->messages->info('Không tìm thấy lệnh đã truy vấn trước đó , vui lòng thử lại sau');
				redirect(current_url(),'refresh');	
			}

			$ordermeetings = $this->term_m->query($last_query)->result();
			if(empty($ordermeetings))
			{
				$this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
				redirect(current_url(),'refresh');
			}
		
			$this->load->helper('file');
			$this->load->library('excel');
			$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	        $cacheSettings = array( 'memoryCacheSize' => '512MB');
	        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	        $objPHPExcel = new PHPExcel();
	        $objPHPExcel->getProperties()->setCreator("WEBDOCTOR.VN")->setLastModifiedBy("WEBDOCTOR.VN")->setTitle(uniqid('Danh sách lịch hẹn khách hàng __'));

	        $objPHPExcel->setActiveSheetIndex(0);
	        $objWorksheet = $objPHPExcel->getActiveSheet();
	        
	        $headings = array('STT','NGÀY TẠO','NGÀY HẸN','NỘI DUNG','KHÁCH HÀNG','NGƯỜI LIÊN HỆ','SỐ ĐIỆN THOẠI','KINH DOANH','KINH DOANH HỖ TRỢ','TRẠNG THÁI');
	        $objWorksheet->fromArray($headings, NULL, 'A1');

	       	$row_index = 2;
	        foreach ($ordermeetings as $key => $ordermeeting)
			{	
				$row_number = $row_index + $key;

	    		$objWorksheet->setCellValueByColumnAndRow(0, $row_number, ($key+1));

	    		// NGÀY TẠO
	    		$created_on		= my_date($ordermeeting->created_on, 'd/m/Y H:i A') ;
	    		$objWorksheet->setCellValueByColumnAndRow(1, $row_number, $created_on);
	    		
	    		// NGÀY HẸN
	    		$start_time     = my_date($ordermeeting->start_time, 'd/m/Y H:i A') ;	
	    		$objWorksheet->setCellValueByColumnAndRow(2, $row_number, $start_time);

	    		// NỘI DUNG
	    		$content     = $ordermeeting->content ?: '';
	    		$objWorksheet->setCellValueByColumnAndRow(3, $row_number, $content);

	    		// KHÁCH HÀNG
	    		$display_name = $ordermeeting->display_name ?: '';
	    		$objWorksheet->setCellValueByColumnAndRow(4, $row_number, $display_name);

	    		// NGƯỜI LIÊN HỆ
	    		$cus_name = $ordermeeting->cus_name ?: '';
	    		$objWorksheet->setCellValueByColumnAndRow(5, $row_number, $cus_name);

	    		// SỐ ĐIỆN THOẠI
	    		$cus_phone = $ordermeeting->cus_phone ?: '';
	    		$objWorksheet->setCellValueExplicit("G{$row_number}", $cus_phone,PHPExcel_Cell_DataType::TYPE_STRING);

	    		// KINH DOANH
	    		$user_id = $ordermeeting->user_id;
	    		$objWorksheet->setCellValueByColumnAndRow(7, $row_number, $this->admin_m->get_field_by_id($user_id, 'display_name') ?: '');
	    		
				// KINH DOANH HỖ TRỢ 
				$support_id = $ordermeeting->support_id;  
	    		$objWorksheet->setCellValueByColumnAndRow(8, $row_number, $this->admin_m->get_field_by_id($support_id, 'display_name') ?: '');
	    		
	    		// TRẠNG THÁI
	    		$status = $ordermeeting->status;
	    		$objWorksheet->setCellValueByColumnAndRow(9, $row_number, $this->config->item($status,'processStates') ?: '');
			}

			$num_rows = count($ordermeetings);
			
			// Set Cells style for Date
		    $objPHPExcel->getActiveSheet()
	        	->getStyle('B'.$row_index.':B'.($num_rows+$row_index))
	        	->getNumberFormat()
		    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

		    $objPHPExcel->getActiveSheet()
	     	   	->getStyle('C'.$row_index.':C'.($num_rows+$row_index))
	     	   	->getNumberFormat()
		    	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);


	        // We'll be outputting an excel file
	        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

	        $folder_upload = "files/ordermeeting/";
	        if(!is_dir($folder_upload))
	        {
	            try 
	            {
	                $oldmask = umask(0);
	                mkdir($folder_upload, 0777, TRUE);
	                umask($oldmask);
	            }
	            catch (Exception $e)
	            {
	                trigger_error($e->getMessage());

	                $this->messages->info('Quá trình xử lý không thành công , vui lòng thử lại !');
					redirect(current_url(),'refresh');
	            }
	        }

	        $file_name = $folder_upload . 'export-ordermeeting-list-' . my_date(time(), 'd-m-Y') . '.xlsx';

	       	try 
	        {
	        	$this->load->helper('download');
	        	$this->load->helper('file');

				delete_files($folder_upload);
				
	            $objWriter->save($file_name); 	

	            force_download($file_name,NULL);
	        }
	        catch (Exception $e) 
	        {
	            trigger_error($e->getMessage());
	            $this->messages->info('Quá trình xử lý không thành công , vui lòng thử lại !');
				redirect(current_url(),'refresh');
	        }
		}
	}


	/**
	 * Insert/Update ordermeeting
	 *
	 * @param      integer  $id     The identifier
	 */
	public function edit($id = 0)
	{
		restrict('mbusiness.ordermeeting.update');
		$id = absint($id);
		$data = $this->data;

		// CHECK REVISION DRAFT
		if(empty($id))
		{
			restrict('mbusiness.ordermeeting.add');
			$draftOrder = $this->ordermeeting_m->get_by(['status'=>'draft','user_id'=>$this->admin_m->id]);
			if($draftOrder) redirect(module_url("ordermeeting/edit/{$draftOrder->id}"),'refresh');

			$insertId = $this->ordermeeting_m->insert(['created_on'=>time(),'user_id'=>$this->admin_m->id,'status'=>'draft']);
			redirect(module_url("ordermeeting/edit/{$insertId}"),'refresh');
		}


		// RESTRICT SCOPRE PERMISSION OF LOGGED USER
		if( ! $this->ordermeeting_m->has_permission($id, 'mbusiness.ordermeeting.update'))
		{
			$this->messages->error('Không có quyền truy cập và chỉnh sửa lịch hẹn này ! Quyền truy cập bị hạn chế.');
			redirect(module_url('ordermeeting'), 'refresh');
		}

		// PROCESS SUBMIT ACTION
		if($this->input->post('submit') !== NULL)
		{
			$this->sendMailOrder($id);
		}

		$data['edit'] = NULL;
		$data['roles'] = array();

		$this->template->title->set('Tạo biểu mẫu đặt lịch hẹn gặp khách hàng');

		if(has_permission('mbusiness.ordermeeting.Manage') === FALSE)
		{
			$this->ordermeeting_m->where('user_id',$this->admin_m->id);
		}

		$orderMeeting = $this->ordermeeting_m->get_by([	'id'=>$id]);
		
		$this->config->load('staffs/group');
		$roleSaleIds = $this->config->item('salesexecutive','groups');
		$saleUsers = $this->admin_m->set_role($roleSaleIds)->set_get_admin()->set_get_active()->get_many_by();
		
		$data['saleUsersKeyValue'] = array_column($saleUsers, 'display_name','user_id');
		$data['edit'] = $orderMeeting;

		parent::render($data,'ordermeeting/edit');
	}


	/**
	 * Sends a mail order.
	 *
	 * @param      integer  $id     The identifier
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function sendMailOrder($id = 0)
	{
		$orderMeeting = $this->ordermeeting_m->get($id);
		if(!$orderMeeting) return FALSE;

		if($orderMeeting->status == 'draft')
		{
			# send mail
			$edit = $this->input->post('edit');
			$result = $this->ordermeeting_m->update($id,$edit);
			$orderMeeting = $this->ordermeeting_m->get($id);

			$this->config->load('staffs/group');
			$roleSaleIds = $this->config->item('salesexecutive','groups');
			$saleUser = $this->admin_m->set_role($roleSaleIds)->set_get_admin()->set_get_active()->get_by();

			$this->load->library('email');

			$mail_to = array('haitm@adsplus.vn');
			$mail_cc = array();
			$mail_bcc = array('');

	        $saleMail = $this->admin_m->get_field_by_id($orderMeeting->user_id,'user_email');

	        $saleName = $this->admin_m->get_field_by_id($orderMeeting->user_id, 'display_name') ?: $saleMail;
	        $saleName = "#{$orderMeeting->user_id} {$saleName}";

	        $saleSupportName = '---';
			if($orderMeeting->support_id)
			{
				$saleSupportMail = $this->admin_m->get_field_by_id($orderMeeting->support_id, 'user_email');
				$saleSupportName = $this->admin_m->get_field_by_id($orderMeeting->support_id,'display_name') ?: $saleSupportMail;
				$saleSupportName = "#{$orderMeeting->support_id} {$saleSupportName}";
			}

			$this->email->from('support@adsplus.vn', 'ERP')->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);

			$title = '[NVKD] Order lịch gặp khách hàng';
			$this->email->subject($title);

			$happened_on = my_date($orderMeeting->start_time,'d-m-Y H:i:s').'-'.my_date($orderMeeting->end_time,'d-m-Y H:i:s');
			$this->table->set_heading(['KHÁCH HÀNG','THỜI GIAN','ĐỊA ĐIỂM','NGƯỜI LIÊN HỆ','ĐIỆN THOẠI','NỘI DUNG','NHÂN VIÊN','HỖ TRỢ']);
			$this->table->add_row($orderMeeting->display_name,$happened_on,$orderMeeting->location,$orderMeeting->cus_name,$orderMeeting->cus_phone,$orderMeeting->content,$saleName,$saleSupportName);
			$mailContent = $this->table->generate();
			$mailContent.= anchor(module_url('index'),"Link truy cập nhanh để xác nhận order lịch hẹn khách hàng");
			$this->email->message($mailContent);

			$isSent = $this->email->send(TRUE);
			$result = $this->ordermeeting_m->update($id,['status'=>'verified','updated_on'=>time(),'user_confirm_id'=>1]);
			$this->messages->success('Yêu cầu đã được gửi, Sau khi người quản lý xác nhận sẽ được tính vào KPI.');
			return;
		}

		if($orderMeeting->status == 'sent')
		{
			# send mail
			$this->messages->info('Yêu cầu đang được chờ duyệt.');
			return;
		}

		if($orderMeeting->status == 'verified')
		{
			# send mail
			$this->messages->success('Yêu cầu đã được duyệt và đã ghi nhận KPI cuộc gặp vào hệ thống!');
			return;
		}
	}
}
/* End of file Ordermeeting.php */
/* Location: ./application/modules/mbusiness/controllers/Ordermeeting.php */