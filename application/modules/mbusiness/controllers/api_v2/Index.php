<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'vendor/autoload.php';

class Index extends MREST_Controller
{
    const PAGE_SIZE = 200;

    /**
     * API dữ liệu lịch hẹn 
     */
    public function ordermeeting_get()
    {
        if( ! has_permission('mbusiness.ordermeeting.access')) parent::response(['status' => TRUE, 'data' => []]);

        $relate_users = $this->admin_m->get_all_by_permissions('mbusiness.ordermeeting.access');
        if($relate_users === FALSE) parent::response(['status' => TRUE, 'data' => []]); // LOGGED USER NOT HAS PERMISSION TO ACCESS

        $this->load->model('mbusiness/ordermeeting_m');
        is_array($relate_users) AND $this->ordermeeting_m->where_in('ordermeeting.user_id', $relate_users);
        
        $startTime  = parent::get('startTime', TRUE) ? start_of_day(parent::get('startTime', TRUE)) : start_of_month();
        $endTime    = parent::get('endTime', TRUE) ? end_of_day(parent::get('endTime', TRUE)) : end_of_month();

        $orders = $this->ordermeeting_m
        ->where('start_time >=', start_of_day($startTime))
        ->where('start_time <=', end_of_day($endTime))
        ->as_array()
        ->get_all();

        if( ! $orders) parent::response(['status'=>true,'data'=>[]]);

        $this->load->config('mbusiness/ordermeeting');
        $this->load->model(['staffs/department_m', 'staffs/user_group_m']);

        $users = $this->admin_m
        ->select('user_id, display_name, user_email')
        ->as_array()
        ->get_many(array_filter(array_unique(array_merge(array_column($orders, 'user_id'), array_column($orders, 'support_id')))));


        $users = $users ? array_column(array_map(function($x){
            $user_groups = $this->term_users_m->get_user_terms($x['user_id'], $this->user_group_m->term_type);
            if( ! empty($user_groups)) $x['group'] = implode(', ', array_column($user_groups, 'term_name'));
            $departments = $this->term_users_m->get_user_terms($x['user_id'], $this->department_m->term_type);
            if( ! empty($departments)) $x['department'] = implode(', ', array_column($departments, 'term_name'));
            return $x;
        }, $users), NULL, 'user_id') : [];

        foreach ($orders as &$order)
        {
            $order['status'] = $this->config->item($order['status'], 'processStates');

            $_user_id       = $order['user_id'];
            $_support_id    = $order['support_id'];
            if(empty($_user_id) && empty($_support_id)) continue;

            empty($_user_id)    OR $order['sale'] = array('id' => $_user_id, 'display_name' => $users[$_user_id]['display_name'], 'email' => $users[$_user_id]['user_email'], 'group' => $users[$_user_id]['group'] ?? '', 'department' => $users[$_user_id]['department'] ?? '' );

            empty($_support_id) OR $order['sale_support'] = array('id' => $_support_id, 'display_name' => $users[$_support_id]['display_name'], 'email' => $users[$_support_id]['user_email'], 'group' => $users[$_support_id]['group'] ?? '', 'department' => $users[$_support_id]['department'] ?? '' );
        }

        parent::response(['status'=>TRUE, 'data' => $orders]);
    }

    /**
     * API dữ liệu lịch hẹn 
     */
    public function phonecall_get()
    {
        if( ! has_permission('mbusiness.activity.access')) parent::response(['status' => TRUE, 'data' => [], 'pagination' => array()]);

        $relate_users = $this->admin_m->get_all_by_permissions('mbusiness.activity.access');
        if($relate_users === FALSE) parent::response(['status' => TRUE, 'data' => []]); // LOGGED USER NOT HAS PERMISSION TO ACCESS
        
        $startTime  = parent::get('startTime', TRUE) ? start_of_day(parent::get('startTime', TRUE)) : start_of_month(date('Y-1-1'));
        $endTime    = parent::get('endTime', TRUE) ? end_of_day(parent::get('endTime', TRUE)) : end_of_month(time());
        $department_id = (int) parent::get('department', TRUE);

        $this->load->model(['staffs/department_m', 'staffs/user_group_m']);
        $this->load->model('mbusiness/call_kpi_m');

        $department_user_ids = [];
        if( ! empty($department_id))
        {
            $_users = $this->term_users_m->get_term_users($department_id, ['admin']);
            $department_user_ids = array_column($_users, 'user_id');
        }

        /* Phone call KPI tracking */
        $this->call_kpi_m->select('user_id, kpi_datetime')->select_sum('result_value')->select_sum('kpi_value')->order_by('kpi_datetime','asc')->group_by('user_id, kpi_datetime');
        if( ! empty($department_user_ids)) $this->call_kpi_m->where_in('user_id', $department_user_ids);

        $total = $this->call_kpi_m->count_by([
            'kpi_type'=>'phonecall',
            'result_value >' => 0,
            'kpi_datetime >=' => my_date($startTime, 'Y-m-d H:i:s'),
            'kpi_datetime <=' => my_date($endTime, 'Y-m-d H:i:s')
        ]);

        $page = parent::get('page', TRUE) ?: 1;
        $cur_page = ($page - 1 )  * self::PAGE_SIZE;
        $pagination = array(
            'min' => $cur_page + 1,
            'max' => ($cur_page + self::PAGE_SIZE) > $total ? $total : ($cur_page + self::PAGE_SIZE),
            'total_rows' => $total
        );

        $this->call_kpi_m->select('user_id, kpi_datetime')->select_sum('result_value')->select_sum('kpi_value')->order_by('kpi_datetime','asc')->group_by('user_id, kpi_datetime');
        if( ! empty($department_user_ids)) $this->call_kpi_m->where_in('user_id', $department_user_ids);

        /* Phone call KPI tracking */
        $callkpis = $this->call_kpi_m
        ->as_array()
        ->limit( self::PAGE_SIZE, ($page - 1)*self::PAGE_SIZE)
        ->get_many_by([
            'kpi_type'=>'phonecall',
            'result_value >' => 0,
            'kpi_datetime >=' => my_date($startTime, 'Y-m-d H:i:s'),
            'kpi_datetime <=' => my_date($endTime, 'Y-m-d H:i:s')
        ]);

        if($callkpis) 
        {

            $users = $this->admin_m->select('user_id, display_name, user_email')->as_array()->get_many(array_filter(array_unique(array_column($callkpis, 'user_id'))));
            $users = $users ? array_column(array_map(function($x){
                $user_groups = $this->term_users_m->get_user_terms($x['user_id'], $this->user_group_m->term_type);
                if( ! empty($user_groups)) $x['group'] = implode(', ', array_column($user_groups, 'term_name'));
                $departments = $this->term_users_m->get_user_terms($x['user_id'], $this->department_m->term_type);
                if( ! empty($departments)) $x['department'] = implode(', ', array_column($departments, 'term_name'));
                return $x;
            }, $users), NULL, 'user_id') : [];

            $callkpis = array_map(function($callkpi) use ($users) {
                $callkpi['kpi_datetime'] = start_of_day($callkpi['kpi_datetime']);
                $callkpi['display_name'] = $users[$callkpi['user_id']]['display_name'];
                $callkpi['email'] = $users[$callkpi['user_id']]['user_email'];
                $callkpi['group'] = $users[$callkpi['user_id']]['group'] ?? '';
                $callkpi['department'] = $users[$callkpi['user_id']]['department'] ?? '' ;

                return $callkpi;

            }, $callkpis);
        }

    	parent::response(['status'=>TRUE, 'pagination' => $pagination, 'data' => $callkpis]);
    }
}
/* End of file Index.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Index.php */