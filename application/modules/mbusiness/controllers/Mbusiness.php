<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbusiness extends Admin_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('mbusiness/ordermeeting_m');	
		$this->load->model('mbusiness/call_kpi_m');

		$this->load->config('staffs/staffs');
		$this->load->config('mbusiness/ordermeeting');
		$this->load->config('mbusiness/mbusiness');
		
		defined('CALL_PER_ORDERMETTING') OR define('CALL_PER_ORDERMETTING',10);
	}

	/**
	 * Commission dashboard
	 *
	 * @param      integer  $user_id  The user identifier
	 */
	public function commission($user_id = 0)
	{
		restrict('mbusiness.commission.access');
		$data = $this->data;

		$user_id = (int) $user_id ?: $this->admin_m->id;
		$data['user_id'] = $user_id;
		$data['dataobject'] = ['user_id'=>$user_id];
		$data['month'] = $this->input->get('month') ?: date('m');

		$this->template->is_box_open->set(1);
		$sale = $this->admin_m->get_field_by_id($user_id);

		$this->template->title->append("Trang thống kê cá nhân - {$sale['display_name']}");
		parent::render($data);
	}

	/**
	 * List commission by sales and ranking
	 */
	public function commissions()
	{
		restrict('mbusiness.commission.access');

		if( ! has_permission('mbusiness.commission.mdeparment')
			AND ! has_permission('mbusiness.commission.mgroup')
			AND ! has_permission('mbusiness.commission.manage')) {

			redirect(admin_url("mbusiness/commission/{$this->admin_m->id}"), 'refresh');
		}

		$data = $this->data;		
		$this->template->is_box_open->set(1);
		$this->template->title->append('Tổng quan tính thưởng bộ phận Kinh Doanh');
		parent::render($data);
	}

	/**
	 * Dashboard
	 */
	public function index()
	{
		restrict('mbusiness.activity');
		$data = $this->data;

		/* Ordermeeting data chart */
		$all_ordermeetings = $this->ordermeeting_m
		->select('id,created_on,start_time,status')
		->order_by('start_time','asc')
		->where_in('status',['sent','verified','deleted'])
		->get_many_by();
		$all_ordermeetings = array_map(function($x){ $x->start_time = $this->mdate->startOfDay($x->start_time); return $x;}, $all_ordermeetings);

		$ordermeetings_group_by_status 	= array_group_by($all_ordermeetings,'status');
		$verified_ordermeetings 		= $ordermeetings_group_by_status['verified'] ?? array();
		$verified_ordermeetings 		= array_group_by($verified_ordermeetings,'start_time');

		$ordermeeting_datachart = array();
		foreach ($verified_ordermeetings as $unixtime => $rows)
		{
			if($unixtime < 10000) continue;
			$ordermeeting_datachart[] = array($unixtime,count($rows));
		}

		$data['ordermeeting_datachart'] = $ordermeeting_datachart;

		/* Phone call KPI tracking */
		$callkpi = $this->call_kpi_m
		->select('kpi_datetime')
		->select_sum('result_value')
		->select_sum('kpi_value')
		->order_by('kpi_datetime','asc')
		->group_by('kpi_datetime')
		->where('kpi_datetime >=',my_date($this->mdate->startOfDay('-30 days'),'Y-m-d H:i:s'))
		->get_many_by(['kpi_type'=>'phonecall']);

		$salekpi = array();
		foreach ($callkpi as $row)
		{
			$datetime = $this->mdate->startOfDay($row->kpi_datetime);
			if(isset($salekpi[$datetime])) $salekpi[$datetime] = array();

			$order_result = 0;
			if(!empty($verified_ordermeetings[$datetime]))
			{
				$order_result = count($verified_ordermeetings[$datetime]);
			}

			$salekpi[$datetime] = [my_date($datetime,'d/m'),(int)$row->kpi_value,(int)($row->result_value+($order_result*CALL_PER_ORDERMETTING)),(int)$row->result_value];
		}

		foreach ($verified_ordermeetings as $unixtime => $values)
		{
			if(isset($salekpi[$unixtime]) || $this->mdate->startOfDay('-30 days') > $unixtime) continue;

			$order_result = count($verified_ordermeetings[$unixtime]);

			$salekpi[$unixtime] = [my_date($unixtime,'d/m'),(int)0,(int)($order_result*CALL_PER_ORDERMETTING),0];
		}

		ksort($salekpi);
		$data['salekpi'] = array_values($salekpi);

		$data['ordermeeting_count_chart'] = array(['Trạng thái lịch hẹn','Tổng']);
		foreach ($ordermeetings_group_by_status as $status => $rows)
		{
			$data['ordermeeting_count_chart'][] = [$this->config->item($status,'processStates'),count($rows)];
		}

		$this->template->title->append('Trang tổng quan thống kê hoạt động của bộ phận kinh doanh ');
		$this->template->is_box_open->set(1);
		
		parent::render($data);
	}

	public function setting($view = 'kpi')
	{
		restrict('mbusiness.setting.access');

		// PAGE SUBMIT SECTION
		if($post = $this->input->post())
		{
			// KPI section submit
			if($this->input->post('kpi_submit') !== NULL)
			{
				restrict('mbusiness.setting_kpi.update');

				if(!empty($post['edit']['leader_kpi_per_day']))
				{
					$this->option_m->set_value('leader_kpi_per_day',$post['edit']['leader_kpi_per_day']);
				}

				if(!empty($post['edit']['member_kpi_per_day']))
				{
					$this->option_m->set_value('member_kpi_per_day',$post['edit']['member_kpi_per_day']);
				}
				
				$this->messages->success('Cập nhật KPI hoàn tất');

				redirect(module_url('setting'),'refresh');	
			}
		}
		
		$data = $this->data;

		// KPI SETTING SECTION
		$data['has_setting_kpi'] = has_permission('mbusiness.setting_kpi.access');
		if($data['has_setting_kpi'])
		{
			$leader_role_id 	= $this->config->item('leader_role_id');
			$member_role_id 	= $this->config->item('member_role_id');
			$members 			= $this->admin_m->count_by(['user_type'=>'admin','role_id'=>$member_role_id,'user_status'=>1]);
			$leaders 			= $this->admin_m->count_by(['user_type'=>'admin','role_id'=>$leader_role_id,'user_status'=>1]);
			$workdays_per_month = $this->config->item('workdays_per_month'); // Số ngày làm việc trong tháng
			
			$leader_kpi_per_day_default = $this->config->item('leader_kpi_per_day'); // KPI trưởng nhóm hiện tại
			$member_kpi_per_day_default = $this->config->item('member_kpi_per_day'); // KPI nhân viên hiện tại
			$leader_kpi_per_day = $this->option_m->get_value('leader_kpi_per_day') ?: $leader_kpi_per_day_default;
			$member_kpi_per_day = $this->option_m->get_value('member_kpi_per_day') ?: $member_kpi_per_day_default;

			$data['leader_role_id'] 	= $leader_role_id;
			$data['member_role_id'] 	= $member_role_id;
			$data['members'] 			= $members;
			$data['leaders'] 			= $leaders;
			$data['workdays_per_month']	= $workdays_per_month;

			$data['leader_kpi_per_day_default'] = $leader_kpi_per_day_default;
			$data['member_kpi_per_day_default'] = $member_kpi_per_day_default;
			$data['leader_kpi_per_day'] 		= $leader_kpi_per_day;
			$data['member_kpi_per_day'] 		= $member_kpi_per_day;

			$data['month_kpi_result'] = $workdays_per_month * ($leaders*$leader_kpi_per_day + $members*$member_kpi_per_day);	
		}

		// KPI SETTING SECTION
		$data['has_setting_creditquota'] = has_permission('mbusiness.setting.access');
		if($data['has_setting_creditquota'])
		{
			$data['credit_quota'] = $this->config->item('credit_quota');
		}

		$data['view'] = $view;
		$data['object_data'] = json_encode($data);

		$this->template->is_box_open->set(1);
		$this->template->title->append('Cấu hình thông số bộ phận kinh doanh');
		parent::render($data);
	}


	public function activity()
	{
		restrict('mbusiness.activity.access');

		$this->template->is_box_open->set(1);
		$this->template->title->set('Quản lý KPI cuộc gọi');
		$this->template->description->set('Trang danh sách theo dõi cuộc gọi và cuộc hẹn');

		parent::render($this->data);
	}
}
/* End of file Mbusiness.php */
/* Location: ./application/modules/mbusiness/controllers/Mbusiness.php */