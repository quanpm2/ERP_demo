<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordermeeting_m extends Base_model {

	public $primary_key = 'id';
	public $_table = 'ordermeeting';


	/**
	 * Determines if it has permission.
	 *
	 * @param      integer  $id          The identifier
	 * @param      string   $permission  The permission
	 * @param      integer  $user_id     The user identifier
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     boolean  True if has permission, False otherwise.
	 */
	public function has_permission($id = 0, $permission = '', $user_id = 0)
	{
		if(empty($permission)) $permission = $this->router->fetch_class().'.'.$this->router->fetch_method().'.access';
		if(empty($user_id)) $user_id = $this->admin_m->id;

		$relate_users = $this->admin_m->get_all_by_permissions($permission, $user_id);
		if(is_bool($relate_users)) return $relate_users;

		$this->load->model('term_users_m');

		$ordermeeting = $this
		->group_start()
			->where_in('ordermeeting.user_id', $relate_users)
			->or_where_in('ordermeeting.support_id', $relate_users)
		->group_end()
		->get($id);

		if( ! $ordermeeting) return FALSE;

		return TRUE;
	}
}
/* End of file Ordermeeting.php */
/* Location: ./application/modules/order_contact/models/Ordermeeting.php */