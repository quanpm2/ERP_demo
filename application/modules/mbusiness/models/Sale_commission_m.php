<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sale_commission_m extends Base_model {

    function __construct()
    {
        parent::__construct();

        $model_names = array(
            'commission_m',
            'staffs/sale_m',
            'staffs/commission_ticket_m',
            'staffs/commission_ticket_direct_m',
            'staffs/commission_ticket_indirect_m' );

        $this->load->model($model_names);

        $this->config->load('mbusiness/commission');
    }

    /**
     * Gets the dataset.
     *
     * @param      array  $attrs  The attributes
     *
     * @return     array  The dataset.
     */
    public function get_dataset($attrs = array())
    {
        $default = array(   
            'data_type' => 'all',
            'term_type' => NULL,
            'term_id'   => 0,
            'user_id'   => 0,
            'start_time' => $this->mdate->startOfMonth(),
            'end_time'   => $this->mdate->endOfDay()
        );
        $args = wp_parse_args($attrs, $default);

        $encrypt_key    = md5(json_encode($args));
        $cache_key      = "modules/mbusiness/{$args['user_id']}sale_commission_{$encrypt_key}";
        $result         = $this->scache->get($cache_key);

        if( ! $result)
        {
            $result     = array();
            $receipts   = $this->commission_m->get_receipts_paid(0, $args['user_id'],$args['start_time'],$args['end_time']);
            if(empty($receipts)) return FALSE;

            $term_ids   = array_group_by($receipts,'term_id');

            $terms      = $this->contract_m->set_term_type()->as_array()->get_many_by('term_id', array_keys($term_ids));
            $terms      = array_column($terms, NULL, 'term_id');

            foreach ($terms as &$term)
            {
                $term['contract_value'] = get_term_meta_value($term['term_id'], 'contract_value');
                $term['contract_code']  = get_term_meta_value($term['term_id'], 'contract_code');

                $start_time = get_term_meta_value($term['term_id'], 'start_service_time') ?: get_term_meta_value($term['term_id'], 'contract_begin');

                $term['start_time'] = $start_time;
                $term['start_date'] = my_date($start_time, 'd/m/Y H:i:s');

                $this->commission_m->set_instance($term['term_id']);
                $commission_m               = $this->commission_m->get_instance();
                $term['actually_collected'] = $commission_m->get_actually_collected($term['term_id'], $args['user_id'], $args['start_time'],$args['end_time']);

                $term['amount_kpi'] = $commission_m->get_amount_kpi($term['term_id'], $args['user_id'], $args['start_time'],$args['end_time']);

                $term['vat']            = get_term_meta_value($term['term_id'], 'vat');
                $term['amount']         = $commission_m->get_amount($term['term_id'], $args['user_id'], $args['start_time'],$args['end_time']);
                $term['amount_not_vat'] = $commission_m->get_amount_not_vat($term['term_id'], $args['user_id'], $args['start_time'],$args['end_time']);
                $term['receipts'] = array_column($term_ids[$term['term_id']], 'post_id');

                $commission_type = 'webdoctor';
                switch ($term['term_type'])
                {
                    case 'domain':
                        $commission_type = 'none';
                        break;

                    case 'google-ads':
                        $commission_type = $commission_m->get_commission_package();
                        break;

                    case 'facebook-ads':
                        $commission_type = 'account_type';
                        break;
                    
                    default:
                        $commission_type = 'webdoctor';
                        break;
                }

                $term['commission_type'] = $commission_type;
            }

            $result['receipts']     = $receipts;
            $result['terms']        = $terms;
            $result['term_types']   = array_group_by($terms, 'term_type');
            $result['term_commission_types'] = array_group_by($terms, 'commission_type');

            /* Calculate overall */
            $result['overall'] = array();

            foreach ( $result['term_commission_types'] as $commission_type => $items)
            {
                $actually_collected = array_sum(array_column($items, 'actually_collected'));
                $rate   = $this->get_commission_rate($actually_collected, $commission_type);
                $result['overall'][$commission_type] = array(
                    'rate' => $rate,

                    'actually_collected'    => $actually_collected,
                    'amount_commisssion'    => $actually_collected * $rate,
                    
                    'contract_value'    => array_sum(array_column($items, 'contract_value')),
                    'amount_not_vat'    => array_sum(array_column($items, 'amount_not_vat')),
                    'amount_kpi'        => array_sum(array_column($items, 'amount_kpi')),
                    'contract_num'      => count($items)
                );
                
                array_sum(array_column($result['overall'], 'amount_commisssion'));
            }

            $result['overall']['contract_value']        = array_sum(array_column($result['overall'], 'contract_value'));
            $result['overall']['amount_not_vat']        = array_sum(array_column($result['overall'], 'amount_not_vat'));
            $result['overall']['amount_commisssion']    = array_sum(array_column($result['overall'], 'amount_commisssion'));
            $result['overall']['actually_collected']    = array_sum(array_column($result['overall'], 'actually_collected'));
            $result['overall']['amount_kpi']            = array_sum(array_column($result['overall'], 'amount_kpi'));

            $result['overall']['contract_num']  = count($result['terms']);

            $result['overall']['adsplus']       = array(                
                'actually_collected' => ($result['overall']['account_type']['actually_collected']??0) + ($result['overall']['cpc_type']['get_actually_collected']??0),

                'amount_kpi' => ($result['overall']['account_type']['amount_kpi']??0) + ($result['overall']['cpc_type']['get_amount_kpi']??0),

                'amount_commisssion' => ($result['overall']['account_type']['amount_commisssion']??0) + ($result['overall']['cpc_type']['amount_commisssion']??0),

                'amount_not_vat' => ($result['overall']['account_type']['amount_not_vat']??0) + ($result['overall']['cpc_type']['amount_not_vat']??0),

                'contract_num' => ($result['overall']['account_type']['contract_num']??0) + ($result['overall']['cpc_type']['contract_num']??0),

                'contract_value' => ($result['overall']['account_type']['contract_value']??0) + ($result['overall']['cpc_type']['contract_value']??0),
            );

            foreach ($result['terms'] as &$term)
            {
                $rate = $result['overall'][$term['commission_type']]['rate'] ?? 0;
                $term['rate'] = $rate;
                $term['amount_commisssion'] = $rate * $term['actually_collected'];
            }

            $this->scache->write($result, $cache_key, 500);
        }

        if($args['data_type'] == 'all') return $result;

        if(empty($result[$args['data_type']])) return NULL;

        return $result[$args['data_type']];
    }

    public function get_rank($user_id = 0)
    {
        $sales = $this->sale_m->set_user_type()
        ->set_role($this->config->item('salesexecutive', 'groups'))->where_in('user_status', 1)->as_array()->get_all();

        $args = array(
            'user_id'   => 0,
            'start_time' => $this->mdate->startOfMonth(),
            'end_time'   => $this->mdate->endOfDay(),
            'data_type' => 'overall'
        );

        foreach ($sales as &$sale)
        {
            $args['user_id'] = $sale['user_id'];
            $data = $this->get_dataset($args);
            if( empty($data)) $sale['amount_not_vat'] = 0;

            $sale['amount_not_vat'] = $data['amount_not_vat'];
        }

        usort($sales, function($a, $b){
            return ($a['amount_not_vat'] < $b['amount_not_vat'] ? 1 : -1); 
        });

        $sales = array_column($sales, 'user_id');

        $index = array_search($user_id ,$sales);

        $result = array(
            'index' => $index + 1,
            'total' => count($sales)
        );

        return $result;
    }

    /**
     * Gets the commission rate.
     *
     * @param      integer  $amount  The amount
     * @param      string   $type    The type
     *
     * @return     integer  The commission rate.
     */
    public function get_commission_rate($amount = 0, $type = 'none')
    {
        $rules = $this->config->item($type, 'commission');

        if( ! $rules) return 0;

        if( is_numeric($rules) || is_string($rules)) return $rules;

        if( ! is_array($rules)) return 0;

        $rate = 0;

        foreach ($rules as $rule)
        {
            if($amount < $rule['min'] || $amount > $rule['max']) continue;

            $rate = $rule['rate'];
            break;
        }

        return $rate;
    }

    /**
     * Update Sale's commission ticket
     *
     * @param      integer  $user_id  The user identifier
     */
    public function update_commission_ticket_direct($user_id = 0, $month = NULL, $year = NULL)
    {
        $month  = $month ?: date('m');
        $year   = $year ?: date('Y');
        if( ! is_numeric($month) || ! is_numeric($year) || ! checkdate($month, 01, $year)) return FALSE;

        $start_time = start_of_month("{$year}/{$month}/01");
        $end_time   = end_of_month($start_time);
        $args                   = ['user_id'=>$user_id,'start_time'=>$start_time,'end_time'=>$end_time];
        $commission_overall     = $this->commission_ticket_m->get_commission_result(array_merge(['data_type'=>'overall'], $args));

        // Không phát sinh thanh toán và giá trị hoa hồng dựa trên doanh thu
        if(empty($commission_overall['amount_commisssion'])) return FALSE;

        $commission_ticket_update_data = array(
            'post_content'  => $commission_overall['amount_commisssion'],
            'post_author'   => $user_id,
            'start_date'    => $start_time,
            'end_date'      => $end_time,
            'post_slug'     => $this->commission_ticket_direct_m->get_commission_level(),
        );

        $meta_data = array(
            'term_commission_types' => $this->commission_ticket_m->get_commission_result(wp_parse_args(['data_type'=>'term_commission_types'], $args)),
        );

        foreach ($commission_overall as $key => $value)
        {
            if( ! is_array($value))
            {
                $meta_data[$key] = $value;
                continue;
            }

            if( ! in_array($key, ['cpc_type','account_type','webdoctor','adsplus'])) continue;

            foreach ($value as $k => $v)
            {
                $meta_data["{$key}_{$k}"] = $v;
            }
        }

        $monthly_commission_ticket_id   = 0;
        $monthly_commission_ticket      = $this->commission_ticket_direct_m
        ->select('post_id')->get_by(['start_date >='=>$start_time,'end_date <='=>$end_time,'post_author'=> $user_id]);

        if( ! $monthly_commission_ticket) // Insert new commission_ticket for target user
        {
            $monthly_commission_ticket_id = $this->commission_ticket_m->insert($commission_ticket_update_data);
        }
        else // Update commission_ticket for target user
        {
            $monthly_commission_ticket_id = $monthly_commission_ticket->post_id;
            $this->commission_ticket_m->set_post_type()->set_post_slug()
            ->update($monthly_commission_ticket->post_id, $commission_ticket_update_data);
        }


        // Update commission ticket postmeta
        foreach ($meta_data as $meta_key => $meta_value)
        {
            $meta_value = ( is_array($meta_value) || is_object($meta_value) ) ? serialize($meta_value) : $meta_value ;
            update_post_meta($monthly_commission_ticket_id, $meta_key, $meta_value);
        }

        return TRUE;
    }

    public function update_commission_ticket_indirect($user_id = 0, $month = NULL, $year = NULL)
    {
        $month  = $month ?: date('m');
        $year   = $year ?: date('Y');

        if( ! is_numeric($month) || ! is_numeric($year) ||  ! checkdate($month, 01, $year)) return FALSE;

        $start_time = start_of_month("{$year}/{$month}/01");
        $end_time   = end_of_month($start_time);

        // 1. Kiểm tra user có phải là cấp bậc trưởng nhóm | trưởng bộ phận
        $this->config->load('staffs/group');

        $manager_role_id = $this->option_m->get_value('manager_role_id');
        if(empty($manager_role_id)) $manager_role_id = $this->config->item('manager_role_id', 'groups');

        $leader_role_id = $this->option_m->get_value('leader_role_id');
        if(empty($leader_role_id)) $leader_role_id = $this->config->item('leader_role_id', 'groups');

        $sale = $this->sale_m->select('user_id,role_id')->set_user_type()->set_role(array($manager_role_id, $leader_role_id))
        ->where('user_status', 1)->get($user_id);
        if( ! $sale) return FALSE;

        $member_user_ids = array();
        $this->load->model('term_users_m');
        $this->load->model('staffs/department_m');
        $this->load->model('staffs/user_group_m');

        switch ($sale->role_id)
        {
            case $leader_role_id: // Vai trò trưởng nhóm

                $groups = $this->term_users_m->get_user_terms($sale->user_id, 'user_group');
                if( ! $groups) return FALSE; // Sale không thuộc bất kỳ nhóm nào

                foreach ($groups as $_group)
                {
                    $members = $this->term_users_m->get_term_users($_group->term_id, 'admin', ['where'=>"user.user_id != {$sale->user_id}"]);
                    if( ! $members) continue;

                    $member_user_ids = array_merge($member_user_ids, array_column($members, 'user_id'));                   
                }

                break;

            case $manager_role_id: // Vai trò trưởng bộ phận
                
                $departments = $this->term_users_m->get_user_terms($sale->user_id, 'department');
                if( ! $departments) return FALSE; // Sale không thuộc bất kỳ phòng ban nào

                $_args = array(
                    'where_in' => ['user.role_id' => $this->sale_m->get_roles()],
                    'where'=>"user.user_id != {$sale->user_id}");

                foreach ($departments as $_department)
                {
                    $_groups = $this->department_m->get_groups($_department->term_id);
                    if(empty($_groups)) continue;

                    foreach ($_groups as $_g)
                    {
                        $members = $this->term_users_m->get_term_users($_g->term_id, 'admin', $_args);
                        if( ! $members) continue;

                        $member_user_ids = array_merge($member_user_ids, array_column($members, 'user_id'));      
                    }
                }

                break;
        }

        if(empty($member_user_ids)) return FALSE;

        $args                   = ['user_id'=>$user_id,'start_time'=>$start_time,'end_time'=>$end_time];
        $commission_overall     = $this->commission_ticket_m->get_commission_result(array_merge(['data_type'=>'overall'], $args));

        // Không phát sinh thanh toán và giá trị hoa hồng dựa trên doanh thu
        $commission_tickets = $this->commission_ticket_direct_m
        ->select('posts.post_id, posts.post_author, posts.post_type, posts.post_content')
        ->where('start_date >=', $start_time)
        ->where('start_date <=', $end_time)
        ->where('posts.post_author !=', $user_id)
        ->get_many_by('posts.post_author', $member_user_ids);
        
        $amount_kpi_result = array('cpc_type'=>[], 'account_type'=>[], 'webdoctor'=>[]);

        foreach ($commission_tickets as $commission_ticket)
        {
            $cpc_type_amount_kpi        = (int) get_post_meta_value($commission_ticket->post_id, 'cpc_type_amount_kpi');
            $account_type_amount_kpi    = (int) get_post_meta_value($commission_ticket->post_id, 'account_type_amount_kpi');
            $webdoctor_amount_kpi       = (int) get_post_meta_value($commission_ticket->post_id, 'webdoctor_amount_kpi');

            $amount_kpi_result['cpc_type'][$commission_ticket->post_author]     = $cpc_type_amount_kpi;
            $amount_kpi_result['account_type'][$commission_ticket->post_author] = $account_type_amount_kpi;
            $amount_kpi_result['webdoctor'][$commission_ticket->post_author]    = $webdoctor_amount_kpi;
        }

        $this->config->load('mbusiness/commission');
        $metadata = array(
            'amount_commisssion' => 0,
            'commission_tickets' => array_column($commission_tickets, 'post_id')
        );

        /* Mặc định sẽ lấy % quản lý theo vai trò trưởng nhóm */
        $c_manager_prefix = 'manage_';         
        /* Trong trường hợp trưởng phòng , % quản lý dựa vào TP */
        if( $sale->role_id == $manager_role_id ) $c_manager_prefix = 'head_department_'; 

        foreach ($amount_kpi_result as $c_type => $amount_kpi_sales)
        {
            $rate               = (float) $this->config->item($c_manager_prefix.$c_type, 'commission');
            $amount_kpi         = array_sum($amount_kpi_sales);
            $amount_commisssion = $rate * $amount_kpi;

            $metadata[$c_type]                        = $amount_kpi_sales;
            $metadata["{$c_type}_amount_kpi"]         = $amount_kpi;
            $metadata["{$c_type}_rate"]               = $rate;
            $metadata["{$c_type}_amount_commisssion"] = $amount_commisssion;

            $metadata['amount_commisssion'] += $amount_commisssion;
        }


        if(empty($metadata['amount_commisssion'])) return FALSE;

        $update_data = array(
            'post_content'  => $metadata['amount_commisssion'],
            'post_author'   => $user_id,
            'start_date'    => $start_time,
            'end_date'      => $end_time,
            'post_slug'     => $this->commission_ticket_indirect_m->get_commission_level()
        );

        $ticket_id = 0;

        $monthly_commission_ticket = $this->commission_ticket_indirect_m->select('post_id')->get_by(['start_date >='=>$start_time,'end_date <='=>$end_time,'post_author'=> $user_id]);

        // INSERT NEW COMMISSION TICKET INDIRECT 
        if( ! $monthly_commission_ticket) 
        {
            $ticket_id = $this->commission_ticket_indirect_m->insert($update_data);

            // Update commission ticket postmeta
            foreach ($metadata as $meta_key => $meta_value)
            {
                $meta_value = ( is_array($meta_value) || is_object($meta_value) ) ? serialize($meta_value) : $meta_value ;
                update_post_meta($ticket_id, $meta_key, $meta_value);
            }

            return TRUE;
        }

        // UPDATE COMMISSION TICKET INDIRECT
        $update_data['updated_on'] = time();

        $ticket_id  = $monthly_commission_ticket->post_id;
        $result     = $this->commission_ticket_indirect_m->set_post_type()->set_post_slug()->update($ticket_id, $update_data);

        if( ! $result) return FALSE;

        foreach ($metadata as $meta_key => $meta_value)
        {
            $meta_value = ( is_array($meta_value) || is_object($meta_value) ) ? serialize($meta_value) : $meta_value ;
            update_post_meta($ticket_id, $meta_key, $meta_value);
        }

        return TRUE;
    }

    /**
     * Calculate Sales Rank by Amount KPI
     *
     * @param      integer  $time   The time
     *
     * @return     string   ( description_of_the_return_value )
     */
    public function calc_sale_rank($time = 0)
    {
        $start_of_month = $this->mdate->startOfMonth($time);
        $end_of_month   = $this->mdate->endOfMonth($time);
        
        $commission_tickets = $this->commission_ticket_m
        ->select('posts.post_id, posts.post_author, postmeta.meta_value as amount_kpi')
        ->where('start_date >=', $start_of_month)
        ->where('start_date <=', $end_of_month)
        ->join('postmeta', "postmeta.post_id = posts.post_id AND posts.post_type = '{$this->commission_ticket_m->post_type}' AND meta_key='amount_kpi'", 'LEFT')
        ->order_by('(postmeta.meta_value)*1','desc')
        ->get_all();

        $this->scache->write(count($commission_tickets), "modules/mbusiness/ranked_sale_num_{$start_of_month}_{$end_of_month}");

        if( ! $commission_tickets) return '??';

        foreach ($commission_tickets as $key => $commission_ticket)
        {
            update_post_meta($commission_ticket->post_id, 'sale_rank', ($key+1));
        }

        return TRUE;
    }
}
/* End of file Sale_commission_m.php */
/* Location: ./application/modules/mbusiness/models/Sale_commission_m.php */