<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbusiness_report_m extends Base_model {

    public $primary_key = 'id';
    public $_table = 'ordermeeting';

    function __construct()
    {
        parent::__construct();

        defined('CALL_PER_ORDERMETTING') OR define('CALL_PER_ORDERMETTING',10);
    }


    /*
    
    */

    /**
     * Sends a monthly callkpi mail.
     * 
     * $today : FALSE - báo cáo của ngày hôm qua ,TRUE - báo cáo ngày hôm nay đến thời điểm hiện tại 
     * 
     * @param      <type>  $is_today  The today
     */
    public function send_monthly_callkpi_mail($is_today = TRUE)
    {
        $time           = $is_today ? time() : strtotime('yesterday');
        if( ! $is_today)
        {
            $this->table->set_caption('<h2>XẾP HẠNG THÁNH CHĂM CHỈ</h2><br>Thống kê cuộc gọi ngày hôm qua '.my_date($time,'d/m/Y'));
            $title = 'Thống kê cuộc gọi của kinh doanh ngày hôm qua '.my_date($time,'d/m/Y');
        }
        else 
        {
            $this->table->set_caption('<h2>XẾP HẠNG THÁNH CHĂM CHỈ</h2><br>Thống kê cuộc gọi trong ngày '.my_date($time,'d/m/Y'));
            $title = 'Thống kê cuộc gọi của kinh doanh ngày '.my_date($time,'d/m/Y H:i:s');
        }

        $role_ids = array(13,9);

        /* Request data for 1 day */
        $startOfDay = $this->mdate->startOfDay($time);
        $endOfDay   = $this->mdate->endOfDay($time);
        $data_kpi_day   = $this->admin_m
        ->select('user.user_id,user.display_name,user.role_id')
        ->select('callkpi.object_id AS ext')
        ->select('SUM(callkpi.result_value)*COUNT(DISTINCT callkpi.kpi_id)/COUNT(`user`.user_id) AS call_kpi_result')
        ->select('SUM(callkpi.kpi_value)*COUNT(DISTINCT callkpi.kpi_id)/COUNT(`user`.user_id) AS call_kpi_value')
        ->select('COUNT(DISTINCT ordermeeting.id) AS meeting_num')
        ->select('COUNT(DISTINCT oms.id) AS support_meeting_num')
        ->select('COUNT(DISTINCT ordermeeting.id)+COUNT(DISTINCT oms.id) AS total_meeting')
        ->select('(SUM(callkpi.result_value)*COUNT(DISTINCT callkpi.kpi_id)/COUNT(`user`.user_id))+((COUNT(DISTINCT ordermeeting.id)+COUNT(DISTINCT oms.id))*'.CALL_PER_ORDERMETTING.') AS total_result')
        ->where('user_type','admin')
        ->where_in('role_id',$role_ids)
        ->join('kpi callkpi','callkpi.user_id = user.user_id AND callkpi.kpi_type = "phonecall" AND callkpi.kpi_datetime BETWEEN \''.my_date($startOfDay,'Y-m-d H:i:s').'\' AND \''.my_date($endOfDay,'Y-m-d H:i:s').'\'','LEFT')
        ->join('ordermeeting',"ordermeeting.user_id = user.user_id AND ordermeeting.status = \"verified\" AND ordermeeting.start_time BETWEEN {$startOfDay} AND {$endOfDay}",'LEFT')
        ->join('ordermeeting oms',"oms.support_id = user.user_id AND oms.status = \"verified\" AND oms.start_time BETWEEN {$startOfDay} AND {$endOfDay}",'LEFT')
        ->order_by('total_result','DESC')
        ->group_by('user.user_id')
        ->get_many_by(['user.user_status'=>1]);

        /* Request data from start Of Month util $time */
        $startOfMonth       = $this->mdate->startOfMonth($time);
        $data_kpi_monthly   = $this->admin_m
        ->select('user.user_id,user.display_name,user.role_id')
        ->select('callkpi.object_id AS ext')
        ->select('SUM(callkpi.result_value)*COUNT(DISTINCT callkpi.kpi_id)/COUNT(`user`.user_id) AS call_kpi_result')
        ->select('SUM(callkpi.kpi_value)*COUNT(DISTINCT callkpi.kpi_id)/COUNT(`user`.user_id) AS call_kpi_value')
        ->select('COUNT(DISTINCT ordermeeting.id) AS meeting_num')
        ->select('COUNT(DISTINCT oms.id) AS support_meeting_num')
        ->select('COUNT(DISTINCT ordermeeting.id)+COUNT(DISTINCT oms.id) AS total_meeting')
        ->select('(SUM(callkpi.result_value)*COUNT(DISTINCT callkpi.kpi_id)/COUNT(`user`.user_id))+((COUNT(DISTINCT ordermeeting.id)+COUNT(DISTINCT oms.id))*'.CALL_PER_ORDERMETTING.') AS total_result')
        ->where('user_type','admin')
        ->where_in('role_id',$role_ids)
        ->join('kpi callkpi','callkpi.user_id = user.user_id AND callkpi.kpi_type = "phonecall" AND callkpi.kpi_datetime BETWEEN \''.my_date($startOfMonth,'Y-m-d H:i:s').'\' AND \''.my_date($endOfDay,'Y-m-d H:i:s').'\'','LEFT')
        ->join('ordermeeting',"ordermeeting.user_id = user.user_id AND ordermeeting.status = \"verified\" AND ordermeeting.start_time BETWEEN {$startOfMonth} AND {$endOfDay}",'LEFT')
        ->join('ordermeeting oms',"oms.support_id = user.user_id AND oms.status = \"verified\" AND oms.start_time BETWEEN {$startOfMonth} AND {$endOfDay}",'LEFT')
        ->order_by('total_result','DESC')
        ->group_by('user.user_id')
        ->get_many_by(['user.user_status'=>1]);

        $data_kpi_monthly = array_column($data_kpi_monthly, NULL,'user_id');

        $this->config->load('table_mail');
        $this->table->set_template($this->config->item('mail_template'));
        $this->table->set_heading('#','Tên','Chức vụ','Cuộc gọi','Cuộc gặp','Quy đổi cuộc gọi theo ngày','Quy đổi cuộc gọi đầu tháng ','Tỷ lệ % đạt KPIs tháng');

        $datatable = array();

        $sumary_date_callkpi = 0;
        $sumary_date_total_meeting = 0;
        $sumary_date_kpi = 0;
        $sumary_monthly_kpi = 0;
        $sumary_monthly_kpi_value = 0;
        $sumary_monthly_kpi_rate = 0;

        foreach ($data_kpi_day as $key => $user)
        {
            $display_name = ($user->ext ? "({$user->ext}) " : '').$user->display_name;
                                                                   
            $role_name = $user->role_id == 9 ? 'NV' : 'TN';

            $kpi_result = (int)$user->total_result.'/'.(int) $user->call_kpi_value;

            $user_kpi_monthly_result = 0;
            $user_monthly_kpi_value = 0;
            $user_kpi_monthly_rate   = 0;
            if(!empty($data_kpi_monthly[$user->user_id]))
            {
                $user_monthly_total_result  = (int)$data_kpi_monthly[$user->user_id]->total_result;
                $user_monthly_kpi_value     = (int)$data_kpi_monthly[$user->user_id]->call_kpi_value;
                $user_monthly_passed_rate   = div($user_monthly_total_result,$user_monthly_kpi_value);

                $user_kpi_monthly_result = "{$user_monthly_total_result}/{$user_monthly_kpi_value}";
                $user_kpi_monthly_rate = div($user_monthly_total_result,$user_monthly_kpi_value)*100;
            }

            $sumary_date_callkpi+= (int)$user->call_kpi_result;
            $sumary_date_total_meeting+= (int) $user->total_meeting;
            $sumary_date_kpi+= (int)$user->total_result;
            $sumary_monthly_kpi+= $user_kpi_monthly_result;
            $sumary_monthly_kpi_value+= $user_monthly_kpi_value;

            $datatable[] = array(
                $display_name,
                $role_name,
                (int)$user->call_kpi_result,
                $user->total_meeting,
                $kpi_result,
                $user_kpi_monthly_result,
                currency_numberformat($user_kpi_monthly_rate,'%',2)
            );
        }

        usort($datatable, function($x,$y){
            $a = (double)$x[6];
            $b = (double)$y[6];
            return $a < $b;
        });

        foreach ($datatable as $key => $row)
        {
            if($key < 3) $row[0] = "<b>{$row[0]}</b>"; // Bold 3 người đầu tiên
            array_unshift($row,$key+1);
            $this->table->add_row($row);
        }

        $this->table->add_row('<b>TỔNG:</b>','','',$sumary_date_callkpi,$sumary_date_total_meeting,$sumary_date_kpi,$sumary_monthly_kpi,currency_numberformat(div($sumary_monthly_kpi,$sumary_monthly_kpi_value)*100,'%',2));

        $content = $this->table->generate();
        
        $this->load->library('email');
        $this->email->from('report@webdoctor.vn','Report');
        $this->email->to('kd@adsplus.vn');
        $this->email->cc('sonnt@adsplus.vn');
        $this->email->subject($title);
        $this->email->message($content);
        return $this->email->send();
    }
}
/* End of file Mbusiness_report_m.php */
/* Location: ./application/modules/mbusiness/models/Mbusiness_report_m.php */