<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

$this->template->stylesheet->add('plugins/datatables/dataTables.bootstrap.css');
$this->template->javascript->add('plugins/datatables/jquery.dataTables.min.js');
$this->template->javascript->add('plugins/datatables/dataTables.bootstrap.min.js');
$this->template->javascript->add('plugins/datatables/extensions/KeyTable/js/dataTables.keyTable.min.js');

?>
<dashboard-commission-overall-component month="<?php echo $month;?>" user_id="<?php echo $user_id;?>"></dashboard-commission-overall-component>

<div class="row">
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-list-ul"></i> Tất cả dịch vụ</a></li>
                <li><a href="#tab_2" data-toggle="tab"><i class="text-yellow fa fa-circle"></i> Adsplus.vn QLTK</a></li>
                <li><a href="#tab_3" data-toggle="tab"><i class="text-orange fa fa-circle"></i> Adsplus.vn CPC</a></li>
                <li><a href="#tab_4" data-toggle="tab"><i class="text-blue fa fa-circle"></i> Webdoctor.vn</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <commission-contracts-component month="<?php echo $month;?>" user_id="<?php echo $user_id;?>"></ommission-contracts-component>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                  <commission-contracts-component month="<?php echo $month;?>" c_type="account_type" user_id="<?php echo $user_id;?>"></ommission-contracts-component>
                </div>
                <div class="tab-pane" id="tab_3">
                  <commission-contracts-component month="<?php echo $month;?>" c_type="cpc_type" user_id="<?php echo $user_id;?>"></ommission-contracts-component>
                </div>

                <div class="tab-pane" id="tab_4">
                  <commission-contracts-component month="<?php echo $month;?>" c_type="webdoctor" user_id="<?php echo $user_id;?>"></ommission-contracts-component>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<script type="text/javascript"> var dataobject = <?php echo json_encode($dataobject);?>; </script>
<?php
$version = start_of_day();
echo $this->template->trigger_javascript(admin_theme_url("modules/mbusiness/overall-commission.js?v={$version}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/mbusiness/commission-contracts.js?v={$version}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/mbusiness/app.js?v={$version}"));