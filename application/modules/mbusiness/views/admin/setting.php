<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="<?php echo ($view == 'kpi' ? 'active' : '');?>"><a href="#tab_1" data-toggle="tab">KPI</a></li>
        <li class="<?php echo ($view == 'credit_quota' ? 'active' : '');?>" ><a href="#tab_2" data-toggle="tab">Quota bảo lãnh</a></li>
        <li class="<?php echo ($view == 'assign_group' ? 'active' : '');?>" ><a href="#tab_3" data-toggle="tab">Tổ chức nhóm</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
        	<div class="row">
	       	<?php
	       	/* SECTION : Common actions for contract */
			if($has_setting_kpi) echo $this->template->common_actions->view('setting/kpi');
	       	?>
	       	</div>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
            The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar, their pronunciation and their most common words. Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators. To achieve this, it would be necessary to have uniform grammar, pronunciation and more common words. If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages.
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_3">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </div>
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</div>