<?php
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
?>

<!-- Ordermeeting calendar chart -->
<div class="box box-primary">
  <div class="box-header with-border">
    <i class="fa fa-bar-chart-o"></i>
    <h3 class="box-title">Sơ đồ thống kê 'lịch hẹn đã được xác nhận'</h3>
    <div class="box-tools pull-right">
      <div class="btn-group">
        <a href="<?php echo module_url('ordermeeting');?>" class="btn btn-default btn-xs">Xem chi tiết</a>
      </div>
    </div>
  </div>
  <div class="box-body">
    <div class="col-xs-8">
      <div id="calendar_basic"></div>
    </div>
    <div class="col-xs-4">
      <div id="piechart_3d"></div>
    </div>
  </div>
  <!-- /.box-body-->
</div>
<!-- /.box -->

<!-- interactive chart -->
<div class="box box-primary">
<div class="box-header with-border">
  <i class="fa fa-bar-chart-o"></i>

  <h3 class="box-title">Thống kê KPI hoạt động của bộ phận Kinh Doanh</h3>

  <div class="box-tools pull-right">
    <div class="btn-group">
      <a href="<?php echo module_url('activity');?>" class="btn btn-default btn-xs">Xem chi tiết</a>
    </div>
  </div>
</div>
<div class="box-body">
  <div id="chart_div" style="height: 400px;"></div>
</div>
<!-- /.box-body-->
</div>  
<!-- /.box -->


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load('current', {packages: ['corechart', 'line', 'calendar']});
google.charts.setOnLoadCallback(drawKPIchart);

var ordermeeting_data = <?php echo json_encode($ordermeeting_datachart);?>;
var ordermeeting_datachart = [];

ordermeeting_data.forEach(function(e,i){
	ordermeeting_datachart.push([new Date(e[0]*1000),e[1]]);
});
google.charts.setOnLoadCallback(function() {drawOrdermeetingCalendarChart(ordermeeting_datachart);} );

var kpiChartData = <?php echo json_encode($salekpi);?>;
google.charts.setOnLoadCallback(function() {drawKPIchart(kpiChartData);} );

var pieChartData = <?php echo json_encode($ordermeeting_count_chart);?>;
google.charts.setOnLoadCallback(function() {drawChart(pieChartData);} );

function drawKPIchart(chartData) {

	var a = [];

	for(var i = 0;i<30;i++)
	{
		a[i] = [Math.floor((Math.random() * 10) + 1)+'/'+Math.floor((Math.random() * 10) + 1),Math.floor((Math.random() * 100) + 1),Math.floor((Math.random() * 100) + 1),Math.floor((Math.random() * 100) + 1)]
	}

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'X');
      data.addColumn('number', 'KPI');
      data.addColumn('number', 'Tổng đạt KPI (+ quy đổi lịch hẹn)');
      data.addColumn('number', 'Số cuộc gọi > 15s');

      data.addRows(chartData);
      var options = {
        hAxis: {
          title: 'Thời gian',
          logScale: true
        },
        vAxis: {
          title: 'KPI',
          logScale: false
        },
        colors: ['#a52714', '#097138','#096138']
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }

function drawOrdermeetingCalendarChart(data) {
       var dataTable = new google.visualization.DataTable();
       dataTable.addColumn({ type: 'date', id: 'Date' });
       dataTable.addColumn({ type: 'number', id: 'Đã được xác nhận' });

       dataTable.addRows(data);
       var chart = new google.visualization.Calendar(document.getElementById('calendar_basic'));

       var options = {
         
         calendar: { cellSize: 15 },
         color: '#1a8763',
       };

       chart.draw(dataTable, options);
   }



function drawChart(data)
{
  var data = google.visualization.arrayToDataTable(data);

  var options = {
  title: 'Thống kê số lượng theo trạng thái',
  is3D: true,
  colors: ['#f4a941', '#41f44f','#f45541']
  };

  var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
  chart.draw(data, options);
}
</script>