<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->stylesheet->add('modules/mbusiness/css/style.css');
$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

echo $this->admin_form->datatable(['remote_url'=>admin_url('mbusiness/ajax/commission/sales'),'is_download'=>TRUE]);

$version = 20180412;
echo $this->template->trigger_javascript(admin_theme_url("modules/component/ui.js?v={$version}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/mbusiness/app.js?v={$version}"));