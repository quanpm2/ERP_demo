<?php
$this->template->javascript->add('plugins/wizard/jquery.bootstrap.wizard.js');
$this->template->javascript->add('plugins/validate/jquery.validate.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');

?>

<div class="col-12">
<?php

echo $this->admin_form->form_open('');
echo $this->admin_form->input('Khách hàng','edit[display_name]',$edit->display_name,'',['addon_begin'=>'<i class="fa fw fa-briefcase"></i>']);
echo $this->admin_form->input('Người liên hệ','edit[cus_name]',$edit->cus_name,'',['addon_begin'=>'<i class="fa fw fa-user-secret"></i>']);
echo $this->admin_form->input('Điện thoại','edit[cus_phone]',$edit->cus_phone,'',['addon_begin'=>'<i class="fa fa-phone"></i>']);

echo form_hidden('edit[start_time]',$edit->start_time);
echo form_hidden('edit[end_time]',$edit->end_time);
echo $this->admin_form->input('Thời gian','datetimerange',$value = '',$help = '', ['addon_begin'=>'<i class="fa fa-clock-o"></i>']);

echo form_hidden('edit[user_id]', $edit->user_id ?: $this->admin_m->id);
echo $this->admin_form->dropdown('Người hỗ trợ', 'edit[support_id]', prepare_dropdown($saleUsersKeyValue,[0=>'-- Chọn người hỗ trợ --']) , $edit->support_id,'',['addon_begin'=>'<i class="fa fw fa-user-plus"></i>']);

echo $this->admin_form->textarea('Địa điểm','edit[location]',$edit->location,'',['rows'=>3,'placeholder'=>'Địa điểm tiếp khách hàng',]);
echo $this->admin_form->textarea('Nội dung','edit[content]', $edit->content);


echo $this->admin_form->formGroup_begin();

if($edit->status == 'draft')
{
	echo form_button(['name'=>'submit','type'=>'submit','class'=>'btn btn-primary text-center'],'<i class="fa fa-fw fa-envelope"></i>Gửi Mail yêu cầu');
}

echo $this->admin_form->formGroup_end();
echo $this->admin_form->form_close();
?>
</div>
<script type="text/javascript">

$(function(){

	var callback = function(start,end){

		$( "input[name='edit[start_time]']").val(start.unix());
		$( "input[name='edit[end_time]']").val(end.unix());

		$("input[name='datetimerange']").val(start.format('DD/MM/YYYY h:mm A') + ' - ' + end.format('DD/MM/YYYY h:mm A'));	
	};

	$("input[name='datetimerange']").daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'DD/MM/YYYY h:mm A'},callback);

	var startTime = $("input[name='edit[start_time]']").val();
	var endTime = $("input[name='edit[end_time]']").val();	
	$("input[name='datetimerange']").val(moment.unix(startTime).format('DD/MM/YYYY h:mm A') + ' - ' + moment.unix(endTime).format('DD/MM/YYYY h:mm A'));

	$('.set-datepicker').datepicker({
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true,
	});

	$("#main-form").validate({
		rules: {
		  'user_phone_private': {
		    minlength: 3,
		    digits: true,
		  },
		  'user_phone': {
		    minlength: 3,
		    digits: true,
		  }
		},
		messages:{
			'user_phone' : {
				minlength : "Số điện thoại phải ít nhất 3 kí tự.",
				digits : "Số điện thoại phải là kiểu số.",
			},
			'user_phone_private' : {
				minlength : "Số điện thoại phải ít nhất 3 kí tự.",
				digits : "Số điện thoại phải là kiểu số.",
			},
		}
	});
});

</script>