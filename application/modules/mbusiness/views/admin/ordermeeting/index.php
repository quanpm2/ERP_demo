<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
?>

<div class="row" id="contract-databuilder-container">
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-list-ul"></i> Tất cả lịch hẹn</a></li>
                <li><a href="#tab_2" data-toggle="tab"><i class="text-red fa fa-circle"></i> Đang chờ duyệt</a></li>
                <li><a href="#tab_3" data-toggle="tab"><i class="text-green fa fa-circle"></i> Đã xác nhận</a></li>
                <li class="pull-right">
                    <?php if (has_permission('mbusiness.ordermeeting.add')): ?> 
                        <button class="btn btn-primary btn-flat" onclick="window.location.href='<?php echo admin_url('mbusiness/ordermeeting/edit');?>'"><i class="fa fa-fw fa-plus "></i> Đặt lịch hẹn</button>
                    <?php endif ?>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <ordermeeting-datatable-component 
                    	remote_url="<?php echo admin_url('mbusiness/ajax/dataset/ordermeeting');?>"></ordermeeting-datatable-component>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <ordermeeting-datatable-component 
                    	remote_url="<?php echo admin_url('mbusiness/ajax/dataset/ordermeeting?where%5Bstatus%5D=sent');?>"></ordermeeting-datatable-component>
                </div>
                <div class="tab-pane" id="tab_3">
                    <ordermeeting-datatable-component 
                    	remote_url="<?php echo admin_url('mbusiness/ajax/dataset/ordermeeting?where%5Bstatus%5D=verified');?>"></ordermeeting-datatable-component>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<?php
echo $this->template->trigger_javascript(admin_theme_url('modules/component/ui.js'));  
echo $this->template->trigger_javascript(admin_theme_url('modules/mbusiness/app.js'));
/* End of file index.php */
/* Location: ./application/modules/mbusiness/views/admin/ordermeeting/index.php */