<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->stylesheet->add('plugins/bootstrap-slider/slider.css');
$this->template->javascript->add('plugins/bootstrap-slider/bootstrap-slider.js');

echo $this->admin_form->box_open('Cấu hình KPI theo quyền hạn nhân viên');
echo $this->admin_form->form_open();
echo $this->admin_form->input('KPI Trưởng nhóm', 'edit[leader_kpi_per_day]', $leader_kpi_per_day,'KPI cuộc gọi mỗi ngày của trưởng nhóm (Mặc định : 25)',['id'=>'leader_kpi','data-slider-id'=>'blue', 'data-slider-min'=>"1",'data-slider-max'=>'80','data-slider-step'=>'1','data-slider-value'=>$leader_kpi_per_day]);
echo $this->admin_form->input('KPI Nhân viên', 'edit[member_kpi_per_day]', $member_kpi_per_day,'KPI cuộc gọi mỗi ngày của nhân viên (Mặc định : 40)',['id'=>'member_kpi','data-slider-id'=>'green', 'data-slider-min'=>"1",'data-slider-max'=>'80','data-slider-step'=>'1','data-slider-value'=>$member_kpi_per_day]);

echo $this->admin_form->formGroup_begin('simulate-recalc','Tổng KPI hiện tại');

echo "<b><span id='month_kpi_result'>{$month_kpi_result}</span></b>
		   =   <b><span id='workdays_per_month'>{$workdays_per_month}</span></b> ngày công * 
		( <b><span id='leaders'>{$leaders}</span></b> trưởng nhóm * <b><span id='leader_kpi_per_day'>{$leader_kpi_per_day}</span></b>
			+ <b><span id='members'>{$members}</span></b> nhân viên * <b><span id='member_kpi_per_day'>{$member_kpi_per_day}</span></b>)";

echo $this->admin_form->formGroup_end();
echo $this->admin_form->box_close(array('kpi_submit', 'Áp dụng'), array('kpi_setdefault_btn', 'Lưu theo mặc định'));
echo $this->admin_form->form_close();
?>
<script type="text/javascript">
var data_object = <?php echo $object_data;?>;
// $("#month-kpi-result").text

function recalc_data(a,b)
{
	var leader_kpi_per_day = $('#leader_kpi').data('slider').getValue();
	var member_kpi_per_day = $('#member_kpi').data('slider').getValue();

	$('#leader_kpi_per_day').text(leader_kpi_per_day);
	$('#member_kpi_per_day').text(member_kpi_per_day);

	var month_kpi_result = $('#workdays_per_month').text() * ($('#leaders').text()*leader_kpi_per_day + $('#members').text()*member_kpi_per_day);

	$('#month_kpi_result').text(month_kpi_result);
}

$(function(){

	var leader_kpi_slider = $('#leader_kpi').slider({
		formatter: function(value) {
			return value;
		},
		tooltip: 'always'
		})
		.on('slide', recalc_data)
		.data('slider');

	var member_kpi_slider = $('#member_kpi').slider({
		formatter: function(value) {
			return value;
		},
		tooltip: 'always'
		})
		.on('slide', recalc_data);

	$('input[name="kpi_setdefault_btn"]').click(function(){
		$('#leader_kpi').slider('setValue',parseInt(data_object.leader_kpi_per_day_default));
		$('#member_kpi').slider('setValue',parseInt(data_object.member_kpi_per_day_default));
	})
})
</script>