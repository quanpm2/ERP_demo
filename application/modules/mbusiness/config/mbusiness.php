<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config["leader_role_id"]		= 13;
$config["member_role_id"]		= 9;
$config["leader_kpi_per_day"]	= 25;
$config["member_kpi_per_day"]	= 40;
$config["workdays_per_month"]	= 24;