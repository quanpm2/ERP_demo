<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

$config['processStates'] = array(
	'draft' => 'Đang soạn',
	'sent' => 'Chờ',
	'verified' => 'Xác nhận',
	'deleted' => 'Hủy bỏ'
);