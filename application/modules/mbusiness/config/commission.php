<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['commission'] = array(

	/* INDIRECT COMISSION RATE FOR MANAGE LEVELS */
	
	'head_department_cpc_type' 		=> 0.01,
	'head_department_account_type' 	=> 0.01,
	'head_department_webdoctor' 	=> 0.01,

	'manage_cpc_type' 		=> 0.015,
	'manage_account_type' 	=> 0.04,
	'manage_webdoctor' 		=> 0.04,

	// DIRECT COMMISSION RATE FOR SALE VIA SERVICE SOLD
	'cpc_type' => array(
			['min' => 0, 'max' => 14999999, 'rate' => 0.035],
			['min' => 15000000, 'max' => PHP_INT_MAX , 'rate' => 0.04] ),

	'account_type' => array(
			['min' => 0, 'max' => 14999999, 'rate' => 0.12],
			['min' => 15000000, 'max' => PHP_INT_MAX , 'rate' => 0.16] ),

	'webdoctor' => array( ['min' => 0, 'max' => PHP_INT_MAX , 'rate' => 0.16] ),
	'none' 		=> 0,
	'default' 	=> 'webdoctor'
);