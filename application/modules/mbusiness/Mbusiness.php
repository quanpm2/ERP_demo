<?php
class Mbusiness_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name()
	{
		return 'Mbusiness';
	}

	public function init()
	{
		$this->_load_menu();
		// $this->_update_permissions();
	}

	private function _update_permissions()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}

		return TRUE;
	}

	/**
	 * Init menu LEFT | NAV ITEM FOR MODULE
	 * then check permission before render UI
	 */
	private function _load_menu()
	{
		$order = 1;
 		if( ! is_module_active('mbusiness')) return FALSE;	

 		if(has_permission('mbusiness.activity.access'))
 		{
	 		$this->menu->add_item(array(
			'id' => 'ordermeeting',
			'name' => 'KPI Kinh Doanh',
			'parent' => 'manager',
			'slug' => admin_url('mbusiness/activity'),
			'icon' => 'fa fa-headphones',
			'order' => $order++,
			),'navbar');
 		}

 		if(has_permission('mbusiness.commission.access'))
 		{
	 		$this->menu->add_item(array(
			'id' => 'commissions',
			'name' => 'Doanh thu tính thưởng',
			'parent' => 'manager',
			'slug' => admin_url('mbusiness/commissions'),
			'icon' => 'fa fa-dollar',
			'order' => $order++,
			),'navbar');
 		}

 		if(has_permission('mbusiness.ordermeeting.access'))
		{
			$this->menu->add_item(array(
			'id' => 'ordermeeting-index',
			'name' => 'Lịch hẹn khách hàng',
			'parent' => 'manager',
			'slug' => admin_url('mbusiness/ordermeeting'),
			'order' => $order++,
			'icon' => 'fa fa-calendar-o'
			), 'navbar');
		}

 		if(!is_module('mbusiness')) return FALSE;

 		if(has_permission('mbusiness.activity.access'))
 		{
	 		$this->menu->add_item(array(
				'id' => 'ordermeeting',
				'name' => 'KPI Kinh Doanh',
				'parent' => 'manager',
				'slug' => module_url(),
				'icon' => 'fa fa-headphones',
				'order' => $order++,
				),'navbar');
 		}
		
		if(has_permission('mbusiness.activity.access'))
		{
			$this->menu->add_item(array(
			'id' => 'mbusiness-dashboard',
			'name' => 'Dashboard',
			'parent' => null,
			'slug' => module_url(),
			'order' => $order++,
			'icon' => 'fa fa-tachometer'
			), 'left');

			$this->menu->add_item(array(
			'id' => 'mbusiness-kpi',
			'name' => 'Thống kê KPI call',
			'parent' => null,
			'slug' => module_url('activity'),
			'order' => $order++,
			'icon' => 'fa fa-headphones'
			), 'left');
		}		


		/* Order meeting */
		
		if(has_permission('mbusiness.ordermeeting'))
		{	
			if(is_module('mbusiness'))
			{
				$isManager = has_permission('mbusiness.ordermeeting.manage');

				if(has_permission('mbusiness.ordermeeting.access') || $isManager)
				{
					$this->menu->add_item(array(
					'id' => 'ordermeeting-index',
					'name' => 'Danh sách lịch hẹn',
					'parent' => NULL,
					'slug' => module_url('ordermeeting'),
					'order' => $order++,
					'icon' => 'fa fa-calendar-o'
					), 'left');
				}

				if(has_permission('mbusiness.ordermeeting.add') || has_permission('mbusiness.ordermeeting.update') || $isManager)
				{
					$this->menu->add_item(array(
					'id' => 'ordermeeting-edit',
					'name' => 'Đặt lịch hẹn',
					'parent' => NULL,
					'slug' => module_url('ordermeeting/edit/0'),
					'order' => $order++,
					'icon' => 'fa fa-plus'
					), 'left');
				}
			}
		}

		if(has_permission('mbusiness.setting.access'))
 		{
	 		$this->menu->add_item(array(
				'id' => 'mbusiness-setting',
				'name' => 'Cấu hình',
				'parent' => null,
				'slug' => module_url('setting'),
				'icon' => 'fa fa-wrench',
				'order' => $order++,
				),'left');
 		}
	}

	public function title()
	{
		return 'Mbusiness';
	}

	public function author()
	{
		return 'THONH';
	}

	public function version()
	{
		return 'beta 0.1';
	}

	public function description()
	{
		return 'Mbusiness';
	}

	private function init_permissions()
	{
		$permissions = array(
			'admin.mbusiness' => array('description' => 'Quản lý Mbusiness','actions' => array('access','add','update','delete','mdeparment','mgroup')),
			'mbusiness.kpi' => array('description' => '','actions' => array('access','add','update','delete','manage','mdeparment','mgroup')),
			'mbusiness.activity' => array('description' => '','actions' => array('access','manage','mdeparment','mgroup')),
			'mbusiness.ordermeeting' => array('description' => '','actions' => array('confirm','access','add','update','delete','manage','mdeparment','mgroup')),
			'mbusiness.setting' => array('description' => '','actions' => array('access','manage'),'mdeparment','mgroup'),
			'mbusiness.setting_kpi' => array('description' => '','actions' => array('access','update','mdeparment','mgroup')),
			'mbusiness.commission' => array('description' => '','actions' => array('access','manage','mdeparment','mgroup'))
		);

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if( ! $permissions) return false;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}