<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Department_m extends Term_m {
	
    public $term_type = 'department';

    function __construct(){

        parent::__construct();

        $model_names = array('staffs/user_group_m','term_users_m');
        $this->load->model($model_names);
    }

    /**
     * Sets the term type.
     *
     * @return     this
     */
    public function set_term_type()
	{
		return $this->where('term_type',$this->term_type);
	}

    public function get_groups($department_id = 0)
    {
        $cache_key  = "term/{$department_id}_groups";
        $data       = $this->scache->get($cache_key);

        if($data && 1==2) return $data;

        $data = $this->user_group_m->set_term_type()->get_many_by(['term_parent' => $department_id]);
        $this->scache->write($data , $cache_key);

        return $data;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function existed_check($value)
    {
        if (empty($value)) {
            return true;
        }
        
        $isExisted = $this->set_term_type()->where('term.term_id', (int) $value)->count_by() > 0;
        if (!$isExisted) {
            $this->form_validation->set_message('existed_check', 'Phòng ban không tồn tại.');
            return false;
        }

        return true;
    }

    /**
     * Retrieves the staff members based on the department ID.
     *
     * @param int $department_id The ID of the department. Defaults to 0.
     * 
     * @return array List of staff members
     */
    public function get_staffs($department_id, $status = [1])
    {
        $builder = $this->set_term_type()
            ->join('term_users', 'term_users.term_id = term.term_id')
            ->join('user', 'user.user_id = term_users.user_id')
            
            ->where('term.term_id =', $department_id)
            ->where('user.user_type =', 'admin')

            ->select('user.user_id')
            ->select('user.display_name')
            ->select('user.user_email')

            ->order_by('user.user_status', 'DESC');

        if(is_array($status))
        {
            $builder->where_in('user.user_status', $status);
        }
        else
        {
            $builder->where('user.user_status =', $status);
        }

        $builder->group_by('user.user_id');

        $data = $builder->as_array()->get_all();

        return $data;
    }
}
/* End of file Department_m.php */
/* Location: ./application/modules/staffs/models/Department_m.php */