<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Call_center_sale_m extends Base_model
{

    public $_table      = 'tbl_sale';
    public $primary_key = 'id';

    public $id = 0;
    public $may_nhanh;
    public $name;
    public $email_sale;
    public $owner_id;

    /**
     * Initialise the model, tie into the CodeIgniter object and
     * try our best to guess the table name.
     */
    public function __construct()
    {
        $CI = &get_instance();
        $this->db = $CI->load->database('log_call_hubspot', TRUE);

        parent::__construct();
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function available_check($value, $user_id)
    {
        if (empty($value)) {
            return true;
        }

        $this->load->model('staffs/staffs_m');

        $call_center = $this->select('id, email_sale')->where('may_nhanh', $value)->get_by();
        if (empty($call_center)) return true;

        $userExisted = $this->staffs_m
            ->set_user_type()
            ->where('user_email', $call_center->email_sale)
            ->where('user_status', 1)
            ->where('user_id !=', (int) $user_id)
            ->count_by() > 0;
        if ($userExisted) {
            $this->form_validation->set_message('available_check', 'Số điện thoại EXT đang được sử dụng');
            return false;
        }

        return true;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function hubspot_check($value, $user_id)
    {
        if (empty($value)) {
            return true;
        }

        $this->load->model('staffs/staffs_m');

        $staff = $this->staffs_m->set_user_type()->where('user_id', $user_id)->get_by();
        if (empty($staff)) {
            return false;
        }

        $hubspotOwner = $this->get_by_email_hubspot_owners($staff->user_email);
        if (empty($hubspotOwner)) {
            $this->form_validation->set_message('hubspot_check', 'Không tìm thấy thông tin người dùng trên hệ thống hubspot');

            return false;
        }

        return true;
    }

    public function get_by_email_hubspot_owners($email)
    {
        $hubspotOwners = $this->get_hubspot_owners();
        if (empty($hubspotOwners)) return false;

        $owner = array_filter($hubspotOwners, function ($item) use ($email) {
            return $item->email == $email;
        });
        if (empty($owner)) return false;

        return reset($owner);
    }

    public function get_hubspot_owners()
    {
        $key_cache = 'hubspot/owners';
        $hubspot_owners_cache = $this->scache->get($key_cache);

        if (!$hubspot_owners_cache) {
            $this->load->config('hubspot');
            $hubspotConfig = $this->config->item('hubspot');
            $hubspotOwnersUrl = "{$hubspotConfig['HUBSPOT_URL']}/owners/v2/owners?hapikey={$hubspotConfig['HUBSPOT_HAPIKEY']}";

            $response = Requests::get($hubspotOwnersUrl);
            if ($response->status_code != 200) return [];

            $data = json_decode($response->body);

            $this->scache->write($data, $key_cache);

            return $data;
        }

        return $hubspot_owners_cache;
    }
}
/* End of file User_m.php */
/* Location: ./application/models/User_m.php */