<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_group_m extends Term_m
{

    public $term_type = 'user_group';


    /**
     * Sets the term type.
     *
     * @return     this
     */
    public function set_term_type()
    {
        return $this->where('term_type', $this->term_type);
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function existed_check($value)
    {
        if (empty($value)) {
            return true;
        }

        $isExisted = $this->set_term_type()->where('term.term_id', (int) $value)->count_by() > 0;
        if (!$isExisted) {
            $this->form_validation->set_message('existed_check', 'Nhóm không tồn tại.');
            return false;
        }

        return true;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function department_check($value, $department_id)
    {
        if (empty($value) || !isset($department_id)) {
            return true;
        }

        $isExisted = $this->set_term_type()
            ->where('term.term_id', (int) $value)
            ->where('term.term_parent', (int) $department_id)
            ->count_by() > 0;
        if (!$isExisted) {
            $this->form_validation->set_message('department_check', 'Nhóm không thuộc phòng ban.');
            return false;
        }

        return true;
    }
}
/* End of file User_group_m.php */
/* Location: ./application/modules/staffs/models/User_group_m.php */