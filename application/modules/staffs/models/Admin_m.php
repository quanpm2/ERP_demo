<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'models/User_m.php');
use \Firebase\JWT\JWT;

class Admin_m extends User_m {

    function __construct()
    {
        parent::__construct();
        $this->setInfo();
    }

    public $before_create = array( 'set_password', 'create_timestamp');

    public $before_update = array( 'set_password' );

    public function create_timestamp($row){

        $row['user_time_create'] = time();

        return $row;
    }

    public function generateKey($user_id = 0, bool $never_expired = false)
    {
        $user_id = $user_id ?: $this->id;

        $this->load->model('role_m');
        $this->load->model('credentials_m');

        $role   = $this->role_m->get($user_id);
        $admin  = $this->set_get_admin()->limit(1)->get($user_id);
        $admin->role_name = $role->role_name ?? '';

        $token = $admin;
        $token->iat = time();
        $token->exp = $never_expired ? strtotime("+50 years", time()) : strtotime("+12 months", time());
        $token = JWT::encode( (array) $token, getenv('JWT_SECRET'), getenv('JWT_ALG'));
        return $token;
    }

    public function set_password($user)
    {
        if(!isset($user['user_pass'])) return $user;
        // $this->load->model('admin_m');
        $salt = (isset($user['user_salt']) ? $user['user_salt'] : $this->random_salt());

        $user['user_pass'] = $this->hash_pass($user['user_pass'], $salt);
        $user['user_salt'] = $salt;
        return $user;
    }

    
    /**
     * Hash password
     * @return String 
     */
    public function hash_pass($pass, $salt = '') {
        return md5(base64_encode(md5($pass.':'.$salt)).$salt);
    }

    /**
     * Random Salt
     * @return String(10)
     */
    public function random_salt($num = 10) {
        $d = base64_encode(md5(time() . rand()));
        return substr($d, 0, $num);
    }

    public function random_key($num_char = 20) {
        $act = base64_encode(md5(time() . rand()));
        return substr($act, 0, $num_char);
    }


    public function setLogin($user) {
        if (!$user) {
            return false;
        }

        $data = array(
            'user_id' => $user->user_id,
            'admin_account' => $user->user_login,
            'admin_name' => $user->display_name,
            'admin_email' => $user->user_email,
            'admin_avatar' => $user->user_avatar,
            'role_id' => $user->role_id,
            'role_name' => $user->role_name,
            'user_time_create' => $user->user_time_create,
            'isAdminLogin' => 1,
            'login_destination' => $user->login_destination ?? '',
            );
        $this->session->set_userdata($data);
        return true;
    }

    public function checkLogin() {
        return $this->session->userdata('isAdminLogin');
    }

    function logout() {
        $this->session->sess_destroy();
        return 1;
    }

    function setInfo()
    {
        if( ! $this->checkLogin() || $this->id != 0) return FALSE;

        $this->id       = $this->session->userdata('user_id');
        $this->account  = $this->session->userdata('admin_account');
        $this->name     = ($this->session->userdata('admin_name') =='')?$this->account:$this->session->userdata('admin_name');
        $this->avatar   = $this->session->userdata('admin_avatar');
        $this->role_id  = $this->session->userdata('role_id');
        $this->role_name = $this->session->userdata('role_name');
        $this->user_time_create = $this->session->userdata('user_time_create');
        $this->login_destination = $this->session->userdata('login_destination');

        return TRUE;
    }
    
    function set_get_admin()
    {
        $this->where('user.user_type','admin');
        return $this;
    }

    function set_role($roleids = ''){
        if(!empty($roleids)) 
            $this->where_in('role_id',$roleids);
        return $this;
    }

    function set_get_active($get_admin = TRUE)
    {
        if($get_admin)
            $this->set_get_admin();
        
        $this->where('user.user_status',1);
        return $this;
    }

    function get_field_by_id($admin_id, $field ='')
    {
        if(!$admin_id)
            return false;
        $key = 'user/admin-'.$admin_id;
        $result = $this->scache->get($key);

        if(!$result)
        {
            $result = $this->as_array()->get($admin_id);
            $this->scache->write($result,$key);
        }

        if($field)
        {
            return isset($result[$field]) ? $result[$field] : '';
        }

        return $result;
    }

    function deleteCache($adminID = 0)
    {
        $caches = array(
            'user/admin-'.$adminID,
            'user/'.$adminID.'_meta',
            );
        foreach($caches as $cache)
        {
            $this->scache->delete_group($cache);
        }
    }

    /**
     * Check validate baiment levels
     *
     * @param      string  $value  The value
     * @param      string  $url    The url
     *
     * @return     string  ( description_of_the_return_value )
     */
    public function bailment_level_check($value = '')
    {
        $bailment_levels = $this->config->item('bailment_levels');
        if(!in_array($value, array_keys($bailment_levels))) 
            return FALSE;
        return TRUE;
    }


    
    /**
     * Gets all by permissions.
     *
     * @param      string   $permission  The permission
     * @param      integer  $user_id     The user identifier
     *
     * @return     array    All by permissions.
     */
    public function get_all_by_permissions($permission = '', $user_id = 0)
    {
        if( ! $permission) return FALSE;

        $user_id    = $user_id ?: $this->admin_m->id;
        $role_id    = $this->get_field_by_id($user_id, 'role_id');

        if( empty($role_id)) return FALSE;

        $users      = array($user_id);

        $array_permission = explode('.', $permission);
        $action = '';
        if(count($array_permission) == 3)
        {
            $action =  array_pop($array_permission);
        }

        $permission = implode('.', $array_permission);

        // CHECK PERMISSION OF LOGGED USER
        if( ! $this->permission_m->has_permission("{$permission}.{$action}", $role_id)) return FALSE;
        
        // LOGGED USER IS ADMINISTRATORS
        if($this->permission_m->has_permission("{$permission}.manage", $role_id)) return TRUE;

        $model_names = ['term_users_m','staffs/department_m','staffs/user_group_m'];
        $this->load->model($model_names);


        // LOGGED USER IS HEAD OF DEPARMENT
        if($this->permission_m->has_permission("{$permission}.mdeparment", $role_id))
        {
            $departments = $this->term_users_m->get_user_terms($user_id, $this->department_m->term_type);
            if(empty($departments)) return $users;

            foreach ($departments as $department)
            {
                $user_groups = $this->department_m->get_groups($department->term_id);
                if(empty($user_groups)) continue;

                foreach ($user_groups as $user_group)
                {
                    $_users = $this->term_users_m->get_term_users($user_group->term_id, 'admin');
                    if(empty($_users)) continue;

                    $users = array_merge($users, array_column($_users, 'user_id'));
                }
            }

            return array_unique($users);
        }

        // LOGGED USER IS LEADER OF GROUP
        if($this->permission_m->has_permission("{$permission}.mgroup", $role_id))
        {
            $user_groups = $this->term_users_m->get_user_terms($user_id, $this->user_group_m->term_type);
            if(empty($user_groups)) return $users;

            foreach ($user_groups as $user_group)
            {
                $_users = $this->term_users_m->get_term_users($user_group->term_id, 'admin');
                if(empty($_users)) continue;

                $users = array_merge($users, array_column($_users, 'user_id'));
            }
            
            return array_unique($users);
        }

        // LOGGED USER IS ACCESS MEMBER IN ALONE ROLE
        return $users;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function existed_check($value, $role_option = '')
    {
        if (empty($value)) {
            return true;
        }

        $query = $this->set_get_admin()
            ->set_get_active()
            ->where('user_id', $value);

        if (!empty($role_option)) {
            $role_ids = $this->option_m->get_value($role_option, TRUE);
            $query->where_in('role_id', $role_ids);
        };

        $isExisted = $query->count_by() > 0;
        if (!$isExisted) {
            $this->form_validation->set_message('existed_check', 'Nhân viên không tồn tại.');
            return false;
        }

        return true;
    }
}
/* End of file Admin_m.php */
/* Location: ./application/modules/staffs/models/Admin_m.php */