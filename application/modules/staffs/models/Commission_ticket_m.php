<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'models/Post_m.php');

class Commission_ticket_m extends Post_m
{
	public $commission_level	= 'direct';
	public $post_type 			= 'commission_ticket';
	public $before_create 		= array('init_data');
	public $before_get			= array('set_post_type','set_post_slug');

	function __construct() 
	{	
		parent::__construct();

		$this->load->model('staffs/sale_m');
		$this->load->model('commission_m');
		$this->load->model('contract/contract_m');

		$this->load->config('contract/contract');
		$this->load->config('staffs/staffs');
	}

	protected function set_post_type()
	{
		$this->where('posts.post_type', $this->post_type);
        return $this;
    }

    protected function set_post_slug()
    {
        $this->where('posts.post_slug', $this->commission_level);
        return $this;
    }

	protected function init_data($row)
	{
		$row['post_type'] 	= $row['post_type'] ?? $this->post_type;
		$row['created_on'] 	= $row['created_on'] ?? time();
		return $row;
	}

	public function get_commission_level()
	{
		return $this->commission_level;
	}

	public function set_commission_level($value = '')
	{
		if(empty($value)) return FALSE;

		if( ! $this->config->item($value, 'commission_levels')) return FALSE;

		$this->commission_level = $value;
	}

	
	public function get_commission_result($args = array())
	{
		$default = array(   
            'data_type' => 'all',
            'term_type' => NULL,
            'term_id'   => 0,
            'user_id'   => 0,
            'start_time' => $this->mdate->startOfMonth(),
            'end_time'   => $this->mdate->endOfDay()
        );

        $args = wp_parse_args($args, $default);

        $encrypt_key    = md5(json_encode($args));
        $cache_key      = "modules/mbusiness/sales_{$args['user_id']}_sale_commission_{$encrypt_key}";
        $result         = $this->scache->get($cache_key);

        if( ! $result || 1==1)
        {
            $result     = array();
            $receipts   = $this->commission_m->get_receipts_paid(0, $args['user_id'],$args['start_time'],$args['end_time']);
            if(empty($receipts)) return FALSE;

            $term_ids   = array_group_by($receipts,'term_id');

            $terms      = $this->contract_m->set_term_type()->as_array()->get_many_by('term_id', array_keys($term_ids));
            $terms      = array_column($terms, NULL, 'term_id');

            foreach ($terms as &$term)
            {
                $term['contract_value'] = get_term_meta_value($term['term_id'], 'contract_value');
                $term['contract_code']  = get_term_meta_value($term['term_id'], 'contract_code');

                $start_time = get_term_meta_value($term['term_id'], 'start_service_time') ?: get_term_meta_value($term['term_id'], 'contract_begin');

                $term['start_time'] = $start_time;
                $term['start_date'] = my_date($start_time, 'd/m/Y H:i:s');

                $this->commission_m->set_instance($term['term_id']);
                $commission_m               = $this->commission_m->get_instance();
                $term['actually_collected'] = $commission_m->get_actually_collected($term['term_id'], $args['user_id'], $args['start_time'],$args['end_time']);

                $term['amount_kpi'] = $commission_m->get_amount_kpi($term['term_id'], $args['user_id'], $args['start_time'],$args['end_time']);

                $term['vat']            = get_term_meta_value($term['term_id'], 'vat');
                $term['amount']         = $commission_m->get_amount($term['term_id'], $args['user_id'], $args['start_time'],$args['end_time']);
                $term['amount_not_vat'] = $commission_m->get_amount_not_vat($term['term_id'], $args['user_id'], $args['start_time'],$args['end_time']);
                $term['receipts'] = array_column($term_ids[$term['term_id']], 'post_id');

                $commission_type = 'webdoctor';
                switch ($term['term_type'])
                {
                    case 'domain':
                        $commission_type = 'none';
                        break;

                    case 'google-ads':
                        $commission_type = $commission_m->get_commission_package();
                        break;

                    case 'facebook-ads':
                        $commission_type = 'account_type';
                        break;
                    
                    default:
                        $commission_type = 'webdoctor';
                        break;
                }

                $term['commission_type'] = $commission_type;
            }



            $result['receipts']     = $receipts;
            $result['terms']        = $terms;
            $result['term_types']   = array_group_by($terms, 'term_type');
            $result['term_commission_types'] = array_group_by($terms, 'commission_type');

            /* Calculate overall */
            $result['overall'] = array();

            foreach ( $result['term_commission_types'] as $commission_type => $items)
            {
                $amount_kpi         = array_sum(array_column($items, 'amount_kpi')); // Doanh thu tính thưởng
                $amount_rate_based  = $amount_kpi; // Doanh thu dùng để xác định tỉ lệ phần hoa hồng

                // Phần trăm hoa hồng của adsplus sẽ dựa vào tổng doanh thu tính thưởng của adsplus + webdoctor
                if(in_array($commission_type, array('account_type','cpc_type')))
                {
                    $_terms_valid = array_filter($terms, function($x){ return in_array($x['commission_type'],['account_type','cpc_type','webdoctor']);});
                    $amount_rate_based = array_sum(array_column($_terms_valid, 'amount_kpi'));
                }

                $rate = $this->get_commission_rate($amount_rate_based, $commission_type);

                $result['overall'][$commission_type] = array(
                    'rate' => $rate,

                    'actually_collected'    => array_sum(array_column($items, 'actually_collected')),
                    'amount_commisssion'    => $amount_kpi * $rate,
                    
                    'contract_value'    => array_sum(array_column($items, 'contract_value')),
                    'amount_not_vat'    => array_sum(array_column($items, 'amount_not_vat')),
                    'amount_kpi'        => array_sum(array_column($items, 'amount_kpi')),
                    'contract_num'      => count($items)
                );
                
                array_sum(array_column($result['overall'], 'amount_commisssion'));
            }

            $result['overall']['contract_value']        = array_sum(array_column($result['overall'], 'contract_value'));
            $result['overall']['amount_not_vat']        = array_sum(array_column($result['overall'], 'amount_not_vat'));
            $result['overall']['amount_commisssion']    = array_sum(array_column($result['overall'], 'amount_commisssion'));
            $result['overall']['actually_collected']    = array_sum(array_column($result['overall'], 'actually_collected'));
            $result['overall']['amount_kpi']            = array_sum(array_column($result['overall'], 'amount_kpi'));

            $result['overall']['contract_num']  = count($result['terms']);

            $result['overall']['adsplus']       = array(                
                'actually_collected' => ($result['overall']['account_type']['actually_collected']??0) + ($result['overall']['cpc_type']['get_actually_collected']??0),

                'amount_kpi' => ($result['overall']['account_type']['amount_kpi']??0) + ($result['overall']['cpc_type']['get_amount_kpi']??0),

                'amount_commisssion' => ($result['overall']['account_type']['amount_commisssion']??0) + ($result['overall']['cpc_type']['amount_commisssion']??0),

                'amount_not_vat' => ($result['overall']['account_type']['amount_not_vat']??0) + ($result['overall']['cpc_type']['amount_not_vat']??0),

                'contract_num' => ($result['overall']['account_type']['contract_num']??0) + ($result['overall']['cpc_type']['contract_num']??0),

                'contract_value' => ($result['overall']['account_type']['contract_value']??0) + ($result['overall']['cpc_type']['contract_value']??0),
            );

            // Tiến hành gán % hoa hồng cho hợp đồng sau khi tính được % hoa hồng của từng loại hoa hồng (từng dịch vụ)
            foreach ($result['terms'] as &$term)
            {
                $rate = $result['overall'][$term['commission_type']]['rate'] ?? 0;
                $term['rate'] = $rate;
                $term['amount_commisssion'] = $rate * $term['amount_kpi'];
            }

            $this->scache->write($result, $cache_key, 500);
        }

        if($args['data_type'] == 'all') return $result;

        if(empty($result[$args['data_type']])) return NULL;

        return $result[$args['data_type']];
	}


	/**
     * Gets the commission rate.
     *
     * @param      integer  $amount  The amount
     * @param      string   $type    The type
     *
     * @return     integer  The commission rate.
     */
    public function get_commission_rate($amount = 0, $type = 'none')
    {
        $rules = $this->config->item($type, 'commission');

        if( ! $rules) return 0;

        if( is_numeric($rules) || is_string($rules)) return $rules;

        if( ! is_array($rules)) return 0;

        $rate = 0;

        foreach ($rules as $rule)
        {
            if($amount < $rule['min'] || $amount > $rule['max']) continue;

            $rate = $rule['rate'];
            break;
        }

        return $rate;
    }
}
/* End of file Commission_ticket_m.php */
/* Location: ./application/modules/staffs/models/Commission_ticket_m.php */