<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'modules/staffs/models/Commission_ticket_m.php');

class Commission_ticket_direct_m extends Commission_ticket_m
{
	public $commission_level = 'direct';
}
/* End of file Commission_ticket_direct_m.php */
/* Location: ./application/modules/staffs/models/Commission_ticket_direct_m.php */