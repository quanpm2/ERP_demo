<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'models/User_m.php');

class Staffs_m extends User_m
{

    function __construct()
    {
        parent::__construct();
    }

    public function set_user_type()
    {
        return $this->where('user_type', 'admin');
    }

    /**
     * Updated a record based on the primary value.
     */
    public function update($primary_value, $data, $skip_validation = FALSE)
    {
        $previous   = $this->as_array()->get($primary_value);
        $result     = parent::update($primary_value, $data, $skip_validation);

        if (empty($result)) return $result;

        $changed_values = array_filter($data, function ($v, $k) use ($previous) {
            return $previous[$k] != $v;
        }, ARRAY_FILTER_USE_BOTH);

        if (empty($changed_values)) return $result;

        $records = array();
        array_walk($changed_values, function ($value, $key) use (&$records, $primary_value, $previous) {
            $records[] = [
                'event' => 'updated',
                'type' => $this->_table,
                'id' => $primary_value,
                'field' => $key,
                'new' => $value,
                'old' => $previous[$key] ?? '',
            ];
        });

        audit($records);

        return $result;
    }

    public function callback($data = NULL, $columns = array())
    {
        if (empty($data)) return $data;
        $this->load->config('staffs/staffs');
        $columns         = $columns ?: $this->config->item('default_columns', 'datasource');
        $dats_columns     = $this->config->item('columns', 'datasource');

        /* Get all metadata columns for this data */
        $columns     = array_filter($dats_columns, function ($x) use ($columns) {
            return in_array($x['name'], $columns);
        });
        $meta_cols     = $columns ? array_group_by($columns, 'type') : [];
        if (!empty($meta_cols['meta'])) {
            $_umetadatas = get_user_meta_value($data['user_id']);
            $_umetadatas = $_umetadatas ? key_value($_umetadatas, 'meta_key', 'meta_value') : [];
            foreach ($meta_cols['meta'] as $_mcol) {
                $data[$_mcol['name']] = $_umetadatas[$_mcol['name']] ?? '';
            }
        }

        return $data;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function existed_check($value)
    {
        $isExisted = $this->set_user_type()->where('user.user_id', (int) $value)->count_by() > 0;
        if (!$isExisted) {
            $this->form_validation->set_message('existed_check', 'Mã nhân viên không tồn tại.');
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function user_phone_check($value, $user_id)
    {
        if (empty($value)) {
            return TRUE;
        }

        $isExisted = $this->set_user_type()
            ->join('usermeta', 'usermeta.user_id = user.user_id', 'left')
            ->where('usermeta.meta_key', 'user_phone')
            ->where('usermeta.meta_value', $value)
            ->where('user.user_id !=', (int) $user_id)
            ->count_by() > 0;

        if ($isExisted) {
            $this->form_validation->set_message('user_phone_check', 'Số công việc đã được sử dụng.');

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @param string $value
     * 
     * @return [type]
     */
    public function user_phone_private_check($value, $user_id)
    {
        if (empty($value)) {
            return TRUE;
        }

        $isExisted = $this->set_user_type()
            ->join('usermeta', 'usermeta.user_id = user.user_id', 'left')
            ->where('usermeta.meta_key', 'user_phone_private')
            ->where('usermeta.meta_value', $value)
            ->where('user.user_id !=', (int) $user_id)
            ->count_by() > 0;

        if ($isExisted) {
            $this->form_validation->set_message('user_phone_private_check', 'Số di động đã được sử dụng.');

            return FALSE;
        }

        return TRUE;
    }
}
