<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'modules/staffs/models/Admin_m.php');

class Sale_m extends Admin_m {

    protected $role_ids = [];

    function __construct()
    {
        parent::__construct();

        $this->init();
    }

    public function init()
    {
        $this->role_ids = $this->option_m->get_value('group_sales_ids', TRUE) ?: [];
        return $this;
    }

    public function set_role($role_ids = array())
    {
        if(empty($role_ids)) $role_ids = $this->role_ids;

        return $this->where_in('role_id',$role_ids);
    }


    /**
     * Gets the roles.
     *
     * @return     Array  The roles.
     */
    public function get_roles()
    {
        return $this->role_ids;
    }

    public function set_user_type()
    {
        return $this->where('user_type','admin');
    }


    public function get_bailment_used($user_id = 0)
    {
        $result = $this
        ->select('user.user_id,user.display_name')
        ->select('COUNT(DISTINCT term.term_id) AS "real_quantity"')
        ->select('SUM(postmeta.meta_value)*COUNT(DISTINCT posts.post_id)/COUNT(postmeta.post_id) AS "real_budget"')

        ->join('term_users','term_users.user_id = user.user_id')
        ->join('term','term.term_id = term_users.term_id')
        ->join('term_posts','term.term_id = term_posts.term_id')
        ->join('posts','posts.post_id = term_posts.post_id')
        ->join('postmeta', 'postmeta.post_id = posts.post_id AND postmeta.meta_key = "amount"','LEFT')

        ->where('user.user_type','admin')
        ->where('posts.post_type','receipt_caution')
        ->where('posts.post_status','publish')
        ->where('user.user_id',$user_id)
        ->where_in('term.term_status',['waitingforapprove','pending','publish'])
        ->group_by('user.user_id')
        ->get_by();

        if( ! $result) return array('budget' => 0,'quantity' => 0);
        return array('budget' => (int)$result->real_budget,'quantity' => $result->real_quantity);
    }


    /**
     * Check validate baiment levels
     *
     * @param      string  $value  The value
     * @param      string  $url    The url
     *
     * @return     string  ( description_of_the_return_value )
     */
    public function bailment_level_check($value = '')
    {
        $bailment_levels = $this->config->item('bailment_levels');
        if(!in_array($value, array_keys($bailment_levels))) 
            return FALSE;
        return TRUE;
    }

    /**
     * @param int $value
     * 
     * @return boolean
     */
    public function staff_business_check($value)
    {
		if(empty($value)) return true;

        $isExisted = $this->set_user_type()->set_role()
            ->where('user.user_id', (int) $value)
            ->select('user.user_id')
            ->count_by() > 0;
        if( ! $isExisted)
        {
            $this->form_validation->set_message('staff_business_check', 'Nhân viên kinh doanh không tồn tại');
            return false;
        }
        
        return true;
    }
}
/* End of file Sale_m.php */
/* Location: ./application/modules/staffs/models/Sale_m.php */