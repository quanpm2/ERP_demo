<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$this->template->javascript->add('vendors/vuejs/vue.min.js');
$this->template->javascript->add(base_url('node_modules/axios/dist/axios.min.js'));
$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js');

$this->template->stylesheet->add('plugins/datatables/dataTables.bootstrap.css');
$this->template->stylesheet->add('plugins/datatables/extensions/FixedHeader/css/dataTables.fixedHeader.css');
$this->template->stylesheet->add('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css');
$this->template->stylesheet->add('plugins/datatables/extensions/Scroller/css/dataTables.scroller.css');
$this->template->javascript->add('plugins/datatables/jquery.dataTables.min.js');
$this->template->javascript->add('plugins/datatables/dataTables.bootstrap.min.js');
$this->template->javascript->add('plugins/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js');
$this->template->javascript->add('plugins/datatables/extensions/KeyTable/js/dataTables.keyTable.min.js');
$this->template->javascript->add('plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js');
$this->template->javascript->add('plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js');
?>

<ugroups-datatable-component />

<?php
$version = '201805031148';
echo $this->template->trigger_javascript(admin_theme_url("modules/component/form.js?v={$version}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/staffs/js/remove-modal-component.js?v={$version}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/staffs/js/management-members-component.js?v={$version}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/staffs/js/ugroups-crud-component.js?v={$version}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/staffs/js/ugroups-datatable-component.js?v={$version}"));
echo $this->template->trigger_javascript(admin_theme_url("modules/staffs/js/app.js?v={$version}"));
/* End of file groups.php */
/* Location: ./application/modules/staffs/views/admin/groups.php */