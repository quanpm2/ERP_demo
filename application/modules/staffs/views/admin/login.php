<!DOCTYPE html>
<html>

<?php echo $this->template->header->view('include/header');?>
  <body class="login-page">
  <script type="text/javascript">
  var base_url ="<?php echo base_url();?>";
function randomBackground () {

  var backgrounds = ['background-1.jpg','background-2.jpg','background-5.jpg','#847257','#28BB9B'];

  background = backgrounds[Math.floor(Math.random() * backgrounds.length)];
  if(background.substring(0, 1) != '#')
  {
    background = base_url+'template/admin/img/background/' + background;
    $('body').css('background','url('+background+')  no-repeat center center fixed');
  }
  else
  {
    $('body').css('background',background);
  }
}
randomBackground();
</script>
    <div class="login-box">
      <div class="login-logo">
        <a href="<?php echo current_url();?>"><img data-wow-delay="0.3s" class="wow fadeInUp animated" src="<?php echo admin_theme_url();?>img/logo.svg" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp; width: 100%;"></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form action="<?php echo admin_url('staffs/sign_in');?>" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Username" name="username"/>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">    
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->

            
          </div>
        </form>
        <?php
            $href = $authUrl ?: $this->google_auth->authUrl;

            if(!empty($href)):

            ?>
            <div class="social-auth-links text-center">
              <p class="login-box-msg">-- HOẶC --</p>
              <a href="<?php echo $href; ?>" class="btn btn-block btn-social btn-google btn-flat">
                <i class="fa fa-google-plus"></i>Đăng nhập ngay  </a>
            </div><!-- /.social-auth-links -->
            <?php

            endif;

            ?>

            <div class="text-center" style="margin-top: 16px">
                <a href="https://adsplus.vn/chinh-sach-bao-mat-thong-tin-adsplus/" target="_blank">
                    <u>Chính sách bảo mật thông tin</u>
                </a>
            </div>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <?php  ?>
  
     <script type="text/javascript">
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>

    <?php  //echo $this->template->footer; ?>
    <?php

      $_messages = $this->messages->get();

      if(isset($_messages) && $_messages){ 

    ?>
    <script type="text/javascript">
    <?php

      foreach($_messages as $key => $messages) { 

        foreach($messages as $message){

    ?>
    $.notify("<?php echo $message;?>", "<?php echo $key;?>");
    <?php 
        }
      }
    ?>
    </script>
    <?php 
      } 
    ?>