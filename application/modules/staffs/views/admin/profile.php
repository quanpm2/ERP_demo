<div class="row">
  <div class="col-md-3">
    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="<?php echo image($profile->user_avatar);?>" alt="User profile picture">
        <h3 class="profile-username text-center"><?php echo $profile->display_name;?></h3>
        <p class="text-muted text-center"><?php echo $profile->user_type;?></p>
        <p class="text-muted text-center" style="word-wrap: break-word;"><?php echo $keyCredential;?></p>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div><!-- /.col -->
  <div class="col-md-9">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab" aria-expanded="true">Thông tin</a></li>
        <?php
        // <li><a href="#timeline" data-toggle="tab" aria-expanded="true">Hoạt động</a></li>
        ?>
        <li><a href="#layout_options" data-toggle="tab" aria-expanded="true">Tùy chỉnh giao diện</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="settings">
          <div class="row">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Tài khoản</label>
              <div class="form-group col-md-10">
                <div class="input-group col-sm-12">
                  <?php echo $profile->user_login;?>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">E-mail</label>
              <div class="form-group col-md-10">
                <div class="input-group col-sm-12">
                  <?php echo $profile->user_email;?>
                </div>
              </div>
            </div>
            <?php

            $user_id = $profile->user_id;

            echo $this->admin_form->input('Họ và tên','',$profile->display_name,'',array('id'=>'x_input_name','data-original-title' =>  'Tên hiển thị','data-pk'=>$user_id,'data-type-data'=>'field','data-name'=>'display_name','is_x_editable'=>'true','data-ajax_call_url'=>'staffs/ajax_edit/' . $user_id));

            echo $this->admin_form->input(
                    'SĐT cá nhân',
                    'user_phone_private',
                    get_user_meta_value($user_id,'user_phone_private',TRUE,TRUE),
                    '',
                    array('id'=>'x_input_phone_private','data-original-title' =>  'Số điện thoại','data-pk'=>$user_id,'data-type-data'=>'meta_data','data-name'=>'user_phone_private','is_x_editable'=>'true','data-ajax_call_url'=>"staffs/ajax_edit/{$user_id}")
                );

            echo $this->admin_form->input(
                    'SĐT công việc',
                    'user_phone',
                    get_user_meta_value($user_id,'user_phone',TRUE,TRUE),
                    'Số điện thoại sẽ được hiển thị trên hệ thống website WEBDOCTOR , ADSPLUS để nhận cuộc gọi từ khách hàng',
                    array('id'=>'x_input_phone','data-original-title' =>  'Số điện thoại','data-pk'=>$user_id,'data-type-data'=>'meta_data','data-name'=>'user_phone','is_x_editable'=>'true','data-ajax_call_url'=>"staffs/ajax_edit/{$user_id}")
                );
            
            $gender     = get_user_meta_value($user_id, 'gender');
            $gender_f   = !empty($gender) ? 'Nam' : 'Nữ';
            echo $this->admin_form->input('Giới tính','',$gender_f,'',array('data-original-title' =>  'Giới tính','data-pk'=>$user_id,'data-type-data'=>'meta_data','data-name'=>'gender','data-type'  =>  'select','data-source'  =>  '[{value: \'1\', text: \'Nam\'}, {value: \'0\', text: \'Nữ\'}]','is_x_editable'=>'true','data-ajax_call_url'=>'staffs/ajax_edit/' . $user_id));

            $is_profile_refresh     = (int) get_user_meta_value($user_id, 'is_profile_refresh');
            $is_profile_refresh_f   = !empty($is_profile_refresh) ? 'Có' : 'Không' ;

            echo $this->admin_form->input('Tự động lấy mới hình ảnh','',$is_profile_refresh_f,'',array('data-original-title' =>  'Giới tính','data-pk'=>$user_id,'data-type-data'=>'meta_data','data-name'=>'is_profile_refresh','data-type'  =>  'select','data-source'  =>  '[{value: \'1\', text: \'Có\'}, {value: \'0\', text: \'Không\'}]','is_x_editable'=>'true','data-ajax_call_url'=>'staffs/ajax_edit/' . $user_id));

            ?>
            <div class="clearfix"></div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nhóm</label>
              <div class="form-group col-md-10">
                <div class="input-group col-sm-12">
                    <?php
                    echo $role->role_name;
                    ?>
                </div>
              </div>
            </div>

          </div> 
        </div><!-- /.tab-pane -->
        
        <div class="tab-pane" id="layout_options">
          <?php 
          echo $this->admin_form->form_open(admin_url().'staffs/setting_options', array('method'=>'post'));
          ?>
          <div>
            <h4 class="control-sidebar-heading">Cấu hình Layout</h4>
            <div class="form-group">
                <label class="control-sidebar-subheading">
                <?php 
                echo form_input(array('type'=>'checkbox','class'=>'pull-right','name'=>'edit_opts[fixed]','data-layout'=>'fixed')) . '<b>Fixed layout</b>';
                ?>
                </label>
                <p>Activate the fixed layout. You can't use fixed and boxed layouts together</p>
            </div>
            <div class="form-group">
                <label class="control-sidebar-subheading">
                <?php 
                echo form_input(array('type'=>'checkbox','class'=>'pull-right','name'=>'edit_opts[layout-boxed]','data-layout'=>'layout-boxed')) . '<b>Boxed layout</b>';
                ?>
                </label>
                <p>Activate the boxed layout</p>
            </div>
            <div class="form-group">
                <label class="control-sidebar-subheading">
                <?php 
                echo form_input(array('type'=>'checkbox','class'=>'pull-right','name'=>'edit_opts[sidebar-collapse]','data-layout'=>'sidebar-collapse')) . '<b>Toggle Sidebar</b>';
                ?>
                <p>Toggle the left sidebar's state (open or collapse)</p>
            </div>
            <div class="form-group">
                <label class="control-sidebar-subheading">
                <?php 
                echo form_input(array('type'=>'checkbox','class'=>'pull-right','name'=>'edit_opts[expandOnHover]','data-enable'=>'expandOnHover')) . '<b>Sidebar Expand on Hover</b>';
                ?>
                </label>
                <p>Let the sidebar mini expand on hover</p>
            </div>
            <div class="form-group">
                <label class="control-sidebar-subheading">
                <?php 
                echo form_input(array('type'=>'checkbox','class'=>'pull-right','name'=>'edit_opts[control-sidebar-open]','data-controlsidebar'=>'control-sidebar-open')) . '<b>Toggle Right Sidebar Slide</b>';
                ?>
                </label>
                <p>Toggle between slide over content and push content effects</p>
            </div>
            <div class="form-group">
                <label class="control-sidebar-subheading">
                <?php 
                echo form_input(array('type'=>'checkbox','class'=>'pull-right','name'=>'edit_opts[toggle]','data-sidebarskin'=>'toggle')) . '<b>Toggle Right Sidebar Skin</b>';
                ?>
                </label>
                <p>Toggle between dark and light skins for the right sidebar</p>
            </div>
            <br/>
            <?php
            echo $this->admin_form->submit( array('type'=>'submit','name'=>'submit','class'=>'btn btn-info setting_options'),'Save changes');
            ?>
            <br/>
            <h4 class="control-sidebar-heading">Skins</h4>
            <ul class="list-unstyled clearfix">
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-blue" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9;"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin">Blue</p>
                </li>
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-black" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe;"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #222;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin">Black</p>
                </li>
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-purple" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin">Purple</p>
                </li>
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-green" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin">Green</p>
                </li>
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-red" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin">Red</p>
                </li>
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-yellow" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin">Yellow</p>
                </li>
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-blue-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9;"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin" style="font-size: 12px">Blue Light</p>
                </li>
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-black-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe;"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin" style="font-size: 12px">Black Light</p>
                </li>
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-purple-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin" style="font-size: 12px">Purple Light</p>
                </li>
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-green-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin" style="font-size: 12px">Green Light</p>
                </li>
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-red-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin" style="font-size: 12px">Red Light</p>
                </li>
                <li style="float:left; width: 33.33333%; padding: 5px;">
                    <a href="javascript:void(0);" data-skin="skin-yellow-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                        <div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span>
                        </div>
                        <div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                        </div>
                    </a>
                    <p class="text-center no-margin" style="font-size: 12px;">Yellow Light</p>
                </li>
            </ul>
          </div>
          <?php 
          echo '<br/>';

          echo $this->admin_form->hidden('','body_class');
          echo $this->admin_form->hidden('','sidebar_class');
          echo $this->admin_form->submit( array('type'=>'submit','name'=>'submit','class'=>'btn btn-info setting_options'),'Save changes');

          echo $this->admin_form->form_close();
          ?>
        </div><!-- /.tab-pane -->
      </div><!-- /.tab-content -->
    </div><!-- /.nav-tabs-custom -->
  </div><!-- /.col -->
</div>

<script type="text/javascript">
  $(function(){
    
    $("#x_input_name").editable('option', 'validate', function(value) {
      if(!value) return 'Vui lòng nhập tên hiển thị !';
    });

    $("#x_input_phone").editable('option', 'validate', function(value) {
      if(!value) return 'Vui lòng nhập số điện thoại !';
      if($.isNumeric(value) != true) return 'Số điện thoại không hợp lệ';
    });

    var ajax_edit_url = '<?php echo admin_url();?>staffs/ajax_edit/<?php echo $profile->user_id;?>';

    $(".setting_options").click(function(){

      $.post( ajax_edit_url, 
        { data : [{'pk':<?php echo $profile->user_id;?>,'type':'meta_data','name':'user_body_class','value':$('body').attr("class")},{'pk':<?php echo $profile->user_id;?>,'type':'meta_data','name':'user_sidebar_class','value':$(".control-sidebar").attr('class')}] }, 
        function( resp ) {
          $.notify(resp.msg, "");
      }, "json");

      return false;
    })
  });
</script>