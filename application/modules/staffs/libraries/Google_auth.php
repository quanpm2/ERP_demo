<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// require_once APPPATH . 'third_party/google-api/autoload.php';
// include_once APPPATH . 'third_party/google-api/Client.php';
// include_once APPPATH . 'third_party/google-api/Auth/OAuth2.php';

class Google_auth {

	public $client;
	public $objOAuthService;
	public $authUrl;

	function __construct()
    {
        $CI =& get_instance();
        $CI->load->config('staffs/auth_google');

        $this->client = new Google_Client();
        $this->client->setApplicationName('Sign In with google account');
        $this->client->setClientId($CI->config->item('id', 'google-client'));
        $this->client->setClientSecret($CI->config->item('secret', 'google-client'));
        $this->client->setRedirectUri(admin_url($CI->config->item('redirect_uri', 'google-client'), 'http'));
        $this->client->addScope($CI->config->item('scope', 'google-client'));
        $this->objOAuthService = new Google_Service_Oauth2($this->client);
        $this->authUrl = $this->client->createAuthUrl();
	}
}