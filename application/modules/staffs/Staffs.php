<?php
class Staffs_Package extends Package
{
	public function name()
	{
		return 'Admin';
	}

	public function init()
	{
		if (has_permission('Admin.Staff') && is_module_active('staffs')) {
			$this->menu->add_item(array(
				'id' => 'staffs',
				'name' => 'Nhân sự',
				'parent' => null,
				'slug' => admin_url('staffs/admin_list'),
				'order' => 1,
				'icon' => 'fa fa-users'
			), 'left');
		}

		if (!is_module('staffs')) return FALSE;

		$order = 2;
		if (has_permission('Admin.Staff')) {
			$this->menu->add_item(array(
				'id' => 'staffs-groups',
				'name' => 'Nhóm',
				'parent' => null,
				'slug' => admin_url('staffs/groups'),
				'order' => $order++,
				'icon' => 'fa fa-users'
			), 'left');

			$this->menu->add_item(array(
				'id' => 'staffs-departments',
				'name' => 'Phòng',
				'parent' => null,
				'slug' => admin_url('staffs/departments'),
				'order' => $order++,
				'icon' => 'fa fa-building'
			), 'left');
		}
	}

	public function title()
	{
		return 'Nhân viên';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Admin';
	}

	private function _update_permissions()
	{
		$permissions = $this->init_permissions();
		if (!$permissions) return FALSE;

		foreach ($permissions as $name => $value) {
			$this->permission_m->add($name, $value['actions'], $value['description']);
		}
	}

	private function init_permissions()
	{
		$permissions = array();
		$permissions['Admin.Staffs'] = array(
			'description' => 'Quản lý nhân lực',
			'actions' => array('view', 'add', 'edit', 'delete', 'update')
		);

		$permissions['Admin.User_group'] = array(
			'description' => 'Quản lý nhóm làm việc',
			'actions' => array('view', 'add', 'edit', 'delete', 'update', 'manage')
		);

		$permissions['Admin.Department'] = array(
			'description' => 'Quản lý phòng ban',
			'actions' => array('view', 'add', 'edit', 'delete', 'update', 'manage')
		);

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if (!$permissions) return false;

		foreach ($permissions as $name => $value) {
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if (!$permissions)
			return false;
		foreach ($permissions as $name => $value) {
			$this->permission_m->delete_by_name($name);
		}
	}
}
