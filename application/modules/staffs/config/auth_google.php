<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['google-client'] = array(

	'id' 			=> getenv('OAUTH_CLIENT_ID') ?: '829526618097-jtc8t3oh1cr4pg45pf3jiapn9s2u2947.apps.googleusercontent.com',
	'secret' 		=> getenv('OAUTH_CLIENT_SECRET') ?: 'VzFwIod_vf2RP8VX_1XHbfmk',
	'redirect_uri' 	=> getenv('OAUTH_REDIRECT_URI') ?: 'admin/oauth_login',
	'scope' 		=> getenv('OAUTH_SCOPE') ?: 'https://www.googleapis.com/auth/userinfo.email',
);