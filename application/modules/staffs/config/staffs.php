<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['user_status'] = array(
	'1' => 'Hoạt động', 
	'-1' =>'Không hoạt động', 
	'0'=>'Khóa', 
);

$config['user_gender'] = array(
    '0' => 'Nữ',
	'1' => 'Nam',
);

$config['bailment_levels'] = array(
	'level_0' => ['name'=>'level_0', 'text'=>'Không bảo lãnh', 'budget'=>0, 'quantity'=>0, 'max_day'=>7],
	'level_1' => ['name'=>'level_1', 'text'=>'Nhân viên chính thức KD', 'budget'=>18000000, 'quantity'=>3, 'max_day'=>7],
	'level_2' => ['name'=>'level_2', 'text'=>'Trưởng nhóm/Chuyên gia KD', 'budget'=>36000000, 'quantity'=>6, 'max_day'=>7],
	'level_3' => ['name'=>'level_3', 'text'=>'Trưởng phòng/Phó phòng KD', 'budget'=>80000000, 'quantity'=>50, 'max_day'=>7]
);

$config['commission_levels'] = array(
	'direct' => 'Trực tiếp',
	'indirect' => 'Gián tiếp (hoa hồng trưởng nhóm)'
);


$columns = array(
	'user_id' => array(
		'name' => 'user.user_id',
		'label' => 'id',
		'filter' => array(
			'type' => 'string',
		),
		'type' => 'field',
	),
	'user_email' => array(
		'name' => 'user.user_email',
		'label' => 'Email khách hàng',
		'filter' => array(
			'type' => 'string',
		),
		'type' => 'field',
	),

	'display_name' => array(
		'name' => 'user.display_name',
		'label' => 'Họ tên',
		'filter' => array(
			'type' => 'string',
		),
		'type' => 'field',
	),

	'user_status' => array(
		'name' => 'user.user_status',
		'label' => 'Trạng thái lead',
		'filter' => array(
			'type' => 'select',
			'multiple' => TRUE,
		),
		'type' => 'field',
	),

	'user_time_create' => array(
		'name' => 'user_time_create',
		'label' => 'Ngày tạo',
		'filter' => array(
			'type' => 'daterange',
			'multiple' => TRUE,
		),
		'type' => 'field',
	),

	'role_id' => array(
		'name' => 'role_id',
		'label' => 'role_id',
		'filter' => array(
			'type' => 'daterange',
			'multiple' => TRUE,
		),
		'type' => 'field',
	),

	'user_login' => array(
		'name' => 'user_login',
		'label' => 'user_login',
		'filter' => array(
			'type' => 'daterange',
			'multiple' => TRUE,
		),
		'type' => 'field',
	),

	'user_avatar' => array(
		'name' => 'user_avatar',
		'label' => 'user_avatar',
		'filter' => array(
			'type' => 'daterange',
			'multiple' => TRUE,
		),
		'type' => 'field',
	),
);

$config['datasource'] = array(
	'columns' => $columns,
	'default_columns' => ['display_name','user_status','user_time_create','user_email','role_id','user_login','user_avatar']
);
unset($columns);