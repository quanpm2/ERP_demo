<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Enum Static Config's Groups IDs  */
$config['groups'] = array(
	'manager_role_id' 	=> 8,
	'leader_role_id' 	=> 13,
	'salesexecutive'	=> [8, 9, 13, 18],
	'webdoctor-member' 	=> [3, 6, 14, 16, 17]
);

/* Enum Config Hotline Company */
$config['zone'] = array(
	'hcm'	=> [
		'name' 		=> 'hcm',
		'text' 		=> 'Hồ Chí Minh',
		'hotline'	=> 'Hotline HCM: 1800 0098 (24/7)',
		'tel'		=> '18000098'
	],
	'hn' => [
		'name'		=>'hn',
		'text'		=> 'Hà Nội',
		'hotline'	=> 'Hotline HN: 1800 0098 (24/7)',
		'tel'		=> '18000098'
	]
);