<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions extends MREST_Controller
{
    public function check_get($permission = '')
    {
        parent::response([
            'status' => TRUE,
            'data' => (bool) has_permission($permission)
        ]); 
    }

    /**
     * API check permission
     */
    public function can_get($permission = '', $contractId = '')
    {
    	$permission = $this->security->xss_clean($permission);
    	$contractId = $this->security->xss_clean($contractId);

    	if(empty($contractId)) return $this->check_get($permission);
    	
    	$this->load->model('contract/contract_m');

    	parent::response([
    		'status' => TRUE,
    		'data' => $this->contract_m->has_permission($contractId, $permission)
    	]);
    }
}