<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Auth extends MREST_Controller
{

    public function __construct($config = 'staffs/auth_rest')
    {
        parent::__construct($config);
    }

    /**
     * LOGIN
     */
    public function login_post()
    {
        $authInfo = parent::post(NULL, TRUE);

        if(empty($authInfo) || empty($authInfo['access_token'])) parent::response('Access token không còn hiệu lực [0]', parent::HTTP_UNAUTHORIZED);

        $user_info = [];

        try
        {            
            $this->config->load('google_service');

            $client = new Google_Client();
            $client->setAccessToken($authInfo);
            $client->setApplicationName( $this->config->item('applicationName', 'google_service') );
            $client->setDeveloperKey( $this->config->item('developerKey', 'google_service') );
            $client->setClientSecret( $this->config->item('clientSecret', 'google_service') );
            $client->setDeveloperKey( $this->config->item('developerKey', 'google_service') );
            $client->addScope( $this->config->item('scope', 'google_service') );

            $service_oauth2 = new Google_Service_Oauth2($client);
            $user_info = $service_oauth2->userinfo->get();
        } 
        catch (Exception $e)
        {
            if(empty($e->getErrors())) parent::response($e->getMessage(), $e->getCode());
            $errors = $e->getErrors();
            $errors = implode(', ', array_column($errors, 'message'));

            parent::response($errors, $e->getCode());
        }


        $admin = $this->admin_m
        ->select('role_id, user_avatar, user_login, user_email, user_id, user_status, user_time_create, display_name')
        ->set_get_admin()->get_by(['user_login' => $user_info['id']]);
        if(empty($admin)) $this->register($user_info);

        if( 0 == $admin->user_status) parent::response("Tài khoản của bạn đã bị khóa, vui lòng liên hệ với Quản trị để biết thêm chi tiết.", parent::HTTP_LOCKED);

        if( 1 == $admin->user_status)
        { 
            $role = $this->role_m->get($admin->role_id);
            $admin->role_name = $role->role_name ?? '';

            $token = $admin;
            $token->iat = time();
            $token->exp = strtotime("+5 hours", time());

            parent::response([ 
                'status' => TRUE,
                'message' => 'Đăng nhập thành công.',
                'loggingIn' => TRUE, 
                'token' => JWT::encode($token, getenv('JWT_SECRET'), getenv('JWT_ALG'))
            ], parent::HTTP_OK);
        }

        parent::response("Tài khoản của bạn đang chờ được kích hoạt", parent::HTTP_UNAUTHORIZED);
    }

    private function register(Google_Service_Oauth2_Userinfoplus $user_info)
    {
        if(empty($user_info)) throw new Exception("Thông tin người dùng chưa đầy đủ");

        $_admin = array(
            'user_login' => $user_info['id'],
            'display_name' => "{$user_info['familyName']} {$user_info['givenName']}",
            'user_status' => -1, 
            'user_email' => $user_info['email'],
            'user_avatar' => $user_info['picture'],
            'user_activation_key' => $this->admin_m->hash_pass($user_info['id']),
            'user_type' => 'admin'
        );

        $insert_id = $this->admin_m->insert($_admin);
        if( ! $insert_id) parent::response("Quá trình khởi tạo tài khoản không thành công.", parent::HTTP_NOT_IMPLEMENTED);

        $usermetas = array(
            'hd' => $user_info['hd'],
            'link' => $user_info['link'],
            'locale' => $user_info['locale'],
            'picture' => $user_info['picture'],
            'verifiedEmail' => $user_info['verifiedEmail']);

        foreach ($usermetas as $key => $value) update_user_meta($insert_id, $key, $value);

        parent::response([
            'status' => TRUE,
            'message' => 'Tài khoản đã được khởi tạo. Liên hệ quản trị viên để kích hoạt tài khoản.'
        ]);
    }

    public function login_options()
    {
        parent::response('ok', parent::HTTP_OK);
    }

    public function checkKey_post()
    {
        $res = ['code'=>'error'];
        if(empty(parent::post('key', TRUE))) parent::response($res);

        $this->load->model('credentials_m');

        if( ! $this->credentials_m->checkKey(parent::post('key'))) parent::response($res);
        parent::response(['code'=>'ok']);
    }
}
/* End of file Auth.php */
/* Location: ./application/modules/staffs/controllers/api_v2/Auth.php */