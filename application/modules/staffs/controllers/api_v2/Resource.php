<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Resource extends MREST_Controller
{
    function __construct()
    {
        $this->autoload['libraries'][] = 'form_validation';

        $this->autoload['models'][] = 'staffs/staffs_m';
        $this->autoload['models'][] = 'staffs/department_m';
        $this->autoload['models'][] = 'staffs/user_group_m';
        $this->autoload['models'][] = 'staffs/call_center_sale_m';
        $this->autoload['models'][] = 'usermeta_m';

        parent::__construct();

        $this->load->config('staffs/staffs');
        $this->load->config('staffs/group');
    }

    public function user_get()
    {
        $default    = array('id' => 0);
        $args       = wp_parse_args(parent::get(NULL, TRUE), $default);

        /* Truy xuất 1 contact */
        if (!empty($args['id'])) $this->get_by($args['id']);

        /* Truy xuất nhiều contact */
        $this->get_many_by();

        parent::response();
    }

    public function get_by($user_id = 0)
    {
        if (!$user_id) parent::responseHandler([], 'ID Không tồn tại', 'error', 400);

        $contact = $this->staffs_m->as_array()->get((int) $user_id);
        if (!$contact) parent::responseHandler([], 'ID Không tồn tại', 'error', 400);

        // Load meta
        $gender = get_user_meta_value($user_id, 'gender');
        $contact['gender'] = $gender;

        $user_phone = get_user_meta_value($user_id, 'user_phone');
        $contact['user_phone'] = $user_phone;

        $user_phone_private = get_user_meta_value($user_id, 'user_phone_private');
        $contact['user_phone_private'] = $user_phone_private;

        $phone_ext = get_user_meta_value($user_id, 'phone-ext');
        $contact['phone_ext'] = $phone_ext;

        $user_zone = get_user_meta_value($user_id, 'user_zone');
        $contact['user_zone'] = $user_zone;

        $user_working_day = get_user_meta_value($user_id, 'user_working_day');
        $contact['user_working_day'] = $user_working_day;

        // Load referral(s)
        $this->load->model('term_users_m');

        $contact['user_group'] = NULL;
        $this->load->model('staffs/user_group_m');
        $user_groups = $this->term_users_m->get_user_terms($user_id, $this->user_group_m->term_type);
        if (!empty($user_groups)) {
            $user_group = reset($user_groups);
            $contact['user_group'] = $user_group->term_id;
        }

        $contact['department'] = NULL;
        $this->load->model('staffs/department_m');
        $departments = $this->term_users_m->get_user_terms($user_id, $this->department_m->term_type);
        if (!empty($departments)) {
            $department = reset($departments);
            $contact['department'] = $department->term_id;
        }

        // Unset sensitive data
        unset($contact['user_login']);
        unset($contact['user_pass']);
        unset($contact['user_type']);
        unset($contact['user_activation_key']);
        unset($contact['user_salt']);
        unset($contact['user_time_create']);

        parent::responseHandler($contact, 'Lấy thông tin nhân viên thành công');
    }

    public function get_many_by()
    {
        $default_columns = $this->config->item('default_columns', 'datasource');
        $default = array(
            'pagination' => ['page' => 1, 'pages' => 35, 'perpage' => 200, 'total' => 350],
            'sort' => ['field' => 'user_id', 'sort' => 'DESC'],
            'query' => '',
            'columns' => $default_columns
        );
        $config = $this->config->item('datasource');
        $args   = wp_parse_args(parent::get(NULL, TRUE), $default);

        if ($config['columns'][$args['sort']['field']]['type'] == 'field') {
            $this->staffs_m->select('user.user_id')->order_by("user.{$args['sort']['field']}", $args['sort']['sort']);
        }
        if (isset($args['query']['filter_type']) && isset($args['query']['filter_value'])) {
            $this->staffs_m->like("{$args['query']['filter_type']}", "{$args['query']['filter_value']}");
        }
        $istart     = ($args['pagination']['page'] - 1) * $args['pagination']['perpage'];
        $ilength    = $args['pagination']['perpage'];
        $this->staffs_m->limit($ilength, $istart);

        $def_columns    = $this->config->item('columns', 'datasource') ?: [];
        $field_columns  = array_group_by($def_columns, 'type');
        if (!empty($field_columns['field']) && $field_columns = array_column($field_columns['field'], 'name')) {
            $this->staffs_m->select($field_columns);
        }

        $contacts = $this->staffs_m->as_array()->where('user_type', 'admin')->get_all();
        $last = $this->staffs_m->last_query();
        $array_query = explode("LIMIT", $last);
        $array_query = explode("FROM", $array_query[0]);
        $query = "SELECT count(*) as qty FROM " . $array_query[1];
        $qty = $this->db->query($query)->result();
        $contacts = array_map(function ($x) use ($args) {
            return $this->staffs_m->callback($x, $args['columns']);
        }, $contacts);

        $total_rows = $qty[0]->qty;

        $response = array(
            'meta' => array(
                'page' =>  $args['pagination']['page'],
                'pages' =>  ceil(div($total_rows, $args['pagination']['perpage'])),
                'perpage' =>  $args['pagination']['perpage'],
                'total' =>  $total_rows,
                'sort' =>  'asc',
                'field' =>  'user_id'
            ),
            'data' => $contacts
        );

        parent::responseHandler($response, 'Lấy thông tin nhân viên thành công');
    }

    public function department_get()
    {
        $departments = $this->department_m->set_term_type()->as_array()->get_many_by();
        if (empty($departments)) {
            parent::responseHandler([], 'Lấy thông tin phòng ban thành công');
        }

        $data = [];
        foreach ($departments as $item) {
            $data[] = [
                'term_id' => $item['term_id'],
                'term_name' => $item['term_name'],
            ];
        }

        parent::responseHandler($data, 'Lấy thông tin phòng ban thành công');
    }

    public function user_group_get()
    {
        $user_group = $this->user_group_m->set_term_type()->as_array()->get_many_by();
        if (empty($user_group)) {
            parent::responseHandler([], 'Lấy thông tin nhóm thành công');
        }

        $data = [];
        foreach ($user_group as $item) {
            $data[] = [
                'term_id' => $item['term_id'],
                'term_name' => $item['term_name'],
                'term_parent' => $item['term_parent'],
            ];
        }

        parent::responseHandler($data, 'Lấy thông tin nhóm thành công');
    }

    public function role_get()
    {
        $roles = $this->role_m->as_array()->get_many_by();
        if (empty($roles)) {
            parent::responseHandler([], 'Lấy thông tin vai trò thành công');
        }

        foreach ($roles as $item) {
            // Disable Administrator role
            if ($item['role_id'] == 1) {
                continue;
            }

            $data[] = [
                'role_id' => $item['role_id'],
                'role_name' => $item['role_name'],
            ];
        }

        parent::responseHandler($data, 'Lấy thông tin vai trò thành công');
    }

    public function config_get()
    {
        $config = [];

        $user_status = $this->config->item('user_status');
        $config['userStatus'] = $user_status;

        $zone = $this->config->item('zone');
        $zone = $zone ? key_value($zone, 'name', 'text') : [];
        $config['zone'] = $zone;

        $gender = [
            '1' => 'Nam',
            '0' => 'Nữ',
        ];
        $config['gender'] = $gender;

        $show_info = [
            '1' => 'Có',
            '0' => 'Không',
        ];
        $config['showInfo'] = $show_info;

        parent::responseHandler($config, 'Lấy config thành công');
    }

    public function user_put($user_id = 0)
    {
        if (!has_permission('admin.staff.update')) {
            parent::responseHandler(null, 'Không có quyền thay đổi thông tin nhân viên', 'error', 403);
        }

        $args = wp_parse_args(parent::put(null, true), ['user_id' => $user_id]);

        // Validate
        $rules = array(
            'user_id' => [
                'field' => 'user_id',
                'label' => 'ID nhân viên',
                'rules' => [
                    'required',
                    array('existed_check', array($this->staffs_m, 'existed_check'))
                ]
            ],
            'role_id' => [
                'field' => 'role_id',
                'label' => 'Vai trò',
                'rules' => [
                    'required',
                    array('existed_check', array($this->role_m, 'existed_check'))
                ]
            ],
            'display_name' => [
                'field' => 'display_name',
                'label' => 'Họ tên',
                'rules' => 'required'
            ],
            'user_email' => [
                'field' => 'user_email',
                'label' => 'E-mail',
                'rules' => 'trim|required|valid_email'
            ],
            'user_status' => [
                'field' => 'user_status',
                'label' => 'Trạng thái',
                'rules' => 'in_list[' . implode(',', array_keys($this->config->item('user_status'))) . ']'
            ],
            'gender' => [
                'field' => 'gender',
                'label' => 'Giới tính',
                'rules' => 'in_list[0,1]'
            ],
            'user_phone' => [
                'field' => 'user_phone',
                'label' => 'Số công việc',
                'rules' => [
                    'regex_match[/^(03|05|07|08|09|01[2|6|8|9]|02[0|1|2|3|4|5|6|7|8|9]{1,2})+([0-9]{8})$/]',
                    array('user_phone_check', function ($value) use ($args) {
                        return $this->staffs_m->user_phone_check($value, $args['user_id']);
                    }),
                ]
            ],
            'user_phone_private' => [
                'field' => 'user_phone_private',
                'label' => 'Số di động',
                'rules' => [
                    'regex_match[/^(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})$/]',
                    array('user_phone_private_check', function ($value) use ($args) {
                        return $this->staffs_m->user_phone_private_check($value, $args['user_id']);
                    }),
                ]
            ],
            'user_zone' => [
                'field' => 'user_zone',
                'label' => 'Khu vực',
                'rules' => 'in_list[' . implode(',', array_keys($this->config->item('zone'))) . ']'
            ],
            'show_info' => [
                'field' => 'show_info',
                'label' => 'Hiển thị thông tin trên website',
                'rules' => 'in_list[0,1]'
            ],
            'department' => [
                'field' => 'department',
                'label' => 'Phòng ban',
                'rules' => [
                    array('existed_check', array($this->department_m, 'existed_check'))
                ]
            ],
            'user_group' => [
                'field' => 'user_group',
                'label' => 'Nhóm',
                'rules' => [
                    array('existed_check', array($this->user_group_m, 'existed_check')),
                    array('department_check', function ($value) use ($args) {
                        return $this->user_group_m->department_check($value, $args['department']);
                    })
                ]
            ],
            'phone_ext' => [
                'field' => 'phone_ext',
                'label' => 'Số điện thoại EXT',
                'rules' => [
                    'regex_match[/^[0-9]{4}$/]',
                    array('available_check', function ($value) use ($args) {
                        return $this->call_center_sale_m->available_check($value, $args['user_id']);
                    }),
                    array('hubspot_check', function ($value) use ($args) {
                        return $this->call_center_sale_m->hubspot_check($value, $args['user_id']);
                    })
                ]
            ],
        );

        $this->form_validation->set_data($args);
        $this->form_validation->set_rules($rules);
        if (FALSE === $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        // Update user data
        $userData = [
            'role_id' => $args['role_id'],
            'display_name' => $args['display_name'],
            'user_status' => $args['user_status'],
            'user_avatar' => $args['user_avatar'],
        ];
        $this->staffs_m->update($user_id, $userData);

        // Update user meta
        if (!empty($args['phone_ext'])) {
            $all_meta_phone_ext = $this->usermeta_m->select('user_id')->where('meta_key', 'phone-ext')
                ->where('meta_value', $args['phone_ext'])
                ->get_all();

            foreach ($all_meta_phone_ext as $item) {
                update_user_meta($item->user_id, 'phone-ext', '');
            }
        }

        $metas = [
            'gender' => $args['gender'],
            'user_phone' => $args['user_phone'],
            'user_phone_private' => $args['user_phone_private'],
            'phone-ext' => $args['phone_ext'],
            'user_zone' => $args['user_zone'],
            'user_working_day' => $args['user_working_day'],
        ];
        foreach ($metas as $key => $value) {
            update_user_meta($user_id, $key, $value);
        }

        // Update user referral
        $this->load->model('staffs/department_m');
        $department = $this->term_users_m->get_user_terms($user_id, $this->department_m->term_type);
        if (!empty($department)) {
            $_department = reset($department);
            $department = $_department->term_id;
        }
        if(empty($args['department']) && !empty($department)){
            $this->term_users_m->delete_term_users($department, $user_id, $this->department_m->term_type);
        }
        else {
            $this->term_users_m->set_user_terms($user_id, $args['department'], $this->department_m->term_type);
        }

        $this->load->model('staffs/user_group_m');
        $user_group = $this->term_users_m->get_user_terms($user_id, $this->user_group_m->term_type);
        if (!empty($user_group)) {
            $_user_group = reset($user_group);
            $user_group = $_user_group->term_id;
        }
        if(empty($args['user_group']) && !empty($user_group)){
            $this->term_users_m->delete_term_users($user_group, $user_id, $this->user_group_m->term_type);
        }
        else {
            $this->term_users_m->set_user_terms($user_id, $args['user_group'], $this->user_group_m->term_type);
        }

        // Update call center
        if (empty($args['phone_ext'])) return parent::responseHandler($args, 'Cập nhật thông tin nhân viên thành công');

        $staff = $this->staffs_m->set_user_type()->where('user_id', $args['user_id'])->get_by();
        $hubspotOwner = $this->call_center_sale_m->get_by_email_hubspot_owners($staff->user_email);
        $remoteList = reset($hubspotOwner->remoteList);

        $dataCallCenterSale = [
            'may_nhanh' => $args['phone_ext'],
            'name' => $args['display_name'],
            'email_sale' => $args['user_email'],
            'owner_id' => $remoteList->ownerId,
        ];
        $callCenterSale = $this->call_center_sale_m->where('may_nhanh', $dataCallCenterSale['may_nhanh'])->get_by();
        if (empty($callCenterSale)) {
            $this->call_center_sale_m->insert($dataCallCenterSale['email_sale']);
        } else {
            $this->call_center_sale_m->update($callCenterSale->id, $dataCallCenterSale);
        }

        parent::responseHandler($args, 'Cập nhật thông tin nhân viên thành công');
    }
}
