<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class Dataset extends MREST_Controller
{
    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'admin_m';
        $this->autoload['models'][] = 'role_m';
        $this->autoload['models'][] = 'usermeta_m';
        $this->autoload['models'][] = 'staffs/department_m';
        $this->autoload['models'][] = 'staffs/user_group_m';
        $this->autoload['models'][] = 'option_m';

        parent::__construct();

        $this->load->config('staffs/staffs');
        $this->load->config('staffs/group');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        if (!has_permission('admin.staff.view')) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            return parent::response($response);
        }

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        // Prepare search
        $statusConfig = $this->config->item('user_status');

        $genderConfig = $this->config->item('user_gender');

        $zoneConfig = $this->config->item('zone');
        $zoneData = $zoneConfig ? key_value($zoneConfig, 'name', 'text') : [];

        $departmentData = $this->department_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $departmentData = $departmentData ? key_value($departmentData, 'term_id', 'term_name') : [];

        $userGroupData = $this->user_group_m->select('term_id, term_name')->set_term_type()->as_array()->get_all();
        $userGroupData = $userGroupData ? key_value($userGroupData, 'term_id', 'term_name') : [];

        $roleData = $this->role_m->select('role_id, role_name')->as_array()->get_all();
        $roleData = $roleData ? key_value($roleData, 'role_id', 'role_name') : [];

        // Build datatable
        $data = $this->data;
        $this->search_filter_receipt();

        $option_name = ['manager_role_id', 'manager_ads_role_id', 'manager_fb_ads_role_id', 'member_kpi_per_day', ''];
        $manager_rold_ids = $this->option_m->where_in('option_name', $option_name)->select('option_value')->as_array()->get_all();
        $manager_rold_ids = array_column($manager_rold_ids, 'option_value');
        $data['content'] = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')
            ->select('user.user_id, user.user_avatar, user.user_email, user.display_name, user.role_id, user.user_time_create, user.user_status')

            // ->add_search('user_id', ['placeholder' => 'Mã nhân sự'])
            ->add_search('display_name', ['placeholder' => 'Tên hiển thị'])
            ->add_search('user_email', ['placeholder' => 'Email đăng nhập'])
            ->add_search('gender', ['content' => form_dropdown(['name' => 'where[gender]', 'class' => 'form-control select2'], prepare_dropdown($genderConfig, 'Giới tính'), parent::get('where[gender]'))])
            ->add_search('role', ['content' => form_dropdown(['name' => 'where[role]', 'class' => 'form-control select2'], prepare_dropdown($roleData, 'Vai trò'), parent::get('where[role]'))])
            ->add_search('zone', ['content' => form_dropdown(['name' => 'where[zone]', 'class' => 'form-control select2'], prepare_dropdown($zoneData, 'Khu vực'), parent::get('where[zone]'))])
            ->add_search('department', ['content' => form_dropdown(['name' => 'where[department]', 'class' => 'form-control select2'], prepare_dropdown($departmentData, 'Phòng ban'), parent::get('where[department]'))])
            ->add_search('user_group', ['content' => form_dropdown(['name' => 'where[user_group]', 'class' => 'form-control select2'], prepare_dropdown($userGroupData, 'Nhóm'), parent::get('where[user_group]'))])
            ->add_search('user_phone', ['placeholder' => 'Số công việc'])
            ->add_search('phone_ext', ['placeholder' => 'Số nội bộ'])
            ->add_search('user_working_day', ['placeholder' => 'Ngày nhận việc', 'class' => 'form-control set-datepicker'])
            ->add_search('created_on', ['placeholder' => 'Ngày tạo', 'class' => 'form-control set-datepicker'])
            ->add_search('user_status', ['content' => form_dropdown(['name' => 'where[user_status]', 'class' => 'form-control select2'], prepare_dropdown($statusConfig, 'Trạng thái'), parent::get('where[user_status]'))])

            // ->add_column('user_id', array('title' => 'ID', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('display_name', array('title' => 'Tên hiển thị', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('user_email', array('title' => 'Tên đăng nhập', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('gender', array('title' => 'Giới tính', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('role', array('title' => 'Vai trò', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('zone', array('title' => 'Khu vực', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('department', array('title' => 'Phòng ban', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('user_group', array('title' => 'Nhóm', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('user_phone', array('title' => 'Số công việc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('phone_ext', array('title' => 'Số nội bộ', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('user_status', array('title' => 'Trạng thái', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('user_working_day', array('title' => 'Ngày nhận việc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('created_on', array('title' => 'Ngày tạo', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('action', array('title' => 'Hành động', 'set_select' => FALSE, 'set_order' => FALSE))

            ->add_column_callback('user_id', function ($data, $row_name) use ($manager_rold_ids) {
                $user_id = (int)$data['user_id'];

                // Get gender
                $genderConfig = $this->config->item('user_gender');
                $data['gender_raw'] = get_user_meta_value($user_id, 'gender');
                $data['gender'] = $genderConfig[$data['gender_raw']] ?? '--';

                // Get role
                $role = $this->role_m->where('role_id', $data['role_id'])->get_by();
                empty($role) ? '--' : $data['role'] = $role->role_name;
                

                // Get gender
                $zoneConfig = $this->config->item('zone');
                $data['zone_raw'] = get_user_meta_value($user_id, 'user_zone');
                $data['zone'] = $zoneConfig[$data['zone_raw']]['text'] ?? '--';

                // Get department
                $data['department'] = '--';
                $departments = $this->term_users_m->get_user_terms($user_id, $this->department_m->term_type);
                if (!empty($departments)) {
                    $department = reset($departments);
                    $data['department'] = $department->term_name;
                }

                // Get user_group
                $data['user_group'] = '--';
                if(in_array($data['role_id'], $manager_rold_ids)){
                    $data['user_group'] = 'Tất cả nhóm';
                }
                else {
                    $user_groups = $this->term_users_m->get_user_terms($user_id, $this->user_group_m->term_type);
                    if (!empty($user_groups)) {
                        $user_group = reset($user_groups);
                        $data['user_group'] = $user_group->term_name;
                    }
                }

                // Get user_phone
                $data['user_phone_raw'] = get_user_meta_value($user_id, 'user_phone');
                $data['user_phone'] = $data['user_phone_raw'] ?: '--';

                // Get phone_ext
                $data['phone_ext_raw'] = get_user_meta_value($user_id, 'phone-ext');
                $data['phone_ext'] = $data['phone_ext_raw'] ?: '--';

                // Get created_on
                $data['created_on'] = date('d/m/Y', $data['user_time_create']);

                // Get user_working_day
                $data['user_working_day_raw'] = get_user_meta_value($user_id, 'user_working_day');
                $data['user_working_day'] = $data['user_working_day_raw'] ? date('d/m/Y', $data['user_working_day_raw']) : '--';

                // Get user status
                $statusConfig = $this->config->item('user_status');
                $data['user_status_raw'] = '' . $data['user_status'];
                $data['user_status'] = $statusConfig[$data['user_status_raw']] ?? 'Khoá';

                return $data;
            }, FALSE)

            ->from('user')
            ->where('user.user_type', 'admin')
            ->group_by('user.user_id');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);
        // dd($this->datatable_builder->last_query());

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export_receipt($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function search_filter_receipt($args = array())
    {
        restrict('admin.staff.view');

        $args = parent::get();
        //if(empty($args) && parent::get('search')) $args = parent::get();  
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // user_id FILTERING & SORTING
        $filter_user_id = $args['where']['user_id'] ?? FALSE;
        if (!empty($filter_user_id)) {
            $this->datatable_builder->like('user.user_id', $filter_user_id);
        }

        $sort_user_id = $args['order_by']['user_id'] ?? FALSE;
        if (!empty($sort_user_id)) {
            $this->datatable_builder->order_by('user.user_id', $sort_user_id);
        }

        // user_email FILTERING & SORTING
        $filter_user_email = $args['where']['user_email'] ?? FALSE;
        if (!empty($filter_user_email)) {
            $this->datatable_builder->like('user.user_email', $filter_user_email);
        }

        $sort_user_email = $args['order_by']['user_email'] ?? FALSE;
        if (!empty($sort_user_email)) {
            $this->datatable_builder->order_by('user.user_email', $sort_user_email);
        }

        // display_name FILTERING & SORTING
        $filter_display_name = $args['where']['display_name'] ?? FALSE;
        if (!empty($filter_display_name)) {
            $this->datatable_builder->like('user.display_name', $filter_display_name);
        }

        $sort_display_name = $args['order_by']['display_name'] ?? FALSE;
        if (!empty($sort_display_name)) {
            $this->datatable_builder->order_by('user.display_name', $sort_display_name);
        }

        // gender FILTERING & SORTING
        $sort_gender = $args['order_by']['gender'] ?? FALSE;
        if (isset($args['where']['gender']) || $sort_gender) {
            $filter_gender = $args['where']['gender'] ?? FALSE;

            $alias = uniqid('gender_');
            $this->datatable_builder->join("usermeta {$alias}", "{$alias}.user_id = user.user_id and {$alias}.meta_key = 'gender'", 'LEFT');

            if (is_numeric($filter_gender)) {
                $this->datatable_builder->where("{$alias}.meta_value", $filter_gender);
            }

            if ($sort_gender) {
                $this->datatable_builder->select("{$alias}.meta_value")
                    ->group_by("{$alias}.meta_value")
                    ->order_by("{$alias}.meta_value", $sort_gender);
            }
        }

        // zone FILTERING & SORTING
        $filter_zone = $args['where']['zone'] ?? FALSE;
        $sort_zone = $args['order_by']['zone'] ?? FALSE;
        if ($filter_zone || $sort_zone) {
            $alias = uniqid('user_zone_');
            $this->datatable_builder->join("usermeta {$alias}", "{$alias}.user_id = user.user_id and {$alias}.meta_key = 'user_zone'", 'LEFT');

            if ($filter_zone) {
                $this->datatable_builder->where("{$alias}.meta_value", $filter_zone);
            }

            if ($sort_zone) {
                $this->datatable_builder->select("{$alias}.meta_value")
                    ->group_by("{$alias}.meta_value")
                    ->order_by("{$alias}.meta_value", $sort_zone);
            }
        }

        // user_status FILTERING & SORTING
        $sort_user_status = $args['order_by']['user_status'] ?? FALSE;
        if (isset($args['where']['user_status'])) {
            $filter_user_status = $args['where']['user_status'] ?? FALSE;
            if (is_numeric($filter_user_status)) {
                $this->datatable_builder->where('user.user_status', $filter_user_status);
            }

            if ($sort_user_status) {
                $this->datatable_builder->order_by('user.user_status', $sort_user_status);
                unset($args['order_by']['user_status']);
            }
        }

        // role FILTERING & SORTING
        $filter_role = $args['where']['role'] ?? FALSE;
        if (!empty($filter_role)) {
            $this->datatable_builder->where('user.role_id', $filter_role);
        }

        $sort_role = $args['order_by']['role'] ?? FALSE;
        if (!empty($sort_role)) {
            $alias = uniqid('role_');
            $this->datatable_builder->join("role {$alias}", "{$alias}.role_id = user.role_id")
                ->order_by("{$alias}.role_name", $sort_role);
        }

        // department FILTERING & SORTING
        $filter_department = $args['where']['department'] ?? FALSE;
        $sort_department = $args['order_by']['department'] ?? FALSE;
        if ($filter_department || $sort_department) {
            $aliasTermUsers = uniqid('term_users_');
            $aliasDepartment = uniqid('term_');

            $this->datatable_builder
                ->join("term_users {$aliasTermUsers}", "{$aliasTermUsers}.user_id = user.user_id")
                ->join("term {$aliasDepartment}", "{$aliasDepartment}.term_id = {$aliasTermUsers}.term_id and {$aliasDepartment}.term_type = 'department'");

            if (!empty($filter_department)) {
                $this->datatable_builder->where("{$aliasDepartment}.term_id", $filter_department);
            }

            if (!empty($sort_department)) {
                $this->datatable_builder->select("{$aliasDepartment}.term_name")
                    ->group_by("{$aliasDepartment}.term_id")
                    ->order_by("{$aliasDepartment}.term_name", $sort_department);
            }
        }

        // user_group FILTERING & SORTING
        $filter_user_group = $args['where']['user_group'] ?? FALSE;
        $sort_user_group = $args['order_by']['user_group'] ?? FALSE;
        if ($filter_user_group || $sort_user_group) {
            $aliasTermUsers = uniqid('term_users_');
            $aliasUserGroup = uniqid('term_');

            $this->datatable_builder
                ->join("term_users {$aliasTermUsers}", "{$aliasTermUsers}.user_id = user.user_id")
                ->join("term {$aliasUserGroup}", "{$aliasUserGroup}.term_id = {$aliasTermUsers}.term_id and {$aliasUserGroup}.term_type = 'user_group'");

            if ($filter_user_group) {
                $this->datatable_builder->where("{$aliasUserGroup}.term_id", $filter_user_group);
            }

            if (!empty($sort_user_group)) {
                $this->datatable_builder->select("{$aliasUserGroup}.term_name")
                    ->group_by("{$aliasUserGroup}.term_id")
                    ->order_by("{$aliasUserGroup}.term_name", $sort_user_group);
            }
        }

        // user_phone FILTERING & SORTING
        $filter_user_phone = $args['where']['user_phone'] ?? FALSE;
        $sort_user_phone = $args['order_by']['user_phone'] ?? FALSE;
        if ($filter_user_phone) {
            $alias = uniqid('user_phone_');

            $this->datatable_builder->join("usermeta {$alias}", "{$alias}.user_id = user.user_id and {$alias}.meta_key = 'user_phone'", 'LEFT');

            if ($filter_user_phone) {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_user_phone);;
            }

            if (!empty($sort_user_phone)) {
                $this->datatable_builder->select("{$alias}.meta_value")
                    ->group_by("{$alias}.meta_value")
                    ->order_by("{$alias}.meta_value", $sort_user_phone);
            }
        }

        // phone_ext FILTERING & SORTING
        $filter_phone_ext = $args['where']['phone_ext'] ?? FALSE;
        $sort_phone_ext = $args['order_by']['phone_ext'] ?? FALSE;
        if ($filter_phone_ext || $sort_phone_ext) {
            $alias = uniqid('phone_ext_');
            $this->datatable_builder->join("usermeta {$alias}", "{$alias}.user_id = user.user_id and {$alias}.meta_key = 'phone-ext'", 'LEFT');

            if ($filter_phone_ext) {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_phone_ext);
            }

            if (!empty($sort_phone_ext)) {
                $this->datatable_builder->select("{$alias}.meta_value")
                    ->group_by("{$alias}.meta_value")
                    ->order_by("{$alias}.meta_value", $sort_phone_ext);
            }
        }


        // created_on FILTERING & SORTING
        $filter_created_on = $args['where']['created_on'] ?? FALSE;
        if (!empty($filter_created_on)) {
            $dates = explode(' - ', $args['where']['created_on']);

            $this->datatable_builder
                ->where("user.user_time_create >=", $this->mdate->startOfDay(reset($dates)))
                ->where("user.user_time_create <=", $this->mdate->endOfDay(end($dates)));
        }

        $sort_created_on = $args['order_by']['created_on'] ?? FALSE;
        if (!empty($sort_created_on)) {
            $this->datatable_builder->order_by('user.user_time_create', $sort_created_on);
        }

        // user_working_day FILTERING & SORTING
        $filter_user_working_day = $args['where']['user_working_day'] ?? FALSE;
        $sort_user_working_day = $args['order_by']['user_working_day'] ?? FALSE;
        if ($filter_user_working_day || $sort_user_working_day) {
            $dates = explode(' - ', $args['where']['user_working_day']);

            $alias = uniqid('user_working_day_');
            $this->datatable_builder->join("usermeta {$alias}", "{$alias}.user_id = user.user_id and {$alias}.meta_key = 'user_working_day'", 'LEFT');

            if ($filter_user_working_day) {
                $this->datatable_builder
                    ->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if (!empty($sort_user_working_day)) {
                $this->datatable_builder->select("{$alias}.meta_value")
                    ->group_by("{$alias}.meta_value")
                    ->order_by("{$alias}.meta_value", $sort_user_working_day);
            }
        }
    }
}
/* End of file Dataset.php */
/* Location: ./application/modules/staffs/controllers/api_v2/Dataset.php */
