<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends API_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function listSales()
	{
		$this->load->config('staffs/group');
		$saleRoleIds = $this->config->item('salesexecutive','groups');

		$data = array();

		$saleUsers = $this->admin_m
		->where_in('role_id',$saleRoleIds)
		->where('user_type','admin')
		->where('user_status',1)
		->order_by('user_id')
		->get_many_by();

		if(empty($saleUsers)) 
			return $data;

		foreach ($saleUsers as $user) 
		{
			$show_info = (int)get_user_meta_value($user->user_id, 'show_info');
			if(empty($show_info)) continue;

			$phone = get_user_meta_value($user->user_id,'user_phone');
			if(empty($phone)) continue;


			$display_name = $user->display_name;
			$segmentName = explode(' ', $display_name);

			$display_name = '';
			$count = count($segmentName);			
			$i = $count - 1;
			for($i;$i >= 0;$i--)
			{
				$segmentName[$i] = trim($segmentName[$i]);
				if(empty($segmentName[$i])) continue;

				if($i == ($count - 1) || $i == ($count - 2))
				{
					$display_name = $segmentName[$i] .' '. $display_name;
					continue;
				}

				$display_name = mb_substr($segmentName[$i],0,1, 'UTF-8').'.'.$display_name;
			}

			$thumbnail = theme_url('images/sale-avatar/callcenter-unonymous.png');

			$type = pathinfo($thumbnail, PATHINFO_EXTENSION);		
			$thumbnailData = file_get_contents($thumbnail);
			$dataUri = 'data:image/' . $type . ';base64,' . base64_encode($thumbnailData);

			$_explodeMail = explode('@', $user->user_email);
			$thumbnailName = !empty($_explodeMail) ? reset($_explodeMail) : '';
			if(!empty($thumbnailName))
			{
				$thumbnailPath = './template/frontend/images/sale-avatar/';
				$thumbnailFileName = $thumbnailPath."{$thumbnailName}.jpg";
				if(file_exists($thumbnailFileName))
				{
					$type = pathinfo($thumbnailFileName, PATHINFO_EXTENSION);		
					$thumbnailData = file_get_contents(theme_url("images/sale-avatar/{$thumbnailName}.{$type}"));
					$dataUri = 'data:image/' . $type . ';base64,' . base64_encode($thumbnailData);
				}
			}

			$sale = array(
				'display_name' => $display_name,
				'phone' => $phone,
				'imgBase' => $dataUri
				);

			$data[] = $sale;
		}

		echo serialize($data);
	}

	function list()
	{
		$response = array('status'=>FALSE,'msg'=>'Xử lý không thành công','data'=>array());
		$args = wp_parse_args(parent::get(NULL, TRUE), array('_domains_hotline' => ''));

		$this->load->config('staffs/group');
		$this->load->config('staffs/staffs');
		$this->load->model('staffs/sale_m');

		$sales = $this->sale_m->select('user.user_id,user.role_id,user.display_name, user.user_email, user.user_status')
		->set_role()->set_user_type()->order_by('user.user_id')->where('user_status', 1)->get_all();

		if( ! $sales)
		{
			$response['msg'] = 'Dữ liệu không được tìm thấy.';
			return parent::renderJson($response);
		}

		$data = array();
		
		$thumbnail 	= theme_url('images/sale-avatar/callcenter-unonymous.png');
		$type 		= pathinfo($thumbnail, PATHINFO_EXTENSION);		
		$thumbnailData 	= file_get_contents($thumbnail);
		$defaultImgBase 		= 'data:image/' . $type . ';base64,' . base64_encode($thumbnailData);
		foreach($sales as $sale)
		{
			$show_info = (int) get_user_meta_value($sale->user_id, 'show_info');
			if(empty($show_info)) continue;

			if( ! empty($args['_domains_hotline']))
			{
				$_domains_hotline = get_user_meta_value($sale->user_id, '_domains_hotline');
				$_domains_hotline = is_serialized($_domains_hotline) ? unserialize($_domains_hotline) : [];

				if(empty($_domains_hotline) || !in_array($args['_domains_hotline'], $_domains_hotline)) continue;
			}

			/* Check thumbnail override of user */
			$imgBase 		= $defaultImgBase;
			$_explodeMail 	= explode('@', $sale->user_email);
			$thumbnailName 	= !empty($_explodeMail) ? reset($_explodeMail) : '';
			if( ! empty($thumbnailName))
			{
				$thumbnailPath 		= './template/frontend/images/sale-avatar/';
				$thumbnailFileName 	= $thumbnailPath."{$thumbnailName}.jpg";
				if(file_exists($thumbnailFileName))
				{
					$type = pathinfo($thumbnailFileName, PATHINFO_EXTENSION);		
					$thumbnailData = file_get_contents(theme_url("images/sale-avatar/{$thumbnailName}.{$type}"));
					$imgBase = 'data:image/' . $type . ';base64,' . base64_encode($thumbnailData);
				}
			}

			$data[] = array(
				'email'       		=> $sale->user_email,
				'display_name' 		=> short_full_name($sale->display_name),
				'gender' 			=> (get_user_meta_value($sale->user_id, 'gender') == 0 ? 'Nữ' : 'Nam'),
				'phone' 			=> get_user_meta_value($sale->user_id, 'user_phone'),
				'phone_private' 	=> get_user_meta_value($sale->user_id, 'phone_private'),
				'phone_ext' 		=> get_user_meta_value($sale->user_id, 'phone_ext'),
				'user_working_day' 	=> my_date(get_user_meta_value($sale->user_id, 'user_working_day')),
				'role'				=> $sale->role_id,
				'user_status' 		=> $this->config->item($sale->user_status, 'user_status'),
				'zone' 				=> $this->config->item(get_user_meta_value($sale->user_id, 'user_zone') ?: 'hcm', 'zone'),
				'imgBase' 			=> $imgBase
			);
		}

		$response['data'] 	= $data;
		$response['status'] = TRUE;
		$response['msg'] 	= 'Hệ thống xử lý hoàn tất .';
		return parent::renderJson($response);
	}
}