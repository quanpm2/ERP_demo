<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataset extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$models = array(
			'term_users_m',
			'contract/category_m',
			'contract/term_categories_m',
			'contract/receipt_m',
			'contract/contract_m',
		);

		$this->load->model($models);
		$this->load->library('datatable_builder');
	}

	public function index()
	{
		$response = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

		if( ! has_permission('admin.contract.view'))
		{
			$response['msg'] = 'Quyền truy cập không hợp lệ.';
			return parent::renderJson($response);
		}

		$defaults 	= ['offset' => 0, 'per_page' => 50, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
		$args 		= wp_parse_args( $this->input->get(), $defaults);

		$data = $this->data; // remove-after

		// Authorization for user role
		if( ! has_permission('admin.contract.manage'))
		{
			$this->datatable_builder->join('term_users','term_users.term_id = term.term_id');
			$this->datatable_builder->join('termmeta tersale_id','tersale_id.term_id = term.term_id AND tersale_id.meta_key = "staff_business"','LEFT');
			$this->datatable_builder->where("(term_users.user_id = {$this->admin_m->id} OR tersale_id.meta_value = {$this->admin_m->id})");
		}

		/* Applies get query params for filter */
		$this->search_filter();

		$columns = array(
			'term_id' => array(
				'field' => 'term_id',
				'attrs' => array('set_select'=> FALSE, 'title'=> 'Số HĐ','set_order'=> TRUE),
				'is_required' => TRUE,
				'callback' => function($data,$row_name) {

					$term_id = $data['term_id'];

					// Mã hợp đồng
					$contract_code = get_term_meta_value($term_id,'contract_code') ?: '<code>HĐ chưa được cấp số</code>';
					
					if(has_permission('admin.contract.edit') || has_permission('admin.contract.add'))
					{
						$contract_code = anchor(admin_url("contract/create_wizard/index/{$term_id}"),$contract_code,['target'=>'_blank']);	
					}

					$contract_code = "<b>{$contract_code}</b><br/>";
					
					// Người tạo
					if($created_by = get_term_meta_value($term_id,'created_by'))
					{
						$created_display_name = $this->admin_m->get_field_by_id($created_by,'display_name'); 
						$contract_code.= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Người tạo'><i class='fa fa-fw fa-user'></i>Tạo bởi :{$created_display_name}</span>";
					}

					// Người tạo
					if($created_on = get_term_meta_value($term_id,'created_on'))
					{
						$created_datetime = my_date($created_on,'d/m/Y');
						$contract_code.= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Ngày tạo'><i class='fa fa-fw  fa-clock-o'></i>Ngày tạo :{$created_datetime}</span>";
					}

					// Get customer display name
					if(!empty($data['term_name']))
					{
						$website = anchor_popup(prep_url($data['term_name']));
						$contract_code.= "<span class='text-muted col-xs-12' data-toggle='tooltip' title='Trang thực hiện'><i class='fa fa-fw fa-globe'></i>Webpage : {$website}</span>";
					}

					// Get customer display name
					$customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company']);
					if( ! empty($customers))
					{
						$customer = end($customers);
						$display_name = mb_ucwords(mb_strtolower($this->admin_m->get_field_by_id($customer->user_id, 'display_name')));

						$contract_code.= br()."<span class='text-muted col-xs-12' data-toggle='tooltip' title='Khách hàng'><i class='fa fa-fw fa-user-secret'></i>KH : {$display_name}</span>";
					}

					$data['contract_code'] = $contract_code;

					$term_name = $data['term_name']?:'<code>chưa-cấp-số</code>';
					
					// Thời gian hợp đồng (từ ngày - đến ngày)
					$contract_begin = get_term_meta_value($term_id,'contract_begin');
					$contract_end 	= get_term_meta_value($term_id,'contract_end');
					$contract_daterange = '<i class="fa fa-fw fa-play" style="color:#72d072;font-size:0.8em"></i> '.($contract_begin ? my_date($contract_begin) : '--') . br();
					$contract_daterange.= '<i class="fa fa-fw fa-stop" style="color: #bb7171;font-size:0.8em"></i> '.($contract_end ? my_date($contract_end) : '--');
					$data['contract_daterange'] = $contract_daterange;

					// Thời gian kích hoạt dịch vụ
					$start_service_time = get_term_meta_value($term_id,'start_service_time');
					$data['start_service_time'] = $start_service_time ? my_date($start_service_time,'Y/m/d') : '--';

					// Giá trị hợp đồng trước VAT
					$contract_value = get_term_meta_value($term_id,'contract_value');
					$data['contract_value'] = currency_numberformat($contract_value,'đ');

					// Nhân viên kinh doanh
					$sale_id = get_term_meta_value($data['term_id'],'staff_business');
					$data['staff_business'] = $this->admin_m->get_field_by_id($sale_id,'display_name') ?: '--';

					// Loại hợp đồng
					$data['_term_type'] = $data['term_type'];
					$data['term_type'] = $this->config->item($data['term_type'],'services');

					// Hợp đồng Ký mới/ Tái ký
					$is_first_contract = get_term_meta_value($term_id,'is_first_contract');
					$data['is_first_contract'] = $is_first_contract ? '<i class="fa fa-fw fa-star"></i>' : '';

					// Trạng thái hợp đồng
					$data['_term_status'] = $data['term_status'];

					if(empty($customers))
					{
						$data['term_status'] = 'Chưa cập nhật<br/> khách hàng';
					}
					else
					{
						$data['_has_customer'] = TRUE;

						$data['term_status'] = is_numeric($data['term_status']) 
						? ($data['term_status'] > 0 ? 'publish' : 'draft')
						: $data['term_status'] ;
						$data['term_status'] = $this->config->item($data['term_status'],'contract_status');
					}

					// Actions column
					if(has_permission('admin.contract.edit'))
					{
						$data['action'] = anchor(module_url("create_wizard/index/{$term_id}"),'<i class="fa fa-fw fa-edit"></i>','title="Cập nhật hợp đồng" class="btn btn-default btn-xs"');
					}

					if(has_permission('admin.contract.add'))
					{
						$is_type_adsplus = in_array($data['_term_type'], array('google-ads'));
						$is_contract_active = in_array($data['_term_status'], array('liquidation','publish','pending','ending'));
						if($is_type_adsplus && $is_contract_active)
							$data['action'].= anchor(module_url("copy/{$data['term_id']}"),'<i class="fa fa-fw fa-copy"></i>','title="Tạo bản sao chép" class="btn btn-default btn-xs" data-toggle="confirmation"');
					}

					return $data;
					}
			)
		);

		$this->datatable_builder->set_filter_position(FILTER_TOP_OUTTER)
		->select('term.term_id,term.term_name')
		->add_search('contract_code',['placeholder'=>'Số hợp đồng'])
		->add_search('term.term_name',['placeholder'=>'Website'])
		->add_search('created_on',['placeholder'=>'Ngày tạo','class'=>'form-control input_daterange'])
		->add_search('verified_on',['placeholder'=>'Ngày Cấp số','class'=>'form-control input_daterange'])
		->add_search('contract_begin',['placeholder'=>'Ngày bắt đầu HĐ','class'=>'form-control input_daterange'])
		->add_search('contract_end',['placeholder'=>'Ngày kết thúc HĐ','class'=>'form-control input_daterange'])
		->add_search('start_service_time',['placeholder'=>'T/g Thực hiện','class'=>'form-control input_daterange'])		

		->add_search('staff_business',['placeholder'=>'Kinh doanh phụ trách'])
		->add_search('created_by',['placeholder'=>'Người tạo'])

		->add_search('term_type',array('content'=> form_dropdown(array('name'=>'where[term_type]','class'=>'form-control select2'),prepare_dropdown($this->config->item('services'),'Tất cả dịch vụ'),$this->input->get('where[term_type]'))))

		->add_search('is_first_contract',array('content'=> form_dropdown(array('name'=>'where[is_first_contract]','class'=>'form-control select2'),[ ''=>'Tất cả',1=>'Ký mới'],$this->input->get('where[is_first_contract]'))))

		->add_search('term_status',array('content'=> form_dropdown(array('name'=>'where[term_status]','class'=>'form-control select2'),prepare_dropdown($this->config->item('contract_status'),'Trạng thái HĐ : Tất cả'),$this->input->get('where[term_status]')))) ;

		$columns = array(

			'term_id' => array(
				'field' => 'term_id',
				'attrs' => array('set_select'=> FALSE, 'title'=> 'Số HĐ','set_order'=> TRUE),
				'is_required' => TRUE,
				'callback' => function($data,$row_name) {

					$term_id = $data['term_id'];

					// Mã hợp đồng
					$contract_code = get_term_meta_value($term_id,'contract_code') ?: '<code>HĐ chưa được cấp số</code>';
					
					if(has_permission('admin.contract.edit') || has_permission('admin.contract.add'))
					{
						$contract_code = anchor(admin_url("contract/create_wizard/index/{$term_id}"),$contract_code,['target'=>'_blank']);	
					}

					$contract_code = "<b>{$contract_code}</b><br/>";
					
					// Người tạo
					if($created_by = get_term_meta_value($term_id,'created_by'))
					{
						$created_display_name = $this->admin_m->get_field_by_id($created_by,'display_name'); 
						$contract_code.= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Người tạo'><i class='fa fa-fw fa-user'></i>Tạo bởi :{$created_display_name}</span>";
					}

					// Người tạo
					if($created_on = get_term_meta_value($term_id,'created_on'))
					{
						$created_datetime = my_date($created_on,'d/m/Y');
						$contract_code.= "<span class='text-muted col-xs-6' data-toggle='tooltip' title='Ngày tạo'><i class='fa fa-fw  fa-clock-o'></i>Ngày tạo :{$created_datetime}</span>";
					}

					// Get customer display name
					if(!empty($data['term_name']))
					{
						$website = anchor_popup(prep_url($data['term_name']));
						$contract_code.= "<span class='text-muted col-xs-12' data-toggle='tooltip' title='Trang thực hiện'><i class='fa fa-fw fa-globe'></i>Webpage : {$website}</span>";
					}

					// Get customer display name
					$customers = $this->term_users_m->get_the_users($term_id, ['customer_person','customer_company']);
					if( ! empty($customers))
					{
						$customer = end($customers);
						$display_name = mb_ucwords(mb_strtolower($this->admin_m->get_field_by_id($customer->user_id, 'display_name')));

						$contract_code.= br()."<span class='text-muted col-xs-12' data-toggle='tooltip' title='Khách hàng'><i class='fa fa-fw fa-user-secret'></i>KH : {$display_name}</span>";
					}

					$data['contract_code'] = $contract_code;

					$term_name = $data['term_name']?:'<code>chưa-cấp-số</code>';
					
					// Thời gian hợp đồng (từ ngày - đến ngày)
					$contract_begin = get_term_meta_value($term_id,'contract_begin');
					$contract_end 	= get_term_meta_value($term_id,'contract_end');
					$contract_daterange = '<i class="fa fa-fw fa-play" style="color:#72d072;font-size:0.8em"></i> '.($contract_begin ? my_date($contract_begin) : '--') . br();
					$contract_daterange.= '<i class="fa fa-fw fa-stop" style="color: #bb7171;font-size:0.8em"></i> '.($contract_end ? my_date($contract_end) : '--');
					$data['contract_daterange'] = $contract_daterange;

					// Thời gian kích hoạt dịch vụ
					$start_service_time = get_term_meta_value($term_id,'start_service_time');
					$data['start_service_time'] = $start_service_time ? my_date($start_service_time,'Y/m/d') : '--';

					// Giá trị hợp đồng trước VAT
					$contract_value = get_term_meta_value($term_id,'contract_value');
					$data['contract_value'] = currency_numberformat($contract_value,'đ');

					// Nhân viên kinh doanh
					$sale_id = get_term_meta_value($data['term_id'],'staff_business');
					$data['staff_business'] = $this->admin_m->get_field_by_id($sale_id,'display_name') ?: '--';

					// Loại hợp đồng
					$data['_term_type'] = $data['term_type'];
					$data['term_type'] = $this->config->item($data['term_type'],'services');

					// Hợp đồng Ký mới/ Tái ký
					$is_first_contract = get_term_meta_value($term_id,'is_first_contract');
					$data['is_first_contract'] = $is_first_contract ? '<i class="fa fa-fw fa-star"></i>' : '';

					// Trạng thái hợp đồng
					$data['_term_status'] = $data['term_status'];

					if(empty($customers))
					{
						$data['term_status'] = 'Chưa cập nhật<br/> khách hàng';
					}
					else
					{
						$data['_has_customer'] = TRUE;

						$data['term_status'] = is_numeric($data['term_status']) 
						? ($data['term_status'] > 0 ? 'publish' : 'draft')
						: $data['term_status'] ;
						$data['term_status'] = $this->config->item($data['term_status'],'contract_status');
					}

					// Actions column
					if(has_permission('admin.contract.edit'))
					{
						$data['action'] = anchor(module_url("create_wizard/index/{$term_id}"),'<i class="fa fa-fw fa-edit"></i>','title="Cập nhật hợp đồng" class="btn btn-default btn-xs"');
					}

					if(has_permission('admin.contract.add'))
					{
						$is_type_adsplus = in_array($data['_term_type'], array('google-ads'));
						$is_contract_active = in_array($data['_term_status'], array('liquidation','publish','pending','ending'));
						if($is_type_adsplus && $is_contract_active)
							$data['action'].= anchor(module_url("copy/{$data['term_id']}"),'<i class="fa fa-fw fa-copy"></i>','title="Tạo bản sao chép" class="btn btn-default btn-xs" data-toggle="confirmation"');
					}

					return $data;
					}	
			),

			'contract_code' => array(
				'field' => 'term_id',
				'attrs' => array('set_select'=> FALSE, 'title'=> 'Số HĐ','set_order'=> TRUE),
				'is_required' => TRUE,
				'callback' => function($data, $row_name) {
					$term_id = $data['term_id'];
					
				}
			),

			'contract_code', array('set_select'=> FALSE, 'title'=> 'Số HĐ','set_order'=> TRUE))
			'term.contract_daterange', array('set_select'=> FALSE,'title'=> 'T/G HĐ'))
			'term.start_service_time', array('set_select'=> FALSE,'title'=> 'T/G kích hoạt'))
			'contract_value', array('set_select'=> FALSE,'title'=> 'Giá trị trước VAT'))
			'staff_business', array('set_select'=>FALSE,'title'=>'Kinh Doanh'))
			'term.term_type','Dịch Vụ')
			'is_first_contract',array('set_select'=>FALSE,'title'=>'Ký mới','set_order'=>FALSE))
			'term.term_status','T.Thái')
			'action', array('set_select'=>FALSE,'title'=>'Actions','set_order'=>FALSE))
		);

		$columns = $this->get_default_cols();
		foreach ($columns as $column)
		{
			$this->datatable_builder->add_column($column['field'], $column['attrs']);

			if(empty($column['callback']) || ! is_callable($column['callback'])) continue;

			$this->datatable_builder->add_column_callback($column['field'], $column['callback'], FALSE);
		}

		

		->from('term');

		$status_list = array_keys($this->config->item('contract_status'));
		array_unshift($status_list, 1);

		$this->datatable_builder
		->where_in('term_status', $status_list)
		->where_in('term_type',array_keys($this->config->item('taxonomy')))
		->group_by('term.term_id');

		$pagination_config = ['per_page' => $args['per_page'],'cur_page' => $args['cur_page']];

		$data = $this->datatable_builder->generate($pagination_config);

		// OUTPUT : DOWNLOAD XLSX
		if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
		{
			$this->export($last_query);
			return TRUE;
		}

		$this->template->title->set('Quản lý hợp đồng');
		$this->template->description->set('Trang danh sách tất cả các hợp đồng');

		return parent::renderJson($data);
	}
}
/* End of file Dataset.php */
/* Location: ./application/modules/contract/controllers/ajax/Dataset.php */