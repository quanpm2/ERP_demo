<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * A base controller with a series of ajax functions
 * validation-in-model support, event callbacks and more.
 * 
 * @copyright Copyright (c) 2017, Thọ Nguyễn Hữu <thonh@webdoctor.vn>
 */

class Bailment extends Admin_Controller {

	/**
     * This model's default primary key or unique identifier.
     * Used by the get(), update() and delete() functions.
     */
	public $model = 'admin_m';


	/**
     * Initialise the model
     */
	function __construct(){

		parent::__construct();
		$this->load->config('staffs');
	}

	/**
	 * Ajax list user with bailment data
	 */
	public function list()
	{
		$response = array(
			'status' => FALSE,
			'msg' => 'Xử lý không thành công',
			'data' => array(
				'paging'=>array(
					'current_page' => null,
					'last_page' => null,
					'next_page_url' => null,
					'prev_page_url' => null,
					'num_page' => null,
				)
			)
		);

		$defaults = array(
			'order_by' => 'user.user_id',
			'order' => 'asc',
			'fields' => 'all',
			'limit' => 10,
			'current_page' => 1
		);

		$args = wp_parse_args( $this->input->get(), $defaults );

		/* Calculate total rows */
		$this->build_query($args);
		$total = $this->admin_m->select('user.user_id')->set_get_admin()->set_role([8,9,13])->count_by();
		
		/* Calculate offset index */
		$args['offset'] = ($args['current_page']-1)*$args['limit'];

		/* Query data */
		$this->build_query($args);
		$users = $this->admin_m
		->select('user.user_id,user.display_name,user.user_email,user.user_status,user.user_time_create')
		->set_get_admin()
		->set_role([8,9,13])
		->limit($args['limit'],$args['offset'])
		->as_array()
		->get_many_by();

		if(empty($users))
		{
			$response['msg'] = 'Dữ liệu không tồn tại hoặc đã bị xóa.';
			return parent::renderJson($response,204);
		}

		foreach ($users as &$user)
		{
			$user['user_phone'] = get_user_meta_value($user['user_id'],'user_phone');
			$user['bailment_level'] = get_user_meta_value($user['user_id'],'bailment_level') ?: 0;
			$user['bailment_budget'] = get_user_meta_value($user['user_id'],'bailment_budget') ?: 0;
			$user['bailment_quantity'] = get_user_meta_value($user['user_id'],'bailment_quantity') ?: 0;
		}

		$data['users'] = $users;
		$data['paging'] = array(
			'current_page' => $args['current_page'],
			'next_page_url' => ($args['current_page']*$args['limit']) < $total ? ($args['current_page']+1) : null,
			'prev_page_url' => ($args['limit'] <= $args['offset'] )? ($args['current_page']-1) : null,
			'num_page' => ceil(div($total,$args['limit']))
			);

		$response['status'] = TRUE;
		$response['msg'] = 'Dữ liệu tải thành công';
		$response['data'] = $data;

		return parent::renderJson($response,200);
	}


	/**
	 * Builds a query.
	 *
	 * @param      array  $args   The arguments
	 */
	protected function build_query($args = array())
	{
		$args = $args ?: $this->input->get();

		// User_id FILTERING & SORTING
		$filter_user_id = $args['where']['user_id'] ?? FALSE;
		$sort_user_id = $args['order_by']['user_id'] ?? FALSE;
		
		if($filter_user_id || $sort_user_id)
		{
			if($filter_user_id)
			{
				$this->admin_m->like('user.user_id',$filter_user_id);
			}

			if($sort_user_id)
			{
				$this->admin_m->order_by("user.user_id",$sort_user_id);
			}
		}

		// display_name FILTERING & SORTING
		$filter_display_name = $args['where']['display_name'] ?? FALSE;
		$sort_display_name = $args['order_by']['display_name'] ?? FALSE;
		
		if($filter_display_name || $sort_display_name)
		{
			if($filter_display_name)
			{
				$this->admin_m->like('user.display_name',$filter_display_name);
			}

			if($sort_display_name)
			{
				$this->admin_m->order_by("user.display_name",$sort_display_name);
			}
		}

		// user_email FILTERING & SORTING
		$filter_user_email = $args['where']['user_email'] ?? FALSE;
		$sort_user_email = $args['order_by']['user_email'] ?? FALSE;
		
		if($filter_user_email || $sort_user_email)
		{
			if($filter_user_email)
			{
				$this->admin_m->like('user.user_email',$filter_user_email);
			}

			if($sort_user_email)
			{
				$this->admin_m->order_by("user.user_email",$sort_user_email);
			}
		}

		// user_phone FILTERING & SORTING
		$filter_user_phone = $args['where']['user_phone'] ?? FALSE;
		$sort_user_phone = $args['order_by']['user_phone'] ?? FALSE;
		
		if($filter_user_phone || $sort_user_phone)
		{
			$alias = uniqid('user_phone_');
			$this->admin_m->join("usermeta {$alias}","{$alias}.meta_key = 'user_phone' and {$alias}.user_id = user.user_id");

			if($filter_user_phone)
			{
				$this->admin_m->like('{$alias}.meta_value',$filter_user_phone);
			}

			if($sort_user_phone)
			{
				$this->admin_m->order_by("{$alias}.meta_value",$sort_user_phone);
			}
		}

		// bailment_budget FILTERING & SORTING
		$filter_bailment_budget = $args['where']['bailment_budget'] ?? FALSE;
		$sort_bailment_budget = $args['order_by']['bailment_budget'] ?? FALSE;
		
		if($filter_bailment_budget || $sort_bailment_budget)
		{
			$alias = uniqid('bailment_budget_');
			$this->admin_m->join("usermeta {$alias}","{$alias}.meta_key = 'bailment_budget' and {$alias}.user_id = user.user_id",'LEFT');

			if($filter_bailment_budget)
			{
				$this->admin_m->like('{$alias}.meta_value',$filter_bailment_budget);
			}

			if($sort_bailment_budget)
			{
				$this->admin_m->order_by("({$alias}.meta_value)*1",$sort_bailment_budget);
			}
		}

		// bailment_quantity FILTERING & SORTING
		$filter_bailment_quantity = $args['where']['bailment_quantity'] ?? FALSE;
		$sort_bailment_quantity = $args['order_by']['bailment_quantity'] ?? FALSE;
		
		if($filter_bailment_quantity || $sort_bailment_quantity)
		{
			$alias = uniqid('bailment_quantity_');
			$this->admin_m->join("usermeta {$alias}","{$alias}.meta_key = 'bailment_quantity' and {$alias}.user_id = user.user_id",'LEFT');

			if($filter_bailment_quantity)
			{
				$this->admin_m->like('{$alias}.meta_value',$filter_bailment_quantity);
			}

			if($sort_bailment_quantity)
			{
				$this->admin_m->order_by("({$alias}.meta_value)*1",$sort_bailment_quantity);
			}
		}
	}


	/**
	 * Update bailment userdata
	 */
	public function update_userdata()
	{
		$post = $this->input->post();

		$response = array('status' => FALSE,'msg' => 'Xử lý không thành công','data' => []);
		if(empty($post['edit']['user_id']) || empty($post['meta']))
		{
			$response['data']['errors'] = ['Dữ liệu đầu vào không hợp lệ'];
			return parent::renderJson($response);
		}

		if( ! $this->admin_m->bailment_level_check($post['meta']['bailment_level']))
		{
			$response['data']['errors'] = ['Định mức nhập vào không hợp lệ'];			
			return parent::renderJson($response);
		}

		$this->load->config('staffs/staffs');
		$bailment_levels = $this->config->item('bailment_levels');

		$k_bailment_level = $k = $post['meta']['bailment_level'];
		if(empty($post['meta']['bailment_budget']))
		{
			$post['meta']['bailment_budget'] = $bailment_levels[$k]['budget'];
		}

		if(empty($post['meta']['bailment_quantity']))
		{
			$post['meta']['bailment_quantity'] = $bailment_levels[$k]['budget'];
		}

		foreach ($post['meta'] as $key => $value)
		{
			update_user_meta($post['edit']['user_id'], $key, $value);
		}

		$response['status'] = TRUE;
		$response['msg'] = 'Cập nhật thành công';
		return parent::renderJson($response,200);
	}


	public function loadQuota($user_id = 0)
	{
		$response = array('status' => FALSE,'msg' => 'Xử lý không thành công','data' => []);

		if( ! has_permission('Admin.Staffs'))
		{
			$response['msg'] = 'Quyền truy cập bị hạn chế, Xin liên hệ quản trị viên để được tiến hành cấp quyền !';
			return parent::renderJson($response); 
		}

		$user = $this->admin_m
		->select('user_id,display_name,user_email,user_status')
		->set_role([8,9,13,18])
		->set_get_active()
		->set_get_admin()->get($user_id);
		if( ! $user)
		{
			$response['msg'] = 'Thông tin user không hợp lệ !';
			return parent::renderJson($response); 
		}

		$this->load->model('staffs/sale_m');
		$quota_used = $this->sale_m->get_bailment_used($user_id);

		$quota_limit = array(
			'budget' => (int) get_user_meta_value($user_id,'bailment_budget'),
			'quantity' => (int) get_user_meta_value($user_id,'bailment_quantity')
		);

		$quota_remain = array(
			'budget' => $quota_limit['budget'] - $quota_used['budget'],
			'quantity' => $quota_limit['quantity'] - $quota_used['quantity'],
		);

		$response['data']['quota'] = array(
			'limit' => $quota_limit,
			'used' => $quota_used,
			'remain' => $quota_remain
		);

		
		$response['status'] = TRUE;
		$response['msg'] = 'Dữ liệu tải thành công';
		return parent::renderJson($response);
	}

	public function updateBailment($post_id = 0)
	{
		prd($this->input->post());
	}

	public function quota_detail($term_id = 0)
	{
		$response = array('status' => FALSE,'msg' => 'Xử lý không thành công','data' => []);

		$this->load->model('googleads/googleads_m');
		$term = $this->googleads_m->set_term_type()
		->where_in('term_status',['pending','publish','waitingforapprove','unverified'])->get($term_id);

		if( ! $term)
		{
			$response['msg'] = 'Hợp đồng không được tìm thấy hoặc không còn hiệu lực';
			return parent::renderJson($response);
		}


		$staff_business = get_term_meta_value($term->term_id,'staff_business');

		$bailment_budget = (int) get_user_meta_value($staff_business,'bailment_budget');
		$bailment_quantity = (int) get_user_meta_value($staff_business,'bailment_quantity');

		if(empty($bailment_budget) && empty($bailment_quantity))
		{
			$response['msg'] = 'Không đủ điều kiện';
			return parent::renderJson($response);
		}

		$user_contract_bailment = $this->user_m
		
		->select('user.user_id,user.display_name')
		->select('COUNT(DISTINCT term.term_id) AS "real_quantity"')
		->select('SUM(termmeta.meta_value)*COUNT(DISTINCT term.term_id)/COUNT(termmeta.term_id) AS "real_quota"')

		->join('term_users','term_users.user_id = user.user_id')
		->join('term','term.term_id = term_users.term_id')
		->join('term_posts','term.term_id = term_posts.term_id')
		->join('posts','posts.post_id = term_posts.post_id')
		->join('termmeta',"termmeta.term_id = term.term_id AND termmeta.meta_key = 'contract_value'",'LEFT')

		->where('user.user_type','admin')
		->where('posts.post_type','receipt_caution')
		->where('posts.post_status','publish')
		->where('user.user_id',$staff_business)
		->where_in('term.term_status',['waitingforapprove','pending','publish'])
		->group_by('user.user_id')
		->get_many_by();

		$response['data'] = array(
			'remain' => array(		
				'budget' => $bailment_budget,
				'quantity' => $bailment_quantity,
			)
		);

		if( ! $user_contract_bailment)
		{
			$response['status'] = TRUE;
			$response['msg'] = 'Đủ điều kiện bảo lãnh';
			return parent::renderJson($response);
		}

		$remain_budget = $bailment_budget - ((int) $user_contract_bailment->real_quota);
		$remain_quantity = $bailment_quantity - ((int) $user_contract_bailment->real_quantity);

		$response['data']['remain']['budget'] = $remain_budget;
		$response['data']['remain']['budget'] = $remain_quantity;
		$response['status'] = TRUE;

		$response['msg'] = 'Dữ liệu tải thành công';
		return parent::renderJson($response);
	}
}
/* End of file Bailment.php */
/* Location: ./application/modules/staffs/controllers/ajax/Bailment.php */