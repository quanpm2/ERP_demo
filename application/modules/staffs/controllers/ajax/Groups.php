<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * A base controller with a series of ajax functions
 * validation-in-model support, event callbacks and more.
 * 
 * @copyright Copyright (c) 2017, Thọ Nguyễn Hữu <thonh@webdoctor.vn>
 */

class Groups extends Admin_Controller {

	/**
     * This model's default primary key or unique identifier.
     * Used by the get(), update() and delete() functions.
     */
	public $model = 'user_group_m';


	/**
     * Initialise the model
     */
	function __construct(){

		parent::__construct();
		
		$models = array('user_group_m','term_users_m');
		$this->load->model($models);

		$this->load->config('staffs');
	}


	/**
	 * Ajax list user with Groups data
	 */
	public function list()
	{
		$response = array( 'status' => FALSE,'msg' => 'Xử lý không thành công','data' => array() );
		if( ! has_permission('admin.user_group.view'))
		{
			$response['msg']='Tài khoản của bạn không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response,200);
		}


		$groups = $this->user_group_m->select('term_id,term_name,term_parent,term_description,term_status,term_count')->set_term_type()->get_all();

		if(!empty($groups))
		{
			foreach ($groups as $group)
			{
				$user_ids = $this->term_users_m->get_the_users($group->term_id,'admin');
				$group->user_ids = $user_ids ? array_column($user_ids, 'user_id') : array();
				$group->term_email = get_term_meta_value($group->term_id, 'email');
			}

		}
		$response['status'] = TRUE;
		$response['msg'] = 'Dữ liệu tải thành công';
		$response['data'] = $groups;



		return parent::renderJson($response,200);
	}


	/**
	 * { function_description }
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function update($term_id = 0)
	{
		$response = array('status' => FALSE,'msg' => 'Xử lý không thành công','data' => []);

		if(empty($term_id))
		{
			if( ! has_permission('admin.user_group.add'))
			{
				$response['msg']='Tài khoản của bạn không có quyền thực hiện tác vụ này.';
				return parent::renderJson($response,200);
			}

			$user_group = $this->user_group_m->select('term_id,term_name')->get_by(['term_type'=>'user_group','term_status'=>'draft']);
			if($user_group) 
			{
				$term_id = $user_group->term_id;
			}
			else
			{
				$insert_id = $this->user_group_m->insert(['term_status'=>'draft','term_count'=>0]);
				$term_id = $insert_id;
			}
		}
		
		if( ! has_permission('admin.user_group.update'))
		{
			$response['msg']='Tài khoản của bạn không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response,200);
		}
		

		$post = $this->input->post();
		$user_group = $this->user_group_m->select('term_id,term_name,term_parent,term_description,term_count,term_status')->get($term_id);
		if( ! $user_group)
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ';
			return parent::renderJson($response);
		}
		
		/* MAPPING USER-GROUP WITH USERS */
		if( $this->input->post('user_ids') !== NULL)
		{
			$post['user_ids'] = $this->input->post('user_ids');
			$post['user_ids'] = array_filter($post['user_ids']); // Remove all empty | zero | null out of the array
			
			$this->term_users_m->set_relations_by_term($term_id, $post['user_ids'], 'admin', [], TRUE);
			$this->user_group_m->update($term_id, ['term_count'=>count($post['user_ids'])]);
		}

		/* UPDATE USER-GROUP DATA FIELDS */
		$email = '';
		if( ! empty($post['edit']))
		{
			
			/* Group name is required */
			if(empty($post['edit']['term_name']))
			{
				$response['msg'] = 'Tên nhóm là bắt buộc';
				return parent::renderJson($response);
			}
			
			if(empty($post['edit']['term_email']))
			{
				$response['msg'] = 'Email nhóm là bắt buộc';
				return parent::renderJson($response);
			}
			$email = $post['edit']['term_email'];
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$response['msg'] = 'Email không hợp lệ';
				return parent::renderJson($response);
			}
			$this->load->model('termmeta_m');

			$check_mail = $this->termmeta_m->select('term_id,meta_value')
			->where('meta_key', 'email')
			->where('meta_value',$email)
			->where_not_in('term_id',$term_id)
			->as_array()
			->get_by();
			
			if(!empty($check_mail)){
				$response['msg'] = 'Email không được trùng';
			  	return parent::renderJson($response);
			}


			unset($post['edit']['term_id']);
			$post['edit']['term_status'] 	= 'publish';
			$post['edit']['term_type'] 		= $this->user_group_m->term_type;

			$insert = array(
				'term_name' => $post['edit']['term_name'],
				'term_parent' => $post['edit']['term_parent'],
				'term_status' => $post['edit']['term_status'],
				'term_type'	  => $post['edit']['term_type'],
				'term_description' => $post['edit']['term_description']
			);

			if($this->user_group_m->update($term_id,$insert))
			{
				if(!empty($insert['term_parent']))
				{
					
					$all_group = $this->user_group_m->select('term_id,term_name,term_parent,term_description,term_status,term_count')->set_term_type()
					->where('term_parent',$insert['term_parent'])->get_all();
					
					if(!empty($all_group))
					{
						$this->load->model('staffs/department_m');
						$this->department_m->update((int)$insert['term_parent'],['term_count'=>count($all_group)]);
					}
				}
				update_term_meta($term_id, 'email',$email);
			}

		}

		$response['status'] = TRUE;
		$response['msg'] = 'Cập nhật thành công thông tin nhóm';
		/* Setting response */
		$response['data'] = array(
			'term_id' => $term_id,
			'term_name' => $post['edit']['term_name'] ?? $user_group->term_name,
			'term_count' => $post['edit']['term_count'] ?? $user_group->term_count,
			'term_status' => $post['edit']['term_status'] ?? $user_group->term_status,
			'term_parent' => $post['edit']['term_parent'] ?? $user_group->term_parent,
			'term_description' => $post['edit']['term_description'] ?? $user_group->term_description,
			'term_email'  => $email ?? $user_group->term_email,
			'user_ids'	=> []
		);

		return parent::renderJson($response,200);
	}
	

	public function delete($term_id = 0)
	{
		$response = array('status' => FALSE,'msg' => 'Xử lý không thành công','data' => []);

		if( ! has_permission('admin.user_group.delete'))
		{
			$response['msg'] = 'Tài khoản của bạn không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}

		$group = $this->user_group_m->set_term_type()->select('term_id,term_parent')->get($term_id);
		if( ! $group)
		{
			$response['msg'] = 'Dữ liệu không tồn tại hoặc đã bị xóa.';
			return parent::renderJson($response);
		}

		$user_ids = $this->term_users_m->get_the_users($group->term_id, 'admin');
		if(!empty($user_ids))
		{
			$response['msg'] = 'Không thể xóa nhóm vì nhóm đang chứa các thành viên';
			return parent::renderJson($response);
		}
		$department_id = $group->term_parent;
		if($this->user_group_m->set_term_type()->delete($term_id))
		{
			if(!empty($department_id))
			{
				$this->load->model('staffs/department_m');
				$department = $this->department_m->select('term_id,term_name,term_parent,term_description,term_status,term_count')->set_term_type()
				->where('term_id',$department_id)->get_all();
				if(!empty($department))
				{
					$this->department_m->update($department[0]->term_id,['term_count'=>$department[0]->term_count -1]);
				}
			}
			$this->load->model('termmeta_m');
			$this->termmeta_m->delete_meta($term_id, 'email');
		}

		$response['status'] = TRUE;
		$response['msg'] = 'Hệ thống đã thành công !';
		return parent::renderJson($response);
	}

	/**
	 * Builds a query.
	 * 
	 * @param      array  $args   The arguments
	 */
	protected function build_query($args = array())
	{
		$args = $args ?: $this->input->get();

		// User_id FILTERING & SORTING
		$filter_user_id = $args['where']['user_id'] ?? FALSE;
		$sort_user_id = $args['order_by']['user_id'] ?? FALSE;
		
		if($filter_user_id || $sort_user_id)
		{
			if($filter_user_id)
			{
				$this->admin_m->like('user.user_id',$filter_user_id);
			}

			if($sort_user_id)
			{
				$this->admin_m->order_by("user.user_id",$sort_user_id);
			}
		}

		// display_name FILTERING & SORTING
		$filter_display_name = $args['where']['display_name'] ?? FALSE;
		$sort_display_name = $args['order_by']['display_name'] ?? FALSE;
		
		if($filter_display_name || $sort_display_name)
		{
			if($filter_display_name)
			{
				$this->admin_m->like('user.display_name',$filter_display_name);
			}

			if($sort_display_name)
			{
				$this->admin_m->order_by("user.display_name",$sort_display_name);
			}
		}

		// user_email FILTERING & SORTING
		$filter_user_email = $args['where']['user_email'] ?? FALSE;
		$sort_user_email = $args['order_by']['user_email'] ?? FALSE;
		
		if($filter_user_email || $sort_user_email)
		{
			if($filter_user_email)
			{
				$this->admin_m->like('user.user_email',$filter_user_email);
			}

			if($sort_user_email)
			{
				$this->admin_m->order_by("user.user_email",$sort_user_email);
			}
		}

		// user_phone FILTERING & SORTING
		$filter_user_phone = $args['where']['user_phone'] ?? FALSE;
		$sort_user_phone = $args['order_by']['user_phone'] ?? FALSE;
		
		if($filter_user_phone || $sort_user_phone)
		{
			$alias = uniqid('user_phone_');
			$this->admin_m->join("usermeta {$alias}","{$alias}.meta_key = 'user_phone' and {$alias}.user_id = user.user_id");

			if($filter_user_phone)
			{
				$this->admin_m->like('{$alias}.meta_value',$filter_user_phone);
			}

			if($sort_user_phone)
			{
				$this->admin_m->order_by("{$alias}.meta_value",$sort_user_phone);
			}
		}

		// Groups_budget FILTERING & SORTING
		$filter_Groups_budget = $args['where']['Groups_budget'] ?? FALSE;
		$sort_Groups_budget = $args['order_by']['Groups_budget'] ?? FALSE;
		
		if($filter_Groups_budget || $sort_Groups_budget)
		{
			$alias = uniqid('Groups_budget_');
			$this->admin_m->join("usermeta {$alias}","{$alias}.meta_key = 'Groups_budget' and {$alias}.user_id = user.user_id",'LEFT');

			if($filter_Groups_budget)
			{
				$this->admin_m->like('{$alias}.meta_value',$filter_Groups_budget);
			}

			if($sort_Groups_budget)
			{
				$this->admin_m->order_by("({$alias}.meta_value)*1",$sort_Groups_budget);
			}
		}

		// Groups_quantity FILTERING & SORTING
		$filter_Groups_quantity = $args['where']['Groups_quantity'] ?? FALSE;
		$sort_Groups_quantity = $args['order_by']['Groups_quantity'] ?? FALSE;
		
		if($filter_Groups_quantity || $sort_Groups_quantity)
		{
			$alias = uniqid('Groups_quantity_');
			$this->admin_m->join("usermeta {$alias}","{$alias}.meta_key = 'Groups_quantity' and {$alias}.user_id = user.user_id",'LEFT');

			if($filter_Groups_quantity)
			{
				$this->admin_m->like('{$alias}.meta_value',$filter_Groups_quantity);
			}

			if($sort_Groups_quantity)
			{
				$this->admin_m->order_by("({$alias}.meta_value)*1",$sort_Groups_quantity);
			}
		}
	}


	/**
	 * Update Groups userdata
	 */
	public function update_userdata()
	{
		$post = $this->input->post();

		$response = array('status' => FALSE,'msg' => 'Xử lý không thành công','data' => []);
		if(empty($post['edit']['user_id']) || empty($post['meta']))
		{
			$response['data']['errors'] = ['Dữ liệu đầu vào không hợp lệ'];
			return parent::renderJson($response);
		}

		if( ! $this->admin_m->Groups_level_check($post['meta']['Groups_level']))
		{
			$response['data']['errors'] = ['Định mức nhập vào không hợp lệ'];			
			return parent::renderJson($response);
		}

		$this->load->config('staffs/staffs');
		$Groups_levels = $this->config->item('Groups_levels');

		$k_Groups_level = $k = $post['meta']['Groups_level'];
		if(empty($post['meta']['Groups_budget']))
		{
			$post['meta']['Groups_budget'] = $Groups_levels[$k]['budget'];
		}

		if(empty($post['meta']['Groups_quantity']))
		{
			$post['meta']['Groups_quantity'] = $Groups_levels[$k]['budget'];
		}

		foreach ($post['meta'] as $key => $value)
		{
			update_user_meta($post['edit']['user_id'], $key, $value);
		}

		$response['status'] = TRUE;
		$response['msg'] = 'Cập nhật thành công';
		return parent::renderJson($response,200);
	}


	public function loadQuota($user_id = 0)
	{
		$response = array('status' => FALSE,'msg' => 'Xử lý không thành công','data' => []);

		if( ! has_permission('Admin.Staffs'))
		{
			$response['msg'] = 'Quyền truy cập bị hạn chế, Xin liên hệ quản trị viên để được tiến hành cấp quyền !';
			return parent::renderJson($response); 
		}

		$user = $this->admin_m
		->select('user_id,display_name,user_email,user_status')
		->set_role([8,9,13,18])
		->set_get_active()
		->set_get_admin()->get($user_id);
		if( ! $user)
		{
			$response['msg'] = 'Thông tin user không hợp lệ !';
			return parent::renderJson($response); 
		}

		$this->load->model('staffs/sale_m');
		$quota_used = $this->sale_m->get_Groups_used($user_id);

		$quota_limit = array(
			'budget' => (int) get_user_meta_value($user_id,'Groups_budget'),
			'quantity' => (int) get_user_meta_value($user_id,'Groups_quantity')
		);

		$quota_remain = array(
			'budget' => $quota_limit['budget'] - $quota_used['budget'],
			'quantity' => $quota_limit['quantity'] - $quota_used['quantity'],
		);

		$response['data']['quota'] = array(
			'limit' => $quota_limit,
			'used' => $quota_used,
			'remain' => $quota_remain
		);

		
		$response['status'] = TRUE;
		$response['msg'] = 'Dữ liệu tải thành công';
		return parent::renderJson($response);
	}


	public function updateGroups($post_id = 0)
	{
		prd($this->input->post());
	}


	public function quota_detail($term_id = 0)
	{
		$response = array('status' => FALSE,'msg' => 'Xử lý không thành công','data' => []);

		$this->load->model('googleads/googleads_m');
		$term = $this->googleads_m->set_term_type()
		->where_in('term_status',['pending','publish','waitingforapprove','unverified'])->get($term_id);

		if( ! $term)
		{
			$response['msg'] = 'Hợp đồng không được tìm thấy hoặc không còn hiệu lực';
			return parent::renderJson($response);
		}


		$staff_business = get_term_meta_value($term->term_id,'staff_business');

		$Groups_budget = (int) get_user_meta_value($staff_business,'Groups_budget');
		$Groups_quantity = (int) get_user_meta_value($staff_business,'Groups_quantity');

		if(empty($Groups_budget) && empty($Groups_quantity))
		{
			$response['msg'] = 'Không đủ điều kiện';
			return parent::renderJson($response);
		}

		$user_contract_Groups = $this->user_m
		
		->select('user.user_id,user.display_name')
		->select('COUNT(DISTINCT term.term_id) AS "real_quantity"')
		->select('SUM(termmeta.meta_value)*COUNT(DISTINCT term.term_id)/COUNT(termmeta.term_id) AS "real_quota"')

		->join('term_users','term_users.user_id = user.user_id')
		->join('term','term.term_id = term_users.term_id')
		->join('term_posts','term.term_id = term_posts.term_id')
		->join('posts','posts.post_id = term_posts.post_id')
		->join('termmeta',"termmeta.term_id = term.term_id AND termmeta.meta_key = 'contract_value'",'LEFT')

		->where('user.user_type','admin')
		->where('posts.post_type','receipt_caution')
		->where('posts.post_status','publish')
		->where('user.user_id',$staff_business)
		->where_in('term.term_status',['waitingforapprove','pending','publish'])
		->group_by('user.user_id')
		->get_many_by();

		$response['data'] = array(
			'remain' => array(		
				'budget' => $Groups_budget,
				'quantity' => $Groups_quantity,
			)
		);

		if( ! $user_contract_Groups)
		{
			$response['status'] = TRUE;
			$response['msg'] = 'Đủ điều kiện bảo lãnh';
			return parent::renderJson($response);
		}

		$remain_budget = $Groups_budget - ((int) $user_contract_Groups->real_quota);
		$remain_quantity = $Groups_quantity - ((int) $user_contract_Groups->real_quantity);

		$response['data']['remain']['budget'] = $remain_budget;
		$response['data']['remain']['budget'] = $remain_quantity;
		$response['status'] = TRUE;

		$response['msg'] = 'Dữ liệu tải thành công';
		return parent::renderJson($response);
	}
}
/* End of file Groups.php */
/* Location: ./application/modules/staffs/controllers/ajax/Groups.php */