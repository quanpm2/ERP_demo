<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * A base controller with a series of ajax functions
 * validation-in-model support, event callbacks and more.
 * 
 * @copyright Copyright (c) 2017, Thọ Nguyễn Hữu <thonh@webdoctor.vn>
 */

class Users extends Admin_Controller {

	/**
     * This model's default primary key or unique identifier.
     * Used by the get(), update() and delete() functions.
     */
	public $model = 'admin_m';

	/**
     * Initialise the model
     */
	function __construct(){

		parent::__construct();
		
		$models = array('user_group_m','term_users_m');
		$this->load->model($models);

		$this->load->config('staffs');
	}


	/**
	 * Get Detail Infomation of user
	 *
	 * @param      integer  $user_id  The user identifier
	 */
	public function view($user_id = 0)
	{
		$response = array('status'=>FALSE,'msg'=>'Xử lý không thành công','data'=>array());

		if( ! has_permission('Admin.Staffs.View'))
		{
			$response['msg'] = 'Không có quyền truy cập thông tin user này.';
			return parent::renderJson($response);
		}

		$user = $this->admin_m->select('role_id,display_name,user_id,user_avatar,user_email,user_status')->set_get_admin()->get($user_id);
		if( ! $user)
		{
			$response['msg'] = 'Không tìm thấy thông tin user';
			return parent::renderJson($response);
		}

		$user->role 	= $this->role_m->select('role_id,role_name,role_description')->get($user->role_id);	
		$user->gender 	= (int) get_user_meta_value($user_id, 'gender');
		$user->phone 	= get_user_meta_value($user_id, 'user_phone');

		$user->phone_ext 		= get_user_meta_value($user_id, 'phone-ext');
		$user->phone_private 	= get_user_meta_value($user_id, 'user_phone_private');
		$user->user_working_day	= my_date(get_user_meta_value($user_id, 'user_working_day'),'d-m-Y') ;
		$user->user_zone 		= get_user_meta_value($user_id, 'user_zone');
		$user->show_info 		= get_user_meta_value($user_id, 'show_info');

		$user->group_id 		= 0;
		$user->department_id 	= 0;

		$_domains_hotline = get_user_meta_value($user_id, '_domains_hotline');
		$user->_domains_hotline = is_serialized($_domains_hotline) ? unserialize($_domains_hotline) : [];

		$this->load->model('term_users_m');
		$this->load->model('staffs/user_group_m');
		$user_groups = $this->term_users_m->get_user_terms($user_id, $this->user_group_m->term_type);
		if( ! empty($user_groups))
		{
			$user_group = reset($user_groups);
			$user->group_id = $user_group->term_id;
		}

		$this->load->model('staffs/department_m');
		$departments = $this->term_users_m->get_user_terms($user_id, $this->department_m->term_type);
		if( ! empty($departments))
		{
			$department = reset($departments);
			$user->department_id = $department->term_id;
		}

		$response['data'] = $user;
		$response['status'] = TRUE;
		$response['msg'] = 'Hệ thống xử lý hoàn tất .';

		return parent::renderJson($response);
	}


	/**
	 * Ajax list user with User data
	 */
	public function list($user_group_id=0)
	{
		$response = array('status'=>FALSE,'msg'=>'Xử lý không thành công','data'=>array());

		$this->load->config('group');

		$role_ids 	= array_merge($this->config->item('salesexecutive','groups'), $this->config->item('webdoctor-member','groups'));
		$defaults 	= array('role_id'=>$role_ids, 'order_by' => 'user.user_id', 'order' => 'asc','user_status'=>1);
		$args		= wp_parse_args( $this->input->get(), $defaults );
		$args_encrypt 	= md5(json_encode($args));
		
		/* Calculate total rows */
		$this->build_query($args);
		$users = $this->admin_m
		->select('user.user_id,user.display_name,user.user_email,user.user_status,user.user_time_create,role.role_id,role.role_name')
		->join('role','role.role_id = user.role_id')
		->set_get_admin()
		->where_in('user.role_id',$role_ids)
		->get_all();

		$response['status'] = TRUE;
		$response['msg'] = 'Dữ liệu tải thành công';
		$response['data'] = $users;

		return parent::renderJson($response,200);
	}


	/**
	 * Update user's infomation
	 *
	 * @param      int  $user_id  The user ID
	 */
	public function update($user_id = 0)
	{
		$response = array('status' => FALSE, 'msg' => 'Xử lý không thành công', 'data' => []);
		$user_id = absint($user_id);

		if( ! has_permission('admin.staffs.update'))
		{
			$response['msg'] = 'Không thể thực hiện tác vụ do quyền truy cập bị hạn chế.';
			return parent::renderJson($response);
		}

		$post = $this->input->post();

		if(empty($post) || empty($user_id)) 
		{
			$response['msg'] = 'Tham số truyền vào không hợp lệ !';
			return parent::renderJson($response);
		}
		
		$user = $this->admin_m->set_get_admin()->get($user_id);
		if( ! $user)
		{
			$response['msg'] = 'Tham số truyền vào không hợp lệ.';
			return parent::renderJson($response);
		}

		$edit = $this->input->post('edit', TRUE);

		if( isset($edit['user_group']))
		{
			$this->term_users_m->set_user_terms($user->user_id, (int) $edit['user_group'], 'user_group');
			unset($edit['user_group']);
		}

		if( isset($edit['department_id']))
		{
			$this->term_users_m->set_user_terms($user->user_id, (int) $edit['department_id'], 'department');
			unset($edit['department_id']);
		}

		$this->admin_m->update($user_id, $edit);

		$meta = $this->input->post('meta', TRUE);
		if( ! empty($meta))
		{
			$meta['user_working_day'] = !empty($meta['user_working_day']) ? strtotime($meta['user_working_day']) : 0;

			$meta['_domains_hotline'] = isset($meta['_domains_hotline']) ? ($meta['_domains_hotline'] ?: []) : [];
			$meta['_domains_hotline'] = serialize($meta['_domains_hotline']);

			foreach ($meta as $key => $value) update_user_meta($user_id, $key, $value);
		}

		$response['status'] = TRUE;
		$response['msg'] 	= 'Cập nhật thành công';
		return parent::renderJson($response);
	}


	/**
	 * List All roles
	 *
	 * @param      string  $value  The value
	 */
	public function list_roles()
	{
		$response = array('status'=>FALSE,'msg'=>'Xử lý không thành công','data'=>array());

		if( ! has_permission('Admin.Staffs.View'))
		{
			$response['msg'] = 'Không có quyền truy cập thông tin user này.';
			return parent::renderJson($response);
		}

		if($this->admin_m->role_id != 1) $this->role_m->where_not_in('role_id', [1,4,5]);
		$roles = $this->role_m->select('role_id,role_name')->get_all();

		if( ! $roles)
		{
			$response['msg'] = 'Không tìm thấy thông tin user';
			return parent::renderJson($response);
		}

		$response['data'] 	= $roles;
		$response['status'] = TRUE;
		$response['msg'] 	= 'Hệ thống xử lý hoàn tất .';
		return parent::renderJson($response);
	}


	/**
	 * List All Departments
	 *
	 * @param      string  $value  The value
	 */
	public function list_departments()
	{
		$response = array('status'=>FALSE,'msg'=>'Xử lý không thành công','data'=>array());

		if( ! has_permission('Admin.Staffs.View'))
		{
			$response['msg'] = 'Không có quyền truy cập thông tin phòng ban .';
			return parent::renderJson($response);
		}

		$this->load->model('staffs/department_m');

		$departments = $this->department_m->select('term_id,term_name,term_description')->set_term_type()->get_all();
		if( ! $departments)
		{
			$response['msg'] = 'Không tìm thấy thông tin';
			return parent::renderJson($response);
		}

		$response['status'] = TRUE;
		$response['data'] 	= $departments;
		$response['msg'] 	= 'Hệ thống xử lý hoàn tất .';
		return parent::renderJson($response);
	}


	/**
	 * List All Groups
	 *
	 * @param      string  $value  The value
	 */
	public function list_groups($department_id = 0)
	{
		$response = array('status'=>FALSE,'msg'=>'Xử lý không thành công','data'=>array());

		if( ! has_permission('Admin.Staffs.View') 
			// || ($this->admin_m->id !== $user_id && !has_permission('Admin.Staffs.Manage'))
			)
		{
			$response['msg'] = 'Không có quyền truy cập thông tin phòng ban .';
			return parent::renderJson($response);
		}

		$this->load->model('staffs/user_group_m');

		$groups = $this->user_group_m->select('term_id,term_name,term_description,term_parent')->set_term_type()
		->where('term_parent', $department_id)
		->get_all();
		if( ! $groups)
		{
			$response['msg'] = 'Không tìm thấy thông tin';
			return parent::renderJson($response);
		}

		$response['status'] = TRUE;
		$response['data'] 	= $groups;
		$response['msg'] 	= 'Hệ thống xử lý hoàn tất .';
		return parent::renderJson($response);
	}


	/**
	 * List config zones
	 *
	 * @return     json
	 */
	public function list_zones()
	{
		$this->load->config('staffs/group');
		$zone = $this->config->item('zone');
		$response['data'] 	= $zone;
		return parent::renderJson($response);
	}


	/**
	 * Builds a query.
	 *
	 * @param      array  $args   The arguments
	 */
	protected function build_query($args = array())
	{
		$args = $args ?: $this->input->get();

		// User_id FILTERING & SORTING
		$filter_user_id = $args['where']['user_id'] ?? FALSE;
		$sort_user_id = $args['order_by']['user_id'] ?? FALSE;
		
		if($filter_user_id || $sort_user_id)
		{
			if($filter_user_id)
			{
				$this->admin_m->like('user.user_id',$filter_user_id);
			}

			if($sort_user_id)
			{
				$this->admin_m->order_by("user.user_id",$sort_user_id);
			}
		}


		// user_status FILTERING & SORTING
		$filter_user_status = $args['where']['user_status'] ?? FALSE;
		$sort_user_status = $args['order_by']['user_status'] ?? FALSE;
		
		if($filter_user_status || $sort_user_status)
		{
			if($filter_user_status)
			{
				$this->admin_m->where('user.user_status',$filter_user_status);
			}

			if($sort_user_status)
			{
				$this->admin_m->order_by("user.user_status",$sort_user_status);
			}
		}


		// display_name FILTERING & SORTING
		$filter_display_name = $args['where']['display_name'] ?? FALSE;
		$sort_display_name = $args['order_by']['display_name'] ?? FALSE;
		
		if($filter_display_name || $sort_display_name)
		{
			if($filter_display_name)
			{
				$this->admin_m->like('user.display_name',$filter_display_name);
			}

			if($sort_display_name)
			{
				$this->admin_m->order_by("user.display_name",$sort_display_name);
			}
		}

		// user_email FILTERING & SORTING
		$filter_user_email = $args['where']['user_email'] ?? FALSE;
		$sort_user_email = $args['order_by']['user_email'] ?? FALSE;
		
		if($filter_user_email || $sort_user_email)
		{
			if($filter_user_email)
			{
				$this->admin_m->like('user.user_email',$filter_user_email);
			}

			if($sort_user_email)
			{
				$this->admin_m->order_by("user.user_email",$sort_user_email);
			}
		}

		// user_phone FILTERING & SORTING
		$filter_user_phone = $args['where']['user_phone'] ?? FALSE;
		$sort_user_phone = $args['order_by']['user_phone'] ?? FALSE;
		
		if($filter_user_phone || $sort_user_phone)
		{
			$alias = uniqid('user_phone_');
			$this->admin_m->join("usermeta {$alias}","{$alias}.meta_key = 'user_phone' and {$alias}.user_id = user.user_id");

			if($filter_user_phone)
			{
				$this->admin_m->like('{$alias}.meta_value',$filter_user_phone);
			}

			if($sort_user_phone)
			{
				$this->admin_m->order_by("{$alias}.meta_value",$sort_user_phone);
			}
		}

		// bailment_budget FILTERING & SORTING
		$filter_bailment_budget = $args['where']['bailment_budget'] ?? FALSE;
		$sort_bailment_budget = $args['order_by']['bailment_budget'] ?? FALSE;
		
		if($filter_bailment_budget || $sort_bailment_budget)
		{
			$alias = uniqid('bailment_budget_');
			$this->admin_m->join("usermeta {$alias}","{$alias}.meta_key = 'bailment_budget' and {$alias}.user_id = user.user_id",'LEFT');

			if($filter_bailment_budget)
			{
				$this->admin_m->like('{$alias}.meta_value',$filter_bailment_budget);
			}

			if($sort_bailment_budget)
			{
				$this->admin_m->order_by("({$alias}.meta_value)*1",$sort_bailment_budget);
			}
		}

		// bailment_quantity FILTERING & SORTING
		$filter_bailment_quantity = $args['where']['bailment_quantity'] ?? FALSE;
		$sort_bailment_quantity = $args['order_by']['bailment_quantity'] ?? FALSE;
		
		if($filter_bailment_quantity || $sort_bailment_quantity)
		{
			$alias = uniqid('bailment_quantity_');
			$this->admin_m->join("usermeta {$alias}","{$alias}.meta_key = 'bailment_quantity' and {$alias}.user_id = user.user_id",'LEFT');

			if($filter_bailment_quantity)
			{
				$this->admin_m->like('{$alias}.meta_value',$filter_bailment_quantity);
			}

			if($sort_bailment_quantity)
			{
				$this->admin_m->order_by("({$alias}.meta_value)*1",$sort_bailment_quantity);
			}
		}
	}
}
/* End of file Users.php */
/* Location: ./application/modules/staffs/controllers/ajax/Users.php */