<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * A base controller with a series of ajax functions
 * validation-in-model support, event callbacks and more.
 * 
 * @copyright Copyright (c) 2017, Hùng Lãm <lamdh@webdoctor.vn>
 */

class Departments extends Admin_Controller {

	/**
     * This model's default primary key or unique identifier.
     * Used by the get(), update() and delete() functions.
     */
	public $model = 'Department_m';


	/**
     * Initialise the model
     */
	function __construct(){

		parent::__construct();
		
		$models = array('department_m','user_group_m','term_users_m');
		$this->load->model($models);

		$this->load->config('staffs');
	}


	/**
	 * Ajax list user with Departments data
	 */
	public function list()
	{
		$response = array( 'status' => FALSE,'msg' => 'Xử lý không thành công','data' => array() );

		if( ! has_permission('admin.department.view'))
		{
			$response['msg']='Tài khoản của bạn không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response,200);
		}

		$departments = $this->department_m->select('term_id,term_name,term_parent,term_description,term_status,term_count')->set_term_type()->get_all();

		if(!empty($departments))
		{
			foreach ($departments as $department)
			{
				$user_group = $this->user_group_m->select('term_id,term_name,term_parent,term_description,term_status,term_count')->set_term_type()->where('term_parent',$department->term_id)->get_all();

				$department->user_group = !empty($user_group)  ? array_column($user_group, 'term_id') : array();

				$department->term_email = get_term_meta_value($department->term_id, 'email');
			}
		}
		
		$response['status'] = TRUE;
		$response['msg'] = 'Dữ liệu tải thành công';
		$response['data'] = $departments;

		return parent::renderJson($response,200);
	}

	public function list_user_groups($department_id=0)
	{
		$response = array('status'=>FALSE,'msg'=>'Không tìm thấy dữ liệu','data'=>array());
		if( ! has_permission('admin.department.view'))
		{
			$response['msg']='Tài khoản của bạn không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response,200);
		}

		$user_group = $this->user_group_m->select('term_id,term_name,term_parent,term_description,term_status,term_count')->set_term_type()->where_in('term_parent',array($department_id,0))->get_all();
		if(empty($user_group))
		{
			return parent::renderJson($response,204);//204 khong co noi dung
		}

		$response['status'] = TRUE;
		$response['msg'] = 'Dữ liệu tải thành công';
		$response['data'] = $user_group;

		return parent::renderJson($response,200);
	}


	/**
	 * { function_description }
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function update($term_id = 0)
	{
		$response = array('status' => FALSE,'msg' => 'Xử lý không thành công','data' => []);
		

		if(empty($term_id))
		{
			if( ! has_permission('admin.department.add'))
			{
				$response['msg']='Tài khoản của bạn không có quyền thực hiện tác vụ này.';
				return parent::renderJson($response,200);
			}

			$department = $this->department_m->select('term_id,term_name')->get_by(['term_type'=>'department','term_status'=>'draft']);
			if($department) 
			{
				$term_id = $department->term_id;
			}
			else
			{
				$insert_id = $this->department_m->insert(['term_status'=>'draft','term_count'=>0]);
				$term_id = $insert_id;
			}
		}

		if( ! has_permission('admin.department.update'))
		{
			$response['msg']='Tài khoản của bạn không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response,200);
		}

		$post = $this->input->post();
		$department = $this->department_m->select('term_id,term_name,term_parent,term_description,term_count,term_status')->get($term_id);

		if( ! $department)
		{
			$response['msg'] = 'Dữ liệu đầu vào không hợp lệ';
			return parent::renderJson($response);
		}
		
		/* MAPPING Department  WITH USER-GROUP */
		if( $this->input->post('user_group') !== NULL)
		{
			$post['user_group'] = $this->input->post('user_group');
			$post['user_group'] = array_filter($post['user_group']); // Remove all empty | zero | null out of the array
			
			$user_groups = $this->user_group_m->select('term_id')->where('term_parent',$term_id)->as_array()->get_all();

			$group_users = array();
			foreach ($user_groups as $user_group) 
			{
				array_push($group_users, implode('',$user_group));
			}
			
			$delete_user_groups = array_diff($group_users,$post['user_group']);
			foreach ($delete_user_groups as $key => $user_group_id) {
				$this->user_group_m->update($user_group_id,['term_parent'=>0]);
			}
			if(!empty($post['user_group'])){
				foreach ($post['user_group'] as $key => $user_group_id) {
					
					$this->user_group_m->update($user_group_id,['term_parent'=>$term_id]);
				}
			}
			//$this->term_users_m->set_relations_by_term($term_id, $post['user_group'], 'admin', [], TRUE);
			$this->department_m->update($term_id, ['term_count'=>count($post['user_group'])]);
		}

		/* UPDATE USER-GROUP DATA FIELDS */
		$email = '';
		if( ! empty($post['edit']))
		{
			
			/* Group name is required */
			if(empty($post['edit']['term_name']))
			{
				$response['msg'] = 'Tên phòng ban là bắt buộc';
				return parent::renderJson($response);
			}
			
			if(empty($post['edit']['term_email']))
			{
				$response['msg'] = 'Email phòng ban là bắt buộc';
				return parent::renderJson($response);
			}
			$email = $post['edit']['term_email'];
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$response['msg'] = 'Email không hợp lệ';
				return parent::renderJson($response);
			}
			$this->load->model('termmeta_m');

			$check_mail = $this->termmeta_m->select('term_id,meta_value')
			->where('meta_key','email')
			->where('meta_value',$email)
			->where_not_in('term_id',$term_id)
			->as_array()
			->get_by();

			if(!empty($check_mail)){
				$response['msg'] = 'Email không được trùng';
			  	return parent::renderJson($response);
			}

			unset($post['edit']['term_id']);
			$post['edit']['term_status'] 	= 'publish';
			$post['edit']['term_type'] 		= $this->department_m->term_type;

			$insert = array(
				'term_name' => $post['edit']['term_name'],
				'term_status' => $post['edit']['term_status'],
				'term_type'	  => $post['edit']['term_type'],
				'term_description' => $post['edit']['term_description']
			);

			$this->department_m->update($term_id,$insert);
			
  			update_term_meta($term_id,'email',$email);

		}

		$response['status'] = TRUE;
		$response['msg'] = 'Cập nhật thành công thông tin phòng ban';
		/* Setting response */
		$response['data'] = array(
			'term_id' => $term_id,
			'term_name' => $post['edit']['term_name'] ?? $department->term_name,
			'term_count' => $post['edit']['term_count'] ?? $department->term_count,
			'term_status' => $post['edit']['term_status'] ?? $department->term_status,
			'term_description' => $post['edit']['term_description'] ?? $department->term_description,
			'term_email'  => $email ?? $department->term_email,
			'user_group'	=> []
		);

		return parent::renderJson($response,200);
	}
	

	public function delete($term_id = 0)
	{
		$response = array('status' => FALSE,'msg' => 'Xử lý không thành công','data' => []);

		if( ! has_permission('admin.department.delete'))
		{
			$response['msg'] = 'Tài khoản của bạn không có quyền thực hiện tác vụ này.';
			return parent::renderJson($response);
		}
		$group = $this->department_m->set_term_type()->select('term_id')->get($term_id);
		if( ! $group)
		{
			$response['msg'] = 'Dữ liệu không tồn tại hoặc đã bị xóa.';
			return parent::renderJson($response);
		}

		$user_group = $this->user_group_m->select('term_id,term_name,term_parent,term_description,term_status,term_count')->set_term_type()->where('term_parent',$term_id)->get_all();
		if(!empty($user_group))
		{
			$response['msg'] = 'Không thể xóa phòng ban vì đang chứa các phòng ban';
			return parent::renderJson($response);
		}

		$this->department_m->set_term_type()->delete($term_id);

		$this->load->model('termmeta_m');

		$this->termmeta_m->delete_meta($term_id, 'email');

		$response['status'] = TRUE;
		$response['msg'] = 'Hệ thống đã thành công !';
		return parent::renderJson($response);
	}

}
/* End of file departments.php */
/* Location: ./application/modules/staffs/controllers/ajax/departments.php */