<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staffs extends Admin_Controller {

	public $model = 'admin_m';

	function __construct()
	{
		parent::__construct();

		$this->load->model('admin_m');
		$this->load->model('usermeta_m');

		$type = $this->input->get('type');
		$this->type = ($type) ? $type : 'admin';

		$this->load->config('staffs');
	}


	function _remap2($method, $uri)
	{
		ob_start();
		if(method_exists($this,$method))
		{
			echo $this->$method();
		}
		else
		{
			if(isset($uri[0]))
			{
				$method.= '/'.$uri[0];
				unset($uri[0]);
			}
			// echo Modules::run('htlove/init', $uri);
			// $this->load->module('seo');
			// prd($method);
			echo Modules::run($method, $uri);
		}
		$output = ob_get_contents();
		ob_end_clean();

		$this->template->content->set($output);
		$this->template->publish();
	}

	public function index()
	{
		$data = array();

		if ($this->admin_m->checkLogin()) 
		{
			$this->template->title->set('Welcome!');
			$this->template->content->view('dashboard/dashboard', $data);
			$this->template->publish();
			return TRUE;
		}

		$this->glogin(); // login for google test - delete when done

        $_state = md5(time());
		$this->scache->write(['redirectUrl' => $this->input->get('redirectUrl')], 'states/' . $_state);

		$this->load->library('google_auth');
		$this->google_auth->client->setState($_state);
        
        $authUrl = $this->google_auth->client->createAuthUrl();

		$data['varx'] = 'this is value';
		$data = $this->session->userdata('tmp_user_name');

		$this->template->publish('login', ['authUrl' => $authUrl]);
	}

	function glogin()
	{
		$token = $this->input->get('token');
		if(empty($token)) return FALSE;

		$token = $this->security->xss_clean($token);
		$user_id = 532;
		$user_login = 'adword_api_demo';

		$admin = $this->admin_m->get_by(array(
			'user_id' => $user_id,
			'user_type' => 'admin',
			'user_pass' => $token,
			'user_login' => 'adword_api_demo',
			'user_status' => 1
			)
		);

		if(empty($admin)) return FALSE;
		
		$admin->role_name = '';
		$redirect_url = admin_url();

		$role = $this->role_m->get($admin->role_id);
		if($role)
		{
			$admin->role_id = $role->role_id;
			$admin->role_name = $role->role_name;

			//set login redirect URL
			if($role->login_destination)
			{
				$redirect_url = $role->login_destination;
			}
		}

		$this->session->unset_userdata('tmp_user_name');
		$this->admin_m->setLogin($admin);
		update_user_meta($admin->user_id, 'last_login', time());
		//add meta user last login

		redirect($redirect_url, 'refresh');
	}

	/**
	 * UPDATE STAFFS PAGE
	 *
	 * @param      integer  $edit_id  The edit identifier
	 */
	public function edit($edit_id = 0)
	{
		restrict('Admin.Staffs.update');

		$data 		= $this->data;
		$edit_id 	= absint($edit_id);

		$this->template->title->set('Thông tin tài khoản');
		$this->template->content_box_class->set('');

		$data['user_id'] 		= $edit_id;
		$data['status_def'] 	= $this->config->item('user_status');
		$data['jsdataobject'] 	= json_encode($data);
		
		parent::render($data);
	}

	public function admin_list()
	{
		restrict('Admin.Staffs');
		
		$this->template->is_box_open->set(1);
		$this->template->title->set('Quản lý tài khoản');
		$this->template->javascript->add(base_url("dist/vStaffIndex.js?v=1"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		$this->template->stylesheet->add(base_url('node_modules/vue2-dropzone/dist/vue2Dropzone.min.css'));
		$this->template->publish();
	}


	/**
	 * Current Admin page
	 */
	function profile()
	{
		$this->template->title->set('Thông tin tài khoản');
		$this->template->content_box_class->set('');
		$data = $this->data;
		$admin = $this->admin_m->set_get_admin()->limit(1)->get($this->admin_m->id);
		$this->load->model('credentials_m');

		$keyCredential = $this->credentials_m->select('password')->where('user_id', $this->admin_m->id)->get_by();
		if( ! $keyCredential)
		{
			$keyCredential = $this->admin_m->generateKey();
			$this->credentials_m->insert( ['password' => $keyCredential, 'user_id' => $this->admin_m->id, 'type' => $this->credentials_m->type]);
		}

		is_string($keyCredential) OR $keyCredential = $keyCredential ? $keyCredential->password : '';

		parent::render(wp_parse_args(array(
			'keyCredential' => $keyCredential,
			'profile' => $admin,
			'role' => $this->role_m->get($admin->role_id)
		), $this->data));
	}

	/**
	 * Sign in function
	 */
	public function sign_in()
	{
		// Get username and password from input
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		// Get Facebook reviewer account ID from environment variable
		$fb_reviewer_account_id = getenv('FACEBOOK_ACCOUNT_ID');

		// Check if any required fields are empty or if the provided username and password are incorrect
		if (empty($username)
			|| empty($password)
			|| empty($fb_reviewer_account_id)
			|| getenv('FACEBOOK_REVIEWER_USERNAME', time()) != $username
			|| getenv('FACEBOOK_REVIEWER_PWD', time()) != $password )
		{
			// Display error message
			$this->messages->error('Invalid username or password');
			
			// Redirect to admin URL
			redirect(admin_url(), 'refresh');
		}

		// Get admin using the Facebook reviewer account ID
		$admin = $this->admin_m->set_get_admin()->get($fb_reviewer_account_id);
		if(empty($admin))
		{
			// Display error message
			$this->messages->error('Invalid username or password [1]');
			
			// Redirect to admin URL
			redirect(admin_url(), 'refresh');
		}

		
		$role = $this->role_m->get($admin->role_id);
		$admin->role_name = $role->role_name ?? '';

		$this->session->unset_userdata('tmp_user_name');
		$this->admin_m->setLogin($admin);
		redirect(admin_url(getenv('FACEBOOK_REVIEWER_REDIRECT_URL')), 'refresh');
	}

	function login()
	{
		$post = $this->input->post();
		if(empty($post)) redirect(admin_url(), 'refresh');

		$post['username'] = $this->input->post('username');

		$this->session->set_userdata('tmp_user_name', $post['username']);

		$admin = $this->admin_m->set_get_admin()->get_by(['user_login' =>$post['username']]);

		if($admin)
		{
			$this->data['admin'] = $admin;

			if($admin->user_status ==0){

				$this->messages->error('Tài khoản của bạn đã bị khóa, vui lòng liên hệ với Quản trị để biết thêm chi tiết.');
			}
			else{

				$role = $this->role_m->get($admin->role_id);

				$admin->role_name = '';

				if($role){

					$admin->role_name = $role->role_name;
				}

				$this->session->unset_userdata('tmp_user_name');

				$this->admin_m->setLogin($admin);

				redirect(admin_url(), 'refresh');
			}
		}

		$this->messages->error('Không đúng mật khẩu hoặc username, vui lòng kiểm tra lại.');

		redirect(admin_url(), 'refresh');
	}


	/**
	 * Check credentials via google Oauth
	 * 
	 * INPUT : 
	 * - GET : string 	code	response code from google oauth
	 * 
	 */
	function oauth_login()
	{
		$code = $this->input->get('code');

		if(empty($code)) redirect(admin_url(), 'refresh');

		$this->load->library('google_auth');

		$this->google_auth->client->authenticate($code);

		$access_token = $this->google_auth->client->getAccessToken();
		if( ! empty($access_token)) $this->google_auth->client->setAccessToken($access_token);
		if( ! $this->google_auth->client->getAccessToken()) redirect(admin_url(), 'refresh');

		$userData = $this->google_auth->objOAuthService->userinfo->get();

		if( ! in_array($userData['hd'],array('sccom.vn','webdoctor.vn','adsplus.vn', '')))
		{
			$this->messages->error('Your Google Account is invalid !');
			redirect(admin_url(), 'refresh');
		}

		$this->session->set_userdata('tmp_user_name', $userData['email']);
		$admin = $this->admin_m->set_get_admin()->get_by(['user_email'	=> $userData['email']]);
		if($admin && $admin->user_id < 8 && $admin->user_login != $userData['id'])
		{
			$this->admin_m->update($admin->user_id, array('user_login' =>  $userData['id']));
		}

		// IF Account not exists , then create new one
		if( ! $admin)
		{
			$this->admin_m->insert(array('display_name'	=>	$userData['name'],
				'user_email' => $userData['email'],
				'user_activation_key' => '',
				'user_status' => -1,
				'user_avatar' => $userData['picture'],
				'user_type'	=>	'admin',
				'user_login'	=>	$userData['id']));

			$this->messages->error('Đăng nhập thành công, vui lòng đợi Quản trị kích hoạt tài khoản cho bạn.');
			redirect(admin_url(), 'refresh');
		}

		// IF ACCOUNT EXISTS, THEN CHECK STATUS BEFORE ACCEPTS LOGIN
		switch ($admin->user_status)
		{
			case 1 : // Tài khoản đã được kích hoạt

				$role 			= $this->role_m->get($admin->role_id);
				$redirect_url 	= admin_url();

				$admin->role_name 			= '';
				$admin->login_destination 	= $redirect_url;

				// Append role infomation for session userdata
				if($role)
				{
					$admin->role_id 			= $role->role_id;
					$admin->role_name 			= $role->role_name;

					//set login redirect URL
					if($role->login_destination)
					{
						$redirect_url 				= $role->login_destination;
						$admin->login_destination 	= $role->login_destination;
					}
				}

				// Check to update display name from google userdata
				if( empty($admin->display_name))
				{
					$display_name = $userData['email'];
					if( ! empty($userData['name']))
					{
						$display_name = $userData['name'];
					}
					else if( ! empty($userData['familyName']) && ! empty($userData['givenName']))
					{
						$display_name = "{$userData['familyName']} {$userData['givenName']}";
					}

					$this->admin_m->update($admin->user_id, array('display_name' =>$display_name ));
					$admin->display_name = $display_name;
					$this->admin_m->deleteCache($admin->user_id);
				}
				

				// Re-check for update user avatart from Google Data
				$is_profile_refresh     = get_user_meta_value($admin->user_id,'is_profile_refresh');
				if( empty($admin->user_avatar) || $is_profile_refresh)
				{
					$this->admin_m->update($admin->user_id, array('user_avatar' => $userData['picture']));

					$admin->user_avatar = $userData['picture'];
					$this->admin_m->deleteCache($admin->user_id);
				}

				$this->session->unset_userdata('tmp_user_name');
				$this->admin_m->setLogin($admin);

				update_user_meta($admin->user_id, 'last_login', time());

                $_stateValues = null;
		        $_state = $this->input->get('state') and $_stateValues = $this->scache->get("states/{$_state}");
		        if (!empty($_stateValues['redirectUrl'])) redirect($_stateValues['redirectUrl'], 'refresh');

				redirect($redirect_url, 'refresh');
				break;

			case 0 : // Tài khoản bị khóa
				$this->messages->error('Tài khoản của bạn hiện tại đã bị khóa , vui lòng liên hệ quản trị viên để biết tiến hành kích hoạt tài khoản');
				break;

			case -1 : // Tài khoản chờ kích hoạt
				$this->messages->error('Tài khoản đã được đưa vào hàng chờ duyệt, vui lòng đợi quản trị kích hoạt tài khoản cho bạn.');
				break;
		}

		redirect(admin_url(), 'refresh');
	}


	/**
	 * { function_description }
	 *
	 * @param      integer  $edit   The edit
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	function ajax_edit($edit = 0)
	{
		$result = array('success' => FALSE, 'msg' => 'Có lỗi xảy ra');

		if( ! $this->input->is_ajax_request()) return parent::renderJson($result);

		$queue = array();

		if($queue[] = $this->input->post())
		{
			if(!empty($queue[0]['data']) && is_array($queue[0]['data'])){

				$queue = $queue[0]['data'];
			}
		}


		foreach ($queue as $item)
		{	
			if( ! in_array($item['type'], ['field','meta_data'])) continue;

			if($item['type'] == 'field')
			{
				$this->admin_m->update($item['pk'], [$item['name'] => $item['value']]);
				continue;
			}
			
			update_user_meta($item['pk'], $item['name'], $item['value']);
		}

		$result['success'] 	= TRUE;
		$result['msg'] 		= 'Cập nhật dữ liệu thành công';

		return parent::renderJson($result);
	}


	public function logout()
	{	
		$this->session->sess_destroy();
		redirect(admin_url(), 'refresh');	
	}


	function check_login()
	{
		if(!in_array($this->router->fetch_method(), array('index','login','oauth_login')))
		{
			parent::check_login();		
		}
	}


	/**
	 * User's groups UI
	 * 1. List all user groups
	 * 2. Create a new user group
	 * 3. Assignee user-group's members
	 */
	public function groups()
	{
		restrict('admin.user_group.view');

		$this->template->is_box_open->set(1);
		$this->template->title->set('Quản lý nhóm làm việc');

		parent::render($this->data);
	}


	/**
	 * Departments UI
	 * 1. List all departments
	 * 2. Create a new department
	 * 3. Assignee department's members
	 */
	public function departments()
	{
		restrict('admin.department.view');
		
		$this->template->is_box_open->set(1);
		$this->template->title->set('Quản lý phòng ban');

		parent::render($this->data);
	}


	protected function search_filter($search_args = array())
	{
		if(empty($search_args)){
			
			if($this->input->get('search'))	$search_args = $this->input->get();
			else return FALSE;
		}	

		if(empty($search_args)) return FALSE;

		$this->hook->add_filter('search_filter', function($args){

			if(!empty($args['where']['user_time_create'])){

				$explode = explode(' - ', $args['where']['user_time_create']);

				$this->admin_ui->where('user_time_create >=', strtotime(reset($explode)));

				$this->admin_ui->where('user_time_create <=', strtotime(end($explode)));

				unset($args['where']['user_time_create']);
			}

			// Role_id FILTERING & SORTING
			$filter_role_id = $args['where']['role_id'] ?? FALSE;
			$sort_role_id = $args['order_by']['role_id'] ?? FALSE;
			if($filter_role_id || $sort_role_id)
			{
				if($filter_role_id)
				{	
					$this->admin_ui->where('user.role_id',$filter_role_id);
				}

				if($sort_role_id)
				{
					$this->admin_ui->join('role', 'role.role_id = user.role_id');
					$this->admin_ui->order_by('role.role_name',$sort_role_id);
				}

				unset($args['where']['role_id'],$args['order_by']['role_id']);
			}

			// Role_id FILTERING & SORTING
			$filter_user_status = $args['where']['user_status'] ?? FALSE;
			$sort_user_status = $args['order_by']['user_status'] ?? FALSE;
			if($filter_user_status || $sort_user_status)
			{
				if($filter_user_status)
				{	
					$this->admin_ui->where('(user.user_status)*1',$filter_user_status);
				}

				if($sort_user_status)
				{
					$this->admin_ui->order_by('user.user_status',$sort_user_status);
				}

				unset($args['where']['user_status'],$args['order_by']['user_status']);
			}

			return $args;			
		});

		parent::search_filter($search_args);
	}


	/**
	 * Đăng nhập dưới dạng tài khoản của {$user_id}
	 */

	/**
	 * Đăng nhập dưới dạng tài khoản của {$user_id}
	 *
	 * @param      integer  $admin_id  ID của tài khoản truy cập
	 *
	 * @return     VOID   Chuyển hướng đến trang mặc định của tài khoản được truy cập
	 */
	function login_to($admin_id = 0)
	{
		$admin = $this->admin_m->get($admin_id);	
		if(!$admin)
		{
			return $this->render404('Thông tin admin không tồn tại');
		}

		$role = $this->role_m->get($admin->role_id);

		$admin->role_name = '';
		$redirect_url = admin_url();
		if($role){

			$admin->role_id = $role->role_id;
			$admin->role_name = $role->role_name;

			if($role->login_destination)
			{
				$redirect_url = $role->login_destination;
			}
		}

		$this->admin_m->setLogin($admin);

		redirect($redirect_url, 'refresh');
	}

    public function ui_builder()
    {
        $has_permission = has_permission('admin.mbusiness.access');
        if(!$has_permission)
        {
            $this->messages->error('Bạn không có quyền thực hiện theo tác này.');

            redirect(admin_url(), 'refresh');
        }

        $this->template->is_box_open->set(1);
		$this->template->title->set('Trang xây dựng giao diện');
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		$this->template->javascript->add('https://code.highcharts.com/highcharts.js');
		$this->template->javascript->add('https://code.highcharts.com/highcharts-more.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/no-data-to-display.js');
		// $this->template->javascript->add('https://code.jquery.com/jquery-3.7.1.min.js');
		// $this->template->javascript->add('datatables.net-dt/css/jquery.dataTables.min.css');
		
		//DataTable
		$this->template->stylesheet->add(base_url('node_modules/datatables.net-dt/css/jquery.dataTables.min.css'));
		$this->template->stylesheet->add(base_url('node_modules/datatables.net-dt/css/jquery.dataTables.css'));
		$this->template->stylesheet->add(base_url('node_modules/datatables.net-dt/css/jquery.dataTables.css'));
		
		$this->template->javascript->add('https://code.highcharts.com/highcharts-more.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/no-data-to-display.js');
		$this->template->stylesheet->add(base_url('src/components/dashboardSale/personal/style.css'));
		$this->template->stylesheet->add(base_url('src/components/dashboardSale/RevenueRank/revenue-rank.css'));
		$this->template->stylesheet->add(base_url('src/components/dashboardSale/SaleKpiProgress/sale-kpi-progress.css'));
		// $this->template->javascript->add('https://code.highcharts.com/modules/exporting.js');
		// $this->template->javascript->add('https://code.highcharts.com/modules/export-data.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/accessibility.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/mouse-wheel-zoom.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/variable-pie.js');

		$this->template->footer->prepend('<script type="text/javascript">var sale_id = ' . $this->admin_m->id . '</script>');
		$this->template->javascript->add(base_url("dist/vUiBuilder.js"));


		$this->template->publish();
    }

	public function ui_builder_group()
    {
        $has_permission = has_permission('admin.mbusiness.access');
        if(!$has_permission)
        {
            $this->messages->error('Bạn không có quyền thực hiện theo tác này.');

            redirect(admin_url(), 'refresh');
        }

        $this->template->is_box_open->set(1);
		$this->template->title->set('Trang xây dựng giao diện trưởng nhóm');
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		$this->template->javascript->add('https://code.highcharts.com/highcharts.js');
		$this->template->javascript->add('https://code.highcharts.com/highcharts-more.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/no-data-to-display.js');
		// $this->template->javascript->add('https://code.jquery.com/jquery-3.7.1.min.js');
		// $this->template->javascript->add('datatables.net-dt/css/jquery.dataTables.min.css');
		
		//DataTable
		$this->template->stylesheet->add(base_url('node_modules/datatables.net-dt/css/jquery.dataTables.min.css'));
		$this->template->stylesheet->add(base_url('node_modules/datatables.net-dt/css/jquery.dataTables.css'));
		$this->template->stylesheet->add(base_url('node_modules/datatables.net-dt/css/jquery.dataTables.css'));
		
		$this->template->javascript->add('https://code.highcharts.com/highcharts-more.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/no-data-to-display.js');
		$this->template->stylesheet->add(base_url('src/components/dashboardSale/personal/style.css'));
		$this->template->stylesheet->add(base_url('src/components/dashboardSale/RevenueRank/revenue-rank.css'));
		$this->template->stylesheet->add(base_url('src/components/dashboardSale/SaleKpiProgress/sale-kpi-progress.css'));
		// $this->template->javascript->add('https://code.highcharts.com/modules/exporting.js');
		// $this->template->javascript->add('https://code.highcharts.com/modules/export-data.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/accessibility.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/mouse-wheel-zoom.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/variable-pie.js');
		$this->template->footer->prepend('<script type="text/javascript">var group_id = ' . $this->admin_m->id . '</script>');
		$this->template->javascript->add(base_url("dist/vUiBuilderGroup.js"));


		$this->template->publish();
    }

	public function ui_builder_manager()
    {
        $has_permission = has_permission('admin.mbusiness.access');
        if(!$has_permission)
        {
            $this->messages->error('Bạn không có quyền thực hiện theo tác này.');

            redirect(admin_url(), 'refresh');
        }

        $this->template->is_box_open->set(1);
		$this->template->title->set('Trang xây dựng giao diện trưởng phòng');
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		$this->template->javascript->add('https://code.highcharts.com/highcharts.js');
		$this->template->javascript->add('https://code.highcharts.com/highcharts-more.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/no-data-to-display.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/treemap.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/exporting.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/accessibility.js');

		$this->template->stylesheet->add(base_url('src/components/dashboardSale/manager/style.css'));

		
		
		//DataTable
		$this->template->stylesheet->add(base_url('node_modules/datatables.net-dt/css/jquery.dataTables.min.css'));
		$this->template->stylesheet->add(base_url('node_modules/datatables.net-dt/css/jquery.dataTables.css'));
		$this->template->stylesheet->add(base_url('node_modules/datatables.net-dt/css/jquery.dataTables.css'));
		
		$this->template->javascript->add('https://code.highcharts.com/highcharts-more.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/no-data-to-display.js');
		$this->template->stylesheet->add(base_url('src/components/dashboardSale/RevenueRank/revenue-rank.css'));
		$this->template->stylesheet->add(base_url('src/components/dashboardSale/SaleKpiProgress/sale-kpi-progress.css'));
		
		$this->template->javascript->add('https://code.highcharts.com/modules/accessibility.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/mouse-wheel-zoom.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/variable-pie.js');
		$this->template->footer->prepend('<script type="text/javascript">var group_id = ' . $this->admin_m->id . '</script>');
		$this->template->javascript->add(base_url("dist/vUiBuilderManager.js"));
		$this->template->publish();
    }
}
/* End of file Staffs.php */
/* Location: ./application/modules/staffs/controllers/Staffs.php */