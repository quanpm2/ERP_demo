<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Api extends REST_Controller
{
    function __construct()
    {
        parent::__construct('staffs/rest');
        $this->load->model('admin_m');
        $this->load->model('term_m');
        $this->load->model('usermeta_m');
        $this->load->model('termmeta_m');
        $this->load->model('term_users_m');
        $this->load->config('staffs/staffs');
        $type = $this->input->get('type');
        $this->type = ($type) ? $type : 'admin';
    }

    public function user_get()
    {
        $default    = array('id' => 0);
        $args       = wp_parse_args(parent::get(NULL, TRUE), $default);
        /* Truy xuất 1 contact */
        if(!empty($args['id'])) $this->get_by($args['id']);

        /* Truy xuất nhiều contact */
        $this->get_many_by();

        parent::response();
    }

    public function get_by($user_id = 0){
        $this->load->model('staffs/staffs_m');
        if( ! $user_id) parent::response('ID Không tồn tại', parent::HTTP_NOT_FOUND);

        $default    = array('columns' => $this->config->item('default_columns', 'datasource'));
        $args       = wp_parse_args(parent::post(NULL, TRUE), $default);

        $columns_def    = $this->config->item('columns', 'datasource');
        $field_cols     = array_filter($args['columns'], function($x) use($columns_def) { return 'field' == ($columns_def[$x]['type'] ?? '0');});

        $contact = $this->staffs_m->select('user.user_id')->select($field_cols)->as_array()->get((int) $user_id);
        if( ! $contact) parent::response('ID Không tồn tại', parent::HTTP_NOT_FOUND);

        $contact = $this->staffs_m->callback($contact, $args['columns']);

        parent::response($contact);
    }

    public function get_many_by(){
        $default_columns = $this->config->item('default_columns', 'datasource');
        $default = array(
            'pagination' => ['page' => 1, 'pages' => 35, 'perpage' => 200, 'total' => 350],
            'sort' => ['field' => 'user_id', 'sort' => 'DESC'],
            'query' => '',
            'columns' => $default_columns
        );
        $config = $this->config->item('datasource');
        $args   = wp_parse_args(parent::get(NULL, TRUE), $default);

        $this->load->model('staffs/staffs_m');

        if($config['columns'][$args['sort']['field']]['type'] == 'field'){
            $this->staffs_m->select('user.user_id')->order_by("user.{$args['sort']['field']}", $args['sort']['sort']);
        }
        if(isset($args['query']['filter_type']) && isset($args['query']['filter_value'])){
            $this->staffs_m->like("{$args['query']['filter_type']}","{$args['query']['filter_value']}");
        }
        $istart     = ($args['pagination']['page'] - 1) * $args['pagination']['perpage'];
        $ilength    = $args['pagination']['perpage'];
        $this->staffs_m->limit($ilength, $istart);

        $def_columns    = $this->config->item('columns', 'datasource');
        $field_columns  = array_group_by($def_columns, 'type');
        if( ! empty($field_columns['field']) && $field_columns = array_column($field_columns['field'], 'name'))
        {
            $this->staffs_m->select($field_columns);
        }

        $contacts = $this->staffs_m->as_array()->where('user_type','admin')->get_all();
        $last = $this->staffs_m->last_query();
        $array_query = explode("LIMIT",$last);
        $array_query = explode("FROM",$array_query[0]);
        $query = "SELECT count(*) as qty FROM " . $array_query[1];
        $qty = $this->db->query($query)->result();
        $contacts = array_map(function($x) use($args) { return $this->staffs_m->callback($x, $args['columns']); }, $contacts);

        $total_rows = $qty[0]->qty;

        $response = array(
            'meta' => array(
                'page' =>  $args['pagination']['page'],
                'pages' =>  ceil(div($total_rows, $args['pagination']['perpage'])),
                'perpage' =>  $args['pagination']['perpage'],
                'total' =>  $total_rows,
                'sort' =>  'asc',
                'field' =>  'user_id'
            ),
            'data' => $contacts
        );

        parent::response($response);
    }

    public function department_get()
    {
        $argv = array();
        // $argv = $this->post();
        $department = $this->term_m->select('*')->where('term_type','department')->as_array()->get_many_by();
        foreach($department as $key => $value){
            $meta = get_term_meta_value($value['term_id']);
            $meta = $meta ? key_value($meta, 'meta_key', 'meta_value') : [];
            $department[$key]['meta'] = $meta;
        }
        parent::response($department);
    }
    public function user_group_get()
    {
        $argv = array();
        $user_group = $this->term_m->select('*')->where('term_type','user_group')->as_array()->get_many_by();
        foreach($user_group as $key => $value){
            $user_ids = $this->term_users_m->get_the_users($value['term_id'],'admin');
            $user_group[$key]['user_ids'] = $user_ids ? array_column($user_ids, 'user_id') : array();
            $meta = get_term_meta_value($value['term_id']);
            $meta = $meta ? key_value($meta, 'meta_key', 'meta_value') : [];
            $user_group[$key]['meta'] = $meta;
        }
        parent::response($user_group);
    }
}
