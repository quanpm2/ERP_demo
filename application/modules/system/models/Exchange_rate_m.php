<?php

if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Exchange_rate_m extends Base_model {
    public $primary_key = 'id';
    public $_table = 'fact_exchange_rate';

    public function set_query()
    {
        return $this->join('dim_time', 'dim_time.id = fact_exchange_rate.time_id')
            ->select('
                dim_time.date,
                fact_exchange_rate.currency_from,
                fact_exchange_rate.currency_to,
                fact_exchange_rate.buy,
                fact_exchange_rate.sell,
                fact_exchange_rate.resource
            ');
    }
}
?>