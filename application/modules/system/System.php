<?php
class System_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name(){
		
		return 'System';
	}

	public function init(){	

		if(has_permission('Admin.System') && is_module_active('system'))
		{

		}
	}

	public function title()
	{
		return 'System';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '0.1';
	}

	public function description()
	{
		return 'System';
	}
	private function init_permissions()
	{
		$permissions = array();
		$permissions['Admin.System'] = array(
			'description' => 'Quản lý System',
			'actions' => array('view','add','edit','delete','update'));

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}