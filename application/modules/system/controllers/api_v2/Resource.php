<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resource extends MREST_Controller
{
    public function menu_get()
    {
        $modules = get_active_modules();
        foreach($modules as $module => $val)
        {
            $r = $this->load->package($module);
            if($r)
            {
                $r->init();
            }
        }

        $data = $this->menu->get_menu();

        parent::response(['status'=> TRUE, 'data' => $this->menu->get_menu()], parent::HTTP_OK);
    }

    public function menu_options()
    {
        parent::response('ok', parent::HTTP_OK);
    }

    public function exchange_rate_get()
    {
        parent::response([
            'status' => true,
            'data' => $this->option_m->get_value('exchange_rate', TRUE)
        ]);
    }
    
    /**
     * manipulation_get
     *
     * @return void
     */
    public function manipulation_get()
    {
        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        if(empty($manipulation_locked))
        {
            $manipulation_locked = [
                'is_manipulation_locked' => FALSE,
                
                'manipulation_locked_at' => NULL,
                'manipulation_locked_by' => NULL,
                
                'manipulation_unlocked_at' => time(),
                'manipulation_unlocked_by' => 1,
            ];
            
            $this->option_m->set_value('manipulation_locked', $manipulation_locked);
        }
        
        $manipulation_locked['manipulation_locked_by_name'] = NULL;
        $manipulation_locked['manipulation_unlocked_by_name'] = NULL;

        $this->load->model('staffs/admin_m');
        if(!empty($manipulation_locked['manipulation_locked_by']))
        {
            $manipulation_locked['manipulation_locked_by_name'] = $this->admin_m->get_field_by_id($manipulation_locked['manipulation_locked_by'], 'display_name');
        }

        if(!empty($manipulation_locked['manipulation_unlocked_by']))
        {
            $manipulation_locked['manipulation_unlocked_by_name'] = $this->admin_m->get_field_by_id($manipulation_locked['manipulation_unlocked_by'], 'display_name');
        }
        

        parent::response([
            'code' => 200,
            'status' => 'success',
            'message' => 'Lấy dữ liệu thành công',
            'data' => $manipulation_locked,
        ]);
    }
    
    /**
     * manipulation_put
     *
     * @return void
     */
    public function manipulation_put()
    {
        $this->load->model('contract/contract_m');
        if(!$this->contract_m->has_manipulation_lock_permission())
        {
            return parent::response([
                'code' => 400,
                'status' => 'error',
                'message' => 'Không có quyền thao tác',
                'data' => [],
            ]);
        }

        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        
        $is_manipulation_locked = $manipulation_locked['is_manipulation_locked'] ?? FALSE;
        if(! $is_manipulation_locked)
        {
            $lock_date = parent::put("lock_date");

            $manipulation_locked['is_manipulation_locked'] = TRUE;
            $manipulation_locked['manipulation_locked_by'] = $this->admin_m->id;
            $manipulation_locked['manipulation_locked_at'] = $lock_date ?: time();

            $this->option_m->set_value('manipulation_locked', $manipulation_locked);

            return parent::response([
                'code' => 200,
                'status' => 'success',
                'message' => 'Đã ghi nhận khóa thao tác',
                'data' => $manipulation_locked,
            ]);
        }

        $manipulation_locked['is_manipulation_locked'] = FALSE;
        $manipulation_locked['manipulation_unlocked_by'] = $this->admin_m->id;
        $manipulation_locked['manipulation_unlocked_at'] = time();

        $this->option_m->set_value('manipulation_locked', $manipulation_locked);
        
        parent::response([
            'code' => 200,
            'status' => 'success',
            'message' => 'Đã ghi nhận mở khóa thao tác',
            'data' => $manipulation_locked,
        ]);
    }

    /**
     * exchange_rate_by_date_get
     *
     * @return void
     */
    public function exchange_rate_by_date_get()
    {
        $default = [
            'date' => my_date(0, 'Y-m-d'),
        ];
        $args = wp_parse_args( $this->input->get(), $default);

        $this->load->model('system/exchange_rate_m');

        $exchange_rate = $this->exchange_rate_m->set_query()
            ->select('description')
            ->where('date =', $args['date'])
            ->order_by('currency_from')
            ->as_array()
            ->get_all();
        if(empty($exchange_rate))
        {
            parent::response([
                'code' => 204,
                'status' => 'success',
                'message' => 'Không tìm thấy dữ liệu',
                'data' => [],
            ]);
        }

        // Move USD to first item
        foreach($exchange_rate as $key => $item) {
            if ($item['currency_from'] === 'USD') {
                unset($exchange_rate[$key]);
                array_unshift($exchange_rate, $item);
                break;
            }
        }

        parent::response([
            'code' => 200,
            'status' => 'success',
            'message' => 'Lấy dữ liệu thành công',
            'data' => $exchange_rate,
        ]);
    }
}
/* End of file Resource.php */
/* Location: ./application/modules/system/controllers/api_v2/Resource.php */