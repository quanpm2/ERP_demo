<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends REST_Controller {

	function __construct()
	{
		parent::__construct('system/rest');
		$this->load->model('staffs/admin_m');


		if(!in_array($this->admin_m->id, [854, 47])) parent::response(NULL, parent::HTTP_UNAUTHORIZED);

		$model_names = [];
		$this->load->model('option_m');
	}

	public function setting_get()
	{
		$id = parent::get('id');
		if(empty($id)) parent::response(NULL, parent::HTTP_UNAUTHORIZED);

		$response = array('status' => FALSE, 'msg' => 'not found command.', 'data' => []);
		if( ! in_array($id, ['con_signature'])) parent::response($response, parent::HTTP_BAD_REQUEST);

		if($id == 'con_signature')
		{
			$this->load->config('staffs/staffs');
			$this->config->load('contract/contract');

			$con_signature_config = $this->option_m->get_value('con_signature_config', TRUE);
			foreach ($con_signature_config as $key => $item)
			{
				if( ! empty($item['key'])) continue;

				unset($con_signature_config[$key]);
			}

			$this->option_m->set_value('con_signature_config', $con_signature_config);

			$con_signature_config = $con_signature_config ? array_values($con_signature_config) : [];
			if( empty($con_signature_config))
			{
				$_default = $this->config->item('default', 'con_signature_config');
				$con_signature_config = [$this->config->item($_default, 'con_signature_config')];
			}

			$con_sign_cfg = array( $this->config->item('default', 'con_signature_config') => 'Theo mặc định' );

			$this->load->model('term_m');
			$this->load->model('staffs/department_m');
			if($departments = $this->department_m->select('term_id,term_name')->set_term_type()->order_by('term_name')->get_all())
			{
				$con_sign_cfg = array_replace($con_sign_cfg, key_value($departments, 'term_id', 'term_name'));
			}

			$response['status'] = TRUE;
			$response['msg'] 	= 'Dữ liệu tải thành công';
			$response['data'] = array(
				'config' => $con_sign_cfg,
				'values' => $con_signature_config
			);

			parent::response($response);
		}

		parent::response($response, parent::HTTP_BAD_REQUEST);
	}

	public function setting_post()
	{
		$id = parent::post('id');
		if(empty($id) || $this->admin_m->id != 47) parent::response(NULL, parent::HTTP_UNAUTHORIZED);

		$options = parent::post('options', TRUE);
		if(empty($options)) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

		$response = array('status' => FALSE, 'msg' => 'not found command.', 'data' => []);

		switch ($id)
		{
			case 'con_signature':
				if(empty($options['con_represent'])) parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);

				$this->option_m->set_value('con_signature_config', $options['con_represent']);
				parent::response(['status' => TRUE, 'msg' => 'Cập nhật dữ liệu thành công.']);
				break;
		}

		parent::response(NULL, parent::HTTP_NOT_ACCEPTABLE);
	}
}
/* End of file Api.php */
/* Location: ./application/modules/system/controllers/Api.php */