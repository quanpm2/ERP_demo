<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class System extends Admin_Controller {

	public $model = '';

	function __construct()
	{
		parent::__construct();
		if( $this->admin_m->role_id != 1) redirect(admin_url());
	}

	public function index()
	{
		$this->submit();

		$data = $this->data;

		$roles = $this->role_m
		->select('role_id,role_name,role_description')
		->order_by('role_name')
		->get_many_by(['role_id !='=>1]);
		
		$data['roles'] = array_column($roles, 'role_name','role_id');
		
		$this->template->is_box_open->set(1);
		parent::render($data);
	}

	public function submit()
	{
		$post = $this->input->post();
		if( ! $post) return FALSE;

		if(empty($post['submit'])) return FALSE;

		$options = $post['options'] ?? [];
		if(empty($options)) return FALSE;

		! empty($options['blacklist_keywords'])
		AND $options['blacklist_keywords'] = array_map(function($x){ return trim($x); }, explode(',', $options['blacklist_keywords']));

        if(! empty($options['blacklist_email_domain'])){
            $options['blacklist_email_domain'] = array_map(function($x){
                $x = trim($x);
                $x = trim($x, '@');
                $x = mb_strtolower($x);

                return $x;
            }, explode(',', $options['blacklist_email_domain']));

            $options['blacklist_email_domain'] = array_unique($options['blacklist_email_domain']);
            $options['blacklist_email_domain'] = array_filter($options['blacklist_email_domain']);
        }

        if(! empty($options['currency_codes'])){
            $bank = $options['currency_codes']['bank'] ?? '';
            $bank = explode(',', $bank);
            $options['currency_codes']['bank'] = array_map(function($x) { return strtoupper(trim($x)); }, $bank);
            
            $open_api = $options['currency_codes']['open_api'] ?? '';
            $open_api = explode(',', $open_api);
            $options['currency_codes']['open_api'] = array_map(function($x) { return strtoupper(trim($x)); }, $open_api);
        }

		foreach ($options as $key => $value) 
		{
			$this->option_m->set_value($key, $value);
		}

		redirect(module_url(),'refresh');
	}
}