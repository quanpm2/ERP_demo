<?php

$this->admin_form->set_col(6, 6);

echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Role - Tiktokads');
echo $this->admin_form->dropdown('Trưởng phòng', 'options[manager_tt_ads_role_id][]', $roles, ($this->option_m->get_value('manager_tt_ads_role_id', TRUE)??[]), 'Trưởng bộ phận kỹ thuật', array('multiple'=>'multiple'));
echo $this->admin_form->dropdown('AM', 'options[leader_tt_ads_role_id][]', $roles, ($this->option_m->get_value('leader_tt_ads_role_id', TRUE)??[]), 'Trưởng nhóm kỹ thuật', array('multiple'=>'multiple'));
echo $this->admin_form->dropdown('GAT', 'options[tt_ads_role_id][]', $roles, ($this->option_m->get_value('tt_ads_role_id', TRUE)??[]), 'Kỹ thuật viên', array('multiple'=>'multiple'));
echo $this->admin_form->dropdown('Vai trò','options[group_tt_ads_role_ids][]', $roles,($this->option_m->get_value('group_tt_ads_role_ids', TRUE)??[]), 'Vai trò Kỹ thuật ADS',array('multiple'=>'multiple'));
echo $this->admin_form->box_close(['submit', 'Lưu lại'], ['button', 'Hủy bỏ']);
echo $this->admin_form->form_close();

echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Role - Facebookads');
echo $this->admin_form->dropdown('Trưởng phòng', 'options[manager_fb_ads_role_id]', $roles, ($this->option_m->get_value('manager_fb_ads_role_id')??[]), 'Trưởng bộ phận kỹ thuật');
echo $this->admin_form->dropdown('AM', 'options[leader_fb_ads_role_id]', $roles, ($this->option_m->get_value('leader_fb_ads_role_id')??[]), 'Trưởng nhóm kỹ thuật');
echo $this->admin_form->dropdown('GAT', 'options[fb_ads_role_id]', $roles, ($this->option_m->get_value('fb_ads_role_id')??[]), 'Kỹ thuật viên');
echo $this->admin_form->dropdown('Vai trò','options[group_fb_ads_role_ids][]', $roles,($this->option_m->get_value('group_fb_ads_role_ids', TRUE)??[]), 'Vai trò Kỹ thuật ADS',array('multiple'=>'multiple'));
echo $this->admin_form->box_close(['submit', 'Lưu lại'], ['button', 'Hủy bỏ']);
echo $this->admin_form->form_close();

echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Roles Webdoctor.vn');
echo $this->admin_form->dropdown('Webdoctor - nội dung','options[group_content_ids][]',$roles,($this->option_m->get_value('group_content_ids', TRUE)??[]),'Danh sách các quyền được chọn khối nội dung',array('multiple'=>'multiple'));
echo $this->admin_form->dropdown('Webdoctor - Kỹ thuật','options[group_webbuild_ids][]',$roles,($this->option_m->get_value('group_webbuild_ids', TRUE)??[]),'Danh sách các quyền được chọn khối kỹ thuật webdoctor',array('multiple'=>'multiple'));
echo $this->admin_form->box_close(['submit', 'Lưu lại'], ['button', 'Hủy bỏ']);
echo $this->admin_form->form_close();

echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Roles 1AD');
echo $this->admin_form->dropdown('Kỹ thuật', 'options[group_1ad_ids][]',$roles,($this->option_m->get_value('group_1ad_ids', TRUE)??[]),'Danh sách các quyền được chọn khối kỹ thuật 1AD',array('multiple'=>'multiple'));
echo $this->admin_form->box_close(['submit', 'Lưu lại'], ['button', 'Hủy bỏ']);
echo $this->admin_form->form_close();

echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Roles Adsplus-Googleads');
echo $this->admin_form->dropdown('1AD - Kỹ thuật', 'options[group_1ad_ids][]',$roles,($this->option_m->get_value('group_1ad_ids', TRUE)??[]),'Danh sách các quyền được chọn khối kỹ thuật 1AD',array('multiple'=>'multiple'));

echo $this->admin_form->dropdown('Trưởng bộ phận ADS', 'options[manager_ads_role_id]', $roles, ($this->option_m->get_value('manager_ads_role_id')??[]), 'Trưởng bộ phận kỹ thuật');
echo $this->admin_form->dropdown('Trưởng nhóm ADS - AM', 'options[leader_ads_role_id]', $roles, ($this->option_m->get_value('leader_ads_role_id')??[]), 'Trưởng nhóm kỹ thuật');
echo $this->admin_form->dropdown('Kỹ thuật ADS - GAT', 'options[ads_role_id]', $roles, ($this->option_m->get_value('ads_role_id')??[]), 'Kỹ thuật viên');
echo $this->admin_form->dropdown('Vai trò ADS','options[group_ads_role_ids][]', $roles,($this->option_m->get_value('group_ads_role_ids', TRUE)??[]), 'Vai trò Kỹ thuật ADS',array('multiple'=>'multiple'));
echo $this->admin_form->box_close(['submit', 'Lưu lại'], ['button', 'Hủy bỏ']);
echo $this->admin_form->form_close();

echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Roles Kinh doanh');
echo $this->admin_form->dropdown('Trưởng bộ phận','options[manager_role_id]', $roles, ($this->option_m->get_value('manager_role_id')??[]), 'Danh sách các nhóm kinh doanh');
echo $this->admin_form->dropdown('Trưởng nhóm','options[leader_role_id]', $roles, ($this->option_m->get_value('leader_role_id')??[]), 'Danh sách các nhóm kinh doanh');
echo $this->admin_form->dropdown('Kinh doanh','options[group_sales_ids][]',$roles,($this->option_m->get_value('group_sales_ids', TRUE)??[]),'Danh sách các nhóm kinh doanh',array('multiple'=>'multiple'));
echo $this->admin_form->box_close(['submit', 'Lưu lại'], ['button', 'Hủy bỏ']);
echo $this->admin_form->form_close();

echo $this->admin_form->form_open();
echo $this->admin_form->box_open('MODULE ADSPLUS');
echo $this->admin_form->input_numberic('Max person received email/sms','options[max_number_curators]',$this->option_m->get_value('max_number_curators')??0);
echo $this->admin_form->textarea('Từ khóa nhạy cảm (SMS)','options[blacklist_keywords]', implode(', ', ($this->option_m->get_value('blacklist_keywords', TRUE) ?: [])));
echo $this->admin_form->box_close(['submit', 'Lưu lại'], ['button', 'Hủy bỏ']);
echo $this->admin_form->form_close();

echo $this->admin_form->form_open();
echo $this->admin_form->box_open('EMAIL VALIDATOR');
echo $this->admin_form->textarea('Danh sách đuôi email cần loại bỏ','options[blacklist_email_domain]', implode(', ', ($this->option_m->get_value('blacklist_email_domain', TRUE) ?: [])), 'Email không chứa ký tự "@", cách nhau bởi dấu ","');
echo $this->admin_form->box_close(['submit', 'Lưu lại'], ['button', 'Hủy bỏ']);
echo $this->admin_form->form_close();

echo $this->admin_form->form_open();
echo $this->admin_form->box_open('TIN VALIDATOR');

echo $this->admin_form->radio('options[is_active_tin_check]', 1, (int) $this->option_m->get_value('is_active_tin_check') == 1, ['id' => 'is_active_tin_check_active'])  . form_label('Kích hoạt', 'is_active_tin_check_active', ['style' => 'margin-right: 16px']);
echo $this->admin_form->radio('options[is_active_tin_check]', 0, (int) $this->option_m->get_value('is_active_tin_check') == 0, ['id' => 'is_active_tin_check_inactive']) . form_label('Không kích hoạt', 'is_active_tin_check_inactive');

echo $this->admin_form->box_close(['submit', 'Lưu lại', 'is_active_tin_check'], ['button', 'Hủy bỏ']);
echo $this->admin_form->form_close();

echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Cấu hình thông báo');

echo $this->admin_form->paragraph('Push slack thông báo thu tiền', '', ['style' => 'font-size: 16px;']);
echo $this->admin_form->radio('options[is_payment_notification_slack_enabled]', 1, (int) $this->option_m->get_value('is_payment_notification_slack_enabled') == 1, ['id' => 'is_payment_notification_slack_enabled_active'])  . form_label('Kích hoạt', 'is_payment_notification_slack_enabled_active', ['style' => 'margin-right: 16px']);
echo $this->admin_form->radio('options[is_payment_notification_slack_enabled]', 0, (int) $this->option_m->get_value('is_payment_notification_slack_enabled') == 0, ['id' => 'is_payment_notification_slack_enabled_inactive']) . form_label('Không kích hoạt', 'is_payment_notification_slack_enabled_inactive');

echo $this->admin_form->box_close(['submit', 'Lưu lại'], ['button', 'Hủy bỏ']);

echo $this->admin_form->form_open();
echo $this->admin_form->box_open('Cấu hình đồng bộ tỉ giá');
echo $this->admin_form->paragraph('Push slack thông báo thu tiền', '', ['style' => 'font-size: 16px;']);
echo $this->admin_form->textarea('Mã tiền tệ lấy từ API của ngân hàng','options[currency_codes][bank]', implode(',', ($this->option_m->get_value('currency_codes', TRUE)['bank'] ?? [])), 'Tỉ giá bao gồm 3 ký tự in hoa, cách nhau bởi dấu ","');
echo $this->admin_form->textarea('Mã tiền tệ lấy từ API mở','options[currency_codes][open_api]', implode(',', ($this->option_m->get_value('currency_codes', TRUE)['open_api'] ?? [])), 'Tỉ giá bao gồm 3 ký tự in hoa, cách nhau bởi dấu ","');
echo $this->admin_form->box_close(['submit', 'Lưu lại'], ['button', 'Hủy bỏ']);

echo $this->admin_form->form_close();