
<button type="button" id="add_task" name="add_new" class="btn btn-info pull-right btn-add-new" data-toggle="modal" data-target="#myModal" onclick="javascript:set_default_form();">
  <i class="glyphicon glyphicon-plus"></i>  Thêm mới
</button> 


<?php 

echo $content['table'];
echo $content['pagination'];

?>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Thêm mới</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <?php 
          echo $this->admin_form->form_open('',array('class'=>'form-group'), array(), TRUE);

          echo $this->admin_form->dropdown('Tuần suất', 'frequency', array('weekly'=>'Hàng tuần'), 'weekly');

          echo $this->admin_form->input('Tiêu đề','title', @$meta['email_subject'], '', array('id'=>'title'));
          
          // echo $this->admin_form->input('Từ ngày','meta[config][report_date]',date('Y/m/d', @$edit->start_date),'', array('class'=>'set-datepicker','id' =>'report_date'));

          // echo $this->admin_form->input('Đến ngày','end_date',date('Y/m/d', @$edit->end_date),'', array('class'=>'set-datepicker','id'=>'end_date'));

          echo $this->admin_form->hidden('','mailid',0,'', array('id'=>'mailid'));

          echo $this->admin_form->input('Người nhận','meta[mail_report][email_to]', @$meta['mail_report']['email_to'], '', array('id'=>'email_to')); 
          echo $this->admin_form->input('CC','meta[mail_report][email_cc]', @$meta['mail_report']['email_cc'], '', array('id'=>'email_cc'));

          echo $this->admin_form->textarea('Nhận xét','post_content', @$meta['post_content'], 'Mỗi nhận xét cách nhau bởi xuống dòng', array('id'=>'post_content'));

          echo $this->admin_form->dropdown('Hoạt động', 'meta[config][action]', array('1_week'=>'1 tuần','1_month' => '1 tháng'), '1_week','Thời gian dữ liệu được lấy kể từ thời điểm gửi', array('id'=>'action'));

          echo $this->admin_form->dropdown('Ngày bắt đầu', 'meta[config][day_of_week]', array('Monday' => 'Thứ 2','Tuesday'=>'Thứ 3', 'Wednesday'=>'Thứ 4', 'Thursday'=>'Thứ 5', 'Friday'=>'Thứ 6', 'Saturday'=>'Thứ 7','Sunday'=>'Chủ nhật'), 'Monday','Thời gian gửi đi trong tuần',  array('id'=>'day_of_week'));
          ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="submit_scheduled_email">Save changes</button>
        <?php $this->admin_form->form_close();?>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function set_default_form()
  {
    $('input#title').val('[BÁO CÁO {website_name_up}] HIỆN TRẠNG DOANH NGHIỆP TỪ NGÀY {start_date} ĐẾN NGÀY {end_date}');
    $('input#mailid').val(0);
  }
  $('.ajax_edit').click(function(ev){
    var id = $(this).data('mailid');
    var jqxhr = $.getJSON( "<?php echo admin_url();?>webdoctor/ajax/email/edit/"+id, function(data) {
     $('input#mailid').val(data.id);
     $('input#title').val(data.title);
     $('input#email_to').val(data.email_to);
     $('input#email_cc').val(data.email_cc);
     $('#post_content').val(data.content);
     // $('select#action').val(data.action).trigger("change");
     $('select#frequency').val(data.frequency).trigger("change");
     // $('select#day_of_week').select2().select2('val',data.day_of_week)
     $('select#day_of_week').val(data.day_of_week).trigger("change");
   })
    .done(function() {
      $('#myModal').modal({
        show: 'true'
      }); 
    })
    .fail(function() {
      $.notify("Có lỗi xảy ra, không kết nối được dữ liệu", "error");
    });
    ev.preventDefault();
    return false;
  });


  $('.ajax_delete').click(function(ev){
    var id = $(this).data('mailid');
    var me = $(this);
    $(me).parents('tr').css('background-color','#FFEB3B');
    var jqxhr = $.get( "<?php echo admin_url();?>webdoctor/ajax/email/delete/"+id, function(data) {
      if(data =='OK')
      {
        $(me).parents('tr').fadeOut();
      }
      else
      {
       $(me).parents('tr').css('background-color','#FFF');
       $.notify(data, "error");
     }
   });
    ev.preventDefault();
    return false;
  });

  $('.set-datepicker').datepicker({
    format: 'yyyy/mm/dd',
    todayHighlight: true,
    autoclose: true,
  // startDate: '-0d'
});
  $(".datepicker").css("z-index", 9999);

  $(function(){
    $.fn.editable.defaults.url = ''; 
    $('.editable').editable({
     type: 'text',
     kpidate: 1,
     name: 'change-kpi',
     params: function(params) {
      // params.a = $(this).data('title');
      params.a = 1;
      return params;
    },
    success: function(response, newValue) {
      if(!response.success) return response.msg;
    },
    defaultValue: 0
  });
  });

</script>