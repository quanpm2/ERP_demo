<button type="button" id="add_task" name="add_new" class="btn btn-info pull-right btn-add-new" data-toggle="modal" data-target="#myModal" onclick="javascript:set_default_form();"><i class="glyphicon glyphicon-plus"></i>  Thêm mới</button> 
<?php echo $content['table'];?>
<?php echo $content['pagination'];?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Thêm mới</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<?php  echo $this->admin_form->form_open();
					// $this->form_builder->set_col(12,3);
					// $this->form_builder->hidden('taskid', '' ,'','','','taskid','Tiêu đề bài viết',''); 
					 echo $this->admin_form->hidden('','taskid',0,'', array('id'=>'taskid')); 
					echo $this->admin_form->input('Tiêu đề','title', '', '', array('id'=>'title'));
					echo $this->admin_form->input('Nội dung','content', '', '', array('id'=>'content'));

					echo $this->admin_form->dropdown('Loại bài viết', 'type', $this->config->item('task_type'), 'post-write','', array('id'=>'type'));

					echo $this->admin_form->input('Ngày bắt đầu','start_date',my_date(),'', array('class'=>'set-datepicker','id'=>'start_date')); 
					echo $this->admin_form->input('Ngày kết thúc','end_date',my_date(),'', array('class'=>'set-datepicker','id'=>'end_date')); 
					echo $this->admin_form->input('Tiến độ','done_ratio', '', '', array('id'=>'done_ratio', 'addon_end' => '%'));
					// $this->form_builder->input('title', '' ,'Tiêu đề','','','title','Tiêu đề bài viết','<i class="glyphicon glyphicon-text-color"></i>');  
					// $this->form_builder->input('content', '' ,'Nội dung','','','content','Mô tả','<i class="glyphicon glyphicon-link"></i>'); 
					// $this->form_builder->option('type', 'post-optimize', 'Loại bài viết','', $this->config->item('task_type'),'type');
					// $this->form_builder->input('start_date', my_date() ,'Ngày bắt đầu','','','start_date','Ngày bắt đầu','<i class="glyphicon glyphicon-calendar"></i>'); 
					// $this->form_builder->input('end_date', my_date() ,'Ngày kết thúc','','','end_date','Ngày kết thúc','<i class="glyphicon glyphicon-calendar"></i>'); 
					// $this->form_builder->input('done_ratio', '100' ,'Tiến độ','','','done_ratio','Tiến độ','','%'); 

					?>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
				<?php echo $this->admin_form->form_close();?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$("select[name='type']").change();
	$('#start_date,#end_date').datepicker({
		format: 'yyyy/mm/dd',
		todayHighlight: true,
		autoclose: true,
  // startDate: '-0d'
});
	$(".datepicker").css("z-index", 9999);

	$('.ajax_edit').click(function(ev){
		var id = $(this).data('taskid');
		var jqxhr = $.getJSON( "<?php echo module_url('ajax/task/edit/');?>"+id, function(data) {
			$('input#taskid').val(data.id);
			$('input#title').val(data.title);
			$('input#content').val(data.content);
			$('input#start_date').val(data.start_date);
			$('input#end_date').val(data.end_date);
			$('select#type').val(data.type);
			$('input#done_ratio').val(data.done_ratio);
		})
		.done(function() {
			$('#myModal').modal({
				show: 'true'
			}); 
		})
		.fail(function() {
			$.notify("Có lỗi xảy ra, không kết nối được dữ liệu", "error");
		});
		ev.preventDefault();
		return false;
	});


	$('.ajax_delete').click(function(ev){
		var id = $(this).data('taskid');
		var me = $(this);
		$(me).parents('tr').css('background-color','#FFEB3B');
		var jqxhr = $.get( "<?php echo module_url('ajax/task/delete/');?>"+id, function(data) {
			if(data =='OK')
			{
				$(me).parents('tr').fadeOut();
			}
			else
			{
				$(me).parents('tr').css('background-color','#FFF');
				$.notify(data, "error");
			}
		});
		ev.preventDefault();
		return false;
	});

	function set_default_form(){
		$('input#title').val('');
		$('input#taskid').val(0);
		$('input#content').val('');
		$('input#start_date').val('<?php echo my_date();?>');
		$('input#end_date').val('<?php echo my_date();?>');
       // $('input#end_date').val(data.end_date);
       $('select#type').val('post-optimize');
       $('input#done_ratio').val(100);
   };
      //Date range picker
      // $('#reservation').daterangepicker();
  </script>