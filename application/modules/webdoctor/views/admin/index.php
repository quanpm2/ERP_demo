

<?php

$time = time();
$time_start = date('Y-m-01', $time);
$time_end = date('Y-m-t', $time);
$total_day_of_month = date('t', $time);
$day = date('d', $time);
$this->table->set_caption('Thống kê tháng: <b>'.date('m/Y',$time).'</b> <a href="'.module_url('add').'" class="btn btn-block btn-danger pull-right col-md-3 " style="width: 10%;">Tạo mới</a>');
$this->table->set_heading(array('Website', 'Bài/chỉ tiêu','	SP/chỉ tiêu',''));

if($lists)
{
	foreach($lists as $term)
	{
		$ga_profile_id = $this->termmeta_m->get_meta_value($term->term_id, 'ga_profile_id');
		$seotraffic_type = $this->termmeta_m->get_meta_value($term->term_id, 'seotraffic_type');
		$kpis = $this->webdoctor_kpi_m->get_kpi_value($term->term_id,$time);
		$total_product = 0;
		$total_post = 0;
		foreach($kpis as $u_id => $kpi)
		{
			$total_product+= $kpi['target_product'];
			$total_post+= $kpi['target_post'];
		}
		$total = $total_product + $total_post;
		$total_done = $this->webdoctor_m->get_real_target($term->term_id,0, $time_start);
		$avg_day = $total / $total_day_of_month;

		$percent = ($total == 0) ? 0 : $total_done / $total;
		$percent = $percent * 100;

		$progress_color = ($total_done >= ($avg_day * $day)) ? 'progress-bar-aqua' : 'progress-bar-red';

		$g = '<div class="progress-group">
		<span class="progress-text" data-toggle="tooltip" title="% thực hiện hiện tại">'.numberformat($percent,2).'%'.'</span>
		<span class="progress-number"><b><span data-toggle="tooltip" title="Tổng thực hiện">'.$total_done.'</span></b>/<span data-toggle="tooltip" title="KPI">'.numberformat($total).'</span></span>
		<div class="progress sm">
			<div class="progress-bar '.$progress_color.' progress-bar-striped" style="width: '.$percent.'%"></div>
		</div>
	</div>';
	$this->table->add_row( 
		anchor(module_url('overview/'.$term->term_id), $term->term_name, 'data-toggle="tooltip" title="Thống kê chi tiết '.$term->term_name.'"'),
		$total_post,
		$total_product,
		$g
		// human_timing("",strtotime('2016-03-01'), 'nữa')

		);
}
}

echo $this->table->generate();

?>