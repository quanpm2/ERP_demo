<?php

$this->table->set_heading(array('Tháng', 'Tên', 'Số lượng bài','Số lượng sản phẩm','Hành động'));

$rows_id = array();
$data = array();
foreach ($targets as $i => $value) 
{
 $row_id = $value['target_date'];
 if(!isset($rows_id[$row_id]))
  $rows_id[$row_id] = 1;
else
  $rows_id[$row_id]++;
$username = $this->admin_m->get_field_by_id($value['user_id'], 'display_name');
$data[] = array($row_id,$username, $value['target_post'],$value['target_product'],anchor(module_url('kpi/'.$id.'/'.$value['utarget_id'].'/delete'), 'Xóa', 'class="btn btn-danger btn-flat btn-xs"'));
}
//btn btn-danger btn-flat btn-xs /$value['utarget_id']
$is_rowspan = FALSE;
if($data)
  foreach ($data as $i => $value) 
  {
   $row_id = $value[0];
   // $row_id = $this->mdate->convert_time($row_id);
   $rowspan = $rows_id[$row_id];
   $value[0] = $this->mdate->date('Y/m',$row_id);

   if($rowspan > 1 && $is_rowspan === FALSE)
   {
    $cell = array( 'data'=>$value[0], 'rowspan' => $rowspan +1);
    $rows_id[$row_id]--;
    unset($value[0]);
    $is_rowspan = TRUE;
    $this->table->add_row($cell);
    $this->table->add_row($value);
  }
  else
  {
    if($is_rowspan)
     unset($value[0]);

   $this->table->add_row($value);

   if($rows_id[$row_id] == 1)
     $is_rowspan = FALSE;
   $rows_id[$row_id]--;
 }
}

echo $this->table->generate();
?>

<?php 

echo $this->admin_form->form_open();

echo $this->admin_form->dropdown('Tháng', 'target_date', $date_contract, '');
echo $this->admin_form->dropdown('Nhân viên thực hiện', 'user_id', $users, '');

echo $this->admin_form->input('Số lượng bài','target_post'); 
echo $this->admin_form->input('Số lượng sản phẩm','target_product'); 
echo $this->admin_form->submit('submit','Lưu lại');
echo $this->admin_form->form_close();

?>