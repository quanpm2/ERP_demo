<!DOCTYPE html PUBLIC "-//W3C//Ddiv XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Ddiv/xhtml1-transitional.ddiv">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Báo cáo công việc <?php echo $site_name;?> từ ngày <?php echo date('d/m/Y', $start_date);?> đến ngày <?php echo date('d/m/Y', $end_date);?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="<?php echo theme_url();?>webdoctor/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo theme_url();?>webdoctor/css/style.css"/>
  <script type="text/javascript" src="<?php echo theme_url();?>webdoctor/js/Chart.js"></script>
</head>
<body>
  <div class="container">
    <div class="head">
      <div class="col-lg-3"><img src="<?php echo theme_url();?>webdoctor/images/webdoctor_logo.png" /></div>
      <div class="col-md-pull-3" style="float:right">sản phẩm của <img src="<?php echo theme_url();?>webdoctor/images/SC_logo.png"  /></div>
    </div>
    <div class="clear"></div>
    <div class="bg-success">
      <h2 class="text-center">BÁO CÁO CÔNG VIỆC <?php echo strtoupper($site_name);?></h2>
      <h5 class="text-center">(Từ ngày <?php echo date('d/m/Y', $start_date);?> đến ngày <?php echo date('d/m/Y', $end_date);?>)</h5>

      <div class="clear10"></div>
    </div>
    <div class="clear10"></div>
    <?php if($ga) { ?>
    <div class="col-lg-8 result">
      <div class="" >
        <h4 class="text-center">SỐ LiỆU CHI TiẾT</h4>

        <div class="row head">
          <div class="col-lg-2">Ngày</div> 
          <?php foreach($ga['headers'] as $day){ ?>
          <div class="col-lg-1 text-center">
            <?php echo date('d/m',strtotime($day));?>
          </div>
          <?php } ?>
          <div class="col-lg-1">Tổng</div>
        </div>

        <div class="row bg-warning">
          <div class="col-lg-2">Thứ</div>

          <?php 
          $days_week = array('CN','T2','T3','T4','T5','T6','T7');
          $result_date = array();
          foreach($ga['headers'] as $day){ 
            $val = $this->mdate->week_name(strtotime($day), $days_week);
            $result_date[] = $val;
            ?>
            <div class="col-lg-1 text-center"><?php echo $val;?></div>
            <?php } ?>
            <div class="col-lg-1">&nbsp;</div>
          </div>

          <div class="result">
            <?php
            $results = array();
            foreach($ga['total'] as $key => $total)
            {
              ?>
              <div class="row">
                <div class="col-lg-2"><?php echo $key;?></div>
                <?php foreach($ga['headers'] as $day){ 
                  $value = isset($ga['results'][$day][$key]) ? $ga['results'][$day][$key] : 0;
                  $results[$key][] = $value;
                  ?>
                  <div class="col-lg-1 text-center"><?php echo $value;?></div>
                  <?php } ?>
                  <div class="col-lg-1"><?php echo $total;?></div>
                </div>
                <?php } ?>
              </div>

            </div>
            <div class="clear10"></div>
          </div>
          

          <div class="col-lg-4 dotnut">
            <div id="canvas-holder" class="col-lg-8" style="margin-left:40px">
              <canvas id="chart-area" width="100" height="100"/>
            </div>
            <div class="col-lg-12 ">
              <div class="organic">Organic Search</div>
              <div class="direct">Direct</div>
              <div class="referral" >Referral</div>
              <div class="social" >Social</div>
            </div>

            <script>

              var doughnutData = [
              {
               value: <?php echo (isset($ga['total']['Organic Search']) && $ga['total']['Organic Search'] > 0) ? $ga['total']['Organic Search'] : 0; ?>,
               color:"#F7464A",
               highlight: "#FF5A5E",
               label: "Organic Search"
             },
             {
               value: <?php echo (isset($ga['total']['Direct']) && $ga['total']['Direct'] > 0) ? $ga['total']['Direct'] : 0; ?>,
               color: "#46BFBD",
               highlight: "#5AD3D1",
               label: "Direct"
             },
             {
               value: <?php echo (isset($ga['total']['Referral']) && $ga['total']['Referral'] > 0) ? $ga['total']['Referral'] : 0; ?>,
               color: "#FDB45C",
               highlight: "#FFC870",
               label: "Referral"
             },
             {
               value: <?php echo (isset($ga['total']['Social']) && $ga['total']['Social'] > 0) ? $ga['total']['Social'] : 0; ?>,
               color: "#949FB1",
               highlight: "#A8B3C5",
               label: "Social"
             }
             ];
           </script> 
           <div class="clear"></div>
         </div>
         <div class="col-lg-12" >
          <h4 class="text-center">BIỂU ĐỒ LƯU LƯỢNG TRUY CẬP - LƯỢC TÌM KIẾM</h4>
          <div class="row">
            <div class="col-lg-9">
              <canvas id="canvas" height="150" width="600"></canvas>
            </div>
            <div class="col-lg-2" style="margin:50px 0 0 40px">
              <div class="totalvisit">Tổng visit</div>
              <div class="organic">Organic search</div>
            </div>
            <script>
             var lineChartData = {
               labels : ["<?php echo implode('","', $result_date);?>"],
               datasets : [
               {
                 label: "Tổng visit",
                 fillColor : "rgba(220,220,220,0)",
                 strokeColor : "rgba(220,220,220,1)",
                 pointColor : "rgba(220,220,220,1)",
                 pointStrokeColor : "#fff",
                 pointHighlightFill : "#fff",
                 pointHighlightStroke : "rgba(220,220,220,1)",
                 data : [<?php echo implode(',', $results['Tổng visit']);?>]
               },
               {
                 label: "Organic search",
                 fillColor : "rgba(247,70,74,0)",
                 strokeColor : "rgba(247,70,74,1)",
                 pointColor : "rgba(247,70,74,1)",
                 pointStrokeColor : "#fff",
                 pointHighlightFill : "#fff",
                 pointHighlightStroke : "rgba(247,70,74,1)",
                 data : [<?php echo implode(',', $results['Organic Search']);?>]
               }
               ]

             }

             window.onload = function(){
              var ctx = document.getElementById("chart-area").getContext("2d");
              window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});
              var ctx2 = document.getElementById("canvas").getContext("2d");
              window.myLine = new Chart(ctx2).Line(lineChartData, {
               responsive: true
             });
            }


          </script> 
        </div>
      </div>


      <div class="clear10"></div>
       <?php } ?>
      <?php if($tasks): ?>
        <div class="col-lg-12 success" >
          <h4 class="text-center">BÁO CÁO CÔNG VIỆC ĐÃ THỰC HIỆN</h4>
          <div class="clear"></div>
          <div class="head">
            <div class="ordinal">Stt</div>
            <div class="issue">Công việc</div>
            <div class="status">Tình trạng</div>
            <div class="deadline">Cập nhật</div>
            <div class="note">Ghi chú</div>
            <div class="clear"></div>
          </div>
          <?php foreach($tasks as $i=>$task): 
          $done = $this->postmeta_m->get_meta($task->post_id,'webdoctor_done_ratio',TRUE,TRUE);
          $done = ($done ==100) ? 'Hoàn thành' :$done.'%';
          ?>
          <div class="row">
            <div class="ordinal"><?php echo ++$i;?></div>
            <div class="issue"><?php echo $task->post_title;?></div>
            <div class="status"><?php echo $done;?></div>
            <div class="status"><?php echo $this->mdate->date('d/m/Y',$task->start_date);?></div>
            <div class="note"><?php 
            if(valid_url($task->post_content))
            {
              $task->post_content = anchor($task->post_content, 'Link',array('title' => $task->post_content,'target' =>'_blank'));
            }
            echo $task->post_content;?></div>
          </div>
        <?php endforeach;?>
      </div>
    <?php endif;?>
    <div class="clear10"></div>




    <?php if($bias['tasks']): ?>
      <div class="col-lg-12 success" >
        <h4 class="text-center">DỰ KIẾN CÔNG VIỆC</h4>
        <div class="clear"></div>
        <div class="head">
          <div class="ordinal">STT</div>
          <div class="issue">Công việc dự kiến</div>
          <div class="deadline">&nbsp;</div>
          <div class="deadline">Deadline</div>
          <div class="note">Ghi chú</div>
          <div class="clear"></div>
        </div>
        <?php foreach($bias['tasks'] as $i=>$task): 
        $done = $this->postmeta_m->get_meta($task->post_id,'webdoctor_done_ratio',TRUE,TRUE);
        $done = ($done ==100) ? 'Hoàn thành' :$done.'%';
        ?>
        <div class="row">
          <div class="ordinal"><?php echo ++$i;?></div>
          <div class="issue"><?php echo $task->post_title;?></div>
          <div class="deadline">&nbsp;</div>
          <div class="deadline"><?php echo $this->mdate->date('d/m/Y',$task->start_date);?></div>
          <div class="note">&nbsp;</div>
        </div>
      <?php endforeach;?>

      <div class="clear10"></div>
    </div>
  <?php endif;?>


  <?php if($reviews): ?>
    <div class="col-lg-12 success" >
      <h4 class="text-center">CHI TIẾT CÔNG VIỆC</h4>
      <div class="row">
        <table cellspacing="0" cellpadding="0" width="100%">
          <tr class="head">
            <td width="4%">STT</td>
            <td width="20%">Các    đầu việc phải làm</td>
            <td width="35%">Lỗi</td>
            <td width="8%">Hiện trạng</td>
            <td>Đề xuất</td>
          </tr>

          <?php foreach($reviews as $i=>$task): 
          $done = $this->postmeta_m->get_meta($task->post_id,'webdoctor_done_ratio',TRUE,TRUE);
          $done = ($done ==100) ? 'Review xong' :$done.'%';
          $group = $this->postmeta_m->get_meta($task->post_id,'webdoctor_review_group',TRUE,TRUE);
          $t = explode('.', $group);

          ?>
          <tr<?php echo in_array(@$t[0], array('2','4','6')) ? ' class="bg-info"':'';?>>
          <td><?php echo $group;?></td>
          <td><?php echo $task->post_title;?></td>
          <td><?php  echo $task->post_content;?></td>
            <td><?php echo $done;?></td>
            <td>&nbsp;</td>
          </tr>
        <?php endforeach;?>
      </table>
    </div>
  </div>
<?php endif;?>
</div>
</body>
</html>
