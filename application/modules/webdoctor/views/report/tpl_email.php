Kính chào quý khách! <br>
<b>WEBDOCTOR.VN</b> kính gửi quý khách báo cáo các công việc đã thực hiện từ ngày <b><?php echo date('d/m/Y',$start_date); ?> đến ngày <?php echo date('d/m/Y',$end_date); ?>.</b> <br>
<?php echo anchor( $report_link, 'Quý khách vui lòng bấm vào đây để xem chi tiết báo cáo này', 'title="Xem chi tiết báo cáo" target="_blank"'); ?>
<p></p>
<?php 
$template = array(
        'table_open' => '<table  border="0" cellpadding="5" cellspacing="0" style="font-size:12px; font-family:Arial" width="100%">',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th style="border-bottom:1px dotted #999">',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td style="border-bottom:1px dotted #999">',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td style="border-bottom:1px dotted #999">',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);

$template['thead_open'] = '<thead><tr>
<th colspan="5" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">BÁO CÁO CÔNG VIỆC ĐÃ THỰC HIỆN</th>
</tr>';
$this->table->set_template($template);

$this->table->set_heading(array('STT', 'Công việc', 'Tình trạng','Cập nhật','Ghi chú'));
// echo '<div>BÁO CÁO CÔNG VIỆC ĐÃ THỰC HIỆN</div>';
foreach($tasks as $i=>$task)
{
	$done = $this->postmeta_m->get_meta($task->post_id,'webdoctor_done_ratio',TRUE,TRUE);
	$done = ($done ==100) ? 'Hoàn thành' :$done.'%';

	if(valid_url($task->post_content))
	{
		$task->post_content = anchor($task->post_content, 'Link',array('title' => $task->post_content,'target' =>'_blank'));
	}
	$this->table->add_row(++$i, $task->post_title, $done, date('d/m/Y',$task->end_date),$task->post_content);
}
echo $this->table->generate();

echo '<br>';

if($bias['tasks']) {
	$template['thead_open'] = '<thead><tr>
<th colspan="5" style="background: #0072bc none repeat scroll 0 0; color: #fff;font-weight: bold;padding: 8px;text-align:center; width:100%">DỰ KIẾN CÔNG VIỆC ('.date('d/m/Y',$bias['start_date']).' đến ngày '.date('d/m/Y',$bias['end_date']).')</th>
</tr>';
$this->table->set_template($template);

	$this->table->set_heading(array('STT', 'Công việc', 'Dự kiến','Ghi chú'));
	// echo 'DỰ KIẾN CÔNG VIỆC ('.date('d/m/Y',$bias['start_date']).' đến ngày '.date('d/m/Y',$bias['end_date']).')';
	foreach($bias['tasks'] as $i=>$task)
	{
		if(valid_url($task->post_content))
		{
			$task->post_content = anchor($task->post_content, 'Link',array('title' => $task->post_content,'target' =>'_blank'));
		}
		$this->table->add_row(++$i, $task->post_title, date('d/m/Y',$task->end_date),$task->post_content);
	}
	echo $this->table->generate();

}
?>

<br>
Cảm ơn quý khách đã sử dụng dịch vụ của <b><a href="//webdoctor.vn" target="_blank" style="color:#00F">WEBDOCTOR.VN</a></b>. <br>

<em style="color:#666; font-size:10px">* Đây là mail báo cáo tự động gửi từ hệ thống, vì vậy Quý khách không phải reply lại email này.</em> <br>
