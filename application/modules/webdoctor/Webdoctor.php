<?php
class Webdoctor_Package extends Package{
	private $website_id;
	public function name()
	{
		return 'Webdoctor';
	}

	public function init(){
		return false;
		if(has_permission('Webdoctor.Index.access') && is_module_active('webdoctor'))
		{
			$this->website_id  = $this->uri->segment(4);
			$module_url = admin_url().'webdoctor/';
			$this->menu->add_item(array(
				'id' => 'webdoctor',
				'name' => 'Webdoctor',
				'parent' => null,
				'slug' => $module_url,
				'order' => 2,
				'icon' => ''
				), 'navbar');

			if(is_module('webdoctor') && $this->website_id > 0)
			{
				$methods = array('overview'=>'Overview','task' => 'Công việc', 'kpi' =>'KPI','email'=>'Email');
				$order = 0;
				foreach($methods as $method=>$name)
				{
					if(has_permission('Webdoctor.'.$method.'.access'))
						$this->menu->add_item(array(
							'id' => 'seotraffic-'.$method,
							'name' => $name,
							'parent' => NULL,
							'slug' => $module_url.$method.'/'.$this->website_id,
							'order' => $order++,
							'icon' => ''
							), 'left');
				}
			}
		}
	}

	public function title()
	{
		return 'Webdoctor';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '0.1';
	}

	public function description()
	{
		return 'Webdoctor';
	}

	private function init_permissions()
	{
		$permissions = array();
		$permissions['Module.Webdoctor'] = array(
			'description' => '',
			'actions' => array('manage'));

		$permissions['Webdoctor.Index'] = array(
			'description' => '',
			'actions' => array('access'));

		$permissions['Webdoctor.Overview'] = array(
			'description' => '',
			'actions' => array('access'));

		$permissions['Webdoctor.Task'] = array(
			'description' => '',
			'actions' => array('access','add','delete','update'));

		$permissions['Webdoctor.KPI'] = array(
			'description' => '',
			'actions' => array('access','add','delete','update'));

		$permissions['Webdoctor.Email'] = array(
			'description' => '',
			'actions' => array('access','add','delete','update'));

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}