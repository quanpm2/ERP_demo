<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Public_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('webdoctor_mail_m');
		// $this->load->module('seotraffic');
		// $this->load->model('seotraffic_report_m');
	}

	public function _remap($method, $seg = '')
	{
		if($method == 'send')
			return $this->send($seg);
		return $this->index($method);
	}


	//report view
	public function index($site_name = '')
	{
		$this->load->model('webdoctor_m');
		
		$this->load->model('postmeta_m');
		$this->load->model('term_m');
		$this->load->library('mdate');

		$site_name = strtolower($site_name);


		$post_id = $this->input->get('p');
		$post = $this->webdoctor_m->get($post_id);
		$term = $this->term_posts_m->get_post_terms($post_id, 'webdoctor');
		if(!$term || !$post_id || !$post)
		{
			show_frontend_404();
		}
		
		$start_date = date('Y-m-d',$post->start_date);
		$end_date = strtotime('+7 day', $post->start_date);
		$end_date = date('Y-m-d',$end_date);

		$code = rawurldecode($this->input->get('domain'));
		

 		if(isset($term[0]))
 			$term = $term[0];

		$decrypted = $this->webdoctor_m->encrypt($code,$term, 'decode');

		// if(strtolower($decrypted) != $site_name)
			// show_frontend_404();
		$id = $term->term_id;
		$data = array();
		$ga_profile_id = $this->termmeta_m->get_meta_value($id, 'ga_profile_id');
		
		$data['ga'] = $this->get_ga($start_date, $end_date,$ga_profile_id);


		$start_date = $this->mdate->startOfDay($start_date);
		$end_date = $this->mdate->endOfDay($end_date);
		$data['tasks'] = $this->webdoctor_m->get_posts_from_date($start_date, $end_date,$id);

		//Dự kiến
		$data['bias'] = array();
		$data['bias']['start_date'] = $end_date + 1;

		$data['bias']['end_date'] = strtotime('+7 day', $data['bias']['start_date']);
		$data['bias']['end_date'] = $this->mdate->endOfDay($data['bias']['end_date']);
		$data['bias']['tasks'] = $this->webdoctor_m->get_posts_from_date($data['bias']['start_date'], $data['bias']['end_date'],$id);

		$data['reviews'] = $this->webdoctor_m->get_posts_from_date(1, 991447835544,$id, array('meta_key'=>'webdoctor_task_type', 'meta_value'=>'review', 'order'=> array('meta_value'=>'asc')));
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		$data['site_name'] = $term->term_name;

		$this->load->view('report/tpl_report', $data);
	}


	private function get_ga($start_date, $end_date, $ga_profile_id)
	{
		if(!$ga_profile_id)
			return FALSE;
		$key_cache = 'ga/'.$ga_profile_id.'/'.$start_date.'-'.$end_date;
		if(!$ga = $this->scache->get($key_cache))
		{
			$ga = $this->webdoctor_mail_m->get_ga($start_date, $end_date, $ga_profile_id);
			$time_cache = 0;
			if($this->mdate->startOfDay($end_date) >= $this->mdate->startOfDay())
				$time_cache = 1800; //cache 30 phút
			$this->scache->write($ga,$key_cache, $time_cache);
		}
		if(!$ga)
			return FALSE;

		$data = array();
		$data['ga'] = array();
		$data['ga']['results'] = array();
		$data['ga']['total'] = array();
		$data['ga']['headers'] = array_keys($ga['results']);

		foreach($ga['results'] as $date => $results)
		{
			unset($results['ga:pageviewsPerSession']);
			foreach($results as $key=>$result)
			{
				$tmp_key = $key;
				switch ($key) {
					case 'ga:avgSessionDuration':
					$result = gmdate('H:i:s',$result);
					$key = 'Time on site';
					break;
					case 'ga:bounceRate':
					$result = (int)$result.'%';
					$key = 'Bounce Rate';
					break;
					case 'ga:sessions':
					$key = 'Tổng visit';
					break;
					case 'Organic Search':
					case 'Referral':
					case 'Social':
					case '(Other)':
					break;

					default:

					break;
				}

				if(isset($ga['totalsForAllResults'][$tmp_key]))
				{
					$val = $ga['totalsForAllResults'][$tmp_key];
					unset($ga['totalsForAllResults'][$tmp_key]);
					switch ($tmp_key) {
						case 'ga:avgSessionDuration':
						$val = gmdate('H:i:s',$val);
						break;
						case 'ga:bounceRate':
						$val = (int)$val.'%';
						break;
						default:

						break;
					}
					$data['ga']['total'][$key] = $val;
				}
				$data['ga']['results'][$date][$key] = $result;
			}
		}
		return $data['ga'];
	}

	public function send()
	{
		$terms = $this->term_m->where('term_status', 'publish')->get_many_by('term_type', 'webdoctor');
		foreach($terms as $term)
		{
			echo $this->webdoctor_mail_m->send_all_reports($term->term_id);
		}
	}
}

/* End of file report.php */
/* Location: ./application/controllers/report.php */