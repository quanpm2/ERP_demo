<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webdoctor extends Admin_Controller {

	public $model = 'webdoctor_m';
	public $term_type = 'webdoctor';
	public $post_type = '';
	public $website_id = 0;
	function __construct(){

		parent::__construct();

		$this->load->model('webdoctor_m');
		$this->load->model('webdoctor_kpi_m');
		$this->load->model('webdoctor_mail_m');
		$type = $this->input->get('type');
		$this->post_type = $this->webdoctor_m->post_type;
		$this->load->config('webdoctor');
		$this->init_website();
		// $this->data['user_type'] = ($type && in_array($type,$this->customer_m->customer_types)) ? $type :'customer_person';
	}

	private function init_website()
	{
		if($this->website_id == 0)
		{
			$this->website_id  = $this->uri->segment(4);
			$method  = $this->uri->segment(3);
			if(!in_array($method, array('index','','ajax','send_mail')))
			{
				$this->term = $this->term_m->get($this->website_id);
				$this->template->title->set(strtoupper($this->term->term_name));
			}
		}
	}
	public function index()
	{
		$data = array();
		$this->template->title->append('Chọn website');
		$data['lists'] = $this->term_m->where_in('term_status', array(1,'publish'))->get_many_by(array(
			'term_type' => $this->term_type,
			));
		parent::render($data);
	}

	public function edit($edit_id = 0)
	{

	}

	public function add()
	{
		
	}

	public function overview($id = 0)
	{
		$data = array();
		parent::render($data);
	}

	public function kpi($id = 0, $delete_id= 0)
	{
		$data = array();
		if($delete_id >0)
		{
			$this->webdoctor_kpi_m->delete($delete_id);
			$this->messages->success('Xóa thành công');
			redirect(module_url('kpi/'.$id.'/'),'refresh');
		}

		if($this->input->post())
		{	
			$array_insert = array();
			$array_insert['target_date'] = $this->input->post('target_date');
			$array_insert['target_date'] = date('Y-m-01',$array_insert['target_date']);
			$array_insert['user_id'] = $this->input->post('user_id');
			$array_insert['target_post'] = $this->input->post('target_post');
			$array_insert['target_product'] = $this->input->post('target_product');
			$array_insert['term_id'] = $id;

			$check = $this->webdoctor_kpi_m->get_by(array(
				'user_id' => $array_insert['user_id'],
				'term_id' => $array_insert['term_id'],
				'target_date' => $array_insert['target_date']
				));
			$new_id = FALSE;
			if($check)
			{
				unset($array_insert['term_id']);
				$new_id = $this->webdoctor_kpi_m->update($check->utarget_id,$array_insert);
				$this->messages->success('Cập KPI nhật thành công');
			}
			else
			{
				$new_id = $this->webdoctor_kpi_m->insert($array_insert);
				$this->messages->success('Thêm KPI thành công');
			}

			if($new_id ==FALSE)
				$this->messages->error('Thêm KPI không thành công');
			redirect(current_url(),'refresh');
		}

		$time_start = strtotime('2016-01-01');
		$time_end = strtotime('2017-01-01');

		$data['time_start'] = $time_start;
		$data['time_end'] = $time_end;

		$data['targets'] = $this->webdoctor_kpi_m->as_array()->order_by('target_date')->get_many_by(array(
			'term_id'=>$id
			));

				//hợp đồng tối đa 48 tháng
		for($i=0;$i<48; $i++)
		{
			$data['date_contract'][$time_start]  = date('Y-m', $time_start);
			$date = $data['date_contract'][$time_start];

			$time_start = strtotime('+1 month', strtotime($date));
			if($time_start > $time_end)
				break;
		}
		$data['users'] = $this->admin_m->select('user_id,display_name')->set_get_active()->order_by('display_name')->get_many_by();
		$data['users'] = key_value($data['users'],'user_id','display_name');

		$data['id'] = $id;
		parent::render($data);
	}
	public function email($term_id = 0)
	{
		$post_type = 'webdoctor-email';
		if($this->input->post())
		{
			$insert = array();
			$meta = $this->input->post('meta');

			$insert['post_title'] =  $this->input->post('title');
			$insert['post_content'] =  $this->input->post('post_content');
			$mail_id = $this->input->post('mailid');
			if($this->input->post('frequency') == 'weekly')
			{
				$insert['start_date'] = strtotime(''.@$meta['config']['day_of_week']);
				if($mail_id && $mail_id >0)
				{
					$this->webdoctor_m->update($mail_id, $insert);
					$post_id =$mail_id;
				}
				else
				{
					$insert['post_type'] = $post_type;
					$insert['end_date'] = strtotime('+7 day',$insert['start_date']);
					$post_id = $this->webdoctor_m->insert($insert);
					$this->term_posts_m->set_post_terms($post_id, array($term_id), $this->term_type);
				}
				$meta['config']['frequency']= $this->input->post('frequency');
				$meta = serialize($meta);
				$this->postmeta_m->update_meta($post_id, 'webdoctor_mail_report',$meta);
			}
			redirect(current_url(),'refresh');
		}

		$term = $this->term;

		$this->search_filter();
		$this->admin_ui->add_column('posts.post_id','ID');
		$this->admin_ui->select('term_id,post_status,end_date');
		$this->admin_ui->add_column('post_title','Tiêu đề', NULL, NULL, array('class' =>'col-md-4'));
		$this->admin_ui->add_column('frequency', array('set_select'=>FALSE,'set_order'=>FALSE, 'title' =>'Lịch gửi'));

		$this->admin_ui->add_column('start_date','Trạng thái');
		$this->admin_ui->add_column('recipients', array('set_select'=>FALSE,'set_order'=>FALSE, 'title' =>'Người nhận'));

		$this->admin_ui->from('posts');
		$this->admin_ui->join('term_posts', 'term_posts.post_id = posts.post_id');
		$this->admin_ui->where('term_id',$term_id);
		$this->admin_ui->where('post_type', $post_type);

		$this->admin_ui->add_column_callback('frequency',function($data, $row_name){
			$meta = $this->postmeta_m->get_meta($data['post_id'], 'webdoctor_mail_report', TRUE, TRUE);
			$meta = @unserialize($meta);
			if($meta)
			{
				$mail_report = $meta['mail_report'];
				$mail_config = $meta['config'];
				$data['frequency'] =  '<b>'.$this->mdate->week_name($mail_config['day_of_week']).'</b> hàng tuần<br>Dữ liệu: 1 tuần hoạt động';
				$data['recipients'] = 'Mail to: '.$mail_report['email_to'];
				if(isset($mail_report['email_cc']) && $mail_report['email_cc'])
				{
					$data['recipients'].='<br> Mail CC: '.$mail_report['email_cc'];
				}
			}
			return $data;
		}, FALSE);
		
		$this->admin_ui->add_column_callback('post_title',function($data, $row_name){
			$data['post_title'] = $this->webdoctor_mail_m->parse_email_template($data['post_title'], array(
				'start_date' => $data['start_date'],
				'end_date' => $data['end_date']
				), $data['term_id']);
			return $data;
		}, FALSE);
		
		$this->admin_ui->add_column_callback('start_date',function($data, $row_name){
			$date = $data['end_date'];
			$status = '';
			switch ($data['post_status']) {
				case 'publish':
				$status = '<span class="label label-info">Chờ gửi</span>';
				break;
				case 'sended':
				$status = '<span class="label label-success">Đã gửi</span>';
				break;
				case 'error':
				$status = '<span class="label label-danger">Gửi bị lỗi</span>';
				break;
			}

			if(time() > $date)
			{
				$text = human_timing($date,"", 'trước');
			}	
			else
			{
				$text = human_timing("",$date, 'nữa');
			}

			$data['start_date'] = ''.$status.' '.$text.'<br>';
			$data['start_date'].= 'Ngày: '.date('d/m/Y',$date);

			return $data;
		}, FALSE);

		$this->admin_ui->add_column('view', array('set_select'=>FALSE, 'title' =>'Xem', 'set_order'=>FALSE));

		$this->admin_ui->add_column_callback('view',function($data, $row_name, $term){
			$time = time();
			$text = '';
			if($data['post_status'] == 'publish')
				$text.= anchor(current_url(),'Edit','class="ajax_edit" data-mailid="'.$data['post_id'].'"').' | ';

			$text.= anchor(current_url(),'Xóa','class="ajax_delete" data-mailid="'.$data['post_id'].'"').' | ';
			$text.= anchor($this->webdoctor_mail_m->get_report_link($data['term_id'], $term->term_name, $data['post_id']),'Xem','target="_blank"');

			$data['view'] = $text;
			return $data;
		}, FALSE,$term);



		$this->admin_ui->order_by("posts.post_id desc");
		$data['content'] = $this->admin_ui->generate();
		$data['term_id'] = $term_id;

		$end_post = $this->term_posts_m
		->join('posts', 'posts.post_id = term_posts.post_id')
		->where('post_type', 'webdoctor-email')
		->order_by('term_posts.post_id','desc')->get_by('term_id', $term_id);

		if($end_post)
		{
			$meta = $this->postmeta_m->get_meta_value($end_post->post_id, 'webdoctor_mail_report');
			$meta = @unserialize($meta);
		}
		

		$data['meta'] = @$meta;
		$this->template->title->prepend('Email ');
		parent::render($data);
	}

	public function task($id = 0)
	{
		$this->term_type = $this->webdoctor_m->term_type;
		$this->post_type = $this->webdoctor_m->post_type;

		if($this->input->post())
		{
			Modules::run('webdoctor/ajax/insert_task',$id);
		} 

		$this->search_filter();
		$this->admin_ui->add_column('posts.post_id','#');
		$this->admin_ui->add_column('post_title','Tiêu đề');
		$this->admin_ui->add_column('post_content', 'Nội dung');
		$this->admin_ui->add_column('start_date','Ngày bắt đầu','$1','date("d/m/Y",start_date)');

		$this->admin_ui->where('post_type',$this->post_type);
		$this->admin_ui->where('term_id',$id);
		$this->admin_ui->order_by("posts.post_id desc");
		$this->admin_ui->from('posts');
		$this->admin_ui->join('term_posts', 'term_posts.post_id = posts.post_id');

		$this->admin_ui->add_column_callback(array('post_content'),array($this,'callback_post_get_meta'), FALSE);
		$this->admin_ui->add_column('view', array('set_select'=>FALSE, 'title' =>'Xem', 'set_order'=>FALSE),anchor(module_option_url('webdoctor','tasks/edit/$1'),'Edit','class="ajax_edit" data-taskid="$1"').' | '.anchor(module_option_url('webdoctor','tasks/edit/$1'),'Xóa','class="ajax_delete" data-taskid="$1"'),'post_id');
		$data['content'] = $this->admin_ui->generate(array(
			'uri_segment' => 5,
			'base_url'=> admin_url().'webdoctor/task/'.$id.'/'
			));
		parent::render($data);
	}


	function callback_post_get_meta($post, $row_name, $arg = array())
	{
		switch ($row_name) {
			case 'post_content':
			if(valid_url($post['post_content']))
			{
				$post['post_content'] = anchor($post['post_content'], 'Link',array('title' => $post['post_content'],'target' =>'_blank'));
			}
			break;



			case 'display_name':
			$post['display_name'] = $this->admin_m->get_field_by_id($post['post_author'],'display_name');
			break;

			case 'type':
			$type = $this->postmeta_m->get_meta($post['post_id'],'webdoctor_task_type',TRUE, TRUE);
			$data = $this->config->item('task_type');
			$post['type'] = isset($data[$type]) ? $data[$type] : '';
			break;

			default:
			break;
		}
		return $post;
	}
}