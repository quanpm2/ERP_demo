<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Admin_Controller {
	public $term_type = '';
	public $post_type = '';
	public $report_status = array();

	function __construct()
	{
		parent::__construct();
		$this->load->model('webdoctor_m');
		$this->term_type = $this->webdoctor_m->term_type;
		$this->post_type = $this->webdoctor_m->post_type;
		$this->load->config('webdoctor');
	}

	public function kpi($id = 0)
	{
		
	}
	public function index($id = 1, $page = 1){		
		die('Notthing');
	}

	function task($type= '', $id = 0)
	{
		switch ($type) {
			case 'edit':
			return $this->ajax_edit($id);
			break;
			case 'delete':
			return $this->ajax_delete($id);
			break;

			default:
				# code...
			break;
		}
	}
	
	function email($type= '', $id = 0)
	{
		$this->load->model('webdoctor_mail_m');
		// $this->term_type = $this->webdoctor_mail_m->term_type;
		$this->post_type = $this->webdoctor_mail_m->mail_type;
		switch ($type) {
			case 'edit':
			return $this->ajax_email_edit($id);
			break;
			case 'delete':
			return $this->ajax_delete($id);
			break;

			default:
				# code...
			break;
		}
	}
	private function ajax_delete($post_id = 0)
	{
		if($post_id <= 0) return '';
		$this->post_m->where('post_type', $this->post_type)->delete($post_id);
		if($this->post_m->affected_rows() > 0)
		{
			echo 'OK';
		}
		else
		{
			echo 'Có lỗi xảy ra';
		}
	}

	private function ajax_email_edit($post_id = 0)
	{
		if($post_id <= 0) return '';
		$post = $this->post_m->get_by(array('post_id'=>$post_id, 'post_type' => $this->post_type));
		if(!$post)
			return FALSE;
		$results = array();
		$results['id'] = $post->post_id;
		$results['title'] = $post->post_title;
		$results['content'] = $post->post_content;
		// $results['content'] = $post->end_date
		$results['start_date'] = my_date($post->start_date);
		$results['end_date'] = my_date($post->end_date);

		$meta = $this->postmeta_m->get_meta_value($post_id,'webdoctor_mail_report');

		$meta = @unserialize($meta);
			if($meta)
			{
				$mail_report = $meta['mail_report'];
				$mail_config = $meta['config'];
				$results['frequency'] = $mail_config['frequency'];
				$results['day_of_week'] = $mail_config['day_of_week'];
				$results['email_to'] =   $mail_report['email_to'];
				$results['email_cc'] =   $mail_report['email_cc'];
			}
			
		$this->output->set_content_type('application/json', 'utf-8')->set_output(json_encode($results)) ->_display();;
		die();
	}
	private function ajax_edit($post_id = 0)
	{
		if($post_id <= 0) return '';
		$post = $this->post_m->get_by(array('post_id'=>$post_id, 'post_type' => $this->post_type));
		if(!$post)
			return FALSE;
		$results = array();
		$results['id'] = $post->post_id;
		$results['title'] = $post->post_title;
		$results['content'] = $post->post_content;
		// $results['content'] = $post->end_date
		$results['start_date'] = my_date($post->start_date);
		$results['end_date'] = my_date($post->end_date);
		$results['type'] = $this->postmeta_m->get_meta($post_id,'webdoctor_task_type',TRUE, TRUE);
		$results['done_ratio'] = $this->postmeta_m->get_meta($post_id,'webdoctor_done_ratio',TRUE, TRUE);
		$this->output->set_content_type('application/json', 'utf-8')->set_output(json_encode($results)) ->_display();;
		die();
	}

	function insert_task($contract_id = 0)
	{
		if(($contract_id >0 ) && ($title = $this->input->post('title'))
			&& ($this->input->post('content')))
		{
			$post_id = $this->input->post('taskid');

			$insert = array();

			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			if($start_date)
				$start_date = strtotime($start_date);
			else
				$start_date = time();

			if($end_date)
				$end_date = strtotime($end_date);
			else
				$end_date = time();

			$insert['post_title'] = $title;
			$insert['start_date'] = $start_date;
			$insert['end_date'] = $end_date;
			$insert['post_content'] = $this->input->post('content');

			if($post_id ==0)
			{
				$insert['created_on'] = time();
				$insert['post_type'] = $this->post_type;
				$insert['post_author'] = $this->admin_m->id;
				$post_id = $this->post_m->insert($insert);
				$this->term_posts_m->set_post_terms($post_id, array($contract_id), $this->term_type);
				$this->messages->add("Thêm ".$title." thành công.",'success');
			}
			else
			{
				$insert['updated_on'] = time();
				$this->post_m->update($post_id,$insert);
				$this->messages->add("Cập nhật ".$title." thành công.",'info');
			}

			$done_ratio = (int)$this->input->post('done_ratio');
			$this->postmeta_m->update_meta($post_id, 'webdoctor_task_type', $this->input->post('type'));
			$this->postmeta_m->update_meta($post_id, 'webdoctor_done_ratio', $done_ratio);

		}
		else
		{
			$this->messages->error("Nội dung hoặc tiêu đề không được bỏ trống");
		}
		redirect(current_url());
	}
}

/* End of file task.php */
/* Location: ./application/controllers/task.php */