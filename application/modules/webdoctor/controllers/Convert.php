<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Convert extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('webdoctor_mail_m');
		$this->load->model('webdoctor_kpi_m');
		$this->load->dbutil();
		$this->load->dbforge();
	}

	public function index()
	{
		$this->convert_table();
		$this->update_mail();
		$this->update_term();
		echo 'done convert_table';

	}

	function update_term()
	{
		$terms = $this->term_m->get_many_by(array(
			'term_type' =>'contract-webdoctor',
			));
		if($terms)
		foreach($terms as $term)
		{
			$this->term_m->update($term->term_id, array(
				'term_type' => 'webdoctor',
				'term_status' => 1
				));
		}
	}

	function update_mail()
	{
		$posts = $this->post_m->get_many_by(array(
			'post_type' =>'webdoctor-email',
			));
		foreach($posts as $post)
		{
			if($post->end_date != 0)
				continue;

			$time_end = strtotime('+7 day',$post->start_date);
			$this->post_m->update($post->post_id, array(
				'end_date' => $time_end
				));
		}
	}
	function convert_table()
	{
		$this->dbforge->drop_table('webdoctor_kpi',TRUE);
		$this->db->query('create table webdoctor_kpi like user_target;');
		$this->db->query('insert into webdoctor_kpi select * from user_target;');
		$this->dbforge->add_column('webdoctor_kpi', array(
			'tmp_target_date' => array(
				'type' => 'date',
				)
			));

		$this->change_date_kpi();
		$this->dbforge->drop_column('webdoctor_kpi', 'target_date');
		$this->db->query('ALTER TABLE webdoctor_kpi
			CHANGE COLUMN tmp_target_date target_date DATE');

	}
	function change_date_kpi(){
		
		$results = $this->webdoctor_kpi_m->get_many_by();
		foreach($results as $result)
		{
			$target_date = $result->target_date;
			// $target_date = $this->mdate->endOfMonth($target_date);
			$target_date = $this->mdate->startOfMonth($target_date);
			// $target_date = strtotime('+1 month',$target_date);
			$this->webdoctor_kpi_m->update($result->utarget_id, array(
				'tmp_target_date' => date('Y-m-01', $target_date)
				));
			$this->webdoctor_kpi_m->delete_cache($result->term_id);


		}	
		$this->term_m->clean_cache();
	}
}

/* End of file convert.php */
/* Location: ./application/controllers/convert.php */