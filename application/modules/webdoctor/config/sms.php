<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['account_name'] = getenv('SMS_INCOME_ACCOUNT_NAME');
$config['service_id'] = getenv('SMS_WEBDOCTOR_SERVICE_ID');
$config['command_code'] = getenv('SMS_WEBDOCTOR_COMMAND_CODE');
$config['account_password'] = getenv('SMS_INCOME_ACCOUNT_PWD');