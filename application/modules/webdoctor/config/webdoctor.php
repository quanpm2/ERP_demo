<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['task_type'] = array(
	'post-optimize'=>'Tối ưu bài viết', 
	'post-write' => 'Tự viết bài', 
	'other' =>'Việc khác', 
	'product-write' =>'Đăng sản phẩm',
	'product-edit' =>'Sửa sản phẩm',
	'review' =>'Review Website',
	'custom-task' =>'Yêu cầu khách hàng'
	);