<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webdoctor_mail_m extends Base_model {
	public $_table = 'mail_log';
	public $primary_key = 'mail_id';
	public $mail_type = 'webdoctor-email';

	function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->model('postmeta_m');
		$this->load->model('webdoctor_m');
		$this->load->model('term_posts_m');
		$this->load->model('term_m');
		$this->load->library('mdate');
	}
	function get_mail($mail_id)
	{
		$this->where('mail_type',$this->mail_type);
		return $this->get($mail_id);
	}
	
	function get_ga($start_date, $end_date, $profileId)
		{
		/*
		Organic Search
		Direct
		Referral
		Social
		-----------
		Acquisition Overview


		Tổng visit
		Page/ Visit
		Bounce Rate
		Time on site
		----------
		Audience Overview
		*/

		if(!$profileId)
			return FALSE;
		$key_cache = 'ga/'.$profileId.'/'.$start_date.'-'.$end_date;
		if($ga = $this->scache->get($key_cache))
		{
			return $ga; //return cache
		}

		$this->load->add_package_path(APPPATH.'third_party/google-analytics', FALSE);
		// $this->load->library('ga');
		$data = array();
		if(strpos($profileId, 'ga:') === FALSE)
			$profileId = 'ga:'.$profileId;

		//Audience Overview
		$metrics = "ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession";
		$optParams = array("dimensions" => "ga:date");

		try{
			$ga_aud = $this->ga->get($profileId, $start_date,$end_date,$metrics,$optParams)->results();
		}
		catch(Google_Service_Exception $e)
		{
			// echo 'There was an Analytics API service error '. $e->getCode() . ':' . $e->getMessage();
			
			$this->load->remove_package_path(APPPATH.'third_party/google-analytics');
			return FALSE;
		}
		//Acquisition Overview
		$metrics = "ga:sessions";
		$optParams = array("dimensions" => "ga:date,ga:channelGrouping");
		$ga_acq = $this->ga->get($profileId, $start_date,$end_date,$metrics,$optParams)->results();

		$data['ga'] = array();
		$data['ga']['results'] = array();
		$data['ga']['totalsForAllResults'] = array();
		if($ga_aud->rows)
		{
			foreach($ga_aud->rows as $row)
			{
				$date = $row[0];
				unset($row[0]);
				$data['ga']['results'][$date] = array();
				$data['ga']['results'][$date]['ga:sessions'] = $row[1];
				$data['ga']['results'][$date]['ga:bounceRate'] = $row[2];
				$data['ga']['results'][$date]['ga:avgSessionDuration'] = $row[3];
				$data['ga']['results'][$date]['ga:pageviewsPerSession'] = $row[4];
			}
		}
		foreach($ga_aud->totalsForAllResults as $key=>$row)
		{
			$data['ga']['totalsForAllResults'][$key] = $row;
		}
		if($ga_acq->rows)
		{
			foreach($ga_acq->rows as $i=>$row)
			{
				$date = $row[0];
				$name = $row[1];
				$val = $row[2];
				$data['ga']['results'][$date][$name] = $val;
				if(!isset($data['ga']['totalsForAllResults'][$name]))
					$data['ga']['totalsForAllResults'][$name] = 0;
				$data['ga']['totalsForAllResults'][$name]+= $val;
			}
		}
		$this->load->remove_package_path(APPPATH.'third_party/google-analytics');

		$time_cache = 0;
		if($this->mdate->startOfDay($end_date) >= $this->mdate->startOfDay())
				$time_cache = 1800; //cache 30 phút
			$this->scache->write($data['ga'],$key_cache, $time_cache);
			
			return $data['ga'];
		}
		
	function get_post($post_id)
	{
		$this->post_m->where('post_type',$this->mail_type);
		return $this->post_m->get($post_id);
	}

	function send_all_reports($term_id)
	{

		$time = time();
		// echo $time;
		$posts = $this->post_m->get_posts(array(
			'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
			'tax_query'=> array(
				'taxonomy' => $this->webdoctor_m->term_type,
				'terms' => $term_id,
				),
			'where' => array(
				'end_date <=' => $time
				),
			'post_status' => 'publish',
			'meta_key' => 'webdoctor_mail_report',
			'post_type' => $this->mail_type
			));

		if(!$posts)
			return 'No email';
		foreach($posts as $post)
		{
			// $this->update_time_last_week($post->post_id);
			// continue;

			$new_post_id = $this->copy($post->post_id);

			$result = $this->email_send_report($post->post_id,$post->term_id,$post->post_title,$post->post_content, $post->start_date);
			

			$this->webdoctor_m->update($post->post_id, array(
				'post_title' => $result['title'],
				'post_status' => $result['post_status'],
				'post_content' => $result['message'],
				'post_excerpt' => isset($result['post_excerpt']) ? $result['post_excerpt'] : '',
				'updated_on' => time()
				));
		}
	}

	function copy($post_or_id)
	{
		if(is_object($post_or_id) || is_array($post_or_id))
			$post = $post_or_id;
		else
			$post = $this->post_m->get($post_or_id);
		if(!$post)
			return 'No post';
		$post_id = $post->post_id;
		unset($post->post_id);
		unset($post->post_status);
		unset($post->post_content);
		unset($post->post_excerpt);
		$post->created_on = time();


		$meta = $this->postmeta_m->get_meta($post_id, 'webdoctor_mail_report', TRUE, TRUE);
		$meta = @unserialize($meta);
		if($meta)
		{
			$post->start_date = strtotime(''.@$meta['config']['day_of_week']);
			$post->end_date = strtotime('+7 day',$post->start_date);
		}
		else
		{
			$post->start_date = strtotime('+7 day',$post->start_date);
			$post->end_date = strtotime('+7 day',$post->start_date);
		}
		
		$post->post_title = '[BÁO CÁO {website_name_up}] HIỆN TRẠNG DOANH NGHIỆP TỪ NGÀY {start_date} ĐẾN NGÀY {end_date}';
		$new_post_id = $this->post_m->insert($post);
		$post_terms = $this->term_posts_m->get_the_terms($post_id);
		if($post_terms)
		{
			foreach($post_terms as $pt)
			{
				$this->term_posts_m->insert(array('term_id' => $pt, 'post_id'=> $new_post_id));
			}
		}

		//insert post meta
		$post_metas = $this->postmeta_m->get_many_by('post_id',$post_id);
		if($post_metas)
		{
			foreach($post_metas as $meta)
			{
				$this->postmeta_m->update_meta($new_post_id, $meta->meta_key, $meta->meta_value);
			}
		}
		return $new_post_id;
	}
	function update_time_last_week($post_id)
	{
		$post = $this->webdoctor_m->get($post_id);
		if(!$post)
			return;
		$start_date = strtotime('last Monday');
		$end_date = strtotime('+7 day',$start_date) - 1;
		$this->webdoctor_m->update($post_id, array(
			'post_status' =>'publish',
			'start_date' =>$start_date,
			'end_date' =>$end_date,
			));
		echo 'update '.$post->post_title.' OK<br>';
	}
	function email_send_report($post_id, $term_id, $title, $content, $time = 1)
	{
		$this->load->library('table');
		$term = $this->term_m->get($term_id);
		if(!$term)
			return 'No email';
		$data = array();
		$result = array();
		$post = $this->webdoctor_m->get($post_id);

		$data['start_date'] = $post->start_date;
		$post->end_date = strtotime('+7 day', $post->start_date);
		$data['end_date'] = $this->mdate->endOfDay($post->end_date);

		$data['report_link'] = $this->get_report_link($term_id, $term->term_name, $post_id);
		$message = $this->create_email_report($term_id, $post_id,$data['start_date'], $data['end_date']);
		$meta = $this->postmeta_m->get_meta($post_id,'webdoctor_mail_report', TRUE, TRUE);
		$meta = @unserialize($meta);
		$email_to = '';
		$email_cc = '';
		if($meta)
		{
			$mail_report = $meta['mail_report'];
			$mail_config = $meta['config'];
			
			$email_to = $mail_report['email_to'];
			if(isset($mail_report['email_cc']) && $mail_report['email_cc'])
			{
				$email_cc = $mail_report['email_cc'];
			}
		}

		$this->email->from('thonh@webdoctor.vn', 'Webdoctor.vn');
		$this->email->to($email_to);
		if($email_cc) $this->email->cc($email_cc);

		$title = $this->parse_email_template($title, array(
			'start_date' => $data['start_date'],
			'end_date' => $data['end_date'],
			), $term_id, $term);

		$this->email->subject($title);
		$this->email->message($message);
		$send_status = FALSE;
		if($message)
		{
			$send_status = $this->email->send();
		}
		else
		{
			$result['post_excerpt'] = 'Không có bài viết gửi.';
		}
		$post_status = ($send_status) ? 'sended' :'error';

		if(!$send_status && $message)
		{
			$result['post_excerpt'] = $this->email->print_debugger();
		}

		$result['title'] = $title;
		$result['message'] = $message;
		$result['email_to'] = $email_to;
		$result['email_cc'] = $email_cc;
		$result['post_status'] = $post_status;
		$this->email->clear();
		return $result;

	}

	function get_report_link($term_id, $term_name, $post_id)
	{
		$key = $this->termmeta_m->get_meta($term_id, 'report_key', TRUE, TRUE);
		if(!$key)
		{
			$key = $this->webdoctor_m->encrypt('',$term_id);
			$this->termmeta_m->update_meta($term_id,'report_key',$key);
		}

		$q = array(
			'domain' => $key,
			'p' => $post_id
			);
		// return base_url().'report/webdoctor/'.$term_name.'?'.http_build_query($q);
		return 'http://report.webdoctor.vn/webdoctor/'.$term_name.'?'.http_build_query($q);
	}

	function parse_email_template($string, $data = array(), $term_id = 0, $term = FALSE)
	{
		$this->load->library('parser');
		$end_date = strtotime('tomorrow')-1;
		$start_date = strtotime('last Monday', $end_date);
		if(!$term)
			$term = $this->term_m->get($term_id);
		$parse_data = array(
			'day' => date('d',$start_date),
			'month' => date('m',$start_date),
			'year' => date('Y',$start_date),
			'week' => 1,
			'start_date' => date('d/m/Y',$start_date),
			'end_date' => date('d/m/Y',$end_date),
			'customer_name' => $this->termmeta_m->get_meta($term_id, 'customer_name', TRUE, TRUE),
			'company_name' => $this->termmeta_m->get_meta($term_id, 'company_name', TRUE, TRUE),
			'website' => $term->term_name,
			'website_name_up' => strtoupper($term->term_name),
			);
		$parse_data = array_merge($parse_data, $data);
		$parse_data['start_date'] = date('d/m/Y',$parse_data['start_date']);
		$parse_data['end_date'] = date('d/m/Y',$parse_data['end_date']);

		return $this->parser->parse_string($string, $parse_data, TRUE);
	}

	function create_email_report($term_id, $post_id, $start_date, $end_date)
	{
		$data = array();
		$term = $this->term_m->get($term_id);
		$key = $this->termmeta_m->get_meta($term_id, 'report_key', TRUE, TRUE);
		if(!$key)
		{
			return FALSE;
		}
		$data['tasks'] = $this->webdoctor_m->get_posts_from_date($start_date, $end_date,$term_id);
		$data['bias'] = array();
		$data['bias']['start_date'] = $end_date + 1;
		$data['bias']['end_date'] = strtotime('+7 day', $data['bias']['start_date']);
		$data['bias']['tasks'] = $this->webdoctor_m->get_posts_from_date($data['bias']['start_date'], $data['bias']['end_date'],$term_id);

		$q = array(
			'from' => date('Y-m-d',$start_date),
			'to' => date('Y-m-d',$end_date),
			'domain' => $key,
			);

		$data['report_link'] = $this->get_report_link($term_id, $term->term_name, $post_id);
		if(!$data['bias']['tasks'] && !$data['tasks'])
		{
			return FALSE;
		}

		// $data['report_link'] = 'http://localhost:9090/webdoctor.vn/report/'.$term->term_name.'?'.http_build_query($q);
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		return $this->load->view('report/tpl_email', $data, TRUE);
	}


}
