<?php

if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Webdoctor_m extends Post_m {

	public $term_type = 'webdoctor';
	public $post_type = 'webdoctor-task';
	function __construct() {

		parent::__construct();
	}

	function encrypt($string, $term, $type='encode')
	{

		$this->load->library('encrypt');
		if(!$term)
			return FALSE;
		if(is_object($term))
		{
			$term_id = $term->term_id;
		}
		else if(is_array($term))
		{
			$term_id = $term['term_id'];
		}
		else
		{
			$term_id = $term;
		}
		$key = 'webdoctor-'.$term_id;

		if($type == 'encode')
			return $this->encrypt->encode($term_id,$key);

		return $this->encrypt->decode($string,$key);
	}
	function get_posts_from_date($start_date, $end_date, $term_id = 0, $r = array())
	{
		$defaults = array('order'=>array('posts.post_id'=>'desc'));
		$args = wp_parse_args( $r, $defaults );
		$this->post_m->order_by($args['order']);
		$this->post_m->where('post_type', $this->post_type);
		$this->post_m->where('start_date >=',$start_date);
		$this->post_m->where('end_date <=',$end_date);
		if($term_id >0)
		{
			$this->post_m->join('term_posts', 'term_posts.post_id = posts.post_id');
			$this->post_m->where('term_id',$term_id);
		}
		if(isset($args['meta_key']))
		{
			$this->webdoctor_m->join('postmeta','postmeta.post_id = posts.post_id');
			$this->webdoctor_m->where('meta_key',$args['meta_key']);
			if(isset($args['meta_value']))
				$this->webdoctor_m->where('meta_value',$args['meta_value']);
		}
		return $this->post_m->get_many_by();
	}

	
	function get_real_target($term_id = 0,$user_id= 0, $date = '', $meta = '')
	{
		if($user_id >0)
			$this->post_m->where('post_author', $user_id);
		if($date)
		{
			$start_time = strtotime($date);
			$end_time = strtotime('+1 month', $start_time) - 1;
			$this->post_m->where('start_date >=',$start_time);
			$this->post_m->where('end_date <=',$end_time);
		}
		if($meta !='')
		{
			$metas = $meta;
			if(!is_array($metas))
				$metas = array($meta);
			$this->postmeta_m->join('postmeta','postmeta.post_id = posts.post_id');

			$this->postmeta_m->where('meta_key', 'webdoctor_task_type');
			$this->postmeta_m->group_start();

			foreach($metas as $meta)
			{
				if(strpos($meta, '*') !== FALSE)
				{
					$meta = str_replace('*', '', $meta);
					$this->postmeta_m->or_like('meta_value',$meta);
				}
				else
				{
					$this->postmeta_m->or_where('meta_value',$meta);
				}
			}
			$this->postmeta_m->group_end();
		}
		if($term_id >0)
		{
			$this->post_m->join('term_posts', 'term_posts.post_id = posts.post_id');
			$this->post_m->where('term_id',$term_id);
		}
		return $this->post_m->count_by(array(
			'post_type' => $this->post_type,
			));
	}
}
?>