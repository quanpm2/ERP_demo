<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webdoctor_kpi_m extends Base_model {
	public $primary_key = 'utarget_id';
	public $_table = 'webdoctor_kpi';
	public $before_create = array('delete_cache');
	public $after_update = array('delete_cache');
	public $before_delete = array('delete_cache');

	function get_kpi_value($term_id, $date = 0,$user_id = 0,  $type = 'all')
	{
		if($date == 0)
			$date = time();
		$date = date('Y-m-01',$date);
		$cache_key = 'modules/webdoctor/'.$term_id.'-'.$date.'-kpi';
		if(!$data = $this->scache->get($cache_key))
		{
			$this->where('target_date', $date);
			$results = $this->get_many_by('term_id', $term_id);
			$data = array();
			if($results)
			{
				foreach($results as $result)
				{
					if(!isset($data[$result->user_id]))
						$data[$result->user_id] = array();
					$data[$result->user_id]['target_product'] = $result->target_product;
					$data[$result->user_id]['target_post'] = $result->target_post;
				}
			}
			$this->scache->write($data, $cache_key);
		}
		$this->delete_cache($term_id);
		if($user_id > 0)
		{
			return isset($data[$user_id][$type]) ? $data[$user_id][$type] : false;
		}
		return $data;
	}

	function delete_cache($term)
	{
		if(is_array($term))
		{
			if(isset($term['term_id']))
			{
				$key_cache = 'modules/webdoctor/'.$term['term_id'].'-';
				$this->scache->delete_group($key_cache);
			}
		}
		else
		{
			$key_cache = 'modules/webdoctor/'.$term.'-';
			$this->scache->delete_group($key_cache);
		}
		return $term;
	}
}
/* End of file Webdoctor_kpi_m.php */
/* Location: ./application/modules/googleads/models/Webdoctor_kpi_m.php */