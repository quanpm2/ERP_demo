<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adaccount_m extends Term_m {
    
    const CACHE_ALL_KEY = "modules/tiktokads/adaccounts-";

    public $term_type = 'tiktok_adaccount';

    function __construct()
    {
        parent::__construct();
        $this->load->model('ads_segment_m');
        $this->load->model('tiktokads/tiktok_account_m');
        $this->load->model('tiktokads/insight_segment_m');
    }

    public function set_term_type()
	{
		return $this->where('term.term_type',$this->term_type);
	}

    /**
	 * Gets the AdCampaign's data insight
	 * If time input not defined , default is get LAST_7_DAYS
	 *
	 * @param      integer  $term_id     The term identifier
	 * @param      integer  $start_time  The start time
	 * @param      integer  $end_time    The end time
	 *
	 * @return     array    The data.
	 */
	public function get_data($term_id = 0,$start_time = FALSE, $end_time = FALSE)
	{
		$end_time		= $end_time ?: time();
		$end_time       = end_of_day($end_time);
		$start_time 	= $start_time ?: strtotime('-7 days',$end_time);
		$start_time 	= start_of_day($start_time);

		$iSegments = $this->term_posts_m->get_term_posts($term_id, $this->insight_segment_m->post_type, [
			'fields' => 'posts.post_id, posts.start_date',
			'where' => [
				'start_date >='	=> $start_time,
				'start_date <='	=> $end_time,
				'post_name' 	=> 'day'
			]
		]);

		if(empty($iSegments)) return [];

		$data = array_map(function($x){
			
			$metadata 		= get_post_meta_value($x->post_id);
			if(empty($metadata)) return [];

			$x = array_merge( (array) $x, array_column($metadata, 'meta_value', 'meta_key'));
			$x['post_id'] 	= (int) $x['post_id'];
			$x['time'] 		= (int) $x['start_date'];
			$x['start_date']= (int) $x['start_date'];
			$x['spend'] 	= (double) ($x['spend'] ?? 0);
			return $x;
		}, $iSegments);

		return array_values(array_filter($data));
	}
}
/* End of file Tiktok_adaccount_m.php */
/* Location: ./application/modules/tiktokads/models/Tiktok_adaccount_m.php */
