<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiktokads_contract_m extends Base_contract_m {

	function __construct() 
	{
		parent::__construct();

		$models = ['tiktokads/tiktokads_kpi_m'];
		$this->load->model($models);
	}

	public function calc_contract_value($term_id = 0)
	{
		$term = $this->term_m->where('term_type','tiktok-ads')->get($term_id);

		if(empty($term)) return FALSE;

		$total 			 		  	  = 0;

		$contract_budget			  = (int)get_term_meta_value($term->term_id,'contract_budget') ;
		$service_fee        		  = (int)get_term_meta_value($term->term_id,'service_fee');

		$tiktok_cost 	 		  = $contract_budget + $service_fee;
		$tiktok_discount_percent = (int)get_term_meta_value($term->term_id,'tiktok_discount_percent');

		if(!empty($tiktok_discount_percent) && $tiktok_discount_amound = ($tiktok_cost * div($tiktok_discount_percent,100)))
		{
			$tiktok_cost-=$tiktok_discount_amound;
		}

		$total+= $tiktok_cost;
		return $total;
	}

	public function has_permission($term_id = 0, $name = '', $action = '',$kpi_type = '',$user_id = 0,$role_id = null)
	{
		$this->load->model('tiktokads_kpi_m');

		$permission = $name.'.'.$action;
		$permission = trim($permission, '.');
		
		if($this->permission_m->has_permission("{$name}.Manage",$role_id)
			&& $this->permission_m->has_permission($permission,$role_id)) 
			return TRUE;

		if(!$this->permission_m->has_permission($permission,$role_id)) return FALSE;

		$user_id = empty($user_id) ? $this->admin_m->id : $user_id;

		if($this->permission_m->has_permission('tiktok.sale.access',$role_id) 
			&& get_term_meta_value($term_id,'staff_business') == $user_id)
			return TRUE;

		$args = array(
			'term_id' => $term_id,
			'user_id' => $user_id);

		if(!empty($kpi_type))
		{
			$args['kpi_type'] = $kpi_type;
		}

		$kpis = $this->webgeneral_kpi_m->get_by($args);
		if(empty($kpis)) return FALSE;

		return TRUE;
	}
	
	/**
	 * Start proc service
	 *
	 * @param      Term_m    $term        The term
	 * @param      string|int  $start_time  The start time
	 *
	 * @return     bool    return TRUE If success , othersise return FALSE
	 */
	public function proc_service($term = FALSE, $start_time = '')
	{
		if(!$term || $term->term_type != 'tiktok-ads') return FALSE;

		$start_time = convert_time($start_time);
        update_term_meta($term->term_id, 'start_service_time', $start_time);
        update_term_meta($term->term_id, 'adaccount_status' , 'APPROVED');

        $this->load->model('tiktokads/tiktokads_report_m');
		$is_sent = $this->tiktokads_report_m->send_activation_mail2customer($term->term_id);
		if( ! $is_sent) return FALSE;

		$this->log_m->insert(array(
			'log_title'		  => 'Kích hoạt thời gian chạy quảng cáo Tiktok Ads',
			'log_status'	  => 1,
			'term_id'		  => $term->term_id,
			'log_type' 		  =>'start_service_time',
			'user_id' 		  => $this->admin_m->id,
			'log_content' 	  => 'Đã gửi mail kích hoạt thời gian chạy quảng cáo Tiktok Ads',
			'log_time_create' => date('Y-m-d H:i:s'),
		 ));

		return TRUE;
	}


	/**
	 * Stops a service.
	 *
	 * @param      <type>  $term      The term
	 * @param      string  $end_time  The end time
	 *
	 * @return     bool    return TRUE If success , othersise return FALSE
	 */
	public function stop_service($term, $end_time = '')
	{
		$end_time = convert_time($end_time);
		$this->contract_m->update($term->term_id, ['term_status'=>'liquidation']);

		update_term_meta($term->term_id,'advertise_end_time',$end_time);
		update_term_meta($term->term_id,'end_service_time',time());
		$this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Thanh lý".');

		$this->log_m->insert(array(
			'log_type' =>'end_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => date('Y/m/d H:i:s')
			));

        $this->load->model('tiktokads/tiktokads_report_m');
		$result = $this->tiktokads_report_m->send_finished_service($term->term_id);

        // Push queue sync service fee
        $this->load->config('amqps');
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        
        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];

        $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $channel     = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $payload = [
            'event' 	=> 'contract_payment.sync.service_fee',
            'contract_id' => $term->term_id
        ];
        $message = new \PhpAmqpLib\Message\AMQPMessage(
            json_encode($payload),
            array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $channel->basic_publish($message, '', $queue);	

        $channel->close();
        $connection->close();

		return $result;
	}
}

/* End of file tiktok_report_m.php */
/* Location: ./application/modules/tiktokads/models/tiktok_report_m.php */