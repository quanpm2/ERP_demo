<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/tiktokads/models/Tiktokads_behaviour_m.php');

class Tiktokads_account_rent_behaviour_m extends Tiktokads_behaviour_m {

	/**
	 * Render Pritable contract
	 *
	 * @return     Array   Data result for Preview Action
	 */
	public function prepare_preview()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$this->data['view_file'] = 'tiktokads/contract/preview/account_rent';

		return $this->data;
	}
}
/* End of file Tiktokads_account_rent_behaviour_m.php */
/* Location: ./application/modules/tiktokads/models/Tiktokads_account_rent_behaviour_m.php */