<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insight_segment_m extends Post_m
{
	/**
     * This model's post_type default field value identifier.
     */
    public $post_type = 'insight_segment';

    public $after_update = array('clear_cache');
    public $before_delete = array('delete_relations');

    /**
     * Sets the post type.
     *
     * @return     this
     */
    public function set_post_type()
	{
		return $this->where('post_type',$this->post_type);
	}

    protected function clear_cache($args)
    {
        if( ! isset($args[2]) && is_numeric($args[2])) return $args;

        $post_id = (int) $args[2];

        $this->load->model('tiktokads/adaccount_m');

        $adaccounts = $this->term_posts_m->get_post_terms($post_id, $this->adaccount_m->term_type, [ 'fields' => 'term.term_id, term_type' ]);
        $adaccounts AND array_walk($adaccounts, function($x){   
            $this->term_posts_m->delete_posts_cached($x->term_id);
        });

        $this->scache->delete_group("post/{$post_id}_terms_");

        return $args;
    }

    protected function delete_relations(int $id)
    {
        $this->load->model('term_posts_m');
        $this->load->model('tiktokads/adaccount_m');
        $adAccounts = $this->term_posts_m->get_post_terms($id, $this->adaccount_m->term_type);
        $this->term_posts_m->delete_post_terms($id, array_column((array) $adAccounts, 'term_id'));
        return $id;
    }

    public function contracts(int $id)
    {
        $iSegment = $this->select('posts.post_id,start_date')->get($id);
        if(empty($iSegment)) return false;

        $adAccounts = $this->term_posts_m->get_post_terms($iSegment->post_id, $this->adaccount_m->term_type);
        if(empty($adAccounts)) return false;

        $segments = $this->ads_segment_m
        ->select('posts.post_id')
        ->set_post_type()
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->where_in('term_posts.term_id', array_unique(array_map('intval', array_filter(array_column($adAccounts, 'term_id')))))
        ->where('start_date <=', $iSegment->start_date)
        ->where('if(end_date IS NULL OR end_date = 0, UNIX_TIMESTAMP(), end_date) >=', $iSegment->start_date)
        ->get_all();

        if(empty($segments)) return false;

        $contracts = $this->tiktokads_m
        ->set_term_type()
        ->select('term.term_id,term_name,term_status,term_type')
        ->join('term_posts', 'term_posts.term_id = term.term_id')
        ->where_in('term_posts.post_id', array_map('intval', array_filter(array_column($segments, 'post_id'))))
        ->get_all();
        
        if(empty($contracts)) return false;

        return $contracts;
    }
}
/* End of file Adset_m.php */
/* Location: ./application/modules/tiktokads/models/Adset_m.php */