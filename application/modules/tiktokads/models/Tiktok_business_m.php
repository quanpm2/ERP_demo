<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Term_m.php');

class Tiktok_business_m extends Term_m 
{
    public $term_type = 'tiktok_business';

    /**
     * Sets the term type.
     *
     * @return     this
     */ 
    public function set_term_type()
    {
        return $this->where('term.term_type', $this->term_type);
    }
}
/* End of file Tiktok_business_m.php */
/* Location: ./application/modules/contract/models/Tiktok_business_m.php */