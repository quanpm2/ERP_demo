<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH. 'models/User_m.php');

class Tiktok_account_m extends User_m {

	/**
	 * User type
	 *
	 * @var        string
	 */
	public $user_type = 'tiktok_account';

	/**
	 * Callback Before create trigged
	 *
	 * @var        array
	 */
    public $before_create = array( 'create_timestamp');

    /**
     * Creates a timestamp.
     *
     * @param      <type>  $row    The row
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function create_timestamp($row)
    {
        $row['user_time_create'] = time();
        return $row;
    }

    /**
     * Sets the type.
     */
    public function set_user_type()
    {
    	return $this->where('user.user_type', $this->user_type);
    }
}
/* End of file Tiktok_account_m.php */
/* Location: ./application/modules/tiktokads/models/Tiktok_account_m.php */