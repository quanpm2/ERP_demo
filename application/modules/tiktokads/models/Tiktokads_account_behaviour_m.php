<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/tiktokads/models/Tiktokads_behaviour_m.php');

class Tiktokads_account_behaviour_m extends Tiktokads_behaviour_m {
	
	/**
	 * Render Pritable contract
	 *
	 * @return     Array  Data result for Preview Action
	 */
	public function prepare_preview()
	{
		parent::is_exist_contract(); // Determines if exist contract.
		dd('tiktokads_account_behaviour_m');
		$data = $this->data;

		$data['data_service'] = array(
			'service_name' 	=> 'Quản lý tài khoản',
			'budget'		=> (int) get_term_meta_value($this->contract->term_id, 'contract_budget'),
			'currency' 		=> get_term_meta_value($this->contract->term_id, 'currency'),
			'service_fee' 	=> (int) get_term_meta_value($this->contract->term_id, 'service_fee')
		);
		
		$data['view_file'] 	= 'tiktokads/contract/preview/account';
		return $data;
	}
}
/* End of file Tiktokads_account_behaviour_m.php */
/* Location: ./application/modules/tiktokads/models/Tiktokads_account_behaviour_m.php */