<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use AdsService\Tiktokads\TiktokService;
use AdsService\Tiktokads\Enums\AdvertiserStatusEnum;
use AdsService\Tiktokads\Enums\AdvertiserRolesEnum;

class AdsAccount extends MREST_Controller
{
    private $request_id = '';

    function __construct()
    {
        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'tiktokads/tiktokads_m';
        $this->autoload['models'][] = 'tiktokads/tiktok_account_m';
        $this->autoload['models'][] = 'tiktokads/adaccount_m';

        $this->autoload['libraries'][]  = 'form_validation';
        $this->autoload['helpers'][]    = 'date';

        parent::__construct();
    }

    public function index_get()
	{
        $args           = wp_parse_args(parent::get(NULL, TRUE), []);
        $args_encypt    = md5(json_encode($args));

        $key_cache      = Adaccount_m::CACHE_ALL_KEY . $args_encypt;
        $adaccounts   = $this->scache->get($key_cache);
        if( ! $adaccounts)
        {
            $adaccounts = $this->adaccount_m
            ->set_term_type()
            ->select('term_id, term_name, term_slug, term_parent')
            ->order_by('term_id', 'DESC')
            ->limit(500);

            if(!empty($args['account_id'])){
                $adaccounts->where("(term_slug LIKE '%{$args['account_id']}%' OR term_name LIKE '%{$args['account_id']}%')");
            }

            $adaccounts = $adaccounts->get_all();
            if( ! $adaccounts)
            {
                return parent::responseHandler([], 'Empty account', 'success', 204);
            }

            $this->scache->write($adaccounts, $key_cache, 300);
        }
        
        $adaccounts = array_map(function($x){
            $x->term_id = (int) $x->term_id;
            $x->term_slug = $x->term_slug;
            $x->term_name = $x->term_name;
            $x->isExternal = get_term_meta_value($x->term_id, 'isExternal') ?: FALSE;
            return $x;
        }, $adaccounts);

        parent::responseHandler($adaccounts, 'OK');
	}

    /**
	 * Create new Adaccount & Business Account
	 */
	public function index_post()
	{
		$this->load->library('form_validation');

		$post = parent::post(null, TRUE);

		$this->form_validation->set_data($post);
        $this->form_validation->set_rules('term_name', 'adaccount name', 'required');
        $this->form_validation->set_rules('term_slug', 'adaccount id', 'required|numeric');

        if(FALSE == $this->form_validation->run())
    	{
    		parent::response([
    			'code' => 400,
    			'error' => $this->form_validation->error_array()
    		]);
    	}

    	$this->load->model('tiktokads/adaccount_m');

    	$isExists = $this->adaccount_m->select('term_id')->set_term_type()->where('term_slug', $post['term_slug'])->count_by() > 0;
    	if($isExists)
    	{
    		parent::response([
				'code' => 400,
				'error' => ["Tài khoản đã tồn tại. không thể thêm mới"]
			]);
    	}

    	$insert_data = array(
            'term_slug' => $post['term_slug'],
            'term_name' => $post['term_name'],
            'term_status' => AdvertiserStatusEnum::value('STATUS_ENABLE'),
            'term_type' => $this->adaccount_m->term_type
        );

    	$insert_id 					= $this->adaccount_m->insert($insert_data);
    	$insert_data['term_id'] 	= (int) $insert_id;
    	$insert_data['isExternal'] 	= true;

		$meta = array(
            'create_time' => time(),
            'currency' => 'VND',
            'owner' => $this->admin_m->id,
            'isExternal' => 1,
			'source' => 'direct'
        );

        foreach ($meta as $meta_key => $meta_value)
        {
            update_term_meta($insert_id, $meta_key, $meta_value);
        }


		parent::response([
			'code' => 200,
			'message' => "Thêm mới thành công",
			'data' => $insert_data
		]);
	}

    /**
     * { function_description }
     *
     * @param      int   $contractId  The contract identifier
     */
    public function redirect_uri_get()
    {
        $this->request_id = md5(__CLASS__ . __METHOD__ . time());

        log_message('info', "[{$this->request_id}] Catch redirect uri");

        $default = [
            'auth_code' => null,
            'state' => null,
        ];
        $args = wp_parse_args($this->input->get(), $default);

        $redirect_url = admin_url('tiktokads/' . $args['state']);
        empty($args['state']) && $redirect_url = admin_url('tiktokads');

        if(empty($args['auth_code']))
        {
            log_message('error', "[{$this->request_id}] auth_code is not exist");

            $this->messages->error('Kết nối không thành công !!!');
            redirect($redirect_url,'refresh');
        }

        log_message('debug', "[{$this->request_id}] Fetching access_token");

        $access_token = $this->fetchAccessToken($args['auth_code']);
        if(FALSE === $access_token)
        {
            log_message('error', "[{$this->request_id}] access_token is not exist");

            $this->messages->error('Không lấy được token truy cập !!!');
            redirect($redirect_url,'refresh');
        }
        
        log_message('debug', "[{$this->request_id}] Fetching tiktok_account_id");
        $tiktok_account_id = $this->get_set_tiktok_account($access_token);
        if(empty($tiktok_account_id))
        {
            log_message('error', "[{$this->request_id}] tiktok_account_id is not exist");

            $this->messages->error('Không lấy được thông tin id của tài khoản !!!');
            redirect($redirect_url,'refresh');
        }

        log_message('debug', "[{$this->request_id}] Sync tiktok ad account");
        $result = $this->sync_ad_account_by_id($tiktok_account_id);
        if(!$result) 
        {
            log_message('error', "[{$this->request_id}] Sync failed");

            $this->messages->error('Truy xuất dữ liệu không thành công !!!');
        }
        else $this->messages->success('Danh sách đã được làm nạp mới.');

        log_message('info', "[{$this->request_id}] Connect tiktok ad account successful");

        redirect($redirect_url,'refresh');
    }

    /**
     * { function_description }
     *
     * @param      int   $contractId  The contract identifier
     */
    public function authentization_uri_get($contractId = 0, $from = 'setting')
    {
        if(FALSE == $this->tiktokads_m->set_contract($contractId)) parent::response(['code' => 400, 'error' => 'Truy vấn không hợp lệ.']);

        $redirect_uri = base_url('api-v2/tiktokads/AdsAccount/redirect_uri');
        $tiktokService = new TiktokService();
        $state = $from . '/' . $contractId;
        $redirect_url = $tiktokService->buildAuthorizationUri($redirect_uri, $state);
        parent::response(['code' => 200, 'data' => $redirect_url]);
    }
    
    /**
     * { function_description }
     *
     * @param      int   $contractId  The contract identifier
     */
    protected function fetchAccessToken($auth_code){
        $tiktokService = new TiktokService();
        $access_token = $tiktokService->fetchAccessToken($auth_code);

        return $access_token;
    }

    /**
     * { function_description }
     *
     * @param      int   $contractId  The contract identifier
     */
    protected function get_set_tiktok_account($access_token){
        if(empty($access_token)) return FALSE;

        $tiktokService = new TiktokService();
        $user_info = $tiktokService->getUserInfo($access_token);
        if(empty($user_info)) return FALSE;

        $tiktok_account_id = NULL;
        $tiktok_account = $this->tiktok_account_m->set_user_type()->get_by(['user_login' => $user_info['core_user_id']]);
        if(empty($tiktok_account)){
            $insert_data = [
                'user_email' => $user_info['email'],
                'user_login' => $user_info['core_user_id'],
                'user_type' => $this->tiktok_account_m->user_type,
                'user_time_create' => $user_info['create_time']
            ];
		    $tiktok_account_id = $this->tiktok_account_m->insert($insert_data);
        }
        else {
            $tiktok_account_id = $tiktok_account->user_id;
        }

        update_user_meta($tiktok_account_id, 'access_token', $access_token);
        
        return $tiktok_account_id;
    }

    protected function sync_ad_account_by_id($tiktok_account_id){
        if(empty($tiktok_account_id)) 
        {
            log_message('error', "[{$this->request_id}] Empty tiktok_account_id");

            return FALSE;
        }

        $tiktok_account = $this->tiktok_account_m->set_user_type()->get($tiktok_account_id);

		if( ! $tiktok_account) 
        {
            log_message('error', "[{$this->request_id}] Empty tiktok_account");

            return FALSE;
        }

        $access_token = get_user_meta_value($tiktok_account->user_id, 'access_token');

        $tiktokService = new TiktokService();

        $cache_key = "modules/tiktokads/{$tiktok_account->user_id}-tiktok_account";
		$adAccounts = $this->scache->get($cache_key);
        if(empty($adAccounts)){
            $adAccounts = [];

            $advertisers = $tiktokService->listAdvertiser($access_token);
            if(empty($advertisers)) 
            {
                log_message('error', "[{$this->request_id}] Empty advertisers");

                return FALSE;
            }

            $advertiser_ids = array_column($advertisers, 'advertiser_id');
            $advertiser_ids = array_map(function($item) { return '' . $item;}, $advertiser_ids);
            
            // Split ids each 5 element
            // Because Method get list advertiser detail is GET
            $advertiser_ids = array_chunk($advertiser_ids, 5);
            while(!empty($advertiser_ids)){
                $advertiser_ids_tmp = array_pop($advertiser_ids);
    
                $advertiser_details = $tiktokService->listAdvertiserDetail($access_token, $advertiser_ids_tmp);
                if(!empty($advertiser_details)) {
                    $advertiser_details = array_map(function($item){ return (array) $item; }, $advertiser_details);
                    $adAccounts = array_merge($adAccounts, $advertiser_details);
                };
            }
    
            $adAccounts AND $this->scache->write($adAccounts, $cache_key, 5*60); // ttl 5 minutes
        }
        
        if(empty($adAccounts)) 
        {
            log_message('error', "[{$this->request_id}] Empty adAccounts");

            return FALSE;
        }

        $adAccountsInDB = $this->adaccount_m
		    ->select('term_id, term_type, term_slug, term_name, term_status')
		    ->set_term_type()
		    ->where_in('term_slug', array_unique(array_column($adAccounts, 'advertiser_id')))
		    ->as_array()
		    ->get_all();
            
        $adAccountsInDB AND $adAccountsInDB = array_column($adAccountsInDB, null , 'term_slug');
        
        $this->load->model('tiktokads/tiktok_business_m');
        $unSpecificBC = $this->tiktok_business_m
        ->set_term_type()
        ->select('term_id')
        ->where_in('term_slug', 0)
        ->get_by();
        if(empty($unSpecificBC))
        {
            log_message('debug', "[{$this->request_id}] Create Unspecific BC. ");

        	$_insertBusiness = array(
                'term_type'        => $this->tiktok_business_m->term_type,
                'term_slug'        => 0,
                'term_name'        => AdvertiserStatusEnum::value('STATUS_UNSPECIFIED'),
                'term_description' => 'Created by default'
            );

    		$_insertBusinessId = $this->tiktok_business_m->insert($_insertBusiness);
    		$_insertBusiness['term_id'] = (int) $_insertBusinessId;
    		$unSpecificBC = (object) $_insertBusiness;
        }

        $is_insert_new_account = FALSE;
        foreach ($adAccounts as $adAccount)
		{
            log_message('debug', "[{$this->request_id}] Store tiktok ad account. " . json_encode($adAccount));

            $metadata = [
                'owner' => $this->admin_m->id,
                'create_time' => $adAccount['create_time'],
                'role' => AdvertiserRolesEnum::value($adAccount['role']),
                'currency' => $adAccount['currency'],
                'timezone' => $adAccount['timezone'],
                'balance' => $adAccount['balance'],
                'source' => 'linked'
            ];

			if(empty($adAccountsInDB[$adAccount['advertiser_id']]))
			{
                log_message('debug', "[{$this->request_id}] Link tiktok ad account to Unspecific BC. ");

                $data = [
					'term_type'        => $this->adaccount_m->term_type,
					'term_slug'        => $adAccount['advertiser_id'],
					'term_name'        => $adAccount['name'],
					'term_parent'      => $unSpecificBC->term_id,
					'term_status'      => AdvertiserStatusEnum::value($adAccount['status'])
				];
				$insert_id = $this->adaccount_m->insert($data);

                foreach($metadata as $key => $value){
                    update_term_meta($insert_id, $key, $value);
                }
                
				$this->term_users_m->set_relations_by_term($insert_id, [ $tiktok_account_id ], $this->tiktok_account_m->user_type);

                $is_insert_new_account = TRUE;

    			continue;
			}

			$_adAccountInDb = $adAccountsInDB[$adAccount['advertiser_id']];
            log_message('debug', "[{$this->request_id}] Link tiktok ad account to BC. " . json_encode($_adAccountInDb));

			if(
                $_adAccountInDb['term_slug'] != $adAccount['advertiser_id'] 
                || $_adAccountInDb['term_name'] != $adAccount['name'] 
                || $_adAccountInDb['term_status'] != AdvertiserStatusEnum::value($adAccount['status'])
            ){
				$this->adaccount_m->update($_adAccountInDb['term_id'], [
					'term_slug' => $adAccount['advertiser_id'],
					'term_name' => $adAccount['name'],
					'term_status' => AdvertiserStatusEnum::value($adAccount['status']),
                ]);
			}
			$this->term_users_m->set_relations_by_term($_adAccountInDb['term_id'], [ $tiktok_account_id ], $this->tiktok_account_m->user_type);
            foreach($metadata as $key => $value){
                update_term_meta($_adAccountInDb['term_id'], $key, $value);
            }
		}

        if($is_insert_new_account) $this->scache->delete_group(Adaccount_m::CACHE_ALL_KEY);

        return TRUE;
    }
}
/* End of file Adaccount.php */
/* Location: ./application/modules/tiktokads/controllers/api_v2/Adaccount.php */
