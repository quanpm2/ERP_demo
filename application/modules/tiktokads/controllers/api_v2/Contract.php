<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use AdsService\AdsInsight\TiktokadsInsight;

/**
 * This class describes a contract.
 */
class Contract extends MREST_Controller
{
	function __construct()
	{
        $this->autoload['models'][] 	= 'tiktokads/tiktokads_m';
        $this->autoload['models'][] 	= 'tiktokads/adaccount_m';
        $this->autoload['models'][] 	= 'tiktokads/tiktokads_kpi_m';
        $this->autoload['models'][] 	= 'ads_segment_m';
        $this->autoload['helpers'][] 	= 'date';

		parent::__construct();
	}

    /**
	 * @param int $id Id hợp đồng FB
	 * 
	 * @return [type]
	 */
	public function overview_get(int $id)
	{
		$data = $this->data;

		if( false == $this->tiktokads_m->set_contract($id)) return parent::responseHandler([], 'Hợp đồng không tồn tại hoặc đã bị xóa.', 'error', 404);
		if( ! $this->tiktokads_m->can('tiktokads.overview.access')) return parent::responseHandler([], 'Không có quyền truy cập.', 'error', 401);

		$contract = $this->tiktokads_m->get_contract();

		$saleId = (int) get_term_meta_value($id, 'staff_business');
		$sale 	= $this->admin_m->get_field_by_id($saleId);

		// $segments = $this->tiktokads_m->getSegments();

		$kpis = $this->tiktokads_kpi_m->order_by('kpi_type')->get_many_by([ 'term_id' => $id]);
		$kpis AND $kpis = array_map(function($kpi) {
			$kpi->display_name = $this->admin_m->get_field_by_id($kpi->user_id, 'display_name');
			return $kpi;
		}, $kpis);

		$data = array(
			'term_id'				=> $id,
			'term'					=> $contract,
			'kpis'					=> $kpis,
			'sale'					=> $sale,
			'contract_code'			=> get_term_meta_value($id, 'contract_code'),
			'representative_name'	=> get_term_meta_value($id, 'representative_name'),
			'representative_email'		=> get_term_meta_value($id, 'representative_email'),
			'representative_address'	=> get_term_meta_value($id, 'representative_address'),
            'exchange_rate_usd_to_vnd'	=> (int)get_term_meta_value($id, 'exchange_rate_usd_to_vnd'),
			'exchange_rate_aud_to_vnd'	=> (int)get_term_meta_value($id, 'exchange_rate_aud_to_vnd'),
		);

		$start_service_time 	= (int) get_term_meta_value($id,'start_service_time');
		$end_service_time 		= (int) get_term_meta_value($id,'end_service_time');
		$advertise_start_time	= (int) get_term_meta_value($id,'advertise_start_time');
		$advertise_end_time		= (int) get_term_meta_value($id,'advertise_end_time');
		$vat 					= div((double) get_term_meta_value($id,'vat'),100);
		$contract_budget 		= (double) get_term_meta_value($id,'contract_budget');
		$service_fee 			= (double) get_term_meta_value($id,'service_fee');
		$payment_amount 		= (double) get_term_meta_value($id,'payment_amount');
        $result_updated_on      = get_term_meta_value($id, 'result_updated_on');
		$time_next_api_check    = get_term_meta_value($id, 'time_next_api_check');

		if(!empty($vat)) $service_fee+= $service_fee*$vat;
		
		$actual_budget 				= (int) get_term_meta_value($id, 'actual_budget');
		$balanceBudgetReceived 		= (int) get_term_meta_value($id, 'balanceBudgetReceived');
        $balanceBudgetAddTo         = (int) get_term_meta_value($id, 'balanceBudgetAddTo');

        $balance_spend 				= (double) get_term_meta_value($id, 'balance_spend');

		$payment_percentage 		= 100 * (double) get_term_meta_value($id,'payment_percentage');

		$data['actual_budget']				= $actual_budget;
		$data['total_actual_budget']		= $actual_budget;
		$data['balanceBudgetReceived']		= $balanceBudgetReceived;
        $data['balanceBudgetAddTo']		    = $balanceBudgetAddTo;
		$data['balance_spend']		        = $balance_spend;
		$data['contract_budget']			= $contract_budget;
		$data['payment_amount']				= $payment_amount;
		$data['payment_percentage']			= $payment_percentage;
		$data['start_service_time']			= $start_service_time;
		$data['end_service_time']			= $end_service_time;
		$data['advertise_start_time']		= $advertise_start_time;
		$data['advertise_end_time']			= $advertise_end_time;
        $data['result_updated_on']          = $result_updated_on ? date('H:i:s d/m/Y', $result_updated_on) : '--';
        $data['time_next_api_check']        = $time_next_api_check ? date('H:i:s d/m/Y', $time_next_api_check) : '--';

		$amount_spend = 0;
		$start_service_time = start_of_day($start_service_time);
		$end_service_time 	= end_of_day(($end_service_time ?: time()));

		$insight 		= array();
		$adsSegments 	= $this->term_posts_m->get_term_posts($id, $this->ads_segment_m->post_type);
		if( ! empty($adsSegments))
		{
			foreach ($adsSegments as &$adsSegment)
			{
				$adaccount = $this->term_posts_m->get_post_terms($adsSegment->post_id, $this->adaccount_m->term_type);
				if(empty($adaccount)) continue;
				
				$adaccount 	= reset($adaccount);
				$_iSegments = $this->adaccount_m->get_data($adaccount->term_id, $adsSegment->start_date, end_of_day( (int) $adsSegment->end_date));
				$insight 	= array_merge($_iSegments, $insight);
				
				$adsSegment->adaccount 		= $adaccount;
				$adsSegment->adaccount_id 	= $adaccount->term_id;
				$adsSegment->insights 		= $_iSegments;
			}
		}

		if($insight)
		{
			$insight = array_group_by($insight, 'start_date');
			$insight = array_map(function($x){
				$_instance = reset($x);
				return [
					'result'		=> array_sum(array_column($x, 'result')),
					'reach'			=> array_sum(array_column($x, 'reach')),
					'spend'			=> array_sum(array_column($x, 'spend')),
                    'impressions'   => array_sum(array_column($x, 'impressions')),
					'date_start' 	=> my_date($_instance['start_date'], 'Y-m-d')
				];
			}, $insight);
		}

		ksort($insight, SORT_NUMERIC);

		$data['chart_data'] = [
			'axis_categories' 	=> array_column($insight, 'date_start'),
			'result' 			=> array_column($insight, 'result'),
			'reach' 			=> array_column($insight, 'reach'),
			'spend' 			=> array_column($insight, 'spend'),
            'impressions'       => array_column($insight, 'impressions'),
			'cost_per_result' 	=> div(array_sum(array_column($insight, 'spend')), array_sum(array_column($insight, 'result')))
		];


		$data['reach']			= array_sum(array_column($insight, 'reach'));
		$data['result'] 		= array_sum(array_column($insight, 'result'));
		$data['impressions']	= array_sum(array_column($insight, 'impressions'));

		$actual_result                      = (double) get_term_meta_value($id, 'actual_result');
		$data['amount_spend']               = $actual_result + $data['balance_spend'];
		$data['amount_spend_percentage']	= div($data['amount_spend'], $actual_budget) * 100;
		

		$data['adAccountsInfomation'] = array();
		if( ! empty($adsSegments))
		{
			$rows = [];
			foreach ($adsSegments as $i => $segment)
			{
                if(empty($segment->insights)) continue;
                
				$spend = round(array_sum(array_column($segment->insights, 'spend')));

				$_instance = reset($segment->insights);
				
				$row = [
					'id' => $_instance['account_id'] ?? null,
					'name' => $_instance['account_name'] ?? null,
					'start' => my_date($segment->start_date, 'd/m/Y'),
					'end' => empty($segment->end_date) ? 'Hiện tại' : my_date($segment->end_date, 'd/m/Y'),
					'cost' => $spend
				];

				$data['adAccountsInfomation'][] = $row;
			}
		}

		parent::responseHandler($data, 'ok');
	}
    
    /**
     * adaccount_insight_put
     *
     * @param  mixed $term_id
     * @return void
     */
    public function adaccount_insight_put(int $term_id = 0)
	{
		$contract 	= (new Tiktokads_m())->set_contract($term_id);
		if( ! $contract)
		{
            return parent::responseHandler([], 'Không tìm thấy dữ liệu của hợp đồng', 'success', 400);
		}

        $status = (new TiktokadsInsight())->syncInsight($term_id);
        if(!$status){
            return parent::responseHandler([], 'Tiến trình đồng bộ gặp sự cố. Vui lòng liên hệ quản trị viên để được hỗ trợ', 'failed', 400);
        }

		return parent::responseHandler([], 'Dữ liệu đã được đồng bộ thành công. Vui lòng chờ trong giây lát.', 'success', 200);
	}

	/**
	 * { function_description }
	 *
	 * @param      int    $term_id  The term identifier
	 *
	 * @return     array  ( description_of_the_return_value )
	 */
	public function adaccount_insight_put_old($term_id = 0)
	{
		$result = ['success'=>'error', 'msg'=>'Xử lý không thành công','data'=>''];
		
		$contract 		= (new tiktokads_m())->set_contract($term_id);
		$behaviour_m 	= $contract->get_behaviour_m();

		try
		{
			$behaviour_m->get_the_progress();
			$behaviour_m->sync_all_amount();
		}
		catch (Exception $e)
		{
			$result['msg'] 		= $e->getMessage();
			return $result;
		}

		$result['success'] 	= 'success';
		$result['msg'] 		= 'Dữ liệu đã được đồng bộ thành công. Vui lòng (F5) để xem sự thay đổi';
		return parent::response($result);
	}


	/**
	 * ĐỒNG BỘ TOÀN BỘ SỐ LIỆU THU CHI CỦA HỢP ĐỒNG DỰA TRÊN IDS HỢP ĐỐNG ĐƯỢC YÊU CẦU
	 *
	 * @return     JSON
	 */
	public function sync_spend_put()
	{
		$ids 	= parent::put('ids', TRUE);
		if(empty($ids)) return parent::response([ 'code' => REST_Controller::HTTP_BAD_REQUEST, 'message' => 'HTTP_BAD_REQUEST']);

		is_array($ids) OR $ids = [$ids];
		$ids = array_unique(array_filter(array_map('intval', $ids)));

		$contracts = $this->tiktokads_m->set_term_type()->where_in('term_id', $ids)->get_all();

		if(empty($contracts)) return parent::response([ 'code' => REST_Controller::HTTP_NO_CONTENT, 'message' => 'HTTP_NO_CONTENT']);

		$result = [ 'total' => count($contracts), 'completed' => 0, 'failed' => 0 ];

		foreach ($contracts as $contract)
		{
            try
			{
                $contract = (new contract_m())->set_contract($contract);
				$behaviour_m = $contract->get_behaviour_m();
    			$behaviour_m->get_the_progress();
				$behaviour_m->sync_all_progress();
				$result['completed']++;
			}
			catch (Exception $e)
			{
				$result['failed']++;
				continue;
			}
		}

		return parent::response([ 'code' => REST_Controller::HTTP_OK, 'data' => $result]);
	}
}
/* End of file Contract.php */
/* Location: ./application/modules/ti/tiktokads/api_v2/Contract.php */