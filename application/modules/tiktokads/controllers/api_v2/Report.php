<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report extends MREST_Controller
{

    public function __construct()
    {
        $this->autoload['models'][] = 'customer/customer_m';
        $this->autoload['models'][] = 'tiktok_m/tiktok_m_m';
        $this->autoload['models'][] = 'tiktok_m/adaccount_m';
        
        parent::__construct();

        $this->load->config('contract/contract');
    }

    public function quarter_get()
    {
        $start_time = start_of_day(parent::get('start_time', TRUE));
        $end_time   = start_of_day(parent::get('end_time', TRUE));

        $m_args = array(
            ['key' => 'result_updated_on', 'value' => $start_time, 'compare' => '>='],
            ['key' => 'adaccount_id', 'value' => "", 'compare' => '!='],
            ['key' => 'contract_begin', 'value' => "", 'compare' => '!='],
            ['key' => 'contract_end', 'value' => "", 'compare' => '!='],
            ['key' => 'advertise_start_time', 'value' => "", 'compare' => '!=']
        );

        $payment_type_enums = [
            'normal' => 'Full VAT',
            'customer' => 'Khách hàng tự thanh toán',
            'behalf' => 'ADSPLUS thu & chi hộ'
        ];

        $cache_key  = 'modules/tiktok_m/report-quarter-'.md5(json_encode([$start_time, $end_time]));
        $terms      = $this->scache->get($cache_key);

        if(empty($terms))
        {
            $terms = $this->tiktok_m_m->select('term.*')->m_find($m_args)->set_term_type()->as_array()->get_all() AND 
            $terms = array_map(function($x) use($end_time){ 
                $x['advertise_end_time'] = get_term_meta_value($x['term_id'], 'advertise_end_time') ?: $end_time;
                return $x;
            }, $terms);
        }

        if(empty($terms)) throw new Exception('Không tìm thấy bất kỳ hợp đồng nào');
        $this->scache->write($terms, $cache_key, 60);

        $customers      = [];
        $contract_ids   = array_unique(array_column($terms, 'term_id'));
        if($contract_ids)
        {
            $customers  = $this->customer_m
            ->select('user.user_id, display_name, term_users.term_id')
            ->set_user_type()
            ->join('term_users','term_users.user_id = user.user_id')
            ->where_in('term_users.term_id', $contract_ids)
            ->as_array()
            ->group_by('term_users.term_id')
            ->get_all() ?: [];

            $customers = array_column($customers, NULL, 'term_id');
        }

        // $this->load->model('tiktok_m/adword_calculation_m');
        // $this->load->model('tiktok_m/base_adwords_m');

        $data               = array();
        $adAccounts         = $this->adaccount_m->set_term_type()->where_in('term_id', array_unique(array_column($terms, 'adaccount_id')))->get_all();

        if(empty($adAccounts)) die('Data is Empty');

        $adAccountContracts = array_group_by($terms, 'adaccount_id');

        foreach ($adAccounts as &$adAccount)
        {
            $_contracts = $adAccountContracts[$adAccount->term_id];

            $adAccount->contracts       = $_contracts;
            $adAccount->contract_type   = count($_contracts) > 1 ? 'renewal' : 'new';

            foreach ($_contracts as $_contract)
            {
                $is_renewal = ! ((bool) get_term_meta_value($_contract['term_id'], 'is_first_contract'));
                $interval   = (new DateTime())->setTimestamp($_contract['advertise_start_time'])->diff((new DateTime())->setTimestamp($_contract['advertise_end_time']));

                /* Contract Durations month */
                $month_durations = number_format($interval->m + ($interval->d/30), 1); 

                $_startTime = max([$_contract['advertise_start_time'], $start_time]);
                $_endTime   = $_contract['advertise_end_time'];

                $insight    = $this->adaccount_m->get_data($adAccount->term_id, $_startTime, $_endTime);
                $cost       = (int)  array_sum(array_column($insight, 'spend'));

                $_instanceInsight       = reset($insight);
                $account_currency_code  = $_instanceInsight['account_currency'];
                $meta_dollar_exchange_rate_key = 'dollar_exchange_rate_' . strtolower($account_currency_code) . '_to_vnd';
                $dollar_exchange_rate   = (float) (get_term_meta_value($_contract['term_id'], $meta_dollar_exchange_rate_key) ?: get_exchange_rate($account_currency_code));

                $budget         = (new tiktok_m_m())->set_contract((object) $_contract)->get_behaviour_m()->calc_budget();
                $service_fee    = (int) get_term_meta_value($_contract['term_id'], 'service_fee');
                $service_rate   = div($service_fee, $budget);

                $payment_type = 'normal';
                if('customer' == get_term_meta_value($_contract['term_id'], 'contract_budget_payment_type'))
                {
                    $payment_type = 'customer';
                    'behalf' == get_term_meta_value($_contract['term_id'], 'contract_budget_customer_payment_type') AND $payment_type = 'behalf';
                }
                
                $data[] = array(
                    $adAccount->term_slug,    
                    $customers[$_contract['term_id']]['display_name'] ?? '---',
                    ($is_renewal ? 'renewal' : 'new'),
                    $payment_type_enums[$payment_type] ?? '',
                    $month_durations,
                    my_date($_contract['contract_begin']),
                    my_date($_contract['advertise_start_time']),
                    my_date($_contract['contract_end']),
                    my_date($_contract['advertise_end_time']),
                    $cost,
                    $account_currency_code,
                    $dollar_exchange_rate,
                    $cost*$service_rate,
                    currency_numberformat($service_rate*100, '%', 2),
                    $account_currency_code == 'VND' ? div($cost, $dollar_exchange_rate) : $cost,
                    ($account_currency_code == 'VND' ? div($cost, $dollar_exchange_rate) : $cost)*$service_rate,
                );
            }
        }

        $this->load->library('table');
        $this->table->set_heading(['AdAccount ID', 'Customer Name', 'Contract type', 'Payment Type', 'Total contract durations (months)', 'Contract Start date (contract)', 'Contract Start date (kick of campaign)', 'Contract End date (contract)', 'Contract End date (Off Campaign)', 'Media spend this Quarter','currency', 'Exchange', 'Management fee on Media Spend this quarter', 'Management fee %','Media spend this Quarter (USD)', 'Management fee on Media Spend this quarter (USD)']);
        echo $this->table->generate($data);
        die();
    }

    /**
     * tiktok_m Ads Monthly Spend
     *
     * @throws     Exception  (description)
     */
    public function monthly_get()
    {
        $month = (int) (parent::get('m', TRUE) ?: date('m'));
        if($month <= 0 && $month > 12) die('tháng không hợp lệ');

        $start_time = start_of_month(date("Y-{$month}-d"));
        $end_time   = end_of_month(date("Y-{$month}-d"));


        $this->load->model('tiktok_m/tiktok_m_m');

        $m_args = array(
            ['key' => 'adaccount_id', 'value' => "", 'compare' => '!='],
            ['key' => 'contract_begin', 'value' => "", 'compare' => '!='],
            ['key' => 'contract_end', 'value' => "", 'compare' => '!='],
        );

        if(empty(parent::get('m', TRUE)) || $month == (int) date('m'))
        {
            $m_args[] = ['key' => 'result_updated_on', 'value' => [ $start_time, $end_time ], 'compare' => 'BETWEEN'];
            $m_args[] = ['key' => 'advertise_start_time', 'value' => "", 'compare' => '!='];
        }
        else
        {
            $m_args[] = ['key' => 'advertise_start_time', 'value' => $end_time, 'compare' => '<='];

            $_field = 'advertise_end_time';
            $_alias = uniqid('meta_');

            $this->tiktok_m_m
            ->select("{$_alias}.meta_value as '{$_field}'")
            ->join("termmeta {$_alias}", "{$_alias}.term_id = term.term_id AND {$_alias}.meta_key = '{$_field}'", 'left')
            ->group_start()
                ->or_group_start() 
                    ->where("{$_alias}.meta_value >=", $start_time)
                    // ->where("{$_alias}.meta_value <=", $end_time)
                ->group_end()
                ->or_where("{$_alias}.meta_value is null")
                ->or_where("{$_alias}.meta_value", 0)
            ->group_end();
        }

        $cache_key  = 'modules/tiktok_m/report-monthy-'.md5(json_encode([$start_time, $end_time]));
        $terms      = $this->scache->get($cache_key);
        if(empty($terms))
        {
            $terms = $this->tiktok_m_m->select('term.term_id, term.term_status, term.term_type')->m_find($m_args)->set_term_type()->as_array()->get_all() AND 
            $terms = array_map(function($x) use($end_time){ 
                $x['advertise_end_time'] = get_term_meta_value($x['term_id'], 'advertise_end_time') ?: $end_time;
                return $x;
            }, $terms);
        }

        $this->db->reset_query();

        if(empty($terms)) throw new Exception('Không tìm thấy bất kỳ hợp đồng  nào');
        $this->scache->write($terms, $cache_key, 60);

        /* LOAD ALL STAFFS OF ADS CONTRACTS */
        $this->load->model('googleads/googleads_kpi_m');
        $kpis = $this->googleads_kpi_m->select('term_id, user_id')->where_in('term_id', array_column($terms, 'term_id'))->setTechnicalKpiTypes()->group_by('term_id')->as_array()->get_all();

        $users = array();
        if($kpis)
        {
            $userIds = array_unique(array_column($kpis, 'user_id'));
            foreach ($userIds as $userId)
            {
                $users[$userId] = $this->admin_m->get_field_by_id($userId);
            } 
            $kpis AND $kpis = array_group_by($kpis, 'term_id');
        }
        /* LOAD ALL STAFFS OF ADS CONTRACTS - END */

        $customers      = [];
        $contract_ids   = array_unique(array_column($terms, 'term_id'));
        if($contract_ids)
        {
            $customers  = $this->customer_m
            ->select('user.user_id, display_name, term_users.term_id')
            ->set_user_type()
            ->join('term_users','term_users.user_id = user.user_id')
            ->where_in('term_users.term_id', $contract_ids)
            ->as_array()
            ->group_by('term_users.term_id')
            ->get_all() ?: [];

            $customers = array_column($customers, NULL, 'term_id');
        }

        $payment_type_enums = [
            'normal' => 'Full VAT',
            'customer' => 'Khách hàng tự thanh toán',
            'behalf' => 'ADSPLUS thu & chi hộ'
        ];

        $data               = array();
        $adAccounts         = $this->adaccount_m->set_term_type()->where_in('term_id', array_unique(array_column($terms, 'adaccount_id')))->get_all();

        if(empty($adAccounts)) die('Data is Empty');

        $adAccountContracts = array_group_by($terms, 'adaccount_id');

        foreach ($adAccounts as &$adAccount)
        {
            $_contracts = $adAccountContracts[$adAccount->term_id];

            $adAccount->contracts       = $_contracts;
            $adAccount->contract_type   = count($_contracts) > 1 ? 'renewal' : 'new';

            foreach ($_contracts as $_contract)
            {
                $is_renewal = ! ((bool) get_term_meta_value($_contract['term_id'], 'is_first_contract'));
                $interval   = (new DateTime())->setTimestamp($_contract['advertise_start_time'])->diff((new DateTime())->setTimestamp($_contract['advertise_end_time']));

                /* Contract Durations month */
                $month_durations = number_format($interval->m + ($interval->d/30), 1); 

                $_startTime = max([$_contract['advertise_start_time'], $start_time]);
                $_endTime   = min($_contract['advertise_end_time'], end_of_month(date("Y-{$month}-d")));

                $insight    = $this->adaccount_m->get_data($adAccount->term_id, $_startTime, $_endTime);
                $cost       = (int)  array_sum(array_column($insight, 'spend'));

                $_contract_daterange    = array();
                $_contract_begin        = get_term_meta_value($_contract['term_id'], 'contract_begin') AND $_contract_daterange[] = $_contract_begin;
                $_contract_end          = get_term_meta_value($_contract['term_id'], 'contract_end') AND $_contract_daterange[] = $_contract_end;
                $_contract_daterange    = implode(' - ', array_map(function($x){ return my_date($x, 'd/m/Y'); }, $_contract_daterange));

                /* PARSE TECHNICAL STAFFS */
                $techStaffs = "";
                empty($kpis[$_contract['term_id']]) OR $techStaffs = implode(', ', array_filter(array_map(function($x) use ($users){
                    if(empty($users[$x['user_id']])) return '';
                    return $users[$x['user_id']]['display_name'] ?: $users[$x['user_id']]['user_email'];
                }, $kpis[$_contract['term_id']])));
                /* PARSE TECHNICAL STAFFS - END */

                $_instanceInsight       = reset($insight);

                $account_currency_code  = $_instanceInsight['account_currency'];
                $meta_dollar_exchange_rate_key = 'dollar_exchange_rate_' . strtolower($account_currency_code) . '_to_vnd';
                $dollar_exchange_rate   = (float) (get_term_meta_value($_contract['term_id'], $meta_dollar_exchange_rate_key) ?: get_exchange_rate());

                $budget         = (new tiktok_m_m())->set_contract((object) $_contract)->get_behaviour_m()->calc_budget();
                $service_fee    = (int) get_term_meta_value($_contract['term_id'], 'service_fee');
                $service_rate   = div($service_fee, $budget);

                $saleId     = (int) get_term_meta_value($_contract['term_id'], 'staff_business');
                $sale       = $this->admin_m->get_field_by_id($saleId);
                $department = $this->term_users_m->get_user_terms($saleId, 'department');
                $department AND $department = reset($department);

                $payment_type = 'normal';
                if('customer' == get_term_meta_value($_contract['term_id'], 'contract_budget_payment_type'))
                {
                    $payment_type = 'customer';
                    'behalf' == get_term_meta_value($_contract['term_id'], 'contract_budget_customer_payment_type') AND $payment_type = 'behalf';
                }


                $actual_result  = (int) get_term_meta_value($_contract['term_id'],'actual_result');
                $actual_budget  = (int) get_term_meta_value($_contract['term_id'], 'actual_budget');
                $remain_budget  = $actual_budget - $actual_result;

                $item = array(
                    'contract_code'     => get_term_meta_value($_contract['term_id'], 'contract_code'),
                    'customer'          => $customers[$_contract['term_id']]['display_name'] ?? '',
                    'Service'           => 'ADSPLUS',
                    'payment_type'      => $payment_type_enums[$payment_type] ?? '',
                    'adaccount_id'         => $adAccount->term_slug,
                    /*Kỹ thuật*/
                    'tech'                  => $techStaffs,
                    /*T/G HĐ*/
                    'contract_daterange'    => $_contract_daterange,
                    'adaccount_name'         => $adAccount->term_name,
                    'contract_value'    => (int) get_term_meta_value($_contract['term_id'], 'contract_value'),
                    /*Ng.Kích hoạt*/
                    'advertise_start_time'  => $_contract['advertise_start_time'] ? my_date($_contract['advertise_start_time'], 'd/m/Y') : '',
                    /*Ng.Kết thúc*/
                    'advertise_end_time'    => $_contract['advertise_end_time'] ? my_date($_contract['advertise_end_time'], 'd/m/Y') : '',
                    /*Trạng thái*/
                    'term_status'           => $this->config->item($_contract['term_status'], 'contract_status'),
                    'remain_budget'         => currency_numberformat($remain_budget),
                    'department'        => $department->term_name ?? '',
                    'sale_name'         => $sale['display_name'],
                    'sale_email'        => $sale['user_email'],
                    'currency'          => $account_currency_code,
                    'exchange'          => number_format($dollar_exchange_rate, 0, ',', ''),
                    'cost'              => (int) number_format($cost, 2, ',', ''),
                    'service_fee'       => (int) number_format($cost*$service_rate, 2, ',', ''),
                    'service_fee_rate'  => currency_numberformat($service_rate*100, '%'),
                );

                $data[] = $item;
            }
        }

        $title          = 'Export dữ liệu Hợp Đồng & Khách hàng đang thực hiện trong tháng ' . date('m', $end_time);
        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));
        
        $sheet = $spreadsheet->getActiveSheet();

        $columns = array(
            'contract_code'     => [ 'type' => 'string', 'label' => 'Mã hợp đồng' ],
            'customer'          => [ 'type' => 'string', 'label' => 'Khách hàng' ],
            'Service'           => [ 'type' => 'string', 'label' => 'Dịch vụ' ],
            'payment_type'      => [ 'type' => 'string', 'label' => 'Loại' ],
            'adaccount_id'      => [ 'type' => 'string', 'label' => 'AdAccount ID' ],
            'adaccount_name'    => [ 'type' => 'string', 'label' => 'AdAccount Name' ],
            'tech'              => [ 'type' => 'string', 'label' => 'Kỹ thuật' ],
            'contract_daterange' => [ 'type' => 'string', 'label' => 'T/G HĐ' ],
            'contract_value'    => [ 'type' => 'number', 'label' => 'Giá tị hợp đồng' ],
            'advertise_start_time'  => [ 'type' => 'string', 'label' => 'Ngày bắt đầu' ],
            'advertise_end_time'    => [ 'type' => 'string', 'label' => 'Ngày kết thúc' ],
            'term_status'       => [ 'type' => 'string', 'label' => 'Trạng thái' ],
            'department'        => [ 'type' => 'string', 'label' => 'Phòng' ],
            'sale_name'         => [ 'type' => 'string', 'label' => 'Tên nhân viên' ],
            'sale_email'        => [ 'type' => 'string', 'label' => 'Email nhân viên' ],
            'currency'          => [ 'type' => 'string', 'label' => 'Currecy' ],
            'remain_budget'     => [ 'type' => 'number', 'label' => 'Ngân sách còn lại' ],
            'exchange'          => [ 'type' => 'number', 'label' => '1 USD = ? VND' ],
            'cost'              => [ 'type' => 'number', 'label' => 'Ngân sách chạy trong tháng' ],
            'service_fee'       => [ 'type' => 'number', 'label' => 'Phí quản lý' ],
            'service_fee_rate'  => [ 'type' => 'number', 'label' => '% Phí quản lý' ],
        );

        $sheet->fromArray(array_column(array_values($columns), 'label'), NULL, 'A1');

        $rowIndex = 2;

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column)
            {
                switch ($column['type'])
                {
                    case 'number': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }    

                $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item[$key]);
                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }


    /**
     * tiktok_m Ads Monthly Spend
     *
     * @throws     Exception  (description)
     */
    public function date_range_get($start = '', $end = '')
    {
        $start_date = DateTime::createFromFormat('Y-m-d', $start);
        $end_date   = DateTime::createFromFormat('Y-m-d', $end);

        if(empty($start_date) || empty($start_date))
            die('Tham số ngày bắt đầu & ngày kết thúc chưa được truyền vào đầy đủ.');

        $start_time = start_of_day($start_date->getTimestamp());
        $end_time = end_of_day($end_date->getTimestamp());

        $this->load->model('tiktok_m/tiktok_m_m');

        $m_args = array(
            ['key' => 'adaccount_id', 'value' => "", 'compare' => '!='],
            ['key' => 'contract_begin', 'value' => "", 'compare' => '!='],
            ['key' => 'contract_end', 'value' => "", 'compare' => '!='],
            ['key' => 'advertise_start_time', 'value' => $end_time, 'compare' => '<=']
        );

        $_field = 'advertise_end_time';
        $_alias = uniqid('meta_');

        $this->tiktok_m_m
        ->select("{$_alias}.meta_value as '{$_field}'")
        ->join("termmeta {$_alias}", "{$_alias}.term_id = term.term_id AND {$_alias}.meta_key = '{$_field}'", 'left')
        ->group_start()
            ->or_group_start() 
                ->where("{$_alias}.meta_value >=", $start_time)
                // ->where("{$_alias}.meta_value <=", $end_time)
            ->group_end()
            ->or_where("{$_alias}.meta_value is null")
            ->or_where("{$_alias}.meta_value", 0)
        ->group_end();

        $cache_key  = 'modules/tiktok_m/report-monthy-'.md5(json_encode([$start_time, $end_time]));
        $terms      = $this->scache->get($cache_key);
        if(empty($terms) || 1==1)
        {
            $terms = $this->tiktok_m_m->select('term.*')->m_find($m_args)->set_term_type()->as_array()->get_all() AND 
            $terms = array_map(function($x) use($end_time){ 
                $x['advertise_end_time'] = get_term_meta_value($x['term_id'], 'advertise_end_time') ?: $end_time;
                return $x;
            }, $terms);
        }

        $this->db->reset_query();

        if(empty($terms)) throw new Exception('Không tìm thấy bất kỳ hợp đồng  nào');
        $this->scache->write($terms, $cache_key, 60);

        $customers      = [];
        $contract_ids   = array_unique(array_column($terms, 'term_id'));
        if($contract_ids)
        {
            $customers  = $this->customer_m
            ->select('user.user_id, display_name, term_users.term_id')
            ->set_user_type()
            ->join('term_users','term_users.user_id = user.user_id')
            ->where_in('term_users.term_id', $contract_ids)
            ->as_array()
            ->group_by('term_users.term_id')
            ->get_all() ?: [];

            $customers = array_column($customers, NULL, 'term_id');
        }

        $payment_type_enums = [
            'normal' => 'Full VAT',
            'customer' => 'Khách hàng tự thanh toán',
            'behalf' => 'ADSPLUS thu & chi hộ'
        ];

        $data               = array();
        $adAccounts         = $this->adaccount_m->set_term_type()->where_in('term_id', array_unique(array_column($terms, 'adaccount_id')))->get_all();

        if(empty($adAccounts)) die('Data is Empty');

        $adAccountContracts = array_group_by($terms, 'adaccount_id');

        foreach ($adAccounts as &$adAccount)
        {
            $_contracts = $adAccountContracts[$adAccount->term_id];

            $adAccount->contracts       = $_contracts;
            $adAccount->contract_type   = count($_contracts) > 1 ? 'renewal' : 'new';

            foreach ($_contracts as $_contract)
            {
                $is_renewal = ! ((bool) get_term_meta_value($_contract['term_id'], 'is_first_contract'));
                $interval   = (new DateTime())->setTimestamp($_contract['advertise_start_time'])->diff((new DateTime())->setTimestamp($_contract['advertise_end_time']));

                /* Contract Durations month */
                $month_durations = number_format($interval->m + ($interval->d/30), 1); 

                $_startTime = max([$_contract['advertise_start_time'], $start_time]);
                $_endTime   = min($_contract['advertise_end_time'], $end_time);

                $insight    = $this->adaccount_m->get_data($adAccount->term_id, $_startTime, $_endTime);
                $cost       = (int)  array_sum(array_column($insight, 'spend'));

                $_instanceInsight       = reset($insight);

                $account_currency_code  = $_instanceInsight['account_currency'];
                $meta_dollar_exchange_rate_key = 'dollar_exchange_rate_' . strtolower($account_currency_code) . '_to_vnd';
                $dollar_exchange_rate   = (float) (get_term_meta_value($_contract['term_id'], $meta_dollar_exchange_rate_key) ?: get_exchange_rate($account_currency_code));

                $budget         = (new tiktok_m_m())->set_contract((object) $_contract)->get_behaviour_m()->calc_budget();
                $service_fee    = (int) get_term_meta_value($_contract['term_id'], 'service_fee');
                $service_rate   = div($service_fee, $budget);

                $saleId     = (int) get_term_meta_value($_contract['term_id'], 'staff_business');
                $sale       = $this->admin_m->get_field_by_id($saleId);
                $department = $this->term_users_m->get_user_terms($saleId, 'department');
                $department AND $department = reset($department);

                $payment_type = 'normal';
                if('customer' == get_term_meta_value($_contract['term_id'], 'contract_budget_payment_type'))
                {
                    $payment_type = 'customer';
                    'behalf' == get_term_meta_value($_contract['term_id'], 'contract_budget_customer_payment_type') AND $payment_type = 'behalf';
                }

                $item = array(
                    'contract_code'     => get_term_meta_value($_contract['term_id'], 'contract_code'),
                    'customer'          => $customers[$_contract['term_id']]['display_name'] ?? '',
                    'Service'           => 'ADSPLUS',
                    'payment_type'      => $payment_type_enums[$payment_type] ?? '',
                    'adaccount_id'      => $adAccount->term_slug,
                    'adaccount_name'    => $adAccount->term_name,
                    'contract_value'    => (int) get_term_meta_value($_contract['term_id'], 'contract_value'),
                    'department'        => $department->term_name ?? '',
                    'sale_name'         => $sale['display_name'],
                    'sale_email'        => $sale['user_email'],
                    'currency'          => $account_currency_code,
                    'exchange'          => number_format($dollar_exchange_rate, 0, ',', ''),
                    'cost'              => (int) number_format($cost, 2, ',', ''),
                    'service_fee'       => (int) number_format($cost*$service_rate, 2, ',', ''),
                    'service_fee_rate'  => currency_numberformat($service_rate*100, '%'),
                );

                $data[] = $item;
            }
        }

        $title          = 'Export dữ liệu Hợp Đồng & Khách hàng thực hiện từ ngày ' . $start_date->format('d/m/Y') . ' đến ngày ' .$end_date->format('d/m/Y');

        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));
        
        $sheet = $spreadsheet->getActiveSheet();

        $columns = array(
            'contract_code'     => [ 'type' => 'string', 'label' => 'Mã hợp đồng' ],
            'customer'          => [ 'type' => 'string', 'label' => 'Khách hàng' ],
            'Service'           => [ 'type' => 'string', 'label' => 'Dịch vụ' ],
            'payment_type'      => [ 'type' => 'string', 'label' => 'Loại' ],
            'adaccount_id'      => [ 'type' => 'string', 'label' => 'AdAccount ID' ],
            'adaccount_name'    => [ 'type' => 'string', 'label' => 'AdAccount Name' ],
            'contract_value'    => [ 'type' => 'number', 'label' => 'Giá tị hợp đồng' ],
            'department'        => [ 'type' => 'string', 'label' => 'Phòng' ],
            'sale_name'         => [ 'type' => 'string', 'label' => 'Tên nhân viên' ],
            'sale_email'        => [ 'type' => 'string', 'label' => 'Email nhân viên' ],
            'currency'          => [ 'type' => 'string', 'label' => 'Currecy' ],
            'exchange'          => [ 'type' => 'number', 'label' => '1 USD = ? VND' ],
            'cost'              => [ 'type' => 'number', 'label' => 'Ngân sách đã xử dụng' ],
            'service_fee'       => [ 'type' => 'number', 'label' => 'Phí quản lý' ],
            'service_fee_rate'  => [ 'type' => 'number', 'label' => '% Phí quản lý' ],
        );

        $sheet->fromArray(array_column(array_values($columns), 'label'), NULL, 'A1');

        $rowIndex = 2;

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column)
            {
                switch ($column['type'])
                {
                    case 'number': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'timestamp': 
                        $value = Date::PHPToExcel($value);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }    

                $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item[$key]);
                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report/';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $this->load->helper('text');
        $title = url_title(convert_accented_characters($title));
        $fileName = "{$folder_upload}-{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file Report.php */
/* Location: ./application/modules/tiktok_m/controllers/api_v2/Report.php */