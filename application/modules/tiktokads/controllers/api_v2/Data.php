<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'tiktokads/tiktokads_m';
        $this->autoload['models'][] = 'tiktokads/adaccount_m';
        parent::__construct($config);
    }

    public function accounts_get($contractId = null)
    {
        $contract   = (new tiktokads_m())->set_contract($contractId);
        $insight    = [];

        $advertise_start_time   =  get_term_meta_value($contractId,'advertise_start_time') ?: time();
        $advertise_end_time     = get_term_meta_value($contractId,'advertise_end_time')  ?: time();

        $adaccount_id   = (int) get_term_meta_value($contractId, 'adaccount_id') AND $insight = $this->adaccount_m->get_data($adaccount_id, $advertise_start_time, $advertise_end_time);

        if($insight)
        {
            $insight = array_column(array_map(function($x){ 

                $x['result']    = (int) ($x['result'] ?? 0);
                $x['reach']     = (int) ($x['reach']  ?? 0);
                $x['spend']     = (int) ($x['spend']  ?? 0);
                return $x;
            }, $insight), NULL, 'date_start');
        }

        ksort($insight);

        parent::response([ 'status' => true, 'data' => $insight ]);
    }

    public function account_spend_get($contractId = null)
    {
        $contract   = (new tiktokads_m())->set_contract($contractId);
        if(empty($contract)) parent::response(['status' => true, 'data' => 0]);
        
        $accounts   = $contract->get_accounts(parent::get(NULL, TRUE));
        if(empty($accounts)) parent::response(['status' => true, 'data' => 0]);

        $spend = array_sum(array_column($accounts, 'spend'));
        parent::response([ 'status' => true, 'data' => $spend ]);
    }

    public function adaccount_spend_get($adaccount_id)
    {
        $args = wp_parse_args( array_filter(parent::get(NULL, TRUE)), [
            'end_time' => time(),
            'ignore_cache' => false,
        ]);


        $_iSegments = $this->adaccount_m->get_data($adaccount_id, $args['start_time'], $args['end_time']);
        if(empty($_iSegments)) parent::response(['code' => 200, 'data' => 0]);

        $sum_spend = array_sum(array_column($_iSegments, 'spend'));

        parent::response([
            'code' => 200,
            'data' => $sum_spend
        ]);
    }

    /**
     * GET All Contracts Realted by Segments
     *
     * @param      int   $id     The identifier
     */
    public function contracts_related_get(int $id)
    {
        if(FALSE == $this->tiktokads_m->set_contract($id)) parent::response(['code' =>  parent::HTTP_BAD_REQUEST, 'error' => parent::HTTP_BAD_REQUEST ]);

        $contract = $this->tiktokads_m->get_contract();

        $segments       = $this->tiktokads_m->getSegments();
        $adaccountIds   = array_column($segments, 'adaccount_id');

        if(empty($adaccountIds))
        {
            parent::response(['code' => parent::HTTP_OK, 'total' => 0, 'data' => []]);    
        }

        $allSegments = $this->ads_segment_m->set_post_type()
        ->select('posts.post_id, start_date, end_date, term_posts.term_id as adaccount_id')
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->where_in('term_posts.term_id', array_unique($adaccountIds))
        ->group_by('posts.post_id')
        ->get_all();

        if(empty($allSegments)) parent::response(['code' => parent::HTTP_OK, 'total' => 0, 'data' => []]);


        $relatedContracts = $this->tiktokads_m->set_term_type()
        ->select('term.term_id, term_name, term_type, term_parent, term_status, object_id, post_id as segment_id')
        ->join($this->term_posts_m->_table, "{$this->term_posts_m->_table}.term_id = term.term_id")
        ->where_in("{$this->term_posts_m->_table}.post_id", array_map('intval', array_unique(array_filter(array_column($allSegments, 'post_id')))))
        ->where('term.term_id <>', $id)
        ->get_all();

        if(empty($relatedContracts)) parent::response(['code' => parent::HTTP_OK, 'total' => 0, 'data' => []]);

        $relatedContracts AND $relatedContracts = array_group_by($relatedContracts, 'term_id');
        $allSegments AND $allSegments = array_column($allSegments, null, 'post_id');

        $contracts = array_map(function($items) use($allSegments){

            $_contract = reset($items);

            $this->load->model('staffs/admin_m');
            $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
            if(!empty($manipulation_locked['manipulation_locked_by']))
            {
                $manipulation_locked['manipulation_locked_by_name'] = $this->admin_m->get_field_by_id($manipulation_locked['manipulation_locked_by'], 'display_name');
            }

            if(!empty($manipulation_locked['manipulation_unlocked_by']))
            {
                $manipulation_locked['manipulation_unlocked_by_name'] = $this->admin_m->get_field_by_id($manipulation_locked['manipulation_unlocked_by'], 'display_name');
            }
            $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                                    && TRUE == (bool)get_term_meta_value($_contract->term_id, 'is_manipulation_locked');
            $manipulation_locked['is_manipulation_locked'] = $is_manipulation_locked;

            return array(
                'term_id' => (int) $_contract->term_id,
                'term_name' => $_contract->term_name,
                'contract_code' => get_term_meta_value($_contract->term_id, 'contract_code') ?: $_contract->term_id,
                'term_type' => $_contract->term_type,
                'term_parent' => (int) $_contract->term_parent,
                'term_status' => $_contract->term_status,
                'advertise_start_time' => (int) get_term_meta_value($_contract->term_id, 'advertise_start_time'),
                'advertise_end_time' => (int) get_term_meta_value($_contract->term_id, 'advertise_end_time'),
                'segments' => array_map(function($item) use($allSegments){
                    return array_map('intval', (array) $allSegments[$item->segment_id]);
                }, $items),
                'manipulation_locked' => $manipulation_locked
            );
        }, $relatedContracts);

        parent::response([
            'code' => parent::HTTP_OK,
            'total' => count($contracts),
            'data' => array_values($contracts)
        ]);
    }

    public function account_cost_get($contractId = null)
    {
        $contract   = (new tiktokads_m())->set_contract($contractId);
        $accounts   = $contract->get_accounts(parent::get(NULL, TRUE));
        
        if(empty($accounts)) parent::response(['status' => true, 'data' => 0]);

        $ins    = reset($accounts);
        $cost   = array_sum(array_column($accounts, 'spend'));

        $exchange_rate = get_exchange_rate($ins['account_currency'], $contractId);
        $exchange_rate AND $cost *= $exchange_rate;

        parent::response([ 'status' => true, 'data' => $cost ]);
    }

    public function adaccount_cost_get($adaccount_id)
    {
        $args = wp_parse_args( array_filter(parent::get(NULL, TRUE)), [
            'end_time' => time(),
            'ignore_cache' => false,
        ]);


        $_iSegments = $this->adaccount_m->get_data($adaccount_id, $args['start_time'], $args['end_time']);
        if(empty($_iSegments)) parent::response(['code' => 200, 'data' => 0]);

        $sum_spend = array_sum(array_column($_iSegments, 'spend'));

        parent::response([
            'code' => 200,
            'data' => $sum_spend
        ]);
    }
}
/* End of file MCCReport.php */
/* Location: ./application/modules/tiktokads/controllers/api_v2/MCCReport.php */