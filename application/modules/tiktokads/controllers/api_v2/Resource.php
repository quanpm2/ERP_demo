<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use AdsService\Tiktokads\Enums\AdvertiserStatusEnum;

class Resource extends MREST_Controller
{
    function __construct()
    {
        $this->autoload['libraries'][]     = 'form_validation';

        $this->autoload['models'][]     = 'tiktokads/tiktokads_m';
        $this->autoload['models'][]     = 'tiktokads/tiktokads_kpi_m';
        $this->autoload['models'][]     = 'option_m';

        parent::__construct();
    }

    /**
     * @return json
     */
    public function kpis_get($contractId)
    {
        if (!has_permission('tiktokads.kpi.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), ['contractId' => $contractId]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->tiktokads_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::response(['code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array()]);
        }

        // Process
        $kpis = $this->tiktokads_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(['term_id' => $inputs['contractId']]);

        $data = [];
        foreach ($kpis as $kpi) {
            $item = [];

            $item['kpi_id'] = $kpi['kpi_id'];
            $item['user_id'] = $kpi['user_id'];
            $item['display_name'] = $this->admin_m->get_field_by_id($kpi['user_id'], 'display_name');
            $item['user_email'] = $this->admin_m->get_field_by_id($kpi['user_id'], 'user_email');

            $data[] = $item;
        }

        return parent::responseHandler($data, 'Lấy dữ liệu thành công.');
    }

    /**
     * @return json
     */
    public function staffs_get()
    {
        if (!has_permission('tiktokads.kpi.access'))
        {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $role_ids = $this->option_m->get_value('group_tt_ads_role_ids', TRUE);
        $staffs = $this->admin_m->select('user_id, display_name, user_email')
            ->where_in('role_id', $role_ids)
            ->set_get_active()
            ->order_by('display_name')
            ->as_array()
            ->get_all();

        if (empty($staffs))
        {
            return parent::responseHandler(null, 'Không có dữ liệu kỹ thuật viên.', 'success', 201);
        }

        return parent::responseHandler($staffs, 'Lấy dữ liệu thành công.');
    }

    /**
     * @return json
     */
    public function assigned_technician_get()
    {
        if (!has_permission('tiktokads.kpi.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $contract_id = parent::get('contract_id');
        if(empty($contract_id))
        {
            return parent::responseHandler(null, 'Hợp đồng không tồn tại.', 'success', 200);
        }

        $role_ids = $this->option_m->get_value('group_tt_ads_role_ids', TRUE);
        $staffs = $this->admin_m->where_in('role_id', $role_ids)
            ->set_get_active()
            ->join('term_users', 'term_users.user_id = user.user_id')
            ->where('term_users.term_id', $contract_id)
            ->select('user.user_id, user.display_name, user.user_email')
            ->order_by('display_name')
            ->as_array()
            ->get_all();
        if (empty($staffs)) {
            return parent::responseHandler(null, 'Không có dữ liệu kỹ thuật viên.', 'success', 200);
        }

        return parent::responseHandler($staffs, 'Lấy dữ liệu thành công.');
    }

    /**
     * @return json
     */
    public function kpi_post($contractId)
    {
        if (!has_permission('tiktokads.kpi.add')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::post(), ['contractId' => $contractId]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->tiktokads_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'staffId',
                'label' => 'staffId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', function ($value) {
                        return $this->admin_m->existed_check($value, 'group_tt_ads_role_ids');
                    })
                ]
            ]
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::response(['code' => parent::HTTP_BAD_REQUEST, 'msg' => $this->form_validation->error_array()]);
        }

        // Process
        $kpi_datetime = time();
        $this->tiktokads_kpi_m->update_kpi_value($inputs['contractId'], 'tech', 1, $kpi_datetime, $inputs['staffId']);
        $this->term_users_m->set_relations_by_term($inputs['contractId'], [$inputs['staffId']], 'admin');
        $this->tiktokads_m->set_contract($contractId)->get_behaviour_m()->setTechnicianId();

        return parent::responseHandler([], 'Thêm kỹ thuật thành công.');
    }

    /**
     * @return json
     */
    public function kpi_delete($contractId, $kpiId)
    {
        if (!has_permission('tiktokads.kpi.delete')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::delete(), [
            'contractId' => $contractId,
            'kpiId' => $kpiId
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->tiktokads_m, 'existed_check'))
                ]
            ],
            [
                'field' => 'kpiId',
                'label' => 'kpiId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->tiktokads_kpi_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        // Remove Kpi
        $this->tiktokads_kpi_m->delete($kpiId);
        
        // Reset relation
        $kpis = $this->tiktokads_kpi_m->where('term_id', $contractId)->select('user_id')->as_array()->get_all();
        $kpis = array_column($kpis, 'user_id');
        $this->term_users_m->set_term_users($inputs['contractId'], $kpis, 'admin', ['command' => 'replace']);
        $this->tiktokads_kpi_m->delete_cache($kpiId);

        return parent::responseHandler([], 'Xoá kỹ thuật thành công.');
    }

    /**
     *
     * @param      int    $term_id  The term identifier
     *
     * @return     Json  Result
     */
    public function export_disconent_account_get()
    {
        $title = "hop_dong_tt_co_tai_khoan_unlink";

        $data = $this->tiktokads_m
            ->select("term.term_id AS contract_id")
            ->select("MAX(IF(m_contract.meta_key = 'contract_code', m_contract.meta_value, NULL)) as contract_code")
            ->select("adaccount.term_id as adaccount_db_id, adaccount.term_slug as adaccount_id, adaccount.term_name as adaccount_name")
            ->select("audits.created_at AS unlink_date")
            
            ->join('term_posts', 'term_posts.term_id = term.term_id')
            ->join('posts AS ads_segment', "ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = 'ads_segment'")
            ->join('term_posts AS tp_adaccount', "tp_adaccount.post_id = ads_segment.post_id")
            ->join('term AS adaccount', "adaccount.term_id = tp_adaccount.term_id AND adaccount.term_type = 'tiktok_adaccount'")
            ->join('termmeta AS m_contract', "m_contract.term_id = term.term_id AND m_contract.meta_key = 'contract_code'", 'LEFT')
            ->join('audits', "audits.auditable_id = term.term_id and audits.auditable_type = 'term' and audits.auditable_field = 'term_status' and audits.event = 'updated'", 'LEFT')

            ->where('term.term_type', 'tiktok-ads')
            ->where('adaccount.term_status !=', AdvertiserStatusEnum::STATUS_ENABLE)

            ->group_by('term.term_id')

            ->as_array()
            ->get_all();
        if(empty($data)){
            return parent::responseHandler([], 'Không có dữ liệu');
        }

        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title} "));
        $sheet = $spreadsheet->getActiveSheet();

        $columns = $this->getColumns('export');
        $sheet->fromArray(array_column(array_values($columns), 'label'), NULL, 'A1');

        $rowIndex = 2;

        foreach ($data as $item) {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column) {
                switch ($column['type']) {
                    case 'number':
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;

                    case 'decimal':
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("0.00");
                        break;

                    case 'timestamp':
                        $item[$key] = Date::PHPToExcel($item[$key]);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default:
                        break;
                }

                @$sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item[$key]);
                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/tmp/';
        if (!is_dir($folder_upload)) {
            try {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}{$title}.xlsx";

        try {
            (new Xlsx($spreadsheet))->save($fileName);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        
        return parent::responseHandler([], 'File đang được tải về');
    }

    protected function getColumns()
    {
        $config = array(
            'contract_id' => [
                'type' => 'string',
                'label' => 'Id hợp đồng'
            ],
            'contract_code' => [
                'type' => 'string',
                'label' => 'Mã hợp đồng'
            ],
            'adaccount_db_id' => [
                'type' => 'string',
                'label' => 'Id tài khoản hệ thống'
            ],
            'adaccount_id' => [
                'type' => 'string',
                'label' => 'Id tài khoản tiktok'
            ],
            'adaccount_name' => [
                'type' => 'string',
                'label' => 'Tên tài khoản'
            ],
            'unlink_date' => [
                'type' => 'string',
                'label' => 'Ngày ngắt kết nối'
            ],
        );

        return $config;
    }

    /**
     * contract_chains_get
     *
     * @param  int|string $contract_id
     * @return JSON
     */
    public function contract_chains_get($contract_id)
    {
        // Get Chain contract id
        $contract_chain = [];

        $prev_chain = [];
        $prev_contract_id = get_term_meta_value($contract_id, 'previousContractId');
        $archor_id = $contract_id;
        while(!empty($prev_contract_id))
        {
            if(in_array($prev_contract_id, $prev_chain))
            {
                return parent::responseHandler([], 'Chuỗi hợp đồng nối thành vòng tròn. Vui lòng liên hệ bộ phận Công nghệ để được hỗ trợ!!!', 'error', 400);
            }

            $next_contract_id_of_prev_contract_id = get_term_meta_value($prev_contract_id, 'nextContractId');
            if($next_contract_id_of_prev_contract_id != $archor_id)
            {
                return parent::responseHandler([], 'Chuỗi hợp đồng bị đứt đoạn. Vui lòng liên hệ bộ phận Công nghệ để được hỗ trợ!!!', 'error', 400);
            }

            array_unshift($prev_chain , $prev_contract_id);
            $archor_id = $prev_contract_id;
            $prev_contract_id = get_term_meta_value($prev_contract_id, 'previousContractId');
        }
        $contract_chain = array_merge($contract_chain, $prev_chain);
        $contract_chain[] = $contract_id;

        $next_chain = [];
        $next_contract_id = get_term_meta_value($contract_id, 'nextContractId');
        $archor_id = $contract_id;
        while(!empty($next_contract_id))
        {
            if(in_array($next_contract_id, $next_chain))
            {
                return parent::responseHandler([], 'Chuỗi hợp đồng nối thành vòng tròn. Vui lòng liên hệ bộ phận Công nghệ để được hỗ trợ!!!', 'error', 400);
            }

            $prev_contract_id_of_next_contract_id = get_term_meta_value($next_contract_id, 'previousContractId');
            if($prev_contract_id_of_next_contract_id != $archor_id)
            {
                return parent::responseHandler([], 'Chuỗi hợp đồng bị đứt đoạn. Vui lòng liên hệ bộ phận Công nghệ để được hỗ trợ!!!', 'error', 400);
            }

            $next_chain[] = $next_contract_id;
            $archor_id = $next_contract_id;
            $next_contract_id = get_term_meta_value($next_contract_id, 'nextContractId');
        }
        $contract_chain = array_merge($contract_chain, $next_chain);

        // Get Chain contract data
        $contract_chain_str = implode(',', $contract_chain);
        $query = "
            SELECT
                term.term_id,
                SUM(m_insight.meta_value) as spend,
                0 AS join_spend,
                0 AS manual_spend,
                false AS join_direction,
                adaccounts.term_id AS adaccount_id,
                ads_segment.post_id AS ads_segment_id,
                ads_segment.start_date AS ads_segment_start,
                ads_segment.end_date AS ads_segment_end
            FROM
                term
            JOIN term_posts ON term_posts.term_id = term.term_id
            JOIN posts AS ads_segment ON ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = 'ads_segment'
            JOIN term_posts AS tp_segment_adaccount ON tp_segment_adaccount.post_id = ads_segment.post_id
            JOIN term AS adaccounts ON tp_segment_adaccount.term_id = adaccounts.term_id AND adaccounts.term_type = 'tiktok_adaccount'
            JOIN term_posts AS tp_insights ON tp_insights.term_id = adaccounts.term_id
            JOIN posts AS insights ON insights.post_id = tp_insights.post_id 
                AND insights.post_type = 'insight_segment' 
                AND insights.post_name = 'day' 
                AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME( ads_segment.start_date, '%Y-%m-%d 00:00:00' )) 
                AND insights.start_date <= IF( ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP(), ads_segment.end_date )
            JOIN postmeta AS m_insight ON m_insight.post_id = insights.post_id AND m_insight.meta_key = 'spend'
            WHERE
                1 = 1
                AND term.term_type = 'tiktok-ads'
                AND term.term_id IN ({$contract_chain_str})
            GROUP BY term.term_id, adaccounts.term_id, ads_segment.post_id

            UNION ALL

            SELECT
                term.term_id,
                0 AS spend,
                SUM(tbl_join_spend.post_content) AS join_spend,
                0 AS manual_spend,
                m_join_spend.meta_value AS join_direction,
                0 AS ads_segment_id,
                0 AS adaccount_id,
                0 AS ads_segment_start,
                0 AS ads_segment_end
            FROM term
                JOIN term_posts ON term_posts.term_id = term.term_id
                JOIN posts AS tbl_join_spend ON tbl_join_spend.post_id = term_posts.post_id
                JOIN postmeta AS m_join_spend ON m_join_spend.post_id = tbl_join_spend.post_id AND m_join_spend.meta_key = 'join_direction'
            WHERE
                1 = 1
                AND term.term_type = 'tiktok-ads'
                AND term.term_id IN ({$contract_chain_str})
                AND tbl_join_spend.post_type = 'balance_spend'
                AND tbl_join_spend.post_title = 'join_command'
                AND tbl_join_spend.comment_status = 'auto'
            GROUP BY 
                term.term_id,
                tbl_join_spend.post_id

            UNION ALL

            SELECT
                term.term_id,
                0 AS spend,
                0 AS join_spend,
                SUM(manual_spend.post_content) AS manual_spend,
                false AS join_direction,
                0 AS ads_segment_id,
                0 AS adaccount_id,
                0 AS ads_segment_start,
                0 AS ads_segment_end
            FROM term
            JOIN term_posts ON term_posts.term_id = term.term_id
            JOIN posts AS manual_spend ON manual_spend.post_id = term_posts.post_id
            WHERE
                1 = 1
                AND term.term_type = 'tiktok-ads'
                AND term.term_id IN ({$contract_chain_str})
                AND manual_spend.post_type = 'balance_spend'
                AND manual_spend.post_title IS NULL
                AND manual_spend.comment_status = 'direct'
            GROUP BY term.term_id
        ";
        $data = $this->tiktokads_m->query($query)->result();
        $data = json_decode(json_encode($data), TRUE);

        $contract_group_by_id = array_group_by($data, 'term_id');
        $contract_reduce = array_reduce($contract_group_by_id, function($result, $contract_data){
            $instance = reset($contract_data);

            $insights = array_filter($contract_data, function($item){
                return 0 != $item['adaccount_id'];
            });

            // Reduce unique $insights
            $insights = array_reduce($insights, function($result, $insight) {
                $segment_id = $insight['ads_segment_id'];
                $ad_account_id = $insight['adaccount_id'];

                // Filter overlap segment
                $overlap_segment = array_filter($result, function($_segment) use ($ad_account_id, $segment_id){
                    // Ignore current segment
                    if($_segment['ads_segment_id'] == $segment_id)
                    {
                        return FALSE;
                    }

                    return $_segment['adaccount_id'] == $ad_account_id;
                }); 
                if(empty($overlap_segment))
                {
                    $result[] = $insight;
                    return $result;
                }

                return $result;
            }, []);

            $contract_data_reduce = [
                'contract_id' => $instance['term_id'],
                'contract_code' => get_term_meta_value($instance['term_id'], 'contract_code'),
                'actual_budget' => (int) get_term_meta_value($instance['term_id'], 'actual_budget'),
                'spend' => round(array_sum(array_column($insights, 'spend'))),
                'manual_spend' => array_sum(array_column($contract_data, 'manual_spend')),
                'join_spend_from' => array_sum(array_column(array_filter($contract_data, function($item){return 'from' == $item['join_direction'];}), 'join_spend')),
                'join_spend_to' => array_sum(array_column(array_filter($contract_data, function($item){return 'to' == $item['join_direction'];}), 'join_spend')),
            ];

            $result[$instance['term_id']] = $contract_data_reduce;

            return $result;
        }, []);
        
        $contract_chain = array_reverse($contract_chain);
        $contract_chain = array_map(function($contract_id) use ($contract_reduce){
            return $contract_reduce[$contract_id] ?? [];
        }, $contract_chain);
        
        return parent::responseHandler($contract_chain, 'Lấy dữ liệu thành công.');
    }

    /**
     * Retrieve the segment history for a specific contract.
     *
     * @param int $contract_id The ID of the contract.
     * @throws Some_Exception_Class If the validation fails.
     * @return Some_Return_Value The segment history data.
     */
    public function segment_history_get($contract_id)
    {
        $defaults = [
            'contract_id' => $contract_id,
            'per_page' => 50,
            'cur_page' => 1,

        ];
        $args = wp_parse_args(parent::get(), $defaults);

        // Validate
        $rules = array(
            [
                'field' => 'contract_id',
                'label' => 'contract_id',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->tiktokads_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($args);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $deleted_segment_ids = get_term_meta($contract_id, 'segment_deleted_id');
        $deleted_segment_ids = array_column($deleted_segment_ids, 'meta_value');
        $deleted_segment_ids = array_unique($deleted_segment_ids);
        
        $current_segment_ids = $this->tiktokads_m->set_term_type()
        ->join('term_posts AS tp_segments', 'tp_segments.term_id = term.term_id')
        ->join('posts AS segments', 'segments.post_id = tp_segments.post_id')
        ->where('segments.post_type =', 'ads_segment')
        ->where('term.term_id', $contract_id)
        ->select('segments.post_id')
        ->group_by('segments.post_id')
        ->as_array()
        ->get_all();
        $current_segment_ids = array_column($current_segment_ids, 'post_id');
        $current_segment_ids = array_unique($current_segment_ids);

        $segment_ids = array_merge($deleted_segment_ids, $current_segment_ids);
        
        $this->load->model('audits_m');
        $this->load->library('datatable_builder');

        $this->datatable_builder
        ->set_filter_position(FILTER_TOP_OUTTER)
        ->setOutputFormat('JSON')
        
        ->from($this->audits_m->_table)
        ->join('user', "user.user_id = {$this->audits_m->_table}.user_id")
        ->where_in("{$this->audits_m->_table}.auditable_id", $segment_ids)
        ->where_in("{$this->audits_m->_table}.auditable_type", ['posts', 'postmeta'])

        ->add_column('user.display_name', 'Người thao tác', )
        ->add_column("{$this->audits_m->_table}.event", 'Sự kiện')
        ->add_column("{$this->audits_m->_table}.auditable_field", 'Trường dữ liệu')
        ->add_column("{$this->audits_m->_table}.new_values", 'Giá trị mới')
        ->add_column("{$this->audits_m->_table}.old_values", 'Giá trị cũ')
        ->add_column("{$this->audits_m->_table}.created_at", 'T/G thực hiện')

        ->order_by("{$this->audits_m->_table}.created_at", 'desc')
        
        ->add_column_callback('new_values', function ($data, $row_name){
            // auditable_field
            switch($data['auditable_field'])
            {
                case 'post_content':
                    $data['auditable_field'] = 'Tỉ lệ phụ trách';
                    $new_values = json_decode($data['new_values'], TRUE);
                    if(!empty($new_values))
                    {
                        $data['new_values'] = $new_values . '%';
                    }
                    else 
                    {
                        $data['new_values'] = '--';
                    }

                    $old_values = json_decode($data['old_values'], TRUE);
                    if(!empty($old_values))
                    {
                        $data['old_values'] = $old_values . '%';
                    }
                    else 
                    {
                        $data['old_values'] = '--';
                    }

                    break;
                
                case 'post_author':
                    $data['auditable_field'] = 'Người phụ trách';

                    $new_values = json_decode($data['new_values'], TRUE);
                    if(!empty($new_values))
                    {
                        $data['new_values'] = $this->admin_m->get_field_by_id($new_values, 'display_name');
                    }
                    else 
                    {
                        $data['new_values'] = '--';
                    }

                    $old_values = json_decode($data['old_values'], TRUE);
                    if(!empty($old_values))
                    {
                        $data['old_values'] = $this->admin_m->get_field_by_id($old_values, 'display_name');
                    }
                    else 
                    {
                        $data['old_values'] = '--';
                    }

                    break;
                
                case 'start_date':
                    $data['auditable_field'] = 'Ngày bắt đầu';
                    
                    $new_values = json_decode($data['new_values'], TRUE);
                    if(!empty($new_values))
                    {
                        $data['new_values'] = date('Y-m-d H:i:s', $new_values);
                    }
                    else 
                    {
                        $data['new_values'] = '--';
                    }

                    $old_values = json_decode($data['old_values'], TRUE);
                    if(!empty($old_values))
                    {
                        $data['old_values'] = date('Y-m-d H:i:s', $old_values);
                    }
                    else 
                    {
                        $data['old_values'] = '--';
                    }

                    break;

                case 'end_date':
                    $data['auditable_field'] = 'Ngày kết thúc';
                    
                    $new_values = json_decode($data['new_values'], TRUE);
                    if(!empty($new_values))
                    {
                        $data['new_values'] = date('Y-m-d H:i:s', $new_values);
                    }
                    else 
                    {
                        $data['new_values'] = '--';
                    }

                    $old_values = json_decode($data['old_values'], TRUE);
                    if(!empty($old_values))
                    {
                        $data['old_values'] = date('Y-m-d H:i:s', $old_values);
                    }
                    else 
                    {
                        $data['old_values'] = '--';
                    }

                    break;
                
                default:
                    break;
            }

            return $data;
        }, FALSE);
        
        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];
        $data = $this->datatable_builder->generate($pagination_config);

        return parent::response($data, 'Lấy dữ liệu thành công.');
    }
}
/* End of file Resource.php */
/* Location: ./application/modules/tiktokads/controllers/api_v2/Resource.php */