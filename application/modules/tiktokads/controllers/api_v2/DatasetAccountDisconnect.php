<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use AdsService\Tiktokads\Enums\AdvertiserStatusEnum;

class DatasetAccountDisconnect extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'tiktokads/tiktokads_m';
        $this->autoload['models'][] = 'tiktokads/adaccount_m';
        $this->autoload['models'][] = 'tiktokads/tiktokads_kpi_m';
        $this->autoload['models'][] = 'ads_segment_m';
        parent::__construct($config);

        $this->load->config('tiktokads/tiktokads');
        $this->load->config('tiktokads/contract');
    }


    /**
     * Render dataset of FB's services
     */
    public function index_get()
    {
        $response   = array('status' => FALSE, 'msg' => 'Quá trình xử lý không thành công.', 'data' => []);
        $permission = 'tiktokads.index.access';

        if (!has_permission($permission)) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        $relate_users       = $this->admin_m->get_all_by_permissions($permission);
        if ($relate_users === FALSE) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        $this->load->library('datatable_builder');

        if (is_array($relate_users)) {
            $this->datatable_builder->join('term_users', 'term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
        }

        $default = array(
            'offset'        => 0,
            'per_page'      => 10,
            'cur_page'      => 1,
            'is_filtering'  => TRUE,
            'is_ordering'   => TRUE,
            'filter_position'   => FILTER_TOP_OUTTER,
        );

        $args               = wp_parse_args($this->input->get(), $default);

        $this->search_filter();

        $columns = $this->getColumns();
        foreach($columns AS $key => $value) $this->datatable_builder->add_column($key, $value);

        $callbacks = $this->getCallbacks(array_keys($columns));
        foreach($callbacks AS $callback) $this->datatable_builder->add_column_callback($callback['field'], $callback['func'], $callback['row_data']);

        $data =  $this->datatable_builder
            ->setOutputFormat('JSON')

            ->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])
            ->add_search('customer_code', ['placeholder' => 'Mã khách hàng'])
            ->add_search('customer_name', ['placeholder' => 'Khách hàng'])
            ->add_search('adaccount_id', ['placeholder' => 'ID tài khoản'])
            ->add_search('adaccount_name', ['placeholder' => 'Tên tài khoản'])
            ->add_search('staff_business', ['placeholder' => 'Nhân viên kinh doanh'])
            ->add_search('staff_advertise', ['placeholder' => 'Nhân viên kỹ thuật'])

            ->from('term')

            ->select('term.term_id AS contract_id')
            ->select('term.term_status')
            ->select('adaccount.term_slug AS adaccount_id')
            ->select('adaccount.term_name AS adaccount_name')
            ->select('adaccount.term_status AS adaccount_status')
            
            ->join('term_posts AS tp_ads_segment', 'tp_ads_segment.term_id = term.term_id')
            ->join('posts AS ads_segment', "ads_segment.post_id = tp_ads_segment.post_id AND ads_segment.post_type = '{$this->ads_segment_m->post_type}'")
            ->join('term_posts AS tp_adaccount', 'tp_adaccount.post_id = ads_segment.post_id')
            ->join('term AS adaccount', "adaccount.term_id = tp_adaccount.term_id AND adaccount.term_type = '{$this->adaccount_m->term_type}'")

            ->where('term.term_type', $this->tiktokads_m->term_type)
            ->where('adaccount.term_status !=', AdvertiserStatusEnum::STATUS_ENABLE)
            
            ->group_by('term.term_id, adaccount.term_id')

            ->generate(array('per_page' => $args['per_page'], 'cur_page' => $args['cur_page']));

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export($last_query);
            return TRUE;
        }

        parent::response($data);
    }

    protected function getColumns($type = 'default')
    {
        $enums = array(
            'contract_code' => [ 'type' => 'string', 'title' => 'Mã hợp đồng', 'set_order' => TRUE, 'set_select' => FALSE ],
            'customer_code' => [ 'type' => 'string', 'title' => 'Mã khách hàng', 'set_order' => TRUE, 'set_select' => FALSE ],
            'customer_name' => [ 'type' => 'string', 'title' => 'Tên khách hàng', 'set_order' => TRUE, 'set_select' => FALSE ],
            'adaccount_id' => [ 'type' => 'string', 'title' => 'ID tài khoản', 'set_order' => TRUE, 'set_select' => FALSE ],
            'adaccount_name' => [ 'type' => 'string', 'title' => 'Tên tài khoản', 'set_order' => TRUE, 'set_select' => FALSE ],
            'staff_business' => [ 'type' => 'string', 'title' => 'NVKD', 'set_order' => TRUE, 'set_select' => FALSE ],
            'staff_advertise' => [ 'type' => 'string', 'title' => 'NVKT', 'set_order' => FALSE, 'set_select' => FALSE ],
        );

        $config = array(
            'default' => array(
                'contract_code',
                'customer_code',
                'customer_name',
                'adaccount_id',
                'adaccount_name',
                'staff_business',
                'staff_advertise',
            ),

            'export' => array(
                'contract_code',
                'customer_code',
                'customer_name',
                'adaccount_id',
                'adaccount_name',
                'staff_business',
                'staff_advertise',
            )
        );

        return elements($config[$type], $enums);
    }

    protected function getCallbacks($args = [])
    {
        $callbacks = [];

        $callbacks['contract_code'] = [
            'field' => 'contract_code',    
            'func'  => function($data, $row_name){
                $contract_id = $data['contract_id'];

                $data['contract_code'] = get_term_meta_value($contract_id, 'contract_code') ?: '--';

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['customer_code'] = [
            'field' => 'customer_code',    
            'func'  => function($data, $row_name){
                $contract_id = $data['contract_id'];

                $data['customer_code'] = '--';

                $customers = $this->term_users_m->get_the_users($contract_id, ['customer_person','customer_company']);
                if(empty($customers)) return $data;

                $customer = end($customers);
                
                $data['customer_id'] = $customer->user_id;
                $data['customer_code'] = cid($customer->user_id, $customer->user_type);
                $data['customer_name'] = $customer->display_name;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['actual_budget'] = [
            'field' => 'actual_budget',
            'func'  => function($data, $row_name) {
                $contract_id = $data['contract_id'];

                $data['actual_budget'] = (int) get_term_meta_value($contract_id, 'actual_budget');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['spend'] = [
            'field' => 'spend',
            'func'  => function($data, $row_name) {
                $contract_id = $data['contract_id'];

                $data['spend_raw'] = (int) $data['spend'];

                $balance_spend = (int) get_term_meta_value($contract_id, 'balance_spend');
                $data['spend'] = (int) $data['spend'] + $balance_spend;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['remain_budget'] = [
            'field' => 'remain_budget',    
            'func'  => function($data, $row_name){
                $contract_id = $data['contract_id'];
                
                $actual_budget = (int) get_term_meta_value($contract_id, 'actual_budget');
                $balance_spend = (int) get_term_meta_value($contract_id, 'balance_spend');
                $data['remain_budget'] = $actual_budget - (int) $data['spend_raw'] - $balance_spend;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['staff_business'] = [
            'field' => 'staff_business',    
            'func'  => function($data, $row_name){
                $contract_id = $data['contract_id'];

                $data['staff_business'] = [];
                
                $staff_business 		= get_term_meta_value($contract_id, 'staff_business');
                if(empty($staff_business)) return $data;
                
                $staff_business = $this->admin_m->get_field_by_id($staff_business);
                
                $staff_business = [
                    'user_id' => $staff_business['user_id'],
                    'display_name' => $staff_business['display_name'],
                    'user_avatar' => $staff_business['user_avatar'],
                    'user_email' => $staff_business['user_email'],
                ];
            
                $data['staff_business'] = $staff_business;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $this->load->model('tiktokads/tiktokads_kpi_m');
        $callbacks['staff_advertise'] = [
            'field' => 'staff_advertise',    
            'func'  => function($data, $row_name){
                $contract_id 		= $data['contract_id'];

                $data['staff_advertise'] = [];

			    $kpis = $this->tiktokads_kpi_m->get_kpis($contract_id, 'users', 'tech');
			    if(empty($kpis)) return $data;
                
			    $user_ids = array();
			    foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;

			    $data['staff_advertise'] = array_reduce($user_ids, function($result, $user_id){
			    	$_user = $this->admin_m
                    ->select('user_id, user_email, display_name, user_avatar, user.role_id, role_name')
                    ->join('role', 'role.role_id = user.role_id')
                    ->as_array()
                    ->get($user_id);
                    if(empty($_user)) return $result;

                    $user = [
                        'user_id' => $_user['user_id'],
                        'user_email' => $_user['user_email'],
                        'display_name' => $_user['display_name'],
                        'user_avatar' => $_user['user_avatar'],
                        'role_name' => $_user['role_name'] ?? NULL,
                    ];
                    $result[] = $user;
			    	
                    return $result;
			    }, []);

                return $data;
            },
            'row_data' => FALSE,
        ];

        $selected_callbacks = [];
        if(empty($args)) $selected_callbacks = $callbacks;

        foreach($args AS $callback_name){
            if(!is_string($callback_name)) continue;
            if(!isset($callbacks[$callback_name])) continue;

            $selected_callbacks[$callback_name] = $callbacks[$callback_name];
        }

        return $selected_callbacks;
    }

    protected function search_filter($search_args = array())
    {
        $args = $this->input->get();

        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // Contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $alias = uniqid('contract_code_');
            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_contract_code);
            }

            unset($args['where']['contract_code'], $args['order_by']['contract_code']);
        }

        // customer_code FILTERING & SORTING
        $filter_customer_code = $args['where']['customer_code'] ?? FALSE;
        $sort_customer_code   = $args['order_by']['customer_code'] ?? FALSE;
        if($filter_customer_code || $sort_customer_code)
        {
            $alias = uniqid('customer_code_');
            $this->datatable_builder
            ->join("term_users tu_customer","tu_customer.term_id = term.term_id")
            ->join("user customer","customer.user_id = tu_customer.user_id AND customer.user_type IN ('customer_person', 'customer_company')")
            ->join("usermeta {$alias}","{$alias}.user_id = customer.user_id and {$alias}.meta_key = 'cid'", 'LEFT');

            if($filter_customer_code)
            {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_customer_code);
                unset($args['where']['customer_code']);
            }

            if($sort_customer_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_customer_code);
                unset($args['order_by']['customer_code']);
            }
        }

        // customer_name FILTERING & SORTING
        $filter_customer_name = $args['where']['customer_name'] ?? FALSE;
        $sort_customer_name = $args['order_by']['customer_name'] ?? FALSE;
        if ($filter_customer_name || $sort_customer_name) {
            $alias = uniqid('customer_name_');

            $this->datatable_builder
                ->join("term_users AS tp_{$alias}", "tp_{$alias}.term_id = term.term_id", 'LEFT')
                ->join("user AS {$alias}", "tp_{$alias}.user_id = {$alias}.user_id", 'LEFT')
                ->where_in("{$alias}.user_type", ['customer_person', 'customer_company']);

            if ($filter_customer_name) {
                $this->datatable_builder->like("{$alias}.display_name", $filter_customer_name);
                unset($args['where']['customer_name']);
            }
    
            if ($sort_customer_name) {
                $this->datatable_builder->order_by("{$alias}.display_name", $sort_customer_name);
                unset($args['order_by']['customer_name']);
            }
        }

        // adaccount_id FILTERING & SORTING
        $filter_adaccount_id = $args['where']['adaccount_id'] ?? FALSE;
        $sort_adaccount_id = $args['order_by']['adaccount_id'] ?? FALSE;
        if ($filter_adaccount_id || $sort_adaccount_id) {
            if ($filter_adaccount_id) {
                $this->datatable_builder->like("adaccount.term_slug", $filter_adaccount_id);
                unset($args['where']['adaccount_id']);
            }
    
            if ($sort_adaccount_id) {
                $this->datatable_builder->order_by("adaccount.term_slug", $sort_adaccount_id);
                unset($args['order_by']['adaccount_id']);
            }
        }

        // adaccount_name FILTERING & SORTING
        $filter_adaccount_name = $args['where']['adaccount_name'] ?? FALSE;
        $sort_adaccount_name = $args['order_by']['adaccount_name'] ?? FALSE;
        if ($filter_adaccount_name || $sort_adaccount_name) {
            if ($filter_adaccount_name) {
                $this->datatable_builder->like("adaccount.term_name", $filter_adaccount_name);
                unset($args['where']['adaccount_name']);
            }
    
            if ($sort_adaccount_name) {
                $this->datatable_builder->order_by("adaccount.term_name", $sort_adaccount_name);
                unset($args['order_by']['adaccount_name']);
            }
        }

        // Staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if ($filter_staff_business || $sort_staff_business) {
            $alias = uniqid('staff_business_');
            $this->datatable_builder
                ->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT')
                ->join("user tblcustomer", "{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

            if ($filter_staff_business) {
                $this->datatable_builder->like("tblcustomer.display_name", $filter_staff_business);
                unset($args['where']['staff_business']);
            }

            if ($sort_staff_business) {
                $this->datatable_builder->order_by('tblcustomer.display_name', $sort_staff_business);
                unset($args['order_by']['staff_business']);
            }
        }

        // FIND AND SORT STAFF_ADVERTISE WITH Google_KPI
        $filter_staff_advertise = $args['where']['staff_advertise'] ?? FALSE;
        if ($filter_staff_advertise) {
            $this->datatable_builder->join('googleads_kpi', 'term.term_id = googleads_kpi.term_id', 'LEFT OUTER');
            $this->datatable_builder->join('user', 'user.user_id = googleads_kpi.user_id', 'LEFT OUTER');

            if ($filter_staff_advertise) {
                $this->datatable_builder->like('user.display_name', strtolower($filter_staff_advertise), 'both');
            }

            unset($args['where']['staff_advertise'], $args['order_by']['staff_advertise']);
        }
    }

    public function export($query = '')
    {
        if (empty($query)) return FALSE;

        // remove limit in query string
        $query = explode('LIMIT', $query);
        $query = reset($query);

        $terms = $this->contract_m->query($query)->result();

        if (!$terms) return FALSE;

        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("Báo cáo hợp đồng Google tạm ngưng"));
        $sheet = $spreadsheet->getActiveSheet();

        $columns = $this->getColumns('export');
        $sheet->fromArray(array_column(array_values($columns), 'title'), NULL, 'A1');

        $rowIndex = 2;

        $this->load->model('tiktokads/tiktokads_kpi_m');

        foreach ($terms as $item) {
            $i = 1;
            $colIndex = $i;

            $term_id = $item->contract_id;

            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, get_term_meta_value($term_id, 'contract_code') ?: '--');
            $colIndex++;

            $customer_code = '--';
            $customer_name = '--';
            $customers = $this->term_users_m->get_the_users($term_id, ['customer_person', 'customer_company']);
            if (!empty($customers)) {
                $customer = end($customers);
                
                $customer_code = cid($customer->user_id, $customer->user_type);
                $customer_name = $customer->display_name;
            }
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $customer_code);
            $colIndex++;
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $customer_name);
            $colIndex++;
            
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item->adaccount_id);
            $colIndex++;

            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item->adaccount_name);
            $colIndex++;

            $staff_business         = '--';
            $staff_business_id         = get_term_meta_value($term_id, 'staff_business');
            if (!empty($staff_business_id)) {
                $staff_business = $this->admin_m->get_field_by_id($staff_business_id, 'display_name');
            }
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $staff_business);
            $colIndex++;

            $staff_advertise = '--';
            $kpis = $this->tiktokads_kpi_m->get_kpis($term_id, 'users', 'tech');
            if (!empty($kpis)) {
                $user_ids = array();
                foreach ($kpis as $uids) foreach ($uids as $i => $val) $user_ids[$i] = $i;

                $staff_advertise = implode(', ', array_values(array_map(function ($user_id) {
                    $_user = $this->admin_m->get_field_by_id($user_id);
                    return $_user['display_name'] ?: $_user['user_email'];
                }, $user_ids)));
            }
            $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $staff_advertise);
            $colIndex++;

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report/';
        if (!is_dir($folder_upload)) {
            try {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            } catch (Exception $e) {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}-bao-cao-hop-dong-google-mat-ket-noi-tai-khoan.xlsx";

        try {
            (new Xlsx($spreadsheet))->save($fileName);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }
}
/* End of file DatasetAdsPending.php */
/* Location: ./application/modules/tiktokads/controllers/api_v2/DatasetAdsPending.php */
