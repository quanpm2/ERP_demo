<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class DataReport extends MREST_Controller
{
    private $permission = "reports.spend.access";

    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['models'][]     = 'customer/customer_m';
        $this->autoload['models'][]     = 'tiktokads/tiktokads_m';
        $this->autoload['models'][]     = 'tiktokads/adaccount_m';
        $this->autoload['models'][]     = 'ads_segment_m';
        $this->autoload['models'][]     = 'tiktokads/insight_segment_m';
        $this->autoload['models'][]     = 'staffs/department_m';
        $this->autoload['helpers'][]    = 'array';

        parent::__construct();
        
        $this->load->config('contract/contract');
        $this->load->config('tiktokads/contract');
    }

    /**
     * Tiktok Ads Monthly Spend
     *
     * @throws     Exception  (description)
     */
    public function monthly_get()
    {
        if (!has_permission($this->permission)) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        $args = wp_parse_args(parent::get(null, TRUE), [
            'month' => my_date(time(), 'm'),
            'year' => my_date(time(), 'Y')
        ]);

        $this->load->library('form_validation');
        $this->form_validation->set_data($args);
        $this->form_validation->set_rules('month', 'Month', 'required|integer|greater_than_equal_to[1]|less_than_equal_to[12]');
        $this->form_validation->set_rules('year', 'Year', 'required|integer|greater_than_equal_to[2015]|less_than_equal_to['.my_date(strtotime("+1 year"), 'Y').']');
        if( FALSE == $this->form_validation->run()) parent::response([ 'code' => 400, 'error' => $this->form_validation->error_array() ]);

        $start_time = start_of_month(date("{$args['year']}/{$args['month']}/d"));
        $end_time   = end_of_month(date("{$args['year']}/{$args['month']}/d"));

        $args['start_time'] = $start_time;
        $args['end_time'] = $end_time ;
        $data = $this->build($args);

        return parent::response($data);
    }


    /**
     * Range Spend
     *
     * @throws     Exception  (description)
     */
    public function ranges_get($start = '', $end = '')
    {
        if(empty($start) || empty($end)) parent::response([], 400);
        if (!has_permission($this->permission)) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        $start_time = start_of_day($start);
        $end_time = end_of_day($end);

        $args           = [ 'start_time' => $start_time, 'end_time' => $end_time ];
        $data           = $this->build($args);

        return parent::response($data);
    }

    /**
     * Buidl Raw Data
     */
    protected function build( $args = [])
    {
        $args = wp_parse_args($args, [
            'start_time'    => $args['start_time'],
            'end_time'      => $args['end_time'],
            'offset' => 0,
            'per_page' => 20,
            'cur_page' => 1,
            'is_filtering' => true,
            'is_ordering' => true
        ]);

        $start_time = start_of_day( $args['start_time']);
        $end_time   = end_of_day( $args['end_time']);

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        $relate_users       = $this->admin_m->get_all_by_permissions($this->permission);
        if ($relate_users === FALSE) {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        if (is_array($relate_users)) {
            $this->datatable_builder->join('term_users AS relate_user', 'relate_user.term_id = term.term_id')->where_in('relate_user.user_id', $relate_users);
        }

        $columns = $this->getColumns();
        foreach($columns AS $key => $value) $this->datatable_builder->add_column($key, $value);

        $callbacks = $this->getCallbacks(array_keys($columns));
        foreach($callbacks AS $callback) $this->datatable_builder->add_column_callback($callback['field'], $callback['func'], $callback['row_data']);

        $this->datatable_builder
        ->set_filter_position(FILTER_TOP_INNER)
        ->setOutputFormat('JSON')

        ->add_search('contract_code', ['placeholder' => 'Mã hợp đồng'])
        ->add_search('customer_code', ['placeholder' => 'Mã KH'])
        ->add_search('staff_business', ['placeholder' => 'NVKD'])
        ->add_search('tech', ['placeholder' => 'Kỹ thuật'])
        ->add_search('adaccount_id', ['placeholder' => 'AdAccount ID'])
        ->add_search('adaccount_name', ['placeholder' => 'AdAccount Name'])

        ->from('term')
        
        ->join('term_users AS tu_customer', 'tu_customer.term_id = term.term_id')
        ->join('user AS customers', 'customers.user_id = tu_customer.user_id')
        ->join('termmeta AS m_adaccounts_status', 'm_adaccounts_status.term_id = term.term_id AND m_adaccounts_status.meta_key = "adaccount_status" AND m_adaccounts_status.meta_value = "APPROVED"')
        ->join('term_posts AS tp_segments', 'tp_segments.term_id = term.term_id')
        ->join('posts AS segments', 'segments.post_id = tp_segments.post_id')
        ->join('term_posts AS tp_adaccounts', 'tp_adaccounts.post_id = segments.post_id')
        ->join('term AS adaccounts', 'tp_adaccounts.term_id = adaccounts.term_id')
        ->join('term_posts AS tp_insights', 'tp_insights.term_id = adaccounts.term_id')
        ->join('posts AS insights', 'tp_insights.post_id = insights.post_id ')
        ->join('postmeta AS m_insights', 'm_insights.post_id = insights.post_id AND m_insights.meta_key IN ("spend", "account_currency", "exchange_rate")', 'LEFT')

        ->where('term.term_type', $this->tiktokads_m->term_type)
        ->where_in('customers.user_type', ['customer_person', 'customer_company'])
        ->where_in('term.term_status', ['pending', 'publish', 'ending', 'liquidation', 'remove'])
        ->where('segments.post_type = "ads_segment"')
        ->where("adaccounts.term_type = '{$this->adaccount_m->term_type}'")
        ->where('insights.post_type = "insight_segment"')
        ->where('insights.start_date >= segments.start_date')
        ->where('insights.start_date <= IF(segments.end_date = 0 OR segments.end_date IS NULL, UNIX_TIMESTAMP(), segments.end_date)')
        ->where('insights.start_date >=', $start_time)
        ->where('insights.start_date <=', $end_time)

        ->group_by('term.term_id, segments.post_id, adaccounts.term_id')

        ->select('
            term.term_id,
            term.term_name,
            term.term_status,
            term.term_type,

            customers.user_id AS customer_id,
	        customers.display_name AS customer_name,
	        customers.user_type AS customer_type,
            segments.post_id AS segmentId,
            segments.start_date AS segmentStartDate,
            segments.end_date AS segmentEndDate,
            adaccounts.term_id AS adaccountId,
            adaccounts.term_name AS adaccountName,
            adaccounts.term_slug AS adaccountSlug,
            SUM(IF(m_insights.meta_key = "spend", m_insights.meta_value, 0)) AS spend,
	        MAX(IF(m_insights.meta_key = "account_currency", m_insights.meta_value, NULL)) AS account_currency,
	        MAX(IF(m_insights.meta_key = "exchange_rate", m_insights.meta_value, NULL)) AS exchange_rate
        ');

        $this->search_filter();

        $data = $this->datatable_builder->generate(['per_page' => $args['per_page'],'cur_page' => $args['cur_page']]);
        
        if( ! empty($args['download']) && $last_query = $this->datatable_builder->last_query())
        {
            $this->export($last_query);
            return TRUE;
        }

        return $data;
    }

    protected function search_filter($search_args = array())
    {   
        $args = $this->input->get();

        if( ! empty($args['where'])) $args['where'] = array_map(function($x) { return trim($x);}, $args['where']);

        // Contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if($filter_contract_code || $sort_contract_code)
        {
            $alias = uniqid('contract_code_');
            $this->datatable_builder->join("termmeta {$alias}","{$alias}.term_id = term.term_id AND {$alias}.meta_key = 'contract_code'", 'LEFT');

            if($filter_contract_code)
            {   
                $this->datatable_builder->like("{$alias}.meta_value", $filter_contract_code);
            }

            if($sort_contract_code)
            {
                $this->datatable_builder->order_by('term.term_id', $sort_contract_code);
            }

            unset($args['where']['contract_code'],$args['order_by']['contract_code']);
        }

        // customer_code FILTERING & SORTING
        $filter_customer_code = $args['where']['customer_code'] ?? FALSE;
        $sort_customer_code   = $args['order_by']['customer_code'] ?? FALSE;
        if($filter_customer_code || $sort_customer_code)
        {
            $alias = uniqid('customer_code_');
            $this->datatable_builder
            ->join("usermeta {$alias}","{$alias}.user_id = customers.user_id and {$alias}.meta_key = 'cid'", 'LEFT');

            if($filter_customer_code)
            {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_customer_code);
                unset($args['where']['customer_code']);
            }

            if($sort_customer_code)
            {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_customer_code);
                unset($args['order_by']['customer_code']);
            }
        }

        // adaccount_id FILTERING & SORTING
        $filter_adaccount_id = $args['where']['adaccount_id'] ?? FALSE;
        if($filter_adaccount_id)
        {
            $this->datatable_builder->like('adaccounts.term_slug', $filter_adaccount_id);
            
            unset($args['where']['adaccount_id']);
        }

        $order_adaccount_id = $args['order_by']['adaccount_id'] ?? FALSE;
        if($order_adaccount_id)
        {
            $this->datatable_builder->order_by('adaccounts.term_slug', $order_adaccount_id);
            
            unset($args['order_by']['adaccount_id']);
        }

        // adaccount_name FILTERING & SORTING
        $filter_adaccount_name = $args['where']['adaccount_name'] ?? FALSE;
        if($filter_adaccount_name)
        {
            $this->datatable_builder->like('adaccounts.term_name', $filter_adaccount_name);
            
            unset($args['where']['adaccount_name']);
        }

        $order_adaccount_name = $args['order_by']['adaccount_name'] ?? FALSE;
        if($order_adaccount_name)
        {
            $this->datatable_builder->order_by('adaccounts.term_name', $order_adaccount_name);
            
            unset($args['order_by']['adaccount_name']);
        }

        // Staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if($filter_staff_business || $sort_staff_business)
        {
            $alias = uniqid('staff_business_');
            $this->datatable_builder
            ->join("termmeta {$alias}","{$alias}.term_id = term.term_id and {$alias}.meta_key = 'staff_business'", 'LEFT')
            ->join("user tblcustomer","{$alias}.meta_value = tblcustomer.user_id", 'LEFT');

            if($filter_staff_business)
            {   
                $this->datatable_builder->like("tblcustomer.display_name",$filter_staff_business);
                unset($args['where']['staff_business']);
            }

            if($sort_staff_business)
            {
                $this->datatable_builder->order_by('tblcustomer.display_name',$sort_staff_business);
                unset($args['order_by']['staff_business']);
            }
        }

        // FIND AND SORT STAFF_ADVERTISE WITH TIKTOK_KPI
        $filter_tech = $args['where']['tech'] ?? FALSE;
        if($filter_tech)
        {
            $this->datatable_builder->join('googleads_kpi','term.term_id = googleads_kpi.term_id', 'LEFT OUTER');
            $this->datatable_builder->join('user','user.user_id = googleads_kpi.user_id', 'LEFT OUTER');

            if($filter_tech)
            {   
                $this->datatable_builder->like('user.display_name',strtolower($filter_tech),'both');
            }

            unset($args['where']['tech']);
        }
    }

    protected function export($query = '')
    {
        if(empty($query)) return FALSE;

        $query = explode('LIMIT', $query);
		$query = reset($query);

        $title = my_date(time(), 'YmdHis') . '_bao-cao-spend-tiktok-ads';
        $spreadsheet    = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('ADSPLUS.VN')->setLastModifiedBy('ADSPLUS.VN')->setTitle(uniqid("{$title}_"));
        $sheet = $spreadsheet->getActiveSheet();

        $columns = $this->getColumns('export');
        $sheet->fromArray(array_column(array_values($columns), 'title'), NULL, 'A1');

        $data = $this->db->query($query)->result_array();
        
        $callbacks = $this->getCallbacks(array_keys($columns));

        $rowIndex = 2;

        foreach ($data as $item)
        {
            $i = 1;
            $colIndex = $i;

            foreach ($columns as $key => $column)
            {
                if(isset($callbacks[$key])){
                    $item = call_user_func_array($callbacks[$key]['func'], array($item, $key));
                }

                switch ($column['type'])
                {
                    case 'number': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("#,##0");
                        break;
                    
                    case 'decimal': 
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode("0.00");
                        break;

                    case 'timestamp': 
                        $item[$key] = Date::PHPToExcel($item[$key]);
                        $sheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
                        break;

                    default: break;
                }

                $item[$key] = $item["{$key}_raw"] ?? ($item[$key] ?? '');

                @$sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item[$key]);
                $colIndex++;
            }

            $rowIndex++;
        }

        $folder_upload  = 'files/contract/report';
        if( ! is_dir($folder_upload))
        {
            try 
            {
                mkdir($folder_upload, 0777, TRUE);
                umask(umask(0));
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
                return FALSE;
            }
        }

        $fileName = "{$folder_upload}/{$title}.xlsx";

        try
        {
            (new Xlsx($spreadsheet))->save($fileName);
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage());
            return FALSE;
        }

        $this->load->helper('download');
        force_download($fileName, NULL);
        return TRUE;
    }

    protected function getColumns($type = 'default')
    {
        $enums = array(
            'contract_code'           => [ 'type' => 'string', 'title' => 'Mã hợp đồng', 'set_order' => TRUE, 'set_select' => FALSE ],
            'contract_code_link'      => [ 'type' => 'string', 'title' => 'Mã hợp đồng', 'set_order' => FALSE, 'set_select' => FALSE ],
            'contract_code_link_tag'  => [ 'type' => 'string', 'title' => 'Mã hợp đồng', 'set_order' => FALSE, 'set_select' => FALSE ],
            'customer_code'           => [ 'type' => 'string', 'title' => 'Mã Khách Hàng', 'set_order' => TRUE, 'set_select' => FALSE ],
            'customer_code_1'         => [ 'type' => 'string', 'title' => 'Customer ID', 'set_order' => TRUE, 'set_select' => FALSE ],
            'customer_name'           => [ 'type' => 'string', 'title' => 'Khách hàng', 'set_order' => TRUE, 'set_select' => FALSE ],
            'Service'                 => [ 'type' => 'string', 'title' => 'Dịch vụ', 'set_order' => FALSE, 'set_select' => FALSE ],
            'payment_type'            => [ 'type' => 'string', 'title' => 'Loại', 'set_order' => FALSE, 'set_select' => FALSE ],
            'typeOfService'           => [ 'type' => 'string', 'title' => 'Loại hình dịch vụ', 'set_order' => FALSE, 'set_select' => FALSE ],
            'cid'                     => [ 'type' => 'string', 'title' => 'Customer ID', 'set_order' => FALSE, 'set_select' => FALSE ],
            'adaccount_id'            => [ 'type' => 'string', 'title' => 'AdAccount ID', 'set_order' => TRUE, 'set_select' => FALSE ],
            'adaccount_name'          => [ 'type' => 'string', 'title' => 'AdAccount Name', 'set_order' => TRUE, 'set_select' => FALSE ],
            'tech'                    => [ 'type' => 'string', 'title' => 'Kỹ thuật', 'set_order' => FALSE, 'set_select' => FALSE ],
            'contract_daterange'      => [ 'type' => 'string', 'title' => 'T/G HĐ', 'set_order' => FALSE, 'set_select' => FALSE ],
            'contract_value'          => [ 'type' => 'number', 'title' => 'Giá trị hợp đồng', 'set_order' => FALSE, 'set_select' => FALSE ],
            'start_date'              => [ 'type' => 'string', 'title' => 'Ngày bắt đầu', 'set_order' => FALSE, 'set_select' => FALSE ],
            'end_date'                => [ 'type' => 'string', 'title' => 'Ngày kết thúc', 'set_order' => FALSE, 'set_select' => FALSE ],
            'term_status'             => [ 'type' => 'string', 'title' => 'Trạng thái', 'set_order' => FALSE, 'set_select' => FALSE ],
            'department'              => [ 'type' => 'string', 'title' => 'Phòng', 'set_order' => FALSE, 'set_select' => FALSE ],
            'sale_name'               => [ 'type' => 'string', 'title' => 'Tên nhân viên', 'set_order' => FALSE, 'set_select' => FALSE ],
            'sale_email'              => [ 'type' => 'string', 'title' => 'Email nhân viên', 'set_order' => FALSE, 'set_select' => FALSE ],
            'staff_business'          => [ 'type' => 'string', 'title' => 'NVKD', 'set_order' => TRUE, 'set_select' => FALSE ],
            'staff_business_email'    => [ 'type' => 'string', 'title' => 'Email NVKD', 'set_order' => TRUE, 'set_select' => FALSE ],
            'account_currency'        => [ 'type' => 'string', 'title' => 'Đơn vị tiền', 'set_order' => FALSE, 'set_select' => FALSE ],
            'remain_budget'           => [ 'type' => 'number', 'title' => 'Ngân sách còn lại', 'set_order' => FALSE, 'set_select' => FALSE ],
            'exchange_rate'           => [ 'type' => 'number', 'title' => 'Tỉ giá', 'set_order' => FALSE, 'set_select' => FALSE ],
            'spend'                   => [ 'type' => 'number', 'title' => 'Ngân sách chạy trong tháng', 'set_order' => TRUE, 'set_select' => FALSE ],
            'service_fee'             => [ 'type' => 'number', 'title' => 'Phí quản lý', 'set_order' => FALSE, 'set_select' => FALSE ],
            'discount_amount'         => [ 'type' => 'number', 'title' => 'Giảm giá', 'set_order' => FALSE, 'set_select' => FALSE ],
            'service_fee_rate'        => [ 'type' => 'decimal', 'title' => '% Phí quản lý', 'set_order' => FALSE, 'set_select' => FALSE ],
            'service_fee_rate_actual' => [ 'type' => 'decimal', 'title' => '% Phí quản lý (thực tế)', 'set_order' => FALSE, 'set_select' => FALSE ],
        );

        $config = array(
            'default' => array(
                'contract_code',
                'customer_code',
                'customer_name',
                'Service',
                'payment_type',
                'typeOfService',
                'adaccount_id',
                'adaccount_name',
                'tech',
                'contract_daterange',
                'contract_value',
                'start_date',
                'end_date',
                'term_status',
                'staff_business',
                'department',
                'account_currency',
                'remain_budget',
                'exchange_rate',
                'spend',
                'service_fee',
                'discount_amount',
                'service_fee_rate',
                'service_fee_rate_actual'
            ),

            'export' => array(
                'contract_code',
                'customer_code',
                'customer_name',
                'Service',
                'payment_type',
                'typeOfService',
                'customer_code_1',
                'adaccount_id',
                'adaccount_name',
                'tech',
                'contract_daterange',
                'contract_value',
                'start_date',
                'end_date',
                'term_status',
                'department',
                'staff_business',
                'staff_business_email',
                'account_currency',
                'remain_budget',
                'exchange_rate',
                'spend',
                'service_fee',
                'discount_amount',
                'service_fee_rate',
                'service_fee_rate_actual'
            )
        );

        return elements($config[$type], $enums);
    }

    protected function getCallbacks($args = [])
    {
        $callbacks = [];

        $callbacks['contract_code'] = [
            'field' => 'contract_code',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['contract_code'] = get_term_meta_value($term_id, 'contract_code') ?: '--';
                $data['Service'] ='TIKTOK ADS';

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['customer_code'] = [
            'field' => 'customer_code',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['customer_code'] = '--';
                
                $customer_id = $data['customer_id'];
                if(empty($customer_id)) return $data;
                
                $customer_type = $data['customer_type'];

                $data['customer_code'] = cid($customer_id, $customer_type);

                return $data;
            },
            'row_data' => FALSE,
        ];

        $this->config->load('tiktokads/contract');
        $typeOfServices = $this->config->item('items', 'typeOfServices');
        $callbacks['typeOfService'] = [
            'field' => 'typeOfService',    
            'func'  => function($data, $row_name) use ($typeOfServices){
                $term_id = $data['term_id'];

                $data['typeOfService'] = '--';
                $typeOfService = get_term_meta_value($term_id, 'typeOfService');
                if(empty($typeOfService)) return $data;
                
                $typeOfService = $typeOfServices[$typeOfService] ?? '--';
                $data['typeOfService'] = $typeOfService;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $payment_type_enums = [
            'normal' => 'Full VAT',
            'customer' => 'Khách hàng tự thanh toán',
            'behalf' => 'ADSPLUS thu & chi hộ'
        ];
        $callbacks['payment_type'] = [
            'field' => 'payment_type',    
            'func'  => function($data, $row_name) use ($payment_type_enums){
                $term_id = $data['term_id'];

                $payment_type = 'normal';
                if('customer' == get_term_meta_value($term_id, 'contract_budget_payment_type'))
                {
                    $payment_type = 'customer';
                    'behalf' == get_term_meta_value($term_id, 'contract_budget_customer_payment_type') AND $payment_type = 'behalf';
                }

                $data['payment_type'] = $payment_type_enums[$payment_type] ?? '--';

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['contract_value'] = [
            'field' => 'contract_value',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['contract_value'] = (int)get_term_meta_value($term_id, 'contract_value');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['start_service_time'] = [
            'field' => 'start_service_time',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['start_service_time'] = '--';

                $start_service_time = get_term_meta_value($term_id, 'start_service_time');
                if(empty($start_service_time)) return $data;

                $data['start_service_time_raw'] = $start_service_time;
                $data['start_service_time'] = my_date($start_service_time, 'd/m/Y');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['end_service_time'] = [
            'field' => 'end_service_time',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['end_service_time'] = '--';

                $end_service_time = get_term_meta_value($term_id, 'end_service_time');
                if(empty($end_service_time)) return $data;

                $data['end_service_time_raw'] = $end_service_time;
                $data['end_service_time'] = my_date($end_service_time, 'd/m/Y');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['contract_daterange'] = [
            'field' => 'contract_daterange',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $contract_begin        = get_term_meta_value($term_id, 'contract_begin');
                $contract_end          = get_term_meta_value($term_id, 'contract_end');
                $contract_daterange    = implode(' - ', array_map(function($x){ return my_date($x, 'd/m/Y'); },[$contract_begin, $contract_end]));

                $data['contract_daterange'] = $contract_daterange;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['adaccount_id'] = [
            'field' => 'adaccount_id',    
            'func'  => function($data, $row_name){
                $data['adaccount_id'] = $data['adaccountSlug'] ?? '--';

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['adaccount_name'] = [
            'field' => 'adaccount_name',    
            'func'  => function($data, $row_name){
                $data['adaccount_name'] = $data['adaccountName'];

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['start_date'] = [
            'field' => 'start_date',    
            'func'  => function($data, $row_name){
                $data['start_date'] = my_date($data['segmentStartDate'], 'd/m/Y');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['end_date'] = [
            'field' => 'end_date',
            'func'  => function($data, $row_name){

                if(empty($data['segmentEndDate'])) return $data;
                $data['end_date'] = my_date($data['segmentEndDate'], 'd/m/Y');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['term_status'] = [
            'field' => 'term_status',    
            'func'  => function($data, $row_name){
                $data['term_status'] = $this->config->item($data['term_status'], 'contract_status');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['staff_business'] = [
            'field' => 'staff_business',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['staff_business'] = [];
                
                $staff_business 		= get_term_meta_value($term_id, 'staff_business');
                if(empty($staff_business)) return $data;
                
                $staff_business = $this->admin_m->get_field_by_id($staff_business);
                $staff_business = [
                    'user_id' => $staff_business['user_id'],
                    'display_name' => $staff_business['display_name'],
                    'user_avatar' => $staff_business['user_avatar'],
                    'user_email' => $staff_business['user_email'],
                ];
                $data['staff_business'] = $staff_business;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['department'] = [
            'field' => 'department',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['department'] = '--';
                
                $staff_business 		= get_term_meta_value($term_id, 'staff_business');
                if(empty($staff_business)) return $data;
                
                $departments = $this->term_users_m->get_user_terms($staff_business, $this->department_m->term_type);
                $departments and $data['department'] = implode(', ', array_column($departments, 'term_name'));

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['result_updated_on'] = [
            'field' => 'result_updated_on',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['result_updated_on'] = '--';

                $result_updated_on = get_term_meta_value($term_id, 'result_updated_on');
                if(empty($result_updated_on)) return $data;

                $data['result_updated_on_raw'] = $result_updated_on;
                $data['result_updated_on'] = my_date($result_updated_on, 'd/m/Y');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['remain_pending_days'] = [
            'field' => 'remain_pending_days',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['remain_pending_days'] = '--';

                $result_updated_on = get_term_meta_value($term_id, 'result_updated_on');
                if(empty($result_updated_on)) return $data;

                $data['remain_pending_days'] = round((time() - (int)$result_updated_on) / (60 * 60 * 24), 0);

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['actual_budget'] = [
            'field' => 'actual_budget',
            'func'  => function($data, $row_name) {
                $term_id = $data['term_id'];

                $data['actual_budget'] = (int) get_term_meta_value($term_id, 'actual_budget');

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['spend'] = [
            'field' => 'spend',
            'func'  => function($data, $row_name) {
                $data['spend'] = (int) $data['spend'];

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['discount_amount'] = [
            'field' => 'discount_amount',
            'func'  => function($data, $row_name) {
                $term_id = $data['term_id'];

                $contract_m = (new tiktokads_m())->set_contract($term_id);
                $discount_amount = $contract_m->get_behaviour_m()->calc_disacount_amount();

                $data['discount_amount'] = (int) $discount_amount;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['service_fee'] = [
            'field' => 'service_fee',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $contract_budget = (int) get_term_meta_value($term_id, 'contract_budget');
                $service_fee    = (int) get_term_meta_value($term_id, 'service_fee');
                $service_fee_rate = (float) div($service_fee, $contract_budget);

                $spend = max((int) $data['spend'], 0);

                $data['service_fee'] = $spend * $service_fee_rate;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['service_fee_rate'] = [
            'field' => 'service_fee_rate',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];
                
                $contract_budget = (int) get_term_meta_value($term_id, 'contract_budget');
                $service_fee    = (int) get_term_meta_value($term_id, 'service_fee');

                $data['service_fee_rate'] = div($service_fee, $contract_budget) * 100;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['service_fee_rate_actual'] = [
            'field' => 'service_fee_rate_actual',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];
                
                $contract_m = (new tiktokads_m())->set_contract($term_id);

                $budget         = $contract_m->get_behaviour_m()->calc_budget();
                $service_fee    = (int) get_term_meta_value($term_id, 'service_fee');
                $service_fee_rate = (float) div($service_fee, $budget);

                $discount_amount = $contract_m->get_behaviour_m()->calc_disacount_amount();
                $service_fee_rate_actual = div(max($service_fee - $discount_amount, 0), $budget);
                $data['service_fee_rate_actual'] = $service_fee_rate_actual * 100;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['remain_budget'] = [
            'field' => 'remain_budget',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];
                
                $actual_result  = (int) get_term_meta_value($term_id, 'actual_result');
                $actual_budget  = (int) get_term_meta_value($term_id, 'actual_budget');
                $data['remain_budget']  = $actual_budget - $actual_result;

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['staff_business'] = [
            'field' => 'staff_business',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['staff_business'] = [];
                $data['staff_business_raw'] = '--';
                
                $staff_business 		= get_term_meta_value($term_id, 'staff_business');
                if(empty($staff_business)) return $data;
                
                $staff_business = $this->admin_m->get_field_by_id($staff_business);
                
                $staff_business = [
                    'user_id' => $staff_business['user_id'],
                    'display_name' => $staff_business['display_name'],
                    'user_avatar' => $staff_business['user_avatar'],
                    'user_email' => $staff_business['user_email'],
                ];
            
                $data['staff_business'] = $staff_business;
                $data['staff_business_raw'] = $staff_business['display_name'];

                return $data;
            },
            'row_data' => FALSE,
        ];

        $callbacks['staff_business_email'] = [
            'field' => 'staff_business_email',    
            'func'  => function($data, $row_name){
                $term_id = $data['term_id'];

                $data['staff_business_email'] = [];
                $data['staff_business_email_raw'] = '--';
                
                $staff_business_email 		= get_term_meta_value($term_id, 'staff_business');
                if(empty($staff_business_email)) return $data;
                
                $staff_business_email = $this->admin_m->get_field_by_id($staff_business_email);
                
                $staff_business_email = [
                    'user_id' => $staff_business_email['user_id'],
                    'display_name' => $staff_business_email['display_name'],
                    'user_avatar' => $staff_business_email['user_avatar'],
                    'user_email' => $staff_business_email['user_email'],
                ];
            
                $data['staff_business_email'] = $staff_business_email;
                $data['staff_business_email_raw'] = $staff_business_email['user_email'];

                return $data;
            },
            'row_data' => FALSE,
        ];

        $this->load->model('tiktokads/tiktokads_kpi_m');
        $callbacks['tech'] = [
            'field' => 'tech',    
            'func'  => function($data, $row_name){
                $term_id 		= $data['term_id'];

                $data['tech'] = [];
                $data['tech_raw'] = '--';

			    $kpis = $this->tiktokads_kpi_m->get_kpis($term_id, 'users', 'tech');
			    if(empty($kpis)) return $data;

                $user_ids = array();
			    foreach ($kpis as $uids)
                {
                    if( ! is_array($uids)) continue;

                    foreach ($uids as $i => $val)
                    {
                        $user_ids[$i] = $i;
                    }
                }

                if(empty($user_ids)) return $data;

                $users = $this->admin_m
                    ->select('user_id, user_email, display_name, user_avatar, user.role_id, role_name')
                    ->join('role', 'role.role_id = user.role_id')
                    ->where_in('user_id', $user_ids)
                    ->as_array()
                    ->get_all();
                if(empty($users)) return $data;

                $display_names = array_column($users, 'display_name');
                $display_names = implode(', ', $display_names);
                $data['tech_raw'] = $display_names;

			    $data['tech'] = array_reduce($users, function($result, $_user){
                    $user = [
                        'user_id' => $_user['user_id'],
                        'user_email' => $_user['user_email'],
                        'display_name' => $_user['display_name'],
                        'user_avatar' => $_user['user_avatar'],
                        'role_name' => $_user['role_name'] ?? NULL,
                    ];
                    $result[] = $user;
			    	
                    return $result;
			    }, []);

                return $data;
            },
            'row_data' => FALSE,
        ];

        $selected_callbacks = [];
        if(empty($args)) $selected_callbacks = $callbacks;

        foreach($args AS $callback_name){
            if(!is_string($callback_name)) continue;
            if(!isset($callbacks[$callback_name])) continue;

            $selected_callbacks[$callback_name] = $callbacks[$callback_name];
        }

        return $selected_callbacks;
    }
}
/* End of file DataReport.php */
/* Location: ./application/modules/tiktokads/controllers/api_v2/DataReport.php */