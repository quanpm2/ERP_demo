<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

        if(ENVIRONMENT === 'production')
        {
            redirect(module_url(),'refresh');
        }

		$models = array(
			'tiktokads/tiktokads_m',
			'tiktokads/tiktokads_report_m',
		);

		$this->load->model($models);
	}
	
	/**
	 * send_activation_email
	 *
	 * @param  mixed $term_id
	 * @return void
	 */
	public function send_activation_email($term_id)
	{
		$status = $this->tiktokads_report_m->send_activation_mail2customer($term_id);
        dd($status);
	}
    
    /**
     * send_finished_service_email
     *
     * @param  mixed $term_id
     * @return void
     */
    public function send_finished_service_email($term_id)
	{
		$status = $this->tiktokads_report_m->send_finished_service($term_id);
        dd($status);
	}

    /**
     * sync_all_amount
     *
     * @param  mixed $term_id
     * @return void
     */
    public function sync_all_amount($term_id)
	{
        try
        {
            $contract = (new contract_m())->set_contract($term_id);
            $behaviour_m = $contract->get_behaviour_m();
            $behaviour_m->get_the_progress(FALSE, FALSE, TRUE);
            $behaviour_m->sync_all_amount();
        }
        catch (Exception $e)
        {
            return true;
        }
		
        dd(1);
	}

    /**
     * sync_all_progress
     *
     * @param  mixed $term_id
     * @return void
     */
    public function sync_all_progress($term_id)
	{
        try
        {
            $contract = (new contract_m())->set_contract($term_id);
            $behaviour_m = $contract->get_behaviour_m();
            $behaviour_m->sync_all_progress(FALSE, FALSE, TRUE);
        }
        catch (Exception $e)
        {
            return true;
        }
		
        dd(1);
	}

        
    /**
     * setTechnicianId
     *
     * @param  mixed $term_id
     * @return void
     */
    public function set_technician_id($term_id)
    {
        try
        {
            $contract = (new tiktokads_m())->set_contract($term_id);
            $behaviour_m = $contract->get_behaviour_m();
            $behaviour_m->setTechnicianId();
        }
        catch (Exception $e)
        {
            dd($e->getMessage());
            return true;
        }
		
        dd(1);
    }
}
/* End of file Test.php */
/* Location: ./application/modules/tiktokads/controllers/Test.php */