<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiktokads extends Admin_Controller 
{
	private $term;
	private $term_type = 'tiktok-ads';
	public  $model 	   = 'tiktokads_contract_m';

	public function __construct(){

		parent::__construct();

		$models = [
			'history_m',
			'term_users_m',
			'staffs/sale_m',
			'customer/customer_m',
			'contract/contract_m',
			'contract/contract_wizard_m',
			'contract/invoice_m',
			'tiktokads/tiktokads_kpi_m',
			'tiktokads/tiktokads_contract_m',
			'tiktokads/tiktokads_m',
			'tiktokads/tiktokads_report_m',
			'tiktokads/adaccount_m',
			'staffs/staffs_m'
		];

		$this->load->model($models);
		

		$this->load->config('contract/contract');
		$this->load->config('staffs/group');
		$this->load->config('contract/invoice');
		$this->load->config('tiktokads/tiktokads');

		// kiểm tra tiktokads có tồn tại và user có quyền truy cập ?
		$this->init_website();
	}


	/**
	 * Init|auth|prepare data before access admin
	 */
	private function init_website()
	{
		$term_id = $this->uri->segment(4);
		$method  = $this->uri->segment(3);
		$is_allowed_method = (!in_array($method, array('index','done')));
		if(!$is_allowed_method || empty($term_id)) return FALSE;

        $term = $this->tiktokads_m->select('term_id,term_name,term_parent,term_slug,term_type,term_status')->set_term_type()->get($term_id);		
		if(empty($term))
		{
			$this->messages->error('Hợp đồng không tồn tại hoặc không có quyền truy cập hợp đồng này !');
			redirect(module_url(),'refresh');	
		}
		
		if(empty($term) OR !$this->is_assigned($term_id)) 
		{
			$this->messages->error('Truy cập #'.$term_id.' không hợp lệ !!!');
			redirect(module_url(),'refresh');
		}

		$this->template->title->set(strtoupper(' '.$term->term_name));
		$this->website_id = $this->data['term_id'] = $term_id;
		$this->data['term'] = $this->term = $term;
	}


	/**
	 * Render "Publish" contract datatable
	 */
	public function index()
	{
		restrict('tiktokads.index.access');

        $this->template->is_box_open->set(1);
        $this->template->title->set('Tổng quan dịch vụ Tiktok ADS');
        $this->template->javascript->add(base_url("dist/vTiktokadsAdminIndexPage.js"));
        $this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
        $this->template->javascript->add('plugins/daterangepicker/moment.min.js');
        $this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
        $this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
        $this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
        $this->template->publish();
	}

	/**
	 * The page mapping contract with staffs
	 */
	public function staffs()
	{
		if( ! has_permission('tiktokads.staffs.access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

		$this->template->is_box_open->set(1);
		$this->template->title->set('Bảng phân công phụ trách thực hiện');
		$this->template->javascript->add(base_url("dist/vTiktokConfigurationAssignedKpi.js"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
        $this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
        $this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));

		$this->template->publish();
	}

	public function overview($term_id = 0)
	{
        if(FALSE === $this->tiktokads_m->set_contract($term_id))
		{
			$this->messages->error('Hợp đồng không tồn tại');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === $this->tiktokads_m->can('tiktokads.overview.access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === is_service_start($term_id))
		{
			$this->messages->error('Hợp đồng chưa được kích hoạt');
			redirect(admin_url(), 'refresh');
		}

		$this->template->is_box_open->set(1);
		$this->template->title->set( get_term_meta_value($term_id, 'contract_code') . ' | Dịch vụ Tiktok');
		$this->template->javascript->add(base_url("dist/vTiktokadsAdminViewPage.js"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));

		$this->template->javascript->add('https://code.highcharts.com/highcharts.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/variable-pie.js');
		$this->template->javascript->add('https://code.highcharts.com/modules/exporting.js');
		$this->template->javascript->add('https://www.gstatic.com/charts/loader.js');
		$this->template->footer->prepend('<script type="text/javascript">var contractId = '.$term_id.'</script>');
		$this->template->publish();
	}

	public function kpi($term_id = 0, $delete_id= 0)
	{
        if(FALSE === $this->tiktokads_m->set_contract($term_id))
		{
			$this->messages->error('Hợp đồng không tồn tại');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === $this->tiktokads_m->can('tiktokads.Kpi.access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === is_service_start($term_id))
		{
			$this->messages->error('Hợp đồng chưa được kích hoạt');
			redirect(admin_url(), 'refresh');
		}

        $this->config->load('tiktokads/group');
        $this->template->title->prepend('KPI');

        parent::render($this->data);
	}

	protected function submit_kpi($term_id = 0,$delete_id=0)
	{
		if($delete_id > 0)
		{
			$this->_delete_kpi($term_id,$delete_id);
			redirect(module_url('kpi/'.$term_id.'/'),'refresh');
		}

		restrict('tiktokads.kpi.add');

		$post = $this->input->post(NULL, TRUE);
		if(empty($post)) return FALSE;

		$insert = array();
		$insert['kpi_datetime'] = $this->input->post('target_date', TRUE);
		$insert['user_id'] 		= $this->input->post('user_id', TRUE);
		$insert['kpi_value'] 	= (int)$this->input->post('target_post', TRUE);

		$kpi_type 				= $this->input->post('target_type', TRUE);

		if($insert['kpi_datetime'] < $this->mdate->startOfMonth())
		{
			$this->messages->error('Cập nhật không thành công do tháng cập nhật nhỏ hơn tháng hiện tại');
			redirect(current_url(),'refresh');
		}

		$this->tiktokads_kpi_m->update_kpi_value($term_id, $kpi_type,$insert['kpi_value'], $insert['kpi_datetime'], $insert['user_id']);

		# Update relation of term's user in term_users table
		$this->term_users_m->set_relations_by_term($term_id,[$post['user_id']], 'admin');

		/* Update Technician to Contract */
        $this->tiktokads_m->set_contract($term_id)->get_behaviour_m()->setTechnicianId();
		
		$this->messages->success('Cập nhật thành công');

		redirect(current_url(),'refresh');
	}

	/**
	 * Delete the KPI record in googleads_kpi_m
	 * @param  integer	$term_id	
	 * @param  integer	$delete_id		
	 * @return boolean
	 */
	private function _delete_kpi($term_id = 0, $delete_id = 0)
	{
		restrict('tiktokads.Kpi.Delete');
		if(FALSE === $this->tiktokads_m->has_permission($term_id, 'tiktokads.Kpi.Delete'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			return FALSE;
		}

		$kpi = $this->tiktokads_kpi_m->get($delete_id);

		if( ! $kpi)
		{
			$this->messages->error('Xóa không thành công, dữ liệu không tồn tại!');
			return FALSE;
		}

		// Xóa kpi thành công
		$this->tiktokads_kpi_m->delete_cache($delete_id);
		$this->tiktokads_kpi_m->delete($delete_id);

		$this->tiktokads_m->set_contract($term_id)->get_behaviour_m()->setTechnicianId();

        /* Update Technician to Contract */

		$this->messages->success('Xóa thành công');

		// Nếu KPI đang dành cho bộ phận khác kỹ thuật thì kết thúc quá trình xử lý
		if($kpi->kpi_type != 'tech') return TRUE;

		$sale_role_ids 	= $this->option_m->get_value('group_sales_ids', TRUE) ?: [];
		$role_id 		= $this->admin_m->get_field_by_id($kpi->user_id, 'role_id');

		// NẾU KPI CÓ VAI TRÒ NVKD THÌ KHÔNG CẦN XỬ LÝ TIẾP
		if(empty($sale_role_ids) || in_array($role_id, $sale_role_ids)) return TRUE;

		// TRƯỜNG HỢP KPI KHÁC CỦA USER VẪN CÒN TỒN TẠI
		$kpis = $this->tiktokads_kpi_m->get_many_by(['term_id'=>$term_id,'user_id'=>$kpi->user_id]);
		if( ! empty($kpis)) return TRUE;

		// NẾU USER KHÔNG CÒN BẤT KỲ LIÊN HỆ NÀO VỚI HỢP ĐỒNG => XÓA TERM_USERS
		$this->term_users_m->delete_term_users($term_id, [$kpi->user_id]);
		return TRUE;
	}


	public function setting($term_id = 0)
	{
        if(FALSE === $this->tiktokads_m->set_contract($term_id))
		{
			$this->messages->error('Hợp đồng không tồn tại');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === $this->tiktokads_m->can('tiktokads.setting.access'))
		{
			$this->messages->error('Quyền truy cập không hợp lệ');
			redirect(admin_url(), 'refresh');
		}

        if(FALSE === is_service_start($term_id))
		{
			$this->messages->error('Hợp đồng chưa được kích hoạt');
			redirect(admin_url(), 'refresh');
		}

		$this->submit($term_id);

		$this->template->title->prepend('Cấu hình');
		
		$data = $this->data;
		$data['term'] = $this->term;
		$data['term_id'] = $term_id;
		$this->template->content_box_class->set(time());
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
        $this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		parent::render($data);
	}

	protected function submit($term_id = 0)
	{
		$post = $this->input->post();
		if(empty($post)) return FALSE;

		$term = $this->term_m->get($term_id);
		if(empty($term)) 
		{
			$this->messages->error('Dịch vụ không tồn tại hoặc không có quyền truy cập');
			redirect(module_url("overview/{$term->term_id}"), 'refresh');
		}

        $manager_tt_ads_role_id = $this->option_m->get_value('manager_tt_ads_role_id', TRUE);
        $manager_tt_ads_role_id[] = 1;
		if( in_array($term->term_status, [ 'ending', 'liquidation' ]) && ! in_array( $this->admin_m->role_id, $manager_tt_ads_role_id))
		{
			$this->messages->error('Hợp đồng đã kết thúc và quyền truy cập không phù hợp để thực hiện tác vụ này');
			redirect(current_url(), 'refresh');
		}

		// Update Service infomation
		if($this->input->post('submit') !== NULL)
		{
			restrict('tiktokads.setting.update',current_url());

			if(empty($post['edit']['meta'])) 
			{
				$this->messages->error('Dữ liệu input không hợp lệ');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$metadata  = $post['edit']['meta'];

			/* Update curators box */
			if(!empty($metadata['curator_name']))
			{
				$i = 0;
				$curators = array();
				$count = count($metadata['curator_name']);

				for ($i; $i < $count; $i++)
				{
					$curators[] = array(
						'name' => $metadata['curator_name'][$i],
						'phone' => $metadata['curator_phone'][$i],
						'email' => $metadata['curator_email'][$i]
						);
				}

				$contract_curators = serialize($curators);

				$this->history_m->table = $this->term_m->_table;
				$this->history_m->object_id = $term_id;
				$this->history_m
				->set_original_data(['contract_curators'=>get_term_meta_value($term_id,'contract_curators')])
				->get_dirty(['contract_curators'=>$contract_curators])
				->save();

				update_term_meta($term_id,'contract_curators',$contract_curators);
				$this->messages->success('Cập nhật thành công !');

				unset($metadata['curator_name'],$metadata['curator_phone'],$metadata['curator_email']);
			}

			if( ! empty($metadata['advertise_start_time']))
			{
				$metadata['advertise_start_time'] = $this->mdate->startOfDay($metadata['advertise_start_time']);
			}

			if( ! empty($metadata['advertise_end_time']))
			{
				$metadata['advertise_end_time'] = $this->mdate->startOfDay($metadata['advertise_end_time']);
			}

			if( ! empty($metadata['adcampaign_ids']))
			{
				$metadata['adcampaign_ids'] = serialize($metadata['adcampaign_ids']);
			}

			if($metadata)
			{
				foreach ($metadata as $meta_key => $meta_value)
				{
					update_term_meta($term_id,$meta_key,$meta_value);
				}

				$this->messages->success('Cập nhật thành công');	
			}
			
			redirect(module_url("setting/{$term_id}"),'refresh');
		}


		// Start service command
		if($this->input->post('start_service') !== NULL)
		{
			if( ! $this->tiktokads_m->has_permission($term_id,'tiktokads.start_service.update'))
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$advertise_start_time = $post['edit']['meta']['advertise_start_time'] ?: '';
			$result  = $this->tiktokads_contract_m->proc_service($term,$advertise_start_time);
			if( ! $result)
			{
				$this->messages->error('Có lỗi xảy ra! Vui lòng kiểm tra cấu hình cài đặt hoặc thêm nhân viên kỹ thuật') ;
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			/* Phân tích hợp đồng ký mới | tái ký */
			$this->load->model('contract/base_contract_m');
			$this->base_contract_m->detect_first_contract($term_id);

			/* Gửi SMS thông báo kích hoạt hợp đồng đến khách hàng */
			$this->load->model('contract/contract_report_m');
			$this->contract_report_m->send_sms_activation_2customer($term_id);

			$this->messages->success('Đã gửi mail thành công');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}


		// End service command
		if($this->input->post('stop_service') !== NULL)
		{
			if( ! $this->tiktokads_m->has_permission($term_id, 'tiktokads.stop_service.update'))
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$advertise_end_time = $this->mdate->endOfDay($post['edit']['meta']['advertise_end_time']);
			$has_send = $this->tiktokads_contract_m->stop_service($term,$advertise_end_time);
			if( ! $has_send) 
			{	
				$this->messages->error('Xảy ra lỗi , vui lòng thử lại sau !');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$this->messages->success('Hệ thống đã gửi email thông báo đến khách hàng thành công !');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
	}

	protected function is_assigned($term_id = 0,$kpi_type = '')
	{
		// check user has manager role permission
		$class  = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		$result = $this->tiktokads_m->has_permission($term_id);
		return $result;
	}
}
/* End of file tiktokads.php */
/* Location: ./application/modules/tiktokads/controllers/tiktokads.php */