<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adinsight extends API_Controller {

	function __construct()
	{
		parent::__construct();

		$models = array(
			'tiktokads/tiktokads_m',
			'term_posts_m',
		);

		$this->load->model($models);
	}

    public function index($term_id = 0)
    {
        $defaults = array(
			'orderby'=>'start_date',
			'order'=>'ASC',
			'fields'=>'all',
			'where'=>'',
			'start_time' => $this->mdate->startOfDay(strtotime('2017/08/21')),
			'end_time' => $this->mdate->endOfDay(),
		);
		$args = wp_parse_args($defaults, $this->get());

        $start_time = $this->mdate->startOfDay($args['start_time']);
        $end_time = $this->mdate->endOfDay($args['end_time']);

        /**
		 * Truy xuất dữ liệu hợp đồng và xác thực thông tin
		 *
		 * @var        tiktokads_m
		 */
		$data = $this->tiktokads_m
            ->select("adaccount.term_name AS account_name")
            ->select("COALESCE(insights.start_date, balance_spend.start_date) as start_date")
            ->select("MAX(IF(insight_metadata.meta_key = 'account_currency', insight_metadata.meta_value, NULL)) AS account_currency")
            ->select("MAX(IF(insight_metadata.meta_key = 'clicks', insight_metadata.meta_value, 0)) AS clicks")
            ->select("MAX(IF(insight_metadata.meta_key = 'cpc', insight_metadata.meta_value, 0)) AS cpc")
            ->select("MAX(IF(insight_metadata.meta_key = 'cpm', insight_metadata.meta_value, 0)) AS cpm")
            ->select("MAX(IF(insight_metadata.meta_key = 'cpp', insight_metadata.meta_value, 0)) AS cpp")
            ->select("MAX(IF(insight_metadata.meta_key = 'ctr', insight_metadata.meta_value, 0)) AS ctr")
            ->select("MAX(IF(insight_metadata.meta_key = 'impressions', insight_metadata.meta_value, 0)) AS impressions")
            ->select("MAX(IF(insight_metadata.meta_key = 'reach', insight_metadata.meta_value, 0)) AS reach")
            ->select("COALESCE(SUM(IF(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, NULL)), balance_spend.post_content) AS spend")
            ->select("COALESCE(ads_segment.post_type, balance_spend.post_type) as segment_type")
            ->select("COALESCE(max(if(adaccount_metadata.meta_key = 'source', adaccount_metadata.meta_value, NULL)), balance_spend.comment_status) as insight_source")
            ->select("tp_contract_ads_segment.post_id")

            ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
            ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'LEFT')  
            ->join('term_posts AS tp_segment_adaccount', 'tp_segment_adaccount.post_id = ads_segment.post_id', 'LEFT')  
            ->join('term AS adaccount', 'tp_segment_adaccount.term_id = adaccount.term_id AND adaccount.term_type = "tiktok_adaccount"', 'LEFT')  
            ->join('termmeta AS adaccount_metadata', 'adaccount_metadata.term_id = adaccount.term_id AND meta_key = "source"', 'LEFT')  
            ->join('term_posts AS tp_adaccount_insights', 'tp_adaccount_insights.term_id = adaccount.term_id', 'LEFT')  
            ->join('posts AS insights', "tp_adaccount_insights.post_id = insights.post_id 
                AND (insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, '%Y-%m-%d 00:00:00')) 
                    AND insights.start_date <= IF(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP(), ads_segment.end_date))
                AND insights.post_type = 'insight_segment' AND insights.post_name = 'day'
                AND (insights.start_date >= {$start_time}
                    AND insights.start_date <= {$end_time})", 'LEFT')
            ->join('postmeta insight_metadata', 'insight_metadata.post_id = insights.post_id AND insight_metadata.meta_key IN ("account_currency", "clicks", "cpc", "cpm", "cpp", "ctr", "impressions", "reach", "spend")', 'LEFT')

            ->join('posts AS balance_spend', "
                balance_spend.post_id = tp_contract_ads_segment.post_id 
                AND balance_spend.post_type = 'balance_spend'
                AND balance_spend.start_date >= IF(balance_spend.comment_status = 'auto', 0, {$start_time})
                AND balance_spend.start_date <= IF(balance_spend.comment_status = 'auto', 0, {$end_time})
            ", 'LEFT')
            
            ->where('term.term_id', $term_id)
            ->where('(insights.post_id > 0 or balance_spend.post_id > 0)')
            ->group_by('term.term_id, tp_contract_ads_segment.post_id, adaccount.term_id, insights.post_id')
            ->order_by('start_date', 'DESC')
            
            ->as_array()
            ->get_all();
        
        if( ! $data)
        {
            $response['msg'] = 'Hợp đồng không tồn tại hoặc không hợp lệ !';
            return parent::render($response,500);
        }

        $response['data'] = $data;
        $this->default_response['auth'] = 1;
		return parent::render($response);
    }
}
/* End of file Adinsight.php */
/* Location: ./application/modules/tiktokads/controllers/Api/Adinsight.php */