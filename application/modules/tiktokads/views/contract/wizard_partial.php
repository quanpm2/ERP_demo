<?php
$this->template->javascript->add(base_url("dist/vTiktokadsConfigurationContractBox.js"));
?>

<div class="col-md-12" id="service_tab">
<?php 

$this->config->load('googleads/contract');

echo $this->admin_form->form_open();
echo form_hidden('edit[term_id]', $edit->term_id);

?>
<div id="vTiktokadsConfigurationContractBox">
    <v-tiktokads-configuration-contract-box term_id="<?php echo $edit->term_id;?>"></v-tiktokads-configuration-contract-box>
</div>

<?php

// BEGIN::Curators
echo $this->admin_form->box_open('Thông tin người nhận báo cáo');
$curators = @unserialize(get_term_meta_value($edit->term_id,'contract_curators'));

if(empty($curators))
{
    $curators = array();
    $curators[] = [
        'name'=>get_term_meta_value($edit->term_id,'representative_name'),
        'phone'=>get_term_meta_value($edit->term_id,'representative_phone'),
        'email'=>get_term_meta_value($edit->term_id,'representative_email')
    ];

    /* Load dữ liệu báo cáo tự hợp đồng cũ */
    $this->load->model('googleads/googleads_m');
    $old_terms = $this->googleads_m
    ->select('term_id')
    ->set_term_type()
    ->order_by('term_id','desc')
    ->where('term_parent', $edit->term_parent)
    ->limit(5)
    ->get_all();

    if( ! empty($old_terms))
    {
        foreach ($old_terms as $_term)
        {
            $_curators = get_term_meta_value($_term->term_id, 'googleads-curators');
            $_curators = is_serialized($_curators) ? unserialize($_curators) : [];
            if(empty($_curators)) continue;

            $_names = array_column($curators, 'name');
            $_email = array_column($curators, 'email');

            $_curators = array_filter($_curators, function($x) use($_names, $_email) {
                if( empty($x['name'])) return FALSE;
                if( in_array($x['name'], $_names) || in_array($x['email'], $_email)) return FALSE;
                return TRUE;
            });

            if(empty($_curators)) continue;

            $curators = wp_parse_args( $_curators, $curators);
        }
    }
}

$max_curator = $this->option_m->get_value('max_number_curators');
$max_curator = $max_curator ?: $this->config->item('max_input_curators','googleads');
for ($i=0; $i < $max_curator; $i++)
{ 
    $name = '';
    $phone = '';
    $email = '';
    if(!empty($curators[$i]))
    {
        $name = $curators[$i]['name'];
        $phone = $curators[$i]['phone'];
        $email = $curators[$i]['email'];
    }

    echo $this->admin_form->formGroup_begin();
    ?>
    <div class="col-xs-4" style="padding-left: 0;">
    <?php echo form_input(['name'=>'edit[meta][contract_curators][curator_name][]','class'=>'form-control','placeholder'=>'Người nhận báo cáo'],$name);?>
    </div>
    <div class="col-xs-4">
    <?php echo form_input(['name'=>'edit[meta][contract_curators][curator_email][]','class'=>'form-control','placeholder'=>'Email'],$email); ?>
    </div>
    <div class="col-xs-4">
    <?php echo form_input(['name'=>'edit[meta][contract_curators][curator_phone][]','class'=>'form-control','placeholder'=>'Số điện thoại'],$phone);?>
    </div>
    <?php
    echo $this->admin_form->formGroup_end();
}

echo $this->admin_form->box_close();
// End::Curators
// 
// GENERAL CONFIG BOX

$advertise_start_time = get_term_meta_value($edit->term_id,'advertise_start_time');
$advertise_start_date = $advertise_start_time ? my_date($advertise_start_time,'d-m-Y') : '';

$advertise_end_time = get_term_meta_value($edit->term_id,'advertise_end_time');
$advertise_end_date = $advertise_end_time ? my_date($advertise_end_time,'d-m-Y') : '';

echo $this->admin_form->submit('','confirm_step_service','confirm_step_service','',['style'=>'display:none;','id'=>'confirm_step_service']);
echo $this->admin_form->form_close();
// END GENERAL CONFIG BOX
?>
</div>

<script type="text/javascript">
$(function(){

	// Init datepicker
	$('.set-datepicker').datepicker({
		format: 'yyyy/mm/dd',
		todayHighlight: true,
		autoclose: true,	
	});

	/* INIT RADIO COMPONENT FOR BUDGET PAYMENT TYPE */
    $('#service_tab input[type="radio"][name="edit[meta][contract_budget_payment_type]"]')
    .iCheck({checkboxClass: 'icheckbox_flat-blue', radioClass: 'iradio_flat-blue' })
    .on('ifChecked', function(event){

        if(_.isEqual('normal', $(this).val()))
        {
            $('#service_tab div.contractBudgetCustomerPaymentTypeEl').hide();
            return;
        }
        
        $('#service_tab div.contractBudgetCustomerPaymentTypeEl').show();
    });

    /* INIT RADIO COMPONENT FOR BUDGET PAYMENT TYPE VIA WHICH OBJECT */
    $('#service_tab input[type="radio"][name="edit[meta][contract_budget_customer_payment_type]"]')
    .iCheck({checkboxClass: 'icheckbox_flat-blue', radioClass: 'iradio_flat-blue' });

    $('.select2').select2();
    $('.select2').css('width','100%'); 
});
</script>