<?php $this->load->view('tiktokads/contract/preview/header');?>
<p>Dựa trên:Nhu cầu của Bên A và khả năng cung cấp của Bên B về dịch vụ. Hai bên thống nhất ký kết hợp đồng kinh tế với các điều khoản cụ thể như sau: <br /><br /></p>

<strong>Điều 1: Điều khoản chung</strong><br />
Bên B nhận cung cấp cho Bên A dịch vụ quảng cáo trên mạng xã hội TikTok theo các nội dung sau: <br />
<ul>
    <li>Quảng cáo của Bên A sẽ xuất hiện tại các vị trí cho phép đặt quảng cáo của TikTok và theo tài khoản mà Bên A cung cấp thông tin. Trang web mà quảng cáo sẽ xuất hiện là social TikTok <a hreft="//www.TikTok.com">(www.TikTok.com)</a></li>
    <li>Bên B sẽ tư vấn và phối hợp với Bên A xây dựng các chiến dịch quảng cáo, hình ảnh và thông điệp để chiến dịch quảng cáo đạt hiệu quả cao nhất nhưng không chịu trách nhiệm thực hiện thiết kế hình ảnh quảng cáo. Trường hợp Bên A có nhu cầu cần thiết kế hình ảnh/ thông điệp quảng cáo thì Bên B sẽ tính phí thiết kế theo bảng giá dịch vụ quy định tại thời điểm yêu cầu.</li>
    <li>Thông tin của quảng cáo sẽ do Bên A cung cấp và nội dung quảng cáo phải đạt được sự thống nhất của hai bên trước khi thực hiện.</li>
    <li>Bên B sẽ đề xuất với Bên A những giải pháp để tăng cường hiệu quả của chiến dịch quảng cáo.</li>
    <li>Toàn bộ chiến dịch được thực hiện cho Bên A tại địa chỉ sau: <a href="<?php echo get_term_meta_value($term_id, 'link_fanpage_tiktok') ;?>"><?php echo get_term_meta_value($term_id, 'link_fanpage_tiktok') ;?></a></li>
</ul>


<br/>
<p><b>Điều 2: Giá trị hợp đồng</b></p>
<p>Đơn vị tính giá: Việt Nam Đồng (VND).</p>
<p>Thanh toán bằng tiền đồng Việt Nam.</p>
<?php

$total = $data_service['budget'];

$text_contract_period  = $contract_days . ' ngày (từ ' . date('d/m/Y', $contract_begin) . ' đến ' . date('d/m/Y', $contract_end) . ')';

$this->table->clear();
$this->table->set_template( ['table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%" id="tiktok-ads">'] );
$this->table->set_heading( array('Hạng mục', 'Ngân sách', 'Thời gian', 'Thành tiền'))
->add_row('Quảng cáo TikTok ', array('data' => currency_numberformat($data_service['budget']), 'align' => 'center') , $text_contract_period, array('data' => currency_numberformat($data_service['budget']), 'align' => 'center'));

if ($fct)
{
    $fct_amount = div($data_service['budget'], 0.95)*div($fct, 100); 
    $total+=$fct_amount;

    $this->table->add_row(['data'=>'Thuế nhà thầu','colspan'=>3],['data'=>currency_numberformat($fct_amount),'align'=>'center']);
}

$total+= $data_service['service_fee'];
$this->table->add_row(array('data' => 'Phí dịch vụ', 'colspan' => 3), array('data' => currency_numberformat($data_service['service_fee']), 'align' => 'center')) ;

$discount_percent             = get_term_meta_value($term_id, 'tiktokads_discount_percent') ;
if( !empty($discount_percent) || $discount_percent > 0 ) 
{ 
    $data_service['budget']              = get_term_meta_value($term_id, 'contract_budget') ;
    $total_service_fee                  = (! get_term_meta_value($term_id, 'service_fee') ) ? 0 : get_term_meta_value($term_id, 'service_fee') ;

    $discount_amount = ($data_service['budget']+$data_service['service_fee']) *  div($discount_percent, 100);
    $total-= $discount_amount;
    $this->table->add_row(array('data' => 'Giảm giá ' . $discount_percent . '%', 'colspan' => 3) , array('data' => currency_numberformat($discount_amount) , 'align' => 'center')) ;
}

$this->table->add_row(array('data' => 'Tổng cộng', 'colspan' => 3) , array('data' => currency_numberformat($total) , 'align' => 'center')) ;

if(!empty($vat))
{
    $tax   = $total * ($vat/100);
    $total = cal_vat($total,$vat);
    $total = ceil($total);

    $this->table->add_row( array('data'=>'VAT ' . '(' . $vat . ' %)' ,'colspan' => 3),array('data'=> '<strong>'.$vat.'&#37; ('.currency_numberformat($tax).') </strong>', 'align' => 'center' ));    
}

$text_vat = ' chưa bao gồm VAT' ;
if(!empty($vat)) $text_vat = ' đã bao gồm VAT';
$this->table->add_row(array('data'=>'Tổng thanh toán' . $text_vat ,'colspan'=> 3), array('data'=>currency_numberformat($total),'align'=>'center'));

echo $this->table->generate();              
?>
<p><em>Bằng chữ: <b><i><?php echo ucfirst(mb_strtolower(convert_number_to_words($total)));?> đồng.</i></b></em><br/>
</div>  

<?php if (!empty($fct)): ?>    
<p><em><b><u>Ghi chú : </u></b>Thuế nhà thầu nước ngoài = Doanh thu tính thuế Thu nhập doanh nghiệp (Ngân sách quảng cáo/95%)*Thuế suất thuế nhà thầu(5%))</em></p>
<?php endif ?>

<br/>
<p><b>Điều 3: Hỗ trợ miễn phí 1 bài viết quảng cáo TikTok với ngân sách quảng cáo trên 20 triệu (không bao gồm phí dịch vụ)</b></p>
<ul>    
    <li>Nội dung bài viết: không có các từ ngữ vi phạm chính sách TikTok.</li>
    <li>Số từ trong bài viết dưới 400 từ</li>
    <li>Thời gian viết bài từ 24 giờ – 48 giờ kể từ khi Bên A gửi lại cho Bên B mẫu thông tin viết bài do Bên B đã cung cấp cho Bên A</li>
    <li>Bài viết chính thức: là bài viết thuộc một trong hai trường hợp sau:</li>
    <ul>    
        <li>Bài viết demo trở thành bài viết chính thức khi Bên A không yêu cầu Bên B chỉnh sửa trong thời hạn hai (02) ngày kể từ ngày nhận được bàì viết demo</li>
        <li>Bài viết sau khi Bên B chỉnh sửa lần cuối cùng (không quá 2 lần)</li>
    </ul>
</ul>


<br/>

<?php if( ! empty($bank_info)) :?>
<p><strong>Điều 4: Phương thức thanh toán</strong><br />Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin tài khoản do Bên B cung cấp, cụ thể:</p>
<ul style="padding-left:0;list-style:none">
<?php foreach ($bank_info as $label => $text) :?>
    <?php if (is_array($text)) : ?>
        <?php foreach ($text as $key => $value) :?>
            <li>- <?php echo $key;?>: <?php echo $value;?></li>
        <?php endforeach;?>
    <?php continue; endif;?>
    <li>- <?php echo $label;?>: <?php echo $text;?></li>
<?php endforeach;?>
</ul>
<p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
<?php endif; ?>

<p>- Lội trình thanh toán:</p>
<p>Ngay sau khi hợp đồng được ký BÊN A thanh toán cho BÊN B 100% chi phí quảng cáo trực tuyến</p>



<?php if ($vat > 0): ?>
<p>Bên B xuất hóa đơn giá trị gia tăng cho Bên A theo thông tin sau:</p>
<ul>
    <li><u>Tên công ty</u>: <?php echo $customer->display_name ?? '';?></li>
    <li><u>Địa chỉ</u>: <?php echo $data_customer['Địa chỉ'] ?? '';?></li>
    <li><u>Mã số thuế</u>: <?php echo $data_customer['Mã số thuế'] ?? '';?></li>
</ul>
<p>Và gửi về địa chỉ email: <?php echo $customer->representative['email'] ?? '';?></p>
<?php endif ?>

<br/>

<p><strong>Điều 5: Thời gian xuất hiện quảng cáo và dừng chiến dịch</strong></p>
<ul>    
    <li>quảng cáo sẽ xuất hiện trên hệ thống quảng cáo của TikTok dự kiến từ ngày <?php echo my_date($contract_begin,'d/m/Y');?> đến ngày <?php echo my_date($contract_end,'d/m/Y');?> hoặc đến khi hết ngân sách.</li>
    <li>Kể từ khi hợp đồng được ký kết, Bên A thanh toán tiền cho Bên B theo quy định của hợp đồng này và Bên A duyệt đề xuất triển khai của Bên B. Bên B có trách nhiệm gửi đề xuất đến Bên A trong vòng 03 ngày làm việc kể từ ngày hợp đồng được ký kết.</li>
    <li>Trong thời gian thực hiện chiến dịch quảng cáo, nếu Bên A muốn dừng quảng cáo cho một Số ngày nhất định, Bên A cần thông báo trước cho Bên B ít nhất 03 ngày bằng văn bản hoặc bằng email. Trong trường hợp đó, Số ngày hiện quảng cáo trên TikTok của Bên A sẽ được tăng thêm đúng bằng Số ngày tạm dừng quảng cáo nhưng thời gian dừng không quá 1 tháng (30 ngày).</li>
    <li>Trong trường hợp Bên A chậm thanh toán cho Bên B theo quy định tại điều 3 của hợp đồng thì Bên B sẽ tạm dừng chiến dịch quảng cáo tương ứng với số ngày chậm thanh toán của Bên A. Bên B có trách nhiệm đưa quảng cáo xuất hiện trở lại 01 (một) ngày sau khi nhận được đầy đủ phần thanh toán bị chậm của Bên A.</li>
    <li>Chiến dịch quảng cáo sẽ hoàn tất khi Bên B gửi Thông báo kết thúc dịch vụ qua email cho Bên A. Sau 2 ngày kể từ khi nhận được email từ Bên B, nếu Bên A không có phản hồi khác, chiến dịch quảng cáo được xem là kết thúc.</li>
</ul>

<br>
<strong>Điều 6: Chấm dứt hợp đồng</strong><br />
<p>
    Trong trường hợp Bên A có thay đổi chiến dịch quảng cáo và muốn chấm dứt hợp đồng trước thời hạn thì Bên A phải gửi thông báo trước bằng văn bản cho Bên B trước 07 (bảy) ngày khi chính thức dừng quảng cáo và phải được Bên B chấp nhận về thời điểm dừng hợp đồng.
    <br/>
    Bên B sẽ không thực hiện hoàn trả cho Bên A giá trị phí dịch vụ Quản lý tài khoản trong trường hợp Bên A chấm dứt hợp đồng trước hạn.
</p>

<br/>
<strong>Điều 7: Sự kiện bất khả kháng</strong><br />
Trong trường hợp có những sự kiện bất khả kháng không lường trước được, trách nhiệm và thời hạn thực hiện hợp đồng của cả hai bên sẽ được xem xét, đàm phán và quyết định lại. Các sự kiện bất khả kháng bao gồm nhưng không giới hạn các rủi ro sau:</p>
<ul>
    <li>Sản phẩm hoặc tài khoản vi phạm chính sách quảng cáo TikTok</li>
    <li>Rủi ro do ngừng hoặc lỗi kĩ thuật từ dịch vụ TikTok cung cấp</li>
    <li>Rủi ro về đường truyền internet, cơ sở hạ tầng mạng quốc gia</li>
    <li>Thiên tai, chiến tranh, khủng bố, hoả hoạn, dịch bệnh...</li>
</ul>

<br/>
<p>
    <strong>Điều 8: Tranh chấp và phân xử</strong><br />
    Hai bên cam kết thực hiện những điều khoản trong hợp đồng. Nếu có vướng mắc, mỗi bên thông báo cho nhau để cùng bàn bạc giải quyết trên tinh thần hợp tác, thiện chí, vì lợi ích cả hai bên. Trong trường hợp không thể giải quyết được bất đồng, tranh chấp sẽ được giải quyết tại Tòa án có thẩm quyền tại TP Hồ Chí Minh.<br />
</p>

<br/>
<p><strong>Điều 9: Điều khoản khác</strong></p>
<p>
    Hợp đồng có hiệu lực kể từ ngày ký và được xem như tự động thanh lý khi Hai bên thực hiện đầy đủ tất cả các điều khoản nêu trong hợp đồng này. <br/>
    Mọi sửa đổi hoặc bổ sung cho Hợp đồng này sẽ chỉ có hiệu lực sau khi các đại diện của cả hai bên ký kết bằng văn bản tạo thành bộ phận hợp nhất của hợp đồng.<br/>
    Hợp đồng này được làm thành 02 bản có giá trị ngang nhau, Bên A giữ 01 bản, Bên B giữ 01 bản.
</p>

<?php $this->load->view('tiktokads/contract/preview/footer'); ?>