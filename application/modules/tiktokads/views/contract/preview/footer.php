

<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
    <tr>
        <td width="44%" valign="top" style="text-align:center">
            <p>
                <strong>BÊN A</strong><br />
                <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                <strong><?php echo mb_strtoupper($customer->display_name);?>  </strong>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
            <td rowspan="2"></td>

            <td width="46%" valign="top" style="text-align:center">
                <p><strong>BÊN B</strong><br />
                    <strong>THAY MẶT VÀ ĐẠI DIỆN CHO</strong><br />
                    <strong>CÔNG TY CỔ PHẦN QUẢNG CÁO CỔNG VIỆT NAM</strong>
                </p>
                <p><strong>&nbsp;</strong></p>
                <p><strong>&nbsp;</strong></p>
                <p>&nbsp;</p>
            </td>
    </tr>
    <tr>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_customer['Đại diện'] ;?></strong><br/>
                <?php echo $data_customer['Chức vụ'] ;?>
            </p>
        </td>
        <td valign="top" style="text-align:center">
            <p>
                <strong><?php echo $data_represent['Đại diện'] ?? ''; ?></strong><br />
                <?php echo $data_represent['Chức vụ']; ?>
            </p>
        </td>
    </tr>
</table>

<?php if(!empty($contract_budget_customer_payment_type) && 'behalf' == $contract_budget_customer_payment_type) $this->load->view('googleads/contract/preview/behaft_appendix');?>

<script type="text/javascript">

    (function() {
        var beforePrint = function() {
            console.log('Functionality to run before printing.');
        };
        var afterPrint = function() {
            console.log('Functionality to run after printing');
        };

        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');
            mediaQueryList.addListener(function(mql) {
                console.log(mql);
                if (mql.matches) {
                    beforePrint();
                } else {
                    afterPrint();
                }
            });
        }

        window.onbeforeprint = beforePrint;
        window.onafterprint = afterPrint;
    }());
</script>
</body>
</html>