<div class="col-md-12" id="review-partial">

    <h2 class="page-header">

        <small class="pull-right">Ngày tạo : <?php echo my_date(time(), 'd-m-Y'); ?></small>
    </h2>

    <div class="row">
        <div class="col-xs-6">
            <p class="lead">Thông tin khách hàng</p>
            <div class="table-responsive">
                <?php

                $representative_gender  = force_var(get_term_meta_value($edit->term_id, 'representative_gender'), 'Bà', 'Ông');
                $representative_name    = get_term_meta_value($edit->term_id, 'representative_name') ?: '';
                $display_name           = "{$representative_gender} {$representative_name}";
                $representative_email   = get_term_meta_value($edit->term_id, 'representative_email');
                $representative_address = get_term_meta_value($edit->term_id, 'representative_address');
                $representative_phone   = get_term_meta_value($edit->term_id, 'representative_phone');
                $contract_daterange     = get_term_meta_value($edit->term_id, 'contract_daterange');

                echo $this->table->clear()
                    ->add_row('Người đại diện', $display_name ?: 'Chưa cập nhật')
                    ->add_row('Email', $representative_email ?: 'Chưa cập nhật')
                    ->add_row('Địa chỉ', $representative_address ?: 'Chưa cập nhật')
                    ->add_row('Số điện thoại', $representative_phone ?: 'Chưa cập nhật')
                    ->add_row('Chức vụ', $edit->extra['representative_position'] ?? 'Chưa cập nhật')
                    ->add_row('Mã Số thuế', $edit->extra['customer_tax'] ?? 'Chưa cập nhật')
                    ->add_row('Thời gian thực hiện', $contract_daterange ?: 'Chưa cập nhật')
                    ->generate();
                ?>
            </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
            <p class="lead">Thông số dịch vụ</p>
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                <?php

                $advertise_start_time = get_term_meta_value($edit->term_id, 'advertise_start_time');
                $advertise_end_time = get_term_meta_value($edit->term_id, 'advertise_end_time');

                $advertise_start_time = empty($advertise_start_time) ? $advertise_start_time : my_date($advertise_start_time, 'd-m-Y');
                $advertise_end_time = empty($advertise_end_time) ? $advertise_end_time : my_date($advertise_end_time, 'd-m-Y');

                echo
                'Ngày chạy dịch vụ : ' . force_var($advertise_start_time, 'Chưa cập nhật') . '<br/>',
                'Ngày kết thúc dịch vụ : ' . force_var($advertise_end_time, 'Chưa cập nhật') . '<br/>';
                ?>
            </p>
            <p class="lead">Cam kết hợp đồng</p>

            <div class="table-responsive">
                <?php
                $this->table->clear()->add_row(array('Dịch vụ thực hiện', 'Quảng cáo Tik-Tok ads'));

                $total = 0;

                $this->config->load('tiktokads/contract');
                $budget = (float) get_term_meta_value($edit->term_id, 'contract_budget');
                $fee_service = (float) get_term_meta_value($edit->term_id, 'service_fee');
                $total = $budget + $fee_service;

                $isAccountForRent   = (bool) get_term_meta_value($edit->term_id, 'isAccountForRent');
                $this->table->add_row('Hình thức quản lý', !$isAccountForRent ? 'Quản lý theo tài khoản' : 'Cho thuê tài khoản');

                $contract_budget_payment_type = get_term_meta_value($edit->term_id, 'contract_budget_payment_type');
                empty($contract_budget_payment_type) and $contract_budget_payment_type = $this->config->item('default', 'contract_budget_payment_types');
                $this->table->add_row('Phương thức thanh toán <u><i>( Ngân sách Quảng Cáo )</i>', $this->config->item('enums', 'contract_budget_payment_types')[$contract_budget_payment_type] ?? '');

                $service_fee_payment_type = get_term_meta_value($edit->term_id, 'service_fee_payment_type');
                $this->table->add_row('Phương thức thanh toán phí dịch vụ', $this->config->item($service_fee_payment_type, 'service_fee_payment_type') ?? '--');

                $this->table->add_row('Ngân sách chạy quảng cáo', currency_numberformat($budget));

                $contract_budget_payment_type = get_term_meta_value($edit->term_id, 'contract_budget_payment_type');
                if ('customer' != $contract_budget_payment_type) {
                    $service_provider_tax_rate = (float) get_term_meta_value($edit->term_id, 'service_provider_tax_rate');
                    $service_provider_tax = (float) get_term_meta_value($edit->term_id, 'service_provider_tax');
                    !empty($service_provider_tax) and $this->table->add_row('Thuế Tiktok thu (' . ($service_provider_tax_rate * 100) . ' %)', currency_numberformat($service_provider_tax));

                    $this->table->add_row('Phí dịch vụ', currency_numberformat($fee_service));

                    $original_service_fee_rate = (float) get_term_meta_value($edit->term_id, 'original_service_fee_rate');
                    $this->table->add_row('% Phí dịch vụ', currency_numberformat($original_service_fee_rate * 100, ' %'));
                }

                $promotions = get_term_meta_value($edit->term_id, 'promotions') ?: [];
                $promotions and $promotions = unserialize($promotions);

                if ($promotions) {
                    foreach ($promotions as $key => $value) {
                        $_name  = $value['name'];
                        $_value = $value['value'];
                        $_text = currency_numberformat($_value, 'percentage' == $value['unit'] ? ' %' : ' VNĐ');

                        $this->table->add_row(['Khuyến mãi : ' . $_name, $_text]);
                    }

                    $discount_amount = (float) get_term_meta_value($edit->term_id, 'discount_amount');
                    $this->table->add_row('Giá trị khuyến mãi', currency_numberformat($discount_amount));
                }

                $contract_value = (float) get_term_meta_value($edit->term_id, 'contract_value');
                $this->table->add_row('Giá trị hợp đồng', currency_numberformat($contract_value));

                $vat = force_var(get_term_meta_value($edit->term_id, 'vat'), 0);
                if ($vat > 0) {
                    $this->table->add_row(array("VAT", "{$vat} %"));
                }

                $_total = $contract_value * (1 + div($vat, 100));
                $this->table->add_row('Thành tiền', currency_numberformat($_total));

                $this->load->model('option_m');
                $exchange_rates = $this->option_m->get_value('exchange_rate', TRUE);
                foreach ($exchange_rates as $currency => $exchange_rate) {
                    $meta_key = 'exchange_rate_' . strtolower($currency);

                    $meta_value = (float) get_term_meta_value($edit->term_id, $meta_key) ?? 0;
                    if (!empty($meta_value)) {
                        $regex = '/^([a-z]{3})_to_[a-z]{3}$/';
                        preg_match($regex, $currency, $currency);
                        $currency = strtoupper($currency[1]);
                        $this->table->add_row("Tỉ giá 1 {$currency}", currency_numberformat($meta_value));
                    }
                }

                echo $this->table->generate();
                ?>
            </div>

            <?php if ('range' == $service_fee_payment_type) : ?>
                <p class="lead">Mức % phí dịch vụ</p>
                <div class="table-responsive">
                    <?php
                    $this->table->clear()->add_row(array('Mức ngân sách từ', 'đến mức ngân sách', '% Phí dịch vụ'));
                    $service_fee_plan = unserialize(get_term_meta_value($edit->term_id, 'service_fee_plan'));
                    foreach ($service_fee_plan as $row) {
                        if (0 == $row['to']) {
                            $this->table->add_row([currency_numberformat($row['from']), 'trở lên', number_format((float)$row['service_fee_rate'], 0, ',', '.') . '%']);
                            continue;
                        }
                        $this->table->add_row([currency_numberformat($row['from']), currency_numberformat($row['to']), number_format((float)$row['service_fee_rate'], 0, ',', '.') . '%']);
                    }
                    echo $this->table->generate();
                    ?>
                </div>

            <?php endif; ?>
        </div>
        <!-- /.col -->
    </div>
</div>
<?php

echo '<div class="clearfix"></div>',
$this->admin_form->form_open(),
$this->admin_form->hidden('', 'edit[term_status]', 'waitingforapprove'),
form_hidden('edit[term_id]', $edit->term_id),
$this->admin_form->submit('', 'confirm_step_finish', 'confirm_step_finish', '', array('style' => 'display:none;', 'id' => 'confirm_step_finish')),
$this->admin_form->form_close();
?>