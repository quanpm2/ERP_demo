<?php
$this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
$this->template->javascript->add(base_url("dist/vTiktokadsConfigurationKpiBox.js?v=202202241311"));
?>

<div id="vTiktokadsConfigurationKpiBox">
    <v-tiktokads-configuration-kpi-box :term_id="<?php echo $term->term_id; ?>"></v-tiktokads-configuration-kpi-box>
</div>