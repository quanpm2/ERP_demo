<h1>* Spend tính từ ngày <?php echo $start_date ?></h1>

<table border="1">
    <tr>
        <td style="padding: 0 8px;">Tổng spend ban đầu</td>
        <td style="padding: 0 8px;"><?php echo currency_numberformat((float)$sum_spend, '') ?></td>
    </tr>
    <tr>
        <td style="padding: 0 8px;">Tổng spend trùng</td>
        <td style="padding: 0 8px;"><?php echo currency_numberformat((float)$sum_spend_of_duplicated_insight, '') ?></td>
    </tr>
    <tr>
        <td style="padding: 0 8px;"><b>Tổng spend sau tính toán</b></td>
        <td style="padding: 0 8px;"><b><?php echo currency_numberformat((float)$sum_spend - (float)$sum_spend_of_duplicated_insight, '') ?></b></td>
    </tr>
</table>
<p>* Tổng spend trùng = Tổng spend trùng của ngày của tài khoản</p>
<p>* Spend trùng của ngày = Tổng spend của ngày trùng - spend chuẩn của ngày trùng</p>
<br/>
<p><a target="_blank" href="<?php echo admin_url('tiktokads/overview/' . $contract_id) ?>">=> Tổng quan</a></p>
<p><a target="_blank" href="<?php echo admin_url('tiktokads/fix/fix_join_all_contract') ?>">=> Chuỗi nối</a></p>

<?php
foreach($duplicated_insights as $ad_account_id => $duplicated_insight){
    echo "<h3>Tài khoản {$ad_account_id}</h3>";

    foreach($duplicated_insight as $date => $insights){
        $spend = currency_numberformat(array_sum(array_column($insights, 'spend')), '');
        echo '<h4>Ngày ' . $date . ' (' . count($insights) . '): '.$spend.'</h4>';
    }
}
?>

<?php 
echo $this->admin_form->form_open();
echo $this->admin_form->hidden('', 'start_date', $start_date);
echo $this->admin_form->submit('submit_fix_insight_contract', 'Xác nhận xoá trùng');
echo $this->admin_form->form_close();
?>