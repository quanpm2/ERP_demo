<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$is_hidden = TRUE;
$is_allowed_recalc_all_fbindex = FALSE;

if($this->admin_m->id == 47) 
{
	$is_allowed_recalc_all_fbindex = TRUE;
	$is_hidden = FALSE;
}

if($is_hidden) return; // Nếu không có bất cứ actions được phép thể hiện thì return;                       

echo $this->admin_form->set_col(12,0)->box_open();

if($is_allowed_recalc_all_fbindex)
{
	echo form_button(['id'=>'async-all-tt-insight-data','class'=>'btn btn-primary'],'<i class="fa fa-fw fa-refresh"></i>Đồng bộ dữ liệu');
}

echo $this->admin_form->box_close();
?>

<script type="text/javascript">
	$(function(){

		$('[data-toggle=confirmation]').confirmation();
		$(".input-mask").inputmask();
		$('.set-datepicker').datepicker({
			format: 'dd-mm-yyyy',
			todayHighlight: true,
			autoclose: true,
		});

		$("#async-all-tt-insight-data").click(function(){

			var _self = $(this);
			_self.find('i.fa-refresh').addClass('fa-spin');

			fetch(`${base_url}api-v2/tiktokads/contract/adaccount_insight/<?php echo $term_id;?>`, { method: 'PUT' })
			.then(response => {
				if (!response.ok) throw Error("Quá trình xử lý không thành công.")
    			return response.json()
			})
			.then(response => {
				$.notify(response.msg,response.success)
				_self.find('i.fa-refresh').removeClass('fa-spin')
			})
			.catch(error => { 
				$.notify(error, 'error')
				_self.find('i.fa-refresh').removeClass('fa-spin')
			})
		});

	});
</script>