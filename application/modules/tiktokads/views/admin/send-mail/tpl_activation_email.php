<?php 
   $this->load->view('header');  
   $term                = $this->term_m->get($term_id);
   $content_style = 'font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;';
?>
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td height="15"></td>
   </tr>
   <tr>
      <td align="center">
         <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
               <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #f58220;">
                  <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                     <tr>
                        <!--Title-->
                        <td height="40" align="center" bgcolor="#f58220" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Kích hoạt dịch vụ</td>
                        <!--End title-->
                        <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape_adsplus.png" width="25" height="40" alt="img" /></td>
                     </tr>
                  </table>
                  <!--Space-->
                  <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                     <tr>
                        <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                           <p style="padding-left: 24px;">&nbsp;</p>
                        </td>
                     </tr>
                  </table>
                  <!--End Space-->
                  <!--detail-->
                  <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                     <tr>
                        <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Thông báo kích hoạt dịch vụ quảng cáo TikTok</td>
                     </tr>
                  </table>
                  <!--end detail-->
               </td>
            </tr>
            <tr>
               <td bgcolor="#ffffff">
                  <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                     <tbody>
                        <tr>
                           <td height="25"></td>
                        </tr>
                        <!--Content-->
                        <tr>
                           <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                 ADSPLUS.VN gửi email thông báo kích hoạt dịch vụ quảng cáo trực tuyến TikTok cho website: <?php echo anchor(prep_url($term->term_name));?>
                                 </p>
                           </td>
                        </tr>
                        <!--End Content-->
                        <tr>
                           <td align="left" height="30"></td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </table>
      </td>
   </tr>
   <tr>
      <td height="15"></td>
   </tr>
</table>   

<!-- Thông tin hợp đồng -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td height="15"></td>
   </tr>
   <tr>
      <td align="center">
         <table class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
               <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #f58220;">
                  <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                     <tr>
                        <!--Title-->
                        <td height="40" align="center" bgcolor="#f58220" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin hợp đồng</td>
                        <!--End title-->
                        <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape_adsplus.png" width="25" height="40" alt="img" /></td>
                     </tr>
                  </table>
                  <!--Space-->
                  <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                     <tr>
                        <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                           <p style="padding-left: 24px;">&nbsp;</p>
                        </td>
                     </tr>
                  </table>
                  <!--End Space-->
                  <!--detail-->
                  <!--end detail-->
               </td>
            </tr>
            <tr>
               <td bgcolor="#ffffff">
                  <?php 
                     $term                = $this->term_m->get($term_id);
                     
                     $representative_name = get_term_meta_value($term_id, 'representative_name');
                     $contract_code       = get_term_meta_value($term_id, 'contract_code');
                     $contract_begin      = get_term_meta_value($term_id, 'contract_begin');
                     $contract_end        = get_term_meta_value($term_id, 'contract_end');

                     $start_service_time  = ( ! get_term_meta_value($term_id, 'start_service_time') ) ? '--' : my_date(get_term_meta_value($term_id, 'start_service_time'), 'd/m/Y') ;
                     $end_service_time    = ( ! get_term_meta_value($term_id, 'end_service_time') ) ? '--' : my_date(get_term_meta_value($term_id, 'end_service_time'), 'd/m/Y') ;    
                     
                     $staff_id            = get_term_meta_value($term_id, 'staff_business');
                     if( ! $staff_id ) $staff_id  = '' ;
                     $staff_name          = $this->admin_m->get_field_by_id($staff_id, 'display_name');
                     if( ! $staff_name ) $staff_name  = 'Chưa có' ; 
                     
                     $tech_kpis           = $this->tiktokads_kpi_m->select('user_id')
                                                                ->where('term_id',$term_id)
                                                                ->where('kpi_type','tech')
                                                                ->as_array()
                                                                ->get_many_by();
                     
                     $technical_staffs_ids = array_column($tech_kpis, 'user_id');

                     $technical_staffs_userid        = '' ;
                     $technical_staffs_username      = 'Chưa có' ;
               
                     if( ! empty($technical_staffs_ids) ) 
                     {
                           foreach ($technical_staffs_ids as $technical_staffs_id)
                           {
                               $user_name                      = $this->admin_m->get_field_by_id($technical_staffs_id,'display_name');
                               $technical_staffs_userid        = $technical_staffs_id ;
                               $technical_staffs_username      = $user_name ;
                               $technical_staffs_mail          = $this->admin_m->get_field_by_id($technical_staffs_id,'email'); 

                           }
                     }                            
                     echo $technical_staffs_mail ;          
                     ?>                            
                  <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                     <tbody>
                        <tr>
                           <td height="25"></td>
                        </tr>
                        <!--Content-->
                        <tr>
                           <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tr>
                              <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Mã hợp đồng: </td>
                              <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $contract_code ?></td>
                           </tr>
                           <tr>
                              <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Tên dịch vụ</td>
                              <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">TikTok Ads</td>
                           </tr>

                           <tr>
                              <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px; background: #f2f2ff; padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Ngân sách</td>
                              <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;background: #f2f2ff;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo number_format(get_term_meta_value($term_id, 'contract_budget')) ;?> VNĐ</td>
                           </tr>

                           <tr>
                              <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Phí dịch vụ</td>
                              <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo number_format(get_term_meta_value($term_id, 'service_fee')) ;?> VNĐ</td>
                           </tr>

                           <?php if(get_term_meta_value($term_id, 'vat')) : ?>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">VAT</td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo force_var(get_term_meta_value($term_id, 'vat'), 0) ;?> %</td>
                              </tr>
                           <?php endif ?>
                              <!--<tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Tổng tiền trước VAT</td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo number_format(get_term_meta_value($term_id, 'contract_value')) ;?> VNĐ</td>
                              </tr>-->
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Thời gian bắt đầu</td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo my_date($contract_begin,'d/m/Y') ;?></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Thời gian kết thúc</td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo my_date($contract_end,'d/m/Y') ;?></td>
                              </tr>

                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Thời gian chạy quảng cáo</td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php echo $start_service_time ;?></td>
                              </tr>

                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Thời gian ngừng quảng cáo</td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"><?php echo $end_service_time ;?></td>
                              </tr>

                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Kinh doanh phụ trách</td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">#<?php echo $staff_id; ?> - <?php echo $staff_name ;?></td>
                              </tr>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Kỹ thuật thực hiện</td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#f58220;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">#<?php echo $technical_staffs_userid; ?> - <?php echo $technical_staffs_username ;?></td>
                              </tr>

                           </table>     
                        </tr>
                        <!--End Content-->
                        <tr>
                           <td align="left" height="30"></td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </table>
      </td>
   </tr>
</table>
<?php $this->load->view('footer'); ?>