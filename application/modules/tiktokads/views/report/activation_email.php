<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td align="center">
      <p style="font-family:Roboto;color:#ffffff;font-size:30px; font-weight:300; margin:5px 0;  text-transform:uppercase;"><?php echo @$title;?></p>
    </td>
  </tr>
  <tr>
    <td align="center">
      <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Mã hợp đồng: 
        <strong>
          <?php echo get_term_meta_value($term->term_id,'contract_code');?>
        </strong>
      </p>
      <?php
      $contract_begin = get_term_meta_value($term->term_id,'contract_begin');
      $contract_end = get_term_meta_value($term->term_id,'contract_end');

      $time_contract = 0;
      if(!empty($contract_begin) && !empty($contract_end))
      {
        $time_contract = $contract_end - $contract_begin;
      }

      $start_time = get_term_meta_value($term->term_id,'advertise_start_time');

      $end_time = $start_time + $time_contract;

      $contract_date = my_date($start_time,'d/m/Y').' - '.my_date($end_time,'d/m/Y');
      ?>
      <p style="font-family:Roboto;color:#ffffff;margin:5px 0;font-size:14px">Thời gian thực hiện từ: <span style="font-weight:bold"><?php echo $contract_date;?></span> .</p> &nbsp;
    </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6e6" style="padding:20px; font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <p style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;color:#363636;">Kính chào Quý khách</p>
      <p>Adsplus.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>
      <p>Adsplus.vn gửi email thông báo kích hoạt & thực hiện hợp đồng ADSPLUS cho <?php echo anchor(prep_url($term->term_name),$term->term_name);?></p>
    </td>
  </tr>

  <tr>
    <td align="center" style="border:20px solid #e6e6e6;"><table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
      <tbody>
        <tr>
          <td bgcolor="#fff">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
              <tbody>
                <tr>
                  <td bgcolor="#fff">
                    <?php
                    $template = array(
                      'table_open' => '<table border="0" cellspacing="0" cellpadding="5" width="100%" style="font-size:13px">',
                      'row_alt_start' => '<tr bgcolor="#f4f8fb">');

                    $this->table->set_template($template);
                    $this->table->add_row(array('data'=>'PHẦN CAM KẾT','style'=>'color:#fff;font-weight:bold','width'=>'80%','colspan'=>2,'bgcolor'=>'#f58220'));

                    $this->table->add_row('Thời gian bắt đầu :',my_date($start_time,'d/m/Y'));
                    $this->table->add_row('Thời gian kết thúc dự kiến :',my_date($end_time,'d/m/Y'));

                    $contract_budget = (int) get_term_meta_value($term->term_id,'contract_budget');
                    $this->table->add_row('Ngân sách :',currency_numberformat($contract_budget));

                    $budget = get_term_meta_value($term->term_id,'contract_budget');

                      if(!empty($isJoinedContract))
                      {
                        $_heading = ' Ngân sách hợp đồng bổ sung';
                        $balanceBudgetReceived > 0 AND $_heading = 'Ngân sách cấn trừ';

                        $this->table->set_template($template);
                        $this->table->add_row(array(
                          'data'    => $_heading,
                          'style'   => 'color:#fff;font-weight:bold',
                          'width'   => '80%',
                          'colspan' => 2,
                          'bgcolor' => '#f58220'
                        ));

                        $balanceBudgetReceivedText  = currency_numberformat( abs($balanceBudgetReceived) );
                        $_balanceDescription        = "Số tiền ngân sách {$balanceBudgetReceivedText} sẽ được bổ sung từ mã HĐ {$previousContractCode}";
                        $balanceBudgetReceived > 0 AND $_balanceDescription = "Số tiền ngân sách {$balanceBudgetReceivedText} sẽ được trừ ra từ Mã HĐ {$previousContractCode}";

                        $this->table->add_row(array( 'data' => $_balanceDescription, 'colspan' => 2 ));
                        echo $this->table->generate();
                        echo "<br/>";

                        $budget+= $balanceBudgetReceived;
                      }
                      
                      $format_budget = currency_numberformat($budget);

                      $account_currency_codes = get_term_meta($term->term_id, 'account_currency_codes');
                      $account_currency_codes = array_column($account_currency_codes, 'meta_value');
                      if(!empty($account_currency_codes))
                      {
                        foreach($account_currency_codes as $account_currency_code)
                        {
                            $exchange_rate = get_exchange_rate($account_currency_code, $term->term_id);
                            $format_budget.= ' = '.currency_numberformat(div($budget, $exchange_rate)," $account_currency_code",2);
                            $this->table->add_row('Ngân sách :',$format_budget);
                            
                            if($account_currency_code != 'VND'){
                                $this->table->add_row(
                                    'Tỷ giá bán ngày '.my_date($start_time,'d/m/Y').':',
                                    "1 $account_currency_code = ".currency_numberformat($exchange_rate)
                                );
                            }
                        }

                      }

                    echo $this->table->generate();
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>


  <tr>
    <td align="center" style="border:20px solid #e6e6e6;">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
        <tbody>
          <tr>
            <td bgcolor="#fff">
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:10px #ffffff solid;background:#ffffff;font-size:13px;color:#363636; font-family:Arial, Helvetica, sans-serif;">
                <tbody>
                  <tr>
                    <td bgcolor="#fff">
                      <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">Thông tin khách hàng</p>
                      <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                        <tbody>
                          <tr bgcolor="#f4f8fb">
                            <td width="25%" height="25">Tên khách hàng</td>
                            <td>: <?php echo 'Anh/Chị '.get_term_meta_value($term->term_id,'representative_name');?></td>
                          </tr>
                          <tr>
                            <td height="25">Email</td>
                            <td>: <?php echo mailto(get_term_meta_value($term->term_id,'representative_email'));?></td>
                          </tr>
                          <tr bgcolor="#f4f8fb">
                            <td  height="25">Điện thoại di động</td>
                            <td>: <?php echo get_term_meta_value($term->term_id,'representative_phone');?></td>
                          </tr>
                          <tr>
                            <td height="25">Địa chỉ</td>
                            <td>: <?php echo get_term_meta_value($term->term_id,'representative_address');?></td>
                          </tr>
                        </tbody>
                      </table>
                      <?php
                        $curators = get_term_meta_value($term->term_id,'contract_curators');
                        $curators = @unserialize($curators);

                        $curators = array_filter($curators, function($item){
                            return !(empty($item['name']) && empty($item['phone']) && empty($item['email']));
                        });
                      ?>
                      <?php if(!empty($curators)):?>
                        <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">
                            Thông tin báo cáo công việc website 
                            <?php echo anchor(prep_url($term->term_name),$term->term_name);?>
                        </p>
                        <?php
                            $names = $emails = $phones =array();
                            foreach ($curators as $item) {
                                if(!empty($item['name'])) $names[] = $item['name'];
                                if(!empty($item['phone'])) $phones[] = $item['phone'];
                                if(!empty($item['email'])) $emails[] = mailto($item['email']);
                            }
                        ?>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:13px">
                            <tbody>
                            <?php if(!empty($names)):?>
                                <tr bgcolor="#f4f8fb">
                                    <td width="25%"  height="25">Nhận báo cáo</td>
                                    <td>: <?php echo empty($names) ? '' : implode(', ', $names);?></td>
                                </tr>
                            <?php endif;?>
                            <?php if(!empty($phones)):?>
                                <tr>
                                    <td height="25">SMS </td>
                                    <td>: <span style="font-weight:bold"><?php echo empty($phones) ? '' : implode(', ', $phones);?></span></td>
                                </tr>
                            <?php endif;?>
                            <?php if(!empty($emails)):?>
                                <tr bgcolor="#f4f8fb">
                                    <td  height="25">Email</td>
                                    <td>: <?php echo empty($emails) ? '' : implode(', ', $emails);?></td>
                                </tr>
                            <?php endif;?>
                            </tbody>
                        </table>
                      <?php endif; ?>
                    </td>
                  </tr>
                </tbody>
              </table>          
              <p style="font-family:tahoma;font-size:16px;font-weight:bold;color:#363636">&nbsp;</p>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
  <!-- <tr>
    <td bgcolor="#e6e6e6" style="border-left:20px solid #e6e6e6;border-right: 20px solid #e6e6e6;  ">
      <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
          <tr>
            <td bgcolor="#f58220" >
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px"   >
                <tbody>
                  <tr>
                    <td>
                      <p style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:16px ; font-weight:bold;margin:10px 20px">Các dịch vụ ADSPLUS.VN sẽ cung cấp cho Quý khách : </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#e6e6e6">
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px">
                <tbody>
                  <tr>
                    <td valign="top"><img alt="arrow down" src="http://webdoctor.vn/images/arrowdown-1.png"></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px;border:10px #ffffff solid;background:#ffffff; margin-bottom: 20px">
                <tbody>
                  <tr>
                    <td bgcolor="#ffffff" style="font-size: 13px;
                    color: #363636;
                    font-family: Arial, Helvetica, sans-serif;" >
                    <p><strong>1. Gắn code Goolge Analytics  : </strong>Sau khi kích hoạt dịch vụ, Adsplus.vn sẽ gởi code  <font style="color:#03F">Google Analytics gắn vào website </font> => việc này giúp Adsplus theo dõi được hiệu quả quảng cáo. </p>
                    <p ><strong>2. Nhận báo cáo hàng ngày từ Google : </strong>Anh (Chị) sẽ nhận được báo cáo hàng ngày từ Google, việc này thể hiện sự minh bạch trong quảng cáo </p>
                    <p ><strong>3. Nhận báo cáo tuần từ hệ thống. </strong> Anh (Chị) sẽ nhận được báo cáo tuần từ hệ thống Adsplus.vn, giúp anh chị theo dõi được tiến độ chạy quảng cáo</p>
                    <p ><strong>3. Nhận SMS hàng ngày. </strong> Anh (Chị) sẽ nhận được SMS hàng ngày thông báo số nhấp chuột mỗi ngày.</p>
                    <p >Cảm ơn anh/chị ! </p>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </td>
</tr> -->
</table>