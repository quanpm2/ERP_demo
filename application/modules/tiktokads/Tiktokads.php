<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiktokads_Package extends Package
{	
	const ROOT_ROLE_ID = 1;

	function __construct()
	{
		parent::__construct();
		$this->website_id  = $this->uri->segment(4);
	}

	public function name()
	{
		return 'Tiktok Ads';
	}

	public function init()
	{
		$this->_load_menu();
        // $this->_update_permissions();
	}

	/**
	 * Init menu LEFT | NAV ITEM FOR MODULE
	 * then check permission before render UI
	 */
	private function _load_menu()
	{
		$order 	= 1;
		$itemId = 'tiktokads';
 
 		if( ! is_module_active('tiktokads')) return FALSE;

		if(has_permission('tiktokads.Index.access'))
		{
			$this->menu->add_item(array(
				'id'        => $itemId,
				'name' 	    => 'Tiktok Ads',
				'parent'    => null,
				'slug'      => admin_url('tiktokads'),
				'order'     => $order++,
				'icon'		=> 'fa fa-lg fa-tumblr-square',
				'is_active' => is_module('tiktokads')
				),'left-ads-service');	
		}

 		if(!is_module('tiktokads')) return FALSE;
		
		if(has_permission('tiktokads.Index.access'))
		{
			$this->menu->add_item(array(
			'id' => 'tiktokads-service',
			'name' => 'Danh sách',
			'parent' => $itemId,
			'slug' => admin_url('tiktokads'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-database'
			), 'left-ads-service');
		}

		if(has_permission('tiktokads.staffs.manage'))
		{

			$this->menu->add_item(array(
			'id' => 'tiktokads-staffs',
			'name' => 'Phân công/Phụ trách',
			'parent' => $itemId,
			'slug' => admin_url('tiktokads/staffs'),
			'order' => $order++,
			'icon' => 'fa fa-fw fa-xs fa-group'
			), 'left-ads-service');
		}

		$left_navs = array(
			'overview'=> array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-xs fa-tachometer',
				),
			'kpi' => array(
				'name' => 'KPI',
				'icon' => 'fa fa-fw fa-xs fa-heartbeat',
				),
			'setting'  => array(
				'name' => 'Cấu hình',
				'icon' => 'fa fa-fw fa-xs fa-cogs',
				)
			);

		$term_id = $this->uri->segment(4);

		$this->website_id = $term_id;

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("tiktokads.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "tiktokads-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
			), 'left-ads-service');
		}
	}

	public function title()
	{
		return 'Tiktok Ads';
	}

	public function author()
	{
		return 'thonh';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Quản lý hợp đồng Tiktok';
	}
	
	/**
	 * Init default permissions for tiktokads module
	 *
	 * @return     array  permissions default array
	 */
	private function init_permissions()
	{
		return array(

			'tiktokads.index' => array(
				'description' => 'Quản lý Tiktok Ads',
				'actions' => ['access','update','add','delete','manage','mdeparment','mgroup']),

			'tiktokads.done' => array(
				'description' => 'Dịch vụ đã xong',
				'actions' => ['access','add','delete','update','manage','mdeparment','mgroup']),
		
			'tiktokads.overview' => array(
				'description' => 'Tổng quan',
				'actions' => ['access','manage','mdeparment','mgroup']),

			'tiktokads.kpi' => array(
				'description' => 'KPI',
				'actions' => ['access','add','delete','update','manage','mdeparment','mgroup']),
			
			'tiktokads.start_service' => array(
				'description' => 'Kích hoạt Tiktok Ads',
				'actions' => ['access','update','manage','mdeparment','mgroup']),
			
			'tiktokads.stop_service' => array(
				'description' => 'Ngừng Tiktok Ads',
				'actions' => ['access','update','manage','mdeparment','mgroup']),
			
			'tiktokads.setting' => array(
				'description' => 'Cấu hình',
				'actions' => ['access','add','delete','update','manage']),
            
            'tiktokads.staffs' => array(
                'description' => 'Phân công phụ trách',
                'actions' => ['access', 'add', 'delete', 'update', 'manage']
            ),

            'tiktokads.metrics_core' => array(
                'description' => 'Quyền update các dữ liệu nâng cao',
                'actions' => ['update', 'manage']
            ),

            'tiktokads.unjoin_contract' => array(
                'description' => 'Quyền gỡ nối hợp đồng',
                'actions' => array('access', 'update', 'manage')
            ),

            'tiktokads.rejoin_contract' => array(
                'description' => 'Quyền fix nối hợp đồng',
                'actions' => array('access', 'update', 'manage')
            ),

            'tiktokads.fix' => array(
                'description' => 'Quyền fix dữ liệu (Chỉ dành cho admin)',
                'actions' => array('access', 'update', 'manage')
            ),
		);

	}

	private function _update_permissions()
	{
		$permissions = $this->init_permissions();
		if(empty($permissions)) return false;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	/**
	 * Install module
	 *
	 * @return     bool  status of command
	 */
	public function install()
	{
		$permissions = $this->init_permissions();
		if( ! $permissions) return FALSE;

		$rootPermissions = $this->role_permission_m->get_by_role_id(self::ROOT_ROLE_ID);
		$rootPermissions AND $rootPermissions = array_column($rootPermissions, 'role_id', 'permission_id');

		foreach($permissions as $name => $value)
		{
			$description   = $value['description'];
			$actions 	   = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);

			if( ! empty($rootPermissions[$permission_id])) continue;

			$this->role_permission_m->insert([
				'permission_id'	=> $permission_id,
				'role_id'		=> self::ROOT_ROLE_ID,
				'action'		=> serialize($actions)
			]);
		}

		return TRUE;
	}

	/**
	 * Uninstall module command
	 *
	 * @return     bool  status of command
	 */
	public function uninstall()
	{
		$permissions = $this->init_permissions();

		if(!$permissions) return FALSE;

		if($pers = $this->permission_m->like('name','tiktokads')->get_many_by())
		{
			foreach($pers as $per)
			{
				$this->permission_m->delete_by_name($per->name);
			}
		}

		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}

		return TRUE;
	}
}
/* End of file tiktokads.php */
/* Location: ./application/modules/tiktokads/models/tiktokads.php */