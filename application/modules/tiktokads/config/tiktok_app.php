<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['tiktok_app'] = array(
	'credential' 	=> array(
        'app_id' 		=> getenv('TIKTOK_APP_ID') ?: '495292314010324',
        'app_secret' 	=> getenv('TIKTOK_APP_SECRET') ?: 'b27239966b8d54c4b2e6aecd1a288cf4',
        'app_version' => getenv('TIKTOK_APP_VERSION') ?: 'v3.2'
    )
);