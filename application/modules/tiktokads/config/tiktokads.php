<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['adaccount_status'] = [
	'states' => [
		'UNSPECIFIED' 		=> 'Chưa cấu hình',
		'PENDING_APPROVAL'  => 'Đã cấu hình',
		'APPROVED' 			=> 'Đã kích hoạt',
		'REMOVED' 			=> 'Đã xóa cấu hình',
		'UNKNOWN' 			=> 'Chưa xác định',
	],
	'default' => 'UNSPECIFIED'
];

$config['packages'] = array(
	'service' => array(),
	'default' =>''
);

/* Loại dịch vụ */
$config['stypes'] = array(

	/* Dịch vụ quản lý tài khoản quảng cáo - thu phí dịch vụ */
	'account_type' => array( 'name' => 'account_type', 'label' => 'Quản lý tài khoản' ),

	/* Dịch vụ mặc định */
	'default' => 'account_type'
);

$config['tiktokads'] = array(
	'status' => array(
		'pending' => 'Ngưng hoạt động',
		'publish' => 'Đang hoạt động'
	)
);