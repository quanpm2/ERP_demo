<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order_contact extends Admin_Controller {

	public $model = 'order_m';

	function __construct(){

		parent::__construct();

		$this->load->model('order_contact_m');
		$this->load->model('order_contact_email_m');
		$this->load->config('order_contact');
	}

	/**
	 * [index Hiển thị tất cả các order email chưa đọc]
	 */
	public function index(){

		# kiểm tra order nào đã qua 1 tháng nhưng vẫn chưa được xử lý
		$this->order_contact_email_m->refill_order();

		$unread_emails = $this->scache->get('unread_emails');
		if(empty($unread_emails)){
			$unread_emails = $this->order_contact_email_m->get_unread_email();
			// $this->scache->write($unread_emails,'unread_emails', 3600);
			$this->scache->write($unread_emails,'unread_emails', 1);
		}

		$caption = 'Danh sách order dịch vụ';

		$is_locked = $this->is_locked();
		if($is_locked){
			$expire_time = $this->usermeta_m->get_meta_value($this->admin_m->id,'order_expire_time_lock');	
			$caption .= ' (<i>Order tiếp theo có thể nhận sau '.dateDifference(time(),$expire_time,'','').')</i>';
		}

		$this->table->add_row(array('STT','Tiêu đề','Tác vụ'));
		$this->table->set_caption($caption);

		if(!empty($unread_emails)){

			# Lấy những message_id không được hiển thị bởi admin hiện tại
			$message_ids = $this->usermeta_m->get_meta($this->admin_m->id,'deny_message_id',FALSE,TRUE);

			foreach ($unread_emails as $stt=>$email){

				$is_denied_message = (in_array($email['id'], $message_ids));
				if($is_denied_message) continue;

				$actions = '---';
				if(!$is_locked)
					$actions = anchor(module_url("take_email/{$email['id']}"),'<i class="fa fa-fw fa-thumb-tack"></i>','title="Nhận order" class="btn btn-default btn-xs"');

				$this->table->add_row(array(++$stt,$email['subject'],$actions));
			}
		}
		else $this->table->add_row(array('---','---','---'));

		$data['content'] = $this->table->generate();
		parent::render($data);
	}

	/**
	 * [take_email Tiếp nhận order email về trang quản lý order email của user]
	 * @param  [string] message_id
	 */
	public function take_email($message_id){

		if($this->is_locked()){
			$this->messages->error('Tài khoản của bạn vẫn trong thời gian chờ đợi.Vui lòng chờ!');
			redirect(module_url(),'refresh');
		}

		$data = array();
		$data['email'] = $this->order_contact_email_m->get_email($message_id);
		if(empty($data['email'])) return FALSE;

		$order = $this->order_contact_m
		->get_set_order_service()
		->where('post_name',$message_id)
		->get_by();

		$is_owned_another_admin = (!empty($order) && $order->post_author != $this->admin_m->id && $order->post_author != 0);
		if($is_owned_another_admin){
			$this->messages->error('Email đã được tiếp nhận xử lý bởi người khác , vui lòng chọn order khác !');
			redirect(module_url());
		}

		if(empty($order)){

			$insert_data = array('post_title'=>$data['email']['subject'],
				'post_content' => $data['email']['body'],
				'post_name' => $message_id,
				'post_author' => $this->admin_m->id,
				'created_on' => time(),
				'post_status' => 'pending');

			$this->order_contact_email_m->save_order_contact_email($insert_data);

			$unread_emails = $this->order_contact_email_m->get_unread_email();
		}
		else
		{
			$update_data = array('post_id' => $order->post_id,
				'post_name' => $message_id,
				'post_author' => $this->admin_m->id,
				'created_on' => time(),
				'post_status' => 'pending');
			$this->order_contact_email_m->save_order_contact_email($update_data,'update');
		}

		# expire time = time now add 1800 seconds : 30 min
		$this->usermeta_m->update_meta($this->admin_m->id,'order_expire_time_lock', time() + 1800);

		redirect(module_url('receive_emails'),'refresh');
	}

	/**
	 * [receive_emails Hiển thị tất cả các email admin đang sở hữu]
	 */
	public function receive_emails(){

		$this->data['emails'] = $this->order_contact_m
		->select('posts.post_id,posts.post_title,posts.post_name,posts.post_author,posts.created_on,posts.post_status,posts.post_content,term_posts.term_id')
		->join('term_posts','term_posts.post_id = posts.post_id','LEFT')
		->get_set_order_service()
		->where('post_author',$this->admin_m->id)
		->order_by('created_on','DESC')
		->get_many_by();

		parent::render($this->data);	
	}
	
	/**
	 * [map_contract create contract and mapping to contact order] 
	 * @param  [int]
	 */
	public function map_contract($post_id){

		$this->load->config('contract/contract');
		$term_types = array_keys($this->config->item('services'));
		$term_types[] = ''; // none stype when contract uninitialized

		$post_term = $this->term_posts_m
		->where('term_posts.post_id',$post_id)
		->where_in('term.term_type',$term_types)
		->join('term','term.term_id = term_posts.term_id')
		->select('term.term_id,term.term_name')
		->get_by();

		# if exists contract then redirect to action create_wizard
		if(!empty($post_term)) {	
			redirect(current_module_url("contract/create_wizard/{$post_term->term_id}"),'refresh');
		}

		# else create new empty contract and redirect to action create_wizard
		$this->load->model('contract/contract_m');
		$term_id = $this->contract_m->insert(array('term_description'=>'','term_status'=>'0'));
		$this->term_posts_m->set_post_terms($post_id, $term_id);

		$this->order_contact_m->update($post_id, array('post_status'=>'pending'));

		update_term_meta($term_id,'staff_business',$this->admin_m->id);
		update_term_meta($term_id,'order_id',$post_id);

		redirect(current_module_url("contract/create_wizard/{$term_id}"),'refresh');
	}

	/**
	 * [is_locked	Kiểm tra user hiện tại có đang trong 
	 * 					thời gian khóa không được nhận order hay không]
	 * @return boolean
	 */
	private function is_locked(){
		$order_expire_time_lock = force_var($this->usermeta_m->get_meta_value($this->admin_m->id,'order_expire_time_lock'));
		$is_locked = !empty($order_expire_time_lock) && (time() <= $order_expire_time_lock);
		return $is_locked;
	}

	public function approval_contract(){

		$this->hook->add_action('prepare_query_condition',function(){
			$this->admin_ui
			->unset_column('action')
			->join('termmeta tmsale','tmsale.term_id = term.term_id', 'INNER')
			->where('tmsale.meta_key','staff_business')
			->where('tmsale.meta_value',$this->admin_m->id)
			->where('term.term_status','waitingforapprove');
		});		

		$this->data['content'] = Modules::run('contract/renderDatatable');

		parent::render($this->data,'contract/approval_contract');
	}

	function auth()
	{
		$this->load->add_package_path(APPPATH.'third_party/google-gmail/');
		$this->load->library('gmail');
		$this->gmail->aut();
		
	}

	function view($message_id = 0)
	{
		$cache_key = 'modules/order_contact/mail-'.$message_id;
		$service = $this->gmail->get_service();
		if(!$service) return FASLE;

		$message = $this->gmail->getMessage($service, 'me',$message_id);
		$subject = $this->gmail->get_subject($message);
		$rawData = $this->gmail->get_body($message);
		$r = array(
			'id'=>$message_id,
			'subject' =>$subject,
			'body' =>$rawData,
			);
		prd($r);
	}

	function get_body_null()
	{
		$service = $this->gmail->get_service();
		if(!$service) return FASLE;
		$unread_emails = $this->order_contact_email_m->get_unread_email();

		$data = array();
		if($unread_emails)
		{
			foreach($unread_emails as $mail)
			{
				$is_empty = empty($mail['body']);
				$this->table->add_row(array($mail['id'],$mail['subject'], ($is_empty) ?'Null':'OK'));
				if($is_empty)
				{
					$cache_key = 'modules/order_contact/mail-'.$mail['id'];
					$this->scache->delete( $cache_key);
				}
			}
		}
		$data['content'] = $this->table->generate();
		parent::render($data,'blank');
	}
}
/* End of file Order_contact.php */
/* Location: ./application/modules/order_contact/controllers/Order_contact.php */