<?php
$this->template->javascript->add('plugins/tinymce/tinymce.min.js');
?>
<div>
	<?php
	
	$this->table->set_caption('Thông tin order dịch vụ');

	$this->table->add_row(array('Tiêu đề','Nội dung','Tình trạng','Ngày nhận','Action'));

	if(!empty($emails)){
		foreach ($emails as $email) {
			$status = $this->config->item($email->post_status,'order_status');

			$actions = anchor('#','<i class="fa fa-fw fa-send-o"></i>','title="Email yêu cầu tạo hợp đồng" data-postid="'.$email->post_id.'" class="btn btn-default btn-xs load_form"');

			$this->table->add_row(array($email->post_title,$email->post_content,$status,my_date($email->created_on,'d/m/Y'),$actions));
		}
	}

	echo $this->table->generate();
	?>
</div>
<div class="modal fade" id="email-model" tabindex="-1" role="dialog" aria-labelledby="email-model" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Gửi yêu cầu tạo hợp đồng</h4>
      </div>
      <?php
      echo $this->admin_form->form_open();
      ?>
      <div class="modal-body">
      	<div class="row">
			<div class="col-md-12">
				<?php 
				echo 
				form_hidden('post_id', 0),
				$this->admin_form->textarea('Nội dung','content','','',array('class'=>'editor','id'=>'email_content'));
				?>
			</div>
		</div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <?php 
        echo form_submit(array('name'=>'request_contract','type'=>'submit','class'=>'btn btn-primary'),'Send Mail');
        ?>
      </div>
      <?php 
      echo $this->admin_form->form_close();
      ?>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
$(function(){

	tinymce.init({ 
		selector:'.editor' ,
		theme: "modern", 
		height: 300, 
		subfolder:"", 
		plugins: [ 
		"advlist autolink link image lists charmap print preview hr pagebreak", 
		"searchreplace wordcount visualblocks visualchars code media nonbreaking", 
		"table contextmenu directionality paste textcolor" 
		], 
		image_advtab: true, 
		toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor | link unlink anchor | image | preview code",
		// file_browser_callback: RoxyFileBrowser,
	});

	$(".load_form").click(function(e){
		var postid = $(this).data('postid');
		$hidden_field = $("#email-model").find('input[name=post_id]').val(postid);
		$.post(admin_url + 'order_contact/mapping_contract/' + postid, {}, 
			function( resp ) { 
				if(resp.html != ''){
					tinymce.activeEditor.setContent(resp.html);
				}
			}, "json");
		$('#email-model').modal('show');
		e.preventDefault();
	});
});
</script>