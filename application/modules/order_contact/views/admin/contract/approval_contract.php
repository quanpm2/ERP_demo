<div class="row">
  <div class="col-md-2 pull-right control-group form-group">
    <div class="btn-group">
      <?php echo anchor(admin_url('contract/create_wizard'), 'Thêm mới Hợp đồng', 'class="btn btn-primary"');?>
      
  </div>
</div>

<?php
if(!empty($content)){

  $this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');

  $this->template->javascript->add('plugins/daterangepicker/moment.min.js');

  $this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
  
  echo $content['table'] . $content['pagination'];
}
?>
<script type="text/javascript">

$(function(){
  $(".pace-activity").remove();
  $('[data-toggle=confirmation]').confirmation();

  $('table input[type="checkbox"]').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });

   //Enable check and uncheck all functionality
  $(".checkbox-toggle").click(function () {
    var clicks = $(this).data('clicks');
    if (clicks) {
            //Uncheck all checkboxes
            $("table input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
        } else {
            //Check all checkboxes
            $("table input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
        }          
        $(this).data("clicks", !clicks);
  });

  $("#remove-post").click(function () {
        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
          var ids = [];
          $('.deleteRow').each(function(){
            if($(this).is(':checked')) { 
              ids.push($(this).val());
            }
          });
          var ids_string = ids.toString();  // array to string conversion 
          alert(ids_string);
          $.ajax({
            type: "POST",
            url: "",
            data: {data_ids:ids_string},
            success: function(result) {
                  // dttable.draw(); // redrawing datatable
              },
              async:false
          });
      }
  });

  $(".input_daterange").daterangepicker({
    format: 'DD-MM-YYYY',
  });
})
</script>
<style type="text/css">
  .tooltip-inner {
    max-width: none;
    white-space: nowrap;
}
</style>