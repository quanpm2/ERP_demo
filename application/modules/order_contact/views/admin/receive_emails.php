<div>
<?php
$this->table->set_caption('Thông tin order dịch vụ');
$this->table->add_row(array('Tiêu đề','Nội dung','Tình trạng','Ngày nhận','Action'));

if(!empty($emails)){
	foreach ($emails as $email) {
		$actions = '---';
		$order_status = $this->config->item('order_status');
		$status = $order_status[$email->post_status];
		unset($order_status['pending']);

		if(!in_array($email->post_status, array_keys($order_status))){
			$action_url = module_url("map_contract/{$email->post_id}");
			if(empty($email->term_id))
				$actions = anchor($action_url,'<i class="fa fa-fw fa-plus"></i>','title="Tạo hợp đồng" class="btn btn-default btn-xs"');
			else  $actions = anchor($action_url,'<i class="fa fa-fw fa-pencil"></i>','title="Cập nhật hợp đồng" class="btn btn-default btn-xs"');
		}
		
		$this->table->add_row(array($email->post_title,$email->post_content,$status,my_date($email->created_on,'d/m/Y'),$actions));
	}
}

echo $this->table->generate();
?>
</div>