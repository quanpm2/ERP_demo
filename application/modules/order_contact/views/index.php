<?php
if(!empty($content)){
  
  echo $content['table'] . $content['pagination'];
}
$this->table->set_caption('Danh sách order');

$this->table->add_row(array());

?>

<script type="text/javascript">

  $('[data-toggle=confirmation]').confirmation();
  
  $('table input[type="checkbox"]').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });

   //Enable check and uncheck all functionality
   $(".checkbox-toggle").click(function () {
    var clicks = $(this).data('clicks');
    if (clicks) {
            //Uncheck all checkboxes
            $("table input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
        } else {
            //Check all checkboxes
            $("table input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
        }          
        $(this).data("clicks", !clicks);
    });
   $("#remove-post").click(function () {
          if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
            var ids = [];
            $('.deleteRow').each(function(){
              if($(this).is(':checked')) { 
                ids.push($(this).val());
              }
            });
            var ids_string = ids.toString();  // array to string conversion 
            alert(ids_string);
            $.ajax({
              type: "POST",
              url: "<?php echo $this->url_delete;?>",
              data: {data_ids:ids_string},
              success: function(result) {
                    // dttable.draw(); // redrawing datatable
                },
                async:false
            });
        }
    });

  </script>