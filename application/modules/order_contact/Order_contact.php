<?php
class Order_contact_Package extends Package{

	function __construct(){
		parent::__construct();
	}

	public function name(){
		return 'Order contract';
	}

	public function init()
	{
		if(is_module_active('order_contact') && has_permission('Order_contact.Index')){

			$this->menu->add_item(array(
				'id' => 'email-order',
				'name' => 'Email order',
				'parent' => NULL,
				'slug' => admin_url('order_contact/index'),
				'order' => 1,
				),'navbar');

			if(is_module('order_contact')){

				$methods = array('index'=>'Email Order','receive_emails' => 'Email đã nhận');
				$order = 1;
				foreach($methods as $method => $name){
					$this->menu->add_item(array(
						'id' => 'order-contact-email-'.$method,
						'name' => $name,
						'parent' => NULL,
						'slug' => module_url($method),
						'order' => $order++,
						'icon' => ''
						), 'left');
				}

				$this->menu->add_item(array(
					'id' => 'order-contact-email-approval-contract',
					'name' => 'Hợp đồng chờ duyệt',
					'parent' => NULL,
					'slug' => module_url('approval_contract'),
					'order' => 9,
					'icon' => ''
					), 'left');
			}
		}
	}

	public function title(){
		return 'OrderContract';
	}

	public function author(){
		return 'Thonh';
	}

	public function version(){
		return '0.1';
	}

	public function description(){
		return 'Quản lý order yêu cầu dịch vụ';
	}

	private function init_permissions(){

		$permissions = array();

		$permissions['Admin.Order_contact'] = array(
			'description' => '',
			'actions' => array('manage'));

		$permissions['Module.Order_contact'] = array(
			'description' => '',
			'actions' => array('manage'));

		$permissions['Order_contact.Index'] = array(
			'description' => 'Quản lý order email',
			'actions' => array('access','take'));

		$permissions['Order_contact.Receive_emails'] = array(
			'description' => 'Quản lý order email',
			'actions' => array('access','add','delete','update'));

		$permissions['Order_contact.contract'] = array(
			'description' => 'Quản lý order email',
			'actions' => array('access','add','delete','update'));

		return $permissions;
	}

	private function init_default_user(){
		$user_system = $this->admin_m->where('user_type','system')->get_by();
		if(empty($user_system)) $this->admin_m->insert(array('user_type'=>'system','display_name'=>'Hệ thống'));
	}

	public function install(){

		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value){
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}

		$this->init_default_user();
	}

	public function uninstall(){

		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
			$this->permission_m->delete_by_name($name);
	}
}