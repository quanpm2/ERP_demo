<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_contact_m extends Post_m {

	public $post_type = 'order-service';

	function __construct(){
   		parent::__construct();
   	}

	public function insert($data, $skip_validation = FALSE){

		if(empty($data['post_type'])) 
			$data['post_type'] = $this->post_type;

		parent::insert($data,$skip_validation);
	}

	public function get_set_order_service(){
		return $this->where('posts.post_type',$this->post_type);
	}
}
/* End of file Order_contact_m.php */
/* Location: ./application/modules/order_contact/models/Order_contact_m.php */