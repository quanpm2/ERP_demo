<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_contact_email_m extends Base_Model {

	public function __construct(){
		parent::__construct();

		$this->load->model('order_contact/order_contact_m');
		$this->load->model('staffs/admin_m');

		$this->load->add_package_path(APPPATH.'third_party/google-gmail/');
		$this->load->library('gmail');
	}

	/**
	 * @param  [array]
	 * @param  [string]
	 * @return [int]
	 */
	public function save_order_contact_email($data, $action = 'insert'){

		if(empty($data)) return FALSE;

		$service = $this->gmail->get_service();
		if(!$service) return FASLE;

		$insert_id = 0;
		if($action == 'update'){

			$data = !is_array($data) ? (array) $data : $data;
			if(!empty($data['post_id'])){
				$this->order_contact_m->update($data['post_id'],$data);
				unset($data['post_id']);
			}
		}
		else $insert_id = $this->order_contact_m->insert($data);
		
		$this->gmail->makeMessagesIsRead($service,$data['post_name']);

		return $insert_id;
	}

	/**
	 * @param  [String]
	 * @return [Array]
	 */
	public function get_email($message_id){

		$service = $this->gmail->get_service();
		if(empty($service)) return FASLE;

		$message = $this->gmail->getMessage($service, 'me',$message_id);
		if(empty($message)) return FALSE;

		$subject = $this->gmail->get_subject($message);
		$rawData = $this->gmail->get_body($message);
		return array('id' => $message_id,
					'subject' => $subject,
					'body' => $rawData);
	}

	/**
	 * @return [Array]
	 */
	public function get_unread_email(){

		$service = $this->gmail->get_service();
		if(!$service) return FASLE;

		$result = array();
		
		$lists = $this->gmail->listMessagesUnRead($service);

		if($lists){
			
			foreach($lists as $list){
				$message_id = $list->getId();
				$cache_key = 'modules/order_contact/mail-'.$message_id;
				if(!$r = $this->scache->get($cache_key))
				{
					$message = $this->gmail->getMessage($service, 'me',$message_id);
					$subject = $this->gmail->get_subject($message);
					$rawData = $this->gmail->get_body($message);
					$r = array(
						'id'=>$message_id,
						'subject' =>$subject,
						'body' =>$rawData,
					);
					$this->scache->write($r, $cache_key);
				}
				$result[] = $r;	
			}
		}
		return $result;
	}

	public function refill_order(){

		# fetch all order not convert to contract
		$avail_orders = $this->scache->get('modules/order_contact/expired_contact_order_email');
		if(empty($avail_orders)){
			$avail_orders = $this->order_contact_m
				->select('posts.post_id,posts.post_name,term_posts.term_id')
				->get_set_order_service()
				->join('term_posts','term_posts.post_id = posts.post_id')
				->where('posts.post_status','pending')
				->where('posts.created_on < UNIX_TIMESTAMP(NOW() - INTERVAL 1 MONTH)')
				->where('posts.created_on !=',0)
				->get_many_by();

			$this->scache->write($avail_orders,'modules/order_contact/expired_contact_order_email', 7200);
		}

		if(empty($avail_orders)) 
			return FALSE;

		$service = $this->gmail->get_service();
		if(!$service) 
			return FASLE;

		# make all message isRead to Unread
		foreach ($avail_orders as $order){

			# Chuyển email order về trạng thái UNREAD 
			$this->gmail->makeMessagesUnRead($service,$order->post_name);

			# Cập nhật thông tin sở hữu order email về blank
			$blank_order_data = array('post_author'=>0,'created_on'=>0,'post_status'=>'pending');
			$this->order_contact_m->update($order->post_id,$blank_order_data);

			# add mesage id deny for this user
			$this->usermeta_m->add_meta($this->admin_m->id,'deny_message_id',$order->post_name,FALSE);

			# reset thông tin hợp đồng
			if(!empty($order->term_id)){

				$this->termmeta_m
					->where('term_id',$order->term_id)
					->delete_by();
					
				$key_cache = $this->termmeta_m->meta_type.'/'.$order->term_id . '_meta';
				$this->scache->delete($key_cache);
			}

		}

		$unread_emails = $this->get_unread_email();
		$this->scache->write($unread_emails,'unread_emails', 3600);
	}
}
/* End of file Order_contact_email_m.php */
/* Location: ./application/modules/order_contact/models/Order_contact_email_m.php */