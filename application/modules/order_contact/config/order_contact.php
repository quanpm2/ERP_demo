<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['order_status'] = array( 
	'approval' => 'Chờ duyệt',
	'pending' => 'Chưa xử lý',
	'publish' => 'Đã xử lý',
);

$config['recipients'] = array(
	'thonh@webdoctor.vn'
);

$config['email_cfg'] = array(
	'to' => array('thonh@webdoctor.vn'),
	'bcc' => array(),
);