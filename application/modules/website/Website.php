<?php
class Website_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name(){
		
		return 'Website';
	}

	public function init(){	

		if(has_permission('Admin.Website') && is_module_active('website'))
		{
			$this->menu->add_item(array(
				'id' => 'Website',
				'name' => 'Website',
				'parent' => 'manager',
				'slug' => admin_url('website/'),
				'order' => 1,
				'icon' => 'fa fa-fw fa-krw'
				), 'navbar');
		}
	}

	public function title()
	{
		return 'Website';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '0.1';
	}

	public function description()
	{
		return 'Website';
	}
	private function init_permissions()
	{
		$permissions = array();
		$permissions['Admin.Website'] = array(
			'description' => 'Quản lý Website',
			'actions' => array('view','add','edit','delete','update'));

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}