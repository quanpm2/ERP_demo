<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends Admin_Controller {

	public $model = 'website_m';

	public $term_type = 'website';

	function __construct(){
		
		parent::__construct();

		restrict('Admin.Website');

		$this->load->model('website_m');
	}

	public function index(){

		restrict('Admin.Website.view');

		$this->search_filter();

		$this->admin_ui->where('term_type',$this->term_type);

		$this->admin_ui->add_column('term_id','ID');

		$this->admin_ui->add_column('term_name','Tên');

		$this->admin_ui->add_column('domain', array('set_select'=>FALSE, 'title' =>'Thông tin domain', 'set_order'=>FALSE),NULL, NULL, array('class' =>'col-md-2'));
		$this->admin_ui->add_column_callback('domain', function($data, $row_name){
			return $data;
		}, FALSE);

		$this->admin_ui->add_column('services', array('set_select'=>FALSE, 'title'=>'Dịch vụ đang dùng','set_order'=>FALSE));
		$this->admin_ui->add_column_callback('services', function($data, $row_name){

			if($services = $this->term_m
				->where('term_parent', $data['term_id'])
				->where('term_type !=', $this->term_type)
				->order_by('term_type')
				->get_many_by()){

				$data['services'] = array_map(function($x){ return $x->term_type;}, $services);

				if(empty($data['services'])) $data['services'] = '';
				else {

					$ref_services = array( 'webdoctor' => 'webdoctor', 'seo-traffic' => 'SEO Chặng', 'google-ads' => 'Google Ads');

					foreach ($data['services'] as $key => $value) {
						//echo "$value <br/>" ;
						if(!array_key_exists($value, $ref_services)) continue;
						$data['services'][$key] = $ref_services[$value];
					}

					$data['services'] = implode(', ', $data['services']);
				}
			}

			return $data;
		}, FALSE);

		$this->admin_ui->add_column('control'
			, array('title'			=>	'<button class="btn btn-default btn-xs checkbox-toggle">
										<i class="fa fa-square-o"></i>
									</button>' . 
									(has_permission('Admin.Website.Delete') ? '<button type="button" class="btn btn-default btn-xs" id="remove-post"><i class="fa fa-trash-o"></i></button>' : ''), 
					'orderable'		=> FALSE,
					'searchable'	=> FALSE ,
					'set_select'	=> FALSE,
					'set_order'		=> FALSE)
			, '<input type="checkbox" class="deleteRow" value="$1"/>' 
			// . anchor($this->data['url_edit'] . "/$1", '<i class="fa fa-pencil"></i>', 'class="btn btn-default btn-xs"')
			, $this->{$this->model}->primary_key);

		$this->admin_ui->from($this->{$this->model}->_table);

		$this->data['content'] = $this->admin_ui->generate(array(
			'uri_segment' => 4,
			'base_url'=> module_url('index/'),
		));

		parent::render($this->data);
	}

	public function delete(){

		restrict('Admin.Website.delete');

		$result = array('success'=>FALSE,'msg'=>'Cập nhật không thành công.');

		if($post = $this->input->post()){

			$post['data_ids'] = explode(',', $post['data_ids']);

			$post['not_delete_ids'] = array();

			$service_belongs = $this->term_m
			->select('term_parent')
			->where('term_type !=', 'website')
			->where_in('term_parent', $post['data_ids'])
			->get_many_by();

			if(!empty($service_belongs)) $post['not_delete_ids'] = array_map(function($x){return $x->term_parent;}, $service_belongs);

			$post['data_ids'] = array_diff($post['data_ids'], $post['not_delete_ids']);

			if(!empty($post['data_ids'])){

				if($this->{$this->model}->delete_many($post['data_ids'])){

					$result['success'] = TRUE;

					$result['msg'] = 'Dữ liệu đã được xóa';

					$result['deleted'] = array_values($post['data_ids']);
				}
			}

			if(!empty($post['not_delete_ids'])){

				$result['notdeleted'] = $post['not_delete_ids'];
			}
			
		}

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}
}