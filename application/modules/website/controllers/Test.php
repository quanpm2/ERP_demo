<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('unit_test');
		$this->load->library('domain');
	}
	public function index()
	{
		
		//test Expired Date
		$domains = array(
			'sccom.vn'=>'09/06/2016',
			'lamdep7ngay.com'=>'26/12/2016',

			);
		foreach($domains as $domain => $exp)
		{
			if(!$result = $this->scache->get('domain/whois-'.$domain))
			{
				$result = $this->domain->whois($domain);
				$this->scache->write($result, 'domain/whois-'.$domain);
			}

			$test_name = 'Test domain '.$domain.' - '.date('d/m/Y',$result['Expired Date']).' vs '.$exp;
			$this->unit->run(date('d/m/Y',$result['Expired Date']), $exp, $test_name);
		}

		echo $this->unit->report();
	}
	function t()
	{
		$r = $this->domain->whois('sccom.vn');
		prd(date('d/m/Y H:i:s',$r['Expired Date']));
		prd($r);
	}
}

/* End of file test.php */
/* Location: ./application/controllers/test.php */