<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['packages'] = array(
	'service' => array(

		'minimal' => array(
			'name' 			=> 'minimal',
			'label'			=> 'Phần mềm Pushdy',
			'icon' 			=> 'fa-paint-brush',
			'price' 		=> 2000000,
			'unit' 			=> 'month',
			'description' 	=> '
				Đối với Gói cước phần mềm mà Bên A đã mua, Phần mềm đã bao gồm chức năng/tính năng sau: <br/>
				☒ Chức năng Maxleads (ENTERPRISE) <br/>
				☒ Chức năng Smart Banner (ENTERPRISE) <br/>
				☒ Chức năng Recommendation (ENTERPRISE) <br/>
				☒ Chức năng Push Notification (ENTERPRISE) <br/>
			',
			'appendix'	=> null,
			'buy2get1' 	=> TRUE,
            'is_active' => FALSE,
		),

		'customize' => array(
			'name' 			=> 'customize',
			'label'			=> 'Phần mềm Pushdy',
			'icon' 			=> 'fa-paint-brush',
			'price' 		=> 2500000,
			'unit' 			=> 'month',
			'description' 	=> '
				Đối với Gói cước phần mềm mà Bên A đã mua, Phần mềm đã bao gồm chức năng/tính năng sau: <br/>
				☒ Chức năng Maxleads (ENTERPRISE) <br/>
				☒ Chức năng Smart Banner (ENTERPRISE) <br/>
				☒ Chức năng Recommendation (ENTERPRISE) <br/>
				☒ Chức năng Push Notification (ENTERPRISE) <br/>
			',
			'appendix'	=> null,
			'buy2get1' 	=> false,
            'is_active' => FALSE,
        ),

        'pushdy_04052022' => [
            'name' 		=> 'pushdy_04052022',
			'label'		=> 'Gói phần mềm Pushdy',
			'icon' 		=> 'fa-paint-brush',
			'price' 	=> 200000,
			'unit' 		=> 'package',
            'is_active' => TRUE,
        ],

        // 'pushdy_6_month' => [
        //     'name' 		=> 'pushdy_6_month',
		// 	'label'		=> 'Gói Pushdy 6 tháng',
		// 	'icon' 		=> 'fa-paint-brush',
		// 	'price' 	=> 1200000,
		// 	'unit' 		=> 'package',
        //     'is_active' => TRUE,
        // ],

        // 'pushdy_12_month' => [
        //     'name' 		=> 'pushdy_12_month',
		// 	'label'		=> 'Gói Pushdy 12 tháng',
		// 	'icon' 		=> 'fa-paint-brush',
		// 	'price' 	=> 2400000,
		// 	'unit' 		=> 'package',
        //     'is_active' => TRUE,
        // ],
	),
	'default' =>'pushdy_04052022'
);