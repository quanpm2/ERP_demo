<?php
class Pushdy_Package extends Package
{
	const ROOT_ROLE_ID = 1;

	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Get Name of Package
	 *
	 * @return     string  ( description_of_the_return_value )
	 */
	public function name()
	{
		return 'Pushdy';
	}

	/**
	 * Init Package
	 */
	public function init()
	{
		$this->_load_menu();
	}

	public function _load_menu()
	{
	}

	public function title()
	{
		return 'Pushdy';
	}

	public function author()
	{
		return 'thonh';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Pushdy';
	}

	private function init_permissions()
	{
		$permissions = array();

		$permissions['Contract.Pushdy'] = array(
			'description' 	=> 'Trang chính',
			'actions' 		=> [ 'manage', 'access' ]
		);

		$permissions['Pushdy.Index'] = array(
			'description' 	=> 'Trang chính',
			'actions' 		=> [ 'manage', 'access' ]
		);

		$permissions['Pushdy.Overview'] = array(
			'description' 	=> 'Trang tổng quan',
			'actions' 		=> [ 'manage', 'access' ]
		);

		$permissions['Pushdy.start_service'] = array(
			'description' 	=> 'Kích hoạt/thực hiện dịch vụ',
			'actions' 		=> [ 'update', 'manage', 'mdeparment', 'mgroup' ]
		);

		$permissions['Pushdy.stop_service'] = array(
			'description'	=> 'Kết thúc dịch vụ',
			'actions'		=> [ 'update', 'manage', 'mdeparment', 'mgroup' ]
		);

		return $permissions;
	}

	/**
	 * Install module
	 *
	 * @return     bool  status of command
	 */
	public function install()
	{
		$permissions = $this->init_permissions();
		if( ! $permissions) return FALSE;

		$rootPermissions = $this->role_permission_m->get_by_role_id(self::ROOT_ROLE_ID);
		$rootPermissions AND $rootPermissions = array_column($rootPermissions, 'role_id', 'permission_id');

		foreach($permissions as $name => $value)
		{
			$description   = $value['description'];
			$actions 	   = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);

			if( ! empty($rootPermissions[$permission_id])) continue;

			$this->role_permission_m->insert([
				'permission_id'	=> $permission_id,
				'role_id'		=> self::ROOT_ROLE_ID,
				'action'		=> serialize($actions)
			]);
		}

		return TRUE;
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		$pers = $this->permission_m->like('name','Pushdy')->get_many_by();

		if($pers)
		foreach($pers as $per)
		{
			$this->permission_m->delete_by_name($per->name);
		}
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}