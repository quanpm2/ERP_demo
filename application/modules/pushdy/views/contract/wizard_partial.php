<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->template->javascript->add(base_url("dist/vPushdyConfigurationContractBox.js"));

$formAction = admin_url("contract/create_wizard/index/{$edit->term_id}");
?>
<div class="col-md-12" id="service_tab">
	<form action="<?php echo $formAction;?>" method="post" accept-charset="utf-8">
		<input type="hidden" name="edit[term_id]" value="<?php echo $edit->term_id?>">
		<div id="vPushdyConfigurationContractBox">
		    <v-pushdy-configuration-contract-box :id="<?php echo $edit->term_id;?>" mode="update"></v-pushdy-configuration-contract-box>
		</div>
		<div class="form-group">
			<div class="form-group col-md-10">
				<div class="input-group col-sm-12">
					<input type="submit" name="confirm_step_service" value="confirm_step_service" id="confirm_step_service" style="display:none;" class="form-control ">
				</div>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript"> var app_root = new Vue({ el: '#service_tab' }); </script>