<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<br/>
<p><strong>Xét theo, </strong> nhu cầu của Bên A và khả năng cung cấp dịch vụ của Bên B. Hai bên thống nhất ký kết Hợp đồng cung cấp phần mềm  với các điều khoản cụ thể như sau:</p>

<p><u><strong>ĐIỀU 1. ĐỊNH NGHĨA</strong></u></p>

<ul style="padding-left:0;list-style:none">
	<li>1.1. "Phần mềm Pushdy": là phần mềm phân phối thông báo trên điện thoại và máy tính được xây dựng bởi Công ty Cổ phần Mobiletech. </li>
	<li>1.2. "Chức năng Smart Banner": là chức năng hiển thị các thông báo dựa theo hành vi của người sử dụng.</li>
	<li>1.3. "Chức năng Recommendation": là chức năng  gợi ý những sản phẩm phù hợp cho người dùng dựa trên phân tích hành vi sử dụng Web của họ.</li>
	<li>1.4. "Chức năng Web Push": là chức gửi thông báo đẩy tới trình duyệt web (web notification) của người sử dụng.</li>
	<li>1.5. "Chức năng Mobile Push": là chức năng gửi thông báo đẩy (push notification) đến thiết bị di động hệ điều hành iOS/Android của người sử dụng.</li>
	<li>1.6. "Chức năng Max Leads": là chức năng thiết lập các nút liên hệ đa kênh trên website.</li>
</ul>

<br/>
<p><u><strong>ĐIỀU 2. ĐỐI TƯỢNG CỦA HỢP ĐỒNG</strong></u></p>
<ul style="padding-left:0;list-style:none">
	<li>
		<p><b>2.1. Mức phí</b></p>
		<ul style="padding-left:0;list-style:none">
			<li>- Bên A đồng ý mua và Bên B đồng ý cung cấp quyền sử dụng Phần mềm phân phối thông báo Pushdy (sau đây gọi tắt là Phần mềm) với nội dung chi tiết sau:</li>
			<?php
			$this->table->clear();
			$this->table->set_template(['table_open' => '<table border="1" cellspacing="0" cellpadding="3" width="100%">']);
			$this->table->set_heading(['TT', 'Tên sản phẩm', 'Thời gian sử dụng', 'Thành tiền</br>(VNĐ)', 'Ghi chú']);

			$packageName = $service_package_config['label'];
			$packageName.= '<br/><i>(Sản phẩm không chịu thuế VAT)</i>';
			$packageName.= '<br/><b>Tên miền/ứng dụng cần kết nối :</b><br/>';
			$packageName.= $website;

			$contractDates = my_date($contract_begin, 'd/m/Y').' đến '.my_date($contract_end, 'd/m/Y');

			$_row = array(
				[ 'data' => 1, 'align' => 'center'],
			    $packageName,
			    [ 'data' => $contractDates, 'align' => 'center'],
			    [ 'data' => currency_numberformat($contract_value, ''), 'align'=>'right'],
			    [ 'data' => $service_package_config['description'] ?? '', 'align' => 'left'],
			);

			$this->table->add_row($_row);
			$this->table->add_row(null, array('data'=> '<p><em>Bằng chữ: ' . ucfirst(mb_strtolower(convert_number_to_words($contract_value))) .' đồng</em></p>', 'colspan'=>6)) ;

			echo $this->table->generate();
			?>
			<li>- Bên B đảm bảo cung cấp Phần mềm cho Bên A đúng với gói cước mà Bên A đã đăng ký.</li>
			<li>- Thời gian sử dụng của Phần mềm được tính bắt đầu từ ngày Bên B cung cấp cho Bên A Thông tin tài khoản đăng nhập để truy cập vào Phần mềm.</li>
		</ul>
	</li>
	<li>
		<p><b>2.2  Phương thức thanh toán</b></p>
		<p>Bên A thực hiện thanh toán thông qua chuyển khoản vào tài khoản ngân hàng của Bên B theo thông tin sau:</p>
		<?php if( ! empty($bank_info)) :?>
		<ul style="list-style:none">
		<?php foreach ($bank_info as $label => $text) :?>
		    <?php if (is_array($text)) : ?>
		        <?php foreach ($text as $key => $value) :?>
		            <li>- <?php echo $key;?>: <?php echo $value;?></li>
		        <?php endforeach;?>
		    <?php continue; endif;?>
		    <li>- <?php echo $label;?>: <?php echo $text;?></li>
		<?php endforeach;?>
		</ul>
		<p><b>Nội dung chuyển khoản: </b>  &lt;Tên Cty/ cá nhân&gt; thanh toán hợp đồng &lt;Số&gt; &lt;tên miền&gt;</p>
		<?php endif; ?>
		<p>Bên B xuất hóa đơn giá trị gia tăng cho Bên A theo thông tin sau:</p>
		<ul>
		    <li><u>Tên công ty</u>: <?php echo $customer->display_name ?? '';?></li>
		    <li><u>Địa chỉ</u>: <?php echo $data_customer['Địa chỉ'] ?? '';?></li>
		    <li><u>Mã số thuế</u>: <?php echo $data_customer['Mã số thuế'] ?? '';?></li>
		    <li>Hóa đơn điện tử được Bên B gửi cho Bên A theo địa chỉ email: <?php echo $customer->representative['email'] ?? '';?></li>
		</ul>
	</li>

	<li>
		<p><b>2.3. Thời hạn thanh toán:</b></p>

		<p>Bên A có trách nhiệm thanh toán cho Bên B 100% giá trị gói phần mềm ngay sau khi Hai Bên ký kết Hợp đồng</p>
		<p>Sau khi Hợp đồng được ký kết và thanh toán, mọi yêu cầu chấm dứt hay hủy bỏ đều không được chấp nhận. Khoản phí này sẽ không được hoàn trả trừ trường hợp Gói phần mềm không được cung cấp do lỗi của bên B.</p>
		<p>Trước ngày sử dụng hết Gói phần mềm đã thanh toán ở Điều 2, Bên B sẽ gửi Đề nghị thanh toán cho Bên A, trong Đề nghị thanh toán Bên B sẽ thông báo chi tiết về số tiền Gói phần mềm tiếp theo. Bên A sẽ thanh toán số tiền sử dụng Gói phần mềm tiếp theo cho Bên B trong vòng 05 ngày kể từ ngày nhận được Đề nghị thanh toán của Bên B. Quá thời hạn trên, nếu Bên A không thanh toán, Bên B có quyền chấm dứt Hợp đồng với Bên A.</p>
	</li>
</ul>

<p><u><b><strong>ĐIỀU 3. BÀN GIAO</strong></b></u></p>

<p>Bên B sẽ cung cấp cho Bên A thông tin tài khoản đăng nhập để truy cập vào Phần mềm theo địa chỉ email do Bên A cung cấp trong thời gian không quá 3 ngày làm việc kể từ khi Hai Bên ký kết Hợp đồng và Bên A thanh toán xong cước phí sử dụng Phần mềm đã nêu tại Điều 2.</p>

<p><u><b><strong>ĐIỀU 4. TRÁCH NHIỆM CÁC BÊN</strong></b></u></p>
<ul style="padding-left:0;list-style:none">
	<li>
		<p><b>4.1  Trách nhiệm Bên A</b></p>
		<ul style="padding-left:0;list-style:none">
			<li>- Bố trí máy tính và thiết bị cần thiết để có thể truy cập sử dụng Phần mềm qua mạng Internet.</li>
			<li>- Thực hiện đúng các quy định và hướng dẫn của Bên B.</li>
			<li>- Bảo mật và chịu trách nhiệm trong trường hợp làm lộ thông tin tài khoản đăng nhập và các thông tin do Bên B cung cấp. Nghĩa vụ bảo mật theo quy định tại Điều này vẫn có hiệu lực ngay cả khi quan hệ hợp tác giữa Hai Bên chấm dứt.</li>
			<li>- Chịu trách nhiệm hoàn toàn về nội dung thông tin, dữ liệu của Bên A khi sử dụng Phần mềm.</li>
			<li>- Thanh toán đầy đủ và đúng hạn Gói phần mềm theo quy định tại Điều 2.</li>
			<li>- Tuân thủ các nghĩa vụ khác quy định trong Hợp đồng</li>
		</ul>
	</li>
	<li>
		<p><b>4.3  Trách nhiệm Bên B</b></p>
		<ul style="padding-left:0;list-style:none">
			<li>- Đảm bảo duy trì hoạt động cho hệ thống để Bên A có thể truy cập sử dụng liên tục. </li>
			<li>- Bảo mật dữ liệu và thực hiện sao lưu dữ liệu nhằm đảm bảo an toàn dữ liệu của Bên A.</li>
			<li>- Không tiết lộ các yêu cầu, các tài liệu, dữ liệu và thông tin liên quan đến việc thực hiện Hợp đồng mà Bên A đã cung cấp. Nghĩa vụ bảo mật theo quy định tại Điều này vẫn có hiệu lực ngay cả khi quan hệ hợp tác giữa Hai Bên chấm dứt.</li>
			<li>- Hướng dẫn miễn phí cho Bên A sử dụng phần mềm</li>
			<li>- Xuất hóa đơn điện tử cho Bên A theo Gói Phần mềm mà Bên A đã đăng ký.</li>
			<li>- Tuân thủ các nghĩa vụ khác quy định trong Hợp đồng.</li>

		</ul>
	</li>
</ul>

<p><u><b><strong>ĐIỀU 5. NGỪNG QUYỀN SỬ DỤNG PHẦN MỀM</strong></b></u></p>

<ul style="padding-left: 0;list-style: none;">
	<li>
		<p>- Bên B sẽ ngừng quyền sử dụng Phần mềm của Bên A khi xảy ra một trong các trường hợp sau: </p>
		<ul>
			<li>Khi Bên A không hoàn thành nghĩa vụ thanh toán theo quy định tại Điều 2.</li>
			<li>Khi có yêu cầu từ cơ quan nhà nước có thẩm quyền.</li>
		</ul>
	</li>
	
	<li>- Khi xảy ra một trong các trường hợp trên, Bên B sẽ tiến hành ngừng quyền sử dụng Phần mềm đồng thời thông báo cho Bên A về việc ngừng quyền sử dụng.</li>
	<li>- Trong trường hợp có nhu cầu sử dụng lại Phần mềm, Bên A cần làm thủ tục đăng ký sử dụng lại với Bên B.</li>
</ul>

<p><u><b><strong>ĐIỀU 6. HỖ TRỢ SỬ DỤNG PHẦN MỀM</strong></b></u></p>
<p>- Bên B đảm bảo hỗ trợ miễn phí cho Bên A các vấn đề liên quan đến việc sử dụng Phần mềm trong suốt thời gian có hiệu lực của Hợp đồng.</p>
<p>- Trong quá trình sử dụng Phần mềm, nếu Bên A có bất kỳ vướng mắc hoặc sự cố nào phát sinh, Bên A sẽ thông báo cho bộ phận hỗ trợ kỹ thuật của Bên B.</p>
<p>- Thời gian hỗ trợ kỹ thuật: Trong giờ hành chính trừ thứ 7, chủ nhật và các ngày nghỉ, Lễ Tết theo quy định.</p>


<p><u><b><strong>ĐIỀU 7. NÂNG CẤP PHẦN MỀM</strong></b></u></p>
<p>- Trong trường hợp Bên A có nhu cầu nâng cấp Gói Phần mềm, Hai Bên sẽ tiến hành thỏa thuận để thực hiện tùy theo từng nhu cầu cụ thể.</p>

<p><u><b><strong>ĐIỀU 8. BẤT KHẢ KHÁNG</strong></b></u></p>
<p>- Sự kiện bất khả kháng là sự kiện xảy ra sau khi ký Hợp đồng, do những điều kiện bất thường xảy ra mang tính khách quan ngoài sự kiểm soát, các bên không thể lường trước được hoặc không thể khắc phục được.</p>
<p>- Việc một bên không hoàn thành nghĩa vụ của mình do sự kiện bất khả kháng sẽ không phải là cơ sở để bên kia chấm dứt Hợp đồng. Các bên phải tiến hành biện pháp ngăn ngừa hợp lý và các biện pháp thay thế cần thiết để hạn chế tối đa ảnh hưởng do sự kiện bất khả kháng gây ra.</p>
<p>- Khi xảy ra trường hợp bất khả kháng, hai bên phải kịp thời thông báo cho nhau về sự kiện đó và nguyên nhân gây ra sự kiện. Nếu bất khả kháng kéo dài 15 ngày, Hai Bên sẽ thương lượng để đi đến giải quyết thoả đáng cho tình huống đó.</p>
<br/>

<p><u><b><strong>ĐIỀU 9. CHẤM DỨT HỢP ĐỒNG</strong></b></u></p>
<p>Hợp đồng sẽ chấm dứt khi:</p>
<ul>
	<li>Bên A sử dụng hết gói Phần mềm mà Bên A không tiếp tục gia hạn thời gian sử dụng Phần mềm tiếp theo. Trong trường hợp này, Hợp đồng sẽ tự động thanh lý trong vòng 30 ngày kể từ ngày Bên A sử dụng hết gói Phần mềm.</li>
	<li>Hai Bên thỏa thuận về việc chấm dứt Hợp đồng.</li>
	<li>
		Một bên đơn phương chấm dứt Hợp đồng:
		<ul>
			<li>Bên A có quyền đơn phương chấm dứt Hợp đồng khi Bên B vi phạm các nghĩa vụ quy định trong Hợp đồng, vi phạm quy định của Pháp luật về việc cung cấp Phần mềm. Trong trường hợp này, Bên B có nghĩa vụ trả lại cho Bên A phần giá trị Hợp đồng còn lại tương ứng với thời gian chưa sử dụng.</li>
			<li>Bên B có quyền đơn phương chấm dứt Hợp đồng nếu Bên A vi phạm các nghĩa vụ quy định trong Hợp đồng, vi phạm quy định của Pháp luật về việc sử dụng Phần mềm. Trong trường hợp này, Bên B không phải hoàn trả bất kỳ chi phí gì mà Bên A đã thanh toán cho Bên B.</li>
			<li>Khi một trong Hai Bên vi phạm Hợp đồng, bên còn lại sẽ gửi thông báo bằng email cung cấp theo Hợp đồng này yêu cầu Bên vi phạm khắc phục và/hoặc sửa chữa những vi phạm đó. Trong vòng 05 ngày kể từ ngày Bên bị vi phạm gửi thông báo mà Bên vi phạm không khắc phục và/hoặc sửa chữa, Bên bị vi phạm có quyền Đơn phương chấm dứt Hợp đồng và không phải bồi thường bất kỳ thiệt hại nào phát sinh do việc chấm dứt Hợp đồng trong trường hợp đó.</li>

		</ul>
	</li>
</ul>

<p><u><b><strong>ĐIỀU 10. ĐIỀU KHOẢN THI HÀNH</strong></b></u></p>
<ul style="padding-left: 0;list-style: none">
	<li>- Hai Bên cam kết thực hiện nghiêm túc nội dung của Hợp đồng này. Bất kỳ vấn đề nào chưa được thỏa thuận hoặc chưa được quy định cụ thể trong Hợp đồng này sẽ được hiểu và vận dụng theo các quy định Pháp luật có liên quan.</li>
	<li>- Hai bên cam kết thực hiện những điều khoản trong hợp đồng. Nếu có vướng mắc, mỗi bên thông báo cho nhau để cùng bàn bạc giải quyết trên tinh thần hợp tác, thiện chí, vì lợi ích cả hai bên. Trong trường hợp không thể giải quyết được bất đồng, tranh chấp sẽ được giải quyết tại Tòa án có thẩm quyền tại TP Hồ Chí Minh.</li>
	<li>- Bất cứ sự thay đổi nào của Hợp đồng này chỉ có giá trị khi lập thành văn bản và được sự đồng ý của các bên có liên quan. 
	Hợp đồng này được lập thành 2 (hai) bản có giá trị pháp lý như nhau, mỗi bên giữ 1 (một) bản làm căn cứ thực hiện.</li>
</ul>