<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contracts extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'pushdy/pushdy_m';

        parent::__construct($config);

        $this->load->config('pushdy/pushdy');
    }


    /**
     * Get Contract
     *
     * @param      <type>  $id     The identifier
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function index_get($id)
    {
        if(FALSE == $this->pushdy_m->set_contract($id))
        {
            return parent::response([ 'code' => parent::HTTP_UNAUTHORIZED, 'error' => 'Không có quyền truy xuất hoặc hợp đồng không khả dụng !']);
        }

        if(FALSE === $this->pushdy_m->can('admin.contract.view'))
        {
            return parent::response([ 'code' => parent::HTTP_UNAUTHORIZED, 'error' => 'Không có quyền truy xuất hoặc hợp đồng không khả dụng !']);
        }

        $contract = $this->pushdy_m->get_contract();
        
        $contract->service_package          = get_term_meta_value($id, 'service_package') ?: 'pushdy_04052022';
        $contract->discount_amount          = (int) get_term_meta_value($id, 'discount_amount');
        $contract->price                    = (int) get_term_meta_value($id, 'price');
        $contract->quantity                 = (int) get_term_meta_value($id, 'quantity');

        parent::response([ 'data' => $contract, 'code' => parent::HTTP_OK ]);
    }


    public function service_put($id)
    {
        if(FALSE == $this->pushdy_m->set_contract($id))
        {
            parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng ! [0]', parent::HTTP_UNAUTHORIZED);
        }

        if(FALSE === $this->pushdy_m->can('admin.contract.view'))
        {
            parent::response('Không có quyền truy xuất hoặc hợp đồng không khả dụng ! [1]', parent::HTTP_UNAUTHORIZED);
        }

        $response = [ 'code' => 400, 'messages' => [] ];

        $this->load->library('form_validation');

        $args                   = parent::put(NULL, TRUE);

        $manipulation_locked = $this->option_m->get_value('manipulation_locked', TRUE);
        $is_manipulation_locked = (bool) $manipulation_locked['is_manipulation_locked']
                          && FALSE == (bool)get_term_meta_value($id, 'is_manipulation_locked');
        if($is_manipulation_locked)
        {
            $started_service = (int) get_term_meta_value($id, 'started_service') ?: time();
            $started_service = end_of_day($started_service);
            $manipulation_locked_at = end_of_day($manipulation_locked['manipulation_locked_at']);
            if($manipulation_locked_at > $started_service)
            {
                $manipulation_locked_at = my_date($manipulation_locked_at, 'd-m-Y');
                parent::response([ 'code' => parent::HTTP_NOT_ACCEPTABLE, 'error' => "Hợp đồng đã khoá thao tác lúc {$manipulation_locked_at}. Vui lòng liên hệ bộ phận kế toán để mở khoá."]);
            }
        }

        $service_package_config = $this->config->item('service', 'packages');
        $enumsServicePackages   = array_column($service_package_config, 'name');

        $discount_min_value = 0;
        $discount_max_value = PHP_INT_MAX;
        if( ! empty($service_package_config[$args['service_package']]['buy2get1']))
        {
            $discount_min_value = $args['price'] * floor(div($args['quantity'], 3));
            $discount_max_value = $args['price'] * floor(div($args['quantity'], 3));
        }
        else
        {
            $discount_max_value = $args['price'] * $args['quantity'];
        }

        $this->form_validation->set_data($args);
        $this->form_validation->set_rules('service_package', 'Gói dịch vụ', 'required|in_list['.implode(',', $enumsServicePackages).']');
        $this->form_validation->set_rules('quantity', 'Số năm', 'required|integer|greater_than_equal_to[1]');
        $this->form_validation->set_rules('price', 'Chi phí', 'required|integer');

        $this->form_validation->set_rules('discount_amount', 'Giảm giá', "required|integer|greater_than_equal_to[{$discount_min_value}]|less_than_equal_to[{$discount_max_value}]");

        if( FALSE == $this->form_validation->run())
        {
            parent::response([ 'code' => 400, 'error' => $this->form_validation->error_array()]);
        }
        
        if( ! empty($service_package_config[$args['service_package']]))
        {
            update_term_meta($id, 'service_package_config', serialize($service_package_config[$args['service_package']]));
        }

        isset($args['service_package']) AND update_term_meta($id, 'service_package', $args['service_package']);
        isset($args['price'])           AND update_term_meta($id, 'price', $args['price']);
        isset($args['quantity'])        AND update_term_meta($id, 'quantity', $args['quantity']);
        isset($args['discount_amount']) AND update_term_meta($id, 'discount_amount', $args['discount_amount']);

        $contract_begin = get_term_meta_value($id, 'contract_begin') ?: time();
        $contract_end = strtotime('+' . $args['quantity'] . ' months', $contract_begin);
        update_term_meta($id, 'contract_end', end_of_day($contract_end));

        try
        {
            update_term_meta($id, 'contract_value', $this->pushdy_m->calc_contract_value());

            $this->pushdy_m->delete_all_invoices(); // Delete all invoices available
            $this->pushdy_m->create_invoices(); // Create new contract's invoices
            $this->pushdy_m->sync_all_amount(); // Update all invoices's amount & receipt's amount

            $response['code']       = 200;
            $response['messages'][] = 'Đợt thanh toán đã đồng bộ thành công !';
            $response['messages'][] = 'Số liệu thu chi đã được đồng bộ thành công !';
        }
        catch (Exception $e)
        {
            parent::response([ 'code' => 400, 'error' => $e->getMessage() ]);
        }

        parent::response($response);
    }
}
/* End of file Contracts.php */
/* Location: ./application/modules/pushdy/controllers/api_v2/Contracts.php */