<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resource extends MREST_Controller
{
    /**
     * CONSTRUCTION
     *
     * @param      string  $config  The configuration
     */
    function __construct($config = 'rest')
    {
        $this->autoload['models'][] = 'pushdy/pushdy_m';

        parent::__construct($config);

        $this->load->config('pushdy/pushdy');
    }

    /**
     * /GET CONFIG
     */
    public function config_get()
    {
        if( ! has_permission('admin.contract.view')) parent::response([ 'code' => parent::HTTP_UNAUTHORIZED, 'error' => 'Quyền hạn không hợp lệ.' ]);

        $service_plan = $this->config->item('service', 'packages');
        $service_plan = array_reduce($service_plan, function($result, $item){
            $item['is_active'] AND $result[$item['name']] = $item;

            return $result;
        }, []);

        parent::response([
            'code' => parent::HTTP_OK,
            'data' => array(
                'services' => $service_plan,
                'default' => $this->config->item('default', 'packages')
            )
        ]);
    }
}
/* End of file Resource.php */
/* Location: ./application/modules/pushdy/controllers/api_v2/Resource.php */