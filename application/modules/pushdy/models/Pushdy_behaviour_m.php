<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Behaviour_m.php');

class Pushdy_behaviour_m extends Behaviour_m {

	function __construct()
	{
		parent::__construct();
		$this->load->config('pushdy/pushdy');
		$this->load->helper('date');
	}

	/**
	 * Proc Service
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	function proc_service()
	{
		$email = new \Notifications\Erp\Contract\ServiceStarted\Email(['contract' => $this->contract]);
		dd($email->render());
		parent::is_exist_contract(); // Determines if exist contract.
		$contractId = $this->contract->term_id;

		$this->init_setting_default($term_id);

		$this->send_mail2admin($term_id);

		update_term_meta($term_id, 'start_service_time',0);

		return true;






		dd('called in Behaviour_m');
		parent::is_exist_contract(); // Determines if exist contract.
		$contractId = $this->contract->term_id;

		$now = time();
		update_term_meta($contractId, 'start_service_time', $now);
		/* Luôn luôn lấy tỉ giá ngay tại thời điểm function được gọi*/
		update_term_meta($contractId, 'dollar_exchange_rate', get_exchange_rate());

		$this->log_m->insert(array(
			'log_type' =>'start_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $contractId,
			'log_content' => my_date($now, 'Y/m/d H:i:s')
			));

		$this->load->model('googleads/googleads_report_m');
		$stat = $this->googleads_report_m->send_activation_mail2customer($contractId);

		try
        {
        	$this->get_the_progress();
        	$this->sync_all_amount();
        }
        catch (Exception $e) {}

		return $stat;
	}

	/**
	 * Calculates the contract value.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     boolean    The contract value.
	 */
	public function calc_contract_value()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$term_id = $this->contract->term_id;

		$price = (int) get_term_meta_value($term_id, 'price');
		$quantity = ((int) get_term_meta_value($term_id, 'quantity')) ?: 1;
		$discount_amount = (int) get_term_meta_value($term_id ,'discount_amount');
		
		$result = ($price*$quantity) - $discount_amount;
		return max($result, 0);
	}

	/**
	 * Creates invoices.
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function create_invoices()
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$term_id 		= $this->contract->term_id;
		$contract_value = get_term_meta_value($term_id, 'contract_value') ?: 0;
		if(empty($contract_value)) throw new Exception("Giá trị hợp đồng ({$contract_value}) không hợp lệ.");

		$number_of_payments = 1;

		$contract_begin = get_term_meta_value($term_id, 'contract_begin');
		$contract_end 	= get_term_meta_value($term_id, 'contract_end');
		$num_dates 		= diffInDates($contract_begin,$contract_end);
		$num_days4inv 	= ceil(div($num_dates,$number_of_payments));

		$amount_per_payments = div($contract_value, $number_of_payments);
		
		$start_date 	= $contract_begin;
		$invoice_items 	= array();

		for($i = 0 ; $i < $number_of_payments; $i++)
		{	
			if($num_days4inv == 0) break;

			$end_date = $this->mdate->endOfDay(strtotime("+{$num_days4inv} day -1 second", $start_date));

			$inv_id = $this->invoice_m
			->insert(array(
				'post_title' => "Thu tiền đợt ". ($i + 1),
				'post_content' => "",
				'start_date' => $start_date,
				'end_date' => $end_date,
				'post_type' => $this->invoice_m->post_type
				));

			if(empty($inv_id)) continue;

			$this->term_posts_m->set_post_terms($inv_id, $term_id, $this->contract->term_type);

			$quantity = $num_days4inv;

			// Đợt 1: 50% giá trị HĐ
			if($number_of_payments == 3) {
				
				if($i == 0) {
					$amount_per_payments_3_count = (50 * $contract_value)/100 ;
				}

				// Đợt 2: 30% giá trị HĐ 
				elseif($i == 1) {
					$amount_per_payments_3_count = (30 * $contract_value)/100 ;
				}

				// Đợt 3: 20% giá trị HĐ
				elseif($i == 2) {
					$amount_per_payments_3_count = (20 * $contract_value)/100 ;
				}
			}

			$this->invoice_item_m->insert(array(
					'invi_title' => 'Giá dịch vụ',
					'inv_id' => $inv_id,
					'invi_description' => '',
					'invi_status' => 'publish',
					//'price' => $amount_per_payments,
					'price' => ($number_of_payments == 3) ? $amount_per_payments_3_count : $amount_per_payments,
					'quantity' => 1,
					'invi_rate' => 100,
					'total' => $this->invoice_item_m->calc_total_price(($number_of_payments == 3) ? $amount_per_payments_3_count : $amount_per_payments, 1, 100)
				));

			$start_date = strtotime('+1 second', $end_date);

			$day_end = $num_dates - $num_days4inv;
			if($day_end < $num_days4inv)
			{
				$num_days4inv = $day_end;
			}
		}
		return TRUE;
	}


	/**
	 * Preview data for printable contract version
	 *
	 * @return     String  HTML content
	 */
	public function prepare_preview()
	{
		if( ! $this->contract) return FALSE;

		parent::prepare_preview();

		$data 		= $this->data;
		$term_id 	= $this->contract->term_id;

		$service_package_config = $this->config->item('service', 'packages');
		$service_package 		= get_term_meta_value($term_id,'service_package') ?: $this->config->item('default', 'packages');
		$printable_title 		= $this->config->item($this->contract->term_type, 'printable_title');
		
		/* Thông tin người đại diện duyệt sản phẩm */
		$data['qc_confirm_name'] 	= (get_term_meta_value($term_id, 'qc_confirm_gender') == 1 ? 'Ông ' : 'Bà ').str_repeat('&nbsp', 1);
		$data['qc_confirm_name']	.= get_term_meta_value($term_id,'qc_confirm_name');
		$data['qc_confirm_email'] 	= get_term_meta_value($term_id, 'qc_confirm_email');
		$data['qc_confirm_phone'] 	= get_term_meta_value($term_id, 'qc_confirm_phone');

		$data['service_package']		= $service_package;
		$data['printable_title'] 		= $printable_title[$service_package] ?? $printable_title;

		$data['price'] 	= (int) get_term_meta_value($term_id, 'price');
		$data['quantity'] 					= (int) get_term_meta_value($term_id, 'quantity');
		$data['discount_amount'] 		= (int) get_term_meta_value($term_id, 'discount_amount');
		
		$data['payment_bank_account'] 	= get_term_meta_value($term_id, 'payment_bank_account') ?: 'company';
		$data['bank_info'] 				= $this->config->item('company', 'bank_infos');

		$representative_zone 	= get_term_meta_value($term_id, 'representative_zone') ?: 'hcm';
		if('person' == $data['payment_bank_account'])
		{
			$bank_info 			= $this->config->item('person', 'bank_infos');
			$data['bank_info'] 	= array_map(function($bankInfo){
				if(isset($bankInfo['representative_zone'])) unset($bankInfo['representative_zone']);
				return $bankInfo;
			}, array_filter($bank_info, function($x) use ($representative_zone){
				return $x['representative_zone'] == $representative_zone;
			}));	
		}

		$data['discount_amount'] 	= (int) get_term_meta_value($term_id ,'discount_amount');

		/* Chi tiết các đợt thanh toán */
		// $data['invoices']		 	= $this->term_posts_m->get_term_posts($term_id, $this->invoice_m->post_type, ['orderby'=>'posts.end_date']);
		// $data['number_of_payments'] = count($data['invoices']);

		$data['service_package_config'] = get_term_meta_value($term_id, 'service_package_config');
		$data['service_package_config'] AND $data['service_package_config'] = unserialize($data['service_package_config']);

		$data['website'] = prep_url($this->contract->term_name);
        
        $data['view_file'] = 'pushdy/contract/preview';
        
        $verified_on = get_term_meta_value($this->contract->term_id, 'verified_on') ?: time();
		$verified_on > end_of_day(strtotime('2022/04/19')) AND $data['view_file'] = 'pushdy/contract/preview_after_20_04_2022';

		return $data;
	}

	/**
	 * Sends an activation email.
	 *
	 * @param      string     $type_to  The type to
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     <type>     ( description_of_the_return_value )
	 */
	public function send_activation_email($type_to = 'admin')
	{
		if( ! $this->contract) throw new Exception("Hợp đồng chưa được khởi tạo.");

		$this->load->model('pushdy/pushdy_report_m');
		return $this->pushdy_report_m->init($this->contract->term_id)->send_activation_email($type_to);
	}
}
/* End of file Pushdy_behaviour_m.php */
/* Location: ./application/modules/pushdy/models/Pushdy_behaviour_m.php */