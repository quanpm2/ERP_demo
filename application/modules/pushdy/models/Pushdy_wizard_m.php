<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Contract_wizard_m.php');

class Pushdy_wizard_m extends Contract_wizard_m {

	function __construct()
	{
		parent::__construct();
		$this->load->config('pushdy/pushdy');
		$this->load->model('pushdy/pushdy_m');
	}


	/**
	 * Add Hook when in model change service detail
	 *
	 * @param      Array  $post   The post
	 */
	public function wizard_act_service($post)
	{
		parent::wizard_act_service($post);

		$this->hook->add_filter('ajax_act_service',	function($post){

			$term_id 	= $post['edit']['term_id'] ?? 0;
			$metadata 	= $post['meta'] ?? [];

			if(FALSE == $this->pushdy_m->set_contract($term_id)) throw new Exception("Loại hợp đồng không hợp lệ.");

			// Cập nhật thông số chi tiết dịch vụ
			$this->load->config('pushdy/pushdy');
			$service_package_config = $this->config->item('service', 'packages');
            $service_package_config = array_reduce($service_package_config, function($result, $item){$item['is_active'] AND $result[$item['name']] = $item; return $result;}, []);
			$enumsServicePackages = array_column($service_package_config, 'name');

			$this->load->library('form_validation');
			$this->form_validation->set_data($metadata);
			$this->form_validation->set_rules('service_package', 'Gói dịch vụ', 'required|in_list['.implode(',', $enumsServicePackages).']');
			$this->form_validation->set_rules('quantity', 'Số lượng', 'required|integer|greater_than_equal_to[1]');
			$this->form_validation->set_rules('price', 'Chi phí', 'required|integer');

            $discount_min_value = 0;
            $discount_max_value = $metadata['price'] * $metadata['quantity'];

			$this->form_validation->set_rules('discount_amount', 'Giảm giá', "required|integer|greater_than_equal_to[{$discount_min_value}]|less_than_equal_to[{$discount_max_value}]");

			if(FALSE == $this->form_validation->run()) throw new Exception( json_encode($this->form_validation->error_array()));

			if( ! empty($service_package_config[$metadata['service_package']]))
	        {
	            update_term_meta($term_id, 'service_package_config', serialize($service_package_config[$metadata['service_package']]));
	        }

			isset($metadata['service_package']) AND update_term_meta($term_id, 'service_package', $metadata['service_package']);
			isset($metadata['price']) AND update_term_meta($term_id, 'price', (int) $metadata['price']);
			isset($metadata['quantity']) AND update_term_meta($term_id, 'quantity', (int) $metadata['quantity']);
			isset($metadata['discount_amount']) AND update_term_meta($term_id, 'discount_amount', (int) $metadata['discount_amount']);

			$contract_begin = get_term_meta_value($term_id, 'contract_begin') ?: start_of_day();

			$contract_end = strtotime('+' . (int)$metadata['quantity'] . ' months', $contract_begin);
			update_term_meta($term_id, 'contract_end', end_of_day($contract_end));
			
			try
			{
				update_term_meta($term_id, 'contract_value', $this->pushdy_m->calc_contract_value());
			}
			catch (Exception $e)
			{
				return $post;
			}
			
			return $post;
			
		}, 9);
	}
}
/* End of file Pushdy_wizard_m.php */
/* Location: ./application/modules/pushdy/models/Pushdy_wizard_m.php */