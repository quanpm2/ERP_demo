<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Contract_m.php');

class Pushdy_m extends Contract_m {

	public $term_type = 'pushdy';

	public function start_service()
	{
		return $this->proc_service();
	}

	public function proc_service()
	{
		if( ! $this->contract) throw new Exception("ID Hợp đồng không hợp lệ");
		return $this->get_behaviour_m()->proc_service();
	}
}
/* End of file Pushdy_m.php */
/* Location: ./application/modules/pushdy/models/Pushdy_m.php */