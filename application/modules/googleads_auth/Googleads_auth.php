<?php
class Googleads_auth_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name(){
		
		return 'Googleads_auth';
	}

	public function init(){	

		if(has_permission('Admin.Googleads_auth') && is_module_active('googleads_auth'))
		{

		}
	}

	public function title()
	{
		return 'Googleads_auth';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '0.1';
	}

	public function description()
	{
		return 'Googleads_auth';
	}
	private function init_permissions()
	{
		$permissions = array();
		$permissions['Admin.Googleads_auth'] = array(
			'description' => 'Quản lý Googleads_auth',
			'actions' => array('view','add','edit','delete','update'));

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}
	}
}