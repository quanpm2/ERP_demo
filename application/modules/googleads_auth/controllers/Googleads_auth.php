<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Google\Auth\CredentialsLoader;
use Google\Auth\OAuth2;

use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\v201809\cm\OrderBy;
use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\SortOrder;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomerService;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\AdWords\v201809\mcm\CustomerService;


class Googleads_auth extends Admin_Controller {

	const PAGE_LIMIT = 500;

	public $model = '';

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$auth_config = [
		'authorizationUri' => 'https://accounts.google.com/o/oauth2/v2/auth',
		'tokenCredentialUri' => CredentialsLoader::TOKEN_CREDENTIAL_URI,
		'redirectUri' => base_url('googleads_auth'),
		'clientId' => getenv('OAUTH_CLIENT_ID'),
		'clientSecret' => getenv('OAUTH_CLIENT_SECRET'),
		'scope' => 'https://www.googleapis.com/auth/adwords'
		];

		$oauth2 = new OAuth2($auth_config);

		if (!isset($_GET['code'])) 
		{
			// Create a 'state' token to prevent request forgery.
			// Store it in the session for later validation.
			$oauth2->setState(sha1(openssl_random_pseudo_bytes(1024)));
			$_SESSION['oauth2state'] = $oauth2->getState();

			// Redirect the user to the authorization URL.
			$config = ['access_type' => 'offline'];

			header('Location: ' . $oauth2->buildFullAuthorizationUri($config));
			exit;
		}

		$code = $_GET['code'];
		$oauth2->setCode($code);
		$authToken = $oauth2->fetchAuthToken();
		prd($authToken);

		printf("Your refresh token is: %s\n\n", $authToken['refresh_token']);
		printf(
			"Copy the following lines to your 'adsapi_php.ini' file:\n"
			. "clientId = \"%s\"\n"
			. "clientSecret = \"%s\"\n"
			. "refreshToken = \"%s\"\n",
			$clientId,
			$clientSecret,
			$authToken['refresh_token']
			);
	}

	public function get_customers()
	{
		$oAuth2Credential = (new OAuth2TokenBuilder())
		->withClientId("")
		->withClientSecret("")
		->withRefreshToken("")
		->build();

		$session = (new AdWordsSessionBuilder())
		->withDeveloperToken($this->config->item('googleadsDeveloperToken'))
		->withUserAgent("example.com:ReportDownloader:V3.2")
		->withOAuth2Credential($oAuth2Credential)
		->build();
		
		$adWordsServices = new AdWordsServices();
		$customerService = $adWordsServices->get($session, CustomerService::class);
		prd($customerService->getCustomers());
	}

	public function runExample(AdWordsServices $adWordsServices,AdWordsSession $session) 
	{
		$managedCustomerService = $adWordsServices->get($session, ManagedCustomerService::class);

    	// Create selector.
		$selector = new Selector();
		$selector->setFields(['CustomerId', 'Name','CanManageClients']);
		$selector->setOrdering([new OrderBy('CustomerId', SortOrder::ASCENDING)]);
		$selector->setPaging(new Paging(0, self::PAGE_LIMIT));

    	// Maps from customer IDs to accounts and links.
		$customerIdsToAccounts = [];
		$customerIdsToChildLinks = [];
		$customerIdsToParentLinks = [];

		$totalNumEntries = 0;
		do 
		{
      		// Make the get request.
			$page = $managedCustomerService->get($selector);

      		// Create links between manager and clients.
			if ($page->getEntries() !== null) 
			{
				$totalNumEntries = $page->getTotalNumEntries();
				if ($page->getLinks() !== null) 
				{
					foreach ($page->getLinks() as $link) 
					{
						$customerIdsToChildLinks[$link->getManagerCustomerId()][] = $link;
						$customerIdsToParentLinks[$link->getClientCustomerId()] = $link;
					}
				}

				foreach ($page->getEntries() as $account) 
				{
					$customerIdsToAccounts[$account->getCustomerId()] = $account;
				}
			}

      		// Advance the paging index.
			$selector->getPaging()->setStartIndex($selector->getPaging()->getStartIndex() + self::PAGE_LIMIT);

		} 
		while ($selector->getPaging()->getStartIndex() < $totalNumEntries);

    	// Find the root account.
		$rootAccount = null;
		foreach ($customerIdsToAccounts as $account) 
		{
			if (!array_key_exists($account->getCustomerId(),$customerIdsToParentLinks)) 
			{
				$rootAccount = $account;
				break;
			}
		}

		if ($rootAccount !== null) 
		{
	      	// Display results.
			$this->printAccountHierarchy($rootAccount, $customerIdsToAccounts, $customerIdsToChildLinks);
		} 
		else 
		{
			printf("No accounts were found.\n");
		}
	}

	public function getHierachyAccount()
	{
		$oAuth2Credential = (new OAuth2TokenBuilder())
		->withClientId("")
		->withClientSecret("")
		->withRefreshToken("")
		->build();
		
		$session = (new AdWordsSessionBuilder())
		->withDeveloperToken($this->config->item('googleadsDeveloperToken'))
		->withUserAgent("example.com:ReportDownloader:V3.2")
		->withClientCustomerId("")
		->withOAuth2Credential($oAuth2Credential)
		->build();

		$this->runExample(new AdWordsServices(), $session);
	}
	
	private function printAccountHierarchy($account,$customerIdsToAccounts, $customerIdsToChildLinks, $depth = null) 
	{
		if ($depth === null) 
		{
			print "(Customer ID, Account Name)\n";
			$this->printAccountHierarchy($account, $customerIdsToAccounts, $customerIdsToChildLinks, 0);
			return;
		}

		print str_repeat('-', $depth * 2);
		$customerId = $account->getCustomerId();
		printf("%s, %s\n", $customerId, $account->getName());

		if (array_key_exists($customerId, $customerIdsToChildLinks)) 
		{
			foreach ($customerIdsToChildLinks[$customerId] as $childLink) 
			{
				$childAccount = $customerIdsToAccounts[$childLink->getClientCustomerId()];
				$this->printAccountHierarchy($childAccount, $customerIdsToAccounts,$customerIdsToChildLinks, $depth + 1);
			}
		}
	}
}