<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['packages'] = array(
    'service' => array(
        '.com'         => array(
            'name'                  => '.com',
            'label'                 => 'Tên miền (.com)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .com  : ',
            'label_maintain'        => 'Phí duy trì tên miền .com : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 0 ,  // phí khởi tạo
            'fee_maintain'          => 357000 ,// phí duy trì 
            'fee_initialization_maintain'    => 0 + 298000,
            'status' => 'active'
        ),
        
        '.vn'         => array(
            'name'                  => '.vn',
            'label'                 => 'Tên miền (.vn)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .vn  : ',
            'label_maintain'        => 'Phí duy trì tên miền .vn : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 280000 ,  // phí khởi tạo
            'fee_maintain'          => 969000 ,// phí duy trì 
            'fee_initialization_maintain'    => 280000 + 528000,
            'status' => 'active'
        ),

        '.com.vn'         => array(
            'name'                  => '.com.vn',
            'label'                 => 'Tên miền (.com.vn)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .com.vn  : ',
            'label_maintain'        => 'Phí duy trì tên miền .com.vn : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 280000 ,  // phí khởi tạo
            'fee_maintain'          => 370000 ,// phí duy trì 
            'fee_initialization_maintain'    => 280000 + 411000,
            'status' => 'active'
        ),

        '.net.vn'         => array(
            'name'                  => '.net.vn',
            'label'                 => 'Tên miền (.net.vn)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .net.vn  : ',
            'label_maintain'        => 'Phí duy trì tên miền .net.vn : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 280000 ,  // phí khởi tạo
            'fee_maintain'          => 370000 ,// phí duy trì 
            'fee_initialization_maintain'    => 280000 + 411000,
            'status' => 'active'
        ),

        '.edu.vn'         => array(
            'name'                  => '.edu.vn',
            'label'                 => 'Tên miền (.edu.vn)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .edu.vn  : ',
            'label_maintain'        => 'Phí duy trì tên miền .edu.vn : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 190000 ,  // phí khởi tạo
            'fee_maintain'          => 215000 ,// phí duy trì 
            'fee_initialization_maintain'    => 190000 + 201000,
            'status' => 'active'
        ),

        '.org.vn'         => array(
            'name'                  => '.org.vn',
            'label'                 => 'Tên miền (.org.vn)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .org.vn  : ',
            'label_maintain'        => 'Phí duy trì tên miền .org.vn : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 190000 ,  // phí khởi tạo
            'fee_maintain'          => 215000 ,// phí duy trì 
            'fee_initialization_maintain'    => 190000 + 201000,
            'status' => 'active'
        ),

        '.info.vn'         => array(
            'name'                  => '.info.vn',
            'label'                 => 'Tên miền (.info.vn)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .info.vn  : ',
            'label_maintain'        => 'Phí duy trì tên miền .info.vn : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 190000 ,  // phí khởi tạo
            'fee_maintain'          => 215000 ,// phí duy trì 
            'fee_initialization_maintain'    => 190000 + 201000,
            'status' => 'active'
        ),
        
        '.org'         => array(
            'name'                  => '.org',
            'label'                 => 'Tên miền (.org)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .org  : ',
            'label_maintain'        => 'Phí duy trì tên miền .org : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 0 ,  // phí khởi tạo
            'fee_maintain'          => 333000 ,// phí duy trì 
            'fee_initialization_maintain'    => 0 + 278000,
            'status' => 'active'
        ),
        
        '.net'         => array(
            'name'                  => '.net',
            'label'                 => 'Tên miền (.net)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .net  : ',
            'label_maintain'        => 'Phí duy trì tên miền .net : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 0 ,  // phí khởi tạo
            'fee_maintain'          => 357000 ,// phí duy trì 
            'fee_initialization_maintain'    => 0 + 298000,
            'status' => 'active'
        ),

        '.asia'         => array(
            'name'                  => '.asia',
            'label'                 => 'Tên miền (.asia)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .asia  : ',
            'label_maintain'        => 'Phí duy trì tên miền .asia : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 0 ,  // phí khởi tạo
            'fee_maintain'          => 410000 ,// phí duy trì 
            'fee_initialization_maintain'    => 0 + 342000,
            'status' => 'active'
        ),
        
        '.info'         => array(
            'name'                  => '.info',
            'label'                 => 'Tên miền (.info)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .info  : ',
            'label_maintain'        => 'Phí duy trì tên miền .info : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 0 ,  // phí khởi tạo
            'fee_maintain'          => 298000 ,// phí duy trì 
            'fee_initialization_maintain'    => 0 + 249000,
            'status' => 'active'
        ),
        
        '.co'         => array(
            'name'                  => '.co',
            'label'                 => 'Tên miền (.co)', 
            'label_initialization'  => 'Phí khởi tạo tên miền .co  : ',
            'label_maintain'        => 'Phí duy trì tên miền .co : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_initialization'    => 0 ,  // phí khởi tạo
            'fee_maintain'          => 700000 ,// phí duy trì 
            'fee_initialization_maintain'    => 0 + 584000,
            'status' => 'active'
        ),

        'domain_international' => array(
            'label'                 => 'Tên miền quốc tế ( .com, .net, .org…)',
            'name'                  => 'domain_international',
            'label_initialization'  => 'Phí khởi tạo tên miền quốc tế:',
            'label_maintain'        => 'Phí duy trì tên miền quốc tế ( .com,.net,…) :',
            'label_initialization_maintain' => 'Phí duy trì hàng năm là : ',
            'fee_initialization'    => 0,       // phí khởi tạo
            'fee_maintain'          => 239000,   // phí duy trì
            'fee_initialization_maintain'    => 0 + 239000,
            'status' => 'unactive'
        ),
        
        'domain_vietnam_com_vn' => array(
            'label' => 'Tên miền Việt Nam (.com.vn, .net.vn…)',
            'name'  => 'domain_vietnam_com_vn',  
            'fee_initialization'    => 200000 ,  // phí khởi tạo
            'label_initialization'  => 'Phí khởi tạo tên miền Việt Nam  : ',
            'label_maintain'        => 'Phí duy trì tên miền Việt Nam ( .com.vn, .net.vn, ...) : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_maintain'          => 495000 ,// phí duy trì 
            'fee_initialization_maintain'    => 200000 + 495000,
            'status' => 'unactive'
        ),

        'domain_vietnam_vn'         => array(
            'label'                 => 'Tên miền Việt Nam (.vn)', 
            'name'                  => 'domain_vietnam_vn',
            'fee_initialization'    => 200000 ,  // phí khởi tạo
            'label_initialization'  => 'Phí khởi tạo tên miền Việt Nam  : ',
            'label_maintain'        => 'Phí duy trì tên miền Việt Nam ( .com.vn, .net.vn, ...) : ',
            'label_initialization_maintain' => 'Phí khởi tạo và duy trì năm đầu tiên là :',
            'fee_maintain'          => 550000 ,// phí duy trì 
            'fee_initialization_maintain'    => 550000 + 200000,
            'status' => 'unactive'
        ),
    ),
    'default'       =>'domain_international' 
);

$config['number_year_register'] = array(
    '1' => '1 năm',
    '2' => '2 năm',
    '3' => '3 năm',
    '4' => '4 năm',
    '5' => '5 năm',
    '6' => '6 năm',
    '7' => '7 năm',
    '8' => '8 năm',
    '9' => '9 năm',
    '10' => '10 năm'
) ;

$config['times'] = array(
    // THỜI GIAN HIỆN TẠI
    'now_time'                              => time(),
    //'now_time'                              => strtotime('2018/09/01'),

    // TRƯỚC BAO NHIỀU NGÀY GỬI MAIL ĐỂ THÔNG BÁO SẮP HẾT HẠN HỢP ĐỒNG
    'number_day_alert_before_when_off'      => 30 ,

    // TRONG BAO NHIÊU NGÀY NHẮC NHỞ ADMIN TẮT DOMAIN
    'number_day_alert_admin_off_domain'      => 3
) ;