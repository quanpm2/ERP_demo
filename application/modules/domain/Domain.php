<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Domain_Package extends Package
{
	function __construct()
	{
		parent::__construct();
	}

	public function name()
	{
		return 'Quản lý hợp đồng Domain';
	}

	public function init()
	{
		$this->_load_menu();
		$this->_update_permissions();
	}

	/**
	 * Init menu LEFT | NAV ITEM FOR MODULE
	 * then check permission before render UI
	 */
	private function _load_menu()
	{
		$order = 1;
		$itemId = 'admin-domain';
 
 		if(!is_module_active('domain')) return FALSE;	
 		
		if(has_permission('domain.index.access'))
		{
			$this->menu->add_item(array(
				'id'        => $itemId,
				'name' 	    => 'Domain',
				'parent'    => null,
				'slug'      => admin_url('domain'),
				'order'     => $order++,
				'icon' => 'fa fa-database',
				'is_active' => is_module('domain')
				),'left-service');	
		}

		if(!is_module('domain')) return FALSE;
		if(has_permission('domain.index.access'))
		{
			// $this->menu->add_item(array(
			// 'id' => 'domain-service',
			// 'name' => 'Đang thực hiện',
			// 'parent' => $itemId,
			// 'slug' => admin_url('domain'),
			// 'order' => $order++,
			// 'icon' => 'fa fa-fw fa-xs fa-circle-o'
			// ), 'left-service');

            $this->menu->add_item(array(
                'id' => 'domain-service',
                'name' => 'Tổng quan',
                'parent' => $itemId,
                'slug' => module_url('domain'),
                'order' => $order++,
                'icon' => 'fa fa-fw fa-xs fa-circle-o'
                ), 'left-service');
		}
		
		$left_navs = array(
						'overview'=> array(
							'name' => 'Tổng quan',
							'icon' => 'fa fa-fw fa-xs fa-tachometer',
						)
					) ;

		$term_id = $this->uri->segment(4);

		$this->website_id = $term_id;

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("domain.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "domain-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
				), 'left-service');
		}

		
	}

	public function title()
	{
		return 'Domain';
	}

	public function author()
	{
		return 'tambt';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Quản lý tên miền(domain)';
	}
	
	/**
	 * Init default permissions for domain module
	 *
	 * @return     array  permissions default array
	 */
	private function init_permissions()
	{
		return array(

			'domain.index' => array(
				'description' => 'Quản lý domain',
				'actions' => ['manage','access','update','add','delete']),

			'domain.done' => array(
				'description' => 'Dịch vụ đã xong',
				'actions' => ['manage','access','add','delete','update']),
		
			'domain.overview' => array(
				'description' => 'Tổng quan',
				'actions' => ['manage','access','add','delete','update']),
	
			'domain.start_service' => array(
				'description' => 'Kích hoạt domain',
				'actions' => ['manage','access','update']),
			
			'domain.stop_service' => array(
				'description' => 'Ngừng domain',
				'actions' => ['manage','access','update']),
			
			'domain.renew' => array(
				'description' => 'Gia hạn hợp đồng cho thuê domain',
				'actions' => ['manage','update'])
			);
	}

	private function _update_permissions()
	{
		$permissions = array();
	
		if(!$permissions) return false;
		
		foreach($permissions as $name => $value){
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	/**
	 * Install module
	 *
	 * @return     bool  status of command
	 */
	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);
			$this->role_permission_m->insert(['role_id'=>1,'permission_id'=>$permission_id,'action'=>serialize($actions)]);
		}

		return TRUE;
	}

	/**
	 * Uninstall module command
	 *
	 * @return     bool  status of command
	 */
	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		if($pers = $this->permission_m->like('name','domain')->get_many_by())
		{
			foreach($pers as $per)
			{
				$this->permission_m->delete_by_name($per->name);
			}
		}

		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}

		return TRUE;
	}
}
/* End of file domain.php */
/* Location: ./application/modules/domain/models/domain.php */