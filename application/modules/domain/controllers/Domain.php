<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Domain extends Admin_Controller 
{
	private $term;
	private $term_type = 'domain';
	public  $model = 'domain_contract_m';

	public function __construct(){

		parent::__construct();
		
		$this->load->model('term_users_m');
		$this->load->model('domain/domain_contract_m');
		$this->load->model('domain/domain_m');
		$this->load->model('domain/domain_report_m');
		$this->load->model('customer/customer_m');
		$this->load->model('contract/contract_m');
		$this->load->model('contract/contract_wizard_m');
		$this->load->model('contract/invoice_m');
		$this->load->model('webgeneral/webgeneral_kpi_m');

		$this->load->model('message/message_m');
		$this->load->model('message/note_contract_m'); 

		$this->load->model('staffs/sale_m'); 

		$this->load->config('contract/contract');
		$this->load->config('domain/domain') ;
		$this->load->config('staffs/group');
		$this->load->config('contract/invoice');
		$this->config->load('domain/group')   ;

		// kiểm tra Domain có tồn tại và user có quyền truy cập ?
		$this->init_website();
	}

	private function init_website()
	{
		$term_id = $this->uri->segment(4);
		$method  = $this->uri->segment(3);
		$is_allowed_method = (!in_array($method, array('index','done')));
		if(!$is_allowed_method || empty($term_id)) return FALSE;

		$term = $this->term_m
		->where('term_status','publish')
		->where('term_type',$this->term_type)
		->where('term_id',$term_id)
		->get_by();

		if(empty($term) OR !$this->is_assigned($term_id)) 
		{
			$this->messages->error('Truy cập #'.$term_id.' không hợp lệ !!!');
			redirect(module_url(),'refresh');
		}

		$this->template->title->set(strtoupper(' '.$term->term_name));
		$this->website_id = $this->data['term_id'] = $term_id;
		$this->data['term'] = $this->term = $term;
	}

    public function index()
	{
		restrict('hosting.index.access');
        
        $this->template->is_box_open->set(1);
		$this->template->title->set('Tổng quan dịch vụ tên miền');
		$this->template->javascript->add(base_url("dist/vDomainIndex.js?v=1"));
		$this->template->stylesheet->add('plugins/daterangepicker/daterangepicker-bs3.css');
		$this->template->javascript->add('plugins/daterangepicker/moment.min.js');
		$this->template->javascript->add('plugins/daterangepicker/daterangepicker.js');
		$this->template->stylesheet->add('https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css');
		$this->template->stylesheet->add('https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css');
		$this->template->stylesheet->add(base_url('node_modules/vue2-daterange-picker/dist/vue2-daterange-picker.css'));
		$this->template->stylesheet->add(base_url('node_modules/vue2-dropzone/dist/vue2Dropzone.min.css'));
		$this->template->publish();
	}

	public function overview($term_id = 0)
	{
		restrict('domain.Overview.Access');

		$term = $this->term_m->get($term_id);

		if(empty($term)){
			$this->messages->error('Dịch vụ không tồn tại');
			redirect(module_url(),'refresh');
		}

		$time = time();	
		$day = date('d',$time) - 1;

		if($day == 0){
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}

		$contract_begin        = get_term_meta_value($term_id, 'contract_begin');
    	$contract_end          = get_term_meta_value($term_id, 'contract_end');

    	$date_start  		   = date('d/m/Y', $contract_begin);
		$date_end 	 		   = date('d/m/Y', $contract_end);


		$description = 'Từ '.$date_start.' đến '.$date_end;

		$time_start  = $this->mdate->startOfDay(strtotime($date_start));
		$time_end    = $this->mdate->endOfDay(strtotime($date_end));

		$this->template->description->append($description);
		$this->template->title->prepend('Tổng quan');
		$this->template->javascript->add('plugins/chartjs/Chart.js');

		/* Load danh sách nhân viên kinh doanh */
		$staffs = $this->sale_m->select('user_id,user_email,display_name')->set_user_type()->set_role()->as_array()->get_all();
		$staffs = array_map(function($x){ $x['display_name'] = $x['display_name'] ?: $x['user_email']; return $x; }, $staffs);
		$data['staffs'] = key_value($staffs, 'user_id', 'display_name');

		$data['websites'] = array();

		if(!empty($data['cus_belongs'])){
			$_webs = $this->term_users_m
			->where(array('term_users.user_id' => reset($data['cus_belongs']),'term.term_type'=>'website'))
			->join('term','term.term_id = term_users.term_id')
			->get_many_by();
			if(!empty($_webs))
				foreach ($_webs as $w)
					$data['websites'][$w->term_id] = $w->term_name;
		}

		if(!empty($data['edit']->term_parent)){
			$bonus_website = $this->contract_m->get($data['edit']->term_parent);
			if(!empty($bonus_website))
				$data['websites'][$bonus_website->term_id] = $bonus_website->term_name;
		}

		//$data['ajax_segment'] = "contract/ajax_dipatcher/ajax_edit/{$data['edit']->term_id}";

		$data['invoice_url'] = module_url('invoices/');


		$data['term_id'] 			 = $term_id ;
		
		parent::render($data);
	}

	protected function is_assigned($term_id = 0,$kpi_type = '')
	{
		// check user has manager role permission
		$class 	= $this->router->fetch_class();
		$method = $this->router->fetch_method();
		$result = $this->domain_m->has_permission($term_id,"{$class}.{$method}",'access');
		return $result;
	}

	private function has_permission($name ='', $action ='', $term_id = 0)
	{
		if($term_id == 0)
			$term_id = $this->website_id;
		return $this->domain_m->has_permission($term_id,$name, $action);
	}
}
/* End of file domain.php */
/* Location: ./application/modules/Domain/controllers/domain.php */