<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fix extends Admin_Controller {

	public function __construct(){
		parent::__construct();

		$models = array(
			'domain/domain_m',
			'domain/domain_contract_m',
			'domain/domain_report_m',
		);

		$this->load->model($models);	
	}
		
		
	/**
	 * Sends a mail before suspend.
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function send_mail_before_suspend($term_id = 0)
	{
		$this->domain_report_m->send_mail_before_suspend($term_id);
	}

	public function send_mail_alert_before_when_off($term_id = 0)
	{
		$this->domain_report_m->send_mail_alert_before_when_off($term_id);
	}


	function send_activation_mail2customer2($term_id = 0)
	{
		$this->load->model('domain/report_m');
		$this->report_m->init($term_id);
		$this->report_m->send_activation_email();
	}

	public function send_mail_info_activated($term_id = 0)
	{
		$this->domain_report_m->send_mail_info_activated($term_id);
	}

	public function send_mail_info_finish($term_id) {
		$this->domain_report_m->send_mail_info_finish($term_id) ;
	}

	public function send_mail_alert_expiring_soon($term_id) {
		$this->domain_report_m->send_mail_alert_expiring_soon($term_id) ;
	}

	public function send_mail_info_to_admin($term_id) {
		$this->domain_report_m->send_mail_info_to_admin($term_id);		
	}

	public function send_mail_extend($term_id) {
		$this->domain_report_m->send_mail_extend($term_id);		
	}

	public function send_mail_alert_admin_off_domain($term_id) {
		$this->domain_report_m->send_mail_alert_admin_off_domain($term_id);		
	}

}
/* End of file Fix.php */
/* Location: ./application/modules/domain/controllers/Fix.php */