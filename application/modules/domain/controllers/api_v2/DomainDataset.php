<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * RECEIPTS API FOR BACK-END
 */
class DomainDataset extends MREST_Controller
{
    protected $permission = 'domain.index.access';

    public function __construct()
    {
        $this->autoload['libraries'][] = 'datatable_builder';

        $this->autoload['helpers'][] = 'form';
        $this->autoload['helpers'][] = 'text';
        $this->autoload['helpers'][] = 'array';

        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'domain_m';

        parent::__construct();

        $this->config->load('domain/group');
        $this->config->load('domain/domain');
    }

    /**
     * Return Data-source for datatable
     * @return json
     */
    public function index_get()
    {
        $response   = array('status'=>FALSE,'msg'=>'Quá trình xử lý không thành công.','data'=>[]);

        $defaults   = ['offset' => 0, 'per_page' => 20, 'cur_page' => 1, 'is_filtering' => true, 'is_ordering' => true];
        $args       = wp_parse_args(parent::get(), $defaults);

        $relate_users       = $this->admin_m->get_all_by_permissions($this->permission);

        // LOGGED USER NOT HAS PERMISSION TO ACCESS
        if($relate_users === FALSE)
        {
            $response['msg'] = 'Quyền truy cập không hợp lệ.';
            parent::response($response);
        }

        if(is_array($relate_users))
        {
            $this->datatable_builder->join('term_users','term_users.term_id = term.term_id')->where_in('term_users.user_id', $relate_users);
        }

        // Prepare search
        $service_packages = $this->config->item('service', 'packages');
        $service_packages = key_value($service_packages, 'name', 'label');

        // Build datatable
        $data = $this->data;
        $this->search_filter_receipt();

        $data['content'] = $this->datatable_builder
            ->set_filter_position(FILTER_TOP_INNER)
            ->setOutputFormat('JSON')
            ->select('term.term_id, term.term_name')

            ->add_search('contract_code', ['placeholder' => 'Website'])
            ->add_search('contract_begin', ['placeholder' => 'T/G bắt đầu', 'class' => 'form-control set-datepicker'])
            ->add_search('contract_end', ['placeholder' => 'T/G kết thúc', 'class' => 'form-control set-datepicker'])
            ->add_search('domain_service_package', ['content' => form_dropdown(['name' => 'where[domain_service_package]', 'class' => 'form-control select2'], prepare_dropdown($service_packages, 'Gói'), parent::get('where[domain_service_package]'))])
            ->add_search('domain_number_year_register', ['placeholder' => 'T/G Đăng ký'])
            ->add_search('contract_value', ['placeholder' => 'Giá trị hợp đồng'])
            ->add_search('staff_business', ['placeholder' => 'NVKD'])

            // ->add_column('user_id', array('title' => 'ID', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_code', array('title' => 'Website', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_begin', array('title' => 'T/G bắt đầu', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_end', array('title' => 'T/G kết thúc', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('domain_service_package', array('title' => 'Gói', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('domain_number_year_register', array('title' => 'T/G Đăng ký', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('contract_value', array('title' => 'Trước VAT', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('payment_percentage', array('title' => 'T/Đ thanh toán', 'set_select' => FALSE, 'set_order' => FALSE))
            ->add_column('staff_business', array('title' => 'NVKD', 'set_select' => FALSE, 'set_order' => TRUE))
            ->add_column('action', array('title' => 'Hành động', 'set_select' => FALSE, 'set_order' => FALSE))

            ->add_column_callback('term_id', function ($data, $row_name) use ($service_packages) {
                $term_id = (int)$data['term_id'];

                // Get contract_code
                $contract_code = get_term_meta_value($term_id, 'contract_code');
                $data['contract_code'] = $contract_code ?? '--';

                // Get contract_begin
                $data['contract_begin_raw'] = get_term_meta_value($term_id, 'contract_begin');
                $data['contract_begin'] = $data['contract_begin_raw'] ? date('d/m/Y', $data['contract_begin_raw']) : '--';

                // Get contract_end
                $data['contract_end_raw'] = get_term_meta_value($term_id, 'contract_end');
                $data['contract_end'] = $data['contract_end_raw'] ? date('d/m/Y', $data['contract_end_raw']) : '--';

                // Get domain_service_package
                $data['domain_service_package_raw'] = get_term_meta_value($term_id, 'domain_service_package');
                $data['domain_service_package'] = $data['domain_service_package_raw'] ? $service_packages[$data['domain_service_package_raw']] : '--';

                // Get domain_number_year_register
                $data['domain_number_year_register_raw'] = get_term_meta_value($term_id, 'domain_number_year_register');
                $data['domain_number_year_register'] = $data['domain_number_year_register_raw'] ? "{$data['domain_number_year_register_raw']} năm" : '--';

                // Get contract_value
                $data['contract_value_raw'] = (int)get_term_meta_value($term_id, 'contract_value');
                $data['contract_value'] = '' . $data['contract_value_raw'] ? currency_numberformat($data['contract_value_raw'], 'đ') : '--';

                // Get payment_percentage
                $invs_amount = (float)get_term_meta_value($term_id, 'invs_amount');
                $payment_amount = (float)get_term_meta_value($term_id, 'payment_amount');
                $payment_percentage = 100 * (float) get_term_meta_value($term_id, 'payment_percentage');
                $data['payment_progress'] = [
                    'payment_amount' => $payment_amount,
                    'invs_amount' => $invs_amount,
                    'payment_percentage' => $payment_percentage,
                ];

                // Get staff_business
                $sale_id = get_term_meta_value($data['term_id'], 'staff_business');
                if (empty($sale_id)) return $data;

                $data['staff_business'] = $this->admin_m->get_field_by_id($sale_id, 'display_name');

                $departments = $this->term_users_m->get_user_terms($sale_id, 'department');
                $departments and $data['staff_business'] .= '<br /><small style="font-weight:bold;font-style: italic;">' . implode(', ', array_map(function ($x) {
                    return $x->term_name;
                }, $departments)) . '</small>';

                // Add on
                $data['is_service_proc'] = is_service_proc($term_id);
                $data['is_service_end'] = is_service_end($term_id);

                return $data;
            }, FALSE)

            ->from('term')
            ->where('term.term_type', $this->domain_m->term_type)
            ->where('term_status', 'publish')
            ->group_by('term.term_id');

        $pagination_config = ['per_page' => $args['per_page'], 'cur_page' => $args['cur_page']];

        $data = $this->datatable_builder->generate($pagination_config);
        // dd($this->datatable_builder->last_query());

        // OUTPUT : DOWNLOAD XLSX
        if (!empty($args['download']) && $last_query = $this->datatable_builder->last_query()) {
            $this->export_domain($last_query);
            return TRUE;
        }

        $this->template->title->append('Danh sách các đợt thanh toán đã thu');
        return parent::response($data);
    }

    /**
     * Xử lý các điều kiện filter từ GET
     */
    protected function search_filter_receipt($args = array())
    {
        restrict($this->permission);

        $args = parent::get();
        //if(empty($args) && parent::get('search')) $args = parent::get();  
        if (!empty($args['where'])) $args['where'] = array_map(function ($x) {
            return trim($x);
        }, $args['where']);

        // contract_code FILTERING & SORTING
        $filter_contract_code = $args['where']['contract_code'] ?? FALSE;
        $sort_contract_code = $args['order_by']['contract_code'] ?? FALSE;
        if ($filter_contract_code || $sort_contract_code) {
            $alias = uniqid('contract_code_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'contract_code'", 'LEFT');

            if ($filter_contract_code) {
                $this->datatable_builder->like("{$alias}.meta_value", $filter_contract_code);
            }

            if ($sort_contract_code) {
                $this->datatable_builder->order_by("{$alias}.meta_value", $sort_contract_code);
            }
        }

        // contract_begin FILTERING & SORTING
        $filter_contract_begin = $args['where']['contract_begin'] ?? FALSE;
        $sort_contract_begin = $args['order_by']['contract_begin'] ?? FALSE;
        if ($filter_contract_begin || $sort_contract_begin) {
            $dates = explode(' - ', $args['where']['contract_begin']);

            $alias = uniqid('contract_begin_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'contract_begin'", 'LEFT');

            if ($filter_contract_begin) {
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_begin) {
                $this->datatable_builder->group_by("{$alias}.meta_value")->order_by("{$alias}.meta_value", $sort_contract_begin);
            }
        }

        // contract_end FILTERING & SORTING
        $filter_contract_end = $args['where']['contract_end'] ?? FALSE;
        $sort_contract_end = $args['order_by']['contract_end'] ?? FALSE;
        if ($filter_contract_end || $sort_contract_end) {
            $dates = explode(' - ', $args['where']['contract_end']);

            $alias = uniqid('contract_end_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'contract_end'", 'LEFT');

            if ($filter_contract_end) {
                $this->datatable_builder->where("{$alias}.meta_value >=", $this->mdate->startOfDay(reset($dates)))
                    ->where("{$alias}.meta_value <=", $this->mdate->endOfDay(end($dates)));
            }

            if ($sort_contract_end) {
                $this->datatable_builder->group_by("{$alias}.meta_value")->order_by("{$alias}.meta_value", $sort_contract_end);
            }
        }

        // domain_service_package FILTERING & SORTING
        $filter_domain_service_package = $args['where']['domain_service_package'] ?? FALSE;
        $sort_domain_service_package = $args['order_by']['domain_service_package'] ?? FALSE;
        if ($filter_domain_service_package || $sort_domain_service_package) {
            $alias = uniqid('domain_service_package_');

            $this->datatable_builder->join("termmeta {$alias}", "{$alias}.term_id = term.term_id and {$alias}.meta_key = 'domain_service_package'", 'LEFT');

            if ($filter_domain_service_package) {
                $this->datatable_builder->where("{$alias}.meta_value", $filter_domain_service_package);
            }

            if ($sort_domain_service_package) {
                $this->datatable_builder->select("{$alias}.meta_value")
                    ->group_by("{$alias}.meta_value")
                    ->order_by("{$alias}.meta_value", $sort_domain_service_package);
            }
        }

        // domain_number_year_register FILTERING & SORTING
        $filter_domain_number_year_register = $args['where']['domain_number_year_register'] ?? FALSE;
        $sort_domain_number_year_register = $args['order_by']['domain_number_year_register'] ?? FALSE;
        if ($filter_domain_number_year_register || $sort_domain_number_year_register) {
            $alias = uniqid('domain_number_year_register_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'domain_number_year_register'", 'LEFT');

            if (is_numeric($filter_domain_number_year_register)) {
                $this->datatable_builder->where("{$alias}.meta_value >=", (int)$filter_domain_number_year_register);
            }

            if ($sort_domain_number_year_register) {
                $this->datatable_builder->group_by("{$alias}.meta_value")->order_by("CAST({$alias}.meta_value as UNSIGNED)", $sort_domain_number_year_register);
            }
        }

        // contract_value FILTERING & SORTING
        $filter_contract_value = $args['where']['contract_value'] ?? FALSE;
        $sort_contract_value = $args['order_by']['contract_value'] ?? FALSE;
        if ($filter_contract_value || $sort_contract_value) {
            $alias = uniqid('contract_value_');

            $this->datatable_builder->join("termmeta {$alias}", "term.term_id = {$alias}.term_id and {$alias}.meta_key = 'contract_value'", 'LEFT');

            if (is_numeric($filter_contract_value)) {
                $this->datatable_builder->where("{$alias}.meta_value >=", (int)$filter_contract_value);
            }

            if ($sort_contract_value) {
                $this->datatable_builder->group_by("{$alias}.meta_value")->order_by("CAST({$alias}.meta_value as UNSIGNED)", $sort_contract_value);
            }
        }

        // staff_business FILTERING & SORTING
        $filter_staff_business = $args['where']['staff_business'] ?? FALSE;
        $sort_staff_business = $args['order_by']['staff_business'] ?? FALSE;
        if ($filter_staff_business || $sort_staff_business) {
            $alias_staff_business = uniqid('staff_business_');
            $alias_user = uniqid('staff_business_');

            $this->datatable_builder->join("termmeta {$alias_staff_business}", "term.term_id = {$alias_staff_business}.term_id and {$alias_staff_business}.meta_key = 'staff_business'", 'LEFT')
                ->join("user {$alias_user}", "{$alias_staff_business}.meta_value = {$alias_user}.user_id and {$alias_user}.user_type = 'admin'", 'LEFT');

            if ($filter_staff_business) {
                $this->datatable_builder->like("{$alias_user}.display_name", $filter_staff_business);
            }

            if ($sort_staff_business) {
                $this->datatable_builder->order_by("{$alias_user}.display_name", $sort_staff_business)
                    ->group_by("{$alias_user}.user_id");
            }
        }
    }

    /**
     * Xuất file excel danh sách phiếu thanh toán dựa vào method receipt()
     *
     * @param      string  $query  The query
     *
     * @return     bool trả về kiểu boolean, đồng thời tạo kết nối tải file đến browser nếu file được khởi tạo thành công
     */
    public function export_domain($query = '')
    {
        restrict($this->permission);

        if (empty($query)) return FALSE;

        // remove limit in query string
        $pos = strpos($query, 'LIMIT');
        if (FALSE !== $pos) $query = mb_substr($query, 0, $pos);

        $domains = $this->domain_m->query($query)->result();
        if (!$domains) {
            $this->messages->info('Dữ liệu rỗng , vui lòng lọc trước khi thực hiện "EXPORT"');
            redirect(current_url(), 'refresh');
        }

        $this->load->library('excel');
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array('memoryCacheSize' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel
            ->getProperties()
            ->setCreator("WEBDOCTOR.VN")
            ->setLastModifiedBy("WEBDOCTOR.VN")
            ->setTitle(uniqid('Danh sách dịch vụ Domain __'));

        $objPHPExcel = PHPExcel_IOFactory::load('./files/excel_tpl/domain/domain-list-export.xlsx');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $row_index = 3;

        foreach ($domains as $key => &$domain) {
            // Prepare data
            $term_id = (int)$domain->term_id;

            // Get contract_code
            $contract_code = get_term_meta_value($term_id, 'contract_code') ?? '--';
            // Get contract_begin
            $contract_begin = get_term_meta_value($term_id, 'contract_begin');
            $contract_begin = $contract_begin ? date('d/m/Y', $contract_begin) : '--';

            // Get contract_end
            $contract_end = get_term_meta_value($term_id, 'contract_end');
            $contract_end = $contract_end ? date('d/m/Y', $contract_end) : '--';

            // Get domain_service_package
            $service_packages = $this->config->item('service', 'packages');
            $service_packages = key_value($service_packages, 'name', 'label');
            $domain_service_package = get_term_meta_value($term_id, 'domain_service_package');
            $domain_service_package = $domain_service_package ? $service_packages[$domain_service_package] : '--';

            // Get domain_number_year_register
            $domain_number_year_register = get_term_meta_value($term_id, 'domain_number_year_register');
            $domain_number_year_register = $domain_number_year_register ? "{$domain_number_year_register} năm" : '--';

            // Get contract_value
            $contract_value = (int)get_term_meta_value($term_id, 'contract_value');
            $contract_value = $contract_value ?: '--';

            // Get staff_business
            $sale_id = get_term_meta_value($term_id, 'staff_business');
            $staff_business = $this->admin_m->get_field_by_id($sale_id, 'display_name') ?: '--';

            $row_number = $row_index + $key;

            $i = 0;

            // Set Cell
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $key + 1);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_code);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_begin);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_end);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $domain_service_package);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $domain_number_year_register);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $contract_value);
            $objWorksheet->setCellValueByColumnAndRow($i++, $row_number, $staff_business);
        }

        $numbers_of_domain = count($domains);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $row_index . ':A' . ($numbers_of_domain + $row_index))
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $file_name = "tmp/export-domain-service.xlsx";
        $objWriter->save($file_name);

        try {
            $objWriter->save($file_name);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return FALSE;
        }

        if (file_exists($file_name)) {
            $this->load->helper('download');
            force_download($file_name, NULL);
        }

        return FALSE;
    }
}
/* End of file DomainDataset.php */
/* Location: ./application/modules/domain/controllers/api_v2/DomainDataset.php */