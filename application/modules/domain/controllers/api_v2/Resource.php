<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Resource extends MREST_Controller
{
    function __construct()
    {
        $this->autoload['libraries'][] = 'form_validation';

        $this->autoload['models'][] = 'log_m';
        $this->autoload['models'][] = 'contract/base_contract_m';
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'domain/domain_m';
        $this->autoload['models'][] = 'domain/domain_contract_m';

        $this->autoload['helpers'][] = 'date';

        parent::__construct();

        $this->load->config('domain/domain');
    }

    public function contracts_get($contract_id)
    {
        if (!has_permission('domain.overview.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), [
            'contract_id' => $contract_id,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contract_id',
                'label' => 'contract_id',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->domain_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        // Process
        $data = [];

        $data['term_id'] = $inputs['contract_id'];

        $contract_code = get_term_meta_value($inputs['contract_id'], 'contract_code');
        $data['contract_code'] = $contract_code ?: '';

        $contract_begin = get_term_meta_value($inputs['contract_id'], 'contract_begin');
        $data['contract_begin'] = isset($contract_begin) ? my_date($contract_begin, 'd/m/Y') : '';

        $contract_end = get_term_meta_value($inputs['contract_id'], 'contract_end');
        $data['contract_end'] = isset($contract_end) ? my_date($contract_end, 'd/m/Y') : '';

        $contract_region = get_term_meta_value($inputs['contract_id'], 'contract_region');
        $data['contract_region'] = $contract_region ?: '';

        $contract_value = get_term_meta_value($inputs['contract_id'], 'contract_value');
        $data['contract_value'] = (int) $contract_value ?: 0;

        $start_server_time = get_term_meta_value($inputs['contract_id'], 'start_server_time');
        $data['start_server_time'] = isset($start_server_time) ? my_date($start_server_time, 'd/m/Y') : '';

        $service_packages = $this->config->item('service', 'packages');
        $service_packages = key_value($service_packages, 'name', 'label');
        $data['domain_service_package_raw'] = get_term_meta_value($inputs['contract_id'], 'domain_service_package');
        $data['domain_service_package'] = $data['domain_service_package_raw'] ? $service_packages[$data['domain_service_package_raw']] : '--';

        $representative_gender = get_term_meta_value($inputs['contract_id'], 'representative_gender');
        $data['representative_gender'] = $representative_gender ?: '';

        $representative_name = get_term_meta_value($inputs['contract_id'], 'representative_name');
        $data['representative_name'] = $representative_name ?: '';

        $representative_email = get_term_meta_value($inputs['contract_id'], 'representative_email');
        $data['representative_email'] = $representative_email ?: '';

        $representative_phone = get_term_meta_value($inputs['contract_id'], 'representative_phone');
        $data['representative_phone'] = $representative_phone ?: '';

        $sale_id = get_term_meta_value($inputs['contract_id'], 'staff_business');
        $data['staff_business_id'] = $sale_id ?: '';
        $data['staff_business_name'] = $this->admin_m->get_field_by_id($sale_id, 'display_name') ?: '';
        $data['staff_business_email'] = $this->admin_m->get_field_by_id($sale_id, 'user_email') ?: '';
        $data['staff_business_avatar'] = $this->admin_m->get_field_by_id($sale_id, 'user_avatar') ?: '';

        

        return parent::responseHandler($data, 'Lấy thông tin thành công');
    }

    public function customer_get($contract_id)
    {
        if (!has_permission('domain.overview.access')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::get(), [
            'contract_id' => $contract_id,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contract_id',
                'label' => 'contract_id',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->domain_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        // Process
        $data = [];

        $representative_gender = get_term_meta_value($inputs['contract_id'], 'representative_gender');
        $representative_name = get_term_meta_value($inputs['contract_id'], 'representative_name');
        $data['name'] = ($representative_gender == 1 ? 'Ông' : 'Bà') . ' ' . $representative_name;

        $representative_email = get_term_meta_value($inputs['contract_id'], 'representative_email');
        $data['email'] = $representative_email;

        return parent::responseHandler($data, 'Lấy thông tin thành công');
    }

    /**
     * @return json
     */
    public function start_contract_put($contractId)
    {
        if (!has_permission('domain.start_service.update')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::put(), [
            'contractId' => $contractId,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->domain_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $term = $this->domain_m->set_term_type()->where('term_id', $inputs['contractId'])->get_by();

        $is_proc_service = $this->domain_contract_m->proc_service($term);
        if (!$is_proc_service) {
            return parent::responseHandler([], 'Có lỗi trong quá trình khởi chạy.', 'error', 400);
        }

        return parent::responseHandler([], 'Dịch vụ khởi chạy thành công.');
    }

    /**
     * @return json
     */
    public function stop_contract_put($contractId)
    {
        if (!has_permission('domain.stop_service.update')) {
            return parent::responseHandler(null, 'Quyền truy cập không hợp lệ.', 'error', 403);
        }

        $inputs = wp_parse_args(parent::put(), [
            'contractId' => $contractId,
        ]);

        // Validate
        $rules = array(
            [
                'field' => 'contractId',
                'label' => 'contractId',
                'rules' => [
                    'required',
                    'integer',
                    array('existed_check', array($this->domain_m, 'existed_check'))
                ]
            ],
        );

        $this->form_validation->set_data($inputs);
        $this->form_validation->set_rules($rules);
        if (FALSE == $this->form_validation->run()) {
            return parent::responseHandler(null, $this->form_validation->error_array(), 'error', 400);
        }

        $term = $this->domain_m->set_term_type()->where('term_id', $inputs['contractId'])->get_by();

        $is_stop_service = $this->domain_contract_m->stop_service($term);
        if (!$is_stop_service) {
            return parent::responseHandler([], 'Có lỗi trong quá trình kết thúc hợp đồng.', 'error', 400);
        }

        return parent::responseHandler([], 'Hợp đồng đã chuyển sang trạng thái "Đã kết thúc".');
    }
}
/* End of file Resource.php */
/* Location: ./application/modules/domain/controllers/api_v2/Resource.php */