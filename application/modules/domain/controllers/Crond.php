<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crond extends Public_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('domain/domain_report_m');
		$this->load->config('domain/domain') ;

		defined('NOW_TIME') OR define('NOW_TIME',$this->config->item('now_time', 'times'));
	}

	public function delete_log() {
		$this->db->where(['log_type' => 'warning_domain_extend'])->delete('log');
	}

	public function index() 
	{
		$terms = $this->term_m->get_many_by(['term_type' =>'domain','term_status' =>'publish']);		
		if(empty($terms)) return FALSE ; 

		//$this->send_mail_extend($terms) ;
		$this->send_mail_alert_admin_off_domain($terms) ;
	}

	// GỬI MAIL THÔNG BÁO TRƯỚC BAO NHIÊU NGÀY
	public function send_mail_extend($terms = null) 
	{	
		if(empty($terms)) return FALSE ;

		// SỐ NGÀY THÔNG BÁO TRƯỚC KHI KẾT THÚC DOMAIN
		$number_day_alert_before_when_off = $this->config->item('number_day_alert_before_when_off', 'times') ;

		// THỜI GIAN HIỆN TẠI
		$now = $this->mdate->startOfDay(NOW_TIME) ;

		foreach ($terms as $key => $term) 
		{
			// KIỂM TRA ĐÃ GỬI MAIL HAY CHƯA
			$check_send_mail_alert = get_term_meta_value($term->term_id, 'send_mail_alert_before_when_off') ?: '';
			if( !empty($check_send_mail_alert) ) continue ;

			// KIỂM TRA CÓ NGÀY KẾT THÚC HỢP ĐỒNG HAY CHƯA
			$contract_end 	= get_term_meta_value($term->term_id,'contract_end');
			if( ! $contract_end ) continue ;
			$contract_end   = $this->mdate->startOfDay($contract_end);

			// THỜI GIAN HIÊN TẠI > THỜI GIAN HĐ => KHÔNG HỢP LỆ
			if($now > $contract_end) continue;

			// TÍNH MỐC THỜI GIAN GỬI MAIL
			$timeline	=	strtotime("- $number_day_alert_before_when_off days", $contract_end) ;
			$timeline 	=	$this->mdate->startOfDay($timeline) ;

			// KIỂM TRA THỜI GIAN HỢP LỆ ĐỂ GỬI MAIL
			if( ($timeline <= $now) && ($now <= $contract_end ) )
			{
				// TIẾN HÀNH GỬI MAIL CẢNH BÁO TRƯỚC 30 NGÀY
				$send_status = $this->domain_report_m->send_mail_alert_before_when_off($term->term_id) ;
				
				// GỬI MAIL THẤT BẠI => TIẾP TỤC GỬI , GHI LOG
				if(FALSE == $send_status) 
				{
					$this->log_m->insert(array(
						'log_title'			=> 'Gửi mail trước ' . $number_day_alert_before_when_off . ' thất bại',
						'log_status'		=> 0,
						'term_id'			=> $term->term_id,
						'user_id'			=> $this->admin_m->id,
						'log_content'		=> 'Gửi mail thông báo thất bại, gửi lại saU',
						'log_type'			=> 'send_mail_alert_before_' . $number_day_alert_before_when_off . '_day',
						'log_time_create' 	=> date('Y-m-d H:i:s')
					)); 
					continue ;
				}
			}	
			 
		}
	}

	// GỬI MAIL THÔNG BÁO CHO ADMIN TẮT DOMAIN
	public function send_mail_alert_admin_off_domain($terms = null) 
	{	
		if(empty($terms)) return FALSE ;

		// SỐ NGÀY THÔNG BÁO TRƯỚC KHI KẾT THÚC DOMAIN
		$number_day_alert_admin_off_domain = $this->config->item('number_day_alert_admin_off_domain', 'times') ;

		// THỜI GIAN HIỆN TẠI
		$now = $this->mdate->startOfDay(NOW_TIME) ;

		foreach ($terms as $key => $term) 
		{
			// KIỂM TRA ĐÃ GỬI MAIL HAY CHƯA
			$check_send_mail_alert = get_term_meta_value($term->term_id, 'send_mail_alert_admin_off_domain') ?: '';
			if( !empty($check_send_mail_alert) ) continue ;

			// KIỂM TRA CÓ NGÀY KẾT THÚC HỢP ĐỒNG HAY CHƯA
			$contract_end 	= get_term_meta_value($term->term_id,'contract_end');
			if( ! $contract_end ) continue ;
			$contract_end   = $this->mdate->startOfDay($contract_end);

			// THỜI GIAN HIÊN TẠI > THỜI GIAN HĐ => KHÔNG HỢP LỆ
			if($now < $contract_end) continue;

			// TÍNH MỐC THỜI GIAN GỬI MAIL
			$timeline	=	strtotime("+ $number_day_alert_admin_off_domain days", $contract_end) ;
			$timeline 	=	$this->mdate->startOfDay($timeline) ;

			// KIỂM TRA THỜI GIAN HỢP LỆ ĐỂ GỬI MAIL
			if( ($timeline > $now) && ($now > $contract_end) )
			{
				// TIẾN HÀNH GỬI MAIL CẢNH BÁO TRƯỚC 30 NGÀY
				$send_status = $this->domain_report_m->send_mail_alert_admin_off_domain($term->term_id) ;
				
				// GỬI MAIL THẤT BẠI => TIẾP TỤC GỬI , GHI LOG
				if(FALSE == $send_status) 
				{
					$this->log_m->insert(array(
						'log_title'			=> 'Gửi mail trước ' . $number_day_alert_admin_off_domain . ' thất bại',
						'log_status'		=> 0,
						'term_id'			=> $term->term_id,
						'user_id'			=> $this->admin_m->id,
						'log_content'		=> 'Gửi mail thông báo thất bại, gửi lại saU',
						'log_type'			=> 'send_mail_alert_admin_off_domain_' . $number_day_alert_admin_off_domain . '_day',
						'log_time_create' 	=> date('Y-m-d H:i:s')
					)); 
					continue ;
				}
			}	
			 
		}
	}
}