<?php
    
    $representative_gender = get_term_meta_value($term_id, 'representative_gender');
    $contract_begin        = get_term_meta_value($term_id, 'contract_begin');
    $contract_end          = get_term_meta_value($term_id, 'contract_end');
    $contract_region       = get_term_meta_value($term_id, 'contract_region');
    $contract_type         = get_term_meta_value($term_id, 'contract_type');
    $start_server_time     = get_term_meta_value($term_id, 'start_server_time') ;
    $start_server_time     = (!isset($start_server_time)) ? 'Chưa kích hoạt' : $this->mdate->date('d/m/Y', $start_server_time) ;

    // THÔNG TIN CHI TIẾT DOMAIN
    $services = $this->config->item('service', 'packages') ;
    $domain_service_package = get_term_meta_value($term_id,'domain_service_package');
    
    $domain_fee_initialization  = get_term_meta_value($term_id, 'domain_fee_initialization') ?: $services[$domain_service_package]['fee_initialization'];
    $domain_fee_maintain        = get_term_meta_value($term_id, 'domain_fee_maintain') ?: $services[$domain_service_package]['fee_maintain'];
    $domain_fee_initialization_maintain = get_term_meta_value($term_id, 'domain_fee_initialization_maintain') ?: $domain_fee_maintain + $domain_fee_initialization;


    $sale_id               = get_term_meta_value($term_id,'staff_business');

    $staff_business        = 'Chưa có' ;
    if($sale_id) $staff_business        = $this->admin_m->get_field_by_id($sale_id,'display_name');
?>
<div class="row">


    <div class="col-sm-6">
        <?php   
            // --------------------------------------------------------------------
            // THÔNG TIN HỢP ĐỒNG
            // --------------------------------------------------------------------
              $this->table->set_caption('Thông tin hợp đồng')
                          ->add_row(array('Mã hợp đồng',get_term_meta_value($term_id, 'contract_code')))
                          ->add_row('Gói:', '<b>'          .    $services[$domain_service_package]['label']      . '</b>')
                          ->add_row('Phí khởi tạo', '<b>'    . currency_numberformat($domain_fee_initialization) . '</b>')
                          ->add_row('Phí duy trì', '<b>'    . currency_numberformat($domain_fee_maintain)  . '</b>')
                          ->add_row('Phí duy trì và khởi tạo hàng năm', '<b>' . currency_numberformat($domain_fee_initialization_maintain) . '/1 năm</b>')
                          ->add_row('Ngày bắt đầu', empty($contract_begin) ? $empty_chars : $this->mdate->date('d/m/Y', $contract_begin))
                          ->add_row('Ngày kết thúc', empty($contract_end) ? $empty_chars : $this->mdate->date('d/m/Y', $contract_end))
                          ->add_row('Ngày kích hoạt', $start_server_time)
                          ->add_row('Nhân viên kinh doanh', $staff_business)
                          ->add_row('Kỹ thuật thực hiện', @$technical_staffs) ;   
              echo $this->table->generate();
        ?>
    </div>
    <div class="col-sm-6">
         <?php 
         // --------------------------------------------------------------------
         // THÔNG TIN KHÁCH HÀNG
         // --------------------------------------------------------------------
           $this->table->set_caption('Thông tin khách hàng')
                       ->add_row('Người đại diện',
        '<span id="incline-data">' .($representative_gender == 1 ? 'Ông ' : 'Bà ').str_repeat('&nbsp', 2).get_term_meta_value($term_id,'representative_name').'</span>')
                       ->add_row('Email',get_term_meta_value($term_id, 'representative_email')) ;
            echo $this->table->generate(); 
         ?>       
    </div>
</div> 

    


   