<?php 

   // CONFIG
   $services = $this->config->item('service', 'packages') ;

   // THÔN TIN DOMAIN
   $domain_service_package = get_term_meta_value($term_id,'domain_service_package'); 
   $label_domain_service_package = $services[$domain_service_package]['label'];

   $domain_fee_initialization          = get_term_meta_value($term_id, 'domain_fee_initialization') ?: $services[$domain_service_package]['fee_initialization'];
   $domain_fee_maintain                = get_term_meta_value($term_id, 'domain_fee_maintain') ?: $services[$domain_service_package]['fee_maintain'];
   $domain_fee_initialization_maintain = get_term_meta_value($term_id, 'domain_fee_initialization_maintain') ?: $domain_fee_maintain + $domain_fee_initialization;

   // TỔNG GIÁ TRỊ HỢP ĐỒNG TRƯỚC VAT
   $contract_value = get_term_meta_value($term_id, 'contract_value') ?: 0;

   $representative_name = get_term_meta_value($term_id, 'representative_name');
   $contract_code       = get_term_meta_value($term_id, 'contract_code');
   $contract_begin      = get_term_meta_value($term_id, 'contract_begin');
   $contract_end        = get_term_meta_value($term_id, 'contract_end');

   $start_time = get_term_meta_value($term_id,'contract_begin');
   $end_time   = get_term_meta_value($term_id,'contract_end');

   $service_date = $this->mdate->date('d/m/Y', $start_time).' - '. $this->mdate->date('d/m/Y', $end_time);
   $contract_date = $this->mdate->date('d/m/Y', get_term_meta_value($term_id,'contract_begin')).' - ' . $this->mdate->date('d/m/Y', get_term_meta_value($term_id,'contract_end'));
   
   $staff_id            = get_term_meta_value($term_id, 'staff_business');
   if( ! $staff_id ) $staff_id  = '' ;
   $staff_name          = $this->admin_m->get_field_by_id($staff_id, 'display_name');
   if( ! $staff_name ) $staff_name  = 'Chưa có' ; 
?>
      <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td height="15"></td>
         </tr>
         <tr>
            <td align="center">
               <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tr>
                              <!--Title-->
                              <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Kích hoạt dịch vụ</td>
                              <!--End title-->
                              <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                           </tr>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tr>
                              <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                 <p style="padding-left: 24px;">&nbsp;</p>
                              </td>
                           </tr>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tr>
                              <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;">Thông báo kích hoạt dịch vụ</td>
                           </tr>
                        </table>
                        <!--end detail-->
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <!--Content-->
                              <tr>
                                 <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000; line-height:28px;">
                                    <p><b>Kính chào Quý khách !</b> WEBDOCTOR.VN cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</p>
                                    <p>Chúng tôi xin thông báo tới quý khách Hợp đồng <?php echo $contract_code ;?> đã được kích hoạt</p>.
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <!--Content-->
                              <tr>
                                 <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000; line-height:28px;">
                                    <p style="font-family: open sans, arial, sans-serif; font-size:15px">Mọi chi tiết xin vui lòng liên hệ với chúng tôi theo thông tin dưới đây:</p>
                                    <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <?php if(!empty($staff)) { ?>
                                             <tr>
                                                <!--<td>Kinh doanh phụ trách</td>-->
                                                <td>
                                                      <tr>
                                                         <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Kinh doanh <?= $staff->display_name?>: </td>
                                                         <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php
                                                            $phone = $this->usermeta_m->get_meta_value($staff->user_id,'user_phone');
                                                            if(!$phone) $phone = '(028) 7300. 4488';
                                                            
                                                            echo $phone; ?></td>
                                                      </tr>
                                                      <tr>
                                                         <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                         <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?= $staff->user_email ?></td>
                                                      </tr>
                                                </td>
                                             </tr>
                                          <?php } ?>

                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Hotline Trung tâm Kinh doanh:</td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">(028) 7300. 4488</td>
                                          </tr>
                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">sales@webdoctor.vn</td>
                                          </tr>
                                          <tr>
                                             <td height="25"></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <!--End Content-->
                              <tr>
                                 <td align="left" height="30"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td height="15"></td>
         </tr>
      </table>
      </tr>
      </tbody></table>
      
      <!-- Thông tin hợp đồng -->
      <table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td height="15"></td>
         </tr>
         <tr>
            <td align="center">
               <table class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tr>
                              <!--Title-->
                              <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin hợp đồng</td>
                              <!--End title-->
                              <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img" /></td>
                           </tr>

                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tr>
                              <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                 <p style="padding-left: 24px;">&nbsp;</p>
                              </td>
                           </tr>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <!--end detail-->
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#ffffff">
                        <?php 
                           
                                                       
                           ?>                            
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <!--Content-->
                             
                              <?php 
                                 $rows = array();
                                 $rows[] = array('Mã hợp đồng', get_term_meta_value($term_id,'contract_code'));
                                 $rows[] = array('Thời gian hợp đồng', $contract_date);
                                 $rows[] = array('Thời gian thực hiện', $service_date);
                                 $rows[] = array('Gói', $label_domain_service_package);
                                 $rows[] = array('Thời gian đăng ký', $domain_number_year_register . ' năm');
                                 $rows[] = array('Phí khởi tạo', currency_numberformat($domain_fee_initialization));
                                 $rows[] = array('Phí duy trì', currency_numberformat($domain_fee_maintain) . '/1 năm');

                                 // KIỂM TRA ĐÃ GIA HẠN HAY CHƯA
                                 $has_domain_renew  = get_term_meta_value($term_id, 'has_domain_renew') ?: 0;

                                 // TỔNG PHÍ GIA HẠN HẰNG NĂM
                                 $domain_fee_maintain_years = $domain_fee_maintain * ($domain_number_year_register - 1) ;
                                 if($has_domain_renew == 1) $domain_fee_maintain_years =  $domain_fee_maintain * $domain_number_year_register;

                                 if($has_domain_renew == 0)
                                 {
                                    $rows[] = array('Phí khởi tạo và duy trì năm đầu tiên', currency_numberformat($domain_fee_initialization_maintain));
                                 }

                                 $rows[] = array('Tổng tiền trước VAT', currency_numberformat($contract_value)  );
                                                                    
                                 foreach($rows as $i=>$row):
                                    ?>
                              <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                                 <tr>
                                    <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#000;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                    <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                    </td>
                                 </tr>
                              <?php endforeach; ?>
                              <!--End Content-->
                              <tr>
                                 <td align="left" height="30"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      </tr>
      <tr>
         <td height="15"></td>
      </tr>
      </table>
      </tr>
      </tbody></table>
