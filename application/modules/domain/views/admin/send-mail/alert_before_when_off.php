<?php 
   // CONFIG
   $services = $this->config->item('service', 'packages') ;

   // THÔN TIN DOMAIN
   $domain_service_package = get_term_meta_value($term->term_id,'domain_service_package'); 
   $label_domain_service_package = $services[$domain_service_package]['label'];

   $domain_fee_initialization          = get_term_meta_value($term->term_id, 'domain_fee_initialization') ?: $services[$domain_service_package]['fee_initialization'];
   $domain_fee_maintain                = get_term_meta_value($term->term_id, 'domain_fee_maintain') ?: $services[$domain_service_package]['fee_maintain'];  
    $domain_fee_initialization_maintain = get_term_meta_value($term->term_id, 'domain_fee_initialization_maintain') ?: $domain_fee_maintain + $domain_fee_initialization;

   // TỔNG GIÁ TRỊ HỢP ĐỒNG TRƯỚC VAT
   $contract_value = get_term_meta_value($term_id, 'contract_value') ?: 0;


   $is_expired = ($contract_end < $time);
   $bg_color = ($is_expired) ? '#dd4b39' : '#ff9f00';

   $service_date = $this->mdate->date('d/m/Y', $contract_begin).' - '. $this->mdate->date('d/m/Y', $contract_end);
   $contract_date = $this->mdate->date('d/m/Y', get_term_meta_value($term->term_id,'contract_begin')).' - ' . $this->mdate->date('d/m/Y', get_term_meta_value($term->term_id,'contract_end') );
    
   // $title_bar = ($is_expired) ? 'Thông báo thời hạn dịch vụ' : 'Cảnh báo thời hạn dịch vụ Cảnh báo hết hạn dịch vụ';
   $title_bar = 'Thông báo thời hạn dịch vụ';
   ?>
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid <?php echo $bg_color;?>;">
                        <table width="600" class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" align="center" bgcolor="#ff0000" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông báo gia hạn dịch vụ Domain</td>
                                 <!--End title-->
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <tr>
                                 <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                    Xin chào quý khách.<br>
                                    <p><b>WebDoctor.vn cảm ơn Quý khách đã tin tưởng sử dụng dịch vụ của chúng tôi.</b></p>
                                    <p><b>Chúng tôi xin thông báo về việc dịch vụ Lưu trữ Website (Domain) sắp hết hạn. Sau khi hết hạn Hợp đồng đăng ký Domain, hệ thống sẽ tự động tiến hành tắt Domain của quý khách và mọi thông tin (bao gồm mã nguồn và dữ liệu) sẽ không thể khôi phục<br/><br/>

                                    Quý khách vui lòng liên hệ nhân viên Kinh doanh của Webdoctor.vn để tiến hành gia hạn dịch vụ.</b></p>
                                 </td>
                              </tr>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <tr>
                                 <td>
                                    <!-- date -->
                                    <table style="border-radius:5px;" class="table1-3" bgcolor="#ecf0f1" width="183" border="0" align="left" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <tr>
                                             <td align="center">
                                                <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                   <tbody>
                                                      <tr><td align="center"><table border="0" align="center" cellpadding="0" cellspacing="0">
                                  <tbody>
                                    <tr>
                                      <td height="10"></td>
                                    </tr>
                                    <tr>
                                      <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:30px; color:#333333; font-weight: bold;">CÒN</td>
                                    </tr>
                                    <tr>
                                      <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:60px; color:#34495e;font-weight: bold;line-height: 48px;"> <?php echo $number_day_remaining_extend ;?></td>
                                    </tr>
                                    <tr>
                                      <td align="center" style="font-family: Open sans, Helvetica, sans-serif; font-size:30px; color:#3498db; font-weight: bold;">NGÀY</td>
                                    </tr>
                                    <tr>
                                      <td height="10"></td>
                                    </tr>
                                  </tbody>
                                </table></td></tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- End date -->
                                    <!--Space-->
                                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                       <tbody>
                                          <tr>
                                             <td height="25" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left:24px;">&nbsp;</p>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!--End Space-->
                                    <!--Content-->
                                    <table class="table3-1" width="342" border="0" align="right" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <tr>
                                             <td align="left" style="font-family: open sans, arial, sans-serif; font-size:15px; color:#444444; line-height:28px;">
                                                <b>Website:</b> <?php echo $term->term_name;?><br>
                                                <b>Gói Domain:</b> <?php echo $label_domain_service_package ?><br>
                                                <b>Ngày kết thúc hợp đồng:</b> <?php echo my_date($contract_end,'d/m/Y');?><br>
                                                <!--<b>Ngày off Domain:</b> <?php //echo my_date($domain_suspend_time,'d/m/Y');?><br>-->
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!--End Content-->
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <!--Content-->
                              <tr>
                                 <td align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444; line-height:28px;">
                                    <p style="font-family: open sans, arial, sans-serif; font-size:15px">Để thực hiện gia hạn dịch vụ vui lòng liên hệ với chúng tôi theo thông tin dưới đây:</p>
                                    <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <?php if(!empty($staff)) { ?> 
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Kinh doanh <?php if(!empty($staff->display_name)) echo $staff->display_name ;?>: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">
                                                <?php
                                                   $phone = '' ;

                                                   if(!empty($staff->user_id)) $phone = $this->usermeta_m->get_meta_value($staff->user_id,'user_phone');

                                                   if(!$phone) $phone = '(028) 7300. 4488';
                                                   
                                                   echo $phone; ?></td>
                                             </tr>
                                             <tr>
                                                <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;">Email: </td>
                                                <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;background: #f2f2ff;padding-left: 5px;padding-right: 5px;"><?php if(!empty($staff->user_email)) echo $staff->user_email ;?></td>
                                             </tr>
                                          <?php } ?>   
                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Hotline Trung tâm Kinh doanh:</td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">(028) 7300. 4488</td>
                                          </tr>
                                          <tr>
                                             <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#444444;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">Email: </td>
                                             <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">sales@webdoctor.vn</td>
                                          </tr>
                                          <tr>
                                             <td height="25"></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <p><b>Việc dịch vụ quá hạn không được gia hạn kịp thời có thể dẫn đến gián đoạn sử dụng dịch vụ của quý khách.</b></p>
                                    <p><i>Quý khách vui lòng bỏ qua thông báo này nếu đã thực hiện gia hạn dịch vụ.</i></p>
                                    <p style="text-align:right">CHÂN THÀNH CẢM ƠN</p>
                                 </td>
                              </tr>
                              <!--End Content-->
                              <tr>
                                 <td align="left" height="30"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td height="15"></td>
      </tr>
   </tbody>
</table>

<!-- thong tin hop dong -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin hợp đồng</td>
                                 <!--End title-->
                                 <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tbody>
                              <tr>
                                 <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--end detail-->
                     </td>
                  </tr>
                  <!--start Article-->
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                              <?php 
                                 $rows = array();
                                 $rows[] = array('Mã hợp đồng', get_term_meta_value($term_id,'contract_code'));
                                 $rows[] = array('Thời gian hợp đồng', $contract_date);
                                 $rows[] = array('Thời gian thực hiện', $service_date);
                                 $rows[] = array('Gói', $label_domain_service_package);
                                 $rows[] = array('Thời gian đăng ký', $domain_number_year_register . ' năm');
                                 $rows[] = array('Phí khởi tạo', currency_numberformat($domain_fee_initialization));
                                 $rows[] = array('Phí duy trì', currency_numberformat($domain_fee_maintain) . '/1 năm');

                                 // KIỂM TRA ĐÃ GIA HẠN HAY CHƯA
                                 $has_domain_renew  = get_term_meta_value($term_id, 'has_domain_renew') ?: 0;

                                 // TỔNG PHÍ GIA HẠN HẰNG NĂM
                                 $domain_fee_maintain_years = $domain_fee_maintain * ($domain_number_year_register - 1) ;
                                 if($has_domain_renew == 1) $domain_fee_maintain_years =  $domain_fee_maintain * $domain_number_year_register;

                                 if($has_domain_renew == 0)
                                 {
                                    $rows[] = array('Phí khởi tạo và duy trì năm đầu tiên', currency_numberformat($domain_fee_initialization_maintain));
                                 }

                                 $rows[] = array('Tổng tiền trước VAT', currency_numberformat($contract_value)  );
                                                                    
                                 foreach($rows as $i=>$row):
                                    ?>
                              <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                                 <tr>
                                    <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                    <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                    </td>
                                 </tr>
                              <?php endforeach; ?>                              
                              <tr>
                                 <td height="25"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <!--end Article-->
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- / thong tin hop dong -->

<!-- thong tin khach hang -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
   <tbody>
      <tr>
         <td height="15"></td>
      </tr>
      <tr>
         <td align="center">
            <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr>
                     <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                        <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <!--Title-->
                                 <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin khách hàng</td>
                                 <!--End title-->
                                 <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--Space-->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                           <tbody>
                              <tr>
                                 <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                    <p style="padding-left: 24px;">&nbsp;</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <!--End Space-->
                        <!--detail-->
                        <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--end detail-->
                     </td>
                  </tr>
                  <!--start Article-->
                  <tr>
                     <td bgcolor="#ffffff">
                        <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                           <tbody>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                  
                              <?php 
                                 $rows = array();
                                 
                                 $rows[] = array('Tên khách hàng', 'Anh/Chị ' . get_term_meta_value($term->term_id,'representative_name'));
                                 $rows[] = array('Email',get_term_meta_value($term->term_id, 'representative_email'));
                                 $rows[] = array('Điện thoại di động', get_term_meta_value($term->term_id, 'representative_phone'));
                                 $rows[] = array('Địa chỉ', get_term_meta_value($term->term_id, 'representative_address'));
                                 
                                 foreach($rows as $i=>$row):
                                    ?>
                              <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                              <tr>
                                 <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                 <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                 </td>
                              </tr>
                              <?php endforeach; ?>
                              <tr>
                                 <td height="25"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <!--end Article-->
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- / thong tin khach hang -->
