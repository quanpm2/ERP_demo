<div class="col-md-12" id="service_tab">
    <?php
    $this->load->config('domain/domain');

    // THÔNG TIN DOMAIN TRONG FILE CONFIG
    $services         = array_filter($this->config->item('service', 'packages'), function($x){ return $x['status'] == 'active'; });
    $domain_number_year_registers = $this->config->item('number_year_register') ;

    // DOMAIN ĐƯỢC CHỌN VÀ ĐÃ LƯU VÀO CSDL
    $domain_service_package = get_term_meta_value($edit->term_id, 'domain_service_package');
    if(empty($domain_service_package))
    {
        $domain         = parse_url(prep_url($edit->term_name), PHP_URL_HOST);
        $services       = $this->config->item('service', 'packages');
        $domain_exts    = array_column($services, 'name');

        $domain_exts = array_filter($domain_exts, function($x) use($domain) {
            $pos = strpos($domain, $x);
            return !(empty($pos) || ($pos+strlen($x) != strlen($domain))); 
        });

        switch (count($domain_exts))
        {
            case 0: break;
            case 1:
                $domain_service_package = reset($domain_exts);
                break;
            
            default:
                $domain_exts = array_flip($domain_exts);
                array_walk($domain_exts, function($item, $key){ $domain_exts[$key] = substr_count($key, '.'); });
                uasort($domain_exts, function($a, $b) { return $a <= $b; });
                $domain_exts = array_flip($domain_exts);
                $domain_service_package = reset($domain_exts);
                break;
        }

        $service_default  = $this->config->item('default', 'packages');

    }

    $domain_number_year_register_selected =  get_term_meta_value($edit->term_id, 'domain_number_year_register') ?: $domain_number_year_registers[1] ;

    echo $this->admin_form->form_open(), form_hidden('edit[term_id]', $edit->term_id), $this->admin_form->submit('', 'confirm_step_service', 'confirm_step_service', '', array(
        'style' => 'display:none;',
        'id' => 'confirm_step_service'
        )), $this->admin_form->formGroup_begin();

    // HIỂN THỊ CÁC GÓI DOMAIN
    if ($services)
    {
        $services = array_filter($services, function($x){ return $x['status'] == 'active'; });
        ksort($services);

        // BUTTON CHECKBOX
        foreach ($services as $sname => $service)
        {
            echo '<div class="col-md-4">';
            echo $this->admin_form->radio('edit[meta][domain_service_package]', $sname, (!$domain_service_package || $domain_service_package == $sname) ? TRUE : FALSE, array(
                'data-fee-maintain' => $service['fee_maintain'],
                'data-fee-initialization' => $service['fee_initialization'] ,
                'data-fee-initialization-maintain' => $service['fee_initialization_maintain'] ,
                )), nbs(5), form_label($service['label']);
            echo '</div>';
        }
        
        echo '<div style="clear:both"></div>';

        // THÔNG TIN CHI TIẾT CÁC GÓI DOMAIN
        echo '<div id="load-package-info">';

        // PHÍ DUY TRÌ VÀ KHỞI TẠO NĂM ĐẦU TIÊN
        echo $this->admin_form->input_numberic('Phí khởi tạo và duy trì năm đầu tiên'  , 'edit[meta][domain_fee_initialization_maintain]', $services[$domain_service_package]['fee_initialization_maintain'],'',array('id' => 'domain_fee_initialization_maintain','min' => 0));

        // PHÍ KHỞI TẠO
        echo $this->admin_form->input_numberic('Phí khởi tạo', 'edit[meta][domain_fee_initialization]', $services[$domain_service_package]['fee_initialization'], '', array('id'  => 'domain_fee_initialization','min' => 0));

        // PHÍ DUY TRÌ
        echo $this->admin_form->input_numberic('Phí duy trì', 'edit[meta][domain_fee_maintain]', $services[$domain_service_package]['fee_maintain'], '', array('id' => 'domain_fee_maintain','min' => 0,));

        // SỐ NĂM ĐĂNG KÝ 
        echo $this->admin_form->dropdown('Số năm đăng ký', 'edit[meta][domain_number_year_register]', $domain_number_year_registers ,$domain_number_year_register_selected,'',array('id' => 'domain_number_year_register', 'class' => 'form-control'));

        // GIÁ TRỊ HĐ
        echo $this->admin_form->input_numberic('Tổng tiền thanh toán', 'temp_contract_value', '', '', array('id' => 'temp_contract_value','min' => 0));

        echo '</div>';
    }



    echo $this->admin_form->formGroup_end(), $this->admin_form->submit('', 'confirm_step_service', 'confirm_step_service', '', array(
    'style' => 'display:none;',
    'id' => 'confirm_step_service'
    )), $this->admin_form->form_close();


    ?>
</div>

<script type="text/javascript">
    $(function(){
        /* init iCheck plugin */
        $('#service_tab input[type="radio"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        });
    }) ;

    var domain_number_year_register = 1 ;
    var temp_contract_value         = 0 ;

    function calculate_contract_value(domain_fee_initialization_maintain, domain_fee_maintain)
    {
        $('#temp_contract_value').val(0);
        var domain_number_year_register = $('#domain_number_year_register').val();
        var temp_contract_value         = (domain_fee_initialization_maintain) + (domain_fee_maintain * (domain_number_year_register - 1)) ;      
        $('#temp_contract_value').val(temp_contract_value);     
        return false ;
    }

    function init_contract_value()
    {
        var el                                 = $("#service_tab input:checked");

        var domain_fee_initialization          = $(el).data('fee-initialization') ;
        var domain_fee_maintain                = $(el).data('fee-maintain') ;
        var domain_fee_initialization_maintain = $(el).data('fee-initialization-maintain') ;

        calculate_contract_value(domain_fee_initialization_maintain, domain_fee_maintain) ;
        return false;
    }


    $(document).ready(function() {

        init_contract_value() ;

// CHỌN GÓI DOMAIN : HIỂN THỊ THÔNG TIN CHI TIẾT
$('#service_tab :input').on('ifChecked', function(event) {
    var package                    = $(this).val() ;

    $('#load-package-info input').val('');

    var domain_fee_initialization          = $(this).data('fee-initialization') ;
    var domain_fee_maintain                = $(this).data('fee-maintain') ;
    var domain_fee_initialization_maintain = $(this).data('fee-initialization-maintain') ;

    $('#domain_fee_initialization').val(domain_fee_initialization) ; 
    $('#domain_fee_maintain').val(domain_fee_maintain) ;
    $('#domain_fee_initialization_maintain').val(domain_fee_initialization_maintain) ;  

    calculate_contract_value(domain_fee_initialization_maintain, domain_fee_maintain)  
});

$('#domain_number_year_register').on('change', function (e) {
    init_contract_value() ;
});

// VALIDATE
var load_package_info = $("#load-package-info").closest('form');
load_package_info.validate({  
    rules: {
        'meta[domain_fee_initialization]': {
            required: true,
        },
        'meta[domain_fee_maintain]': {
            required: true,
        },
        'meta[domain_fee_initialization_maintain]': {
            required: true,
        }          
    },
    messages:{
        'meta[domain_fee_initialization]': {
            required: 'Phí khởi tạo không được để trống',
            digits: 'Kiểu dữ liệu không hợp lệ , phí khởi tạo phải là kiểu số',
        },
        'meta[domain_fee_maintain]': {
            required: 'Phí khởi tạo không được để trống',
            digits: 'Kiểu dữ liệu không hợp lệ , phí khởi tạo phải là kiểu số',
        },
        'meta[domain_fee_initialization_maintain]': {
            required: 'Phí duy trì và khởi tạo hàng năm không được để trống',
            digits: 'Kiểu dữ liệu không hợp lệ, phí duy trì và khởi tạo hàng năm phải là kiểu số',
        },
    }
});
}) ;
</script>