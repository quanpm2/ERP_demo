<?php 
  // --------------------------------------------------------------------
  // - Load file cấu hình : domain/config/domain.php
  // - Thông số cấu hình
  // --------------------------------------------------------------------
  $this->config->load('domain/domain') ;

  // THÔNG TIN CÁC GÓI DOMAIN
  $services = $this->config->item('service', 'packages') ;
?>
<div class="col-md-12" id="review-partial">

  <h2 class="page-header">
    
    <small class="pull-right">Ngày tạo : <?php echo $this->mdate->date('d-m-Y'); ?></small>
  </h2>

  <div class="row">
  <?php

  // --------------------------------------------------------------------
  // THÔNG TIN KHÁCH HÀNG
  // --------------------------------------------------------------------
    $representative_gender  = force_var(get_term_meta_value($edit->term_id,'representative_gender'),'Bà','Ông');
  $representative_name    = get_term_meta_value($edit->term_id,'representative_name') ?: '';
  $display_name           = "{$representative_gender} {$representative_name}";
  $representative_email   = get_term_meta_value($edit->term_id,'representative_email');
  $representative_address = get_term_meta_value($edit->term_id,'representative_address');
  $representative_phone   = get_term_meta_value($edit->term_id,'representative_phone');

  $contract_begin       = get_term_meta_value($edit->term_id,'contract_begin');
  $contract_begin_date  = my_date($contract_begin,'d/m/Y');
  $contract_end         = get_term_meta_value($edit->term_id,'contract_end');
  $contract_end_date    = my_date($contract_end,'d/m/Y');
  $contract_daterange   = "{$contract_begin_date} đến {$contract_end_date}";
        
  echo $this->admin_form->set_col(6)->box_open('Thông tin khách hàng');
  echo $this->table->clear()
    ->add_row('Người đại diện',$display_name?:'Chưa cập nhật')
    ->add_row('Email',$representative_email?:'Chưa cập nhật')
    ->add_row('Địa chỉ',$representative_address?:'Chưa cập nhật')
    ->add_row('Số điện thoại',$representative_phone?:'Chưa cập nhật')
    ->add_row('Chức vụ',$edit->extra['representative_position']??'Chưa cập nhật')
    ->add_row('Mã Số thuế',$edit->extra['customer_tax']??'Chưa cập nhật')
    ->add_row('Thời gian thực hiện',$contract_daterange?:'Chưa cập nhật')
    ->generate();
  echo $this->admin_form->box_close();

  
  // --------------------------------------------------------------------
  // THÔNG TIN HỢP ĐỒNG
  // --------------------------------------------------------------------

    // GÓI DOMAIN
    $domain_service_package = $this->termmeta_m->get_meta_value($edit->term_id, 'domain_service_package');
    
    // PHÍ KHỞI TẠO
    $domain_fee_initialization  = get_term_meta_value($edit->term_id, 'domain_fee_initialization') ?: $services[$domain_service_package]['fee_initialization'];

    // PHÍ DUY TRÌ
    $domain_fee_maintain        = get_term_meta_value($edit->term_id, 'domain_fee_maintain') ?: $services[$domain_service_package]['fee_maintain'];

    // PHÍ DUY TRÌ VÀ KHỞI TẠO NĂM ĐẦU TIÊN
    $domain_fee_initialization_maintain = get_term_meta_value($edit->term_id, 'domain_fee_initialization_maintain') ?: $domain_fee_maintain + $domain_fee_initialization;

    $domain_number_year_register       = get_term_meta_value($edit->term_id, 'domain_number_year_register') ?: 1 ;
    $contract_value             = $domain_fee_initialization_maintain + ($domain_fee_maintain * ($domain_number_year_register - 1)) ;

    // THỜI GIAN BẮT ĐẦU HỢP ĐỒNG
    $contract_begin             = get_term_meta_value($edit->term_id, 'contract_begin') ;

    // CẬP NHẬT THỜI GIAN KẾT THÚC
    $contract_end               = strtotime("+ $domain_number_year_register year", $contract_begin) ;
    update_term_meta($edit->term_id, 'contract_end', $contract_end) ;

    // CẬP NHẬT GIÁ TRỊ HĐ
    update_term_meta($edit->term_id, 'contract_value', $contract_value);   

    // HIỂN THỊ THÔNG TIN DOMAIN
    echo $this->admin_form->set_col(6)->box_open('Thông tin hợp đồng');
    echo $this->table->clear()
                    ->add_row('Gói domain', $services[$domain_service_package]['label'])
                    ->add_row(array('Phí khởi tạo', currency_numberformat($domain_fee_initialization)))
                    ->add_row(array('Phí duy trì',  currency_numberformat($domain_fee_maintain)))
                    ->add_row(array('Phí khởi tạo và duy trì năm đầu tiên', currency_numberformat($domain_fee_initialization_maintain)))
                    ->add_row(array('Phí duy trì các năm tiếp theo', currency_numberformat(($domain_fee_maintain * ($domain_number_year_register - 1)))))
                    ->add_row('Thời gian đăng ký', $domain_number_year_register . ' năm')
                    ->add_row('Tổng chi phí', currency_numberformat(get_term_meta_value($edit->term_id, 'contract_value')))
                    ->generate();
    echo $this->admin_form->box_close();

 ?>
  </div>

</div>
<div class="clearfix"></div>

<?php

$hidden_values = ['edit[term_status]'=>'waitingforapprove','edit[term_id]'=>$edit->term_id,'edit[term_type]'=>$edit->term_type];
echo $this->admin_form->form_open('',[],$hidden_values);
echo $this->admin_form->submit('','confirm_step_finish','confirm_step_finish','', array('style'=>'display:none;','id'=>'confirm_step_finish'));
echo $this->admin_form->form_close();
?>