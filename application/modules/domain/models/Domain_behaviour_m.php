<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Behaviour_m.php');

class Domain_behaviour_m extends Behaviour_m {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('date');

		$this->config->load('domain/domain') ;
		$this->config->load('contract/contract');
	}

	/**
	 * Preview data for printable contract version
	 *
	 * @return     String  HTML content
	 */
	public function prepare_preview()
	{
		if( ! $this->contract) return FALSE;

		parent::prepare_preview();

		$data = $this->data;

		
		$data['service_packages'] = $this->config->item('service','packages');
		$data['domain_service_package'] = get_term_meta_value($this->contract->term_id, 'domain_service_package');

		$domain_cells = array();

		$max_cells 		= 48 ;
        $domain_cells 	= str_split($this->contract->term_name) ;

		$count_row_term_name = count($domain_cells) ;
		$missing_row         = $max_cells - $count_row_term_name;

		if($missing_row > 0)
		{
		    $add_row_empty = array();
		    $number_loop   = $max_cells - $count_row_term_name; 
		    for($i = 1; $i <= $number_loop; $i++)
		    {
		        $add_row_empty[$i] = '';
		    }
		}
		$domain_cells = array_merge($domain_cells, $add_row_empty);
		$data['domain_cells'] = array_chunk($domain_cells, 24);
		
		$data['content'] 	= $this->load->view('domain/contract/preview', $data ,TRUE);

		return $data;
	}


	/**
	 * Calculates the contract value.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The contract value.
	 */
	public function calc_contract_value()
	{
		parent::is_exist_contract(); // Determines if exist contract.

		$domain_packages = $this->config->item('service', 'packages');

		$term_id 			= $this->contract->term_id;
		$service_package 	= get_term_meta_value($term_id, 'domain_service_package'); // GÓI DOMAIN ĐƯỢC CHỌN
		
	    $fee_maintain 	= (int) get_term_meta_value($term_id, 'domain_fee_maintain') ?: $domain_packages[$service_package]['fee_maintain']; // PHÍ DUY TRÌ
	    $fee_maintain 	= $fee_maintain ?: $domain_packages[$service_package]['fee_maintain'];

		$number_year_register 	= get_term_meta_value($term_id, 'domain_number_year_register') ?: 1 ; // SỐ NĂM ĐĂNG KÝ

	    // KIỂM TRA ĐÃ GIA HẠN HAY CHƯA
	    $has_domain_renew  = get_term_meta_value($term_id, 'has_domain_renew');
	    if($has_domain_renew) return $fee_maintain * $number_year_register;

		$fee_initialization  	= (int) get_term_meta_value($term_id, 'domain_fee_initialization'); // PHÍ KHỞI TẠO
		$fee_initialization 	= $fee_initialization ?: $domain_packages[$service_package]['fee_initialization'];
	    
	    $fee_initialization_maintain = (int) get_term_meta_value($term_id, 'domain_fee_initialization_maintain'); // PHÍ KHỞI TẠO VÀ DUY TRÌ NĂM ĐẦU TIÊN
	    $fee_initialization_maintain = $fee_initialization_maintain ?: ($fee_maintain + $fee_initialization);

	    return $fee_initialization_maintain + ($fee_maintain * ($number_year_register - 1));
	}

    /**
     * Compute Actual Budget of Paid Payments
     * @return double Actual Budget
     */
    public function calc_payment_service_fee()
    {
        if( ! $this->contract) throw new \Exception('Property Contract undefined.');

        $stop_status = ['ending', 'liquidation'];
        if(!in_array($this->contract->term_status, $stop_status)) return 0;

        return parent::calc_payment_service_fee();
    }
}
/* End of file Domain_behaviour_m.php */
/* Location: ./application/modules/domain/models/Domain_behaviour_m.php */