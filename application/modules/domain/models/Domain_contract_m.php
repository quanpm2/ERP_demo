<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Domain_contract_m extends Base_contract_m {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('domain/domain_m');

		$this->config->load('domain/domain') ;
		$this->config->load('contract/contract');
	}

	public function calc_contract_value($term_id = 0)
	{
		if(empty($term_id)) return FALSE;

		$total 			 		  = 0;

		$domain_fee_initialization  		= get_term_meta_value($term_id, 'domain_fee_initialization');
      	$domain_fee_maintain        		= get_term_meta_value($term_id, 'domain_fee_maintain');

      	// SỐ NĂM ĐĂNG KÝ
		$domain_number_year_register 		= get_term_meta_value($term_id, 'domain_number_year_register') ?: 1 ;

      	// KIỂM TRA ĐÃ GIA HẠN HAY CHƯA
	    $has_domain_renew  = get_term_meta_value($term_id, 'has_domain_renew') ?: 0;
	 
	    // GIÁ TRỊ HĐ
	    $contract_value = ($domain_fee_initialization + $domain_fee_maintain) + ($domain_fee_maintain * ($domain_number_year_register - 1)) ;
		if($has_domain_renew == 1) $contract_value =  $domain_fee_maintain * $domain_number_year_register;
		
		$domain_cost 	 		  = $contract_value;
		$domain_discount_percent = (int)get_term_meta_value($term_id,'domain_discount_percent');

		if(!empty($domain_discount_percent) && $domain_discount_amound = ($domain_cost * div($domain_discount_percent,100)))
		{
			$domain_cost-=$domain_discount_amound;
		}

		$total+= $domain_cost;
		return $total;
	}
	
	public function has_permission($term_id = 0, $name = '', $action = '',$kpi_type = '',$user_id = 0,$role_id = null)
	{
		$this->load->model('webgeneral/webgeneral_kpi_m');

		$permission = $name.'.'.$action;
		$permission = trim($permission, '.');
		
		if($this->permission_m->has_permission("{$name}.Manage",$role_id)
			&& $this->permission_m->has_permission($permission,$role_id)) 
			return TRUE;

		if(!$this->permission_m->has_permission($permission,$role_id)) return FALSE;

		$user_id = empty($user_id) ? $this->admin_m->id : $user_id;

		if($this->permission_m->has_permission('domain.sale.access',$role_id) 
			&& get_term_meta_value($term_id,'staff_business') == $user_id)
			return TRUE;

		$args = array(
			'term_id' => $term_id,
			'user_id' => $user_id);

		if(!empty($kpi_type))
		{
			$args['kpi_type'] = $kpi_type;
		}

		$kpis = $this->webgeneral_kpi_m->get_by($args);
		if(empty($kpis)) return FALSE;

		return TRUE;
	}

	public function start_service($term = null)
	{
		restrict('domain.start_service');	
		if(empty($term) || $term->term_type !='domain') return FALSE;

		/***
		 * Khởi tạo Meta thời gian dịch vụ để thuận tiện cho tính năng sắp xếp và truy vấn
		 */
		$start_service_time = get_term_meta_value($term->term_id, 'start_service_time');
		empty($start_service_time) AND update_term_meta($term->term_id, 'start_service_time', 0);

		return $this->proc_service($term);
	}

	public function proc_service($term = FALSE)
	{
		$this->load->model('domain/domain_report_m') ;
		if(!$term || $term->term_type != 'domain') return FALSE;
		$term_id = $term->term_id;

		$staff_id            = get_term_meta_value($term_id, 'staff_business');    
        if( ! $staff_id ) $this->messages->info('Chưa có nhân viên kinh doanh');
     
		$is_send = $this->domain_report_m->send_mail_info_activated($term_id) ;



		if(FALSE === $is_send) return FALSE ;
		update_term_meta($term_id, 'start_service_time' , time());

		/* Phân tích hợp đồng ký mới | tái ký */
		$this->load->model('contract/base_contract_m');
		$this->base_contract_m->detect_first_contract($term_id);

		/* Gửi SMS thông báo kích hoạt hợp đồng đến khách hàng */
		$this->load->model('contract/contract_report_m');
		$this->contract_report_m->send_sms_activation_2customer($term_id);

		$this->log_m->insert(array(
						'log_type' =>'start_service_time',
						'user_id' => $this->admin_m->id,
						'term_id' => $term->term_id,
						'log_content' => date('Y/m/d H:i:s', time()). ' - Start Service Time'
					 ));

		return TRUE;
	}

	public function stop_service($term)
	{
		$this->load->model('domain/domain_report_m') ;
		if(!$term || $term->term_type != 'domain') return FALSE;
		$term_id = $term->term_id;
		
		$is_send = $this->domain_report_m->send_mail_info_finish($term_id);
		if( FALSE === $is_send) return FALSE ;

		update_term_meta($term_id,'end_service_time', time());
		$this->contract_m->update($term_id, array('term_status'=>'ending'));
		$this->log_m->insert(array(
			'log_type' =>'end_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term_id,
			'log_content' => date('Y/m/d H:i:s', time()) . ' - End Service Time'
			));

        // Push queue sync service fee
        $this->load->config('amqps');
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        
        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];

        $connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $channel     = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $payload = [
            'event' 	=> 'contract_payment.sync.service_fee',
            'contract_id' => $term_id
        ];
        $message = new \PhpAmqpLib\Message\AMQPMessage(
            json_encode($payload),
            array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $channel->basic_publish($message, '', $queue);	

        $channel->close();
        $connection->close();
        
		return TRUE;
	}


	public function renew($term_id = 0, $data = null)
	{
		$this->load->model('contract/contract_m') ;
		$this->load->model('domain/domain_report_m');

		$domain_contract_end = $data['domain_contract_end'] ;
		$domain_number_year_register = $data['domain_number_year_register'] ;
			
		# Nhân bản domain
		$term_renew_id = $this->clone($term_id);
		if(empty($term_renew_id)) return FALSE ;

		# Thông tin hợp đồng vừa clone
		$term 				  = $this->term_m->get($term_renew_id);
		
		# Cập nhật lại thời gian
		$domain_contract_end = $data['domain_contract_end'] ;
		update_term_meta($term_renew_id, 'contract_begin', $domain_contract_end);
		update_term_meta($term_renew_id, 'contract_end', strtotime( "+$domain_number_year_register year", $domain_contract_end)) ;
		update_term_meta($term_renew_id,'end_contract_time', '');

		# Cập nhật lại thông số

		$_contract = new contract_m();
		$_contract->set_contract($term);

		// contract codes
		update_term_meta($term->term_id,'contract_code', $_contract->gen_contract_code());

		// % giảm giá domain
		update_term_meta($term_renew_id,'domain_discount_percent', 0);
		
		// cập nhật thông tin lại gói domain
		$domain_service_package = get_term_meta_value($term_renew_id, 'domain_service_package') ;
		$services = $this->config->item('service','packages');
		if(!empty($services) && $domain_service_package)
		{
			foreach ($services as $key => $service) 
			{
				if($key == $domain_service_package)
				{
					// number_year_register
					update_term_meta($term_renew_id,'domain_number_year_register', $domain_number_year_register);

					// fee_initialization
					update_term_meta($term_renew_id,'domain_fee_initialization', $service['fee_initialization']);

					// fee_maintain
					update_term_meta($term_renew_id,'domain_fee_maintain', $service['fee_maintain']);

					// fee_initialization_maintain
					update_term_meta($term_renew_id,'domain_fee_initialization_maintain', $service['fee_initialization'] + $service['fee_maintain']);

					// contract value 
					$contract_value = $service['fee_maintain'] * $domain_number_year_register;
					update_term_meta($term_renew_id,'contract_value', $contract_value) ; 
				}
			}
			update_term_meta($term_renew_id,'has_domain_renew', 1);
		}

		// trạng thái
		$this->term_m->update($term_renew_id, array('term_status' => 'waitingforapprove'));
			
		# Lưu log
		$is_log = $this->log_m->insert(array(
					'log_type' => "clone_contract_domain",
					'user_id'  => $this->admin_m->id,
					'term_id'  => $term_renew_id,
					'log_content' => date('Y/m/d H:i:s', time()) . '- Clone domain'
				  ));
		if($is_log == FALSE) return FALSE;

		# Kết thúc HĐ cũ
 		$result = $this->contract_m->stop_contract($term_id);
 		if(!$result) return FALSE ;
 		
 		// Tự động tạo các đợt thanh toán cho hợp đồng mới gia hạn
 		$term_renew = $this->domain_m->set_term_type()->get($term_renew_id);
 		if(!empty($term_renew))
 		{
	 		parent::create_default_invoice($term_renew);
	 		parent::sync_all_amount($term_renew_id);
 		}

		return $term_renew_id;
	}

    /**
	 * Gửi email nhắc khách hàng gia hạn dịch vụ
	 *
	 * @param      <type>  $term   The term
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function notice_renew_service($term = FALSE)
	{
        if(!$term || $term->term_type != 'domain') return FALSE;
		$term_id = $term->term_id;

		$this->load->model('domain/domain_report_m') ;
		$is_send = $this->domain_report_m->send_mail_alert_before_when_off($term_id);

		if(FALSE === $is_send) return FALSE ;

		return TRUE;
	}
}

/* End of file domain_contract_m.php */
/* Location: ./application/modules/domain/models/domain_contract_m.php */