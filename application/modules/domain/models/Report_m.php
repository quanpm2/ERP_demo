<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Webdoctor_report_m.php');

class Report_m extends Webdoctor_report_m {

    function __construct()
    {
        $this->recipients['cc'][] = 'luannn@webdoctor.vn';
        $this->recipients['cc'][] = 'haitm@adsplus.vn';

        $this->autoload['models'][] = 'domain/domain_m';
        
        parent::__construct();
    }

    /**
     * Sends an activation email.
     *
     * @param      string  $type_to  The type to
     */
    public function send_activation_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;

        $this->data['subject']      = '[DOMAIN] Hợp đồng '.get_term_meta_value($this->term_id, 'contract_code').' đã được kích hoạt.';
        $this->data['content_tpl']  = 'domain/report/activation_email';
        $this->data['sale']         = $this->sale;

        $this->data['domain_number_year_register'] = get_term_meta_value($this->term_id, 'domain_number_year_register') ?: 1 ;

        return parent::send_activation_email($type_to);
    }

    public function init($term_id = 0)
    {
        $term = $this->domain_m->set_term_type()->get($term_id);
        if( ! $term) return FALSE;

        $this->term     = $term;
        $this->term_id  = $term_id;

        $this->init_people_belongs();
        return $this;
    }

    /**
     * Sets the mail to.
     *
     * @param      string  $type_to  The type to
     *
     * @return     self    ( description_of_the_return_value )
     */
    public function set_mail_to($type_to = 'admin')
    {
        parent::set_mail_to($type_to);

        $recipients = $this->recipients;

        if($type_to == 'customer')
        {
            // Nếu gửi mail cho khách hàng
			if($representative_email = get_term_meta_value($this->term_id,'representative_email'))
			{
				$this->recipients['to'][] = $representative_email;
			}

			// Email người nhận báo cáo được cấu hình theo dịch vụ
			if($mail_report = get_term_meta_value($this->term_id, 'mail_report'))
			{
				$mails = explode(',',trim($mail_report));
				if(!empty($mails))
				{
					foreach ($mails as $mail) 
					{
						$this->recipients['to'][] = $mail;
					}
				}
			}

            $this->recipients  = $recipients;
            return $this;
        }

        return $this;
    }
}
/* End of file Report_m.php */
/* Location: ./application/modules/googleads/models/Report_m.php */