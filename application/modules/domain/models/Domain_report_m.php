<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Domain_report_m extends Base_Model {

	public function __construct() 
	{
		parent::__construct();

		$this->load->library('email');

		$this->load->model('staffs/admin_m');

		$this->config->load('domain/domain') ;

		defined('SALE_MANAGER_ID') OR define('SALE_MANAGER_ID',18);
		defined('NUMDAYS_BEFORE_SUSPEND') OR define('NUMDAYS_BEFORE_SUSPEND',6);
		defined('NOW_TIME') OR define('NOW_TIME',$this->config->item('now_time', 'times'));
	}

	// -------------------------------------------------------
	// KHỞI TẠO HĐ - GỬI MAIL KÍCH HOẠT CHO KHÁCH HÀNG
	// -------------------------------------------------------
	public function send_mail_info_activated($term_id = 0)
	{
		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

        $title  = 'Hợp đồng '.get_term_meta_value($term_id, 'contract_code').' đã được kích hoạt.';

		$data['term_id']          				= $term_id ;
		$data['title']            			    = $title ;
		$data['email_source']            			    = $title ;
		$data['term']							= $term;

		// SỐ NĂM ĐĂNG KÝ DOMAIN
		$domain_number_year_register 		 = get_term_meta_value($term_id, 'domain_number_year_register') ?: 1 ;
		$data['domain_number_year_register'] = $domain_number_year_register ;

		// HIỂN THỊ THÔNG TIN KINH DOANH
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	   = $sale;
		
		$this->set_mail_to($term_id, 'customer') ;

		$this->email->subject($title);

		$data['content']	= $this->load->view('domain/admin/send-mail/tpl_activation_email', $data, TRUE);
		$content 			= $this->load->view('email/blue_modern_tpl',$data,TRUE);
		$this->email->message($content);
		
		$send_status = $this->email->send();
		if(!$send_status) return FALSE;

		// THỰC HIỆN GHI LOG	
		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Gửi mail khi kích hoạt domain bằng email',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id,
			'log_content'	=> 'Đã gửi mail thành công',
			'log_type'		=> 'domain-report-email',
			'log_time_create' => date('Y-m-d H:i:s'),
		));

		return TRUE;
	}

	// -------------------------------------------------------
	// KẾT THÚC HĐ - GỬI MAIL KẾT THÚC CHO KHÁCH HÀNG
	// -------------------------------------------------------
	public function send_mail_info_finish($term_id = 0)
	{
		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

		$data = array();
		$data['time'] = time();
		$data['title'] = 'Kết thúc hợp đồng';

		$data['term'] = $term;
		$data['term_id'] = $term_id;

		// SỐ NĂM ĐĂNG KÝ DOMAIN
		$domain_number_year_register 		 = get_term_meta_value($term_id, 'domain_number_year_register') ?: 1 ;
		$data['domain_number_year_register'] = $domain_number_year_register ;

		$title = 'Thông báo kết thúc hợp đồng Webdoctor';
		$data['email_source']            			    = $title ;

		// HIỂN THỊ THÔNG TIN KINH DOANH
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	   = $sale;

		// GỬI MAIL
		$this->set_mail_to($term_id, 'customer') ;
		     //->set_mail_to($term_id, 'mailreport')
		     //->set_mail_to($term_id, 'admin') ;
		$this->email->subject($title);
		
		$data['content']	= $this->load->view('domain/admin/send-mail/end_contract', $data, TRUE);
		$content 			= $this->load->view('email/blue_modern_tpl',$data,TRUE);
		$this->email->message($content);

 		$send_status 		= $this->email->send();
		if ( FALSE === $send_status ) return FALSE ;
		
		// GHI LOG	
		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Gửi mail kết thúc hợp đồng',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id,
			'log_content'	=> 'Đã gửi mail thành công',
			'log_type'		=> 'domain-report-email',
			'log_time_create' => date('Y-m-d H:i:s'),
		));

		return TRUE ;
	} 

	// -------------------------------------------------------
	// GỬI MAIL THÔNG BÁO TRƯỚC 30 NGÀY KẾT THÚC DOMAIN
	// -------------------------------------------------------

	public function send_mail_alert_before_when_off($term_id = 0)
	{
		// SỐ NGÀY THÔNG BÁO TRƯỚC KHI KẾT THÚC DOMAIN
		$number_day_alert_before_when_off = $this->config->item('number_day_alert_before_when_off', 'times') ;

		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

		$data 			 = array();

		// NGÀY KẾT THÚC HỢP ĐỒNG
		$contract_end 	= get_term_meta_value($term_id,'contract_end');
		if( ! $contract_end ) return FALSE ;
		$contract_end   = $this->mdate->startOfDay($contract_end);
		
		// THỜI GIAN HIỆN TẠI
		$now    		 = $this->mdate->startOfDay(NOW_TIME);
		$data['time']    = $now;

		// TẠO CHUỖI THỜI GIAN
		$string_day      = $this->mdate->date('d/m/Y h:i:s', $now) ;

		// MÃ HỢP ĐỒNG
		$contract_code = get_term_meta_value($term_id, 'contract_code') ;

		// SỐ NĂM ĐĂNG KÝ DOMAIN
		$domain_number_year_register 		 = get_term_meta_value($term_id, 'domain_number_year_register') ?: 1 ;
		$data['domain_number_year_register'] = $domain_number_year_register ;

		// TÍNH MỐC THỜI GIAN GỬI MAIL
		$timeline	=	strtotime("- $number_day_alert_before_when_off days", $contract_end) ;
		$timeline 	=	$this->mdate->startOfDay($timeline) ;
		
		$number_day_remaining_extend = diffInDates($now, $contract_end);
		$data['number_day_remaining_extend'] = $number_day_remaining_extend ;

		$data['contract_begin'] = get_term_meta_value($term_id,'send_mail_alert_before_when_off');
		$data['contract_end'] = get_term_meta_value($term_id,'contract_end');


		$data['title']   = "Cảnh báo Hợp đồng $contract_code sắp hết hạn ($string_day)" ;
		$data['email_source']            			    = $data['title'] ;
		$data['term']    = $term;
		$data['term_id'] = $term_id;

		// HIỂN THỊ THÔNG TIN KINH DOANH
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	   = $sale;
		$data['sale'] 	   = $sale;

		// THÔNG TIN GỬI MAIL
		$data['content']	= $this->load->view('domain/report/send_mail_alert_before_when_off', $data, TRUE);
		$content 			= $this->load->view('email/blue_modern_tpl',$data,TRUE);
		
        $this->set_mail_to($term_id,'customer');
		$this->email->subject($data['title']);
		$this->email->message($content);
		$send_status = $this->email->send();

		// ĐÃ GỬI MAIL THÔNG BÁO THÀNH CÔNG
		if(TRUE == $send_status) {
			update_term_meta($term_id,'send_mail_alert_before_when_off', $number_day_alert_before_when_off);
			// GHI LOG
			$this->log_m->insert(array(
					'log_title'			=> 'Email thông báo gia hạn dịch vụ domain trước ' . $number_day_alert_before_when_off . ' ngày',
					'log_status'		=> 1,
					'term_id'			=> $term_id,
					'user_id'			=> $this->admin_m->id,
					'log_content'		=> '',
					'log_type'			=> 'send_mail_alert_before_when_off',
					'log_time_create' 	=> date('Y-m-d')
			));
		}

		return $send_status;
	}

	// -------------------------------------------------------
	// GỬI MAIL THÔNG BÁO CHO ADMIN TẮT DOMAIN TRONG 3 NGÀY
	// -------------------------------------------------------

	public function send_mail_alert_admin_off_domain($term_id = 0)
	{
		// SỐ NGÀY THÔNG BÁO TRƯỚC KHI KẾT THÚC DOMAIN
		$number_day_alert_admin_off_domain = $this->config->item('number_day_alert_admin_off_domain', 'times') ;

		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

		$data 			 = array();

		// SỐ NĂM ĐĂNG KÝ DOMAIN
		$domain_number_year_register 		 = get_term_meta_value($term_id, 'domain_number_year_register') ?: 1 ;
		$data['domain_number_year_register'] = $domain_number_year_register ;

		// NGÀY KẾT THÚC HỢP ĐỒNG
		$contract_end 	= get_term_meta_value($term_id,'contract_end');
		if( ! $contract_end ) return FALSE ;
		$contract_end   = $this->mdate->startOfDay($contract_end);
		
		// THỜI GIAN HIỆN TẠI
		$now    		 = $this->mdate->startOfDay(NOW_TIME);
		$data['time']    = $now;

		// TẠO CHUỖI THỜI GIAN
		$string_day      = $this->mdate->date('d/m/Y h:i:s', $now) ;

		// MÃ HỢP ĐỒNG
		$contract_code = get_term_meta_value($term_id, 'contract_code') ;

		// SỐ NĂM ĐĂNG KÝ DOMAIN
		$domain_number_year_register 		 = get_term_meta_value($term_id, 'domain_number_year_register') ?: 1 ;
		$data['domain_number_year_register'] = $domain_number_year_register ;

		// TÍNH MỐC THỜI GIAN GỬI MAIL
		$timeline	=	strtotime("+ $number_day_alert_admin_off_domain days", $contract_end) ;
		$timeline 	=	$this->mdate->startOfDay($timeline) ;
		
		$number_day_remaining_extend = diffInDates($now, $timeline);
		$data['number_day_remaining_extend'] = $number_day_remaining_extend ;

		$data['contract_begin'] = get_term_meta_value($term_id,'send_mail_alert_admin_off_domain');
		$data['contract_end'] = get_term_meta_value($term_id,'contract_end');


		$data['title']   = "[WEBDOCTOR] Cảnh báo Hợp đồng $contract_code đã hết hạn ($string_day)" ;
		$data['term']    = $term;
		$data['term_id'] = $term_id;

		// HIỂN THỊ THÔNG TIN KINH DOANH
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	   = $sale;

		// THÔNG TIN GỬI MAIL
		$content = $this->load->view('admin/send-mail/alert_admin_off_domain', $data, TRUE);
		$this->set_mail_to($term_id,'admin');
		$this->email->subject($data['title']);
		$this->email->message($content);
		$send_status   = $this->email->send();

		// ĐÃ GỬI MAIL THÔNG BÁO THÀNH CÔNG
		if(TRUE == $send_status) {
			update_term_meta($term_id,'send_mail_alert_admin_off_domain', $number_day_alert_admin_off_domain);
			// GHI LOG
			$this->log_m->insert(array(
					'log_title'			=> 'Email thông báo cho admin tắt domain trong ' . $number_day_alert_admin_off_domain . ' ngày',
					'log_status'		=> 1,
					'term_id'			=> $term_id,
					'user_id'			=> $this->admin_m->id,
					'log_content'		=> '',
					'log_type'			=> 'send_mail_alert_admin_off_domain',
					'log_time_create' 	=> date('Y-m-d')
			));
		}

		return $send_status;
	}

	/**
	 * Sets the mail to.
	 *
	 * @param      integer  $term_id  The term identifier
	 * @param      string   $type_to  The type to [admin|customer|mailreport|specified]
	 *
	 * @return     self     ( description_of_the_return_value )
	 */
	public function set_mail_to($term_id = 0,$type_to = 'admin')
    {
        $this->email->clear(TRUE);

        $recipients = array(
            'mail_to' => array(),
            'mail_cc' => array(),
            'mail_bcc' => array('thonh@webdoctor.vn')
        );

        if(is_array($type_to) && !empty($type_to)) 
        {
            $recipients = array_merge_recursive($type_to,$recipients);
            $type_to = $type_to['type_to'] ?? 'specified';
        }

        extract($recipients);

        $this->email->from('support@adsplus.vn', 'Adsplus.vn');

        // Nếu email được cấu hình phải gửi cho một nhóm người được truyền vào
        if($type_to == 'specified')
        {
        	$this->recipients = $mail_to;
	        $this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
	        return $this;
        }

        // Nếu email được cấu hình gửi cho nhóm quản lý
        if($type_to == 'mailreport')
        {
        	$mail_to = array('thonh@webdoctor.vn');
        	$this->recipients = $mail_to;
        	$this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
	        return $this;
        }

        // Nếu KD nghỉ, người nhận là người quản lý
        $sale_id = get_term_meta_value($term_id,'staff_business');
        $sale 	 = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID); 
        
        // Nếu gửi mail cho 'admin'
        if($type_to == 'admin')
        {
        	if($sale)
        	{
        		$mail_cc[] = $sale->user_email;
        	}

        	$this->recipients = $mail_to;
        	$this->email->to($mail_to)->cc($mail_cc)->bcc($mail_bcc);
	        return $this;
        }

        // Nếu gửi mail cho khách hàng
		if($representative_email = get_term_meta_value($term_id,'representative_email'))
		{
			$mail_to[] = $representative_email;
		}

		// Email người nhận báo cáo được cấu hình theo dịch vụ
		if($mail_report = get_term_meta_value($term_id,'mail_report'))
		{
			$mails = explode(',',trim($mail_report));
			if(!empty($mails))
			{
				foreach ($mails as $mail) 
				{
					$mail_to[] = $mail;
				}
			}
		}

		if(!empty($tech_kpi))
    	{
    		foreach ($tech_kpi as $i)
    		{
    			$mail = $this->admin_m->get_field_by_id($i->user_id,'user_email');
    			if(empty($mail)) continue;

    			$mail_cc[] = $mail;
    		}
    	}

    	if($sale)
    	{
    		$mail_cc[] = $sale->user_email;
    	}

        $this->recipients = $mail_to;

        $this->email->from('support@adsplus.vn', 'Adsplus.vn')
		        	->to($mail_to)
		        	->cc($mail_cc)
		        	->bcc($mail_bcc);
        
        return $this;
    }

	// THÔNG BÁO SẮP HẾT HẠN HỢP ĐỒNG
	public function send_mail_alert_expiring_soon($term_id = 0, $type = 'before' , $day = '')
	{
		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

		$data 			 = array();
		$data['time']    = time();
		$data['title']   = 'Cảnh báo sắp hết hạn hợp đồng';
		$data['email_source']            			    = $data['title'] ;
		$data['term']    = $term;
		$data['term_id'] = $term_id;

		// Hiển thị thông tin kinh doanh ra ngoài view
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	   = $sale;

		$this->set_mail_to($term_id,'customer');
			 //->set_mail_to($term_id, 'admin');
		$this->email->subject($data['title']);

		$data['content']	= $this->load->view('admin/send-mail/warning_expires', $data, TRUE);
		$content 			= $this->load->view('email/blue_modern_tpl',$data,TRUE);
		$this->email->message($content);

		$send_status   = $this->email->send();
		
		if( FALSE === $send_status ) return FALSE ;

		if( ! empty($type) ) $type =  '_' . $type ; // before || after
		if( ! empty($day) && is_integer($day) )  $day  =  '_' . $day . 'day' ; // day : int 		

		// Thực hiện ghi log
		$log_id = $this->log_m->insert(array(
						'log_title'			=> 'Gửi mail cảnh báo sắp hết hạn hợp đồng' ,
						'log_status'		=> 1,
						'term_id'			=> $term_id,
						'user_id'			=> $this->admin_m->id,
						'log_content'		=> 'Đã gửi mail cảnh báo thành công',
						'log_type'			=> 'warning_domain_deadline' . $type . $day,
						'log_time_create' 	=> date('Y-m-d H:i:s')
				  )); 

		if($log_id) return TRUE ;
	}


	/**
	 * Sends a mail before suspend.
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     bool   TRUE if success , otherwise return FALSE
	 */
	public function send_mail_before_suspend($term_id = 0)
	{
		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

		$data 			 = array();
		//$now    		 = $this->mdate->startOfDay();
		$now    		 = strtotime('2027/11/02');
		echo "<pre>now: - "  ; print_r(date('d/m/Y', $now)) ; echo "</pre>" ; 
		$data['time']    = $now;

		// SỐ NĂM ĐĂNG KÝ DOMAIN
		$domain_number_year_register 		 = get_term_meta_value($term_id, 'domain_number_year_register') ?: 1 ;
		$data['domain_number_year_register'] = $domain_number_year_register ;

		$domain_suspend_time = get_term_meta_value($term->term_id,'domain_suspend_time');
		if(empty($domain_suspend_time))
		{
			$contract_end = get_term_meta_value($term_id,'contract_end');
			echo "<pre>contract_end: - "  ; print_r(date('d/m/Y', $contract_end)) ; echo "</pre>" ; 
			$domain_suspend_time = $this->mdate->startOfDay(strtotime('+'.NUMDAYS_BEFORE_SUSPEND.' days',$now));
			update_term_meta($term_id,'domain_suspend_time', $domain_suspend_time);
		}

		// Đã quá hạn từ lúc cảnh báo off domain
		if($domain_suspend_time < $now) return FALSE; 
		
		echo "<pre>domain_suspend_time: - "  ; print_r(date('d/m/Y', $domain_suspend_time)) ; echo "</pre>" ;

		$data['domain_suspend_time'] = $domain_suspend_time;
		$data['contract_begin'] = get_term_meta_value($term_id,'domain_suspend_time');
		$data['contract_end'] = get_term_meta_value($term_id,'contract_end');

		$left_days = diffInDates($now,$domain_suspend_time);
		$data['number_of_extension_days'] = $left_days;

		$data['title']   = '[WEBDOCTOR] Cảnh báo đã hết hạn dịch vụ lưu trữ website';
		$data['email_source']            			    = $data['title'] ;
		$data['term']    = $term;
		$data['term_id'] = $term_id;

		// Hiển thị thông tin kinh doanh ra ngoài view
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	   = $sale;

		//prd($data) ;
		$data['content']	= $this->load->view('admin/send-mail/mail_before_suspend', $data, TRUE);
		$content 			= $this->load->view('email/blue_modern_tpl',$data,TRUE);
		$this->email->message($content);

		$this->set_mail_to($term_id,'customer');
		$this->email->subject($data['title']);
			
		

		$send_status   = $this->email->send();

		$this->log_m->insert(array(
				'log_title'			=> 'Email thông báo gia hạn dịch vụ' ,
				'log_status'		=> 1,
				'term_id'			=> $term_id,
				'user_id'			=> $this->admin_m->id,
				'log_content'		=> '',
				'log_type'			=> 'send_mail_before_suspend',
				'log_time_create' 	=> date('Y-m-d H:i:s')
			)); 

		return $send_status;
	}

	// -------------------------------------------------------
	// THÔNG BÁO GIA HẠN TRONG 30 NGÀY SAU KHI KẾT THÚC HĐ: KHÔNG DÙNG
	// -------------------------------------------------------
	public function send_mail_extend($term_id = 0)
	{
		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

		$data 			 = array();
		$time    		 = time();
		$time    		 = $this->mdate->startOfDay($time);
		$data['time']    = $time ;
  
   		$extension_day   						= strtotime('+30 day', get_term_meta_value($term_id,'contract_end'));
   		$extension_day   						= $this->mdate->startOfDay($extension_day) ;
   		$number_of_extension_days               = ($extension_day - $time)/(24*60*60) ;
   		if($number_of_extension_days == 0) $number_of_extension_days += 1 ; 
   		$data['number_of_extension_days']       =  $number_of_extension_days;

		$data['title']   = 'Thông báo gia hạn domain';
		$data['email_source']            			    = $data['title'] ;
		$data['term']    = $term;
		$data['term_id'] = $term_id;

		// Hiển thị thông tin kinh doanh ra ngoài view
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	   = $sale;

		$this->set_mail_to($term_id,'customer');
			 //->set_mail_to($term_id, 'admin');
		$this->email->subject($data['title']);

		$data['content']	= $this->load->view('admin/send-mail/extend_contract', $data, TRUE);
		$content 			= $this->load->view('email/blue_modern_tpl',$data,TRUE);
		$this->email->message($content);

		$send_status   = $this->email->send();
		
		if( FALSE === $send_status ) return FALSE ;
		
		// Thực hiện ghi log
		$log_id = $this->log_m->insert(array(
						'log_title'			=> 'Gửi mail gia hạn domain trong 7 ngày' ,
						'log_status'		=> 1,
						'term_id'			=> $term_id,
						'user_id'			=> $this->admin_m->id,
						'log_content'		=> 'Đã gửi mail thông báo gia hạn thành cônng',
						'log_type'			=> 'warning_domain_extend',
						'log_time_create' 	=> date('Y-m-d H:i:s')
		)); 

		if($log_id) return TRUE ;
	}

	// -------------------------------------------------------
	// GỬI MAIL CHO ADMIN
	// -------------------------------------------------------
	public function send_mail_info_to_admin($term_id)
	{
		$term = $this->term_m->get($term_id);
        if(empty($term)) return FALSE;

        $time = time();

		$contract_code 		= get_term_meta_value($term_id, 'contract_code');
		$title 				= 'Hợp đồng domain '.$contract_code.' đã được khởi tạo.';
		$data 				= array();
		$data['title'] 		= $title ;

		$data['term']		= $term;
		$data['term_id'] 	= $term_id;
		$data['time']       = $time ;

		// SỐ NĂM ĐĂNG KÝ DOMAIN
		$domain_number_year_register 		 = get_term_meta_value($term_id, 'domain_number_year_register') ?: 1 ;
		$data['domain_number_year_register'] = $domain_number_year_register ;
	
		// Hiển thị thông tin kinh doanh ra ngoài view
		$sale_id      	   = get_term_meta_value($term_id, 'staff_business');
		$sale 	 		   = $this->admin_m->set_get_active()->get($sale_id) ?: $this->admin_m->get(SALE_MANAGER_ID);
		$data['staff'] 	   = $sale;

		$this->set_mail_to($term_id,['mail_to'=>'thonh@webdoctor.vn','type_to'=>'admin']);
		$this->email->subject($title);
		$this->email->message($this->load->view('domain/admin/send-mail/send_mail_admin', $data, TRUE));

		$send_status 			  				= $this->email->send();

		if( FALSE === $send_status ) return FALSE ;

		// Thực hiện ghi log	
		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Thông báo khởi tạo hợp đồng',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id,
			'log_content'	=> 'Đã gửi mail thành công',
			'log_type'		=> 'domain-report-email',
			'log_time_create' => date('Y-m-d H:i:s'),
		));

		return TRUE ;
	}
}
/* End of file domain_report_m.php */
/* Location: ./application/modules/domain/models/domain_report_m.php */