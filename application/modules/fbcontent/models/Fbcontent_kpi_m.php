<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/googleads/models/Googleads_kpi_m.php');

class Fbcontent_kpi_m extends Googleads_kpi_m {

	public $_table = 'googleads_kpi';
	public $primary_key = 'kpi_id';
	public $before_create = array('delete_cache');
	public $after_update = array('delete_cache');
	public $before_delete = array('delete_cache');


	function __construct()
	{
		parent::__construct();
		$this->load->model('googleads/googleads_kpi_m');
	}

	/**
	 * Gets the kpis.
	 *
	 * @param      integer  $term_id   The term identifier
	 * @param      string   $type      The type
	 * @param      string   $kpi_type  The kpi type
	 * @param      integer  $time      The time
	 * @param      integer  $user_id   The user identifier
	 *
	 * @return     <type>   The kpis.
	 */
	function get_kpis($term_id = 0, $type = 'all', $kpi_type = 'account_type', $time = 0, $user_id = 0)
	{
		return parent::get_kpis($term_id, $type, $kpi_type, $time, $user_id);
	}
}

/* End of file Fbcontent_kpi_m.php */
/* Location: ./application/modules/onead/models/Fbcontent_kpi_m.php */