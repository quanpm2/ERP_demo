<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Contract_wizard_m.php');

class Fbcontent_wizard_m extends Contract_wizard_m {

	function __construct()
	{
		parent::__construct();

		$models = array('fbcontent/fbcontent_m');
		$this->load->model($models);
	}


	/**
	 * Add Hook when in model change service detail
	 *
	 * @param      Array  $post   The post
	 */
	public function wizard_act_service($post)
	{
		parent::wizard_act_service($post);

		// Cập nhật các thông số chi tiết của hợp đồng
		$this->hook->add_filter('ajax_act_service',	function($post){

			$metadata 	= $post['meta'] ?? [];
			$term_id 	= $post['edit']['term_id'] ?? 0;

			// Cập nhật thông số chi tiết dịch vụ
			$this->load->config('fbcontent/fbcontent');
			$service_package_config = $this->config->item('service', 'packages');
			empty($service_package_config[$metadata['service_package']]['items']) OR update_term_meta($term_id, 'service_package_items', serialize($service_package_config[$metadata['service_package']]['items']));

			update_term_meta($term_id, 'service_package', $metadata['service_package']);
			update_term_meta($term_id, 'quantity', $metadata['quantity']);
			update_term_meta($term_id, 'price', $metadata['price']);
			update_term_meta($term_id, 'discount_amount', $metadata['discount_amount']);

			// Cập nhật thời gian hợp đồng theo số năm đăng ký sử dụng dịch vụ
			$contract_begin = (int) get_term_meta_value($term_id, 'contract_begin');
			update_term_meta($term_id, 'contract_end', strtotime('+1 day', $contract_begin));

			try
			{
				/* Cập nhật giá trị hợp đồng */
				$contract 		= $this->fbcontent_m->set_contract($term_id);
				$contract_value = $this->fbcontent_m->calc_contract_value();
				update_term_meta($term_id, 'contract_value', $contract_value);
			}
			catch (Exception $e) { return $post; }
			
			return $post;

		}, 9);
	}
}
/* End of file Fbcontent_wizard_m.php */
/* Location: ./application/modules/fbcontent/models/Fbcontent_wizard_m.php */