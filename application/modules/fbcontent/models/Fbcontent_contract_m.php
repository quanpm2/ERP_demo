<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Base_report_m.php');

class Fbcontent_contract_m extends Base_contract_m {

	function __construct() 
	{
		parent::__construct();
		$this->load->model('fbcontent/fbcontent_m');
	}
	
	public function has_permission($term_id = 0, $name = '', $action = '',$kpi_type = '',$user_id = 0,$role_id = null)
	{
		$this->load->model('fbcontent/fbcontent_kpi_m');

		$permission = $name.'.'.$action;
		$permission = trim($permission, '.');
		
		if($this->permission_m->has_permission("{$name}.Manage",$role_id)
			&& $this->permission_m->has_permission($permission,$role_id)) 
			return TRUE;

		if(!$this->permission_m->has_permission($permission,$role_id)) return FALSE;

		$user_id = empty($user_id) ? $this->admin_m->id : $user_id;

		if($this->permission_m->has_permission('Fbcontent.sale.access',$role_id) 
			&& get_term_meta_value($term_id,'staff_business') == $user_id)
			return TRUE;

		$args = array(
			'term_id' => $term_id,
			'user_id' => $user_id);

		if(!empty($kpi_type))
		{
			$args['kpi_type'] = $kpi_type;
		}

		$kpis = $this->webgeneral_kpi_m->get_by($args);
		if(empty($kpis)) return FALSE;

		return TRUE;
	}

	/**
	 * Starts a service.
	 *
	 * @param      <type>   $term   The term
	 *
	 * @return     boolean  ( description_of_the_return_value )
	 */
	public function start_service($term = null)
	{
		restrict('Fbcontent.start_service');

		if( empty($term) || $term->term_type != 'fbcontent') return false;

		$this->load->model('fbcontent/fbcontent_report_m') ;
		$this->fbcontent_report_m->init($term);
		$this->fbcontent_report_m->send_init_service_email();

		/***
		 * Khởi tạo Meta thời gian dịch vụ để thuận tiện cho tính năng sắp xếp và truy vấn
		 */
		$start_service_time = get_term_meta_value($term->term_id, 'start_service_time');
		empty($start_service_time) AND update_term_meta($term->term_id, 'start_service_time', 0);
		
		$end_service_time = get_term_meta_value($term->term_id, 'end_service_time');
		empty($end_service_time) AND update_term_meta($term->term_id, 'end_service_time', 0);

		return TRUE;
	}


	/**
	 * Kỹ thuật phụ trách tiến hành kích hoạt thực hiện dịch vụ
	 *
	 * @param      <type>  $term   The term
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function proc_service($term = FALSE)
	{
		if(!$term || $term->term_type != 'fbcontent') return FALSE;
		$term_id = $term->term_id;

		$this->load->model('fbcontent/fbcontent_report_m') ;
		$this->fbcontent_report_m->init($term);
		$is_send = $this->fbcontent_report_m->send_activation_email();

		if(FALSE === $is_send) return FALSE ;

		update_term_meta($term_id, 'start_service_time' , time());

		$this->log_m->insert(array(
			'log_type' =>'start_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term->term_id,
			'log_content' => date('Y/m/d H:i:s', time()). ' - Start Service Time'
		 ));

		return TRUE;
	}

	/**
	 * Stops a service.
	 *
	 * @param      <type>  $term   The term
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function stop_service($term)
	{
		if(!$term || $term->term_type != 'fbcontent') return FALSE;
		$term_id = $term->term_id;

		$this->load->model('fbcontent/fbcontent_report_m') ;
		$this->fbcontent_report_m->init($term);
		$is_send = $this->fbcontent_report_m->send_finished_email();
		if( FALSE === $is_send) return FALSE ;

		update_term_meta($term_id,'end_service_time', time());
		$this->contract_m->update($term_id, array('term_status'=>'ending'));
		$this->log_m->insert(array(
			'log_type' =>'end_service_time',
			'user_id' => $this->admin_m->id,
			'term_id' => $term_id,
			'log_content' => date('Y/m/d H:i:s', time()) . ' - End Service Time'
			));
		return TRUE;
	}
}

/* End of file Fbcontent_contract_m.php */
/* Location: ./application/modules/fbcontent/models/Fbcontent_contract_m.php */