<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'models/Base_report_m.php');

class Fbcontent_report_m extends Base_report_m {

    function __construct()
    {
        $this->recipients['cc'][]   = 'baont@adsplus.vn';
        parent::__construct();

        $this->load->model('googleads/googleads_kpi_m');
    }

    //send mail cho kinh doanh, kỹ thuật
    public function send_init_service_email($type_to = 'admin')
	{
		if( ! $this->term) return FALSE;

		$this->autogen_msg          = FALSE;
        $this->recipients['cc'][]   = 'ketoan@adsplus.vn';

        $this->set_mail_to($type_to); /* Add người nhận mặc định theo hợp đồng vào email */
        $this->set_send_from('reporting'); /* Cấu hình email gửi tự động random */

        $contract_code = get_term_meta_value($this->term_id, 'contract_code');

        $default = array(
            'term'      			=> $this->term,
            'term_id'   			=> $this->term_id,

            'title'     => "Hợp đồng {$this->data['contract_code']} đã được khởi tạo.",
            'subject'   => "Hợp đồng {$this->data['contract_code']} đã được khởi tạo.",
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'time'		=> time(),
            'content_tpl' => 'fbcontent/report/init_service_email',
        );

        $this->data = wp_parse_args($this->data, $default);

        if( empty($this->data['content_tpl'])) return FALSE;

        $this->data['content'] = null;

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        $result = parent::send_email();
		if( FALSE === $result ) return FALSE ;

		// Thực hiện ghi log	
		$log_id = $this->log_m->insert(array(
			'log_title'		=> 'Thông báo khởi tạo hợp đồng',
			'log_status'	=> 1,
			'term_id'		=> $term_id,
			'user_id'		=> $this->admin_m->id,
			'log_content'	=> 'Đã gửi mail thành công',
			'log_type'		=> 'fbcontent-report-email',
			'log_time_create' => date('Y-m-d H:i:s'),
		));

		return TRUE ;
	}

	/**
     * Sends an activation email.
     *
     * @param      string  $type_to  The type to
     */
    public function send_activation_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;


        $this->autogen_msg          = FALSE;
        $this->recipients['cc'][]   = 'ketoan@adsplus.vn';

        $this->set_mail_to($type_to); /* Add người nhận mặc định theo hợp đồng vào email */
        $this->set_send_from('reporting'); /* Cấu hình email gửi tự động random */

        $default = array(
            'term'      => $this->term,
            'term_id'   => $this->term_id,
            'title'     => 'Thông tin kích hoạt dịch vụ',
            'subject'   => '[GURU.EDU.VN] Thông báo kích hoạt dịch vụ FBCONTENT cho HĐ'.$this->data['contract_code'],
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'content_tpl' => 'fbcontent/report/activation_email',
        );

        $this->data = wp_parse_args($this->data, $default);
        if( empty($this->data['content_tpl'])) return FALSE;

        $this->data['content'] = $this->render_content($this->data['content_tpl']);

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        $result = $this->send_email();
        $result AND $this->log_m->insert(array(
            'log_title'     => 'Gửi mail khi kích hoạt FBCONTENT bằng email',
            'log_status'    => 1,
            'term_id'       => $this->term_id,
            'user_id'       => $this->admin_m->id,
            'log_content'   => 'Đã gửi mail thành công',
            'log_type'      => 'fbcontent-report-email',
            'log_time_create' => date('Y-m-d H:i:s'),
        ));

        return $result;
    }


    /**
     * Sends a finished email.
     *
     * @param      string  $type_to  The type to
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function send_finished_email($type_to = 'customer')
    {
        if( ! $this->term) return FALSE;

        $this->autogen_msg          = FALSE;
        $this->recipients['cc'][]   = 'ketoan@adsplus.vn';

        $this->set_mail_to($type_to); /* Add người nhận mặc định theo hợp đồng vào email */
        $this->set_send_from('reporting'); /* Cấu hình email gửi tự động random */

        $default = array(
            'term'      => $this->term,
            'term_id'   => $this->term_id,
            'contract_date' => my_date($this->data['contract_begin'], 'd/m/Y').' - '.my_date($this->data['contract_end'], 'd/m/Y'),
            'title'     => 'Thông tin kết thúc dịch vụ',
            'subject'   => "[1AD.VN] Thông báo kết thúc dịch vụ HĐ {$this->data['contract_code']}",
            'customer'  => $this->customer ?? NULL,
            'workers'   => $this->workers,
            'content_tpl' => 'fbcontent/report/finished_email',
        );

        $this->data = wp_parse_args($this->data, $default);
        if( empty($this->data['content_tpl'])) return FALSE;

        $this->data['content'] = $this->render_content($this->data['content_tpl']);

        $this->email->subject($this->data['subject']);
        $this->email->message($this->data['content']);

        $result = $this->send_email();
        $result AND $this->log_m->insert(array(
            'log_title'     => 'Gửi mail kết thúc hợp đồng',
            'log_status'    => 1,
            'term_id'       => $this->term_id,
            'user_id'       => $this->admin_m->id,
            'log_content'   => 'Đã gửi mail thành công',
            'log_type'      => 'fbcontent-report-email',
            'log_time_create' => date('Y-m-d H:i:s'),
        ));

        return $result;
    }


	/**
	 * Init Service Generate data
	 *
	 * @param      integer  $term_id  The term identifier
	 *
	 * @return     self     ( description_of_the_return_value )
	 */
	public function init($term_id = 0)
    {
        parent::init($term_id);

        if( ! $this->term_id) return FALSE;

        $this->load->config('fbcontent/fbcontent');

        $service_package_config = $this->config->item('service', 'packages');
        $service_package 		= get_term_meta_value($this->term_id, 'service_package');
        $service_package_label 	= $service_package_config[$service_package]['label'] ?? 'null';

        $data = array(

            'contract_value' 		=> get_term_meta_value($this->term_id, 'contract_value'),
            'discount_amount'		=> get_term_meta_value($this->term_id, 'discount_amount'),
            'quantity'				=> get_term_meta_value($this->term_id, 'quantity'),
            'price' 				=> get_term_meta_value($this->term_id, 'price'),
            'vat' 					=> get_term_meta_value($this->term_id, 'vat'),

            'service_package' 		=> $service_package,
            'service_package_label' => $service_package_label,
            'service_package_items' => @unserialize(get_term_meta_value($this->term_id, 'service_package_items')),

            'contract_code' => get_term_meta_value($this->term_id, 'contract_code'),
            'contract_begin' => get_term_meta_value($this->term_id, 'contract_begin'),
            'contract_end' => get_term_meta_value($this->term_id, 'contract_end'),

            'start_service_time' => get_term_meta_value($this->term_id, 'start_service_time') ?: 0,
        );

        $this->data = wp_parse_args($data, $this->data);
        return $this;
    }

    /**
     * Loads workers.
     *
     * @return     self  ( description_of_the_return_value )
     */
    public function load_workers()
    {
        parent::load_workers();

        $kpis = $this->googleads_kpi_m->select('user_id')->where('term_id',$this->term_id)->group_by('user_id')->get_many_by();
        if( ! $kpis) return $this;

        $_workers       = array_map(function($x){ return $this->admin_m->get_field_by_id($x->user_id); }, $kpis);
        $this->workers  = wp_parse_args(array_column($_workers, NULL, 'user_email'), $this->workers);

        return $this;
    }

    /**
     * Render Send content;
     *
     * @param      <type>  $content_view  The content view
     * @param      string  $layout        The layout
     *
     * @return     Mixed  Result
     */
    public function render_content($content_view, $layout = 'report/template/email/1ad')
    {
        if( ! $content_view || ! $layout) return FALSE;

        $data = $this->data;
        if( ! is_array($data)) $data = array($data);

        $data['content'] = $this->load->view($content_view, $data, TRUE);

        $content = $this->load->view($layout, $data, TRUE);
        if( ! $content) return FALSE;
        return $content;
    }
}
/* End of file Fbcontent_report_m.php */
/* Location: ./application/modules/fbcontent/models/Fbcontent_report_m.php */