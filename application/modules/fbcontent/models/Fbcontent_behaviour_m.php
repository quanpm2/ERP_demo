<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. 'modules/contract/models/Behaviour_m.php');

class Fbcontent_behaviour_m extends Behaviour_m {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
	}

	/**
	 * Preview data for printable contract version
	 *
	 * @return     String  HTML content
	 */
	public function prepare_preview()
	{
		if( ! $this->contract) return FALSE;

		parent::prepare_preview();

		$this->config->load('fbcontent/fbcontent');

		$contract = $this->get_contract();

		$data = $this->data;
		$data['price'] 				= (int) get_term_meta_value($contract->term_id, 'price');
		$data['quantity'] 			= (int) get_term_meta_value($contract->term_id, 'quantity');
		$data['discount_amount'] 	= (int) get_term_meta_value($contract->term_id, 'discount_amount');
		$data['contract_value'] 	= (int) get_term_meta_value($contract->term_id, 'contract_value');
		
		$data['service_package'] 	 	= get_term_meta_value($contract->term_id, 'service_package');
		$service_package_config 		= $this->config->item('service', 'packages');
		$data['service_package_config'] = $service_package_config[$data['service_package']];

		$service_package_items			= get_term_meta_value($contract->term_id, 'service_package_items');
		$data['service_package_items'] 	= is_serialized($service_package_items) ? unserialize($service_package_items) : [];

		$data['data_represent']['Địa chỉ'] 		= $this->config->item('address', 'contract_represent');
		$data['data_represent']['Điện thoại'] 	= $this->config->item('phone', 'contract_represent');
		$data['data_represent']['Đại diện'] 	= 'Ông Nguyễn Anh Tuấn';
		$data['data_represent']['Chức vụ'] 		= 'Giám đốc Chi Nhánh Hồ Chí Minh';

		$data['content'] = $this->load->view('fbcontent/contract/preview', $data ,TRUE);
		return $data;
	}
	
	/**
	 * Calculates the contract value.
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     integer    The contract value.
	 */
	public function calc_contract_value()
	{
		parent::is_exist_contract(); // Determines if exist contract.
		$term_id = $this->get_contract()->term_id;

		$result = (((int) get_term_meta_value($term_id, 'price'))*((int) get_term_meta_value($term_id, 'quantity'))) - ((int) get_term_meta_value($term_id, 'discount_amount'));
		return $result;
	}

	public function start_service()
	{
		
		$this->contract_m->update($term->term_id,['term_status'=>'publish']);
		$this->messages->success('Trạng thái hợp đồng đã được chuyển sang "đang chạy".');

		$this->load->model('googleads/adwords_report_m');
		$this->adwords_report_m->init($term->term_id);
		$is_sent = $this->adwords_report_m->send_started_email();

		if($is_sent) $this->messages->success('Email thông báo bắt đầu thực hiện hợp đồng đã được gửi đến team Adsplus');
		else $this->messages->error('Email gửi không thành công.');

        return $is_sent;
	}
}
/* End of file Fbcontent_behaviour_m.php */
/* Location: ./application/modules/fbcontent/models/Fbcontent_behaviour_m.php */