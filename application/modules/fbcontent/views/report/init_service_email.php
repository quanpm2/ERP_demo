<?php
$start_time = get_term_meta_value($term->term_id,'contract_begin');
$end_time   = get_term_meta_value($term->term_id,'contract_end');


$is_expired = ($end_time < $time);
$bg_color = ($is_expired) ? '#dd4b39' : '#ff9f00';

?>

<?php $content_style = 'font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;';?>

<!-- thong tin hop dong -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td height="15"></td>
        </tr>
        <tr>
            <td align="center">
                <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <!--Title-->
                                            <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin hợp đồng</td>
                                            <!--End title-->
                                            <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--Space-->
                                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                    <tbody>
                                        <tr>
                                            <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left: 24px;">&nbsp;</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--End Space-->
                                <!--detail-->
                                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--end detail-->
                            </td>
                        </tr>
                        <!--start Article-->
                        <tr>
                            <td bgcolor="#ffffff">
                                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <?php
                                        $contract_date = my_date(get_term_meta_value($term->term_id,'contract_begin'),'d/m/Y').' - '.my_date(get_term_meta_value($term->term_id,'contract_end'),'d/m/Y');
                                        ?>
                                        <?php 
                                        $rows = array(
                                            [ 'Mã hợp đồng', get_term_meta_value($term->term_id,'contract_code') ],
                                            [ 'Thời gian hợp đồng', $contract_date ],
                                            [ 'Gói dịch vụ', $service_package_label ],
                                            [ 'Giá trị hợp đồng',  currency_numberformat($contract_value)],
                                        );

                                        if( ! empty($service_package_items))
                                        {
                                            foreach ($service_package_items as $item)
                                            {
                                                $rows[] = [ $item['duration'], $item['content'] ];
                                            }
                                        }
                                        ?>

                                        <?php foreach ($rows as $i => $row): ?>
                                        <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>    
                                        <tr>
                                            <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                            <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                            </td>
                                        </tr>
                                        <?php endforeach ?>
                                    <tr>
                                        <td height="25"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <!--end Article-->
                </tbody>
            </table>
        </td>
    </tr>
</tbody>
</table>
<!-- ong tin hop dong -->

<!-- thong tin khach hang -->
<table width="100%" align="center" bgcolor="#ecf0f1" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td height="15"></td>
        </tr>
        <tr>
            <td align="center">
                <table style="border-bottom:2px solid #e0e0e0;" class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="left" bgcolor="#f8f8f8" style="border-top:3px solid #3498db;">
                                <table class="table-full" style="mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <!--Title-->
                                            <td height="40" align="center" bgcolor="#3498db" style="font-family: Open sans, Arial, sans-serif; font-weight:bold; font-size:18px; color:#ffffff;padding-left: 15px;padding-right: 15px;">Thông tin khách hàng</td>
                                            <!--End title-->
                                            <td class="hide" align="left" bgcolor="#f8f8f8"><img src="http://webdoctor.vn/images/title_shape.png" width="25" height="40" alt="img"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--Space-->
                                <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                    <tbody>
                                        <tr>
                                            <td height="0" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left: 24px;">&nbsp;</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--End Space-->
                                <!--detail-->
                                <table class="table-full" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="40" align="center" style="font-family:Open sans,  Arial, sans-serif; font-size:14px;font-style: italic; color:#95a5a6;padding-left: 10px;padding-right: 10px;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--end detail-->
                            </td>
                        </tr>
                        <!--start Article-->
                        <tr>
                            <td bgcolor="#ffffff">
                                <table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <?php
                                        $contract_date = my_date(get_term_meta_value($term->term_id,'contract_begin'),'d/m/Y').' - '.my_date(get_term_meta_value($term->term_id,'contract_end'),'d/m/Y');
                                        ?>
                                        <?php 
                                        $rows = array();
                                        $rows[] = array('Tên khách hàng', 'Anh/Chị '.get_term_meta_value($term->term_id,'representative_name'));
                                        $rows[] = array('Email',get_term_meta_value($term->term_id, 'representative_email'));
                                        $rows[] = array('Điện thoại di động', get_term_meta_value($term->term_id, 'representative_phone'));
                                        $rows[] = array('Địa chỉ', get_term_meta_value($term->term_id, 'representative_address'));

                                        foreach($rows as $i=>$row):
                                            ?>
                                        <?php $bg = (($i%2 ==0) ? 'background: #f2f2ff;':''); ?>
                                        <tr>
                                            <td width="250" align="right" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#999999;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;" ><?php echo $row[0];?>: </td>
                                            <td width="300" align="left" style="font-family: open sans, arial, sans-serif; font-size:14px; color:#3498db;padding-top: 8px;padding-bottom: 8px;<?php echo $bg; ?>padding-left: 5px;padding-right: 5px;"><?php echo $row[1];?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                        <td height="25"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <!--end Article-->
                </tbody>
            </table>
        </td>
    </tr>
</tbody>
</table>
<!-- thong tin khach hang -->