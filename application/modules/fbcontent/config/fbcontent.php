<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['packages'] = [
    'service' => [
        'basic' => [
            'label' => 'Gói Tiết Kiệm',
            'name' => 'basic',
            'short_name' => 'Gói Tiết Kiệm',
            'price' => 2900000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'items' => [    
                [
                    'item' => 'Kế hoạch Content Fanpage',
                    'content' => 'Lên kế hoạch xây dựng nội dung fanpage: định hướng nội dung, phân loại các nhóm nội dung, thời gian lên bài.',
                    'duration' => '3 Ngày làm việc' 
                ],
                [
                    'item' => 'Setup 20 bài Content Fanpage',
                    'content' => 'Trung bình mỗi ngày 1 bài, dựa theo kế hoạch xây dựng nội dung đã chốt với Bên A',
                    'duration' => '1 Tháng' 
                ],
                [
                    'item' => 'Thiết kế ảnh bìa cho Fanpage',
                    'content' => 'Thiết kế tối đa ảnh 1 ảnh bìa cho fanpage hoặc trang giới thiệu.',
                    'duration' => '3 Ngày làm việc'
                ]
            ]
        ],

        'standard' => [
            'label' => 'Gói tiêu chuẩn',
            'name' => 'standard',
            'short_name' => 'Gói tiêu chuẩn',
            'price'   => 6350000,
            'discount_amount_default' => 0,
            'status' => 'active',
            'items' => [
                [
                    'item'      => 'Kế hoạch Content Fanpage',
                    'content'   => 'Lên kế hoạch xây dựng nội dung fanpage: định hướng nội dung, phân loại các nhóm nội dung, thời gian lên bài.',
                    'duration'  => '3 Ngày làm việc'
                ],
                [
                    'item'      => 'Setup 75 bài Content Fanpage',
                    'content'   => 'Trung bình mỗi ngày 1 bài, dựa theo kế hoạch xây dựng nội dung đã chốt với Bên A',
                    'duration'  => '3 Tháng'
                ],
                [
                    'item'      => 'Setup Chatbot lên Fanpage',
                    'content'   => 'Cài đặt nội dung để tự động trả lời tin nhắn, tự động trả lời bình luận của khách hàng.',
                    'duration'  => '3 Ngày làm việc'
                ],
                [
                    'item'      => 'Thiết kế ảnh bìa cho Fanpage',
                    'content'   => 'Thiết kế tối đa ảnh 3 ảnh bìa cho fanpage hoặc trang giới thiệu.',
                    'duration'  => '3 Ngày làm việc'
                ],
                [
                    'item'      => 'Tăng tương tác cho bài viết',
                    'content'   => 'Chia sẻ 10 nội dung lên các hội nhóm, profile,.. để thu hút khách hàng quan tâm.',
                    'duration'  => '3 Tháng'
                ]
            ]
        ],
    ],
    'default' =>'standard'
];