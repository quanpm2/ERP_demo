<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fbcontent extends Admin_Controller 
{
	private $term;
	private $term_type = 'fbcontent';
	public  $model = 'fbcontent_contract_m';

	public function __construct(){

		parent::__construct();
		
		$this->load->model(['customer/customer_m', 'staffs/sale_m', 'fbcontent/fbcontent_m', 'fbcontent/fbcontent_kpi_m']);
		$this->load->config('contract/contract');
		$this->load->config('staffs/group');
		$this->load->config('contract/invoice');

		// kiểm tra fbcontent có tồn tại và user có quyền truy cập ?
		$this->init_website();
	}

	private function init_website()
	{
		$term_id = $this->uri->segment(4);
		$method  = $this->uri->segment(3);
		$is_allowed_method = (!in_array($method, array('index','done')));
		if( ! $is_allowed_method || empty($term_id)) return FALSE;
		
		$term = $this->fbcontent_m->set_term_type()->where('term_status', 'publish')->get($term_id);
		if(empty($term) OR !$this->is_assigned($term_id)) 
		{
			$this->messages->error('Truy cập #'.$term_id.' không hợp lệ !!!');
			redirect(module_url(),'refresh');
		}

		$this->template->title->set(strtoupper(' '.$term->term_name));
		$this->website_id = $this->data['term_id'] = $term_id;
		$this->data['term'] = $this->term = $term;
	}

	public function index()
	{
		restrict('Fbcontent.index.access');

		$this->template->is_box_open->set(1);
		$this->template->title->set('Tổng quan dịch vụ');

		parent::render($this->data);
	}


	/**
	 * { function_description }
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function overview($term_id = 0)
	{
		restrict('Fbcontent.Overview.Access');
		if(empty($this->fbcontent_kpi_m->get_kpis($term_id, 'users')) 
			&& (has_permission('Fbcontent.kpi.access') || has_permission('Fbcontent.kpi.update') || has_permission('Fbcontent.kpi.add'))) redirect(module_url("kpi/{$term_id}"), 'refresh');

		redirect(module_url("setting/{$term_id}"), 'refresh');
	}

	/**
	 * KPI HANDLER FORM SUBMIT
	 *
	 * @param      integer  $term_id    The term identifier
	 * @param      integer  $delete_id  The delete identifier
	 */
	public function kpi($term_id = 0, $delete_id = 0)
	{
		if($delete_id > 0)
		{
			restrict('Fbcontent.kpi.delete');
			$check = $this->fbcontent_kpi_m->get($delete_id);
			$is_deleted = FALSE;
			if($check)
			{
				if(strtotime($check->kpi_datetime) >= $this->mdate->startOfMonth())
				{
					$this->fbcontent_kpi_m->delete_cache($delete_id);
					$this->fbcontent_kpi_m->delete($delete_id);
					$is_deleted = TRUE;
					$this->messages->success('Xóa thành công');
				}
			}
			($is_deleted) OR $this->messages->error('Xóa không thành công');
			redirect(module_url("setting/{$term_id}"), 'refresh');
		}

		$post 	= $this->input->post();
		if(empty($post)) redirect(module_url("setting/{$term_id}"), 'refresh');
		restrict('Fbcontent.kpi.add');
		$kpi_datetime = $this->input->post('target_date');
		if( ! $kpi_datetime >= $this->mdate->startOfMonth())
		{
			$this->messages->error('Cập nhật không thành công do tháng cập nhật nhỏ hơn tháng hiện tại');
			redirect(module_url("setting/{$term_id}"), 'refresh');
		}

		$this->fbcontent_kpi_m->update_kpi_value($term_id, $this->input->post('target_type'), (int)$this->input->post('target_post'), $kpi_datetime, $this->input->post('user_id'));
		$this->fbcontent_kpi_m->delete_cache($delete_id);
		$this->messages->success('Cập nhật thành công');

		redirect(module_url("setting/{$term_id}"), 'refresh');
	}

	/**
	 * 1AD's Setting Page
	 *
	 * @param      integer  $term_id  The term identifier
	 */
	public function setting($term_id = 0)
	{
		restrict('Fbcontent.setting');

		$this->template->title->prepend('Cấu hình');
		$this->setting_submit($term_id);
		
		$data = $this->data;
		$data['term'] 		= $this->term;
		$data['term_id'] 	= $term_id;
		$data['kpi_type'] = 'account_type';
		$data['term_id']  = $term_id;
		$data['time'] 	  = start_of_month();
		
		$time_start = get_term_meta_value($term_id, 'contract_begin');
		$time_end 	= get_term_meta_value($term_id, 'contract_end');
		$time_start = strtotime('-1 month');
		$time_end 	= strtotime('+2 month', $time_end);

		$data['time_start'] = $time_start;
		$data['time_end'] = $time_end;
		$data['time_start'] = $this->mdate->startOfDay($data['time_start']);
		$data['time_end'] = $this->mdate->endOfDay($data['time_end']);

		$data['targets'] = [ 'account_type' => [] ];

		$targets = $this->fbcontent_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(['term_id' => $term_id]);
		foreach($this->fbcontent_kpi_m->as_array()->order_by('kpi_datetime')->get_many_by(['term_id' => $term_id]) as $target)
		{
			$data['targets'][$target['kpi_type']][] = $target;
		}

		$data['users'] = [];
		$role_ids = $this->option_m->get_value('group_1ad_ids', TRUE)
		AND $data['users'] = $this->admin_m->select('user_id, display_name, user_email')->where_in('role_id', $role_ids)->set_get_active()->order_by('display_name')->as_array()->get_all();

		parent::render($data);
	}

	protected function setting_submit($term_id = 0)
	{
		$post = $this->input->post();
		if(empty($post)) return FALSE;
		
		if( ! empty($post['start_process']) )
		{
			if( ! $this->fbcontent_m->has_permission($term_id, 'Fbcontent.start_service') )
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"), 'refresh');
			}

			$this->load->model('fbcontent/fbcontent_contract_m');
			$this->fbcontent_contract_m->proc_service($this->term) ? $this->messages->success('Cập nhật thành công') : $this->messages->error('Có lỗi xảy ra');
			
			/* Phân tích hợp đồng ký mới | tái ký */
			$this->load->model('contract/base_contract_m');
			$this->base_contract_m->detect_first_contract($term_id);


			/* Gửi SMS thông báo kích hoạt hợp đồng đến khách hàng */
			$this->load->model('contract/contract_report_m');
			$this->contract_report_m->send_sms_activation_2customer($term_id);
			
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
		else if( ! empty($post['end_process']))
		{
			if( ! $this->fbcontent_m->has_permission($term_id, 'Fbcontent.stop_service') )
			{	
				$this->messages->error('Không có quyền thực hiện tác vụ này.');
				redirect(module_url("setting/{$term_id}"),'refresh');
			}

			$this->load->model('fbcontent/fbcontent_contract_m');
			$this->fbcontent_contract_m->stop_service($this->term) ? $this->messages->success('Cập nhật thành công') : $this->messages->error('Có lỗi xảy ra');

			if(TRUE === $stat) $this->messages->success('Trạng thái hợp đồng đã được chuyển sang "Đã kết thúc".');
			redirect(module_url("setting/{$term_id}"),'refresh');
		}
	}

	protected function is_assigned($term_id = 0,$kpi_type = '')
	{
		// check user has manager role permission
		$class = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		$result = $this->fbcontent_m->has_permission($term_id,"{$class}.{$method}",'access');
		return $result;
	}
}
/* End of file Fbcontent.php */
/* Location: ./application/modules/fbcontent/controllers/Fbcontent.php */