<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;
use GuzzleHttp\Client as Guzzle;

class Test extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function send_init_service_email($id)
	{
		$this->load->model('fbcontent/fbcontent_report_m');
		$this->load->model('fbcontent/fbcontent_m');

		$contract = $this->fbcontent_m->set_term_type()->get($id);

		$this->fbcontent_report_m->init($contract);
		$this->fbcontent_report_m->send_init_service_email();
		dd($this->fbcontent_report_m);
	}

	public function send_contract_verified_email($id)
	{
		$this->load->model('fbcontent/fbcontent_m');
		$term = $this->fbcontent_m->get($id);
		
		$this->load->model('contract/common_report_m');
		$this->common_report_m->init($term);
		$this->common_report_m->send_contract_verified_email();
	}
}
/* End of file Test.php */
/* Location: ./application/modules/fbcontent/controllers/Test.php */