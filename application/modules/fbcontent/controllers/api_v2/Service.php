<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'vendor/autoload.php';

class Service extends MREST_Controller
{
    function __construct()
    {
        $this->autoload['models'][] = 'contract/contract_m';
        $this->autoload['models'][] = 'fbcontent/fbcontent_m';
        $this->autoload['libraries'][] = 'form_validation';

        parent::__construct('fbcontent/rest');

        $this->load->config('fbcontent/fbcontent');
    }

    /**
     * API dữ liệu các đợt thanh toán
     */
    public function index_put()
    {
        if( ! $this->contract_m->has_permission(parent::get('id', NULL), 'admin.contract.edit')) parent::response(['status' => false, 'code' => 401, 'message' => '401 Unauthorized']);

        $id     = parent::get('id', NULL);
        $_data  = array(
            'id'                => $id,
            'price'             => parent::put('price', NULL),
            'quantity'          => parent::put('quantity', NULL),
            'discount_amount'   => parent::put('discount_amount', NULL),
            'service_package'   => parent::put('service_package', NULL),
            'additionServices'  => parent::put('additionServices', NULL),
        );

        $this->form_validation->set_data($_data);

        $service_packages = $this->config->item('service', 'packages');

        $configRules = array(
            [
                'field' => 'id',
                'label' => 'Contract Id',
                'rules' => 'required|integer'
            ],
            [
                'field' => 'service_package',
                'label' => 'Service Package',
                'rules' => 'required|in_list['.implode(',', array_column($service_packages, 'name')).']'
            ],
            [
                'field' => 'quantity',
                'label' => 'quantity',
                'rules' => 'required|integer|greater_than_equal_to[1]'
            ],
            [
                'field' => 'price',
                'label' => 'price',
                'rules' => 'required|integer|greater_than_equal_to[1]'
            ],
            [
                'field' => 'discount_amount',
                'label' => 'discount_amount',
                'rules' => 'integer'
            ]
        );

        $this->form_validation->set_rules($configRules);
        if(FALSE == $this->form_validation->run()) parent::response(['status' => false, 'code' => parent::HTTP_BAD_REQUEST, 'message' => $this->form_validation->error_array()]);

        /* Update Service package  */
        if($_data['service_package'] && $_data['service_package'] != get_term_meta_value($id, 'service_package'))
        {
            empty($service_packages[$_data['service_package']]['items']) OR update_term_meta($id, 'service_package_items', serialize($service_packages[$_data['service_package']]['items']));
            update_term_meta($id, 'service_package', $_data['service_package']);
        }

        $_data['price'] == get_term_meta_value($id, 'price')                        OR update_term_meta($id, 'price', (double) $_data['price']);
        $_data['quantity'] == get_term_meta_value($id, 'quantity')                  OR update_term_meta($id, 'quantity', (double) $_data['quantity']);
        $_data['discount_amount'] == get_term_meta_value($id, 'discount_amount')    OR update_term_meta($id, 'discount_amount', (double) $_data['discount_amount']);

        if( ! empty($_data['additionServices']))
        {
            update_term_meta($id, 'additionServices', serialize($_data['additionServices']));
            update_term_meta($id, 'additionServicesPrice', array_sum(array_column($_data['additionServices'], 'value')));
        }

        try
        {
            $contract           = $this->fbcontent_m->set_contract($id);
            $_contract_value    = $this->fbcontent_m->calc_contract_value();

            if($_contract_value == get_term_meta_value($id, 'contract_value')) parent::response(['status' => true, 'code' => parent::HTTP_OK ]);

            update_term_meta($id, 'contract_value', (double) $_contract_value);
            
            /* Delete all invoices available */            
            $this->fbcontent_m->delete_all_invoices();
            /* Create new contract's invoices */            
            $this->fbcontent_m->create_invoices();
            /* Update all invoices's amount & receipt's amount */
            $this->fbcontent_m->sync_all_amount();

            parent::response(['status' => true, 'code' => parent::HTTP_OK, 'message' => 'Quá trình xử lý hoàn tất']);
        }
        catch (Exception $e)
        {
           parent::response([ 'status' => false, 'code' => parent::HTTP_BAD_REQUEST, 'message' => $e->getMessage() ]);
        }

        parent::response([ 'status' => true, 'code' => parent::HTTP_OK, 'message' => 'Quá trình xử lý hoàn tất']);
    }
}
/* End of file Index.php */
/* Location: ./application/modules/googleads/controllers/api_v2/Index.php */