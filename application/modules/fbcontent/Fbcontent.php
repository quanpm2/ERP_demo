<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fbcontent_Package extends Package
{
	public function name()
	{
		return 'Dịch vụ Content FB';
	}

	public function init()
	{
		$this->_load_menu();
	}

	/**
	 * Init menu LEFT | NAV ITEM FOR MODULE
	 * then check permission before render UI
	 */
	private function _load_menu()
	{
		$order = 1;
		$itemId = 'admin-fb-content';
 
 		if(!is_module_active('fbcontent')) return FALSE;	
 		
		if(has_permission('Fbcontent.Index.access'))
		{
			$this->menu->add_item(array(
				'id'        => $itemId,
				'name' 	    => 'FB-Content',
				'parent'    => null,
				'slug'      => admin_url('fbcontent'),
				'order'     => $order++,
				'icon' 		=> 'fa fa-pencil',
				'is_active' => is_module('fbcontent')
			),'left-service');	
		}

		if( ! is_module('fbcontent')) return FALSE;

		if(has_permission('Fbcontent.Index.access'))
		{
			$this->menu->add_item(array(
				'id' => 'fbcontent-service',
				'name' => 'Dịch vụ',
				'parent' => $itemId,
				'slug' => admin_url('fbcontent'),
				'order' => $order++,
				'icon' => 'fa fa-fw fa-xs fa-circle-o'
			), 'left-service');
		}
		
		$left_navs = array(
			'overview'=> array(
				'name' => 'Tổng quan',
				'icon' => 'fa fa-fw fa-tachometer',
			),
			'setting'  => array(
				'name' => 'Cấu hình',
				'icon' => 'fa fa-fw fa-cogs',
			)
		);

		$term_id = $this->uri->segment(4);

		$this->website_id = $term_id;

		if(empty($term_id) || !is_numeric($term_id)) return FALSE;

		foreach ($left_navs as $method => $name) 
		{
			if(!has_permission("Fbcontent.{$method}.access")) continue;

			$icon = $name;
			if(is_array($name))
			{
				$icon = $name['icon'];
				$name = $name['name'];
			}

			$this->menu->add_item(array(
				'id' => "fbcontent-{$method}",
				'name' => $name,
				'parent' => $itemId,
				'slug' => module_url("{$method}/{$term_id}"),
				'order' => $order++,
				'icon' => $icon
			), 'left-service');
		}
	}

	private function _update_permissions()
	{
		$permissions = array();
		if( ! $permissions) return TRUE;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function title()
	{
		return 'Dịch vụ Content FB';
	}

	public function author()
	{
		return 'thonh';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'Dịch vụ Content FB';
	}
	
	/**
	 * Init default permissions for 1AD module
	 *
	 * @return     array  permissions default array
	 */
	private function init_permissions()
	{
		return array(

			'Fbcontent.index' => array(
				'description' => 'Quản lý HĐ 1AD.VN',
				'actions' => ['manage','access','update','add','delete']),

			'Fbcontent.overview' => array(
				'description' => 'Tổng quan',
				'actions' => ['manage','access','add','delete','update']),

			'Fbcontent.start_service' => array(
				'description' => 'Kích hoạt dịch vụ',
				'actions' => ['manage','access','update']),
			
			'Fbcontent.stop_service' => array(
				'description' => 'kết thúc dịch vụ',
				'actions' => ['manage','access','update']),
			
			'Fbcontent.setting' => array(
				'description' => 'Cấu hình',
				'actions' => ['manage','access','add','delete','update']
			),
		);
	}

	/**
	 * Install module
	 *
	 * @return     bool  status of command
	 */
	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$permission_id = $this->permission_m->add($name, $actions, $description);
			$this->role_permission_m->insert(['role_id' => 1, 'permission_id' => $permission_id, 'action' => serialize($actions)]);
		}

		return TRUE;
	}

	/**
	 * Uninstall module command
	 *
	 * @return     bool  status of command
	 */
	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions) return FALSE;

		if($pers = $this->permission_m->like('name','Fbcontent')->get_many_by())
		{
			foreach($pers as $per)
			{
				$this->permission_m->delete_by_name($per->name);
			}
		}

		foreach($permissions as $name => $value)
		{
			$this->permission_m->delete_by_name($name);
		}

		return TRUE;
	}
}
/* End of file Fbcontent.php */
/* Location: ./application/modules/fbcontent/models/Fbcontent.php */