<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Public_Controller {
	private $week_range = array();
	function __construct()
	{
		parent::__construct();
		$this->load->model('seotraffic_m');
		$this->load->model('seotraffic_report_m');
		$this->week_range = $this->seotraffic_m->week_range;
	}

	public function _remap($method, $seg = '')
	{
		if($method == 'send')
			return $this->send();
		// if($method == 'send_sms')
			// return $this->send_sms();

		return $this->index($method);
	}


	public function send()
	{
		$this->load->model('seotraffic_mail_m');
		$terms = $this->term_m->where('term_status',1)->get_many_by('term_type', 'seo-traffic');
		foreach($terms as $term)
		{
			echo $this->seotraffic_mail_m->send_all_reports($term->term_id);
		}
	}

	
	public function index($site_name = '')
	{
		$this->load->library('mdate');
		$this->load->model('term_m');
		$this->load->model('termmeta_m');
		$this->load->model('seotraffic_m');
		$this->load->model('seotraffic_kpi_m');
		$site_name = strtolower($site_name);
		$week = $this->input->get('w');
		$p = $this->input->get('p');

		$week = explode('-', $week);
		$data = array();
		$data['ga_label'] = $this->seotraffic_report_m->ga_label;

		if(!$p)
		{
			die('URL không tồn tại');
		}
		$post = $this->post_m->get_by(array(
			'post_type' =>'seotraffic-email',
			'post_id' =>$p
			));
		if(!$post)
		{
			die('URL không tồn tại');
		}


		$week_range = $this->seotraffic_m->init_date_range($post->start_date);
		$week = $this->seotraffic_m->get_dayOfWeekName(date('d', $post->start_date));
		$start_date = $week_range[$week][0];
		$end_date =$week_range[$week][1];

		$term = $this->term_m->get_by(array('term_name'=>$site_name, 'term_type' => $this->seotraffic_m->term_type));
		$code = rawurldecode($this->input->get('domain'));
		$code = rawurldecode($code);
		$decrypted_termID = $this->seotraffic_m->encrypt($code,$term,'decode');

		if($p != '1077')
			if(!$decrypted_termID || !$term ||  $decrypted_termID !== $term->term_id)
			{
				die('Lỗi');
			}

			$id = $term->term_id;
			
			$ga_profile_id = $this->termmeta_m->get_meta($id, 'ga_profile_id', TRUE, TRUE);
			$data['seotraffic_type'] = $this->termmeta_m->get_meta($id, 'seotraffic_type', TRUE, TRUE);

			$data['ga'] = $this->get_ga($start_date, $end_date,$ga_profile_id);

			$start_date = $this->mdate->startOfDay($start_date);
			$end_date = $this->mdate->endOfDay($end_date);
			$dayEndOfMonth = date("Y-m-t", $end_date);
			$dayEndOfMonth = $this->mdate->endOfDay($dayEndOfMonth);

			$data['ga_total'] = $this->get_ga(date('Y-m-01',$start_date), date('Y-m-d',$dayEndOfMonth),$ga_profile_id);
			$data['ga_total']['total']['total'] = 0;

			foreach($data['ga_total']['total'] as $k =>$ga_total)
			{
				$total = $data['ga_total']['total'];
				foreach($data['ga_label'] as $ga_label_key => $ga_label_name)
				{
					$data['ga_total']['total'][$ga_label_key] = (isset($data['ga_total']['total'][$ga_label_key])) ? $data['ga_total']['total'][$ga_label_key] : 0;
				}
			}

			$data['numDateOfMonth'] = date('d',$dayEndOfMonth) - date('d',$end_date);
			$data['kpi'] = @$this->seotraffic_kpi_m->select('kpi_value')->get_by(array('term_id'=>$id, 'kpi_datetime' => date('Y-m-01',$start_date)));
			$data['kpi'] = (isset($data['kpi']->kpi_value)) ? $data['kpi']->kpi_value: 0;
			$ga_weeks = $this->get_ga_week($ga_profile_id, $end_date);

			$data['ga_weeks'] = array();
			$data['ga_weeks']['total'] = array();

			foreach($ga_weeks['ga'] as $week_name =>$ga_week)
			{
				$total = $ga_week['total'];
				foreach($data['ga_label'] as $ga_label_key => $ga_label_name)
				{
					$val = (isset($total[$ga_label_key])) ? $total[$ga_label_key]: 0;
					$data['ga_weeks'][$week_name][$ga_label_key] = $val;

					if(!isset($data['ga_weeks']['total'][$ga_label_key]))
						$data['ga_weeks']['total'][$ga_label_key] = 0;
					$data['ga_weeks']['total'][$ga_label_key]+= $val;
				}
			}

			$data['post'] = $post;
			$data['start_date'] = $start_date;
			$data['end_date'] = $end_date;
			$data['ga_profile_id'] = $ga_profile_id;
			$data['dayEndOfMonth'] = $dayEndOfMonth;

			$data['site_name'] = strtoupper($term->term_name);
			$this->load->view('report/index', $data);
		}

		private function get_ga_week($ga_profile_id, $date = 0)
		{
			if($date == 0)
				$date = time();
			$data = array();
			$month = date('m', $date);
			$data['ga'] = array();
			$date = $this->mdate->startOfMonth($date);
			$week_range = $this->seotraffic_m->init_date_range($date);
			foreach($week_range as $week_name => $date)
			{
				$data['ga'][$week_name] = $this->get_ga($date[0], $date[1], $ga_profile_id);
			}
			return $data;
		}

		private function get_ga($start_date, $end_date, $ga_profile_id)
		{
			$this->load->model('seotraffic_report_m');
			return $this->seotraffic_report_m->get_ga($start_date, $end_date, $ga_profile_id);
		}
	}

	/* End of file Report.php */
/* Location: ./application/modules/seotraffic/controllers/Report.php */