<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crond extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('sms');
		$this->load->model('seotraffic_log_m');
		$this->load->model('seotraffic_mail_m');
		$this->load->model('seotraffic_kpi_m');
		$this->load->model('seotraffic_report_m');
	}
	public function index()
	{
		$this->create_send_sms();
		$this->send_sms();
	}

	public function send_sms()
	{

		$logs = $this->seotraffic_log_m->get_many_by(array(
			'log_status' => -1
			));
		if($logs)
		{
			foreach($logs as $log)
			{

				$phone = $this->termmeta_m->get_meta_value($log->term_id, 'phone_report');
				$results = $this->sms->send($phone, $log->log_title);

				if(!$phone)
					continue;

				foreach($results as $i=>$result)
				{
					$arr = array(
						'log_title'=>$result['message'],
						'log_status'=>$result['response']->SendSMSResult,
						'user_id'=> 0,
						'term_id'=> $log->term_id,
						'log_type'=> 'seotraffic-report-sms',
						'log_time_create'=> date('Y-m-d H:i:s'),
						);

					$log_id = $log->log_id;
					if($i==0)
					{
						$this->seotraffic_log_m->update($log_id, $arr);
					}
					else
					{
						$log_id = $this->seotraffic_log_m->insert($arr);
					}

					if($result['response']->SendSMSResult == 0)
					{
						$day = date('d');
						$week = $this->seotraffic_m->get_dayOfWeekName($day);
						$date_range = $this->seotraffic_m->get_date_range($week, time());
						$report_next_time = strtotime($date_range[1]);
						$report_next_time = $this->mdate->endOfDay($report_next_time) + 1;
						$this->termmeta_m->update_meta($log->term_id, 'sms-report-nextsend', $report_next_time);
					}
					$this->seotraffic_log_m->meta->update_meta($log_id, 'sms_recipient', $result['recipient']);
					$this->seotraffic_log_m->meta->update_meta($log_id, 'sms_service_id', $result['config']['service_id']);
					echo 'sended '.$log_id.'<br>';
				}
			}
		}
	}
	public function create_send_sms()
	{

		$terms = $this->term_m->where('term_status',1)->get_many_by('term_type', 'seo-traffic');

		$time = time();
		$day = date('d',$time) - 1;
		if($day == 0)
		{
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}

		$time = strtotime(date('Y-m-'.$day,$time));

		$dateEnfOfMonth = date('d',$this->mdate->endOfMonth($time));

		$logs = $this->seotraffic_log_m->get_many_by(array(
			'log_status' => -1
			));

		$term_wating_send = array();
		if($logs)
		{
			foreach($logs as $log)
			{
				$term_wating_send[] = $log->term_id;
			}
		}

		foreach($terms as $term)
		{
			$id = $term->term_id;
			$phone = $this->termmeta_m->get_meta_value($id, 'phone_report');
			if($phone)
			{

				//Nếu id đã tồn tại trong hàng đợi thì bỏ qua
				if(in_array($id, $term_wating_send))
					continue;

				$report_next_time = $this->termmeta_m->get_meta_value($id, 'sms-report-nextsend');
				if($report_next_time > time())
					continue;
				$start_date = $this->mdate->startOfMonth($time);
				$end_date = $time;
				$type = $this->termmeta_m->get_meta_value($id, 'seotraffic_type');

				$ga_profile_id = $this->termmeta_m->get_meta_value($id, 'ga_profile_id');
				$ga_data = $this->seotraffic_m->get_ga(date('Y-m-d',$start_date),date('Y-m-d',$end_date),$ga_profile_id);
				$ga_data_result = 0;
				if(isset($ga_data['totalsForAllResults'][$type]))
				{
					$ga_data_result = $ga_data['totalsForAllResults'][$type];
				}

				if($ga_data_result == 0)
					continue;

				$kpi = $this->seotraffic_kpi_m->get_kpi_value($id, $time);
				$percent_predict =  $this->seotraffic_m->predict_traffic($ga_data_result, $day, $dateEnfOfMonth, $kpi);

				$type = $this->seotraffic_report_m->get_ga_label($type);
				
				$msg = 'BC Tuan {week}/{month} {website}: '.$type.' '.$ga_data_result.', du kien dat '.$percent_predict.'% | chi tiet lh: 0972 899 723 (Mr. Hai)';

				$title = $this->seotraffic_mail_m->parse_email_template($msg, array(
					'start_date' => $end_date,
					'end_date' => $end_date,
					), $id, $term);

				$log_id = $this->seotraffic_log_m->insert(array(
					'log_title'=>$title,
					'log_status'=> -1,
					'term_id'=>$id,
					'user_id'=> 0,
					'log_type'=> 'seotraffic-report-sms',
					'log_time_create'=> date('Y-m-d H:i:s'),
					));

				echo $log_id.'<br>';
			}
		}
	}
}

/* End of file crond.php */
/* Location: ./application/controllers/crond.php */