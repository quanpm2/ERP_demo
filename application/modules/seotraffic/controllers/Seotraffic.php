<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seotraffic extends Admin_Controller {

	public $model = 'seotraffic_m';

	public $term_type = 'seo-traffic';

	public $website_id = 0;
	
	public $term = FALSE;

	function __construct()
	{
		parent::__construct();
		$this->load->model('seotraffic_m');
		$this->load->model('seotraffic_kpi_m');
		$this->load->model('seotraffic_report_m');
		$this->load->model('seotraffic_log_m');
		$this->load->add_package_path(APPPATH.'third_party/google-analytics/');
		$this->init_website();
		// $this->output->enable_profiler(TRUE);
	}

	private function init_website()
	{
		$this->website_id  = $this->uri->segment(4);
		$method  = $this->uri->segment(3);
		if(!in_array($method, array('index','','ajax')))
		{
			$this->term = $this->term_m->get($this->website_id);
			if($this->term)
			{
				$this->template->title->set(strtoupper($this->term->term_name));
			}
		}
	}

	public function index()
	{
		$data = array();
		$this->template->title->append('Chọn website');
		$data['lists'] = $this->term_m->where_in('term_status',array('publish', '1'))->get_many_by(array(
			'term_type' => $this->term_type,
			));
		// $this->template->content->cache(60, 'seotraffic-index');
		parent::render($data);
	}

	function overview($id = 0)
	{
		$this->load->model('seotraffic_report_m');
		$this->template->javascript->add('plugins/chartjs/Chart.js');
		$data = array();
		
		$time = time();
		$day = date('d',$time) - 1;
		if($day == 0)
		{
			$time = strtotime('yesterday',$time);
			$day = date('d',$time);
		}
		$start_date = date('Y-m-01',$time);

		$day = str_pad($day, 2, "0", STR_PAD_LEFT);
		$end_date = date('Y-m-'.$day, $time);
		$dayEndOfMonth = date("Y-m-t", $time);
		$dayEndOfMonth = $this->mdate->endOfDay($dayEndOfMonth);
		$ga_profile_id = $this->termmeta_m->get_meta($id, 'ga_profile_id', TRUE, TRUE);
		$kpi = $this->seotraffic_kpi_m->get_kpi_value($id, $time);

		$kpi = $kpi / date('d',$dayEndOfMonth);
		$kpi = numberformat($kpi);
		$ga_data = $this->seotraffic_report_m->get_ga($start_date, $end_date,$ga_profile_id);
		if(!$ga_data)
		{
			parent::render($data,'blank');
			return true;
		}
		$charData = array();
		$charData['ga:sessions'] = array();
		$charData['Organic Search'] = array();
		$charData['kpi'] = array();

		foreach($ga_data['headers'] as $header) {
			$charData['ga:sessions'][] = (isset($ga_data['results'][$header]['ga:sessions']) ? $ga_data['results'][$header]['ga:sessions'] : 0);

			$charData['Organic Search'][] = (isset($ga_data['results'][$header]['Organic Search']) ? $ga_data['results'][$header]['Organic Search'] : 0);
			$charData['kpi'][] = $kpi;

		}
		$charData['ga:sessions'] = implode(',', $charData['ga:sessions']);
		$charData['Organic Search'] = implode(',', $charData['Organic Search']);
		$charData['kpi'] = implode(',', $charData['kpi']);
		
		$charDataHeader = array();
		foreach($ga_data['headers'] as $header)
		{
			$charDataHeader[] = date('d/m', strtotime($header));
		}

		$data['charDataHeader'] = implode('","', $charDataHeader);
		$data['charDataHeader'] = '"'.$data['charDataHeader'].'"';

		$data['charData'] = $charData;
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		$this->template->title->prepend('Tổng quan ');
		$this->template->title->append(' tháng '.date('m/Y',$time));
		parent::render($data);
	}
	
	public function kpi($edit_id = 0,$delete_id= 0)
	{
		$this->submit_kpi($edit_id,$delete_id);

		$data = array();
		$kpis = $this->seotraffic_kpi_m->get_kpi_value($edit_id);
		$data['kpis'] = array();
		if($kpis)
		{
			foreach($kpis as $date => $kpi)
			{
				$data['kpis'][$date] = $kpi;
			}
		}

		$data['edit_id'] = $edit_id;
		$time_start = strtotime('2015-08-15');
		$time_end = strtotime('2016-08-15');
		$data['time_start'] = $this->mdate->startOfDay($time_start);
		$data['time_end'] = $this->mdate->endOfDay($time_end);
		$data['time'] = $this->mdate->startOfMonth(time());
		$this->template->title->prepend('KPI ');
		$data['date_contract'] = array();	
		for($i=$data['time_start'];$i< $data['time_end']; $i++)
		{
			$data['date_contract'][$time_start]  = date('Y-m', $time_start);
			$date = $data['date_contract'][$time_start];

			$time_start = strtotime('+1 month', strtotime($date));
			if($time_start > $time_end)
				break;
		}

		if(is_module_active('backlink'))
		{	
			$data['targets'] = array();
			$data['targets']['backlink'] = array();
			$data['targets']['keyword'] = array();

			$this->load->model('backlink/base_kpi_m');
			$backlink_targets = $this->base_kpi_m
			->where('term_id',$edit_id)
			->as_array()
			->order_by('kpi_datetime')
			->get_many_by();

			if(!empty($backlink_targets))
				foreach ($backlink_targets as $target)
					$data['targets'][$target['kpi_type']][] = $target;

			$data['users'] = $this->admin_m->select('user_id,display_name')->where_in('role_id', $this->config->item('group_memberId'))->set_get_active()->order_by('display_name')->get_many_by();
			$data['users'] = key_value($data['users'],'user_id','display_name');
		}

		parent::render($data);
	}

	public function submit_kpi($term_id = 0,$delete_id = 0)
	{
		if($delete_id > 0)
		{
			$this->_delete_kpi($term_id,$delete_id);
			redirect(module_url('kpi/'.$term_id.'/'),'refresh');
		}
		
		$post = $this->input->post();
		if(empty($post)) return FALSE;

		if($this->input->post('submit_kpi_backlink') !== NULL)
		{
			$kpi_datetime = $this->input->post('target_date');
			$user_id = $this->input->post('user_id');
			$kpi_value = (int)$this->input->post('target_post');
			$kpi_type = $this->input->post('target_type');

			Modules::run('backlink/update_kpi',$term_id,$kpi_datetime,$user_id,$kpi_value,$kpi_type);
		}

		redirect(current_url(),'refresh');
	}

	private function _delete_kpi($term_id = 0,$delete_id=0)
	{
		$_tmp_model = 'webgeneral_kpi_m';
		$is_backlink_kpi = $this->input->get('kpi_type') == 'backlink';
		if($is_backlink_kpi){
			$_tmp_model = 'backlink_kpi_m';
			$this->load->model('backlink/backlink_kpi_m');
		}

		$check = $this->{$_tmp_model}->get($delete_id);
		$is_deleted = false;
		if($check)
		{
			if(strtotime($check->kpi_datetime) >= $this->mdate->startOfMonth())
			{
				$this->{$_tmp_model}->delete_cache($delete_id);
				$this->{$_tmp_model}->delete($delete_id);
				$is_deleted = true;
				$this->messages->success('Xóa thành công');
			}
		}
		($is_deleted) OR $this->messages->error('Xóa không thành công');
	}

	public function setting($term_id = 0){

		$data = array();
		if($this->input->post())
		{
			$metas = $this->input->post('meta');
			if($metas)
			{
				foreach($metas as $key=>$value)
				{
					if($key == 'report_key')
					{
						if($value == '')
						{
							$value = $this->seotraffic_m->encrypt('',$term_id);
						}
					}
					$this->termmeta_m->update_meta($term_id, $key, $value);
				}
			}
			$this->messages->success('Cập nhật thành công');
			redirect(current_url(),'refresh');
		}

		// $term = $this->term_m->get($term_id);
		$data['term'] = $this->term;
		$data['ga_accounts'] = $this->seotraffic_report_m->reload_account_ga();
		$data['term_id'] = $term_id;
		$this->template->title->prepend('Cấu hình ');
		parent::render($data);
	}

	//show to frontend
	public function report($edit_id = 0)
	{
		
		$data = array();

		// $this->template->body_class->set('sidebar-collapse');
		// $data['edit_id'] = $edit_id;
		parent::render($data);
	}

	public function email($term_id = 0)
	{
		$this->load->model('seotraffic_mail_m');
		$post_type = 'seotraffic-email';
		if($this->input->post())
		{
			$insert = array();
			$meta = $this->input->post('meta');

			$insert['post_title'] =  $this->input->post('title');
			$insert['post_content'] =  $this->input->post('post_content');
			$mail_id = $this->input->post('mailid');
			if($this->input->post('frequency') == 'weekly')
			{
				$day = date('d');
				$week = $this->seotraffic_m->get_dayOfWeekName($day);
				$date_range = $this->seotraffic_m->get_date_range($week, time());
				if(!isset($date_range[1]))
					return ;

				$insert['start_date'] = strtotime($date_range[0]);
				$insert['end_date'] = strtotime($date_range[1]);
				$insert['end_date'] = $this->mdate->endOfDay($insert['end_date']) + 1;
				if($mail_id && $mail_id >0)
				{
					$this->seotraffic_m->update($mail_id, $insert);
					$post_id =$mail_id;
				}
				else
				{
					$insert['post_type'] = $post_type;
					$post_id = $this->seotraffic_m->insert($insert);
					$this->term_posts_m->set_post_terms($post_id, array($term_id), $this->term_type);
				}
				$meta['config']['frequency']= $this->input->post('frequency');
				$meta = serialize($meta);
				$this->postmeta_m->update_meta($post_id, 'seotraffic_mail_report',$meta);
			}
			redirect(current_url(),'refresh');
		}

		$term = $this->term;

		$this->search_filter();
		$this->admin_ui->add_column('posts.post_id','ID');
		$this->admin_ui->select('term_id,end_date, start_date');
		$this->admin_ui->add_column('post_title','Tiêu đề', NULL, NULL, array('class' =>'col-md-4'));
		$this->admin_ui->add_column('frequency', array('set_select'=>FALSE, 'title' =>'Dữ liệu gửi', 'set_order'=> FALSE),NULL, NULL, array('class' =>'col-md-2'));

		$this->admin_ui->add_column('post_status','Trạng thái',NULL, NULL, array('class' =>'col-md-1'));
		$this->admin_ui->add_column('recipients', array('set_select'=>FALSE,'set_order'=> FALSE, 'title' =>'Người nhận'));

		$this->admin_ui->from('posts');
		$this->admin_ui->join('term_posts', 'term_posts.post_id = posts.post_id');
		$this->admin_ui->where('term_id',$term_id);
		$this->admin_ui->where('post_type', $post_type);

		$this->admin_ui->add_column_callback('frequency',function($data, $row_name){
			$meta = $this->postmeta_m->get_meta($data['post_id'], 'seotraffic_mail_report', TRUE, TRUE);
			$meta = @unserialize($meta);
			if($meta)
			{
				$mail_report = $meta['mail_report'];
				$mail_config = $meta['config'];
				// $data['frequency'] =  '<b>'.$this->mdate->week_name($mail_config['day_of_week']).'</b> hàng tuần<br>Dữ liệu: 1 tuần hoạt động';

				$s = $data['start_date'];
				$data['end_date'] = $this->mdate->endOfDay($data['end_date']) + 1;
				$data['frequency'] = 'Từ ngày: <b>'.date('d/m/Y',$s).'</b> <br>Đến ngày: <b>'.date('d/m/Y',$data['end_date']).'</b>';

				$data['recipients'] = 'Mail to: '.$mail_report['email_to'];
				if(isset($mail_report['email_cc']) && $mail_report['email_cc'])
				{
					$data['recipients'].='<br> Mail CC: '.$mail_report['email_cc'];
				}
			}

			return $data;
		}, FALSE);
		
		$this->admin_ui->add_column_callback('post_title',function($data, $row_name){
			$data['post_title'] = $this->seotraffic_mail_m->parse_email_template($data['post_title'], array(
				'start_date' => $data['start_date']
				), $data['term_id']);
			return $data;
		}, FALSE);
		
		$this->admin_ui->add_column_callback('post_status',function($data, $row_name){
			$date = $data['end_date'];
			$status = '';
			switch ($data['post_status']) {
				case 'publish':
				$status = '<span class="label label-info">Chờ gửi</span>';
				break;
				case 'sended':
				$status = '<span class="label label-success">Đã gửi</span>';
				break;
				case 'error':
				$status = '<span class="label label-danger">Gửi bị lỗi</span>';
				break;
			}

			if(time() > $date)
			{
				$text = human_timing($date,"", 'trước');
			}	
			else
			{
				$text = human_timing("",$date, 'nữa');
			}
			
			$data['post_status'] = ''.$status.' <br>'.$text.'<br>';

			return $data;
		}, FALSE);

		$this->admin_ui->add_column('view', array('set_select'=>FALSE, 'title' =>'Xem', 'set_order'=> FALSE));

		$this->admin_ui->add_column_callback('view',function($data, $row_name, $term){
			$time = time();
			$text = '';
			if($data['post_status'] == 'publish')
				$text.= anchor(current_url(),'Edit','class="ajax_edit" data-mailid="'.$data['post_id'].'"').' | ';

			$text.= anchor(current_url(),'Xóa','class="ajax_delete" data-mailid="'.$data['post_id'].'"').' | ';
			$text.= anchor($this->seotraffic_mail_m->get_report_link($data['term_id'], $term->term_name, $data['post_id']),'Xem','target="_blank"');

			$data['view'] = $text;
			return $data;
		}, FALSE,$term);

		// $this->admin_ui->order_by("posts.post_id desc");
		$data['content'] = $this->admin_ui->generate();
		$data['term_id'] = $term_id;
		$this->template->title->prepend('Email ');
		parent::render($data);
	}

	function ajax_kpi($term_id = 0)
	{
		$result = array('success'=>false, 'msg'=>'Có lỗi xảy ra');
		if($type = $this->input->post('name'))
		{
			$pk = $this->input->post('pk');
			$val = $this->input->post('value');
			switch ($type) {
				case 'change-kpi':
				$this->seotraffic_kpi_m->where('term_id',$term_id)->where('kpi_datetime',$pk)->update_by(array('kpi_value'=>$val));
				$count_row = $this->db->affected_rows();
				if($count_row == 0)
				{
					$this->seotraffic_kpi_m->insert(array(
						'term_id' => $term_id,
						'kpi_datetime' => $pk,
						'kpi_value'=>$val));
				}
				$this->seotraffic_kpi_m->delete_cache($term_id);
				$result['success'] = true;
				break;
				
				default:
					# code...
				break;
			}
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}


	function task($id = 0)
	{
		if($this->input->post())
		{
			Modules::run('seotraffic/tasks/insert_task',$id);
		} 

		$this->search_filter();

		$this->admin_ui->add_column('posts.post_id','#');
		$this->admin_ui->add_column('post_title','Tiêu đề');
		$this->admin_ui->add_column('post_content', 'Nội dung');
		$this->admin_ui->add_column('post_author', 'Người tạo');
		$this->admin_ui->add_column('start_date','Ngày thêm','$1','date("d/m/Y",start_date)');

		$this->admin_ui->where('post_type', 'seotraffic-task');
		$this->admin_ui->where('term_id',$id);
		$this->admin_ui->order_by("posts.post_id desc");
		$this->admin_ui->from('posts');
		$this->admin_ui->join('term_posts', 'term_posts.post_id = posts.post_id');

		$this->admin_ui->add_column_callback(array('post_content','post_author'),array($this,'callback_post_get_meta'), FALSE);
		$this->admin_ui->add_column('view', array('set_select'=>FALSE, 'set_order'=> FALSE, 'title' =>'Xem'),anchor(module_option_url('webdoctor','tasks/edit/$1'),'Edit','class="ajax_edit" data-taskid="$1"').' | '.anchor(module_option_url('webdoctor','tasks/edit/$1'),'Xóa','class="ajax_delete" data-taskid="$1"'),'post_id');
		$data['content'] = $this->admin_ui->generate();
		parent::render($data);
	}
	
	function callback_post_get_meta($post, $row_name, $arg = array())
	{
		switch ($row_name) {
			case 'post_content':
			if(valid_url($post['post_content']))
			{
				$post['post_content'] = anchor($post['post_content'], 'Link',array('title' => $post['post_content'],'target' =>'_blank'));
			}
			break;
			case 'post_author':
			$post['post_author'] = $this->admin_m->get_field_by_id($post['post_author'], 'display_name');
			break;

			default:
			break;
		}
		return $post;
	}

	public function documentation()
	{
		$data = array();
		if($this->router->fetch_method())
		{
			// echo 'xx';
		}
		$this->template->javascript->add('plugins/documentation/docs.js');
		$this->template->stylesheet->add('plugins/documentation/style.css');
		$this->template->body_class->append(' fixed');
		// $this->template->content_box_class->set('');
		$this->template->title->set('Hướng dẫn sử dụng');
		// $this->menu->add_item(array(
		// 	'id' => 'seotraffic-213',
		// 	'name' => 'AAA',
		// 	'parent' => NULL,
		// 	'slug' => '#introduction',
		// 	'order' => 5,
		// 	'icon' => 'fa fa-circle-o'
		// 	), 'left');
		for($i=0; $i<20; $i++)
		{
			$this->menu->add_item(array(
			'id' => 'seotraffic-'.$i,
			'name' => 'A '.$i,
			'parent' => NULL,
			'slug' => '#downloadx-'.$i,
			'order' => $i,
			'icon' => 'fa fa-circle-o'
			), 'left');
		}
		

		// $this->menu->inject_item('<li class="header">MAIN NAVIGATION</li>', 'first');

// echo $this->menu->render_menu('left','Item-0');
// $this->menu->clear('left');
		// $this->menu->clear('left');

		parent::render($data);
	}

	function ajax($type = '', $id = 0)
	{
		switch ($type) {
			case 'email_edit':
			$post_id = $id;
			if($post_id <= 0) return '';
			$post = $this->post_m->get_by(array('post_id'=>$post_id, 'post_type' => 'seotraffic-email'));
			if(!$post)
				return FALSE;
			$results = array();
			$results['id'] = $post->post_id;
			$results['title'] = $post->post_title;
			$results['content'] = $post->post_content;
			$meta = $this->postmeta_m->get_meta($post_id,'seotraffic_mail_report',TRUE, TRUE);
			$meta = @unserialize($meta);

			$results['email_cc'] = '';
			$results['email_to'] = '';
			$results['action'] = '';
			if($meta)
			{
				$config = $meta['config'];
				$mail_report = $meta['mail_report'];
				$results['email_cc'] = $mail_report['email_cc'];
				$results['email_to'] = $mail_report['email_to'];
				$results['action'] = $config['action'];
				$results['day_of_week'] = $config['day_of_week'];
				$results['frequency'] = $config['frequency'];
			}
			$this->output->set_content_type('application/json', 'utf-8')->set_output(json_encode($results)) ->_display();;
			die();
			break;
			case 'email-delete':
			$post_id = $id;
			if($post_id >0)
			{
				$check = $this->post_m->where('post_type', 'seotraffic-email')->get($post_id);
				if($check)
				{
					$this->post_m->delete_post($post_id);
					die('OK');
				}
				die('Không xóa được, không đúng định dạng tác vụ');
			}
			die('Không xóa được, có lỗi xảy ra');
			break;
			case 'ga-reload':
			$results = $this->seotraffic_report_m->reload_account_ga(true);
			$data = array();
			foreach($results as $id =>$result)
			{
				$data[] = array("id"=> $id, 'text'=>$result);
			}
			die(json_encode($data));
			break;
			default:
				# code...
			break;
		}
	}

	public function sms($id = 0)
	{
		$data = array();
		$this->load->library('sms');
		if($meta = $this->input->post('meta'))
		{
			if(empty($meta['phone_report']))
				return false;

			$this->termmeta_m->update_meta($id, 'phone_report', $meta['phone_report']);

			redirect(current_url(),'refresh');
		}
		$data['meta']['phone_report'] = $this->termmeta_m->get_meta_value($id, 'phone_report');
		$data['id'] = $id;
		$data['logs'] = $this->seotraffic_log_m->order_by('log_id','desc')->get_many_by(array(
			'log_type'=> 'seotraffic-report-sms',
			'term_id' =>$id

			));
		parent::render($data);
	}

	public function send_sms()
	{
		$this->load->library('sms');
		$r = $this->sms->send('0962243823','HieuPT test SMS '.date('d/m/Y H:i:s'));
		// $r = $this->sms->send('0962243823','Báo cáo các công việc đã thực hiện được HIEUPT');
		// $r = $this->sms->get_status_label(359);
		prd($r);
	}
}