<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check extends Admin_Controller {

	public function index($next_month = 0)
	{
		$this->load->model('seotraffic_report_m');
		$this->load->library('mdate');
		$this->load->add_package_path(APPPATH.'third_party/google-analytics/');

		
		$terms = $this->term_m->where('term_status',1)->get_many_by('term_type', 'seo-traffic');
		$sms = $this->sms($terms);
		$time = time();
		if($next_month == 1)
		{
			$time =  date('Y-m-d', strtotime('+1 month'));
			$time =  $this->mdate->startOfMonth($time);

		}

		$kpis = $this->kpi($terms, $time);
		// prd($kpis);
		$ga_accounts = $this->seotraffic_report_m->reload_account_ga();

		foreach($terms as $term)
		{
			$ga_id = $this->termmeta_m->get_meta_value($term->term_id, 'ga_profile_id');

			$this->table->add_row(
				$term->term_id,
				$term->term_name,
				(isset($sms[$term->term_id]) ? $sms[$term->term_id] : '---'),
				(isset($kpis[$term->term_id]) ? $kpis[$term->term_id] : '---'),
				(isset($ga_accounts[$ga_id]) ? $ga_accounts[$ga_id] : '---')
				);
		}

		$this->table->set_caption(''.date('d/m/Y', $time));
		$this->table->set_heading('#', 'Name', 'SMS', 'KPIs', 'GA');
		$x = $this->table->generate();
		$this->template->title->set('Tình trạng cấu hình các dự án');
		parent::render(array('content'=>$x), 'blank');
	}

	private function sms($terms)
	{
		$time = time();
		$result = array();
		if($terms)
		{
			foreach($terms as $term)
			{
				$phone = $this->termmeta_m->get_meta_value($term->term_id, 'phone_report');
				$result[$term->term_id] = $phone;
			}
		}
		return $result;
	}
	private function kpi($terms, $time)
	{
		
		$result = array();

		$this->load->model('seotraffic_kpi_m');
		if($terms)
		{
			foreach($terms as $term)
			{
				$kpis = $kpis = $this->seotraffic_kpi_m->get_kpi_value($term->term_id, $time);
				$result[$term->term_id] = $kpis;
			}
		}
		return $result;
	}

}

/* End of file check.php */
