<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seotraffic_report_m extends Base_Model {

	public $ga_label = array(
		'ga:sessions' =>'Tổng visit',
		'Organic Search' => 'Organic Search',
		'Direct' => 'Direct',
		'Referral' => 'Referral',
		'Social'=> 'Social',
		'ga:bounceRate'=> 'Bounce Rate',
		'ga:pageviewsPerSession'=> 'Pages / Session',
		'ga:avgSessionDuration'=> 'Time On Site');

	function get_ga_label($label = '')
	{
		return isset($this->ga_label[$label]) ? $this->ga_label[$label] 
		: 'Không xác định';
	}

	function reload_account_ga($type = false)
	{
		// $this->load->library('ga');
		$results = array();

		//get all account
		if($type || !($accounts = $this->scache->get('modules/seotraffic/ga-listAccount')))
		{
			$items = $this->ga->getListAccount();
			$accounts = array();
			if($items)
				foreach($items as $item)
				{
					$id = $item->getId();
					$accounts[$id] = array();
					$accounts[$id]['id'] = $id;
					$accounts[$id]['name'] = $item->getName();
				}
				$this->scache->write($accounts,'modules/seotraffic/ga-listAccount');
			}
			if(!$accounts)
				return null;
			//get list webproperties
			$webproperties = array();
			foreach($accounts as $account)
			{
				$accountId = $account['id'];
				$key_cache = 'modules/seotraffic/ga-property-'.$accountId;
				if(!$webproperties[$accountId] = $this->scache->get($key_cache))
				{
					$items = $this->ga->getListWebproperties($accountId);
					$webproperties[$accountId] = array();
					if($items)
					{
						foreach($items as $item)
						{
							$id = $item->getId();
							$webproperties[$accountId][$id] = array();
							$webproperties[$accountId][$id]['id'] = $id;
							$webproperties[$accountId][$id]['name'] = $item->getName();
						}
						$this->scache->write($webproperties[$accountId], $key_cache);
					}
					
				}
			}
			if(!$webproperties)
			{
				return null;
			}
			$profiles = array();

			foreach($webproperties as $accountId =>$webproperty)
			{
				foreach($webproperty as $web)
				{
					$id = $web['id'];
					$name = $web['name'];
					$key_cache = 'modules/seotraffic/ga-profile-'.$accountId.'-'.$id;

					if(!$profiles = $this->scache->get($key_cache))
					{
							// $profiles[$accountId][$id] = array();
						$listProfiles = $this->ga->getListProfiles($accountId, $id);

						if($listProfiles)
						{
							foreach($listProfiles as $i =>$item)
							{
								$tmp = array();
								$tmp['id'] = $item->getId();
								$tmp['name'] = $item->getName();
								$tmp['websiteUrl'] = $item->getWebsiteUrl();
								$profiles[] = $tmp;
							}
							$this->scache->write($profiles, $key_cache);
						}
					}
					if($profiles)
					{
						foreach($profiles as $profile)
						{
							$results[$profile['id']] = $profile['name'].' - '.$profile['websiteUrl'];
						}
					}

				}
			}
			return $results;
		}

		public function get_ga($start_date, $end_date, $ga_profile_id)
		{
			$ga = $this->seotraffic_m->get_ga($start_date, $end_date, $ga_profile_id);
			if(!$ga)
				return FALSE;

			$data = array();
			$data['ga'] = array();
			$data['ga']['results'] = array();
			$data['ga']['total'] = array();
			$data['ga']['headers'] = array_keys($ga['results']);

			foreach($ga['results'] as $date => $results)
			{
				foreach($results as $key=>$result)
				{
					$tmp_key = $key;
					switch ($key) {
						case 'ga:avgSessionDuration':
						$result = gmdate('H:i:s',$result);
					// $key = 'Time on site';
						break;
						case 'ga:pageviewsPerSession':
						$result = round($result, 2);
						break;

						case 'ga:bounceRate':
						$result = (int)$result.'%';
					// $key = 'Bounce Rate';
						break;
						case 'ga:sessions':
					// $key = 'Tổng visit';
						break;
						case 'Organic Search':
						case 'Referral':
						case 'Social':
						case '(Other)':
						break;

						default:

						break;
					}

					if(isset($ga['totalsForAllResults'][$tmp_key]))
					{
						$val = $ga['totalsForAllResults'][$tmp_key];
						unset($ga['totalsForAllResults'][$tmp_key]);
						switch ($tmp_key) {
							case 'ga:avgSessionDuration':
							$val = gmdate('H:i:s',$val);
							break;
							case 'ga:bounceRate':
							$val = (int)$val.'%';
							break;
							case 'ga:pageviewsPerSession':
							$val = round($val, 2);
							break;
							default:

							break;
						}
						$data['ga']['total'][$key] = $val;
					}
					$data['ga']['results'][$date][$key] = $result;
				}
			}
			return $data['ga'];
		}
	}

	/* End of file seotraffic_report_m.php */
/* Location: ./application/modules/seotraffic/models/seotraffic_report_m.php */