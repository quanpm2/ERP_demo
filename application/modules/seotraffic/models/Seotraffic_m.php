<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seotraffic_m extends Post_m {

	public $dayOfWeekName = array();
	public $week_range = array();
	public $term_type = 'seo-traffic';

	function __construct()
	{
		parent::__construct();
		$this->init_dayOfWeekName();
		
	}

	/*
		Kiểm tra xem ngày đưa vào là tuần bao nhiêu của tháng
	*/
		function get_dayOfWeekName($day = 0)
		{
			return (isset($this->dayOfWeekName[$day])) ? $this->dayOfWeekName[$day] : 0;
		}

		function get_date_range($week = 1, $current_date = 0)
		{
			$week_range = $this->init_date_range($current_date);
			return (isset($week_range[$week])) ? $week_range[$week] : 0;
		}

		function init_date_range($date = 0)
		{
			if($date == 0)
				$date = time();
			$week_range = array();
			$week_range[1] = array(date('Y-m-01',$date), date('Y-m-07',$date));
			$week_range[2] = array(date('Y-m-08',$date), date('Y-m-14',$date));
			$week_range[3] = array(date('Y-m-15',$date), date('Y-m-21',$date));
			$week_range[4] = array(date('Y-m-22',$date), date('Y-m-t',$date));
			return $week_range;
		}
		

		private function init_dayOfWeekName()
		{
			for($i=1; $i<=31; $i++)
			{
				if($i <=7)
					$week = 1;
				else if($i <=14)
					$week = 2;
				else if($i <=21)
					$week = 3;
				else
					$week = 4;
				$j = str_pad($i, 2, "0", STR_PAD_LEFT);
				$this->dayOfWeekName[$j] = $week;
			}
			return $this;
		}

		function encrypt($string, $term, $type='encode')
		{
			
			$this->load->library('encrypt');
			if(!$term)
				return FALSE;
			if(is_object($term))
			{
				$term_id = $term->term_id;
			}
			else if(is_array($term))
			{
				$term_id = $term['term_id'];
			}
			else
			{
				$term_id = $term;
			}
			$key = 'webdoctor-'.$term_id;

			if($type == 'encode')
				return $this->encrypt->encode($term_id,$key);

			return $this->encrypt->decode($string,$key);
		}

		function get_ga($start_date, $end_date, $profileId)
		{
		/*
		Organic Search
		Direct
		Referral
		Social
		-----------
		Acquisition Overview


		Tổng visit
		Page/ Visit
		Bounce Rate
		Time on site
		----------
		Audience Overview
		*/

		if(!$profileId)
			return FALSE;
		$key_cache = 'ga/'.$profileId.'/'.$start_date.'-'.$end_date;
		if($ga = $this->scache->get($key_cache))
		{
			return $ga; //return cache
		}

		$this->load->add_package_path(APPPATH.'third_party/google-analytics', FALSE);
		// $this->load->library('ga');
		// $data = array();
		// if(strpos($profileId, 'ga:') === FALSE)
		// 	$profileId = 'ga:'.$profileId;

		//Audience Overview
		$metrics = "ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession";
		$optParams = array("dimensions" => "ga:date");

		try{
			$ga_aud = $this->ga->get($profileId, $start_date,$end_date,$metrics,$optParams)->results();
		}
		catch(Google_Service_Exception $e)
		{
			// echo 'There was an Analytics API service error '. $e->getCode() . ':' . $e->getMessage();
			
			$this->load->remove_package_path(APPPATH.'third_party/google-analytics');
			return FALSE;
		}
		//Acquisition Overview
		$metrics = "ga:sessions";
		$optParams = array("dimensions" => "ga:date,ga:channelGrouping");
		$ga_acq = $this->ga->get($profileId, $start_date,$end_date,$metrics,$optParams)->results();

		$data['ga'] = array();
		$data['ga']['results'] = array();
		$data['ga']['totalsForAllResults'] = array();
		if($ga_aud->rows)
		{
			foreach($ga_aud->rows as $row)
			{
				$date = $row[0];
				unset($row[0]);
				$data['ga']['results'][$date] = array();
				$data['ga']['results'][$date]['ga:sessions'] = $row[1];
				$data['ga']['results'][$date]['ga:bounceRate'] = $row[2];
				$data['ga']['results'][$date]['ga:avgSessionDuration'] = $row[3];
				$data['ga']['results'][$date]['ga:pageviewsPerSession'] = $row[4];
			}
		}
		foreach($ga_aud->totalsForAllResults as $key=>$row)
		{
			$data['ga']['totalsForAllResults'][$key] = $row;
		}
		if($ga_acq->rows)
		{
			foreach($ga_acq->rows as $i=>$row)
			{
				$date = $row[0];
				$name = $row[1];
				$val = $row[2];
				$data['ga']['results'][$date][$name] = $val;
				if(!isset($data['ga']['totalsForAllResults'][$name]))
					$data['ga']['totalsForAllResults'][$name] = 0;
				$data['ga']['totalsForAllResults'][$name]+= $val;
			}
		}
		$this->load->remove_package_path(APPPATH.'third_party/google-analytics');

		$time_cache = 0;
		if($this->mdate->startOfDay($end_date) >= $this->mdate->startOfDay())
				$time_cache = 1800; //cache 30 phút
			$this->scache->write($data['ga'],$key_cache, $time_cache);
			
			return $data['ga'];
		}

		function predict_traffic($curent_total = 0, $curent_day= 0, $total_day = 0, $total = 0, $type = 'percent')
		{
			if(!$total || $total == 0)
				return 0;
			if(!$curent_day || $curent_day == 0)
				return 0;

			$avg = $curent_total/$curent_day;
			$result = $avg * $total_day;
			if($type == 'percent')
			{
				$result = $result / $total;
				$result = round($result, 4) * 100;
			}
			return round($result, 2);
			
		}
	}

	/* End of file Seotraffic_m.php */
/* Location: ./application/modules/seotraffic/models/Seotraffic_m.php */