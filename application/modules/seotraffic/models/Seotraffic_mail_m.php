<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seotraffic_mail_m extends Base_model {
	public $_table = 'mail_log';
	public $primary_key = 'mail_id';
	public $mail_type = 'seotraffic-email';

	function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->model('postmeta_m');
		$this->load->model('seotraffic_m');
		$this->load->model('term_posts_m');
		$this->load->model('term_m');
		$this->load->library('mdate');
	}
	function get_mail($mail_id)
	{
		$this->where('mail_type',$this->mail_type);
		return $this->get($mail_id);
	}
	
	function get_post($post_id)
	{
		$this->post_m->where('post_type',$this->mail_type);
		return $this->post_m->get($post_id);
	}

	function send_all_reports($term_id)
	{

		$time = time();
		// echo $time ;
		$posts = $this->post_m->get_posts(array(
			'select' =>'posts.post_id,post_title,post_content,post_type,start_date,end_date,term.term_id,term_name,term_type,term_parent',
			'tax_query'=> array(
				'taxonomy' => $this->seotraffic_m->term_type,
				'terms' => $term_id,
				),
			'where' => array(
				'end_date <=' => $time
				),
			'post_status' => 'publish',
			// 'meta_key' => 'webdoctor_mail_report',
			'post_type' => $this->mail_type
			));

		if(!$posts)
			return 'No email';

		foreach($posts as $post)
		{
			$new_post_id = $this->copy($post->post_id);
			$result = $this->email_send_report($post->post_id,$post->term_id,$post->post_title,$post->post_content, $post->start_date);
			if($result['post_status'] == 'sended')
			{

			}

			$this->seotraffic_m->update($post->post_id, array(
				'post_title' => $result['title'],
				'post_status' => $result['post_status'],
				'updated_on' => time()
				));
		}
	}

	function copy($post_or_id)
	{
		if(is_object($post_or_id) || is_array($post_or_id))
			$post = $post_or_id;
		else
			$post = $this->post_m->get($post_or_id);
		if(!$post)
			return 'No post';
		$post_id = $post->post_id;
		unset($post->post_id);
		unset($post->post_status);
		unset($post->post_content);
		unset($post->post_excerpt);
		$post->created_on = time();
		$next_week =  $this->get_next_week($post->start_date);

		if($next_week == 1)
		{
			$post->start_date  = strtotime('+1 month', $post->start_date);
		}
		$date_range = $this->seotraffic_m->get_date_range($next_week, $post->start_date);

		if(!isset($date_range[1]))
			return;
		$post->post_title = 'Báo cáo chỉ số GA tuần {week} tháng {month}/{year} của {website_name_up}';
		$post->start_date = $this->mdate->startOfDay($date_range[0]);
		$post->end_date = $this->mdate->endOfDay($date_range[1]);
		$new_post_id = $this->post_m->insert($post);
		$post_terms = $this->term_posts_m->get_the_terms($post_id);
		if($post_terms)
		{
			foreach($post_terms as $pt)
			{
				$this->term_posts_m->insert(array('term_id' => $pt, 'post_id'=> $new_post_id));
			}
		}

		//insert post meta
		$post_metas = $this->postmeta_m->get_many_by('post_id',$post_id);
		if($post_metas)
		{
			foreach($post_metas as $meta)
			{
				$this->postmeta_m->update_meta($new_post_id, $meta->meta_key, $meta->meta_value);
			}
		}
		return $new_post_id;
	}

	function get_next_week($date)
	{
		$week = $this->seotraffic_m->get_dayOfWeekName(date('d',$date));
		$week =(int)$week;
		$week = $week % 4;
		return ++$week;
	}

	function email_send_report($post_id, $term_id, $title, $content, $time = 1)
	{
		$term = $this->term_m->get($term_id);
		if(!$term)
			return 'No email';
		$data = array();
		$result = array();
		// $this->seotraffic_m->init_date_range($time);
		
		$data['week'] = $this->seotraffic_m->get_dayOfWeekName(date('d',$time));
		$date_range = $this->seotraffic_m->get_date_range($data['week'], $time);

		if(count($date_range) <2)
			return 'No date range';

		$data['start_date'] = strtotime($date_range[0]);
		$data['end_date'] = $this->mdate->endOfDay($date_range[1]);

		$data['report_link'] = $this->get_report_link($term_id, $term->term_name, $post_id);

		$message = $this->load->view('report/tpl_email', $data, TRUE);
		$meta = $this->postmeta_m->get_meta($post_id,'seotraffic_mail_report', TRUE, TRUE);
		$meta = @unserialize($meta);
		$email_to = '';
		$email_cc = '';
		if($meta)
		{
			$mail_report = $meta['mail_report'];
			$mail_config = $meta['config'];
			
			$email_to = $mail_report['email_to'];
			if(isset($mail_report['email_cc']) && $mail_report['email_cc'])
			{
				$email_cc = $mail_report['email_cc'];
			}
		}

		$this->email->from('sccom@sccom.vn', 'Webdoctor.vn');
		$this->email->to($email_to);
		if($email_cc) $this->email->cc($email_cc);

		$title = $this->parse_email_template($title, array(
			'start_date' => $data['start_date'],
			'end_date' => $data['end_date'],
			'week' => $data['week'],
			), $term_id, $term);
		$this->email->subject($title);
		$this->email->message($message);
		
		$send_status = TRUE;
		$send_status = $this->email->send();
		$post_status = ($send_status) ? 'sended' :'error';

		if(!$send_status)
		{
			$result['post_excerpt'] = $this->email->print_debugger();
		}
		$result['title'] = $title;
		$result['message'] = $message;
		$result['email_to'] = $email_to;
		$result['email_cc'] = $email_cc;
		$result['post_status'] = $post_status;

		return $result;

	}
	function get_report_link($term_id, $term_name, $post_id)
	{
		$key = $this->termmeta_m->get_meta($term_id, 'report_key', TRUE, TRUE);
		if(!$key)
		{
			$key = $this->seotraffic_m->encrypt('',$term_id);
			$this->termmeta_m->update_meta($term_id,'report_key',$key);
		}

		$q = array(
			'p' => $post_id,
			'domain' => $key,
			);
		// return base_url().'report/seo-traffic/'.$term_name.'?'.http_build_query($q);
		return 'http://report.webdoctor.vn/seo-traffic/'.$term_name.'?'.http_build_query($q);
	}

	function parse_email_template($string, $data = array(), $term_id = 0, $term = FALSE)
	{
		$this->load->library('parser');
		$end_date = strtotime('tomorrow')-1;
		$start_date = (isset($data['start_date'])) ? $data['start_date'] :strtotime('last Monday', $end_date);

		if(!$term)
			$term = $this->term_m->get($term_id);
		$parse_data = array(
			'day' => date('d',$start_date),
			'month' => date('m',$start_date),
			'year' => date('Y',$start_date),
			'week' => 1,
			'start_date' => date('d/m/Y',$start_date),
			'end_date' => date('d/m/Y',$end_date),
			'customer_name' => $this->termmeta_m->get_meta($term_id, 'customer_name', TRUE, TRUE),
			'company_name' => $this->termmeta_m->get_meta($term_id, 'company_name', TRUE, TRUE),
			'website' => $term->term_name,
			'website_name_up' => strtoupper($term->term_name),
			);
		$parse_data['week'] = $this->seotraffic_m->get_dayOfWeekName($parse_data['day']);
		$parse_data = array_merge($parse_data, $data);
		return $this->parser->parse_string($string, $parse_data, TRUE);
	}

}

/* End of file seotraffic_mail_m.php */
/* Location: ./application/modules/seotraffic/models/seotraffic_mail_m.php */