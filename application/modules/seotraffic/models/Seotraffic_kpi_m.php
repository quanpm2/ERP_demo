<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seotraffic_kpi_m extends Base_Model {
	public $_table = 'seotraffic_kpi';
	public $primary_key = 'kpi_id';
	public $before_create = array('delete_cache');
	public $after_update = array('delete_cache');
	public $before_delete = array('delete_cache');

	function get_kpi_value($term_id = 0, $time = 0)
	{
		if($time == 0)
			$date = date('Y-m-01');
		else
			$date = date('Y-m-01',$time);
		$key_cache = 'modules/seotraffic/kpi-'.$term_id;
		if(($kpis = $this->scache->get($key_cache)) === FALSE)
		{
			$results = $this->order_by('kpi_datetime','asc')->get_many_by(array(
				'term_id' => $term_id
				));
			$kpis = array();
			if($results)
			{
				foreach($results as $result)
				{
					$kpis[$result->kpi_datetime] = $result->kpi_value;
				}
			}
			$this->scache->write($kpis,$key_cache);
		}

		if($time > 0 && isset($kpis[$date]))
			return $kpis[$date];

		if($time > 0 && !isset($kpis[$date]))
			return 0;
		return $kpis;
	}

	function delete_cache($term)
	{
		if(is_array($term))
		{
			if(isset($term['term_id']))
			{
				$key_cache = 'modules/seotraffic/kpi-'.$term['term_id'];
				$this->scache->delete($key_cache);
			}
		}
		else
		{
			$key_cache = 'modules/seotraffic/kpi-'.$term;
			$this->scache->delete($key_cache);
		}
		return $term;
	}
}

/* End of file seotraffic_kpi_m.php */
/* Location: ./application/modules/seotraffic/models/seotraffic_kpi_m.php */