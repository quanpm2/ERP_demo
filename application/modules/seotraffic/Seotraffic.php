<?php
class Seotraffic_Package extends Package
{
	function __construct()
	{
		parent::__construct();
		$this->website_id  = $this->uri->segment(4);
	}

	public function name()
	{
		return 'Seotraffic';
	}

	public function init(){
		
		if(is_module_active('seotraffic') && has_permission('Seotraffic.Index.access'))
		{
			$module_url = admin_url().'seotraffic/';
			$this->menu->add_item(array(
				'id' => 'seotraffic',
				'name' => 'SEO chặng',
				'parent' => null,
				'slug' => $module_url,
				'order' => 1,
				'icon' => ''
				), 'navbar');
			if(is_module('seotraffic'))
			{
				$this->menu->add_item(array(
					'id' => 'seotraffic-documentation',
					'name' => 'Hướng dẫn sử dụng',
					'parent' => null,
					'slug' => $module_url.'documentation.html',
					'order' => 99,
					'icon' => ''
					), 'left');
				// $this->menu->add_item(array(
				// 	'id' => 'seotraffic-sms',
				// 	'name' => 'SMS',
				// 	'parent' => null,
				// 	'slug' => $module_url.'sms/',
				// 	'order' => 4,
				// 	'icon' => ''
				// 	), 'left');

				if($this->website_id > 0)
				{
					$methods = array(
						'overview'=>'Overview',
						'email' => 'Email',
						'kpi'=>'KPI',
						'sms'=>'SMS',
						'task'=>'Bài viết',
						'setting' =>'Cấu hình'
						);
					$order = 0;
					foreach($methods as $method=>$name)
					{
						if(has_permission('Seotraffic.'.$method.'.access'))
						{
							$this->menu->add_item(array(
								'id' => 'seotraffic-'.$method,
								'name' => $name,
								'parent' => NULL,
								'slug' => $module_url.$method.'/'.$this->website_id.'/',
								'order' => $order++,
								'icon' => ''
								), 'left');
						}
					}
				}
			}
		}
	}

	public function title()
	{
		return 'SEO chặng';
	}

	public function author()
	{
		return 'HTLove';
	}

	public function version()
	{
		return '1.0';
	}

	public function description()
	{
		return 'SEO chặng';
	}
	
	private function init_permissions()
	{
		$permissions = array();
		$permissions['Module.Seotraffic'] = array(
			'description' => '',
			'actions' => array('manage'));

		$permissions['Seotraffic.Index'] = array(
			'description' => '',
			'actions' => array('access'));
		
		$permissions['Seotraffic.Overview'] = array(
			'description' => '',
			'actions' => array('access'));

		$permissions['Seotraffic.KPI'] = array(
			'description' => '',
			'actions' => array('access','add','delete','update'));

		$permissions['Seotraffic.Setting'] = array(
			'description' => '',
			'actions' => array('access','add','delete','update'));

		$permissions['Seotraffic.Email'] = array(
			'description' => '',
			'actions' => array('access','add','delete','update'));
		
		$permissions['Seotraffic.Task'] = array(
			'description' => '',
			'actions' => array('access','add','delete','update'));

		$permissions['Seotraffic.Documentation'] = array(
			'description' => '',
			'actions' => array('access'));

		$permissions['Seotraffic.SMS'] = array(
			'description' => '',
			'actions' => array('access','add','delete','update'));

		return $permissions;
	}

	public function install()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			$description = $value['description'];
			$actions = $value['actions'];
			$this->permission_m->add($name, $actions, $description);
		}
	}

	public function uninstall()
	{
		$permissions = $this->init_permissions();
		if(!$permissions)
			return false;
		foreach($permissions as $name => $value)
		{
			// $this->permission_m->delete_by_name($name);
		}
	}
}