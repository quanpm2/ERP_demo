 <?php 


 echo $this->admin_form->form_open('',array(), array(), TRUE);

 echo $this->admin_form->input('Số điện thoại','meta[phone_report]', @$meta['phone_report'], 'Mỗi số điện thoại cách nhau bởi dấu ,', array('id'=>'phone_report'));

 echo $this->admin_form->submit('submit','Lưu lại');

 $this->admin_form->form_close();


 $report_next_time = $this->termmeta_m->get_meta_value($id, 'sms-report-nextsend');
 if(!$report_next_time)
 	$report_next_time = 'Chưa có';
 else
 	$report_next_time = date('d/m/Y H:i:s', $report_next_time);
 
 $this->table->set_heading(array('#', 'Phone', 'Brand','Thời gian', 'Nội dung', 'Trạng thái'));
 $this->table->set_caption('Danh sách SMS<br><em>Ngày gửi tiếp theo: '. $report_next_time);
 if($logs)
 	foreach($logs as $log)
 	{
 		$phone = $this->seotraffic_log_m->meta->get_meta_value($log->log_id,'sms_recipient');
 		$brand = $this->seotraffic_log_m->meta->get_meta_value($log->log_id,'sms_service_id');
 		$this->table->add_row(array(
 			$log->log_id, 
 			$phone, 
 			$brand,
 			$log->log_time_create,
 			$log->log_title,
 			$this->sms->get_status_label($log->log_status)
 			));

 	}

 	echo $this->table->generate();

 	?>