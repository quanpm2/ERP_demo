<span class="label label-success">Dự đoán đạt</span> <span class="label label-info">Dự đoán gần đạt (80% - 90%)</span> <span class="label label-warning">Dự đoán có thể đạt (50% - 80%)</span> <span class="label label-danger">Dự đoán khó đạt (< 50%)</span>

<?php

$time = time();
$day = date('d',$time) - 1;


if($day == 0)
{
	$time = strtotime('yesterday',$time);
	$day = date('d',$time);

}

$time = strtotime(date('Y-m-'.$day, $time));

$start_date = date('Y-m-01',$time);
$day = str_pad($day, 2, "0", STR_PAD_LEFT);
$end_date = date('Y-m-'.$day, $time);

$dateEnfOfMonth = date('d',$this->mdate->endOfMonth($time));
$this->table->set_heading(array('#','Website', 'Hiện tại/Chỉ tiêu','Tình trạng visit/ngày	
	','Dự đoán','Loại'));
$day_remain = $dateEnfOfMonth - $day;
$day_remain = ($day_remain == 0) ? 1 : $day_remain;

$this->table->set_caption('Thời gian thống kê: '.date('01/m/Y',$time).' - '.date('d/m/Y',$time).' (còn lại '.$day_remain.' ngày )');
if($lists)
{
	foreach($lists as $i=>$term)
	{
		$ga_profile_id = $this->termmeta_m->get_meta_value($term->term_id, 'ga_profile_id');
		$seotraffic_type = $this->termmeta_m->get_meta_value($term->term_id, 'seotraffic_type');

		$ga_data = $this->seotraffic_m->get_ga($start_date, $end_date,$ga_profile_id);

		
		$kpi = $this->seotraffic_kpi_m->get_kpi_value($term->term_id, $time);

		$ga_data_result = '';
		if(isset($ga_data['totalsForAllResults'][$seotraffic_type]))
		{
			$ga_data_result = $ga_data['totalsForAllResults'][$seotraffic_type];
		}

		if(!$kpi || !$seotraffic_type || !$ga_data_result)
		{
			$text_notice = anchor(admin_url().'seotraffic/kpi/'.$term->term_id, 'Chưa cấu hình chỉ tiêu', 'data-toggle="tooltip" title="Cấu hình KPI"');
			if(!$ga_data_result)
			{
				$text_notice = anchor(admin_url().'seotraffic/kpi/'.$term->term_id, 'Không có dữ liệu GA', 'data-toggle="tooltip" title="GA ID: '.$ga_profile_id.'"');
			}
			else if(!$seotraffic_type)
			{
				$text_notice = anchor(admin_url().'seotraffic/setting/'.$term->term_id, 'Chưa cấu hình loại hợp đồng', 'data-toggle="tooltip" title="Cấu hình loại hợp đồng"');
			}
			$kpi = $text_notice;
			$g = $kpi;
			$txt_visit_day = $kpi;
			$g2 = $kpi;
			$visit_avg = '';
		}
		else
		{

			$num_predict =  $this->seotraffic_m->predict_traffic($ga_data_result, $day, $dateEnfOfMonth, $kpi, '21313');

			if($kpi == 0)
				$percent = 0;
			else
				$percent =  ($ga_data_result / $kpi) * 100;

			$percent_predict =  $this->seotraffic_m->predict_traffic($ga_data_result, $day, $dateEnfOfMonth, $kpi);

			$visit_day = ($num_predict - $kpi) / $day_remain;

		//set progress color
			if($percent_predict >= 50 && $percent_predict < 80)
			{
				$progress_color = 'progress-bar-yellow';
			} 
			else if($percent_predict >= 80 && $percent_predict < 90)
			{
				$progress_color = 'progress-bar-aqua';
			}
			else if($percent_predict > 90)
			{
				$progress_color = 'progress-bar-green';
			}
			else
			{
				$progress_color = 'progress-bar-red';
			}

		// $progress_color = ($percent_predict >= 100) ? 'progress-bar-aqua' : 'progress-bar-red';

			$visit_day = numberformat($visit_day);
		// $ga_data_result = numberformat($ga_data_result);

			if($ga_data_result == 0)
				$visit_avg = 0;
			else
				$visit_avg = $kpi / $dateEnfOfMonth;

			$visit_avg = numberformat($visit_avg,2);
			$num_must = ($kpi - $ga_data_result) / $day_remain;
			$visit_day = $num_must - $visit_avg;
			$visit_day = numberformat($visit_day);

			$txt_visit_day = ($percent_predict >= 100 && $percent > 0) ? '<span type="button" class="">Dự đoán đạt</span>' : 
			'<span class="">Thiếu: '.($visit_day).' visit/ngày so với KPI <br> Mỗi ngày cần: '.numberformat($num_must).' visit</span>';

			$g = '<div class="progress-group">
			<span class="progress-text" data-toggle="tooltip" title="% thực hiện hiện tại">'.numberformat($percent,2).'%'.'</span>
			<span class="progress-number"><b><span data-toggle="tooltip" title="Visit đã thực hiện">'.numberformat($ga_data_result).'</span></b>/<span data-toggle="tooltip" title="KPI">'.numberformat($kpi).'</span></span>
			<div class="progress sm">
				<div class="progress-bar '.$progress_color.' progress-bar-striped" style="width: '.$percent.'%"></div>
			</div>
		</div>';

		$g2 = '<div class="progress-group">
		<span class="progress-text" data-toggle="tooltip" title="% dự kiến đạt cuối tháng">'.numberformat($percent_predict,2).'%'.'</span>
		<span class="progress-number"><b><span data-toggle="tooltip" title="Visit dự kiến đạt cuối tháng">'.numberformat($num_predict).'</span></b>/<span data-toggle="tooltip" title="KPI">'.numberformat($kpi).'</span></span>
		<div class="progress sm">
			<div class="progress-bar '.$progress_color.' progress-bar-striped" style="width: '.$percent_predict.'%"></div>
		</div>
	</div>';
}


$seotraffic_type = $this->seotraffic_report_m->get_ga_label($seotraffic_type);
$this->table->add_row(array(
	'<span data-toggle="tooltip" title="#'.$term->term_id.'">'.(++$i).'</span>',
	anchor(admin_url().'seotraffic/overview/'.$term->term_id, $term->term_name, 'data-toggle="tooltip" title="Thống kê chi tiết '.$term->term_name.'"'),
	$g, 
	$txt_visit_day,
	$g2,
	'Loại: <b>'.$seotraffic_type.'</b><br>Visit/ngày: <b>'.$visit_avg.'</b>', 
		// human_timing("",strtotime('2016-03-01'), 'nữa')

	));
}
}

echo $this->table->generate();

?>