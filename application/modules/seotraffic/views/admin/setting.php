<?php
echo $this->admin_form->form_open();

echo $this->admin_form->dropdown('Loại','meta[seotraffic_type]', array( 'ga:sessions' => 'Tổng visit', 'Organic Search'=>'Organic Search'), $this->termmeta_m->get_meta_value($term_id,'seotraffic_type'));

echo $this->admin_form->dropdown('GA Table ID','meta[ga_profile_id]', $ga_accounts, $this->termmeta_m->get_meta($term_id,'ga_profile_id',TRUE, TRUE),'',array('addon_end'=>'<span class="btn btn-xs btn-danger" id="btn-ajax-reload"><i class="fa fa-refresh"></i></span>'));

echo $this->admin_form->input('Report Key','meta[report_key]',$this->termmeta_m->get_meta($term_id,'report_key',TRUE, TRUE),'Key này dùng để tạo báo cáo, nếu thay đổi báo cáo cũ sẽ bị lỗi khi xem');
echo $this->admin_form->submit('submit','Lưu lại');
echo $this->admin_form->form_close();
?>

<script type="text/javascript">
  $(function(){
    $.fn.editable.defaults.url = '<?php echo admin_url();?>seotraffic/ajax_kpi/<?php echo $term_id;?>'; 
    $('.editable').editable({
     type: 'text',
     kpidate: 1,
     name: 'change-kpi',
     validate: function(value) {
      if($.isNumeric(value) != true) {
        return 'Vui lòng nhập KPI là số';
      }
      if(value < 0) {
        return 'Vui lòng nhập KPI có giá trị > 0';
      }
    },
    params: function(params) {
      // params.a = $(this).data('title');
      params.a = 1;
      return params;
    },
    success: function(response, newValue) {
      if(!response.success) return response.msg;
    },
    defaultValue: 0
  });
    $('#btn-ajax-reload').click(function(){
      var data2 = [{ id: 0, text: 'enhancement' }, { id: 1, text: 'bug' }, { id: 2, text: 'duplicate' }, { id: 3, text: 'invalid' }, { id: 4, text: 'wontfix' }];
      $.ajax({url: '<?php echo module_url();?>ajax/ga-reload',dataType: 'json', success: function(result){
            // $('form > .form-group').eq(1).html(result);
            $('form > .form-group  select').eq(1).select2({
               allowClear: true,
              data: result
            });

            $('form > .form-group .select2-container').eq(1).css('width','100%');
          }});
    });
    // To make Pace works on Ajax calls
    $(document).ajaxStart(function() { Pace.restart(); });
    $('.ajax').click(function(){
      $.ajax({url: '/ajax/<?php echo $term_id;?>/check_connect_ga', success: function(result){
        $('.ajax-content').html('<hr>Ajax Request Completed !');
      }});
    });
  });
</script>