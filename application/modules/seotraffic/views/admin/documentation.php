
<div class="content body">
<?php for($i=0; $i<20; $i++): ;?>
<section id="downloadx-<?php echo $i;?>">
  <h2 class="page-header"><a href="#downloadx-<?php echo $i;?>">Introduction <?php echo $i;?></a></h2>
  <p class="lead">
    <b>AdminLTE</b> is a popular open source WebApp template for admin dashboards and control panels.
    It is a responsive HTML template that is based on the CSS framework Bootstrap 3.
    It utilizes all of the Bootstrap components in its design and re-styles many
    commonly used plugins to create a consistent design that can be used as a user
    interface for backend applications. AdminLTE is based on a modular design, which
    allows it to be easily customized and built upon. This documentation will guide you through
    installing the template and exploring the various components that are bundled with the template.
  </p>
</section><!-- /#introduction -->
<?php endfor;?>
 </div>
<!-- ============================================================= -->

