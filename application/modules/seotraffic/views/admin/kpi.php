<div class="row">
  <div class="col-md-12">
    <?php 
    echo $this->template->kpi_backlink->view('kpi_backlink');
    ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <?php 
    echo $this->template->kpi_backlink_keyword->view('kpi_keyword');
    ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <!-- Custom Tabs -->
    <?php

    $this->table->set_caption('KPI');
    $this->table->set_heading(array('Tháng', 'KPI'));

    foreach ($date_contract as $key => $date) 
    {
      $cdate = my_date($key,'m/Y');
      $val = isset($kpis[$date]) ? $kpis[$date] : '';
      $text = $val;

      if($text == '' || $time_start >= $time)
      $text = '<a href="#" class="editable" data-type="text" data-pk="'.$date.'" data-placement="right" data-placeholder="Required" data-title="Điền KPI của tháng '.$cdate.'"">'.$val.'</a>';

      $this->table->add_row($cdate, $text);
    }
    
    echo $this->table->generate();
    // echo $this->admin_form->form_open();

    // echo $this->admin_form->input('1231312','111','5555');
    // echo $this->admin_form->submit('submit','Lưu lại');
    // echo $this->admin_form->form_close();
    ?>
    <!-- nav-tabs-custom -->
</div>
</div>
<!-- /.col -->

<script type="text/javascript">
  $(function(){
    $.fn.editable.defaults.url = '<?php echo admin_url();?>seotraffic/ajax_kpi/<?php echo $edit_id;?>'; 
    $('.editable').editable({
     type: 'text',
     kpidate: 1,
     name: 'change-kpi',
     validate: function(value) {
      if($.isNumeric(value) != true) {
        return 'Vui lòng nhập KPI là số';
      }
      if(value < 0) {
        return 'Vui lòng nhập KPI có giá trị > 0';
      }
    },
    params: function(params) {
      // params.a = $(this).data('title');
      params.a = 1;
      return params;
    },
    success: function(response, newValue) {
      if(!response.success) return response.msg;
    },
    defaultValue: 0
  });
  });
</script>