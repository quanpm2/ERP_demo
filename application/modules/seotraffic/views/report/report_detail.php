<div class="col-md-12 result">
    <div class="" >
      <h4 class="text-center">SỐ LiỆU CHI TiẾT</h4>
      <div class="row head col-md-12">
        <div class="col-md-1">Ngày</div>
        <?php foreach($ga['headers'] as $header) { ?>
        <div class="col-md-1 col-md-1 text-center"><?php echo date('d/m', strtotime($header));?></div>
        <?php } ?>
        <div class="col-md-1 col-md-1">Tổng</div>
      </div>
      <div class="row bg-warning col-md-12">
        <div class="col-md-1">Thứ</div>
        <?php
        foreach($ga['headers'] as $header) { ?>
        <div class="col-md-1 col-md-1 text-center"><?php echo $this->mdate->week_name(strtotime($header), array('CN','T2','T3','T4','T5','T6','T7'));?></div>
        <?php } ?>
        <div class="col-md-3 col-md-1">&nbsp;</div>
      </div>
      <div class="result row col-md-12">
      <?php 
     
      foreach($ga['total'] as $key=> $total) { ?>
        <div class="row">
          <div class="col-md-1 col-md-2"><?php echo (isset($ga_label[$key]) ? $ga_label[$key] : $key);?></div>
          <?php foreach($ga['headers'] as $header) {
            $val = isset($ga['results'][$header][$key]) ? $ga['results'][$header][$key] : 0;
            ?>
          <div class="col-md-1 col-md-1 text-center"><?php echo $val;?></div>
          <?php } ?> 
          <div class="col-md-1 col-md-1 text-center"><?php echo $total;?></div>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="clear10"></div>
  </div>