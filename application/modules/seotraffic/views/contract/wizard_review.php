<div class="col-md-12" id="review-partial">
<h2 class="page-header">
	<i class="fa fa-globe"></i> Mã hợp đồng : <?php echo $this->termmeta_m->get_meta_value($edit->term_id, 'contract_code');?> 
	<small class="pull-right">Ngày tạo : <?php echo my_date(time(), 'd-m-Y');?></small>
</h2>
<div class="row">
	<div class="col-xs-6">
      <p class="lead">Thông tin khách hàng</p>
      <div class="table-responsive">
      <?php
      echo
      $this->table
        ->clear()
        ->add_row(array('Người đại diện',force_var(get_term_meta_value($edit->term_id, 'representative_gender'),'Bà','Ông').' '.
            get_term_meta_value($edit->term_id, 'representative_name')))
        ->add_row(array('Email',force_var(get_term_meta_value($edit->term_id,'representative_email'),'Chưa cập nhật')))
        ->add_row(array('Địa chỉ',force_var(get_term_meta_value($edit->term_id,'representative_address'),'Chưa cập nhật')))
        ->add_row(array('Số điện thoại',force_var(get_term_meta_value($edit->term_id,'representative_phone'),'Chưa cập nhật')))
        ->add_row(array('Chức vụ',force_var($edit->extra['representative_position'],'Chưa cập nhật')))
        ->add_row(array('Mã Số thuế',force_var($edit->extra['customer_tax'],'Chưa cập nhật')))
        ->add_row(array('Thời gian thực hiện',force_var(get_term_meta_value($edit->term_id,'contract_daterange'),'Chưa cập nhật')))
        ->generate();
      ?>
      </div>
    </div>
    <!-- /.col -->
    <div class="col-xs-6">
      	<p class="lead">Thông số SEO Chặng</p>
		<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
    Loại : <?php echo $this->termmeta_m->get_meta_value($edit->term_id, 'seotraffic_type');?> <br/>
		GA Table ID : <?php echo $this->termmeta_m->get_meta_value($edit->term_id, 'ga_profile_id');?> <br/>
		</p>
    </div>
    <!-- /.col -->    
  </div>
</div>
<?php

echo '<div class="clearfix"></div>',
$this->admin_form->form_open(),
$this->admin_form->hidden('','edit[term_status]', 'waitingforapprove'),
form_hidden('edit[term_id]',$edit->term_id),
$this->admin_form->submit('','confirm_step_finish','confirm_step_finish','', array('style'=>'display:none;','id'=>'confirm_step_finish')),
$this->admin_form->form_close();
?>