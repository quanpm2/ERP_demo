<div class="col-md-12" id="service_tab">
<?php
echo $this->admin_form->form_open();

echo form_hidden('edit[term_id]',$edit->term_id);

echo $this->admin_form->dropdown('Loại','edit[meta][seotraffic_type]', array( 'ga:sessions' => 'Tổng visit', 'Organic Search'=>'Organic Search'), $this->termmeta_m->get_meta_value($edit->term_id,'seotraffic_type'));

echo $this->admin_form->dropdown('GA Table ID','edit[meta][ga_profile_id]', @$ga_accounts, $this->termmeta_m->get_meta_value($edit->term_id,'ga_profile_id'));

echo $this->admin_form->submit('','confirm_step_service','confirm_step_service','',array('style'=>'display:none;','id'=>'confirm_step_service'));

echo $this->admin_form->form_close();
?>

</div>
<script type="text/javascript">
	$(".select2").select2();
	$('#service_tab .select2-container').css('width','100%');
</script>
