<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

class MY_Loader extends MX_Loader {

	private $_package_module_loaded = array();
	public function package($name = '')
	{
		$name = ucfirst($name);
		$alias = strtolower($name);
		$class_name = $name.'_Package';
		if (isset($this->_package_module_loaded[$alias]))	
		{
			return $this->_package_module_loaded[$alias];
		}
		$file = APPPATH.'modules/'.$alias.'/'.$name.'.php';
		if(file_exists($file))
		{
			require_once($file);
			$class = new $class_name();
			$this->_package_module_loaded[$alias] = $class;
			return $class;
		}
		return FALSE;
	}

	public function get_modules_loaded($module = ''){

		if(isset($this->_package_module_loaded[$module])){
			
			return $this->_package_module_loaded[$module];
		}
		
		return $this->_package_module_loaded;
	}
}
