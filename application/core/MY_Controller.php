<?php
require APPPATH."third_party/MX/Controller.php";
/**
 * The base controller which is used by the Front and the Admin controllers
 */
class MY_Controller extends MX_Controller {

	 /**
     * @var array Stores a number of items to 'autoload' when the class
     * constructor runs. This allows any controller to easily set items which
     * should always be loaded, but not to force the entire application to
     * autoload it through the config/autoload file.
     */
    public $autoload = array(
        'libraries' => array(),
        'helpers'   => array(),
        'models'    => array(),
    );

	function __construct()
	{
		parent::__construct();
        require_once(APPPATH.'third_party/cms-core/libraries/Core.php');
        require_once(APPPATH.'third_party/cms-core/libraries/Package.php');
        require_once(APPPATH.'third_party/cms-core/libraries/Post_type.php');
        // $this->load->model('meta_m');
        // require_once(APPPATH.'third_party/cms-core/models/Meta_m.php');
		// Handle any autoloading here...
        // $this->autoload['models'][] = 'meta_m';
        $this->autoload['libraries'][] = 'hook';
        $this->autoload['models'][] = 'log_m';
        $this->autoload['models'][] = 'logmeta_m';
        $this->autoload_classes();
        $this->load->add_package_path(APPPATH.'third_party/erp-core/');
		// $this->load->model('init_m');
     
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
	}


    /**
     * Autoloads any class-specific files that are needed throughout the
     * controller. This is often used by base controllers, but can easily be
     * used to autoload models, etc.
     *
     * @return void
     */
    public function autoload_classes()
    {
        // Using ! empty() because count() returns 1 for certain error conditions

        if (! empty($this->autoload['libraries'])
            && is_array($this->autoload['libraries'])
        ) {
            foreach ($this->autoload['libraries'] as $library) {
                $this->load->library($library);
            }
        }

        if (! empty($this->autoload['helpers'])
            && is_array($this->autoload['helpers'])
        ) {
            foreach ($this->autoload['helpers'] as $helper) {
                $this->load->helper($helper);
            }
        }

        if (! empty($this->autoload['models'])
            && is_array($this->autoload['models'])
        ) {
            foreach ($this->autoload['models'] as $model) {
                $this->load->model($model);
            }
        }
    }

    /**
     * Check token to access this class
     *
     * @return     bool  TRUE If token is valid , otherwise return FALSE
     */
    function check_token()
    {
        $token = $this->input->get('token'); 
        if(empty($token)) return FALSE;

        $valid_token = $this->build_token();
        return $token ==  $valid_token;
    }

    function build_token()
    {
        return (base64_encode(md5($this->mdate->startOfDay()).md5($this->mdate->startOfDay('yesterday'))));
    }
}
