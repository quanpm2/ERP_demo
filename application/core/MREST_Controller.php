<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
use \Firebase\JWT\JWT;
use Firebase\JWT\Key;

class MREST_Controller extends REST_Controller {


	public $data 		= array();
	public $autoload 	= array(
		'libraries' => array(
			'messages',
			'admin_ui',
			'admin_form',
			'table',
			'mdate'
		),
		'helpers'   => array(),
		'models'    => array(
			'term_m', 
			'term_posts_m', 
			'termmeta_m', 
			'option_m', 
			'post_m', 
			'postmeta_m',
			'usermeta_m',
			'role_m',
			'permission_m',
			'staffs/admin_m'
			),
		);

	/**
	 * CONSTRUCTION
	 *
	 * @param      string  $config  The configuration
	 */
	function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->autoload_classes();

		header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") die();

		if($this->input->method() === 'options') parent::response('ok', parent::HTTP_OK);

		$_class 	= $this->router->fetch_class();
		$_method 	= $this->router->fetch_method();
		if( ! $this->admin_m->checkLogin() && strtolower($_class) != 'auth' && $_method != 'login') $this->auth();

		require_once(APPPATH.'third_party/cms-core/libraries/Core.php');
        require_once(APPPATH.'third_party/cms-core/libraries/Package.php');
        require_once(APPPATH.'third_party/cms-core/libraries/Post_type.php');
        $this->load->add_package_path(APPPATH.'third_party/erp-core/');
	}

	/**
	 * Authorization check Token
	 * @return void
	 */
	protected function auth()
    {
    	$headers = $this->input->get_request_header('Authorization');
    	$headers = $headers ? explode(' ', $headers) : '';
    	$token = !empty($headers) ? $headers[1] : '';
    	if (empty($token)) {
    		$token = $this->input->get('token');
    	}
    	$token 		= $token ?: parent::get('token', TRUE);
    	$decoded 	= NULL;

		if(empty($token)) 
		{
			$this->responseHandler(null, parent::HTTP_UNAUTHORIZED, 'error', parent::HTTP_UNAUTHORIZED);
		}
		
		try
		{
			$decoded = JWT::decode($token, new Key(getenv('JWT_SECRET'), getenv('JWT_ALG')));

			$this->admin_m->setLogin($decoded);
			$this->admin_m->setInfo();
			return true;
		}
		catch (Exception $e)
		{
			$invalid = ['status' => $e->getMessage()]; //Respon if credential invalid
			parent::response($invalid, parent::HTTP_UNAUTHORIZED);//401
		}

		$this->load->model('credentials_m');
		if( ! $this->credentials_m->checkKey($token)) parent::response(['status'=>FALSE], parent::HTTP_UNAUTHORIZED);

		$this->admin_m->setLogin($decoded);
		$this->admin_m->setInfo();
    }
	
	/**
     * Autoloads any class-specific files that are needed throughout the
     * controller. This is often used by base controllers, but can easily be
     * used to autoload models, etc.
     *
     * @return void
     */
    public function autoload_classes()
    {
        if ( ! empty($this->autoload['libraries']) && is_array($this->autoload['libraries']))
        {
            $this->load->library($this->autoload['libraries']);
        }

        if ( ! empty($this->autoload['helpers']) && is_array($this->autoload['helpers']))
        {
            $this->load->helper($this->autoload['helpers']);
        }

        if ( ! empty($this->autoload['models']) && is_array($this->autoload['models']))
        {
            $this->load->model($this->autoload['models']);
        }
    }

	/**
	 * 
	 * Cast to JSend Response 
	 * 
	 * @param mixed $data
	 * @param mixed $messageOrErrors
	 * @param string $status
	 * @param int $code
	 * 
	 * @return [type]
	 */
	public function responseHandler($data, $messageOrErrors, $status = "success", $code = 200, $extend_data = [])
	{
        $default_response = [
			'status' => $status,
			'code' => $code,
			'message' => $messageOrErrors,
			'data' => $data,
		];
        if(!empty($extend_data))
        {
            $default_response = array_merge($default_response, $extend_data);
        }
        
		parent::response($default_response);
	}
}
/* End of file MREST_Controller.php */
/* Location: ./application/core/MREST_Controller.php */