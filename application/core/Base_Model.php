<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Base_Model extends MY_Model
{
	protected $timestamps = FALSE;
	private $cache_name = NULL;
	private $cache_ttl = FALSE;
	private $_ci = NULL;
	private $cache_path = NULL;

	public $attributes 				= array();
	public $protected_attributes 	= array();

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable_attributes 	= [];
    protected $default_attributes 	= [];

    public function __construct()
    {
        parent::__construct();

	    array_unshift($this->before_create, 'fillable_attributes', 'default_attributes');
	    array_unshift($this->before_update, 'fillable_attributes');
    }

    public function default_attributes($row)
    {
    	if(empty($this->default_attributes)) return $row;
    	$row = wp_parse_args($row, $this->default_attributes);
        return $row;
    }

    public function fillable_attributes($row)
    {
    	if(empty($this->fillable_attributes)) return $row;

    	foreach ($row as $key => $value)
    	{
    		if(in_array($key, $this->fillable_attributes)) continue;

    		if(is_object($row))
    		{
    			unset($row->$key);
    			continue;
    		}

    		unset($row[$key]);
    	}
        return $row;
    }
    
	public function cache($cache_name = '', $cache_ttl = 0)
	{
		if($cache_name !== NULL)
		{
			if($this->_ci === NULL)
			{
				$this->_ci = & get_instance();
				$this->_ci->load->driver('cache', array('adapter' => 'file', 'backup' => 'file'));
				$path = $this->_ci->config->item('cache_path');
				$path = ($path == '') ? APPPATH.'cache/' : $path;
				$this->cache_path = $path;
			}
			$this->cache_name = $cache_name.'.cache';
			$this->cache_ttl = ($cache_ttl == 0) ? 31536000 : $cache_ttl;
			$this->create_dirs($this->cache_path.$cache_name);
		}
		return $this;
	}

	public function get_all()
	{
		$where = func_get_args();
		if($this->cache_name)
		{
			return $this->_create_cache(__FUNCTION__, $where);
		}
		return call_user_func_array(array('parent', __FUNCTION__), $where);
	}

	public function get_by()
	{
		$where = func_get_args();
		if($this->cache_name)
		{
			return $this->_create_cache(__FUNCTION__, $where);
		}
		return call_user_func_array(array('parent', __FUNCTION__), $where);
	}

	public function reset_cache()
	{
		$this->cache_name = NULL;
		$this->cache_ttl = FALSE;
		return $this;
	}

	private function _create_cache($method_name = '', $args = array())
	{
		$contents = $this->_ci->cache->get($this->cache_name);
		if(!$contents)
		{
			$contents = call_user_func_array(array('parent', $method_name), $args);
			$this->_ci->cache->save($this->cache_name, $contents, $this->cache_ttl);
		}
		$this->reset_cache();
		return $contents;
	}

	public function create_dirs($file =''){
		if(is_dir($file))
			return $file;

		$dir_paths = explode("/",$file);
		$dir = "";
		for($i=0; $i<count($dir_paths) -1; $i++)
		{
			$dir_path = $dir_paths[$i];
			$dir.= $dir_path."/";
			if (!is_dir($dir)) 
			{
				mkdir($dir);
				chmod($dir, 0777);
			}
		}
		return $dir;
	}

	/**
	 * Auto chunks array values if length greater than 25
	 *
	 * @param      <type>  $key     The key
	 * @param      <type>  $values  The values
	 *
	 * @return     self
	 */
	public function where_in($key = null, $values = null)
    {
    	$values = !is_array($values) ? [$values] : $values;
    	if(count($values) <= 25) return parent::where_in($key, $values);

    	parent::group_start();

    	$chunks = array_chunk($values, 25);
		foreach($chunks as $chunk) parent::or_where_in($key, $chunk);

		parent::group_end();

		return $this;
    }

    /**
	 * Auto chunks array values if length greater than 25
	 *
	 * @param      <type>  $key     The key
	 * @param      <type>  $values  The values
	 *
	 * @return     self
	 */
    public function where_not_in($key = null, $values = null)
    {
    	$values = !is_array($values) ? [$values] : $values;
    	if(count($values) <= 25) return parent::where_not_in($key, $values);

    	parent::group_start();

    	$chunks = array_chunk($values, 25);
		foreach($chunks as $chunk) parent::where_not_in($key, $chunk);

		parent::group_end();

		return $this;
    }
}