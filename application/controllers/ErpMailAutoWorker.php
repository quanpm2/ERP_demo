<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpAmqpLib\Connection\AMQPStreamConnection;

class ErpMailAutoWorker extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->config('amqps');

        $models = array(
            'contract/base_contract_m',
            'contract/contract_m',
            'contract/contract_report_m',
            'onead/onead_m',
            'oneweb/oneweb_m',
            'hosting/hosting_m',
            'domain/domain_m',
            'log_m'
        );
        $this->load->model($models);
    }

    public function handle()
    {
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        $queue = 'mail_auto.event.default';
        $connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);

        $channel = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $callback = function ($msg) {

            echo ' [x] Received ', $msg->body, "\n";

            $args = json_decode($msg->body);
            $args = wp_parse_args($args, [
                'action' => null,
                'event' => null,
                'paymentId' => null,
            ]);

            switch ($args['event']) {
                case 'onead.notice.renew':
                    $this->send_notice_renew_mail_onead($args['log_id'] ?? 0);
                    break;
                case 'oneweb.notice.renew':
                    $this->send_notice_renew_mail_oneweb($args['log_id'] ?? 0);
                    break;
                case 'hosting.notice.renew':
                    $this->send_notice_renew_mail_hosting($args['log_id'] ?? 0);
                    break;
                case 'domain.notice.renew':
                    $this->send_notice_renew_mail_domain($args['log_id'] ?? 0);
                    break;
                default:
                    # code...
                    break;
            }

            $msg->ack();
        };

        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($queue, '', false, false, false, false, $callback);

        while ($channel->is_open()) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }


    /**
     * Send notice renew mail for 1Ad service
     * 
     * @param mixed $paymentId
     * 
     * @return [type]
     */
    public function send_notice_renew_mail_onead($log_id)
    {
        $log = $this->log_m->where('log_id', $log_id)
            ->where('log_status', $this->log_m->status_pending)
            ->where('log_type', 'mail_event_onead_notice_renew')
            ->get_by();
        if (empty($log)) {
            return FALSE;
        }

        $excute_time = (int) get_log_meta_value($log->log_id, 'excute_time');
        if ($excute_time > 3) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_running]);

        $excute_time += 1;
        update_log_meta($log->log_id, 'excute_time', $excute_time);

        $contract = $this->contract_m->where('term_id', $log->term_id)->get_by();
        if (empty($contract)) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $_instance = $this->contract_m->get_contract_model($contract);
        if (!is_string($_instance)) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }
        $this->load->model($_instance, 'instance_m');

        if (!method_exists($this->instance_m, 'notice_renew_service')) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $is_send = $this->instance_m->notice_renew_service($contract);
        if (!$is_send) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_error]);

            return FALSE;
        }

        $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_success]);
        return TRUE;
    }

    /**
     * Send notice renew mail for 1Ad service
     * 
     * @param mixed $paymentId
     * 
     * @return [type]
     */
    public function send_notice_renew_mail_oneweb($log_id)
    {
        $log = $this->log_m->where('log_id', $log_id)
            ->where('log_status', $this->log_m->status_pending)
            ->where('log_type', 'mail_event_oneweb_notice_renew')
            ->get_by();
        if (empty($log)) {
            return FALSE;
        }

        $excute_time = (int) get_log_meta_value($log->log_id, 'excute_time');
        if ($excute_time > 3) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_running]);

        $excute_time += 1;
        update_log_meta($log->log_id, 'excute_time', $excute_time);

        $contract = $this->contract_m->where('term_id', $log->term_id)->get_by();
        if (empty($contract)) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $_instance = $this->contract_m->get_contract_model($contract);
        if (!is_string($_instance)) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }
        $this->load->model($_instance, 'instance_m');

        if (!method_exists($this->instance_m, 'notice_renew_service')) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $is_send = $this->instance_m->notice_renew_service($contract);
        if (!$is_send) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_error]);

            return FALSE;
        }

        $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_success]);
        return TRUE;
    }

    /**
     * Send notice renew mail for Hosting service
     * 
     * @param mixed $paymentId
     * 
     * @return [type]
     */
    public function send_notice_renew_mail_hosting($log_id)
    {
        $log = $this->log_m->where('log_id', $log_id)
            ->where('log_status', $this->log_m->status_pending)
            ->where('log_type', 'mail_event_hosting_notice_renew')
            ->get_by();
        if (empty($log)) {
            return FALSE;
        }

        $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_running]);

        $excute_time = (int) get_log_meta_value($log->log_id, 'excute_time');
        if ($excute_time > 3) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $excute_time += 1;
        update_log_meta($log->log_id, 'excute_time', $excute_time);

        $contract = $this->contract_m->where('term_id', $log->term_id)->get_by();
        if (empty($contract)) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $_instance = $this->contract_m->get_contract_model($contract);
        if (!is_string($_instance)) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }
        $this->load->model($_instance, 'instance_m');

        if (!method_exists($this->instance_m, 'notice_renew_service')) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $is_send = $this->instance_m->notice_renew_service($contract);
        if (!$is_send) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_error]);

            return FALSE;
        }

        $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_success]);
        return TRUE;
    }

    /**
     * Send notice renew mail for Domain service
     * 
     * @param mixed $paymentId
     * 
     * @return [type]
     */
    public function send_notice_renew_mail_domain($log_id)
    {
        $log = $this->log_m->where('log_id', $log_id)
            ->where('log_status', $this->log_m->status_pending)
            ->where('log_type', 'mail_event_domain_notice_renew')
            ->get_by();
        if (empty($log)) {
            return FALSE;
        }

        $excute_time = (int) get_log_meta_value($log->log_id, 'excute_time');
        if ($excute_time > 3) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_running]);

        $excute_time += 1;
        update_log_meta($log->log_id, 'excute_time', $excute_time);

        $contract = $this->contract_m->where('term_id', $log->term_id)->get_by();
        if (empty($contract)) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $_instance = $this->contract_m->get_contract_model($contract);
        if (!is_string($_instance)) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }
        $this->load->model($_instance, 'instance_m');

        if (!method_exists($this->instance_m, 'notice_renew_service')) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_finish]);

            return FALSE;
        }

        $is_send = $this->instance_m->notice_renew_service($contract);
        if (!$is_send) {
            $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_error]);

            return FALSE;
        }

        $this->log_m->update($log->log_id, ['log_status' => $this->log_m->status_success]);
        return TRUE;
    }
}
/* End of file ErpNotificationsWorker.php */
/* Location: ./application/controllers/ErpNotificationsWorker.php */