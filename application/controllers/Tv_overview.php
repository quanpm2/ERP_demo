<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tv_overview extends Admin_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('contract/contract_m');
		$this->load->model('staffs/admin_m');
		$this->load->library('admin_ui');
		$this->load->library('admin_form');
		$this->load->config('googleads/googleads');
		$this->load->config('contract/contract');
	}

	public function index()
	{
		$data = array();
		$data = array_merge($data,$this->fetch_adsplus(),$this->fetch_webgeneral());
		$this->load->view('frontend/tv_overview/index',$data);
	}

	public function topSaleAlltime(){

		$data = array('staff_map_term'=>array());
		$start_time = $this->mdate->startOfDay(date('1-1-Y'));
		$this->load->config('staffs/group');
		$staff_business = $this->admin_m
			->set_role($this->config->item('salesexecutive','groups'))
			->set_get_active()
			->as_array()
			->get_many_by();
		$staff_business = array_column($staff_business,'user_email','user_id');
		if(!empty($staff_business))
			foreach ($staff_business as $key => $value)
				$data['staff_map_term'][$key] = 0;

		$services = $this->config->item('services');
		$terms = $this->contract_m
			->select('term.term_id,,term.term_type')
			->where_in('term.term_status',array('publish'))
			->where_in('term.term_type',array_keys($this->config->item('services')))
			->get_many_by();

		$this->load->model('webgeneral/webgeneral_config_m');
		if(!empty($terms))
			foreach ($terms as $term){

				$term->contract_begin = get_term_meta_value($term->term_id, 'contract_begin');

				if($term->contract_begin < $start_time) continue;

				if($term->term_type == 'webgeneral'){
					$service_package = get_term_meta_value($term->term_id,'service_package');
					if($service_package == 'startup') continue;
				}

				$term->staff_business = $this->termmeta_m->get_meta_value($term->term_id, 'staff_business');

				if($term->staff_business){	
					if(empty($data['staff_map_term'][$term->staff_business])) {
						$data['staff_map_term'][$term->staff_business] = 1;
						continue;
					}

					$data['staff_map_term'][$term->staff_business]++;
				}
			}

		# remove nghiatv@sccom.vn user
		unset($data['staff_map_term'][214]);

		$data['series_data'] = array();

		foreach($data['staff_map_term'] as $key => $value){

			$email = $this->admin_m->get_field_by_id($key,'user_email');
			$email = substr($email,0,strpos($email, '@'));
			$data['series_data'][] = array( 'name'=>$email, 'y'=> $value);
		}

		$data['series_data'] = json_encode($data['series_data']);

		$this->load->view('frontend/tv_overview/topsalealltime',$data);
	}

	public function top_sale(){

		$contract_status = $this->config->item('contract_status');
		if(!empty($contract_status)){
			unset($contract_status['draft']);
			unset($contract_status['waitingforapprove']);
			unset($contract_status['pending']);
			unset($contract_status['remove']);
		}

		$start_time = $this->mdate->startOfDay(date('1-m-Y'));
		$contract_status = array_keys($contract_status);
		$terms = $this->contract_m
		->select('term.term_id,term.term_type')
		->where_in('term.term_type', array_keys($this->config->item('services')))
		->where_in('term.term_status', $contract_status)
		->get_many_by();

		$data = array();
		$data['staff_map_term'] = array();
		$this->load->config('staffs/group');
		$staff_business = $this->admin_m
		->set_role($this->config->item('salesexecutive','groups'))
		->set_get_active()
		->as_array()
		->get_many_by();

		$staff_business = array_column($staff_business,'user_email','user_id');

		// remove nghiatv@sccom.vn
		unset($staff_business[214]);
		unset($staff_business[155]);

		if(!empty($staff_business))
			foreach ($staff_business as $key => $value)
				$data['staff_map_term'][$key] = 0;

		foreach ($terms as $term){

			$user_id = $this->termmeta_m->get_meta_value($term->term_id, 'staff_business');

			//sale bị khóa tk nhưng có hợp đồng vẫn không được tính
			if(!array_key_exists($user_id, $staff_business)) continue;


			$term->start_service_time = get_term_meta_value($term->term_id,'start_service_time');
			if($term->start_service_time < $start_time) continue;

			if($term->term_type == 'webgeneral'){
				$service_package = get_term_meta_value($term->term_id,'service_package');
				if($service_package == 'startup') 
					continue;
			}

			

			if($term->staff_business = $user_id){

				if(empty($data['staff_map_term'][$term->staff_business])) {

					$data['staff_map_term'][$term->staff_business] = 1;

					continue;
				}

				$data['staff_map_term'][$term->staff_business]++;
			}
		}

		//luôn luôn để A Hải đứng vị trí 1
		$haitm_val = (isset($data['staff_map_term'][18])) ? $data['staff_map_term'][18] : 0;
		$data['staff_map_term'][18] = 999999999;// gán giá trị tạm để sort luôn đứng 1

		arsort($data['staff_map_term']);
		$data['staff_map_term'][18] = $haitm_val; //cập nhật lại giá trị

		$data['series_data'] = array();
		foreach ($data['staff_map_term'] as $key => $value){

			$email = $this->admin_m->get_field_by_id($key,'user_email');
			$email = substr($email,0,strpos($email, '@'));

			$img_src = '/template/frontend/tv_overview/images/avatar/'.$email.'.png';
			if(!file_exists('.'.$img_src))
			{
				$img_src = '/template/frontend/tv_overview/images/avatar/unassigned.png';
			}
			
			$data['images'][] = base_url($img_src);
			$data['series_data'][] = array( 'name'=>$email, 'y'=> $value);
		}

		$data['series_data'] = json_encode($data['series_data']);
		$data['images'] = json_encode($data['images']);
		
		$this->load->view('frontend/tv_overview/top_sale',$data);
	}

	private function fetch_adsplus()
	{
		$result = array();
		$terms = $this->contract_m
		->where('term.term_type', 'google-ads')
		->where_in('term.term_status', array_keys($this->config->item('status','googleads')))
		->get_many_by();

		$start_time = $this->mdate->startOfDay(date('1-m-Y'));
		$terms_lates = 0;
		foreach ($terms as $term){
			$term->started_service = get_term_meta_value($term->term_id,'start_service_time');
			if($term->started_service < $start_time) continue;
			++$terms_lates;
		}

		$result['adsplus_count'] = count($terms);
		$result['adsplus_new_count'] = $terms_lates;

		return $result;
	}

	private function fetch_webgeneral()
	{
		$this->load->model('webgeneral/webgeneral_m');

		$result = array();

		$terms = $this->contract_m
		->where('term.term_type', $this->webgeneral_m->term_type)
		->where_in('term.term_status', 'publish')
		->get_many_by();

		$terms_lates = 0;
		$start_time = $this->mdate->startOfDay(date('1-m-Y'));
		foreach ($terms as $key => $term){
			#ignore startup contract
			$service_package = get_term_meta_value($term->term_id,'service_package');
			if($service_package == 'startup') {
				unset($terms[$key]);
				continue;
			}

			$term->start_service_time = get_term_meta_value($term->term_id,'start_service_time');
			if($term->start_service_time < $start_time) continue;

			++$terms_lates;
		}

		$result['webgeneral_count'] = count($terms);
		$result['webgeneral_new_count'] = $terms_lates;
		return $result;
	}

	function listcontractsale($u_id = 0)
	{
		$contract_status = $this->config->item('contract_status');
		if(!empty($contract_status)){
			unset($contract_status['draft']);
			unset($contract_status['waitingforapprove']);
			unset($contract_status['pending']);
			unset($contract_status['remove']);
		}

		$start_time = $this->mdate->startOfDay(date('1-m-Y'));
		$contract_status = array_keys($contract_status);
		$terms = $this->contract_m
		->select('term.term_id,term.term_type, term_name, term_status')
		->where_in('term.term_type', array_keys($this->config->item('services')))
		->where_in('term.term_status', $contract_status)
		->get_many_by();

		$data = array();
		$data['staff_map_term'] = array();
		$data['staff_terms'] = array();
		$this->load->config('staffs/group');
		$staff_business = $this->admin_m
		->set_role($this->config->item('salesexecutive','groups'))
		->set_get_active()
		->as_array()
		->get_many_by();

		$staff_business = array_column($staff_business,'user_email','user_id');

		// remove nghiatv@sccom.vn
		unset($staff_business[214]);


		if(!empty($staff_business))
			foreach ($staff_business as $key => $value)
				$data['staff_map_term'][$key] = 0;

		foreach ($terms as $term){

			$user_id = $this->termmeta_m->get_meta_value($term->term_id, 'staff_business');

			//sale bị khóa tk nhưng có hợp đồng vẫn không được tính
			if(!array_key_exists($user_id, $staff_business)) continue;


			$term->start_service_time = get_term_meta_value($term->term_id,'start_service_time');
			if($term->start_service_time < $start_time) continue;

			if($term->term_type == 'webgeneral'){
				$service_package = get_term_meta_value($term->term_id,'service_package');
				if($service_package == 'startup') 
					continue;
			}

			

			if($term->staff_business = $user_id){

				if(empty($data['staff_map_term'][$term->staff_business])) {

					$data['staff_terms'][$term->staff_business][] = $term;
					$data['staff_map_term'][$term->staff_business] = 1;

					continue;
				}

				$data['staff_map_term'][$term->staff_business]++;
				unset($term->term_id);

				$term->start_service_time = date('d/m/Y', $term->start_service_time);
				$term->staff_business_name = $this->admin_m->get_field_by_id($term->staff_business, 'display_name');
				$data['staff_terms'][$term->staff_business][] = $term;
			}
		}

		if($u_id > 0 && isset($data['staff_map_term'][$u_id]))
		{
			echo 'Total: '.$data['staff_map_term'][$u_id].'<br>';
			prd($data['staff_terms'][$u_id]);
		}
		prd($data['staff_terms']);
	}

	function tt()
	{
		$this->load->model('webgeneral/webgeneral_m');
		$this->load->model('webgeneral/webgeneral_config_m');

		$result = array();

		$terms = $this->contract_m
		->where('term.term_type', $this->webgeneral_m->term_type)
		->where_in('term.term_status', 'publish')
		->get_many_by();

		$terms_lates = 0;
		$start_time = $this->mdate->startOfDay(date('1-m-Y'));
		$i =1;
		$this->load->library('table');
		foreach ($terms as $key => $term){
			#ignore startup contract
			$service_package = get_term_meta_value($term->term_id,'service_package');
			if($service_package == 'startup') {
				unset($terms[$key]);
				continue;
			}

			// if($term->start_service_time < $start_time) continue;

			$term->start_service_time = get_term_meta_value($term->term_id,'start_service_time');
			++$terms_lates;

			$name = $this->webgeneral_config_m->get_package_name($term->term_id);

				$term->staff_business = $this->termmeta_m->get_meta_value($term->term_id, 'staff_business');

				$term->staff_business_name = $this->admin_m->get_field_by_id($term->staff_business, 'display_name');

			$this->table->add_row(($i++), $term->term_name,$name, $term->staff_business_name);
		}

		
		$this->table->set_heading('#', 'Website', 'Gói sử dụng', 'Kinh doanh');
		echo $this->table->generate();
 
		$result['webgeneral_count'] = count($terms);
		$result['webgeneral_new_count'] = $terms_lates;
		return $result;
	}
}