<?php

use Notifications\Erp\Reports\Ads\Googleads\CustomerDailyClicks\Sms;

defined('BASEPATH') OR exit('No direct script access allowed');

class Scripts extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();
        $this->load->config('amqps');
        $this->load->model('googleads/googleads_log_m');
        $this->load->model('googleads/googleads_m');
        $this->load->model('contract/contract_m');
        $this->load->model('contract/receipt_m');
        $this->load->model('term_posts_m');
	}

    public function daily_sms_test()
    {
        $event      = new Sms(['contract' => 50386]);
        $result     = $event->send();
    }
}
/* End of file Scripts.php */
/* Location: ./application/controllers/tests/Scripts.php */