<?php

use Notifications\Erp\Reports\Ads\Googleads\CustomerDailyClicks\Sms;

defined('BASEPATH') OR exit('No direct script access allowed');

class Facebookads_m_tests extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();

        $this->load->model('contract/contract_m');
        $this->load->model('contract/receipt_m');
        $this->load->model('term_posts_m');
	}

    public function test_get_insights()
    {
        $this->load->model('facebookads/facebookads_m');
        
        // FACEBOOK-ADS/36415/0121/FACEBOOK.COM/30SHINEOFFICIAL
        $contractId = 36415;
        $this->facebookads_m->set_contract($contractId);
        
        $result = $this->facebookads_m->get_insights([
            // 'summary' => true, 
            'fields' => [
                'spend',
                'clicks',
                'impressions'
            ]
        ]);

        dd($result);
    }
}
/* End of file Scripts.php */
/* Location: ./application/controllers/tests/Scripts.php */