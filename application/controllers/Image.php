<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends Public_Controller {

	// $folder = '';
	function index()
	{
		// basic info
        $path = $this->uri->uri_string();

        // load the allowed image sizes
        $this->load->config("images");
        $folder_root = $this->config->item("image_folder_root");
        $folder_cache = $this->config->item("image_folder_cache");
        if($path)
        {
        	$path = explode('/', $path);
        	unset($path[0]);
        	$cache_path = $folder_cache.implode('/', $path);
        	$path = $folder_root.implode('/', $path);
        }

        $pathinfo = pathinfo($path);
        $size = explode("-", $pathinfo["filename"]);
        $size = end($size);
        $original = $pathinfo["dirname"] . "/" . str_ireplace("-" . $size, "", $pathinfo["basename"]);
        
        // original image not found, show 404
        if (!file_exists($original)) {
            show_404($original);
        }
        
        $sizes = $this->config->item("image_sizes");
        $allowed = FALSE;
        
        if (stripos($size, "x") !== FALSE) {
            // dimensions are provided as size
            @list($width, $height) = explode("x", $size);
            
            // security check, to avoid users requesting random sizes
            foreach ($sizes as $s) {
                if ($width == $s[0] && $height == $s[1]) {
                    $allowed = TRUE;
                    break;
                }
            }
        } else if (isset($sizes[$size])) {
            // optional, the preset is provided instead of the dimensions
            // NOTE: the controller will be executed EVERY time you request the image this way
            @list($width, $height) = $sizes[$size];
            $allowed = TRUE;
            
            // set the correct output path
            $path = str_ireplace($size, $width . "x" . $height, $path);
        }
         
        // only continue with a valid width and height
        if ($allowed && $width >= 0 && $height >= 0) {

            $this->create_dirs($cache_path);
            // initialize library
            $config["source_image"] = $original;
            $config['new_image'] = $cache_path;
            $config["width"] = $width;
            $config["height"] = $height;
            $config["dynamic_output"] = FALSE; // always save as cache
            
            $this->load->library('image_lib');
			$this->image_lib->initialize($config);
			
            $this->image_lib->fit();
        }
        else
        {
        	$this->output->set_status_header('404'); 
        	exit(0);
        }

        // check if the resulting image exists, else show the original
        if (file_exists($cache_path)) {
            $output = $cache_path;
        } else {
            $output = $original;
        }

        $info = getimagesize($output);
       
        // output the image
        header("Content-Disposition: filename={$output};");
        header("Content-Type: {$info["mime"]}");
        header('Content-Transfer-Encoding: binary');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');
        
        readfile($output);
	}

    private function create_dirs($file ='')
    {
        $dir_paths = explode("/",$file);
        $dir = "";
        for($i=0; $i<count($dir_paths) -1; $i++){
            $dir_path = $dir_paths[$i];
            $dir.= $dir_path."/";
            if (!is_dir($dir)) {
                mkdir($dir);
                chmod($dir, 0777);
            }
        }
        return $dir;
    }
}
