<?php defined('BASEPATH') OR exit('No direct script access allowed');

use PhpAmqpLib\Connection\AMQPStreamConnection;

class ErpFbListerner extends Public_Controller 
{
	public function __construct() 
	{
        error_reporting(-1);
		ini_set('display_errors', 1);

        parent::__construct();
        $this->load->config('amqps');
        $this->load->model('facebookads/facebookads_m');
	}

    public function handle()
    {
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        $queue          = $this->config->item('facebookads', 'amqps_queues');

        $connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        
        $channel = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $callback = function ($msg) {

            echo ' [x] Received ', $msg->body, "\n";

            $args = json_decode($msg->body);
            $args = wp_parse_args($args, [
                'action' => null,
                'event' => null,
                'contractId' => null,
            ]);

            $contractId = $args['contractId'];

            switch ($args['event']) {
                        
                case 'fbapi.sync' :
                    
                    try
                    {
                        $contract = (new facebookads_m())->set_contract($contractId);
                        $contract->update_insights();

                        $behaviour_m 	= $contract->get_behaviour_m();
                        $behaviour_m->get_the_progress();
                        $behaviour_m->sync_all_amount();

                        update_term_meta($contractId, 'time_next_api_check', strtotime('+30 minutes'));
                    }
                    catch (Exception $e)
                    {
                        trigger_error($e->getMessage());
                    }
                    
                    break;

                default:
                    # code...
                    break;
            }

            $msg->ack();
        };

        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($queue, '', false, false, false, false, $callback);

        while ($channel->is_open()) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();   
    }
}
/* End of file ErpNotificationsWorker.php */
/* Location: ./application/controllers/ErpNotificationsWorker.php */