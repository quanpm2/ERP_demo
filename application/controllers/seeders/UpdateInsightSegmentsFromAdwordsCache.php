<?php

use Google\AdsApi\Common\AdsSession;

defined('BASEPATH') OR exit('No direct script access allowed');

class UpdateInsightSegmentsFromAdwordsCache extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();
        $this->load->config('amqps');
        $this->load->model('googleads/googleads_log_m');
        $this->load->model('googleads/googleads_m');
        $this->load->model('googleads/mcm_account_m');
        $this->load->model('contract/contract_m');
        $this->load->model('contract/receipt_m');
        $this->load->model('term_posts_m');

        $this->load->helper('directory');
	}

    public function index()
    {
        /**
         * Việc cần làm tiếp theo, cần xử lý trường hợp , đối với các hợp đồng có dữ liệu cache, tuy nhiên lại không thể sử dụng Googleads API V09 để đọc lại dữ liệu, bây giờ cần phải tiến hành xử lý đồng bộ thật hợp lý ở phần này : 
         * - Lấy tất cả danh sách các hợp đồng đã từng thực hiện trong quá khứ
         * - Dựa vào thời gian hợp đồng , sẽ đọc lại dữ liệu quảng cáo từ cache
         *     - Không cần xử lý nếu không có kết quả từ file cache
         *     - Ngược lại, nếu có kết quả , kiểm tra xem có insight_segments tương ứng hay chưa ?
         *         - Nếu đã có rồi, thì bỏ qua hợp đồng và xử lý hợp đồng tiếp theo
         *         - Tiến hành thêm mới vào DB'
         * - Việc cần lưu ý :
         *     - Khởi tạo sẵn một số hợp đồng trong quá khứ có dữ liệu cache
         *     - Ghi chú lại danh sách ID và ID các tài khoản tương ứng
         *     - Tiến hành xóa dữ liệu từ các tài khoản và insightt_segment để xử lý (hoặc tự code để tự xóa khi bắt đầu script - phục vụ cho việc test)
         *     - Mỗi phần code xử lý cần phải được log lại vào *.log file ghi chi tiết tiến trình xử lý.
         */

        $requestId = md5(time().__CLASS__.__METHOD__);

        $map = directory_map(APPPATH . '/cache/google-adword/');
        $mcmAccountIds = array_map(function($x){
            return preg_replace("/[^0-9]/", "", $x );
        }, array_keys($map));

        log_message('debug', json_encode([
            'mcm_account_ids' => $mcmAccountIds
        ], JSON_PRETTY_PRINT));

        $mcmAccounts = $this->mcm_account_m->where_in('term_name', $mcmAccountIds)->get_all();

        log_message('debug', json_encode([
            'found' => count($mcmAccounts),
            'query' => $this->db->last_query()
        ], 
            JSON_OBJECT_AS_ARRAY
            // JSON_PRETTY_PRINT
        ));

        $insights = [];

        foreach($map as $cid => $ymd)
        {
            $cid = preg_replace("/[^0-9]/", "", $cid );
            $insights[$cid] = [];    
            foreach($ymd as $year => $md)
            {
                $year = preg_replace("/[^0-9]/", "", $year );
                if( 4 != strlen($year)) {
                    log_message('debug', 'Year Invalid ' . json_encode([ $year, $md ]));
                    continue;
                }

                foreach($md as $month => $ds)
                {
                    $month = preg_replace("/[^0-9]/", "", $month );
                    if( 2 != strlen($month)) {
                        log_message('debug', 'Month Invalid ' . json_encode([ $month, $ds ]));
                        continue;
                    }

                    foreach($ds as $day => $cache_name)
                    {
                        $day = preg_replace("/[^0-9]/", "", $day );
                        if( 2 != strlen($day)) {
                            log_message('debug', 'Month Invalid ' . json_encode([ $day, $cache_name ]));
                            continue;
                        }

                        $_items = $this->scache->get("google-adword/{$cid}/{$year}/{$month}/{$day}/ACCOUNT_PERFORMANCE_REPORT");
                        if(empty($_items)) continue;

                        // log_message('debug', 'insight data cached ' . json_encode($_items));
                        $insights[$cid] = array_merge($_items, $insights[$cid]);
                        log_message('debug', "CID {$cid} has ". count($_items) ." insights data detected at {$year}/{$month}/{$day}");
                    }
                }
            }

            if(empty($insights[$cid]))
            {
                unset($insights[$cid]);
                continue;
            }

            $insights[$cid] = array_group_by($insights[$cid], 'day');
        }

        log_message('debug', json_encode(['total' => count(array_keys($insights))]));

        $this->load->model('ads_segment_m');
        $adsSegments = $this->ads_segment_m
        ->set_post_type()
        ->select('term.term_id, term.term_type, term.term_status')
        ->select('posts.post_id, post_type, start_date, end_date')
        ->select('adaccounts.term_id as adaccountId, adaccounts.term_type as adaccountType, adaccounts.term_name as adaccountName, adaccounts.term_slug as adaccountSlug, adaccounts.term_status as adaccountStatus')
        ->join('term_posts as tp_adaccounts', 'tp_adaccounts.post_id = posts.post_id')
        ->join('term as adaccounts', 'adaccounts.term_id = tp_adaccounts.term_id')
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->join('term', 'term.term_id = term_posts.term_id')
        ->where_in('tp_adaccounts.term_id', array_map('intval', array_column($mcmAccounts, 'term_id')))
        ->where('adaccounts.term_type', 'mcm_account')
        ->where('term.term_type', 'google-ads')
        ->group_by('term.term_id, posts.post_id, tp_adaccounts.term_id')
        ->get_all();

        log_message('debug', json_encode(['contract_founds' => count(array_unique(array_column($adsSegments, 'term_id')))]));

        if(empty($adsSegments))
        {
            log_message('debug', json_encode(['code' => 404]));
            die();
        }

        $insert_insight_segments = [];

        foreach($adsSegments as $ads_segment)
        {
            $start = start_of_day($ads_segment->start_date);
            $end = end_of_day($ads_segment->end_date ?: time());

            $_insights = $insights[$ads_segment->adaccountName] ?? false;
            if(empty($_insights)) continue;

            // log_message('debug', json_encode(['$_insights' => $_insights]));

            $_insights = array_filter($_insights, function($x) use($start, $end){
                $_d = start_of_day($x);
                return $_d >= $start && $_d <= $end;
            }, ARRAY_FILTER_USE_KEY);

            $dates = array_keys($_insights);
            $dates = array_map(function($x){ return start_of_day($x); }, $dates);
            $dates = array_filter($dates, function($x) use($start, $end){
                return $x >= $start && $x <= $end; 
            });

            if(empty($dates)) continue;

            $existed_insight_segments = $this->insight_segment_m
            ->set_post_type()
            ->select('posts.post_id, posts.start_date')
            ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$ads_segment->adaccountId})")
            ->where('start_date >=', $start)
            ->where('end_date <=', $end)
            ->where_in('start_date', $dates)
            ->as_array()
            ->get_all();

            if( ! empty($existed_insight_segments))
            {
                $_dates_ignored = array_unique(array_column($existed_insight_segments, 'start_date'));
                $_insights = array_filter($_insights, function($x) use($_dates_ignored){
                    return in_array($x, $_dates_ignored);
                }, ARRAY_FILTER_USE_KEY);
            }

            if(empty($_insights))
            {
                log_message('debug', json_encode(['code' => 204]));
                continue;
            }

            $_insights = array_map(function($_rows) use($ads_segment, &$insert_insight_segments){

                $_ins = reset($_rows);
                $result = [
                    'account_currency'      => $_ins['currency'],
                    'account'               => $_ins['account'],
                    'day'                   => $_ins['day'],
                    'impressions'           => array_sum(array_column($_rows, 'impressions')),
                    'clicks'                => array_sum(array_column($_rows, 'clicks')),
                    'invalidClicks'         => array_sum(array_column($_rows, 'invalidClicks')),
                    'cost'                  => array_sum(array_column($_rows, 'cost')),
                    'cpc'                   => div(array_sum(array_column($_rows, 'cost')), array_sum(array_column($_rows, 'clicks'))),
                    'conversions'           => array_sum(array_column($_rows, 'conversions')),
                    'network'               => 'all',
                ];

                $result['spend']    = convertMicroCost($result['cost']);
                $result['cpc']      = convertMicroCost($result['cpc']);

                $meta_exchange_rate_key = '';

                if('VND' != $result['account_currency'])
                {
                    $contract_id = $ads_segment->term_id;

                    $exchange_rate = false;

                    'USD' == $result['account_currency'] AND $exchange_rate = (int) get_term_meta_value($contract_id, 'dollar_exchange_rate');

                    empty($exchange_rate) AND $exchange_rate = (int) get_term_meta_value($contract_id, strtolower("exchange_rate_{$result['account_currency']}_to_vnd"));
                    empty($exchange_rate) AND $exchange_rate = get_exchange_rate($result['account_currency']);

                    $result['exchange_rate']    = $exchange_rate;
                    $result['spend']            = (double) $result['spend'] * $exchange_rate;
                }
                
                return $result;

            }, $_insights);

            if(empty($_insights)) continue;

            foreach($_insights as $_insight)
            {
                $insert_insight_segments[] = [
                    'insight_segment' => [
                        'post_name' 	=> 'day',
                        'post_type' 	=> 'insight_segment',
                        'start_date' 	=> start_of_day($_insight['day']),
                        'end_date' 		=> start_of_day($_insight['day'])
                    ],
                    'pivot' => [
                        'term_id' => $ads_segment->adaccountId
                    ],
                    'metrics' => $_insight
                ];

                // log_message('debug', json_encode($insert_insight_segments, JSON_PRETTY_PRINT));
                // die();
            }
        }

        if(empty($adsSegments))
        {
            log_message('debug', json_encode(['code' => 404, 'message' => 'Không có gì cần được thêm mới vào DB.']));
            die();
        }

        log_message('debug', json_encode([
            'code' => 200,
            'data' => [
                'new records' => count($insert_insight_segments)
            ]
        ]));

        foreach($insert_insight_segments as $item)
        {
            $_isegment_id = $this->insight_segment_m->insert($item['insight_segment']);
            $this->term_posts_m->set_term_posts( $item['pivot']['term_id'], [$_isegment_id], $this->insight_segment_m->post_type);

            $item_array = ['account_currency', 'spend', 'cost', 'cpc', 'impressions', 'clicks', 'invalidClicks', 'conversions', 'network', 'exchange_rate'];
            $_metadata = elements($item_array, $item['metrics']);

            foreach($_metadata as $key => $value)
            {
                update_post_meta($_isegment_id, $key, (string) $value);
            }

            log_message('debug', '+1 record added.');
        }
    }
}
/* End of file ErpNotificationsWorker.php */
/* Location: ./application/controllers/ErpNotificationsWorker.php */