<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbfixture extends Public_Controller {

    public function __construct()
    {
        parent::__construct();

        if(is_cli() === FALSE)
        {
            exit();
        }
    }

    public function migrate()
    {
        $this->load->library('migration');
        if ($this->migration->current() === FALSE)
        {
            echo $this->migration->error_string() . PHP_EOL;
        }
        
        exit('every thing done !');
    }
}