<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TinUpdate extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();

        $this->load->model([
            'contract/contract_m',
            'contract/receipt_m',
            'term_posts_m'
        ]);
	}

    public function index(int $contract_id = 0, int $user_id = 0, $tin)
    {
        $requestId = md5(json_encode([ __CLASS__, __METHOD__, time() ]));

        $contract = $this->contract_m->set_term_type()->get($contract_id);

        $extra = get_term_meta_value($contract_id, 'extra');
        $extra = is_serialized($extra) ? unserialize($extra) : [];
        $extra['customer_tax'] = $tin;
        update_term_meta($contract_id, 'extra', serialize($extra));
        update_user_meta($user_id, 'customer_tax', $tin);
    }
}
/* End of file StartOfDaySegmentFormatlized.php.php */
/* Location: ./application/controllers/hotfix/StartOfDaySegmentFormatlized.php.php */


// php index.php hotfix/TinUpdate index 57024 14167 01E8036154