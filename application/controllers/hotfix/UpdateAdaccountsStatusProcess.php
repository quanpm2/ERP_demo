<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UpdateAdaccountsStatusProcess extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();

        $this->load->model([
            'contract/contract_m'
        ]);
	}

    public function index()
    {
        $contracts = $this->contract_m
        ->where_in('term.term_type', ['google-ads', 'facebook-ads'])
        ->where_in('term.term_status', ['pending', 'publish'])
        ->get_all();

        if(empty($contracts)) return TRUE;

        foreach($contracts AS $contract)
        {
            $contract_id            = $contract->term_id;
            $adaccounts             = get_term_meta($contract_id, 'adaccounts', FALSE, TRUE);
            $_adaccount_status_prev = get_term_meta_value($contract_id, 'adaccount_status');

            $is_service_proc    = is_service_proc($contract_id);
            $_adaccount_status  = $is_service_proc ? 'REMOVED' : 'UNSPECIFIED';
            ! empty($adaccounts) AND $_adaccount_status = $is_service_proc ? 'APPROVED' : 'PENDING_APPROVAL';

            if($_adaccount_status == $_adaccount_status_prev) continue;

            update_term_meta($contract_id, 'adaccount_status', $_adaccount_status);

            log_message('debug', json_encode([
                'contractId' => $contract_id,
                'type' => $contract->term_type,
                'adaccounts' => $adaccounts,
                'is_service_proc' => $is_service_proc,
                'old' => $_adaccount_status_prev, 
                'new' => $_adaccount_status
            ]));
        }
    }
}