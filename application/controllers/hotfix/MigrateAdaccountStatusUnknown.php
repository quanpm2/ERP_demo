<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MigrateAdaccountStatusUnknown extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();

        $this->load->model([
            'contract/base_contract_m',
            'contract/contract_m',
            'term_posts_m',
            'ads_segment_m'
        ]);
	}

    public function index()
    {
        $request_id = md5(uniqid( __CLASS__ . time()));

        $contract_type = ['tiktok-ads'];
        $contracts = $this->contract_m
            ->where_in('term.term_status', [
                'publish',
                'pending',
                // 'ending',
                // 'liquidation',
            ])
            ->where_in('term.term_type', $contract_type)
            ->join('termmeta', 'termmeta.term_id = term.term_id AND termmeta.meta_key IN ("start_service_time", "adaccount_status")', 'LEFT')
            ->select('term.term_id, term.term_type, term.term_status')
            ->select('MAX(IF(termmeta.meta_key = "start_service_time", termmeta.meta_value, NULL)) AS start_service_time')
            ->select('MAX(IF(termmeta.meta_key = "adaccount_status", termmeta.meta_value, "")) AS adaccount_status')
            ->group_by('term.term_id')
            ->having('adaccount_status = "UNKNOWN"')
            ->or_having('adaccount_status = ""')
            ->get_all();

        if(empty($contracts))
        {
            log_message('debug', "[{$request_id}] No Contracts Invalid");
            return TRUE;
        }

        log_message('debug', "[{$request_id}] -- " . json_encode([
            'num_of_contracts' => count($contracts)
        ]));

        log_message('debug', "[{$request_id}] -- Loop started.");

        foreach($contracts as $contract)
        {
            log_message('debug', "[{$request_id}] -- Contract #{$contract->term_id} loaded.");

            $segments = $this->term_posts_m->get_term_posts($contract->term_id, $this->ads_segment_m->post_type);
            $is_start_service = !empty($contract->start_service_time);
            if(!empty($segments))
            {
                log_message('debug', "[{$request_id}] -- Contract #{$contract->term_id} has " . count($segments) . ' segment(s)');

                if($is_start_service)
                {
                    log_message('debug', "[{$request_id}] -- Contract #{$contract->term_id} STARTED");

                    update_term_meta($contract->term_id, 'adaccount_status', 'APPROVED');
                    log_message('debug', "[{$request_id}] -- Contract #{$contract->term_id} - Update from 'UNKNOWN' => 'APPROVED'");
    
                    continue;
                }

                log_message('debug', "[{$request_id}] -- Contract #{$contract->term_id} NOT STARTED");
    
                update_term_meta($contract->term_id, 'adaccount_status', 'PENDING_APPROVAL');
                log_message('debug', "[{$request_id}] -- Contract #{$contract->term_id} - Update from 'UNKNOWN' => 'PENDING_APPROVAL'");

                continue;
            }
            
            if(!$is_start_service)
            {
                log_message('debug', "[{$request_id}] -- Contract #{$contract->term_id} NOT HAD ANY SEGMENTS");
                log_message('debug', "[{$request_id}] -- Contract #{$contract->term_id} NOT STARTED");

                update_term_meta($contract->term_id, 'adaccount_status', 'UNSPECIFIED');
                log_message('debug', "[{$request_id}] -- Contract #{$contract->term_id} - Update from 'UNKNOWN' => 'UNSPECIFIED'");
            }
        }
    }
}