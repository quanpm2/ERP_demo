<?php defined('BASEPATH') OR exit('No direct script access allowed');

class StartOfDaySegmentFormatlized extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();

        $this->load->model([
            'contract/contract_m',
            'contract/receipt_m',
            'term_posts_m'
        ]);
	}

    public function index()
    {
        $requestId = md5(json_encode([ __CLASS__, __METHOD__, time() ]));

        $this->load->model('ads_segment_m');
        
        $segments = $this->ads_segment_m
        ->set_post_type()
        ->get_all();

        $segments = array_map(function($x){
            
            $x->start_date_expected = start_of_day($x->start_date);
            $x->isDateInvalid = $x->start_date != $x->start_date_expected;
            return $x;

        }, $segments);

        log_message('debug', json_encode([
            'num_of_invalid' => count(array_filter($segments, function($x){ return $x->isDateInvalid; })),
            'total' => count($segments)
        ]));

        $invalidSegments = array_filter($segments, function($x){
            return $x->isDateInvalid;
        });

        foreach($invalidSegments as $segment)
        {
            $this->post_m->update($segment->post_id, [
                'start_date' => start_of_day($segment->start_date)
            ]);
        }
    }
}
/* End of file StartOfDaySegmentFormatlized.php.php */
/* Location: ./application/controllers/hotfix/StartOfDaySegmentFormatlized.php.php */
