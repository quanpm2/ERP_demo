<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Notifications\Erp\Reports\Ads\Googleads\CustomerInsightReportWeekly\Email as WeeklyReportEmail;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class ReQueueReceiptsWebServices extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();

        $this->load->model([
            'contract/contract_m',
            'contract/receipt_m',
            'term_posts_m'
        ]);

        $this->load->config('amqps');
	}

    public function index()
    {
        $requestId = md5(json_encode([ __CLASS__, __METHOD__, time() ]));

        $receipts = $this->receipt_m
        ->set_post_type()
        ->select('posts.post_id')
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->join('term', 'term.term_id = term_posts.term_id')
        ->where_in('term.term_type', array( 'oneweb', 'hosting', 'domain', 'onead'))
        ->group_by('posts.post_id')
        ->get_all();

        log_message('debug', json_encode([
            'requestId' => $requestId,
            'found' => count($receipts)
        ]));

		$amqps_host 	= $this->config->item('host', 'amqps');
		$amqps_port 	= $this->config->item('port', 'amqps');
		$amqps_user 	= $this->config->item('user', 'amqps');
		$amqps_password = $this->config->item('password', 'amqps');
		
        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];

		$connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
		$channel 	= $connection->channel();
		$channel->queue_declare($queue, false, true, false, false);

        foreach ($receipts as $receipt)
        {
            $payload = [
				'event' => 'contract_payment.sync.metadata',
				'paymentId'	=> $receipt->post_id,
				'log_id' => 0
			];

            $message = new \PhpAmqpLib\Message\AMQPMessage(
                json_encode($payload),
                array('delivery_mode' => \PhpAmqpLib\Message\AMQPMessage::DELIVERY_MODE_PERSISTENT)
            );

            $channel->basic_publish($message, '', $queue);	
        }

		$channel->close();
		$connection->close();

        log_message('debug', json_encode([
            'requestId' => $requestId,
            'message' => 'push to queue completed .'
        ]));
        
        return TRUE;
    }
}
/* End of file ReQueueReceiptsWebServices.php.php */
/* Location: ./application/controllers/hotfix/ReQueueReceiptsWebServices.php.php */