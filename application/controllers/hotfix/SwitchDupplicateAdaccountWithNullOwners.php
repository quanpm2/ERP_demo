<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SwitchDupplicateAdaccountWithNullOwners extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();

        $this->load->model([
            'contract/contract_m',
            'facebookads/adaccount_m',
            'facebookads/insight_segment_m'
        ]);
	}

    public function index()
    {
        dd('fff');
        $query = '
            select term.*, term_users.user_id as u_user_id, user.user_id, user.user_type
            from term 
            INNER JOIN ( SELECT term.term_slug FROM term where term_type = "adaccount" group by term_slug having count(*) >= 2 ) dup on term.term_slug = dup.term_slug
            left join term_users on term_users.term_id = term.term_id
            left join user on user.user_id = term_users.user_id and user.user_type = "fbAccount"
            where term.term_type = "adaccount"
            group by term.term_id
            -- having term_users.user_id is null
        ';

        $dupplicate_records = $this->contract_m->query($query)->result();
        dd($dupplicate_records);
        $dupplicate_records AND $dupplicate_records = array_group_by($dupplicate_records, 'term_slug');
        log_message('debug', json_encode([
            'Num of dupplicate adaccounts is ' . count(array_keys($dupplicate_records))
            ],
            // JSON_PRETTY_PRINT
        ));

        foreach($dupplicate_records as $adAccountId => $adAccounts)
        {
            log_message('debug', json_encode([
                    'Index of AdAccount' => $adAccountId,
                ],
                // JSON_PRETTY_PRINT
            ));

            $adAccountRonin = array_filter($adAccounts, function ($x){ return empty($x->u_user_id); });
            $adAccountRonin AND $adAccountRonin = reset($adAccountRonin);

            $adAccountSinobi = array_filter($adAccounts, function ($x){ return ! empty($x->u_user_id); });
            $adAccountSinobi AND $adAccountSinobi = reset($adAccountSinobi);

            if( empty($adAccountSinobi))
            {
                log_message('debug', 'BOTH ADACCOUNTS IS RONIN .');
                continue;
            }

            $newAdAccountRoninName = $adAccountSinobi->term_name . " (không hoạt động)";
            $this->adaccount_m->update($adAccountRonin->term_id, [ 'term_name' => $newAdAccountRoninName ]);
            $adAccountRonin->term_name = $newAdAccountRoninName;

            log_message('debug', json_encode([
                    'Ronin name changed to ' . $newAdAccountRoninName
                ],
                // JSON_PRETTY_PRINT
            ));

            log_message('debug', json_encode([
                    'Ronin' => $adAccountRonin,
                    'Sinobi' => $adAccountSinobi,
                ],
                JSON_PRETTY_PRINT
            ));

            $foundLeftItemsQuery  = '
            SELECT a_set.* FROM (
                SELECT `posts`.`post_id`, `post_type`, `post_name`, `start_date`, `end_date` 
                FROM `posts` 
                JOIN `term_posts` ON `term_posts`.`post_id` = `posts`.`post_id` WHERE `term_posts`.`term_id` = "'.$adAccountRonin->term_id.'" AND `post_type` = "insight_segment"
            ) AS a_set
            left join (
                SELECT * FROM (
                    SELECT `posts`.`post_id`, `post_type`, `post_name`, `start_date`, `end_date` 
                    FROM `posts` 
                    JOIN `term_posts` ON `term_posts`.`post_id` = `posts`.`post_id` WHERE `term_posts`.`term_id` = "'. $adAccountSinobi->term_id .'" AND `post_type` = "insight_segment"
                ) AS t
            ) as b_set on b_set.start_date = a_set.start_date
            where b_set.start_date is null
            ';

            $leftOuterJoinRecords = $this->insight_segment_m->query($foundLeftItemsQuery)->result();
            log_message('debug', count($leftOuterJoinRecords). ' records found.');

            if( ! empty($leftOuterJoinRecords))
            {
                foreach($leftOuterJoinRecords as $_row)
                {
                    $insert_data = (array)$_row;
                    unset($insert_data['post_id']);
                    $insertedId = $this->insight_segment_m->insert($insert_data);

                    $_postmeta = get_instance()->postmeta_m->get_meta($_row->post_id, '', true, true);
                    $_postmeta AND $_postmeta = array_map(function($x) use ($insertedId){
                        return [
                            'post_id' => $insertedId,
                            'meta_key' => $x['meta_key'], 
                            'meta_value' => $x['meta_value']
                        ];
                    }, $_postmeta);

                    $this->postmeta_m->insert_many($_postmeta);

                    $this->term_posts_m->set_term_posts($adAccountSinobi->term_id, [ $insertedId ], $this->insight_segment_m->post_type, TRUE);
                }

                $hasRootFbAccount = $this->term_users_m->where('user_id', 13331)->where('term_id', $adAccountSinobi->term_id)->count_by() > 0;
                if( ! $hasRootFbAccount)
                {
                    $this->term_users_m->set_term_users( $adAccountSinobi->term_id, array(13331), 'fbaccount');
                }
            }

            $segments = $this->ads_segment_m->set_post_type()
            ->join('term_posts', 'term_posts.post_id = posts.post_id')
            ->where('term_id', $adAccountRonin->term_id)
            ->get_all();

            if( ! empty($segments))
            {
                foreach($segments as $segment)
                {
                    $this->term_posts_m->delete_post_terms($segment->post_id, [ $adAccountRonin->term_id ]);
                    $this->term_posts_m->set_term_posts( $adAccountSinobi->term_id, array($segment->post_id), 'ads_segment', $append = TRUE);

                    $contract = $this->term_posts_m->get_post_terms( $segment->post_id, 'facebook-ads');
                    $contract AND $contract = reset($contract);

                    $contract->adaccounts = get_term_meta($contract->term_id, 'adaccounts', FALSE, TRUE);
                    $contract->adaccounts AND $contract->adaccounts = array_filter(array_map('intval', $contract->adaccounts), function($x) use($adAccountRonin){
                        return $x != $adAccountRonin->term_id;
                    });

                    $contract->adaccounts[] = $adAccountSinobi->term_id;

                    $this->termmeta_m->delete_meta($contract->term_id, 'adaccounts');
                    array_walk($contract->adaccounts, function($_adAccountId) use($contract){
                        $this->termmeta_m->add_meta($contract->term_id, 'adaccounts', $_adAccountId);
                    });
                }
            }
        }
    }
}