<?php defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Psr7\Request;
use Notifications\Erp\Reports\Ads\Googleads\CustomerInsightReportWeekly\Email as WeeklyReportEmail;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class RollbackWorkersInsightsStageSync extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();

        $this->load->model([
            'contract/contract_m',
            'contract/receipt_m',
            'term_posts_m'
        ]);

        $this->load->config('amqps');
        $this->load->helper('url');
	}

    public function index()
    {
        $requestId = md5(json_encode([ __CLASS__, __METHOD__, time() ]));
        log_message('debug', json_encode([
            'requestId' => $requestId
        ]));

        $watches_queues = $this->config->item('watches_insight_queues', 'amqps_queues');
        if(empty($watches_queues))
        {
            log_message('debug', json_encode([
                'requestId' => $requestId, 
                'message' => "Watches queue list is empty . Exited."
            ]));

            die();
        }

        $options = [
            'auth' => [
                $this->config->item('user', 'amqps'),
                $this->config->item('password', 'amqps')
            ]
        ];    
        $numOfMsgUnread = 0;

        foreach($watches_queues as $item)
        {
            $rabbitMQAPI = prep_url($this->config->item('host', 'amqps') . ":15672/api/queues/%2F/{$item}");

            log_message('debug', json_encode([
                'requestId' => $requestId, 
                'message' => 'Init & request RabbitMQ API',
                'API' => $rabbitMQAPI
            ]));

            $response = Requests::get($rabbitMQAPI, [], $options);
            if( 200 != $response->status_code)
            {
                log_message('debug', json_encode([
                    'requestId' => $requestId, 
                    'message' => 'RabbitMQ response != 200',
                    'API' => $rabbitMQAPI
                ]));

                die();
            }

            $rabbitMQResponse = json_decode($response->body);
            $numOfMsgUnread+= ($rabbitMQResponse->messages ?? 0);
        }

        if( $numOfMsgUnread > 0)
        {
            log_message('debug', json_encode([
                'requestId' => $requestId, 
                'message' => 'Queue in running ! Exit , waiting for next request.',
                'num_of_messages' => $numOfMsgUnread
            ]));

            die();
        }
        
        $this->load->model('termmeta_m');

        $deadRecords = $this->contract_m
        ->select('term.term_id')
        ->join('termmeta', 'termmeta.term_id = term.term_id')
        ->where_in('term.term_type', [ 'google-ads', 'facebook-ads', 'tiktok-ads', 'zalo-ads' ])
        ->where('meta_key', 'stage_sync')
        ->where_in('meta_value', ['inqueue', 'running'])
        ->group_by('term.term_id')
        ->get_all();

        if( empty($deadRecords))
        {
            log_message('debug', json_encode([
                'requestId' => $requestId, 
                'message' => 'Dead Records not found ! Exit , waiting for next request.'
            ]));
            die();
        }

        foreach($deadRecords as $deadRecord)
        {
            update_term_meta($deadRecord->term_id, 'stage_sync', 'rollback');
        }

        log_message('debug', json_encode([
            'requestId' => $requestId, 
            'message' => count($deadRecords) ." has been rollback"
        ]));
    }
}
/* End of file ReQueueReceiptsWebServices.php.php */
/* Location: ./application/controllers/hotfix/ReQueueReceiptsWebServices.php.php */