<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpAmqpLib\Connection\AMQPStreamConnection;

class ErpConvertToolWoker extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->config('amqps');

        $this->load->config('googleads/excel_adseditor');
    }

    public function handle()
    {
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        $queue          = $this->config->item('utility', 'amqps_queues');

        $connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);

        $channel = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $callback = function ($msg) use ($amqps_host, $amqps_port, $amqps_user, $amqps_password) {

            echo ' [x] Received ', $msg->body, "\n";

            $args = json_decode($msg->body, TRUE);
            $args = wp_parse_args($args, [
                'action' => null,
                'event' => null,
                'payload' => null,
            ]);

            switch ($args['event']) {
                case 'utility.convert.adword':
                    if (!isset($args['payload']['file_token'])) break;

                    $file_token = $args['payload']['file_token'];
                    $this->convertAdwordFile($file_token);

                    break;
                default:
                    # code...
                    break;
            }

            $msg->ack();
        };

        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($queue, '', false, false, false, false, $callback);

        while ($channel->is_open()) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }

    public function convertAdwordFile($file_token)
    {
        $cache_key = 'modules/googleads/convert/' . $file_token;
        $data = $this->scache->get($cache_key);
        if (empty($data) || time() > $data['time_expire']) {
            $data['error'] = 'Tập tin hết hiệu lực';

            return FALSE;
        }

        $file_name = $data['file_name'];
        if (empty($file_name)) {
            $data['error'] = 'File dữ liệu không tồn tại';

            return FALSE;
        }

        try {
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file_name);
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($file_name);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, TRUE, FALSE, TRUE);
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();

            return FALSE;
        }

        array_shift($sheetData);

        $sheetData = array_filter($sheetData, function($item){ return $item['A'] != ""; });
        $total = count($sheetData);
        $data['total'] = $total;
        $data['progress'] = 0;
        $data['progress_percent'] = 0;
        $this->scache->write($data, $cache_key);
        
        if (empty($sheetData)) {
            $data['result'] = [];

            return TRUE;
        };

        $this->load->helper('text');

        try
        {
            $export_template    = FCPATH . 'files/excel_tpl/tool/google-adwords-editors-export-template-v2.csv';
            $reader             = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($export_template);
            $spreadsheet        = $reader->load($export_template);
        }
        catch (Exception $e) { 
            $data['error'] = $e->getMessage();

            return FALSE;
        }

        $spreadsheet
        ->getProperties()
        ->setCreator('ADSPLUS.VN')
        ->setLastModifiedBy('ADSPLUS.VN')
        ->setTitle('Export Adword Editor XLSX File')
        ->setSubject('Export Adword Editor XLSX File')
        ->setDescription('The File Converted From Adsplus Template XLSX');

        $spreadsheet->setActiveSheetIndex(0);

        $iRow = 2;

        $progress = 0;
        $result = [
            'campaign' => 0,
            'ad_group' => 0,
            'keyword' => 0,
            'heading' => 0,
            'description' => 0,
        ];
        $data_group_by_campaign = array_group_by($sheetData, 'A');
        foreach($data_group_by_campaign as $campaign_name => &$campaign_data){
            $result['campaign'] += 1;

            // Set cells value for CAMPAIGN
            $spreadsheet->getActiveSheet()
            ->setCellValue("A{$iRow}", $campaign_name) // Campaign
            ->setCellValue("E{$iRow}", 'Search') // Campaign Type
            ->setCellValue("F{$iRow}", 'Google search') // Networks
            ->setCellValue("G{$iRow}", 'All') // Languages
            ->setCellValue("O{$iRow}", 'Location of presence'); // Targeting method
            $iRow++;

            $campaign_data = array_group_by($campaign_data , 'B');
            foreach($campaign_data as $ad_group_name => &$ad_group_data){
                $result['ad_group'] += 1;

                // Setup Ads
                $instance_ads = reset($ad_group_data);
                $spreadsheet->getActiveSheet()
                ->setCellValue("A{$iRow}", $campaign_name) // Campaign
                ->setCellValue("R{$iRow}", $ad_group_name) // Ad Group
                ->setCellValue("BC{$iRow}", $instance_ads['E'] ?? '') // Final URL
                ->setCellValue("BD{$iRow}", $instance_ads['F'] ?? '') // Final mobile URL
                ->setCellValue("CR{$iRow}", $instance_ads['G'] ?? '' ) // Path 1 
                ->setCellValue("CS{$iRow}", $instance_ads['H'] ?? ''); // Path 2

                $heading = ['I' => 'BH', 'K' => 'BJ', 'L' => 'BL', 'M' => 'BN', 'N' => 'BP', 'O' => 'BR', 'P' => 'BT', 'Q' => 'BV', 'R' => 'BX', 'S' => 'BZ', 'T' => 'CB', 'U' => 'CD', 'V' => 'CF', 'W' => 'CH'];
                foreach($heading as $source_col => $target_col){
                    if(!empty($instance_ads[$source_col])){
                        $result['heading'] += 1;

                        $spreadsheet->getActiveSheet()->setCellValue("{$target_col}{$iRow}", ('"' . $instance_ads[$source_col] . '"'));
                    }
                }
                    
                $description = ['X' => 'CJ', 'Y' => 'CL', 'Z' => 'CN', 'AA' => 'CP'];
                foreach($description as $source_col => $target_col){
                    if(!empty($instance_ads[$source_col])){
                        $result['description'] += 1;

                        $spreadsheet->getActiveSheet()->setCellValue("{$target_col}{$iRow}", ('"' . $instance_ads[$source_col] . '"'));
                    }
                }
                $iRow++;

                $ad_group_data = array_group_by($ad_group_data , 'C');
                foreach($ad_group_data as $keyword_name => &$keyword_data){
                    $result['keyword'] += 2;

                    $instance_keyword = reset($keyword_data);

                    $spreadsheet->getActiveSheet()
                    ->setCellValue("A{$iRow}", $campaign_name) // Campaign
                    ->setCellValue("R{$iRow}", $ad_group_name) // Ad Group
                    ->setCellValue("AT{$iRow}", $keyword_name) // Keyword
                    ->setCellValue("S{$iRow}", $instance_keyword['D'] ?? '0') // Max CPC
                    ->setCellValue("AU{$iRow}", 'Exact'); // Criterion Type
                    $iRow++;

                    $spreadsheet->getActiveSheet()
                    ->setCellValue("A{$iRow}", $campaign_name) // Campaign
                    ->setCellValue("R{$iRow}", $ad_group_name) // Ad Group
                    ->setCellValue("AT{$iRow}", $keyword_name) // Keyword
                    ->setCellValue("S{$iRow}", $instance_keyword['D'] ?? '0') // Max CPC
                    ->setCellValue("AU{$iRow}", 'Phrase'); // Criterion Type
                    $iRow++;

                    $progress++;
                    $data['progress'] = $progress;
                    $progress_percent = round(div($progress, $total) * 100, 0);
                    $data['progress_percent'] = $progress_percent;
                    $this->scache->write($data, $cache_key);
                }
            }
        }

        try{
            $export_file  = 'tmp/' . uniqid('export-converted-csv-adword-editor-') . '.csv';
            $writer     = new \PhpOffice\PhpSpreadsheet\Writer\Csv($spreadsheet);
    
            $writer->setUseBOM(true);
            $writer->setEnclosure('');
            $writer->save(FCPATH . $export_file);
        }
        catch(Exception $e){
            $data['error'] = $e->getMessage();

            return FALSE;
        }

        $data['export_file'] = $export_file;
        $data['result'] = $result;

        $this->scache->write($data, $cache_key);

        return TRUE;
    }
}
/* End of file ErpNotificationsWorker.php */
/* Location: ./application/controllers/ErpNotificationsWorker.php */