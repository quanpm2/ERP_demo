<?php defined('BASEPATH') or exit('No direct script access allowed');

class ErpContractReportTool extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contract/contract_m');
        $this->load->model('report_m');
        $this->load->model('balance_spend_m');
        $this->load->model('term_posts_m');
        $this->load->model('term_users_m');
    }

    public function reloadTbl()
    {
        $query = "
            SELECT
                term.term_id,
                term.term_type,
                customer.user_id,
                customer.display_name,
                customer.user_type,
                m_term.service_fee_rate_actual,
                insights.post_id,
                insights.post_type,
                insights.start_date,
                insights.end_date,
                0 AS amount_payment,
                0 AS budget_payment,
                0 AS service_fee_payment,
                0 AS fct_amount_payment,
                0 AS vat_amount_payment,
                m_insight.meta_value as spend,
                0 AS balance_spend
            FROM
                term
                JOIN term_users AS tu_customers ON tu_customers.term_id = term.term_id
                JOIN user AS customer ON customer.user_id = tu_customers.user_id AND customer.user_type IN ( 'customer_person', 'customer_company' )
                JOIN term_posts ON term_posts.term_id = term.term_id
                JOIN posts AS ads_segment ON ads_segment.post_id = term_posts.post_id AND ads_segment.post_type = 'ads_segment'
                JOIN term_posts AS tp_segment_adaccount ON tp_segment_adaccount.post_id = ads_segment.post_id
                JOIN term AS adaccounts ON tp_segment_adaccount.term_id = adaccounts.term_id AND adaccounts.term_type IN ( 'adaccount', 'mcm_account' )
                JOIN term_posts AS tp_insights ON tp_insights.term_id = adaccounts.term_id
                JOIN posts AS insights ON insights.post_id = tp_insights.post_id 
                    AND insights.post_type = 'insight_segment' 
                    AND insights.post_name = 'day' 
                    AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME( ads_segment.start_date, '%Y-%m-%d 00:00:00' )) 
                    AND insights.start_date <= IF( ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP(), ads_segment.end_date )
                JOIN postmeta AS m_insight ON m_insight.post_id = insights.post_id AND m_insight.meta_key = 'spend'
                LEFT JOIN
                (
                    SELECT 
                        termmeta.term_id,
                            MAX(IF(termmeta.meta_key = 'service_fee_rate_actual', `termmeta`.`meta_value`, NULL)) AS service_fee_rate_actual
                    FROM
                        termmeta
                    GROUP BY termmeta.term_id
                ) m_term ON term.term_id = m_term.term_id
            WHERE
                term.term_type IN ('google-ads', 'facebook-ads')
                AND term.term_status IN ( 'pending', 'publish', 'ending', 'liquidation' ) 
            GROUP BY customer.user_id, term.term_id, term_posts.post_id, adaccounts.term_id, insights.post_id

            UNION ALL

            SELECT 
                term.term_id,
                term.term_type,
                customer.user_id,
                customer.display_name,
                customer.user_type,
                m_term.service_fee_rate_actual,
                receipt_payment.post_id,
                receipt_payment.post_type,
                receipt_payment.start_date,
                receipt_payment.end_date,
                m_receipt_payment.amount_payment,
                m_receipt_payment.budget_payment,
                m_receipt_payment.service_fee_payment,
                m_receipt_payment.fct_amount_payment,
                m_receipt_payment.vat_amount_payment,
                0 as spend,
                0 AS balance_spend
            FROM term
            JOIN term_users AS tu_customers ON tu_customers.term_id = term.term_id
            JOIN user AS customer ON customer.user_id = tu_customers.user_id AND customer.user_type IN ( 'customer_person', 'customer_company' )
            JOIN term_posts ON term_posts.term_id = term.term_id
            JOIN posts AS receipt_payment ON receipt_payment.post_id = term_posts.post_id and receipt_payment.post_type = 'receipt_payment'
            LEFT JOIN
            (
                SELECT 
                    termmeta.term_id,
                        MAX(IF(termmeta.meta_key = 'service_fee_rate_actual', `termmeta`.`meta_value`, NULL)) AS service_fee_rate_actual
                FROM
                    termmeta
                GROUP BY termmeta.term_id
            ) m_term ON term.term_id = m_term.term_id
            LEFT JOIN
            (
                SELECT 
                    post_id,
                        MAX(IF(postmeta.meta_key = 'amount', `postmeta`.`meta_value`, 0)) AS amount_payment,
                        MAX(IF(postmeta.meta_key = 'actual_budget', `postmeta`.`meta_value`, 0)) AS budget_payment,
                        MAX(IF(postmeta.meta_key = 'service_fee', `postmeta`.`meta_value`, 0)) AS service_fee_payment,
                        MAX(IF(postmeta.meta_key = 'fct_amount', `postmeta`.`meta_value`, 0)) AS fct_amount_payment,
                        MAX(IF(postmeta.meta_key = 'vat_amount', `postmeta`.`meta_value`, 0)) AS vat_amount_payment
                FROM
                    postmeta
                GROUP BY postmeta.post_id
            ) m_receipt_payment ON m_receipt_payment.post_id = receipt_payment.post_id
            WHERE
            term.term_type IN ('webdoctor', 'seo-traffic', 'seo-top', 'google-ads', 'webgeneral', 'webdesign', 'oneweb', 'pushdy', 'hosting', 'domain', 'facebook-ads', 'tiktok-ads', 'weboptimize', 'banner', 'fbcontent', 'courseads', 'onead') 
            AND receipt_payment.post_status = 'paid'
            GROUP BY term.term_id

            UNION ALL

            SELECT 
                term.term_id,
                term.term_type,
                customer.user_id,
                customer.display_name,
                customer.user_type,
                m_term.service_fee_rate_actual,
                balance_spend.post_id,
                balance_spend.post_type,
                balance_spend.start_date,
                balance_spend.end_date,
                0 AS amount_payment,
                0 AS budget_payment,
                0 AS service_fee_payment,
                0 AS fct_amount_payment,
                0 AS vat_amount_payment,
                0 as spend,
                balance_spend.post_content AS balance_spend
            FROM term
            JOIN term_users AS tu_customers ON tu_customers.term_id = term.term_id
            JOIN user AS customer ON customer.user_id = tu_customers.user_id AND customer.user_type IN ( 'customer_person', 'customer_company' )
            JOIN term_posts ON term_posts.term_id = term.term_id
            JOIN posts AS balance_spend ON balance_spend.post_id = term_posts.post_id and balance_spend.post_type = 'balance_spend'
            LEFT JOIN
            (
                SELECT 
                    termmeta.term_id,
                        MAX(IF(termmeta.meta_key = 'service_fee_rate_actual', `termmeta`.`meta_value`, NULL)) AS service_fee_rate_actual
                FROM
                    termmeta
                GROUP BY termmeta.term_id
            ) m_term ON term.term_id = m_term.term_id
            WHERE
            term.term_type IN ('google-ads', 'facebook-ads')
            AND balance_spend.post_status = 'publish'
            GROUP BY term.term_id
        ";
        $data = $this->contract_m->query($query)->result();

        $this->load->model('report_m');
        $this->db->trans_start();

        $this->report_m->where('1=1')->delete_by();

        if (!empty($data)) {
            foreach ($data as $item) {
                $this->report_m->insert([
                    "term_id" => $item->term_id,
                    "term_type" => $item->term_type,
                    "user_id" => $item->user_id,
                    "post_id" => $item->post_id,
                    "post_type" => $item->post_type,
                    "spend" => $item->spend,
                    "amount_payment" => $item->amount_payment,
                    "budget_payment" => $item->budget_payment,
                    "service_fee_payment" => $item->service_fee_payment,
                    "fct_amount_payment" => $item->fct_amount_payment,
                    "vat_amount_payment" => $item->vat_amount_payment,
                    "display_name" => $item->display_name,
                    "user_type" => $item->user_type,
                    "start_date" => $item->start_date,
                    "end_date" => $item->end_date,
                    // "contract_budget_payment_type" => $item->contract_budget_payment_type,
                    "service_fee_rate_actual" => $item->service_fee_rate_actual,
                    // "contract_begin" => $item->contract_begin,
                    // "contract_end" => $item->contract_end,
                    // "advertise_start_time" => $item->advertise_start_time,
                    "balance_spend" => $item->balance_spend,
                ]);
            }
        }

        $this->db->trans_complete();
    }
}
/* End of file ErpNotificationsWorker.php */
/* Location: ./application/controllers/ErpNotificationsWorker.php */