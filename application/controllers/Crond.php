<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crond extends Public_Controller {

	public function index()
	{
		$h = date('G');
		$d = date('d');

		$this->cmd('crond/exc_php/start');

		$token = parent::build_token();

		// if(in_array($h, array(9, 14)))
		// {
		// 	$this->cmd('crond/exc_php/webgeneral-cron');
		// 	$this->cmd('admin/webgeneral/cron');
		// }

		// if(in_array($h, array(11)))
		// {
		// 	$this->cmd('crond/exc_php/webgeneral-warning_expires');
		// 	$this->cmd('admin/webgeneral/cron/warning_expires');
		// }

		// if(in_array($h, array(8)))
		// {
		// 	$this->cmd('crond/exc_php/phonebook-report-1');
		// }

		// if(in_array($h, array(12)))
		// {
		// 	$this->cmd('crond/exc_php/phonebook-report-2');
		// }

		if(in_array($h, array(10, 16)))
		{
			$this->cmd('crond/exc_php/webbuild-cron');
			$this->cmd('admin/webbuild/crond');
		}

		$this->cmd('admin/googleads/cron');
		$this->cmd("admin/facebookads/cron/index?token={$token}");
		$this->cmd("admin/mbusiness/cron/index?token={$token}");
		$this->cmd("admin/contract/cron/index?token={$token}");
		$this->cmd('admin/contract/cron_receipt/index');

		if(in_array($h, array(16)))
		{
			$this->cmd('admin/hosting/crond');
		}

		$this->cmd('crond/exc_php/end');
		echo '['.date('Y-m-d H:i:s').'] '."\n";
	}

	public function adsService()
	{
		$h = date('G');
		$d = date('d');

		$this->cmd('crond/exc_php/start');
		$token = parent::build_token();
		$this->cmd('admin/googleads/cron/frequency');
		$this->cmd('crond/exc_php/end');
		echo '['.date('Y-m-d H:i:s').'] '."\n";
	}

	public function syncAdsplusReceipt()
	{
		// $this->cmd('crond/exc_php/start');
		// $this->cmd('admin/contract/cron_receipt/index');
		// $this->cmd('crond/exc_php/end');
		// echo '['.date('Y-m-d H:i:s').'] '."\n";
	}

	function exc_php($content = '')
	{
		file_put_contents('./log/crond/'.date('Y-m-d').'.txt', '['.date('Y-m-d H:i:s').'] '.$content."\n", FILE_APPEND);
	}

	private function cmd($uri = '')
	{
		$this->get_data($uri);
		// exec('php /var/www/erp/index.php '.$uri);
	}

	private function get_data($url = '')
	{
		$url = 'http://erp.webdoctor.vn/'.$url;
		$headers = array();
		$headers[] = 'Content-type: charset=utf-8'; 
		$headers[] = 'Connection: Keep-Alive';
		$ua = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_USERAGENT, $ua);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);

		$data = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
	}
}
