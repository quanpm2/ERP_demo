<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module extends Admin_Controller {

	public $model = 'permission_m';

	function __construct(){

		parent::__construct();
		$this->load->model($this->model);
		$this->url_index = admin_url().'module';

        if($this->admin_m->id != 47) redirect(admin_url(), 'refresh');
	}

	/**
     * { function_description }
     */
    function index()
    {
        $this->load->helper('directory');
        $this->load->library('table');
        $this->table->set_heading(array( 'Tên', 'Mô tả', 'Tác giả', 'version', '' ));

        $modules = directory_map(APPPATH.'modules/', 1);
        
        $modules_name = array();
        $modules_active = get_active_modules();

        foreach($modules as $i=>$module)
        {
            $module = trim($module,DIRECTORY_SEPARATOR);
            $tmp_package = $this->load->package($module);
            if( ! $tmp_package) continue;


            $link = '<a href="'.$this->url_index.'/';
            $link.= (isset($modules_active[$module])) ? 'uninstall' : 'install';
            $link.= '/'.$module.'">';
            $link.= (isset($modules_active[$module])) ? 'Gỡ cài đặt' : 'Cài đặt';
            $link.='</a>';
            $this->table->add_row($tmp_package->title(), $tmp_package->description(), $tmp_package->author(), $tmp_package->version(), $link);
        }
        
        $this->template->title->set('Quản lý Modules');
        $this->template->content->set($this->table->generate());
        $this->template->publish();
    }

    /**
     * Installs the given module name.
     *
     * @param      <type>  $module_name  The module name
     */
    function install($module_name)
    {
        $package = $this->load->package($module_name);
        $package->install();

        $modules_active = get_active_modules();

        if(!$modules_active || !is_array($modules_active))
        {
            $modules_active = array();
        }

        if( ! isset($modules_active[$module_name])) $modules_active[$module_name] = '';

        $this->option_m->set_value('module_active', $modules_active);
        $this->messages->success('Cài đặt '.$package->title(). ' thành công');
        redirect($this->url_index.'','refresh');

    }

    function uninstall($module_name)
    {
        $package = $this->load->package($module_name);
        $package->uninstall();

        $modules_active = get_active_modules();

        if(!$modules_active || !is_array($modules_active))
        {
            $modules_active = array();
        }
        if(isset($modules_active[$module_name]))
        {
            unset($modules_active[$module_name]);
        }

        $this->option_m->set_value('module_active', $modules_active);
        $this->messages->success('Gỡ bỏ '.$package->title(). ' thành công');
        redirect($this->url_index.'','refresh');
    }

    function create($module_name = '', $overwrite = 0)
    {
        $folder_module = APPPATH.'modules/'.$module_name.'/';
        $this->load->helper('file');
        if(is_dir($folder_module))
        {
            ($overwrite) 
            ? rename($folder_module, APPPATH.'modules/'.$module_name.'-'.time().'/') 
            : die('Modules exists');
        }

        $module_name_lower = strtolower($module_name);
        $module_name_upcase = ucfirst($module_name);
        copy_directory(APPPATH.'modules/example/', $folder_module);


        $package = read_file($folder_module.'Example.php');
        $package = str_replace('example', $module_name_lower, $package);
        $package = str_replace('Example', $module_name_upcase, $package);
        write_file($folder_module.'Example.php', $package);
        rename($folder_module.'Example.php', $folder_module.$module_name_upcase.'.php');

        $controller = read_file($folder_module.'controllers/Example.php');
        $controller = str_replace('example', $module_name_lower, $controller);
        $controller = str_replace('Example', $module_name_upcase, $controller);
        write_file($folder_module.'controllers/Example.php', $controller);
        rename($folder_module.'controllers/Example.php', $folder_module.'controllers/'.$module_name_upcase.'.php');

        $model = read_file($folder_module.'models/Example_m.php');
        $model = str_replace('example', $module_name_lower, $model);
        $model = str_replace('Example', $module_name_upcase, $model);
        write_file($folder_module.'models/Example_m.php', $model);
        rename($folder_module.'models/Example_m.php', $folder_module.'models/'.$module_name_upcase.'_m.php');

        echo 'tạo module '.$module_name.' thành công';
    }
}