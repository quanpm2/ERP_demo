<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Notifications\Erp\Reports\Ads\Googleads\CustomerDailyClicks\Sms as CustomerDailyClicks;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class ErpNotificationWorker extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();
        $this->load->config('amqps');
        $this->load->model('googleads/googleads_log_m');
	}

    public function handle()
    {
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        $queue          = $this->config->item('googleads', 'amqps_queues');

        $connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        
        $channel = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $callback = function ($msg) use($amqps_host, $amqps_port, $amqps_user, $amqps_password) {

            echo ' [x] Received ', $msg->body, "\n";

            $args = json_decode($msg->body);
            $args = wp_parse_args($args, [
                'action' => null,
                'event' => null,
                'contractId' => null,
            ]);

            switch ($args['event']) {
                        
                case 'googleads.daily_sms' :

                    $_connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);

                    $_queue     = 'googleads.report.daily_sms';
                    $_channel 	= $_connection->channel();
                    $_channel->queue_declare($_queue, false, true, false, false);

                    $_message = new AMQPMessage( json_encode($args), array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT) );
                    $_channel->basic_publish($_message, '', $_queue);

                    $_channel->close();
		            $_connection->close();

                    break;

                case 'googleads.cron.alert' :

                    $_connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);

                    $_queue     = 'googleads.data.sync';
                    $_channel 	= $_connection->channel();
                    $_channel->queue_declare($_queue, false, true, false, false);

                    $_message = new AMQPMessage( json_encode($args), array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT) );
                    $_channel->basic_publish($_message, '', $_queue);

                    $_channel->close();
		            $_connection->close();
                    break;

                default:
                    # code...
                    break;
            }

            $msg->ack();
        };

        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($queue, '', false, false, false, false, $callback);

        while ($channel->is_open()) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();   
    }

    public function sync_data()
    {
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        $queue          = 'googleads.data.sync';

        $connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        
        $channel = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $callback = function ($msg) {

            echo ' [x] Received ', $msg->body, "\n";

            $args = json_decode($msg->body);
            $args = wp_parse_args($args, [
                'action' => null,
                'event' => null,
                'contractId' => null,
            ]);

            $this->load->model('googleads/googleads_m');

            try
            {
                $contract = (new googleads_m())->set_contract($args['contractId']);
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();
            }
            catch (Exception $e)
            {
                trigger_error($e->getMessage());
            }

            $msg->ack();
        };

        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($queue, '', false, false, false, false, $callback);

        while ($channel->is_open()) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();   
    }

    public function daily_sms()
    {
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        $queue          = 'googleads.report.daily_sms';

        $connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        $channel = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $callback = function ($msg) {

            echo ' [x] Received ', $msg->body, "\n";

            $args = json_decode($msg->body);
            $args = wp_parse_args($args, [
                'action' => null,
                'event' => null,
                'contractId' => null,
            ]);

            $hasSent = $this->googleads_log_m
            ->where('term_id', $args['contractId'])
            ->where('log_time_create >= curdate()')
            ->where('log_time_create < curdate() + interval 1 day')
            ->where('log_type', 'googleads-report-sms')
            ->where('log_status >', 0)
            ->count_by() > 0;

            /** Dont't Send if this messages is excuted lately */
            if($hasSent)
            {
                $msg->ack();
                return true;
            }

            $result = false;
            $message = null;

            try
            {
                $event      = new CustomerDailyClicks(['contract' => $args['contractId']]);
                $result     = $event->send();
                $message    = serialize($result);
            }
            catch(\Exception $e)
            {
                $result     = false;
                $message    = $e->getMessage();
            }

            $msg->ack();
        };

        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($queue, '', false, false, false, false, $callback);

        while ($channel->is_open()) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();   
    }
}
/* End of file ErpNotificationsWorker.php */
/* Location: ./application/controllers/ErpNotificationsWorker.php */