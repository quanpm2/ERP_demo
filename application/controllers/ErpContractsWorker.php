<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Notifications\Erp\Reports\Ads\Googleads\CustomerInsightReportWeekly\Email as WeeklyReportEmail;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class ErpContractsWorker extends Public_Controller 
{
	public function __construct() 
	{
        parent::__construct();
        $this->load->config('amqps');
        $this->load->model('contract/base_contract_m');
        $this->load->model('googleads/googleads_log_m');
        $this->load->model('googleads/googleads_m');
        $this->load->model('facebookads/facebookads_m');
        $this->load->model('contract/contract_m');
        $this->load->model('contract/receipt_m');
        $this->load->model('term_posts_m');
	}

    public function migrate_contracts_has_dollar_exchange_meta()
    {
        $metadata = $this->termmeta_m->where('meta_key', 'dollar_exchange_rate')->get_all();

        foreach($metadata as $row)
        {
            $_v = (int) get_term_meta_value($row->term_id, "exchange_rate_usd_to_vnd");
            $_v OR update_term_meta($row->term_id, 'exchange_rate_usd_to_vnd', $row->meta_value);
        }
    }

    public function handle()
    {
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');

        $amqps_queues = $this->config->item('internal_queue', 'amqps_queues');
        $queue = $amqps_queues['contract_events'];
        
        $connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);
        
        $channel = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $callback = function ($msg) {

            echo ' [x] Received ', $msg->body, "\n";

            $args = json_decode($msg->body);
            $args = wp_parse_args($args, [
                'action' => null,
                'event' => null,
                'paymentId' => null,
                'contract_id' => null
            ]);

            switch ($args['event']) {
                case 'contract_payment.sync.metadata':
                    $this->compute_payment_metadata($args['paymentId'], $args['log_id'] ?? 0);
                    break;
                    
                case 'migrate.receipt.meta_vat':
                    $this->migrate_receipts_vat();
                    break;

                case 'contract_payment.sync.service_fee':
                    $this->compute_service_fee_metadata($args['contract_id']);
                    break;
                
                case 'contract_payment.recalc.metadata':
                    $this->recalc_receipt_contract($args['contract_id'], $args['log_id'] ?? 0);
                    break;
                
                case 'contract_chain.googleads.recalc':
                    $this->recalc_googleads_contract_chain($args['contract_id']);
                    break;

                case 'contract_chain.facebookads.recalc':
                    $this->recalc_facebookads_contract_chain($args['contract_id']);
                    break;

                case 'contract_chain.tiktokads.recalc':
                    $this->recalc_tiktokads_contract_chain($args['contract_id']);
                    break;
                
                default:
                    # code...
                    break;
            }

            $msg->ack();
        };

        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($queue, '', false, false, false, false, $callback);

        while ($channel->is_open()) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();   
    }


    /**
     * Tính toán các metadata của payment
     * 
     * @param mixed $paymentId
     * 
     * @return [type]
     */
    public function compute_payment_metadata($paymentId, $log_id = 0)
    {
        $paymentId = (int) $paymentId;
        if(empty($paymentId)) return false;

        $receipt = $this->receipt_m->get($paymentId);
        if( ! $receipt) return false;

        if('paid' != $receipt->post_status) return TRUE;

        $contract = $this->term_posts_m->get_post_terms($paymentId, $this->contract_m->getTypes());
        $contract AND $contract = array_filter(array_values($contract));
        if( ! empty($contract))
        {
            $contract = array_merge(...$contract);
            $contract = reset($contract);
        }

        $_receipt = (new receipt_m())->forceFill( (array) $receipt);
        $contract = $_receipt->contract();

        $_instance = $_receipt->get_instance();

        $vat_amount     = $_instance->calc_vat_amount();
        update_post_meta($_receipt->post_id, 'vat_amount', $vat_amount);

        $actual_budget  = $_instance->calc_actual_budget();
        update_post_meta($_receipt->post_id, 'actual_budget', $actual_budget);

        $fct_amount     = $_instance->calc_fct();
        update_post_meta($_receipt->post_id, 'fct_amount', $fct_amount);

        $deal_reduction_fee = (int) $_instance->calc_deal_reduction_fee();
        update_post_meta($_receipt->post_id, 'deal_reduction_fee', $deal_reduction_fee);
        
        $service_fee    = $_instance->calc_service_fee();
        update_post_meta($_receipt->post_id, 'service_fee', ($deal_reduction_fee > 0 ? max([ $service_fee - $deal_reduction_fee, 0]) : $service_fee));

        $service_provider_tax = $_instance->calc_service_provider_tax();
        update_post_meta($_receipt->post_id, 'service_provider_tax', $service_provider_tax);

        $this->contract_m->set_contract($contract);
        $this->contract_m->sync_all_amount();

        if($log_id > 0) $this->log_m->update($log_id, [ 'log_status' => 1 ]);
        else $this->log_m->update_by([ 'log_type' => 'queue-receipt-sync-metadata', 'log_content' => $paymentId, 'log_status' => 0 ], [ 'log_status' => 1 ]);
        
        log_message('debug', "Receipt #{$paymentId} of Contract #{$contract->term_id} is updated.");

        return TRUE;
    }

    public function contract_data_compare()
	{
		$include = 0;
		$force_refresh = true;
		$show_all = false;

		if( ! empty($include)) $this->googleads_m->where('term_id', $include);

		$contracts = $this->googleads_m
		->set_term_type()
		->order_by('term_id', 'desc')
		->where_not_in('term_status', [ 'draft', 'unverified' ])
		->get_all();

		$data = [];

		foreach($contracts as $contract)
		{
			$actual_budget = (int) get_term_meta_value($contract->term_id, 'actual_budget');
			$actual_budget_payments = 0;
			$posts = $this->term_posts_m->get_term_posts($contract->term_id, ['receipt_payment', 'receipt_payment_on_behaft']);
			$posts AND $posts = array_filter($posts, function($x){
				return $x->post_status == 'paid';
			});

			$postIds = [];
			if( ! empty($posts))
			{
				$postIds = array_column($posts, 'post_id');
				$actual_budget_payments = array_sum(array_map(function($x){
					return (double) get_post_meta_value($x->post_id, 'actual_budget');
				}, $posts));
			}

			$item = array(
				'contract_code' => get_term_meta_value($contract->term_id, 'contract_code'),
				'typeOfService' => get_term_meta_value($contract->term_id, 'typeOfService'),
				'actual_budget' => round((int) $actual_budget, -2),
				'actual_budget_payments' => round((int) $actual_budget_payments, -2),
				'postIds' => $postIds,
				'term_id' => $contract->term_id
			);

			$item['same'] = $item['actual_budget'] == $item['actual_budget_payments'];
			$data[] = $item;
		}

		echo div(array_sum(array_column($data, 'same')), count($data)) * 100 . '%';

		$changesPostIds = [];
		$show_all OR $data = array_filter($data, function($x) use (&$changesPostIds, $include){			
			if(empty($x['same']))
			{
				$changesPostIds = array_merge($x['postIds'], $changesPostIds);
			}

			if( ! empty($include) && $x['term_id'] == $include) return true; 

			return empty($x['same']);
		});

		if( $force_refresh &&  ! empty($changesPostIds))
		{
			$this->log_m
				->where_in('log_content', $changesPostIds)
				->where('log_type', 'queue-receipt-sync-metadata')
				->update_by([  'log_status' => 0 ]);
		}

        echo 'ok';

		// $data = array_map(function($x){

		// 	$x['actual_budget'] = [
		// 		// 'data' => currency_numberformat($x['actual_budget'], ''),
		// 		'data' => $x['actual_budget'],
		// 		'align' => 'right'
		// 	];

		// 	$x['actual_budget_payments'] = [
		// 		// 'data' => currency_numberformat($x['actual_budget_payments'], ''),
		// 		'data' => $x['actual_budget_payments'],
		// 		'align' => 'right'
		// 	];
		// 	return $x;
		// }, $data);

		// echo $this->table->generate($data);dd
	}

    public function migrate_receipts_vat()
    {
        $this->load->model('contract/receipt_m');

        $receipts = $this->receipt_m
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->join('term', 'term.term_id = term_posts.term_id')
        ->where('post_status', 'paid')
        ->where_in('post_type', ['receipt_payment', 'receipt_caution', 'receipt_payment_on_behaft'])
        ->where_in('term.term_type', $this->contract_m->getTypes())
        ->order_by('posts.post_id', 'desc')
        ->get_all();

        foreach($receipts as $receipt){
            echo 'Migrating receipt #' . $receipt->post_id . "\n";

            $vat = get_post_meta_value($receipt->post_id, 'vat');
            if(!empty($vat)){
                continue;
            }

            $vat = get_term_meta_value($receipt->term_id, 'vat');
            if(empty($vat)){
                update_post_meta($receipt->post_id, 'vat', 0);
                log_message('debug', $this->db->last_query());

                continue;
            }

            update_post_meta($receipt->post_id, 'vat', $vat);
            log_message('debug', $this->db->last_query());
        }
    }

    public function compute_service_fee_metadata($contract_id)
    {
        $service_fee_payment_type = get_term_meta_value($contract_id, 'service_fee_payment_type');
        if('range' == $service_fee_payment_type){
            $this->recalc_receipt_contract($contract_id);
            return TRUE;
        }

        $this->contract_m->set_contract($contract_id);
        $service_fee = $this->contract_m->get_behaviour_m()->calc_payment_service_fee();
        update_term_meta($contract_id, 'service_fee', $service_fee);

        log_message('debug', "compute_service_fee_metadata | {$contract_id} | SUCCESS | Contract compute service fee completed.");
        return TRUE;
    }

    public function recalc_receipt_contract($contract_id, $log_id = 0){
        $service_fee_payment_type = get_term_meta_value($contract_id, 'service_fee_payment_type');
        if('range' != $service_fee_payment_type){
            if($log_id > 0) $this->log_m->update($log_id, [ 'log_status' => -1 ]);
            else $this->log_m->update_by([ 'log_type' => 'queue-receipt-recalc-metadata', 'log_content' => $contract_id, 'log_status' => 0 ], [ 'log_status' => -1 ]);
        
            log_message('debug', "Contract #{$contract_id} is not range contract");

            return FALSE;
        }

        // Get receipt by contract
        $receipts = $this->receipt_m
        ->join('term_posts', 'term_posts.post_id = posts.post_id')
        ->join('term', 'term.term_id = term_posts.term_id')
        ->where('post_status', 'paid')
        ->where_in('post_type', ['receipt_payment', 'receipt_caution', 'receipt_payment_on_behaft'])
        ->where('term.term_id', $contract_id)
        ->order_by('posts.post_id', 'desc')
        ->select('term.term_id, posts.post_id')
        ->get_all();
        if(empty($receipts)){
            if($log_id > 0) $this->log_m->update($log_id, [ 'log_status' => 1 ]);
            else $this->log_m->update_by([ 'log_type' => 'queue-receipt-recalc-metadata', 'log_content' => $contract_id, 'log_status' => 0 ], [ 'log_status' => 1 ]);
        
            log_message('debug', "Contract #{$contract_id} has been updated.");

            return TRUE;
        }

        // Get service_fee_rate_actual
        $this->contract_m->set_contract($contract_id);
        $service_fee_rate_actual = $this->contract_m->get_behaviour_m()->calc_range_service_fee_rate_actual();
        $service_fee_rate_actual *= 100;

        // Update receipt
        foreach($receipts as $receipt){
            update_post_meta($receipt->post_id, 'service_fee_rate', $service_fee_rate_actual);
            $this->compute_payment_metadata($receipt->post_id);
        }

        if($log_id > 0) $this->log_m->update($log_id, [ 'log_status' => 1 ]);
        else $this->log_m->update_by([ 'log_type' => 'queue-receipt-recalc-metadata', 'log_content' => $contract_id, 'log_status' => 0 ], [ 'log_status' => 1 ]);
        
        log_message('debug', "Contract #{$contract_id} has been updated.");

        return TRUE;
    }
    
    /**
     * recalc_googleads_contract_chain
     *
     * @param  mixed $contract_id
     * @return void
     */
    private function recalc_googleads_contract_chain($contract_id){
        // Setup chains
        $contract_id_chain = [$contract_id];
        
        $prev_contract_id = get_term_meta_value($contract_id, 'previousContractId');
        if(!empty($prev_contract_id)) array_unshift($contract_id_chain, $prev_contract_id);

        $is_conflict = FALSE;
        $is_stop = FALSE;
        $_contract_id = $contract_id;
        while(!$is_stop)
        {
            $next_contract_id = get_term_meta_value($_contract_id, 'nextContractId');
            if(empty($next_contract_id)) 
            {
                $is_stop = TRUE;
                
                break;
            }

            if(in_array($next_contract_id, $contract_id_chain))
            {
                $is_conflict = TRUE;
                $is_stop = TRUE;
                
                break;
            }

            array_push($contract_id_chain, $next_contract_id);
            $_contract_id = $next_contract_id;
        }
        
        if($is_conflict) 
        {
            log_message('error', "Contract #{$contract_id} has conflicted with another contract in chain.");
            
            return FALSE;
        }

        // Recompute chain
        $contracts = $this->googleads_m->set_term_type()
            ->select("term.term_id AS contract_id")
            ->select('MAX(IF(contract_metadata.meta_key = "actual_budget", contract_metadata.meta_value, NULL)) AS actual_budget')
            ->select('MAX(IF(contract_metadata.meta_key = "advertise_start_time", contract_metadata.meta_value, NULL)) AS advertise_start_time')
            ->select('MAX(IF(contract_metadata.meta_key = "advertise_end_time", contract_metadata.meta_value, NULL)) AS advertise_end_time')
            ->select("MAX(IF(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, NULL)) AS spend")
            ->select("balance_spend.post_content AS balance_spend_value")
            ->select("balance_spend.comment_status AS balance_spend_type")
            ->select("adaccount.term_id AS adaccount_id")
            ->select("COALESCE(ads_segment.post_type, balance_spend.post_type) AS segment_type")
            ->select("COALESCE(ads_segment.post_id, balance_spend.post_id) AS ads_segment_id")
            ->select("ads_segment.start_date AS ads_segment_start")
            ->select("ads_segment.end_date AS ads_segment_end")

            ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
            ->join('posts AS balance_spend', 'balance_spend.post_id = tp_contract_ads_segment.post_id  AND balance_spend.post_type = "balance_spend"', 'LEFT')
            ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'LEFT')  
            ->join('term_posts AS tp_segment_adaccount', 'tp_segment_adaccount.post_id = ads_segment.post_id', 'LEFT')  
            ->join('term AS adaccount', 'tp_segment_adaccount.term_id = adaccount.term_id AND adaccount.term_type = "mcm_account"', 'LEFT')  
            ->join('termmeta AS adaccount_metadata', 'adaccount_metadata.term_id = adaccount.term_id AND meta_key = "source"', 'LEFT')  
            ->join('term_posts AS tp_adaccount_insights', 'tp_adaccount_insights.term_id = adaccount.term_id', 'LEFT')  
            ->join('posts AS insights', 'tp_adaccount_insights.post_id = insights.post_id AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) AND insights.start_date <= if(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) AND insights.post_type = "insight_segment" AND insights.post_name = "day"', 'LEFT')
            ->join('postmeta AS insight_metadata', 'insight_metadata.post_id = insights.post_id AND insight_metadata.meta_key = "spend"', 'LEFT')
            ->join('termmeta AS contract_metadata', 'contract_metadata.term_id = term.term_id AND contract_metadata.meta_key IN ("actual_budget", "advertise_start_time", "advertise_end_time")', 'LEFT')
            
            ->where('( adaccount.term_id > 0 OR balance_spend.post_id > 0)')
            ->group_by('term.term_id, ads_segment.post_id, adaccount.term_id, insights.post_id, balance_spend.post_id')

            ->where_in('term.term_id', $contract_id_chain)
            ->as_array()
            ->get_all();

        $contracts_group_by_id = array_group_by($contracts, 'contract_id');
        $contracts_group_by_id = array_map(function($item){
            $instance = reset($item);

            $contract_spend_group_by_segment_type = array_group_by($item, 'segment_type');
            
            // Reduce unique $insights
            $insights = array_filter($contract_spend_group_by_segment_type['ads_segment'] ?? [], function($item){
                return 0 != $item['adaccount_id'];
            });
            $insights = array_reduce($insights, function($result, $insight) {
                $segment_id = $insight['ads_segment_id'];
                $ad_account_id = $insight['adaccount_id'];

                // Filter overlap segment
                $overlap_segment = array_filter($result, function($_segment) use ($ad_account_id, $segment_id){
                    // Ignore current segment
                    if($_segment['ads_segment_id'] == $segment_id)
                    {
                        return FALSE;
                    }

                    return $_segment['adaccount_id'] == $ad_account_id;
                }); 
                if(empty($overlap_segment))
                {
                    $result[] = $insight;
                    return $result;
                }

                return $result;
            }, []);
            $spend = round(array_sum(array_column($insights, 'spend')));

            $_direct_balance_spend = array_filter($item, function($_spend){ return 'direct' == $_spend['balance_spend_type']; });
            $direct_balance_spend = array_sum(array_column($_direct_balance_spend, 'balance_spend_value'));

            $contract_id = $instance['contract_id'];
            $actual_budget = $instance['actual_budget'];
            $advertise_start_time = $instance['advertise_start_time'];
            $advertise_end_time = $instance['advertise_end_time'];

            $actual_result = get_term_meta_value($contract_id, 'actual_result');

            $result = [
                'contract_id' => $contract_id,
                'contract_code' => get_term_meta_value($contract_id, 'contract_code'),
                'spend' => (int)$spend,
                'actual_result' => (int)$actual_result,
                'balance_spend_value' => (int)$direct_balance_spend,
                'actual_budget' => (int)$actual_budget,
                'advertise_start_time' => $advertise_start_time,
                'advertise_end_time' => $advertise_end_time,
            ];
            return $result;
        }, $contracts_group_by_id);

        // Pick 2 contract_id
        $origination_id = array_shift($contract_id_chain);
        $destination_id = array_shift($contract_id_chain);

        $this->load->model('balance_spend_m');
        $this->load->model('googleads/googleads_m');
        $this->load->model('googleads/googleads_behaviour_m');

        $is_end = FALSE;
        while(!$is_end)
        {
            $origination_data = $contracts_group_by_id[$origination_id] ?? NULL;
            $destination_data = $contracts_group_by_id[$destination_id] ?? NULL;
            if(empty($origination_data) || empty($destination_data))
            {
                $is_end = TRUE;
                continue;
            }

            // Calc destination balanceBudgetReceived
            $origination_balance_budget_received = (int) get_term_meta_value($origination_id, 'balanceBudgetReceived');
            $destination_balance_budget_received = (int) $origination_data['actual_budget'] - ((int) $origination_data['spend'] + (int) $origination_data['balance_spend_value'] + $origination_balance_budget_received);

            // Give (join to)
            $old_given_balance_spend_items = $this->balance_spend_m
            ->set_post_type()
            ->select('posts.post_id')
            ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$origination_id})")
            ->where('posts.comment_status', 'auto')
            ->where('posts.post_title', 'join_command')
            ->as_array()
            ->get_all();
            $old_given_balance_spend_items = array_filter($old_given_balance_spend_items, function($item){
                $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                return 'to' == $join_direction;
            });

            if( ! empty($old_given_balance_spend_items))
            {
                $this->term_posts_m->delete_term_posts($origination_id, array_column($old_given_balance_spend_items, 'post_id'));
                $this->balance_spend_m->delete_many(array_column($old_given_balance_spend_items, 'post_id'));
            }

            update_term_meta($origination_id, 'nextContractId', $destination_id);
            update_term_meta($origination_id, 'balanceBudgetAddTo', $destination_balance_budget_received);
            $post_excerpt = 'Nối đến HĐ ' . get_term_meta_value($destination_id, 'contract_code');
            $balance_spend_join_to_date = get_term_meta_value($origination_id, 'advertise_end_time');
            $balance_spend_join_to_id = $this->balance_spend_m->insert([
                'post_type' => $this->balance_spend_m->post_type,
                'comment_status' => 'auto',
                'post_title' => 'join_command',
                'post_status' => 'publish',
                'post_content' => $destination_balance_budget_received,
                'post_excerpt' => $post_excerpt,
                'start_date' => $balance_spend_join_to_date,
                'end_date' => $balance_spend_join_to_date,
            ]);
            update_post_meta($balance_spend_join_to_id, 'join_direction', 'to');
            $this->term_posts_m->set_term_posts( $origination_id, [ $balance_spend_join_to_id ], $this->balance_spend_m->post_type);
            $this->log_m->insert([
                'log_type'        => 'joinContracts',
                'log_status'      => 1,
                'term_id'         => $origination_id,
                'log_content'     => json_encode(
                    [
                        'destination_id' => $destination_id, 
                        'given' => $destination_balance_budget_received
                    ]
                ),
                'user_id'         => $this->admin_m->id
            ]);

            try
            {
                $contract = (new googleads_m())->set_contract($origination_id);
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();
            }
            catch (Exception $e)
            {
                log_message('error', "[ErpContractsWorker::recalc_googleads_contract_chain] Sync amount #{$origination_id} error. " . json_encode(['error' => $e->getMessage()]));
            }

            // Receive (join from)
            $old_received_balance_spend_items = $this->balance_spend_m
            ->set_post_type()
            ->select('posts.post_id')
            ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$destination_id})")
            ->where('posts.comment_status', 'auto')
            ->where('posts.post_title', 'join_command')
            ->as_array()
            ->get_all();
            $old_received_balance_spend_items = array_filter($old_received_balance_spend_items, function($item){
                $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                return 'from' == $join_direction;
            });

            if( ! empty($old_received_balance_spend_items))
            {
                $this->term_posts_m->delete_term_posts($destination_id, array_column($old_received_balance_spend_items, 'post_id'));
                $this->balance_spend_m->delete_many(array_column($old_received_balance_spend_items, 'post_id'));
            }

            update_term_meta($destination_id, 'previousContractId', $origination_id);
            update_term_meta($destination_id, 'balanceBudgetReceived', (-1) * $destination_balance_budget_received);
            $post_excerpt = 'Nối từ HĐ ' . get_term_meta_value($origination_id, 'contract_code');
            $balance_spend_join_from_date = get_term_meta_value($destination_id, 'advertise_start_time');
            $balance_spend_join_from_id = $this->balance_spend_m->insert([
                'post_type' => $this->balance_spend_m->post_type,
                'comment_status' => 'auto',
                'post_title' => 'join_command',
                'post_status' => 'publish',
                'post_content' => (-1) * $destination_balance_budget_received,
                'post_excerpt' => $post_excerpt,
                'start_date' => $balance_spend_join_from_date,
                'end_date' => $balance_spend_join_from_date,
            ]);
            update_post_meta($balance_spend_join_from_id, 'join_direction', 'from');
            $this->term_posts_m->set_term_posts( $destination_id, [ $balance_spend_join_from_id ], $this->balance_spend_m->post_type);
            
            $this->log_m->insert([
                'log_type'        => 'joinContracts',
                'log_status'      => 1,
                'term_id'         => $destination_id,
                'log_content'     => json_encode(
                    [
                        'origination_id' => $origination_id, 
                        'received' => (-1) * $destination_balance_budget_received
                    ]
                ),
                'user_id'         => $this->admin_m->id
            ]);

            try
            {
                $contract = (new googleads_m())->set_contract($destination_id);
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();
            }
            catch (Exception $e)
            {
                log_message('error', "[ErpContractsWorker::recalc_googleads_contract_chain] Sync amount #{$destination_id} error. " . json_encode(['error' => $e->getMessage()]));
            }

            // Following chain
            if(empty($contract_id_chain)) $is_end = TRUE;
            else 
            {
                $origination_id = $destination_id;
                $destination_id = array_shift($contract_id_chain);
            }
        }
    }

    /**
     * recalc_facebookads_contract_chain
     *
     * @param  mixed $contract_id
     * @return void
     */
    private function recalc_facebookads_contract_chain($contract_id){
        // Setup chains
        $contract_id_chain = [$contract_id];
        
        $prev_contract_id = get_term_meta_value($contract_id, 'previousContractId');
        if(!empty($prev_contract_id)) array_unshift($contract_id_chain, $prev_contract_id);

        $is_conflict = FALSE;
        $is_stop = FALSE;
        $_contract_id = $contract_id;
        while(!$is_stop)
        {
            $next_contract_id = get_term_meta_value($_contract_id, 'nextContractId');
            if(empty($next_contract_id)) 
            {
                $is_stop = TRUE;
                
                break;
            }

            if(in_array($next_contract_id, $contract_id_chain))
            {
                $is_conflict = TRUE;
                $is_stop = TRUE;
                
                break;
            }

            array_push($contract_id_chain, $next_contract_id);
            $_contract_id = $next_contract_id;
        }
        
        if($is_conflict) 
        {
            log_message('error', "Contract #{$contract_id} has conflicted with another contract in chain.");
            
            return FALSE;
        }

        // Recompute chain
        $contracts = $this->facebookads_m->set_term_type()
            ->select("term.term_id AS contract_id")
            ->select('MAX(IF(contract_metadata.meta_key = "actual_budget", contract_metadata.meta_value, NULL)) AS actual_budget')
            ->select('MAX(IF(contract_metadata.meta_key = "advertise_start_time", contract_metadata.meta_value, NULL)) AS advertise_start_time')
            ->select('MAX(IF(contract_metadata.meta_key = "advertise_end_time", contract_metadata.meta_value, NULL)) AS advertise_end_time')
            ->select("MAX(IF(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, NULL)) AS spend")
            ->select("balance_spend.post_content AS balance_spend_value")
            ->select("balance_spend.comment_status AS balance_spend_type")
            ->select("adaccount.term_id AS adaccount_id")
            ->select("COALESCE(ads_segment.post_type, balance_spend.post_type) AS segment_type")
            ->select("COALESCE(ads_segment.post_id, balance_spend.post_id) AS ads_segment_id")
            ->select("ads_segment.start_date AS ads_segment_start")
            ->select("ads_segment.end_date AS ads_segment_end")

            ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
            ->join('posts AS balance_spend', 'balance_spend.post_id = tp_contract_ads_segment.post_id  AND balance_spend.post_type = "balance_spend"', 'LEFT')
            ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'LEFT')  
            ->join('term_posts AS tp_segment_adaccount', 'tp_segment_adaccount.post_id = ads_segment.post_id', 'LEFT')  
            ->join('term AS adaccount', 'tp_segment_adaccount.term_id = adaccount.term_id AND adaccount.term_type = "adaccount"', 'LEFT')  
            ->join('termmeta AS adaccount_metadata', 'adaccount_metadata.term_id = adaccount.term_id AND meta_key = "source"', 'LEFT')  
            ->join('term_posts AS tp_adaccount_insights', 'tp_adaccount_insights.term_id = adaccount.term_id', 'LEFT')  
            ->join('posts AS insights', 'tp_adaccount_insights.post_id = insights.post_id AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) AND insights.start_date <= if(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) AND insights.post_type = "insight_segment" AND insights.post_name = "day"', 'LEFT')
            ->join('postmeta AS insight_metadata', 'insight_metadata.post_id = insights.post_id AND insight_metadata.meta_key = "spend"', 'LEFT')
            ->join('termmeta AS contract_metadata', 'contract_metadata.term_id = term.term_id AND contract_metadata.meta_key IN ("actual_budget", "advertise_start_time", "advertise_end_time")', 'LEFT')
            
            ->where('( adaccount.term_id > 0 OR balance_spend.post_id > 0)')
            ->group_by('term.term_id, ads_segment.post_id, adaccount.term_id, insights.post_id, balance_spend.post_id')

            ->where_in('term.term_id', $contract_id_chain)
            ->as_array()
            ->get_all();

        $contracts_group_by_id = array_group_by($contracts, 'contract_id');
        $contracts_group_by_id = array_map(function($item){
            $instance = reset($item);

            $contract_spend_group_by_segment_type = array_group_by($item, 'segment_type');
            
            // Reduce unique $insights
            $insights = array_filter($contract_spend_group_by_segment_type['ads_segment'] ?? [], function($item){
                return 0 != $item['adaccount_id'];
            });
            $insights = array_reduce($insights, function($result, $insight) {
                $segment_id = $insight['ads_segment_id'];
                $ad_account_id = $insight['adaccount_id'];

                // Filter overlap segment
                $overlap_segment = array_filter($result, function($_segment) use ($ad_account_id, $segment_id){
                    // Ignore current segment
                    if($_segment['ads_segment_id'] == $segment_id)
                    {
                        return FALSE;
                    }

                    return $_segment['adaccount_id'] == $ad_account_id;
                }); 
                if(empty($overlap_segment))
                {
                    $result[] = $insight;
                    return $result;
                }

                return $result;
            }, []);
            $spend = round(array_sum(array_column($insights, 'spend')));

            $_direct_balance_spend = array_filter($item, function($_spend){ return 'direct' == $_spend['balance_spend_type']; });
            $direct_balance_spend = array_sum(array_column($_direct_balance_spend, 'balance_spend_value'));

            $contract_id = $instance['contract_id'];
            $actual_budget = $instance['actual_budget'];
            $advertise_start_time = $instance['advertise_start_time'];
            $advertise_end_time = $instance['advertise_end_time'];

            $actual_result = get_term_meta_value($contract_id, 'actual_result');

            $result = [
                'contract_id' => $contract_id,
                'contract_code' => get_term_meta_value($contract_id, 'contract_code'),
                'spend' => (int)$spend,
                'actual_result' => (int)$actual_result,
                'balance_spend_value' => (int)$direct_balance_spend,
                'actual_budget' => (int)$actual_budget,
                'advertise_start_time' => $advertise_start_time,
                'advertise_end_time' => $advertise_end_time,
            ];
            return $result;
        }, $contracts_group_by_id);

        // Pick 2 contract_id
        $origination_id = array_shift($contract_id_chain);
        $destination_id = array_shift($contract_id_chain);

        $this->load->model('balance_spend_m');
        $this->load->model('facebookads/facebookads_m');
        $this->load->model('facebookads/facebookads_behaviour_m');

        $is_end = FALSE;
        while(!$is_end)
        {
            $origination_data = $contracts_group_by_id[$origination_id] ?? NULL;
            $destination_data = $contracts_group_by_id[$destination_id] ?? NULL;
            if(empty($origination_data) || empty($destination_data))
            {
                $is_end = TRUE;
                continue;
            }

            // Calc destination balanceBudgetReceived
            $origination_balance_budget_received = (int) get_term_meta_value($origination_id, 'balanceBudgetReceived');
            $destination_balance_budget_received = (int) $origination_data['actual_budget'] - ((int) $origination_data['spend'] + (int) $origination_data['balance_spend_value'] + $origination_balance_budget_received);

            // Give (join to)
            $old_given_balance_spend_items = $this->balance_spend_m
            ->set_post_type()
            ->select('posts.post_id')
            ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$origination_id})")
            ->where('posts.comment_status', 'auto')
            ->where('posts.post_title', 'join_command')
            ->as_array()
            ->get_all();
            $old_given_balance_spend_items = array_filter($old_given_balance_spend_items, function($item){
                $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                return 'to' == $join_direction;
            });

            if( ! empty($old_given_balance_spend_items))
            {
                $this->term_posts_m->delete_term_posts($origination_id, array_column($old_given_balance_spend_items, 'post_id'));
                $this->balance_spend_m->delete_many(array_column($old_given_balance_spend_items, 'post_id'));
            }

            update_term_meta($origination_id, 'nextContractId', $destination_id);
            update_term_meta($origination_id, 'balanceBudgetAddTo', $destination_balance_budget_received);
            $post_excerpt = 'Nối đến HĐ ' . get_term_meta_value($destination_id, 'contract_code');
            $balance_spend_join_to_date = get_term_meta_value($origination_id, 'advertise_end_time');
            $balance_spend_join_to_id = $this->balance_spend_m->insert([
                'post_type' => $this->balance_spend_m->post_type,
                'comment_status' => 'auto',
                'post_title' => 'join_command',
                'post_status' => 'publish',
                'post_content' => $destination_balance_budget_received,
                'post_excerpt' => $post_excerpt,
                'start_date' => $balance_spend_join_to_date,
                'end_date' => $balance_spend_join_to_date,
            ]);
            update_post_meta($balance_spend_join_to_id, 'join_direction', 'to');
            $this->term_posts_m->set_term_posts( $origination_id, [ $balance_spend_join_to_id ], $this->balance_spend_m->post_type);
            $this->log_m->insert([
                'log_type'        => 'joinContracts',
                'log_status'      => 1,
                'term_id'         => $origination_id,
                'log_content'     => json_encode(
                    [
                        'destination_id' => $destination_id, 
                        'given' => $destination_balance_budget_received
                    ]
                ),
                'user_id'         => $this->admin_m->id
            ]);

            try
            {
                $contract = (new facebookads_m())->set_contract($origination_id);
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();
            }
            catch (Exception $e)
            {
                log_message('error', "[ErpContractsWorker::recalc_facebookads_contract_chain] Sync amount #{$origination_id} error. " . json_encode(['error' => $e->getMessage()]));
            }

            // Receive (join from)
            $old_received_balance_spend_items = $this->balance_spend_m
            ->set_post_type()
            ->select('posts.post_id')
            ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$destination_id})")
            ->where('posts.comment_status', 'auto')
            ->where('posts.post_title', 'join_command')
            ->as_array()
            ->get_all();
            $old_received_balance_spend_items = array_filter($old_received_balance_spend_items, function($item){
                $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                return 'from' == $join_direction;
            });

            if( ! empty($old_received_balance_spend_items))
            {
                $this->term_posts_m->delete_term_posts($destination_id, array_column($old_received_balance_spend_items, 'post_id'));
                $this->balance_spend_m->delete_many(array_column($old_received_balance_spend_items, 'post_id'));
            }

            update_term_meta($destination_id, 'previousContractId', $origination_id);
            update_term_meta($destination_id, 'balanceBudgetReceived', (-1) * $destination_balance_budget_received);
            $post_excerpt = 'Nối từ HĐ ' . get_term_meta_value($origination_id, 'contract_code');
            $balance_spend_join_from_date = get_term_meta_value($destination_id, 'advertise_start_time');
            $balance_spend_join_from_id = $this->balance_spend_m->insert([
                'post_type' => $this->balance_spend_m->post_type,
                'comment_status' => 'auto',
                'post_title' => 'join_command',
                'post_status' => 'publish',
                'post_content' => (-1) * $destination_balance_budget_received,
                'post_excerpt' => $post_excerpt,
                'start_date' => $balance_spend_join_from_date,
                'end_date' => $balance_spend_join_from_date,
            ]);
            update_post_meta($balance_spend_join_from_id, 'join_direction', 'from');
            $this->term_posts_m->set_term_posts( $destination_id, [ $balance_spend_join_from_id ], $this->balance_spend_m->post_type);
            
            $this->log_m->insert([
                'log_type'        => 'joinContracts',
                'log_status'      => 1,
                'term_id'         => $destination_id,
                'log_content'     => json_encode(
                    [
                        'origination_id' => $origination_id, 
                        'received' => (-1) * $destination_balance_budget_received
                    ]
                ),
                'user_id'         => $this->admin_m->id
            ]);

            try
            {
                $contract = (new facebookads_m())->set_contract($destination_id);
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();
            }
            catch (Exception $e)
            {
                log_message('error', "[ErpContractsWorker::recalc_facebookads_contract_chain] Sync amount #{$destination_id} error. " . json_encode(['error' => $e->getMessage()]));
            }

            // Following chain
            if(empty($contract_id_chain)) $is_end = TRUE;
            else 
            {
                $origination_id = $destination_id;
                $destination_id = array_shift($contract_id_chain);
            }
        }
    }

    /**
     * recalc_tiktokads_contract_chain
     *
     * @param  mixed $contract_id
     * @return void
     */
    private function recalc_tiktokads_contract_chain($contract_id){
        $this->load->model('balance_spend_m');
        $this->load->model('tiktokads/tiktokads_m');
        $this->load->model('tiktokads/tiktokads_behaviour_m');

        // Setup chains
        $contract_id_chain = [$contract_id];
        
        $prev_contract_id = get_term_meta_value($contract_id, 'previousContractId');
        if(!empty($prev_contract_id)) array_unshift($contract_id_chain, $prev_contract_id);

        $is_conflict = FALSE;
        $is_stop = FALSE;
        $_contract_id = $contract_id;
        while(!$is_stop)
        {
            $next_contract_id = get_term_meta_value($_contract_id, 'nextContractId');
            if(empty($next_contract_id)) 
            {
                $is_stop = TRUE;
                
                break;
            }

            if(in_array($next_contract_id, $contract_id_chain))
            {
                $is_conflict = TRUE;
                $is_stop = TRUE;
                
                break;
            }

            array_push($contract_id_chain, $next_contract_id);
            $_contract_id = $next_contract_id;
        }
        
        if($is_conflict) 
        {
            log_message('error', "Contract #{$contract_id} has conflicted with another contract in chain.");
            
            return FALSE;
        }

        // Recompute chain
        $contracts = $this->tiktokads_m->set_term_type()
            ->select("term.term_id AS contract_id")
            ->select('MAX(IF(contract_metadata.meta_key = "actual_budget", contract_metadata.meta_value, NULL)) AS actual_budget')
            ->select('MAX(IF(contract_metadata.meta_key = "advertise_start_time", contract_metadata.meta_value, NULL)) AS advertise_start_time')
            ->select('MAX(IF(contract_metadata.meta_key = "advertise_end_time", contract_metadata.meta_value, NULL)) AS advertise_end_time')
            ->select("MAX(IF(insight_metadata.meta_key = 'spend', insight_metadata.meta_value, NULL)) AS spend")
            ->select("balance_spend.post_content AS balance_spend_value")
            ->select("balance_spend.comment_status AS balance_spend_type")
            ->select("adaccount.term_id AS adaccount_id")
            ->select("COALESCE(ads_segment.post_type, balance_spend.post_type) AS segment_type")
            ->select("COALESCE(ads_segment.post_id, balance_spend.post_id) AS ads_segment_id")
            ->select("ads_segment.start_date AS ads_segment_start")
            ->select("ads_segment.end_date AS ads_segment_end")

            ->join('term_posts AS tp_contract_ads_segment', 'tp_contract_ads_segment.term_id = term.term_id')
            ->join('posts AS balance_spend', 'balance_spend.post_id = tp_contract_ads_segment.post_id  AND balance_spend.post_type = "balance_spend"', 'LEFT')
            ->join('posts AS ads_segment', 'ads_segment.post_id = tp_contract_ads_segment.post_id AND ads_segment.post_type = "ads_segment"', 'LEFT')  
            ->join('term_posts AS tp_segment_adaccount', 'tp_segment_adaccount.post_id = ads_segment.post_id', 'LEFT')  
            ->join('term AS adaccount', 'tp_segment_adaccount.term_id = adaccount.term_id AND adaccount.term_type = "tiktok_adaccount"', 'LEFT')  
            ->join('termmeta AS adaccount_metadata', 'adaccount_metadata.term_id = adaccount.term_id AND meta_key = "source"', 'LEFT')  
            ->join('term_posts AS tp_adaccount_insights', 'tp_adaccount_insights.term_id = adaccount.term_id', 'LEFT')  
            ->join('posts AS insights', 'tp_adaccount_insights.post_id = insights.post_id AND insights.start_date >= UNIX_TIMESTAMP(FROM_UNIXTIME(ads_segment.start_date, "%Y-%m-%d 00:00:00")) AND insights.start_date <= if(ads_segment.end_date = 0 OR ads_segment.end_date IS NULL, UNIX_TIMESTAMP (), ads_segment.end_date) AND insights.post_type = "insight_segment" AND insights.post_name = "day"', 'LEFT')
            ->join('postmeta AS insight_metadata', 'insight_metadata.post_id = insights.post_id AND insight_metadata.meta_key = "spend"', 'LEFT')
            ->join('termmeta AS contract_metadata', 'contract_metadata.term_id = term.term_id AND contract_metadata.meta_key IN ("actual_budget", "advertise_start_time", "advertise_end_time")', 'LEFT')
            
            ->where('( adaccount.term_id > 0 OR balance_spend.post_id > 0)')
            ->group_by('term.term_id, ads_segment.post_id, adaccount.term_id, insights.post_id, balance_spend.post_id')

            ->where_in('term.term_id', $contract_id_chain)
            ->as_array()
            ->get_all();

        $contracts_group_by_id = array_group_by($contracts, 'contract_id');
        $contracts_group_by_id = array_map(function($item){
            $instance = reset($item);

            $contract_spend_group_by_segment_type = array_group_by($item, 'segment_type');
            
            // Reduce unique $insights
            $insights = array_filter($contract_spend_group_by_segment_type['ads_segment'] ?? [], function($item){
                return 0 != $item['adaccount_id'];
            });
            $insights = array_reduce($insights, function($result, $insight) {
                $segment_id = $insight['ads_segment_id'];
                $ad_account_id = $insight['adaccount_id'];

                // Filter overlap segment
                $overlap_segment = array_filter($result, function($_segment) use ($ad_account_id, $segment_id){
                    // Ignore current segment
                    if($_segment['ads_segment_id'] == $segment_id)
                    {
                        return FALSE;
                    }

                    return $_segment['adaccount_id'] == $ad_account_id;
                }); 
                if(empty($overlap_segment))
                {
                    $result[] = $insight;
                    return $result;
                }

                return $result;
            }, []);
            $spend = round(array_sum(array_column($insights, 'spend')));

            $_direct_balance_spend = array_filter($item, function($_spend){ return 'direct' == $_spend['balance_spend_type']; });
            $direct_balance_spend = array_sum(array_column($_direct_balance_spend, 'balance_spend_value'));

            $contract_id = $instance['contract_id'];
            $actual_budget = $instance['actual_budget'];
            $advertise_start_time = $instance['advertise_start_time'];
            $advertise_end_time = $instance['advertise_end_time'];

            $actual_result = get_term_meta_value($contract_id, 'actual_result');

            $result = [
                'contract_id' => $contract_id,
                'contract_code' => get_term_meta_value($contract_id, 'contract_code'),
                'spend' => (int)$spend,
                'actual_result' => (int)$actual_result,
                'balance_spend_value' => (int)$direct_balance_spend,
                'actual_budget' => (int)$actual_budget,
                'advertise_start_time' => $advertise_start_time,
                'advertise_end_time' => $advertise_end_time,
            ];
            return $result;
        }, $contracts_group_by_id);

        // Pick 2 contract_id
        $origination_id = array_shift($contract_id_chain);
        $destination_id = array_shift($contract_id_chain);

        $is_end = FALSE;
        while(!$is_end)
        {
            $origination_data = $contracts_group_by_id[$origination_id] ?? NULL;
            $destination_data = $contracts_group_by_id[$destination_id] ?? NULL;
            if(empty($origination_data) || empty($destination_data))
            {
                $is_end = TRUE;
                continue;
            }

            // Calc destination balanceBudgetReceived
            $origination_balance_budget_received = (int) get_term_meta_value($origination_id, 'balanceBudgetReceived');
            $destination_balance_budget_received = (int) $origination_data['actual_budget'] - ((int) $origination_data['spend'] + (int) $origination_data['balance_spend_value'] + $origination_balance_budget_received);

            // Give (join to)
            $old_given_balance_spend_items = $this->balance_spend_m
            ->set_post_type()
            ->select('posts.post_id')
            ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$origination_id})")
            ->where('posts.comment_status', 'auto')
            ->where('posts.post_title', 'join_command')
            ->as_array()
            ->get_all();
            $old_given_balance_spend_items = array_filter($old_given_balance_spend_items, function($item){
                $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                return 'to' == $join_direction;
            });

            if( ! empty($old_given_balance_spend_items))
            {
                $this->term_posts_m->delete_term_posts($origination_id, array_column($old_given_balance_spend_items, 'post_id'));
                $this->balance_spend_m->delete_many(array_column($old_given_balance_spend_items, 'post_id'));
            }

            update_term_meta($origination_id, 'nextContractId', $destination_id);
            update_term_meta($origination_id, 'balanceBudgetAddTo', $destination_balance_budget_received);
            $post_excerpt = 'Nối đến HĐ ' . get_term_meta_value($destination_id, 'contract_code');
            $balance_spend_join_to_date = get_term_meta_value($origination_id, 'advertise_end_time');
            $balance_spend_join_to_id = $this->balance_spend_m->insert([
                'post_type' => $this->balance_spend_m->post_type,
                'comment_status' => 'auto',
                'post_title' => 'join_command',
                'post_status' => 'publish',
                'post_content' => $destination_balance_budget_received,
                'post_excerpt' => $post_excerpt,
                'start_date' => $balance_spend_join_to_date,
                'end_date' => $balance_spend_join_to_date,
            ]);
            update_post_meta($balance_spend_join_to_id, 'join_direction', 'to');
            $this->term_posts_m->set_term_posts( $origination_id, [ $balance_spend_join_to_id ], $this->balance_spend_m->post_type);
            $this->log_m->insert([
                'log_type'        => 'joinContracts',
                'log_status'      => 1,
                'term_id'         => $origination_id,
                'log_content'     => json_encode(
                    [
                        'destination_id' => $destination_id, 
                        'given' => $destination_balance_budget_received
                    ]
                ),
                'user_id'         => $this->admin_m->id
            ]);

            try
            {
                $contract = (new contract_m())->set_contract($origination_id);
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();
            }
            catch (Exception $e)
            {
                log_message('error', "[ErpContractsWorker::recalc_googleads_contract_chain] Sync amount #{$origination_id} error. " . json_encode(['error' => $e->getMessage()]));
            }

            // Receive (join from)
            $old_received_balance_spend_items = $this->balance_spend_m
            ->set_post_type()
            ->select('posts.post_id')
            ->join('term_posts', "(term_posts.post_id = posts.post_id and term_posts.term_id = {$destination_id})")
            ->where('posts.comment_status', 'auto')
            ->where('posts.post_title', 'join_command')
            ->as_array()
            ->get_all();
            $old_received_balance_spend_items = array_filter($old_received_balance_spend_items, function($item){
                $join_direction = get_post_meta_value($item['post_id'], 'join_direction');

                return 'from' == $join_direction;
            });

            if( ! empty($old_received_balance_spend_items))
            {
                $this->term_posts_m->delete_term_posts($destination_id, array_column($old_received_balance_spend_items, 'post_id'));
                $this->balance_spend_m->delete_many(array_column($old_received_balance_spend_items, 'post_id'));
            }

            update_term_meta($destination_id, 'previousContractId', $origination_id);
            update_term_meta($destination_id, 'balanceBudgetReceived', (-1) * $destination_balance_budget_received);
            $post_excerpt = 'Nối từ HĐ ' . get_term_meta_value($origination_id, 'contract_code');
            $balance_spend_join_from_date = get_term_meta_value($destination_id, 'advertise_start_time');
            $balance_spend_join_from_id = $this->balance_spend_m->insert([
                'post_type' => $this->balance_spend_m->post_type,
                'comment_status' => 'auto',
                'post_title' => 'join_command',
                'post_status' => 'publish',
                'post_content' => (-1) * $destination_balance_budget_received,
                'post_excerpt' => $post_excerpt,
                'start_date' => $balance_spend_join_from_date,
                'end_date' => $balance_spend_join_from_date,
            ]);
            update_post_meta($balance_spend_join_from_id, 'join_direction', 'from');
            $this->term_posts_m->set_term_posts( $destination_id, [ $balance_spend_join_from_id ], $this->balance_spend_m->post_type);
            
            $this->log_m->insert([
                'log_type'        => 'joinContracts',
                'log_status'      => 1,
                'term_id'         => $destination_id,
                'log_content'     => json_encode(
                    [
                        'origination_id' => $origination_id, 
                        'received' => (-1) * $destination_balance_budget_received
                    ]
                ),
                'user_id'         => $this->admin_m->id
            ]);

            try
            {
                $contract = (new contract_m())->set_contract($destination_id);
                $behaviour_m = $contract->get_behaviour_m();
                $behaviour_m->get_the_progress();
                $behaviour_m->sync_all_amount();
            }
            catch (Exception $e)
            {
                log_message('error', "[ErpContractsWorker::recalc_tiktokads_contract_chain] Sync amount #{$destination_id} error. " . json_encode(['error' => $e->getMessage()]));
            }

            // Following chain
            if(empty($contract_id_chain)) $is_end = TRUE;
            else 
            {
                $origination_id = $destination_id;
                $destination_id = array_shift($contract_id_chain);
            }
        }
    }
    
    /**
     * lock_manipulation_contract
     *
     * @return void
     */
    public function lock_manipulation_contract($start_time = NULL)
    {
        $start_time = $start_time ?: start_of_day(end_of_month(strtotime('-2month')));
        $end_time = end_of_day(end_of_month(strtotime('-1month')));

        $contracts = $this->contract_m->set_term_type()
        ->join('termmeta AS m_end_service_time', '
            m_end_service_time.term_id = term.term_id 
            AND m_end_service_time.meta_key = "end_service_time"
        ')
        ->join('termmeta AS m_is_manipulation_locked', '
            m_is_manipulation_locked.term_id = term.term_id 
            AND m_is_manipulation_locked.meta_key = "is_manipulation_locked"
        ', 'LEFT')
        ->select('term.term_id')
        ->select('term.term_status')
        ->where_in('term.term_status', ['ending', 'liquidation', 'remove'])
        ->where('m_end_service_time.meta_value >=', $start_time)
        ->where('m_end_service_time.meta_value <=', $end_time)
        ->where('(
            m_is_manipulation_locked.meta_value IS NULL 
            OR m_is_manipulation_locked.meta_value = 0
        )', NULL, NULL)
        ->as_array()
        ->get_all();
        
        if(empty($contracts))
        {
            log_message('info', '[ErpContractsWorker::lock_manipulation_contract] Nothing contract can lock manipulation');
            
            return TRUE;
        }

        log_message('info', '[ErpContractsWorker::lock_manipulation_contract] Start lock manipulation');
        foreach($contracts as $contract)
        {
            $contract_id = $contract['term_id'];

            $is_manipulation_locked = (bool)get_term_meta_value($contract_id, 'is_manipulation_locked');
            if($is_manipulation_locked)
            {
                $manipulation_locked_at = get_term_meta_value($contract_id, 'manipulation_locked_at');
                if(!empty($manipulation_locked_at)) 
                {
                    log_message('debug', "[ErpContractsWorker::lock_manipulation_contract] Contract #{$contract_id} is locked. Ignore process");
                    continue;
                }
            }

            log_message('debug', "[ErpContractsWorker::lock_manipulation_contract] Contract #{$contract_id} is being lock");

            update_term_meta($contract_id, 'is_manipulation_locked', 1);
            update_term_meta($contract_id, 'manipulation_locked_at', $end_time);

            $this->log_m->insert([
                'log_type' =>'lock_manipulation', 
                'user_id' => 1,
                'term_id' => $contract_id,
                'log_content' => 1,
                'log_time_create' => my_date(time(),'Y/m/d H:i:s')
            ]);

            log_message('debug', "[ErpContractsWorker::lock_manipulation_contract] Contract #{$contract_id} locked");
        }

        return TRUE;
    }

    /**
     * recalc_tiktok_contract_actual_budget
     *
     * @return void
     */
    public function recalc_tiktok_contract_actual_budget()
    {
        $contracts = $this->contract_m->set_term_type()
            ->join('termmeta AS m_actual_budget', 'm_actual_budget.term_id = term.term_id AND m_actual_budget.meta_key = "actual_budget"')
            ->join('term_posts AS tp_receipt', 'term.term_id = tp_receipt.term_id')
            ->join('posts AS receipt', 'receipt.post_id = tp_receipt.post_id AND receipt.post_type IN ("receipt_payment", "receipt_payment_on_behaft")')
            ->join('postmeta AS m_receipt_actual_budget', 'm_receipt_actual_budget.post_id = receipt.post_id AND m_receipt_actual_budget.meta_key = "actual_budget"')
            ->where('term.term_type', 'tiktok-ads')
            ->where('receipt.post_status ', 'paid')
            ->select([
                'term.term_id AS contract_id',
                'm_actual_budget.meta_value AS actual_budget',
                'SUM(DISTINCT(m_receipt_actual_budget.meta_value)) AS receipt_actual_budget',
            ])
            ->group_by('term.term_id')
            ->having('actual_budget <> receipt_actual_budget', FALSE, FALSE)
            ->as_array()
            ->get_all();

        if(empty($contracts))
        {
            return TRUE;
        }

        $stat = [
            'total' => count($contracts),
            'success' => 0,
            'failed' => 0,
            'data' => [
                'success' => [],
                'failed' => [],
            ],
        ];

        foreach($contracts as $contract){
            $contract_id = $contract['contract_id'];

            $actual_budget = 0;

            try
            {
                $this->contract_m->set_contract($contract_id);
                $actual_budget = $this->contract_m->get_behaviour_m()->calc_actual_budget();
                $actual_budget = round($actual_budget);
            }
            catch(Exception $e)
            {
                $message = "Contract #{$contract_id} catch exception. " . json_encode([
                    'exception' => $e->getMessage(),
                ]);
                log_message('error', $message);

                $stat['failed'] += 1;
                $stat['data']['failed'][$contract_id] = $message;
                
                continue;
            }

            if($actual_budget != $contract['receipt_actual_budget'])
            {
                $message = "Contract #{$contract_id} actual_budget recaled difference with payment. " . json_encode([
                    'actual_budget_recaled' => $actual_budget, 
                    'receipt_actual_budget' => $contract['receipt_actual_budget']
                ]);
                log_message('error', $message);

                $stat['failed'] += 1;
                $stat['data']['failed'][$contract_id] = $message;
                
                continue;
            }

            $stat['success'] += 1;
            $stat['data']['success'][$contract_id] = [
                'old_actual_budget' => $contract['contract_id'],
                'new_actual_budget' => $actual_budget,
            ];

            // update_term_meta($contract_id, 'actual_budget', $actual_budget);
        }

        echo json_encode($stat);

        return TRUE;
    }
}
/* End of file ErpNotificationsWorker.php */
/* Location: ./application/controllers/ErpNotificationsWorker.php */