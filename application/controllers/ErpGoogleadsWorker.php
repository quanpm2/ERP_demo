<?php defined('BASEPATH') or exit('No direct script access allowed');

use Notifications\Erp\Reports\Ads\Googleads\CustomerInsightReportWeekly\Email as WeeklyReportEmail;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class ErpGoogleadsWorker extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->config('amqps');

        $this->load->model('term_posts_m');
        $this->load->model('ads_segment_m');
        $this->load->model('base_report_m');
        $this->load->model('contract/contract_m');
        $this->load->model('googleads/googleads_m');
        $this->load->model('googleads/mcm_account_m');
        $this->load->model('googleads/base_adwords_m');
    }

    public function handle()
    {
        $amqps_host     = $this->config->item('host', 'amqps');
        $amqps_port     = $this->config->item('port', 'amqps');
        $amqps_user     = $this->config->item('user', 'amqps');
        $amqps_password = $this->config->item('password', 'amqps');
        $queue = 'googleads.event.default';
        $connection = new AMQPStreamConnection($amqps_host, $amqps_port, $amqps_user, $amqps_password);

        $channel = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $callback = function ($msg) {

            echo ' [x] Received ', $msg->body, "\n";

            $args = json_decode($msg->body);
            $args = wp_parse_args($args, [
                'action' => null,
                'event' => null,
                'contract_id' => null,
            ]);

            switch ($args['event']) {
                case 'googleads.migrate.spend':
                    $this->googleads_migrate_spend($args['contract_id']);
                    break;

                default:
                    # code...
                    break;
            }

            $msg->ack();
        };

        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($queue, '', false, false, false, false, $callback);

        while ($channel->is_open()) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }

    public function googleads_migrate_spend($contract_id)
    {
        $contract_id = (int) $contract_id;
        if (empty($contract_id)) {
            echo "Empty contract id \n";
            return false;
        };

        $contract     = (new googleads_m())->set_contract($contract_id);
        if (!$contract) {
            echo "Contract not exist \n";
            return FALSE;
        }

        $adsSegments    = $this->term_posts_m->get_term_posts($contract_id, $this->ads_segment_m->post_type);
        if (empty($adsSegments)) {
            echo "Contract segments not exist \n";
            return FALSE;
        }

        $insights = [];
        foreach ($adsSegments as $adsSegment) {
            $adaccount = $this->term_posts_m->get_post_terms($adsSegment->post_id, $this->mcm_account_m->term_type);
            if (empty($adaccount)) continue;

            $adaccount  = reset($adaccount);

            $_segment_start = $adsSegment->start_date;
            $_segment_end = $adsSegment->end_date ?: time();

            $_start = max(strtotime('2021-01-01'), $_segment_start);
            $_end   = min(time(), $_segment_end);

            if ($_start > $_end) continue;

            $this->base_adwords_m->set_mcm_account($adaccount->term_id);

            $account_data = $this->base_adwords_m->get_cache('ACCOUNT_PERFORMANCE_REPORT', $_start, $_end);
            $account_data and $account_data = array_filter($account_data);

            if (empty($account_data)) continue;

            foreach ($account_data as $timestamp => $accounts) {
                if (empty($accounts)) continue;

                $insights = array_merge($insights, $accounts);
            }

            $insights = array_group_by($insights, 'day');

            $insights = array_map(function ($_insights) {
                $_insight = reset($_insights);

                return [
                    'currency' => $_insight['currency'],
                    'account' => $_insight['account'],
                    'day' => $_insight['day'],

                    'impressions' => array_sum(array_column($_insights, 'impressions')),
                    'clicks' => array_sum(array_column($_insights, 'clicks')),
                    'invalidClicks' => array_sum(array_column($_insights, 'invalidClicks')),
                    'cost' => array_sum(array_column($_insights, 'cost')),
                    'avgCPC' => div(array_sum(array_column($_insights, 'cost')), array_sum(array_column($_insights, 'clicks'))),
                    'conversions' => array_sum(array_column($_insights, 'conversions')),
                    'allConv' => array_sum(array_column($_insights, 'allConv')),
                    'network' => 'all',
                ];
            }, $insights);

            $first_row = reset($insights);
            $has_date = isset($first_row['day']);
            if (!$has_date) return true;

            $days = array_unique(array_column($insights, 'day'));

            $_isegments = $this->term_posts_m->get_term_posts($adaccount->term_id, $this->insight_segment_m->post_type, [
                'fields' => 'posts.post_id, posts.end_date',
                'where' => [
                    'end_date >=' => start_of_day(min($days)),
                    'end_date <=' => end_of_day(max($days)),
                    'post_name' => 'day'
                ]
            ]);

            $_isegments and $_isegments = array_column($_isegments, null, 'end_date');

            foreach ($insights as $data) {
                $datetime = start_of_day($data['day']);
                $_isegment = $_isegments[$datetime] ?? null;

                if (empty($_isegment)) {
                    $_isegmentInsert     = [
                        'post_name'     => 'day',
                        'post_type'     => 'insight_segment',
                        'start_date'     => $datetime,
                        'end_date'         => $datetime
                    ];

                    $_isegment_id = $this->insight_segment_m->insert($_isegmentInsert);
                    $this->term_posts_m->set_term_posts($adaccount->term_id, [$_isegment_id], $this->insight_segment_m->post_type);

                    $_isegment = (object) $_isegmentInsert;
                    $_isegment->post_id = $_isegment_id;
                }

                $data['spend'] = div($data['cost'], 1000000);
                $data['cpc'] = div($data['avgCPC'], 1000000);

                $data['account_currency'] = $data['currency'];
                $meta_exchange_rate_key = '';
                if ('VND' != $data['account_currency']) {
                    $meta_exchange_rate_key = 'exchange_rate_' . strtolower($data['account_currency']) . '_to_vnd';
                    $exchange_rate = (int)get_post_meta_value($_isegment->post_id, $meta_exchange_rate_key);
                    empty($exchange_rate) and $data['account_currency'] == 'USD' and $exchange_rate = (int)get_term_meta_value($contract_id, 'dollar_exchange_rate');
                    empty($exchange_rate) and $exchange_rate = (int)get_term_meta_value($contract_id, $meta_exchange_rate_key);
                    empty($exchange_rate) and $exchange_rate = get_exchange_rate($data['account_currency']);

                    $data[$meta_exchange_rate_key] = $exchange_rate;
                    $data['spend'] = (float)$data['spend'] * $exchange_rate;
                }

                $item_array = ['account_currency', 'spend', 'cost', 'cpc', 'impressions', 'clicks', 'invalidClicks', 'conversions', 'network'];
                !empty($meta_exchange_rate_key) and $item_array[] = $meta_exchange_rate_key;
                $data = elements($item_array, $data);

                foreach ($data as $key => $value) {
                    update_post_meta($_isegment->post_id, $key, (string) $value);
                }
            }
        }

        update_term_meta($contract_id, 'is_migrated', true);

        echo "Migrated {$contract_id}";
        return true;
    }
}
/* End of file ErpNotificationsWorker.php */
/* Location: ./application/controllers/ErpNotificationsWorker.php */