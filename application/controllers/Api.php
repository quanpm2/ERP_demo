<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends Public_Controller {
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{

	}

	function user()
	{
		$this->load->model('log_m');
		$this->load->model('user_m');
		$token = $this->input->get('token');
		$check = $this->log_m->get_by(array(
			'log_type' =>'webgeneral-wp-login',
			'log_title' => 'User hash login',
			'log_content' => $token
			));
		$user = $this->user_m->get($check->user_id);
		$result = array();
		if(!$check || !$user)
			die(json_encode($result));

		$result['user_id'] = get_term_meta_value($check->term_id,'wp_user_id'); //user id of wordpress
		$result['wdt_id'] = $check->user_id;
		$result['token'] = $token;
		$result['display_name'] = $user->display_name;
		die(json_encode($result));
	}

}