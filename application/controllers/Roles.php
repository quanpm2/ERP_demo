<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends Admin_Controller {

	public $model = 'role_m';

	function __construct(){

		parent::__construct();

		$this->load->model($this->model);

		$this->load->model('permission_m');

		$this->load->model('role_permission_m');

		restrict('Admin.Role');
	}

	public function index(){
		restrict('Admin.Role.view');

		$this->search_filter();

		$this->admin_ui->add_search('role_id');

		$this->admin_ui->add_search('role_name');

		$this->admin_ui->add_search('role_description');

		$this->admin_ui->add_search('control',array('content'=> $this->admin_form->submit('search','Search')));

		$this->admin_ui->add_column('role_id','#');

		$this->admin_ui->add_column('role_name','Name');

		$this->admin_ui->add_column('role_description','Description');

		$this->admin_ui->add_column('control'
			, array('title'			=>	'<button class="btn btn-default btn-xs checkbox-toggle">
										<i class="fa fa-square-o"></i>
									</button>' . 
									(has_permission('Admin.Role.Delete') ? '<button type="button" class="btn btn-default btn-xs" id="remove-post"><i class="fa fa-trash-o"></i></button>' : ''), 
					'orderable'		=> FALSE,
					'searchable'	=> FALSE ,
					'set_select'	=> FALSE,
					'set_order'		=> FALSE)
			, '<input type="checkbox" class="deleteRow" value="$1"/>' . anchor($this->data['url_edit'] . "/$1", '<i class="fa fa-pencil"></i>', 'class="btn btn-default btn-xs"')
			, $this->{$this->model}->primary_key);

		$this->admin_ui->from($this->{$this->model}->_table);

		$this->admin_ui->where('deleted', FALSE);

		$this->admin_ui->order_by('role_id desc');

		$this->data['content'] = $this->admin_ui->generate(array(
			'uri_segment' => 4,
			'base_url'=> module_url('index/'),
		));

		parent::render($this->data,'roles/index');
	}

	public function edit($edit_id = 0){

		restrict('Admin.Role.add,Admin.Role.edit');
		$this->submit($edit_id);
		$data = array();
		$data['permissions'] = array();
		$data['actions'] = array();
		$data['edit'] = $this->role_m->get($edit_id);
		$permissions = $this->permission_m->get_many_by('status', 1);

		if($permissions)
		{
			foreach($permissions as $permission)
			{
				$actions = @unserialize($permission->action);
				$gpermissions = explode('.', $permission->name);
				if($gpermissions && count($gpermissions) == 2)
				{
					$domain = $gpermissions[0];
					$context = $gpermissions[1];
					$data['permissions'][$domain][$context] = array();
					if($actions && is_array($actions))
					{
						foreach($actions as $action)
						{
							$action = ucfirst($action);
							$data['permissions'][$domain][$context][$action] = $action;
							$data['actions'][$domain][$action] = $context.'.'.$action;
						}
					}
				}
				$data['permission'][$permission->name] = @$permission;
			}
		}

		$data['edit_id'] = $edit_id;

		$this->template->title->set('Quản lý quyền hạn theo vai trò');

		return parent::render($data,'roles/edit');
	}

	private function submit($role_id = 0)
	{
		if(!$this->input->post('submit_permission'))
			return false;
		
		$post = $this->input->post('edit');
		$role = array();
		$role['role_name'] = @$post['role_name'];
		$role['role_description'] = @$post['role_description'];
		$role['login_destination'] = @$post['login_destination'];
		if($role_id >0 )
		{
			$this->role_m->update($role_id,$role);
		}
		else
		{
			$role_id = $this->role_m->insert($role);
		}

		$this->role_permission_m->delete_many($role_id);
		$this->permission_m->delete_cache($role_id);

		if($permissions = $this->input->post('permissions'))
		{
			$pers = $this->permission_m->where_in('permission_id', array_keys($permissions))->get_many_by();
			if(!$pers) return false;
			$pers = key_value($pers, 'permission_id', 'name');
			foreach($permissions as $permission_id =>$actions)
			{
				if(!isset($pers[$permission_id]))
					continue;
				if(!$actions)
					continue;

				$actions_update = array();
				foreach($actions as $action)
				{
					$permission_name = $pers[$permission_id].'.'.$action;
					if( !permission_exists($permission_name) )
						continue;
					$actions_update[] = $action;
				}
				$this->role_permission_m
				->insert(array(
					'role_id' => $role_id,
					'permission_id' => $permission_id,
					'action' => serialize($actions_update)
					));
			}

		}
		$this->scache->delete_group('role/role-');
		$this->messages->success('Cập nhật thành công');
		redirect(admin_url().'roles/edit/'.$role_id,'refresh');
	}

	public function delete(){

		restrict('Admin.Role.delete');

		parent::delete();
	}

	function test()
	{

		// restrict('Admin.Banners');


		// $this->load->library('unit_test');

		// $run1 = has_permission('Admin.Banners');
		// $run2 = has_permission('FALSE.Banners');

		// $this->unit->run($run1, TRUE, 'Test function has_permission return TRUE');
		// $this->unit->run($run2, FALSE, 'Test function has_permission return FALSE');


		// $run1 = permission_exists('Admin.Banners');
		// $run2 = permission_exists('FALSE.Banners');
		// $this->unit->run($run1, TRUE, 'Test function permission_exists return TRUE');
		// $this->unit->run($run2, FALSE, 'Test function permission_exists return FALSE');

		// echo $this->unit->report();

		$x = $this->permission_m->add('Kaka.hieu', array('haha', 'hahaha 213'));
		 // $this->output->enable_profiler(TRUE);
	}
}