<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions extends Admin_Controller {

	public $model = 'permission_m';

	function __construct(){

		parent::__construct();
		$this->load->model($this->model);
	}

	public function index(){

		$data = array();

		$this->admin_ui->add_column('name','ID');

		$this->admin_ui->add_column('description','Description');
		
		return parent::renderList();
	}

	public function init(){

		$permissions = array();

		$permissions['Admin.Role'] = array(
			'description' => 'Quản lý nhóm Admin',
			'actions' => array('view','add','edit','delete','update'));

		$permissions['Admin.Module'] = array(
			'description' => 'Quản lý Module',
			'actions' => array('view','add','edit','delete','update'));

		foreach($permissions as $name => $value){

			$description = $value['description'];

			$actions = $value['actions'];
			
			$this->permission_m->add($name, $actions, $description);
		}
	}
	function test()
	{

		// restrict('Admin.Banners');


		$this->load->library('unit_test');
		$role_id = 1;
		$run1 = $this->permission_m->has_permission('Admin.Banners', $role_id);
		
		$run2 = $this->permission_m->has_permission('FALSE.Banners', $role_id);

		$this->unit->run($run1, TRUE, 'Test function has_permission return TRUE');
		$this->unit->run($run2, FALSE, 'Test function has_permission return FALSE');


		$run1 = $this->permission_m->has_permission('Admin.Banners.access', $role_id);
		
		$run2 = $this->permission_m->has_permission('Admin.Banners.false', $role_id);

		$this->unit->run($run1, TRUE, 'Test function has_permission 3agrs return TRUE');
		$this->unit->run($run2, FALSE, 'Test function has_permission 3agrs return FALSE');


		$run1 = $this->permission_m->permission_exists('Admin.Banners', $role_id);
		$run2 = $this->permission_m->permission_exists('FALSE.Banners', $role_id);
		$this->unit->run($run1, TRUE, 'Test function permission_exists return TRUE');
		$this->unit->run($run2, FALSE, 'Test function permission_exists return FALSE');


		echo $this->unit->report();
		 // $this->output->enable_profiler(TRUE);
	}
}