# Set the base image for subsequent instructions
FROM alpine:3.18.5

# Enabled for PHP 7
RUN echo "http://dl-cdn.alpinelinux.org/alpine/v3.15/main" >> /etc/apk/repositories
RUN echo "http://dl-cdn.alpinelinux.org/alpine/v3.15/community" >> /etc/apk/repositories

# Update packages
RUN apk update && apk upgrade

# Install dependencies
RUN apk add --no-cache \
    git \
    curl \
    zip \
    unzip \
    openssh-client \
    php7 \
    php7-json \
    php7-phar \
    php7-iconv \
    php7-openssl \
    php7-mbstring \
    php7-posix \
    nodejs \
    npm
    
# Install Composer
RUN curl --silent --show-error "https://getcomposer.org/installer" | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy
RUN composer global require "laravel/envoy=^2.8" "vlucas/phpdotenv=^5.5"

# Set working directory
ENTRYPOINT cd /home && /bin/sh