function addWorkingDay(start_time, days)
{
	let startDate 	= moment.unix(start_time);

	// If start_time is weekends , then set to monday of next week
	if(6 == startDate.day()) startDate.add(2, 'days');
	else if(0 == startDate.day()) startDate.add(1, 'days');

	let endDate = _.cloneDeep(startDate);
	endDate.add(days, 'days');

	let weekendDays = 0;
	while((count_weekend_days(start_time, endDate.format("X")) - weekendDays) > 0)
	{
		let _weekend_days 	= count_weekend_days(start_time, endDate.format("X"));
		let left 			= _weekend_days - weekendDays;
		weekendDays 		= _weekend_days;

		endDate.add(left, 'days');
	}

	endDate.subtract(1, 'seconds');

	return { startDate : _.cloneDeep(startDate), endDate : _.cloneDeep(endDate)};
}

function count_weekend_days(start_time, end_time)
{
    let weekends    = 0;
    var startDate   = moment.unix(start_time);
    let days        = moment.unix(end_time).diff(startDate, 'days') + 1;

    let start = 0;
    while(start < days)
    {
        start++;

        let tmp = _.cloneDeep(startDate);
        tmp.add(start, 'days');

        if(!_.includes([0, 6], tmp.day())) continue; // saturday = 6, sunday = 0;
        weekends++;
    }

    return weekends;
};

var tasks = [
	{
		created_on: "0",
		end_date: "1534784399",
		post_author: "1",
		post_id: "80163",
		post_status: "publish",
		post_title: "Thiết kế giao diện ban đầu",
		start_date: "1534352400",
	},
	{
		created_on: "0",
		end_date: "1534957199",
		post_author: "1",
		post_id: "80164",
		post_status: "publish",
		post_title: "Duyệt giao diện",
		start_date: "1534784400",
	},
	{
		created_on: "0",
		end_date: "1535043599",
		post_author: "1",
		post_id: "80165",
		post_status: "publish",
		post_title: "Chỉnh sửa giao diện",
		start_date: "1534957200",
	},
	{
		created_on: "0",
		end_date: "1535389199",
		post_author: "1",
		post_id: "80166",
		post_status: "publish",
		post_title: "Tiến hành lập trình động cho Website",
		start_date: "1535043600",
	},
	{
		created_on: "0",
		end_date: "1535392799",
		post_author: "1",
		post_id: "80167",
		post_status: "publish",
		post_title: "Tối ưu phiên bản Mobile",
		start_date: "1535389200",
	},
	{
		created_on: "0",
		end_date: "1535392799",
		post_author: "1",
		post_id: "80168",
		post_status: "publish",
		post_title: "Đưa Website lên Server chạy thực tế",
		start_date: "1535389200",
	},
	{
		created_on: "0",
		end_date: "1535475599",
		post_author: "1",
		post_id: "80169",
		post_status: "publish",
		post_title: "Nghiệm thu và bàn giao",
		start_date: "1535389200",
	},
	{
		created_on: "0",
		end_date: "1535479199",
		post_author: "1",
		post_id: "80170",
		post_status: "publish",
		post_title: "Chuyển tên miền và chạy thực tế",
		start_date: "1535475600",
	},
	{
		created_on: "0",
		end_date: "1535479199",
		post_author: "1",
		post_id: "80171",
		post_status: "publish",
		post_title: "Đăng ký cài đặt Google Analytics",
		start_date: "1535475600",
	},
	{
		created_on: "0",
		end_date: "1535479199",
		post_author: "1",
		post_id: "80172",
		post_status: "publish",
		post_title: "Đăng ký cài đặt Google Webmaster Tool",
		start_date: "1535475600",
	},
	{
		created_on: "0",
		end_date: "1535479199",
		post_author: "1",
		post_id: "80173",
		post_status: "publish",
		post_title: "Kiểm tra Google PageSpeed Insights",
		start_date: "1535475600",
	},
	{
		created_on: "0",
		end_date: "1535479199",
		post_author: "1",
		post_id: "80174",
		post_status: "publish",
		post_title: "Kiểm tra và cấu hình Robots",
		start_date: "1535475600",
	},
	{
		created_on: "0",
		end_date: "1535479199",
		post_author: "1",
		post_id: "80175",
		post_status: "publish",
		post_title: "Đăng ký Google Analytics",
		start_date: "1535475600",
	},
	{
		created_on: "0",
		end_date: "1535479199",
		post_author: "1",
		post_id: "80176",
		post_status: "publish",
		post_title: "Lock cài đặt Plugins, cấu hình bảo mật file config",
		start_date: "1535475600",
	}
];

console.log(tasks);

let draggedIndex = 1;
let droppedIndex = 0;

/* Fake drag & drop task item */
var changedTasks = _.cloneDeep(tasks);
var tmp = changedTasks[draggedIndex];
changedTasks[draggedIndex] = changedTasks[droppedIndex];
changedTasks[droppedIndex] = tmp;

var draggedTasks = tasks[draggedIndex];
/* Calculate date before reinitialize tasks (Fill to the dragged Task spot) */

let _cDate = _.cloneDeep($tasks[$draggedIndex]);

for (var i = draggedIndex; i < tasks.length - 1; i++)
{
	console.log('cname ' 	+ tasks[i].post_title);
	console.log('cst ' 		+ moment.unix(tasks[i].start_date).format());
	console.log('cet ' 		+ moment.unix(tasks[i].end_date).format());

	console.log('nname ' 	+ tasks[i + 1].post_title);
	console.log('nst ' 		+ moment.unix(tasks[i + 1].start_date).format());
	console.log('net ' 		+ moment.unix(tasks[i + 1].end_date).format());

	let taskDays 		= moment.unix(tasks[i + 1].end_date).diff(moment.unix(tasks[i + 1].start_date), 'days') + 1;
	let noneWorkingDays = count_weekend_days(tasks[i + 1].start_date, tasks[i + 1].end_date);
	let workingDays 	= taskDays - noneWorkingDays;

	/* so sánh lấy ngày bắt đầu */
	// Nếu ngày bắt đầu của ndate 	> 	ngày kết thúc của cdate => start ndate = start cdate
	// 								<= 	ngày kết thúc của cdate => start ndate

	let _r = addWorkingDay(tasks[i].start_date, workingDays);

	_.set(tasks, (i + 1) + '.start_date', _r.startDate.format("X"));
	_.set(tasks, (i + 1) + '.end_date', _r.startDate.format("X"));

	// tasks[i + 1].start_date 	= _r.startDate.format("X");
	// tasks[i + 1].end_date 	= _r.startDate.format("X");

	console.log("++++++++++++++++++++++++++++++++++++++++++");
	console.log('rst ' 		+ moment.unix(tasks[i + 1].start_date).format());
	console.log('ret ' 		+ moment.unix(tasks[i + 1].end_date).format());
	console.log("++++++++++++++++++++++++++++++++++++++++++");

	// continue;
	// // tasks[draggedIndex + 1].start_date = tasks[draggedIndex].start_date;
	// // tasks[draggedIndex + 1].end_date 	= addWorkingDay(tasks[draggedIndex + 1].start_date, workingDays);

	// console.log("++++++++++++++++++++++++++++++++++++++++++");
	// console.log('st ' + moment.unix(tasks[draggedIndex + 1].start_date).format());
	// console.log('et ' + moment.unix(tasks[draggedIndex + 1].end_date).format());
	// console.log("++++++++++++++++++++++++++++++++++++++++++");
}
// console.log(draggedTasks);
// console.log(changedTasks);