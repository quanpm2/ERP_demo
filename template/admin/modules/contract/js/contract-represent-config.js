Vue.component('contract-represent-config-component', {
    data : function(){
        return {
            dropdown : () => { return []; },
            dat : () => { return []; },
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
        }
    },
    methods : {
        initData : function(){
            var self = this;
            axios.get(admin_url + 'system/api/setting/id/con_signature')
            .then(response => {
                self.dropdown = response.data.data.config;
                self.dat = response.data.data.values;
            })
            .catch(e => {
              this.errors.push(e);
            });
        },
        addRow : function(){

            let tmp = {
                key : '',
                company : '',
                address : '',
                phone : '',
                tax : '',
                name : '',
                title : '',
                guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            };

            let data = this.dat;
            this.dat = [];
            data.push(tmp);
            this.dat = data;
        },
        removeRow : function(value){

            let data = this.dat;
            for (var i = 0; i < data.length; i++)
            {
                console.log(data[i].key);
                if(data[i].guid != value.guid) continue;
                data.splice(i, 1);
            }

            this.dat = [];
            this.dat = data;
            if(this.dat <= 0) this.addRow();
            return true;
        },
        update : function(){
            $.post(admin_url + 'system/api/setting/id/con_signature', $("#"+this.guid+" form").serializeArray(), 
            function(response) { 
                // $("#customer_profile").append(resp.content);
                console.log(response);
            },
            'json');
        }
    },
    created : function(){
        this.initData();
    },
    template: `
    <div class="box box-solid" :id="guid">
        <form method="POST">
            <input type="hidden" name="id" value="con_signature"/>
            <div class="box-header with-border">
                <h5 class="box-title"><i class="fa fw fa-vine"></i> Đại diện ký hợp đồng</h5>
            </div>
            <div class="box-body">
                    <table class="table">
                        <tbody>
                            <tr 
                                v-for="(item,index) in dat" 
                                is="contract-represent-config-row-component" 
                                v-on:add_new_row="addRow"
                                v-on:remove_row="removeRow"
                                :show_add_btn="index == (dat.length-1)"
                                :show_delete_btn="true"
                                :dropdown="dropdown" 
                                :item="item"></tr>
                        </tbody>
                    </table>
            </div>
            <div class="box-footer clearfix text-center">
                <button class="btn btn-primary btn-flat pull-right" name="signature-submit" type="button" v-on:click="update">Cập nhật</button>
            </div>
        </form>
    </div>
    `
});