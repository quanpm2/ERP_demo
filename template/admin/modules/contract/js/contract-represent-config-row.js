Vue.component('contract-represent-config-row-component', {
    props: {
        dropdown : {
            type : Array,
            default : function(){ return [];}
        },
        item : {
            type : Object,
            default : function(){ return {};}
        },
        show_add_btn : {
            type : Boolean,
            default : false,
        },
        show_delete_btn : {
            type : Boolean,
            default : false,
        }
    },
    computed : {
        dropdownOptions: function(){

            if(this.dropdown.length == 0) return [];

            let result = [];

            for(var i in this.dropdown)
            {
                if( ! this.dropdown.hasOwnProperty(i)) continue;
                result.push({key:i,value:this.dropdown[i]});
            }

            return result;
        },
    },
    template: `
    <tr>
        <td>
            <select v-model="item.key" class="form-control">
                <option v-if="dropdownOptions.length > 0" v-for="opt in dropdownOptions" :value="opt.key" v-value="opt.value" v-text="opt.value"></option>
            </select>
        </td>
        <td>
            <input type="hidden" :name="'options[con_represent]['+item.key+'][key]'" v-model="item.key">
            <input type="text" :name="'options[con_represent]['+item.key+'][address]'" v-model="item.address" class="form-control" placeholder="Địa chỉ văn phòng">
        </td>
        <td>
            <input type="text" :name="'options[con_represent]['+item.key+'][phone]'" v-model="item.phone" class="form-control" placeholder="Số điện thoại">
        </td>
        <td>
            <input type="text" :name="'options[con_represent]['+item.key+'][tax]'" v-model="item.tax" class="form-control" placeholder="Mã số thuế">
        </td>
        <td>
            <input type="text" :name="'options[con_represent]['+item.key+'][name]'" v-model="item.name" class="form-control" placeholder="Người đại diện">
        </td>
        <td>
            <input type="text" :name="'options[con_represent]['+item.key+'][title]'" v-model="item.title" class="form-control" placeholder="Chức vụ">
        </td>
        <td>
            <input type="text" :name="'options[con_represent]['+item.key+'][loa]'" v-model="item.loa" class="form-control" placeholder="Giấy ủy quyền số">
        </td>
        <td v-if="this.dropdownOptions.length > 1">
            <button v-on:click="$emit('remove_row', item)" type="button" class="btn btn-block btn-danger btn-flat"><i class="fa fa-close"></i></button>
        </td>
        <td v-if="this.dropdownOptions.length > 1">
            <button v-on:click="$emit('add_new_row')" v-if="show_add_btn == true" type="button" class="btn btn-block btn-success btn-flat"><i class="fa fa-plus"></i></button>
        </td>
    </tr>`
});