Vue.component('term-bailment-component', {
    props: ['term_id','post_id','user_id','caution_day','late_days','contract_code','sale_displayname','prev_amount','end_date','end_time'],
    data: function(){
        return {
            budget: 0,
            messages : null,
            msgType : null,
            quota : { limit : { budget: 0, quantity: 0},used : { budget: 0, quantity: 0},remain : { budget: 0, quantity: 0}},
            postData : {
                user_id : this.user_id,
                post_id : this.post_id,
                term_id : this.term_id,
                edit : {
                    end_date : this.end_date
                },
                meta : {
                    amount : this.prev_amount,
                }
            }
        } 
    },
    mounted : function(){
        this.initDatepickerEl();
    },
    computed: {
        labelText: function(){

            if(this.user_id == '')
            {
                return '';
            }

            // Trường hợp không có bất kỳ phát sinh bảo lãnh nào ở hợp đồng thì chỉ cần
            // hiển thị chỗ gợi ý thêm bảo lãnh để popup modal
            if(this.postData.post_id == 0)
            {
                return "<span class='text-green' style='text-decoration: none;border-bottom: dashed 1px #0088cc;'><i>Thêm mới</i></span>"
            }
            
            var date = moment.unix(this.end_time).format('DD-MM-YYYY');
            var classobj = parseInt(this.end_time) < moment().unix() ? 'text-red' : 'text-green';
            var result = "<span class='"+classobj+"' style='text-decoration: none;border-bottom: dashed 1px #0088cc;'><i>"+this.postData.edit.end_date+"</i></span>";
            return result;
        },
        remainQuantity: function(){
            return this.quota.remain.quantity.toLocaleString('en');
        },
        remainBudget: function(){
            var amount = (this.quota.remain.budget + this.prev_amount) - this.postData.meta.amount;

            if(amount < 0 ) return "<span class='text-red'>"+amount.toLocaleString('en') + ' đ'+"</span>";
            else if(amount < 2000000 ) return "<span class='text-yellow'>"+amount.toLocaleString('en') + ' đ'+"</span>";
            else return "<span class='text-green'>"+amount.toLocaleString('en') + ' đ'+"</span>";

            return amount.toLocaleString('en') + ' đ';
        },
        modelId : function(){
            return 'setting-bailment-modal-'+this.term_id;
        },
        linkTargetModalId : function(){
            return '#'+this.modelId;
        },
        formId: function(){
            return 'frm-'+this.modelId;
        },
        datepickerElId : function(){
            return 'datepicker-ui-'+this.modelId;
        },
        msgAlertBoxClass: function(){
            if(this.msgType == null) return '';
            else if(this.msgType == true) return 'alert alert-info';
            else if(this.msgType == false) return 'alert alert-warning';
            return '';
        },
    },
    watch: { },
    methods: {
        initDatepickerEl: function(){
            
            var self = this;

            $('#'+this.datepickerElId).datepicker({format: 'dd-mm-yyyy',startDate: '-3d'})
            .on('changeDate', function(e){
                self.postData.edit.end_date = $(this).val();
            });
        },
        renderModalId : function() { return 'setting-bailment-modal-'+this.term_id; },
        loadFormData: function(){

            var seft = this;

            axios
            .get(baseURL + "staffs/ajax/bailment/loadQuota/" + this.user_id)
            .then(function (response) {
                
                /* Clear alert messages */    
                seft.clearAlertItems();

                var dat = response.data;
                if(dat.status == false)
                {
                    seft.msgType = dat.status;    
                    seft.messages = [dat.msg];
                    return;
                }

                seft.quota = dat.data.quota;
            });
        },
        clearAlertItems : function(){
            this.messages = null;
            this.msgType = null;
        },
        validateBeforeSubmit: function(){

            if(this.quota.remain.quantity < 0)
            {
                this.msgType = false;
                this.messages = ['Số lượng hợp đồng được bảo lãnh đã vượt quá giới hạn cho phép !']
                return false ;
            }

            if(this.budget > this.quota.remain.budget)
            {
                this.msgType = false;
                this.messages = ['Số tiền bảo lãnh vượt quá mức cho phép !']
                return false ;
            }

            return true;
        },
        bailmentSubmit: function() {

            this.clearAlertItems();
            if( ! this.validateBeforeSubmit()) return false;

            var self = this;
            var postUrl = baseURL + "contract/ajax/receipt/updateReceiptCaution/" + this.postData.post_id;
            console.log(this.postData.post_id);

            /* POST DATA */
            $.post(postUrl, this.postData, function(response){

                if( ! response.status)
                {
                    self.msgType = false;
                    self.messages = [response.msg]
                    return false ;
                }

                self.msgType = true;
                self.messages = [response.msg];

                self.post_id = response.data.post_id;
                self.postData.post_id = response.data.post_id;
                console.log(self.postData.post_id);
                return true;

            }, "json");
        }
    },
    template: `
    <div class="bailment-cell">
        <a v-on:click="loadFormData" href="#" data-toggle="modal" :data-target="linkTargetModalId" v-html="labelText"></a>

        <!-- Modal -->
        <div class="modal fade" :id="modelId" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">

            <form :id="formId" method="POST" class="form-horizontal">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Số HĐ: {{ contract_code }}</h5>
                  </div>
                  <div class="modal-body">

                    <template v-if="messages">
                        <div v-for="message in messages" v-bind:class="msgAlertBoxClass" role="alert">
                            {{ message }}
                        </div>
                    </template>

                    <p>Kinh doanh phụ trách : <b><span v-text="sale_displayname"></span></b></p>
                    <p>Số hợp đồng có thể bảo lãnh : <b><span v-text="remainQuantity"></span></b></p>
                    <p>Ngân sách bảo lãnh còn lại : <b><span v-html="remainBudget"></span></b></p>

                    <div class="input-group date">
                        <div class="input-group-addon"><i class="fa fa-usd"></i> </div>
                        <input type="text" class="form-control" v-model="postData.meta.amount" min="1" :max="quota.remain.budget"/>
                    </div>
                    <p class="help-block">Số tiền mà nhân viên kinh doanh phụ trách đứng ra đảm bảo</p>

                    <div class="input-group date">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i> </div>
                        <input type="text" class="form-control pull-right" :id="datepickerElId" :value="postData.edit.end_date">
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button v-on:click="bailmentSubmit" type="button" class="btn btn-primary">Hoàn tất</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
    </div>
    `
});

var app_root = new Vue({
    el: '#app-container',
    methods : {

    }
});