/**
 * Component quản lý thông tin dự thu của các phòng ban , nhóm , nhân viên kinh doanh và hợp đồng
 */
Vue.component('revenue-statistic-container-component', {
    data: function(){

        var guid = 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);});

        return {
            userGroupsSelected: [],
            usersSelected: [],
            terms: {
                waitingforapprove: [],
                bailment: [],
                bailment_expired: []
            },
            summary: null,

            departmentsTab   : { guid: 'departments-' + guid, text: 'Phòng kinh doanh', badge: "", isActive: false },
            userGroupsTab   : { guid: 'userGroups-' + guid, text: 'Nhóm kinh doanh', badge: "", isActive: true },
            usersTab        : { guid: 'users-' + guid, text: 'Kinh doanh', badge: "", isActive: false},
            termsTab        : { guid: 'terms-' + guid, text: 'Hợp đồng', badge: "", isActive: false },
        };
    },
    watch: {
        userGroupsSelected: function(){
            this.usersSelected = [];
            if(this.userGroupsSelected.length == 0) 
            {
                this.userGroupsTab.badge = '';
            }
            else this.userGroupsTab.badge = "Đã chọn " + this.userGroupsSelected.length + ' nhóm';
        },
        usersSelected: function(){
            if(this.usersSelected.length == 0) this.usersTab.badge = '';
            else this.usersTab.badge = "Đã chọn " + this.usersSelected.length + ' NVKD';
        },
        summary: function(){
            this.$emit('update_summary_boxes',this.summary);
        }
    },
    methods: {
        userGroupsUpdated: function(userGroups){ this.userGroupsSelected = userGroups; },
        usersUpdated: function(users){
            this.usersSelected = users;
        },
        termsUpdated: function(data){ this.terms = data; },
        summaryUpdated: function(data){ this.summary = data; }
    },
    template: `
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">

            <ul class="nav nav-tabs">
                <li :class="userGroupsTab.isActive ? 'active' : ''"><a :href="'#'+userGroupsTab.guid" data-toggle="tab">{{ userGroupsTab.text }} <span class="badge" v-text="userGroupsTab.badge"></span></a></li>
                <li :class="usersTab.isActive ? 'active' : ''"><a :href="'#'+usersTab.guid" data-toggle="tab">{{ usersTab.text }} <span class="badge" v-text="usersTab.badge"></span></a></li>
                <li :class="termsTab.isActive ? 'active' : ''"><a :href="'#'+termsTab.guid" data-toggle="tab">{{ termsTab.text }} <span class="badge" v-text="termsTab.badge"></span></a></li>
                <li class="pull-right"></li>
            </ul>

            <div class="tab-content">

                <div :class="userGroupsTab.isActive ? 'tab-pane active' : 'tab-pane'" :id="userGroupsTab.guid">

                    <usergroups-revenue-component v-on:users_groups_updated="userGroupsUpdated" :selected="userGroupsSelected"/>

                    <div class="clearfix"></div>
                </div>

                <div :class="usersTab.isActive ? 'tab-pane active' : 'tab-pane'" :id="usersTab.guid">
                    <users-revenue-component 
                        v-on:terms_updated="termsUpdated"
                        v-on:summary_updated="summaryUpdated"
                        v-on:users_updated="usersUpdated" 
                        :user_groups_selected="userGroupsSelected" />
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
                <div :class="termsTab.isActive ? 'tab-pane active' : 'tab-pane'" :id="termsTab.guid">

                    <terms-datatable-component :datasource="terms.waitingforapprove" />
                    <terms-datatable-component :datasource="terms.bailment" />
                    <terms-datatable-component :datasource="terms.bailment_expired" />

                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    `
});