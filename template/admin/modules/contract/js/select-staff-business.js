Vue.component("select-staff-business", {
    props: {
        guid: {
            type: String,
            default: () => {
                return "xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx".replace(
                    /[xy]/g,
                    function (c) {
                        var r = (Math.random() * 16) | 0,
                            v = c == "x" ? r : (r & 0x3) | 0x8;
                        return v.toString(16);
                    }
                );
            },
        },
        is_manage: {},
        term_id: {
            required: true,
            type: Number | String,
        },
        staff_business_id: {
            type: Number | String,
        },
        datasource: {
            required: true,
            type: Array,
        },
        disabled: {
            type: Boolean,
            default: false,
        },
        name: {
            type: String,
            default: "staff_business",
        },
        extra_data: {
            type: Array,
            default: [],
        },
        remote_url: {
            required: true,
            type: String,
        },
        reload_if_success: {
            type: Boolean,
            default: true,
        },
    },

    data() {
        return {
            payload: {},
            is_loading: false,
        };
    },

    watch: {
        datasource: function () {
            if ("undefined" == typeof this.datasource) return [];
            this.initSelect2();
        },

        staff_business_id: function () {
            this.$emit("on_changed", this.staff_business_id);
        },
    },

    computed: {
        staff_business_assigned_to_name() {
            let dispaly_name = _.map(this.extra_data, (item) => {
                return _.get(item, "display_name");
            });

            return _.join(dispaly_name, ", ");
        },

        staff_business_assigned_to_id() {
            let id = _.map(this.extra_data, (item) => {
                return _.get(item, "id");
            });

            return id;
        },

        is_show_warning() {
            if (_.isEmpty(this.payload)) {
                return false;
            }

            let selected_id = parseInt(_.get(this.payload, "value")) || null;

            return !_.includes(selected_id, this.staff_business_assigned_to_id);
        },
    },

    mounted() {
        this.initSelect2();
    },

    updated() {
        this.initSelect2();
    },
    methods: {
        initSelect2() {
            let self = this;

            $("#select-" + this.guid).editable({
                disabled: self.disabled,
                emptytext: "Chưa có",
                source: self.datasource,
                display: function (value, source) {
                    value = _.parseInt(value);
                    if (_.gt(value, 0)) {
                        const _item = _.find(self.datasource, (o) => {
                            return _.eq(o.id, value);
                        });
                        if (!_.isEmpty(_item)) {
                            $(this).text(_item.text);
                        } else {
                            $(this).empty();
                        }
                    } else {
                        $(this).empty();
                    }
                },
                params: function (params) {
                    params.type = $(this).data("type-data");
                    return params;
                },
                url: function (params) {
                    self.payload = params;

                    return true;
                },
            });
        },

        save_overwrite() {
            let payload = _.clone(this.payload);
            _.set(payload, "is_overwrite", true);

            this.save(payload);
        },

        save_all() {
            let payload = _.clone(this.payload);
            _.set(payload, "is_overwrite", false);

            this.save(payload);
        },

        save(payload) {
            this.is_loading = true;

            fetch(this.remote_url, {
                method: "POST", // *GET, POST, PUT, DELETE, etc.
                headers: { "Content-Type": "application/json" },
                mode: "cors", // no-cors, cors, *same-origin
                cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                credentials: "same-origin", // include, *same-origin, omit
                redirect: "follow", // manual, *follow, error
                referrer: "no-referrer", // no-referrer, *client
                body: JSON.stringify(payload),
            })
                .then((response) => {
                    return response.text();
                })
                .then((result) => {
                    result = JSON.parse(result);

                    if (_.isString(result)) {
                        $.notify(result, "error");

                        return;
                    }

                    let messages = [];
                    if (_.isString(result.msg)) messages.push(result.msg);
                    else messages = result.msg;

                    _.forEach(messages, function (message) {
                        $.notify(message, "success");
                    });
                    if (this.reload_if_success)
                        setTimeout(() => {
                            window.location.reload();
                        }, 1800);

                    return result;
                })
                .finally(() => {
                    this.is_loading = false;
                });
        },
    },
    template: `
        <div>
            <p style="margin: 0">
                <a type="text" class="editable" 
                    data-type="select2"
                    data-original-title="Nhân viên kinh doanh"
                    data-type-data="meta"
                    :id="'select-' + guid" href="#"
                    :name="name" 
                    :data-name="name" 
                    :value="staff_business_id"
                    :data-value="staff_business_id" 
                    :data-pk="term_id"
                    v-text="staff_business_id"></a>
            </p>

            <div v-if="is_show_warning">
                <p style="margin-top: 10px;">
                    Kinh doanh <b>chưa được lưu</b> vì khách hàng đang được Kinh doanh <b>{{staff_business_assigned_to_name}}</b> đang phụ trách
                </p>
                
                <button class="btn btn-sm btn-default" :disabled="is_loading" @click="save_all">Ghi nhận tất cả</button>
                <button class="btn btn-sm btn-primary" :disabled="is_loading" @click="save_overwrite">Chỉ ghi nhận kinh doanh đang chọn</button>
            </div>
        </div>
    `,
});
