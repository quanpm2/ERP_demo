/**
 * Component quản lý thông tin tất cả các nhân viên
 * Input : group_id
 * Output : users + terms belongs users
 */
Vue.component('users-revenue-component', {
    props: ['user_groups_selected'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            users: [],
            checkedUsers: [],
            usersRevenueUrl: jsobjectdata.ajaxUrl.usersRevenue,
            usersTermsUrl: jsobjectdata.ajaxUrl.terms,
        };
    },
    created: function(){
        this.loadUsers();
        this.loadTerms();
    },
    updated: function(){
        this.buildDatatable();
    },
    watch: {
        user_groups_selected: function(){

            /* Ngược lại , filter user theo nhóm */
            this.destroyDatatable();
            this.loadUsers();
            this.loadTerms();
        },
        checkedUsers : function(){
            this.loadTerms();
            this.$emit('users_updated',this.checkedUsers);
        },
        users : function(){
            this.buildDatatable();
        }
    },
    methods: {

        loadUsers: function(){
            
            var seft = this;
            var group_ids = this.user_groups_selected.length > 0 ? this.user_groups_selected : 'all';

            axios
            .get(this.usersRevenueUrl, { params: { group_id : group_ids }})
            .then(function (response) {
                var _data = response.data.data;
                seft.users = _data;
            });
        },

        loadTerms: function(){

            var seft = this;
            var user_ids = this.checkedUsers.length > 0 ? this.checkedUsers : 'all';
            var group_ids = this.user_groups_selected.length > 0 ? this.user_groups_selected : 'all';

            axios
            .get(this.usersTermsUrl, { params: { user_id : user_ids , group_id : group_ids}})
            .then(function (response) {
                var _data = response.data.data;

                seft.$emit('terms_updated',_data.terms);
                seft.$emit('summary_updated',_data.summary);
            });   
        },

        destroyDatatable: function(){
            if ($.fn.DataTable.isDataTable("#userTermData"))
            {
                $("#userTermData").DataTable().destroy();
            }
        },
        buildDatatable : function(){

            // $.fn.dataTable.ext.errMode = 'none';
            $("#userTermData").DataTable({
                data: this.users,
                select: { style: 'multi' },
                columns: [  
                    {
                        title : "", data : "user_id",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return '<td><input class="userCheckboxes" type="checkbox" value="' + data + '"></td>';
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },

                    {   title : 'STT', data : 'increment' },
                    {   title : "Nhân viên Kinh doanh", data : "display_name"},
                    { 
                        title : "Dự thu hợp đồng mới", data : "payment_amount_remaining_waitingforapprove",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        title : "Dự thu hợp đồng đang chạy (bảo lãnh đến hạn)", data : "payment_amount_remaining_bailment",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        title : "Dự thu hợp đồng đang chạy (bảo lãnh quá hạn)", data : "payment_amount_remaining_bailment_expired",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    }

                ],
                "language": {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Hiển thị _MENU_ kết quả mỗi trang",
                    "sZeroRecords":  "Không tìm thấy kết quả nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 kết quả",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm kiếm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Trang đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Trang cuối"
                    }
                }
            });

            var seft = this;

            $("#userTermData .userCheckboxes").off().on('change', function(){

                var isChecked = $(this).prop('checked');
                var user_id = $(this).val();

                /* If input checkbox is uncheck and it value is existed in available checkedUsers Array then remove it from away Array */
                if( ! isChecked && seft.checkedUsers.indexOf(user_id) != -1)
                {
                    seft.checkedUsers.splice(seft.checkedUsers.indexOf(user_id), 1);
                    return true;
                }

                /* Otherwise then push input checkbox value to checkedUsers for next request  */
                seft.checkedUsers.push(user_id);
                return true;
            });
        } 
    },
    template: ` <table id="userTermData" class="table no-margin"></table> `
});