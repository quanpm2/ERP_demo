/**
 * Component quản lý thông tin tất cả các nhân viên
 * Input : group_id
 * Output : users + terms belongs users
 */
Vue.component('usergroups-revenue-component', {
    data: function(){
        return {
            checkedUsersGroups : [],
            usersGroups : [],
            usersGroupsRevenueUrl : jsobjectdata.ajaxUrl.usersGroupsRevenue,
            headings : [
                '',
                'Nhóm',
                'Số lượng thành viên',
                'Dự thu hợp đồng mới (chờ duyệt)',
                'Dự thu hợp đồng đang chạy (bảo lãnh đến hạn)',
                'Dự thu hợp đồng đang chạy (bảo lãnh quá hạn)'
            ]
        }
    },
    created: function()
    {
        this.loadUsersGroups();
    },
    updated: function()
    {
        this.buildDatatable();
    },
    watch: {
        checkedUsersGroups: function(){
            this.$emit('users_groups_updated',this.checkedUsersGroups);
        }
    },
    methods: {
        
        loadUsersGroups: function(){
            var seft = this;
            var user_id = 'all';

            axios
            .get(this.usersGroupsRevenueUrl)
            .then(function (response) {
                var _data = response.data.data;
                seft.usersGroups = _data;
            });   
        },

        buildDatatable : function(){
            
            var _datacolumns = [
                { },
                { data : "group_name" },
                { 
                    data : "group_count",
                    render: $.fn.dataTable.render.number( ',', '.', 0 ),
                    "sClass" : 'text-right'
                },
                { 
                    data : "payment_amount_remaining_waitingforapprove",
                    render: function ( data, type, row ) {

                        /* If display or filter data is requested, format the currency */ 
                        if( type === 'display')
                        {
                            return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                        }

                        /*
                         * Otherwise the data type requested (`type`) is type detection or
                         * sorting data, for which we want to use the raw date value, so just
                         * that, unaltered
                         */
                        return data;
                    },
                    "sClass" : 'text-right'
                },
                { 
                    data : "payment_amount_remaining_bailment",
                    render: function ( data, type, row ) {

                        /* If display or filter data is requested, format the currency */ 
                        if( type === 'display')
                        {
                            return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                        }

                        /*
                         * Otherwise the data type requested (`type`) is type detection or
                         * sorting data, for which we want to use the raw date value, so just
                         * that, unaltered
                         */
                        return data;
                    },
                    "sClass" : 'text-right'
                },
                { 
                    data : "payment_amount_remaining_bailment_expired",
                    render: function ( data, type, row ) {

                        /* If display or filter data is requested, format the currency */ 
                        if( type === 'display')
                        {
                            return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                        }

                        /*
                         * Otherwise the data type requested (`type`) is type detection or
                         * sorting data, for which we want to use the raw date value, so just
                         * that, unaltered
                         */
                        return data;
                    },
                    "sClass" : 'text-right'
                }

            ];

            $.fn.dataTable.ext.errMode = 'none';

            $("#usersGroupsRevenue").DataTable({
                columns: _datacolumns,
                "language": {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Hiển thị _MENU_ kết quả mỗi trang",
                    "sZeroRecords":  "Không tìm thấy kết quả nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 kết quả",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm kiếm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Trang đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Trang cuối"
                    }
                }
            });
        }
    },
    template: `
    <table id="usersGroupsRevenue" class="table no-margin">
        <thead><tr><th v-for="item in headings" v-text="item"></th></tr></thead>
        <tbody>
            <template v-if="usersGroups != null">
                <tr v-for="item in usersGroups">
                    <td><input type="checkbox" :value="item.group_id" v-model="checkedUsersGroups"></td>
                    <td v-text="item.group_id == 0 ? '__Chưa xác định' : item.group_name"></td>
                    <td v-text="item.group_count"></td>
                    <td class="text-right" v-text="item.payment_amount_remaining_waitingforapprove"></td>
                    <td class="text-right" v-text="item.payment_amount_remaining_bailment"></td>
                    <td class="text-right" v-text="item.payment_amount_remaining_bailment_expired"></td>
                </tr>
            </template>
        </tbody>
    </table>
    `
});