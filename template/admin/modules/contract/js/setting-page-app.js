Vue.component('setting-bailment-component', {
    props: ['user_object'],
    data : function(){
        return {
            bailment_levels: jsobjectdata.bailment_levels,
            formAction: jsobjectdata.ajaxUrl.settingUserBailment,
            messages : null,
            msgType : null
        }
    },
    computed : {
        postdata: function() {
            return {
                edit: {
                    user_id: this.user_object.user_id,  
                },
                meta: {
                    bailment_level : this.user_object.bailment_level,
                    bailment_budget : this.user_object.bailment_budget,
                    bailment_quantity : this.user_object.bailment_quantity,
                }
            };
        },
        msgAlertBoxClass: function(){
            if(this.msgType == null) return '';
            else if(this.msgType == true) return 'alert alert-info';
            else if(this.msgType == false) return 'alert alert-warning';
            return '';
        }
    },
    watch: {
        user_object: function(){

            if( ! this.user_object) return;
            this.activeModal();
            this.clearAlertItems();
        },
    },
    methods: {
        clearAlertItems : function(){
            this.messages = null;
            this.msgType = null;
        },
        activeModal: function(){

            if( ! this.user_object) return;
            $("#setting-bailment-modal").modal('show');
        },
        deactiveModal: function(){

            $("#setting-bailment-modal").modal('hide');
            this.user_object = null;
        },
        suggetBailmentValues: function()
        {
            var self = this;
            $.each(this.bailment_levels,function(k,v){
                if(k == self.user_object.bailment_level)
                {
                    self.user_object.bailment_budget = v.budget;
                    self.user_object.bailment_quantity = v.quantity;
                }
            });
        },
        submitForm: function(){

            var seft = this;

            $.post(this.formAction, this.postdata, function( response ) {

                seft.msgType = response.status;
                if( ! response.status)
                {
                    seft.messages = response.data.errors;
                }
                else seft.messages = [response.msg];

            }, "json");
            
        },
        formatNumber: function(num)
        {
            return num.toLocaleString('en');
        }
    },
    template: `
    <div id="setting-bailment-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <template v-if="user_object != null">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Cấu hình định mức bảo lãnh cho nhân viên</h4>
                </div>
                <div class="modal-body">

                    <template v-if="messages">
                        <div v-for="message in messages" v-bind:class="msgAlertBoxClass" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ message }}
                        </div>
                    </template>

                    <form id="updateBailmentFrm" method="POST" class="form-horizontal">
                        <input type="hidden" name="edit[user_id]" v-model="user_object.user_id"/>
                        <div class="form-group">
                            <label for="bailment_level" class="col-sm-3 control-label">Định mức bảo lãnh</label>
                            <div class="col-sm-9">
                                <select v-on:change="suggetBailmentValues" v-model="user_object.bailment_level" v-if="bailment_levels" class="form-control input-sm" name="meta[bailment_level]">
                                    <option v-for="item in bailment_levels" :value="item.name" v-text="item.text"></option>
                                </select> 
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="bailment_budget" class="col-sm-3 control-label">Ngân sách tối đa</label>
                            <div class="col-sm-9">
                                <input type="number" v-model="user_object.bailment_budget" class="form-control" name="meta[bailment_budget]" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="bailment_quantity" class="col-sm-3 control-label">Số HĐ tối đa</label>
                            <div class="col-sm-9">
                                <input type="number" v-model="user_object.bailment_quantity" class="form-control" v-bind:value="user_object.bailment_quantity" name="meta[bailment_quantity]" />
                            </div>
                        </div>

                    </form>
                </div>
            </template>
            <div class="modal-footer">
                <button v-on:click="submitForm" type="button" class="btn btn-primary">Lưu Thay đổi</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
        </div>
    </div>
    `
});

Vue.component('datatable-pagination-component',{
    props: ['options'],
    computed : {
        current_page_text: function(){
            return this.options.current_page + '/' + this.options.num_page;
        }
    },
    methods : {
        updateCurrentPage : function(value){
            this.$emit('update_current_page',value);
        }
    },
    template: `
    <ul v-if="options" class="pagination pagination-sm pull-right">
        <li v-if="options.prev_page_url">
            <a href="#" v-on:click="updateCurrentPage(options.prev_page_url)"><i class="fa fa-fw fa-chevron-left"></i></a>
        </li>
        <li><a v-text="current_page_text"></a></li>
        <li v-if="options.next_page_url">
            <a href="#" v-on:click="updateCurrentPage(options.next_page_url)"><i class="fa fa-fw fa-chevron-right"></i></a>
        </li>
    </ul>
    `
});

Vue.component('sales-datatable-component', {
    props: [],
    data : function(){
       return {
            users : null,
            editUserObject : null,
            baseURL : admin_url,
            limit : 100,
            current_page : 1,
            showSearch : false,
            headings : [
                { 
                    key: 'user_id', name: '#', set_order: true, set_search:true,
                    searchOptions : {
                        type : 'input',
                        name : 'where[user_id]'
                    }
                },
                { 
                    key: 'display_name', name: 'Họ và tên', set_order: true, set_search:true,
                    searchOptions : {
                        type : 'input',
                        name : 'where[display_name]'
                    }
                },
                { 
                    key: 'user_email', name: 'E-mai', set_order: true, set_search:true,
                    searchOptions : {
                        type : 'input',
                        name : 'where[user_email]'
                    }
                },
                { 
                    key: 'user_phone', name: 'Số điện thoại', set_order: true, set_search:true,
                    searchOptions : {
                        type : 'input',
                        name : 'where[user_phone]'
                    }
                },
                { 
                    key: 'bailment_budget', name: 'Định mức', set_order: true, set_search:true,
                    searchOptions : {
                        type : 'input',
                        name : 'where[bailment_budget]'
                    }
                },
                { 
                    key: 'bailment_quantity', name: 'Số lượng', set_order: true, set_search:true,
                    searchOptions : {
                        type : 'input',
                        name : 'where[bailment_quantity]'
                    }
                },
                // { 
                //     key: 'bailment_process', name: 'Thực tế', set_order: true, set_search:true,
                //     searchOptions : {
                //         type : 'input',
                //         name : 'where[bailment_process]'
                //     }
                // },
                { 
                    key: 'action', name: 'Action', set_order: false, set_search:false 
                },
            ],
            searchs : [
                { key: 'user_id', name: '#', type:'input' },
                { key: 'display_name', name: 'Họ và tên', type:'input' },
                { key: 'user_email', name: 'E-mai', type:'input' },
                { key: 'user_phone', name: 'Số điện thoại', type:'input' },
                { key: 'bailment_budget', name: 'Định mức', type:'input' },
                { key: 'bailment_quantity', name: 'Số lượng', type:'input' },
                // { key: 'bailment_process', name: 'Thực tế', type:'input' },
            ],
            paging : null,
            order_by : {key : 'user_id', order : 'desc'},
            asc_icon : '<i class="fa fa-fw fa-sort-alpha-asc"></i>',
            desc_icon : '<i class="fa fa-fw fa-sort-alpha-desc"></i>'
        };
    },
    watch : {
        current_page : function(){
            this.buildTable();
        },
        order_by : function(){
            this.buildTable();
        },
        limit : function(){
            this.current_page = 1;
            this.buildTable();
        }
    },
    created : function() { this.buildTable(); },
    computed : {
        resource_url : function(){
            var seft = this;
            var order_obj = {};
            order_obj[this.order_by.key] = this.order_by.order;


            var params = { limit: this.limit, current_page: this.current_page , order_by: order_obj};
            var query = $.param( params );

            return this.baseURL + 'staffs/ajax/bailment/list?'+query;
        },
        colspan : function(){
            return this.headings.length;
        },
        isEmpty: function(){
            return (this.users == null || this.users.length <=0);
        },
    },
    methods : {
        buildTable : function() {
            var seft = this;
            axios
            .get(this.resource_url)
            .then(function (response) {

                var _data = response.data.data;
                if(_data.users.length == 0) return false;
                seft.users = _data.users;
                seft.paging = _data.paging; 
            });
        },
        updateCurrentPage : function(data){
            this.current_page = data;
        },
        updateOrderBy : function(order_by){

            if(this.order_by.key == order_by)
            {
                this.order_by = {key: order_by, order: (this.order_by.order == 'asc' ? 'desc' : 'asc')};
            }
            else
            {
                this.order_by = {key: order_by, order: 'desc'};
            }
        },
        sortFilter : function(order_by,order){
            this.order_by.key = order_by;
            this.order_by.order = (order == 'desc' ? 'asc' : 'desc');
        },
        genSortIcon : function(order_by){
            var elicon = this.asc_icon; 
            if(this.order_by.key == order_by)
            {
                elicon =  this.order_by.order == 'asc' ? this.asc_icon : this.desc_icon;
            }
            return elicon;
        },
        editUserObjectUpdated : function(editUserObject){
            this.editUserObject = editUserObject;
        },
        currencyFormatting: function(num)
        {
            num = parseInt(num);
            return num.toLocaleString('en') + 'đ';
        }
    },
    template: `
    <div class="container-fluid">

        <div class="col-md-3">
            <label >Hiển thị 
                <select class="input-sm" v-model="limit">
                    <option v-bind:value="10">10</option>
                    <option v-bind:value="25">25</option>
                    <option v-bind:value="50">50</option>
                    <option v-bind:value="100">100</option>
                    <option v-bind:value="200">200</option>
                </select> 
                 kết quả
            </label>
        </div>

        <datatable-pagination-component v-on:update_current_page="updateCurrentPage" v-bind:options="paging"></datatable-pagination-component>

        <table border="1" cellpadding="4" cellspacing="1" class="table table-responsive table-bordered table-hover">
            <thead v-if="headings">
                <tr>
                    <th v-for="heading in headings">
                        <a v-if="heading.set_order" v-html="genSortIcon(heading.key)" v-on:click="updateOrderBy(heading.key)"></a> {{ heading.name }} 
                    </th> 
                <tr/>
            </thead>
            <tbody>
                <tr v-if="showSearch">
                    <template v-for="heading in headings">
                        <td v-if="heading.set_search">
                            <template v-if="heading.type == input">
                                <input class="form-control" name="" value="" placeholder="heading.name"/>
                            </template>
                        </td>
                        <td v-else v-bind:colspan="colspan"></td>
                    </template>
                    
                </tr>
                <tr v-if="isEmpty">
                    <td v-bind:colspan="colspan" align="center">Không tìm thấy dữ liệu</td>
                </tr>
                <template v-else>
                    <tr v-for="user in users">
                        <td style="width: 5%" v-text="user.user_id"></td>
                        <td style="width: 20%" v-text="user.display_name"></td>
                        <td style="width: 20%" v-text="user.user_email"></td>
                        <td style="width: 20%" v-text="user.user_phone"></td>
                        <td style="width: 15%" v-text="currencyFormatting(user.bailment_budget)"></td>
                        <td style="width: 15%" v-text="user.bailment_quantity"></td>
                        <td>
                            <button v-on:click="editUserObjectUpdated(user)" class="btn btn-default btn-xs"><i class="fa fa-fw fa-cogs"></i></button>
                        </td>
                    </tr>
                </template>
            </tbody>
        </table>

        <datatable-pagination-component v-on:click="" v-on:update_current_page="updateCurrentPage" v-bind:options="paging"></datatable-pagination-component>

        <setting-bailment-component v-bind:user_object="editUserObject"></setting-bailment-component>
    </div>
    `
});

var app_root = new Vue({
    el: '#setting-page-root',
    data: {
        selectTab : 'bailment',
        salesUsers : null,
    },

    watch : { },

    computed : {
        
    },
    
    created: function(){ 

    },
    mounted : function(){ },
    methods: {

        unixToDate : function(unix) { return new Date(unix * 1000).format('d/m/Y H:i:s'); },
    }
});