/**
 * Component quản lý thông tin tất cả các nhân viên
 * Input : group_id
 * Output : users + terms belongs users
 */
Vue.component('terms-datatable-component', {
    props: ['datasource','heading'],
    data: function(){
        return { 
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            headings : ['Số hợp đồng','Khách hàng','Giá trị trước VAT','Phí dịch vụ','Thuế VAT','Dự thu','Còn phải thu','Tình trạng','Hạn bảo lãnh']
        }
    },
    beforeUpdate: function(){
        this.destroyDatatable();
    },
    updated : function(){
        this.buildDatatable();
    },
    methods: {
        destroyDatatable: function(){
            if ($.fn.DataTable.isDataTable("#"+this.guid))
            {
                $("#"+this.guid).DataTable().destroy();
            }
        },
        buildDatatable : function(){

            var datasource = this.datasource;
            $("#"+this.guid).DataTable({
                columns: [
                    { data : "contract_code" },
                    {  data : "customer_name" },
                    { 
                        data : "contract_value",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "service_fee",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "vat",
                        render: function ( data, type, row ) { return parseInt(data) +' %'; },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "invs_amount",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "payment_amount_remaining",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "term_status",
                        render: function ( data, type, row ) {

                            if(row.term_status == 'waitingforapprove' || row.term_status == 'Chờ duyệt') return 'Chờ duyệt';
                            else if(row.term_status == 'pending' || row.term_status == 'Tạm ngưng') return 'Tạm ngưng';
                            else if(row.term_status == 'publish' || row.term_status == 'Đang chạy') return 'Đang chạy';
                            else return '--';
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "receipt_end_date",
                        render: function ( data, type, row ) {

                            if(data == '' || data == 0 || data == 1) return '';

                            /* If display or filter data is requested, format the date */ 
                            if( type === 'display' || type === 'filter')
                            {
                                return moment.unix(row.receipt_end_date).format('DD-MM-YYYY');
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                ],
                "language": {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Hiển thị _MENU_ kết quả mỗi trang",
                    "sZeroRecords":  "Không tìm thấy kết quả nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 kết quả",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm kiếm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Trang đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Trang cuối"
                    }
                }
            });
        }
    },
    template: `
    <div class="col-md-12 col-xs-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title" v-text="heading"></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table v-bind:id="guid" class="table no-margin">
                        <thead>
                            <tr><th v-for="item in headings" v-text="item"></th></tr>
                        </thead>
                        <tbody>
                            <template v-if="datasource != null">
                                <tr v-for="item in datasource" v-if="item.payment_amount_remaining > 0">
                                    <td v-text="item.contract_code"></td>
                                    <td v-text="item.customer_name"></td>
                                    <td class="text-right" v-text="item.contract_value"></td>
                                    <td class="text-right" v-text="item.service_fee"></td>
                                    <td class="text-right" v-text="item.vat"></td>
                                    <td class="text-right" v-text="item.invs_amount"></td>
                                    <td class="text-right" v-text="item.payment_amount_remaining"></td>
                                    <td class="text-right" v-text="item.term_status"></td>
                                    <td class="text-right" v-text="item.receipt_end_date"></td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    `
});