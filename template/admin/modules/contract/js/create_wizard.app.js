Vue.component('wizard-partial-google-ads-component', {
    props : ['term','config'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
        };
    },
    template : `
    <div>
    <form action="http://localhost:9090/erp.webdoctor.vn/admin/contract/create_wizard/index/6552.html" method="post" accept-charset="utf-8">
        <input type="hidden" name="edit[term_id]" value="6552">
        <div class="col-md-12">
            <div class="box box-theme">
                <div class="box-header with-border">
                    <h3 class="box-title">Loại hình chạy quảng cáo</h3>
                    <div class="box-tools pull-right">
                        <button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Loại hình</label>
                        <div class="form-group col-md-10">
                            <div class="iradio_flat-blue checked" aria-checked="false" aria-disabled="false" style="position: relative;">
                                <input type="radio" name="edit[meta][service_type]" value="cpc_type" checked="checked" data-type="cpc_type" class="radio_service_type" _vimium-has-onclick-listener="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" _vimium-has-onclick-listener="" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                            &nbsp;
                            <label>CPC</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="iradio_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;">
                                <input type="radio" name="edit[meta][service_type]" value="account_type" data-type="account_type" class="radio_service_type" _vimium-has-onclick-listener="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" _vimium-has-onclick-listener="" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                            &nbsp;
                            <label>Quản lý tài khoản</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Kênh quảng cáo</label>
                        <div class="form-group col-md-10">
                            <div class="input-group col-sm-12">
                                <select multiple="" name="edit[meta][network_type][]" id="id_2078" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" style="width: 100%;">
                                    <option value="search" selected="selected">Search</option>
                                    <option value="display" selected="selected">GDN</option>
                                    <option value="facebook">Facebook</option>
                                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" _vimium-has-onclick-listener=""><ul class="select2-selection__rendered"><li class="select2-selection__choice" title="Search"><span class="select2-selection__choice__remove" role="presentation">×</span>Search</li>
                                <li class="select2-selection__choice" title="GDN"><span class="select2-selection__choice__remove" role="presentation">×</span>GDN</li>
                                <li class="select2-search select2-search--inline">
                                    <input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="textbox" placeholder="" style="width: 0.75em;">
                                </li>
                                </ul>
                                </span>
                                </span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="nav-tabs-custom">
                        <div class="tab-content" style="padding: 0px;">
                            <div id="cpc_type" class="tab-pane-service-type tab-pane active">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">[SEARCH] CPC cam kết</label>
                                    <div class="form-group col-md-10">
                                        <div class="input-group col-sm-12">
                                            <input type="text" name="edit[meta][contract_cpc]" value="" id="id_4860" class="form-control ">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">[SEARCH] Click cam kết</label>
                                    <div class="form-group col-md-10">
                                        <div class="input-group col-sm-12">
                                            <input type="text" name="edit[meta][contract_clicks]" value="" id="id_4848" class="form-control ">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">[GDN] CPC cam kết</label>
                                    <div class="form-group col-md-10">
                                        <div class="input-group col-sm-12">
                                            <input type="text" name="edit[meta][gdn_contract_cpc]" value="" id="id_5895" class="form-control ">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">[GDN] Click cam kết</label>
                                    <div class="form-group col-md-10">
                                        <div class="input-group col-sm-12">
                                            <input type="text" name="edit[meta][gdn_contract_clicks]" value="" id="id_5426" class="form-control ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="account_type" class="tab-pane-service-type tab-pane ">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Ngân sách cam kết</label>
                                    <div class="form-group col-md-10">
                                        <div class="input-group col-sm-12">
                                            <input type="text" name="edit[meta][contract_budget]" value="" id="id_460" class="form-control ">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Tỷ giá 1$ = ? VNĐ</label>
                                    <div class="form-group col-md-10">
                                        <div class="input-group col-sm-12">
                                            <input type="text" name="edit[meta][exchange_rate_usd_to_vnd]" value="22830" id="id_3304" class="form-control ">
                                        </div>
                                        <p class="help-block">Tỉ giá USD tại thời điểm hiện tại. Để trống nếu Adword chạy bằng VNĐ . Hệ thống sẽ dựa vào dữ liệu này để đối soát .</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Tỷ giá 1$ = ? VNĐ</label>
                                    <div class="form-group col-md-10">
                                        <div class="input-group col-sm-12">
                                            <input type="text" name="edit[meta][exchange_rate_aud_to_vnd]" value="0" id="id_3304" class="form-control ">
                                        </div>
                                        <p class="help-block">Tỉ giá AUD tại thời điểm hiện tại. Để trống nếu Adword chạy bằng VNĐ . Hệ thống sẽ dựa vào dữ liệu này để đối soát .</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Phí dịch vụ (VNĐ)</label>
                                    <div class="form-group col-md-10">
                                        <div class="input-group col-sm-12">
                                            <input type="text" name="edit[meta][service_fee]" value="" id="id_3203" class="form-control ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-theme">
                <div class="box-header with-border">
                    <h3 class="box-title">Cấu hình chung</h3>
                    <div class="box-tools pull-right">
                        <button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Ngày bắt đầu</label>
                        <div class="form-group col-md-10">
                            <div class="input-group col-sm-12">
                                <input type="text" name="edit[meta][googleads-begin_time]" value="" id="id_5162" class="form-control form-control set-datepicker">
                            </div>
                            <p class="help-block">Thời gian chạy adword chính thức</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Ngày Kết thúc</label>
                        <div class="form-group col-md-10">
                            <div class="input-group col-sm-12">
                                <input type="text" name="edit[meta][googleads-end_time]" value="" id="id_2211" class="form-control form-control set-datepicker">
                            </div>
                            <p class="help-block">Thời gian kết thúc adword chính thức</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Dịch vụ tặng kèm</label>
                        <div class="form-group col-md-10">
                            <div class="input-group col-sm-12">
                                <select name="edit[meta][bonus_package]" id="id_2366" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" style="width: 100%;">
                                    <option value="minimum" selected="selected">Không có dịch vụ tặng kèm</option>
                                    <option value="basic">Gói ngân sách QC từ 2 - 6 triệu/tháng</option>
                                    <option value="normal">Gói ngân sách QC trên 6 triệu/tháng</option>
                                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_2366-container"><span class="select2-selection__rendered" id="select2-id_2366-container" title="Không có dịch vụ tặng kèm">Không có dịch vụ tặng kèm</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span>
                                </span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-group col-md-10">
                <div class="input-group col-sm-12">
                    <input type="submit" name="confirm_step_service" value="confirm_step_service" id="confirm_step_service" class="form-control " style="display: none;">
                </div>
            </div>
        </div>
    </form>
    </div>
    
    `
});


var app_root = new Vue({
    el: '#app-container',
    computed: {},
    methods: {}
});