Vue.component('actions-component', {
    props: ['checked_users'],
    data: function(){
        return {
            exportFileUrl: base_url + 'admin/contract/ajax/dataset/exportUsersRevenue',
            isLoading: false
        };
    },
    methods: {
        exportFile: function(e){

            if(this.isLoading) {
                $.notify('Hệ thống đang xử lý ,...', "warning");
                return;
            }

            this.isLoading = true;
            var seft = this;
            
            axios
            .get(this.exportFileUrl, { params: {user_id : this.checked_users }})
            .then(function (response) {
                var response = response.data;
                if( ! response.status)
                {
                    $.notify(response.msg, "error");
                    seft.isLoading = false;
                    return false;
                }

                $.notify(response.msg,"success");
                seft.isLoading = false;

                window.open(base_url + response.data, '_seft');
                return true;
            });
        }
    },
    template: `
    <a v-on:click="exportFile" class="btn btn-app">
        <template v-if="isLoading">
            <i class="fa fa-pulse fa-spinner"></i> Tạo báo cáo...
        </template>
        <template v-else>
            <i class="fa fa-download"></i> Xuất báo cáo
        </template>
    </a>
    `
});

/* info-box */
Vue.component('info-box-component', {
    props: ['datasource','color','heading','icon'],
    computed: {
    	bg_color: function(){

    		if(this.color == 'yellow') return 'info-box bg-yellow';
    		else if(this.color == 'aqua') return 'info-box bg-aqua';
    		else if(this.color == 'aqua') return 'info-box bg-aqua';
    		else if(this.color == 'green') return 'info-box bg-green';
    		else if(this.color == 'red') return 'info-box bg-red';

    		return 'info-box';
    	},

        payment_amount_remaining: function(){

            if(this.datasource == null) return '0';
            return this.datasource.payment_amount_remaining.toLocaleString('en') + ' đ';
        },
        progress_rate: function(){
            if(this.datasource == null) return 0;

            if(this.datasource.payment_amount > this.datasource.invs_amount) return 100;

            return (this.datasource.payment_amount/this.datasource.invs_amount)*100
        },
        payment_amount: function(){
            if(this.datasource == null) return '0';
    		return this.datasource.payment_amount.toLocaleString('en');
        },
        invs_amount: function(){
            if(this.datasource == null) return '0';
            return this.datasource.invs_amount.toLocaleString('en');
        },
    	description: function(){
            return this.payment_amount +'/'+this.invs_amount + 'đ';
    		return 'Đã thu ' + this.payment_amount +' / '+this.invs_amount + 'đ';
    	}
    },
    template: `
    <div class="col-md-3">
	    <div v-bind:class="bg_color">
	        <div class="info-box-content" style="margin-left:0px">
	            <span class="info-box-text" v-text="heading"></span>
	            <span class="info-box-number" v-text="payment_amount_remaining"></span>
	            <div class="progress">
	                <div class="progress-bar" v-bind:style="{width: progress_rate +'%'}"></div>
	            </div>
	            <span class="progress-description" v-text="description"></span>
	        </div>
	        <!-- /.info-box-content -->
	    </div>
	    <!-- /.info-box -->
	</div>
    `
});

Vue.component('userss-revenue-component', {
    props: [''],
    data: function(){
        return {
            apiUrl : base_url + 'admin/contract/ajax/dataset/usersrevenue/',
            apiUsersTermsUrl : base_url + 'admin/contract/ajax/dataset/revenueUsers/',
            userTerms : null,
            checkedUsers: jsobjectdata.user_ids,
        };
    },
    watch: {
        checkedUsers : function(){
            this.$emit('checked_users_updated',this.checkedUsers);
            this.loadDatatableTerms();
        }
    },
    computed: {

        headings: function(){

            return [
                '',
                'STT',
                'Nhân viên kinh doanh',
                'Dự thu hợp đồng mới (chờ duyệt)',
                'Dự thu hợp đồng đang chạy (bảo lãnh đến hạn)',
                'Dự thu hợp đồng đang chạy (bảo lãnh quá hạn)'
            ];
        }
    },
    created: function(){
        this.loadDatatableTerms();
        this.loadDatatableUserTerms();
    },
    updated: function(){
        this.buildDatatable();
    },
    methods: {

        loadDatatableTerms: function(){
            var seft = this;
            var user_id = 'all';
            if(this.checkedUsers.length > 0) user_id = this.checkedUsers

            axios
            .get(this.apiUrl, { 
                params: {
                    user_id : user_id 
                }
            })
            .then(function (response) {
                var _data = response.data.data;
                seft.$emit('update_summary_boxes',_data);
            });   
        },
        loadDatatableUserTerms: function(){
            var seft = this;
            axios
            .get(this.apiUsersTermsUrl)
            .then(function (response) {

                var _data = response.data.data;
                seft.userTerms = _data;
            });   
        },
        currencyFormat: function(amount)
        {
            return amount.toLocaleString('en') + 'đ';
        },
        buildDatatable : function(){
            
            var _datacolumns = [
                { },
                { data : "increment" },
                { data : "display_name"},
                { 
                    data : "payment_amount_remaining_waitingforapprove",
                    render: $.fn.dataTable.render.number( ',', '.', 0 ),
                    "sClass" : 'text-right'
                },
                { 
                    data : "payment_amount_remaining_bailment",
                    render: $.fn.dataTable.render.number( ',', '.', 0 ),
                    "sClass" : 'text-right'
                },
                { 
                    data : "payment_amount_remaining_bailment_expired",
                    render: $.fn.dataTable.render.number( ',', '.', 0 ),
                    "sClass" : 'text-right'
                }

            ];

            $.fn.dataTable.ext.errMode = 'none';

            $("#userTermData").DataTable({
                columns: _datacolumns,
                "language": {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Hiển thị _MENU_ kết quả mỗi trang",
                    "sZeroRecords":  "Không tìm thấy kết quả nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 kết quả",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm kiếm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Trang đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Trang cuối"
                    }
                }
            });
        } 
    },
    template: `
    <table id="userTermData" class="table no-margin">
        <thead><tr><th v-for="item in headings" v-text="item"></th></tr></thead>
        <tbody>
            <template v-if="userTerms != null">
                <tr v-for="userTerm in userTerms">
                    <td><input type="checkbox" :value="userTerm.user_id" v-model="checkedUsers"></td>
                    <td v-text="userTerm.increment"></td>
                    <td v-text="userTerm.display_name"></td>
                    <td class="text-right" v-text="userTerm.payment_amount_remaining_waitingforapprove"></td>
                    <td class="text-right" v-text="userTerm.payment_amount_remaining_bailment"></td>
                    <td class="text-right" v-text="userTerm.payment_amount_remaining_bailment_expired"></td>
                </tr>
            </template>
        </tbody>
    </table>
    `
});

var app_root = new Vue({
    el: '#app-container',

    data: {
        totalSummary: null,
        bailmentSummary: null,
        bailmentExpriedSummary: null,
        waitingforapproveSummary: null,
        checkedUsers : [],
        terms_waitingforapprove : null,
        terms_bailment : null,
        terms_bailment_expired : null,
        // user_terms: null,
    },
    computed: {
        checkedUsersEl: function(){

            if(this.checkedUsers.length == 0) return 'Tất cả';
            return 'Đã chọn ' + this.checkedUsers.length + ' NVKD';
        },
        checkedUsersDescEl: function(){

            if(this.checkedUsers.length == 0) return 'Tất cả';
            return 'Hiển thị cho '+this.checkedUsers.length+' NVKD';
        }
    },
    methods: {
        updateSummaryBoxes : function(newData){
            
            this.totalSummary = newData.total;
            this.bailmentSummary = newData.bailment;
            this.bailmentExpriedSummary = newData.bailment_expired;
            this.waitingforapproveSummary = newData.waitingforapprove;

            // this.terms_waitingforapprove = newData.terms.waitingforapprove;
            // this.terms_bailment = newData.terms.bailment;
            // this.terms_bailment_expired = newData.terms.bailment_expired;
        },
        checkedUsersUpdated: function(values)
        {
            this.checkedUsers = values;
        }
    }
});