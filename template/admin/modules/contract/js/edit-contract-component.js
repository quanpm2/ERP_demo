Vue.component('edit-contract-component', {
    props: {
        id: { type: Number, default : 0 },
        guid : { type : String, default : () => { return 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);});} },
        read_only : { type : Boolean, default : false },
        update_mode : { type : Boolean, default : true }
    },
    data: function(){
        return {
            isLoading : false,
            readOnly : false,
            contract : null
        }
    },
    created : function(){
        this.loadContractData();
    },
    updated : function(){
        $( '#container-'+this.guid+' .myeditable' ).each(function( index ) {
            $(this).editable({
                url : (function(base_url, el) {

                    if(el.data("ajax_call_url") != undefined){

                        base_url += el.data("ajax_call_url");
                    }
                    else base_url += "<?php echo $ajax_segment;?>";
                    
                    return base_url; 

                })(admin_url, $(this)),
                params: function(params) {
                    params.type = $(this).data("type-data");
                    return params;
                },
                success: function(data, newValue) {
                    if($.isEmptyObject(data.response)) return;
                    if($.isEmptyObject(data.response.response)) return;
                    if(data.response.response.hasOwnProperty("jscallback")){
                        $.each(data.response.response.jscallback,function(i,e){
                            var fn = window[e.function_to_call];
                            fn(e.data);
                        });
                    }
                },
                defaultValue: "",
            });   
        });
    },
    methods : {

        loadContractData : function(){
            
            var seft = this;
            let obj = {
                id : this.id,
                meta : [
                    'lock_editable',
                    'contract_code',
                    'representative_gender',
                    'representative_name',
                    'representative_email',
                    'representative_address',
                    'representative_zone',
                    'representative_phone',
                    'representative_position',
                    'customer_tax',
                    'contract_value',
                    'number_of_payments',
                    'vat',
                    'contract_begin',
                    'contract_end',
                    'customer'
                ]
            };

            axios.get(admin_url + 'contract/api/contract/index?' + $.param(obj))
            .then(function (response) {
                seft.contract = response.data.data;
                console.log(seft.contract);
                seft.lock_editable = new Boolean(parseInt(response.data.data.lock_editable));
                seft.readOnly = seft.lock_editable;
            })
            .catch(function (error) {});
        },

        currencyFormat : function(value){
            let val = parseInt(value);
            if( Number.isNaN(val)) return '0 đ';
            return val.toLocaleString() + ' đ';
        },

        unixToDate : function(value){
            return moment.unix(value).utc().format("DD/MM/YYYY");
        },
    },
    template: `
    <div class="col-md-6" :id="'container-'+guid">
        <table border="1" cellpadding="2" cellspacing="1" class="table table-responsive table-bordered table-hover col-6">
            <caption>Thông tin hợp đồng</caption>
            <tbody v-if="contract">
                <tr>
                    <td>Mã hợp đồng</td>
                    <td>
                        <xeditable :pk="id" type="text" name="contract_code" :value="contract.contract_code" metadata="meta" title="Số hợp đồng" :remote_url="admin_url + 'contract/api/contract/update'"></xeditable>
                    </td>
                </tr>
                <tr>
                    <td>Khách hàng</td>
                    <td>Lê Tuấn Cương<br>
                    <a :href="admin_url + 'customer/edit/' + contract.customer.user_id" target="_blank">xem hồ sơ khách hàng</a></td>
                </tr>
                <tr>
                    <td>Người đại diện</td>
                    <td><xeditable :pk="id" type="text" name="representative_name" :value="contract.representative_name" title="Người đại diện" metadata="meta" :remote_url="admin_url + 'contract/api/contract/update'"></xeditable></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><xeditable :pk="id" type="text" name="representative_email" :value="contract.representative_email" title="Email" metadata="meta" :remote_url="admin_url + 'contract/api/contract/update'"></xeditable></td>
                </tr>
                <tr>
                    <td>Địa chỉ</td>
                    <td><xeditable :pk="id" type="text" name="representative_address" :value="contract.representative_address" title="Địa chỉ người đại diện" metadata="meta" :remote_url="admin_url + 'contract/api/contract/update'"></xeditable></td>
                </tr>
                <tr>
                    <td>Vùng miền</td>
                    <td><xeditable :pk="id" type="text" name="representative_address" :value="contract.representative_address" title="Vùng miền" metadata="meta" :remote_url="admin_url + 'contract/api/contract/update'"></xeditable></td>
                    <td>
                        <div class="input-group col-sm-12"><a href="#" type="text" value="Hồ Chí Minh" id="id_203" data-original-title="Vùng miền" data-pk="9709" data-type-data="meta_data" data-name="representative_zone" data-type="select" data-value="hcm" data-source="[{value: 'hcm', text: 'Hồ Chí Minh'},{value: 'hn', text: 'Hà Nội'}]" none_label="1" override_jscript="1" class="myeditable editable editable-click">Hồ Chí Minh</a></div>
                    </td>
                </tr>
                <tr>
                    <td>Số điện thoại</td>
                    <td>
                        <div class="input-group col-sm-12"><a href="#" type="text" value="0908494861" id="id_485" data-original-title="Số điện thoại" data-pk="9709" data-type-data="meta_data" data-type="number" data-name="representative_phone" none_label="1" override_jscript="1" class="myeditable editable editable-click">0908494861</a></div>
                    </td>
                </tr>
                <tr>
                    <td>Chức vụ</td>
                    <td>
                        <div class="input-group col-sm-12"><a href="#" type="text" value="" id="id_7973" data-original-title="Chức vụ" data-pk="9709" data-type-data="meta_data" data-name="representative_position" data-value="" none_label="1" override_jscript="1" class="myeditable editable editable-click editable-empty">Empty</a></div>
                    </td>
                </tr>
                <tr>
                    <td>Mã Số thuế</td>
                    <td>
                        <div class="input-group col-sm-12"><a href="#" type="text" value="" id="id_6520" data-original-title="Mã Số thuế" data-pk="9709" data-type-data="extra" data-name="customer_tax" data-value="" none_label="1" override_jscript="1" class="myeditable editable editable-click editable-empty">Empty</a></div>
                    </td>
                </tr>
                <tr>
                    <td>Giá trị hợp đồng</td>
                    <td>
                        <div class="input-group col-sm-12"><a href="#" type="text" value="6,000,000 VNĐ" id="id_3551" data-original-title="Giá trị hợp đồng" data-pk="9709" data-type-data="meta_data" data-type="number" data-name="contract_value" data-value="6000000" data-ismoney="true" none_label="1" override_jscript="1" class="myeditable editable editable-click">6,000,000 VNĐ</a></div>
                    </td>
                </tr>
                <tr>
                    <td>Số lần thanh toán</td>
                    <td>
                        <div class="input-group col-sm-12"><a href="#" type="text" value="3" id="id_6085" data-original-title="Số lần thanh toán" data-pk="9709" data-type-data="meta_data" data-type="number" data-name="number_of_payments" data-value="3" data-ismoney="true" none_label="1" override_jscript="1" class="myeditable editable editable-click">3</a></div>
                    </td>
                </tr>
                <tr>
                    <td>% VAT</td>
                    <td>
                        <div class="input-group col-sm-12"><a href="#" type="text" value="0%" id="id_6429" data-original-title="% VAT" data-pk="9709" data-type-data="meta_data" data-type="number" data-name="vat" data-value="0" none_label="1" override_jscript="1" class="myeditable editable editable-click">0%</a></div>
                    </td>
                </tr>
                <tr>
                    <td>Ngày bắt đầu</td>
                    <td>
                        <div class="input-group col-sm-12"><a href="#" type="text" value="05-07-2018" id="id_3828" data-original-title="Ngày bắt đầu" data-pk="9709" data-type-data="meta_data" data-type="combodate" data-value="05-07-2018" data-format="DD-MM-YYYY" data-viewformat="DD-MM-YYYY" data-template="D MM YYYY" data-name="contract_begin" none_label="1" override_jscript="1" class="myeditable editable editable-click">05-07-2018</a></div>
                    </td>
                </tr>
                <tr>
                    <td>Ngày kết thúc</td>
                    <td>
                        <div class="input-group col-sm-12"><a href="#" type="text" value="20-07-2018" id="id_3287" data-original-title="Ngày kết thúc" data-pk="9709" data-type-data="meta_data" data-type="combodate" data-value="20-07-2018" data-format="DD-MM-YYYY" data-viewformat="DD-MM-YYYY" data-template="D MM YYYY" data-name="contract_end" none_label="1" override_jscript="1" class="myeditable editable editable-click">20-07-2018</a></div>
                    </td>
                </tr>
                <tr>
                    <td>Tệp đính kèm</td>
                    <td>
                        <form action="http://localhost:9090/erp.webdoctor.vn/admin/contract/edit/9709.html" method="post" accept-charset="utf-8">
                            <div class="input-group col-sm-12">
                                <div class="file-input file-input-new">
                                    <div class="kv-upload-progress hide">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                0%
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group ">
                                        <div tabindex="500" class="form-control file-caption  kv-fileinput-caption">
                                            <div class="file-caption-name"></div>
                                        </div>
                                        <div class="input-group-btn">
                                            <button type="button" tabindex="500" title="Abort ongoing upload" class="btn btn-default hide fileinput-cancel fileinput-cancel-button"><i class="glyphicon glyphicon-ban-circle"></i> <span class="hidden-xs">Cancel</span></button>
                                            <a href="http://localhost:9090/erp.webdoctor.vn/admin/contract/ajax_dipatcher/act_upload_real_file" tabindex="500" title="Upload selected files" class="btn btn-default fileinput-upload fileinput-upload-button"><i class="glyphicon glyphicon-upload"></i> <span class="hidden-xs">Upload</span></a>
                                            <div tabindex="500" class="btn btn-primary btn-file"><i class="glyphicon glyphicon-folder-open"></i> <span class="hidden-xs">Browse …</span><input type="file" name="fileinput" value="" id="contract-inputfile" multiple="multiple" none_label="1" class="form-control file_loading"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class="help-block"></p>
                            <div id="grid_files"></div>
                            <p></p>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    `
});