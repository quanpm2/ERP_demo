Vue.component('list-invoices-component', {
    props: {
        id: { type: Number, default : 0 },
        guid : { type : String, default : () => { return 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);});} },
        read_only : { type : Boolean, default : false },
        update_mode : { type : Boolean, default : true }
    },
    data: function(){
        return {
            invoices : [],
            isLoading : false,
            readOnly : false
        }
    },
    created : function(){
        this.loadContractData();
        this.getInvoices();
    },
    updated : function(){
        $( '#container-'+this.guid+' .myeditable' ).each(function( index ) {
            $(this).editable({
                url : (function(base_url, el) {

                    if(el.data("ajax_call_url") != undefined){

                        base_url += el.data("ajax_call_url");
                    }
                    else base_url += "<?php echo $ajax_segment;?>";
                    
                    return base_url; 

                })(admin_url, $(this)),
                params: function(params) {
                    params.type = $(this).data("type-data");
                    return params;
                },
                success: function(data, newValue) {
                    if($.isEmptyObject(data.response)) return;
                    if($.isEmptyObject(data.response.response)) return;
                    if(data.response.response.hasOwnProperty("jscallback")){
                        $.each(data.response.response.jscallback,function(i,e){
                            var fn = window[e.function_to_call];
                            fn(e.data);
                        });
                    }
                },
                defaultValue: "",
            });   
        });
    },
    methods : {

        loadContractData : function(){
            
            var seft = this;
            let obj = {
                id : this.id,
                meta : ['lock_editable']
            };

            axios.get(admin_url + 'webbuild/api/contract/index?' + $.param(obj))
            .then(function (response) {

                seft.lock_editable = new Boolean(parseInt(response.data.data.lock_editable));
                seft.readOnly = seft.lock_editable;
            })
            .catch(function (error) {});
        },

        getInvoices : function(){
            var seft = this;
            let obj = {contract_id : this.id};
            axios.get(base_url + 'api-v2/contract/invoices/index?' + $.param(obj))
            .then(function (response) { 
                seft.invoices = response.data.data;
            })
            .catch(function (error) {});
        },

        currencyFormat : function(value){
            let val = parseInt(value);
            if( Number.isNaN(val)) return '0 đ';
            return val.toLocaleString() + ' đ';
        },

        unixToDate : function(value){
            return moment.unix(value).utc().format("DD/MM/YYYY");
        },

        sync : function(){

            var seft = this;
            seft.isLoading = true;
            $.post(admin_url + 'contract/api/contract/sync_invoices', {id:this.id})
            .done(function(response) {

                let messages = response.msg;
                for(let i in messages)
                {
                    $.notify(messages[i], 'success');
                }

                seft.getInvoices();
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                $.notify(JSON.parse(jqXHR.responseText));
            })
            .always(function(){
                seft.isLoading = false;
            });
        },
    },
    template: `
    <div class="box box-warning box-solid" :id="'container-'+ guid">
        <div class="box-header with-border">
            <h3 class="box-title"><i>Danh sách các đợt thanh toán</i></h3>
            <div class="box-tools pull-right" v-if="readOnly == false">
                <button v-if="isLoading" type="button" class="btn btn-flat btn-sm btn-success pull-right disabled">
                    <i class="fa fa-spinner fa-spin"> </i> Đang tiến hàng đồng bộ ...
                </button>
                <button v-else v-on:click="sync" type="button" class="btn btn-flat btn-sm btn-success pull-right"><i class="fa fa-fw  fa-random"></i> Đồng bộ | Khởi tạo đợt thanh toán</button>
            </div>
        </div>
        <div class="box-body">
            <table border="1" cellpadding="2" cellspacing="1" class="table table-responsive table-bordered table-hover"">
            <thead>
                <tr>
                    <th>Tiêu đề</th>
                    <th>Ngày bắt đầu</th>
                    <th>Ngày kết thúc</th>
                    <th align="right">Tổng tiền</th>
                </tr>
            </thead>
            <tbody>
                <tr v-if="invoices.length > 0" v-for="item in invoices">
                    <td>
                        <xeditable
                        :pk="item.post_id"
                        type="text"
                        name="post_title"
                        :value="item.post_title"
                        :remote_url="admin_url + 'contract/invoices/ajax_dipatcher/ajax_edit/' + item.post_id"
                        metadata="field"
                        title="Tiêu đề đợt thanh toán"></xeditable>
                    </td>
                    <td>
                        <xeditable
                        :pk="item.post_id"
                        type="combodate"
                        name="start_date"
                        :value="unixToDate(item.start_date)"
                        :remote_url="admin_url + 'contract/invoices/ajax_dipatcher/ajax_edit/' + item.post_id"
                        metadata="field"
                        title="Ngày bắt đầu phiếu thu"></xeditable>
                    </td>
                    <td>
                        <xeditable
                        :pk="item.post_id"
                        type="combodate"
                        name="end_date"
                        :value="unixToDate(item.end_date)"
                        :remote_url="admin_url + 'contract/invoices/ajax_dipatcher/ajax_edit/' + item.post_id"
                        metadata="field"
                        title="Ngày kết thúc phiếu thu"></xeditable>
                    </td>
                    <td v-text="currencyFormat(item.price_total)" align="right"></td>
                </tr>
                <tr v-else>
                    <td colspan="4">Chưa có hóa đơn</td>
                </tr>
            </tbody>
        </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    `
});