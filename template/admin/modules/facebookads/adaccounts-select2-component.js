Vue.component('adaccounts-select2-component',{
    props: ['name','label','value','url', 'adaccount_source', 'external_bmid', 'external_adaccount_id', 'external_adaccount_spent'],
    data: function(){
        return {
            options : [],
            guid: _.uniqueId('adaccounts-select2-'),
        };
    },
    computed : {
    },
    updated: function(){
        $('#'+this.guid).css('width', '100%');
        $('#'+this.guid).select2();
        $('#'+this.guid).removeClass('form-control');
    },
    created: function() {
        this.loadDataSource();
    },
    methods : {

        loadDataSource () {
            return axios.get(`${base_url}api-v2/facebookads/adaccounts`)
            .then(response => {
                this.options = _.get(response, 'data.data')
            })
        }
    },
    template: `
    <div>
        <div class="input-group form-group">
            <div class="radio">
                <label style="margin-right:10px">
                    <input type="radio" name="edit[meta][adaccount_source]" v-model="adaccount_source" value="internal">Tài khoản nội bộ
                </label>
                <label>
                    <input type="radio" name="edit[meta][adaccount_source]" v-model="adaccount_source" value="external">Tài khoản ngoài
                </label>
            </div>
        </div>

        <div v-if="'internal' == adaccount_source">
            <div class="input-group form-group">
                <span class="input-group-addon" v-html="label"></span>
                <select :name="name" :id="guid" class="form-control" multiple="multiple">
                    <option value="0">Chưa cấu hình</option>
                    <template v-if="options.length > 0">
                        <option v-for="item in options" 
                            :selected="item.term_id == value ? 'selected' : ''"  
                            :value="item.term_id">{{ item.term_name }} - {{ item.term_slug }}</option>
                    </template>
                </select>
                <span class="input-group-addon"><input type="submit" name="authentization" value="Reload" class="btn btn-xs btn-danger"  />
                </span>
            </div>
        </div>

        <div v-if="'external' == adaccount_source" class="input-group form-group">
            <span class="input-group-addon" style="min-width: 110px;text-align: left;"><i class='fa fa-buysellads'></i> BM</span>
            <input name="edit[meta][external_bmid]" v-model="external_bmid" type="text" class="form-control" />
        </div>

        <div v-if="'external' == adaccount_source" class="input-group form-group">
            <span class="input-group-addon" style="min-width: 110px;text-align: left;"><i class='fa fa-buysellads'></i> ID Tài khoản</span>
            <input name="edit[meta][external_adaccount_id]" v-model="external_adaccount_id" type="text" class="form-control" />
        </div>
    </div>
    `
});