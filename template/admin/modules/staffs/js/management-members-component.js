Vue.component('management-members-component', {
    props: ['user_groups','selected_user_group','guid'],
    data: function(){

        return {
            users: [],
            loadUsersUrl: admin_url + 'staffs/ajax/users/list/',
            usersTableId: "table-" + this.guid,
            headings: ["","Họ và tên","E-mail","Quyền hạn"],
            checkedUsers: [],
            checkedNamesusers : '',
            excludeUsers : [],
        }
    },
    watch: {
        selected_user_group: function(){

            if(this.selected_user_group == null)
            {
                return;
            }

            this.checkedUsers = this.selected_user_group.user_ids;
            this.checkedNamesusers = '';

            this.getExcludeUsers();
        },
    },
    computed: {

        userGroupUpdateApi: function(){
            return admin_url + 'staffs/ajax/groups/update/' + this.selected_user_group.term_id;
        },
        modalHeading : function() {
            return "Quản lý thành viên trong nhóm [Số lượng nhóm viên : " + this.checkedUsers.length + " ]";
        },

        checkedGroupUsers : function(){
            let result = [];

            if(_.isEmpty(this.checkedUsers)) return result;

            var seft = this;
            _.forEach(this.checkedUsers, function(user_id) {

                // console.log(user_id, seft.users);
                let user = _.find(seft.users, { 'user_id': user_id});
                if(_.isEmpty(user)) return;

                result.push(user);
            });

            return result;
        },
    },
    created: function() {

        /* List thông tin nhân viên kinh doanh đang hoạt động */
        this.loadUsers();
    },

    beforeUpdate: function(){
        this.destroyDatatable();
    },

    updated: function() {
        /* Construct users list datatable */
        this.buildDatatable();
    },
    methods: {

        removeUser : function(user_id)
        {
            let _checkedUsers = this.checkedUsers;
            _.pull(_checkedUsers, user_id);

            this.checkedUsers = [];
            this.checkedUsers = _checkedUsers;
        },

        getExcludeUsers : function(){
            
            if(this.selected_user_group == null) return;

            var relateUsers = this.selected_user_group.user_ids;
            var excludeUsers = [];

            for (var i = 0; i < this.user_groups.length; i++)
            {
                if(this.user_groups[i].user_ids.length == 0) continue;

                var j = 0;
                for (j; j < this.user_groups[i].user_ids.length; j++) {

                    if($.inArray(this.user_groups[i].user_ids[j], relateUsers) >= 0) continue;

                    excludeUsers.push(this.user_groups[i].user_ids[j]);
                }
            }

            this.excludeUsers = excludeUsers;
        },

        destroyDatatable: function(){
            if ($.fn.DataTable.isDataTable("#"+this.usersTableId))
            {
                $("#"+this.usersTableId).DataTable().destroy();
            }
        },

        /* Build data table handle all sale users */
        buildDatatable : function(){

            if ($.fn.DataTable.isDataTable("#"+this.usersTableId)) return true;

            $.fn.dataTable.ext.errMode = 'none';
            $("#"+this.usersTableId).DataTable({
                columns: [
                    { },
                    { data : "display_name" },
                    { data : "user_email" },
                    { data : "role_name" }  
                ],
                "language": {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Hiển thị _MENU_ kết quả mỗi trang",
                    "sZeroRecords":  "Không tìm thấy kết quả nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 kết quả",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm kiếm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Trang đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Trang cuối"
                    }
                }
            });
        },

        /* Request List all Sale users  */
        loadUsers: function(){
            var seft = this;
            axios
            .get(this.loadUsersUrl, { params: {user_id : this.checked_users }})
            .then(function (response) {
                var _data = response.data.data;
                seft.users = [];
                seft.users = _data;
            });
        },

        /* Functional output Notify message on right screen */
        notify: function(msg,className){

            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },

        /* Submit handler update selected User mapping to User Group */
        submit: function(){

            var seft = this;

            var params = { user_ids : (this.checkedUsers.length == 0 ? [0] : this.checkedUsers) }

            $.post(this.userGroupUpdateApi, params , function( response ) {

                seft.msgType = response.status;
                if( ! response.status)
                {
                    seft.notify(response.msg, "error");
                    return false;
                }

                seft.notify(response.msg, "success");
                seft.selected_user_group.term_count = seft.checkedUsers.length;
                seft.selected_user_group.user_ids = seft.checkedUsers;
                seft.$emit('user_group_updated',seft.selected_user_group);

            }, "json");
        }
    },
    template: `
    <div class="modal fade" :id="guid">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" v-text="modalHeading"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-xs-7">
                            <table :id="usersTableId" class="table no-margin">
                                <thead><tr><th v-for="item in headings" v-text="item"></th></tr></thead>
                                <tbody>
                                    <tr v-for="user in users" v-if="$.inArray(user.user_id, excludeUsers) < 0">
                                        <td><input type="checkbox" :value="user.user_id" v-model="checkedUsers"></td>
                                        <td v-text="user.display_name"></td>
                                        <td v-text="user.user_email"></td>
                                        <td class="text-right" v-text="user.role_name"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-5">
                            <h4 class="no-margin">Danh sách thành viên</h4>
                            <br>
                            <p v-if="_.isEmpty(checkedGroupUsers)" class="text-muted"><i>Chưa có bất kỳ thành viên nào trong nhóm.</i></p>
                            <ul v-else class="todo-list">
                                <li v-for="i in checkedGroupUsers">
                                    <span class="text">{{i.display_name}} ({{i.role_name}})</span>
                                    <div class="tools">
                                        <i v-on:click="removeUser(i.user_id)" class="fa fa-trash-o"></i>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button v-on:click="submit" type="button" class="btn btn-primary">Lưu thay đổi</button>
                </div>
            </div>
        </div>
    </div>
    `
});