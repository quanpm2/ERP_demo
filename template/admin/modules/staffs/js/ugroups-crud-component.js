Vue.component('ugroups-crud-component',{

    props: ['selected_user_group','guid'],
    data: function(){

        return {
            postData: {
                edit: {
                    term_id: 0,
                    term_description: '',
                    term_name:"",
                    term_parent:0,
                    term_email:""
                }
            },
            messages: null,
            msgType: null,
            departments: [],
        }
    },
    watch: {
        selected_user_group: function(){
            if(this.selected_user_group == null)
            {
                this.postData.edit = {term_id: 0,term_name:"",term_description:"",term_parent:0,term_email:""};

                return;
            }

            this.postData.edit = {
                term_id: this.selected_user_group.term_id,
                term_name: this.selected_user_group.term_name,
                term_description: this.selected_user_group.term_description,
                term_parent: this.selected_user_group.term_parent,
                term_email:this.selected_user_group.term_email
            };
            
        }
    },
    computed: {
        userGroupsUpdateApi: function(){

            return admin_url + 'staffs/ajax/groups/update/' + this.postData.edit.term_id;
        },
        modalHeading: function(){

            if(this.selected_user_group == null) return "Thêm mới nhóm làm việc";
            return "Cập nhật nhóm làm việc"
        },
    },
    created: function() {
        this.loadDepartments();
    },
    methods: {

        loadDepartments: function(){
            var seft = this;
            axios
            .get(admin_url + 'staffs/ajax/users/list_departments')
            .then(function (response) {
                var _data = response.data.data;

                var departments = [];
                for (var i = 0; i < _data.length; i++)
                {
                    departments.push({'key':_data[i].term_id,'text':_data[i].term_name});
                }

                seft.departments = departments;
            });
        },

        notify: function(msg,className)
        {
            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },
        submit: function(){

            var seft = this;
           
            $.post(this.userGroupsUpdateApi, this.postData, function( response ) {

                seft.msgType = response.status;
                if( ! response.status)
                {
                    seft.notify(response.msg, 'error');
                    return false;
                }

                /**
                 * Thông báo có sự thay đổi dữ liệu phía component child
                 * Nếu Thêm mới : thông báo đã thêm mới , ngược lại thì thông báo đã cập nhật
                 */
                if(seft.selected_user_group == null) seft.$emit('user_group_inserted',response.data);
                else seft.$emit('user_group_updated',response.data);

                seft.selected_user_group = response.data;
                seft.notify(response.msg, 'success');

            }, "json");
        }
    },
    template: `
    <div class="modal fade" :id="guid">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" v-text="modalHeading"></h4>
                </div>
                <div class="modal-body">

                    <form id="updateBailmentFrm" method="POST" class="form-horizontal">

                        <div class="form-group">
                            <label for="term_name" class="col-sm-2 control-label">Tên nhóm</label>
                            <div class="form-group col-md-10">
                                <div class="input-group col-sm-10">
                                    <input type="text" v-model="postData.edit.term_name" class="form-control" name="meta[term_name]" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="term_name" class="col-sm-2 control-label">Mô tả</label>
                            <div class="form-group col-md-10">
                                <div class="input-group col-sm-10">
                                    <textarea v-model="postData.edit.term_description" class="form-control" name="meta[term_description]" class="form-control" rows="3" placeholder="Mô tả ..."></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="term_parent" class="col-sm-2 control-label">Phòng ban</label>
                            <div class="form-group col-md-10">
                                <div class="input-group col-sm-10">
                                    <select name="edit[term_parent]" class="form-control" v-model="postData.edit.term_parent">
                                        <option value="0">Chưa cấu hình</option>
                                        <template v-if="departments.length > 0">
                                            <option v-for="opt in departments" :value="opt.key" v-text="opt.text">
                                                {{ opt.text }}
                                            </option>
                                        </template>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="term_email" class="col-sm-2 control-label">Email</label>
                            <div class="form-group col-md-10">
                                <div class="input-group col-sm-10">
                                    <input type="text" v-model="postData.edit.term_email" class="form-control" name="meta[term_email]" />
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button v-on:click="submit" type="button" class="btn btn-primary">Lưu thay đổi</button>
                </div>
            </div>
        </div>
    </div>
    `
});
