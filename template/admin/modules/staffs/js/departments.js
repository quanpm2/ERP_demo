Vue.component('remove-modal-component', {
    props: ['selected_user_group','guid'],
    computed: {
        userDepartmentRemovedApi: function(){ return admin_url + 'staffs/ajax/departments/delete/' + this.selected_user_group.term_id; },
        modalHeading : function() { return "Thông báo xác nhận"; }
    },
    methods: {

        /* Functional output Notify message on right screen */
        notify: function(msg,className){

            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },

        /* Submit handler update selected User mapping to User Department */
        submit: function(){

            if(this.selected_user_group == null)
            {
                this.notify('Thông tin không hợp lệ ! Vui lòng thử lại sau .', "warn");
                return false;
            }

            if(this.selected_user_group.user_group.length > 0)
            {
                this.notify('Yêu cầu xử lý không hợp lệ ! Vui lòng thử lại sau .', "warn");
                return false;
            }

            var seft = this;

            $.post(this.userDepartmentRemovedApi, {}, function( response ) {

                seft.msgType = response.status;
                if( ! response.status)
                {
                    seft.notify(response.msg, "error");
                    return false;
                }

                seft.notify(response.msg, "success");
                seft.$emit('user_department_removed',seft.selected_user_group);

            }, "json");
        }
    },
    template: `
    <div class="modal fade" :id="guid" v-if="selected_user_group != null">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" v-text="modalHeading"></h4>
                </div>
                <div class="modal-body">
                    <p v-if="selected_user_group.user_group.length > 0">
                        phòng ban đang chứa <span v-text="selected_user_group.user_group.length"></span> thành viên nên không thể xóa ! <br/>
                        Nếu muốn tiếp tục thực hiện ! xóa các thành viên ra khỏi phòng ban và thực hiện tại
                    </p>
                    <p v-else>Bạn có chắc muốn xóa phòng ban ! Sau khi thực hiện sẽ không thể hoàn tác !!!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button v-if="selected_user_group.user_group.length == 0" v-on:click="submit" type="button" class="btn btn-primary" data-dismiss="modal">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
    `
});


Vue.component('management-members-component', {
    props: ['selected_user_group','guid','department_id'],
    data: function(){

        return {
            user_groups: [],
            loadUsersUrl:null,
            userGroupsTableId: "table-" + this.guid,
            headings: ["","Tên phòng ban","E-mail","Số lượng thành viên"],
            checkedUserGroups: [],
            checkedNamesusers : ''
        }
    },
    watch: {
        selected_user_group: function(){

            if(this.selected_user_group == null)
            {
                
                return;
            }
            this.checkedUserGroups = this.selected_user_group.user_group;
            this.checkedNamesusers = '';
        },
        department_id:function(){
            this.loadUsersUrl = admin_url + 'staffs/ajax/departments/list_user_groups/'+ this.department_id,
            this.loadUsers();
        }
    },
    computed: {

        userDepartmentUpdateApi: function(){
            return admin_url + 'staffs/ajax/departments/update/' + this.selected_user_group.term_id;
        },
        modalHeading : function() {
            return "Quản lý phòng ban trong Phòng ban [Số lượng phòng ban : " + this.checkedUserGroups.length + " ]";
        },
        checkNameUsers: function() {
            var names = [];
            if(this.checkedUserGroups.length == 0) return '';

            for (var i = 0; i < this.checkedUserGroups.length; i++)
            {
               if(typeof this.user_groups != 'undefined')
               {
                    for (var j = 0; j < this.user_groups.length; j++)
                    {
                        if(this.checkedUserGroups[i] != this.user_groups[j].term_id) continue;

                        names.push(this.user_groups[j].term_name + '('+this.user_groups[j].term_count+')');
                    }  
               }
                  
            }
            if(names.length > 0) return "phòng ban : " +  names.join();
            return '';
        }
    },
    created: function() {

        /* List thông tin nhân viên kinh doanh đang hoạt động */
        
    },

    updated: function() {
        /* Construct users list datatable */
        this.buildDatatable();
    },
    methods: {

        /* Build data table handle all sale users */
        buildDatatable : function(){

            if ($.fn.DataTable.isDataTable("#"+this.userGroupsTableId)) return true;

            $.fn.dataTable.ext.errMode = 'none';
            $("#"+this.userGroupsTableId).DataTable({
                columns: [
                    { },
                    { data : "term_name" },
                    { data : "term_email" },
                    { data : "term_count" }  
                ],
                "language": {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Hiển thị _MENU_ kết quả mỗi trang",
                    "sZeroRecords":  "Không tìm thấy kết quả nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 kết quả",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm kiếm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Trang đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Trang cuối"
                    }
                }
            });
        },

        /* Request List all Sale users  */
        loadUsers: function(){
            var seft = this;
            axios
            .get(this.loadUsersUrl, { params: {user_id : this.checked_users }})
            .then(function (response) {
                var _data = response.data.data;
                seft.user_groups = _data;
            });
        },

        /* Functional output Notify message on right screen */
        notify: function(msg,className){

            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },

        /* Submit handler update selected User mapping to User Department */
        submit: function(){

            var seft = this;

            var params = { user_group : (this.checkedUserGroups.length == 0 ? [0] : this.checkedUserGroups) }


            $.post(this.userDepartmentUpdateApi, params , function( response ) {

                seft.msgType = response.status;
                if( ! response.status)
                {
                    seft.notify(response.msg, "error");
                    return false;
                }

                seft.notify(response.msg, "success");
                seft.selected_user_group.term_count = seft.checkedUserGroups.length;
                seft.selected_user_group.user_group = seft.checkedUserGroups;
                seft.$emit('user_department_updated',seft.selected_user_group);

            }, "json");
        }
    },
    template: `
    <div class="modal fade" :id="guid">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" v-text="modalHeading"></h4>
                </div>
                <div class="modal-body">
                    <p v-text="checkNameUsers"></p>
                    <table :id="userGroupsTableId" class="table no-margin">
                        <thead><tr><th v-for="item in headings" v-text="item"></th></tr></thead>
                        <tbody>
                            <tr v-for="user_group in user_groups">
                                <td><input type="checkbox" :value="user_group.term_id" v-model="checkedUserGroups"></td>
                                <td v-text="user_group.term_name"></td>
                                <td v-text="user_group.term_mail"></td>
                                <td class="text-right" v-text="user_group.term_count"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button v-on:click="submit" type="button" class="btn btn-primary">Lưu thay đổi</button>
                </div>
            </div>
        </div>
    </div>
    `
});

Vue.component('departments-crud-component',{

    props: ['selected_user_group','guid'],
    data: function(){

        return {
            postData: {
                edit: {
                    term_id: 0,
                    term_description: ''
                }
            },
            messages: null,
            msgType: null,
            departments: [],
        }
    },
    watch: {
        selected_user_group: function(){

            if(this.selected_user_group == null)
            {
                this.postData.edit = {term_id: 0,term_name:"",term_description:"",term_parent:0,term_email:""};
                return;
            }

            this.postData.edit = {
                term_id: this.selected_user_group.term_id,
                term_name: this.selected_user_group.term_name,
                term_description: this.selected_user_group.term_description,
                term_parent: this.selected_user_group.term_parent,
                term_email : this.selected_user_group.term_email,
            };

        }
    },
    computed: {
        userDepartmentsUpdateApi: function(){

            return admin_url + 'staffs/ajax/departments/update/' + this.postData.edit.term_id;
        },
        modalHeading: function(){

            if(this.selected_user_group == null) return "Thêm mới phòng ban làm việc";
            return "Cập nhật phòng ban làm việc"
        },
    },
    created: function() {
        this.loadDepartments();
    },
    methods: {

        loadDepartments: function(){
            var seft = this;
            axios
            .get(admin_url + 'staffs/ajax/users/list_departments')
            .then(function (response) {
                var _data = response.data.data;

                var departments = [];
                for (var i = 0; i < _data.length; i++)
                {
                    departments.push({'key':_data[i].term_id,'text':_data[i].term_name});
                }

                seft.departments = departments;
            });
        },

        notify: function(msg,className)
        {
            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },
        submit: function(){

            var seft = this;

            $.post(this.userDepartmentsUpdateApi, this.postData, function( response ) {

                seft.msgType = response.status;
                if( ! response.status)
                {
                    seft.notify(response.msg, 'error');
                    return false;
                }

                /**
                 * Thông báo có sự thay đổi dữ liệu phía component child
                 * Nếu Thêm mới : thông báo đã thêm mới , ngược lại thì thông báo đã cập nhật
                 */
                if(seft.selected_user_group == null) seft.$emit('user_department_inserted',response.data);
                else seft.$emit('user_department_updated',response.data);

               

                seft.selected_user_group = response.data;
                seft.notify(response.msg, 'success');

            }, "json");
        }
    },
    template: `
    <div class="modal fade" :id="guid">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" v-text="modalHeading"></h4>
                </div>
                <div class="modal-body">

                    <form id="updateDepartmentFrm" method="POST" class="form-horizontal">

                        <div class="form-group">
                            <label for="term_name" class="col-sm-2 control-label">Tên phòng</label>
                            <div class="form-department col-md-10">
                                <div class="input-department col-sm-10">
                                    <input type="text" v-model="postData.edit.term_name" class="form-control" name="meta[term_name]" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="term_name" class="col-sm-2 control-label">Mô tả</label>
                            <div class="form-department col-md-10">
                                <div class="input-department col-sm-10">
                                    <textarea v-model="postData.edit.term_description" class="form-control" name="meta[term_description]" class="form-control" rows="3" placeholder="Mô tả ..."></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="term_email" class="col-sm-2 control-label">Email</label>
                            <div class="form-department col-md-10">
                                <div class="input-department col-sm-10">
                                    <input type="text" v-model="postData.edit.term_email" class="form-control" name="meta[term_email]" />
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button v-on:click="submit" type="button" class="btn btn-primary">Lưu thay đổi</button>
                </div>
            </div>
        </div>
    </div>
    `
});

Vue.component('departments-datatable-component', {
    data: function(){
        return {
            userDepartments: [],
            headings: ["Phòng","Mô tả","Số lượng phòng ban trong phòng ban",""],
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),             
            userDepartmentsApi: admin_url + 'staffs/ajax/departments/list',
            selectedUserGroup: null,
            department_id:null
        };
    },
    computed: {
        modalId: function() {
            return "modal-" + this.guid;
        },
        modalMemberManagerId: function(){
            return "modal-member-" + this.guid;
        },
        modalDepartmentRemoveId: function(){
            return "modal-removal-" + this.guid;
        }
    },
    watch:{
        userDepartments:function(){
            
        }
    },
    created: function(){
        this.loadUserDepartments();
    },
    beforeUpdate: function(){
        this.destroyDatatable();
    },
    updated: function(){
        this.buildDatatable();
    },
    methods: {

        loadUserDepartments: function(){
            var seft = this;
            axios
            .get(this.userDepartmentsApi)
            .then(function (response) {
                var _data = response.data.data;
                seft.userDepartments = _data;
            });
        },

        currencyFormat: function(amount){

            return amount.toLocaleString('en') + 'đ';
        },

        destroyDatatable: function(){
            if ($.fn.DataTable.isDataTable("#"+this.guid))
            {
                $("#"+this.guid).DataTable().destroy();
            }
        },

        buildDatatable : function(){

            $("#" + this.guid).DataTable({
                columns: [
                    { data : "term_name" },
                    { data : "term_description" },
                    { data : "term_count"},
                    {},
                ],
                "language": {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Hiển thị _MENU_ kết quả mỗi trang",
                    "sZeroRecords":  "Không tìm thấy kết quả nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 kết quả",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm kiếm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Trang đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Trang cuối"
                    }
                }
            });
        },

        userDepartmentUpdated: function(data) {

            if(this.userDepartments.length == 0) return;
            
            var userDepartments = this.userDepartments;
            var i = 0;
            for (i; i < userDepartments.length; i++)
            {
                if(userDepartments[i].term_id != data.term_id) continue;
                userDepartments[i] = data;
                break;
            }

            this.userDepartments = userDepartments;
            this.selectedUserGroup = data;
        },


        /* Remove the item out of user-departments */
        userDepartmentRemoved: function(data)
        {
            if(this.userDepartments.length == 0) return;
            this.selectedUserGroup = null;
            
            var userDepartments = this.userDepartments;
            var i = 0;
            for (i; i < userDepartments.length; i++)
            {
                if(userDepartments[i].term_id != data.term_id) continue;
                    
                userDepartments.splice(i,1);
                break;
            }

            this.userDepartments = userDepartments;
        },

        userDepartmentInserted: function(data) {
            
            this.selectedUserGroup = data;
            this.userDepartments.push(data);
        },

        /* Click Event Listening create new user-Department*/
        initCreateUserDepartment: function(){

            this.selectedUserGroup = null;
        },

        /* Click Event Listening updated user-Department click*/
        selectedUserGroupChanges: function(data){
            this.selectedUserGroup = data;
            this.department_id = data.term_id;
        },
    },
    template: `
    <div class="box">
        <div class="box-header with-border">
            <button class="btn btn-primary btn-flat pull-right" v-on:click="initCreateUserDepartment" data-toggle="modal" :data-target="'#'+modalId"><i class="fa fa-plus"></i> Phòng ban</button>
        </div>

        <div class="box-body">
            <table :id="guid" class="table no-margin">
                <thead><tr><th v-for="item in headings" v-text="item"></th></tr></thead>
                <tbody>
                    <template v-if="userDepartments.length > 0">
                        <tr v-for="item in userDepartments">
                            <td v-text="item.term_name"></td>
                            <td v-text="item.term_description"></td>
                            <td class="text-right" v-text="item.term_count"></td>
                            <td class="text-right">
                                <button data-toggle="modal" :data-target="'#'+modalId" v-on:click="selectedUserGroupChanges(item)"  class="btn btn-default btn-xs"><i class="fa fa-fw fa-edit"></i></button>
                                <button data-toggle="modal" :data-target="'#'+modalMemberManagerId" v-on:click="selectedUserGroupChanges(item)" :department_id = "item.term_id" class="btn btn-default btn-xs"><i class="fa fa-fw fa-users"></i></button>
                                <button data-toggle="modal" :data-target="'#'+modalDepartmentRemoveId" v-on:click="selectedUserGroupChanges(item)" class="btn btn-default btn-xs"><i class="fa fa-fw fa-trash"></i></button>
                            </td>
                        </tr>
                    </template>
                </tbody>
            </table>
        </div>

        <departments-crud-component v-on:user_department_updated="userDepartmentUpdated" v-on:user_department_inserted="userDepartmentInserted" v-bind:guid="modalId" v-bind:selected_user_group="selectedUserGroup"/>
        <management-members-component v-on:user_department_updated="userDepartmentUpdated" v-bind:guid="modalMemberManagerId" v-bind:selected_user_group="selectedUserGroup" v-bind:department_id ="department_id"/>
        <remove-modal-component v-on:user_department_removed="userDepartmentRemoved" v-bind:guid="modalDepartmentRemoveId" v-bind:selected_user_group="selectedUserGroup"/>

    </div>
    `
});