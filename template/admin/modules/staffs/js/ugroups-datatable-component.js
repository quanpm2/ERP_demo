Vue.component('ugroups-datatable-component', {
    data: function(){
        return {
            userGroups: [],
            headings: ["Nhóm","Mô tả","Số lượng thành viên trong nhóm",""],
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),             
            userGroupsApi: admin_url + 'staffs/ajax/groups/list',
            selectedUserGroup: null,
        };
    },
    computed: {
        modalId: function() {
            return "modal-" + this.guid;
        },
        modalMemberManagerId: function(){
            return "modal-member-" + this.guid;
        },
        modalGroupRemoveId: function(){
            return "modal-removal-" + this.guid;
        }
    },
    created: function(){
        this.loadUserGroups();
    },
    beforeUpdate: function(){
        this.destroyDatatable();
    },
    updated: function(){
        this.buildDatatable();
    },
    methods: {

        loadUserGroups: function(){
            var seft = this;
            axios
            .get(this.userGroupsApi)
            .then(function (response) {
                var _data = response.data.data;
                seft.userGroups = _data;
            });
        },

        currencyFormat: function(amount){

            return amount.toLocaleString('en') + 'đ';
        },

        destroyDatatable: function(){
            if ($.fn.DataTable.isDataTable("#"+this.guid))
            {
                $("#"+this.guid).DataTable().destroy();
            }
        },

        buildDatatable : function(){

            $("#" + this.guid).DataTable({
                columns: [
                    { data : "term_name" },
                    { data : "term_description" },
                    { data : "term_count"},
                    {},
                ],
                "language": {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Hiển thị _MENU_ kết quả mỗi trang",
                    "sZeroRecords":  "Không tìm thấy kết quả nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 kết quả",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm kiếm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Trang đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Trang cuối"
                    }
                }
            });
        },

        userGroupUpdated: function(data) {

            if(this.userGroups.length == 0) return;
            
            var userGroups = this.userGroups;
            var i = 0;
            for (i; i < userGroups.length; i++)
            {
                if(userGroups[i].term_id != data.term_id) continue;
                userGroups[i] = data;
                break;
            }

            this.userGroups = userGroups;
            this.selectedUserGroup = data;
        },


        /* Remove the item out of user-groups */
        userGroupRemoved: function(data)
        {
            if(this.userGroups.length == 0) return;
            this.selectedUserGroup = null;
            
            var userGroups = this.userGroups;
            var i = 0;
            for (i; i < userGroups.length; i++)
            {
                if(userGroups[i].term_id != data.term_id) continue;
                    
                userGroups.splice(i,1);
                break;
            }

            this.userGroups = userGroups;
        },

        userGroupInserted: function(data) {
            
            this.selectedUserGroup = data;
            this.userGroups.push(data);
        },

        /* Click Event Listening create new user-group*/
        initCreateUserGroup: function(){

            this.selectedUserGroup = null;
        },

        /* Click Event Listening updated user-group click*/
        selectedUserGroupChanges: function(data){
            this.selectedUserGroup = data;
        }
    },
    template: `
    <div class="box">
        <div class="box-header with-border">
            <button class="btn btn-primary btn-flat pull-right" v-on:click="initCreateUserGroup" data-toggle="modal" :data-target="'#'+modalId"><i class="fa fa-plus"></i> Nhóm làm việc</button>
        </div>

        <div class="box-body">
            <table :id="guid" class="table no-margin">
                <thead><tr><th v-for="item in headings" v-text="item"></th></tr></thead>
                <tbody>
                    <template v-if="userGroups.length > 0">
                        <tr v-for="item in userGroups">
                            <td v-text="item.term_name"></td>
                            <td v-text="item.term_description"></td>
                            <td class="text-right" v-text="item.user_ids.length"></td>
                            <td class="text-right">
                                <button data-toggle="modal" :data-target="'#'+modalId" v-on:click="selectedUserGroupChanges(item)" class="btn btn-default btn-xs"><i class="fa fa-fw fa-edit"></i></button>
                                <button data-toggle="modal" :data-target="'#'+modalMemberManagerId" v-on:click="selectedUserGroupChanges(item)" class="btn btn-default btn-xs"><i class="fa fa-fw fa-users"></i></button>
                                <button data-toggle="modal" :data-target="'#'+modalGroupRemoveId" v-on:click="selectedUserGroupChanges(item)" class="btn btn-default btn-xs"><i class="fa fa-fw fa-trash"></i></button>
                            </td>
                        </tr>
                    </template>
                </tbody>
            </table>
        </div>

        <ugroups-crud-component v-on:user_group_updated="userGroupUpdated" v-on:user_group_inserted="userGroupInserted" v-bind:guid="modalId" v-bind:selected_user_group="selectedUserGroup"/>
        <management-members-component v-bind:user_groups="userGroups" v-on:user_group_updated="userGroupUpdated" v-bind:guid="modalMemberManagerId" v-bind:selected_user_group="selectedUserGroup"/>
        <remove-modal-component v-on:user_group_removed="userGroupRemoved" v-bind:guid="modalGroupRemoveId" v-bind:selected_user_group="selectedUserGroup"/>

    </div>
    `
});