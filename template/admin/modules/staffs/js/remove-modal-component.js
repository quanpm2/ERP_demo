Vue.component('remove-modal-component', {
    props: ['selected_user_group','guid'],
    computed: {
        userGroupRemovedApi: function(){ return admin_url + 'staffs/ajax/groups/delete/' + this.selected_user_group.term_id; },
        modalHeading : function() { return "Thông báo xác nhận"; }
    },
    methods: {

        /* Functional output Notify message on right screen */
        notify: function(msg,className){

            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },

        /* Submit handler update selected User mapping to User Group */
        submit: function(){

            if(this.selected_user_group == null)
            {
                this.notify('Thông tin không hợp lệ ! Vui lòng thử lại sau .', "warn");
                return false;
            }

            if(this.selected_user_group.user_ids.length > 0)
            {
                this.notify('Yêu cầu xử lý không hợp lệ ! Vui lòng thử lại sau .', "warn");
                return false;
            }

            var seft = this;

            $.post(this.userGroupRemovedApi, {}, function( response ) {

                seft.msgType = response.status;
                if( ! response.status)
                {
                    seft.notify(response.msg, "error");
                    return false;
                }

                seft.notify(response.msg, "success");
                seft.$emit('user_group_removed',seft.selected_user_group);

            }, "json");
        }
    },
    template: `
    <div class="modal fade" :id="guid" v-if="selected_user_group != null">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" v-text="modalHeading"></h4>
                </div>
                <div class="modal-body">
                    <p v-if="selected_user_group.user_ids.length > 0">
                        Nhóm đang chứa <span v-text="selected_user_group.user_ids.length"></span> thành viên nên không thể xóa ! <br/>
                        Nếu muốn tiếp tục thực hiện ! xóa các thành viên ra khỏi nhóm và thực hiện tại
                    </p>
                    <p v-else>Bạn có chắc muốn xóa nhóm ! Sau khi thực hiện sẽ không thể hoàn tác !!!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button v-if="selected_user_group.user_ids.length == 0" v-on:click="submit" type="button" class="btn btn-primary" data-dismiss="modal">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
    `
});