Vue.component('oneweb-service-price-component', {
    props: {
        id: { type: Number, default : 0 },
        guid : { type : String, default : _.uniqueId('oneweb-service-price-component-') },
        read_only : { type : Boolean, default : false },
        update_mode : { type : Boolean, default : true }
    },
    data: function(){
        return {
            config : {},
            service_packages : [],
            discount_plans : [],

            service_package : null,
            service_package_price : 0,
            years: null,
            
            advanced_functions : [],
            discount_plan : null,
            discount_amount : 0,
            discount : { min : 0, max : 0 },

            formValidate : null,
            isLoading : false,
            readOnly : false
        }
    },
    computed : {

        packagePrice () { return this.service_package_price * this.years},

        /* Giá trị hợp đồng chưa áp dụng giảm giá */
        _price () {
            let value = this.packagePrice || 0;
            let advancedFunctionsValue = _.isEmpty(this.advanced_functions) ? 0 : _.sum(_.map(this.advanced_functions, o => { return _.toInteger(o.value) }))
            return value + advancedFunctionsValue
        },

        /* Giá trị hợp đồng sau khi đã giảm giá */
        contractValue () { return this._price - this.discount_amount },

        /**
         * Get Service Package label
         *
         * @return     {string}  Service package Label
         */
        service_package_label () {

            if(_.isEmpty(this.config) || !_.has(this.config, `services.${this.service_package}.label`)) return 'Chưa chọn gói.'
            return _.get(this.config, `services.${this.service_package}.label`)
        },
        /* Discount Combo plan Amount */
        comboDiscountAmount () {

            if(!_.isEqual(this.discount_plan, "combo")) return 0;

            let saleOpt =  _.findLast(_.get(this.config, 'discount.packages.combo.sale'), o => { return o.buy < this.years })
            return _.isEmpty(saleOpt) ? 0 : _.multiply(saleOpt.free, this.service_package_price)
        },


    },
    watch: {

        read_only () { this.readOnly = this.read_only },

        /* Watch when config data changed */
        config () {

            if(_.isEmpty(this.config)) return
            this.service_packages = _.map(this.config.services, o => { return {key: o.name, value: o.label} })
            this.discount_plans = _.map(this.config.discount.packages, o => { return {key: o.name, value: o.label} })
            
            if(!_.gt(this.service_package_price, 0) 
                && _.gt(_.get(this.config, 'services.normal.price'), 0)) this.service_package_price = _.get(this.config, 'services.normal.price')

            this.calcDiscountData();
        },

        service_package () {

            if(_.isEmpty(this.service_package) || !_.has(this.config, `services.${this.service_package}.price`)) return
            this.service_package_price = _.get(this.config, `services.${this.service_package}.price`, 0);
        },
        // contractValue () { this.calcDiscountData() },
        discount_plan () { this.calcDiscountData() },

        /* Discount Combo plan Amount */
        comboDiscountAmount () { this.discount_amount = this.comboDiscountAmount }
    },
    created : function(){
        this.loadConfig();
        this.loadContractData();
    },
    mounted : function(){
        this.bindFormValidate();
        this.calcDiscountData();
    },
    methods : {

        loadContractData : function(){
            
            var seft = this;
            let obj = {
                id : this.id,
                meta : ['service_package', 'service_package_price', 'advanced_functions', 'discount_plan', 'discount_amount', 'price', 'years']
            };

            axios.get(admin_url + 'oneweb/api/contract/index?' + $.param(obj))
            .then( response => {

                let result                  = response.data.data
                this.price                  = _.toInteger(_.get(result, "price") || 0)
                this.years                  = _.toInteger(_.get(result, "years") || 1)
                this.service_package        = _.get(result, "service_package") || "normal"

                if(_.gt(_.toInteger(_.get(result, "service_package_price", 0)), 0))
                {
                    this.service_package_price  = _.toInteger(_.get(result, "service_package_price", 0));
                }

                this.advanced_functions     = _.get(result, "advanced_functions") || [ {name: '', value: 0} ]
                this.discount_plan          = _.get(result, "discount_plan") || "combo"
                this.discount_amount        = _.toInteger(_.get(result, "discount_amount") || 0)
                this.lock_editable          = new Boolean(_.toInteger(_.get(result, "lock_editable") || 0))
                this.readOnly               = new Boolean(_.toInteger(_.get(result, "lock_editable") || 0))
                return response
            })
            .catch(function (error) {})
        },

        boxModeChanged : function(){
            this.update_mode = !this.update_mode;
        },
        resetData : function(){
            this.loadContractData();
            this.boxModeChanged();
            this.formValidate.resetForm();
        },
        service_package_changed : function(value){
            this.service_package = value;
        },

        loadConfig : function(){

            axios.get(`${admin_url}oneweb/api/contract/config/id/${this.id}`)
            .then(response => { 
                this.config = response.data.data; 
                return response 
            })
            .catch(function (error) {})
        },

        bindFormValidate : function(){

            var _self = this;
            this.formValidate = $("#form-" + this.guid).validate({
                rules : {
                   'meta[discount_amount]': {
                        digits: true,
                        min : function(){ return _self.discount.min },
                        max : function(){ return _self.discount.max },
                    },
                },
                messages : {
                    'meta[discount_amount]' : {
                        digits: 'Số tiền giảm giá không hợp lệ.',
                        min : 'Số tiền giảm giá không được là kiểu số âm.',
                        max : function() { return 'Vượt mức giảm giá tối đa : ' + _self.discount.max.toLocaleString() + ' đ'},  
                    },
                },
            });
        },
        
        /**
         * Calcualte Discount Data
         */
        calcDiscountData () {

            let total = this.contractValue;

            switch(this.discount_plan)
            {
                case 'combo':
                    return
                    break;

                case 'normal':
                    total-= 10000000;
                    total = total > 0 ? total : 0;
                    break;

                case 'cross-sell':
                    total = 0;
                    break;
            }

            let maxPercentage   = ( !_.isEmpty(this.config.discount) && !_.isEmpty(this.config.discount.packages) && !_.isEmpty(this.config.discount.packages[this.discount_plan])) ? this.config.discount.packages[this.discount_plan].sale.max : 0
            this.discount = {min: 0, max: _.multiply(total, _.divide(maxPercentage, 100))};
        },

        /* Add New Advanced Features row */
        addAdvancedFeature () { this.advanced_functions.push( {name: "", value: ""}) },

        /* Removes specified by Index of Advanced Features row */
        removeAdvancedFeature (index) { this.advanced_functions.splice(index, 1) },

        formSubmit : function(){

            var seft = this;
            seft.isLoading = true;
            $.post(admin_url + 'oneweb/api/contract/service/id/' + this.id + '/meta/' + 'discount_plan', $("#form-"+this.guid).serializeArray())
            .done(function(response) {
                
                let messages = response.msg;
                for(let i in messages)
                {
                    $.notify(messages[i], 'success');
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                $.notify(JSON.parse(jqXHR.responseText));
            })
            .always(function(){
                seft.isLoading = false;
            });
        },

        currencyFormat : function(value){
            let val = parseInt(value);
            if( Number.isNaN(val)) return '0 đ';
            return val.toLocaleString() + ' đ';
        }
    },
    template: `
    <div class="box box-primary" :id="'container-'+ guid">
        <div class="box-header with-border">
            <h3 class="box-title"><i>Thông tin dịch vụ (Gói & giá trị dịch vụ)</i></h3>
        </div>
        <div class="box-body">
            <form :id="'form-'+guid" method="post" accept-charset="utf-8">

                <input style="text-align:right" type="hidden" name="id" :value="id"/>
                
                <table border="1" cellpadding="2" cellspacing="1" class="table table-responsive table-bordered table-hover" style="table-layout: fixed;">
                    <thead>
                        <tr>
                            <th>Đầu mục/ dịch vụ / chức năng</th>
                            <th align="right" width="10%">Số năm</th>
                            <th align="right" width="30%">Giá trị</th>
                            <th width="10%"></th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>
                                <b><u><i>Gói {{ service_package_label }} ( {{ service_package_price.toLocaleString('de-DE') }} )</i></u></b>
                                <input style="text-align:right" type="hidden" name="meta[service_package_price]" v-model="service_package_price"/>
                                <input type="hidden" name="meta[service_package]" v-model="service_package"/>
                            </td>
                            <td v-if="!update_mode">
                                <select name="meta[years]" v-model="years" class="form-control">
                                    <option v-for="n in 10" :value="n">{{ n }} năm</option>
                                </select>
                            </td>
                            <td v-else>
                                <strong>{{ years }} năm</strong>
                            </td>

                            <td align="right">
                                <strong>{{ packagePrice.toLocaleString('de-DE') }} đ</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table border="1" cellpadding="2" cellspacing="1" class="table table-responsive table-bordered table-hover" style="table-layout: fixed;">
                    <thead>
                        <tr>
                            <th>Chức năng nâng cao</th>
                            <th align="right" width="10%">Số ngày làm việc</th>
                            <th align="right" width="30%">Giá trị</th>
                            <th width="10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item,index) in advanced_functions">
                            <template v-if="!update_mode">
                                <td>
                                    <input v-model="item.name" type="text" class="form-control valid" :name="'meta[advanced_functions]['+index+'][name]'" placeholder="Tính năng 1"/>
                                </td>
                                <td>
                                    <select :name="'meta[advanced_functions]['+index+'][days]'" v-model="item.days" class="form-control">
                                        <option v-for="n in 15" :value="n">{{ n }} ngày</option>
                                    </select>
                                </td>
                                <td align="right"><input style="text-align:right" v-model="item.value" type="number" class="form-control valid" :name="'meta[advanced_functions]['+index+'][value]'" placeholder="Báo giá tính năng 1"/></td>
                                <td>
                                    <button v-if="index == (advanced_functions.length-1)" v-on:click='addAdvancedFeature' type="button" class="btn btn-flat btn-sm btn-primary">
                                        <i class="fa fa-fw fa-plus"></i> 
                                    </button>
                                    <button v-if="index != 0" v-on:click='removeAdvancedFeature(index)' type="button" class="btn btn-flat btn-sm btn-danger">
                                        <i class="fa fa-fw fa-trash"></i> 
                                    </button>
                                </td>
                            </template>
                            <template v-else-if="item.name != ''">
                                <td><u>Chức năng : </u> {{ item.name }} </td>
                                <td align="right" v-text="item.days"></td>
                                <td align="right" v-text="currencyFormat(item.value)"></td>
                                <td></td>
                            </template>
                        </tr>
                        <tr>
                            <td colspan="2"><b><u><i>Tổng giá trị thiết kế</i></u></b></td>
                            <td align="right"><b v-text="currencyFormat(_price)"></b></td>
                        </tr>
                        <tr>
                            <td>
                                <b><u><i>Chương trình giảm giá</i></u></b>
                                <div class="form-group">
                                    <div class="radio" v-for="item in discount_plans">
                                        <label>
                                            <input style="text-align:right" v-model="discount_plan" :disabled="update_mode" type="radio" name="meta[discount_plan]" :value="item.key" > {{ item.value }}
                                        </label>
                                    </div>
                                </div>
                            </td>
                            <template v-if="discount_plan == 'combo'">
                                <td></td>
                                <td align="right">
                                    <strong>{{ comboDiscountAmount.toLocaleString('de-DE') }} đ</strong>
                                </td>
                            </template>
                            <template v-else>
                                <td></td>
                                <td align="right">
                                    <label v-if="update_mode" v-text="'-' + currencyFormat(discount_amount)"></label>
                                    <input style="text-align:right" v-else name="meta[discount_amount]" type="number" step="50000" :min="discount.min" :max="discount.max" v-model="discount_amount" class="form-control">
                                </td>
                            </template>


                            <td></td>
                        </tr>
                        <tr>
                            <td><b><u><i>Tổng</i></u></b></td>
                            <td></td>
                            <td align="right"><b v-text="currencyFormat(contractValue)"></b></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    `
});