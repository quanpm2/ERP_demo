Vue.component('oneweb-tasks-calendar-create-update',{
     props: {
        guid : { type : String, default : () => { return _.uniqueId(['_wbtccu_']); }},
        contract_id : { type: Number, default : 0 },
    },
    data : function(){
        return {
            data : null,
            admin_url : admin_url,
        }
    },
    computed:{

    },
    mounted:function(){
        this.firtLoad();
        this.initValidate();
    },
    methods:{
        firtLoad: function() {
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });
            //Datemask dd-mm-yyyy
            $('#fromdate,#todate').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
            
            this.set_color();
            $('input[name="edit[meta][post_status]"]').on('ifClicked', function (event) {
                if(this.value=="on"){
                    return $('#color-chooser > li > a > .color-orange').trigger("click");

               }
               return $('#color-chooser > li > a > .color-green').trigger("click");
            });
        },
        initValidate : function(){

            $('#add-new-frm').validate({
                rules: {
                    'edit[meta][start_date]': {
                        required: true,
                    },
                    'edit[meta][end_date]': {
                        required: true,
                    },
                    'edit[meta][post_title]': {
                        required: true,
                    }
                },
                messages:{
                    'edit[meta][start_date]': {
                        required: "Trường này là bắt buộc.",
                    },
                    'edit[meta][end_date]': {
                        required: "Trường này là bắt buộc.",
                    },
                    'edit[meta][post_title]': {
                        required: "Trường này là bắt buộc.",
                    }
                }
            });
        },
        // set color for button Add
        set_color: function(){
            /* ADDING EVENTS */
            var currColor = '#f39c12' //Red by default
            //Color chooser button
            var colorChooser = $('#color-chooser-btn');
            $('#color-chooser > li > a').click(function (e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css('color')
            //Add color effect to button
            $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
            })


        },
        /* Notify MSG out to screen */
        notify: function(msg,className){

            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },

        submit: function(){
            var calendar = this
            var isValid = $('#add-new-frm').valid();
            if( ! isValid) return false;
            //validate fromdate and todate for dd-mm-yyyy
            if($('#fromdate').val().indexOf('d')!=-1){
                $('#fromdate').addClass('error');
                $('#fromdate-error').css("display","inline-block");
                $('#fromdate-error').text('Ngày tháng năm không chính xác.');
                $('#fromdate-error').show();
                 return false;
            }
            if($('#fromdate').val().indexOf('m')!=-1){
                $('#fromdate').addClass('error');
                $('#fromdate-error').css("display","inline-block");
                $('#fromdate-error').text('Ngày tháng năm không chính xác.');
                $('#fromdate-error').show();
                 return false;
            }
            if($('#fromdate').val().indexOf('y')!=-1) {
                $('#fromdate').addClass('error');
                $('#fromdate-error').css("display","inline-block");
                $('#fromdate-error').text('Ngày tháng năm không chính xác.');
                $('#fromdate-error').show();
                 return false;
            }
            if($('#todate').val().indexOf('d')!=-1) {
                $('#todate').addClass('error');
                $('#enddate-error').text('Ngày tháng năm không chính xác.');
                $('#enddate-error').css("display","inline-block");
                $('#enddate-error').show();
                 return false;
            }
            if($('#todate').val().indexOf('m')!=-1){
                $('#todate').addClass('error');
                $('#enddate-error').text('Ngày tháng năm không chính xác.');
                $('#enddate-error').css("display","inline-block");
                $('#enddate-error').show();
                 return false;
            }
            if($('#todate').val().indexOf('y')!=-1){
                $('#todate').addClass('error');
                $('#enddate-error').text('Ngày tháng năm không chính xác.');
                $('#enddate-error').css("display","inline-block");
                $('#enddate-error').show();
                 return false;
            }


            var calendar = this;
            this.isLoading = true;
            
            let updateApiUrl = admin_url + 'oneweb/api/contract/task/contract_id/' + this.contract_id + '/id/' + $("#taskid").val();
            $.ajax({
                type: 'PUT', // Use POST with X-HTTP-Method-Override or a straight PUT if appropriate.
                dataType: 'json', // Set datatype - affects Accept header
                url: updateApiUrl, // A valid URL
                headers: {"X-HTTP-Method-Override": "PUT"}, // X-HTTP-Method-Override set to PUT.
                data: $('#add-new-frm').serializeArray() // Some data e.g. Valid JSON as a string
            })
            .done(function(response) {
                let messages = _.isString(response) ? [response] : response;
                _.forEach(messages, function(msg) { calendar.notify(msg, 'success') });

                //refetch Calendar
                $('#calendar').fullCalendar('removeEvents');
                $('#calendar').fullCalendar('refetchEvents');
                //renew form
                calendar.add_new();
            })
            .fail(function(jqXHR, textStatus, errorThrown) { calendar.notify(JSON.parse(jqXHR.responseText)); setTargetAction('', true);})

        },
        add_new:function(){
            $('#process').parent().trigger("click");
            $("#add-new-frm").trigger("reset");
            $('#box_title').text('Thêm mới');

        },
        
    },
    template:`
    <div :id="guid">
        <div class="box box-solid;" style="display:none;">
            <div class="box-header with-border">
            <h4 class="box-title">Draggable Events</h4>
            </div>
            <div class="box-body">

            <div id="external-events">
                <div class="external-event bg-green">Lunch</div>
                <div class="external-event bg-yellow">Go home</div>
                <div class="external-event bg-aqua">Do homework</div>
                <div class="external-event bg-light-blue">Work on UI design</div>
                <div class="external-event bg-red">Sleep tight</div>
                <div class="checkbox">
                <label for="drop-remove">
                    <input type="checkbox" id="drop-remove">
                    remove after drop
                </label>
                </div>
            </div>
            </div>
        </div>
            <div class="box box-solid">
                <div class="box-header with-border">
                <h3 id="box_title" class="box-title">Thêm mới</h3>
                </div>
                <div class="box-body">
                <div class="btn-group" style="width: 100%; margin-bottom: 10px;display:none">
                    <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                    <ul class="fc-color-picker" id="color-chooser">
                    <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-yellow" href="#"><i class="fa fa-square color-yellow"></i></a></li>
                    <li><a class="text-orange" href="#"><i class="fa fa-square color-orange"></i></a></li>
                    <li><a class="text-green" href="#"><i class="fa fa-square color-green"></i></a></li>
                    <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                    </ul>
                </div>
                <form method="post" id="add-new-frm" accept-charset="utf-8">
                    <div class="form-group" style="display:none;">
                        <div class="input-group" style="width: 100%;">
                            <input id="taskid" name="edit[post_id]" type="text" class="form-control" placeholder="Id">
                            <input id="check_complete" name="edit[meta][check_complete]" type="text" value="process" class="form-control" placeholder="check_complete">
                        </div>
                    </div>
                    <div class="form-group">
                        <strong>Tiêu đề</strong>
                        <div class="input-group" style="width: 100%;">
                                <input id="new-event" type="text" name ="edit[post_title]" class="form-control" placeholder="Tiêu đề">
                        </div>
                    </div>
                    <div class="form-group">
                        <strong>Ngày bắt đầu</strong>
                        <div class="input-group" style="margin-bottom: 2%;">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                            <input type="text" id="fromdate" name="edit[start_date]" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
                            <label id="fromdate-error" class="error" for="fromdate" style="display: none;"></label>
                        </div>

                        <strong>Ngày kết thúc</strong>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" id ="todate" name="edit[end_date]" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
                            <label id="enddate-error" class="error" for="todate" style="display: none;"></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group" style="float: left; margin-right: 5%;">
                            <label>
                                <input type="checkbox" id="sms_status" name="meta[sms_status]" value="1" class="minimal" checked> SMS
                            </label>
                        </div>
                        <div class="form-group" id="radio-checked" style="margin-top: 3%;">
                            <label class="" style="margin-right: 5%;color: #f39c12">
                            <input type="radio" id="process" name="edit[post_status]" class="minimal" checked="" value="process" style="position: absolute; opacity: 0;">
                            Đang thực hiện</label>
                            <label class="" style="color:#00a65a">
                            <input type="radio" id="complete" name="edit[post_status]" class="minimal" value="complete" style="position: absolute; opacity: 0;">
                            Hoàn thành</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group" style="width: 100%;">
                            <textarea id="new-content" name ="edit[meta][post_content]" type="text" style="resize: none;" class="form-control" placeholder="Nội dung" rows="5"></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group" style="float: right;">
                            <button id="add-new-event" v-on:click="submit" type="button" style="background-color: #f39c12; border-color: #f39c12;" class="btn btn-primary btn-flat">Lưu</button>

                            <button id="refresh" v-on:click="add_new" type="button"  style="background-color: #696969; border-color: #696969;" class="btn btn-primary btn-flat">Làm mới</button>
                    </div>
                </form>

            </div>

            </div>
        </div>
    </div>

    `
});