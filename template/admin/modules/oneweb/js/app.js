Vue.component('oneweb-index-component',{
    props: ['options'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            selectedTermId : 0,
            publish_remote_url : admin_url + 'oneweb/ajax/dataset?&where%5Bterm_status%5D=publish&order_by%5Bstart_service_time%5D=desc',
            ending_remote_url : admin_url + 'oneweb/ajax/dataset?&where%5Bterm_status%5D=ending&order_by%5Bstart_service_time%5D=desc',
            term_id:0
          }
    },
    computed : {
        current_page_text: function(){
            return this.options.current_page + '/' + this.options.num_page;
        }
    },
    methods : {
        dataChanged: function(value)
        {
            var seft = this;
            $('div#container-' + this.guid + ' span.messages-list a.btn-note').off('click').on('click',function(e){
                seft.selectedTermId = $(this).data('term_id')
            });
            $('.payment-alert').click(function(){
                seft.term_id = $(this).attr('term_id');
                $('#paymentAlert').modal('show');
            })
        },
        paymentAlertOk:function(){
            var seft = this;
            $.post(admin_url + 'oneweb/ajax/send_mail_payment_alert/' + seft.term_id,
            function(response){
                if(response.status){
                    return seft.notify(response.msg,"success"); 
                }
                return seft.notify(response.msg,"error");
            })
        },
        notify: function(msg,className){
            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },
    },
    template: `
    <div>
        <div class="row" :id="'container-' + guid">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-list-ul"></i> Tất cả dịch vụ</a></li>
                        <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-stop"></i> Đã kết thúc</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                        <datatable-builder-component is_filtering="false" is_ordering="true" per_page="50" :remote_url="publish_remote_url" v-on:data_changed="dataChanged" ></datatable-builder-component> 

                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                        <datatable-builder-component is_filtering="false" is_ordering="true" per_page="50" :remote_url="ending_remote_url" v-on:data_changed="dataChanged" ></datatable-builder-component> 
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->
            <comments-list-partial-component :term_id="selectedTermId" user_id="<?php echo $this->admin_m->id;?>"></comments-list-partial-component>
        </div>
        <!-- /.row -->
        <!-- Modal -->
            <div id="paymentAlert" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="modal-title">Gửi mail?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="Cancel" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-info" id="OK" :term_id="term_id" v-on:click="paymentAlertOk" data-dismiss="modal">Xác nhận</button>
                    </div>
                    </div>

                </div>
            </div>
        <!-- /Modal -->
    </div>
    `
});

Vue.component('dashboard-component', {
    data: function(){
        return {
            user : null,
            admin_url: admin_url,
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
        };
    },
    computed: {
    },
    created: function(){ },
    methods: {
        getUser : function(){
            var seft = this;

            var getUserInfoUrl = this.admin_url + 'staffs/ajax/users/view/' + this.user_id;

            axios
            .get(getUserInfoUrl)
            .then(function (response) {
                var _data = response.data.data;
                seft.user = _data;
            });
        },
    },
    template: `
    `
});


var app_root = new Vue({
    el: '#app-container'
});