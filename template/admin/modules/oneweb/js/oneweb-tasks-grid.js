Vue.component('oneweb-tasks-grid',{
    props: {
        guid : { type : String, default : () => { return 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);});} },
        contract_id : { type: Number, default : 0 },
    },
    data: function(){
        return {
            moment : moment,
            tasks : [],
            targetTask : {},
            sortableHandler : {},
            targetAction : {
                act : '',
                state : false,
            },
            jQuery : window.$
        }
    },
    computed : { },
    created : function(){
        this.loadTasks();
    },
    mounted: function(){

        var seft = this;

        // Constructing the todo list UI
        jQuery('div#' + this.guid + ' ul.todo-list').sortable({
            placeholder         : 'sort-highlight',
            handle              : '.handle',
            forcePlaceholderSize: true,
            zIndex              : 999999,
            update: function( event, ui ) {
            },
            beforeStop : function ( event, ui){

                jQuery("#"+seft.guid+"-sortableConfirm").modal("show");
                // jQuery(this).sortable( "cancel" );

                seft.sortableHandler.changedArray   = jQuery(this).sortable("toArray");
                seft.sortableHandler.taskDragged    = jQuery(ui.item).attr('id');
                console.log(seft.sortableHandler);
            },
            start : function ( event, ui){
                seft.sortableHandler.originArray = jQuery(this).sortable("toArray");
            },
        });

        
    },
    methods : {
        setTargetAction : function(act, state){
            this.targetAction.act   = act;
            this.targetAction.state = state;
        },
        count_weekend_days(start_time, end_time)
        {
            let weekends    = 0;
            let startDate   = moment().unix(start_time);
            let days        = moment().unix(end_time).diff(startDate, 'days') + 1;

            let start = 0;
            while(start < days)
            {
                start++;

                let tmp = startDate;
                tmp.add(start, 'days');

                if(!_.includes([0, 6], tmp.day())) continue; // saturday = 6, sunday = 0;

                weekends++;
            }

            return weekends;
        },

        updateSortableItems : function(type) {

            if(_.isEmpty(this.sortableHandler))
            {
                $.notify("Quá trình xử lý bị gián đoạn . Vui lòng 'hủy bỏ' hoặc thực hiện 'tác vụ' khác để thử lại", "error");
                return;
            }

            if(!_.includes(["override", "insert"], type))
            {
                $.notify("Lệnh xử lý không hợp lệ. Vui lòng thử lại hoặc liên hệ thonh@webdoctor.vn để biết thông tin chi tiết", "error");
                return;
            }

            // Case override dragged task with same day of drop task
            if("override" == type)
            {

            }

            // Code cập nhật thời gian tác vụ tại đây
        },

        updateTask : function(){

            let form = jQuery("div#" + this.guid + ' form')

            if(_.isEmpty(this.targetTask) || _.isEmpty(form))
            {
                $.notify('Thao tác không hợp lệ [0]', 'error');
                return;
            }

            let updateApiUrl = admin_url + 'oneweb/api/contract/task/contract_id/' + this.contract_id + '/id/' + this.targetTask.post_id;
            var seft = this;

            seft.setTargetAction('update', false);

            $.ajax({
                type: 'PUT', // Use POST with X-HTTP-Method-Override or a straight PUT if appropriate.
                dataType: 'json', // Set datatype - affects Accept header
                url: updateApiUrl, // A valid URL
                headers: {"X-HTTP-Method-Override": "PUT"}, // X-HTTP-Method-Override set to PUT.
                data: form.serializeArray() // Some data e.g. Valid JSON as a string
            })
            .done(function(response) {
                let messages = _.isString(response) ? [response] : response;
                _.forEach(messages, function(msg) { $.notify(msg, 'success') });

                seft.unsetTargetTask(1);
                seft.loadTasks();
                seft.setTargetAction('', true);
            })
            .fail(function(jqXHR, textStatus, errorThrown) { $.notify(JSON.parse(jqXHR.responseText)); setTargetAction('', true);})
        },

        insertTask : function(){

            let form = jQuery("div#" + this.guid + ' form')

            if(_.isEmpty(this.targetTask) || _.isEmpty(form))
            {
                $.notify('Thao tác không hợp lệ [0]', 'error');
                return;
            }

            let createApiUrl = admin_url + 'oneweb/api/contract/task/contract_id/' + this.contract_id;
            var seft = this;
            seft.setTargetAction('update', false);
            
            $.ajax({
                type: 'POST', // Use POST with X-HTTP-Method-Override or a straight PUT if appropriate.
                dataType: 'json', // Set datatype - affects Accept header
                url: createApiUrl, // A valid URL
                headers: {"X-HTTP-Method-Override": "POST"}, // X-HTTP-Method-Override set to PUT.
                data: form.serializeArray() // Some data e.g. Valid JSON as a string
            })
            .done(function(response) {
                let messages = _.isString(response) ? [response] : response;
                _.forEach(messages, function(msg) { $.notify(msg, 'success') });

                seft.unsetTargetTask(1);
                seft.loadTasks();
                seft.setTargetAction('', true);
            })
            .fail(function(jqXHR, textStatus, errorThrown) { $.notify(JSON.parse(jqXHR.responseText));setTargetAction('', true); })
        },

        loadTasks : function(){

            let tasksApiUrl = admin_url + 'oneweb/api/contract/task/contract_id/' + this.contract_id;
            var seft = this;

            axios.get(tasksApiUrl)
            .then(function (response) {
                if(_.isEmpty(response.data.data)) return false;

                seft.tasks = response.data.data;
            })
            .catch(function (error){
                $.notify(error.response.data, 'error');
            });
        },

        addNullTask : function(){

            if(_.findIndex(this.tasks, function(o) { return o.post_id == 0; }) >= 0) return;

            let nullTask = {
                post_id : 0,
                post_title : "",
                post_status : "",
                start_date : moment().startOf('day').format("X"),
                end_date : moment().endOf('day').format("X"),
                post_type : "oneweb-task"
            };

            let _tasks = _.cloneDeep(this.tasks);

            this.tasks = _.concat([nullTask], _tasks);
            this.setTargetTask(_.find(this.tasks, function(o) { return o.post_id == 0; }));
        },

        setTargetTask : function(task){
            
            this.unsetTargetTask();
            this.targetTask = task;

            if(task.post_id != 0) _.remove(this.tasks, function(o){ return o.post_id == 0; });

            return true;
        },
        unsetTargetTask : function(task){
            this.targetTask = {};
        },
        cancelEditModeTargetTask : function(){

            // Insert mode then remove
            if(this.targetTask.post_id == 0)
            {
                let result = this.tasks;
                _.remove(this.tasks, function(o){ return o.post_id == 0; });

                this.tasks = [];
                this.tasks = result;
                return true;
            }

            // Edit mode then disable form
            this.unsetTargetTask();
        },

        deleteTargetTask : function(){

            var _seft = this;
            this.setTargetAction('delete', false);

            axios
            .delete(admin_url + 'oneweb/api/contract/task/contract_id/' + this.contract_id + '/id/' + this.targetTask.post_id)
            .then(function (response){
                $.notify(response.data, 'success');
                jQuery('#'+_seft.guid+'-delConfirm').modal('hide');
                _seft.setTargetAction('', true);

                let tasks = _.cloneDeep(_seft.tasks);
                _.remove(tasks, function(o){ return o.post_id == _seft.targetTask.post_id; });
                _seft.tasks = [];
                _seft.tasks = tasks
            })
            .catch(function (error){
                $.notify(error.response.data, 'error');
                _seft.setTargetAction('delete', true);
            })
            .then(function (){
            });
        }
    },
    template: `
    <div class="box box-primary">
        <div class="box-header ui-sortable-handle">
            <i class="ion ion-clipboard"></i>
            <h3 class="box-title">Danh sách tác vụ</h3>

            <div class="box-tools pull-right">
                <button type="button" v-on:click="addNullTask" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Thêm tác vụ</button>
            </div>
        </div>
        <!-- /.box-header -->

        <!-- Grid items -->
        <div :id="guid" class="box-body">
            <ul v-if="tasks" class="todo-list">
                <li class="container-fluid">

                    <span class="col-xs-3 info-box-text"><b><u><i>Mô tả tác vụ</i></u></b></span>
                    <span class="col-xs-2 info-box-text"><b><u><i>Ngày bắt đầu</i></u></b></span>
                    <span class="col-xs-2 info-box-text"><b><u><i>Ngày kết thúc</i></u></b></span>
                    <span class="col-xs-4 info-box-text"><b><u><i>Trạng thái</i></u></b></span>
                </li>

                <li v-for="task in tasks"  :id="task.post_id" :class="'container-fluid' + (task.post_status == 'complete' ? ' done ' : '')">

                    <template v-if="!_.isEmpty(targetTask) && targetTask.post_id == task.post_id">
                        <form method="POST">
                            <span class="col-xs-3 handle text">
                                <input class="form-control no-border-but-bottom col-xs-10" name="edit[post_title]" :value="task.post_title" type="text" placeholder="Mô tả tác vụ ..."/>
                            </span>

                            <span class="col-xs-2 handle"> 
                                <idatepicker-component name="edit[start_date]" :value="moment.unix(task.start_date).format('DD-MM-YYYY')" extraClass="no-border-but-bottom"></idatepicker-component>
                            </span>

                            <span class="col-xs-2 handle">                       
                                <idatepicker-component name="edit[end_date]" :value="moment.unix(task.end_date).format('DD-MM-YYYY')" extraClass="no-border-but-bottom"></idatepicker-component>
                            </span>

                            <span class="col-xs-4 handle">
                                <div class="checkbox col-xs-4">
                                    <label v-if="_.isEqual(task.sms_status, 2)">
                                        <input disabled checked type="checkbox"> SMS
                                    </label>
                                    <label v-else>
                                        <input v-on:change="setTargetTask(task)" v-model="task.sms_status" name="meta[sms_status]" value="1" type="checkbox"> SMS
                                    </label>
                                </div>
                                <div class="radio col-xs-8">
                                    <label>
                                        <input v-on:change="setTargetTask(task)" v-model="targetTask.post_status" type="radio" name="edit[post_status]" value="process"> Đang thực hiện
                                    </label>
                                    <label>
                                        <input v-on:change="setTargetTask(task)" v-model="targetTask.post_status" type="radio" name="edit[post_status]" value="complete"> Hoàn thành
                                    </label>
                                </div>
                            </span>
                        </form>
                    </template>
                    <template v-else>
                        <span class="col-xs-3 handle text" v-on:dblclick="setTargetTask(task);">
                            <i class="fa fa-ellipsis-v"></i>
                            <i class="fa fa-ellipsis-v"></i>
                            {{task.post_title}}
                        </span>

                        <span class="col-xs-2 " v-on:dblclick="setTargetTask(task);" v-text="moment.unix(task.start_date).format('DD-MM-YYYY')"></span>
                        <span class="col-xs-2 " v-on:dblclick="setTargetTask(task);" v-text="moment.unix(task.end_date).format('DD-MM-YYYY')"></span>

                        <span class="col-xs-4 " v-on:dblclick="setTargetTask(task);">
                            <div class="checkbox col-xs-4">
                                <label v-if="_.isEqual(task.sms_status, 2)">
                                    <input :name="_.uniqueId('is_sent-')" disabled checked type="checkbox"> SMS
                                </label>
                                <label v-else>
                                    <input v-on:change="setTargetTask(task)" v-model="task.sms_status" name="meta[sms_status]" value="1" type="checkbox"> SMS
                                </label>
                            </div>
                            <div class="radio col-xs-8">
                                <label>
                                    <input v-on:change="setTargetTask(task)" v-model="task.post_status" :name="_.uniqueId('post_status-')" value="process" type="radio" checked=""> Đang thực hiện
                                </label>
                                <label>
                                    <input v-on:change="setTargetTask(task)" v-model="task.post_status" :name="_.uniqueId('post_status-')" value="complete" type="radio" checked=""> Hoàn thành
                                </label>
                            </div>
                        </span>
                    </template>

                    <div class="tools pull-right box-tools" style="position: absolute;">
                        <button v-if="!_.isEmpty(targetTask) && targetTask.post_id == task.post_id" v-on:click="cancelEditModeTargetTask" type="button" class="btn btn-default btn-sm pull-right"><i class="fa fa-close"></i></button>
                        <button v-else type="button" v-on:click="setTargetAction('delete', true);setTargetTask(task);jQuery('#'+guid+'-delConfirm').modal('show');" class="btn btn-danger btn-sm pull-right"><i class="fa fa-trash"></i></button>
                        
                        <button v-if="!_.isEmpty(targetTask) && targetTask.post_id == task.post_id && targetAction.act == 'update' && !targetAction.state" 
                            class="btn btn-success btn-sm daterange pull-right disabled" 
                            data-toggle="tooltip" 
                            type="button" 
                            style="margin-right: 5px;"> <i class="fa fa-circle-o-notch fa-spin"></i></button>
                        <button v-else-if="!_.isEmpty(targetTask) && targetTask.post_id == task.post_id" v-on:click="if(_.eq(targetTask.post_id, 0)) {insertTask();} else updateTask();" type="button" class="btn btn-success btn-sm daterange pull-right" data-toggle="tooltip" style="margin-right: 5px;"> <i class="fa fa-check"></i></button>
                        
                        <button v-else v-on:click="setTargetTask(task)" type="button" class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" style="margin-right: 5px;"><i class="fa fa-edit"></i></button>
                    </div>
                </li>
            </ul>

            <div :id="guid + '-delConfirm'" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><i class="fa fw fa-exclamation-circle"> </i> Xác nhận xóa tác vụ <u>"{{ targetTask.post_title }}"</u></h4>
                        </div>
                        <div class="modal-body">
                            <p>Tác vụ <u>"{{ targetTask.post_title }}"</u> sẽ được xóa hoàn toàn ra khỏi dịch vụ ! <br> <u>Lưu ý : Thời gian trên hợp đồng sẽ không được cập nhật lại.</u></p>
                        </div>
                        <div v-if="!_.isEmpty(targetTask)" class="modal-footer">

                            <button v-if="targetAction.act == 'delete' && targetAction.state" type="button" v-on:click="deleteTargetTask" class="btn btn-danger"><i class="fa fw fa-remove"></i> Xóa tác vụ</button>
                            <button v-else-if="targetAction.act == 'delete' && !targetAction.state" type="button" class="btn btn-danger disabled"><i class="fa fa-circle-o-notch fa-spin"></i> Xóa tác vụ</button>

                            <button type="button" v-on:click="cancelEditModeTargetTask" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
                        </div>
                    </div>
                </div>
            </div>

            <div :id="guid + '-sortableConfirm'" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Chọn phương thức xử lý</h4>
                        </div>
                        <div class="modal-body">
                            <p>Note : Tác vụ sẽ được xóa hoàn toàn ra khỏi dịch vụ ! <br> <u>Lưu ý : Thời gian trên hợp đồng sẽ không được cập nhật lại.</u></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" v-on:click="updateSortableItems('override')" class="btn btn-success">Ghi đè tác vụ</button>
                            <button type="button" v-on:click="updateSortableItems('insert')" class="btn btn-primary">Chèn tác vụ</button>
                            <button type="button" v-on:click="jQuery('div#' + guid + ' ul.todo-list').sortable('cancel');sortableHandler = {};" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    `
});