Vue.component('extend-contract-button-component', {
    props: ['isHidden','term_id'],
    data: function(){
        return {
            admin_url: admin_url,
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            isLoading : false,
        };
    },
    created: function(){ },
    methods: {
        submit: function(){

            this.isLoading = true;
            var seft = this;

            $.ajax({
                type: 'POST',
                url: admin_url + 'webbuild/ajax/proc_service/' + this.term_id,
                data: { service_package : this.service_package },
                success: function(response){

                    $.notify(response.msg, {className: (response.status ? 'success' : 'error'), clickToHide: true, autoHideDelay: 15000,});

                    seft.isLoading = false;

                    if(response.status) 
                    {
                        seft.isHidden = true;
                        $('#modal-'+ seft.guid).modal('hide');
                        seft.isHidden = true;
                    }
                },
                dataType: 'json'
            });
        },
        service_package_updated: function(value){
            this.service_package = value;
        }
    },
    template: `
    <span>
        <button :id="'btn-' + guid" type="button" class="btn bg-orange" data-toggle="modal" :data-target="'#modal-' + guid">
            <template v-if="!isLoading"><i class="fa fa-fw fa-files-o"></i> Gia hạn</template>
            <template v-else><i class="fa fa-spinner fa-pulse fa-check"></i> Đang xử lý ...</template>
        </button>

        <div class="modal fade" :id="'modal-' + guid" tabindex="-1" role="dialog" aria-hidden="true" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-exclamation-circle"></i> Xác nhận gia hạn hợp đồng</h4>
                    </div>
                    <div class="modal-body">
                        <p>Hệ thống sẽ xử lý các tác vụ sau : </p>
                        <ol>
                            <li>Gia hạn thời hạn của chính hợp đồng này</li>
                            <li>Dừng việc cảnh báo gia hạn đến khách hàng</li>
                            <li>Gửi E-mail thông báo hợp đồng đã được gia hạn</li>
                            <li>Gửi SMS thông báo hợp đồng đã được gia hạn</li>
                        </ol>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Hủy bỏ</button>
                        <button v-if="!isLoading" v-on:click="submit" type="button" :id="'btn-' + guid" class="btn btn-primary"><i class="fa fa-check"></i> Xác nhận</button>
                        <button v-else type="button" class="btn btn-primary disabled"><i class="fa fa-spinner fa-pulse fa-check"></i> Đang xử lý...</button>
                    </div>
                </div>
            </div>
        </div>
    </span>
    `
});