Vue.component('proc-service-component', {
    props: ['isHidden','term_id'],
    data: function(){
        return {
            array_package:null,
            admin_url: admin_url,
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            isLoading : false,
            service_packages : jsobjectdata.service_packages,
            service_package: jsobjectdata.service_package,
            reloadCalendar : false,

            targetTask : null
        };
    },
    computed: {
        servicePackagesOptions: function(){
            if( typeof this.service_packages == 'undefined') return [];
            var options = [];
            for (var i = 0; i < this.service_packages.length; i++)
            {
                options.push({key:this.service_packages[i].name, value:this.service_packages[i].label});
            }
            return options;
        },
    },
    created: function(){
    },
    
    methods: {

        getTargetTask : function(){ return this.targetTask; },
        setTargetTask : function(task){ this.targetTask = task; },
        getTasks : function(){ return this.post_data; },

        setTasks : function(tasks){
            this.tasks = [];
            this.tasks = tasks;
        },

        proc_service_submit: function(){

            this.isLoading = true;
            var seft = this;

            $.ajax({
                type: 'PUT',
                url: admin_url + 'webbuild/api/service/proc/contract_id/' + this.term_id, // A valid URL
                headers: {"X-HTTP-Method-Override": "PUT"}, // X-HTTP-Method-Override set to PUT.
                data: { service_package : this.service_package, tasks: seft.array_package },
                success: function(response){

                    let messages = _.isString(response) ? [response] : response;
                    _.forEach(messages, function(msg) { $.notify(msg, 'success') });

                    seft.isLoading = false;

                    if(response.status) 
                    {
                        seft.isHidden = true;
                        $('#modal-'+ seft.guid).modal('hide');
                        seft.isHidden = true;
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    $.notify(JSON.parse(jqXHR.responseText)); 
                    seft.isLoading = false;
                },
                dataType: 'json'
            });
        },
        service_package_updated: function(value){
            this.service_package = value;
        },
        dataSubmit:function(value){
            var packge = this;
            packge.array_package = value;
        },
        reloadCalendarUpdated:function(e){
            this.reloadCalendar = true;
        },
        
    },
    template: `
    <div>
        <div v-if=" ! isHidden" :id="'container-' + guid" class="col-md-2">
            <div class="row">
                <button :id="'btn-' + guid" type="button" v-on:click="reloadCalendarUpdated" class="btn btn-default" data-toggle="modal" :data-target="'#modal-' + guid">
                    <template  v-if="!isLoading"><i class="fa fa-fw fa-play"></i> Thực hiện thiết kế</template>
                    <template v-else><i class="fa fa-spinner fa-pulse fa-check"></i> Đang xử lý ...</template>
                </button>
            </div>

            <div class="modal fade" :id="'modal-' + guid" tabindex="-1" role="dialog" aria-hidden="true" style="width: 100%;height: auto; overflow-x: hidden;
            overflow-y: auto;" >
                <div class="modal-dialog  modal-lg" role="document" style="width: 100%;height: auto;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-exclamation-circle"></i> Xác nhận thực hiện dịch vụ</h4>
                        </div>
                        <div class="modal-body">
                            <select2-component label="Package" name="package_service" v-on:on_changed="service_package_updated" :value="service_package" :options="servicePackagesOptions"></select2-component>
                            <p>Hệ thống sẽ xử lý các tác vụ sau : </p>
                            <ol>
                                <li>Khởi tạo danh sách các tác vụ mặc định <u><i> theo gói dịch vụ thiết kế đã chọn ở trên</i></u>.</li>
                                <li>Gửi E-mail thông báo dịch vụ đã được kích hoạt đến các bên liên quan (kinh doanh - khách hàng - bộ phận kỹ thuật).</li>
                                <li>Gửi SMS thông báo kích hoạt dịch vụ đến khách hàng.</li>
                            </ol>
                            
                        </div>
                        <section class="content" id="app-content">
                                <div class="row">
                                    <div class="col-md-3"><view-setting-cru-calender-component :reload="reloadCalendar" v-on:returndata="dataSubmit" :getDataFromCalendar="array_package" :term_id="this.term_id"/></div>
                                    <div class="col-md-9"><view-setting-calender-component :reload="reloadCalendar" v-on:returndata="dataSubmit" :term_id="this.term_id" :service_package = "service_package"/></div>

                                </div>
                            </section>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Hủy bỏ</button>
                            <button v-if="!isLoading" v-on:click="proc_service_submit" type="button" :id="'btn-' + guid" class="btn btn-primary"><i class="fa fa-check"></i> Xác nhận</button>
                            <button v-else type="button" class="btn btn-primary disabled"><i class="fa fa-spinner fa-pulse fa-check"></i> Đang xử lý...</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    `
});

Vue.component('stop-service-component', {
    props: ['isHidden','term_id'],
    data: function(){
        return {
            admin_url: admin_url,
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            isLoading : false,
        };
    },
    methods: {
        stop_service_submit: function(){

            this.isLoading = true;
            var seft = this;

            $.ajax({
                type: 'POST',
                url: admin_url + 'webbuild/ajax/stop_service/' + this.term_id,
                success: function(response){

                    $.notify(response.msg, {className: (response.status ? 'success' : 'error'), clickToHide: true, autoHideDelay: 15000,});

                    seft.isLoading = false;

                    if(response.status) 
                    {
                        seft.isHidden = true;
                        $('#modal-'+ seft.guid).modal('hide');
                        seft.isHidden = true;
                    }
                },
                dataType: 'json'
            });
        },
    },
    template: `
    <div>
        <div v-if=" ! isHidden" :id="'container-' + guid" class="col-md-2">
            <div class="row">
                <button :id="'btn-' + guid" type="button" class="btn btn-danger" data-toggle="modal" :data-target="'#modal-' + guid">
                    <template v-if="!isLoading"><i class="fa fa-fw fa-stop"></i> Kết thúc hợp đồng</template>
                    <template v-else><i class="fa fa-spinner fa-pulse fa-check"></i> Đang xử lý ...</template>
                </button>
                
            </div>

            <div class="modal fade" :id="'modal-' + guid" tabindex="-1" role="dialog" aria-hidden="true" >
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><i class="fa fa-exclamation-circle"></i> Xác nhận kết thúc dịch vụ</h4>
                        </div>
                        <div class="modal-body">
                            <p>Hệ thống sẽ xử lý các tác vụ sau : </p>
                            <ol>
                                <li>Hợp đồng sẽ được chuyển thành trạng thái "kết thúc"</li>
                                <li>Mọi chỉnh sửa ở dịch vụ sẽ không còn hiệu lực</li>
                            </ol>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Hủy bỏ</button>
                            <button v-if="!isLoading" v-on:click="stop_service_submit" type="button" :id="'btn-' + guid" class="btn btn-primary"><i class="fa fa-check"></i> Xác nhận</button>
                            <button v-else type="button" class="btn btn-primary disabled"><i class="fa fa-spinner fa-pulse fa-check"></i> Đang xử lý...</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    `
});

// payment Alert 

Vue.component('alert-payment-component',{
    props: ['term_id'],
    data: function(){
        return {
            admin_url: admin_url,
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            isLoading : false,
        };
    },
    methods:{
        sendMail:function(){
            $('#paymentAlert').modal('show');
        },
        paymentAlertOk:function(){
            var packge = this;
            packge.isLoading = true;
            var term_id = 0;
            term_id = this.term_id;
            $.post(admin_url + 'webbuild/ajax/send_mail_payment_alert/' + term_id,
            function(response){
               packge.isLoading = false;
                if(response.status){
                    
                    return packge.notify(response.msg,"success"); 
                    
                }
                
                return packge.notify(response.msg,"error"); 


            })

        },
        notify: function(msg,className){

            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },
    },
    template:`
        <div>
            <div class="col-md-2">
                <button :id="'btn-send-mail-' + guid" type="button" v-on:click="sendMail" class="btn btn-warning">
                    <template  v-if="!isLoading"><i class="fa fa-paper-plane"></i> Thông báo tiến độ</template>
                    <template v-else><i class="fa fa-spinner fa-pulse fa-check"></i> Đang xử lý ...</template>
                </button>
            </div>

            <!-- Modal -->
            <div id="paymentAlert" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="modal-title">Gửi mail?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="Cancel" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-info" id="OK" v-on:click="paymentAlertOk" data-dismiss="modal">Xác nhận</button>
                    </div>
                    </div>

                </div>
            </div>
            <!-- /Modal -->
        </div>

    `
})

//calender
Vue.component('view-setting-calender-component',{
    props:['term_id','service_package','reload'],
    data:function(){
        post_data:[];
        admin_url:admin_url;
        service_package:null;
        start:null;
        end:null
    },
    watch:{
        service_package:function(){
            var a= this;
            this.loadTaskConfig();
            setTimeout(function(){  
                $('#calendar').fullCalendar( 'destroy' );
                a.firstload();
            }, 1000);
        },
        reload : function(){
            if(this.reload == true)
            {
                this.firstload();
               
                setTimeout(function(){$('#calendar').fullCalendar('render');}, 1000);
            }
        }
    },
    computed:{},
    created: function(){
        this.loadTaskConfig();
    },
    mounted:function(){
        this.changePackage();
        this.firstload();
    },
    methods:{
        
        getTargetTask : function(){ return this.targetTask; },
        setTargetTask : function(task){ this.targetTask = task; },
        getTasks : function(){ return this.post_data; },

        setTasks : function(tasks){
            this.post_data = [];
            this.post_data = tasks;
        },

        changePackage:function(){
            var package = this;
            $('select[name=package_service]').on('change',function(){
                service_package = $('select[name=package_service]').find(":selected").val();
            })
        },
        loadTaskConfig : function(){
            var seft = this;
            
            axios.get(admin_url + 'webbuild/api/contract/task/contract_id/'+ this.term_id +'/package/' + this.service_package)
            .then(function (response) {

                if(_.isEmpty(response.data.data)) return false;

                let _tasks = _.map(response.data.data, function(x){ x.id = _.uniqueId('task_uniq_'); return x; });

                seft.setTasks(_tasks);
                seft.$emit('returndata', seft.getTasks());
            })
            .catch(function (error){
                $.notify(error.response.data, 'error');
            });
        },
        firstload : function(){
            
            var seft = this;
            var del = false;

            $('#calendar').fullCalendar({
                header    : {
                    left  : 'prev,next today',
                    center: 'title',
                    right : 'month'
                },
                buttonText: {
                    today: 'today',
                    month: 'month',
                    week : 'week',
                    day  : 'day'
                },
                events: function(start, end, timezone, callback) {

                    if(_.isEmpty(seft.post_data))
                    {
                        callback([]);
                        return;
                    }

                    let events = _.map(seft.post_data, function(x){
                        return {
                            title          : x.post_title,
                            start          : moment.unix(x.start_date).toDate(),
                            end            : moment(moment.unix(x.end_date).toDate()).add(16,'hour').add(59,'minute').add(59,'second'),
                            allDay         : true,
                            backgroundColor: x.post_status =='complete' ? '#00a65a' : '#f39c12',
                            borderColor    : x.post_status =='complete' ? '#00a65a' : '#f39c12',
                            id             : x.id
                        }
                    });

                    callback(events);
                },
                timeFormat: '(:)',
                editable  : true,
                droppable : true,
                eventRender: function( event, element, view ) {
                    //add span delete for event
                    element.find('div.fc-content').append('<span  id="remove_'+event.id+'" data-toggle="modal" href="#myModal" ><i class="fa fa-remove" style="float:right;margin-right: 1%;"></i></span>');
                    element.find('#remove_'+event.id).on('click', function() {
                        del = true;
                        $('#OK').on('click',function(){
                            $('#myModal').modal('hide');
                        
                            // delete item in seft.post_data
                            seft.post_data.forEach(e=>{
                                if(e.id == event.id){
                                    seft.post_data.splice(seft.post_data.indexOf(e), 1);
                                }
                            })
                            seft.$emit('returndata',seft.post_data);
                            del = false;
                            $('#calendar').fullCalendar('removeEvents');
                            $('#calendar').fullCalendar('refetchEvents',seft.post_data);
                        })
                    });
                },
                eventClick: function(calEvent, jsEvent, view) {
                    if(!del){
                        $('#box_title').text('Chỉnh sửa ' +' '+ calEvent.title);
                        seft.post_data.forEach(e => {
                            if(e.id==calEvent.id){
                                var start = calEvent.start.format('DD/MM/YYYY');
                                var end = moment(calEvent.end).add(-1,'day').format('DD/MM/YYYY');
                                $('#fromdate').val(start);
                                $('#todate').val(end);
                                $('#taskid').val(calEvent.id);
                                $('#new-content').val(calEvent.post_content);
                                $('#new-event').val(calEvent.title);
                                if(calEvent.post_status=="complete"){
                                    //$('#complete').parent().trigger("click");
                                   
                                    $('#process').attr('disabled',true);
                                    $('#complete').attr('disabled',true);
                                }else{
                                    $('#process').removeAttr('disabled',false);
                                    $('#complete').attr('disabled',false);
                                    $('#process').parent().trigger("click");
                                   
                                }
                                
                                if(e.sms_status== 1){
                                    $('#sms_status').parent().addClass('checked');
                                    $('#sms_status').parent().removeClass('disabled') ;
                                }else if(e.sms_status == 2){
                                    $('#sms_status').parent().addClass('disabled') ;
                                    $('#sms_status').parent().addClass('checked');
                                }else{
                                    $('#sms_status').parent().removeClass('checked');
                                    $('#sms_status').parent().removeClass('disabled') ;
                                }

                            }
                        })
                    }

                },
                eventDrop: function(event, delta, revertFunc) {
                    seft.post_data.forEach(e => {
                        if(e.id==event.id){
                            
                            var start = event.start.format("X");//event.start.format('DD/MM/YYYY');
                            var end = event.end==null? start: moment(event.end).add(-1,'day').add(1,'minute').format("X");

                            e.start_date = start;
                            e.end_date = end;
                        }
                    })
                    seft.$emit('returndata',seft.post_data);
                },
                eventResize :function( event, delta, revertFunc, jsEvent, ui, view ) {
                    seft.post_data.forEach(e => {
                        if(e.id==event.id){
                            var start = event.start.format("X");//event.start.format('DD/MM/YYYY');
                            var end = event.end==null? start: moment(event.end).add(-1,'day').add(1,'minute').format("X");

                            e.start_date = start;
                            e.end_date = end;
                        }
                    })
                    
                    seft.$emit('returndata',seft.post_data);
                },
                eventAllow:function(dropInfo, draggedEvent){
                    seft.start = parseInt(dropInfo.start.format("X"));
                     
                    seft.end = parseInt(dropInfo.end.format("X"));//moment(dropInfo.end).add(-1,'day').format("X");
                    
                }
            })
            
        },
    },
    template:`
    <div>
        <div class="box box-primary">
            <div class="box-body no-padding">
            <div id="calendar"></div>
            </div>
        </div>

        <!-- Modal -->
        <div id="myModal" class="modal fade" >
            <div class="modal-dialog" style="margin: auto;transform: translateY(-50%);top: 50%;">
                <!-- Modal content-->
                <div class="modal-content">
                <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    <h4 id="modal-title">Chắt chắn xóa?</h4>
                </div>
                <div class="modal-footer">
                    <!--<button type="button" class="btn btn-default" id="Cancel" data-dismiss="modal">Đóng</button>-->
                    <button type="button" class="btn btn-info" id="OK" >Xác nhận</button>
                   
                </div>
                </div>

            </div>
        </div>
    </div>
    `
});

Vue.component('view-setting-cru-calender-component',{
    props:['term_id','getDataFromCalendar','reload'],
    data:function(){
        data:null;
        admin_url:admin_url;
    },
    watch:{
        
        reload : function(){
            if(this.reload == true)
            {
                this.data = this.getDataFromCalendar;
            }

        },
        getDataFromCalendar:function(){
            this.data = this.getDataFromCalendar;
        }
    },
    computed:{

    },
    mounted:function(){
        this.firtLoad();
        this.initValidate();
       
        
    },
    methods:{
        firtLoad: function() {
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });
            $('#sms_status').parent().addClass('checked');
            //Datemask dd/mm/yyyy
            $('#fromdate,#todate').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
            
            this.set_color();
            $('input[name="edit[meta][post_status]"]').on('ifClicked', function (event) {
                if(this.value=="on"){
                    return $('#color-chooser > li > a > .color-orange').trigger("click");

               }
               return $('#color-chooser > li > a > .color-green').trigger("click");
            });
        },
        initValidate : function(){

            $('#add-new-frm').validate({
                rules: {
                    'edit[meta][start_date]': {
                        required: true,
                    },
                    'edit[meta][end_date]': {
                        required: true,
                    },
                    'edit[meta][post_title]': {
                        required: true,
                    }
                },
                messages:{
                    'edit[meta][start_date]': {
                        required: "Trường này là bắt buộc.",
                    },
                    'edit[meta][end_date]': {
                        required: "Trường này là bắt buộc.",
                    },
                    'edit[meta][post_title]': {
                        required: "Trường này là bắt buộc.",
                    }
                }
            });
        },
        // set color for button Add
        set_color: function(){
            /* ADDING EVENTS */
            var currColor = '#f39c12' //Red by default
            //Color chooser button
            var colorChooser = $('#color-chooser-btn');
            $('#color-chooser > li > a').click(function (e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css('color')
            //Add color effect to button
            $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
            })


        },
        /* Notify MSG out to screen */
        notify: function(msg,className){

            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },

        ascii:function(string){
            var str = string;
            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
            str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
            str = str.replace(/đ/g,"d");
            str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
            str = str.replace(/ + /g," ");
            str = str.trim(); 
            return str;
        },

        formatDate:function(date){
            var arrDate = date.split("/");

            return [arrDate[2], arrDate[1], arrDate[0]].join('-');
        },

        submit: function(){

             var calendar = this;
            
            var isValid = $('#add-new-frm').valid();
            if( ! isValid) return false;
            //validate fromdate and todate for dd/mm/yyyy
            if($('#fromdate').val().indexOf('d')!=-1){
                $('#fromdate').addClass('error');
                $('#fromdate-error').css("display","inline-block");
                $('#fromdate-error').text('Ngày tháng năm không chính xác.');
                $('#fromdate-error').show();
                 return false;
            }
            if($('#fromdate').val().indexOf('m')!=-1){
                $('#fromdate').addClass('error');
                $('#fromdate-error').css("display","inline-block");
                $('#fromdate-error').text('Ngày tháng năm không chính xác.');
                $('#fromdate-error').show();
                 return false;
            }
            if($('#fromdate').val().indexOf('y')!=-1) {
                $('#fromdate').addClass('error');
                $('#fromdate-error').css("display","inline-block");
                $('#fromdate-error').text('Ngày tháng năm không chính xác.');
                $('#fromdate-error').show();
                 return false;
            }
            if($('#todate').val().indexOf('d')!=-1) {
                $('#todate').addClass('error');
                $('#enddate-error').text('Ngày tháng năm không chính xác.');
                $('#enddate-error').css("display","inline-block");
                $('#enddate-error').show();
                 return false;
            }
            if($('#todate').val().indexOf('m')!=-1){
                $('#todate').addClass('error');
                $('#enddate-error').text('Ngày tháng năm không chính xác.');
                $('#enddate-error').css("display","inline-block");
                $('#enddate-error').show();
                 return false;
            }
            if($('#todate').val().indexOf('y')!=-1){
                $('#todate').addClass('error');
                $('#enddate-error').text('Ngày tháng năm không chính xác.');
                $('#enddate-error').css("display","inline-block");
                $('#enddate-error').show();
                 return false;
            }
           
            // chuyen dd/mm/yyyy to timespan
            var frdate = Math.round(new Date(calendar.formatDate($('#fromdate').val())+" 00:00:00").getTime()/1000);
            var todate = Math.round(new Date(calendar.formatDate($('#todate').val())+" 23:59:59").getTime()/1000);
            // save in array or update
            if($('#taskid').val()==""){
                var dt = {
                    post_title:$('#new-event').val(),
                    start_date:frdate,
                    end_date:todate,
                    title_ascii:calendar.ascii($('#new-event').val()),
                    id: calendar.data.length +1,
                    post_type:"webbuild-task",
                    post_author:1
                }
                calendar.data.push(dt);
            }else{
               
                calendar.data.forEach(e => {
                    
                    if(parseInt($('#taskid').val())==e.id){
                        e.post_title = $('#new-event').val(),
                        e.start_date = frdate,
                        e.end_date = todate,
                        e.title_ascii = calendar.ascii($('#new-event').val()),
                        e.id = calendar.data.length +1,
                        e.post_type = "webbuild-task",
                        e.post_author = 1
                    }
                })
            }
            calendar.add_new();
            //reset Calendar
            $('#calendar').fullCalendar('removeEvents',calendar.getDataFromCalendar);
            $('#calendar').fullCalendar('refetchEvents',calendar.data);
            //emit data new for save
            calendar.$emit('returndata',calendar.data);
        },
        add_new:function(){
            $('#process').parent().trigger("click");
            $("#add-new-frm").trigger("reset");
            $('#box_title').text('Thêm mới');

        }
    },
    template:`
    <div>
        <div class="box box-solid;" style="display:none;">
            <div class="box-header with-border">
            <h4 class="box-title">Draggable Events</h4>
            </div>
            <div class="box-body">

            <div id="external-events">
                <div class="external-event bg-green">Lunch</div>
                <div class="external-event bg-yellow">Go home</div>
                <div class="external-event bg-aqua">Do homework</div>
                <div class="external-event bg-light-blue">Work on UI design</div>
                <div class="external-event bg-red">Sleep tight</div>
                <div class="checkbox">
                <label for="drop-remove">
                    <input type="checkbox" id="drop-remove">
                    remove after drop
                </label>
                </div>
            </div>
            </div>
        </div>
            <div class="box box-solid">
                <div class="box-header with-border">
                <h3 id="box_title" class="box-title">Thêm mới</h3>
                </div>
                <div class="box-body">
                <div class="btn-group" style="width: 100%; margin-bottom: 10px;" style="display:none">
                    <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                    <ul class="fc-color-picker" id="color-chooser">
                    <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-yellow" href="#"><i class="fa fa-square color-yellow"></i></a></li>
                    <li><a class="text-orange" href="#"><i class="fa fa-square color-orange"></i></a></li>
                    <li><a class="text-green" href="#"><i class="fa fa-square color-green"></i></a></li>
                    <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                    </ul>
                </div>
                <form method="post" id="add-new-frm" accept-charset="utf-8">
                <div class="form-group" style="display:none;">
                    <div class="input-group" style="width: 100%;">
                        <input id="taskid" name="edit[meta][id]" type="text" class="form-control" placeholder="Id">
                    </div>
                </div>
                <div class="form-group">
                    <strong>Tiêu đề</strong>
                    <div class="input-group" style="width: 100%;">
                            <input id="new-event" type="text" name ="edit[meta][post_title]" class="form-control" placeholder="Tiêu đề">
                    </div>
                </div>
                <div class="form-group">
                    <strong>Ngày bắt đầu</strong>
                    <div class="input-group" style="margin-bottom: 2%;">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                        <input type="text" id="fromdate" name="edit[meta][start_date]" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        <label id="fromdate-error" class="error" for="fromdate" style="display: none;"></label>
                    </div>

                    <strong>Ngày kết thúc</strong>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id ="todate" name="edit[meta][end_date]" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        <label id="enddate-error" class="error" for="todate" style="display: none;"></label>
                    </div>
                </div>
                <div class="form-group">
                <div class="form-group" style="float: left; margin-right: 5%;">
                    <label>
                        <input type="checkbox" id="sms_status" name="edit[meta][sms_status]" class="minimal" checked disabled> SMS
                    </label>
                </div>
                <div class="form-group" id="radio-checked" style="margin-top: 3%;">
                            <label class="" style="margin-right: 5%;color: #f39c12">
                            <input type="radio" id="process" name="edit[meta][post_status]" class="minimal" checked="" value="on" style="position: absolute; opacity: 0;">
                            Đang thực hiện</label>
                            <label class="" style="color:#00a65a">
                            <input type="radio" id="complete" name="edit[meta][post_status]" class="minimal" value="off" style="position: absolute; opacity: 0;" disabled>
                            Hoàn thành</label>
                        </div>

                </div>
                
                <div class="form-group" style="float: right;">
                        <button id="add-new-event" v-on:click="submit" type="button" style="background-color: #f39c12; border-color: #f39c12;" class="btn btn-primary btn-flat">Lưu</button>

                        <button id="refresh" v-on:click="add_new" type="button"  style="background-color: #696969; border-color: #696969;" class="btn btn-primary btn-flat">Làm mới</button>

                    </div>
                </div>
                </form>
                

            </div>

            </div>
        </div>
    </div>

    `
});