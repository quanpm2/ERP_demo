Vue.component('webbuild-tasks-page',{
    props: {
        guid : { type : String, default : () => { return _.uniqueId(['_wtc_']); }},
        contract_id : { type: Number, default : 0 },
    },
    data: function(){
        return {
            view : 'todolist',
        }
    },
    methods : {
        setView : function(type){ this.view = type; }
    },
    template: `
    <div :id="guid" class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
            <li :class="view=='todolist'?'active':''">
                <a v-on:click="setView('todolist')" :href="guid+'tab_pane'" data-toggle="tab"><i class="fa fa-list-ul"></i></a>
            </li>
            <li :class="view=='calendar'?'active':''">
                <a v-on:click="setView('calendar')" :href="guid+'tab_pane'" data-toggle="tab"><i class="fa fa-calendar"></i></a>
            </li>
            <li class="pull-left header"><i class="fa fa-tasks"></i></li>
        </ul>
        <div class="tab-content no-padding">
            <div class="container-fluid">
                <br/>
                <div class="tab-pane active" :id="guid+'tab_pane'">
                    <template v-if="_.isEqual(view, 'todolist')"><webbuild-tasks-grid :contract_id="contract_id"></webbuild-tasks-grid></template>
                    <template v-else>
                        <div>
                            <div class="col-md-3"><webbuild-tasks-calendar-create-update :contract_id="contract_id"></webbuild-tasks-calendar-create-update></div>
                            <div class="col-md-9"><webbuild-tasks-calendar :contract_id="contract_id"></webbuild-tasks-calendar></div>
                        </div>
                    </template>
                </div>
            </div>
        </div>
    </div>
    `
});