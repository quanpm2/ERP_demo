Vue.component('comments-list-partial-component', {
    props: ['term_id','user_id'],
    data: function(){
        return {
            messages : null,
            admin_url: admin_url,
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            messageContent: '',
        };
    },
    watch : {
        term_id : function(){

            this.getMessages();
            $('#modal-comment-' + this.guid).modal('show');
        }
    },
    computed: {},
    created: function(){ },
    methods: {

        addMessage : function(){

            var seft = this;
            var user_id = 'all';
            
            axios
            .get(this.admin_url + 'message/ajax/add?term_id='+this.term_id+'&msg_content='+this.messageContent)
            .then(function (response) {
                if( ! response.data.status) return false;

                var messages = seft.messages;
                if(messages == null) messages = [];

                messages.push(response.data.data);
                seft.messages = messages;
            }); 

        },

        getMessages: function(){
            var seft = this;
            var user_id = 'all';

            this.messages = null;

            axios
            .get(this.admin_url + 'message/ajax/dataset?term_id='+this.term_id)
            .then(function (response) {
                
                if( ! response.data.status) return false;

                seft.messages = response.data.data;
            });  
        },
    },
    template: `
    <span>
        <div class="modal fade" :id="'modal-comment-' + guid">
            <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Ghi chú</h4>
                  </div>
                  <div class="modal-body">

                    <!-- Conversations are loaded here -->
                    <div v-if="messages != null" class="direct-chat-messages">
                        <template v-for="message in messages">
                            <div :class="user_id == message.user_id ? 'direct-chat-msg' : 'direct-chat-msg right'">
                              <div class="direct-chat-info clearfix">
                                <span :class="user_id == message.user_id ? 'direct-chat-name pull-left' : 'direct-chat-name pull-left'" v-text="message.user.display_name"></span>
                                <span :class="user_id == message.user_id ? 'direct-chat-timestamp pull-left' : 'direct-chat-timestamp pull-left'" v-text="message.created_date"></span>
                              </div>
                              <img class="direct-chat-img" :src="message.user.user_avatar" :alt="message.user_id">
                              <div class="direct-chat-text" v-text="message.msg_content"></div>
                            </div>
                            <!-- /.direct-chat-msg -->
                        </template>
                    </div>
                    <span v-else>Hiện tại chưa có ghi chú ...</span>
                  </div>
                  <div class="modal-footer">
                    <div class="input-group">
                      <input type="text" name="message" v-model.trim="messageContent" placeholder="nhập thông tin bạn cần ghi chú vào đây ..." class="form-control">
                      <span class="input-group-btn">
                        <button v-on:click="addMessage" type="submit" class="btn btn-primary btn-flat">Send</button>
                      </span>
                    </div>
                  </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </span>
    `
});