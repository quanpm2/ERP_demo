Vue.component('webbuild-tasks-calendar',{
    props: {
        guid : { type : String, default : () => { return _.uniqueId(['_wbtc_']); }},
        contract_id : { type: Number, default : 0 },
    },
    data: function(){
        return {
            admin_url: admin_url,
            checkdrop: true,
            defaultDate: null,
            tasks : []
        }
    },
    mounted: function(){
        this.firstload();
    },
    updated: function(){
        this.notify();
    },
    methods: {
        notify: function(msg,className){
            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },

        setTasks : function(values){ if(_.isEmpty(values)) return false; this.tasks = []; this.tasks = values; },
        getTasks : function() { return _.isEmpty(this.tasks) ? [] : _.cloneDeep(this.tasks); },

        firstload : function(){
            var task    = this;
            var seft    = this;
            var del     = false;
            
            $('#calendar').fullCalendar({
                header    : {
                    left  : 'prev,next today',
                    center: 'title',
                    right : 'month'
                },
                buttonText: {
                    today: 'today',
                    month: 'month',
                    week : 'week',
                    day  : 'day'
                },
                timezone: 'Asia/Ho_Chi_Minh',
                events: function(start, end, timezone, callback) {

                    axios.get(admin_url + 'webbuild/api/contract/task/contract_id/' + seft.contract_id)
                    .then(function (response) {

                        if(_.isEmpty(response.data.data)) return false;
                        console.log(response.data.data);
                        let tasks   = response.data.data;
                        seft.setTasks(response.data.data);

                        for (var i = 0; i < tasks.length; i++)
                        {
                            if(_.isEqual(tasks[i].post_status, "complete")) continue;
                            if(_.isEmpty(seft.defaultDate)) seft.defaultDate = tasks[i].start_date;

                            seft.defaultDate = seft.defaultDate < tasks[i].start_date ? seft.defaultDate : tasks[i].start_date;
                        }

                        seft.defaultDate = moment(moment.unix(seft.defaultDate).toDate()).format('YYYY-MM-DD');

                        callback(_.map(tasks, function(o){
                            return {
                                title           : o.post_title,
                                start           : moment.unix(o.start_date).toDate(),
                                end             : moment(moment.unix(o.end_date).toDate()).add(16,'hour').add(59,'minute').add(59,'second'),//1 su kien duoc xay ra tu ngay a den ngay b. Calendar se tinh toi 23:59:59 cua ngay b. ma 23:59:59 la ngay moi. Nen calerdar khong hien thi tu a toi b ma hien thi tu a den b-1(ngay) ngay enddate vi the phai + 1.
                                backgroundColor : o.post_status =='complete'?'#00a65a':'#f39c12',
                                borderColor     : o.post_status =='complete'?'#00a65a':'#f39c12',
                                post_id         : o.post_id,
                                allDay          : true
                            }
                        }));
                    })
                    .catch(function (error) {
                        if(_.isEmpty(error.response)) return;

                        $.notify(error.response.data, 'error');
                        seft.checkdrop = false;
                    });                       
                },
                editable  : true,
                droppable : true,
                timeFormat: '(:)',
                eventDurationEditable : true,
                eventRender: function( event, element, view ) {
                    //add span delete for event
                    element.find('div.fc-content').append('<span  id="remove_'+event.post_id+'" ><i class="fa fa-remove" style="float:right;margin-right: 1%;"></i></span>');

                    element.find('#remove_'+event.post_id).on('click', function() {
                        del = true;
                        $('#myModal').modal('show');
                        $('#OK').on('click',function(){

                            axios
                            .delete(admin_url + 'webbuild/api/contract/task/contract_id/' + seft.contract_id + '/id/' + event.post_id)
                            .then(function (response){
                                
                                $.notify(response.data, 'success');
                                $('#calendar').fullCalendar('refetchEvents');

                                let tasks = _.cloneDeep(seft.tasks);
                                _.remove(tasks, function(o){ return o.post_id == event.post_id; });
                                seft.setTasks(tasks);
                            })
                            .catch(function (error){
                                $.notify(error.response.data, 'error');
                                _seft.setTargetAction('delete', true);
                            })
                            .then(function (){
                            });

                            del = false;
                            event.post_id = null;
                        })
                    });
                },
                eventClick: function(calEvent, jsEvent, view) {
                    if(!del){
                        $('#box_title').text('Chỉnh sửa ' +' '+ calEvent.title);
                        seft.getTasks().forEach(e => {
                            if(e.post_id==calEvent.post_id){
                                var start = calEvent.start.format('DD-MM-YYYY');
                                var end = calEvent.end==null? start: moment(calEvent.end).add(-1,'day').add(1,'minute').format('DD-MM-YYYY');
                                $('#fromdate').val(start);
                                $('#todate').val(end);
                                $('#taskid').val(e.post_id);
                                $('#new-content').val(e.post_content);
                                $('#new-event').val(e.post_title);
                                if(e.post_status=="complete"){
                                    $('#complete').parent().trigger("click");
                                    $('#check_complete').val("complete");
                                    $('#process').attr('disabled',true);
                                    //$('#complete').attr('disabled',true);
                                }else{
                                    $('#check_complete').val("process");
                                    $('#process').removeAttr('disabled',false);
                                    $('#complete').attr('disabled',false);
                                    $('#process').parent().trigger("click");
                                   
                                }
                                
                                if(e.sms_status== 1){
                                    $('#sms_status').parent().addClass('checked');
                                    $('#sms_status').parent().removeClass('disabled') ;
                                }else if(e.sms_status == 2){
                                    $('#sms_status').parent().addClass('disabled') ;
                                    $('#sms_status').parent().addClass('checked');
                                }else{
                                    $('#sms_status').parent().removeClass('checked');
                                    $('#sms_status').parent().removeClass('disabled') ;
                                }

                            }
                        })
                    }

                },
                eventDrop: function(event, delta, revertFunc) {

                    seft.getTasks().forEach(e => {
                        if(e.post_id==event.post_id){
                            var start = event.start.format('DD-MM-YYYY');
                            var end = event.end==null? start: moment(event.end).add(-1,'day').add(1,'minute').format('DD-MM-YYYY');
                            $('#fromdate').val(start);
                            $('#todate').val(end);
                            $('#taskid').val(e.post_id);
                            $('#new-content').val(e.post_content);
                            $('#new-event').val(e.post_title);
                            if(e.post_status=="complete"){
                                $('#complete').parent().trigger("click");
                                $('#process').attr('disabled',true);
                                $('#check_complete').val("complete");
                                task.checkdrop = false;
                            }else{
                                $('#check_complete').val("process");
                                $('#process').removeAttr('disabled',false);
                                $('#complete').attr('disabled',false);
                                $('#process').parent().trigger("click");
                            }
                            if(e.sms_status== 1){
                                $('#sms_status').parent().addClass('checked');
                                $('#sms_status').parent().removeClass('disabled') ;
                            }else if(e.sms_status== 2){
                                $('#sms_status').parent().addClass('disabled') ;
                                $('#sms_status').parent().addClass('checked');
                            }else{
                                $('#sms_status').parent().removeClass('checked');
                                $('#sms_status').parent().removeClass('disabled') ;
                            }
                          $('#add-new-event').trigger("click"); 
                          if(!task.checkdrop){
                            revertFunc();
                          }
                          task.checkdrop = true;
                        }
                    })
                },
                eventResize :function( event, delta, revertFunc, jsEvent, ui, view ) {
                    seft.getTasks().forEach(e => {
                        if(e.post_id==event.post_id){
                            var start = event.start.format('DD-MM-YYYY');
                            var end = event.end==null? start: moment(event.end).add(-1,'day').add(1,'minute').format('DD-MM-YYYY');
                            $('#fromdate').val(start);
                            $('#todate').val(end);
                            $('#taskid').val(e.post_id);
                            $('#new-content').val(e.post_content);
                            $('#new-event').val(e.post_title);
                            if(e.post_status=="complete"){
                                $('#complete').parent().trigger("click");
                                $('#process').attr('disabled',true);
                                $('#check_complete').val("complete");
                                //$('#complete').attr('disabled',true);
                                task.checkdrop = false;
                            }else{
                                $('#check_complete').val("process");
                                $('#process').removeAttr('disabled');
                                $('#complete').attr('disabled',false);
                                $('#process').parent().trigger("click");
                            }
                            if(e.sms_status== 1){
                                $('#sms_status').parent().addClass('checked');
                                $('#sms_status').parent().removeClass('disabled') ;
                            }else if(e.sms_status== 2){
                                $('#sms_status').parent().addClass('disabled') ;
                                $('#sms_status').parent().addClass('checked');
                            }else{
                                $('#sms_status').parent().removeClass('checked');
                                $('#sms_status').parent().removeClass('disabled') ;
                            }
                            $('#add-new-event').trigger("click");
                            if(!task.checkdrop){
                                revertFunc();
                            }
                            task.checkdrop = true;
                        }
                    })
                },
                eventAllow:function(dropInfo, draggedEvent){
                    var start = dropInfo.start.format('DD-MM-YYYY');
                    var end = moment(dropInfo.end).add(-1,'day').format('DD-MM-YYYY');
                     
                    $('#fromdate').val(start);
                    $('#todate').val(end);
                }
            })
            // nếu có tác vụ thì sẽ hiển thị ngày tháng của tác vụ nếu không có thì show ngày hiện tại
            setTimeout(() => {
                $('#calendar').fullCalendar('gotoDate',task.defaultDate);
            }, 1000);
            
        },
    },
    template: `
    <div :id="guid">
        <div class="box box-primary">
            <div class="box-body no-padding">
            <div id="calendar"></div>
            </div>
        </div>

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 id="modal-title">Chắt chắn xóa?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="Cancel" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-info" id="OK" data-dismiss="modal">Xác nhận</button>
                </div>
                </div>

            </div>
        </div>
    </div>
    `
});