Vue.component('fbcontent-index-component',{
    props: ['options','remote_url'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            selectedTermId : 0,
            url : admin_url + 'fbcontent/ajax/dataset?&where%5Bterm_status%5D=publish&order_by%contract_begin%5D=desc',
            term_id:0,
            active: true
        } 
    },
    computed : {
        current_page_text: function(){
            return this.options.current_page + '/' + this.options.num_page;
        }
    },

    mounted(){
        this.active = false;
        this.active = true;
    },

    methods : {
        
        send_email_receipt_payment_paid : function(){

            var btn = $("#send-mail-payment");
            var form = btn.closest('form');

            btn.find('i').attr('class','fa fa-fw fa-refresh fa-spin');

            $.ajax({
                url : admin_url + 'contract/ajax/receipt/send_email_receipt_payment_paid',
                data : form.serializeArray(),
                success : function(response) {

                    btn.find('i').attr('class', 'fa fa-fw fa-send');

                    if(response.status == false)
                    {
                        $.notify(response.msg, 'error');
                        return ;
                    }

                    $.notify(response.msg);
                },
                type : 'POST',
                async :true
            })
        },

        dataChanged: function(value)
        {
            var seft = this;
            $(".select2").select2({'placeholder':$(this).attr('placeholder')});
            $(".set-datepicker").daterangepicker({format: 'DD-MM-YYYY'});

            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({checkboxClass: 'icheckbox_minimal-blue',radioClass: 'iradio_minimal-blue'});
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({checkboxClass: 'icheckbox_minimal-red',radioClass: 'iradio_minimal-red'});
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({checkboxClass: 'icheckbox_flat-green',radioClass: 'iradio_flat-green'});

            $('#all-checkbox').on('ifChecked', function(e) { $('.table input:checkbox').iCheck('check'); });
            $('#all-checkbox').on('ifUnchecked', function(e) { $('.table input:checkbox').iCheck('uncheck'); });

            $("button#send-mail-payment").off('click').on('click',function(){
                seft.send_email_receipt_payment_paid();
            });

            // console.log($('select[name=where[departments]]').text());
            $("select[name*=departments]").select2().on("select2:selecting", function(e) { 
              item = $(e.params.args.originalEvent.toElement).data('data')
              //console.log(item.id);
              $.get(admin_url + 'contract/ajax/receipt/getGroupByParent/'+item.id,function(res){
               
                if(res.status){
                  $('select[name*=user_groups]').html('').select2({data: [{id: '', text: 'Nh�m'}]});
                  $('select[name*=user_groups]').val($("select[name*=user_groups] option:first").val(''));
                  for (var key in res.data) {
                        var element  = res.data[key];

                        if(element.term_id){
                            $('select[name*=user_groups]').select2({data: [{id: element.term_id, text: element.term_name}]});
                        }else{
                            var children = [];
                            for(var k in element){
                                children.push({
                                    id  : k,
                                    text: element[k]
                                });
                            }
                            $('select[name*=user_groups]').select2({
                                data:[{
                                  text    : key,
                                  children: children
                                }]});
                        }
                   }
                }
              })
            });
        },
    },
    template: `
    <div v-if="active">
        <datatable-builder-component is_filtering="false" is_ordering="true" :is_download="true" per_page="50" :remote_url="url" v-on:data_changed="dataChanged" ></datatable-builder-component> 
    </div>
    `
});