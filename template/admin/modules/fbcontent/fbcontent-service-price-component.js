Vue.component('fbcontent-service-price-component', {
    props: {
        id: { type: Number, default : 0 },
        guid : { type : String, default : () => { return _.uniqueId('fbcontent-service-component') }},
        read_only : { type : Boolean, default : false },
    },

    data: () => ({

        config : {},
        service_package : null,
        quantity : 1,
        price: 0,
        discount_amount : 0,

        additionServices: [],

        formValidate : null,
        isLoading : false,
        readOnly : false , 

        loading: true,
    }),

    watch: {
        service_package () { if( ! _.isEmpty(this.config)) this.price = _.get(this.config, `services.${this.service_package}.price`) },
    },
    created () { 
        this.loadConfig().then(result => {
            if(_.isEmpty(this.additionServices)) this.additionServices = [{ name: '', value: 0}]
            return result
        })
    },
    mounted () { this.bindFormValidate() },
    methods : {

        save () {

            this.loading = true

            const payload = {
                service_package: this.service_package,
                quantity: this.quantity,
                price: this.price,
                discount_amount: this.discount_amount,
                additionServices: this.additionServices
            }

            return axios.put(`${base_url}api-v2/fbcontent/service/index/id/${this.id}`, payload)
            .then(result => {

                const response      = result.data
                const messageType   = 200  == response.code ? 'success' :'error'

                if(_.isEmpty(response.message))
                {
                    $.notify("Xử lý hoàn tất, chưa xác định mã thông báo .", messageType)
                    if(messageType == 'success') setTimeout(function(){ window.location.reload(); }, 1500);
                    return result
                }

                if(_.isString(response.message)) $.notify(response.message, messageType)  
                else _.forEach(response.message, i => { $.notify(i, messageType) })

                if(messageType == 'success') setTimeout(function(){ window.location.reload(); }, 1500);

                return result
            })
            .then( x => {
                this.loading = false
            })
        },

        loadContractData : function(){

            return axios.get(`${admin_url}fbcontent/api/contract/index?` + $.param({ id : this.id, meta : [ 'lock_editable', 'service_package', 'quantity', 'price', 'discount_amount', 'additionServices' ] }))
            .then(result => {

                const _service_package = _.get(result.data.data, 'service_package')
                this.service_package = _.isEmpty(_service_package) ? _.get(this.config, 'default') : _service_package

                const _price = _.get(result.data.data, 'price', 0)
                this.price = _.gt(_price, 0) ? _price : 0

                const _discount_amount = _.get(result.data.data, 'discount_amount', 0)
                this.discount_amount = _.gt(_discount_amount, 0) ? _discount_amount : 0

                const _quantity = _.get(result.data.data, 'quantity', 1)
                this.quantity = _.gt(_quantity, 0) ? _quantity : 1

                const _additionServices = _.get(result.data.data, 'additionServices', [])
                if( ! _.isEmpty(_additionServices)) this.additionServices = _.map( _additionServices, o => {
                    o.value = _.parseInt(o.value)
                    return o
                })

                const _lock_editable = _.parseInt(_.get(result.data.data, 'lock_editable', 0))
                this.lock_editable = new Boolean(_lock_editable)
                this.readOnly = this.lock_editable

                return result
            })
        },

        loadConfig : function(){

            return axios.get(`${admin_url}fbcontent/api/contract/config/id/${this.id}`)
            .then( response => { 
                this.config = response.data.data
                return response
            })
            .then(response => {
                return this.loadContractData();
            })
            .then(response => {
                this.loading = false
            })
        },

        bindFormValidate : function(){
            
            var _self = this;
            this.formValidate = $(`#container-${this.guid}`).closest('form').validate({
                rules : {
                    'meta[quantity]': {
                        digits: true,
                        min : 1,
                    },
                   'meta[discount_amount]': {
                        digits: true,
                        max : function(){ return _self.quantity*_self.price },
                    },
                },
                messages : {
                    'meta[quantity]' : {
                        digits: 'số lượng tài khoản không hợp lệ',
                        min : 'Số lượng tài khoản phải >= 1.'
                    },
                    'meta[discount_amount]' : {
                        digits: 'Số tiền giảm giá không hợp lệ.',
                        max : 'Số tiền giảm giá không được là lớn hơn giá trị dịch vụ',
                    },
                },
            });
        },

        currencyFormat : function(value){
            let val = parseInt(value);
            if( Number.isNaN(val)) return '0 đ';
            return val.toLocaleString() + ' đ';
        },

        /**
         * Adds an addition service.
         */
        addAdditionService : function(){
            this.additionServices.push({name:'',value : ''});
        },

        /**
         * Removes an addition service.
         *
         * @param      {<type>}  index   The index
         */
        removeAdditionService : function(index){
            this.additionServices.splice(index, 1);
        },
    },
    template: `
    <div class="box box-primary" :id="'container-'+ guid">
        <div class="box-header with-border">
            <h3 class="box-title"><i>Thông tin dịch vụ (Gói & giá trị dịch vụ)</i></h3>
        </div>
        <div class="box-body">
            <input style="text-align:right" type="hidden" name="id" :value="id"/>
            <table border="1" cellpadding="2" cellspacing="1" class="table table-responsive table-bordered table-hover">
                <tbody>

                    <tr>
                        <td>Gói dịch vụ</td>
                        <td>Số lượng</td>
                        <td>Đơn giá</td>
                    </tr>

                    <template>
                        <tr>
                            <td>
                                <div v-for="item in config.services" class="checkbox">
                                    <label>
                                        <input type="checkbox" type="radio" class="minimal" name="meta[service_package]" v-model="service_package" v-bind:value="item.name">
                                        {{ item.label }}
                                    </label>
                                </div>
                            </td>
                            <td>
                                <input type="number" name="meta[quantity]" v-model.number="quantity" class="form-control" placeholder="Số tài khoản quảng cáo" align="right">
                            </td>
                            <td>
                                <input type="number" name="meta[price]" v-model.number="price" class="form-control" placeholder="Đơn giá" align="right">
                            </td>
                        </tr>
                    </template>

                    <tr>
                        <td>
                            <label class="control-label"><u>Giảm giá</u></label>
                        </td>
                        <td></td>
                        <td>
                            <input type="number" name="meta[discount_amount]" v-model.number="discount_amount" class="form-control" placeholder="Giá trị giảm">
                        </td>
                    </tr>

                    <tr>
                        <td><b><u><i>Giá trị dịch vụ</i></u></b></td>
                        <td></td>
                        <td align="right"><b v-text="currencyFormat(this.price*this.quantity)"></b></td>
                    </tr>
                    <tr>
                        <td><b><u><i>Chương trình giảm giá</i></u></b></td>
                        <td></td>
                        <td align="right">
                            <b v-text="'-' + currencyFormat(this.discount_amount)"></b>
                        </td>
                    </tr>
                    <tr>
                        <td><b><u><i>Thành tiền</i></u></b></td>
                        <td></td>
                        <td align="right"><b v-text="currencyFormat((this.price*this.quantity)-this.discount_amount)"></b></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div v-if="readOnly == false && read_only == false" class="box-footer">
            <div class="pull-right">
                <button type="button" @click="save()" class="btn btn-primary" :disabled="loading">
                    <template v-if="!loading"><i class="fa fa-pencil"></i> Lưu thay đổi</template>
                    <template v-else>ĐANG XỬ LÝ...</template>
                </button>
            </div>
        </div>
        <div v-if="loading" class="overlay">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
    `
});