Vue.component('contract-component',{
    props: ['options','remote_url'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            selectedTermId : 0,
            url : this.remote_url,
            term_id:0
        }
    },
    computed : {
        current_page_text: function(){
            return this.options.current_page + '/' + this.options.num_page;
        }
    },
    methods : {
        send_email_receipt_payment_paid : function(term_id){
            var btn = $("#send-mail-payment");
            var form = btn.closest('form');

            btn.find('i').attr('class','fa fa-fw fa-refresh fa-spin');
            var result =false;
            $.ajax({
                url : admin_url + 'courseads/ajax/Contract/proc_service/' + term_id,
                data : {'term_id':term_id},
                success : function(response) {

                    btn.find('i').attr('class', 'fa fa-fw fa-send');

                    if(response.status == false)
                    {
                        $.notify(response.msg, 'error');
                        return ;
                    }
                    $.notify(response.msg, 'success');

                },
                type : 'POST',
                async :true
            });
            // return result;
        },
        multi_send_email : function(){
            var seft = this;
            if($("input:checkbox.post_ids:checked").val() ==undefined){
                $.notify('Bạn chưa chọn id phiếu đăng ký', 'error');
                return ;
            }
            $("input:checkbox.post_ids:checked").each(function(){
                var checked = seft.send_email_receipt_payment_paid($(this).val());
                $(this).iCheck('disable').iCheck('check').removeClass('post_ids');

            });
        },
        dataChanged: function(value)
        {
            var seft = this;
            $(".select2").select2({'placeholder':$(this).attr('placeholder')});
            $(".set-datepicker").daterangepicker({format: 'DD-MM-YYYY'});

            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

              //Red color scheme for iCheck
              $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
              });
              //Flat red color scheme for iCheck
              $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
              });
              $("button#send-mail-payment").off('click').on('click',function(){
                  seft.multi_send_email();
              });
              $('#all-checkbox').on('ifChecked', function(e) {
                $('.table .post_ids').iCheck('check');
              });

              $('#all-checkbox').on('ifUnchecked', function(e) {
                $('.table .post_ids').iCheck('uncheck');
              });

              $(':input[name|="notification_of_payment"]').parent().removeClass('col-xs-2').addClass('col-xs-3') ;

        },

    },
    template: `
    <div>
        <datatable-builder-component is_filtering="false" is_ordering="true" per_page="50" :remote_url="url" v-on:data_changed="dataChanged" ></datatable-builder-component>
    </div>
    `
});
