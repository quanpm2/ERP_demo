Vue.component('course-datatable-component', {
    props : {
        courses : { type: Array, default : [] },
        contract_courses : { type: Array, default : [] },
        totalCourses : { type : Number, default : 0 },
        discount_amount : { type : Number, default : 0 }
    },
    computed : {
        total : function(){
            return this.totalCourses - this.discount_amount;
        },
        courseOptions: function(){

            if(this.courses.length == 0) return [];

            let courses = this.courses;
            return courses.map((item, index, courses) => { return {key: item.post_id, value: item.post_title}; });
        },
    },
    created : function(){
        this.initCoursesDefault();
    },
    methods : {

        initCoursesDefault : function(){

            if(this.contract_courses.length <= 0)
            {
                this.addRow();
                return false;
            }

            for (var i = 0; i < this.contract_courses.length; i++)
            {
                this.contract_courses[i].guid = 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);});
                this.contract_courses[i].quantity = parseInt(this.contract_courses[i].quantity);
                this.contract_courses[i].post_id = parseInt(this.contract_courses[i].post_id);
            }
        },

        addRow : function(){
            tmp = {
                post_id : 0,
                quantity : 1,
                value : 0,
                guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            };

            let _contract_courses = this.contract_courses;
            this.contract_courses = [];

            _contract_courses.push(tmp);
            this.contract_courses = _contract_courses;
        },

        removeRow : function(value){

            let _contract_courses = this.contract_courses;

            for (var i = 0; i < _contract_courses.length; i++)
            {
                if(_contract_courses[i].guid != value) continue;
                _contract_courses.splice(i, 1);
            }

            this.contract_courses = [];
            this.contract_courses = _contract_courses;
            this.calcTotal();

            if(_contract_courses.length <= 0) this.addRow();

            return true;
        },

        itemChanged : function(item)
        {
            let _contract_courses   = this.contract_courses;
            let _totalCourses        = 0;

            for (var i = 0; i < _contract_courses.length; i++)
            {
                if(_contract_courses[i].guid != item.guid) continue;

                _contract_courses[i].post_id        = item.course_id;
                _contract_courses[i].course_value   = item.course_value;
                _contract_courses[i].quantity       = item.course_quantity;
            }

            this.contract_courses = _contract_courses;
            this.calcTotal();
        },
        calcTotal : function()
        {
            let _totalCourses = 0;
            for (var i = 0; i < this.contract_courses.length; i++) 
            {
                _totalCourses += this.contract_courses[i].course_value*this.contract_courses[i].quantity;
            }

            this.totalCourses = _totalCourses;
        }
    },
    template: `
    <table border="1" cellpadding="2" cellspacing="1" class="table table-responsive table-bordered table-hover">
        <caption>Chi tiết đăng ký khóa học</caption>
        <thead>
            <tr> <th>Khóa học</th><th>Đơn giá</th><th>Số lượng</th><th>Thành tiền</th></tr>
        </thead>
        <tbody>
            <tr v-for="(item, index) in contract_courses" 
                is="course-row-component"
                :courses="courses" 
                :course="item"
                :show_add_btn="index == (contract_courses.length-1)"
                :show_delete_btn="true"
                v-on:update_item="itemChanged"
                v-on:remove_row="removeRow"
                v-on:add_new_row="addRow">
            </tr>
            <tr>
                <td><b>Giá trị giảm giá</b></td>
                <td><input type="number" name="meta[discount_amount]" v-model="discount_amount" class="form-control"></td>
                <td></td>
                <td v-html="(new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(discount_amount))"></td>
            </tr>
            <tr>
                <td colspan="3"><b>Thành tiền</b></td>
                <td v-html="(new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(total))"></td>
            </tr>
        </tbody>
    </table>
    `
}); 