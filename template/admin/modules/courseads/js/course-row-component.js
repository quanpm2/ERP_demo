Vue.component('course-row-component', {
    props: {
    	courses: {
			type: Array,
            default : []
        },
        course: {
            type: Object,
			default : null
        },
		course_id : {
			type : Number,
			default : 0,
		},
		course_value : {
			type : Number,
			default : 0,
		},
		course_quantity : {
			type : Number,
			default : 1,
		},
		total : {
			type : Number,
			default : '',
		},
        show_add_btn : {
            type : Boolean,
            default : false,
        },
        show_delete_btn : {
            type : Boolean,
            default : false,
        }
    },
    watch:{
    	course_value : function(){
    		this.total = this.course_value*this.course_quantity;
    	},
    	course_quantity : function(){
    		this.total = this.course_value*this.course_quantity;
    	},
    	total : function(){
            
            let item = {
    			guid : this.course.guid,
    			course_id : this.course_id,
    			course_value : this.course_value,
    			course_quantity : this.course_quantity
    		};

    		this.$emit('update_item',item);
    	},
    },
    computed : {
    	total_f : function(){

    		return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(this.total));
    	},
    	courseOptions: function(){

            if(this.courses.length == 0) return [];

            let courses = this.courses;
            return courses.map((item, index, courses) => { return {key: item.post_id, value: item.post_title}; });
        },
    },
    created : function(){
        this.mappingCourse();
    },
    methods : {
        mappingCourse : function(){

            if(this.course == null) return;

            this.course_id          = this.course.post_id;
            this.course_value       = this.course.course_value;
            this.course_quantity    = this.course.quantity;
        },
    	course_changed: function(value){
            this.course_id = value;

            for (var i = 0; i < this.courses.length; i++)
            {
            	if(this.courses[i].post_id != this.course_id) continue;            	
            	this.course_value = this.courses[i].course_value;
            }
        }
    },
    template: `
    <tr>
    	<td><select2-component name="meta[courses][post_id][]" :value="course.post_id" :options="courseOptions" v-on:on_changed="course_changed"></select2-component></td>
    	<td><input type="number" name="meta[courses][course_value][]" v-model.number="course_value" :value="course.course_value" class="form-control" placeholder="Giá trị khóa học"></td>
    	<td><input type="number" name="meta[courses][quantity][]" v-model.number="course_quantity" :value="course.quantity" class="form-control" placeholder="Số lượng"></td>
    	<td v-text="total_f"></td>
        <td v-if="this.courses.length > 1">
            <button v-on:click="$emit('remove_row', course.guid)" type="button" class="btn btn-block btn-danger btn-flat"><i class="fa fa-close"></i></button>
        </td>
        <td v-if="this.courses.length > 1">
            <button v-on:click="$emit('add_new_row')" v-if="show_add_btn == true" type="button" class="btn btn-block btn-success btn-flat"><i class="fa fa-plus"></i></button>
        </td>
    </tr>`
});