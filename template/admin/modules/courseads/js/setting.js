Vue.component('proc-service-component', {
    props: ['begin_date','term_id','isHidden'],
    data: function(){
        return {
            array_package:null,
            admin_url: admin_url,
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            isLoading : false,

        };
    },
    watch : {
        service_packages : function(){
            //console.log(this.service_package);
        },
        array_package:function(){
           //console.log(this.array_package);
        }
    },
    computed: {

    },
    created: function(){
    },

    methods: {
        proc_service_submit: function(){
            this.isLoading = true;
            var seft = this;

            $.ajax({
                type: 'POST',
                url: admin_url + 'courseads/ajax/contract/proc_service/'+seft.term_id,
                success: function(response){

                	console.log(response);

                    $.notify(response.msg, {className: (response.status ? 'success' : 'error'), clickToHide: true, autoHideDelay: 15000,});

                    this.isLoading = false;

                    if(response.status)
                    {
                        this.isHidden = true;
                        $('#modal-'+ seft.guid).modal('hide');
                        this.isHidden = true;
                    }
                },
                dataType: 'json'
            });
        },
        service_package_updated: function(value){
            this.service_package = value;
        },
        dataSubmit:function(value){
            var packge = this;
            packge.array_package = value;
        },
        reloadCalendarUpdated:function(e){
            this.reloadCalendar = true;
        },

    },
    template: `
    <div class="col-md-12 box box-theme box-body">
        <div v-if=" ! isHidden" :id="'container-' + guid" class="col-md-2">
            <div class="row">
                <button :id="'btn-' + guid" type="button" v-on:click="proc_service_submit" class="btn btn-default" data-toggle="modal" :data-target="'#modal-' + guid">
                    <template  v-if="!isLoading"><i class="fa fa-fw fa-play"></i> Kích hoạt </template>
                    <template v-else><i class="fa fa-spinner fa-pulse fa-check"></i> Đã kích hoạt</template>
                </button>
            </div>
        </div>

    </div>
    `
});
