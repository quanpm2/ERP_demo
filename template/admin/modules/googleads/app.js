/* Loading thông số bảo lãnh */
Vue.component('receiptcaution-quota-component',{
    props: ['options'],
    computed : {
        current_page_text: function(){
            return this.options.current_page + '/' + this.options.num_page;
        }
    },
    methods : {
        updateCurrentPage : function(value){
            this.$emit('update_current_page',value);
        }
    },
    template: `
    <ul v-if="options" class="pagination pagination-sm pull-right">
        <li v-if="options.prev_page_url">
            <a href="#" v-on:click="updateCurrentPage(options.prev_page_url)"><i class="fa fa-fw fa-chevron-left"></i></a>
        </li>
        <li><a v-text="current_page_text"></a></li>
        <li v-if="options.next_page_url"><a href="#" v-on:click="updateCurrentPage(options.next_page_url)"><i class="fa fa-fw fa-chevron-right"></a></li>
    </ul>
    `
});

var app_root = new Vue({
    el: '#app-container'
});