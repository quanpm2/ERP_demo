Vue.component('checkbox-component', {
    props : ['name','value','picked','text'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
        }
    },
    mounted: function() {

        $('#'+this.guid+'.minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass   : 'iradio_minimal-red'
        })
    },
    template: `
    <div class="checkbox">
        <label>
            <input :id="guid" class="minimal" type="checkbox" :value="value" :name="name" v-model="picked"> 
            <span v-html="text"></span>
        </label>
    </div>
    `
});

Vue.component('radio-component', {
    props : ['name','value','picked','text'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
        }
    },
    mounted: function() {
        $('#'+this.guid+'.minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass   : 'iradio_minimal-blue'
        })
    },
    template:`
    <div class="radio">
        <label>
            <input :id="guid" class="minimal" type="radio" :value="value" :name="name" v-model="picked"> 
            <span v-html="text"></span>
        </label>
    </div>
    `
});

Vue.component('input-component', {
    props : ['type','name','label','value','help','suffixAddon','prefixAddon'],
    created: function() {
            
    },
    computed: {
        hasAddon: function(){
            if(this.suffixAddon !== undefined || this.prefixAddon !== undefined) 
                return true;
            return false;    
        }
    },
    template:`
    <div class="form-group">

        <label for="inputEmail3" class="col-sm-4 control-label" v-text="label"></label>

        <template v-if="hasAddon">
            <div class="input-group" >

                <div v-if="prefixAddon !== undefined" class="input-group-addon"><i :class="prefixAddon"></i></div>

                <input v-if="type=='text'" type="text" class="form-control" :name="name" v-model.trim="value" />
                <input v-else-if="type=='number'" type="number" class="form-control" :name="name" v-model.number="value" />

                <div v-if="suffixAddon !== undefined" class="input-group-addon"><i :class="suffixAddon"></i></div>
            </div>
        </template>
        <template v-else>
            <div class="col-sm-8">
                <input v-if="type=='text'" type="text" class="form-control" :name="name" v-model.trim="value" />
                <input v-if="type=='number'" type="number" class="form-control" :name="name" v-model.number="value" />
            </div>
        </template>
    </div>
    `
});