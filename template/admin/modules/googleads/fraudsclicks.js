Vue.component('adword-tracking-integrated-component', {
    props: [],
    data : function(){
        return {
            campaigns: [],
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            clientTrackingJs : '<script type="text/javascript" src="//unpkg.com/axios/dist/axios.min.js" async="async"></script>',
            hintLabel : '<i class="fa fa-buysellads"></i>Tích hợp Tracking Code lên dịch vụ quảng cáo Adwords',
            modalOptions: {
                title: '<i class="fa fa-plug"></i> TÍCH HỢP TRACKING CODE'
            },
            url_get_campaigns: jsobjectdata.url_get_campaigns,
            url_request_campaigns: jsobjectdata.url_request_campaigns,
            url_mutate_trackingtemplateurl: jsobjectdata.url_mutate_trackingtemplateurl,
            term_id: jsobjectdata.term_id,

            campaignsRefreshState : false,
            setTrackingTemplateState : false,
        }
    },
    mounted: function(){
        // $("#container-"+this.guid+" a.btn-google").trigger('click');
    },
    created: function(){
        this.loadCampaigns();
    },
    methods: {

        campaignsRefresh: function(){

            this.campaignsRefreshState = true;
            var seft = this;
            axios
            .get(this.url_request_campaigns, {
                params: {
                    term_id: this.term_id
                }
            })
            .then(function (response) {

                if(response.data.status == false)
                {
                    $.notify(response.data.msg, "warning");
                    return;
                }

                $.notify(response.data.msg, "info");

                $.notify("Đang tiến hành tải dữ liệu chiến dịch ...", "info");
                seft.loadCampaigns();
                
                seft.campaignsRefreshState = false;

                return true;
            });
        },
        trackingTemplateSubmit : function()
        {
            this.setTrackingTemplateState = true;
            if(this.campaigns.length == 0)
            {
                $.notify("Không tìm thấy chiến dịch ! Load tất cả chiến dịch mới trước khi thực hiện tác vụ này",'warning');
                this.setTrackingTemplateState = false;
                return true;
            }

            var seft = this;
            var i = 0;
            for (i; i < this.campaigns.length; i++)
            {
                if(this.campaigns[i].trackingUrlTemplate != null) continue;

                if( ! $.inArray(this.campaigns[i].trackingUrlTemplate,["enabled"])) continue;
                // if( ! $.inArray(this.campaigns[i].trackingUrlTemplate,["pending","eligible","paused"])) continue;

                this.setTrackingTemplateState = true;

                this.campaigns[i].trackingUrlTemplate = 'loading';
                this.callMutateTrackingTemplateUrl(i,this.campaigns[i]);

            }
            this.setTrackingTemplateState = false;
        },
        callMutateTrackingTemplateUrl: function(index,campaign)
        {
            var seft = this;
            axios
            .get(this.url_mutate_trackingtemplateurl, { params: { term_id: this.term_id , adcampaign_id: campaign.post_id} })
            .then(function (response) {

                if(response.data.status == false)
                {
                    $.notify(response.data.msg, "warning");
                    seft.campaigns[index].trackingUrlTemplate = null;
                    return;
                }
                seft.campaigns[index].trackingUrlTemplate = response.data.data.trackingUrlTemplate;
                return true;
            });
        },
        loadCampaigns: function(){
            var seft = this;

            axios
            .get(this.url_get_campaigns, {
                params: {
                    term_id: this.term_id
                }
            })
            .then(function (response) {

                if(response.data.status == false)
                {
                    $.notify(response.data.msg, "warning");
                    return;
                }
                
                seft.campaigns = response.data.data;
                return true;
            });
        }
    },
    template: `
    <div :id="'container-' + guid">
        <a class="btn btn-block btn-social btn-google" data-toggle="modal" v-html="hintLabel" :data-target="'#modal-'+guid"></a>
       
        <!-- adword-tracking-integrated-component modal -->
        <div class="modal fade" :id="'modal-'+guid">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" v-html="modalOptions.title"></h4>
                    </div>
                    <div class="modal-body">

                        <p><b>Bước 1:</b> Tích hợp Tracking Template URL cho tất cả các Campaigns</p>
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Danh sách chiến dịch</h3>

                                <div class="box-tools">
                                    <ul class="pagination pagination-sm no-margin pull-right">

                                        <li v-if="!campaignsRefreshState">
                                            <a href="#" v-on:click="campaignsRefresh" ><i class="fa fa-refresh"></i> Chiến dịch</a>
                                        </li>
                                        <li v-else>
                                            <a href="#" class="disabled"><i class="fa fa-spin fa-refresh"></i> Đang lấy dữ liệu...</a>
                                        </li>

                                        <template v-if="campaigns.length > 0">
                                            <li v-if="!setTrackingTemplateState">
                                                <a href="#" v-on:click="trackingTemplateSubmit"><i class="fa fa-cog"></i> Tích hợp Tracking Code</a>
                                            </li>
                                            <li v-else>
                                                <a href="#" class="disabled"><i class="fa fa-spin fa-cog"></i> Đang tích hợp Tracking...</a>
                                            </li>
                                        </template>
                                    </ul>
                                </div>
                            </div>
                            <div class="box-body">
                                <template v-if="campaigns.length > 0">
                                    <table class="table table-striped">
                                        <tr>
                                            <th>#</th>
                                            <th>Chiến dịch</th>
                                            <th>Trạng thái</th>
                                            <th style="width: 40px">Tracking Template</th>
                                        </tr>
                                        <tr v-for="(campaign,index) in campaigns">
                                            <td v-text="index+1"></td>
                                            <td v-text="campaign.post_name"></td>
                                            <td v-text="campaign.post_status"></td>
                                            <td>
                                                <span v-if="campaign.trackingUrlTemplate == 'loading'" class="label label-warning">Đang xử lý...</span>
                                                <span v-else-if="campaign.trackingUrlTemplate == null" class="label label-default">chưa cấu hình</span>
                                                <span v-else class="label label-success">Hoàn thành</span>
                                            </td>
                                        </tr>
                                    </table>
                                <template>
                                <template v-else>
                                    <p>Không tìm thấy bất kỳ chiến dịch nào trong tài khoản (CLICK LOAD CHIẾN DỊCH MỚI)</p>
                                </template>
                            </div>
                        </div>

                        <p><b>Bước 2:</b> Coppy/Paste đoạn script sau vào website đang chạy quảng cáo</p>
                        <p><pre v-text="clientTrackingJs"></pre></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
});