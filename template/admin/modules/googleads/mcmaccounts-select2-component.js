Vue.component('mcmaccounts-select2-component',{
    props: ['name','label','value','url'],
    data: function(){
        return {
            options : [],
            guid: _.uniqueId('mcmaccounts-select2-'),
        };
    },
    computed : {
    },
    updated: function(){
        $('#'+this.guid).css('width', '100%');
        $('#'+this.guid).select2();
        $('#'+this.guid).removeClass('form-control');
    },
    created: function() {
        this.loadDataSource();
    },
    methods : {

        loadDataSource: function(){
            var seft = this;

            axios
            .get(this.url)
            .then(function (response) {
                var _data = response.data.data;
                seft.options = _data;
            });   
        },

    },
    template: `
    <div class="input-group form-group">
        <span class="input-group-addon" v-html="label"></span>
        <select :name="name" :id="guid" class="form-control">
            <option value="0">Chưa cấu hình</option>
            <template v-if="options.length > 0">
                <option v-for="item in options" :selected="item.term_id == value ? 'selected' : ''"  :value="item.term_id">{{ item.term_name }} - {{ item.account_name }}</option>
            </template>
        </select>
        <span class="input-group-addon"><input type="submit" name="authentization" value="Reload" class="btn btn-xs btn-danger"  />
        </span>
    </div>
    <br />
    `
});