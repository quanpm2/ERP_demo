Vue.component('ip-overview-component', {
    props:['visitors','guid'],
    data: function(){
        return {
            ips : [],
            title: 'Thống kê IP hợp lệ - đã chặn',
            subTitle : 'Tracking sys',
        };
    },
    watch: {
        visitors: function(){
            this.drawChart();
        }
    },
    methods: {

        loadChartData: function(){
        },

        drawChart : function(){

            var visitorsGrouped = [];
            var daysSeries = [];
            var numIpSeries = [];
            var numBlockedIpSeries = [];
            var numUnblockedIpSeries = [];

            this.visitors.map(function(element, index){

                // Check day has been grouped
                if( ! visitorsGrouped.hasOwnProperty(element.day)){
                    visitorsGrouped[element.day] = [];
                }

                visitorsGrouped[element.day].push(element);
            });

            daysSeries = Object.keys(visitorsGrouped);
            daysSeries.map(function(element){

                var numIp = $.unique(visitorsGrouped[element].map( x => x.ip)).length;
                var numBlockedIp = $.unique(visitorsGrouped[element].filter(x => x.status == 'blocked').map( x => x.ip)).length;
                var numUnlockedIp = numIp - numBlockedIp;

                numIpSeries.push(numIp);
                numBlockedIpSeries.push(numBlockedIp);
                numUnblockedIpSeries.push(numUnlockedIp);
            });

            var chartOptions = {
                chart: { zoomType: 'xy' },
                title: { text: this.title },
                subtitle: { text: this.subTitle},
                xAxis: [{ categories: daysSeries, crosshair: true }],
                yAxis: [
                    {
                        labels: { format: '{value} IP', style: { color: Highcharts.getOptions().colors[2] } },
                        title: { text: 'Tổng IP truy cập', style: { color: Highcharts.getOptions().colors[2] } },
                        opposite: true
                    },
                    {
                        gridLineWidth: 0,
                        title: { text: 'Tổng IP bị khóa', style: { color: Highcharts.getOptions().colors[0] } },
                        labels: { format: '{value} IP', style: { color: Highcharts.getOptions().colors[0] }}
                    },
                    {
                        gridLineWidth: 0,
                        title: { text: 'Tổng IP hợp lệ', style: { color: Highcharts.getOptions().colors[1] } },
                        labels: { format: '{value} IP', style: { color: Highcharts.getOptions().colors[1] } },
                        opposite: true
                    }
                ],
                tooltip: {
                    shared: true
                },
                legend: { layout: 'vertical', align: 'left', x: 80, verticalAlign: 'top', y: 55, floating: true, backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF' },
                series: [{
                        name: 'Tổng IP truy cập', type: 'column', yAxis: 1, 
                        data: numIpSeries, tooltip: { valueSuffix: 'IP' } 
                    }, {
                        name: 'Tổng IP đã bị khóa',
                        type: 'spline',
                        yAxis: 2,
                        data: numBlockedIpSeries,
                        marker: {
                            enabled: false
                        },
                        dashStyle: 'shortdot',
                        tooltip: {
                            valueSuffix: ' IP'
                        }

                    }, {
                        name: 'Tổng IP hợp lệ',
                        type: 'spline',
                        data: numUnblockedIpSeries,
                        tooltip: {
                            valueSuffix: ' IP'
                        }
                    }
                ]
            };

            var chart_container_id = 'chart-container-'+this.guid;
            var chart = new Highcharts.chart(chart_container_id, chartOptions);
        }
    },
    template:`
    <div class="col-md-12">
        <div :id="'chart-container-'+guid" ></div>
    </div>
    `
});

Vue.component('ip-tracking-component', {
    props: ['visitors','guid'],
    data: function(){
        return {
            ips : [],
            title: 'Thống kê IP truy cập',
            headings: ['Ngày','IP','Clicks','Device','OS','Browser','Risk']
        };
    },
    methods : { unixToDatetime: function(unix) { return moment.unix(unix).format("DD-MM-YYYY HH:MM:SS"); }, },
    template:`
    <div class="col-md-12" :id="'iptracking-container-'+guid">
        <table class="table table-hover">
            <thead>
                <tr><th v-for="heading in headings" v-text="heading"></th></tr>
            </thead>
            <tbody>
                <tr v-if="visitors.length == 0">
                    <td :colspan="headings.length" align="center">Chưa có dữ liệu</td>
                </tr>
                <template v-else>
                    <tr v-for="visitor in visitors">
                        <td v-text="unixToDatetime(visitor.created_on)"></td>
                        <td v-text="visitor.ip"></td>
                        <td v-text="visitor.device"></td>
                        <td v-text="visitor.platform"></td>
                        <td v-text="visitor.browser"></td>
                        <td>0</td>
                    </tr>
                </template>
            </tbody>
        </table>
    </div>
    `
});

Vue.component('ip-blocked-component', {
    props: ['guid'],
    data: function(){
        return {
            ipBLockeds : [],
            title: 'Thống kê IP đã chặn',
            headings: ['Trạng thái','IP','Bắt đầu khóa','Kết thúc khóa'],
            urlgetIPBlockedLogs: jsobjectdata.url_get_blocked_logs,
            term_id : jsobjectdata.term_id,
        };
    },
    created: function(){
        this.getIPBlockedLogs();
    },
    methods : {
        removeIPBlocked: function(ipBlocked)
        {
            // remove ip blocked codeing here
            return true;
        },
        getIPBlockedLogs : function(){
            var seft = this;
            axios
            .get(this.urlgetIPBlockedLogs, { params: { term_id: this.term_id } })
            .then(function (response) {

                if(response.data.status == false)
                {
                    $.notify(response.data.msg, "warning");
                    return;
                }

                seft.ipBLockeds = response.data.data;
                return true;  
            });
        },
        unixToDatetime: function(unix)
        {
            return moment.unix(unix).format("DD-MM-YYYY HH:MM:SS");
        },
        status_text: function(ipBlocked){

            if(ipBlocked.end_time < moment.unix()) return 'Hết hiệu lực';

            var result = '';

            switch(ipBlocked.status) {
                case 'ending':
                    result = 'Đã hủy chặn'    
                    break;
                default:
                    result = 'Đang chặn'
            }

            return result;
        }
    },
    template:`
    <div class="col-md-12" :id="'iptracking-container-'+guid">
        <table class="table table-hover">
            <thead>
                <tr><th v-for="heading in headings" v-text="heading"></th></tr>
            </thead>
            <tbody>
                <tr v-if="ipBLockeds.length == 0">
                    <td :colspan="headings.length" align="center">Chưa có dữ liệu</td>
                </tr>
                <template v-else>
                    <tr v-for="ipBlocked in ipBLockeds">
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-default" v-html="status_text(ipBlocked)"></button>

                                <button :click="removeIPBlocked(ipBlocked)" v-if="ipBlocked.status == 'publish'" type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fw fa-stop" ></i>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                            </div>
                        </td>
                        <td v-text="ipBlocked.ip"></td>
                        <td v-text="unixToDatetime(ipBlocked.start_time)"></td>
                        <td v-text="unixToDatetime(ipBlocked.end_time)"></td>
                    </tr>
                </template>
            </tbody>
        </table>
    </div>
    `
});

Vue.component('rule-configuration-component', {
    props : ['guid','rules'],
    data: function(){
        return {
            title: 'Rule cảnh báo theo hệ thống',
            urlGetConfRules : jsobjectdata.url_get_rule_conf,
            urlUpdateConfRules: jsobjectdata.url_update_rule_conf,
            term_id: jsobjectdata.term_id,
        };
    },
    created: function() {
        this.getRuleConf();
    },
    methods: {
        secondsToMinutes: function(seconds){
            return seconds/60;
        },
        getRuleConf: function(){

            var seft = this;
            axios
            .get(this.urlGetConfRules, { params: { term_id: this.term_id } })
            .then(function (response) {

                if(response.data.status == false)
                {
                    $.notify(response.data.msg, "warning");
                    return;
                }

                seft.rules = response.data.data;
                return true;  
            });
        },
        updateRuleConf: function(){

            var seft = this;
            var postdata = $('#rule-frm-'+this.guid).serializeArray();

            $.post( this.urlUpdateConfRules + '/' + this.term_id, postdata, function(response){

                if(!response.status)
                {
                    $.notify(response.msg, "error");
                    return false;
                }

                $.notify(response.msg, "success");

            }, "json");
        },
    },
    template:`
    <div class="row">

        <div class="col-md-2 col-xs-12 pull-right">
            <button v-on:click="updateRuleConf" type="button" class="btn btn-block btn-primary"> <i class="fa  fa-floppy-o"></i> Cập nhật </button>
        </div>

        <adword-tracking-integrated-component />

        <div class="clearfix"></div>
        <br/>

        <form :id="'rule-frm-'+guid" method="POST" v-if="rules !== null">

            <div class="col-md-6">
                <!-- Horizontal Form -->
                <div class="box box-info">

                    <div class="box-header">
                        <h3 class="box-title">
                            <checkbox-component name="rules[ad][active]" :value="true" :picked="rules.ad.active" text="Chặn theo lượt truy cập từ 'Mẫu quảng cáo' của IP" />
                        </h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <div class="box-body">

                        <input-component 
                            :value="rules.ad.max_clicks"
                            label="Số lần truy cập tối đa" 
                            type="number" 
                            suffixAddon="fa fa-laptop"
                            name="rules[ad][max_clicks]" />

                        <input-component 
                            :value="secondsToMinutes(rules.ad.durations)"
                            label="Trong khoảng (đvt:phút)" 
                            type="number" 
                            suffixAddon="fa fa-clock-o"
                            name="rules[ad][durations]" />
                    </div>
                </div>
                <!-- /.box -->

                <!-- Horizontal Form -->
                <div class="box">

                    <div class="box-header">
                        <h3 class="box-title"><checkbox-component name="rules[adgroup][active]" :value="true" :picked="rules.adgroup.active" text="Chặn theo lượt truy cập từ 'Nhóm quảng cáo' của IP" /></h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <div class="box-body">

                        <input-component 
                            :value="rules.adgroup.max_clicks"
                            label="Số lần truy cập tối đa" 
                            type="number" 
                            suffixAddon="fa fa-laptop"
                            name="rules[adgroup][max_clicks]"/>

                        <input-component 
                            :value="secondsToMinutes(rules.adgroup.durations)"
                            label="Trong khoảng (đvt:phút)" 
                            type="number" 
                            suffixAddon="fa fa-clock-o"
                            name="rules[adgroup][durations]"/>  
                    </div>
                </div>
                <!-- /.box -->

                <!-- Horizontal Form -->
                <div class="box box-success">

                    <div class="box-header">
                        <h3 class="box-title"><checkbox-component name="rules[adcampaign][active]" :value="true" :picked="rules.adcampaign.active" text="Chặn theo lượt truy cập từ 'Chiến dịch quảng cáo' của IP" /></h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <div class="box-body">

                        <input-component 
                            :value="rules.adcampaign.max_clicks"
                            label="Số lần truy cập tối đa" 
                            type="number" 
                            suffixAddon="fa fa-laptop"
                            name="rules[adcampaign][max_clicks]"/>
                            
                        <input-component 
                            :value="secondsToMinutes(rules.adcampaign.durations)"
                            label="Trong khoảng (đvt:phút)" 
                            type="number" 
                            suffixAddon="fa fa-clock-o"
                            name="rules[adcampaign][durations]"/>  
                    </div>
                </div>
                <!-- /.box -->

                <!-- Horizontal Form -->
                <div class="box box box-primary">

                    <div class="box-header">
                        <h3 class="box-title"><checkbox-component name="rules[ip_series_p3][active]" :value="true" :picked="rules.ip_series_p3.active" text="Chặn theo Dãy IP (xxx.xxx.xxx.*)" /> </h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <div class="box-body">

                        <input-component 
                            :value="rules.ip_series_p3.max_clicks"
                            label="Số lần truy cập tối đa" 
                            type="number" 
                            suffixAddon="fa fa-laptop"
                            name="rules[ip_series_p3][max_clicks]"/>
                            
                        <input-component 
                            :value="secondsToMinutes(rules.ip_series_p3.durations)"
                            label="Trong khoảng (đvt:phút)" 
                            type="number" 
                            suffixAddon="fa fa-clock-o"
                            name="rules[ip_series_p3][durations]"/>  
                    </div>
                </div>
                <!-- /.box -->

                <!-- Horizontal Form -->
                <div class="box box-info">

                    <div class="box-header">
                        <h3 class="box-title"><checkbox-component name="rules[ip_series_p2][active]" :value="true" :picked="rules.ip_series_p2.active" text="Chặn theo Dãy IP (xxx.xxx.*)" /> </h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <div class="box-body">

                        <input-component 
                            :value="rules.ip_series_p2.max_clicks"
                            label="Số lần truy cập tối đa" 
                            type="number" 
                            suffixAddon="fa fa-laptop"
                            name="rules[ip_series_p2][max_clicks]"/>
                            
                        <input-component 
                            :value="secondsToMinutes(rules.ip_series_p2.durations)"
                            label="Trong khoảng (đvt:phút)" 
                            type="number" 
                            suffixAddon="fa fa-clock-o"
                            name="rules[ip_series_p2][durations]"/>  
                    </div>
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-6">

                <!-- Horizontal Form -->
                <div class="box box-primary">

                    <div class="box-header">
                        <h3 class="box-title"><checkbox-component name="rules[behavior][active]" :value="true" :picked="rules.behavior.active" text="Chặn theo hành vi" /> </h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <div class="box-body">

                        <input-component 
                            :value="secondsToMinutes(rules.behavior.min_time_on_site)"
                            label="Time on page tối thiểu" 
                            type="number" 
                            suffixAddon="fa fa-circle-o-notch"
                            name="rules[behavior][min_time_on_site]"/>

                        <input-component 
                            :value="rules.behavior.max_sessions"
                            label="Số lần truy cập tối đa" 
                            type="number" 
                            suffixAddon="fa fa-laptop"
                            name="rules[behavior][max_sessions]"/>

                        <input-component 
                            :value="secondsToMinutes(rules.behavior.durations)"
                            label="Trong khoảng (đvt:phút)" 
                            type="number" 
                            suffixAddon="fa fa-clock-o"
                            name="rules[behavior][durations]"/>
                  </div>
                </div>
                <!-- /.box -->

                <!-- Horizontal Form -->
                <div class="box box-info">

                    <div class="box-header">
                        <h3 class="box-title">
                            <checkbox-component name="rules[devices][active]" :value="true" :picked="rules.devices.active" text="Chặn theo thiết bị" />
                        </h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <div class="box-body">
                        <!-- radio -->
                        <div class="form-group">

                            <checkbox-component :value="true" :picked="rules.devices.browser_olds" name="rules[devices][browser_olds]" text="Khi người dùng <b><u>không sử dụng trình duyệt</u></b>"/>
                            <checkbox-component :value="true" :picked="rules.devices.browser_undetected" name="rules[devices][browser_undetected]" text="Khi sử dụng <b><u>trình duyệt có version quá cũ</u></b>"/>
                            <checkbox-component :value="true" :picked="rules.devices.mouse_undetected" name="rules[devices][mouse_undetected]" text="Khi <b><u>không sử dụng chuột và phím</u></b>"/>
                            <checkbox-component :value="true" :picked="rules.devices.screen_undetected" name="rules[devices][screen_undetected]" text="Khi <b><u>có độ phân giải màn hình computer bất thường</u></b>"/>
                            <checkbox-component :value="true" :picked="rules.devices.os_undetected" name="rules[devices][os_undetected]" text="Khi <b><u>Hệ điều hành thuộc phạm vi bất thường đối với một người dùng bình thường (Gnu, Unknown Unix OS, symbian, irix, aix...)</u></b>"/>
                        </div>
                    </div>
                </div>
                <!-- /.box -->

                <!-- Horizontal Form -->
                <div class="box box-danger">

                    <div class="box-header">
                        <h3 class="box-title">Thời gian phục hồi hiệu lực các IP đã bị chặn</h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <div class="box-body">
                        <radio-component :picked="rules.recovery" value="noexpired" name="rules[recovery]" text="Chặn vĩnh viễn"/>
                        <radio-component :picked="rules.recovery" value="finish" name="rules[recovery]" text="Sau khi kết thúc hợp đồng dịch vụ"/>
                        <radio-component :picked="rules.recovery" value="1" name="rules[recovery]" text="Sau 1 ngày <i>( 00:00AM sẽ tự động phục hồi )</i>"/>
                        <radio-component :picked="rules.recovery" value="2" name="rules[recovery]" text="Sau 2 ngày <i>( 00:00AM sẽ tự động phục hồi )</i>"/>
                        <radio-component :picked="rules.recovery" value="7" name="rules[recovery]" text="Sau 1 tuần <i>( 00:00AM sẽ tự động phục hồi )</i>"/>
                        <radio-component :picked="rules.recovery" value="60" name="rules[recovery]" text="Sau 2 tháng <i>( 00:00AM sẽ tự động phục hồi )</i>"/>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </form>
    </div>
    `
});

Vue.component('adword-tracking-integrated-component', {
    props: [],
    data : function(){
        return {
            campaigns: [],
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            clientTrackingJs : '<script type="text/javascript" src="//unpkg.com/axios/dist/axios.min.js" async="async"></script>',
            hintLabel : '<i class="fa fa-buysellads"></i>Tích hợp Tracking Code lên dịch vụ quảng cáo Adwords',
            modalOptions: {
                title: '<i class="fa fa-plug"></i> TÍCH HỢP TRACKING CODE'
            },
            term_id: jsobjectdata.term_id,
            url_get_campaigns: jsobjectdata.url_get_campaigns,
            url_request_campaigns: jsobjectdata.url_request_campaigns,
            url_mutate_trackingtemplateurl: jsobjectdata.url_mutate_trackingtemplateurl,
            campaignsRefreshState : false,
            setTrackingTemplateState : false,
        }
    },
    methods: {

        campaignsRefresh: function(){

            this.campaignsRefreshState = true;
            var seft = this;
            axios
            .get(this.url_request_campaigns, {
                params: {
                    term_id: this.term_id
                }
            })
            .then(function (response) {

                if(response.data.status == false)
                {
                    $.notify(response.data.msg, "warning");
                    return;
                }

                $.notify(response.data.msg, "info");

                $.notify("Đang tiến hành tải dữ liệu chiến dịch ...", "info");
                seft.loadCampaigns();
                
                seft.campaignsRefreshState = false;

                return true;
            });
        },
        trackingTemplateSubmit : function()
        {
            this.setTrackingTemplateState = true;
            if(this.campaigns.length == 0)
            {
                $.notify("Không tìm thấy chiến dịch ! Load tất cả chiến dịch mới trước khi thực hiện tác vụ này",'warning');
                this.setTrackingTemplateState = false;
                return true;
            }

            var seft = this;
            var i = 0;
            for (i; i < this.campaigns.length; i++)
            {
                if(this.campaigns[i].trackingUrlTemplate != null) continue;

                if( ! $.inArray(this.campaigns[i].trackingUrlTemplate,["enabled"])) continue;
                // if( ! $.inArray(this.campaigns[i].trackingUrlTemplate,["pending","eligible","paused"])) continue;

                this.setTrackingTemplateState = true;

                this.campaigns[i].trackingUrlTemplate = 'loading';
                this.callMutateTrackingTemplateUrl(i,this.campaigns[i]);

            }
            this.setTrackingTemplateState = false;
        },
        callMutateTrackingTemplateUrl: function(index,campaign)
        {
            var seft = this;
            axios
            .get(this.url_mutate_trackingtemplateurl, { params: { term_id: this.term_id , adcampaign_id: campaign.post_id} })
            .then(function (response) {

                if(response.data.status == false)
                {
                    $.notify(response.data.msg, "warning");
                    seft.campaigns[index].trackingUrlTemplate = null;
                    return;
                }
                seft.campaigns[index].trackingUrlTemplate = response.data.data.trackingUrlTemplate;
                return true;
            });
        },
        loadCampaigns: function(){
            var seft = this;

            axios
            .get(this.url_get_campaigns, {
                params: {
                    term_id: this.term_id
                }
            })
            .then(function (response) {

                if(response.data.status == false)
                {
                    $.notify(response.data.msg, "warning");
                    return;
                }
                
                seft.campaigns = response.data.data;
                return true;
            });
        },
        bindTermId: function(){

            if(this.campaigns.length == 0)
            {
                this.loadCampaigns();
            }
        }
    },
    template: `
    <div class="col-md-5" :id="' container-' + guid">
        <a v-on:click="bindTermId" class="btn btn-block btn-social btn-google" data-toggle="modal" v-html="hintLabel" :data-target="'#modal-'+guid"></a>
       
        <!-- adword-tracking-integrated-component modal -->
        <div class="modal fade" :id="'modal-'+guid">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" v-html="modalOptions.title"></h4>
                    </div>
                    <div class="modal-body">

                        <p><b>Bước 1:</b> Tích hợp Tracking Template URL cho tất cả các Campaigns</p>
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Danh sách chiến dịch</h3>

                                <div class="box-tools">
                                    <ul class="pagination pagination-sm no-margin pull-right">

                                        <li v-if="!campaignsRefreshState">
                                            <a href="#" v-on:click="campaignsRefresh" ><i class="fa fa-refresh"></i> Chiến dịch</a>
                                        </li>
                                        <li v-else>
                                            <a href="#" class="disabled"><i class="fa fa-spin fa-refresh"></i> Đang lấy dữ liệu...</a>
                                        </li>

                                        <template v-if="campaigns.length > 0">
                                            <li v-if="!setTrackingTemplateState">
                                                <a href="#" v-on:click="trackingTemplateSubmit"><i class="fa fa-cog"></i> Tích hợp Tracking Code</a>
                                            </li>
                                            <li v-else>
                                                <a href="#" class="disabled"><i class="fa fa-spin fa-cog"></i> Đang tích hợp Tracking...</a>
                                            </li>
                                        </template>
                                    </ul>
                                </div>
                            </div>
                            <div class="box-body">
                                <template v-if="campaigns.length > 0">
                                    <table class="table table-striped">
                                        <tr>
                                            <th>#</th>
                                            <th>Chiến dịch</th>
                                            <th>Trạng thái</th>
                                            <th style="width: 40px">Tracking Template</th>
                                        </tr>
                                        <tr v-for="(campaign,index) in campaigns">
                                            <td v-text="index+1"></td>
                                            <td v-text="campaign.post_name"></td>
                                            <td v-text="campaign.post_status"></td>
                                            <td>
                                                <span v-if="campaign.trackingUrlTemplate == 'loading'" class="label label-warning">Đang xử lý...</span>
                                                <span v-else-if="campaign.trackingUrlTemplate == null" class="label label-default">chưa cấu hình</span>
                                                <span v-else class="label label-success">Hoàn thành</span>
                                            </td>
                                        </tr>
                                    </table>
                                <template>
                                <template v-else>
                                    <p>Không tìm thấy bất kỳ chiến dịch nào trong tài khoản (CLICK LOAD CHIẾN DỊCH MỚI)</p>
                                </template>
                            </div>
                        </div>

                        <p><b>Bước 2:</b> Coppy/Paste đoạn script sau vào website đang chạy quảng cáo</p>
                        <p><pre v-text="clientTrackingJs"></pre></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
});

Vue.component('frauds-clicks-report-component', {
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            indexTab: {
                guid: '',
                text: '<i class="fa fa fa-pie-chart"> Tổng quan</i> ',
                badge: "",
                isActive: false
            },
            ipTab: {
                guid: '',
                text: '<i class="fa fa-history"> Lịch sử IP hoạt động</i>',
                badge: "",
                isActive: false
            },
            ipBlockedTab: {
                guid: '',
                text: '<i class="fa fa-shield"> Lịch sử chặn IP</i>',
                badge: "",
                isActive: true
            },
            ruleTab: {
                guid: '',
                text: '<i class="fa fa-cogs"> Cấu hình Rule</i>',
                badge: "",
                isActive: false
            },
            rules: null,
            urlGetVisitors : jsobjectdata.url_get_visitors,
            visitors: [],
            term_id: jsobjectdata.term_id
        };
    },
    created: function() {
        this.indexTab.guid = 'indexTab-' + this.guid;
        this.ipTab.guid = 'ipTab-' + this.guid;
        this.ipBlockedTab.guid = 'ipBlockedTab-' + this.guid;
        this.ruleTab.guid = 'ruleTab-' + this.guid;

        this.getVisitors();
    },
    methods: {
        accessRuleZone: function(){

        },
        getRuleConf: function(){

            var seft = this;
            axios
            .get(this.urlGetConfRules, { params: { term_id: this.term_id } })
            .then(function (response) {

                if(response.data.status == false)
                {
                    $.notify(response.data.msg, "warning");
                    return;
                }

                seft.rules = response.data.data;
                return true;  
            });
        },
        getVisitors: function(){

            var seft = this;
            axios
            .get(this.urlGetVisitors, { params: { term_id: this.term_id } })
            .then(function (response) {

                if(response.data.status == false)
                {
                    $.notify(response.data.msg, "warning");
                    return;
                }

                if(response.data.data != undefined) 
                {
                    seft.visitors = response.data.data;
                }
            });
        }
    },
    template: `
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">

            <ul class="nav nav-tabs">
                <li :class="indexTab.isActive ? 'active' : ''"><a :href="'#'+indexTab.guid" data-toggle="tab"><span v-html="indexTab.text"></span> <span class="badge" v-text="indexTab.badge"></span></a></li>
                <li :class="ipTab.isActive ? 'active' : ''"><a :href="'#'+ipTab.guid" data-toggle="tab"><span v-html="ipTab.text"></span> <span class="badge" v-text="ipTab.badge"></span></a></li>
                <li :class="ipBlockedTab.isActive ? 'active' : ''"><a :href="'#'+ipBlockedTab.guid" data-toggle="tab"><span v-html="ipBlockedTab.text"></span> <span class="badge" v-text="ipBlockedTab.badge"></span></a></li>
                <li :class="ruleTab.isActive ? 'active' : ''"><a :href="'#'+ruleTab.guid" data-toggle="tab"><span v-html="ruleTab.text"></span> <span class="badge" v-text="ruleTab.badge" v-on:click="accessRuleZone"></span></a></li>
                <li class="pull-right"></li>
            </ul>

            <div class="tab-content">

                <div :class="indexTab.isActive ? 'tab-pane active' : 'tab-pane'" :id="indexTab.guid">
                    <ip-overview-component v-bind:visitors="visitors" :guid="guid" />
                    <div class="clearfix"></div>
                </div>

                <div :class="ipTab.isActive ? 'tab-pane active' : 'tab-pane'" :id="ipTab.guid">
                    <ip-tracking-component v-bind:visitors="visitors" :guid="guid"/>
                    <div class="clearfix"></div>
                </div>

                <div :class="ipBlockedTab.isActive ? 'tab-pane active' : 'tab-pane'" :id="ipBlockedTab.guid">
                    <ip-blocked-component v-bind:visitors="visitors" :guid="guid"/>
                    <div class="clearfix"></div>
                </div>

                <div :class="ruleTab.isActive ? 'tab-pane active' : 'tab-pane'" :id="ruleTab.guid">
                    <rule-configuration-component :guid="guid" :rules="rules" />
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
    `
});