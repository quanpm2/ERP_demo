Vue.component('adword-editor-convert-tool-component-v2', {
	props: {
        uid : { type : String, default : () => { return _.uniqueId("adword-editor-convert-tool-component");}}
    },
    data: function(){
    	return {
            'action' : `${base_url}/api-v2/googleads/ExcelAdsEditor/upload`,
    		'downloadAction' : `${base_url}/api-v2/googleads/ExcelAdsEditor/export`,
    		'adGroups' : [],
    		'fileToken' : "",
    		'sampleXlsx' : `${base_url}/files/excel_tpl/tool/google-adwords-editors-import-template-v2.xlsx`,
            'objsQuantity' : null,
            'textAds' : null,
            'textHeadline': null,
            'textDescription': null,

            'criterionTypeEnum': {
                'Exact': 'Chính xác',
                'Phrase': 'Cụm từ',
                'Broad': 'Chỉnh sửa rộng',
            }
    	}
    },
    watch: {
        adGroups: function(){

            let _adGroups   = _.uniq(_.map(this.getAdGroups(), 'adGroup'));
            let _campaigns  = _.uniq(_.map(this.getAdGroups(), 'campaign'));
            let _keywords   = _.flattenDepth(_.map(this.getAdGroups(), 'keywords'));
            let _textHeadline    = _.flattenDepth(_.map(this.getAdGroups(), 'headLine'));
            let _textDescription    = _.flattenDepth(_.map(this.getAdGroups(), 'description'));

            let objsQuantity = [
                { text: "Campaign", value: _campaigns.length},
                { text: "Ad-group", value: _adGroups.length},
                { text: "Keyword", value: _keywords.length},
                { text: "Mẫu Tiêu Đề", value: _textHeadline.length},
                { text: "Mẫu Mô Tả", value: _textDescription.length}
            ];

            this.textHeadline    = null;
            this.textHeadline    = _textHeadline;

            this.textDescription    = null;
            this.textDescription    = _textDescription;

            this.objsQuantity = null;
            this.objsQuantity = objsQuantity;
        }
    },
    mounted: function() {
    	this.initFileInput();
    },
    methods : {
    	UploadXlsx: function(){},
        downloadCSV: function(){

            var data = new FormData();
            data.append('fileToken', this.getFileToken());

            fetch(this.downloadAction, { method: "POST", body: data})
            .then(response => {
                if( ! response.ok) {
                    $.notify(response.statusText, "error");
                    throw new Error(response.statusText);  
                } 
                return response;
            })
            .then(response => response.json())
            .then(response => {
                $.notify(response.message, "success");
                if( ! _.isEmpty(response.fileName)) 
                {
                    $.notify("Đang chuẩn bị file để tải xuống ! Chọn nơi để lưu file ...", "info");
                    window.location = response.fileName;
                }
            });
        },
    	initFileInput: function(){
    		var _seft = this;
			$(`#${this.uid}-inputfile`).fileinput({
				uploadUrl: _seft.action,
				uploadAsync: true,
				showPreview: false,
				showRemove: true,
				uploadExtraData: { btnImportFile: "btnImportFile" },
				allowedFileExtensions: ['xls', 'xlsx'],
			})
			.on('fileuploaded', function(event, data, previewId, index) {

				if( ! data.response.status) return;

				_seft.setAdGroups(data.response.data.adGroups);
				_seft.setFileToken(data.response.data.fileToken);
                $.notify(data.response.message, "success");
			});
    	},

    	getAdGroups: function(){ return this.adGroups },
    	setAdGroups: function(adGroups){ this.adGroups = []; this.adGroups = adGroups; },
    	getFileToken: function(){ return this.fileToken },
    	setFileToken: function(fileToken){ this.fileToken = ""; this.fileToken = fileToken; },
    },
    template: `
    <form :action="action" method="post" accept-charset="utf-8">

		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Tệp dữ liệu (*.xlsx)</label>
			<div class="form-group col-md-10">
				<div class="input-group col-sm-12">
					<input :id="uid + '-inputfile'" type="file" name="inputFile" class="form-control file_loading" >
				</div>
				<p class="help-block">
					<a :href="sampleXlsx">Tải về mẫu template file import</a>
				</p>
			</div>
		</div>


        <!-- correct -->
        <div v-if="!_.isEmpty(adGroups)" class="col-md-12 text-center"> 
            <a v-for="o in objsQuantity" class="btn btn-app">
                <span class="badge bg-purple" v-text="o.value"></span>
                <i class="fa fa-cube"></i> {{ o.text }}
            </a>
            <a v-on:click='downloadCSV()' class="btn btn-app bg-green">
                <i class="fa fa-cloud-download"></i> Export File (.XLSX) For Adword Editor
            </a>
        </div>

        <table v-if="!_.isEmpty(adGroups)" class="table table-striped">
            <caption style="text-align: center;text-transform: capitalize;"><u>List Mẫu Quảng Cáo</u></caption>
            <thead>
                <tr>
                    <th>Chiến dịch</th>
                    <th>Nhóm quảng cáo</th>
                    <th>Từ khoá</th>
                    <th>Mẫu Tiêu Đề</th>
                    <th>Mẫu Mô Tả</th>
                    <th>URL cuối cùng</th>
                    <th>URL mobile cuối cùng</th>
                    <th>Đường dẫn 1</th>
                    <th>Đường dẫn 2</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="ads in adGroups">
                    <td v-text="ads.campaign"></td>
                    <td v-text="ads.adGroup"></td>
                    <td>
                        <p v-for="keyword in _.get(ads, 'keywords')">
                            <span><b>{{keyword.keyword}}</b></span>
                            <br/>
                            <span>maxCPC: {{keyword.maxCPC}}</span>
                            <br/>
                            <span>Đối sánh: {{criterionTypeEnum[keyword.criterionType]}}</span>
                        </p>
                    </td>
                    <td>
                        <p v-for="headline in _.get(ads, 'headLine')">
                            <span>{{headline}}</span>
                            <br/>
                        </p>
                    </td>
                    <td>
                        <p v-for="_description in _.get(ads, 'description')">
                            <span>{{_description}}</span>
                            <br/>
                        </p>
                    </td>
                    <td v-text="ads.finalURL"></td>
                    <td v-text="ads.finalMobileURL"></td>
                    <td v-text="ads.path_1"></td>
                    <td v-text="ads.path_2"></td>
                </tr>
            </tbody>
        </table>
    </form>
    `
});