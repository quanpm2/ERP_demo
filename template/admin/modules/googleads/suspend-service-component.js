Vue.component('suspend-service-component', {
    props: ['isHidden','term_id','end_date'],
    data: function(){
        return {
            admin_url: admin_url,
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            isLoading : false,
        };
    },
    methods: {
        suspend_service_submit: function(){

            this.isLoading = true;
            var seft = this;

            $.ajax({
                type: 'POST',
                url: admin_url + 'googleads/ajax/contract/suspend_service/' + this.term_id,
                data : $('#suspend-frm-'+this.guid).serializeArray(),
                success: function(response){

                    $.notify(response.msg, {className: (response.status ? 'success' : 'error'), clickToHide: true, autoHideDelay: 15000,});

                    seft.isLoading = false;

                    if(response.status) 
                    {
                        seft.isHidden = true;
                        $('#modal-'+ seft.guid).modal('hide');
                        seft.isHidden = true;
                    }
                },
                dataType: 'json'
            });
        },
    },
    template: `
    <div v-if=" ! isHidden" :id="'container-' + guid" class="col-md-2">

        <button :id="'btn-' + guid" type="button" class="btn btn-warning" data-toggle="modal" :data-target="'#modal-' + guid">
            <template v-if="!isLoading"><i class="fa fa-fw fa-bell-slash-o"></i> Thanh lý hợp đồng</template>
            <template v-else><i class="fa fa-spinner fa-pulse fa-check"></i> Đang xử lý ...</template>
        </button>

        <div class="modal fade" :id="'modal-' + guid" tabindex="-1" role="dialog" aria-hidden="true" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form :id="'suspend-frm-' + guid" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><i class="fa fa-exclamation-circle"></i> Xác nhận thanh lý hợp đồng</h4>
                        </div>
                        <div class="modal-body">
                            <datepicker-component type="text" name="end_date" label="Thời gian kết thúc" :value="end_date"></datepicker-component>
                            <p>Hệ thống sẽ xử lý các tác vụ sau : </p>
                            <ol>
                                <li>Hợp đồng sẽ được chuyển thành trạng thái "Thanh lý"</li>
                                <li>Gửi Email báo cáo toàn bộ dữ liệu quảng cáo</li>
                                <li>Mọi chỉnh sửa ở dịch vụ sẽ không còn hiệu lực</li>
                            </ol>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Hủy bỏ</button>
                            <button v-if="!isLoading" v-on:click="suspend_service_submit" type="button" :id="'btn-' + guid" class="btn btn-primary"><i class="fa fa-check"></i> Xác nhận</button>
                            <button v-else type="button" class="btn btn-primary disabled"><i class="fa fa-spinner fa-pulse fa-check"></i> Đang xử lý...</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    `
});