Vue.component('proc-service-component', {
    props: ['isHidden','term_id','begin_date'],
    data: function(){
        return {
            admin_url: admin_url,
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            isLoading : false
        };
    },
    created: function(){ },
    methods: {
        proc_service_submit: function(){

            this.isLoading = true;
            var seft = this;

            $.ajax({
                type: 'POST',
                url: admin_url + 'googleads/ajax/contract/proc_service/' + this.term_id,
                data : $('#proc-frm-'+this.guid).serializeArray(),
                success: function(response){

                    $.notify(response.msg, {className: (response.status ? 'success' : 'error'), clickToHide: true, autoHideDelay: 15000,});

                    seft.isLoading = false;

                    if(response.status) 
                    {
                        seft.isHidden = true;
                        $('#modal-'+ seft.guid).modal('hide');
                        seft.isHidden = true;
                    }
                },
                dataType: 'json'
            });
        },
    },
    template: `
    <div v-if=" ! isHidden" :id="'container-' + guid" class="col-md-2">

        <button :id="'btn-' + guid" type="button" class="btn btn-primary" data-toggle="modal" :data-target="'#modal-' + guid">
            <template v-if="!isLoading"><i class="fa fa-fw fa-play"></i> Thực hiện dịch vụ</template>
            <template v-else><i class="fa fa-spinner fa-pulse fa-check"></i> Đang xử lý ...</template>
        </button>

        <div class="modal fade" :id="'modal-' + guid" tabindex="-1" role="dialog" aria-hidden="true" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form :id="'proc-frm-' + guid" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-exclamation-circle"></i> Xác nhận thực hiện dịch vụ</h4>
                        </div>
                        <div class="modal-body">
                            <p>Hệ thống sẽ xử lý các tác vụ sau : </p>
                            <ol>
                                <li><i><u>Cập nhật tỉ giá Dollar từ API ngân hàng</u></i> nếu tỷ giá chưa được sắp xếp</li>
                                <li><i><u>Gửi E-Mail</u></i> đến khách hàng , kinh doanh và kỹ thuật phụ trách</li>
                                <li><i><u>Gửi SMS thông báo thực hiện</u></i> đến khách hàng</li>
                                <li><i><u>Khởi tạo các tiến trình giao tiếp Adword API</u></i></li>
                            </ol>
                            <datepicker-component type="text" name="begin_date" label="Thời gian bắt đầu" :value="begin_date"></datepicker-component>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Hủy bỏ</button>
                            <button v-if="!isLoading" v-on:click="proc_service_submit" type="button" :id="'btn-' + guid" class="btn btn-primary"><i class="fa fa-check"></i> Xác nhận</button>
                            <button v-else type="button" class="btn btn-primary disabled"><i class="fa fa-spinner fa-pulse fa-check"></i> Đang xử lý...</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    `
});