Vue.component('datatable-builder-component', {

    props: ['is_filtering','is_ordering','remote_url','cur_page','per_page'],

    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            headings : [],
            filters : null,
            rows : [],
            sortable : {
                name : '',
                value : '',
            },
            isLoading : true,
        };
    },
    watch : {
        per_page : function(){ 
            this.remote_query(false);
        },
    },
    created : function(){
        this.init();
    },
    updated : function(){
        this.bindSortableEvent();
        this.bindFilterableElements();
    },
    methods : {

        init : function(){
            this.remote_query(true);
        },

        /* bind CLICK event to sortable button */
        bindSortableEvent : function(){

            var seft = this;
            $("#datatable-form-" + this.guid + " .btn-sortable").each(function(i,e){

                $(e).off('click').on('click', function() {
                    
                    $("#datatable-form-" + seft.guid + " .sort-input").val($(e).val());
                    $("#datatable-form-" + seft.guid + " .sort-input").attr('name',$(e).attr('name'));

                    seft.remote_query(false);
                });
            });
        },

        bindFilterableElements : function(){

            $("#datatable-form-" + this.guid + ' .input_daterange').daterangepicker({format: 'DD-MM-YYYY' });
            return true;
        },

        remote_query : function(init){
            
            this.isLoading = true;
            var seft = this;

            var remote_url = this.remote_url;
            if(this.remote_url.indexOf("?") <= 0) remote_url+='?';

            axios
            .get(remote_url + $("#datatable-form-" + this.guid).serialize())
            .then(function (response) {

                var _data = response.data;
                if(init)
                {
                    seft.filters = _data.filters
                }

                seft.headings = _data.headings;
                seft.rows = _data.rows;
                seft.isLoading = false;
            });
        },
    },
    template: `
    <div :id="'datatable-container-' + guid">
        <form :id="'datatable-form-' + guid">
            <div v-if="filters != null" :id="'filter-panel-' + guid" class="collapse in filter-panel">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div v-for="filter in filters" v-html="filter"></div>

                        <div class="col-xs-2 form-group">
                            <button v-on:click="remote_query(false)" type="button" name="search" class="btn btn-info"><i class="fa fa-search"></i> Tìm kiếm</button>
                        </div>
                    </div>
                </div> 
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <div>
                                Hiển thị 
                                <select name="per_page" class="custom-select custom-select-sm" v-model="per_page">
                                    <option selected value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="500">500</option>
                                </select> mỗi trang
                                 | <i class="fa fa-filter"></i> Filter mặc định
                            </div> 
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding" >

                            <input type="hidden" class="sort-input"/>

                            <table :id="guid" border="1" cellpadding="2" cellspacing="1" class="table table-responsive table-bordered table-hover">
                                <thead v-if="headings != 'undefined'"> 
                                    <tr>
                                        <th v-for="heading in headings" v-html="heading"></th> 
                                    </tr>
                                </thead>
                                <tbody v-if="rows != 'undefined'"> 
                                    <tr v-for="row in rows"> 
                                        <td v-for="item in row" v-html="item"></td>
                                    </tr> 
                                </tbody>
                            </table>
                        </div>

                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <li><a href="#">«</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">»</a></li>
                            </ul>
                        </div>

                        <div v-if="isLoading" class="overlay">
                            <i class="fa fa-spinner fa-spin"></i>
                        </div>

                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </form>
    </div> 
    `
});