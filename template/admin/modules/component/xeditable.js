Vue.component('xeditable-component', {
    props: ['value','title','pk','datatype','dataname','url_callback'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
        }
    },
    mounted: function(){
        this.initComponent();
    },
    methods : {
        initComponent : function(){
            var el = this;
            $('#xeditable-' + this.guid).editable({

                url : el.url_callback,
                params: function(params) {
                    params.type = el.datatype
                    return params;
                },
                success: function(response, newValue) {
                    if(!response.success) return response.msg;
                },
                defaultValue: "",
            });
        }
    },
    template: `    
    <a href="#" 
        :value="value" :id="'xeditable-' + guid" :data-original-title="title" 
        :data-pk="pk" :data-name="dataname" class="myeditable editable editable-click editable-empty" v-text="value"></a>
    `
});