Vue.component('datatable-builder-component', {

    props: {
        is_filtering: { type: Boolean, default : true },
        is_ordering: { type: Boolean, default : true },
        is_download: { type: Boolean, default : false },
        remote_url : { type : String, default : "" },
        cur_page: { type: Number, default : 1 },
        per_page: { type: Number, default : 20 },
        controller : { type : String, default : ""},
        guid : { type : String, default : () => { return 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);});} },
    },

    data: function(){
        return {
            columns : [],
            headings : [],
            filters : null,
            actions : null,
            rows : [],
            pagination  : null,
            sortable : {
                name : '',
                value : '',
            },
            isLoading : true,
            config : {},
            columnsSelected : [],
            groupColumnsSaved : [],
            groupColumnsSavedNameChecked : false,
            groupColumnsSavedName : '',
        };
    },
    computed : {
        limitOptions : function(){ 

            let result = [];
            if( ! this.pagination) return result; // When result is null

            let defaultOpts = [10,20,50,100,200,500];
            let max         = this.pagination.total_rows;
            for (var i = 0; i < defaultOpts.length; i++)
            {
                if(max < defaultOpts[i])
                {
                    result.push(defaultOpts[i]);
                    break;
                }

                result.push(defaultOpts[i]);
            }

            return result;
        },
        pagingDescription : function(){
            if( ! this.pagination || this.pagination.max == 0) return '';
            return '<i>' + this.pagination.min + ' - ' + this.pagination.max + ' / '+ this.pagination.total_rows +'</i>';
        }
    },
    watch : {
        per_page : function(){ 
            $("#datatable-form-" + this.guid + " input.cur_page").val(1);
            this.remote_query(false);
        },

    },
    created : function(){
        this.init();

        // Load config columns
        this.loadConfig();
    },
    updated : function(){
        this.bindSortableEvent();
        this.bindFilterableElements();
        this.bindPaginationAnchorEvent();
        this.bindFormSubmitEvent();
        this.$emit('data_changed',true);

        var seft = this;

        // jQuery UI sortable for the todo list
        $('ul.todo-list').sortable({
            placeholder         : 'sort-highlight',
            handle              : '.handle',
            forcePlaceholderSize: true,
            zIndex              : 999999
        });
    },
    methods : {

        modifyColumnsApply : function(){

            let values = $(".todo-list").sortable( "toArray", {attribute  : "column"});

            if(this.groupColumnsSavedNameChecked && this.groupColumnsSavedName)
            {
                let groupColumnsSaved = JSON.parse(localStorage.getItem("groupColumnsSaved"));
                if(_.isNull(groupColumnsSaved)) groupColumnsSaved = [];

                let currentUrl = location.pathname;

                let _i = _.findIndex(groupColumnsSaved, { 'url' : currentUrl, 'name' : this.groupColumnsSavedName });
                if(_i >= 0) groupColumnsSaved[_i].columns = values;
                else groupColumnsSaved.push({ 'url' : currentUrl, 'name' : this.groupColumnsSavedName, 'columns' : values});

                localStorage.setItem('groupColumnsSaved', JSON.stringify(groupColumnsSaved)); // Save data to browser memory
                    
                // Reset input group calomns saved to default
                this.groupColumnsSavedNameChecked = false;
                this.groupColumnsSavedName = '';

                this.groupColumnsSaved = [];
                this.groupColumnsSaved = groupColumnsSaved;
            }

            this.columnsSelected = values;
            this.remote_query(true);
        },

        removeColumnSelected : function(key){

            let _columnSelected = this.columnsSelected;
            _columnSelected     = _.pull(_columnSelected, key);

            this.columnsSelected = [];
            this.columnsSelected = _columnSelected;
        },

        applyGroupColumnSaved : function(event, key)
        {
            event.preventDefault();

            let groupColumnsSaved = JSON.parse( localStorage.getItem("groupColumnsSaved"));
            if(_.isNull(groupColumnsSaved)) return false;

            let currentUrl = location.pathname;
            let i = _.findIndex(groupColumnsSaved, { 'url' : currentUrl, 'name' : key });

            if(i >= 0) 
            {
                console.log(groupColumnsSaved[i]);
                this.columnsSelected = [];
                this.columnsSelected = groupColumnsSaved[i].columns;
                this.remote_query(true);
            }

            return false;
        },

        removeGroupColumnSaved : function(event, key)
        {
            event.preventDefault();

            let groupColumnsSaved = JSON.parse( localStorage.getItem("groupColumnsSaved"));
            if(_.isNull(groupColumnsSaved)) return false;

            let currentUrl = location.pathname;
            let _i = _.findIndex(groupColumnsSaved, { 'url' : currentUrl, 'name' : key });
            if(_i >= 0) 
            {
                _.pullAt(groupColumnsSaved, _i);

                this.groupColumnsSaved = [];
                this.groupColumnsSaved = groupColumnsSaved

                localStorage.setItem("groupColumnsSaved", JSON.stringify(groupColumnsSaved)); // Save data to browser memory
            }

            return false;
        },

        getColumnByKey : function(key){
            if( ! _.has(this.config.columns, key)) return 'Rỗng';
            return this.config.columns[key];
        },

        loadConfig : function(){

            var seft = this;

            axios.get(admin_url + this.controller + '/rest/contract/config')
            .then(function (response) {
                seft.config = response.data.data;
                if(_.isEmpty(self.columnsSelected))
                {
                    seft.columnsSelected = seft.config.default_columns;
                }
            })
            .catch(function (error) {
            });


            this.loadLocalStorage();
        },

        loadLocalStorage : function(){
            let groupColumnsSaved = JSON.parse(localStorage.getItem("groupColumnsSaved"));
            if(_.isNull(groupColumnsSaved)) return false;

            
            let currentUrl = location.pathname;
            groupColumnsSaved = _.filter(groupColumnsSaved, { 'url' : currentUrl });

            this.groupColumnsSaved = [];
            this.groupColumnsSaved = groupColumnsSaved;
        },

        init : function(){
            this.remote_query(true);
        },

        /* bind CLICK event to sortable button */
        bindPaginationAnchorEvent : function(){

            var seft = this;
            $("#datatable-form-" + this.guid + " a.pagination_anchor_navigator").each(function(index,element){

                $(element).off('click').on('click', function(e) {
                    e.preventDefault();
                    if( $(this).data('ci-pagination-page') == '') return true;
                    $("#datatable-form-" + seft.guid + " input.cur_page").val($(this).data('ci-pagination-page'));
                    seft.remote_query(false);
                    return false;
                });
            });
        },

        /* bind CLICK event to sortable button */
        bindSortableEvent : function(){

            var seft = this;
            $("#datatable-form-" + this.guid + " .btn-sortable").each(function(i,e){

                $(e).off('click').on('click', function() {
                    
                    $("#datatable-form-" + seft.guid + " .sort-input").val($(e).val());
                    $("#datatable-form-" + seft.guid + " .sort-input").attr('name',$(e).attr('name'));

                    seft.remote_query(false);
                });
            });
        },

        bindFilterableElements : function(){

            $("#datatable-form-" + this.guid + ' .input_daterange').daterangepicker({format: 'DD-MM-YYYY' });
            $("#datatable-form-" + this.guid + ' .input_monthpicker').datepicker({format: 'mm-yyyy',minViewMode: 1, viewmode: 'months', minviewmode: 'months'});
            $('#datatable-container-' + this.guid + " .select2").select2();
            $('#datatable-container-' + this.guid + " span.select2").css('width', '100%');
            return true;
        },

        bindFormSubmitEvent : function(){

            $("form#datatable-form-" + this.guid).off('submit').on('submit',function(e){
                e.preventDefault();
            });
        },

        remote_query : function(init){
            
            this.isLoading = true;
            var seft = this;

            axios
            .get(this.getAjaxUrl())
            .then(function (response) {

                var _data = response.data;
                if(init)
                {
                    seft.filters = _data.filters;

                    if(typeof _data.actions != 'undefined') seft.actions = _data.actions;
                }

                seft.headings = _data.headings;
                seft.rows = _data.rows;
                seft.pagination = _data.pagination;
                seft.isLoading = false;
            })
            .catch(function(err){
                console.log(err);
                seft.isLoading = false;
            });
        },

        /* build ajax url */
        getAjaxUrl : function(){
            var ajax_url = this.remote_url;
            if(ajax_url.indexOf("?") <= 0) ajax_url+='?';

            let params = $("#datatable-form-" + this.guid).serializeArray();

            if(this.columnsSelected.length > 0) params.push({'name':'columns', value:this.columnsSelected});
            
            return ajax_url + '&' + $.param(params);
        },

        /* build download href */
        getDownloadUrl : function(){ return this.getAjaxUrl() + '&download=1'; },
    },
    
    template: `
    <div :id="'datatable-container-' + guid">
        <form :id="'datatable-form-' + guid">
            <div v-if="filters != null" :id="'filter-panel-' + guid" class="collapse in filter-panel">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div v-for="filter in filters" v-html="filter"></div>

                        <div class="col-md-3 col-xs-12">
                            <button v-on:click="remote_query(false)" name="search" class="btn btn-info"><i class="fa fa-search"></i> Tìm kiếm</button>
                            <a v-if="is_download == true" :href="getDownloadUrl()" class="btn btn-success"><i class="fa fa-download"></i> Export</a>
                            <span v-if="actions != null" v-html="actions"></span>
                        </div>
                    </div>
                </div> 
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <div>
                                Hiển thị:
                                <select name="per_page" class="custom-select custom-select-sm" v-model="per_page">
                                    <option v-for="option in limitOptions" :value="option" v-text="option"></option>
                                </select>
                                <span style="margin-left:10px; font-weight: bold;font-style: italic;" v-html="pagingDescription"></span>
                            </div>
                            <div class="box-tools" style="top: -14px;">

                                <template v-if="!_.isEmpty(config.columns)">
                                    <a v-if="_.isEmpty(groupColumnsSaved)" data-toggle="modal" :data-target="'#modal-' + guid" class="btn btn-flat btn-sm text-black"><i class="fa fa-2x fa-columns"></i><br>CỘT</a>
                                    <template v-else>
                                        <a class="btn btn-flat btn-sm text-black" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-2x fa-columns"></i><br>CỘT
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            <li><a href="" data-toggle="modal" :data-target="'#modal-' + guid"><i class="fa fw fa-cogs"></i> Tùy chỉnh</a></li>
                                            <li class="divider"></li>
                                            <li class="col-xs-12 disabled"><u><i>Các cột đã lưu</i></u></li>
                                            <li v-for="gcs in groupColumnsSaved">

                                                <a href="#" v-on:click="applyGroupColumnSaved($event, gcs.name)" class="col-xs-10" v-text="gcs.name">
                                                    {{gcs.name}}
                                                </a>
                                                <div class="tools col-xs-2" style="padding-right:10px">
                                                    <a href="#" v-on:click="removeGroupColumnSaved($event, gcs.name)"><i class="fa fa-close"></i></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </template>
                                </template>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding main-datatable">
                            <div class="clearfix"></div>
                            <input type="hidden" class="sort-input"/>
                            <input type="hidden" name="cur_page" class="cur_page"/>

                            <table :id="guid" border="1" name='sticky' cellpadding="2" cellspacing="1" class="table-main-dataset table table-responsive table-bordered table-condensed table-hover sticky">
                                <thead v-if="headings != 'undefined'" style="background:#ffffff;"> 
                                    <tr>
                                        <th nowrap="nowrap" style="vertical-align:top;" v-for="heading in headings" v-html="heading"></th> 
                                    </tr>
                                </thead>
                                <tbody v-if="rows"> 
                                    <tr v-for="row in rows">
                                        <td style="white-space:nowrap;" v-for="item in row" v-html="item"></td>
                                    </tr> 
                                </tbody>
                                <tbody v-else>
                                    <tr><td align="center"><b><i>Dữ liệu không tồn tại hoặc đã bị xóa</b></i></td></tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="box-footer clearfix">
                            <div class="box-tools">
                                <div v-if="pagination != null && pagination.html != ''">
                                    <span v-html="pagination.html"></span>
                                    <span style="font-weight: bold;font-style: italic;"><i>Hiển thị {{pagination.min}} đến {{pagination.max}} trong tổng số {{pagination.total_rows}}</i></span>
                                </div>
                            </div>
                        </div>

                        <div v-if="isLoading" class="overlay">
                            <i class="fa fa-spinner fa-pulse fa-spin"></i>
                        </div>

                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>

            <div class="modal" :id="'modal-' + guid">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">SỬA ĐỔI CỘT</h4>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="col-md-8 border-right">

                                    <div class="col-xs-6" v-for="column in config.columns">
                                        <div class="form-group"> 
                                            <div class="checkbox">
                                              <label>
                                                <input v-model="columnsSelected" :value="column.name" name="column_selected" type="checkbox">&nbsp&nbsp<span v-text="column.label"></span>
                                              </label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <h4 class="no-margin">Các cột của bạn</h4>
                                    <p class="text-muted"><i>Kéo và thả để sắp xếp lại</i></p>
                                    <ul class="todo-list" id="my-todo-list">
                                        <li v-for="column in columnsSelected" :column="column">
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <span class="text" v-text="getColumnByKey(column).label"></span>
                                            </span>
                                            <div class="tools">
                                                <i v-on:click="removeColumnSelected(column)" class="fa fa-close"></i>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="checkbox pull-left">
                                <label>
                                    <input v-model="groupColumnsSavedNameChecked" type="checkbox">&nbsp&nbsp Lưu & sử dụng lại 
                                    <input v-if="groupColumnsSavedNameChecked" class="no-border-but-bottom" v-model="groupColumnsSavedName" type="text" placeholder="với tên..."/>
                                </label>
                            </div>

                            <button type="button" v-on:click="modifyColumnsApply" class="btn btn-primary">ÁP DỤNG</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">HỦY</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                    </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

        </form>
    </div> 
    `
});

Vue.component('info-box-component', {
    props: ['bgcolor','icon','title','description'],
    computed: {
        bg_color : function(){
            if(typeof this.bgcolor == 'undefined') return 'bg-aqua';
            return this.bgcolor;
        },
        fa_icon : function(){
            if(typeof this.icon == 'undefined') return 'fa fa-circle-o';
            return this.icon;
        },
        headline : function(){
            if(typeof this.title == 'undefined') return '';
            return this.title;
        },
        desc : function(){
            if(typeof this.description == 'undefined') return '';
            return this.description;
        }
    },
    template: `    
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span :class="'info-box-icon ' + bg_color"><i :class="fa_icon"></i></span>
            <div class="info-box-content">
                <span class="info-box-text" v-html="headline"></span>
                <span class="info-box-number" v-html="desc"></span>
            </div>
        </div>
    </div>
    `
});