Vue.component('xeditable', {
    props: {
        pk : { type: Number, default : 1 },
        type : { type : String, default : ""},
        name : { type : String, default : ""},
        value : { type : String, default : ""},
        remote_url : { type : String, default : ""},
        title : { type : String, default : ""},
        metadata : { type : String, default : ""},
        datasource : { type : Array, default : []},
        disabled : { type : Boolean, default : false},
        guid : { type : String, default : () => { return 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);});} },
        reload_if_success: { type : Boolean, default : false},
    },

    mounted: function(){
        this.init();
    },
    methods : {
        init : function(){

            var _seft = this;
            switch(this.type)
            {
                case 'checklist' :

                    $('#xeditable-' + this.guid).editable({
                        url : _seft.remote_url,
                        disabled : _seft.disabled,
                        emptytext: 'Chưa có',
                        params: function(params) {
                            params.type = $(this).data("type-data");
                            return params;
                        },
                        source : function(){ return _seft.datasource; },
                        display: function(value, sourceData) {

                            if(_.isEmpty(value)) 
                            {
                                $(this).empty();
                                return true;
                            }

                            if( ! _.isEmpty(value) && _.isArray(value)) _.remove(value, function(o){ return _.isEmpty(o); });

                            $(this).text(_.join(_.map(value, function(i){

                                const obj = _.find(sourceData, {'value': i});
                                if(_.isEmpty(obj)) return ""
                                return obj.value

                            }), ', '));
                        },  

                        success: function(data, newValue) {

                            let messages = [];
                            if(_.isString(data.msg)) messages.push(data.msg);
                            else messages = data.msg;

                            _.forEach(messages, function(message){ $.notify(message, 'success'); });

                            if(_seft.reload_if_success) setTimeout( () => { window.location.reload() }, 1800)

                        },
                        error: function(response) { return JSON.parse(response.responseText); },
                    });
                    break;

                case 'select2':

                    const _source = JSON.parse(_seft.datasource)

                    $('#xeditable-' + this.guid).editable({
                        url : _seft.remote_url,
                        disabled : _seft.disabled,
                        emptytext: 'Chưa có',
                        params: function(params) {
                            params.type = $(this).data("type-data");
                            return params;
                        },
                        source : _source,

                        display: function(value, source) {
                            value = _.parseInt(value)
                            if( _.gt(value, 0))
                            {
                                const _item = _.find(_source, o => { 
                                    return _.eq(o.id, value)
                                })
                                if(!_.isEmpty(_item))
                                {
                                    $(this).text(_item.text)
                                }
                                else
                                {
                                    $(this).empty();
                                }
                            }
                            else { $(this).empty(); }
                        },  

                        success: function(data, newValue) {
                            let messages = [];
                            if(_.isString(data.msg)) messages.push(data.msg);
                            else messages = data.msg;

                            _.forEach(messages, function(message){ $.notify(message, 'success'); });
                            if(_seft.reload_if_success) setTimeout( () => { window.location.reload() }, 1800)
                        },
                        error: function(response) { return JSON.parse(response.responseText); },
                    });
                    break;
                
                case 'select' : 
                    $('#xeditable-' + this.guid).editable({
                        url : _seft.remote_url,
                        disabled : _seft.disabled,
                        emptytext: 'Chưa có',
                        params: function(params) {
                            params.type = $(this).data("type-data");
                            return params;
                        },
                        source : function(){ return _seft.datasource; },
                        
                        display: function(value, sourceData) {
                            elem = $.grep(sourceData, function(o){return o.value == value;});
                            if(elem.length) { $(this).text(elem[0].text);  } 
                            else { $(this).empty(); }
                        },  

                        success: function(data, newValue) {
                            let messages = [];
                            if(_.isString(data.msg)) messages.push(data.msg);
                            else messages = data.msg;

                            _.forEach(messages, function(message){ $.notify(message, 'success'); });
                            if(_seft.reload_if_success) setTimeout( () => { window.location.reload() }, 1800)
                        },
                        error: function(response) { return JSON.parse(response.responseText); },
                    });
                    break;

                default :

                    $('#xeditable-' + this.guid).editable({
                        url : _seft.remote_url,
                        disabled : _seft.disabled,
                        emptytext: 'Chưa có',
                        params: function(params) {
                            params.type = $(this).data("type-data");
                            return params;
                        },
                        success: function(data, newValue) {
                            let messages = [];
                            if(_.isString(data.msg)) messages.push(data.msg);
                            else messages = data.msg;
                            
                            _.forEach(messages, function(message){ $.notify(message, 'success'); });
                            if(_seft.reload_if_success) setTimeout( () => { window.location.reload() }, 1800)
                        },
                        error: function(response) { return JSON.parse(response.responseText); },
                    });
                    break;
            }
        },
        currencyFormat : function(value){
            let val = parseInt(value);
            if( Number.isNaN(val)) return '0';
            return val.toLocaleString();
        },
    },
    template: `
    <span>
        <a v-if="type=='text' || type=='textarea'" :id="'xeditable-' + guid" href="#"
            :type="type" 
            :name="name" 
            :value="value" 
            :data-original-title="title" 
            :data-pk="pk" 
            :data-type-data="metadata" 
            :data-name="name" 
            :data-value="value" 
            :data-type="type"
            class="editable" v-text="value"></a>

        <a v-if="type=='email'" :id="'xeditable-' + guid" href="#"
            :type="type" 
            :name="name" 
            :value="value" 
            :data-original-title="title" 
            :data-pk="pk" 
            :data-type-data="metadata" 
            :data-name="name" 
            :data-value="value"
            data-type="email" 
            class="editable" v-text="value"></a>

        <a v-if="type=='number'" :id="'xeditable-' + guid" href="#"
            type="text" 
            data-type="number"
            :name="name" 
            :value="value" 
            :data-original-title="title" 
            :data-pk="pk" 
            :data-type-data="metadata" 
            :data-name="name" 
            :data-value="value" 
            class="editable" v-text="currencyFormat(value)"></a>

        <a v-if="type=='combodate'" :id="'xeditable-' + guid" href="#"
            type="text" 
            data-type="combodate"
            :name="name" 
            :value="value" 
            :data-original-title="name"
            :data-pk="pk" 
            :data-type-data="metadata" 
            :data-name="name" 
            :data-value="value" 
            data-template="D MM YYYY" 
            data-format="DD-MM-YYYY" 
            data-viewformat="DD-MM-YYYY" 
            class="editable" v-text="value"></a>

        <a v-if="type=='select'" :id="'xeditable-' + guid" href="#"
            type="text" 
            data-type="select"
            :name="name" 
            :value="value" 
            :data-original-title="title"
            :data-pk="pk" 
            :data-type-data="metadata" 
            :data-name="name" 
            :data-value="value" 
            class="editable" v-text="value"></a>

        <a v-if="type=='select2'" :id="'xeditable-' + guid" href="#"
            type="text" 
            data-type="select2"
            :name="name" 
            :value="value" 
            :data-original-title="title"
            :data-pk="pk" 
            :data-type-data="metadata" 
            :data-name="name" 
            :data-value="value" 
            class="editable" v-text="value"></a>

        <a v-if="type=='checklist'" :id="'xeditable-' + guid" href="#"
            type="text" 
            data-type="checklist"
            :name="name" 
            :value="value" 
            :data-original-title="title"
            :data-pk="pk" 
            :data-type-data="metadata" 
            :data-name="name" 
            :data-value="value" 
            class="editable" v-text="value"></a>
    </span>  
    `
});

Vue.component('input-component', {
    props: ['type','name','label','value','help','attrs'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
        }
    },
    mounted: function(){
        this.initComponent();
    },
    methods : {
        initComponent : function(){
            var el = this;
            $('#xeditable-' + this.guid).editable({

                url : el.url_callback,
                params: function(params) {
                    params.type = el.datatype
                    return params;
                },
                success: function(response, newValue) {
                    if(!response.success) return response.msg;
                },
                defaultValue: "",
            });
        }
    },
    template: `    
    <div class="form-group">
        <label :for="guid" class="col-sm-2 control-label" v-html="label"></label>
        <div class="form-group col-md-10">
            <div class="input-group col-sm-12">
                <input :type="type" :name="name" :value="value" :id="'input-'+guid" class="form-control valid">
                <p v-if="help" class="help-block" v-text="help"></p>
            </div>
        </div>
    </div>
    `
});

Vue.component('input-component', {
    props: ['type','name','label','value','help','attrs'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
        }
    },
    mounted: function(){
        this.initComponent();
    },
    methods : {
        initComponent : function(){
            var el = this;
            $('#xeditable-' + this.guid).editable({

                url : el.url_callback,
                params: function(params) {
                    params.type = el.datatype
                    return params;
                },
                success: function(response, newValue) {
                    if(!response.success) return response.msg;
                },
                defaultValue: "",
            });
        }
    },
    template: `    
    <div class="form-group">
        <label :for="guid" class="col-sm-2 control-label" v-html="label"></label>
        <div class="form-group col-md-10">
            <div class="input-group col-sm-12">
                <input :type="type" :name="name" :value="value" :id="'input-'+guid" class="form-control valid">
                <p v-if="help" class="help-block" v-text="help"></p>
            </div>
        </div>
    </div>
    `
});

Vue.component('datepicker-component', {
    props: ['type','name','label','value','help','attrs','extraClass'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
        }
    },
    mounted: function(){
        this.initComponent();
    },
    methods : {
        initComponent : function(){
            
            $('#input-' + this.guid).datepicker({
                format: 'dd-mm-yyyy',
                todayHighlight: true,
                autoclose: true,
            });
        }
    },
    template: `    
    <div class="form-group">
        <label :for="guid" class="col-sm-2 control-label" v-html="label"></label>
        <div class="form-group col-md-10">
            <div class="input-group col-sm-12">
                <input :type="type" :name="name" :value="value" :id="'input-'+guid" :class="'form-control valid ' + extraClass">
                <p v-if="help" class="help-block" v-text="help"></p>
            </div>
        </div>
    </div>
    `
});


Vue.component('idatepicker-component', {
    props: {
        name        : { type: String, default : '' },
        value       : { type: String, default : '' },
        extraClass  : { type: String, default : '' },
        guid        : { type : String, default : () => { return _.uniqueId('idatepicker-component-');}}
    },
    updated : function(){
        this.initComponent();
    },
    mounted: function(){
        this.initComponent();
    },
    methods : {
        initComponent : function(){

            $('#' + this.guid).datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                language : 'vi',
            });
        }
    },
    template: `<input type="text" :name="name" :value="value" :id="guid" :class="'form-control valid ' + extraClass">`
});

Vue.component('select2-component',{
    props: ['type','name','label','value','help','attrs','options','remote_url','isSelect2'],
    data: function(){
        return {
            options : typeof this.options != 'undefined' ? this.options : [],
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
        };
    },
    watch: {

        options: function()
        {
            if(typeof this.options == 'undefined') return [];
            this.initSelect2();
        },

        value: function()
        {
            this.$emit('on_changed',this.value);
        }
    },
    mounted: function(){
        this.initSelect2();
    },
    created: function(){
    },
    updated: function(){
        this.initSelect2();
    },
    methods : {

        initSelect2: function(){

            if(this.isSelect2 == true)
            {
                let select_el = $('#select-' + this.guid);
                if (select_el.hasClass("select2-hidden-accessible"))
                {
                    select_el.select2('destroy');
                }

                select_el.css('width', '100%');
                select_el.select2();

                var self = this;

                select_el.on("change", function (e) {  self.value = select_el.val(); });

                select_el.removeClass('form-control');
            }
        }
    },
    template: `
    <div class="form-group">
        <label :for="'input-'+guid" class="col-sm-2 control-label" v-html="label"></label>
        <div class="form-group col-md-10">
            <div class="input-group col-sm-12">

                <select :name="name" :id="'select-' + guid" class="form-control" v-model="value">
                    <template v-if="typeof options != 'undefined' && options.length > 0">
                        <option v-for="opt in options" :value="opt.key" v-value="opt.value" v-text="opt.value"></option>
                    </template>
                </select>

                <p v-if="help" class="help-block" v-text="help"></p>
            </div>
        </div>
    </div>
    `
});

Vue.component('select-component',{
    props : {
        name : { type: String, default : ""},
        value : { default : ""},
        options : { type : Array, default : function(){ return []; }},
        guid : { type : String, default : () => { return 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}); }}
    },
    watch: {

        options: function()
        {
            if(typeof this.options == 'undefined') return [];
            this.initSelect2();
        },

        value: function() { this.$emit('on_changed',this.value); }
    },
    template: `
    <select :name="name" :id="'select-' + guid" class="form-control" v-model="value"  >
        <template v-if="typeof options != 'undefined' && options.length > 0">
            <option v-for="opt in options" :value="opt.key" v-value="opt.value" v-text="opt.value"></option>
        </template>
    </select>
    `
});