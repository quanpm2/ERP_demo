Vue.component('view-cru-calender-component',{
    props:['term_id'],
    data:function(){
        data:null;
        admin_url:admin_url;
    },
    computed:{

    },
    mounted:function(){
        this.firtLoad();
        this.initValidate();
    },
    methods:{
        firtLoad: function() {
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });
            //Datemask dd/mm/yyyy
            $('#fromdate,#todate').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
            
            this.set_color();
            $('input[name="edit[meta][post_status]"]').on('ifClicked', function (event) {
                if(this.value=="on"){
                    return $('#color-chooser > li > a > .color-orange').trigger("click");

               }
               return $('#color-chooser > li > a > .color-green').trigger("click");
            });
        },
        initValidate : function(){

            $('#add-new-frm').validate({
                rules: {
                    'edit[meta][start_date]': {
                        required: true,
                    },
                    'edit[meta][end_date]': {
                        required: true,
                    },
                    'edit[meta][post_title]': {
                        required: true,
                    }
                },
                messages:{
                    'edit[meta][start_date]': {
                        required: "Trường này là bắt buộc.",
                    },
                    'edit[meta][end_date]': {
                        required: "Trường này là bắt buộc.",
                    },
                    'edit[meta][post_title]': {
                        required: "Trường này là bắt buộc.",
                    }
                }
            });
        },
        // set color for button Add
        set_color: function(){
            /* ADDING EVENTS */
            var currColor = '#f39c12' //Red by default
            //Color chooser button
            var colorChooser = $('#color-chooser-btn');
            $('#color-chooser > li > a').click(function (e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css('color')
            //Add color effect to button
            $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
            })


        },
        /* Notify MSG out to screen */
        notify: function(msg,className){

            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },

        submit: function(){
            var calendar = this
            var isValid = $('#add-new-frm').valid();
            if( ! isValid) return false;
            //validate fromdate and todate for dd/mm/yyyy
            if($('#fromdate').val().indexOf('d')!=-1){
                $('#fromdate').addClass('error');
                $('#fromdate-error').css("display","inline-block");
                $('#fromdate-error').text('Ngày tháng năm không chính xác.');
                $('#fromdate-error').show();
                 return false;
            }
            if($('#fromdate').val().indexOf('m')!=-1){
                $('#fromdate').addClass('error');
                $('#fromdate-error').css("display","inline-block");
                $('#fromdate-error').text('Ngày tháng năm không chính xác.');
                $('#fromdate-error').show();
                 return false;
            }
            if($('#fromdate').val().indexOf('y')!=-1) {
                $('#fromdate').addClass('error');
                $('#fromdate-error').css("display","inline-block");
                $('#fromdate-error').text('Ngày tháng năm không chính xác.');
                $('#fromdate-error').show();
                 return false;
            }
            if($('#todate').val().indexOf('d')!=-1) {
                $('#todate').addClass('error');
                $('#enddate-error').text('Ngày tháng năm không chính xác.');
                $('#enddate-error').css("display","inline-block");
                $('#enddate-error').show();
                 return false;
            }
            if($('#todate').val().indexOf('m')!=-1){
                $('#todate').addClass('error');
                $('#enddate-error').text('Ngày tháng năm không chính xác.');
                $('#enddate-error').css("display","inline-block");
                $('#enddate-error').show();
                 return false;
            }
            if($('#todate').val().indexOf('y')!=-1){
                $('#todate').addClass('error');
                $('#enddate-error').text('Ngày tháng năm không chính xác.');
                $('#enddate-error').css("display","inline-block");
                $('#enddate-error').show();
                 return false;
            }


            var calendar = this;
            this.isLoading = true;
            $.post( admin_url + 'webbuild/tasks/update/'+ this.term_id, $('#add-new-frm').serializeArray() , function( response ) {
                
                if( ! response.success)
                {
                    calendar.notify(response.msg, "error");
                    return false;
                }
                calendar.notify(response.msg, "success");
                //refetch Calendar
                $('#calendar').fullCalendar('removeEvents');
                $('#calendar').fullCalendar('refetchEvents');
                //renew form
                calendar.add_new();
            }, "json");
        },
        add_new:function(){
            $('#process').parent().trigger("click");
            $("#add-new-frm").trigger("reset");
            $('#box_title').text('Thêm mới');

        },
        
    },
    template:`
    <div>
        <div class="box box-solid;" style="display:none;">
            <div class="box-header with-border">
            <h4 class="box-title">Draggable Events</h4>
            </div>
            <div class="box-body">

            <div id="external-events">
                <div class="external-event bg-green">Lunch</div>
                <div class="external-event bg-yellow">Go home</div>
                <div class="external-event bg-aqua">Do homework</div>
                <div class="external-event bg-light-blue">Work on UI design</div>
                <div class="external-event bg-red">Sleep tight</div>
                <div class="checkbox">
                <label for="drop-remove">
                    <input type="checkbox" id="drop-remove">
                    remove after drop
                </label>
                </div>
            </div>
            </div>
        </div>
            <div class="box box-solid">
                <div class="box-header with-border">
                <h3 id="box_title" class="box-title">Thêm mới</h3>
                </div>
                <div class="box-body">
                <div class="btn-group" style="width: 100%; margin-bottom: 10px;" style="display:none">
                    <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                    <ul class="fc-color-picker" id="color-chooser">
                    <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-yellow" href="#"><i class="fa fa-square color-yellow"></i></a></li>
                    <li><a class="text-orange" href="#"><i class="fa fa-square color-orange"></i></a></li>
                    <li><a class="text-green" href="#"><i class="fa fa-square color-green"></i></a></li>
                    <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                    <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                    </ul>
                </div>
                <form method="post" id="add-new-frm" accept-charset="utf-8">
                <div class="form-group" style="display:none;">
                    <div class="input-group" style="width: 100%;">
                        <input id="taskid" name="edit[meta][post_id]" type="text" class="form-control" placeholder="Id">
                        <input id="check_complete" name="edit[meta][check_complete]" type="text" value="process" class="form-control" placeholder="check_complete">
                    </div>
                </div>
                <div class="form-group">
                    <strong>Tiêu đề</strong>
                    <div class="input-group" style="width: 100%;">
                            <input id="new-event" type="text" name ="edit[meta][post_title]" class="form-control" placeholder="Tiêu đề">
                    </div>
                </div>
                <div class="form-group">
                    <strong>Ngày bắt đầu</strong>
                    <div class="input-group" style="margin-bottom: 2%;">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                        <input type="text" id="fromdate" name="edit[meta][start_date]" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        <label id="fromdate-error" class="error" for="fromdate" style="display: none;"></label>
                    </div>

                    <strong>Ngày kết thúc</strong>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id ="todate" name="edit[meta][end_date]" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        <label id="enddate-error" class="error" for="todate" style="display: none;"></label>
                    </div>
                </div>
                <div class="form-group">
                <div class="form-group" style="float: left; margin-right: 5%;">
                    <label>
                        <input type="checkbox" id="sms_status" name="edit[meta][sms_status]" class="minimal" checked> SMS
                    </label>
                </div>
                <div class="form-group" id="radio-checked" style="margin-top: 3%;">
                            <label class="" style="margin-right: 5%;color: #f39c12">
                            <input type="radio" id="process" name="edit[meta][post_status]" class="minimal" checked="" value="on" style="position: absolute; opacity: 0;">
                            Đang thực hiện</label>
                            <label class="" style="color:#00a65a">
                            <input type="radio" id="complete" name="edit[meta][post_status]" class="minimal" value="off" style="position: absolute; opacity: 0;">
                            Hoàn thành</label>
                        </div>

                </div>
                <div class="form-group">
                    <div class="input-group" style="width: 100%;">
                        <textarea id="new-content" name ="edit[meta][post_content]" type="text" style="resize: none;" class="form-control" placeholder="Nội dung" rows="5"></textarea>
                    </div>
                </div>
                
                <div class="form-group" style="float: right;">
                        <button id="add-new-event" v-on:click="submit" type="button" style="background-color: #f39c12; border-color: #f39c12;" class="btn btn-primary btn-flat">Lưu</button>

                        <button id="refresh" v-on:click="add_new" type="button"  style="background-color: #696969; border-color: #696969;" class="btn btn-primary btn-flat">Làm mới</button>

                    </div>
                </div>
                </form>

            </div>

            </div>
        </div>
    </div>

    `
});

Vue.component('view-calender-component',{
    props:['term_id','checktask'],
    data:function(){
        post_data:null;
        admin_url:admin_url;
        checkdrop:true;
        defaultDate:null
    },
    computed:{

    },
    mounted:function(){
        this.firstload();
    },
    updated:function(){
        this.notify();
    },
    methods:{
        notify: function(msg,className){
            $.notify(msg,{
                className: className,
                clickToHide: true,
                autoHideDelay: 15000,
            });
        },
        firstload : function(){
            var task = this;
            var del = false;//check click span delete if click del=true finish return false line 358,381,387
            
            $('#calendar').fullCalendar({
                header    : {
                    left  : 'prev,next today',
                    center: 'title',
                    right : 'month'
                },
                buttonText: {
                    today: 'today',
                    month: 'month',
                    week : 'week',
                    day  : 'day'
                },
                timezone: 'Asia/Ho_Chi_Minh',
                events: function(start, end, timezone, callback) {
                    var get_all_url = admin_url + 'webbuild/tasks/get_all';
                   
                    $.post(get_all_url,
                        {term_id:  task.term_id},
                        function(response){
                            if(!response.success){
                                task.notify(response.msg, "error");
                                task.checkdrop = false;
                                return task.checkdrop;
                            }
                            var data = response.data;
                            var _data = [];
                            //convert object to array
                            Object.keys(data).forEach(function(key) {
                                _data.push(data[key]);
                                if(data[key]['post_status']!="complete") {
                                    if(task.defaultDate == null){
                                        task.defaultDate = data[key]['start_date'];
                                    }else{
                                        task.defaultDate = task.defaultDate < data[key]['start_date'] ? task.defaultDate : data[key]['start_date'];
                                    }
                                }
                            })
                            task.defaultDate = moment(moment.unix(task.defaultDate).toDate()).format('YYYY-MM-DD');
                            //save rows from database in task.postdata
                            task.post_data = _data;

                            var events=[];
                            task.post_data.forEach(e => {
                                var color = e.post_status =='complete'?'#00a65a':'#f39c12';
                                // var a = moment.unix(e.start_date).toDate();
                                // a.setHours(0,0,0);
                                // var b = moment.unix(e.end_date).toDate();
                                // b.setHours(23,59,59);
                                // console.log(a,b);
                                events.push({
                                    title          : e.post_title,
                                    start          : moment.unix(e.start_date).toDate(),
                                    end            : moment(moment.unix(e.end_date).toDate()).add(16,'hour').add(59,'minute').add(59,'second'),//1 su kien duoc xay ra tu ngay a den ngay b. Calendar se tinh toi 23:59:59 cua ngay b. ma 23:59:59 la ngay moi. Nen calerdar khong hien thi tu a toi b ma hien thi tu a den b-1(ngay) ngay enddate vi the phai + 1.
                                    backgroundColor: color,
                                    borderColor    : color,
                                    post_id        :e.post_id,
                                    allDay:true
                                });
                            });
                            callback(events);
                        }
                    )
                       
                },
                editable  : true,
                droppable : true,
                timeFormat: '(:)',
                eventDurationEditable : true,
                eventRender: function( event, element, view ) {
                    //add span delete for event
                    element.find('div.fc-content').append('<span  id="remove_'+event.post_id+'" ><i class="fa fa-remove" style="float:right;margin-right: 1%;"></i></span>');

                    element.find('#remove_'+event.post_id).on('click', function() {
                        del = true;
                        $('#myModal').modal('show');
                        $('#OK').on('click',function(){
                            var delete_url = admin_url + 'webbuild/tasks/delete';
                            $.post(
                                delete_url,
                                {
                                    term_id:  task.term_id,
                                    post_id:  event.post_id
                                },function(response){
                                    if(response.success){
                                        task.notify(response.msg, "success");
                                        $('#calendar').fullCalendar('refetchEvents');
                                    }
                                    
                                }
                            )
                            // delete item in task.post_data
                            task.post_data.forEach(e=>{
                                if(e.post_id == event.post_id){
                                    task.post_data.splice(task.post_data.indexOf(e), 1);
                                }
                            })
                            del = false;
                            event.post_id = null;
                        })
                    });
                },
                eventClick: function(calEvent, jsEvent, view) {
                    if(!del){
                        $('#box_title').text('Chỉnh sửa ' +' '+ calEvent.title);
                        task.post_data.forEach(e => {
                            if(e.post_id==calEvent.post_id){
                                var start = calEvent.start.format('DD/MM/YYYY');
                                var end = calEvent.end==null? start: moment(calEvent.end).add(-1,'day').add(1,'minute').format('DD/MM/YYYY');
                                $('#fromdate').val(start);
                                $('#todate').val(end);
                                $('#taskid').val(e.post_id);
                                $('#new-content').val(e.post_content);
                                $('#new-event').val(e.post_title);
                                if(e.post_status=="complete"){
                                    $('#complete').parent().trigger("click");
                                    $('#check_complete').val("complete");
                                    $('#process').attr('disabled',true);
                                    //$('#complete').attr('disabled',true);
                                }else{
                                    $('#check_complete').val("process");
                                    $('#process').removeAttr('disabled',false);
                                    $('#complete').attr('disabled',false);
                                    $('#process').parent().trigger("click");
                                   
                                }
                                
                                if(e.sms_status== 1){
                                    $('#sms_status').parent().addClass('checked');
                                    $('#sms_status').parent().removeClass('disabled') ;
                                }else if(e.sms_status == 2){
                                    $('#sms_status').parent().addClass('disabled') ;
                                    $('#sms_status').parent().addClass('checked');
                                }else{
                                    $('#sms_status').parent().removeClass('checked');
                                    $('#sms_status').parent().removeClass('disabled') ;
                                }

                            }
                        })
                    }

                },
                eventDrop: function(event, delta, revertFunc) {
                    task.post_data.forEach(e => {
                        if(e.post_id==event.post_id){
                            var start = event.start.format('DD/MM/YYYY');
                            var end = event.end==null? start: moment(event.end).add(-1,'day').add(1,'minute').format('DD/MM/YYYY');
                            $('#fromdate').val(start);
                            $('#todate').val(end);
                            $('#taskid').val(e.post_id);
                            $('#new-content').val(e.post_content);
                            $('#new-event').val(e.post_title);
                            if(e.post_status=="complete"){
                                $('#complete').parent().trigger("click");
                                $('#process').attr('disabled',true);
                                $('#check_complete').val("complete");
                                task.checkdrop = false;
                            }else{
                                $('#check_complete').val("process");
                                $('#process').removeAttr('disabled',false);
                                $('#complete').attr('disabled',false);
                                $('#process').parent().trigger("click");
                            }
                            if(e.sms_status== 1){
                                $('#sms_status').parent().addClass('checked');
                                $('#sms_status').parent().removeClass('disabled') ;
                            }else if(e.sms_status== 2){
                                $('#sms_status').parent().addClass('disabled') ;
                                $('#sms_status').parent().addClass('checked');
                            }else{
                                $('#sms_status').parent().removeClass('checked');
                                $('#sms_status').parent().removeClass('disabled') ;
                            }
                          $('#add-new-event').trigger("click"); 
                          if(!task.checkdrop){
                            revertFunc();
                          }
                          task.checkdrop = true;
                        }
                    })
                },
                eventResize :function( event, delta, revertFunc, jsEvent, ui, view ) {
                    task.post_data.forEach(e => {
                        if(e.post_id==event.post_id){
                            var start = event.start.format('DD/MM/YYYY');
                            var end = event.end==null? start: moment(event.end).add(-1,'day').add(1,'minute').format('DD/MM/YYYY');
                            $('#fromdate').val(start);
                            $('#todate').val(end);
                            $('#taskid').val(e.post_id);
                            $('#new-content').val(e.post_content);
                            $('#new-event').val(e.post_title);
                            if(e.post_status=="complete"){
                                $('#complete').parent().trigger("click");
                                $('#process').attr('disabled',true);
                                $('#check_complete').val("complete");
                                //$('#complete').attr('disabled',true);
                                task.checkdrop = false;
                            }else{
                                $('#check_complete').val("process");
                                $('#process').removeAttr('disabled');
                                $('#complete').attr('disabled',false);
                                $('#process').parent().trigger("click");
                            }
                            if(e.sms_status== 1){
                                $('#sms_status').parent().addClass('checked');
                                $('#sms_status').parent().removeClass('disabled') ;
                            }else if(e.sms_status== 2){
                                $('#sms_status').parent().addClass('disabled') ;
                                $('#sms_status').parent().addClass('checked');
                            }else{
                                $('#sms_status').parent().removeClass('checked');
                                $('#sms_status').parent().removeClass('disabled') ;
                            }
                            $('#add-new-event').trigger("click");
                            if(!task.checkdrop){
                                revertFunc();
                            }
                            task.checkdrop = true;
                        }
                    })
                },
                eventAllow:function(dropInfo, draggedEvent){
                    var start = dropInfo.start.format('DD/MM/YYYY');
                    var end = moment(dropInfo.end).add(-1,'day').format('DD/MM/YYYY');
                     
                    $('#fromdate').val(start);
                    $('#todate').val(end);
                }
            })
            // nếu có tác vụ thì sẽ hiển thị ngày tháng của tác vụ nếu không có thì show ngày hiện tại
            setTimeout(() => {
                $('#calendar').fullCalendar('gotoDate',task.defaultDate);
            }, 1000);
            
        },
    },
    template:`
    <div>
        <div class="box box-primary">
            <div class="box-body no-padding">
            <div id="calendar"></div>
            </div>
        </div>

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 id="modal-title">Chắt chắn xóa?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="Cancel" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-info" id="OK" data-dismiss="modal">Xác nhận</button>
                </div>
                </div>

            </div>
        </div>
    </div>
    `
});