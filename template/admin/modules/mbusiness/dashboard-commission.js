Vue.component('dashboard-commission-terms-component', {
    props: ['remote_url','heading'],
    data: function(){
        return { 
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            datasource : null,
        }
    },
    computed : {
        total_amount_not_vat : function(){
            if( ! this.datasource) return '';
            var i = 0;
            var result = 0;
            for (i; i < this.datasource.length; i++)
            {
                result += this.datasource[i].amount_not_vat;
            }
            return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(result));
        },

        total_actually_collected : function(){
            if( ! this.datasource) return '';
            var i = 0;
            var result = 0;
            for (i; i < this.datasource.length; i++)
            {
                result += this.datasource[i].actually_collected;
            }

            return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(result));
        },

        total_amount_commisssion : function(){
            if( ! this.datasource) return '';
            var i = 0;
            var result = 0;
            for (i; i < this.datasource.length; i++)
            {
                result += this.datasource[i].amount_commisssion;
            }

            return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(result));
        },
    },
    beforeUpdate: function(){
        this.destroyDatatable();
    },
    updated : function(){
        this.buildDatatable();
    },
    created : function(){

        var self = this;
        axios
        .get(this.remote_url)
        .then(function (response) {

            var response = response.data;
            if( ! response.status) { return false; }

            self.datasource = response.data;
        });
    },
    methods: {
        destroyDatatable: function(){
            if ($.fn.DataTable.isDataTable("#"+this.guid))
            {
                $("#"+this.guid).DataTable().destroy();
            }
        },
        buildDatatable : function(){

            var datasource = this.datasource;
            
            $("#"+this.guid).DataTable({
                columns: [
                    { data : "contract_code" },
                    { data : "start_date" },
                    { 
                      data : "amount_not_vat",
                      render: function ( data, type, row ) {

                        /* If display or filter data is requested, format the currency */ 
                        if( type === 'display')
                        {
                            return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                        }

                            /*
                            * Otherwise the data type requested (`type`) is type detection or
                            * sorting data, for which we want to use the raw date value, so just
                            * that, unaltered
                            */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "actually_collected",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "amount_commisssion",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                ],
                "language": {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Hiển thị _MENU_ kết quả mỗi trang",
                    "sZeroRecords":  "Không tìm thấy kết quả nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 kết quả",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm kiếm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Trang đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Trang cuối"
                    }
                }
            });
        }
    },
    template: `
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><span v-text="heading"></span></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div>
                        <table v-bind:id="guid" class="table no-margin">
                            <thead>

                                <tr>
                                    <th>HỢP ĐỒNG THỰC HIỆN</th>
                                    <th>NGÀY THỰC HIỆN</th>
                                    <th>
                                        DOANH SỐ <br/> <span class="text-red" v-text="total_amount_not_vat"></span>
                                    </th>
                                    <th>D.THU TÍNH THƯỞNG <br/> <span class="text-red" v-text="total_actually_collected"></span></th>
                                    <th>THƯỞNG <br/> <span class="text-red" v-text="total_amount_commisssion"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <template v-if="datasource != null">
                                    <tr v-for="item in datasource" v-if="item.amount_not_vat > 0">
                                        <td v-text="item.contract_code"></td>
                                        <td v-text="item.start_date"></td>
                                        <td class="text-right" v-text="item.amount_not_vat"></td>
                                        <td class="text-right" v-text="item.amount_kpi"></td>
                                        <td class="text-right" v-text="item.amount_kpi*item.commission_rate"></td>
                                    </tr>
                                </template>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    `
});