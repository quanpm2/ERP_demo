Vue.component('dashboard-commission-overall-component', {
    props: {
        user_id : {
            type : Number,
            default : ''
        },
        month : {
            type : Number,
            default : ''
        },
    },
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            data : null,
            rank : null,
        };
    },
    computed : {
        remote_url : function(){
            return base_url + 'admin/mbusiness/ajax/commission/user/'+ this.user_id +'?month='+this.month; 
        },
        remote_get_rank_url : function(){
            return base_url + 'admin/mbusiness/ajax/commission/user_rank/' + this.user_id +'?month=' + this.month;
        }
    },
    created : function(){
        this.onLoad();
    },
    methods: {
        numberFormat : function(amount){
            if( ! amount) return '---';
            return (new Intl.NumberFormat('vi-VN').format(Math.round((amount/1000))*1000));
        },
        onLoad: function(){

            self = this;

            axios
            .get(this.remote_url)
            .then(function (response) {

                var response = response.data;
                if( ! response.status) { return false; }
                self.data = response.data;
            });

            axios
            .get(this.remote_get_rank_url)
            .then(function (response) {

                var response = response.data;
                if( ! response.status) { return false; }

                self.rank = response.data;
            });
        },

    },
    template: `
    <div :id="'mbusiness-ordermeeting-index-' + guid" class="row" v-if="data != null">
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h4><span v-text="numberFormat(data.direct_ticket.amount_not_vat)"></span> / <span>300.000.000<span></h4>
                    <p><i>Doanh thu trên <span v-text="numberFormat(data.direct_ticket.contract_num)"></span> HĐ</i></p>
                </div>
                <div class="icon">
                    <i class="fa fa-dollar"></i>
                </div>
                <a href="#" class="small-box-footer">
                DOANH SỐ KÝ / KPIS
                </a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <template>
                    <div class="inner">
                        <h4><span v-text="numberFormat(data.direct_ticket.adsplus_amount_commisssion)"></span> / <span v-text="numberFormat(data.direct_ticket.adsplus_actually_collected)"><span></h4>
                        <p><i><span v-text="data.direct_ticket.adsplus_contract_num"></span> HĐ</i></p>
                    </div>
                </template>
                <div class="icon">
                    <i class="fa fa-buysellads"></i>
                </div>
                <a href="#" class="small-box-footer">THƯỞNG/ D.SỐ THƯỞNG ADSPLUS</a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-green">

                <template>
                    <div class="inner">
                        <h4><span v-text="numberFormat(data.direct_ticket.webdoctor_amount_commisssion)"></span> / <span v-text="numberFormat(data.direct_ticket.webdoctor_actually_collected)"><span></h4>
                        <p><i><span v-text="data.direct_ticket.webdoctor_contract_num"></span> HĐ</i></p>
                    </div>
                </template>
               
                <div class="icon">
                    <i class="fa fa-code"></i>
                </div>
                <a href="#" class="small-box-footer">THƯỞNG/ D.SỐ THƯỞNG WEBDOCTOR</a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-12">
            <div class="col-lg-6 col-xs-12 no-padding">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">

                    <h4 title="Hoa hồng trực tiếp"> <i class="fa fa-fw fa-user"></i> + <span v-text="numberFormat(data.direct_ticket.amount_commisssion)"></span> </h4>
                    <h4 v-if="data.indirect_ticket" title="Thưởng trách nhiệm"> <i class="fa fa-fw fa-group"></i> + <span v-text="numberFormat(data.indirect_ticket.amount_commisssion)"></span> </h4>
                    <h4 v-else title="Thưởng trách nhiệm">% HH trực tiếp</h4>

                </div>
                <div class="icon">
                    <i class="fa fa-gift"></i>
                </div>
                <a href="#" class="small-box-footer"> THƯỞNG THÁNG 3 </a>
            </div>
        </div>

        <div v-if="rank != null" class="col-lg-6 col-xs-12 no-padding">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h4><b><span v-text="rank.index"></span></b> / <span v-text="rank.total"></span></h4>
                    <p>của phòng KD</p>
                </div>
                <div class="icon">
                    <i class="fa fa-star-o"></i>
                </div>
                <a href="#" class="small-box-footer">
                XẾP HẠNG
                </a>
            </div>
        </div>
        </div>
    </div>
    <!-- /.row -->
    `
});

