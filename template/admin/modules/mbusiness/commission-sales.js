Vue.component('commission-sales-component', {
    data: function(){
        return { 
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            datasource : null,
            remote_url : admin_url + 'mbusiness/ajax/commission/users',
            heading : 'Xếp hạng nhân viên kinh doanh'
        }
    },
    watch : {
        datasource : function(){
            this.destroyDatatable();
            this.buildDatatable();
        }
    },
    computed : {
        total_amount_not_vat : function(){
            if( ! this.datasource) return '';
            var i = 0;
            var result = 0;
            for (i; i < this.datasource.length; i++)
            {
                result += this.datasource[i].amount_not_vat;
            }
            return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(result));
        },

        total_actually_collected : function(){
            if( ! this.datasource) return '';
            var i = 0;
            var result = 0;
            for (i; i < this.datasource.length; i++)
            {
                result += this.datasource[i].actually_collected;
            }

            return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(result));
        },

        total_amount_commisssion : function(){
            if( ! this.datasource) return '';
            var i = 0;
            var result = 0;
            for (i; i < this.datasource.length; i++)
            {
                result += this.datasource[i].amount_commisssion;
            }

            return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(result));
        },
    },
    beforeUpdate: function(){
        this.destroyDatatable();
    },
    updated : function(){
        this.buildDatatable();
    },
    created : function(){

        var self = this;
        axios
        .get(this.remote_url)
        .then(function (response) {

            var response = response.data;
            if( ! response.status) { return false; }

            self.datasource = response.data;
        });
    },
    methods: {
        destroyDatatable: function(){
            if ($.fn.DataTable.isDataTable("#"+this.guid))
            {
                $("#"+this.guid).DataTable().destroy();
            }
        },
        buildDatatable : function(){

            // $.fn.dataTable.ext.errMode = 'none';
            $("#"+this.guid).DataTable({
                'order':  [0, 'asc'],
                data: this.datasource,
                "autoWidth": true,
                columns: [
                    { 
                        data : 'rank',
                        render: function ( data, type, row)
                        {
                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                let rank    = '';

                                switch (true) {
                                    case row.rank > 0 && row.rank <= 2 : 
                                        rank = '<small class="label pull-right bg-blue">'+row.rank+'</small>';
                                        break;

                                    case row.rank > 2 && row.rank <= 10 : 
                                        rank = '<small class="label pull-right bg-green">'+row.rank+'</small>';
                                        break;

                                    case row.rank > 10 && row.rank <= 20 : 
                                        rank = '<small class="label pull-right bg-yellow">'+row.rank+'</small>';
                                        break;

                                    default : 
                                        rank = '<small class="label pull-right bg-red">'+row.rank+'</small>';
                                        break;        
                                }

                                return '<a href="'+ admin_url + 'mbusiness/commission/' + row.user_id+'" target="_blank"><img src="' + row.user_avatar + '" class="img-circle" alt="User Image" width="50" height="50">'+rank+'</a>';
                            }

                            /*
                            * Otherwise the data type requested (`type`) is type detection or
                            * sorting data, for which we want to use the raw date value, so just
                            * that, unaltered
                            */
                            return data;
                        },
                    },
                    { data : "display_name", width:"100px" },
                    { data : "department" },
                    { data : "user_group" },
                    { data : "contract_num" },
                    { 
                      data : "amount_not_vat",
                      render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                            * Otherwise the data type requested (`type`) is type detection or
                            * sorting data, for which we want to use the raw date value, so just
                            * that, unaltered
                            */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                      data : "amount_kpi",
                      render: function ( data, type, row )
                      {
                        /* If display or filter data is requested, format the currency */ 
                        if( type === 'display')
                        {
                            return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                        }

                            /*
                            * Otherwise the data type requested (`type`) is type detection or
                            * sorting data, for which we want to use the raw date value, so just
                            * that, unaltered
                            */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "actually_collected",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "adsplus_amount_commisssion",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "webdoctor_amount_commisssion",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "amount_commisssion",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(data));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                ],

                "language": {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Hiển thị _MENU_ kết quả mỗi trang",
                    "sZeroRecords":  "Không tìm thấy kết quả nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 kết quả",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm kiếm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Trang đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Trang cuối"
                    }
                }
            });
        },
        numberFormat : function(amount){
            if( ! amount) 
            {
                return '---';
            }
            return new Intl.NumberFormat('vi-VN', { maximumSignificantDigits: 3 }).format(amount) + 'đ';
        },
    },
    template: `
    <div class="col-md-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><span v-text="heading"></span></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div>
                        <table v-bind:id="guid" class="table no-margin">
                            <thead>
                                <tr>
                                    <th>Xếp hạng</th>
                                    <th>NVKD</th>
                                    <th>Phòng ban</th>
                                    <th>Nhóm</th>
                                    <th>HĐ</th>
                                    <th>Doanh số</th>
                                    <th>DT KPIS</th>
                                    <th>DT thưởng</th>
                                    <th>Thưởng Adsplus</th>
                                    <th>Thưởng Webdoctor</th>
                                    <th>Thưởng</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    `
});