Vue.component('commission-contracts-component', {
    props: {
        month : {
            type : Number,
            default : ''
        },
        c_type : {
            type : String,
            default : ''
        },
        user_id : {
            type : Number,
            default : ''
        },
        heading : Array
    },
    data: function(){
        return { 
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            contracts : null,
        }
    },
    computed : {
        remote_url : function(){

            let params = {};

            if(this.month != '') params['month'] = this.month;
            if(this.c_type != '') params['commission_type'] = this.c_type;
            if(this.user_id != '') params['user_id'] = this.user_id;

            return admin_url + 'mbusiness/ajax/commission/contracts?' + $.param(params);
        },

        total_amount_not_vat : function(){

            if( ! this.contracts) return '';
            var i = 0;
            var result = 0;
            
            for (i; i < this.contracts.length; i++)
            {
                result += this.contracts[i].amount_not_vat;
            }
            
            return (new Intl.NumberFormat('vi-VN').format(Math.round((result/1000))*1000));
        },

        total_amount_kpi : function(){

            if( ! this.contracts) return '';
            var i = 0;
            var result = 0;
            
            for (i; i < this.contracts.length; i++)
            {
                result += this.contracts[i].amount_kpi;
            }
            
            return (new Intl.NumberFormat('vi-VN').format(Math.round((result/1000))*1000));
        },
        
        total_amount_commisssion : function(){
            if( ! this.contracts) return '';
            var i = 0;
            var result = 0;
            for (i; i < this.contracts.length; i++)
            {
                result += this.contracts[i].amount_kpi*this.contracts[i].commission_rate;
            }

            
            return (new Intl.NumberFormat('vi-VN').format(Math.round((result/1000))*1000));
        },
    },
    beforeUpdate: function(){ this.destroyDatatable(); },
    updated : function(){ this.buildDatatable(); },
    created : function(){

        var self = this;
        axios
        .get(this.remote_url)
        .then(function (response) {

            var response = response.data;
            if( ! response.status) { return false; }

            self.contracts = response.data;
        });
    },
    methods: {
        destroyDatatable: function(){
            if ($.fn.DataTable.isDataTable("#"+this.guid))
            {
                $("#"+this.guid).DataTable().destroy();
            }
        },
        buildDatatable : function(){

            var contracts = this.contracts;
            
            $("#"+this.guid).DataTable({
                columns: [
                    { data : "contract_code" },
                    { data : "start_date" },
                    { 
                      data : "amount_not_vat",
                      render: function ( data, type, row ) {

                        /* If display or filter data is requested, format the currency */ 
                        if( type === 'display')
                        {
                            return (new Intl.NumberFormat('vi-VN').format(Math.round(data/1000)*1000));
                        }

                            /*
                            * Otherwise the data type requested (`type`) is type detection or
                            * sorting data, for which we want to use the raw date value, so just
                            * that, unaltered
                            */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "actually_collected",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN').format(Math.round(data/1000)*1000));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "commission_rate",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN').format(data)) + '%';
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                    { 
                        data : "amount_commisssion",
                        render: function ( data, type, row ) {

                            /* If display or filter data is requested, format the currency */ 
                            if( type === 'display')
                            {
                                return (new Intl.NumberFormat('vi-VN').format(Math.round(data/1000)*1000));
                            }

                            /*
                             * Otherwise the data type requested (`type`) is type detection or
                             * sorting data, for which we want to use the raw date value, so just
                             * that, unaltered
                             */
                            return data;
                        },
                        "sClass" : 'text-right'
                    },
                ],
                "pageLength": 100,
                "language": {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Hiển thị _MENU_ kết quả mỗi trang",
                    "sZeroRecords":  "Không tìm thấy kết quả nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 kết quả",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm kiếm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Trang đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Trang cuối"
                    }
                }
            });
        }
    },
    template: `
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <table v-bind:id="guid" class="table no-margin">
                            <thead>

                                <tr>
                                    <th>HỢP ĐỒNG THỰC HIỆN</th>
                                    <th>NGÀY THỰC HIỆN</th>
                                    <th>
                                        DOANH SỐ <br/> <span class="text-red" v-text="total_amount_not_vat"></span>
                                    </th>
                                    <th>D.THU TÍNH THƯỞNG <br/> <span class="text-red" v-text="total_amount_kpi"></span></th>
                                    <th>(%) hoa hồng <br/> <span class="text-red"></span></th>
                                    <th>THƯỞNG <br/> <span class="text-red" v-text="total_amount_commisssion"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <template v-if="contracts != null">
                                    <tr v-for="item in contracts" v-if="item.amount_not_vat > 0">
                                        <td>
                                            {{item.contract_code}} <br/>
                                            <small>
                                                <span class='text-muted col-xs-12' data-toggle='tooltip' title='Vai trò'><i class='text-yellow fa fa-fw fa-star'></i>{{item.user.display_name}} - {{item.user.role_name}}</span>

                                                <span class='text-muted col-xs-12' data-toggle='tooltip' title='Nhóm'><i class='text-success fa fa-fw fa-users'></i>{{item.user.user_group}} - {{item.user.department}}</span>
                                            </small>
                                        </td>
                                        <td v-text="item.start_date"></td>
                                        <td class="text-right" v-text="item.amount_not_vat"></td>
                                        <td class="text-right" v-text="item.amount_kpi"></td>
                                        <td class="text-right" v-text="item.commission_rate*100"></td>
                                        <td class="text-right" v-text="item.amount_kpi*item.commission_rate"></td>
                                    </tr>
                                </template>
                            </tbody>
                        </table>
        </div>
    </div>
    `
});