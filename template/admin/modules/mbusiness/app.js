Vue.component('ordermeeting-datatable-component', {
    props : ['remote_url'],
    data: function(){
        return {
            guid: 'xxxxxxxx-xxxx-9xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) { var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);}),
            exportFileUrl: base_url + 'admin/mbusiness/ajax/dataset/ordermeeting',
            isLoading: false,
            action : null,
            action_url : null,
            modalSetting : {
                action : null,
                action_url : null,
                heading : null,
                description : null
            },
            targetDOM : null,
        };
    },
    methods: {
        
        dataChanged: function(value)
        {
            this.bindContentPopover();
            this.bindConfirmOrder();
            this.bindDelete();
        },

        modalSubmit : function(){

            if(this.modalSetting.action_url == null) return false;

            var seft = this;
            axios
            .get(this.modalSetting.action_url)
            .then(function (response) {
                var _data = response.data;
                $.notify(_data.msg, {className: (_data.status ? 'success' : 'error'), clickToHide: true, autoHideDelay: 15000,});
                $('#mbusiness-ordermeeting-actions-modal-' + seft.guid).modal('toggle')

                if(_data == false) return true;

                switch (seft.modalSetting.action) {

                    case 'confirm' : 
                        seft.targetDOM.hide();
                        break;

                    case 'delete' : 
                        seft.targetDOM.closest('tr').hide();
                        break;

                    default: /*do nothing*/
                }
            });
        },

        bindConfirmOrder : function(){
            
            var seft = this;

            /* Bind click event listener to all new confirm buttons rendered */
            $('#mbusiness-ordermeeting-index-' + this.guid + ' .confirm-order-metting').off('click')
            .on('click',function(e){

                seft.modalSetting.action_url    = $(this).attr('href');
                seft.modalSetting.title         = 'Xác nhận duyệt lịch hẹn khách hàng';
                seft.modalSetting.description   = 'Bạn có đồng ý xác nhận lịch hẹn là hợp lệ ? sau khi xác nhận hệ thống sẽ bắt đầu tính KPI cho kinh doanh phụ trách !';
                seft.modalSetting.action        = 'confirm';

                seft.targetDOM = $(this);

                $('#mbusiness-ordermeeting-actions-modal-' + seft.guid).modal();

                e.preventDefault();
                return false;
            });
        },

        bindDelete : function(){

            var seft = this;

            /* Bind click event listener to all new confirm buttons rendered */
            $('#mbusiness-ordermeeting-index-' + this.guid + ' .delete-order-metting').off('click')
            .on('click',function(e){

                seft.modalSetting.action_url    = $(this).attr('href');
                seft.modalSetting.title         = 'Xác nhận xóa lịch hẹn khách hàng';
                seft.modalSetting.description   = 'Bạn có đồng ý xóa lịch hẹn ? hệ thống sẽ loại bỏ và không tính điểm KPI cho lịch hẹn này !';
                seft.modalSetting.action        = 'delete';

                seft.targetDOM = $(this);

                $('#mbusiness-ordermeeting-actions-modal-' + seft.guid).modal();

                e.preventDefault();
                return false;
            });
        },

        bindContentPopover : function(){ $('#mbusiness-ordermeeting-index-' + this.guid + ' [data-toggle="popover"]').popover(); },

        exportFile: function(e){

            if(this.isLoading) {
                $.notify('Hệ thống đang xử lý ,...', "warning");
                return;
            }

            this.isLoading = true;
            var seft = this;
            
            axios
            .get(this.exportFileUrl, { params: {user_id : this.checked_users }})
            .then(function (response) {
                var response = response.data;
                if( ! response.status)
                {
                    $.notify(response.msg, "error");
                    seft.isLoading = false;
                    return false;
                }

                $.notify(response.msg,"success");
                seft.isLoading = false;

                window.open(base_url + response.data, '_seft');
                return true;
            });
        }
    },
    template: `
    <div :id="'mbusiness-ordermeeting-index-' + guid">
        <datatable-builder-component is_filtering="false" is_ordering="true" per_page="50" :remote_url="remote_url" is_download="1" v-on:data_changed="dataChanged"></datatable-builder-component>
        <!-- Modal -->
        <div class="modal fade" :id="'mbusiness-ordermeeting-actions-modal-' + guid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" v-text="modalSetting.title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
              </div>
              <div class="modal-body" v-html="modalSetting.description"></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" v-on:click="modalSubmit">Xác nhận</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy bỏ</button>
              </div>
            </div>
          </div>
        </div>
        <div
    </div>
    `
});

var app_root = new Vue({
    el: '#app-container'
});