export const adWarehouseModule = {
    namespaced: true,
    state: () => ({
        datatable: [],

        ad_account_selected: {},
        ad_business_manager_selected: {},
    }),
    getters: {
        
    },
    actions: {
        find_ad_account({state, commit}, ad_account_id){
            let ad_account = _.find(state.datatable, item => {
                return _.get(item, "ad_account_id") == ad_account_id;
            });

            commit("set_ad_account_selected", ad_account);
        },
        find_ad_business_manager({state, commit}, ad_business_manager_id){
            let ad_business_manager = _.find(state.datatable, item => {
                return _.get(item, "ad_business_manager_id") == ad_business_manager_id;
            });

            commit("set_ad_business_manager_selected", ad_business_manager);
        }
    },
    mutations: {
        set_datatable(state, data) {
            state.datatable = data
        },
        set_ad_account_selected(state, data) {
            state.ad_account_selected = data
        },
        set_ad_business_manager_selected(state, data) {
            state.ad_business_manager_selected = data
        }
    },
};
