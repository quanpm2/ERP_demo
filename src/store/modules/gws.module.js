export const gwsModule = {
    namespaced: true,
    state: () => ({
        datatable: [],

        contract_selected: {},
    }),
    getters: {
        
    },
    actions: {
        find_contract({state, commit}, contract_id){
            let contract = _.find(state.datatable, item => {
                return _.get(item, "term_id") == contract_id;
            });

            commit("set_contract_selected", contract);
        },
        
        remove_contract({state, commit}, contract_id){
            let contract_index = _.findIndex(state.datatable, item => {
                return _.get(item, "term_id") == contract_id;
            });

            commit("remove_contract", contract_index);
        }
    },
    mutations: {
        set_datatable(state, data) {
            state.datatable = data
        },
        
        set_contract_selected(state, data) {
            state.contract_selected = data
        },

        remove_contract(state, index){
            let datatable = _.clone(state.datatable);
            _.remove(datatable, (item, _index) => (_index == index));

            state.datatable = datatable;
        }
    },
};
