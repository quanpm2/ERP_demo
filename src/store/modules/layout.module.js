export const layoutModule = {
    namespaced: true,
    state: () => ({
        is_expand_dialog: false,
    }),
    getters: {
        
    },
    actions: {
    },
    mutations: {
        set_is_expand_dialog(state, status) {
            state.is_expand_dialog = status
        },
    },
};
