export const dimensionDashboardModule = {
    namespaced: true,
    state: () => ({
        dimension: null,
    }),
    actions: {
        setDimension({ commit }, dimension) {
            commit("setDimensionState", dimension);
        }
    },
    mutations: {
        setDimensionState(state, dimension) {
            state.dimension = dimension
        },
    },
};
