import Vue from "vue/dist/vue.esm.js";
import Vuex from "vuex/dist/vuex.esm.js";
import { adWarehouseModule } from "./modules/adWarehouse.module";
import { layoutModule } from "./modules/layout.module";
import { gwsModule } from "./modules/gws.module";
import { dimensionDashboardModule } from "./modules/dimensionDashboard.module";
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        adWarehouse: adWarehouseModule,
        layout: layoutModule,
        gws: gwsModule,
        dimensionDashboard: dimensionDashboardModule,
    },
});