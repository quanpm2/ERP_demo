export const contractConfig = { valueOf }

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function valueOf(value) {

    const states = {
        draft : 'Đang soạn',
        unverified : 'Chưa cấp số',
        waitingforapprove : 'Cấp số',
        pending : 'Ngưng hoạt động',
        publish : 'Đang thực hiện',
        ending : 'Đã kết thúc',
        liquidation : 'Thanh lý',
        remove : 'Hủy bỏ'
    }
    
    return _.get(states, value)
}