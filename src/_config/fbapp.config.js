export const fbAppConfig = { valueOf }

/**
 * Get VAlue of Faceboook app
 *
 * @param      {<type>}  value   The value
 * @return     {<type>}  { description_of_the_return_value }
 */
function valueOf(value) {

    const app = {
        app_id: 871025543430030,
        graph_version: "v14.0"
    }
    
    return _.get(app, value)
}