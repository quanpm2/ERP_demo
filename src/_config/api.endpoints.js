export const ApiEndpoints = { valueOf }

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function valueOf(value) {

    const states = {
        googleads : `${admin_url}/googleads/` ,
        facebookads : `${base_url}api-v2/facebookads/`,
        googleadsV2 : `${base_url}api-v2/googleads/`,
        tiktokads : `${base_url}api-v2/tiktokads/`,
        zaloads : `${base_url}api-v2/zaloads/`,
        customer : `${base_url}api-v2/customer/`,
        adWarehouse: `${base_url}api-v2/contract/DatasetAdWarehouseIE/`,
        gws: `${base_url}api-v2/gws/`,
        factWarehouseSummary: `${base_url}api-v2/contract/DatasetAdWarehouseSummary/`
    }
    
    return _.get(states, value)
}