import { responseService } from "./response.service";

export const systemService = {
    getExchangeRate,
    
    getManipulationLock,
    setManipulationLock,
};

/**
 * Get by staff id
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function getExchangeRate() {
    return responseService.fetchRetry(
        `${base_url}api-v2/system/resource/exchange_rate`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Get manipulation lock
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function getManipulationLock() {
    return responseService.fetchRetry(
        `${base_url}api-v2/system/resource/manipulation`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Toggle set manipulation lock
 *
 * @return     {Object}  the response
 */
function setManipulationLock(requestOptionsBody = {}) {
    return responseService.fetchRetry(
        `${base_url}api-v2/system/resource/manipulation`,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}