import { responseService } from './response.service'

const qs = require('qs');

export const contractOnewebService = {
    update,
    getConfig
}

/**
 * Update Contract
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function update(id, requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/oneweb/contracts/service/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the configuration.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The configuration.
 */
function getConfig(id) {
    
    return responseService.fetchRetry(`${base_url}oneweb/api/contract/config/id/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}