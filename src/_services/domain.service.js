import { responseService } from "./response.service";

export const domainService = {
    getContract,
    getCustomerByContractId,
    startService,
    stopService,
};

/**
 * Get contract
 *
 * @return     {Object}  the response
 */
function getContract(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/domain/resource/contracts/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Get customer by contract id
 *
 * @return     {Object}  the response
 */
function getCustomerByContractId(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/domain/resource/customer/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Start service
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
 function startService(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/domain/resource/start_contract/${contractId}`,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}

/**
 * Start service
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function stopService(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/domain/resource/stop_contract/${contractId}`,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}
