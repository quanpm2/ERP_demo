import { responseService } from "./response.service";

export const oneadService = {
    getContract,
    getStaffs,
    getSetting,
    addKpi,
    removeKpi,
    startService,
    stopService,
};

/**
 * Get contract
 *
 * @return     {Object}  the response
 */
function getContract(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/onead/resource/index/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Get setting
 *
 * @return     {Object}  the response
 */
function getSetting(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/onead/resource/setting/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Get technical staffs
 *
 * @return     {Object}  the response
 */
function getStaffs() {
    return responseService.fetchRetry(
        `${base_url}api-v2/onead/resource/staffs`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Add technical staffs
 *
 * @return     {Object}  the response
 */
function addKpi(contractId, requestOptionsBody) {
    return responseService.fetchRetry(
        `${base_url}api-v2/onead/resource/kpi/${contractId}`,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Remove technical staffs
 *
 * @return     {Object}  the response
 */
function removeKpi(contractId, kpiId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/onead/resource/kpi/${contractId}/${kpiId}`,
        {
            method: "DELETE", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        1
    );
}

/**
 * Start contract
 *
 * @return     {Object}  the response
 */
function startService(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/onead/resource/start_contract/${contractId}`,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}

/**
 * Stop contract
 *
 * @return     {Object}  the response
 */
function stopService(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/onead/resource/stop_contract/${contractId}`,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}
