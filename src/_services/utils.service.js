import { responseService } from "./response.service";

export const UtilsService = {
    getExchangeRateByDate,
};

/**
 * Get exchange rate by date
 *
 * @return     {Object}  the response
 */
function getExchangeRateByDate(date_string) {
    return responseService.fetchRetry(
        `${base_url}api-v2/system/resource/exchange_rate_by_date?date=${date_string}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}