import { responseService } from "./response.service";

export const gwsService = {
    getContract,
    getCustomerByContractId,
    getTechStaffs,
    getStaffPerform,
    addTechStaff,
    removeTechStaff,
    startService,
    stopService,
};

/**
 * Get contract
 *
 * @return     {Object}  the response
 */
function getContract(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/gws/resource/contract_service/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Get customer by contract id
 *
 * @return     {Object}  the response
 */
function getCustomerByContractId(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/gws/resource/customer/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Get all tech by contract id
 *
 * @return     {Object}  the response
 */
function getTechStaffs() {
    return responseService.fetchRetry(
        `${base_url}api-v2/gws/resource/tech_staffs`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Get tech of contract by contract id
 *
 * @return     {Object}  the response
 */
function getStaffPerform(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/gws/resource/tech_staffs/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Add technical staffs
 *
 * @return     {Object}  the response
 */
function addTechStaff(contractId, kpiId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/gws/contract/add_tech_staff/${contractId}/${kpiId}`,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}

/**
 * Remove technical staffs
 *
 * @return     {Object}  the response
 */
function removeTechStaff(contractId, kpiId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/gws/contract/remove_tech_staff/${contractId}/${kpiId}`,
        {
            method: "DELETE", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        1
    );
}

/**
 * Start contract
 *
 * @return     {Object}  the response
 */
function startService(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/gws/contract/start_service/${contractId}`,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}

/**
 * Stop contract
 *
 * @return     {Object}  the response
 */
function stopService(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/gws/contract/stop_service/${contractId}`,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}
