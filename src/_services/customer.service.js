import { responseService } from './response.service'

import qs from 'qs'

export const customerService = {
    get,
    getOwned,
    isUniqueTin,
    isUniqueEmail,
    isUniquePhone,
    isUniqueWebPage,
    isVerifyEmail,
    create,
    update,
    search,

    createWebsite,
    updateWebsite,
    deleteWebsite,

    list_tax_code,
    find_company_by_tax_code,
    get_config,
};

/**
 * Get one
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function get(id) {
    return responseService.fetchRetry(`${base_url}api-v2/customer/index/view/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Get many
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function getOwned(query) {

    return responseService.fetchRetry(`${base_url}api-v2/customer/index/owned` + (!_.isEmpty(query) ? `?q=${query}` : ''), {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 2)
}

/**
 * Determines whether the specified value is unique tin.
 *
 * @param      {<type>}   value   The value
 * @return     {boolean}  True if the specified value is unique tin, False otherwise.
 */
function isUniqueTin(value, ignoreId) {

    const params = qs.stringify({ 'value': value, 'ignoreId': ignoreId })
    return responseService.fetchRetry(`${base_url}api-v2/customer/index/is_unique_tin/${value}?${params}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Determines whether the specified value is unique tin.
 *
 * @param      {<type>}   value   The value
 * @return     {boolean}  True if the specified value is unique tin, False otherwise.
 */
function isUniqueWebPage(value, ignoreId) {
    const params = qs.stringify({ 'value': value, 'id': ignoreId })
    return responseService.fetchRetry(`${base_url}api-v2/customer/index/is_unique_webpage?${params}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Determines if unique email.
 *
 * @param      {<type>}   value   The value
 * @param      {<type>}   type    The type
 * @return     {boolean}  True if unique email, False otherwise.
 */
function isUniqueEmail(value, type, ignoreId) {

    const params = qs.stringify({ 'value': value, 'type': type, 'ignoreId': ignoreId })
    return responseService.fetchRetry(`${base_url}api-v2/customer/index/is_unique_email?${params}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Determines if verify email
 *
 * @param      {<type>}   value   The value
 * @param      {<type>}   type    The type
 * @return     {boolean}  True if unique email, False otherwise.
 */
 function isVerifyEmail(value, type, ignoreId) {

    const params = qs.stringify({ 'value': value, 'type': type, 'ignoreId': ignoreId })
    return responseService.fetchRetry(`${base_url}api-v2/customer/index/is_verify_email?${params}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 2)
}

/**
 * Determines if unique phone.
 *
 * @param      {<type>}   value   The value
 * @param      {<type>}   type    The type
 * @return     {boolean}  True if unique phone, False otherwise.
 */
function isUniquePhone(value, type, ignoreId) {
    
    const params = qs.stringify({ 'value': value, 'type': type, 'ignoreId': ignoreId })
    return responseService.fetchRetry(`${base_url}api-v2/customer/index/is_unique_phone?${params}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Create new Customer
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function create(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/customer/index/create`
    return responseService.fetchRetry(endpoint, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        redirect: 'follow',
        referrer: 'no-referrer',
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Create new Customer
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function update(id, requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/customer/index/update/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        redirect: 'follow',
        referrer: 'no-referrer',
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Creates a website.
 *
 * @param      {<type>}  requestOptionsBody  The request options body
 * @return     {<type>}  { description_of_the_return_value }
 */
function createWebsite(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/customer/Resource/website`
    return responseService.fetchRetry(endpoint, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        redirect: 'follow',
        referrer: 'no-referrer',
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Update a website
 *
 * @param      {<type>}  id                  The identifier
 * @param      {<type>}  requestOptionsBody  The request options body
 * @return     {<type>}  { description_of_the_return_value }
 */
function updateWebsite(id, requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/customer/Resource/website/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        redirect: 'follow',
        referrer: 'no-referrer',
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Delete the website
 *
 * @param      {<type>}  id                  The identifier
 * @param      {<type>}  requestOptionsBody  The request options body
 * @return     {<type>}  { description_of_the_return_value }
 */
function deleteWebsite(customerId, websiteId)
{
    let endpoint = `${base_url}api-v2/customer/Resource/website/${customerId}/${websiteId}`
    return responseService.fetchRetry(endpoint, {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        redirect: 'follow',
        referrer: 'no-referrer'
    }, 1)
}

/**
 * Searches for the first match.
 *
 * @param      {<type>}  payload  The payload
 * @return     {<type>}  { description_of_the_return_value }
 */
function search(payload) {
    
    const params = qs.stringify(payload)
    return responseService.fetchRetry(`${base_url}api-v2/customer/AdvancedFiltering/search?${params}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * List tax code
 *
 * @param      {<type>}  filters  The filters
 * @return     {<type>}  { description_of_the_return_value }
 */
 function list_tax_code(filters) {
    const params = qs.stringify(filters)
    return responseService.fetchRetry(`${base_url}api-v2/customer/resource/tax_code?${params}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Find tax code
 *
 * @param      {<type>}  taxCode  The tax code
 * @return     {<type>}  { description_of_the_return_value }
 */
 function find_company_by_tax_code(taxCode) {
    return responseService.fetchRetry(`${base_url}api-v2/customer/resource/find_company_by_tax_code/${taxCode}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Find tax code
 *
 * @param      {<type>}  taxCode  The tax code
 * @return     {<type>}  { description_of_the_return_value }
 */
 function get_config() {
    return responseService.fetchRetry(`${base_url}api-v2/customer/config`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}