import { responseService } from './response.service'
export const requestService = { get}

/**
 * Get OrderMeeting
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function get(endPoint)
{
    return responseService.fetchRetry(endPoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}