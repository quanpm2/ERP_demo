import { responseService } from './response.service'

export const  permissionService = {
    can
};

/**
 * Check permission
 *
 * @param      {<type>}  permission  The Permission
 * @param      {<type>}  contractId  The contract Id
 * @return     {Object}  the response
 */
function can(permission, contractId) {

    let args = [ `${base_url}api-v2/staffs/permissions/can`, permission ]

    if(_.gt(contractId, 0)) args.push( contractId )

    const endPoint = _.join(args, '/')

    return responseService.fetchRetry(endPoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Get many
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function getOwned(query) {

    return responseService.fetchRetry(`${base_url}api-v2/customer/index/owned` + (!_.isEmpty(query) ? `?q=${query}` : ''), {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 2)
}