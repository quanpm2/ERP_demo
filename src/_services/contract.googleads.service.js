import _ from 'lodash';
import { responseService } from './response.service'

const qs = require('qs');

export const contractGoogleadsService = {
    get,
    getOverview,
    getActivatedByCid,
    getAccountCost,
    isValidStartDate, 
    getRelatedByCidGet,
    stopService,
    startService,
    getCids,
    getConfig,
    getOption,
    getExchangeRate,
    update,

    getAdAccounts,
    updateConfiguration,
    getAuthentizationUri,
    computeMetrics,

    /**
     * Get Related Contract by Contract Id
     */
    getRelatedContracts,

    /**
     * Get Segment Mcm Account Insight Data
     */
    getSegmentInsight,
    getAdAccountCost,
    join,
    rejoin,
    unjoin,
    addMcmAccount,

    toggleLockManipulation,
};

/**
 * Gets the segment insight.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The segment insight.
 */
 function getOverview(id) {
    
    return responseService.fetchRetry(`${base_url}api-v2/googleads/contract/overview/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the segment insight.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The segment insight.
 */
function getSegmentInsight(id) {
    
    return responseService.fetchRetry(`${base_url}api-v2/googleads/data/segments/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getRelatedContracts(id) {
    
    return responseService.fetchRetry(`${base_url}api-v2/googleads/data/contracts_related/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function get(id) {

    return responseService.fetchRetry(`${base_url}api-v2/googleads/contracts/index/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}


/**
 * Gets the cids.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The cids.
 */
function getCids(id) {

    return responseService.fetchRetry(`${base_url}api-v2/googleads/contracts/cids`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the cids.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The cids.
 */
function getAdAccounts(id) {
    let url = `${base_url}api-v2/googleads/McmAccounts`;
    if(!_.isEmpty(id)){
        url += `?account_id=${id}`;
    }

    return responseService.fetchRetry(url, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getActivatedByCid(id) {

    return responseService.fetchRetry(`${base_url}api-v2/googleads/contracts/activated_by_cid/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the related by cid get.
 *
 * @param      {<type>}  cid      The cid
 * @param      {<type>}  payload  The payload
 * @return     {<type>}  The related by cid get.
 */
function getRelatedByCidGet(cid, requestOptionsBody)
{
    let _qs = qs.stringify(requestOptionsBody);
    
    return responseService.fetchRetry(`${base_url}api-v2/googleads/contracts/related_by_cid/${cid}?${_qs}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}


/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function isValidStartDate(id, cid, time) {

    return responseService.fetchRetry(`${base_url}api-v2/googleads/contracts/is_valid_start_date/${id}/${cid}/${time}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getAccountCost(id, requestOptionsBody) {

    let _qs = qs.stringify(requestOptionsBody);
    return responseService.fetchRetry(`${base_url}api-v2/googleads/data/account_cost/${id}?${_qs}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getAdAccountCost(adaccount_id, requestOptionsBody) {

    let _qs = qs.stringify(requestOptionsBody);
    return responseService.fetchRetry(`${base_url}api-v2/googleads/data/adaccount_cost/${adaccount_id}?${_qs}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function join(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/googleads/contracts/join`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function rejoin(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/googleads/contracts/rejoin`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function stopService(id, requestOptionsBody)
{

    let endpoint = `${base_url}api-v2/googleads/contracts/stop_service/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function startService(id, requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/googleads/contracts/proc_service/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getConfig(item = '') {

    return responseService.fetchRetry(`${base_url}api-v2/googleads/config/items/${item}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getOption(item = '') {

    return responseService.fetchRetry(`${base_url}api-v2/googleads/config/options/${item}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
 function getExchangeRate() {
    return responseService.fetchRetry(`${base_url}api-v2/googleads/config/exchange_rate`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * { function_description }
 *
 * @param      {<type>}  id                  The identifier
 * @param      {<type>}  requestOptionsBody  The request options body
 * @return     {<type>}  { description_of_the_return_value }
 */
function updateConfiguration(id, requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/googleads/contracts/configurations/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Update Contract
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function update(id, requestOptionsBody)
{

    let endpoint = `${base_url}api-v2/googleads/contracts/index/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Update Contract
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function computeMetrics(id)
{
    let endpoint = `${base_url}api-v2/googleads/contract/adaccount_insight/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
    }, 1)
}

/**
 * Gets the authentization uri.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The authentization uri.
 */
function getAuthentizationUri(id) {

    return responseService.fetchRetry(`${base_url}api-v2/googleads/McmAccounts/authentization_uri/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * addMcmAccount
 *
 * @param      {<type>}  requestOptionsBody
 */
function addMcmAccount(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/googleads/McmAccounts`
    return responseService.fetchRetry(endpoint, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Unjoin contract
 *
 * @param      {string}  original_contract_id
 * @param      {string}  unjoin_contract_id
 * 
 * @return     {object} JSON response
 */
function unjoin(original_contract_id, unjoin_contract_id)
{
    let requestOptionsBody = {
        original_contract_id,
        unjoin_contract_id
    };

    let endpoint = `${base_url}api-v2/googleads/contracts/unjoin`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * lock_manipulation
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The authentization uri.
 */
function toggleLockManipulation(id, requestOptionsBody = null) {
    return responseService.fetchRetry(`${base_url}api-v2/googleads/contracts/toggle_lock_manipulation/${id}`, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}