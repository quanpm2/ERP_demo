import { responseService } from "./response.service";

export const staffService = {
  getStaffById,
  getDepartment,
  getUserGroup,
  getRole,
  getConfig,
  updateStaff,
};

/**
 * Get by staff id
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function getStaffById(staffId) {
  return responseService.fetchRetry(
    `${base_url}api-v2/staffs/resource/user?id=${staffId}`,
    {
      method: "GET", // *GET, POST, PUT, DELETE, etc.
      headers: { "Content-Type": "application/json" },
    },
    2
  );
}

/**
 * Get department
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function getDepartment() {
  return responseService.fetchRetry(
    `${base_url}api-v2/staffs/resource/department`,
    {
      method: "GET", // *GET, POST, PUT, DELETE, etc.
      headers: { "Content-Type": "application/json" },
    },
    2
  );
}

/**
 * Get user group
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function getUserGroup() {
  return responseService.fetchRetry(
    `${base_url}api-v2/staffs/resource/user_group`,
    {
      method: "GET", // *GET, POST, PUT, DELETE, etc.
      headers: { "Content-Type": "application/json" },
    },
    2
  );
}

/**
 * Get role
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function getRole() {
  return responseService.fetchRetry(
    `${base_url}api-v2/staffs/resource/role`,
    {
      method: "GET", // *GET, POST, PUT, DELETE, etc.
      headers: { "Content-Type": "application/json" },
    },
    2
  );
}

/**
 * Get config
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function getConfig() {
  return responseService.fetchRetry(
    `${base_url}api-v2/staffs/resource/config`,
    {
      method: "GET", // *GET, POST, PUT, DELETE, etc.
      headers: { "Content-Type": "application/json" },
    },
    2
  );
}

/**
 * Update staff
 *
 * @param      {<type>}  username  The username
 * @param      {<type>}  password  The password
 * @return     {Object}  the response
 */
function updateStaff(staffId, requestOptionsBody) {
  return responseService.fetchRetry(
    `${base_url}api-v2/staffs/resource/user/${staffId}`,
    {
      method: "PUT", // *GET, POST, PUT, DELETE, etc.
      headers: { "Content-Type": "application/json" },
      mode: "cors", // no-cors, cors, *same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      redirect: "follow", // manual, *follow, error
      referrer: "no-referrer", // no-referrer, *client
      body: JSON.stringify(requestOptionsBody),
    },
    1
  );
}
