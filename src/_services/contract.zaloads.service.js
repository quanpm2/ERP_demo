import { responseService } from "./response.service";

const qs = require("qs");

export const contractZaloadsService = {
    get,
    getActivatedByAdaccountId,
    getAccountCost,
    isValidStartDate,
    getRelatedByAdaccountIdGet,
    joinContracts,
    stopService,
    startService,
    getConfig,
    getOption,
    update,
    getAdAccounts,
    addAdAccount,
    updateConfiguration,
    exchangeLongLiveToken,
    syncAdaccounts,

    computeMetrics,

    /* Get Related Contract by Contract Id */
    getRelatedContracts,

    /* Get Segment Mcm Account Insight Data */
    getSegmentInsight,
    getAdAccountCost,

    join,
    rejoin,
    getOverview,

    getKpiStaffs,
    getSetting,
    addKpi,
    removeKpi,
};

/**
 * Gets the segment insight.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The segment insight.
 */
function getOverview(id) {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/contract/overview/${id}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Gets the segment insight.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The segment insight.
 */
function getSegmentInsight(id) {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/data/segments/${id}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getRelatedContracts(id) {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/data/contracts_related/${id}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function get(id) {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/Contracts/index/${id}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function getActivatedByAdaccountId(id) {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/Contracts/activated_by_adaccount_id/${id}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Gets the related by adaccount_id get.
 *
 * @param      {<type>}  adaccount_id      The adaccount_id
 * @param      {<type>}  payload  The payload
 * @return     {<type>}  The related by adaccount_id get.
 */
function getRelatedByAdaccountIdGet(adaccount_id, requestOptionsBody) {
    let _qs = qs.stringify(requestOptionsBody);

    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/Contracts/related_by_adaccount_id/${adaccount_id}?${_qs}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function isValidStartDate(id, adaccount_id, time) {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/Contracts/is_valid_start_date/${id}/${adaccount_id}/${time}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function getAccountCost(id, requestOptionsBody) {
    let _qs = qs.stringify(requestOptionsBody);
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/data/account_cost/${id}?${_qs}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function joinContracts(sourceId, destinationId, requestOptionsBody) {
    let endpoint = `${base_url}api-v2/zaloads/contracts/join/${sourceId}/${destinationId}`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function stopService(id, requestOptionsBody) {
    let endpoint = `${base_url}api-v2/zaloads/contracts/stop_service/${id}`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function udpateConfigurations(id, requestOptionsBody) {
    const endpoint = `${base_url}api-v2/zaloads/contracts/configurations/${id}`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function startService(id, requestOptionsBody) {
    let endpoint = `${base_url}api-v2/zaloads/contracts/proc_service/${id}`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getConfig(item = "") {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/config/items/${item}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Gets the adAccounts.
 *
 * @return     {<type>}  The cids.
 */
function getAdAccounts() {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/adaccounts`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

function addAdAccount(requestOptionsBody) {
    let endpoint = `${base_url}api-v2/zaloads/adaccounts`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * { function_description }
 *
 * @param      {<type>}  id                  The identifier
 * @param      {<type>}  requestOptionsBody  The request options body
 * @return     {<type>}  { description_of_the_return_value }
 */
function updateConfiguration(id, requestOptionsBody) {
    let endpoint = `${base_url}api-v2/zaloads/contracts/configurations/${id}`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getOption(item = "") {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/config/options/${item}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}
/**
 * Update Contract
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function update(id, requestOptionsBody) {
    let endpoint = `${base_url}api-v2/zaloads/contracts/index/${id}`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Exchange long-live token
 *
 * @param      {<type>}  requestOptionsBody  The request options body
 * @return     {<type>}  { description_of_the_return_value }
 */
function exchangeLongLiveToken(requestOptionsBody) {
    let endpoint = `${base_url}api-v2/zaloads/FbAccounts/exchange_longlive_token`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Get, insert new Adaccount via FbAccount
 *
 * @param      {<type>}  fbAccountId  The fb account identifier
 * @return     {<type>}  { description_of_the_return_value }
 */
function syncAdaccounts(fbAccountId) {
    let endpoint = `${base_url}api-v2/zaloads/Adaccounts/sync_by_account_id/${fbAccountId}`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getAdAccountCost(adaccount_id, requestOptionsBody) {
    let _qs = qs.stringify(requestOptionsBody);
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/data/adaccount_cost/${adaccount_id}?${_qs}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function join(requestOptionsBody) {
    let endpoint = `${base_url}api-v2/zaloads/contracts/join`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function rejoin(requestOptionsBody) {
    let endpoint = `${base_url}api-v2/zaloads/contracts/rejoin`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Update Contract
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function computeMetrics(id) {
    let endpoint = `${base_url}api-v2/zaloads/contract/adaccount_insight/${id}`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}

/**
 * Get KPI staff
 *
 */
function getKpiStaffs() {
    let endpoint = `${base_url}api-v2/zaloads/resource/kpi_staff`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Get setting
 *
 * @param      {<type>}  id      The identifier
 */
function getSetting(id) {
    let endpoint = `${base_url}api-v2/zaloads/resource/setting/${id}`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Add kpi staff
 *
 * @param      {<type>}  id      The identifier
 */
function addKpi(id, payload) {
    let endpoint = `${base_url}api-v2/zaloads/resource/kpi_staff/${id}`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(payload),
        },
        1
    );
}

/**
 * Remove kpi staff
 *
 * @param      {<type>}  id      The identifier
 */
function removeKpi(id, staffId) {
    let endpoint = `${base_url}api-v2/zaloads/resource/kpi_staff/${id}/${staffId}`;
    return responseService.fetchRetry(
        endpoint,
        {
            method: "DELETE", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        1
    );
}
