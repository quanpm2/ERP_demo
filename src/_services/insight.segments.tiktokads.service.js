import { responseService } from './response.service'

const qs = require('qs');

export const insightSegmentsTiktokadsService = { get, create, update, remove };


/**
 * Gets the Insight Data specified identifier.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  { description_of_the_return_value }
 */
function get(id) {

    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/segments/insight/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Create New Insight Data
 *
 * @param      {<type>}  segmentId           The segment identifier
 * @param      {<type>}  requestOptionsBody  The request options body
 * @return     {<type>}  { description_of_the_return_value }
 */
function create(segmentId, requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/tiktokads/segments/insight/${segmentId}`
    return responseService.fetchRetry(endpoint, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}


/**
 * Update Insight Data
 *
 * @param      {<type>}  segmentId         The segment identifier
 * @param      {<type>}  insightSegmentId  The insight segment identifier
 * @return     {<type>}  { description_of_the_return_value }
 */
function update(segmentId, insightSegmentId, requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/tiktokads/segments/insight/${segmentId}/${insightSegmentId}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}


/**
 * Remove Insight Data
 *
 * @param      {<type>}  segmentId         The segment identifier
 * @param      {<type>}  insightSegmentId  The insight segment identifier
 * @return     {<type>}  { description_of_the_return_value }
 */
function remove(segmentId, insightSegmentId)
{
    let endpoint = `${base_url}api-v2/tiktokads/segments/insight/${segmentId}/${insightSegmentId}`
    return responseService.fetchRetry(endpoint, {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
    }, 1)
}