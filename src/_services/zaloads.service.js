import { responseService } from "./response.service";

export const zaloadsService = {
    getStaffs,
    getKpis,
    addKpi,
    removeKpi,
};

/**
 * Get setting
 *
 * @return     {Object}  the response
 */
function getKpis(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/resource/setting/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Get technical staffs
 *
 * @return     {Object}  the response
 */
function getStaffs() {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/resource/kpi_staff`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Add technical staffs
 *
 * @return     {Object}  the response
 */
function addKpi(contractId, requestOptionsBody) {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/resource/kpi_staff/${contractId}`,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Remove technical staffs
 *
 * @return     {Object}  the response
 */
function removeKpi(contractId, kpiId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/zaloads/resource/kpi_staff/${contractId}/${kpiId}`,
        {
            method: "DELETE", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        1
    );
}