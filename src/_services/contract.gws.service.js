import _ from "lodash";
import { responseService } from "./response.service";

const qs = require("qs");

export const contractGwsService = {
    getConfig,
    getContractService,

    updateMetadata,
};

function getConfig() {
    return responseService.fetchRetry(
        `${base_url}api-v2/gws/resource/config`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

function getContractService(term_id) {
    return responseService.fetchRetry(
        `${base_url}api-v2/gws/resource/contract_service/${term_id}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

function updateMetadata(id, payload)
{
    let endpoint = `${base_url}api-v2/gws/contract/metadata/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(payload)
    }, 1)
}