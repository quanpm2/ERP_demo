import { responseService } from "./response.service";

export const hostingService = {
    getContract,
    getCustomerByContractId,
    getStaffs,
    getStaffPerform,
    addKpi,
    removeKpi,
    startService,
    stopService,
};

/**
 * Get contract
 *
 * @return     {Object}  the response
 */
function getContract(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/hosting/resource/contracts/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Get customer by contract id
 *
 * @return     {Object}  the response
 */
function getCustomerByContractId(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/hosting/resource/customer/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Get all tech by contract id
 *
 * @return     {Object}  the response
 */
function getStaffs() {
    return responseService.fetchRetry(
        `${base_url}api-v2/hosting/resource/tech_staffs`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Get tech of contract by contract id
 *
 * @return     {Object}  the response
 */
function getStaffPerform(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/hosting/resource/tech_staffs/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        2
    );
}

/**
 * Add technical staffs
 *
 * @return     {Object}  the response
 */
function addKpi(contractId, requestOptionsBody) {
    return responseService.fetchRetry(
        `${base_url}api-v2/hosting/resource/kpi/${contractId}`,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Remove technical staffs
 *
 * @return     {Object}  the response
 */
function removeKpi(contractId, kpiId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/hosting/resource/kpi/${contractId}/${kpiId}`,
        {
            method: "DELETE", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        1
    );
}

/**
 * Start contract
 *
 * @return     {Object}  the response
 */
function startService(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/hosting/resource/start_service/${contractId}`,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}

/**
 * Stop contract
 *
 * @return     {Object}  the response
 */
function stopService(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/hosting/resource/stop_service/${contractId}`,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}
