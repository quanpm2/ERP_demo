import { responseService } from './response.service'

const qs = require('qs');

export const contractTiktokadsService = {
    get,
    getActivatedByCid,
    getAccountSpend,
    getAdAccountSpend,
    isValidStartDate, 
    getRelatedByCidGet,
    joinContracts,
    stopService,
    startService,
    getCids,
    getConfig,
    getOption,
    update,
    getOverview,
    getAdAccounts,
    updateConfiguration,
    addAdAccount,

    authorization,
    computeMetrics,

    /* Get Related Contract by Contract Id */
    getRelatedContracts,

    /* Get Segment Mcm Account Insight Data */
    // getSegmentInsight,
    getAdAccountCost,
    getAccountCost,

    // Join, Fix join, Unjoin
    join,
    rejoin,
    unjoin,

    // Lock manipulation
    toggleLockManipulation,
};

/**
 * Gets the segment insight.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The segment insight.
 */
 function getOverview(id) {
    
    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/contract/overview/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function get(id) {

    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/contracts/index/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}


/**
 * Gets the cids.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The cids.
 */
function getCids(id) {

    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/contracts/cids`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getActivatedByCid(id) {

    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/contracts/activated_by_cid/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the related by cid get.
 *
 * @param      {<type>}  cid      The cid
 * @param      {<type>}  payload  The payload
 * @return     {<type>}  The related by cid get.
 */
function getRelatedByCidGet(cid, requestOptionsBody)
{
    let _qs = qs.stringify(requestOptionsBody);
    
    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/contracts/related_by_cid/${cid}?${_qs}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}


/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function isValidStartDate(id, cid, time) {

    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/contracts/is_valid_start_date/${id}/${cid}/${time}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getAccountSpend(id, requestOptionsBody) {

    let _qs = qs.stringify(requestOptionsBody);
    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/data/account_spend/${id}?${_qs}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
 function getAdAccountSpend(id, requestOptionsBody) {

    let _qs = qs.stringify(requestOptionsBody);
    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/data/adaccount_spend/${id}?${_qs}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function joinContracts(sourceId, destinationId, requestOptionsBody)
{

    let endpoint = `${base_url}api-v2/tiktokads/contracts/join/${sourceId}/${destinationId}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function stopService(id, requestOptionsBody)
{

    let endpoint = `${base_url}api-v2/tiktokads/contracts/stop_service/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function startService(id, requestOptionsBody)
{

    let endpoint = `${base_url}api-v2/tiktokads/contracts/proc_service/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getConfig(item = '') {

    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/config/items/${item}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getOption(item = '') {

    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/config/options/${item}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Update Contract
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function update(id, requestOptionsBody)
{

    let endpoint = `${base_url}api-v2/tiktokads/contracts/index/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

function authorization(term_id, from = 'setting'){
    // googleads/McmAccounts/authentization_uri
    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/AdsAccount/authentization_uri/${term_id}/${from}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the adAccounts.
 *
 * @return     {<type>}  The cids.
 */
 function getAdAccounts(id) {
    let url = `${base_url}api-v2/tiktokads/AdsAccount`;
    if(!_.isEmpty(id)){
        url += `?account_id=${id}`;
    }

    return responseService.fetchRetry(url, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * { function_description }
 *
 * @param      {<type>}  id                  The identifier
 * @param      {<type>}  requestOptionsBody  The request options body
 * @return     {<type>}  { description_of_the_return_value }
 */
 function updateConfiguration(id, requestOptionsBody)
 {
     let endpoint = `${base_url}api-v2/tiktokads/contracts/configurations/${id}`
     return responseService.fetchRetry(endpoint, {
         method: 'PUT', // *GET, POST, PUT, DELETE, etc.
         headers: { 'Content-Type': 'application/json' },
         mode: 'cors', // no-cors, cors, *same-origin
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         credentials: 'same-origin', // include, *same-origin, omit
         redirect: 'follow', // manual, *follow, error
         referrer: 'no-referrer', // no-referrer, *client
         body: JSON.stringify(requestOptionsBody)
     }, 1)
 }

function addAdAccount(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/tiktokads/AdsAccount`
    return responseService.fetchRetry(endpoint, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Update Contract
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
 function computeMetrics(id)
 {
     let endpoint = `${base_url}api-v2/tiktokads/contract/adaccount_insight/${id}`
     return responseService.fetchRetry(endpoint, {
         method: 'PUT', // *GET, POST, PUT, DELETE, etc.
         headers: { 'Content-Type': 'application/json' },
         mode: 'cors', // no-cors, cors, *same-origin
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         credentials: 'same-origin', // include, *same-origin, omit
         redirect: 'follow', // manual, *follow, error
         referrer: 'no-referrer', // no-referrer, *client
     }, 1)
 }

 /**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getRelatedContracts(id) {
    
    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/data/contracts_related/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getAdAccountCost(adaccount_id, requestOptionsBody) {

    let _qs = qs.stringify(requestOptionsBody);
    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/data/adaccount_cost/${adaccount_id}?${_qs}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function getAccountCost(id, requestOptionsBody) {

    let _qs = qs.stringify(requestOptionsBody);
    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/data/account_cost/${id}?${_qs}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function join(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/tiktokads/contracts/join`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

 /**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
 function rejoin(requestOptionsBody)
 {
     let endpoint = `${base_url}api-v2/tiktokads/contracts/rejoin`
     return responseService.fetchRetry(endpoint, {
         method: 'PUT', // *GET, POST, PUT, DELETE, etc.
         headers: { 'Content-Type': 'application/json' },
         mode: 'cors', // no-cors, cors, *same-origin
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         credentials: 'same-origin', // include, *same-origin, omit
         redirect: 'follow', // manual, *follow, error
         referrer: 'no-referrer', // no-referrer, *client
         body: JSON.stringify(requestOptionsBody)
     }, 1)
 }

 /**
 * Unjoin contract
 *
 * @param      {string}  original_contract_id
 * @param      {string}  unjoin_contract_id
 * 
 * @return     {object} JSON response
 */
function unjoin(original_contract_id, unjoin_contract_id)
{
    let requestOptionsBody = {
        original_contract_id,
        unjoin_contract_id
    };

    let endpoint = `${base_url}api-v2/tiktokads/contracts/unjoin`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * lock_manipulation
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The authentization uri.
 */
function toggleLockManipulation(id, requestOptionsBody = null) {
    return responseService.fetchRetry(`${base_url}api-v2/tiktokads/contracts/toggle_lock_manipulation/${id}`, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}