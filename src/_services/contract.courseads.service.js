import { responseService } from './response.service'

const qs = require('qs');

export const contractCourseadsService = {
    getCourses,
    get
};


/**
 * Gets the courses.
 *
 * @return     {<type>}  The courses.
 */
function getCourses() {

    return responseService.fetchRetry(`${base_url}/api-v2/courseads/contracts/courses`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function get(id) {

    return responseService.fetchRetry(`${base_url}api-v2/courseads/Contracts/index/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}
