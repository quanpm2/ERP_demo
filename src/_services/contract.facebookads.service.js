import { responseService } from './response.service'

const qs = require('qs');

export const contractFacebookadsService = {
    get,
    getActivatedByAdaccountId,
    getAccountCost,
    isValidStartDate, 
    getRelatedByAdaccountIdGet,
    joinContracts,
    stopService,
    startService,
    getConfig,
    getExchangeRate,
    getOption,
    update,
    getAdAccounts,
    addAdAccount,
    updateConfiguration,
    exchangeLongLiveToken,
    syncAdaccounts,
    getAssignedKpi,

    toggleLockManipulation,

    computeMetrics,

    /* Get Related Contract by Contract Id */
    getRelatedContracts,

    /* Get Segment Mcm Account Insight Data */
    getSegmentInsight,
    getAdAccountCost,

    join, 
    rejoin,
    getOverview,
    unjoin,
};

/**
 * Gets the segment insight.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The segment insight.
 */
 function getOverview(id) {
    
    return responseService.fetchRetry(`${base_url}api-v2/facebookads/contract/overview/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the segment insight.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The segment insight.
 */
 function getSegmentInsight(id) {
    
    return responseService.fetchRetry(`${base_url}api-v2/facebookads/data/segments/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getRelatedContracts(id) {
    
    return responseService.fetchRetry(`${base_url}api-v2/facebookads/data/contracts_related/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function get(id) {

    return responseService.fetchRetry(`${base_url}api-v2/facebookads/Contracts/index/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function getActivatedByAdaccountId(id) {

    return responseService.fetchRetry(`${base_url}api-v2/facebookads/Contracts/activated_by_adaccount_id/${id}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the related by adaccount_id get.
 *
 * @param      {<type>}  adaccount_id      The adaccount_id
 * @param      {<type>}  payload  The payload
 * @return     {<type>}  The related by adaccount_id get.
 */
function getRelatedByAdaccountIdGet(adaccount_id, requestOptionsBody)
{
    let _qs = qs.stringify(requestOptionsBody);
    
    return responseService.fetchRetry(`${base_url}api-v2/facebookads/Contracts/related_by_adaccount_id/${adaccount_id}?${_qs}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}


/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function isValidStartDate(id, adaccount_id, time) {

    return responseService.fetchRetry(`${base_url}api-v2/facebookads/Contracts/is_valid_start_date/${id}/${adaccount_id}/${time}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function getAccountCost(id, requestOptionsBody) {

    let _qs = qs.stringify(requestOptionsBody);
    return responseService.fetchRetry(`${base_url}api-v2/facebookads/data/account_cost/${id}?${_qs}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function joinContracts(sourceId, destinationId, requestOptionsBody)
{

    let endpoint = `${base_url}api-v2/facebookads/contracts/join/${sourceId}/${destinationId}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function stopService(id, requestOptionsBody)
{

    let endpoint = `${base_url}api-v2/facebookads/contracts/stop_service/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function udpateConfigurations(id, requestOptionsBody)
{
    const endpoint = `${base_url}api-v2/facebookads/contracts/configurations/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the activated by adaccount_id.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by adaccount_id.
 */
function startService(id, requestOptionsBody)
{

    let endpoint = `${base_url}api-v2/facebookads/contracts/proc_service/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}


/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getConfig(item = '') {

    return responseService.fetchRetry(`${base_url}api-v2/facebookads/config/items/${item}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
 function getExchangeRate() {
    return responseService.fetchRetry(`${base_url}api-v2/facebookads/config/exchange_rate`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}


/**
 * Gets the adAccounts.
 *
 * @return     {<type>}  The cids.
 */
function getAdAccounts(id) {
    let url = `${base_url}api-v2/facebookads/adaccounts`;
    if(!_.isEmpty(id)){
        url += `?account_id=${id}`;
    }

    return responseService.fetchRetry(url, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

function addAdAccount(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/facebookads/adaccounts`
    return responseService.fetchRetry(endpoint, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * { function_description }
 *
 * @param      {<type>}  id                  The identifier
 * @param      {<type>}  requestOptionsBody  The request options body
 * @return     {<type>}  { description_of_the_return_value }
 */
function updateConfiguration(id, requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/facebookads/contracts/configurations/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getOption(item = '') {
    return responseService.fetchRetry(`${base_url}api-v2/facebookads/config/options/${item}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}
/**
 * Update Contract
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function update(id, requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/facebookads/contracts/index/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Exchange long-live token
 *
 * @param      {<type>}  requestOptionsBody  The request options body
 * @return     {<type>}  { description_of_the_return_value }
 */
function exchangeLongLiveToken(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/facebookads/FbAccounts/exchange_longlive_token`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Get, insert new Adaccount via FbAccount
 *
 * @param      {<type>}  fbAccountId  The fb account identifier
 * @return     {<type>}  { description_of_the_return_value }
 */
function syncAdaccounts(fbAccountId)
{
    let endpoint = `${base_url}api-v2/facebookads/Adaccounts/sync_by_account_id/${fbAccountId}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
    }, 1)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
 function getAdAccountCost(adaccount_id, requestOptionsBody) {

    let _qs = qs.stringify(requestOptionsBody);
    return responseService.fetchRetry(`${base_url}api-v2/facebookads/data/adaccount_cost/${adaccount_id}?${_qs}`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
 function join(requestOptionsBody)
 {
     let endpoint = `${base_url}api-v2/facebookads/contracts/join`
     return responseService.fetchRetry(endpoint, {
         method: 'PUT', // *GET, POST, PUT, DELETE, etc.
         headers: { 'Content-Type': 'application/json' },
         mode: 'cors', // no-cors, cors, *same-origin
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         credentials: 'same-origin', // include, *same-origin, omit
         redirect: 'follow', // manual, *follow, error
         referrer: 'no-referrer', // no-referrer, *client
         body: JSON.stringify(requestOptionsBody)
     }, 1)
 }

 /**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function rejoin(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/facebookads/contracts/rejoin`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

 /**
 * Update Contract
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function computeMetrics(id)
{
    let endpoint = `${base_url}api-v2/facebookads/contract/adaccount_insight/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
    }, 1)
}

/**
 * Get dataset kpi assigned
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getAssignedKpi()
{
    let endpoint = `${base_url}api-v2/facebookads/DatasetKpi`
    return responseService.fetchRetry(endpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

/**
 * Unjoin contract
 *
 * @param      {string}  original_contract_id
 * @param      {string}  unjoin_contract_id
 * 
 * @return     {object} JSON response
 */
function unjoin(original_contract_id, unjoin_contract_id)
{
    let requestOptionsBody = {
        original_contract_id,
        unjoin_contract_id
    };

    let endpoint = `${base_url}api-v2/facebookads/contracts/unjoin`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * lock_manipulation
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The authentization uri.
 */
function toggleLockManipulation(id, requestOptionsBody = null) {
    return responseService.fetchRetry(`${base_url}api-v2/facebookads/contracts/toggle_lock_manipulation/${id}`, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}