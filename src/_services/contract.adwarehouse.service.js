import { responseService } from './response.service'

const qs = require('qs');

export const contractAdWarehouseService = {
    getSystemUsersSource,
    getIndustriesSource,
    getBusinessManagerSource,
    createIndustry,

    getBusinessManagerById,
    updateBusinessManagerSystemUser,

    getAdAccount,
    updateAdAccount,

    getBmBySystemUserId,
    saveBM,
};

function getSystemUsersSource(){
    let apiEndpoint = `${base_url}api-v2/contract/adWarehouse/system_users`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

function getIndustriesSource(){
    let apiEndpoint = `${base_url}api-v2/contract/adWarehouse/industries`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

function getBusinessManagerById(business_manager_id){
    let apiEndpoint = `${base_url}api-v2/contract/adWarehouse/business_manager_by_id/${business_manager_id}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

function getBusinessManagerSource(){
    let apiEndpoint = `${base_url}api-v2/contract/adWarehouse/business_manager`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

function updateBusinessManagerSystemUser(business_manager_id, system_users){
    let apiEndpoint = `${base_url}api-v2/contract/adWarehouse/update_business_manager_system_users/${business_manager_id}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify({system_users})
    }, 1)
}

function getAdAccount(ad_account_id){
    let apiEndpoint = `${base_url}api-v2/contract/adWarehouse/ad_account/${ad_account_id}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

function updateAdAccount(ad_account_id, payload){
    let apiEndpoint = `${base_url}api-v2/contract/adWarehouse/update_ad_account/${ad_account_id}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(payload)
    }, 1)
}

function createIndustry(payload){
    let apiEndpoint = `${base_url}api-v2/contract/adWarehouse/industries`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(payload)
    }, 1)
}

function getBmBySystemUserId(system_user_id){
    let _qs = qs.stringify({ system_user_id });

    let apiEndpoint = `${base_url}api-v2/contract/adWarehouse/get_bm_by_system_user_id?${_qs}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}

function saveBM(system_user_id, payload){
    let apiEndpoint = `${base_url}api-v2/contract/adWarehouse/save_bm`

    payload['system_user_id'] = system_user_id;

    return responseService.fetchRetry(apiEndpoint, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(payload)
    }, 1)
}