import { responseService } from './response.service'

const qs = require('qs');

export const contractService = {
    get,
    insertReceipt,
    updateReceipt,
    getReceipt,
    get_contracts_by_contract_code_get,
    get_latest_contracts,
    send_payment_completed,
    insertBalanceSpend,
    updateBalanceSpend,
    deleteBalanceSpend,
    start_service,
    syncInvoices,
    stop_service,
};

/**
 * Get Payment Receipt
 *
 * @param      string  id      The identifier
 * @return     array  contracts
 */
function getReceipt(id) {

    let apiEndpoint = typeof base_url == "undefined" ? window.location.protocol + "//" + window.location.hostname + '/' : base_url
    apiEndpoint = `${apiEndpoint}api-v2/contract/receipts/view/${id}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 1)
}

/**
 * Gets contracts by contract code
 *
 * @param      string  id      The identifier
 * @return     array  contracts
 */
function get_contracts_by_contract_code_get(search) {

    let _qs = qs.stringify({ value: search });
    let apiEndpoint = typeof base_url == "undefined" ? window.location.protocol + "//" + window.location.hostname + '/' : base_url
    apiEndpoint = `${apiEndpoint}api-v2/contract/utilities/get_contracts_by_contract_code?${_qs}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 1)
}

/**
 * Gets contracts by contract code
 *
 * @param      string  id      The identifier
 * @return     array  contracts
 */
function get_latest_contracts() {

    let apiEndpoint = typeof base_url == "undefined" ? window.location.protocol + "//" + window.location.hostname + '/' : base_url
    apiEndpoint = `${apiEndpoint}api-v2/contract/utilities/latest_contracts`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 1)
}

/**
 * Gets the activated by cid.
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function get(id) {

    let apiEndpoint = typeof base_url == "undefined" ? window.location.protocol + "//" + window.location.hostname + '/' : base_url
    apiEndpoint = `${apiEndpoint}api-v2/contract/index/view/contracts/${id}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}


/**
 * Insert new Receipt
 * @param mixed requestOptionsBody
 * 
 * @return [type]
 */
function insertReceipt(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/contract/receipts/index`
    return responseService.fetchRetry(endpoint, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Update new Receipt
 * @param mixed id
 * @param mixed requestOptionsBody
 * 
 * @return [type]
 */
function updateReceipt(id, requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/contract/receipts/index/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Send Email to Customer for Payment completed
 * 
 * @param mixed requestOptionsBody
 * 
 * @return [type]
 */
function send_payment_completed(id)
{
    let endpoint = `${base_url}api-v2/contract/report/send_payment_completed/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
    }, 1)
}

 /**
 * Start service for Payment completed
 * 
 * @param mixed requestOptionsBody
 * 
 * @return [type]
 */
function start_service(id)
{
    let endpoint = `${base_url}api-v2/contract/resource/start_service/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
    }, 1)
}

/**
 * Start service for Payment completed
 * 
 * @param mixed requestOptionsBody
 * 
 * @return [type]
 */
 function stop_service(id)
 {
     let endpoint = `${base_url}api-v2/contract/resource/stop_service/${id}`
     return responseService.fetchRetry(endpoint, {
         method: 'PUT', // *GET, POST, PUT, DELETE, etc.
         headers: { 'Content-Type': 'application/json' },
         mode: 'cors', // no-cors, cors, *same-origin
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         credentials: 'same-origin', // include, *same-origin, omit
         redirect: 'follow', // manual, *follow, error
         referrer: 'no-referrer', // no-referrer, *client
     }, 1)
 }
 
/**
 * Insert new Receipt
 * @param mixed requestOptionsBody
 * 
 * @return [type]
 */
function insertBalanceSpend(requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/contract/resource/balance_spend`
    return responseService.fetchRetry(endpoint, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

 /**
  * Update new Receipt
  * @param mixed id
  * @param mixed requestOptionsBody
  * 
  * @return [type]
  */
function updateBalanceSpend(id, requestOptionsBody)
{
    let endpoint = `${base_url}api-v2/contract/resource/balance_spend/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}
 
 
 /**
  * Delete the balance spend record
  * 
  * @param mixed id
  * @param mixed requestOptionsBody
  * 
  * @return [type]
  */
function deleteBalanceSpend(contractId, id)
{
    let endpoint = `${base_url}api-v2/contract/resource/balance_spend/${contractId}/${id}`
    return responseService.fetchRetry(endpoint, {
        method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
    }, 1)
}

/**
 * Sync invoice
 *
 * @param      string  id      The identifier
 * @return     array  contracts
 */
 function syncInvoices(id) {
    let apiEndpoint = typeof base_url == "undefined" ? window.location.protocol + "//" + window.location.hostname + '/' : base_url
    apiEndpoint = `${apiEndpoint}contract/api/contract/sync_invoices`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify({id})
    }, 1)
}