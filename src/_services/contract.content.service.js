import { responseService } from './response.service'

const qs = require('qs');

export const contractContentService = {
    get,
    update,
    clear
};

/**
 * Gets the activated by cid.
 *x
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function get(contractId) {

    let apiEndpoint = typeof base_url == "undefined" ? window.location.protocol + "//" + window.location.hostname + '/' : base_url
    apiEndpoint = `${apiEndpoint}api-v2/contract/index/content/contracts/${contractId}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' }
    }, 3)
}


/**
 * Update Contract
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function update(contractId, contentId, requestOptionsBody)
{
    let apiEndpoint = typeof base_url == "undefined" ? window.location.protocol + "//" + window.location.hostname + '/' : base_url
    apiEndpoint = `${apiEndpoint}api-v2/contract/index/content/contracts/${contractId}/contents/${contentId}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(requestOptionsBody)
    }, 1)
}

/**
 * Clear Contract Content
 *
 * @param      {<type>}  id      The identifier
 */
function clear(contractId)
{
    let apiEndpoint = typeof base_url == "undefined" ? window.location.protocol + "//" + window.location.hostname + '/' : base_url
    apiEndpoint = `${apiEndpoint}api-v2/contract/index/content/contracts/${contractId}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
    }, 1)
}