import { responseService } from "./response.service";

export const tiktokadsService = {
    getStaffs,
    getKpis,
    addKpi,
    removeKpi,
    getContractChains,
    getAssignedTechnician,
    getContractChainsV2,
};

/**
 * Get setting
 *
 * @return     {Object}  the response
 */
function getKpis(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/tiktokads/resource/kpis/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Get technical staffs
 *
 * @return     {Object}  the response
 */
function getStaffs() {
    return responseService.fetchRetry(
        `${base_url}api-v2/tiktokads/resource/staffs`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Add technical staffs
 *
 * @return     {Object}  the response
 */
function addKpi(contractId, requestOptionsBody) {
    return responseService.fetchRetry(
        `${base_url}api-v2/tiktokads/resource/kpi/${contractId}`,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestOptionsBody),
        },
        1
    );
}

/**
 * Remove technical staffs
 *
 * @return     {Object}  the response
 */
function removeKpi(contractId, kpiId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/tiktokads/resource/kpi/${contractId}/${kpiId}`,
        {
            method: "DELETE", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        1
    );
}

/**
 * Get contract chain
 *
 * @return     {Object}  the response
 */
function getContractChains(contract_id)
{
    return responseService.fetchRetry(
        `${base_url}api-v2/tiktokads/contracts/contract_chains/${contract_id}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Retrieves the assigned technician for a given contract ID.
 *
 * @param {number} contract_id - The ID of the contract.
 * @return {Promise} A promise that resolves to the assigned technician information.
 */
function getAssignedTechnician(contract_id)
{
    return responseService.fetchRetry(
        `${base_url}api-v2/tiktokads/resource/assigned_technician?contract_id=${contract_id}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Get contract chain v2
 *
 * @return     {Object}  the response
 */
function getContractChainsV2(contract_id)
{
    return responseService.fetchRetry(
        `${base_url}api-v2/tiktokads/resource/contract_chains/${contract_id}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}