import { responseService } from './response.service'

const qs = require('qs');

export const dashboardSaleService = {
    getSaleCommissions,
    getSaleRankings,
    getSaleNewRenewalCounts,
    getRevenue,
    getAggegate,
    getCustomerRatioByRevenue,
    getProportionOfEachService,
    getSourceOfCustomersByRevenue,
    getKpiRevenue,
    getCustomerRatioByRevenue,
    getProportionOfEachService,
    getSourceOfCustomersByRevenue,
    getDataTableNewContract
};

function getSaleCommissions(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/commissions?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json'},
    }, 3)
}

function getSaleNewRenewalCounts(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/new_renewal_counts?${qs.stringify(payload)}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}

function getSaleRankings(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/rankings?${qs.stringify(payload)}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}


function getRevenue(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/revenue?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
    }, 3)
}


function getAggegate(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/aggregate?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}

function getKpiRevenue(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/kpi_revenue?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}


function getCustomerRatioByRevenue(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/revenue_by_source?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}

function getProportionOfEachService(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/service_revenue_share?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}

function getSourceOfCustomersByRevenue(payload) {
    let apiEndpoint = `${base_url}api-v2/reporting/sales/revenue_by_source?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}


function getDataTableNewContract(payload) {
    let apiEndpoint = `${base_url}api-v2/reporting/sales/new_contract_signed?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}

