import { responseService } from './response.service'

const qs = require('qs');

export const dashboardSaleGroupService = {
    getSaleCommissions,
    getSaleRankings,
    getSaleNewRenewalCounts,
    getRevenue,
    getAggegate,
    getCustomerRatioByRevenue,
    getProportionOfEachService,
    getSourceOfCustomersByRevenue,
    getKpiRevenue,
    getCustomerRatioByRevenue,
    getProportionOfEachService,
    getSourceOfCustomersByRevenue,
    getMembersRevenueShare,
    getMembersKpiRevenue,
    getDataTableNewContract,
    getDataTableRankingByRevenue
};

function getSaleCommissions(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/commissions?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}

function getSaleNewRenewalCounts(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/new_renewal_counts?${qs.stringify(payload)}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}

function getSaleRankings(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/rankings?${qs.stringify(payload)}`

    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}


function getRevenue(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/revenue?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
    }, 3)
}


function getAggegate(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/aggregate?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}

function getKpiRevenue(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/kpi_revenue?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}


function getCustomerRatioByRevenue(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/kpi/revenue_by_source?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}

function getProportionOfEachService(payload){
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/kpi/revenue_by_source${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}

function getSourceOfCustomersByRevenue(payload) {
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/kpi/revenue_by_source?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}




function getMembersKpiRevenue(payload) {
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/members_kpi_revenue?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}

function getMembersRevenueShare(payload) {
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/members_revenue_share?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}


function getDataTableNewContract(payload) {
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/new_contract_signed?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)
}


function getDataTableRankingByRevenue(payload) {
    let apiEndpoint = `${base_url}api-v2/reporting/sales/groups/members_ranking?${qs.stringify(payload)}`
    return responseService.fetchRetry(apiEndpoint, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: { 'Content-Type': 'application/json' },
    }, 3)

}
