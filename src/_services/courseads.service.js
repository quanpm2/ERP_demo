import { responseService } from "./response.service";

export const courseadsService = {
    getDetail,
    getStudent,
    updateStudent,
    startService,
    stopService,
    getCourseadCode,
    updateCourseadCode,
};

/**
 * Get detail
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getDetail(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/detail/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Get student
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getStudent(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/student/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Update student
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function updateStudent(contractId, requestData) {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/student/${contractId}`,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestData),
        },
        1
    );
}

/**
 * Start service
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function startService(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/start_contract/${contractId}`,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}

/**
 * Start service
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function stopService(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/stop_contract/${contractId}`,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}

/**
 * Get course code
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
 function getCourseadCode(contractId) {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/courseads_code/${contractId}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
        },
        1
    );
}

/**
 * Update coursead code
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
 function updateCourseadCode(contractId, requestData) {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/courseads_code/${contractId}`,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestData),
        },
        1
    );
}