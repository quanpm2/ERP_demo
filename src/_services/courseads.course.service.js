import { responseService } from "./response.service";

export const courseService = {
    getConfig,
    getTeachers,
    postCreate,
    getCourse,
    putUpdate,
};

/**
 * Get config
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getConfig() {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/configs`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Get teachers
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getTeachers() {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/teachers`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Create course
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function postCreate(requestData) {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/create_course`,
        {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestData),
        },
        1
    );
}

/**
 * Get course
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function getCourse(id) {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/course/${id}`,
        {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
        },
        3
    );
}

/**
 * Upde course
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  The activated by cid.
 */
function putUpdate(id, requestData) {
    return responseService.fetchRetry(
        `${base_url}api-v2/courseads/resource/course/${id}`,
        {
            method: "PUT", // *GET, POST, PUT, DELETE, etc.
            headers: { "Content-Type": "application/json" },
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            redirect: "follow", // manual, *follow, error
            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify(requestData),
        },
        1
    );
}
