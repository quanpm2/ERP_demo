import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vUIBuilderGroup from '@/components/examples/UIBuilderGroup.vue'

Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
    new Vue({
        el : "#app-container",
        components: {
            vUIBuilderGroup
        },
        template: `
            <section id="app-container" class="content">
                <div class="row">
                    <div class="col-md-12">
                        <vUIBuilderGroup></vUIBuilderGroup>
                    </div>
                </div>
            </section>
        `
    });
});
