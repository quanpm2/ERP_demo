import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vUIBuilder from '@/components/examples/UIBuilder.vue'

Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
    new Vue({
        el : "#app-container",
        components: {
            vUIBuilder
        },
        template: `
            <section id="app-container" class="content">
                <div class="row">
                    <div class="col-md-12">
                        <vUIBuilder></vUIBuilder>
                    </div>
                </div>
            </section>
        `
    });
});
