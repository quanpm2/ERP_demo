import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vUIBuilderManager from '@/components/examples/UIBuilderManager.vue'

Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
    new Vue({
        el : "#app-container",
        components: {
            vUIBuilderManager
        },
        template: `
            <section id="app-container" class="content">
                <div class="row">
                    <div class="col-md-12">
                        <vUIBuilderManager></vUIBuilderManager>
                    </div>
                </div>
            </section>
        `
    });
});
