import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vTiktokadsOverview from '@/components/tiktokads/vTiktokadsOverview.vue'

Vue.use(VeeValidate)

window.addEventListener('DOMContentLoaded', (event) => {

	new Vue({
		
		el : "#app-container",

		data () {

			return {
				contractId: contractId,
				activeTab: window.location.hash.slice(1) ? window.location.hash.slice(1) : "tab_1"
			}
		},

		components: {
			vTiktokadsOverview,
		},

		methods: {

			changeTab(tabId) {
				this.activeTab = tabId
				history.pushState({}, null, `#${tabId}`)
			}
		},

		template: `
		<section class="content" id="admin-tiktokads-index">
			<div class="row">
			    <div class="col-md-12">
			        <div class="nav-tabs-custom">
			            <ul class="nav nav-tabs">
							<li :class="[activeTab == 'tab_1' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_1')" href="#tab_1" data-toggle="tab">
			                		<i class="fa fa-list-ul"></i> Tổng quan
			                	</a>
			                </li>
			            </ul>
			            <div class="tab-content">
							<div :class="['tab-pane', activeTab == 'tab_1' ? 'active' : '']" id="tab_1">
								<vTiktokadsOverview v-if="activeTab == 'tab_1'" v-bind:id="contractId" />
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</section>
		`
	});
});