import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vTiktokadsFixJoinContract from '@/components/tiktokads/FixJoinContract.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-tiktokads-fix-join-contract",
		components: {
			vTiktokadsFixJoinContract
		},
	});
});
