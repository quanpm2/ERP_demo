import Vue from "vue/dist/vue.esm.js";
import VeeValidate, { Validator } from "vee-validate/dist/vee-validate.esm.js";
import vi from 'vee-validate/dist/locale/vi';
import vTiktokadsContractManipulationLockBox from "@/components/tiktokads/vTiktokadsContractManipulationLockBox.vue";

Vue.use(VeeValidate);
Validator.localize('vi', vi);

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#tiktokads-contract-manipulation-lock-box",
        components: {
            vTiktokadsContractManipulationLockBox,
        },
    });
});
