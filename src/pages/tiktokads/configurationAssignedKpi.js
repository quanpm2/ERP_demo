import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vTiktokadsConfigurationAssignedKpi from '@/components/tiktokads/vTiktokadsConfigurationAssignedKpi.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-container",
		components: {
			vTiktokadsConfigurationAssignedKpi	
		},
        data: () => ({
			activeTab: window.location.hash.slice(1) ? window.location.hash.slice(1) : "tab-1",
		}),
		methods: {
			changeTab (tabId) {
				this.activeTab = tabId
				history.pushState({}, null, `#${tabId}`)
			}
		},
        template: `
            <section class="content" id="admin-tiktokads-index">
                <div class="row">
                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li :class="[activeTab == 'tab-1' ? 'active' : '']">
                                    <a v-on:click="changeTab('tab-1')" href="#tab-1" data-toggle="tab">
                                        <i class="fa fa-list-ul"></i> Chưa phân công
                                    </a>
                                </li>
                                <li :class="[activeTab == 'tab-2' ? 'active' : '']">
                                    <a v-on:click="changeTab('tab-2')" href="#tab-2" data-toggle="tab">
                                        <i class="fa fa-list-ul"></i> Tổng quan
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div :class="['tab-pane', activeTab == 'tab-1' ? 'active' : '']" id="tab-1">
                                    <vTiktokadsConfigurationAssignedKpi v-if="activeTab == 'tab-1'" :is_unassigned="true"></vTiktokadsConfigurationAssignedKpi>
                                </div>
                                <div :class="['tab-pane', activeTab == 'tab-2' ? 'active' : '']" id="tab-2">
                                    <vTiktokadsConfigurationAssignedKpi v-if="activeTab == 'tab-2'"></vTiktokadsConfigurationAssignedKpi>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        `
	});
});
