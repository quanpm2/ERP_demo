import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vTiktokadsConfigurationServiceBox from '@/components/tiktokads/vTiktokadsConfigurationServiceBox.vue'
import vSegmentHistory from '@/components/tiktokads/vSegmentHistory.vue'

Vue.use(VeeValidate)

window.addEventListener('DOMContentLoaded', (event) => {

    new Vue({
        
        el : "#configuration-service-view-box",
        name: 'v-configuration-service-view-box',

        components: {
            vTiktokadsConfigurationServiceBox,
            vSegmentHistory
        },

        data () {

            return {
                term_id: term_id,
                admin_id: admin_id,
                has_manage_ads_segment: has_manage_ads_segment,

                activeTab: window.location.hash.slice(1) ? window.location.hash.slice(1) : "tab_1"
            }
        },

        methods: {

            changeTab(tabId) {
                this.activeTab = tabId
                history.pushState({}, null, `#${tabId}`)
            }
        },

        template: `
            <section class="content" id="configuration-service-view-box">
                <div class="row">
                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li :class="[activeTab == 'tab_1' ? 'active' : '']">
                                    <a v-on:click="changeTab('tab_1')" href="#tab_1" data-toggle="tab">
                                        <i class="fa fa-fw fa-xs fa-cogs"></i> Cấu hình dịch vụ
                                    </a>
                                </li>
                                <li :class="[activeTab == 'tab_2' ? 'active' : '']">
                                    <a v-on:click="changeTab('tab_2')" href="#tab_2" data-toggle="tab">
                                        <i class="fa fa-history"></i> Lịch sử cấu hình
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <vTiktokadsConfigurationServiceBox 
                                    v-if="activeTab == 'tab_1'" 
                                    :class="['tab-pane', activeTab == 'tab_1' ? 'active' : '']" 
                                    :term_id="term_id" 
                                    :admin_id="admin_id" 
                                    :has_manage_ads_segment="has_manage_ads_segment" 
                                />
                                <div 
                                    id="tab_1"
                                >
                                    
                                </div>
                                <div 
                                    v-if="activeTab == 'tab_2'" 
                                    :class="['tab-pane', activeTab == 'tab_2' ? 'active' : '']" 
                                    id="tab_2"
                                >
                                    <vSegmentHistory 
                                        v-if="activeTab == 'tab_2'" 
                                        v-bind:term_id="term_id" 
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        `
    });
});