import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vTiktokadsConfigurationContractPromotionsBox from '@/components/tiktokads/vTiktokadsConfigurationContractPromotionsBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vTiktokadsConfigurationContractPromotionsBox",
		components: {
			vTiktokadsConfigurationContractPromotionsBox	
		}
	});
});
