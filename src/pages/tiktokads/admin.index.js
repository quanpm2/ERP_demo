import Vue from "vue/dist/vue.esm.js";
import vTiktokadsDatatable from "@/components/tiktokads/vTiktokadsDatatable.vue";
import vTiktokadsUnconfiguredDatatable from '@/components/tiktokads/vTiktokadsUnconfiguredDatatable.vue'
import vTiktokadsAdsPending from '@/components/tiktokads/vTiktokadsAdsPending.vue'
import vTiktokadsAccountDisconnect from '@/components/tiktokads/vTiktokadsAccountDisconnect.vue'
import vSpendReportContainer from '@/components/tiktokads/vSpendReportContainer.vue'
import vSpendSyncContainer from '@/components/tiktokads/vSpendSyncContainer.vue'

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#app-container",

        data() {
            return {
                activeTab: window.location.hash.slice(1)
                    ? window.location.hash.slice(1)
                    : "tab_1",
            };
        },

        methods: {
            changeTab(tabId) {
                this.activeTab = tabId;
                history.pushState({}, null, `#${tabId}`);
            },
        },

        components: {
            vTiktokadsDatatable,
            vTiktokadsUnconfiguredDatatable,
            vTiktokadsAdsPending,
            vTiktokadsAccountDisconnect,
            vSpendReportContainer,
            vSpendSyncContainer
        },
        template: `
		<section class="content" id="admin-tiktokads-index">
			<div class="row">
			    <div class="col-md-12">
			        <div class="nav-tabs-custom">
			            <ul class="nav nav-tabs">
							<li :class="[activeTab == 'tab_1' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_1')" href="#tab_1" data-toggle="tab">
			                		<i class="fa fa-list-ul"></i> Tất cả dịch vụ
			                	</a>
			                </li>
                            <li :class="[activeTab == 'tab_2' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_2')" href="#tab_2" data-toggle="tab">
			                		<i style="margin-right: 10px" class="fa fa-exclamation-circle"></i> Hợp đồng chưa cấu hình
			                	</a>
			                </li>
                            <li :class="[activeTab == 'tab_3' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_3')" href="#tab_3" data-toggle="tab">
			                		<i style="margin-right: 10px" class="fa fa-pause"></i> Đang tạm ngưng
			                	</a>
			                </li>
                            <li :class="[activeTab == 'tab_4' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_4')" href="#tab_4" data-toggle="tab">
									<i style="margin-right: 10px" class="fa fa-link"></i> Hợp đồng mất kết nối tài khoản
			                	</a>
			                </li>
							<li :class="[activeTab == 'tab_5' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_5')" href="#tab_5" data-toggle="tab">
									<i style="margin-right: 10px" class="fa fa-line-chart"></i> Báo cáo Spend
			                	</a>
			                </li>
                            <li :class="[activeTab == 'tab_6' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_6')" href="#tab_6" data-toggle="tab">
			                		<i class="fa fa-refresh"></i> Đồng bộ spend
			                	</a>
			                </li>
                            <!-- Tạm dừng tính năng 
                            <li :class="[activeTab == 'tab_7' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_7')" href="#tab_7" data-toggle="tab">
									<i style="margin-right: 10px" class="fa fa-history"></i> Lịch sử đồng bộ
			                	</a>
			                </li>
                            -->
			            </ul>
			            <div class="tab-content">
							<div :class="['tab-pane', activeTab == 'tab_1' ? 'active' : '']" id="tab_1">
                                <vTiktokadsDatatable v-if="activeTab == 'tab_1'" />
			                </div>
                            <div :class="['tab-pane', activeTab == 'tab_2' ? 'active' : '']" id="tab_2">
                                <vTiktokadsUnconfiguredDatatable v-if="activeTab == 'tab_2'" />
			                </div>
							<div :class="['tab-pane', activeTab == 'tab_3' ? 'active' : '']" id="tab_3">
								<vTiktokadsAdsPending v-if="activeTab == 'tab_3'" />
			                </div>
                            <div :class="['tab-pane', activeTab == 'tab_4' ? 'active' : '']" id="tab_4">
								<vTiktokadsAccountDisconnect v-if="activeTab == 'tab_4'" />
			                </div>
                            <div :class="['tab-pane', activeTab == 'tab_5' ? 'active' : '']" id="tab_5">
								<vSpendReportContainer v-if="activeTab == 'tab_5'" />
			                </div>
                            <div :class="['tab-pane', activeTab == 'tab_6' ? 'active' : '']" id="tab_6">
                                <vSpendSyncContainer v-if="activeTab == 'tab_6'" /> 
			                </div>
                            <!--
                            <div :class="['tab-pane', activeTab == 'tab_7' ? 'active' : '']" id="tab_7">
                                <v-spend-sync-history v-if="activeTab == 'tab_7'" /> 
			                </div>
                            -->
			            </div>
			        </div>
			    </div>
			</div>
		</section>
		`,
    });
});