import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vTiktokadsConfigurationBox from '@/components/tiktokads/vTiktokadsConfigurationBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-process-service-container",
		components: {
			vTiktokadsConfigurationBox	
		}
	});
});
