import Vue from "vue/dist/vue.esm.js";
import VeeValidate from "vee-validate/dist/vee-validate.esm.js";
import vTiktokadsConfigurationKpiBox from "@/components/vTiktokadsConfigurationKpiBox.vue";
Vue.use(VeeValidate);

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#vTiktokadsConfigurationKpiBox",
        components: {
            vTiktokadsConfigurationKpiBox,
        },
    });
});
