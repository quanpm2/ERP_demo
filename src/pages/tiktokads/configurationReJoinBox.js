import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vTiktokadsConfigurationReJoinBox from '@/components/tiktokads/vTiktokadsConfigurationReJoinBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-rejoin-service-container",
		components: {
			vTiktokadsConfigurationReJoinBox	
		}
	});
});