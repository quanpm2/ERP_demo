import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vTiktokadsConfigurationContractBox from '@/components/tiktokads/vTiktokadsConfigurationContractBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vTiktokadsConfigurationContractBox",
		components: {
			vTiktokadsConfigurationContractBox	
		}
	});
});
