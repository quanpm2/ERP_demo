import Vue from "vue/dist/vue.esm.js";
import VeeValidate from "vee-validate/dist/vee-validate.esm.js";
import vContractAuditDatatable from "@/components/vContractAuditDatatable.vue";

Vue.use(VeeValidate);

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#app-container",
        data() {
            return {
                activeTab: window.location.hash.slice(1)
                    ? window.location.hash.slice(1)
                    : "data-list",
            };
        },

        methods: {
            changeTab(tabId) {
                this.activeTab = tabId;
                history.pushState({}, null, `#${tabId}`);
            },
        },

        components: {
            vContractAuditDatatable,
        },

        template: `
		<section class="content" id="admin-googleads-index">
			<div class="row">
			    <div class="col-md-12">
			        <div class="nav-tabs-custom">
			            <ul class="nav nav-tabs">
							<li :class="[activeTab == 'data-list' ? 'active' : '']">
			                	<a v-on:click="changeTab('data-list')" href="#data-list" data-toggle="tab">
			                		<i class="fa fa-list-ul"></i> Data log
			                	</a>
			                </li>
			            </ul>
			            <div class="tab-content">
							<div :class="['tab-pane', activeTab == 'data-list' ? 'active' : '']" id="data-list">
								<vContractAuditDatatable v-if="activeTab == 'data-list'"></vContractAuditDatatable>
							</div>
			            </div>
			        </div>
			    </div>
			</div>
		</section>
		`,
    });
});
