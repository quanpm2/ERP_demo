import Vue from "vue/dist/vue.esm.js";
import VeeValidate from "vee-validate/dist/vee-validate.esm.js";
import vContractReportSynthesis from "@/components/vContractReportSynthesis.vue";
import vContractReportWarningOverBudget from "@/components/vContractReportWarningOverBudget.vue";
import vContractReportReceiptCautionDueSoon from "@/components/vContractReportReceiptCautionDueSoon.vue";
import vContractReportRevenue from "@/components/vContractReportRevenue.vue";
import vContractReportSpend from "@/components/vContractReportSpend.vue";
import vContractReportStop from "@/components/vContractReportStop.vue";

Vue.use(VeeValidate);

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#app-container",
        data() {
            return {
                activeTab: window.location.hash.slice(1)
                    ? window.location.hash.slice(1)
                    : "synthesis-report",
            };
        },

        methods: {
            changeTab(tabId) {
                this.activeTab = tabId;
                history.pushState({}, null, `#${tabId}`);
            },
        },

        components: {
            vContractReportSynthesis,
            vContractReportWarningOverBudget,
            vContractReportReceiptCautionDueSoon,
            vContractReportRevenue,
            vContractReportSpend,
            vContractReportStop,
        },

        template: `
		<section class="content" id="contract-report-index">
			<div class="row">
			    <div class="col-md-12">
			        <div class="nav-tabs-custom">
			            <ul class="nav nav-tabs">
							<li :class="[activeTab == 'synthesis-report' ? 'active' : '']">
			                	<a v-on:click="changeTab('synthesis-report')" href="#synthesis-report" data-toggle="tab">
			                		<i class="fa fa-list-ul"></i> Báo cáo tổng hợp
			                	</a>
			                </li>
                            <li :class="[activeTab == 'spent-report' ? 'active' : '']">
			                	<a v-on:click="changeTab('spent-report')" href="#spent-report" data-toggle="tab">
			                		<i class="fa fa-location-arrow"></i> Báo cáo spend
			                	</a>
			                </li>
                            <li :class="[activeTab == 'contract-report-revenue' ? 'active' : '']">
                                <a v-on:click="changeTab('contract-report-revenue')" href="#contract-report-revenue" data-toggle="tab">
                                    <i class="fa fa-money"></i> Báo cáo doanh thu
                                </a>
			                </li>
                            <li :class="[activeTab == 'warning-over-budget' ? 'active' : '']">
                                <a v-on:click="changeTab('warning-over-budget')" href="#warning-over-budget" data-toggle="tab">
                                    <i class="fa fa-warning"></i> Báo cáo cảnh báo hợp đồng chạy lố
                                </a>
			                </li>
                            <li :class="[activeTab == 'receipt-caution-due-soon' ? 'active' : '']">
                                <a v-on:click="changeTab('receipt-caution-due-soon')" href="#receipt-caution-due-soon" data-toggle="tab">
                                    <i class="fa fa-exclamation"></i> Báo cáo cảnh báo hợp đồng có bảo lãnh
                                </a>
			                </li>
                            <li :class="[activeTab == 'contract-stop-report' ? 'active' : '']">
                                <a v-on:click="changeTab('contract-stop-report')" href="#contract-stop-report" data-toggle="tab">
                                    <i class="fa fa-stop"></i> Báo cáo hợp đồng đã kết thúc
                                </a>
			                </li>
			            </ul>
			            <div class="tab-content">
							<div :class="['tab-pane', activeTab == 'synthesis-report' ? 'active' : '']" id="synthesis-report">
								<vContractReportSynthesis v-if="activeTab == 'synthesis-report'"></vContractReportSynthesis>
							</div>
                            <div :class="['tab-pane', activeTab == 'spent-report' ? 'active' : '']" id="spent-report">
								<vContractReportSpend v-if="activeTab == 'spent-report'"></vContractReportSpend>
							</div>
                            <div :class="['tab-pane', activeTab == 'contract-report-revenue' ? 'active' : '']" id="contract-report-revenue">
								<vContractReportRevenue v-if="activeTab == 'contract-report-revenue'"></vContractReportRevenue>
							</div>
                            <div :class="['tab-pane', activeTab == 'warning-over-budget' ? 'active' : '']" id="warning-over-budget">
								<vContractReportWarningOverBudget v-if="activeTab == 'warning-over-budget'"></vContractReportWarningOverBudget>
							</div>
                            <div :class="['tab-pane', activeTab == 'receipt-caution-due-soon' ? 'active' : '']" id="receipt-caution-due-soon">
								<vContractReportReceiptCautionDueSoon v-if="activeTab == 'receipt-caution-due-soon'"></vContractReportReceiptCautionDueSoon>
							</div>
                            <div :class="['tab-pane', activeTab == 'contract-stop-report' ? 'active' : '']" id="contract-stop-report">
								<vContractReportStop v-if="activeTab == 'contract-stop-report'"></vContractReportStop>
							</div>
			            </div>
			        </div>
			    </div>
			</div>
		</section>
		`,
    });
});
