import Vue from "vue/dist/vue.esm.js";
import VeeValidate, { Validator } from "vee-validate/dist/vee-validate.esm.js";
import vi from 'vee-validate/dist/locale/vi';
import vContractManipulationLockBox from "@/components/contract/vContractManipulationLockBox.vue";

Vue.use(VeeValidate);
Validator.localize('vi', vi);

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#contract-manipulation-lock-box",
        components: {
            vContractManipulationLockBox,
        },
    });
});
