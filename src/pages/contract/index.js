import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vCustomerContractWizardTab from '@/components/vCustomerContractWizardTab.vue'

Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-containercontainer",
		components: {
			vCustomerContractWizardTab	
		}
	});
});
