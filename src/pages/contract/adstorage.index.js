import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vAdStorageIndexContainer from '@/components/vAdStorageIndexContainer.vue'

window.addEventListener('DOMContentLoaded', (event) => {

	new Vue({
		el : "#app-container",
		data () {
			return {
				activeTab: window.location.hash.slice(1) ? window.location.hash.slice(1) : "tab-1",
			}
		},

		methods: {
			changeTab (tabId) {
				this.activeTab = tabId
				history.pushState({}, null, `#${tabId}`)
			}
		},

		components: {
			vAdStorageIndexContainer
		},

		template: `
		<section class="content" id="admin-adstorage-index">
			<div class="row">
			    <div class="col-md-12">
			        <div class="nav-tabs-custom">
			            <ul class="nav nav-tabs">
							<li :class="[activeTab == 'tab-1' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab-1')" href="#tab-1" data-toggle="tab">
			                		<i class="fa fa-list-ul"></i> Tài khoản kết thúc
			                	</a>
			                </li>
			            </ul>
						<div class="tab-content">
							<div :class="['tab-pane', activeTab == 'tab-1' ? 'active' : '']" id="tab-1">
								<vAdStorageIndexContainer v-if="activeTab == 'tab-1'"></vAdStorageIndexContainer>
							</div>
						</div>
			        </div>
			    </div>
			</div>
		</section>
		`
	});
});
