import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vContractPrintableNavTop from '@/components/vContractPrintableNavTop.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vContractPrintableNavTop",
		components: {
			vContractPrintableNavTop	
		}
	});
});
