import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vContractReceiptsIndexContainer from '@/components/vContractReceiptsIndexContainer.vue'
import vContractReceiptsBatchCreateOrUpdateContainer from '@/components/vContractReceiptsBatchCreateOrUpdateContainer.vue'
import vContractReceiptsIndexStartService from '@/components/vContractReceiptsIndexStartService.vue'

Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {

	new Vue({
		el : "#app-container",
		data () {
			return {
				activeTab: window.location.hash.slice(1) ? window.location.hash.slice(1) : "data-list",
			}
		},

		methods: {
			changeTab (tabId) {
				this.activeTab = tabId
				history.pushState({}, null, `#${tabId}`)
			}
		},

		components: {
			vContractReceiptsIndexContainer,
			vContractReceiptsBatchCreateOrUpdateContainer,
			vContractReceiptsIndexStartService
		},

		template: `
		<section class="content" id="admin-googleads-index">
			<div class="row">
			    <div class="col-md-12">
			        <div class="nav-tabs-custom">
			            <ul class="nav nav-tabs">
							<li :class="[activeTab == 'data-list' ? 'active' : '']">
			                	<a v-on:click="changeTab('data-list')" href="#data-list" data-toggle="tab">
			                		<i class="fa fa-list-ul"></i> Danh sách thanh toán
			                	</a>
			                </li>
							<li :class="[activeTab == 'batch-start' ? 'active' : '']">
								<a v-on:click="changeTab('batch-start')" href="#batch-start" data-toggle="tab">
									<i class="fa fa-play"></i> Khởi chạy hợp đồng
								</a>
							</li>
			                <li :class="[activeTab == 'batch-import' ? 'active' : '']">
			                	<a v-on:click="changeTab('batch-import')" href="#batch-import" data-toggle="tab">
			                		<i class="fa fa-cubes"></i> Import đợt thanh toán
			                	</a>
			                </li>
			            </ul>
			            <div class="tab-content">
							<div :class="['tab-pane', activeTab == 'data-list' ? 'active' : '']" id="data-list">
								<vContractReceiptsIndexContainer v-if="activeTab == 'data-list'"></vContractReceiptsIndexContainer>
							</div>
							<div :class="['tab-pane', activeTab == 'batch-start' ? 'active' : '']" id="batch-start">
								<vContractReceiptsIndexStartService v-if="activeTab == 'batch-start'"></vContractReceiptsIndexStartService>
							</div>
			                <div :class="['tab-pane', activeTab == 'batch-import' ? 'active' : '']" id="batch-import">
								<vContractReceiptsBatchCreateOrUpdateContainer></vContractReceiptsBatchCreateOrUpdateContainer>
							</div>
			            </div>
			        </div>
			    </div>
			</div>
		</section>
		`
	});
});
