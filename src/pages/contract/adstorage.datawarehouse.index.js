import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vAdStorageIndexContainer from '@/components/vAdStorageIndexContainer.vue'
import vViewerComponent from '@/components/viewers/vViewerComponent.vue'
import vAdAccount from '@/components/AdWarehouse/vAdAccount.vue'
import vBussinessManager from '@/components/AdWarehouse/vBussinessManager.vue'
import vCreateBusinessManager from '@/components/AdWarehouse/vCreateBusinessManager.vue'
import vAdWarehouseContainer from '@/components/AdWarehouse/vAdWarehouseContainer.vue'

import store from '@/store/store.js';

Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {

	new Vue({
		el : "#app-container",
        store,
		data () {
			return {
				activeTab: window.location.hash.slice(1) ? window.location.hash.slice(1) : "tab-1",
				views : [
					{
						id: 2,
						name: 'fact-warehouse-summary',
						label: 'Số liệu kho tài khoản',
						columns : [
							'bm_id', 
							'ad_warehouse_id',
							'ad_account_id'
						],
						filters: [
							{	
								column: 'bm_id',
								operator: '=',
								value: '1'
							},
							{	
								column: 'ad_account_name',
								operator: 'like',
								value: 'aaa'
							},
						],
						order_by: {
							column: 'ad_account_name', 
							director : 'asc'
						}
					},
					{
						id: 1,
						name: 'all',
						label: 'Tất cả',
						columns : [

							'fact_warehouse_exchange_id',
							'ad_business_manager_business_id',
							'ad_business_manager_name',
							'ad_account_name',
							'ad_account_account_id',

							'fact_warehouse_exchange_begin_balance',
							'fact_warehouse_exchange_import',
							'fact_warehouse_exchange_export_use',
							'fact_warehouse_exchange_export_suspend',
							'fact_warehouse_exchange_balance',

							'ad_account_status',
							'ad_account_amount_spent',
							'ad_account_age',
							'ad_account_stock_status',
							'ad_account_type_of_ownership',
							'ad_account_creation_type'
						],
                        primary_column: 'fact_warehouse_exchange_id',
						filters: [
							{	
								column: 'bm_id',
								operator: '=',
								value: '1'
							},
							{	
								column: 'ad_account_name',
								operator: 'like',
								value: 'aaa'
							},
						],
						order_by: {
							column: 'ad_account_name', 
							director : 'asc'
						}
					}
				]
			}
		},

        computed: {
            ad_account(){
                return this.$store.state.adWarehouse.ad_account_selected
            },

            ad_account_status(){
                return _.get(this.ad_account, "ad_account_status", "")
            },
    
            ad_business_manager(){
                return this.$store.state.adWarehouse.ad_business_manager_selected
            },

            ad_business_manager_status(){
                return _.get(this.ad_business_manager, "ad_business_manager_verification_status", "")
            }
        },

		methods: {
            get(item, ref){
                return _.get(item, ref)
            },
			changeTab (tabId) {
				this.activeTab = tabId
				history.pushState({}, null, `#${tabId}`)
			}
		},

		components: {
			vAdWarehouseContainer,
			vAdStorageIndexContainer,
			vViewerComponent,
            vAdAccount,
            vCreateBusinessManager,
            vBussinessManager,
		},

		template: `
		<section class="content" id="admin-adstorage-index">
			<vAdWarehouseContainer />
		</section>
		`
	});
});