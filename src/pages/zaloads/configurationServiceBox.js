import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vZaloadsConfigurationServiceBox from '@/components/vZaloadsConfigurationServiceBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-configuration-service-box-container",
		components: {
			vZaloadsConfigurationServiceBox	
		}
	});
});
