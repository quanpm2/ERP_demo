import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vZaloadsConfigurationBox from '@/components/vZaloadsConfigurationBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-process-service-container",
		components: {
			vZaloadsConfigurationBox	
		}
	});
});
