import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vZaloadsConfigurationContractCuratorsBox from '@/components/vZaloadsConfigurationContractCuratorsBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vZaloadsConfigurationContractCuratorsBox",
		components: {
			vZaloadsConfigurationContractCuratorsBox	
		}
	});
});
