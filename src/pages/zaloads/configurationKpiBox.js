import Vue from "vue/dist/vue.esm.js";
import VeeValidate from "vee-validate/dist/vee-validate.esm.js";
import vZaloadsConfigurationKpiBox from "@/components/vZaloadsConfigurationKpiBox.vue";
Vue.use(VeeValidate);

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#vZaloadsConfigurationKpiBox",
        components: {
            vZaloadsConfigurationKpiBox,
        },
    });
});
