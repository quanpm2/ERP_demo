import Vue from "vue/dist/vue.esm.js";
import vZaloadsDatatable from "@/components/vZaloadsDatatable.vue";
// import vSpendReportContainer from '@/components/zaloads/vSpendReportContainer.vue'
// import vSpendSyncContainer from '@/components/zaloads/vSpendSyncContainer.vue'
// import vSpendSyncHistory from '@/components/zaloads/vSpendSyncHistory.vue'

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#app-container",

        data() {
            return {
                activeTab: window.location.hash.slice(1)
                    ? window.location.hash.slice(1)
                    : "tab_1",
            };
        },

        methods: {
            changeTab(tabId) {
                this.activeTab = tabId;
                history.pushState({}, null, `#${tabId}`);
            },
        },

        components: {
            vZaloadsDatatable,
            // vSpendReportContainer,
            // vSpendSyncContainer,
            // vSpendSyncHistory,
        },
        template: `
		<section class="content" id="admin-facebookads-index">
			<div class="row">
			    <div class="col-md-12">
			        <div class="nav-tabs-custom">
			            <ul class="nav nav-tabs">
							<li :class="[activeTab == 'tab_1' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_1')" href="#tab_1" data-toggle="tab">
			                		<i class="fa fa-list-ul"></i> Tất cả dịch vụ
			                	</a>
			                </li>
                            <!--
							<li :class="[activeTab == 'tab_2' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_2')" href="#tab_2" data-toggle="tab">
			                		<i class="fa fa-pause"></i> Báo cáo Spend
			                	</a>
			                </li>
                            <li :class="[activeTab == 'tab_3' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_3')" href="#tab_3" data-toggle="tab">
			                		<i class="fa fa-refresh"></i> Đồng bộ spend
			                	</a>
			                </li>
                            <li :class="[activeTab == 'tab_4' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_4')" href="#tab_4" data-toggle="tab">
			                		<i class="fa fa-history"></i> Lịch sử đồng bộ
			                	</a>
			                </li>
                            -->
			            </ul>
			            <div class="tab-content">
							<div :class="['tab-pane', activeTab == 'tab_1' ? 'active' : '']" id="tab_1">
                                <vZaloadsDatatable v-if="activeTab == 'tab_1'" />
			                </div>
                            <!--
							<div :class="['tab-pane', activeTab == 'tab_2' ? 'active' : '']" id="tab_2">
								<vSpendReportContainer v-if="activeTab == 'tab_2'" /> 
			                </div>
                            <div :class="['tab-pane', activeTab == 'tab_3' ? 'active' : '']" id="tab_3">
                                 <v-spend-sync-container v-if="activeTab == 'tab_3'" /> 
			                </div>
                            <div :class="['tab-pane', activeTab == 'tab_4' ? 'active' : '']" id="tab_4">
                                 <v-spend-sync-history v-if="activeTab == 'tab_4'" />
			                </div>
                            -->
			            </div>
			        </div>
			    </div>
			</div>
		</section>
		`,
    });
});
