import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vZaloadsConfigurationContractPromotionsBox from '@/components/vZaloadsConfigurationContractPromotionsBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vZaloadsConfigurationContractPromotionsBox",
		components: {
			vZaloadsConfigurationContractPromotionsBox	
		}
	});
});
