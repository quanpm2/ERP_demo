import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
Vue.use(VeeValidate, { fastExit: true });

import vDatatableBuilder from '@/components/vDatatableBuilder.vue'
import vCustomerCreateComponent from '@/components/vCustomerCreateComponent.vue'

import { customerService } from '@/_services/customer.service.js'
import { ApiEndpoints } from '@/_config/api.endpoints.js'

window.addEventListener('DOMContentLoaded', (event) => {

	new Vue({
		el : "#app-container",
		data () {
			return {
				apiDataset: ApiEndpoints.valueOf('customer') + '/dataset',
				filter_position: 1
			}
		},
		methods: {
			doSomethingAfterCreated (args) {
				$.notify('Trang sẽ tự khởi tạo lại trong vòng 2 giây nữa', 'info')
				setTimeout( () => { window.location.reload() }, 2000)
			}
		},
		components: {
			vDatatableBuilder,
			vCustomerCreateComponent
		},
		template: `
		<section class="content" id="admin-googleads-index">
			<div class="row">
			    <div class="col-md-12">
			        <div class="nav-tabs-custom">
			            <ul class="nav nav-tabs">
			                <li class="active">
			                	<a href="#tab_1" data-toggle="tab">
			                		<i class="fa fa-list-ul"></i> Thông tin liên hệ
			                	</a>
			                </li>
			                <li class="pull-right">
			                    <vCustomerCreateComponent v-on:created="doSomethingAfterCreated" />
			                </li>
			            </ul>
			            <div class="tab-content">
			                <div class="tab-pane active" id="tab_1">
			                   <v-datatable-builder
		                   		:is_filtering="false"
                                :is_load_config="false"
		                   		:is_ordering="true"
		                   		:is_download="true"
		                   		:per_page="50"
		                   		:filter_position="filter_position"
		                   		:remote_url="apiDataset"></v-datatable-builder> 
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</section>
		`
	});
});
