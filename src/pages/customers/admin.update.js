import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
Vue.use(VeeValidate, { fastExit: true });

import vCustomerUpdateComponent from '@/components/vCustomerUpdateComponent.vue'
import { customerService } from '@/_services/customer.service.js'
import { ApiEndpoints } from '@/_config/api.endpoints.js'

window.addEventListener('DOMContentLoaded', (event) => {

	new Vue({
		el : "#app-container",
		data () {
			return {
				userId: userId,
				apiDataset: ApiEndpoints.valueOf('customer') + '/dataset',
				filter_position: 1
			}
		},
		methods: {
			doSomethingAfterCreated (args) {
				$.notify('Trang sẽ tự khởi tạo lại trong vòng 2 giây nữa', 'info')
				setTimeout( () => { window.location.reload() }, 2000)
			}
		},
		components: {
			vCustomerUpdateComponent
		},
		template: `
		<section class="content" id="admin-googleads-index">
			<div class="row">
			    <div class="col-md-12">
			        <vCustomerUpdateComponent :id="userId"/>
			    </div>
			</div>
		</section>
		`
	});
});
