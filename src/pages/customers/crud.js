import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vCustomerCrudContainer from '@/components/vCustomerCrudContainer.vue'

Vue.use(VeeValidate, { fastExit: true });

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-container-crud-container",
		components: {
			vCustomerCrudContainer	
		}
	});
});
