import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vPushdyConfigurationContractBox from '@/components/vPushdyConfigurationContractBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vPushdyConfigurationContractBox",
		components: {
			vPushdyConfigurationContractBox	
		}
	});
});
