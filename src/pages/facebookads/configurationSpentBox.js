import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vFacebookadsConfigurationSpentBox from '@/components/vFacebookadsConfigurationSpentBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-spent-service-container",
		components: {
			vFacebookadsConfigurationSpentBox	
		}
	});
});
