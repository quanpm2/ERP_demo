import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vFacebookadsConfigurationContractBox from '@/components/vFacebookadsConfigurationContractBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vFacebookadsConfigurationContractBox",
		components: {
			vFacebookadsConfigurationContractBox
		}
	});
});
