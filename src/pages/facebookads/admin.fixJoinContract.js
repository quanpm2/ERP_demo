import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vFacebookadsFixJoinContract from '@/components/facebookads/FixJoinContract.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-facebookads-fix-join-contract",
		components: {
			vFacebookadsFixJoinContract
		},
	});
});
