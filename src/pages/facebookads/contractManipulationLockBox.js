import Vue from "vue/dist/vue.esm.js";
import VeeValidate, { Validator } from "vee-validate/dist/vee-validate.esm.js";
import vi from 'vee-validate/dist/locale/vi';
import vFacebookadsContractManipulationLockBox from "@/components/facebookads/vFacebookadsContractManipulationLockBox.vue";

Vue.use(VeeValidate);
Validator.localize('vi', vi);

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#facebookads-contract-manipulation-lock-box",
        components: {
            vFacebookadsContractManipulationLockBox,
        },
    });
});
