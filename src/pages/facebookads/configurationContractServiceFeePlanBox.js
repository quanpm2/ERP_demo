import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vFacebookadsConfigurationContractServiceFeePlanBox from '@/components/vFacebookadsConfigurationContractServiceFeePlanBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vFacebookadsConfigurationContractServiceFeePlanBox",
		components: {
			vFacebookadsConfigurationContractServiceFeePlanBox	
		}
	});
});
