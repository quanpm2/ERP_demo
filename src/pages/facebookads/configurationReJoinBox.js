import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vFacebookadsConfigurationReJoinBox from '@/components/vFacebookadsConfigurationReJoinBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-rejoin-service-container",
		components: {
			vFacebookadsConfigurationReJoinBox	
		}
	});
});