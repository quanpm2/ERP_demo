import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vOnewebDatatable from '@/components/vOnewebDatatable.vue'

Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {

	new Vue({
		el : "#app-container",
		components: {
			vOnewebDatatable	
		}
	});
});
