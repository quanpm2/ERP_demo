import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vFacebookadsConfigurationContractPromotionsBox from '@/components/vFacebookadsConfigurationContractPromotionsBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vFacebookadsConfigurationContractPromotionsBox",
		components: {
			vFacebookadsConfigurationContractPromotionsBox	
		}
	});
});
