import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vOneWebDatatableBkp from '@/components/vOneWebDatatableBkp.vue'

Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {

	new Vue({
		el : "#app-container",
		components: {
			vOneWebDatatableBkp	
		}
	});
});
