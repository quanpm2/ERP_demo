import Vue from "vue/dist/vue.esm.js";
import VeeValidate from "vee-validate/dist/vee-validate.esm.js";
import VeeLocaleVi from "vee-validate/dist/locale/vi.js";

import vOnewebDatatable from "@/components/vOnewebDatatable.vue";

Vue.use(VeeValidate, {
    locale: "vi",
    dictionary: {
        vi: { messages: VeeLocaleVi.messages },
    },
});

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#app-container",
        components: {
            vOnewebDatatable,
        },
        data: () => ({
            activeTab: window.location.hash.slice(1)
                ? window.location.hash.slice(1)
                : "data-list",
        }),
        methods: {
            changeTab(tabId) {
                this.activeTab = tabId;
                history.pushState({}, null, `#${tabId}`);
            },
        },
        template: `
        <section class="content" id="onead-index">
            <div class="row">
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
							<li :class="[activeTab == 'data-list' ? 'active' : '']">
			                	<a v-on:click="changeTab('data-list')" href="#data-list" data-toggle="tab">
			                		<i class="fa fa-list-ul"></i> Tổng quan dịch vụ
			                	</a>
			                </li>
			            </ul>
                        <div class="tab-content">
                            <div :class="['tab-pane', activeTab == 'data-list' ? 'active' : '']" id="data-list">
                                <vOnewebDatatable v-if="activeTab == 'data-list'"></vOnewebDatatable>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        `,
    });
});
