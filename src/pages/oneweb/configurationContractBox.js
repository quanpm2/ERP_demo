import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vOnewebConfigurationContractBox from '@/components/vOnewebConfigurationContractBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vOnewebConfigurationContractBox",
		components: {
			vOnewebConfigurationContractBox	
		}
	});
});
