import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vUnjoinContract from '@/components/googleads/vUnjoinContract.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-unjoin-service-container",
		components: {
			vUnjoinContract	
		}
	});
});