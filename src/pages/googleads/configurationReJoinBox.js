import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vGoogleadsConfigurationReJoinBox from '@/components/vGoogleadsConfigurationReJoinBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-rejoin-service-container",
		components: {
			vGoogleadsConfigurationReJoinBox	
		}
	});
});
