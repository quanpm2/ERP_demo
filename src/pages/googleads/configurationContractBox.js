import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vGoogleadsConfigurationContractBox from '@/components/vGoogleadsConfigurationContractBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vGoogleadsConfigurationContractBox",
		components: {
			vGoogleadsConfigurationContractBox	
		}
	});
});
