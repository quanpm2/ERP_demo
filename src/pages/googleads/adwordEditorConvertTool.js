import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vAdwordEditorConvertToolV2 from '@/components/googleads/vAdwordEditorConvertToolV2.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-container",
		components: {
			vAdwordEditorConvertToolV2	
		},
        template: "<vAdwordEditorConvertToolV2></vAdwordEditorConvertToolV2>"
	});
});
