import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vGoogleadsConfigurationContractPromotionsBox from '@/components/vGoogleadsConfigurationContractPromotionsBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vGoogleadsConfigurationContractPromotionsBox",
		components: {
			vGoogleadsConfigurationContractPromotionsBox	
		}
	});
});
