import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vGoogleadsConfigurationContractCuratorsBox from '@/components/vGoogleadsConfigurationContractCuratorsBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#vGoogleadsConfigurationContractCuratorsBox",
		components: {
			vGoogleadsConfigurationContractCuratorsBox	
		}
	});
});
