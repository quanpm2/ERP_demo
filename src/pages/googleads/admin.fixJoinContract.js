import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vGoogleadsFixJoinContract from '@/components/googleads/FixJoinContract.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-googleads-fix-join-contract",
		components: {
			vGoogleadsFixJoinContract
		},
	});
});
