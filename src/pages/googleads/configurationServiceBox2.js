import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vGoogleadsConfigurationServiceBox2 from '@/components/vGoogleadsConfigurationServiceBox2.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-configuration-service-box-container2",
		components: {
			vGoogleadsConfigurationServiceBox2	
		}
	});
});
