import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vDatatableBuilder from '@/components/vDatatableBuilder.vue'
import vSpendSyncContainer from '@/components/googleads/vSpendSyncContainer.vue'
import vGoogleadsAdsPending from '@/components/vGoogleadsAdsPending.vue'
import vGoogleadsUnconfiguredDatatable from '@/components/vGoogleadsUnconfiguredDatatable.vue'
import vSpendReportContainer from '@/components/googleads/vSpendReportContainer.vue'
import vHistory from '@/components/googleads/vHistory.vue'

import { ApiEndpoints } from '@/_config/api.endpoints.js'

import DateRangePicker from 'vue2-daterange-picker'

window.addEventListener('DOMContentLoaded', (event) => {

	new Vue({

		el : "#app-container",

		data () {

			let dateRanges = {};

			let today = new Date()
			today.setHours(0, 0, 0, 0)
			dateRanges['Hôm nay'] = [today, today]

			let yesterday = new Date()
			yesterday.setDate(today.getDate() - 1)
			yesterday.setHours(0, 0, 0, 0);
			dateRanges['Hôm qua'] = [yesterday, yesterday]

			let y = today.getFullYear()
			let m = today.getMonth()

			let thisMonthStart 	= new Date(y, m, 1)
			let thisMonthEnd 	= new Date(y, m + 1, 0)

			dateRanges['Tháng này'] = [thisMonthStart, thisMonthEnd]

			let i = 1
			while( i < 4)
			{
				let _start 	= new Date(y, (m-i), 1)
				let _end 	= new Date(y, (m-i) + 1, 0)
				dateRanges[`Tháng ${_start.getMonth()+1}`] = [_start, _end]
				i++
			}

			return {
				apiDataset: ApiEndpoints.valueOf('googleads') + '/ajax/dataset',
				APISpendDefaultParams: [],

				APISpend: null,
				APISpendByMonth: ApiEndpoints.valueOf('googleadsV2') + '/DataReport/monthly',
				APISpendByDateRange: ApiEndpoints.valueOf('googleadsV2') + '/DataReport/ranges',

				filter_position: 1,
				activeTab: window.location.hash.slice(1) ? window.location.hash.slice(1) : "tab_1",

				dateRange: {
					startDate: thisMonthStart,
					endDate: thisMonthEnd,
				},

				dateRanges : dateRanges
			}
		},

		created () {
			this.switchAPISpend()
		},

		methods: {

			switchAPISpend () {

				this.APISpend = null
				this.APISpendDefaultParams = []
				let _type = this.getAPITypeByDateRanges()
				
				if('MONTHLY' == _type)
				{
					let _m = this.dateRange.startDate.getMonth() + 1
					let _y = this.dateRange.endDate.getFullYear()

					this.APISpend = `${this.APISpendByMonth}?month=${_m}&year=${_y}`
                	this.APISpendDefaultParams 	= [ { month: _m, year: _y } ]

					return true
				}

				let _start = parseInt(this.dateRange.startDate.getTime()/1000)
				let _end = parseInt(this.dateRange.endDate.getTime()/1000)
				this.APISpend = `${this.APISpendByDateRange}/${_start}/${_end}`
				return true
			},

			getAPITypeByDateRanges () {

				let start = this.dateRange.startDate
				let end = this.dateRange.endDate

				/** Lấy theo từng ngày */
				if(start.toDateString() == end.toDateString()) return "DATERANGE"

				let _isSameMonth = start.getMonth() == end.getMonth()
				let _isSameYear = start.getFullYear() == end.getFullYear()

				/** Lấy theo khoảng thời gian */
				if(!_isSameMonth || !_isSameYear) return "DATERANGE"

				/** Trường hợp ngày bắt đầu và kết là cùng tháng và cùng năm */
				if(_isSameMonth && _isSameYear)
				{
					let thisMonthStart 	= new Date(start.getFullYear(), start.getMonth(), 1)
					let thisMonthEnd 	= new Date(start.getFullYear(), start.getMonth() + 1, 0)
					let isStartOfMonthDay = start.getDate() == thisMonthStart.getDate()
					let isEndOfMonthDay = end.getDate() == thisMonthEnd.getDate()

					/** Trường hợp lấy cả tháng */
					if(isStartOfMonthDay && isEndOfMonthDay) return 'MONTHLY'
				}

				/** Trường hợp còn lại sẽ được lấy theo khoảng thời gian */
				return 'DATERANGE'
			},

			DatesSelected () {
				this.switchAPISpend()
			},

			dateFormat (classes, date) {

				let _minDate = new Date()
				_minDate.setMonth(_minDate.getMonth() - 3)

				if (!classes.disabled)
				{
					classes.disabled = date.getTime() < _minDate
				}
				return classes
			},

			changeTab (tabId) {
				this.activeTab = tabId
				history.pushState({}, null, `#${tabId}`)
			}
		},

		components: {
			vDatatableBuilder,
			DateRangePicker,
            vSpendSyncContainer,
            vGoogleadsAdsPending,
            vGoogleadsUnconfiguredDatatable,
            vSpendReportContainer,
            vHistory
		},
		template: `
		<section class="content" id="admin-googleads-index">
			<div class="row">
			    <div class="col-md-12">
			        <div class="nav-tabs-custom">
			            <ul class="nav nav-tabs">
			                <li :class="[activeTab == 'tab_1' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_1')" href="#tab_1" data-toggle="tab">
			                		<i style="margin-right: 10px" class="fa fa-list-ul"></i> Tất cả dịch vụ
			                	</a>
			                </li>
                            <li :class="[activeTab == 'tab_2' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_2')" href="#tab_2" data-toggle="tab">
			                		<i style="margin-right: 10px" class="fa fa-exclamation-circle"></i> Hợp đồng chưa cấu hình
			                	</a>
			                </li>
			                <li :class="[activeTab == 'tab_3' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_3')" href="#tab_3" data-toggle="tab">
			                		<i style="margin-right: 10px" class="fa fa-pause"></i> Đang tạm ngưng
			                	</a>
			                </li>
							<li :class="[activeTab == 'tab_4' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_4')" href="#tab_4" data-toggle="tab">
			                		<i style="margin-right: 10px" class="fa fa-line-chart"></i> Báo cáo Spend
			                	</a>
			                </li>
                            <li :class="[activeTab == 'tab_5' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_5')" href="#tab_5" data-toggle="tab">
                                    <i style="margin-right: 10px" class="fa fa-refresh"></i> Đồng bộ spend
			                	</a>
			                </li>
                            <li :class="[activeTab == 'tab_6' ? 'active' : '']">
			                	<a v-on:click="changeTab('tab_6')" href="#tab_6" data-toggle="tab">
                                    <i style="margin-right: 10px" class="fa fa-history"></i> Lịch sử thao tác
			                	</a>
			                </li>
			            </ul>
			            <div class="tab-content">
			                <div :class="['tab-pane', activeTab == 'tab_1' ? 'active' : '']" id="tab_1">
			                   <v-datatable-builder 
                                    v-if="activeTab == 'tab_1'"
			                   		:is_filtering="false"
			                   		:is_ordering="true"
			                   		:is_download="true"
			                   		:per_page="50"
			                   		:filter_position="filter_position"
			                   		:remote_url="apiDataset"
									v-bind:is_load_config="false"
								></v-datatable-builder> 
			                </div>
                            <div :class="['tab-pane', activeTab == 'tab_2' ? 'active' : '']" id="tab_2">
                                <vGoogleadsUnconfiguredDatatable v-if="activeTab == 'tab_2'" />
			                </div>
			                <div :class="['tab-pane', activeTab == 'tab_3' ? 'active' : '']" id="tab_3">
                                <vGoogleadsAdsPending v-if="activeTab == 'tab_3'" />
			                </div>
							<div :class="['tab-pane', activeTab == 'tab_4' ? 'active' : '']" id="tab_4">
			                   <vSpendReportContainer v-if="activeTab == 'tab_4'" />
			                </div>
                            <div :class="['tab-pane', activeTab == 'tab_5' ? 'active' : '']" id="tab_5">
                                <vSpendSyncContainer v-if="activeTab == 'tab_5'" /> 
			                </div>
                            <div :class="['tab-pane', activeTab == 'tab_6' ? 'active' : '']" id="tab_6">
                                <vHistory v-if="activeTab == 'tab_6'" /> 
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</section>
		`
	});
});
