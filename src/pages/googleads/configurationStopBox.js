import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vGoogleadsConfigurationStopBox from '@/components/vGoogleadsConfigurationStopBox.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	new Vue({
		el : "#app-stop-process-service-container",
		components: {
			vGoogleadsConfigurationStopBox	
		}
	});
});
