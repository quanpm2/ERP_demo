import Vue from "vue/dist/vue.esm.js";
import VeeValidate from "vee-validate/dist/vee-validate.esm.js";
import vUtilities from "@/components/googleads/Utilities.vue";
Vue.use(VeeValidate);

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#app-container",
        components: {
            vUtilities,
        },
        template: `
        <section class="content" id="admin-facebookads-index">
            <div class="row">
                <div class="col-md-12">
                    <vUtilities></vUtilities>
                </div>
            </div>
        </section>`,
    });
});
