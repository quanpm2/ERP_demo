import Vue from "vue/dist/vue.esm.js";
import VeeValidate from "vee-validate/dist/vee-validate.esm.js";
import VeeLocaleVi from "vee-validate/dist/locale/vi.js";
import vStaffListDatatable from "@/components/vStaffListDatatable.vue";

Vue.use(VeeValidate, {
    locale: "vi",
    dictionary: {
        vi: { messages: VeeLocaleVi.messages },
    },
});

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#app-container",
        components: {
            vStaffListDatatable,
        },
        template: `
            <section class="content" id="staff-index">
            	  <div class="row">
            	      <div class="col-md-12">
                <div class="nav-tabs-custom">
            	              <div class="tab-content">
            	                  <div class="tab-pane active">
                            <vStaffListDatatable></vStaffListDatatable>
            	  				        </div>
            	              </div>
            	          </div>
            	      </div>
            	  </div>
            </section>
        `,
    });
});
