import Vue from "vue/dist/vue.esm.js";
import VeeValidate from "vee-validate/dist/vee-validate.esm.js";
import vCourseadsDatatable from "@/components/vCourseadsDatatable.vue";
import vCourseadsContractDatatable from "@/components/vCourseadsContractDatatable.vue";

Vue.use(VeeValidate);

window.addEventListener("DOMContentLoaded", (event) => {
    new Vue({
        el: "#app-container",
        components: {
            vCourseadsDatatable,
            vCourseadsContractDatatable,
        },
        data: () => ({
            activeTab: window.location.hash.slice(1)
                ? window.location.hash.slice(1)
                : "contract-list",
        }),
        methods: {
            changeTab(tabId) {
                this.activeTab = tabId;
                history.pushState({}, null, `#${tabId}`);
            },
        },
        template: `
        <section class="content" id="courseads-index">
            <div class="row">
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li :class="[activeTab == 'contract-list' ? 'active' : '']">
                                <a v-on:click="changeTab('contract-list')" href="#contract-list" data-toggle="tab">
                                    <i class="fa fa-list-ul"></i> Danh sách hợp đồng
                                </a>
                            </li>
                            <li :class="[activeTab == 'data-list' ? 'active' : '']">
			                	<a v-on:click="changeTab('data-list')" href="#data-list" data-toggle="tab">
			                		<i class="fa fa-list-ul"></i> Danh sách khoá học
			                	</a>
			                </li>
			            </ul>
                        <div class="tab-content">
                            <div v-if="'contract-list' == activeTab" :class="['tab-pane', activeTab == 'contract-list' ? 'active' : '']" id="contract-list">
                                <vCourseadsContractDatatable></vCourseadsContractDatatable>
                            </div>
                            <div v-if="'data-list' == activeTab" :class="['tab-pane', activeTab == 'data-list' ? 'active' : '']" id="data-list">
                                <vCourseadsDatatable></vCourseadsDatatable>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        `,
    });
});
