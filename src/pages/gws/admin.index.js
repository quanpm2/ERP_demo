import Vue from 'vue/dist/vue.esm.js'
import store from '@/store/store.js';
import vGwsDatatable from '@/components/gws/vGwsDatatable.vue';

window.addEventListener('DOMContentLoaded', (event) => {
    new Vue({
        el : "#app-container",
        store,
        data() {
            return {
                activeTab: window.location.hash.slice(1)
                    ? window.location.hash.slice(1)
                    : "tab_1",
            };
        },

        methods: {
            changeTab(tabId) {
                this.activeTab = tabId
                history.pushState({}, null, `#${tabId}`)
            }
        },

        components: {
            vGwsDatatable,
        },
        template: `
        <section class="content" id="admin-gws-index">
            <div class="row">
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li :class="[activeTab == 'tab_1' ? 'active' : '']">
                                <a v-on:click="changeTab('tab_1')" href="#tab_1" data-toggle="tab">
                                    <i style="margin-right: 10px" class="fa fa-list-ul"></i> Tất cả dịch vụ
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div :class="['tab-pane', activeTab == 'tab_1' ? 'active' : '']" id="tab_1">
                                <vGwsDatatable v-if="activeTab == 'tab_1'" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        `
    });
});
