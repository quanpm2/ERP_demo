import Vue from 'vue/dist/vue.esm.js'
import VeeValidate from 'vee-validate/dist/vee-validate.esm.js'
import vContractConfiguration from '@/components/gws/vContractConfiguration.vue'
Vue.use(VeeValidate);

window.addEventListener('DOMContentLoaded', (event) => {
	window.contract_configuration = new Vue({
        name: 'gws-contract-configuration',
		el : "#vContractConfiguration",
		components: {
			vContractConfiguration	
		}
	});
});
