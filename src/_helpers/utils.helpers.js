import _isEmpty from "lodash/isEmpty";

export const Utils = { tinValidator, phoneValidator };

function tinValidator(value) {
    // Validation Syntax with Vietnam TIN
    if(/^[0-9a-zA-Z]{10}(-\d{3})?$/g.test(value)) return true;

    // Validation Syntax with Australia TIN
    if(/^(((\d *?){11})|([0-9a-zA-Z]{2}-[0-9a-zA-Z]{7}))$/g.test(value)) return true;
    
    // Validation Syntax with Japan TIN
    if(/^[0-9]{12,13}$/g.test(value)) return true;

    // Validation Syntax with Hongkong TIN
    if(/^(([A-Z]\d{6}[0-9A])|([0-9]{8}-[0-9A-Z]{3}-[0-9A-Z]{2}-[0-9A-Z]{2}-[0-9A-Z]{1}))$/g.test(value)) return true;

    // Validation Syntax with Singapore TIN
    if(/^(([0-9]{8,9}[a-zA-Z])|((F000|F   )[0-9]{5}[a-zA-Z])|((A|4)[0-9]{7}[a-zA-Z])|((20|19)[0-9]{2}(LP|LL|FC|PF|RF|MQ|MM|NB|CC|CS|MB|FM|GS|DP|CP|NR|CM|CD|MD|HS|VH|CH|MH|CL|XL|CX|HC|RP|TU|TC|FB|FN|PA|PB|SS|MC|SM|GA|GB)[0-9]{4}[a-zA-Z]))$/g.test(value)) return true;

    // Validation Syntax with Taiwan TIN
    if(/^[0-9]{8}$/g.test(value)) return true;

    return false;
}

function phoneValidator(value) {
    // Validate VN phone number
    if(/^((03|05|07|08|09|01[2|6|8|9]|02[0|1|2|3|4|5|6|7|8|9]{1,2})+([0-9]{8})|(1900+([0-9]{4}|[0-9]{6})))$/g.test(value)) return true;

    // Validate Australia phone number
    if(/^(((\d *?){11})|([0-9a-zA-Z]{2}-[0-9a-zA-Z]{7}))$/g.test(value)) return true;

    // Validate US phone number
    if(/^\+61+[0-9]{9}$/g.test(value)) return true;

    // Validate South Korea phone number
    if(/^((001|\+82)+[0-9]{10})$/g.test(value)) return true;

    // Validate Hong Kong phone number
    if(/^((001|\+852)+[0-9]{8})$/g.test(value)) return true;

    // Validate Singapore phone number
    if(/^((001|\+65)+[0-9]{8})$/g.test(value)) return true;

    // Validate Taiwan phone number
    if(/^((0(2|3|4|5|6|7|8|9)|\+8869)+[0-9]{8})$/) return true;

    // Validate Poland phone number
    if(/^((0048|\+48)+[0-9]{9,10})$/) return true;

    return false;
}

export  function converNumber( number, local='en-US' ) {
    if(isNaN(number)) return 0
    return Number(number).toLocaleString(local)
}


export function getSaleId(){
    const sale_id_params = window.location.search
    const sale_id_url = new URLSearchParams(sale_id_params).get('sale_id')
    if(_isEmpty(sale_id_url)) return Number(sale_id) // sale_id được khai báo tại script global trong phần footer của page
    return Number(sale_id_url)
}

export function getGroupId(){
    const group_id_params = window.location.search
    const group_id_url = new URLSearchParams(group_id_params).get('group_id')
    if(_isEmpty(group_id_url)) return Number(group_id) // group_id được khai báo tại script global trong phần footer của page
    return Number(group_id_url)
}

export function isEmpty(value) {
    // Kiểm tra xem giá trị có phải là null hoặc undefined không
    if (value === null || value === undefined) {
      return true;
    }
  
    // Kiểm tra xem giá trị có phải là một chuỗi không và có độ dài bằng 0 không
    if (typeof value === "string" && value.trim() === "") {
      return true;
    }
  
    // Kiểm tra xem giá trị có phải là một mảng không và có độ dài bằng 0 không
    if (Array.isArray(value) && value.length === 0) {
      return true;
    }
  
    // Kiểm tra xem giá trị có phải là một đối tượng không và không có thuộc tính nào không
    if (typeof value === "object" && Object.keys(value).length === 0) {
      return true;
    }
  
    // Kiểm tra xem giá trị có phải là NaN không
    if (typeof value === "number" && isNaN(value)) {
      return true;
    }
  
    // Nếu không thuộc các trường hợp trên, coi giá trị không rỗng
    return false;
  }