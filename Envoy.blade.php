@setup
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();

    $repository_url = env('CI_REPOSITORY_URL', NULL);

    $commit_tag = env('CI_COMMIT_TAG', NULL);
    $commit_sha = env('CI_COMMIT_SHA', NULL);
    $commit_short_sha = env('CI_COMMIT_SHORT_SHA', NULL);

    $work_dir = env('CICD_WORK_DIR', NULL);
    $releases_dir = $work_dir  .'/releases';

    $now = time();
    $new_release_dir = $releases_dir .'/'. date('Ymd', $now) . '_' . $commit_short_sha;

    $day_delete_after = (int)env('CICD_SOURCE_EXPIRED_DAYS', 7) + 1;
    $max_dir_date = date('Ymd', strtotime("-{$day_delete_after} days"));

    $host = env('CICD_HOST', NULL);
    $user_name = env('CICD_USERNAME', NULL);

    $app_env = env('CICD_APP_ENV', NULL);
    
    $telegram_token = env('CICD_TELEGRAM_TOKEN', NULL);
    $telegram_chat_id = env('CICD_TELEGRAM_CHAT_ID', NULL);
@endsetup

@servers(['web' => $user_name . '@' . $host])

@story('deploy')
    clone_repository
    prepare_source
    update_symlinks
    clear_old_source
@endstory

@task('clone_repository')
    echo "Start clone repository"

    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    [ "$(ls -A {{ $new_release_dir }})" ] || git clone --depth 1 {{ $repository_url }} {{ $new_release_dir }}
    
    echo "Fetch source from commit {{ $commit_sha }} to directory {{ $new_release_dir }}"
    cd {{ $new_release_dir }}
    git fetch --all --tags

    echo "Checkout to tag"
    git checkout {{ $commit_tag }}

    echo "Finish clone repository"
@endtask

@task('prepare_source')
    echo "Start prepare source"
    cd {{ $new_release_dir }}

    echo "Create symlink .env file"
    ln -fs {{ $work_dir }}/environments/.env {{ $new_release_dir }}/application/.env

    echo "Create symlink config file"
    ln -fs {{ $work_dir }}/environments/config.php {{ $new_release_dir }}/application/config/development/config.php

    echo "Create symlink cache directory"
    ln -fs {{ $work_dir }}/environments/cache {{ $new_release_dir }}/application/cache

    echo "Create symlink files directory"
    [ -d {{ $work_dir }}/environments/files ] || mkdir {{ $work_dir }}/environments/files
    cp -rfv {{ $new_release_dir }}/files {{ $work_dir }}/environments
    rm -rfv {{ $new_release_dir }}/files
    ln -fs {{ $work_dir }}/environments/files {{ $new_release_dir }}/files
    chmod -R 775 {{ $work_dir }}/environments/files

    echo "Create symlink custom contract directory"
    [ -d {{ $work_dir }}/environments/contract_custom ] || mkdir {{ $work_dir }}/environments/contract_custom
    cp -rfv {{ $new_release_dir }}/application/modules/contract/views/admin/preview/custom/* {{ $work_dir }}/environments/contract_custom
    rm -rfv {{ $new_release_dir }}/application/modules/contract/views/admin/preview/custom
    ln -fs {{ $work_dir }}/environments/contract_custom {{ $new_release_dir }}/application/modules/contract/views/admin/preview/custom
    chmod -R 775 {{ $work_dir }}/environments/contract_custom

    echo "Create symlink vendor directory"
    ln -nfs {{ $work_dir }}/environments/vendor {{ $new_release_dir }}/application/vendor

    echo "Install composer package"
    cd {{ $new_release_dir }}/application
    composer install --prefer-dist --no-scripts -q -o --verbose

    echo "Install nodejs package"
    cd {{ $new_release_dir }}
    $(which yarn) install --modules-folder {{ $work_dir }}/environments/node_modules

    echo "Create symlink node_modules directory"
    ln -nfs {{ $work_dir }}/environments/node_modules {{ $new_release_dir }}/node_modules

    echo "Build UI bundle"
    cd {{ $new_release_dir }}
    $(which yarn) build

    echo "Finish prepare source"
@endtask

@task('update_symlinks')
    echo "Start update symlinks"
    
    ln -nfs {{ $new_release_dir }} {{ $work_dir }}/current

    echo "Finish update symlinks"
@endtask

@task('clear_old_source')
    echo "Start clear old source in {{ $releases_dir }}"

    max_dir_date=$((({{ $max_dir_date }})))
    for dir_name in $(ls {{ $releases_dir }})
    do
        if [[ $dir_name =~ ^([0-9]{8})_.*$ ]]
        then
            dir_date="${BASH_REMATCH[1]}"
            dir_date=$(($dir_date))
        
            if [[ $dir_date -gt $max_dir_date ]]
            then
                continue
            fi
            
            delete_path="{{ $releases_dir }}/$dir_name"
            echo "Remove dir $delete_path"
            $(rm -rf $delete_path)
        fi
    done

    echo "Finish clear old source in {{ $releases_dir }}"
@endtask

@error
    @telegram($telegram_token, $telegram_chat_id, '[ERP system] [' . $commit_short_sha . '] gặp lỗi ở bước ' . $task . ' tại môi trường [' . $app_env . ']. Xách dép chạy ngay đi.')
@enderror

@success
    @telegram($telegram_token, $telegram_chat_id, '[ERP system] [' . $commit_short_sha . '] đã triển khai lên môi trường [' . $app_env . ']')
@endsuccess
